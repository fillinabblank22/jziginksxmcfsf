﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceFilteringList.ascx.cs" Inherits="Orion_InterfacesDiscovery_Controls_Discovery_InterfaceFilteringList" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register Src="~/Orion/InterfacesDiscovery/Controls/Discovery/InterfaceFilterExpression.ascx" TagPrefix="orion" TagName="InterfaceFilterExpression" %>

<input id="interfaceListFilteringServiceUrl" type="hidden" value="<%=(this.FilteringServiceUrl)%>"/>

<orion:Include runat="server" File="InterfacesDiscovery/Controls/Discovery/InterfaceFiltering.js" />
<orion:Include runat="server" File="InterfacesDiscovery/Controls/Discovery/InterfaceFiltering.css" />

<script id="SelectedPropertiesTemplate" type="text/x-template">
    <table>
        <tr>
            <th class="status"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_36)%></th>
            <th class="port"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_37)%>&nbsp;<img src="/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Icon.Info.png" id="PortInfoIcon" class="link"></th>
            <th class="protocol {{ ShowProtocols ? '' : 'hidden' }}"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_38)%></th>
            <th class="hardware"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_39)%></th>
        </tr>
        <tr>
            <td class="status">{{ AddCheckboxWithID('up','propertyCheck status', 2) }} <img src="/NetPerfMon/images/small-up.gif"> <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_40)%></td>
            <td class="port">{{ AddCheckboxWithID('trunk','propertyCheck port', 2) }} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_43)%></td>
            <td class="protocol {{ ShowProtocols ? '' : 'hidden' }}">{{ AddCheckboxWithID('snmp','propertyCheck protocol', 2) }} SNMP</td>
            <td class="hardware">{{ AddCheckboxWithID('physical','propertyCheck type', 2) }} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_46)%></td>
        </tr>
        <tr>
            <td class="status">{{ AddCheckboxWithID('down','propertyCheck status', 0) }} <img src="/NetPerfMon/images/small-down.gif"> <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_41)%></td>
            <td class="port">{{ AddCheckboxWithID('access','propertyCheck port', 2) }} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_44)%></td>
            <td class="protocol {{ ShowProtocols ? '' : 'hidden' }}">{{ AddCheckboxWithID('wmi','propertyCheck protocol', 2) }} WMI</td>
            <td class="hardware">{{ AddCheckboxWithID('virtual','propertyCheck type', 2) }} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_47)%></td>
        </tr>
        <tr>
            <td class="status">{{ AddCheckboxWithID('shutdown','propertyCheck status', 0) }} <img src="/NetPerfMon/images/small-shutdown.gif"> <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_42)%></td>
            <td class="port">{{ AddCheckboxWithID('unknown','propertyCheck port', 2) }} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_45)%></td>
            <td class="protocol {{ ShowProtocols ? '' : 'hidden' }}"></td>
            <td class="hardware">{{ AddCheckboxWithID('unknown','propertyCheck type', 2) }} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_45)%></td>
        </tr>
    </table>
</script>

<script id="GroupsTemplate" type="text/x-template">
    
    <div class="groupTitle ReportHeader">
             <span class="groupCell expand">&nbsp;</span>
             <span class="groupCell checkbox">{{ AddCheckbox('GlobalCheck', '2') }}</span>
             <span class="groupCell selected"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_48)%></span>
             <span class="groupCell available">(<%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_49)%>)</span>
             <span class="groupCell groupName">{{ GroupByText }}</span>
    </div>
    
    {# _.each(d, function(current, index) { #}
    <div class="group {{ index % 2 == 0 ? '' : 'ZebraStripe'}}" id="{{ current.ID }}">
             <span class="groupCell expand exp">{{ AddExpandButton(true) }}</span>
             <span class="groupCell checkbox">{{ AddCheckbox('GrpCheck', current.Selected == 0 ? '0' : current.Selected == current.Available ? '2' : '1') }}</span>
             <span class="groupCell selected">{{ current.Selected }}</span>
             <span class="groupCell available">(<%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_61)%> <span class="value">{{ current.Available }}</span>)</span>
             <span class="groupCell groupName">{{ current.Icon == '' ? '' : '<img src="/NetPerfMon/images/Interfaces/' + current.Icon + '" onError="this.onerror=null;this.src=\'/NetPerfMon/images/Interfaces/0.gif\';">' }}&nbsp;{{ current.Name }}</span>
    </div>
    {# }); #}
</script>

<script id="ListOfInterfacesEmptyTemplate_selected" type="text/x-template">
    <table class="subgrid" groupID="{{ d.groupID }}">
        <tr class="interfaceRow unselectedRow">
            <td class="buttonSize">&nbsp;</td>
            <td class="buttonSize">&nbsp;</td>
            <td colspan="8"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_50)%></td>
        </tr>
        <tr class="interfaceRow">
            <td class="buttonSize">&nbsp;</td>
            <td class="buttonSize">&nbsp;</td>
            <td colspan="8">{{d.all}} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_51)%> |
                <span class="changeShow link" value="unselected"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_52)%> &#187;</span> |
                <span class="changeShow link" value="all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_53)%> &#187;</span>
            </td>
        </tr>
    </table>
</script>

<script id="ListOfInterfacesEmptyTemplate_unselected" type="text/x-template">
    <table class="subgrid" groupID="{{ d.groupID }}">
        <tr class="interfaceRow unselectedRow">
            <td class="buttonSize">&nbsp;</td>
            <td class="buttonSize">&nbsp;</td>
            <td colspan="8"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_54)%></td>
        </tr>
        <tr class="interfaceRow">
            <td class="buttonSize">&nbsp;</td>
            <td class="buttonSize">&nbsp;</td>
            <td colspan="8">{{d.all}} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_55)%> |
                <span class="changeShow link" value="selected"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_56)%> &#187;</span> |
                <span class="changeShow link" value="all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_53)%> &#187;</span>
            </td>
        </tr>
    </table>
</script>

<script id="ListOfInterfacesTemplate" type="text/x-template">
    <table class="subgrid" groupID="{{ d.groupID }}">
        <tr>
            <th class="buttonSize">&nbsp;</th>
            <th class="buttonSize">&nbsp;</th>
            <th class="separator iType {{ HideColumn('type') }}"><%=(this.TypeLabelText)%></th>
            <th class="separator iName"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_20)%></th>
            <th class="separator nName {{ HideColumn('node') }}"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_17)%></th>
            <th class="separator iDescr"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_21)%></th>
            <th class="separator iAlias {{ HideColumn('alias') }}"><%=(this.AliasLabelText)%></th>
            <th class="separator iPort"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_37)%></th>
            <th class="separator iProtocol"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_38)%></th>
            <th class="separator iPhysical"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_39)%></th>
            <th class="separator iVlanID">VLAN ID</th>
        </tr>
        {{ _.template($('#ListOfInterfaceSubTemplate').html(), {'d': d}) }}
    </table>
</script>

<script id="ListOfInterfaceSubTemplate" type="text/x-template">
        {#  _.each(d.Interfaces, function(current, index) { #}
<tr id="{{current.ID}}" class="interfaceRow {{ current.Selected ? '' : 'unselectedRow' }} {{ ((d.Left == 0) && (index == (d.count - 1))) ? 'last' : '' }}">
<td>&nbsp;</td>
<td>{{ AddCheckbox('IntCheck', current.Selected ? '2' : '0') }}</td>
<td class="separator {{ HideColumn('type') }}">{{current.Type}}</td>
<td class="separator"><img src="/NetPerfMon/images/small-{{current.Icon}}.gif">&nbsp;{{ current.Name }}</td>
<td class="separator {{ HideColumn('node') }}">{{current.Node}}</td>
<td class="separator">{{ current.Descr }}</td>
<td class="separator {{ HideColumn('alias') }}">{{current.Alias}}</td>
<td class="separator">{{ current.Port }}</td>
<td class="separator">{{ current.Prot }}</td>
<td class="separator">{{ current.Virt }}</td>
<td class="separator">{{ current.Vlans }}</td>
</tr>
        {# }); #}

        {# if(d.Left > 0) { #}
        <tr class="pager">
            <td colspan="2">&nbsp;</td>
            <td colspan="8">{{d.Left}} <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_57)%> |
                <span class="MoreInterfaces link" offset="{{d.Index}}" blockSize="{{d.block}}" groupID="{{d.groupID}}"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_59)%> &#187;</span> |
                <span class="MoreInterfaces link" offset="{{d.Index}}" blockSize="{{d.Left}}" groupID="{{d.groupID}}"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_58)%> &#187;</span>
            </td>
        </tr>    
        {# } #}
</script>
<div id="ShowError" class="hidden">
	<div id="message"></div>
	<pre id="stackTrace"></pre>
</div>

<div id="loader" class="hidden">
    <img src="/Orion/images/animated_loading_sm3_whbg.gif"/>
    <span class="title"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_213 %>" /></span>
</div>

<div id="noResults" class="hidden">
    <span class="ActionName"><%=(this.NoResultsLabelText)%></span><br /><br />
    <%= String.Format(this.NoResultsInfoText, String.Format(this.NoResultsKbLinkUrl, Resources.CoreWebContent.CurrentHelpLanguage)) %>
    <br /><br />
    <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_3)%>    
</div>

<div id="results" class="hidden">
<span class="ActionName"><%=(this.HeaderText)%></span><br />
<br />
<div style="text-align: left; margin-bottom: 5px;"><span class="sw-suggestion sw-suggestion-noicon"><span class="selectedInterfaces">0</span> <%=(this.NumOfItemsSelectedText)%></span></div>
<div>
    <div id="SelectionCriteria"  class="grid">
        <table class="title ">
            <tr>
                <td><label><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_6)%></label></td>
            </tr>
        </table>
        <div class="body">
            <label><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_7)%></label>
            <div id="SelectedProperties" class="panel"></div>
               
            <orion:InterfaceFilterExpression ID="InterfaceFilterExpression1" runat="server" TypeLabelText="<%# this.TypeLabelText%>" AliasLabelText="<%# this.AliasLabelText%>" />                

            <a id="ReselectButton" class="sw-btn sw-btn-secondary"><span class="sw-btn-c"><span class="sw-btn-t"><%=(this.ReselectItemsText)%></span></span></a>
        </div>
    </div>
    <br />
    <div class="grid">
        <table class="title">
            <tr>
                <td width="470px"><label><%=(this.ListOfItemsText)%></label></td>

                <td style="text-align:right; padding-right: 5px; ">
                    <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_11)%>
                    <select id="GroupBy">
                        <option value="<%=Convert.ToString(GroupByValue.Type).ToLower()%>"><%=(this.TypeLabelText)%></option>
                        <option value="<%=Convert.ToString(GroupByValue.Node).ToLower()%>"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_17)%></option>
                        <option value="<%=Convert.ToString(GroupByValue.Alias).ToLower()%>"><%=(this.AliasLabelText)%></option>
                        <option value="<%=Convert.ToString(GroupByValue.Vlan).ToLower()%>">VLAN ID</option>
                        <option value="<%=Convert.ToString(GroupByValue.PortType).ToLower()%>"><%= (Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_64) %></option>
                    </select>
            
                    <%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_12)%>
                    <select id="Show">
                        <option value="all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_13)%></option>
                        <option value="selected"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_14)%></option>
                        <option value="unselected"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_15)%></option>
                    </select>
                </td>
            </tr>
        </table>
        <div id="ListOfInterfaces"></div>
    </div>
    <div style="text-align: right; margin-top:5px;"><span class="sw-suggestion sw-suggestion-noicon"><span class="selectedInterfaces">0</span> <%=(this.NumOfItemsSelectedText)%></span></div>
</div>
</div>
<div id="PortInfoTooltip" class="tooltip sw-suggestion sw-suggestion-noicon">
<%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_63)%>
</div>
<script type="text/javascript">
    $('#GroupBy option[value="<%= GroupByOption %>"]').prop('selected', true);
    function GetAdvancedExpression() { 
        return $('#<%= this.InterfaceFilterExpression1.ClientID %>').data('get_expression_callback')(); 
    }
</script>