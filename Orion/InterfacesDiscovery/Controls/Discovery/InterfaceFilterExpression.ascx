﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceFilterExpression.ascx.cs" Inherits="Orion_InterfacesDiscovery_Controls_Discovery_InterfaceFilterExpression" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<orion:Include runat="server" File="InterfacesDiscovery/Controls/Discovery/InterfaceFilterExpression.js" />
<orion:Include runat="server" File="InterfacesDiscovery/Controls/Discovery/InterfaceFiltering.css" />

<script id="ExpressionTemplate" type="text/x-template">
    <div class="expression">
        <span class="expressionCell">
            <select class="property">
                <option value="type"><%=(this.TypeLabelText)%></option>
                <option value="name"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_20)%></option>
                <option value="descr"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_21)%></option>
                <option value="alias"><%=(this.AliasLabelText)%></option>
                <option value="node"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_17)%></option>
                <option value="all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_22)%></option>
                <option value="vlan">VLAN ID</option>
            </select>
        </span>
        <span class="expressionCell">
            <select class="operator">
                <option value="any"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_24)%></option>
                <option value="all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_25)%></option>
                <option value="equals"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_26)%></option>
                <option value="!all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_27)%></option>
                <option value="!any"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_28)%></option>
                <option value="!equals"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_29)%></option>
                <option value="regex"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_30)%></option>
                <option value="!regex"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_31)%></option>
            </select>
            <select class="operator vlan hidden">
                <option value="#any"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_32)%></option>
                <option value="#all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_33)%></option>
                <option value="!#all"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_34)%></option>
                <option value="!#any"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_35)%></option>
            </select>
        </span>
        <span class="expressionCell">
            <input type="text" class="value" onkeypress="return (event.keyCode != 13)">
        </span>
        <span class="expressionCell remove">
            <img src="/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Button.Remove.png">
        </span>
        <span class="expressionCell add hidden">
            <img src="/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Button.Add.png">
        </span>
    </div>
</script>

<script id="ExpressionNinjaTemplate" type="text/x-template">
    <div id="ExpressionNinja" class="hidden link">
        <img src="/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Icon.Ninja.png"> 
        <small>
        <orion:HelpLink ID="ExpressionsHelpLink" runat="server" HelpUrlFragment="OrionCoreAGRegularExpressions"
        HelpDescription="<%$ Resources: InterfacesDiscoveryWebContent, INTERFACESFILTERING_MP0_62%>" CssClass="" />
        </small>
    </div>
</script>

<div id="<%= this.ClientID %>" class="FilterExpression">

    <div id="AdvancedOptions" class="hidden">
      <br />
      <span class="ButtonCollapse" style="display: none;">&nbsp;</span>
      <label><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_8)%></label>
      <div id="SelectedKeywords" class="panel"></div>
    </div>
    <br />
    <div>
      <span id="ShowAdvancedOptions" class="link"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_9)%></span>    
    </div>

    <input type="hidden" runat="server" id="SerializedExpression" class="SerializedExpression" />

</div>