﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SolarWinds.Orion.Web;

public partial class Orion_InterfacesDiscovery_Controls_Discovery_InterfaceFilterExpression : System.Web.UI.UserControl
{
    /// <summary>
    /// Interface type label text
    /// </summary>
    public string TypeLabelText { get; set; }

    /// <summary>
    /// Interface alias label text
    /// </summary>
    public string AliasLabelText { get; set; }
    
    /// <summary>
    /// List of expressions represented by the control
    /// </summary>
    public List<InterfaceFiltering.Expression> Expressions {
        get
        {
            return JsonConvert.DeserializeObject<List<InterfaceFiltering.Expression>>(this.SerializedExpression.Value);
        }
        set
        {
            this.SerializedExpression.Value = JsonConvert.SerializeObject(value);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        ScriptManager.RegisterStartupScript(this, this.GetType(), this.ClientID + "Startup", "$(function() { initInterfaceFilter('" + this.ClientID + "');}); ", true);
        ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), this.ClientID + "Submit", "$('#" + this.ClientID + "').data('store_expression_callback')();");
    }
}
