﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_InterfacesDiscovery_Controls_Discovery_InterfaceFilteringList : System.Web.UI.UserControl
{
    /// <summary>
    /// URL to interface filtering back-end Web service (.asmx)
    /// </summary>
    public string FilteringServiceUrl { get; set; }

    /// <summary>
    /// URL to KB article displayed when discovery finds no interfaces
    /// </summary>
    public string NoResultsKbLinkUrl { get; set; }

    /// <summary>
    /// Text displayed when discovery finds no interfaces
    /// </summary>
    public string NoResultsLabelText { get; set; }

    /// <summary>
    /// More detailed text displayed when discovery finds no interfaces
    /// </summary>
    public string NoResultsInfoText { get; set; }

    /// <summary>
    /// Main header displayed on resource
    /// </summary>
    public string HeaderText { get; set; }

    /// <summary>
    /// Text used to compose number of selected items bubble
    /// </summary>
    public string NumOfItemsSelectedText { get; set; }

    /// <summary>
    /// Text displayed on reselect interfaces button
    /// </summary>
    public string ReselectItemsText { get; set; }

    /// <summary>
    /// Header for results grid
    /// </summary>
    public string ListOfItemsText { get; set; }

    /// <summary>
    /// Interface type label text
    /// </summary>
    public string TypeLabelText { get; set; }

    /// <summary>
    /// Group by option text
    /// </summary>
    public string GroupByOption { get; set; }
    /// <summary>
    /// Interface alias label text
    /// </summary>
    public string AliasLabelText { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
public enum GroupByValue
{
    Type,
    Node,
    Alias,
    Vlan,
    PortType
}