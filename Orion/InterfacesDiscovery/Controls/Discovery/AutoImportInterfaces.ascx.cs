﻿using SolarWinds.Logging;
using SolarWinds.Orion.Web.Discovery;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Interfaces.Common.Models.Discovery;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Interfaces.Common.Enums;
using SolarWinds.Orion.Core.Models.Enums;

public partial class Orion_InterfacesDiscovery_Controls_Discovery_AutoImportInterfaces : AutoImportWizardStep
{
    private static Log log = new Log();
    private const string IfStatusIconFormat = "<img src=\"/NetPerfMon/images/small-{0}.gif\" />";

    private Dictionary<string, IfAutoImportStatus> IfStatusCollection = new Dictionary<string, IfAutoImportStatus>()
    {
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_40, IfAutoImportStatus.Up },
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_41, IfAutoImportStatus.Down },
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_42, IfAutoImportStatus.Shutdown }
    };

    private Dictionary<string, VlanPortType> PortModeCollection = new Dictionary<string, VlanPortType>()
    {
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_43, VlanPortType.Trunk },
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_44, VlanPortType.Access},
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_45, VlanPortType.Unknown}
    };

    private Dictionary<string, bool?> HardwateTypeCollection = new Dictionary<string, bool?>()
    {
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_46, (bool?)false },
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_47, (bool?)true },
        { Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_45, (bool?)null }
    };

    protected InterfacesDiscoveryPluginConfiguration Config
    {
        get
        {
            var config = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<InterfacesDiscoveryPluginConfiguration>();
            if (config == null)
            {
                config = new InterfacesDiscoveryPluginConfiguration();
                ConfigWorkflowHelper.DiscoveryConfigurationInfo.AddDiscoveryPluginConfiguration(config);
            }
            return config;
        }
    }

    public override string Name
    {
        get
        {
            return Resources.InterfacesDiscoveryWebContent.AutoImportWizardInterfacesStepName;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        EnsureDataBound();
        
        this.InterfaceFilterExpression1.TypeLabelText = Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_16; 
        this.InterfaceFilterExpression1.AliasLabelText = Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_18;
    }

    private void EnsureDataBound()
    {
        if (InterfaceStatusCheckboxes.Items.Count == 0)
        {
            InterfaceStatusCheckboxes.DataTextField = "DisplayName";
            InterfaceStatusCheckboxes.DataValueField = "Value";
            InterfaceStatusCheckboxes.DataSource = IfStatusCollection.Select(status => new { 
                DisplayName = string.Format(IfStatusIconFormat, status.Value.ToString()) + status.Key,
                Value = (int)status.Value
            });
            InterfaceStatusCheckboxes.DataBind();
        }
        if (InterfacePortModeCheckboxes.Items.Count == 0)
        {
            InterfacePortModeCheckboxes.DataTextField = "DisplayName";
            InterfacePortModeCheckboxes.DataValueField = "Value";
            InterfacePortModeCheckboxes.DataSource = PortModeCollection.Select(port => new { DisplayName = port.Key, Value = (int)port.Value });
            InterfacePortModeCheckboxes.DataBind();
        }
        if (InterfaceHardwareCheckboxes.Items.Count == 0)
        {
            InterfaceHardwareCheckboxes.DataTextField = "DisplayName";
            InterfaceHardwareCheckboxes.DataValueField = "Value";
            InterfaceHardwareCheckboxes.DataSource = HardwateTypeCollection.Select(hw => new { DisplayName = hw.Key, Value = (bool?)hw.Value });
            InterfaceHardwareCheckboxes.DataBind();
        }
    }

    public override void LoadData()
    {
        EnsureDataBound();

        // set defaults for autoimport (if needed)
        Config.SetDefaults();

        foreach (ListItem item in InterfaceStatusCheckboxes.Items)
            item.Selected = Config.AutoImportStatus.Contains(AsStatus(item.Value));

        foreach (ListItem item in InterfacePortModeCheckboxes.Items)
            item.Selected = Config.AutoImportVlanPortTypes.Contains(AsPortMode(item.Value));

        foreach (ListItem item in InterfaceHardwareCheckboxes.Items)
            item.Selected = Config.AutoImportVirtualTypes.Contains(AsHardwareType(item.Value));

        this.InterfaceFilterExpression1.Expressions = Config.AutoImportExpressionFilter;
    }

    public override void SaveData()
    {
        EnsureDataBound();

        Config.AutoImportStatus = InterfaceStatusCheckboxes.Items
            .OfType<ListItem>()
            .Where(item => item.Selected)
            .Select(item => AsStatus(item.Value)).ToList();

        Config.AutoImportVlanPortTypes = InterfacePortModeCheckboxes.Items
            .OfType<ListItem>()
            .Where(item => item.Selected)
            .Select(item => AsPortMode(item.Value)).ToList();

        Config.AutoImportVirtualTypes = InterfaceHardwareCheckboxes.Items
            .OfType<ListItem>()
            .Where(item => item.Selected)
            .Select(item => AsHardwareType(item.Value)).ToList();

        Config.AutoImportExpressionFilter = this.InterfaceFilterExpression1.Expressions;
    }

    private static IfAutoImportStatus AsStatus(string value)
    {
        return (IfAutoImportStatus)int.Parse(value);
    }

    private static VlanPortType AsPortMode(string value)
    {
        return (VlanPortType)int.Parse(value);
    }

    private static bool? AsHardwareType(string value)
    {
        return !string.IsNullOrWhiteSpace(value) ? (bool?)bool.Parse(value) : (bool?)null;
    }
}