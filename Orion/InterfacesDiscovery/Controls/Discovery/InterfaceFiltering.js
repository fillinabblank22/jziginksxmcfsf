﻿var GroupBy = null;             // group by - value of selected item
var GroupByText = null;         // group by - text of selected item
var Show = null;                // show - value of selected item
var GroupID = null;             // id of group that is currently expanding
var BlockSize = 10;             // number of interfaces that is send at once
var SelectedInterfaces = 0;     // global number of selected interfaces
var AvailableInterfaces = 0;    // global number of all interfaces
var ShowProtocols = true;       // show Protocol (WMI & SNMP)

function OnError(status) {
    // hide current content
    $("#noResults").attr('class', 'hidden');
    $("#results").attr('class', 'hidden');
    $("#loader").attr('class', 'hidden');
    $("#ShowError").removeClass('hidden');

    // show error message
    var r = $.parseJSON(status.responseText);
    $("#ShowError #message").text(r.Message);
    $("#ShowError #stackTrace").text(r.StackTrace);
}

function CallService(method, data, onSuccess) {
    var serviceUrl = $("input#interfaceListFilteringServiceUrl").val();

    $.ajax({
        type: "POST",
        url: serviceUrl + method,
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: OnError,
        success: onSuccess
    });
}

// called once after page is loaded
function Initialization() {
    CallService("Initialization", "{}", function (data, status) {  // todo: load properties settings from server?
        ShowProtocols = data.d.ShowProtocols;

        var template = $('#SelectedPropertiesTemplate').html();
        var filledTemplate = _.template(template, "{}");
        $('#SelectedProperties').html(filledTemplate);

        OnReselectInterfaces();

        // show tooltip when hover
        $("#PortInfoIcon").hover(function () {
            var pos = $("#PortInfoIcon").position();
            var tooltip = $("#PortInfoTooltip");
            $(tooltip).css('top', pos.top).css('left', pos.left + 20);
            $(tooltip).show();

        }, function () {
            $("#PortInfoTooltip").hide();
        });

    });
}

function OnGroupByChange() {
    CallService("GetGroups", "{'type':'" + GroupBy + "', 'show':'" + Show + "'}", function (data, status) {
        var template = $('#GroupsTemplate').html();
        var filledTemplate = _.template(template, data);
        $('#ListOfInterfaces').html(filledTemplate);

        OnSelectedInterfacesChange();
    });
}

function OnReselectInterfaces() {

    var statuses = new Array();
    var types = new Array();
    var ports = new Array();
    var protocols = new Array();

    $.each($("#SelectedProperties .propertyCheck.status"), function (i, o) {
        if ($(o).attr('state') == 2) {
            // add selected properties
            statuses.push($(o).attr('id'));
        }
    });

    $.each($("#SelectedProperties .propertyCheck.type"), function (i, o) {
        if ($(o).attr('state') == 2) {
            // add selected properties
            types.push($(o).attr('id'));
        }
    });

    $.each($("#SelectedProperties .propertyCheck.port"), function (i, o) {
        if ($(o).attr('state') == 2) {
            // add selected properties
            ports.push($(o).attr('id'));
        }
    });

    $.each($("#SelectedProperties .propertyCheck.protocol"), function (i, o) {
        if ($(o).attr('state') == 2) {
            // add selected properties
            protocols.push($(o).attr('id'));
        }
    });

    CallService("ReselectInterfaces", "{'statuses':'" + statuses + "', 'types':'" + types + "', 'ports':'" + ports + "', 'protocols':'" + protocols + "', 'expressions':" + GetAdvancedExpression() + "}", function (data, status) {
        SelectedInterfaces = data.d.Selected;
        AvailableInterfaces = data.d.Available;

        // hide loader
        $("#loader").attr('class', 'hidden');

        if (AvailableInterfaces == 0) {
            $("#noResults").removeClass('hidden');
            $("#results").attr('class', 'hidden');
        }
        else {
            $("#results").removeClass('hidden');
            $("#noResults").attr('class', 'hidden');

            OnGroupByChange();
        }
    });
}

function OnGroupExpand() {
    // do nothing if we are already expanding something
    if (GroupID != null) return;

    GroupID = $(this).parent().attr('id');

    $(this).parent().addClass("selected");

    CallService("GetInterfaces", "{'type':'" + GroupBy + "', 'value':'" + GroupID + "', 'show': '" + Show + "', 'offset':'0', 'items':'" + BlockSize + "'}", function (data, status) {
        data.d.groupID = GroupID;
        data.d.header = true;
        data.d.block = (data.d.Left < BlockSize) ? data.d.Left : BlockSize;
        data.d.count = data.d.Interfaces.length;

        if (data.d.Interfaces.length > 0) {
            var template = $('#ListOfInterfacesTemplate').html();
            var filledTemplate = _.template(template, data);
            $("#ListOfInterfaces .group#" + GroupID).after(filledTemplate);
        }
        else {
            data.d.all = $("#ListOfInterfaces .group#" + GroupID + " .available .value").html();

            var template = $('#ListOfInterfacesEmptyTemplate_' + Show).html();
            var filledTemplate = _.template(template, data);
            $("#ListOfInterfaces .group#" + GroupID).after(filledTemplate);
        }

        GroupID = null;
    });

    // change button class and icon
    $(this).removeClass('exp');
    $(this).addClass('col');
    $(this).html(AddCollapseButton());
}

function OnGroupCollapse() {
    var groupID = $(this).parent().attr('id');

    $(this).parent().removeClass("selected");

    // remove interfaces
    $("#ListOfInterfaces .subgrid[groupID='" + groupID + "']").remove();

    // change button class and icon
    $(this).removeClass('col');
    $(this).addClass('exp');
    $(this).html(AddExpandButton());
}

function OnMoreInterfaces() {
    // do nothing if we are already expanding something
    if (GroupID != null) return;

    GroupID = $(this).attr('groupID');
    var offset = $(this).attr('offset');
    var blockSize = $(this).attr('blockSize');

    CallService("GetInterfaces", "{'type':'" + GroupBy + "', 'value':'" + GroupID + "', 'show': '" + Show + "', 'offset':'" + offset + "', 'items':'" + blockSize + "'}", function (data, status) {
        data.d.groupID = GroupID;
        data.d.header = false;
        data.d.block = (data.d.Left < BlockSize) ? data.d.Left : BlockSize;
        data.d.count = data.d.Interfaces.length;

        // remove old pager
        $("#ListOfInterfaces .subgrid[groupID='" + GroupID + "'] tr.pager").remove();

        var template = $('#ListOfInterfaceSubTemplate').html();
        var filledTemplate = _.template(template, data);

        // add after last row of subgrid
        $("#ListOfInterfaces .subgrid[groupID='" + GroupID + "'] tr:last").after(filledTemplate);

        GroupID = null;
    });
}


function OnChangeShowLink() {
    $("#Show").val($(this).attr('value'));
    Show = $("#Show").val();
    OnGroupByChange();
}

function UpdateGroupSelector(groupID, increment, updateGlobal) {

    var selected = $("#ListOfInterfaces #" + groupID + " .selected");
    var available = $("#ListOfInterfaces #" + groupID + " .available .value");
    var numSelected = parseInt($(selected).html());
    var numAvailable = parseInt($(available).html());

    var result = numSelected + increment;

    // update group selected elements
    $(selected).html(result);

    if (updateGlobal) {
        // update global selected elements
        SelectedInterfaces += increment;
        OnSelectedInterfacesChange();
    }

    // update group checkbox
    var checkbox = $("#ListOfInterfaces #" + groupID + " .GrpCheck");
    UpdateCheckbox(checkbox, result == 0 ? '0' : result < numAvailable ? '1' : '2');
}

function SelectInterface(groupID, interfaceID, select) {
    var i = $(".subgrid[groupID='" + groupID + "'] .interfaceRow[id='" + interfaceID + "']");

    // if interface is shown
    if (i.length == 1) {
        var cb = $(".IntCheck", i);

        var curState = $(cb).attr('state');
        var newState = select ? 2 : 0;

        if (curState != newState) {
            if (select) $(i).removeClass("unselectedRow");
            else $(i).addClass("unselectedRow");

            UpdateCheckbox(cb, newState);
        }
    }
}

function SelectInterfaceGlobal(ID, select) {
    var is = $(".subgrid .interfaceRow[id='" + ID + "']");

    if (is.length > 0) {
        var cbs = $(".IntCheck", is);
        var newState = select ? 2 : 0;

        if (select) $(is).removeClass("unselectedRow");
        else $(is).addClass("unselectedRow");

        UpdateCheckbox(cbs, newState);
    }
}

function SelectInterfaces(groupID, state) {
    var rowSel = (state == 2) ? ".unselectedRow" : "";
    var is = $(".subgrid[groupID='" + groupID + "'] .interfaceRow" + rowSel);

    if (is.length > 0) {
        if (state == 2) $(is).removeClass("unselectedRow");
        else $(is).addClass("unselectedRow");

        $.each(is, function (i, o) {
            var cb = $(".IntCheck", o);
            UpdateCheckbox(cb, state);
        });
    }
}

function SelectAllInterfaces(state) {
    var rowSel = (state == 2) ? ".unselectedRow" : "";
    var is = $(".subgrid .interfaceRow" + rowSel);

    if (is.length > 0) {
        if (state == 2) $(is).removeClass("unselectedRow");
        else $(is).addClass("unselectedRow");

        $.each(is, function (i, o) {
            var cb = $(".IntCheck", o);
            UpdateCheckbox(cb, state);
        });
    }
}

function SelectAllGroups(state) {
    var gs = $("#ListOfInterfaces .group");

    if (gs.length > 0) {

        $.each(gs, function (i, o) {
            var cb = $(".GrpCheck", o);
            UpdateCheckbox(cb, state);

            var selected = $(".selected", o);
            var available = $(".available .value", o);

            var result = (state == 0) ? 0 : $(available).html();

            // update group selected elements
            $(selected).html(result);
        });

    }
}

function OnInterfaceCheckbox() {
    var iid = $(this).parent().parent().attr('id');
    var state = $(this).attr('state');

    state = (state == 0) ? 2 : 0;
    var select = state == 2;

    UpdateCheckbox($(this), state);

    // add style for unchecked row
    if (!select)
        $(this).parent().parent().addClass("unselectedRow");
    else
        $(this).parent().parent().removeClass("unselectedRow");

    var groupID = $(this).parent().parent().parent().parent().attr('groupid');

    var increment = select ? 1 : -1;

    UpdateGroupSelector(groupID, increment, true);

    CallService("SelectInterface", "{'id':'" + iid + "', 'type':'" + GroupBy + "', 'value':'" + groupID + "', 'select':'" + select + "'}", function (data, status) {
        // when interface is part of multiple groups (group by VLAN ID)
        if (data.d != null) {
            var inc = data.d.Select ? 1 : -1;
            $.each(data.d.Groups, function (i, o) {
                UpdateGroupSelector(o, inc, false);  // update group's check box and number of selected items
                SelectInterface(o, data.d.ID, data.d.Select);   // update checkboxes for visible interfaces
            });
        }
    });
}

function OnGroupCheckbox() {
    // do nothing if we are already doing something with group
    if (GroupID != null) return;

    GroupID = $(this).parent().parent().attr('id');
    var state = $(this).attr('state');

    state = (state == 0 || state == 1) ? 2 : 0;
    var select = state == 2;

    if (Show == 'all') {
        // select / unselect interfaces
        SelectInterfaces(GroupID, state);
    } else {
        // remove interfaces, because otherwise it would break ordering
        $("#ListOfInterfaces .subgrid[groupID='" + GroupID + "']").remove();

        // change button class and icon
        $("#ListOfInterfaces #" + GroupID + " .expand").removeClass('col').addClass('exp').html(AddExpandButton());
    }

    if (GroupBy != 'vlan' && GroupBy != 'porttype') {
        CallService("SelectGroup", "{'type':'" + GroupBy + "', 'value':'" + GroupID + "', 'select':'" + select + "'}", function (data, status) {
            UpdateGroupSelector(GroupID, data.d.Selected, true);
            GroupID = null;
        });
    } else {
        CallService("SelectVLANGroup", "{'type':'" + GroupBy + "', 'value':'" + GroupID + "', 'select':'" + select + "'}", function (data, status) {
            $.each(data.d.Interfaces, function (i, o) {
                SelectInterfaceGlobal(o, data.d.Select);   // update checkboxes for visible interfaces
            });
            $.each(data.d.Groups, function (i, o) {
                UpdateGroupSelector(o.ID, o.Selected, o.ID == GroupID ? true : false);  // update group selectors (update global selector when main group only)
            });
            GroupID = null;
        });
    }
}

function OnGlobalCheckbox() {
    // do nothing if we are already doing something with group
    if (GroupID != null) return;

    GroupID = $(this);
    var state = $(this).attr('state');

    state = (state == 0 || state == 1) ? 2 : 0;

    CallService("SelectAll", "{'select':'" + (state == 2) + "'}", function (data, status) {
        SelectedInterfaces = data.d.Selected;
        AvailableInterfaces = data.d.Available;

        if (Show == 'all') {
            // do not collapse groups
            var state = SelectedInterfaces == 0 ? 0 : 2;
            SelectAllGroups(state)
            SelectAllInterfaces(state);
            OnSelectedInterfacesChange();
            GroupID = null;
        } else {
            // redraw everything, collapse groups
            OnGroupByChange();
            GroupID = null;
        }
    });
}

function OnPropertyCheckbox() {
    var state = $(this).attr('state');
    state = (state == 0 || state == 1) ? 2 : 0;
    UpdateCheckbox($(this), state);
}

function OnSelectedInterfacesChange() {
    // update selected number info
    $(".selectedInterfaces").html(SelectedInterfaces);
	
	if (SW.Core.LicenseLimitNotification != null)
	{
		SW.Core.LicenseLimitNotification.UpdateLicenseLimitNotification(parseInt(SelectedInterfaces));
	}

    // update global checkbox
    UpdateCheckbox($("#ListOfInterfaces .groupTitle .GlobalCheck"), SelectedInterfaces == 0 ? '0' : SelectedInterfaces < AvailableInterfaces ? '1' : '2');
}

function AddExpandButton() {
    return "<img src='/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Button.Expand.gif'>";
}

function AddCollapseButton() {
    return "<img src='/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Button.Collapse.gif'>";
}

function AddCheckbox(className, state) {
    return "<img src='/Orion/InterfacesDiscovery/Images/DiscoveryWizard/checkbox" + state + ".png' class='" + className + "' state='" + state + "'>";
}

function AddCheckboxWithID(id, className, state) {
    return "<img id='" + id + "' src='/Orion/InterfacesDiscovery/Images/DiscoveryWizard/checkbox" + state + ".png' class='" + className + "' state='" + state + "'>";
}

function UpdateCheckbox(checkbox, state) {
    $(checkbox).attr('src', '/Orion/InterfacesDiscovery/Images/DiscoveryWizard/checkbox' + state + '.png').attr('state', state);
}

function HideColumn(columnType) {
    return columnType == GroupBy ? 'hidden' : '';
}

function KeepAlive() {
    CallService("KeepAlive", "{}", null);
    setTimeout(KeepAlive, 30000);
}

// called when page is loaded
$(function () {

    // show loader to indicate that something is happening in the background (service is loading data)
    $("#loader").removeClass('hidden');

    // init Group By dropdown
    GroupBy = $("#GroupBy").val();
    GroupByText = $("#GroupBy :selected").text();
    $("#GroupBy").change(function () {
        GroupBy = $("#GroupBy").val();
        GroupByText = $("#GroupBy :selected").text();
        OnGroupByChange();
    });

    // init Show dropdown
    Show = $("#Show").val();
    $("#Show").change(function () {
        Show = $("#Show").val();
        OnGroupByChange();
    });

    // button click event
    $("#ReselectButton").click(OnReselectInterfaces);

    // init selection criteria
    Initialization();

    // click events for group expand/collapse
    $(document).on("click", "#ListOfInterfaces .group .exp", OnGroupExpand);
    $(document).on("click", "#ListOfInterfaces .group .col", OnGroupCollapse);

    $(document).on("click", "#ListOfInterfaces .subgrid .MoreInterfaces", OnMoreInterfaces);
    $(document).on("click", "#ListOfInterfaces .subgrid .changeShow", OnChangeShowLink);

    // TODO: shoudl we use 'change'?
    // click event for interfaces' checkboxes
    $(document).on("click", "#ListOfInterfaces .subgrid tr td .IntCheck", OnInterfaceCheckbox);
    $(document).on("click", "#ListOfInterfaces .GrpCheck", OnGroupCheckbox);
    $(document).on("click", "#ListOfInterfaces .GlobalCheck", OnGlobalCheckbox);

    $(document).on("click", ".propertyCheck", OnPropertyCheckbox);

    KeepAlive();
});