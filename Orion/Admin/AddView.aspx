<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" AutoEventWireup="true" CodeFile="AddView.aspx.cs" 
    Inherits="Orion_Admin_AddView" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_TM0_48 %>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <table>
        <tr>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_106) %></td>
            <td><asp:TextBox runat="server" ID="txtViewTitle" Rows="1" Columns="40" /></td>
        </tr>
        <tr>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_107) %></td>
            <td>
                <asp:ListBox runat="server" ID="lbxViewTypes" Rows="1" SelectionMode="single" /><br />
            </td>
        </tr>
    </table>

    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="imgSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
    </div>

    <div id="statusMessage">
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtViewTitle" 
                                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_109 %>" Display="Dynamic" />
        <asp:CustomValidator ID="CustomValidator" runat="server" ControlToValidate="txtViewTitle"
                                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_279 %>"
                                    OnServerValidate="TxtViewTitleValidate"
                                    Display="Dynamic"                                    
                                    />
        <asp:PlaceHolder runat="server" ID="phStatusMessage" />
    </div>
    
    <h2 style="text-transform: uppercase;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_108) %></h2>
   
        <asp:Repeater runat="server" ID="rptViewTypes">
            <ItemTemplate>
                
                    <h2><%# DefaultSanitizer.SanitizeHtml(ViewManager.GetFriendlyViewTypeName(Container.DataItem.ToString())) %></h2>
                   
                    <p><%# DefaultSanitizer.SanitizeHtml(ViewManager.GetViewTypeDescription(Container.DataItem.ToString())) %></p>
              
            </ItemTemplate>
        </asp:Repeater>
</asp:Content>

