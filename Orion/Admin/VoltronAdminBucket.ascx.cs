﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// NOTE: this is a voltron specific variation of AdminBucket
/// It is used for the wilbur-based settings page 
/// </summary>
public partial class Orion_Admin_VoltronAdminBucket : UserControl
{
    public Orion_Admin_VoltronAdminBucket()
    {
        Tiles = new List<TileModel>();
        ColumnCount = 3;
    }

	public int ColumnCount { get; set; }

	public string Header { get; set; }

    public IList<TileModel> Tiles { get; set; }

	protected int ColumnWidth
	{
		get { return 100 / ColumnCount; }
	}

    private List<List<TileModel>> SplitByColumns()
    {
        var columnGroups = new List<List<TileModel>>();

        for (int i = 0; i < ColumnCount; i++)
        {
            columnGroups.Add(new List<TileModel>());
        }

        for (int i = 0; i < Tiles.Count; i++)
        {
            columnGroups[i%ColumnCount].Add(Tiles[i]);
        }
        return columnGroups;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        rptColumns.DataSource = SplitByColumns();
        rptColumns.DataBind();
    }

    protected void ColumnDataBound(object sender, RepeaterItemEventArgs e)
    {
        List<TileModel> column = (List<TileModel>)e.Item.DataItem;
        Repeater r = (Repeater)e.Item.FindControl("rptTiles");
        r.DataSource = column;
        r.DataBind();
    }

    protected void TileDataBound(object sender, RepeaterItemEventArgs e)
    {
        TileModel tileModel = (TileModel)e.Item.DataItem;
        Orion_Admin_AdminBucketLinkTile tile = (Orion_Admin_AdminBucketLinkTile)e.Item.FindControl("tile");
        tile.PrimaryLinks = tileModel.PrimaryLinks;
        tile.Links = tileModel.Links;
        tile.Header = tileModel.Header;
        tile.Icon = tileModel.Icon;
    }
}

public class TileModel
{
    public string Header { get; set; }
    public string Icon { get; set; }
    public IList<LinkModel> Links { get; set; }
    public IList<LinkModel> PrimaryLinks { get; set; }
}
