using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.EntityManager;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Core.Common.Extensions;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Settings;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_Admin_PollingSettings : Page
{
    protected bool _areTrapsAllowed;
    protected bool _areSyslogAllowed;
    protected bool _areVMsAllowed;

    protected bool isInterfacesAllowed;
    protected bool isTopologyAllowed;
    private bool isAssetInventoryAllowed;
    private bool isWirelessAllowed;
    private bool isWirelessHeatmapsAllowed;
    private bool isNPMAllowed;
    private bool isUnDPAllowed;
    private bool isUCSAllowed;
    private bool isHardwareHealthAllowed;

    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private readonly CentralizedSettingsDAL _centralizedSettingsDal = new CentralizedSettingsDAL();

    // hwh data
    protected readonly string CiscoEnvMonMib = "CISCO-ENVMON-MIB";
    protected readonly string CiscoEntitySensorMib = "CISCO-ENTITY-SENSOR-MIB";
    private readonly string CiscoPreferredMIBSettingKey = "HardwareHealth.PreferredCiscoMib";
    OrionSetting CiscoPreferredMIBSettingOriginalSetting;
    private readonly string InterfaceObsoleteDataFeatureStatusSetting = "NPM_Settings_Interfaces_ObsoleteDataFeatureStatus";
    private readonly string InterfaceStaleRemovalEnabledSetting = "NPM_Settings_StaleInterfaces_RemovalEnabled";
    private readonly string VolumeStaleRemovalEnabledSetting = "SWNetPerfMon-Settings-StaleVolume-RemovalEnabled";

    protected System.Net.Sockets.AddressFamily IPVersionSelected = System.Net.Sockets.AddressFamily.InterNetwork;

    private static readonly RemoteFeatureManager _featureManager = new RemoteFeatureManager();

    /// <summary>
    /// Dictionary (SettingID, MinValue, MaxValue) to be able to get the values after postback
    /// </summary>
    private Dictionary<string, Tuple<double, double>> RangeValidatorLimits
    {
        get
        {
            Dictionary<string, Tuple<double, double>> dict = (Dictionary<string, Tuple<double, double>>)ViewState["RangeValidatorLimits"];
            if (dict == null)
            {
                ViewState["RangeValidatorLimits"] = new Dictionary<string, Tuple<double, double>>();
                dict = (Dictionary<string, Tuple<double, double>>)ViewState["RangeValidatorLimits"];
            }
            return dict;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    private void ValidatedIntegerSetup(ValidatedTextBox control, string msg)
    {
        Tuple<double, double> minMaxValues;
        if (RangeValidatorLimits.TryGetValue(control.CustomStringValue, out minMaxValues))
        {
            control.MinValue = ((int)minMaxValues.Item1).ToString();
            control.MaxValue = ((int)minMaxValues.Item2).ToString();
        }
        control.ValidatorText = string.Format(msg, control.MinValue, control.MaxValue);
    }

    private void ValidatedIntegerSetup(params ValidatedTextBox[] controls)
    {
        foreach (var control in controls)
        {
            ValidatedIntegerSetup(control, CoreWebContent.WEBCODE_VS0_2);
        }
    }

    private void ValidatedRetainIntegerSetup(params ValidatedTextBox[] controls)
    {
        foreach (var control in controls)
        {
            ValidatedIntegerSetup(control, CoreWebContent.PollingSettings_Retention_Validator);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _areTrapsAllowed = TrapsSysLogsAvailabilityManager.Instance.IsLegacyTrapsEnabled;
        _areSyslogAllowed = TrapsSysLogsAvailabilityManager.Instance.IsLegacySysLogEnabled;
        _areVMsAllowed = _featureManager.GetMaxElementCount("VMs") > 0;

        this.isInterfacesAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");
        this.isTopologyAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Topology");
        this.isWirelessAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Wireless");
        this.isWirelessHeatmapsAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Wireless.Heatmaps");
        this.isAssetInventoryAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.AssetInventory");
        this.isHardwareHealthAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.HardwareHealth");
        this.isNPMAllowed = ModuleManager.InstanceWithCache.IsThereModule("NPM");
        this.isUnDPAllowed = EntityManager.InstanceWithCache.IsThereEntity("Orion.NPM.CustomPollers");
        this.isUCSAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.UCS");

        if (!IsPostBack)
        {
            InterfacePollIntervalRow.Visible = this.isInterfacesAllowed;
            InterfaceStatsPollIntervalRow.Visible = this.isInterfacesAllowed;
            InterfaceObsoleteDataPollCountRow.Visible = this.isInterfacesAllowed;
            InterfaceObsoleteDataEnabledRow.Visible = this.isInterfacesAllowed;

            InterfaceAvailabilityDetailRetentionRow.Visible = this.isInterfacesAllowed;
            InterfaceAvailabilityHourlyRetentionRow.Visible = this.isInterfacesAllowed;
            InterfaceAvailabilityDailyRetentionRow.Visible = this.isInterfacesAllowed;

            InterfacesStaleRemovalEnabledRow.Visible = this.isInterfacesAllowed;
            InterfacesStaleRemovalIntervalDaysRow.Visible = this.isInterfacesAllowed;

            WirelessDetailRetentionRow.Visible = this.isWirelessAllowed;
            WirelessHourlyRetentionRow.Visible = this.isWirelessAllowed;
            WirelessDailyRetentionRow.Visible = this.isWirelessAllowed;
            WirelessSection.Visible = this.isWirelessAllowed;
            WLP_Settings_ClientRdnsTimeout.Visible = this.isWirelessAllowed;
            WLP_Settings_PollRogues.Visible = this.isWirelessAllowed;

            WirelessRDNSTimeout.Visible = this.isWirelessAllowed;
            WirelessRogueAp.Visible = this.isWirelessAllowed;

            WirelessHeatMapSection.Visible = isWirelessHeatmapsAllowed;
            MapGeneration_Time_Row.Visible = isWirelessHeatmapsAllowed;
            ClientLocation_Poll_Interval_Row.Visible = isWirelessHeatmapsAllowed;


            UnDPDetailRetentionRow.Visible = this.isUnDPAllowed;
            UnDPHourlyRetentionRow.Visible = this.isUnDPAllowed;
            UnDPDailyRetentionRow.Visible = this.isUnDPAllowed;

            AssetInventoryPollIntervalDaysRow.Visible = this.isAssetInventoryAllowed;

            UCSAPITimeoutRow.Visible = this.isUCSAllowed;

            TrapMessagesRetentionRow.Visible = _areTrapsAllowed;
            SyslogMessagesRetentionRow.Visible = _areSyslogAllowed;

            // Multicast section
            Multicast_SectionHeader.Visible = this.isNPMAllowed;
            Multicast_RouteTablePollInterval_Control.Visible = this.isNPMAllowed;

            // Hardware health section
            HardwareHealth_SectionHeader.Visible = this.isHardwareHealthAllowed;
            HardwareHealth_StatisticsPollInterval_Control.Visible = this.isHardwareHealthAllowed;
            HardwareHealth_RetainDetail_Control.Visible = this.isHardwareHealthAllowed;
            HardwareHealth_RetainHourly_Control.Visible = this.isHardwareHealthAllowed;
            HardwareHealth_RetainDaily_Control.Visible = this.isHardwareHealthAllowed;
            HardwareHealth_PreferredCiscoMibRow.Visible = this.isHardwareHealthAllowed && this.isNPMAllowed;

            // Routing section
            NPM_RoutingSection.Visible = this.isNPMAllowed;
            NPM_RoutingNeighborPollIntervalControl.Visible = this.isNPMAllowed;
            NPM_RoutingRouteTablePollIntervalControl.Visible = this.isNPMAllowed;
            NPM_RoutingVRFPollIntervalControl.Visible = this.isNPMAllowed;

            //Topology
            AutoDepEnabledRow.Visible = this.isTopologyAllowed;
            TopologyPollIntervalRow.Visible = this.isTopologyAllowed;

            LoadData();

            SWNetPerfMon_Settings_Default_IP_Address_Resolution.SelectedIndex = IPVersionSelected == AddressFamily.InterNetwork ? 0 : 1;
        }
        else
        {
            if (this.isHardwareHealthAllowed && this.isNPMAllowed)
            {
                // load the setting so we can audit the change
                CiscoPreferredMIBSettingOriginalSetting = SettingsDAL.GetSetting(CiscoPreferredMIBSettingKey);
            }
        }

        ValidatedIntegerSetup(SWNetPerfMon_Settings_Default_Node_Poll_Interval,
                      SWNetPerfMon_Settings_Default_Interface_Poll_Interval,
                      SWNetPerfMon_Settings_Default_Volume_Poll_Interval,
                      SWNetPerfMon_Settings_Default_Rediscovery_Interval,
                      SWNetPerfMon_Settings_Default_Node_Topology_Poll_Interval,
                      SWNetPerfMon_Settings_Default_Node_Stat_Poll_Interval,
                      SWNetPerfMon_Settings_Default_Interface_Stat_Poll_Interval,
                      SWNetPerfMon_Settings_Default_Interface_Obsolete_Data_Poll_Count,
                      NPM_Settings_StaleInterfaces_RemovalIntervalDays,
                      SWNetPerfMon_Settings_Default_Volume_Stat_Poll_Interval,
                      SWNetPerfMon_Settings_StaleVolume_RemovalIntervalDays,
                      tbIndexDefragmentationTimeout,
                      SWNetPerfMon_Settings_Retain_Auditing_Trails,
                      SWNetPerfMon_Settings_Baseline_Collection_Duration,
                      SWNetPerfMon_Settings_Retain_Container_Detail,
                      SWNetPerfMon_Settings_Retain_Container_Hourly,
                      SWNetPerfMon_Settings_Retain_Container_Daily,
                      NPM_Settings_InterfaceAvailability_Retain_Detail,
                      NPM_Settings_InterfaceAvailability_Retain_Hourly,
                      NPM_Settings_InterfaceAvailability_Retain_Daily,
                      NPM_Settings_Wireless_Retain_Detail,
                      NPM_Settings_Wireless_Retain_Hourly,
                      NPM_Settings_Wireless_Retain_Daily,
                      NPM_Settings_UnDP_Retain_Detail,
                      NPM_Settings_UnDP_Retain_Hourly,
                      NPM_Settings_UnDP_Retain_Daily,
                      SWNetPerfMon_Downtime_History_Retention,
                      SWNetPerfMon_Settings_Retain_Events,
                      SysLog_MaxMessageAge,
                      Trap_MaxMessageAge,
                      Alert_MaxAlertExecutionTime,
                      SWNetPerfMon_Settings_Retain_Discovery,
                      SWNetPerfMon_Settings_ICMP_Timeout,
                      SWNetPerfMon_Settings_SNMP_Timeout,
                      SWNetPerfMon_Settings_SNMP_Retries,
                      WLP_Settings_ClientRdnsTimeout,
                      NPM_Settings_WLHM_ClientLocation_PollInterval,
                      SWNetPerfMon_Settings_UCS_API_Timeout,
                      SWNetPerfMon_Settings_Default_Fast_Poll_Interval,
                      AssetInventory_PollIntervalDays,
                      Multicast_RouteTablePollInterval,
                      NPM_RoutingNeighborPollInterval,
                      NPM_RoutingRouteTablePollInterval,
                      NPM_RoutingVRFPollInterval,
                      HardwareHealth_StatisticsPollInterval,
                      HardwareHealth_RetainDetail,
                      HardwareHealth_RetainHourly,
                      HardwareHealth_RetainDaily);
        ValidatedRetainIntegerSetup(
                      SWNetPerfMon_Settings_Retain_Detail,
                      SWNetPerfMon_Settings_Retain_Hourly,
                      SWNetPerfMon_Settings_Retain_Daily
            );


        bt_Statistics.DataBind();
        bt_Polling.DataBind();
        bt_IPResolution.DataBind();
        Archive_Time_Validator.DataBind();
        MapGeneration_Time_Validator.DataBind();
    }

    private static void SaveValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        try
        {
            double val = Convert.ToDouble(tb.Text);

            if (tb.UseRangeValidator)
            {
                double min = Convert.ToDouble(tb.MinValue);
                double max = Convert.ToDouble(tb.MaxValue);

                if (val < min || val > max)
                    return;
            }

            SettingsDAL.SetValue(tb.CustomStringValue, val);
        }
        catch (Exception exception)
        {
            log.Error(exception);
        }
    }

    private static void SaveValue(int val, string customStringValue)
    {
        try
        {
            SettingsDAL.SetValue(customStringValue, val);
        }
        catch (Exception exception)
        {
            log.Error(exception);
        }
    }

    private static void SaveStringValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        SettingsDAL.SetDescriptionValue(tb.CustomStringValue, tb.Text);
    }

    private static void SaveDropDownValue(ListControl ddl, string settingID)
    {
        if (ddl == null || String.IsNullOrEmpty(settingID))
            return;

        try
        {
            double val = Convert.ToDouble(ddl.SelectedValue);
            SettingsDAL.SetValue(settingID, val);
        }
        catch { }
    }

    private static void SaveDropDownValueToDescription(ListControl ddl, string settingID)
    {
        if (ddl == null || String.IsNullOrEmpty(settingID))
            return;

        SettingsDAL.SetDescriptionValue(settingID, ddl.SelectedValue);
    }

    private static void SaveIpValue(ITextControl tb, string settingID)
    {
        if (tb == null || String.IsNullOrEmpty(settingID))
            return;

        try
        {
            string val = tb.Text;
            SettingsDAL.SetDescriptionValue(settingID, val);
        }
        catch (Exception exception)
        {
            log.Error(exception);
        }
    }

    private static void SaveOATimeValue(ITextControl tb, string settingID)
    {
        if (tb == null || String.IsNullOrEmpty(settingID))
            return;

        try
        {
            DateTime dtime = Convert.ToDateTime(tb.Text);
            DateTime time = new DateTime();
            DateTime value = time.AddHours(dtime.Hour).AddMinutes(dtime.Minute);
            SettingsDAL.SetValue(settingID, value);
        }
        catch (Exception exception)
        {
            log.Error(exception);
        }
    }

    private static void SaveBoolValue(ICheckBoxControl cb, string settingID)
    {
        if (cb == null || String.IsNullOrEmpty(settingID))
            return;

        try
        {
            SettingsDAL.SetValue(settingID, cb.Checked);
        }
        catch (Exception exception)
        {
            log.Error(exception);
        }
    }

    private void SaveSNMPEncoding()
    {
        if (ParseSnmpEncoding(SnmpSettings.Instance.Encoding).GetWebName() !=
            SNMPEncodingDropDown.SelectedValue) //avoid rewriting custom encoding in database
        {
            _centralizedSettingsDal.UpdateSettingValue("SolarWinds.Orion.Core.Common.Settings.SnmpSettings.Encoding", SNMPEncodingDropDown.SelectedValue);
        }
    }
    
    private static SNMPEncoding ParseSnmpEncoding(string encoding)
    {
        foreach (var e in (SNMPEncoding[])Enum.GetValues(typeof(SNMPEncoding)))
        {
            if (e.GetWebName() == encoding)
            {
                return e;
            }
        }
        return SNMPEncoding.Auto;
    }

    private static bool LoadValue(ref AddressFamily ipVersion, string customStringValue)
    {
        OrionSetting setting = SettingsDAL.GetSetting(customStringValue);

        if (setting != null)
        {
            try
            {
                int settingValue = Convert.ToInt32(setting.SettingValue);
                ipVersion = settingValue == 6 ? AddressFamily.InterNetworkV6 : AddressFamily.InterNetwork;
                return true;
            }
            catch (Exception exception)
            {
                log.Error(exception);
                return false;
            }
        }
        return false;
    }

    private void LoadValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        OrionSetting setting = SettingsDAL.GetSetting(tb.CustomStringValue);

        if (setting != null)
        {
            try
            {
                switch (tb.Type)
                {
                    case ValidatedTextBox.TextBoxType.Double:
                        tb.Text = setting.SettingValue.ToString();
                        RangeValidatorLimits[tb.CustomStringValue] = Tuple.Create(setting.Min, setting.Max);
                        break;

                    case ValidatedTextBox.TextBoxType.Integer:
                        tb.Text = Convert.ToInt32(setting.SettingValue).ToString();
                        RangeValidatorLimits[tb.CustomStringValue] = Tuple.Create(setting.Min, setting.Max);
                        break;

                    default:
                        break;
                }
            }
            catch (Exception exception)
            {
                log.Error(exception);
            }
        }
    }

    private static void LoadDropDownValue(ListControl ddl, string settingID)
    {
        if (ddl == null || String.IsNullOrEmpty(settingID))
            return;

        OrionSetting setting = SettingsDAL.GetSetting(settingID);

        if (setting != null)
        {
            ddl.SelectedValue = setting.SettingValue.ToString();
        }
    }

    private static void LoadDropDownValueFromDescription(ListControl ddl, string settingID)
    {
        if (ddl == null || String.IsNullOrEmpty(settingID))
            return;

        OrionSetting setting = SettingsDAL.GetSetting(settingID);

        if (setting != null)
        {
            ddl.SelectedValue = setting.Description.ToString();
        }
    }

    private static void LoadIpValue(ITextControl tb, string settingID)
    {
        if (tb == null || String.IsNullOrEmpty(settingID))
            return;

        OrionSetting setting = SettingsDAL.GetSetting(settingID);

        if (setting != null)
        {
            try
            {
                tb.Text = IPAddressHelper.ToStringIp(setting.Description);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }

    private static void LoadOATimeValue(ITextControl tb, string settingID)
    {
        if (tb == null || String.IsNullOrEmpty(settingID))
            return;

        OrionSetting setting = SettingsDAL.GetSetting(settingID);

        if (setting != null)
        {
            try
            {
                DateTime time = DateTime.FromOADate(setting.SettingValue);

                // solve rounding problem
                if (time.Second > 30)
                {
                    time = time.AddMinutes(1);
                }

                tb.Text = String.Format("{0}", time.ToShortTimeString());
            }
            catch (Exception exception)
            {
                log.Error(exception);
            }
        }
    }

    private static void LoadBoolValue(ICheckBoxControl cb, string settingID)
    {
        if (cb == null || String.IsNullOrEmpty(settingID))
            return;

        OrionSetting setting = SettingsDAL.GetSetting(settingID);

        if (setting != null)
        {
            cb.Checked = Convert.ToBoolean(setting.SettingValue);
        }
    }

    private static void LoadStringValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        OrionSetting setting = SettingsDAL.GetSetting(tb.CustomStringValue);

        if (setting != null)
        {
            switch (tb.Type)
            {
                case ValidatedTextBox.TextBoxType.Text:
                    tb.Text = setting.Description.ToString();
                    break;

                default:
                    break;
            }
        }
    }

    #region public void LoadData()
    public void LoadData()
    {
        LoadOATimeValue(Archive_Time, "SWNetPerfMon-Settings-Archive Time");

        LoadDropDownValue(SWNetPerfMon_Settings_Availability_Calculation, "SWNetPerfMon-Settings-Availability Calculation");
        LoadDropDownValue(SWNetPerfMon_Settings_Interface_Baseline_Calculation_Frequency, "SWNetPerfMon-Settings-Interface Baseline Calculation Frequency");
        LoadDropDownValueFromDescription(SWNetPerfMon_Settings_Counter_Rollover, "SWNetPerfMon-Settings-Counter Rollover");

        LoadValue(SWNetPerfMon_Settings_Default_Fast_Poll_Interval);

        LoadDropDownValue(EnhancedNodeStatusCalculationSetting, "EnhancedNodeStatusCalculation");
        LoadDropDownValue(EnhancedInterfaceStatusCalculationSetting, "NPM_Settings_Interfaces_EnableEnhancedInterfaceStatusCalculation");

        if (this.isInterfacesAllowed)
        {
            LoadBoolValue(ifObsoleteCb, InterfaceObsoleteDataFeatureStatusSetting);
            LoadValue(SWNetPerfMon_Settings_Default_Interface_Obsolete_Data_Poll_Count);

            LoadValue(SWNetPerfMon_Settings_Default_Interface_Poll_Interval);
            LoadValue(SWNetPerfMon_Settings_Default_Interface_Stat_Poll_Interval);

            LoadBoolValue(NPM_Settings_StaleInterfaces_RemovalEnabled, InterfaceStaleRemovalEnabledSetting);
            LoadValue(NPM_Settings_StaleInterfaces_RemovalIntervalDays);

            LoadValue(NPM_Settings_InterfaceAvailability_Retain_Detail);
            LoadValue(NPM_Settings_InterfaceAvailability_Retain_Hourly);
            LoadValue(NPM_Settings_InterfaceAvailability_Retain_Daily);
        }

        LoadBoolValue(SWNetPerfMon_Settings_StaleVolume_RemovalEnabled, VolumeStaleRemovalEnabledSetting);
        LoadValue(SWNetPerfMon_Settings_StaleVolume_RemovalIntervalDays);

        if (this.isUCSAllowed)
        {
            LoadValue(SWNetPerfMon_Settings_UCS_API_Timeout);
        }


        if (this.isWirelessAllowed)
        {
            LoadValue(NPM_Settings_Wireless_Retain_Detail);
            LoadValue(NPM_Settings_Wireless_Retain_Hourly);
            LoadValue(NPM_Settings_Wireless_Retain_Daily);
            LoadValue(WLP_Settings_ClientRdnsTimeout);
            LoadBoolValue(WLP_Settings_PollRogues, "WLP_Settings_PollRogues");
        }

        if (this.isWirelessHeatmapsAllowed)
        {
            LoadOATimeValue(MapGeneration_Time, "NPM_Settings_WLHM_MapGenerationTime");
            LoadValue(NPM_Settings_WLHM_ClientLocation_PollInterval);
        }

        if (this.isUnDPAllowed)
        {
            LoadValue(NPM_Settings_UnDP_Retain_Detail);
            LoadValue(NPM_Settings_UnDP_Retain_Hourly);
            LoadValue(NPM_Settings_UnDP_Retain_Daily);
        }

        if (this.isNPMAllowed)
        {
            LoadValue(NPM_RoutingNeighborPollInterval);
            LoadValue(NPM_RoutingRouteTablePollInterval);
            LoadValue(NPM_RoutingVRFPollInterval);
            LoadValue(Multicast_RouteTablePollInterval);
        }

        if (this.isHardwareHealthAllowed)
        {
            LoadValue(HardwareHealth_StatisticsPollInterval);
            LoadValue(HardwareHealth_RetainDetail);
            LoadValue(HardwareHealth_RetainHourly);
            LoadValue(HardwareHealth_RetainDaily);

            if (this.isNPMAllowed)
            {
                LoadDropDownValueFromDescription(HardwareHealth_PreferredCiscoMib, CiscoPreferredMIBSettingKey);
            }
        }

        if (this.isTopologyAllowed)
        {
            LoadBoolValue(SWNetPerfMon_Settings_AutoDependency_Enabled, "SWNetPerfMon-Settings-AutoDependency Enabled");
            LoadBoolValue(Topology_IgnoreUnknownPorts, "Topology-IgnoreUnknownPorts");
            LoadValue(SWNetPerfMon_Settings_Default_Node_Topology_Poll_Interval);
        }

        LoadValue(SWNetPerfMon_Downtime_History_Retention);
        LoadValue(SWNetPerfMon_Settings_Default_Node_Poll_Interval);
        LoadValue(SWNetPerfMon_Settings_Default_Node_Stat_Poll_Interval);
        LoadValue(SWNetPerfMon_Settings_Default_Rediscovery_Interval);
        LoadValue(SWNetPerfMon_Settings_Default_Volume_Poll_Interval);
        LoadValue(SWNetPerfMon_Settings_Default_Volume_Stat_Poll_Interval);
        LoadValue(SWNetPerfMon_Settings_ICMP_Timeout);
        LoadValue(tbIndexDefragmentationTimeout);
        LoadValue(SWNetPerfMon_Settings_Retain_Auditing_Trails);
        LoadValue(SWNetPerfMon_Settings_Baseline_Collection_Duration);

        LoadValue(SWNetPerfMon_Settings_Retain_Detail);
        LoadValue(SWNetPerfMon_Settings_Retain_Hourly);
        LoadValue(SWNetPerfMon_Settings_Retain_Daily);

        LoadValue(SWNetPerfMon_Settings_Retain_Container_Detail);
        LoadValue(SWNetPerfMon_Settings_Retain_Container_Hourly);
        LoadValue(SWNetPerfMon_Settings_Retain_Container_Daily);

        LoadValue(SWNetPerfMon_Settings_Retain_Events);
        LoadValue(ref IPVersionSelected, "SWNetPerfMon-Settings-Default Preferred AddressFamily DHCP");

        if (_areSyslogAllowed)
            LoadValue(SysLog_MaxMessageAge);
        if (_areTrapsAllowed)
            LoadValue(Trap_MaxMessageAge);

        LoadValue(Alert_MaxAlertExecutionTime);
        LoadStringValue(Alert_AlertAcknowledgeUrlText);
        LoadValue(SWNetPerfMon_Settings_Retain_Discovery);
        LoadValue(SWNetPerfMon_Settings_SNMP_Retries);
        LoadValue(SWNetPerfMon_Settings_SNMP_Timeout);

        LoadStringValue(SWNetPerfMon_Settings_ICMP_Data);

        LoadBoolValue(SWNetPerfMon_Settings_DHCP_EnableRDNS, "SWNetPerfMon-Settings-DHCP EnableRDNS");
        LoadBoolValue(SWNetPerfMon_Settings_EnableDowntimeMonitoring, "SWNetPerfMon-Settings-EnableDowntimeMonitoring");
        LoadBoolValue(SWNetPerfMon_Settings_Baseline_Calculation, "SWNetPerfMon-Settings-Baseline Calculation");
        LoadBoolValue(SWNetPerfMon_Settings_Baseline_AllowSecureData, "SWNetPerfMon-Settings-Baseline AllowSecureData");
        LoadBoolValue(SWNetPerfMon_Settings_AllowAlertActionsForUnmanaged, "AlertEngine-ProcessUnmanagedObjects");
        LoadBoolValue(SWNetPerfMon_Settings_VulnerabilityCheckDisabled, "SWNetPerfMon-Settings-VulnerabilityCheckDisabled");
        LoadBoolValue(cbIndexDefragmentationEnabled, "dbm-defragmentation-enabled");
        LoadBoolValue(cbAutoResetLogLevelsEnabled, "AutoResetLogLevelsEnabled");

        LoadIpValue(SWNetPerfMon_Settings_Invalid_Dynamic_IP, "SWNetPerfMon-Settings-Invalid Dynamic IP");

        LoadSNMPEncodingDropDown();

        if (this.isAssetInventoryAllowed)
        {
            LoadValue(AssetInventory_PollIntervalDays);
        }
    }

    private void LoadSNMPEncodingDropDown()
    {
        foreach (SNMPEncoding e in Enum.GetValues(typeof(SNMPEncoding)))
        {
            SNMPEncodingDropDown.Items.Add(new ListItem(e.GetDescription(), e.GetWebName()));
        }
        SNMPEncodingDropDown.SelectedValue = SnmpSettings.Instance.Encoding;
    }

    #endregion

    #region protected void Submit_Click(object source, EventArgs e)
    protected void Submit_Click(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        SaveOATimeValue(Archive_Time, "SWNetPerfMon-Settings-Archive Time");

        SaveDropDownValue(SWNetPerfMon_Settings_Availability_Calculation, "SWNetPerfMon-Settings-Availability Calculation");
        SaveDropDownValue(SWNetPerfMon_Settings_Interface_Baseline_Calculation_Frequency, "SWNetPerfMon-Settings-Interface Baseline Calculation Frequency");
        SaveDropDownValueToDescription(SWNetPerfMon_Settings_Counter_Rollover, "SWNetPerfMon-Settings-Counter Rollover");

        SaveDropDownValue(EnhancedNodeStatusCalculationSetting, "EnhancedNodeStatusCalculation");
        SaveDropDownValue(EnhancedInterfaceStatusCalculationSetting, "NPM_Settings_Interfaces_EnableEnhancedInterfaceStatusCalculation");

        IPVersionSelected = SWNetPerfMon_Settings_Default_IP_Address_Resolution.SelectedIndex == 0 ? AddressFamily.InterNetwork : AddressFamily.InterNetworkV6;
        SaveValue(IPVersionSelected == AddressFamily.InterNetworkV6 ? 6 : 4, "SWNetPerfMon-Settings-Default Preferred AddressFamily DHCP");

        SaveValue(SWNetPerfMon_Settings_Default_Fast_Poll_Interval);

        if (this.isInterfacesAllowed)
        {
            SaveValue(SWNetPerfMon_Settings_Default_Interface_Poll_Interval);
            SaveValue(SWNetPerfMon_Settings_Default_Interface_Stat_Poll_Interval);

            SaveBoolValue(NPM_Settings_StaleInterfaces_RemovalEnabled, InterfaceStaleRemovalEnabledSetting);
            SaveValue(NPM_Settings_StaleInterfaces_RemovalIntervalDays);

            SaveBoolValue(ifObsoleteCb, InterfaceObsoleteDataFeatureStatusSetting);
            SaveValue(SWNetPerfMon_Settings_Default_Interface_Obsolete_Data_Poll_Count);

            SaveValue(NPM_Settings_InterfaceAvailability_Retain_Detail);
            SaveValue(NPM_Settings_InterfaceAvailability_Retain_Hourly);
            SaveValue(NPM_Settings_InterfaceAvailability_Retain_Daily);
        }

        SaveBoolValue(SWNetPerfMon_Settings_StaleVolume_RemovalEnabled, VolumeStaleRemovalEnabledSetting);
        SaveValue(SWNetPerfMon_Settings_StaleVolume_RemovalIntervalDays);

        if (this.isUCSAllowed)
        {
            SaveValue(SWNetPerfMon_Settings_UCS_API_Timeout);
        }


        if (this.isWirelessAllowed)
        {
            SaveValue(NPM_Settings_Wireless_Retain_Detail);
            SaveValue(NPM_Settings_Wireless_Retain_Hourly);
            SaveValue(NPM_Settings_Wireless_Retain_Daily);
            SaveValue(WLP_Settings_ClientRdnsTimeout);
            SaveBoolValue(WLP_Settings_PollRogues, "WLP_Settings_PollRogues");
        }

        if (this.isWirelessHeatmapsAllowed)
        {
            SaveOATimeValue(MapGeneration_Time, "NPM_Settings_WLHM_MapGenerationTime");
            SaveValue(NPM_Settings_WLHM_ClientLocation_PollInterval);
        }

        if (this.isUnDPAllowed)
        {
            SaveValue(NPM_Settings_UnDP_Retain_Detail);
            SaveValue(NPM_Settings_UnDP_Retain_Hourly);
            SaveValue(NPM_Settings_UnDP_Retain_Daily);
        }

        if (this.isNPMAllowed)
        {
            SaveValue(NPM_RoutingNeighborPollInterval);
            SaveValue(NPM_RoutingRouteTablePollInterval);
            SaveValue(NPM_RoutingVRFPollInterval);
            SaveValue(Multicast_RouteTablePollInterval);
        }

        if (this.isHardwareHealthAllowed)
        {
            SaveValue(HardwareHealth_StatisticsPollInterval);
            SaveValue(HardwareHealth_RetainDetail);
            SaveValue(HardwareHealth_RetainHourly);
            SaveValue(HardwareHealth_RetainDaily);

            if (isNPMAllowed)
            {
                string currentValue = HardwareHealth_PreferredCiscoMib.SelectedValue;
                string previousValue = (CiscoPreferredMIBSettingOriginalSetting != null) ? CiscoPreferredMIBSettingOriginalSetting.Description : string.Empty;

                bool hasSettingChanged = string.Equals(currentValue, previousValue, StringComparison.InvariantCultureIgnoreCase) == false;

                if (hasSettingChanged)
                {
                    SaveDropDownValueToDescription(HardwareHealth_PreferredCiscoMib, CiscoPreferredMIBSettingKey);
                }
            }
        }

        if (this.isTopologyAllowed)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                // don't allow user to enable/disable AD on demo server
            }
            else
            {
                SaveBoolValue(SWNetPerfMon_Settings_AutoDependency_Enabled, "SWNetPerfMon-Settings-AutoDependency Enabled");
            }
            SaveBoolValue(Topology_IgnoreUnknownPorts, "Topology-IgnoreUnknownPorts");
            SaveValue(SWNetPerfMon_Settings_Default_Node_Topology_Poll_Interval);
        }

        SaveValue(SWNetPerfMon_Downtime_History_Retention);
        SaveValue(SWNetPerfMon_Settings_Default_Node_Poll_Interval);
        SaveValue(SWNetPerfMon_Settings_Default_Node_Stat_Poll_Interval);
        SaveValue(SWNetPerfMon_Settings_Default_Rediscovery_Interval);
        SaveValue(SWNetPerfMon_Settings_Default_Volume_Poll_Interval);
        SaveValue(SWNetPerfMon_Settings_Default_Volume_Stat_Poll_Interval);
        SaveValue(SWNetPerfMon_Settings_ICMP_Timeout);
        SaveValue(tbIndexDefragmentationTimeout);
        SaveValue(SWNetPerfMon_Settings_Retain_Auditing_Trails);
        SaveValue(SWNetPerfMon_Settings_Baseline_Collection_Duration);

        SaveValue(SWNetPerfMon_Settings_Retain_Detail);
        SaveValue(SWNetPerfMon_Settings_Retain_Hourly);
        SaveValue(SWNetPerfMon_Settings_Retain_Daily);

        SaveValue(SWNetPerfMon_Settings_Retain_Container_Detail);
        SaveValue(SWNetPerfMon_Settings_Retain_Container_Hourly);
        SaveValue(SWNetPerfMon_Settings_Retain_Container_Daily);

        SaveValue(SWNetPerfMon_Settings_Retain_Events);

        SaveIpValue(SWNetPerfMon_Settings_Invalid_Dynamic_IP, "SWNetPerfMon-Settings-Invalid Dynamic IP");

        if (_areSyslogAllowed)
            SaveValue(SysLog_MaxMessageAge);

        if (_areTrapsAllowed)
            SaveValue(Trap_MaxMessageAge);

        SaveValue(Alert_MaxAlertExecutionTime);
        SaveStringValue(Alert_AlertAcknowledgeUrlText);
        SaveValue(SWNetPerfMon_Settings_Retain_Discovery);
        SaveValue(SWNetPerfMon_Settings_SNMP_Retries);
        SaveValue(SWNetPerfMon_Settings_SNMP_Timeout);

        if (this.isAssetInventoryAllowed)
        {
            SaveValue(AssetInventory_PollIntervalDays);
        }


        SaveStringValue(SWNetPerfMon_Settings_ICMP_Data);

        SaveBoolValue(SWNetPerfMon_Settings_DHCP_EnableRDNS, "SWNetPerfMon-Settings-DHCP EnableRDNS");
        SaveBoolValue(SWNetPerfMon_Settings_Baseline_Calculation, "SWNetPerfMon-Settings-Baseline Calculation");
        SaveBoolValue(SWNetPerfMon_Settings_Baseline_AllowSecureData, "SWNetPerfMon-Settings-Baseline AllowSecureData");
        SaveBoolValue(SWNetPerfMon_Settings_AllowAlertActionsForUnmanaged, "AlertEngine-ProcessUnmanagedObjects");
        SaveBoolValue(SWNetPerfMon_Settings_VulnerabilityCheckDisabled, "SWNetPerfMon_Settings_VulnerabilityCheckDisabled");

        SaveBoolValue(SWNetPerfMon_Settings_EnableDowntimeMonitoring, "SWNetPerfMon-Settings-EnableDowntimeMonitoring");
        SaveBoolValue(cbIndexDefragmentationEnabled, "dbm-defragmentation-enabled");
        SaveBoolValue(cbAutoResetLogLevelsEnabled, "AutoResetLogLevelsEnabled");

        SaveSNMPEncoding();

        using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
        {
            proxy.RefreshSettingsFromDatabase();
        }
        ReferrerRedirectorBase.Return(Request.Url.ToString());
    }
    #endregion

    protected void Cancel_Click(object source, EventArgs e)
    {
        ReferrerRedirectorBase.Return("/Orion/Admin/");
    }

    protected void Polling_Click(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        int nodeInterval = Convert.ToInt32(SWNetPerfMon_Settings_Default_Node_Poll_Interval.Text);
        int interfaceInterval = 0;
        if (this.isInterfacesAllowed)
        {
            interfaceInterval = Convert.ToInt32(SWNetPerfMon_Settings_Default_Interface_Poll_Interval.Text);
        }

        int volumeInterval = Convert.ToInt32(SWNetPerfMon_Settings_Default_Volume_Poll_Interval.Text);
        int rediscoveryInterval = Convert.ToInt32(SWNetPerfMon_Settings_Default_Rediscovery_Interval.Text);

        int defRediscoveryInterval = 0, defNodePollInterval = 0, defIntPollInterval = 0, defVolPollInterval = 0;

        if (cbLockCustomValues.Checked)
        {
            defRediscoveryInterval = (int)SettingsDAL.GetSetting(SWNetPerfMon_Settings_Default_Rediscovery_Interval.CustomStringValue).SettingValue;
            defNodePollInterval = (int)SettingsDAL.GetSetting(SWNetPerfMon_Settings_Default_Node_Poll_Interval.CustomStringValue).SettingValue;
            defIntPollInterval = (int)SettingsDAL.GetSetting(SWNetPerfMon_Settings_Default_Interface_Poll_Interval.CustomStringValue).SettingValue;
            defVolPollInterval = (int)SettingsDAL.GetSetting(SWNetPerfMon_Settings_Default_Volume_Poll_Interval.CustomStringValue).SettingValue;
        }

        SettingsDAL.UpdateNodesMultiplePollingSetting(rediscoveryInterval, nodeInterval, cbLockCustomValues.Checked,
            defRediscoveryInterval, defNodePollInterval);
        SettingsDAL.UpdateVolumesMultiplePollingSetting(rediscoveryInterval, volumeInterval, cbLockCustomValues.Checked,
            defRediscoveryInterval, defVolPollInterval);

        if (this.isInterfacesAllowed)
        {
            SettingsDAL.UpdateInterfacesMultiplePollingSetting(rediscoveryInterval, interfaceInterval,
                                                               cbLockCustomValues.Checked,
                                                               defRediscoveryInterval, defIntPollInterval);
        }

    }

    protected void IPResolution_Click(object source, EventArgs e)
    {
        using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
            proxy.UpdateSpecificSettingForAllNodes(Node.IPResolutionSettingName,
                SWNetPerfMon_Settings_Default_IP_Address_Resolution.SelectedValue, "DynamicIP=1");
    }

    protected void Statistics_Click(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;
        if (IsDefaultNodeTopologyPollIntervalEnabled.Checked && this.isTopologyAllowed)
        {
            using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
                proxy.UpdateSpecificSettingForAllNodes(Node.TopologyPollIntervalSettingName,
                    SWNetPerfMon_Settings_Default_Node_Topology_Poll_Interval.Text, "ObjectSubType NOT LIKE 'ICMP'");
        }


        if (IsDefaultNodeStatsPollIntervalEnabled.Checked)
        {
            Int16 nodeStatInterval = Convert.ToInt16(SWNetPerfMon_Settings_Default_Node_Stat_Poll_Interval.Text);
            SettingsDAL.SetStatCollectionNodes(nodeStatInterval);
        }
        if (IsDefaultVolumeStatsPollIntervalEnabled.Checked)
        {
            Int16 volumeStatInterval = Convert.ToInt16(SWNetPerfMon_Settings_Default_Volume_Stat_Poll_Interval.Text);

            SettingsDAL.SetStatCollectionVolumes(volumeStatInterval);
        }

        if (this.isInterfacesAllowed && IsDefaultInterfaceStatsPollIntervalEnabled.Checked)
        {
            Int16 interfaceStatInterval = Convert.ToInt16(SWNetPerfMon_Settings_Default_Interface_Stat_Poll_Interval.Text);
            SettingsDAL.SetStatCollectionInterfaces(interfaceStatInterval);
        }

    }

    public void TimeValidator(object sender, ServerValidateEventArgs e)
    {
        DateTime resDate;
        // validate short time format
        e.IsValid = DateTime.TryParseExact(Archive_Time.Text, "t", CultureInfo.CurrentCulture, DateTimeStyles.None, out resDate);
    }

    public void MapGenerationTimeValidator(object sender, ServerValidateEventArgs e)
    {
        DateTime resDate;
        // validate short time format
        e.IsValid = DateTime.TryParseExact(MapGeneration_Time.Text, "t", CultureInfo.CurrentCulture, DateTimeStyles.None, out resDate);
    }

    protected void CollectionDuration_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        int detailDays;
        int durationDays;

        if (Int32.TryParse(SWNetPerfMon_Settings_Retain_Detail.Text, out detailDays) && Int32.TryParse(e.Value, out durationDays))
            e.IsValid = durationDays <= detailDays;
        else
            e.IsValid = true;
    }
}
