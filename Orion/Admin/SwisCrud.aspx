<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"  
        Title="Orion SWIS CRUD" EnableViewState="false" %>
<%-- disable ViewState on this page, we really don't need to maintain anything across postbacks --%>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.2.0"/>

<style type="text/css">
    #contents
    {
        width:80%;
    }
    
    h1 
    {
        font-size: 12pt;
    }

    #textareaDiv input[type="text"], #textareaDiv textarea
    {
        width: 100%;
        border: 1px solid #8FB3D4;
        font-family: Consolas, Monaco, 'Courier New', Courier, monospace;
        font-size: 11px;
        padding: 2px;
        width: 100%;        
    }
    #textareaDiv
    {
        padding: 10px 0px;
    }
    
    #results 
    {
        padding-top: 20px;
    }
    
    .x-grid3-row-alt 
    {
        background-color: #f8f8f8;
    }
    

</style>

<script type="text/javascript">
    handleError = function (error) {
        if (error.get_message() == "Authentication failed.") {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('Your session expired.');
            window.location.reload();
        } else {
            $('#lastErrorMessage').text("Server error: " + error.get_message()).show();
        }
    };

    $().ready(function () {
        $('#createBtn').click(function () {
            var props = JSON.parse($('#propertiesEdit').val());
            Information.Create(props, function (result) {
                $('#uriEdit').val(result);
                $('#lastErrorMessage').text('Created successfully.').show();
            }, handleError);
        });

        $('#readBtn').click(function () {
            var uri = $('#uriEdit').val();
            Information.Read(uri, function (result) {
                $('#propertiesEdit').val(JSON.stringify(result, null, 4));
            }, handleError);
        });

        $('#updateBtn').click(function () {
            var uri = $('#uriEdit').val();
            var props = JSON.parse($('#propertiesEdit').val());
            Information.Update(uri, props, function (result) {
                $('#lastErrorMessage').text('Updated successfully.').show();
            })
        });

        $('#deleteBtn').click(function () {
            var uri = $('#uriEdit').val();
            Information.Delete(uri, function (result) {
                $('#lastErrorMessage').text('Deleted successfully.').show();
            }, handleError);
        });
    });

</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<asp:ScriptManagerProxy ID="ScriptManager1" runat="server" >
		<Services>
			<asp:ServiceReference path="../Services/Information.asmx" />
		</Services>
	</asp:ScriptManagerProxy>

    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <div id="contents" >
        <div id="query">
            <div id="textareaDiv">
                <input type="text" id="uriEdit" value="swis://./Orion/Orion.Settings/SettingID=x"/>
                <textarea id="propertiesEdit" rows="10" cols="80">{}</textarea>
            </div>
            <input type="button" id="createBtn" value="Create"/>
            <input type="button" id="readBtn" value="Read"/>
            <input type="button" id="updateBtn" value="Update"/>
            <input type="button" id="deleteBtn" value="Delete"/>
        </div>
        <div id="results">
            <div id="lastErrorMessage"></div>
            <div id="resultset"></div>
        </div>
    </div>
</asp:Content>

