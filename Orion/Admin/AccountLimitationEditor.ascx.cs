using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Admin_AccountLimitationEditor : System.Web.UI.UserControl
{
    private ProfileCommon _profile;

    public ProfileCommon UserProfile
    {
        get { return _profile; }
        set 
        { 
            _profile = value;
            UpdateProfileLimitations();
            UpdateControlState();
        }
    }

    private void UpdateControlState()
    {
        using (var swisErrorsContext = new SwisErrorsContext())
        {
            if (UserProfile.LimitationID3 <= 0)
            {
                this.Controls.Remove(trLimitation3);
            }
            else
            {
                UpdateRow(Limitation.GetLimitationByID(UserProfile.LimitationID3), phLim3Type, phLim3Detail);
            }

            if (UserProfile.LimitationID2 <= 0)
            {
                this.Controls.Remove(trLimitation2);
            }
            else
            {
                UpdateRow(Limitation.GetLimitationByID(UserProfile.LimitationID2), phLim2Type, phLim2Detail);
            }

            if (UserProfile.LimitationID1 <= 0)
            {
                this.Controls.Remove(trLimitation1);
            }
            else
            {
                UpdateRow(Limitation.GetLimitationByID(UserProfile.LimitationID1), phLim1Type, phLim1Detail);
            }


            // hide the add limitation button if all three limitations are defined
            if ((UserProfile.LimitationID1 != 0) && (UserProfile.LimitationID2 != 0) && (UserProfile.LimitationID3 != 0))
            {
                this.btnNewLimitation.Visible = false;
            }
            else
            {
                this.btnNewLimitation.Visible = true;
            }

            // hide the "no limitations" message if there is at least one nonzero account limitation id
            if (0 == (UserProfile.LimitationID1 | UserProfile.LimitationID2 | UserProfile.LimitationID3))
            {
                this.trNoLimsMessage.Visible = true;
            }
            else
            {
                this.trNoLimsMessage.Visible = false;
            }

            var swisErrorMessages = swisErrorsContext.GetErrorMessages();
            if (swisErrorMessages.Any())
            {
                SwisfErrorControl.SetError(
                    swisErrorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error))
                        .ToList());
                SwisfErrorControl.Visible = true;
            }
            else
            {
                SwisfErrorControl.Visible = false;
            }
        }
    }

    private void UpdateRow(Limitation limit, PlaceHolder phType, PlaceHolder phDetail)
    {
        phType.Controls.Clear();
        phDetail.Controls.Clear();

        phType.Controls.Add(new LiteralControl(HttpUtility.HtmlEncode(limit.Type.Name)));

        if (limit.Type.Method == LimitationMethod.Pattern)
        {
            var encodedItems = limit.Items.Select(HttpUtility.HtmlEncode);
            phDetail.Controls.Add(new LiteralControl(string.Join("<br />", encodedItems)));
        }
        else
        {
            phDetail.Controls.Add(new LiteralControl(string.Join("<br />", limit.GetDisplayNames().Select(HttpUtility.HtmlEncode))));
        }
    }

    private void UpdateProfileLimitations()
    {
        bool needsSave = false;

        if (0 == UserProfile.LimitationID2)
        {
            if (0 != UserProfile.LimitationID3)
            {
                UserProfile.LimitationID2 = UserProfile.LimitationID3;
                UserProfile.LimitationID3 = 0;
                needsSave = true;
            }
        }
    
        if (0 == UserProfile.LimitationID1)
        {
            if (0 != UserProfile.LimitationID2)
            {
                UserProfile.LimitationID1 = UserProfile.LimitationID2;
                UserProfile.LimitationID2 = 0;
                needsSave = true;
            } 
        }

        // this last check needed for when 2 and 3 are nonzero
        if ((0 == UserProfile.LimitationID2) && (0 != UserProfile.LimitationID1) && (0 != UserProfile.LimitationID3))
        {
            UserProfile.LimitationID2 = UserProfile.LimitationID3;
            UserProfile.LimitationID3 = 0;
            needsSave = true;
        }

        if (needsSave)
        {
            UserProfile.Save();
            AccountManagementDAL.UpdateGroupUsersLimitations(UserProfile.UserName, UserProfile.AccountType, UserProfile.LimitationID1, UserProfile.LimitationID2, UserProfile.LimitationID3);
        }
    }

    protected void DeleteClick1(object sender, EventArgs e)
    {
        if(0 != UserProfile.LimitationID1)
        {
            Limitation.DeleteLimitation(UserProfile.LimitationID1);
            UserProfile.LimitationID1 = 0;
            
            UserProfile.Save();
            AccountManagementDAL.UpdateGroupUsersLimitations(UserProfile.UserName, UserProfile.AccountType, UserProfile.LimitationID1, UserProfile.LimitationID2, UserProfile.LimitationID3);

            UpdateProfileLimitations();
            UpdateControlState();
        }
    }

    protected void DeleteClick2(object sender, EventArgs e)
    {
        if (0 != UserProfile.LimitationID2)
        {
            Limitation.DeleteLimitation(UserProfile.LimitationID2);
            UserProfile.LimitationID2 = 0;
            UserProfile.Save();
            AccountManagementDAL.UpdateGroupUsersLimitations(UserProfile.UserName, UserProfile.AccountType, UserProfile.LimitationID1, UserProfile.LimitationID2, UserProfile.LimitationID3);

            UpdateProfileLimitations();
            UpdateControlState();
        }
    }

    protected void DeleteClick3(object sender, EventArgs e)
    {
        if (0 != UserProfile.LimitationID3)
        {
            Limitation.DeleteLimitation(UserProfile.LimitationID3);
            UserProfile.LimitationID3 = 0;
            UserProfile.Save();
            AccountManagementDAL.UpdateGroupUsersLimitations(UserProfile.UserName, UserProfile.AccountType, UserProfile.LimitationID1, UserProfile.LimitationID2, UserProfile.LimitationID3);

            UpdateProfileLimitations();
            UpdateControlState();
        }
    }

    protected void EditClick1(object sender, EventArgs e)
    {
        string SafeUserName = this.Server.UrlEncode(UserProfile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)        
        Response.Redirect(
            string.Format("/Orion/Admin/EditLimitation.aspx?LimitationID={0}&AccountID={1}&ReturnTo={2}", 
                UserProfile.LimitationID1, SafeUserName, ReferrerRedirectorBase.GetReturnUrl())
            );
    }

    protected void EditClick2(object sender, EventArgs e)
    {
        string SafeUserName = this.Server.UrlEncode(UserProfile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)        
        Response.Redirect(
            string.Format("/Orion/Admin/EditLimitation.aspx?LimitationID={0}&AccountID={1}&ReturnTo={2}",
                UserProfile.LimitationID2, SafeUserName, ReferrerRedirectorBase.GetReturnUrl())
            );
    }

    protected void EditClick3(object sender, EventArgs e)
    {
        string SafeUserName = this.Server.UrlEncode(UserProfile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)        
        Response.Redirect(
            string.Format("/Orion/Admin/EditLimitation.aspx?LimitationID={0}&AccountID={1}&ReturnTo={2}",
                UserProfile.LimitationID3, SafeUserName, ReferrerRedirectorBase.GetReturnUrl())
            );
    }

    protected void AddClick(object sender, EventArgs e)
    {
        string SafeUserName = this.Server.UrlEncode(UserProfile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)        
        if (0 == UserProfile.LimitationID1)
            Response.Redirect(string.Format("/Orion/Admin/SelectLimitationType.aspx?AccountID={0}&Index=1&ReturnTo={1}", SafeUserName, ReferrerRedirectorBase.GetReturnUrl()));
        else if(0 == UserProfile.LimitationID2)
            Response.Redirect(string.Format("/Orion/Admin/SelectLimitationType.aspx?AccountID={0}&Index=2&ReturnTo={1}", SafeUserName, ReferrerRedirectorBase.GetReturnUrl()));
        else if (0 == UserProfile.LimitationID3)
            Response.Redirect(string.Format("/Orion/Admin/SelectLimitationType.aspx?AccountID={0}&Index=3&ReturnTo={1}", SafeUserName, ReferrerRedirectorBase.GetReturnUrl()));
    }
}
