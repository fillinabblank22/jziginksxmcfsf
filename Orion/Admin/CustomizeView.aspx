<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" AutoEventWireup="true" CodeFile="CustomizeView.aspx.cs" Inherits="Orion_Admin_CustomizeView" Title="<%$ Resources: CoreWebContent, WEBDATA_AK0_201 %>" %>
<%@ Register TagPrefix="orion" TagName="resourceEditor" Src="~/Orion/Admin/ResourceEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="ViewGroupTitleEditor" Src="~/Orion/Admin/ViewGroupTitleEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="ViewGroupEditor" Src="~/Orion/Admin/ViewGroupEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="ViewNOCEnabler" Src="~/Orion/Admin/ViewNOCEnabler.ascx" %>
<%@ Register TagPrefix="orion" TagName="ViewLimitationEditor" Src="~/Orion/Admin/ViewLimitationEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="ViewGroupEnabler" Src="~/Orion/Admin/ViewGroupEnabler.ascx" %>
<%@ Register TagPrefix="orion" TagName="viewGroupSubNav" Src="~/Orion/ViewGroupSubNav.ascx" %>

<%@ Import Namespace="Resources" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
	<style type="text/css">
        h1 { font-size: large; padding-bottom: 10px; margin: 0; }
        strong { font-weight: bold; }
		.viewColumnButtons input { vertical-align: top; margin-bottom: 4px; }
        <% if (_hideViewLimitation.Contains("ViewLimitationSection"))
           {%>
        #viewLimitation {display: none}
        <% } %>
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <h1><asp:Literal runat="server" ID="PageHeader"></asp:Literal></h1>
    
    <orion:ViewGroupTitleEditor runat="server" ID="myResourceTitleEditor" />
    <orion:ViewGroupEnabler runat="server" ID="myViewGroupEnabler" />   
    
    <!-- This Markup is similar to /Admin/ViewGroupAddTab and should most likly be kept in sync -->
    <table>
        <tr>
            <td class="subNavWrapperAdmin"> <orion:viewGroupSubNav runat="server" ID="myViewGroupSubNav" /> </td>
            <td>
                <orion:ViewGroupEditor runat="server" ID="myViewGroupEditor" />
                <orion:resourceEditor runat="server" ID="myResourceEditor" />
            </td>
        </tr>
    </table>      
    <!-- End region-->
    
   <div style="float:left"> <orion:ViewNOCEnabler runat="server" ID="myViewNOCEnabler" NocViewEnableChangedJs='function(enabled){if (enabled)$("[id*=btnGoToNOC]").show();else $("[id*=btnGoToNOC]").hide();}' /></div>
    
    <div style="clear: left; width: 70%;">
        <div id="viewLimitation">
            <p>
		    <h2><%= CoreWebContent.WEBDATA_VB0_276 %></h2>
                <%= CoreWebContent.WEBDATA_VB0_277 %>
            </p>
        
            <orion:ViewLimitationEditor ID="ViewLimitationEditor1" runat="server" />
        </div>
        <div runat="server" ID="emptyTabLimitation" Visible="False"><%=CoreWebContent.WEBDATA_TM1_NEW_TAB_LIMIT %></div>
        <br />
        <div class="sw-btn-bar" >
            <orion:LocalizableButton ID="LocalizableButton1" runat="server" OnClick="DoneClick" DisplayType="Primary" LocalizedText="Done" />       
            <orion:LocalizableButton ID="btnGoToNOC" runat="server" OnClick="DoneAndGoToNocClick" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_184 %>" />
            <orion:LocalizableButtonLink ID="Preview" runat="server" DisplayType="Secondary" Target="_blank" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_280 %>" Visible="false"  ToolTip="<%$ Resources: CoreWebContent, WEBDATA_VB0_281 %>"/>
        </div>
        <div class="StatusMessage"><asp:PlaceHolder runat="server" ID="phErrorMessage"/></div>
    </div>
</asp:Content>
