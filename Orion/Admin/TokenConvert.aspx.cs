﻿using System;
using SolarWinds.Orion.Core.Common.i18n;

public partial class Orion_Admin_TokenConvert : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ConvertButton_OnClick(object sender, EventArgs e)
    {
        try
        {
            var interop = new InteropServices();
            Dest.Text = interop.ParseData(Source.Text);
        }
        catch (Exception ex)
        {
            Dest.Text = "Error: " + ex.Message;
        }

    }

}