<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
	CodeFile="EditMenuBar.aspx.cs" Inherits="Orion_Admin_EditMenuBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
	<orion:Include runat="server" File="EditMenuBars.css" />
    <orion:Include runat="server" File="jquery-1.7.1/jquery.tinysort.js" />

    <style type="text/css">
    #editCustomMenuItemDialog .sw-pg-dialoginner { padding: 5px 25px 0px 25px; }
    #editCustomMenuItemDialog .sw-pg-dialogfooter { padding: 10px 0px 10px 5px; }
    #editCustomMenuItemDialog { background: white; height: 100%; margin: 0px; }
    #editCustomMenuItemDialog tbody th { text-align: right; }
    #editCustomMenuItemDialog td { padding: 5px 0; }
    #editCustomMenuItemDialog input { margin-left: 5px; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
	<h1>
		<%= DefaultSanitizer.SanitizeHtml(Page.Title) %>
	</h1>
	<br />
	<div runat="server" id="newMenuBar" visible="false" style="margin-bottom: 15px;">
		<br />
		<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_28) %><asp:TextBox runat="server" ID="newName" />&nbsp;<asp:CustomValidator ID="newNameCustomValidator" ControlToValidate="newName" OnServerValidate="newNameCustomValidator_Validate" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_PS0_28 %>" runat="server"></asp:CustomValidator>
	</div>
	<div style="width: 600px; padding-bottom: 10px;">
		<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_29) %>
	</div>
	<div runat="server" id="errorMessage" visible="false" style="color: red; font-weight: bold;
		margin-bottom: 15px;" />
	<table>
		<tr>
			<td style="vertical-align: top;">
				<div id="availableItems">
					<div class="menuHeader">
						<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_30) %></div>
					<asp:Repeater runat="server" ID="availableMenuItems">
						<ItemTemplate>
							<div class="menuItem" title="<%# HttpUtility.HtmlAttributeEncode(Eval("Description").ToString().Trim()) %>" id="<%# DefaultSanitizer.SanitizeHtml(Eval("MenuItemID")) %>">
								<span style="float: left;"><%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Eval("Title").ToString()) %>
                                </span> 
								<span style="float: right;" id="Span1" runat="server" visible='<%# !(bool)Eval("IsSystem") %>'>
                                    <span class="editCustomItem">
                                        <orion:LocalizableButtonLink runat="server" ID="btnEditMenu" DisplayType="Small" LocalizedText="Edit"></orion:LocalizableButtonLink>
                                    </span>
									<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="/NetPerfMon/images/SmallButton.Delete3.png" CssClass="handCursor"
										alt="Edit" CommandArgument='<%# DefaultSanitizer.SanitizeHtml(Eval("MenuItemID")) %>' OnClientClick="return ShowConfirm();"
										OnClick="DeleteCustomMenuItemClick" />
								</span>
                                <div style="clear:both;"></div>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
                <div id="addCustomItem">
                    <orion:LocalizableButton ID="btnAddCustomItem" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_138 %>" DisplayType="Small"/>
                </div>
			</td>
			<td style="vertical-align: top; padding-left: 15px; ">
				
			     <div id="selectedItems">
					<div class="menuHeader">
						<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_31) %></div>
					<asp:Repeater runat="server" ID="menuItems">
						<ItemTemplate>
							<div class="menuItem" title="<%# HttpUtility.HtmlEncode(Eval("Description").ToString().Trim()) %>" id="<%# DefaultSanitizer.SanitizeHtml(Eval("MenuItemID")) %>">
								<span style="float:left;"><%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Eval("Title").ToString())   %></span>
								<span id="Span2" runat="server" style="float: right; " visible='<%# !(bool)Eval("IsSystem") %>'>
									<span class="editCustomItem">
                                        <orion:LocalizableButtonLink runat="server" ID="btnEditMenu" DisplayType="Small" LocalizedText="Edit"></orion:LocalizableButtonLink>
                                    </span>
									<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="/NetPerfMon/images/SmallButton.Delete3.png" CssClass="handCursor"
										alt="Edit" CommandArgument='<%# DefaultSanitizer.SanitizeHtml(Eval("MenuItemID")) %>' OnClientClick="return ShowConfirm();"
										OnClick="DeleteCustomMenuItemClick" />
								</span>
                                <div style="clear:both;"></div>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
				<orion:LocalizableButton ID="ImageButton3" runat="server" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick"
					OnClientClick="if (checkBeforeSave()) {saveCurrentMenu();return true} else return false;" />
                    <orion:LocalizableButtonLink ID="CancelButton" runat="server" LocalizedText="Cancel" DisplayType="Secondary" NavigateUrl="/Orion/Admin/SelectMenuBar.aspx"/>
				<asp:HiddenField runat="server" ID="selectedMenuItems" />
			</td>
		</tr>
	</table>

	<script type="text/javascript">
	    //<![CDATA[      
	    var ShowConfirm = function () { return confirm('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_51) %>'); }
	    var sortAvailable = function() {
	        $("#availableItems .menuItem").tsort();
	    }
	    var saveCurrentMenu = function() {
	        $("#<%= this.selectedMenuItems.ClientID %>").val($("#selectedItems").sortable("toArray"));
	    }
	    var checkBeforeSave = function() {
	        if ($("#<%= this.newName.ClientID %>").val() == '') { alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_48) %>"); return false; }
	        if ($("#selectedItems").sortable("toArray") == '' && !confirm("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_49) %>")) { return false; }
	        return true;
	    }
	    var showDialog = function(title) {

            $("#editCustomMenuItemDialog").show().dialog({
                width: '650px',
                height: 'auto', 
                modal: true, 
                overlay: { "background-color": "black", opacity: 0.4 }, 
                title: $("<div/>").text(title).html(), 
                close: function () { $('#editCustomMenuItemDialog').css('width','auto').css('height','auto'); }
            });

	    };
	    var fillDialog = function(menuItem) {
	        $("#title").val(menuItem.Title);
	        $("#url").val(menuItem.LinkTarget);
	        $("#description").val(menuItem.Description);
	        $("#openNewWindow").get(0).checked = menuItem.OpenInNewWindow;
	        $("#<%= this.editCustomItemID.ClientID %>").val(menuItem.MenuItemID);
	    };
	    var saveCustomItemData = function() {
            $("#<%= this.editCustomTitle.ClientID %>").val($("#title").val());
            $("#<%= this.editCustomURL.ClientID %>").val($("#url").val());
	        $("#<%= this.editCustomDescription.ClientID %>").val($("#description").val());
	        $("#<%= this.editCustomOpenNewWindow.ClientID %>").val($("#openNewWindow").get(0).checked);
	        return validateCustomItem();
	    }
	    var trim = function(s) {
	        return s.replace(/^\s+|\s+$/, '');
        };
        var validateCustomItem = function () {
            if (trim($("#<%= this.editCustomTitle.ClientID %>").val()).length == 0) {
                alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_52) %>');
                return false;
            }
            if (trim($("#<%= this.editCustomURL.ClientID %>").val()).length == 0) {
                alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_53) %>');
                return false;
            }
	        saveCurrentMenu();
	        return true;
	    };
	    var reportError = function(error) {
	        alert(error.get_message() + '\n' + error.get_stackTrace());
	    };

	    var tooltipHandle = 0;
	    var tooltipEnabled = true;

        var cancelTooltip = function() {
	        if (tooltipHandle) clearTimeout(tooltipHandle);
	        setTimeout(function() { $("#MenuItemTooltip").hide(); }, 0);
        };
        
        var disableTooltip = function() {
            cancelTooltip();
            tooltipEnabled = false;
        }
        var enableTooltip = function() {
            tooltipEnabled = true;
        }

	    $(function() {
	        $("form").submit(function() {
	            saveCurrentMenu();
	        });
	        $("#availableItems").sortable({
	            items: "> .menuItem",
	            connectWith: ["#selectedItems"],
	            stop: function() { enableTooltip(); sortAvailable(); }, 
	            deactivate: enableTooltip,
                receive: sortAvailable,
	            cancel: ".menuHeader",       
	            start: function() { disableTooltip(); $(this).css('z-index','2'); }, 
	            over: function() { $(this).css('z-index', '1'); }
	            /* z-index setting is only fix for IE7 issue */
	        });
	        $("#selectedItems").sortable({
	            items: "> .menuItem",
	            connectWith: ["#availableItems"],
	            stop: enableTooltip,
	            deactivate: enableTooltip,
	            cancel: ".menuHeader",
	            start: function() { disableTooltip(); $(this).css('z-index','2'); }, 
	            over: function() { $(this).css('z-index', '1'); }
	        });

      	    $(".menuItem[title!='']").each(function() {
      	        $(this).data("tooltip", this.title).removeAttr("title");
      	    });
      	    $(".menuItem").hover(
      	        function(e) {
      	            if(tooltipEnabled) {
        	            var tooltiptext = $(this).data("tooltip");
        	            if (tooltipHandle) clearTimeout(tooltipHandle);
        	            tooltipHandle = setTimeout(function() {
        	                $("#MenuItemTooltip").text(tooltiptext);
        	                $("#MenuItemTooltip").show();
        	            }, 500);
        	        }
      	        },
      	        cancelTooltip
      		);
      	    $(".menuItem").mousemove(function(e) {
    	       $("#MenuItemTooltip").css("top", (e.pageY + 10) + "px").css("left", (e.pageX + 20) + "px");
      	    });

	        $("#<%= AddEditCancel.ClientID %>").click(function() {
	            $("#editCustomMenuItemDialog").dialog('close');
	        });

	        $("#addCustomItem").click(function () {
	            var newItem = { MenuItemID: 0, Title: '', LinkTarget: '', Description: '', OpenInNewWindow: true };
	            showDialog("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_46) %>");
	            fillDialog(newItem);
	            return false;
	        });
	        $(".editCustomItem").click(function () {
	            var id = $(this).parent().parent().attr("id");
	            PageMethods.GetCustomItem(id, function (result) {
	                showDialog("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_47) %>");
	                fillDialog(result);
	                return false;
	            }, reportError);
	        });
	        sortAvailable();
	    });
	    //]]>
	</script>

	<asp:HiddenField runat="server" ID="editCustomItemID" />
	<asp:HiddenField runat="server" ID="editCustomTitle" />
	<asp:HiddenField runat="server" ID="editCustomURL" />
	<asp:HiddenField runat="server" ID="editCustomDescription" />
	<asp:HiddenField runat="server" ID="editCustomOpenNewWindow" />
	
	<div id="editCustomMenuItemDialog" style="display: none;">
    <div class="sw-pg-dialoginner"><table style="margin: 0 auto;">
			<tbody>
				<tr>
					<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_14) %></th>
					<td><input type="text" id="title" /></td>
				</tr>
				<tr>
					<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_137) %></th>
					<td><input type="text" id="url" size="60" /></td>
				</tr>
				<tr>
					<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_136) %></th>
					<td><input type="text" id="description" size="60" /></td>
				</tr>
				<tr>
					<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_32) %></th>
					<td><input type="checkbox" id="openNewWindow" /></td>
				</tr>
                <tr>
                    <th></th>
                    <td>
                    <div class="sw-btn-bar">
			            <orion:LocalizableButton runat="server" LocalizedText="Ok" DisplayType="Primary" OnClientClick="return saveCustomItemData();" OnClick="EditCustomItemClick" />
			            <orion:LocalizableButtonLink id="AddEditCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" NavigateUrl="#" onclick="return false;"/>
                    </div>
                    </td>
                </tr>
			</tbody>
		</table></div>
	</div>
	<div id="MenuItemTooltip" ></div>
</asp:Content>