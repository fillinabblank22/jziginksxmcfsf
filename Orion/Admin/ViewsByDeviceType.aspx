<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="ViewsByDeviceType.aspx.cs" Inherits="Orion_Admin_ViewsByDeviceType"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_7 %>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <table>
        <tr>
            <th colspan="2" style="text-align:center!important">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_162) %>
            </th>
        </tr>
        <tr>
            <td>
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_210) %></b>
            </td>
            <td>
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_123) %></b>
            </td>
        </tr>
        <asp:Repeater runat="server" ID="rptNodes">
            <ItemTemplate>
                <tr>
                    <td>
                        <%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>
                    </td>
                    <td>
                        <asp:ListBox runat="server" OnDataBinding="NodeViewsDataBinding" Rows="1" SelectionMode="single">
                            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_124 %>" Value="0" />
                        </asp:ListBox>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="interfaceWrapper">
                    <table width="100%">
                        <tr>
                            <th colspan="2" style="text-align:center!important">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_36) %>
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_210) %></b>
                            </td>
                            <td>
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_123) %></b>
                            </td>
                        </tr>
                        <asp:Repeater runat="server" ID="rptInterfaces">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>
                                    </td>
                                    <td>
                                        <asp:ListBox ID="ListBox1" runat="server" OnDataBinding="InterfaceViewsDataBinding"
                                            Rows="1" SelectionMode="single">
                                            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_124 %>" Value="0" />
                                        </asp:ListBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
    </div>
</asp:Content>
