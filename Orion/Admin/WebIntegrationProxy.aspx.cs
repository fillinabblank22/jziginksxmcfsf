﻿using System;
using System.Web;
using System.Text;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Admin_WebIntegrationProxy : System.Web.UI.Page
{
    private static readonly Log Log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private const string UrlParamName = "url";
    private const string LinkIdParamName = "linkId";
    private const string TokenParamName = "sessionId";
    private const string ProductLinkUrl = @"https://www.solarwinds.com/embedded_in_products/productLink.aspx?id={0}";
    //private const string ProductLinkUrl = @"http://qa12.www.solarwinds.com/embedded_in_products/productLink.aspx?id={0}";

    protected void Page_Init(object sender, EventArgs e)
    {
        AuthorizationChecker.AllowAdmin(Resources.CoreWebContent.WEBDATA_TM1_ALLOW_ADMIN);

        var url = Request.QueryString[UrlParamName];
        if (String.IsNullOrEmpty(url))
        {
            string id = Request.QueryString[LinkIdParamName];
            if (!String.IsNullOrEmpty(id))
            {
                url = GetRedirectUrlFromId(id);
            }
            else
            {
                //redirect somewhere else?
                return;
            }
        }
        else
        {
            url = GetRedirectUrlFromurl(url);
        }

        if (Request.HttpMethod.Equals("post", StringComparison.InvariantCultureIgnoreCase))
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(@"<html><body onload='document.forms[""form""].submit()'>");
            sb.AppendFormat("<form name='form' action='{0}' method='post'>", url);
            foreach (var key in Request.Form.AllKeys)
            {
                sb.AppendFormat("<input type='hidden' name='{0}' value='{1}'>", key, HttpUtility.HtmlEncode(Request.Form[key]));
            }
            sb.Append("</form></body></html>");
            Response.Write(sb.ToString());

            Response.End();
        }

        Response.Redirect(url,true);
    }

    private string GetRedirectUrlFromurl(string url)
    {
        var origUrl = url;
        try
        {
            var token = GetUserToken();

            if (!String.IsNullOrEmpty(token))
            {
                var builder = new UriBuilder(url);
                var query = HttpUtility.ParseQueryString(builder.Query);
                query[TokenParamName] = token;
                builder.Query = query.ToString();
                url = builder.ToString();
            }
        }
        catch (Exception ex)
        {
            Log.Error("An error occured while processing url.", ex);
            url = origUrl;
        }
        return url;
    }

    private string GetRedirectUrlFromId(string id)
    {
        string url = String.Format(ProductLinkUrl, id);

        //copy all other url params
        var builder = new UriBuilder(url);
        var query = HttpUtility.ParseQueryString(String.Empty);
        foreach (var key in Request.QueryString.AllKeys)
        {
            if (key != "id")
            {
                query[key != LinkIdParamName ? key : "id"] = Request.QueryString[key];
            }
        }

        var token = GetUserToken();
        if (!String.IsNullOrEmpty(token))
        {
            query[TokenParamName] = token;
        }

        builder.Query = query.ToString();
        return builder.ToString();
    }

    private string GetUserToken()
    {
        using (var proxy = _blProxyCreator.Create(ex => Log.Error("Call to business layer failed.", ex)))
        {
            try
            {
                var username = HttpContext.Current.Profile.UserName;
                return proxy.IsUserWebIntegrationAvailable(username)
                    ? proxy.GetUserWebIntegrationToken(username).Token
                    : null;
            }
            catch (Exception ex)
            {
                Log.Error("An error occured while trying to get user token.", ex);
                return null;
            }
        }
    }
}