using System;
using SolarWinds.Orion.Core.Common.PackageManager;

public partial class Orion_Admin_DependenciesView : System.Web.UI.Page
{
    protected bool allowTopology;
	
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        allowTopology = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Topology");
    }
}
