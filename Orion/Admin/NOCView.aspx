<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" AutoEventWireup="true" CodeFile="NOCView.aspx.cs" 
    Inherits="Orion_Admin_NOCView" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_TM0_48 %>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
<style type="text/css">
  #adminContent{width:auto!important;}
  #adminContent td {padding: 0px 0px 0px 0px !important; vertical-align: middle;}
  .x-tip-body {white-space: nowrap !important;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Admin/js/NOCViewsGrid.js" />
    <div style="font-size: large; padding-bottom: 10px; "><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></div>
    <div id="NOCViewsGrid"></div>
    <input type="hidden" id="ReturnToUrl" value="<%= DefaultSanitizer.SanitizeHtml(ReferrerRedirectorBase.GetReturnUrl()) %>" />
</asp:Content>

