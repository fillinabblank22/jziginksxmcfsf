﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminBucket.ascx.cs" Inherits="Orion_Admin_AdminBucket" %>

<% if (!_hiddenSections.Contains(ID))
   {%>
       
<%--NOTE: this control is not used in voltron.  It is only here so modules (like NPM)--%>
<%--have something to compile against.  The class that is actually used is Orion_Admin_VoltronAdminBucket --%>

<div class="<%= NewStyle?"NewIconContentBucket":"ContentBucket" %>">
    <% if (!WithoutHeader)
       { %>
            <img class="NewBucketIcon" src="<%= DefaultSanitizer.SanitizeHtml(Icon) %>" />
            <div class="HeaderBox">
                <div class="<%= NewStyle ? "NewBucketHeader" : "BucketHeader" %>"><%= DefaultSanitizer.SanitizeHtml(Header) %></div>
                <p>
                    <asp:PlaceHolder ID="placeDescription" runat="server"></asp:PlaceHolder>
                </p>
            </div>
    <% } %>
    <table class="<%= NewStyle?"NewIconBucketLinkContainer":"BucketLinkContainer" %>">
        <asp:Repeater ID="rptRows" runat="server" OnItemDataBound="RowDataBound">
            <ItemTemplate>
                <tr>
                    <asp:Repeater ID="rptCols" runat="server" OnItemDataBound="ColumnDataBound">
                        <ItemTemplate>
                            <td class="LinkColumn" width="<%= ColumnWidth %>%">
                                <asp:MultiView ID="container" runat="server">
                                    <asp:View ID="nonEmpty" runat="server">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span>
                                            <asp:HyperLink runat="server" ID="href"></asp:HyperLink>
                                        </p>
                                        <p>
                                            <asp:Label ID="description" runat="server" Text=""></asp:Label>
                                        </p>
                                    </asp:View>
                                    <asp:View ID="empty" runat="server">&nbsp;</asp:View>
                                    <asp:View ID="custom" runat="server"></asp:View>
                                </asp:MultiView>
                            </td>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</div>
<% } %>