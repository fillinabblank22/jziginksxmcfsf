﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common.Models;

/// <summary>
/// This control displays module/product license details if module/product does not supply its own plugin
/// </summary>
public partial class Orion_Admin_Details_OtherModulesDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log Logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                OtherModules.ItemDataBound += new RepeaterItemEventHandler(ItemDataBound);

                var otherModules = ModuleDetailsHelper.GetOtherModuleDetails();
                var modules = GetModuleAndLicenseInfo(otherModules);
                 
                OtherModules.DataSource = modules;
                OtherModules.DataBind();
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error while displaying details for extra pollers. Details: {0}", ex.ToString());
            }
        }
    }

    void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var moduleBlock = ControlHelper.FindControlRecursive(e.Item, "ModuleBlock") as Orion_Admin_License_ModuleDetailsBlock;
        if (moduleBlock != null)
        {
            var item = (KeyValuePair<string, Dictionary<string, string>>)e.Item.DataItem;
            moduleBlock.DataSource = item.Value;            
            moduleBlock.Name = item.Key;
        }
    }

    private Dictionary<string, Dictionary<string, string>> GetModuleAndLicenseInfo(List<ModuleInfo> moduleInfos)
    {
        var modules = new Dictionary<string, Dictionary<string, string>>();

        foreach (var module in moduleInfos)
        {
            var values = new Dictionary<string, string>();

			if (!String.IsNullOrEmpty(module.LicenseInfo))
				values.Add(Resources.CoreWebContent.WEBCODE_AK0_132, module.LicenseInfo);
            values.Add(Resources.CoreWebContent.WEBCODE_AK0_131, module.ProductName);
            values.Add(Resources.CoreWebContent.WEBDATA_AK0_84, module.Version);
            values.Add(Resources.CoreWebContent.WEBDATA_VB0_176, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.CoreWebContent.WEBCODE_SO0_9 : module.HotfixVersion);

            modules.Add(module.ProductDisplayName, values);
        }
        return modules;
    }    
}
