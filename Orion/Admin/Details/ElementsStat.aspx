<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="ElementsStat.aspx.cs" Inherits="Orion_Admin_Details_ElementsStat" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_209 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:content id="Content1" runat="server" contentplaceholderid="TopRightPageLinks">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHAdminDetailsDatabase" />
    <script>
        $(document).ready(function () {
            $("#tableCounts").tablesorter({ sortList: [[1, 1]] });
        });
    </script>
    <style>
        thead tr .header {
            background-position: right center;
            background-repeat: no-repeat;
            cursor: pointer;
            padding: 5px;
            background-color: #204a63;
            color: white;
            background-image: url("/Orion/images/Arrows/bg.gif");
        }

        tbody tr td {
            padding-right: 5px;
            padding-left: 5px !important;
        }

        thead tr .headerSortUp {
            background-image: url("/Orion/images/Arrows/asc.gif");
        }

        thead tr .headerSortDown {
            background-image: url("/Orion/images/Arrows/desc.gif");
        }
    </style>
</asp:content>

<asp:content contentplaceholderid="adminContentPlaceholder" runat="Server">
    <!-- Page title and hot links -->
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

    <div class="GroupBox" id="DatabaseStats" style="padding: 10px;">
                <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_210) %></h2>
        
        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemBound">
                        <HeaderTemplate>
        <table id="tableCounts" border="1px" cellpadding="5" style="text-align: left; border-style: solid; border-collapse: collapse; min-width: 50%">
                  <thead>
                    <tr> 
                        <asp:Repeater runat="server" ID="headerRepeater">
                            <ItemTemplate>
                                <th> <%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %> </th>
                            </ItemTemplate>
                        </asp:Repeater>
                   </tr>
                </thead> 
                    <tbody>
                    </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                  <asp:Repeater ID="columnRepeater" runat="server">
                                    <ItemTemplate>
                                        <td><%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %></td>
                                    </ItemTemplate>
                                  </asp:Repeater>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> 
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
    </div>

</asp:content>
