﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Admin_License_ModuleDetailsBlock : System.Web.UI.UserControl
{
    public string Name { get; set; }

    public IDictionary<string, string> DataSource
    {
        get { return ModuleInfo.DataSource as IDictionary<string, string>; }
        set {
            if (ModuleInfo.DataSource == null || !ModuleInfo.DataSource.Equals(value))
            {
                ModuleInfo.DataSource = value;
                ModuleInfo.DataBind();
            }
        }
    }
}