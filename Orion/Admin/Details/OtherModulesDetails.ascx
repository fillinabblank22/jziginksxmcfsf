﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OtherModulesDetails.ascx.cs" Inherits="Orion_Admin_Details_OtherModulesDetails" %>

<%@ Register Src="~/Orion/Admin/Details/ModuleDetailsBlock.ascx" TagPrefix="orion" TagName="ModuleDetailBlock" %>

<div>
    <asp:Repeater ID="OtherModules" runat="server">
        <ItemTemplate>
            <orion:ModuleDetailBlock ID="ModuleBlock" runat ="server" />
        </ItemTemplate>
    </asp:Repeater>
</div>