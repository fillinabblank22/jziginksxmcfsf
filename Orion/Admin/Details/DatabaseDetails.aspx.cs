using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web;

using RegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;
using SolarWinds.Orion.Web.Plugins;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Details_DatabaseDetails : System.Web.UI.Page
{
    protected static HashSet<String> _hiddenDatabaseDetailsSections = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.DatabaseDetailsSettingsHideSection)).Select(c => c.Key));

    protected static bool _areTrapsAllowed;
    protected static bool _areSyslogAllowed;

    private string GetReg(string key, string defValue)
    {
        return RegistrySettings.GetSetting("SWNetPerfMon", "Settings", key, defValue);
    }

    private string GetSettings(string key, string defValue)
    {
        OrionSetting setting = SettingsDAL.GetSetting(key);

        return (setting != null) ? setting.SettingValue.ToString() : defValue;
    }

    private DateTime GetTime(string key, DateTime defaultValue, bool forceLoad)
    {
        OrionSetting setting = SettingsDAL.GetSetting(key, forceLoad);

        if ((setting != null) && (setting.SettingValue > 0))
        {
            DateTime time = DateTime.FromOADate(setting.SettingValue);

            // rounding problem
            if (time.Second > 30)
                time = time.AddMinutes(1);
            return time;
        }

        return defaultValue;
    }

    private string GetOATime(string key, string defValue)
    {
        OrionSetting setting = SettingsDAL.GetSetting(key);

        if (setting != null)
        {
            DateTime time = DateTime.FromOADate(setting.SettingValue);

            // rounding problem
            if (time.Second > 30)
                time = time.AddMinutes(1);

            return time.ToShortTimeString();
        }

        return defValue;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        _areTrapsAllowed = TrapsSysLogsAvailabilityManager.Instance.IsLegacyTrapsEnabled;
        _areSyslogAllowed = TrapsSysLogsAvailabilityManager.Instance.IsLegacySysLogEnabled;


        var data = new List<DataProps>();

        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_230, System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().HostName.ToUpper(),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_131, OSVersionHelper.GetOSName(),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_231, System.Environment.OSVersion.Version.ToString(),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_176, System.Environment.OSVersion.ServicePack,""));
        data.Add(new DataProps("&nbsp;", "&nbsp;", ""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_232, DBMetadataDAL.DataSource,""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_233, DBMetadataDAL.Catalog,""));

        string dbsize = DBMetadataDAL.DatabaseSize;
        if (!string.IsNullOrEmpty(dbsize))
            data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_252, dbsize, ""));
        else
            data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_252, string.Format("<font color=\"red\">{0}<//>", Resources.CoreWebContent.WEBDATA_SO0_248 ), ""));

        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_234, HttpUtility.HtmlEncode(DBMetadataDAL.DatabaseEngine).Replace("\n", "<br />"),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_235, HttpUtility.HtmlEncode(SqlDAL.GetSQLEngineVersion()),""));

        string azureDBtier = DBMetadataDAL.AzureTier;
        if (!string.IsNullOrEmpty(azureDBtier))
            data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_AzureTier, azureDBtier , ""));

        string compatibilityLevel = DBMetadataDAL.SQLCompatibilityLevel;
        if (!string.IsNullOrEmpty(compatibilityLevel))
            data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_SQLCompatibilityLevel, compatibilityLevel,""));
        int InMemoryPercent = DBMetadataDAL.InMemoryPercent;

        
        string WarnMessageInMemory = string.Format("{0} <span class=\"LinkArrow\">� </span><a target = \"_blank\" href=\"{2}\" style =\"text-decoration:underline;\">{1}</a>", Resources.CoreWebContent.WEBCODE_InMemoryWarning, Resources.CoreWebContent.WEBDATA_AK0_404, KnowledgebaseHelper.GetMindTouchKBUrl(133667));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_InMemoryPercent, InMemoryPercent.ToString()+" %", InMemoryPercent > 90 ? WarnMessageInMemory : ""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_237, DBMetadataDAL.IntegratedSecurity ? Resources.CoreWebContent.WEBCODE_VB0_238 : Resources.CoreWebContent.WEBCODE_VB0_236,""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_239, DBMetadataDAL.DatabaseUser,""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_240, DBMetadataDAL.ConnectionTimeoutSeconds.ToString(),""));
        data.Add(new DataProps("&nbsp; ", "&nbsp;", ""));

        if (!_hiddenDatabaseDetailsSections.Contains("NetworkElementDetailSection"))
        {
            data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_171, (SolarWinds.Orion.NPM.Web.Node.Count +
                                                                SolarWinds.Orion.NPM.Web.Interface.Count +
                                                                SolarWinds.Orion.NPM.Web.Volume.Count).ToString(),""));
            data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_AK0_162, SolarWinds.Orion.NPM.Web.Node.Count.ToString(),""));
            data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_36, SolarWinds.Orion.NPM.Web.Interface.Count.ToString(),""));
            data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_37, SolarWinds.Orion.NPM.Web.Volume.Count.ToString(),""));
            data.Add(new DataProps("&nbsp;  ", "&nbsp;", ""));
        }

        using (WebDAL q = new WebDAL())
        {
            data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_241, q.GetAlertCount().ToString(),""));
            data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_240, q.GetEventCount().ToString(),""));
            if (!_hiddenDatabaseDetailsSections.Contains("PollerDetailSection"))
            {
                data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_121, q.GetPollerCount().ToString(),""));
                data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_AK0_14, q.EnginesCount().ToString(),""));
            }
            data.Add(new DataProps("&nbsp;   ", "&nbsp;", ""));
        }

        //Time of the last archive is changed from BL, so don't use the cache
        DateTime lastMaintenanceTime = GetTime("SWNetPerfMon-Settings-Last Archive", DateTime.MinValue, true);
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_243, lastMaintenanceTime > DateTime.MinValue ? FormatHelper.FormatDateTime(DateTime.SpecifyKind(lastMaintenanceTime, DateTimeKind.Utc).ToLocalTime(), true) : Resources.CoreWebContent.Status_Unknown,""));

        DateTime maintenanceTime = GetTime("SWNetPerfMon-Settings-Archive Time", DateTime.MinValue, false);
        
        //let's assume the maintenance is scheduled properly and calculate the expected next maintenance time
        DateTime nextMantenanceTime = DateTime.Today.AddHours(2).AddMinutes(15); // default time 02:15
        if (maintenanceTime > DateTime.MinValue)
            nextMantenanceTime = DateTime.Today.AddHours(maintenanceTime.Hour).AddMinutes(maintenanceTime.Minute);

        if (nextMantenanceTime < DateTime.Now)
            nextMantenanceTime = nextMantenanceTime.AddDays(1);

        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_244, FormatHelper.FormatDateTime(nextMantenanceTime, true),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_245, maintenanceTime > DateTime.MinValue ? FormatHelper.FormatToLocalTime(maintenanceTime) : Resources.CoreWebContent.Status_Unknown,""));

        data.Add(new DataProps("&nbsp;    ", "&nbsp;", ""));

        // these values are now saved in database
        //for support - thry to notify them about huge tables related to the core
        string warnMessage = string.Empty;
        if (CommonWebHelper.WarnRowsNumberEnabled)
            warnMessage  = string.Format(Resources.CoreWebContent.WEBDATA_SO0_244, string.Format("<a href='/Orion/Admin/PollingSettings.aspx' target='_blank'>{0}</a >", Resources.CoreWebContent.WEBCODE_TM0_49), string.Format("<a href='/Orion/Admin/Details/DatabaseStats.aspx' target='_blank'>{0}</a >", Resources.CoreWebContent.WEBDATA_AK0_526));

        DataTable dt = CheckForHugeTables();

        DataRow[] result = dt.Select("table_name LIKE '%_Deta%' AND table_name <> 'ContainerStatus_Detail'");
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_246, FormatHelper.FormatDays(GetSettings("SWNetPerfMon-Settings-Retain Detail", "7")), result.Length > 0 ? warnMessage : ""));
        result = dt.Select("table_name LIKE '%Hour%' AND table_name <> 'ContainerStatus_Hourly'");
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_247, FormatHelper.FormatDays(GetSettings("SWNetPerfMon-Settings-Retain Hourly", "30")), result.Length > 0 ? warnMessage : ""));
        result = dt.Select("table_name LIKE '%Dail%' AND table_name <> 'ContainerStatus_Daily'");
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_248, FormatHelper.FormatDays(GetSettings("SWNetPerfMon-Settings-Retain Daily", "365")), result.Length > 0 ? warnMessage : ""));

        result = dt.Select("table_name like 'ContainerStatus_Deta%'");
        data.Add(new DataProps(Resources.CoreWebContent.WebCode_SO0_73, FormatHelper.FormatDays(GetSettings("SWNetPerfMon-Settings-Retain Container Detail", "7")), result.Length > 0 ? warnMessage : ""));
        result = dt.Select("table_name like 'ContainerStatus_Hour%'");
        data.Add(new DataProps(Resources.CoreWebContent.WebCode_SO0_74, FormatHelper.FormatDays(GetSettings("SWNetPerfMon-Settings-Retain Container Hourly", "30")), result.Length > 0 ? warnMessage : ""));
        result = dt.Select("table_name like 'ContainerStatus_Dail%'");
        data.Add(new DataProps(Resources.CoreWebContent.WebCode_SO0_75, FormatHelper.FormatDays(GetSettings("SWNetPerfMon-Settings-Retain Container Daily", "365")), result.Length > 0 ? warnMessage : ""));

        result = dt.Select("table_name = 'Events'");
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_249, FormatHelper.FormatDays(GetSettings("SWNetPerfMon-Settings-Retain Events", "30")), result.Length > 0 ? warnMessage : ""));

        if (_areSyslogAllowed)
        {
            result = dt.Select("table_name LIKE 'SysLog%'");
            data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_250, FormatHelper.FormatDays(GetSettings("SysLog-MaxMessageAge", "7")), result.Length > 0 ? warnMessage : ""));
        }

        if (_areTrapsAllowed)
        {
            result = dt.Select("table_name LIKE 'Trap%'");
            data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_251, FormatHelper.FormatDays(GetSettings("Trap-MaxMessageAge", "7")), result.Length > 0 ? warnMessage : ""));
        }

        data.Add(new DataProps("&nbsp;     ", "&nbsp;", ""));

        DetailBlock.DataSource = data;
    }

    //for support - trying to get the message about row numbers > 10000000 like Adiagn has
    internal static DataTable CheckForHugeTables()
    {
        DataTable table;

        string sqlString = @"SELECT 
		TAB.table_name, IDX.rows as cnt
	FROM sysindexes AS IDX
	INNER JOIN information_schema.tables AS TAB
	ON
		IDX.id = object_id(TAB.table_name)
	WHERE 	
		IDX.indid < 2 and IDX.rows >= {0}
		and (TAB.table_name LIKE 'VolumePerformance%' OR
		  TAB.table_name LIKE 'MemoryMultiLoad%' OR
		   TAB.table_name LIKE 'InterfaceTraffic%' OR
		    TAB.table_name LIKE 'InterfaceErrors%' OR
			TAB.table_name LIKE 'TrapVarbinds%' OR
		    TAB.table_name LIKE 'Events%' OR
			TAB.table_name LIKE 'InterfaceAvailability%' OR
		   TAB.table_name LIKE 'CustomPollerStatistics_%' OR
		    TAB.table_name LIKE 'ContainerStatus%' OR
			 TAB.table_name LIKE 'CPUMultiLoad%'";

        if (_areSyslogAllowed)
            sqlString += @" OR
                TAB.table_name LIKE 'SysLog%'";
        if (_areSyslogAllowed)
            sqlString += @" OR
                TAB.table_name LIKE 'Traps%'";

        sqlString += @")
	ORDER BY 
		cnt, TAB.table_name";

        string sql = string.Format(sqlString, CommonWebHelper.WarnRowsNumber);

        using (SqlCommand cmd = SqlHelper.GetTextCommand(sql))
        {
            table = SqlHelper.ExecuteDataTable(cmd);
        }

        return table;
    }
}
