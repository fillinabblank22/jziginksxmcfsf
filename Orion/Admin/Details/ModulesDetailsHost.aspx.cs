﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Web;
using System.Web.UI;

using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Details_ModulesDetailsHost : System.Web.UI.Page
{
    private static Log _log;
    protected static Log Logger { get { return _log ?? (_log = new Log()); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        LoadPlugins();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void LoadPlugins()
    {
        var pluginControls = new Dictionary<int, Control>();

        // load plugin controls from config
        string root = HttpContext.Current.Server.MapPath("/Orion");
        XmlDocument xmlDoc = new XmlDocument();
        
        foreach (var module in OrionModuleManager.GetModules())
        {
            try
            {
                xmlDoc = XmlHelper.ToXmlDocument(module.GetModuleConfiguration());

                var xmlNodes = xmlDoc.DocumentElement.SelectNodes("moduleDetailPlugins/moduleDetailPlugin");
                if (xmlNodes.Count == 0)
					continue;

                foreach (XmlNode xmlNode in xmlNodes)
                {
					int order = int.Parse(xmlNode.Attributes["order"].Value);
					while (pluginControls.ContainsKey(order))
						order++;

                    pluginControls.Add(
						order, 
                        this.LoadControl(xmlNode.Attributes["control"].Value)
                    ); 
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error while loading module details plugin for module {0}. Details: {1}", module.Name, ex.ToString()); 
            }
        }
        // arrange plugins by "order"
        var orderedPluginControls = pluginControls.OrderBy(p => p.Key);
        
        // Add loaded plugin controls on the page
        pluginDiv.Controls.Clear();
        foreach (var control in orderedPluginControls)
        {
            pluginDiv.Controls.Add(control.Value);

            if (!MainServerTitile.Visible)
            { // set title visible if core control was loaded
                if (null != ControlHelper.FindControlRecursive(pluginDiv, "OrionCoreDetails"))
                    MainServerTitile.Visible = true;
            }
        }
    }
}