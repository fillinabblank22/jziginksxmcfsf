<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrionCoreDetails.aspx.cs" Inherits="Orion_Admin_Details_OrionCoreDetails" 
          MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBCODE_TM0_50 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src ="~/Orion/Admin/Details/DetailsBlock.ascx" TagPrefix="orion" TagName ="DetailsBlock" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionCorePHAdminDetailsCore" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <!-- Page title and hot links -->
    <table style="width: 100%;">
        <tr>
            <td><h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1></td>
        </tr>
    </table>

    <div class="GroupBox">
        <orion:DetailsBlock ID="ServerDetails" runat="server" />
        
        <%if (!_hideOrionDetailsSection.Contains("NetworkElementDetails"))
           { %>
            <orion:DetailsBlock ID="NetObjects" runat="server" />
        <% }%>

        <orion:DetailsBlock ID="ComponentVersions" runat="server" />
    </div>
</asp:Content>
