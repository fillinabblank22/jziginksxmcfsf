<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Engines.aspx.cs" Inherits="Orion_Admin_Details_Engines" 
    MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_14 %>" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <orion:Include runat="server" File="jquery/jquery.timer.js" />
    
    <style type="text/css">
	    .LeftColumn { width: auto; white-space:nowrap !important; }
	    .RightColumn { text-align: left; }
	    .AltRow1 { background-color: #f8f8f8; }
	    .AltRow2 { background-color: white; }   
        .sw-pg-firstcolumn { width: 27em; padding-left: 5px; padding-right: 10px; }
        #enginesTable td.serverName { padding-top: 15px; padding-bottom: 10px; font-weight:bold;}
    </style>
    
    <script type="text/javascript">
  
        var restripTable = function () {
            $('#enginesTable tr:even td').addClass('AltRow1').removeClass('AltRow2');
            $('#enginesTable tr:odd td').addClass('AltRow2').removeClass('AltRow1');
        };
        
        var getRows = function (data) {
            var $rows = $('tr', data);
            
            $rows.find('td:eq(0)').addClass("PropertyHeader sw-pg-firstcolumn");
            $rows.find('td:eq(1)').addClass("Property Col2");
            
            return $rows;
        };

    </script>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
        <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHAdminDetailsPollingEngines" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <orion:Include ID="Include2" runat="server" Framework="Ext" FrameworkVersion="2.2" />
    <orion:Include ID="Include3" runat="server" File="OrionCore.js" />

    <!-- Page title and hot links -->
    <table style="width: 100%;">
        <tr>
            <td><h1 style="font-size:large; padding-bottom:10px;"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1></td>
            <td style="text-align: right;">
                <orion:LocalizableButtonLink runat="server" DisplayType="Primary" NavigateUrl="/sapi/installer/get" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, Engines_DownloadInstallerButtonText %>" />
            </td>
        </tr>
    </table>

    <div class="GroupBox">   
        <table id="enginesTable" style="border-collapse: collapse; width: 100%;" >
            <tr>
                <td class="PropertyHeader sw-pg-firstcolumn AltRow1" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_141) %></td>
                <td class="Property Col2 AltRow1"><asp:Label ID="LastDatabaseUpdate" runat="server"></asp:Label></td>
            </tr>
          
            <asp:Repeater ID="PollingEngines" runat="server" OnItemCommand="Repeater1_OnItemCommand">
                <ItemTemplate>                   
                    <tr>
                        <td colspan="2" class="serverName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_169) %> <%# DefaultSanitizer.SanitizeHtml(Eval("ServerName")) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_356) %></td>
                        <td class="Property Col2 AltRow1">
                            <%# DefaultSanitizer.SanitizeHtml(CalcStatus(Container.DataItem)) %>
                            <%# DefaultSanitizer.SanitizeHtml(GetFIPSCompliantMarkup(Container.DataItem)) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow2" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_357) %></td>
                        <td class="Property Col2 AltRow2"><%# DefaultSanitizer.SanitizeHtml(Eval("ServerType")) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow1" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_358) %></td>
                        <td class="Property Col2 AltRow1"><%# DefaultSanitizer.SanitizeHtml(Eval("EngineVersion")) %></td>
                    </tr>
                    <tr style="display:<%# DefaultSanitizer.SanitizeHtml((Eval("PrimaryLocale").ToString()!="" || Eval("OSLocale").ToString()!="")?"table-row":"none") %>;">
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow2" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ET_1) %></td>
                        <td class="Property Col2 AltRow2"><%# DefaultSanitizer.SanitizeHtml(GetFullLanguage(Container.DataItem, "PrimaryLocale")) %></td>
                    </tr>
                    <tr style="display:<%# DefaultSanitizer.SanitizeHtml((Eval("PrimaryLocale").ToString()!="" || Eval("OSLocale").ToString()!="")?"table-row":"none") %>;">
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow1" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ET_2) %></td>
                        <td class="Property Col2 AltRow1"><%# DefaultSanitizer.SanitizeHtml(GetFullLanguage(Container.DataItem, "OSLocale")) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow2" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_13) %></td>
                        <td class="Property Col2 AltRow2"><%# DefaultSanitizer.SanitizeHtml(Eval("IP")) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow1" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_170) %></td>
                        <td class="Property Col2 AltRow1"><%# DefaultSanitizer.SanitizeHtml(LastSync(Container.DataItem)) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow2" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_175) %></td>
                        <td class="Property Col2 AltRow2"><%# DefaultSanitizer.SanitizeHtml(Eval("PollingCompletion")) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow1" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_359) %></td>
                        <td class="Property Col2 AltRow1">
                        <span style="display:<%# DefaultSanitizer.SanitizeHtml(HideElement(Eval("Elements").ToString()=="0" && !Eval("ServerType").ToString().Equals(MainPollerName, StringComparison.OrdinalIgnoreCase) && Eval("IsInPool").ToString()!="True")) %>;">
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Elements")) %>
                        </span>
                        <span class="sw-suggestion" style="display:<%# DefaultSanitizer.SanitizeHtml(HideElement(Eval("Elements").ToString()!="0" || Eval("IsInPool").ToString()=="True")) %>;"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_545) %> &nbsp;
                                <orion:LocalizableButton id="bt_RemovePoller" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_2 %>" CommandName="RemovePoller" CommandArgument='<%# DefaultSanitizer.SanitizeHtml(Eval("EngineID")) %>' OnClientClick='<%# DefaultSanitizer.SanitizeHtml(string.Format("return confirm(\"{0}{1}?\");", ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_SO0_3), Eval("ServerName"))) %>'/>
                        </span>
                        </td>
                     </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_267) %></td>
                        <td class="Property Col2 AltRow2"><%# DefaultSanitizer.SanitizeHtml(Eval("Nodes")) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_361) %></td>
                        <td class="Property Col2 AltRow1"><%# DefaultSanitizer.SanitizeHtml(Eval("Volumes")) %></td>
                    </tr>
                    <% // Interface table row should stay last to keep zeebra row colorizing correct. Otherwise the page have to be refactored.
                    if (this.AreInterfacesAllowed()) { %>
                    <tr>
                        <td class="PropertyHeader sw-pg-firstcolumn AltRow2" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_360) %></td>
                        <td class="Property Col2 AltRow2"><%# DefaultSanitizer.SanitizeHtml(Eval("Interfaces")) %></td>
                    </tr>
                    <% } %>
                    <tbody id="engineStatus<%# DefaultSanitizer.SanitizeHtml(Eval("EngineID")) %>"></tbody>
                    
                    <script type="text/javascript">
                    //<![CDATA[
                        $(function() {
                            var interval = 10000;
                            var engineId = <%# DefaultSanitizer.SanitizeHtml(Eval("EngineID")) %>;
                            var container = "#engineStatus" + engineId;
                            var loadStatus = function(timer) {
                                timer.stop();
                                
                                $.get('/Orion/Admin/EngineStatusBlock.aspx?EngineID=' + engineId, function(data) {
                                    var $rows = getRows(data);
                                    $(container).html($rows);
                                    restripTable();                                        
                                    timer.reset(interval);
                                });
                            };
                            $.timer(1, loadStatus);
                        });
                    //]]>
                    </script>

                </ItemTemplate>
            </asp:Repeater> 
        
        </table>
        <br />
    </div>
</asp:Content>