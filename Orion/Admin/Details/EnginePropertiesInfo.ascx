<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EnginePropertiesInfo.ascx.cs" Inherits="Orion_Admin_Details_EnginePropertiesInfo" %>

<asp:Repeater runat="server" ID="EnginePropertiesRepeater">
    <ItemTemplate>
        <tr>
            <td><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "PropertyName")) %></td>
            <td><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "PropertyValue")) %></td>
        </tr>
    </ItemTemplate>
</asp:Repeater>

<tr runat="server" visible="false" id="ErrorBlock">
    <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_377) %></td>
    <td class="Property">
        <asp:Label runat="server" ID="ErrorLabel"></asp:Label>
    </td>
</tr>