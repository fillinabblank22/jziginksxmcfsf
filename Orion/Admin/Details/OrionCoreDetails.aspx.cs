﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using RegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;

public partial class Orion_Admin_Details_OrionCoreDetails : System.Web.UI.Page
{
    public static List<String> _hideOrionDetailsSection = new List<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.OrionDetailsHideSection)).Select(c => c.Key));

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var data = new List<DataProps>();

        // Server related
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_230, IPGlobalProperties.GetIPGlobalProperties().HostName.ToUpper(), ""));
		data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_131, GetLocalizedReference(OSVersionHelper.GetOSName(), OSVersionHelper.UnknownOSNameReference),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_VB0_231, System.Environment.OSVersion.Version.ToString(),""));
        string sp = System.Environment.OSVersion.ServicePack;
        data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_176, String.IsNullOrEmpty(sp) ? Resources.CoreWebContent.WEBCODE_SO0_9 : sp,""));

        ServerDetails.DataSource = data;
        ServerDetails.Name = Resources.CoreWebContent.WEBCODE_AK0_140;

        // Net Objects
        data = new List<DataProps>();

        int nodesCount = SolarWinds.Orion.NPM.Web.Node.Count;
        int interfacesCount = SolarWinds.Orion.NPM.Web.Interface.Count;
        int volumesCount = SolarWinds.Orion.NPM.Web.Volume.Count;

        data.Add(new DataProps(Resources.CoreWebContent.WEBCODE_AK0_138, (nodesCount + interfacesCount + volumesCount).ToString(),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_AK0_162, nodesCount.ToString(),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_36, interfacesCount.ToString(),""));
        data.Add(new DataProps(Resources.CoreWebContent.WEBDATA_VB0_37, volumesCount.ToString(),""));

        NetObjects.DataSource = data;
        NetObjects.Name = Resources.CoreWebContent.WEBDATA_VB0_171;

        // Version info
        SortedDictionary<string, string> items = AssemblyInfoHelper.GetAssemblyInfo(RegistrySettings.GetInstallLocation());        
        items.Keys.ToList().ForEach(k=> items[k] = GetLocalizedReference(items[k].ToString(), AssemblyInfoHelper.UnknownAssemblyReference));

        data = items.Select(pair => new DataProps(pair.Key, pair.Value, "")).ToList();

        ComponentVersions.DataSource = data;
        ComponentVersions.Name = Resources.CoreWebContent.WEBCODE_AK0_139;
    }

    protected string GetLocalizedReference(string reference, string template)
    {
        return (reference == template) ? Resources.CoreWebContent.WEBCODE_AK0_6 : reference;
    }
}