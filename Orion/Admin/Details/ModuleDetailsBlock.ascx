<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModuleDetailsBlock.ascx.cs" Inherits="Orion_Admin_License_ModuleDetailsBlock" %>

<div class="ModuleDetailsBlock">
    <asp:Repeater ID="ModuleInfo" runat="server">
        <HeaderTemplate>
            <div class="ModuleDetailsBlockHeader"><%= DefaultSanitizer.SanitizeHtml(Name) %></div>
            <table>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="alternateRow">
                <td class="Col1" style="width: 280px;"><%# DefaultSanitizer.SanitizeHtml(Eval("Key")) %></td>
                <td class="Col2"><%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr>
                <td class="Col1" style="width: 280px;"><%# DefaultSanitizer.SanitizeHtml(Eval("Key")) %></td>
                <td class="Col2"><%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %></td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>