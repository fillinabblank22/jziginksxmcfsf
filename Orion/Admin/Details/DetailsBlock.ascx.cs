﻿using System.Collections.Generic;

public partial class Orion_Admin_Details_DetailsBlock : System.Web.UI.UserControl
{
    public string Name { get; set; }

    public IList<DataProps> DataSource
    {
        get { return ModuleInfo.DataSource as List<DataProps>; }
        set
        {
            if (ModuleInfo.DataSource == null || !ModuleInfo.DataSource.Equals(value))
            {
                ModuleInfo.DataSource = value;
                ModuleInfo.DataBind();
            }
        }
    }
}