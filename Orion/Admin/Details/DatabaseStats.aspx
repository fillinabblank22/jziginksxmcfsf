<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="DatabaseStats.aspx.cs" Inherits="Orion_Admin_Details_DatabaseStats" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_526 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHAdminDetailsDatabase" />
    <script>
        $(document).ready(function () {
            $("#tableCounts").tablesorter({ sortList: [[1, 1]] });
        });
    </script>
    <style>

thead tr .header {
    background-position: right center;
    background-repeat: no-repeat;
    cursor: pointer;
    padding: 5px;
    background-color: #204a63;
    color: white;
    background-image: url("/Orion/images/Arrows/bg.gif");
}

tbody tr td { padding-right: 5px;padding-left: 5px!important;}

thead tr .headerSortUp {
    background-image: url("/Orion/images/Arrows/asc.gif");
}
thead tr .headerSortDown {
    background-image: url("/Orion/images/Arrows/desc.gif");
}

    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <!-- Page title and hot links -->
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

    <div class="GroupBox" id="DatabaseStats">
                <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_237) %></h2>
                    <asp:Repeater ID="Repeater1" runat="server">
                        <HeaderTemplate>
                            <table id="tableCounts" border="1px" cellpadding="5" style="text-align: left; border-style: solid; border-collapse: collapse; min-width: 50%">
                                <thead> 
                                <tr >
                                    <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_238) %> </th>
                                    <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_239) %> </th>
                                    <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_240) %> </th>
                                    <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_241) %> </th>
                                    <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_242) %> </th>
                                    </tr>
                                    </thead>
                                <tbody>  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("tablename")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("row_count")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("reserved")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("data")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("index_size")) %></td>
                                
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("tablename")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("row_count")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("reserved")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("data")) %></td>
                                <td><%# DefaultSanitizer.SanitizeHtml(Eval("index_size")) %></td>
                                
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </tbody> 
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
    </div>

</asp:Content>
