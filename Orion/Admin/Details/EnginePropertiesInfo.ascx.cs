﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Admin.Engines;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Admin_Details_EnginePropertiesInfo : EngineInfoBaseControl
{
    private readonly static Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            LoadEngineProperties();
        }
        catch (Exception ex)
        {
            _log.Error(string.Format("Error fetching details about EngineID {0}.", EngineID), ex);
            ShowErrorBlock(ex);
        }
    }

    private void LoadEngineProperties()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            string query = @"SELECT PropertyName, PropertyType, PropertyValue 
                                     FROM Orion.EngineProperties
                                     WHERE PropertyType IN ('Scale Factor','Total Weight') AND EngineID=@EngineID
                                     ORDER BY CASE WHEN PropertyType = 'Scale Factor' THEN 10 
                                                   WHEN PropertyType = 'Total Weight' THEN 20 
	                                           ELSE 50 END";
            // To add new properties just modify IN statement and ORDER BY clause

            IDictionary<string, IEnginePropertyInterpreter> interpreters = BuildInterpreterMap();

            DataTable engineProperties = swis.Query(query, new Dictionary<string, object> { { "EngineID", EngineID } });
            this.EnginePropertiesRepeater.DataSource = engineProperties;

            foreach (DataRow row in engineProperties.Rows)
            {
                //Process row according to logic specified in FB85422 => delegated to the IEnginePropertyInterperter mapping
                interpreters.GetParticularInterpreterOrDefault(row["PropertyName"].ToString()).InterpretProperty(row);
            }

            this.EnginePropertiesRepeater.DataBind();
        }
    }


    private void ShowErrorBlock(Exception ex)
    {
        EnginePropertiesRepeater.Visible = false;
        ErrorBlock.Visible = true;
        ErrorLabel.Text = (ex is LocalizedExceptionBase) ? (ex as LocalizedExceptionBase).LocalizedMessage : ex.Message;
    }

    /// <summary>
    /// Creates new map of module's engine property interpreters.
    /// </summary>
    private IDictionary<string, IEnginePropertyInterpreter> BuildInterpreterMap()
    {
        return OrionModuleManager.GetEnginePropertyInterpreters();
    }

 
}

internal static class InterpreterMapExtensions
{
    /// <summary>
    /// Gets particular <see cref="IEnginePropertyInterpreter"/> for given property or default if not found.
    /// </summary>
    /// <param name="map">Interpreter's map to be searched within.</param>
    /// <param name="propertyName">Name of property to be interpreter get for.</param>
    public static IEnginePropertyInterpreter GetParticularInterpreterOrDefault(this IDictionary<string, IEnginePropertyInterpreter> map, string propertyName)
    {
        IEnginePropertyInterpreter interpreter;
        string key = propertyName.ToLower();

        if (!map.TryGetValue(key, out interpreter))
        {
            interpreter = new DefaultEnginePropertyInterpreter();
        }

        return interpreter;
    }
}
