<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModulesDetailsHost.aspx.cs" Inherits="Orion_Admin_Details_ModulesDetailsHost" 
     MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_13 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="AdminHeaderContent" ContentPlaceHolderID="adminHeadPlaceholder"  runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <span class="sw-license-manager-link"><a href="/ui/licensing/license-manager"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_525) %></a></span>
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHAdminDetailsLicense" />
</asp:Content>

<asp:Content ID="AdminMainContent" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<!-- Page title and hot links -->
<table style="width: 100%;">
    <tr>
        <td><h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1></td>
    </tr>
</table>

<!-- Plugin box -->
<div class="GroupBox" id="PluginHost">
    <h2 class="PollerTypeTitle" ><asp:Literal ID="MainServerTitile" runat="server" Visible="false" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_397 %>"/></h2>
    <div id="pluginDiv" runat="server">
    </div>
    <br />
</div>
</asp:Content>
