using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.i18n;

public partial class Orion_Admin_Details_ElementsStat : System.Web.UI.Page
{
    private static Log _log = new Log();
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        OrionInclude.CoreFile("jquery.js", OrionInclude.Section.Bottom);
        OrionInclude.CoreFile("jquery/jquery.tablesorter.min.js", OrionInclude.Section.Bottom);
    }

    private DataTable _repResult;
    private DataTable _entitiesDeps;
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            _repResult = GetEnginesTable();
            _entitiesDeps = GetAllManagedEntitiesDeps();

            Dictionary<string, string> listOfEntities = BuildDepsTree(_entitiesDeps);

            using (var proxy = InformationServiceProxy.CreateV3())
            {
                foreach (KeyValuePair<string, string> pair in listOfEntities)
                {
                    try
                    {
                        string query = GenerateQuery(pair.Key, pair.Value);
                        using (var data = proxy.Query(query))
                        {
                            MergeTable(_repResult, data, pair.Key);
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Error getting entity count " + pair.Key, ex);
                    }
                }
            }

            this.Repeater1.DataSource = _repResult;
            this.Repeater1.DataBind();
        }
    }

    private DataTable GetEnginesTable()
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string query = string.Format(@"SELECT EngineID, DisplayName as [{0}], IP as [{1}], ServerType as [{2}]
FROM Orion.Engines",
                Resources.CoreWebContent.WEBDATA_SO0_249, Resources.CoreWebContent.WEBDATA_AF0_13, Resources.CoreWebContent.WEBDATA_SO0_250);
            using (var data = proxy.Query(query))
            {
                return data;
            }
        }
    }
    
    private void MergeTable(DataTable repResult, DataTable data, string columnToAdd)
    {
        string collTitle = GetColumnCaption(columnToAdd);
        DataColumn col = new DataColumn(collTitle, typeof(Int32));
        
        repResult.Columns.Add(col);
        foreach (DataRow row in repResult.Rows)
        {
            string EngineID = row["EngineID"].ToString();
            DataRow[] rows = data.Select("EngineID = " + EngineID);
            if (rows.Length == 0)
                row[collTitle] = 0;
            else
                row[collTitle] = rows[0]["Cnt"];
        }
    }

    private string GetColumnCaption(string columnToAdd)
    {
        if (columnToAdd.Equals("Orion.Nodes"))
            return Resources.CoreWebContent.WEBDATA_AV0_18; //"Nodes";
        if (columnToAdd.Equals("Orion.NetPath.Probes"))
            return Resources.CoreWebContent.WEBDATA_SO0_251; //"NetPath Probes";

        DataRow[] rows = _entitiesDeps.Select(string.Format("TargetType = '{0}'", columnToAdd));
        if (rows == null || rows.Length == 0)
            return columnToAdd;
        else
            return rows[0]["Name"].ToString();
    }

    /*
    Select t.Node.EngineID, Count(1) as Cnt
From Orion.Volumes t
Group by t.Node.EngineID
Order by t.Node.EngineID
    */
    private string GenerateQuery(string key, string value)
    {
        return string.Format(@"Select t{1}.EngineID, Count(1) as Cnt
From {0} t
Group by t{1}.EngineID
Order by t{1}.EngineID", key, value);
    }

    private Dictionary<string, string> BuildDepsTree(DataTable dt)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        result["Orion.Nodes"] = "";
        
        foreach (DataRow row in dt.Rows)
        {
            result[row["TargetType"].ToString()] = BuildElementsPathRecurcive("." + row["TargetPropertyName"].ToString(), row["SourceType"].ToString());
        }

        result["Orion.NetPath.Probes"] = "";

        return result;
    }

    private string BuildElementsPathRecurcive(string targetName, string sourceType)
    {
      DataRow[] row =  _entitiesDeps.Select(string.Format("TargetType = '{0}'", sourceType));
        if (row == null || row.Length == 0)
            return targetName;

        if (row[0]["SourceType"].ToString().Equals("Orion.Nodes"))
            return targetName + "." + row[0]["TargetPropertyName"];

        return BuildElementsPathRecurcive(targetName + "." + row[0]["TargetPropertyName"], row[0]["SourceType"].ToString());
    }

    private DataTable GetAllManagedEntitiesDeps()
    {
        //Getting all hosting relationship 
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string query = @"SELECT r.SourceType, r.TargetType, r.SourcePropertyName, r.TargetPropertyName, e.DisplayName AS Name
FROM Metadata.Relationship r
join Metadata.Entity e on e.FullName = r.TargetType
Where r.BaseType = 'System.Hosting' AND r.SourceType ISA 'System.ManagedEntity' AND r.TargetType ISA 'System.ManagedEntity'";
            using (var data = proxy.Query(query))
            {
                return data;
            }
        }
    }

    protected void Repeater1_ItemBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater headerRepeater = e.Item.FindControl("headerRepeater") as Repeater;
            headerRepeater.DataSource = _repResult.Columns;
            headerRepeater.DataBind();
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater columnRepeater = e.Item.FindControl("columnRepeater") as Repeater;
            columnRepeater.DataSource = ((DataRowView)e.Item.DataItem).Row.ItemArray;
            columnRepeater.DataBind();
        }
    }
}
