﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.EntityManager;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_Details_CoreModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }
    private static HashSet<String> _hiddenLicenseDetailsSection = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.LicenseDetailsHideSection)).Select(c => c.Key));

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo("Orion");
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error while displaying details for Orion Core module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            OrionCoreDetails.Name = module.ProductDisplayName;

			if (!String.IsNullOrEmpty(module.LicenseInfo))
				values.Add(Resources.CoreWebContent.WEBCODE_AK0_132, module.LicenseInfo);
            values.Add(Resources.CoreWebContent.WEBCODE_AK0_131, module.ProductName);
            values.Add(Resources.CoreWebContent.WEBDATA_AK0_84, module.Version);
            values.Add(Resources.CoreWebContent.WEBDATA_VB0_176, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.CoreWebContent.WEBCODE_SO0_9 : module.HotfixVersion);

            if (!_hiddenLicenseDetailsSection.Contains("NodeAndVolumeDetails"))
            {
                // Add nodes/volumes limits/currnt
                try
                {
                    using (WebDAL dal = new WebDAL())
                    {
                        var featureManager = new FeatureManager();
                        int maxNodes = featureManager.GetMaxElementCount(WellKnownElementTypes.Nodes);
                        int maxVolumes = featureManager.GetMaxElementCount(WellKnownElementTypes.Volumes);

                        int nodeCount = dal.GetNodeCount();
                        int volumeCount = dal.GetVolumeCount();
                        const int unlimitedThreshold = 1000000;

                        values.Add(Resources.CoreWebContent.WEBCODE_AK0_135, nodeCount.ToString());
                        values.Add(Resources.CoreWebContent.WEBCODE_AK0_134,
                            maxNodes > unlimitedThreshold ? Resources.CoreWebContent.WEBCODE_AK0_133 : maxNodes.ToString());
                        values.Add(Resources.CoreWebContent.WEBCODE_AK0_137, volumeCount.ToString());
                        values.Add(Resources.CoreWebContent.WEBCODE_AK0_136,
                            maxVolumes > unlimitedThreshold ? Resources.CoreWebContent.WEBCODE_AK0_133 : maxVolumes.ToString());

                        // in case HA is installed then show HA licensing info
                        if (EntityManager.InstanceWithCache.IsThereEntity("Orion.HA.Pools"))
                        {
                            using (var factory = new SwisConnectionProxyFactory())
                            using (var swis = factory.CreateSwisConnection())
                            {
                                var availableLicensesBag = swis.Invoke<PropertyBag>("Orion.Licensing.Licenses", "GetAvailableAssignments", 1, "HA");

                                int availableLicenses = Convert.ToInt32(availableLicensesBag["Available"]);
                                int maxLicenses = Convert.ToInt32(availableLicensesBag["Maximum"]);
                                string maxLicensesStr = maxLicenses.ToString();
                                string usedLicenses = (maxLicenses - availableLicenses).ToString();

                                // for eval report number of existing pools
                                if (maxLicenses > unlimitedThreshold)
                                {
                                    int numberOfPools = swis.Query("SELECT PoolId FROM Orion.HA.Pools").Rows.Count;

                                    usedLicenses = numberOfPools.ToString();
                                    maxLicensesStr = Resources.CoreWebContent.WEBCODE_AK0_133;
                                }

                                values.Add(Resources.CoreWebContent.LicenseDetails_TotalHAPoolsInUse, usedLicenses);
                                values.Add(Resources.CoreWebContent.LicenseDetails_TotalHAPoolsInLicense, maxLicensesStr);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Cannot get number of licensed elements.", ex);
                }
            }

            OrionCoreDetails.DataSource = values;
            break; // we support only one "Primary" engine
        }
    }
}