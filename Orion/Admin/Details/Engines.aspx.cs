﻿using System;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Admin_Details_Engines : System.Web.UI.Page
{
    private TimeSpan syncDelteSec;
    private int keepAliveTimeout;
    
    const float MinPollingCompletion = 5;
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected override void OnLoad(EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string timeoutStr = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("SWNetPerfMon-Settings-Keep Alive Timeout").SettingValue.ToString();
        if (String.IsNullOrEmpty(timeoutStr) || !int.TryParse(timeoutStr, out keepAliveTimeout))
        {
            keepAliveTimeout = 60; // default keep-alive timeout = 1 min
        }
                
        DataTable engines = GetEngines();
        syncDelteSec = EnginesDAL.GetNodeSyncDelta();

        PollingEngines.DataSource = engines;
        PollingEngines.DataBind();

        LastDatabaseUpdate.Text = LastDBUpdate();
    }

    protected string MainPollerName
    {
        get { return "Primary"; }
    }

    protected T SafeConvert<T>(object value, T defaultValue)
    {
        if (value is DBNull) return defaultValue;

        try
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }
        catch
        {
            return defaultValue;
        }
    }

	protected static Log Log = new Log();
	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		Log.Error(ex);
	}

	protected void Repeater1_OnItemCommand(object sender, RepeaterCommandEventArgs e)
	{
		if (e.CommandName == "RemovePoller" && e.CommandArgument != null)
		{
			int engineID = int.Parse(e.CommandArgument.ToString());
			if (engineID != 0)
			{
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				{
                    proxy.DeleteOrionServerByEngineId(engineID);
                    proxy.DeleteEngine(engineID);
				}
			}
		}
	}

    protected string LastSync(object dataItem)
    {
        DateTime keepAlive = SafeConvert<DateTime>(DataBinder.Eval(dataItem, "KeepAlive"), default(DateTime));

        if (keepAlive == default(DateTime) || keepAlive <= new DateTime(2000, 1, 1))
			return Resources.CoreWebContent.WEBCODE_VB0_242;

        DateTime currentTimeDB = SafeConvert<DateTime>(DataBinder.Eval(dataItem, "CurrDate"), default(DateTime));
        return TimeToString((long)(currentTimeDB - keepAlive).TotalSeconds, 300);
    }

	private string TimeToString(long totalSeconds, int threshold)
	{
		if (totalSeconds <= 0)
			return Resources.CoreWebContent.WEBCODE_VB0_114;

		if (totalSeconds == 1)
			return Resources.CoreWebContent.WEBCODE_VB0_261;

		if (totalSeconds < 60)
			return string.Format(Resources.CoreWebContent.WEBCODE_VB0_262, (totalSeconds).ToString());

		string time = string.Format(Resources.CoreWebContent.WEBCODE_VB0_263, ((int)totalSeconds / 60).ToString(), (totalSeconds % 60).ToString());


		if (totalSeconds < threshold)
			return time;

		return string.Format("<font color=\"Red\"><b>{0}</b></font>", time);
	}

	protected string CalcStatus(object dataItem)
	{
		string engineVersion = SafeConvert<string>(DataBinder.Eval(dataItem, "EngineVersion"), null);

		if (engineVersion == null)
			return "";

		int horizon = 120;

		if (engineVersion.Length > 0)
			horizon = keepAliveTimeout;

		DateTime keepAlive = SafeConvert<DateTime>(DataBinder.Eval(dataItem, "KeepAlive"), new DateTime());
        DateTime dbCurr = SafeConvert<DateTime>(DataBinder.Eval(dataItem, "CurrDate"), new DateTime()); 
		//DateTime LastFail = SafeConvert<DateTime>(DataBinder.Eval(dataItem, "FailOverActive"), new DateTime());

        double keepAliveSeconds = (long)(new TimeSpan((dbCurr - keepAlive).Ticks).TotalSeconds);
		//double LastFailSeconds = (((TimeSpan)(Now - LastFail.ToLocalTime())).TotalSeconds);

		//string serverType = SafeConvert<string>(DataBinder.Eval(dataItem, "ServerType"), "");

		string icon, text;

		// Primary Server      
		if (keepAliveSeconds < horizon)
		{
			icon = "Small-Up.gif";
			text = Resources.CoreWebContent.WEBCODE_VB0_264;
		}
		else if (keepAliveSeconds > horizon * 1.5)
		{
			icon = "Small-Down.gif";
			text = Resources.CoreWebContent.WEBCODE_VB0_265;
		}
		else
		{
			icon = "Small-Warning.gif";
			text = Resources.CoreWebContent.WEBCODE_VB0_266;
		}

		return string.Format("<img style=\"vertical-align=middle;\" src=\"/NetPerfMon/images/{0}\" alt=\"{2}\"/>&nbsp;&nbsp;{1}", icon, text, Resources.CoreWebContent.WEBDATA_VB0_14);
	}

	protected string LastDBUpdate()
    {
        //lastnodesync.ToLocalTime();
        return TimeToString((long)(syncDelteSec.TotalSeconds), 360);
    }

	protected string HideElement(bool hide) 
	{
		return hide ? "none" : "inline-block";
	}

    protected bool AreInterfacesAllowed()
    {
        return PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");
    }

	private string ResourceLookup(string prefix, string value)
	{
		const string managerid = "Core.Strings";
		var manager = ResourceManagerRegistrar.Instance;
		var key = manager.CleanResxKey(prefix, value.Trim());
		var result = manager.SearchAll(key, new[] { managerid });
		return String.IsNullOrEmpty(result) ? value : result;
	}

    private DataTable GetEngines()
    {
        DataTable engines = EnginesDAL.GetEnginesTable(includeFreeEngines: true);
        engines.Columns.Add("IsInPool", typeof(System.Boolean));

        DataTable realNumberOfElements = EnginesDAL.GetEnginesRealNumberOfElements();
        var enginesInPool = EnginesDAL.GetEngineIdsInAnyPool();

        foreach (DataRow row in engines.Rows)
        {
            var pollingCompletion = row["PollingCompletion"] as float?;

            if (pollingCompletion != null && pollingCompletion < MinPollingCompletion)
            {
                row["PollingCompletion"] = MinPollingCompletion;
            }

			var serverType = row["ServerType"] as string;
			row["ServerType"] = ResourceLookup("ServerType", serverType);
            //create flag whether the engine is used in any HA pool
            int engineId = (int) row["EngineID"];
            row["IsInPool"] = enginesInPool.Any(id => id == engineId);

            // replace Elements count info from Engines table info with real values
            DataRow[] realNums = realNumberOfElements.Select("EngineID = " + row["EngineID"].ToString());

            if (realNums.Length == 0)
                continue;

            foreach (DataColumn column in realNums[0].Table.Columns)
            {
                if (!engines.Columns.Contains(column.ColumnName))
                    continue;

                row[column.ColumnName] = realNums[0][column.ColumnName];
            }
        }
        return engines;
    }


    protected string GetFullLanguage(object dataItem, string columnName)
    {
        string shortName = string.Empty;

        try
        {
            shortName = SafeConvert<string>(DataBinder.Eval(dataItem, columnName), string.Empty);

            return LocaleConfiguration.LocalizedDisplayName(System.Globalization.CultureInfo.GetCultureInfo(shortName));
        }
        catch (Exception)
        {}

        return shortName;
    }

    protected string GetFIPSCompliantMarkup(object dataItem)
    {
        bool fipsEnabled = (bool) DataBinder.Eval(dataItem, "FIPSModeEnabled");
        if (fipsEnabled)
        {
            string result = string.Format("<br/><img src='/Orion/images/FIPS_icon_16x16.png' alt='{0}'/>&nbsp;&nbsp;{0}", Resources.CoreWebContent.WEBDATA_PF0_2);
            return result;
        }
        return string.Empty;
    }
}
