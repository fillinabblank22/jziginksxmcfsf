<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DetailsBlock.ascx.cs" Inherits="Orion_Admin_Details_DetailsBlock" %>


<style type="text/css">
    .sw-pg-firstcolumn { width: 20em; }
</style>

<div class="DetailsBlock">
    <asp:Repeater ID="ModuleInfo" runat="server">
        <HeaderTemplate>
            <div class="DetailsBlockHeader"><%= DefaultSanitizer.SanitizeHtml(Name) %></div>
            <table>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="alternateRow">
                <td class="PropertyHeader sw-pg-firstcolumn" style="white-space:nowrap;"><%# DefaultSanitizer.SanitizeHtml(Eval("Key")) %></td>
                <td class="Col2"><%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %>
                    <%# DefaultSanitizer.SanitizeHtml((string.IsNullOrEmpty(Eval("WarnMessage").ToString())) ? "" : string.Format(" &nbsp; <div class='sw-suggestion sw-suggestion-warn'><span class='sw-suggestion-icon'></span>{0}</div>", Eval("WarnMessage"))) %>
                    </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr>
                <td class="PropertyHeader sw-pg-firstcolumn" style="white-space:nowrap;"><%# DefaultSanitizer.SanitizeHtml(Eval("Key")) %></td>
                <td class="Col2"><%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %>
                    <%# DefaultSanitizer.SanitizeHtml((string.IsNullOrEmpty(Eval("WarnMessage").ToString())) ?  "" : string.Format(" &nbsp; <div class='sw-suggestion sw-suggestion-warn'><span class='sw-suggestion-icon'></span>{0}</div>", Eval("WarnMessage"))) %>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>