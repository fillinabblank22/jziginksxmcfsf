<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="DatabaseDetails.aspx.cs" Inherits="Orion_Admin_Details_DatabaseDetails" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_12 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src ="~/Orion/Admin/Details/DetailsBlock.ascx" TagPrefix="orion" TagName ="DetailsBlock" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
        <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHAdminDetailsDatabase" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <!-- Page title and hot links -->
        <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>


    <div class="GroupBox" id="DatabaseDetails">
        <orion:DetailsBlock ID="DetailBlock" runat="server" />
        
        <% if (!_hiddenDatabaseDetailsSections.Contains("NetworkElementDetailSection"))
           { %>
        <div class="DetailsLink" >
            <span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/HistoricalStatistics.aspx"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_354) %></a><br />
            <span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/HistoricalStatistics.aspx?ByNode=True"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_355) %></a><br />
            <span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/Details/DatabaseStats.aspx"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_526) %></a><br />
            <span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/Details/ElementsStat.aspx"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_208) %></a><br />
        </div>
        <% } %>
    </div>

</asp:Content>
