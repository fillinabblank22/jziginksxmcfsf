<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExtraPollersModulesDetails.ascx.cs" Inherits="Orion_Admin_Details_ExtraPollersModulesDetails" %>

<%@ Register Src="~/Orion/Admin/Details/ModuleDetailsBlock.ascx" TagPrefix="orion" TagName="ModuleDetailBlock" %>

<div>
    <asp:Repeater ID="AdditionalPollers" runat="server">
        <HeaderTemplate>
            <h2 class="PollerTypeTitle" ><asp:Literal ID="AdditionalPollersTitile" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_421 %>"/></h2>
        </HeaderTemplate>
        <ItemTemplate>
            <orion:ModuleDetailBlock ID="ModuleBlock" runat ="server" />
        </ItemTemplate>
    </asp:Repeater>
</div>