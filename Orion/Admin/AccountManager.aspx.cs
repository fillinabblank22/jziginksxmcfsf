using System;
using System.Web.Security;
using System.Web.UI;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_AccountManager : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        this.accountList.DataSource = Membership.GetAllUsers();
        this.accountList.DataBind();

        base.OnInit(e);
    }

    protected void EditButtonClick(object source, EventArgs e)
    {
        if (!string.IsNullOrEmpty(accountList.SelectedValue))
        {
            string safeUserName = this.Server.UrlEncode(accountList.SelectedValue); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)
            Response.Redirect(string.Format("/Orion/Admin/EditAccount.aspx?AccountID={0}&ReturnTo={1}", safeUserName, ReferrerRedirectorBase.GetReturnUrl()));
        }
    }

    protected void ChangePasswordButtonClick(object source, EventArgs e)
    {
        if (!string.IsNullOrEmpty(accountList.SelectedValue))
        {
            string safeUserName = this.Server.UrlEncode(accountList.SelectedValue); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)
            Response.Redirect(string.Format("/Orion/Admin/ChangePassword.aspx?AccountID={0}&ReturnTo={1}", safeUserName, ReferrerRedirectorBase.GetReturnUrl()));
        }
    }

    protected void AddAccountClick(object source, EventArgs e)
    {
        Response.Redirect(string.Format("/Orion/Admin/NewAccount.aspx?ReturnTo={0}", ReferrerRedirectorBase.GetReturnUrl()));
    }

    protected void DeleteAccountClick(object source, EventArgs e)
    {
        if (!string.IsNullOrEmpty(accountList.SelectedValue))
        {
            try
            {
                // don't allow deletion of Admin account
                if (accountList.SelectedValue.Equals("Admin", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.phErrorMessage.Controls.Clear();
                    this.phErrorMessage.Controls.Add(new LiteralControl("You are not allowed to delete the Admin account."));
                    return;
                }
                else
                {
                    Membership.DeleteUser(accountList.SelectedValue);
                }
            }
            catch
            {
				phErrorMessage.Controls.Add(new LiteralControl(string.Format("Error deleting \"{0}\" account.", accountList.SelectedValue)));
                // bail out so we leave the account name in the list
                return;
            }

            accountList.Items.Remove(accountList.SelectedItem);
        }
    }
}
