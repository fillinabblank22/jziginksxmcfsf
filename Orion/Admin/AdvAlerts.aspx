<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="AdvAlerts.aspx.cs" Inherits="Orion_Admin_AdvAlerts" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_TM0_45 %>" %>

<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="orion" TagName="AutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="AdvAlerts.css" />
    <orion:Include runat="server" File="js/jquery/timePicker.css" />
    <style type="text/css">
        .error
        {
            border: 2px solid red;
        }
        #gridCell div#Grid
        {
            width: 100%;
        }
        #adminContent td
        {
            padding: 0px 0px 0px 0px !important;
        }
        .x-tip-body 
        {
            white-space: nowrap !important;
        }
    </style>
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Admin/js/AdvAlerts.js" />
    <orion:Include runat="server" File="jquery/jquery.timePicker.js" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <script type="text/javascript">
    var okId = "<%= dialogOk.ClientID %>";
    function SwitchInpElStatus(val)      {
    if (val == true)
     {
        $('#editAlertDefDialog :input').removeAttr('disabled');
    }
     else 
     {
     $('#editAlertDefDialog :input').attr('disabled', true);
    } 
}         

function isNumber(elem) {
    var str = elem.value;
    var re = /^[-]?\d*\.?\d*$/;
    str = str.toString();

    if (str.trim().length ==0 || str.trim().length >10 || !str.match(re))
    {
        return false;
    }
    return true;
}

function isInt(elem, maxVal, minVal) {
  var intVal = parseInt(elem.value.trim(), 10);

  return (intVal<maxVal && intVal>=minVal);
}

function validatePeriod(elem, maxVal, minVal){
$('#'+okId).attr('disabled', true);
if (!isNumber(elem))
{
        alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_19) %>");
        elem.focus();
        $(elem).addClass("error");
        return false;
}

if (!isInt(elem, maxVal, minVal))
{
        alert(String.format('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_VB0_278) %>', minVal.toString()));
        elem.focus();
        $(elem).addClass("error");
        return false;
}

    $('.sw-pg-advdialogok-btn').removeAttr('disabled');
    $(elem).removeClass("error");
    return true;
}
    
     jQuery(function() {

     var maxIntVal = Math.pow(2, 31) - 1;
     var minPollInterval = 15;

     var regionalSettings = <%=DatePickerRegionalSettings.GetDatePickerRegionalSettings()%>;
         $('.timePicker').timePicker({ separator: regionalSettings.timeSeparator, show24Hours: regionalSettings.show24Hours });

//         $("#toDate").change(function() {
//             if ($.timePicker("#fromDate").getTime() > $.timePicker(this).getTime()) 
//             {
//                 $(this).addClass("error");
//             }
//             else
//              {
//                 $(this).removeClass("error");
//             }
//         });
         
         $("#executeInterval").change(function() 
         {
                if ($("#timeFrame").val()=='60')
                    return validatePeriod(this, Math.round(maxIntVal/60), 1);
                else
                   if ($("#timeFrame").val()=='3600')
                    return validatePeriod(this, Math.round(maxIntVal/3600), 1);

             return validatePeriod(this, maxIntVal, minPollInterval);
         });

         $("#timeFrame").change(function() 
         {
                var el = $get("executeInterval");
                if ($(this).val()=='60')
                    return validatePeriod(el, Math.round(maxIntVal/60),1);
                else
                   if ($(this).val()=='3600')
                    return validatePeriod(el, Math.round(maxIntVal/3600),1);

             return validatePeriod(el, maxIntVal, minPollInterval);
         });

    //SwitchInpElStatus(false);
     });
    </script>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="../Services/Information.asmx" />
            <asp:ServiceReference Path="../Services/AlertsAdmin.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <input type="hidden" name="WebAlertProps_columns" id="WebAlertProps_columns" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("Orion_AdvAlerts_Columns")) %>' />
    <table width="1150px;">
        <tr>
            <td style="font-size: large">
                <h1>
                    <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
            </td>
        </tr>
        <tr>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_95) %>
                <br />
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_421) %>
            </td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td id="gridCell">
                <div id="Grid" ></div>
            </td>
        </tr>
    </table>
    <div id="editAlertDefDialog">
        <div style="overflow: auto; height: 480px">
            <table class="advAlerts">
                <tbody>
                    <tr>
                        <th style="width: 200px">
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_385) %></b>
                        </th>
                        <td style="width: 67%">
                            <input type="text" id="alertName" size="80" />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_422) %></b>
                        </th>
                        <td>
                            <textarea id="alertDescription" name="alertDescription" rows="5" cols="20" style="width: 99%;"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_423) %></b>
                        </th>
                        <td>
                            <select id="enabled">
                                <option value="1" selected="selected"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_124) %></option>
                                <option value="0"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_125) %></option>
                            </select>
                            <br />
                            <div class="alDescription">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_424) %>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_425) %></b>
                        </th>
                        <td>
                            <select id="ignoreTimeout">
                                <option value="1" selected="selected"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_124) %></option>
                                <option value="0"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_125) %></option>
                            </select>
                            <br />
                            <div class="alDescription">
                                <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_279,
                                    SettingsDAL.GetSetting("AlertEngine-MaxAlertExecutionTimeout").SettingValue.ToString(),
                                                                                                        String.Format("<a href=\"/Orion/Admin/PollingSettings.aspx?ReturnTo={0}\">", this.ReturnUrl),
                                                                                                                                                                            "</a>")) %>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_426) %></b>
                        </th>
                        <td>
                            <input type="text" id="executeInterval" size="8" />
                            <select id="timeFrame">
                                <option value="1" selected="selected"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %></option>
                                <option value="60"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %></option>
                                <option value="3600"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_427) %></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_428) %> </b>
                        </th>
                        <td>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_205) %>
                            <input type="text" class="timePicker" id="fromDate" size="8" />
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_206) %>
                            <input type="text" class="timePicker" id="toDate" size="8" />
                            <br />
                            <div class="alDescription">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_429) %>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_430) %></b>
                        </th>
                        <td>
                            <table class="daysTable" width="100%">
                                <tr>
                                    <td style="width: 25%">
                                        <input type="checkbox" id="cbDOW1" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_431) %>
                                    </td>
                                    <td style="width: 25%">
                                        <input type="checkbox" id="cbDOW6" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_432) %>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%">
                                        <input type="checkbox" id="cbDOW2" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_433) %>
                                    </td>
                                    <td style="width: 25%">
                                        <input type="checkbox" id="cbDOW7" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_434) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <input type="checkbox" id="cbDOW3" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_435) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <input type="checkbox" id="cbDOW4" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_436) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <input type="checkbox" id="cbDOW5" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_437) %>
                                        <br />
                                        <div class="alDescription">
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_438) %>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true" CssClass="colPanel">
                                <TitleTemplate>
                                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_439) %></b>
                                </TitleTemplate>
                                <BlockTemplate>
                                    <table style="background-color: #FAF5C6; height: 25px; width: 100%" cellpadding="0" cellspacing="0" >
                                    <tr>
                                        <td style="width: 20px;">
                                            <img src="/Orion/images/NotificationImages/hint_message_icon_16x16.png" alt="" id="descrImage" style="cursor: pointer;" />
                                        </td>
                                        <td class="alDescription">
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_440) %>
                                        </td>
                                    </tr>
                                    </table>
                                    <table style="margin-top: 0px; padding: 0px; width:100%">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_441) %></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span id="triggerQuery" style="width: 95%;"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_442) %></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label id="triggerActions" style="font-style: italic"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_443) %></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_444) %></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span id="resetQuery" style="width: 95%; "></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_445) %></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label id="resetActions" style="font-style: italic"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_443) %></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_446) %></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span id="suppressionQuery" style="width: 95%; "></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </BlockTemplate>
                            </orion:CollapsePanel>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input id="alertGuid" type="hidden" />
        </div>
        <div class="sw-btn-bar-wizard" style="padding-right:27px;" >
            <orion:LocalizableButtonLink CssClass="sw-pg-advdialogok-btn" ID="dialogOk" runat="server" LocalizedText="Ok" DisplayType="Primary"  style="cursor: pointer;"  />
            <orion:LocalizableButtonLink CssClass="sw-pg-advdialogcancel-btn" ID="dialogCancel" LocalizedText="Cancel" DisplayType="Secondary" runat="server" style="cursor: pointer;" />
        </div>
    </div>
    <div id="descrAlertDialog">
        <table style="font-size: 10pt; background-color: White; margin: 8px; margin-top: 0px;
            padding: 5px; height: 90%; width: 97%;">
            <tbody>
                <tr>
                    <td id="alertDescr" style="vertical-align: top">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_447) %></b><br />
                    </td>
                </tr>
            </tbody>
        </table>
        <div style="text-align: right; margin-right: 8px; padding-top: 10px;">
            <orion:LocalizableButtonLink CssClass="sw-pg-advdialogDescr-btn" ID="dialogDescrCancel" LocalizedText="Cancel" DisplayType="Secondary" runat="server" style="cursor: pointer;" />
        </div>
    </div>
</asp:Content>
