<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" CodeFile="SmtpServersManager.aspx.cs" Inherits="Orion_Admin_SmtpServersManager" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TD0_1 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />    
    <orion:Include runat="server" File="RenderControl.js" />
    <orion:Include runat="server" File="Admin/js/SmtpServersManager.js"/>
    <style type="text/css">
        h1 { font-size: large!important; padding: 15px 0px 10px 10px !important; }
        #adminContent td {padding: 0px !important; vertical-align: middle; border-right-color: #d0d0d0 !important;}
        .x-grid3-hd-row td { border-right: 1px solid !important;}
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <div id="mainContent">
        <h1 class="page-title"><%= DefaultSanitizer.SanitizeHtml(this.Title) %></h1>

        <input type="hidden" name="SMTPServersManager_PageSize" id="SMTPServersManager_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("SMTPServersManager_PageSize")) %>' />
        <input type="hidden" name="SMTPServersManager_SortOrder" id="SMTPServersManager_SortOrder" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "SMTPServersManager_SortOrder",WebConstants.SMTPServersManagerDafultSortOrder)) %>' />
        <input type="hidden" name="SMTPServersManager_SelectedColumns" id="SMTPServersManager_SelectedColumns" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "SMTPServersManager_SelectedColumns",WebConstants.SMTPServersManagerDafultSelectedColumns)) %>' />
        <div id="SmtpServersManagerGrid" style="margin:20px"></div>
    </div>

    <%--TODO: need localize--%>
    <div id="addItemDialog" style="display:none;" ></div> 
    <%if(SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer){%> <div id="isOrionDemoMode" style="display: none;" ></div> <%}%>
</asp:Content>
