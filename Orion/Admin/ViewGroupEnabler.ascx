<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewGroupEnabler.ascx.cs" Inherits="Orion_Admin_ViewGroupEnabler" %>
<%@ Import Namespace="Resources" %>
	<style type="text/css">
        .chkEnableLeftNav label { padding-left: 5px; }
	</style>

    <table style="padding-top: 20px;">
        <tr>
            <td><h2><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB1_35) %></h2></td>
        </tr>
        <tr>
            <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB1_36) %></td>
        </tr>
        <tr>
            <td> <asp:CheckBox ID="chkEnableLeftNav" class="chkEnableLeftNav" onclick="ClickHandler();" OnCheckedChanged="chkEnableLeftNav_CheckedChanged" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_37 %>" /> </td>
        </tr>
    </table>
    <div id="dialog" title="<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB1_61) %>" style="display:none; white-space: nowrap;">
	    <p><b><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB1_38) %></b></p>
        <br />
        <p>
            <input type="radio" name="rbResources" id="move" checked="checked"/>
            <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB1_39_Option1) %>
        </p>
        <p>
            <input type="radio" name="rbResources" id="remove" />
            <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB1_39_Option2) %>
        </p>
       <div class="sw-btn-bar">
            <orion:LocalizableButtonLink ID="btnDialogOk" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_40 %>"  ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_40 %>"/>
            <orion:LocalizableButtonLink ID="btnDialogCancel" runat="server" DisplayType="Primary" LocalizedText="Cancel"  ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>"/>
        </div>
    </div>

    <script type="text/javascript">
         $(function() {
            $('#<%=btnDialogOk.ClientID %>').click(  chkEnableLeftNavPostback );
            $('#<%=btnDialogCancel.ClientID %>').click( CloseDialog);
        });
         
        function chkEnableLeftNavPostback() {
            var resources = $('input[name=rbResources]:checked')[0];
            if (resources && resources.id == "remove") {
                <%= Page.GetPostBackEventReference(chkEnableLeftNav, true.ToString()) %>
            } else {
                <%= Page.GetPostBackEventReference(chkEnableLeftNav, false.ToString()) %>
            }
        }
        function CloseDialog(){ $("#dialog").dialog( "close" );}
        function ClickHandler() {
            if (!$('.chkEnableLeftNav > input')[0].checked) {
                $("#dialog").dialog({
                    title:"<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_54) %>",
                    modal: true,
                    width: 320,
                    close: function(){ 
                        //Put checkmark back if user is cancelling
                        $('.chkEnableLeftNav > input')[0].checked = true;
                    }		    
                });
            }
            else
                <%= Page.GetPostBackEventReference(chkEnableLeftNav) %>
            return false;
        }        
       
    </script>

   