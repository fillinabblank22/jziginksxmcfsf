using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.Helpers;
using Image = System.Drawing.Image;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;
using SolarWinds.Orion.Web.Model.CentralizedSettings;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Admin_Settings : System.Web.UI.Page
{
    private static Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    protected static HashSet<String> _hiddenWebConsoleSettings = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.WebConsoleSettingsHideSection)).Select(c => c.Key));
    private readonly CentralizedSettingsDAL _centralizedSettingsDal = new CentralizedSettingsDAL();
    private const string EnableHstsPolicyCentralizedSetting = "WebsiteSecuritySettings.EnableHstsPolicy";
    private const string RealTimeChartsUpdateIntervalSetting = "Orion.Web.RealTimeChartsUpdateInterval";

    protected override void OnInit(EventArgs e)
    {
        if (!IsPostBack)
        {
            tbSessionTimeout.Text = WebSettingsDAL.Get("Session Timeout", "10");
            ddlWindowsAccountLogin.SelectedValue = WebSettingsDAL.Get("WindowsAccountLogin", "True");
            tbAutoRefresh.Text = WebSettingsDAL.Get("Auto Refresh", "5");
            tbWidgetRefresherTime.Text = WebSettingsDAL.Get("Modern Widget Refresh Rate", "45");
            tbAutoRefreshActiveAlerts.Text = WebSettingsDAL.Get("Auto Refresh Active Alerts", "1");
            var isChartSettingsSectionHidden = _hiddenWebConsoleSettings.Contains("ChartSettingsSection");

            if (!string.IsNullOrEmpty(WebSettingsDAL.NewSiteLogo))
            {
                logoCb.Checked = true;
                LogoBlock.Style["display"] = "block";
                LogoBlockFromUrl.Style["display"] = "block";
                logoFileName.Text = Path.GetFileName(WebSettingsDAL.SiteLogo);
            }
            else
            {
                noLogoBlock.Style["display"] = "block";
            }

            if (!string.IsNullOrEmpty(WebSettingsDAL.NewNOCSiteLogo))
            {
                nocLogoCb.Checked = true;
                NocLogoBlock.Style["display"] = "block";
                nocLogoBlockFromUrl.Style["display"] = "block";
                nocLogoFileName.Text = Path.GetFileName(WebSettingsDAL.NOCSiteLogo);
            }
            else
            {
                noNocLogoBlock.Style["display"] = "block";
            }

            RequiredFieldValidator9.Enabled = !isChartSettingsSectionHidden;
            RequiredFieldValidator10.Enabled = !isChartSettingsSectionHidden;
            RequiredFieldValidator11.Enabled = !isChartSettingsSectionHidden;
            RequiredFieldValidator3.Enabled = !isChartSettingsSectionHidden;

            //tbSiteLogo.Text = WebSettingsDAL.Get("Site Logo", "/NetPerfMon/images/SolarWinds.Logo.gif");
            //tbNOCSiteLogo.Text = WebSettingsDAL.NOCSiteLogo;
            tbSiteLoginText.Text = WebSettingsDAL.Get("Site Login Text", "Site Login Text");
            UseAutocompleteInUsernameField.Checked = WebSettingsDAL.UseBrowserAutoComplete;

            try
            {
                tbOrionWebServerFQDN.Text = WebSettingsDAL.Get("Web-OrionWebServerAddress");
            }
            catch
            {
                tbOrionWebServerFQDN.Text = string.Empty;
            }

            try
            {
                var enableHstsPolicy = _centralizedSettingsDal.GetSettingValue(EnableHstsPolicyCentralizedSetting);
                chbEnableHstsPolicy.Checked = Boolean.Parse(enableHstsPolicy);
            }
            catch (Exception ex)
            {
                chbEnableHstsPolicy.Checked = false;
            }

            tbHelpServer.Text = WebSettingsDAL.Get("HelpServer", "https://www.solarwinds.com");

            try
            {
                tbChartAspect.Text = SettingsDAL.GetSetting("Web-ChartAspectRatio").SettingValue.ToString();
            }
            catch
            {
                tbChartAspect.Text = (0.625).ToString();
            }

            try
            {
                tbThumbnailAspect.Text = SettingsDAL.GetSetting("Web-ThumbnailAspectRatio").SettingValue.ToString();
            }
            catch
            {
                tbThumbnailAspect.Text = (0.5).ToString();
            }

            try
            {
                tbPercentile.Text = SettingsDAL.GetSetting("Web-ChartPercentile").SettingValue.ToString();
            }
            catch
            {
                tbPercentile.Text = "0";
            }

            try
            {
                cbNotifyRemovable.Checked =
                    (SettingsDAL.GetSetting("Discovery-NotifyAboutRemovableVolumes").SettingValue == 1);
            }
            catch
            {
                cbNotifyRemovable.Checked = false;
            }

            try
            {
                cbAutomaticGeolocation.Checked =
                    (SettingsDAL.GetSetting("AutomaticGeolocation-Enable").SettingValue == 1);
            }
            catch
            {
                cbAutomaticGeolocation.Checked = true;
            }

            try
            {
                var auditingTrails = SettingsDAL.GetSetting("SWNetPerfMon-AuditingTrails");
                cbAuditingTrails.Checked = auditingTrails == null || auditingTrails.SettingValue == 1;
            }
            catch (Exception)
            {
                cbAuditingTrails.Checked = true;
            }

            try
            {
                tbMaximumSeries.Text =
                    SettingsDAL.GetSetting("Web-MaximalNumberOfSeriesInChart").SettingValue.ToString();
            }
            catch (Exception)
            {
                tbMaximumSeries.Text = "0";
            }

            try
            {
                tbChartRefresh.Text = _centralizedSettingsDal.GetSettingValue(RealTimeChartsUpdateIntervalSetting);
            }
            catch (Exception)
            {
                tbChartRefresh.Text = "2";
            }

            bool SeenNPM = false, SeenAPM = false;
            foreach (var module in SolarWinds.Orion.Web.OrionModuleManager.GetModules())
            {
                SeenNPM |= module.ProductShortName.Equals("NPM");
                SeenAPM |= module.ProductShortName.Equals("SAM");
            }

            ddlRollupWorstStatus.SelectedValue = WebSettingsDAL.Get("RollupWorstStatus", "False");
            ddlNodeChildStatusDisplayMode.SelectedValue = WebSettingsDAL.Get("NodeChildStatusDisplayMode", "NoBlink");

            try
            {
                ddlTipsIntegration.SelectedIndex = WebSettingsDAL.TipsIntegrationEnabled ? 0 : 1;
            }
            catch (Exception)
            {
                ddlTipsIntegration.SelectedIndex = 0;
            }
            lTipsIntegrationText.Text = Resources.CoreWebContent.WEBDATA_LK0_5;

            try
            {
                var pdfRenderAsImage = WebSettingsDAL.GetValue("PdfExport_RenderAsImage", false);
                ddlPdfChartsRenderAsImage.SelectedIndex = pdfRenderAsImage ? 1 : 0;
            }
            catch (Exception)
            {
                ddlPdfChartsRenderAsImage.SelectedIndex = 0;
            }

            (CheckResourceMigration() ? promptChartsRollback : promptChartsMigration).Style["display"] = "none";
        }

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (logoCb.Checked)
            {
                LogoBlock.Style["display"] = "block";
                noLogoBlock.Style["display"] = "none";
                logoFileName.Text = Path.GetFileName(fileUploadImage.HasFile ? fileUploadImage.FileName : WebSettingsDAL.SiteLogo);

                if (hasLogoFile())
                    notification.Style["display"] = "block";

                if (logoFromUrlCb.Checked)
                {
                    LogoFromUrlInput.Style["display"] = "block";
                }
            }
            else
            {
                LogoBlockFromUrl.Style["display"] = "none";
                LogoBlock.Style["display"] = "none";
                noLogoBlock.Style["display"] = "block";
            }

            if (nocLogoCb.Checked)
            {
                NocLogoBlock.Style["display"] = "block";
                noNocLogoBlock.Style["display"] = "none";
                nocLogoFileName.Text = Path.GetFileName(fileUploadImageNoc.HasFile ? fileUploadImageNoc.FileName : WebSettingsDAL.NOCSiteLogo);

                if (hasNocLogoFile())
                    nocNotification.Style["display"] = "block";

                if (nocLogoFromUrlCb.Checked)
                {
                    nocLogoFromUrlInput.Style["display"] = "block";
                }
            }
            else
            {
                nocLogoBlockFromUrl.Style["display"] = "none";
                NocLogoBlock.Style["display"] = "none";
                noNocLogoBlock.Style["display"] = "block";
            }
        }
    }

    protected void CustomLogo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!Page.Request.Params["__EVENTTARGET"].Equals(imgbtnSubmit.UniqueID))
            return;

        if (logoCb.Checked && fileUploadImage.HasFile)
        {
            if (!isApplicableImage(fileUploadImage.FileContent))
            {
                args.IsValid = false;
            }
            else
            {
                // due to http://ajaxcontroltoolkit.codeplex.com/workitem/26729 have to store it to session
                byte[] imageBytes = fileUploadImage.FileBytes;
                Session["SitelogoImage"] = Convert.ToBase64String(imageBytes);
                Session["SitelogoImageFile"] = fileUploadImage.FileName;
            }
        }
    }

    protected void CustomLogoFromUrl_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!Page.Request.Params["__EVENTTARGET"].Equals(imgbtnSubmit.UniqueID))
            return;

        if (logoFromUrlCb.Checked && logoPath.Text.Length > 0)
        {
            if (ValidateLogoFromUrl(logoPath.Text))
            {
                Session["SitelogoImageFile"] = logoPath.Text;
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    private bool ValidateLogoFromUrl(string url)
    {
        try
        {
            Uri uriResult;
            var ext = Path.GetExtension(url);
            return (ext == ".png" ||
                    ext == ".jpg" ||
                    ext == ".gif") ||
                   // in a case url Schema is 'file' then ext was checked above
                   (Uri.TryCreate(url, UriKind.Absolute, out uriResult) &&
                    uriResult != null &&
                    uriResult.Scheme != "file");
        }
        catch (Exception ex)
        {
            log.Error("Error during logo validation.", ex);
            return false;
        }
    }

    protected void CustomNocLogo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!Page.Request.Params["__EVENTTARGET"].Equals(imgbtnSubmit.UniqueID))
            return;

        if (nocLogoCb.Checked && fileUploadImageNoc.HasFile)
        {
            if (!isApplicableImage(fileUploadImageNoc.FileContent))
            {
                args.IsValid = false;
            }
            else
            {
                // due to http://ajaxcontroltoolkit.codeplex.com/workitem/26729 have to store it to session
                byte[] imageBytes = fileUploadImageNoc.FileBytes;
                Session["SiteNoclogoImage"] = Convert.ToBase64String(imageBytes);
                Session["SiteNoclogoImageFile"] = fileUploadImageNoc.FileName;
            }
        }
    }

    protected void CustomNocLogoFromUrl_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!Page.Request.Params["__EVENTTARGET"].Equals(imgbtnSubmit.UniqueID))
            return;

        if (nocLogoFromUrlCb.Checked && nocLogoPath.Text.Length > 0)
        {
            if (ValidateLogoFromUrl(nocLogoPath.Text))
            {
                Session["SiteNoclogoImageFile"] = nocLogoPath.Text;
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    private bool isApplicableImage(Stream imageStream)
    {
        try
        {
            using (Image image = Image.FromStream(imageStream))
                return (image.Width <= 900) && (image.Height <= 200);
        }
        catch
        {
            return false;
        }
    }

    protected void AsyncFileUpload_UploadeComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        //CheckImage(fileUploadImage);
    }

    protected void AsyncFileUpload_UploadeCompleteNoc(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        //CheckImage(fileUploadImageNoc);
    }

    private bool hasLogoFile()
    {
        return (fileUploadImage.HasFile) || (Session["SitelogoImage"] != null);
    }

    private bool hasNocLogoFile()
    {
        return (fileUploadImageNoc.HasFile) || (Session["SiteNoclogoImage"] != null);
    }

    protected void imgbtnSubmit_Click(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        double value = 0;

        WebSettingsDAL.Set("Modern Widget Refresh Rate", tbWidgetRefresherTime.Text);
        WebSettingsDAL.Set("WindowsAccountLogin", ddlWindowsAccountLogin.SelectedValue);
        WebSettingsDAL.Set("RollupWorstStatus", ddlRollupWorstStatus.SelectedValue);
        WebSettingsDAL.Set("Session Timeout", tbSessionTimeout.Text);
        WebSettingsDAL.Set("Auto Refresh", tbAutoRefresh.Text);

        //WebSettingsDAL.Set("Site Logo", tbSiteLogo.Text);
        // due to http://ajaxcontroltoolkit.codeplex.com/workitem/26729 have to store it to session

        if (!logoCb.Checked)
        {
            WebSettingsDAL.NewSiteLogo = null;
            WebSettingsDAL.SiteLogo = string.Empty;
        }
        else if (logoFromUrlCb.Checked && Session["SitelogoImageFile"] != null)
        {
            WebSettingsDAL.SiteLogo = Session["SitelogoImageFile"].ToString();
            Session.Remove("SitelogoImageFile");
            WebSettingsDAL.NewSiteLogo = "nologo";
        }
        else if (Session["SitelogoImage"] != null)
        {
            WebSettingsDAL.NewSiteLogo = Session["SitelogoImage"].ToString();
            Session.Remove("SitelogoImage");
            WebSettingsDAL.SiteLogo = Session["SitelogoImageFile"].ToString();
            Session.Remove("SitelogoImageFile");
        }

        if (!nocLogoCb.Checked)
        {
            WebSettingsDAL.NewNOCSiteLogo = null;
            WebSettingsDAL.NOCSiteLogo = string.Empty;
        }
        else if (nocLogoFromUrlCb.Checked && Session["SiteNoclogoImageFile"] != null)
        {
            WebSettingsDAL.NOCSiteLogo = Session["SiteNoclogoImageFile"].ToString();
            Session.Remove("SiteNoclogoImageFile");
            WebSettingsDAL.NewNOCSiteLogo = "nologo";
        }
        else if (Session["SiteNoclogoImage"] != null)
        {
            WebSettingsDAL.NewNOCSiteLogo = Session["SiteNoclogoImage"].ToString();
            Session.Remove("SiteNoclogoImage");
            WebSettingsDAL.NOCSiteLogo = Session["SiteNoclogoImageFile"].ToString();
            Session.Remove("SiteNoclogoImageFile");
        }

        //WebSettingsDAL.NOCSiteLogo = tbNOCSiteLogo.Text;
        WebSettingsDAL.Set("Site Login Text", WebSecurityHelper.SanitizeHtmlV2(tbSiteLoginText.Text));
        WebSettingsDAL.Set("Web-OrionWebServerAddress", WebSecurityHelper.SanitizeHtmlV2(tbOrionWebServerFQDN.Text));
        WebSettingsDAL.Set("HelpServer", WebSecurityHelper.SanitizeHtmlV2(tbHelpServer.Text));

        var enableHstsPolicy = (chbEnableHstsPolicy.Checked) ? true : false;
        _centralizedSettingsDal.UpdateSettingValue(EnableHstsPolicyCentralizedSetting, enableHstsPolicy.ToString());

        if (!_hiddenWebConsoleSettings.Contains("ChartSettingsSection"))
        {

            value = Convert.ToDouble(tbChartAspect.Text);
            CorrectValue(ref value, 0.1, 5);
            SettingsDAL.SetValue("Web-ChartAspectRatio", value);

            value = Convert.ToDouble(tbThumbnailAspect.Text);
            CorrectValue(ref value, 0.1, 5);
            SettingsDAL.SetValue("Web-ThumbnailAspectRatio", value);

            value = Convert.ToDouble(tbPercentile.Text);
            CorrectValue(ref value, 0, 99);
            SettingsDAL.SetValue("Web-ChartPercentile", value);

            value = Convert.ToDouble(tbMaximumSeries.Text);
            CorrectValue(ref value, 0, 1000);
            SettingsDAL.SetValue("Web-MaximalNumberOfSeriesInChart", value);

            _centralizedSettingsDal.UpdateSettingValue(RealTimeChartsUpdateIntervalSetting, tbChartRefresh.Text);
        }

        WebSettingsDAL.TipsIntegrationEnabled = ddlTipsIntegration.SelectedIndex == 0;

        WebSettingsDAL.SetValue("PdfExport_RenderAsImage", ddlPdfChartsRenderAsImage.SelectedIndex == 1);

        SettingsDAL.SetValue("Discovery-NotifyAboutRemovableVolumes", (cbNotifyRemovable.Checked) ? 1 : 0);
        SettingsDAL.SetValue("AutomaticGeolocation-Enable", (cbAutomaticGeolocation.Checked) ? 1 : 0);

        SettingsDAL.SetValue("SWNetPerfMon-AuditingTrails", cbAuditingTrails.Checked ? 1 : 0);

        // -- overwrite the node child status display mode, and recalculate status for all nodes on change.
        string CurrentDisplayMode = WebSettingsDAL.Get("NodeChildStatusDisplayMode", "");
        WebSettingsDAL.Set("NodeChildStatusDisplayMode", ddlNodeChildStatusDisplayMode.SelectedValue);

        string CurrentRollupMode = WebSettingsDAL.Get("NodeChildStatusParticipationRule", "*");

        WebSettingsDAL.Set("Auto Refresh Active Alerts", tbAutoRefreshActiveAlerts.Text);

        if (ddlNodeChildStatusDisplayMode.SelectedValue.Equals(CurrentDisplayMode) == false)
        {
            NodeChildStatusDAL.InvalidateLocalCache();

            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                proxy.ReflowAllNodeChildStatus();
            }
        }

        WebSettingsDAL.UseBrowserAutoComplete = UseAutocompleteInUsernameField.Checked;

        Response.Redirect("/Orion/Admin/");
    }

    private static void CorrectValue(ref double value, double min, double max)
    {
        if (value < min)
        {
            value = min;
            return;
        }

        if (value > max)
        {
            value = max;
            return;
        }
    }
    
    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    private bool CheckResourceMigration()
    {
        try
        {
            using (var proxy = InformationServiceProxy.CreateV3())
            {
                return proxy.Invoke<bool>("Orion.Resources", "CheckResourceMigration");
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            return false;
        }
    }
}
