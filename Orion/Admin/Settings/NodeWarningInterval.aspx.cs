﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_Settings_NodeWarningInterval : System.Web.UI.Page
{
	public OrionSetting DefaultFastPollInterval;	

	protected void Page_Load(object sender, EventArgs e)
	{
		Initialize();
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty(hfDefaultFastPollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultFastPollInterval", Convert.ToDouble(hfDefaultFastPollInterval.Value));
		
		Response.Redirect("../NPMSettings.aspx");
	}

	protected void btnCancel_Click(object sender, EventArgs e)
	{
		Response.Redirect("../NPMSettings.aspx");
	}

	private void Initialize()
	{
		DefaultFastPollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultFastPollInterval");		
	}
}
