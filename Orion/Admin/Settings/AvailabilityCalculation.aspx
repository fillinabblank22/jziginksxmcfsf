<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/Settings/OrionSettings.master"
    AutoEventWireup="true" CodeFile="AvailabilityCalculation.aspx.cs" Inherits="Orion_Admin_Settings_AvailabilityCalculation"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <a href="../../Admin">Admin</a> &gt; <a href="..">Availability calculation</a> &gt;
                <h1>
                    <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
            </td>
            <td align="right" style="vertical-align: top;">
            </td>
        </tr>
    </table>
    <div class="GroupBox">
        Orion can calculate Node Availability two different ways.
        This is an
        <br />
        advanced feature. If you are not sure how to set this, then select "Method 1".
        <br />
        <br />
        <div class="contentBlock">
            <asp:RadioButtonList runat="server" ID="rblAvailabilityCalculation">
                <asp:ListItem Text="Method 1 - Based on Node Status <br/> Availability Calculations are based on Node Status. Node Availability is 0% when the Node's Status is Down or Unknown. Availability is 100% when the Node's Status is Up."
                    Value="Method1"></asp:ListItem>
                <asp:ListItem Text="Method 2 -Based on Percent Packet Loss <br/> Availability calculations are based on Packet Loss. If the Node is dropping packets, Availability may show less than 100% even though Node Status is Up."
                    Value="Method2"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" />
    <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
</asp:Content>
