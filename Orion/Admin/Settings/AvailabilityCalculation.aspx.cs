﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_Settings_AvailabilityCalculation : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		Initalize();
	}

	private void Initalize()
	{
		int methodNum = Convert.ToInt32(SettingsDAL.GetSetting("NetPerfMon-AvailibilityCalculation").SettingValue);

		rblAvailabilityCalculation.SelectedIndex = methodNum - 1;
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		SettingsDAL.SetValue("NetPerfMon-AvailibilityCalculation", rblAvailabilityCalculation.SelectedIndex +  1);
	}
}
