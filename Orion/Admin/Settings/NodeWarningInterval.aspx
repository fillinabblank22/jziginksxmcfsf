<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/Settings/OrionSettings.master"
    AutoEventWireup="true" CodeFile="NodeWarningInterval.aspx.cs" Inherits="Orion_Admin_Settings_NodeWarningInterval"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <a href="../../Admin">Admin</a> &gt; <a href="..">Node Warning Interval</a> &gt;
                <h1>
                    <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
            </td>
            <td align="right" style="vertical-align: top;">
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    //<![CDATA[
    
        $(document).ready(function()
        {
        var prefix = "ctl00_ctl00_ContentPlaceHolder1_adminContentPlaceholder_";
            
            $("#dDefaultFastPollInterval").slider({
                stepping: 1,
                min: <%=this.DefaultFastPollInterval.Min %>,
                max: <%=this.DefaultFastPollInterval.Max %>,
                startValue: <%= this.DefaultFastPollInterval.SettingValue %>,
                change: function(e, ui)
                {                              
                    var obj = $("#" + prefix + "dDefaultNodeStatPollInterval");
                    var obj1 = $("#" + prefix + "hfDefaultFastPollInterval");
                    obj.text(ui.value);
                    obj1.val(ui.value);
                }
            });
        });
        //]]>
    </script>

    <div class="GroupBox">
        When devices stop responding or are simply dropping packets, Orion<br />
        will switch their status from UP to WARNING. If the device does not respond
        <br />
        within a specified amount of time, the device's status is changed to DOWN.
        <div class="contentBlock">
            This setting specifes the amount of time Orion will wait for a <br />
            device to respond before changing its status to DOWN.
            <br />
            Mark devices as DOWN that have not responded  in <span id="dDefaultFastPollInterval"
                runat="server">
                <%= DefaultSanitizer.SanitizeHtml(this.DefaultFastPollInterval.SettingValue) %>
            </span>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DefaultFastPollInterval.Units) %>
            <div id='dDefaultFastPollInterval' class='ui-slider-1' style="margin: 0px;">
                <div class='ui-slider-handle'>
                </div>
            </div><asp:HiddenField runat="server" ID="hfDefaultFastPollInterval" />          
            <br />
            <br />
        </div>
    </div>
    <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" />
    <asp:Button runat="server" ID="btnCancel" Text="Save" OnClick="btnCancel_Click" />
</asp:Content>
