﻿using System;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.DAL;
using SettingsDAL=SolarWinds.Orion.Web.DAL.SettingsDAL;

public partial class Orion_Admin_Settings_Polling : System.Web.UI.Page
{
	public OrionSetting DefaultNodePollInterval;
	public OrionSetting DefaultVolumePollInterval;
	public OrionSetting DefaultRediscoveryInterval;

	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	private void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		Initialize();
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty(hfDefaultNodePollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultNodePollInterval", Convert.ToDouble(hfDefaultNodePollInterval.Value));
		if (!string.IsNullOrEmpty(hfDefaultVolumePollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultVolumePollInterval", Convert.ToDouble(hfDefaultVolumePollInterval.Value));
		if (!string.IsNullOrEmpty(hfDefaultRediscoveryInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultRediscoveryInterval", Convert.ToDouble(hfDefaultRediscoveryInterval.Value));

		Response.Redirect("../NPMSettings.aspx");
	}

	protected void btnApplyToAll_Click(object sender, EventArgs e)
	{
		int nodePollInterval = Convert.ToInt32(string.IsNullOrEmpty(hfDefaultNodePollInterval.Value) ? "0" : hfDefaultNodePollInterval.Value);
		int volumePollInterval = Convert.ToInt32(string.IsNullOrEmpty(hfDefaultNodePollInterval.Value) ? "0" : hfDefaultVolumePollInterval.Value);

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			if (PollingEngines1.AllSelected)
			{
				if (!string.IsNullOrEmpty(hfDefaultNodePollInterval.Value))
					proxy.BulkUpdateNodePollingInterval(nodePollInterval, 0);
			}
			else
			{
				if (!string.IsNullOrEmpty(hfDefaultNodePollInterval.Value))
					foreach (int engineId in PollingEngines1.SelectedEngines)
					{
						proxy.BulkUpdateNodePollingInterval(nodePollInterval, engineId);
					}
			}
		}

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			if (PollingEngines1.AllSelected)
			{
				if (!string.IsNullOrEmpty(hfDefaultVolumePollInterval.Value))
					proxy.BulkUpdateVolumePollingInterval(volumePollInterval, 0);
			}
			else
			{
				if (!string.IsNullOrEmpty(hfDefaultVolumePollInterval.Value))
					foreach (int engineId in PollingEngines1.SelectedEngines)
					{
						proxy.BulkUpdateVolumePollingInterval(volumePollInterval, engineId);
					}
			}
		}
        
		ClientScript.RegisterStartupScript(this.GetType(), "savesuccess", "Values have been successfully applied");
		Initialize();
	}

	private void Initialize()
	{
		DefaultNodePollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultNodePollInterval");
		DefaultVolumePollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultVolumePollInterval");
		DefaultRediscoveryInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultRediscoveryInterval");
	}
}

