<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" AutoEventWireup="true" 
    CodeFile="EditLimitation.aspx.cs" Inherits="Orion_Admin_EditLimitation" 
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_187 %>" %>
<%@ Register TagPrefix="orion" TagName="SwisfErrorControl" Src="~/Orion/SwisfErrorControl.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_191) %></h1>
    
    <h2><asp:PlaceHolder ID="phTypeName" runat="server" /></h2>
    
    <asp:PlaceHolder ID="phTypeDesc" runat="server" />
    
    <br />
    <orion:SwisfErrorControl runat="server" ID="SwisfErrorControl" Visible="False" />
    <br />
    
    
    <asp:ListBox runat="server" ID="lstNetObjects" DataTextField="Value" DataValueField="Key" Rows="1" SelectionMode="single" />
    
    <asp:CheckBoxList runat="server" ID="cbxLstObjects" DataTextField="Value" DataValueField="Key" />
    
    <asp:TextBox runat="server" ID="txtPattern" Rows="1" Columns="28" />
    
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" automation="Submit" DisplayType="Primary" />
    </div>
</asp:Content>
