using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_ViewLimitationEditor : System.Web.UI.UserControl
{
    private ViewInfo _view = null;

    public ViewInfo View
    {
        get 
        {
            if (null == this._view)
                this._view = ViewManager.GetViewById(Convert.ToInt32(Request.QueryString["ViewID"]));
            

            return _view; 
        }
    }

    protected bool HasLimitation
    {
        get
        {
            if (null == this.View)
                return false;
            if (this.View.LimitationID <= 0)
                return false;
            if (null == Limitation.GetLimitationByID(this.View.LimitationID))
                return false;
            return true;
        }
    }

    private string ArrayToSanitizedString(string[] arr)
    {
        if (arr == null || arr.Length == 0)
            return string.Empty;

        StringBuilder sb = new StringBuilder();
        int counter = arr.Length;
        foreach (string item in arr)
        {

            sb.Append(WebSecurityHelper.HtmlEncode(item));
            counter--;
            if (counter > 0)
                sb.Append("<br />");
        }

        return sb.ToString();
    }

    protected override void OnInit(EventArgs e)
    {
        if (this.HasLimitation)
        {
            this.phDetail.Controls.Clear();
            this.phType.Controls.Clear();
            Limitation lim = Limitation.GetLimitationByID(this.View.LimitationID);
            this.phType.Controls.Add(new LiteralControl(WebSecurityHelper.HtmlEncode(lim.Type.Name)));
            
            if (lim.Type.Method == LimitationMethod.Pattern)
            {
                phDetail.Controls.Add(new LiteralControl(ArrayToSanitizedString(lim.Items)));
            }
            else
            {
                using (var swisErrorsContext = new SwisErrorsContext())
                {
                    phDetail.Controls.Add(new LiteralControl(ArrayToSanitizedString(lim.GetDisplayNames())));
                    var swisErrorMessages = swisErrorsContext.GetErrorMessages();
                    if (swisErrorMessages.Any())
                    {
                        SwisfErrorControl.SetError(
                            swisErrorMessages.Select(
                                error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList());
                        SwisfErrorControl.Visible = true;
                    }
                    else
                    {
                        SwisfErrorControl.Visible = false;
                    }
                }
            }

            this.phDetail.Visible = true;
            this.phType.Visible = true;
            this.phNoLimitation.Visible = false;
            this.btnDeleteLimitation.Visible = true;
        }
        else
        {
            this.phDetail.Visible = false;
            this.phType.Visible = false;
            this.phNoLimitation.Visible = true;
            this.btnDeleteLimitation.Visible = false;
        }


        base.OnInit(e);
    }

    protected void EditLimitation_Click(object sender, EventArgs e)
    {
        if(this.HasLimitation)
            Response.Redirect(string.Format("/Orion/Admin/EditLimitation.aspx?ViewID={0}&LimitationID={1}&ReturnTo={2}", this.View.ViewID, this.View.LimitationID, ReferrerRedirectorBase.GetReturnUrl()));

        Response.Redirect(string.Format("/Orion/Admin/SelectLimitationType.aspx?ViewID={0}&ReturnTo={1}", this.View.ViewID, ReferrerRedirectorBase.GetReturnUrl()));
    }

    protected void DeleteLimitation_Click(object sender, EventArgs e)
    {
        if (this.View.LimitationID > 0)
        {
            Limitation.DeleteLimitation(this.View.LimitationID);
            this.View.LimitationID = 0;
            ViewManager.UpdateView(this.View);

            this.phDetail.Visible = false;
            this.phType.Visible = false;
            this.phNoLimitation.Visible = true;
            this.btnDeleteLimitation.Visible = false;
            SwisfErrorControl.Visible = false;
        }
    }
}
