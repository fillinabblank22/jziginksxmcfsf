using System;
using System.Collections.Generic;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_LimitedAdminPage : System.Web.UI.MasterPage
{
    internal bool _isOrionDemoMode;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if(!Profile.AllowNodeManagement && !(this.Page is IBypassAccessLimitation) )
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                _isOrionDemoMode = true;
				return;
            }
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 }); 
        }
    }

	protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
	{
		if (!CommonWebHelper.IsBreadcrumbsDisabled)
		{
			string viewID = (Request["ViewID"] != null) ? Request["ViewID"].ToString() : null;
			string returnTo = (Request["ReturnTo"] != null) ? Request["ReturnTo"].ToString() : null;
			string accountID = (Request["AccountID"] != null) ? Request["AccountID"].ToString() : null;
			var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
			renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
			OrionSiteMapPath.SetUpRenderer(renderer);
		}
	}
}
