<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TabMenuBars.ascx.cs" Inherits="Orion_Admin_TabMenuBars" %>

<asp:Repeater ID="tabBars" runat="server" OnItemDataBound="TabsDataBound">
	<ItemTemplate>
	    <tr>
            <td>&nbsp;</td>
			<td>
			    <%# DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_290, GetTabText(Eval("TabName") as string))) %>
			</td>
			<td>
				<asp:PlaceHolder ID="PlaceHolder1" runat="server" OnDataBinding="TabsDataBound" />
			</td>
			<td id="td" runat="server">
                <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_289, "<a style=\"color: Blue; text-decoration: underline\" href=\"/Orion/Admin/SelectMenuBar.aspx\" >", "</a>")) %>
            </td>
       </tr>
	</ItemTemplate>
</asp:Repeater>
