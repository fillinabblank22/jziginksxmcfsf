using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_AccountViews : System.Web.UI.Page
{
    protected const string trueImg = "/Orion/images/Check.Green.gif";
    protected const string falseImg = "/Orion/images/Check.None.gif";

	private static ITabManager manager = new TabManager();

    protected override void OnInit(EventArgs e)
    {
        PropertyInfoCollection props = OrionModuleManager.GetUserPropertyInfo();
        List<PropertyInfo> viewProps = new List<PropertyInfo>();
        foreach (string vType in ViewManager.GetViewTypes())
        {
            string propName = ViewManager.GetUserPropertyNameByViewType(vType);

            if (propName.Contains("."))
            {
                string propIndex = propName.Substring(propName.IndexOf('.') + 1);
                if (props.Select(p=> p.Name).Contains(propIndex))
                {
                    viewProps.Add(props[propIndex]);
                }
            }
            
        }

        ProfileInfoCollection pInfoList = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All);
        List<ProfileCommon> profiles = new List<ProfileCommon>();
        foreach (ProfileInfo info in pInfoList)
        {
            profiles.Add(Profile.GetProfile(info.UserName));
        }

        this.AccountTable.Controls.Add(this.CreateAccountTable(profiles, viewProps));

        base.OnInit(e);
    }

    private HtmlTable CreateAccountTable(List<ProfileCommon> profiles, List<PropertyInfo> viewProps)
    {
        HtmlTable table = new HtmlTable();
        table.CellSpacing = 0;
        table.Attributes["class"] = "AccountList";
        // create the rows
        HtmlTableRow headerRow = new HtmlTableRow();
        HtmlTableRow enabledRow = new HtmlTableRow();
        HtmlTableRow defaultNetworkDeviceRow = new HtmlTableRow();
		var tabRows = new List<HtmlTableRow>();
        HtmlTableRow[] viewRows = new HtmlTableRow[viewProps.Count];
        // add the rows to the table
        table.Rows.Add(headerRow);
        table.Rows.Add(enabledRow);
        table.Rows.Add(defaultNetworkDeviceRow);
        // add the first column - headers
        AddHeader(headerRow, "Account");
        AddHeader(enabledRow, "Account Enabled");
        AddHeader(defaultNetworkDeviceRow, "Default Network Device");

		foreach (var tab in manager.GetAllTabs())
		{
			var tabRow = new HtmlTableRow { ID = tab.TabId.ToString() };
			tabRows.Add(tabRow);
			AddHeader(tabRow, string.Format("{0} Tab", tab.TabName));
			table.Rows.Add(tabRow);
		}
        int viewRowsIndex = 0;
        foreach (PropertyInfo view in viewProps)
        {
            HtmlTableRow row = new HtmlTableRow();
            AddHeader(row, view.FriendlyName);
            table.Rows.Add(row);
            viewRows[viewRowsIndex++] = row;
        }
        // add a column for each user
        foreach (ProfileCommon profile in profiles)
        {
            HtmlAnchor link = new HtmlAnchor();
            link.InnerText = profile.UserName;
            link.Attributes["class"] = "AccountLink";
            link.HRef = String.Format("/Orion/Admin/Accounts/EditAccount.aspx?AccountID={0}&ReturnTo={1}", 
                Server.UrlEncode(profile.UserName), ReferrerRedirectorBase.GetReturnUrl());
            AddCell(headerRow, link);

            ImageButton button = new ImageButton();
            button.ImageUrl = profile.AccountEnabled ? trueImg : falseImg;
            button.CommandName = "AccountEnabled";
            button.CommandArgument = profile.UserName;
            button.Click += new ImageClickEventHandler(this.ToggleButton_Click);
            AddCell(enabledRow, button);

            AddCell(defaultNetworkDeviceRow, profile.DefaultNetObject);

			foreach (var tabRow in tabRows)
			{
				var barName = manager.GetCurrentBarName(profile.UserName, Convert.ToInt32(tabRow.ID));
				AddCell(tabRow, (string.IsNullOrEmpty(barName)) ? "None" : string.Format("{0} Menu Bar", barName));
			}

            viewRowsIndex = 0;
            foreach (PropertyInfo view in viewProps)
            {
                string cellContents;
                int viewID = (int)profile.GetProfileGroup(view.ModuleID).GetPropertyValue(view.Name);
                if (viewID > 0)
                {
                    ViewInfo vInfo = ViewManager.GetViewById(viewID);
                    if (vInfo == null)
                        cellContents = "Missing";
                    else
                        cellContents = vInfo.ViewTitle;
                }
                else if (viewID == 0)
                {
                    cellContents = "None";
                }
                else if (viewID == -1)
                {
                    cellContents = "By Device Type";
                }
                else
                {
                    cellContents = "Default";
                }

                AddCell(viewRows[viewRowsIndex++], cellContents);
            }
        }
        return table;
    }

    private static void AddCell(HtmlTableRow row, string contents)
    {
        if (String.IsNullOrEmpty(contents))
        {
            contents = "&nbsp;";
        }
        AddCell(row, new LiteralControl(contents));
    }

    private static void AddCell(HtmlTableRow row, Control contents)
    {
        HtmlTableCell cell = new HtmlTableCell();
        cell.Controls.Add(contents);
        row.Cells.Add(cell);
    }

    private static void AddHeader(HtmlTableRow row, string contents)
    {
        HtmlTableCell cell = new HtmlTableCell();
        cell.Attributes["class"] = "header";
        cell.Controls.Add(new LiteralControl(contents));
        row.Cells.Add(cell);
    }

    protected void ToggleButton_Click(object sender, EventArgs e)
    {
        ImageButton button = (ImageButton)sender;
        string propertyName = button.CommandName;
        string username = button.CommandArgument;
        ProfileCommon thisProfile = Profile.GetProfile(username);

        bool buttonState = button.ImageUrl.Equals(trueImg);
        thisProfile.SetPropertyValue(propertyName, !buttonState);
        thisProfile.Save();
        button.ImageUrl = buttonState ? falseImg : trueImg;

        Response.Redirect(Request.Url.AbsolutePath, true);
    }
}
