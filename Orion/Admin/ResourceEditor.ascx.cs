using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Logging;

public partial class Orion_Admin_ResourceEditor : BaseViewGroupControl
{
    private readonly Log _log = new Log();
    private ResourceInfoCollection _resources;
    private bool _isColumnWidthChanged;
    protected ResourceInfoCollection AllResources
    {
        get
        {
            if (_resources == null)
                _resources = ResourceManager.GetResourcesForView(this.View.ViewID);
            return _resources;
        }
    }

    protected override void RebuildUI()
    {
        this.divColumn2.Visible = this.View.ColumnCount > 1;
        this.divColumn3.Visible = this.View.ColumnCount > 2;
        this.divColumn4.Visible = this.View.ColumnCount > 3;
        this.divColumn5.Visible = this.View.ColumnCount > 4;
        this.divColumn6.Visible = this.View.ColumnCount > 5;
        this.addColumnBox.Visible = this.View.ColumnCount != 6;

        this.btnMoveRight1.Visible = this.View.ColumnCount > 1;
        this.btnMoveLeft1.Visible = this.View.ColumnCount > 1;

        this.btnMoveRight2.Visible = this.View.ColumnCount > 2;
        this.btnMoveLeft2.Visible = this.View.ColumnCount > 2;

        this.btnMoveRight3.Visible = this.View.ColumnCount > 3;
        this.btnMoveLeft3.Visible = this.View.ColumnCount > 3;

        this.btnMoveRight4.Visible = this.View.ColumnCount > 4;
        this.btnMoveLeft4.Visible = this.View.ColumnCount > 4;

        this.btnMoveRight5.Visible = this.View.ColumnCount > 5;
        this.btnMoveLeft5.Visible = this.View.ColumnCount > 5;

        this.Visible = this.View.ColumnCount <= 6 && this.View.ColumnCount > 0 && !this.View.IsReadOnly;

        this.lbxColumn1.DataSource = this.AllResources.GetResourcesForColumn(1);
        this.lbxColumn1.DataBind();
        col1Width.Text = this.View.Column1Width.ToString();

        if (this.View.ColumnCount > 1)
        {
            this.lbxColumn2.DataSource = this.AllResources.GetResourcesForColumn(2);
            this.lbxColumn2.DataBind();
            col2Width.Text = this.View.Column2Width.ToString();
        }

        if (this.View.ColumnCount > 2)
        {
            this.lbxColumn3.DataSource = this.AllResources.GetResourcesForColumn(3);
            this.lbxColumn3.DataBind();
            col3Width.Text = this.View.Column3Width.ToString();
        }

        if (this.View.ColumnCount > 3)
        {
            this.lbxColumn4.DataSource = this.AllResources.GetResourcesForColumn(4);
            this.lbxColumn4.DataBind();
            col4Width.Text = this.View.Column4Width.ToString();
        }

        if (this.View.ColumnCount > 4)
        {
            this.lbxColumn5.DataSource = this.AllResources.GetResourcesForColumn(5);
            this.lbxColumn5.DataBind();
            col5Width.Text = this.View.Column5Width.ToString();
        }

        if (this.View.ColumnCount > 5)
        {
            this.lbxColumn6.DataSource = this.AllResources.GetResourcesForColumn(6);
            this.lbxColumn6.DataBind();
            col6Width.Text = this.View.Column6Width.ToString();
        }

        ResourcePickerDialog.ViewType = View.ViewType;
        BindViewGroup();
    }

    private void BindViewGroup()
    {
        if (this.View.ViewGroup != 0)
        {
            rptTabs.DataSource = ViewManager.GetViewsByGroup(this.View.ViewGroup);
            rptTabs.DataBind();

            divMoveTab1.Visible = true;
            divMoveTab2.Visible = true;
            divMoveTab3.Visible = true;
            divMoveTab4.Visible = true;
            divMoveTab5.Visible = true;
            divMoveTab6.Visible = true;
        }
    }

    public void columnWidth_TextChanged(Object sender, EventArgs e)
    {
        this._isColumnWidthChanged = true;
    }

    private void ReorderResources(ListBox column)
    {
        for (int i = 0; i < column.Items.Count; i++)
            ResourceManager.SetPositionById(i + 1, Convert.ToInt32(column.Items[i].Value));
    }

    private void MoveUp(ListBox column)
    {
        int[] selection = column.GetSelectedIndices();
        for (int i = 0; i < selection.Length; i++)
        {
            int selected = selection[i];
            if (selected > i)
            {
                ListItem item = column.Items[selected];
                column.Items.RemoveAt(selected);
                column.Items.Insert(selected - 1, item);
            }
        }

        ReorderResources(column);
    }

    private void MoveDown(ListBox column)
    {
        int[] selection = column.GetSelectedIndices();

        Array.Reverse(selection);
        for (int i = 0; i < selection.Length; i++)
        {
            int selected = selection[i];
            if (selected < column.Items.Count - (i + 1))
            {
                ListItem item = column.Items[selected];
                column.Items.RemoveAt(selected);
                column.Items.Insert(selected + 1, item);
            }
        }

        ReorderResources(column);
    }

    private void MoveTop(ListBox column)
    {
        int[] selection = column.GetSelectedIndices();
        ListItem[] selectedItems = new ListItem[selection.Length];
        for (int i = 0; i < selection.Length; i++)
            selectedItems[i] = column.Items[selection[i]];

        for (int i = selection.Length - 1; i >= 0; i--)
            column.Items.RemoveAt(selection[i]);

        for (int i = 0; i < selectedItems.Length; i++)
            column.Items.Insert(i, selectedItems[i]);

        ReorderResources(column);
    }

    private void MoveBottom(ListBox column)
    {
        int[] selection = column.GetSelectedIndices();
        ListItem[] selectedItems = new ListItem[selection.Length];
        for (int i = 0; i < selection.Length; i++)
            selectedItems[i] = column.Items[selection[i]];

        for (int i = selection.Length - 1; i >= 0; i--)
            column.Items.RemoveAt(selection[i]);

        column.Items.AddRange(selectedItems);

        ReorderResources(column);
    }

    private void Delete(ListBox column)
    {
        int[] selection = column.GetSelectedIndices();

        for (int i = 0; i < selection.Length; i++)
            ResourceManager.DeleteById(Convert.ToInt32(column.Items[selection[i]].Value));

        for (int i = selection.Length - 1; i >= 0; i--)
            column.Items.RemoveAt(selection[i]);

        ReorderResources(column);
    }

    private void Add(int column)
    {
        ViewManager.UpdateView(this.View);//This will force a view creation if we are working with a new view.

        string returnUrl = UrlHelper.UrlAppendUpdateParameter(Page.Request.Url.ToString(), "ViewID",
                                                                 this.View.ViewID.ToString()); //This will make sure we go back to the right view.
        ReferrerRedirectorBase.SetReturnUrl(returnUrl);
        Response.Redirect(string.Format("AddResources.aspx?ViewID={0}&Column={1}&ReturnTo={2}", this.View.ViewID, column, ReferrerRedirectorBase.GetReturnUrl()));
    }

    private void CloneResources(ListBox column)
    {
        int[] selection = column.GetSelectedIndices();
        ListItem[] selectedItems = new ListItem[selection.Length];
        for (int i = 0; i < selection.Length; i++)
            selectedItems[i] = column.Items[selection[i]];

        Array.Reverse(selection);
        Array.Reverse(selectedItems);

        for (int i = 0; i < selection.Length; i++)
        {
            ResourceInfo info = this.AllResources.FindByID(Int32.Parse(selectedItems[i].Value));
            ResourceInfo newInfo = (ResourceInfo)info.Clone();
            column.Items.Insert(selection[i] + 1, new ListItem(newInfo.Title, newInfo.ID.ToString()));
        }

        ReorderResources(column);
    }

    private void LateralMove(ListBox sourceColumn, ListBox destColumn, int destColumnIndex)
    {
        int[] selection = sourceColumn.GetSelectedIndices();
        Array.Reverse(selection);

        if (selection.Length > 0)
        {
            foreach (ListItem item in destColumn.Items)
                item.Selected = false;
        }

        for (int i = 0; i < selection.Length; i++)
        {
            ListItem item = sourceColumn.Items[selection[i]];
            ResourceManager.SetPositionById((destColumn.Items.Count + 1), destColumnIndex, Convert.ToInt32(item.Value));
            sourceColumn.Items.Remove(item);
            destColumn.Items.Add(item);
        }
    }

    private void MoveTab(ListBox column)
    {
        int destinationViewID;
        int destinationColumnNum;

        if (int.TryParse(selectedViewID.Value, out destinationViewID) && int.TryParse(selectedColumn.Value, out destinationColumnNum))
        {
            int[] selection = column.GetSelectedIndices();
            var itemsToRemove = new List<ListItem>();
            foreach (int index in selection)
            {
                ListItem item = column.Items[index];
                int resourceId = int.Parse(item.Value);
                //put moved resources to bottom of the new column
                ResourceManager.MoveToView(destinationColumnNum, resourceId, destinationViewID);
                itemsToRemove.Add(item);
            }
            foreach (var item in itemsToRemove)
            {
                column.Items.Remove(item);
            }
            ReorderResources(column); //reorder the source column
            RefreshPage();
        }
    }

    #region Button Event Handlers
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnAdd1))
            Add(1);
        else if (sender.Equals(this.btnAdd2))
            Add(2);
        else if (sender.Equals(this.btnAdd3))
            Add(3);
        else if (sender.Equals(this.btnAdd4))
            Add(4);
        else if (sender.Equals(this.btnAdd5))
            Add(5);
        else if (sender.Equals(this.btnAdd6))
            Add(6);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnDelete1))
            Delete(lbxColumn1);
        else if (sender.Equals(this.btnDelete2))
            Delete(lbxColumn2);
        else if (sender.Equals(this.btnDelete3))
            Delete(lbxColumn3);
        else if (sender.Equals(this.btnDelete4))
            Delete(lbxColumn4);
        else if (sender.Equals(this.btnDelete5))
            Delete(lbxColumn5);
        else if (sender.Equals(this.btnDelete6))
            Delete(lbxColumn6);
    }

    protected void btnClone_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnClone1))
            CloneResources(lbxColumn1);
        else if (sender.Equals(this.btnClone2))
            CloneResources(lbxColumn2);
        else if (sender.Equals(this.btnClone3))
            CloneResources(lbxColumn3);
        else if (sender.Equals(this.btnClone4))
            CloneResources(lbxColumn4);
        else if (sender.Equals(this.btnClone5))
            CloneResources(lbxColumn5);
        else if (sender.Equals(this.btnClone6))
            CloneResources(lbxColumn6);
    }

    protected void btnMoveTab_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnMoveTab))
            MoveTab(this.lbxColumn1);
        else if (sender.Equals(this.btnMoveTab2))
            MoveTab(this.lbxColumn2);
        else if (sender.Equals(this.btnMoveTab3))
            MoveTab(this.lbxColumn3);
        else if (sender.Equals(this.btnMoveTab4))
            MoveTab(this.lbxColumn4);
        else if (sender.Equals(this.btnMoveTab5))
            MoveTab(this.lbxColumn5);
        else if (sender.Equals(this.btnMoveTab6))
            MoveTab(this.lbxColumn6);
    }

    protected void btnMoveTop_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnMoveTop1))
            MoveTop(this.lbxColumn1);
        else if (sender.Equals(this.btnMoveTop2))
            MoveTop(this.lbxColumn2);
        else if (sender.Equals(this.btnMoveTop3))
            MoveTop(this.lbxColumn3);
        else if (sender.Equals(this.btnMoveTop4))
            MoveTop(this.lbxColumn4);
        else if (sender.Equals(this.btnMoveTop5))
            MoveTop(this.lbxColumn5);
        else if (sender.Equals(this.btnMoveTop6))
            MoveTop(this.lbxColumn6);
    }

    protected void btnMoveUp_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnMoveUp1))
            MoveUp(this.lbxColumn1);
        else if (sender.Equals(this.btnMoveUp2))
            MoveUp(this.lbxColumn2);
        else if (sender.Equals(this.btnMoveUp3))
            MoveUp(this.lbxColumn3);
        else if (sender.Equals(this.btnMoveUp4))
            MoveUp(this.lbxColumn4);
        else if (sender.Equals(this.btnMoveUp5))
            MoveUp(this.lbxColumn5);
        else if (sender.Equals(this.btnMoveUp6))
            MoveUp(this.lbxColumn6);
    }

    protected void btnMoveDown_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnMoveDown1))
            MoveDown(this.lbxColumn1);
        else if (sender.Equals(this.btnMoveDown2))
            MoveDown(this.lbxColumn2);
        else if (sender.Equals(this.btnMoveDown3))
            MoveDown(this.lbxColumn3);
        else if (sender.Equals(this.btnMoveDown4))
            MoveDown(this.lbxColumn4);
        else if (sender.Equals(this.btnMoveDown5))
            MoveDown(this.lbxColumn5);
        else if (sender.Equals(this.btnMoveDown6))
            MoveDown(this.lbxColumn6);
    }

    protected void btnMoveBottom_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnMoveBottom1))
            MoveBottom(this.lbxColumn1);
        else if (sender.Equals(this.btnMoveBottom2))
            MoveBottom(this.lbxColumn2);
        else if (sender.Equals(this.btnMoveBottom3))
            MoveBottom(this.lbxColumn3);
        else if (sender.Equals(this.btnMoveBottom4))
            MoveBottom(this.lbxColumn4);
        else if (sender.Equals(this.btnMoveBottom5))
            MoveBottom(this.lbxColumn5);
        else if (sender.Equals(this.btnMoveBottom6))
            MoveBottom(this.lbxColumn6);
    }

    protected void LateralLeftMove_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnMoveLeft1))
            LateralMove(this.lbxColumn2, this.lbxColumn1, 1);
        else if (sender.Equals(this.btnMoveLeft2))
            LateralMove(this.lbxColumn3, this.lbxColumn2, 2);
        else if (sender.Equals(this.btnMoveLeft3))
            LateralMove(this.lbxColumn4, this.lbxColumn3, 3);
        else if (sender.Equals(this.btnMoveLeft4))
            LateralMove(this.lbxColumn5, this.lbxColumn4, 4);
        else if (sender.Equals(this.btnMoveLeft5))
            LateralMove(this.lbxColumn6, this.lbxColumn5, 5);
    }

    protected void LateralRightMove_Click(object sender, EventArgs e)
    {
        if (sender.Equals(this.btnMoveRight1))
            LateralMove(this.lbxColumn1, this.lbxColumn2, 2);
        else if (sender.Equals(this.btnMoveRight2))
            LateralMove(this.lbxColumn2, this.lbxColumn3, 3);
        else if (sender.Equals(this.btnMoveRight3))
            LateralMove(this.lbxColumn3, this.lbxColumn4, 4);
        else if (sender.Equals(this.btnMoveRight4))
            LateralMove(this.lbxColumn4, this.lbxColumn5, 5);
        else if (sender.Equals(this.btnMoveRight5))
            LateralMove(this.lbxColumn5, this.lbxColumn6, 6);
    }
    #endregion

    protected void AddColumn(object sender, EventArgs e)
    {
        Page.Validate();
        if (!Page.IsValid)
        {
            return;
        }
        bool isNewView = View.ViewID == 0;
        View.ColumnCount = View.ColumnCount + 1;
        switch (View.ColumnCount)
        {
            case 2:
                View.Column3Width = 500;
                break;
            case 3:
                View.Column3Width = 450;
                break;
            case 4:
                View.Column4Width = 450;
                break;
            case 5:
                View.Column5Width = 450;
                break;
            case 6:
                View.Column6Width = 450;
                break;
        }
        ViewManager.UpdateView(View);

        if (isNewView)
        {
            RefreshPage();
        }
        else
        {
            RebuildUI();
        }
    }

    protected void UpdateColumnWidth(int deletedColumn, int totalColumns)
    {
        if (totalColumns > 1 && deletedColumn == 1)
            this.View.Column1Width = this.View.Column2Width;

        if (totalColumns > 2 && deletedColumn <= 2)
            this.View.Column2Width = this.View.Column3Width;

        if (totalColumns > 3 && deletedColumn <= 3)
            this.View.Column3Width = this.View.Column4Width;

        if (totalColumns > 4 && deletedColumn <= 4)
            this.View.Column4Width = this.View.Column5Width;

        if (totalColumns > 5 && deletedColumn <= 5)
            this.View.Column5Width = this.View.Column6Width;
    }

    public void UpdateColumnsWidth()
    {
        if (this._isColumnWidthChanged)
        {
            int tmp;

            if (this.View.ColumnCount > 0 && Int32.TryParse(this.col1Width.Text, out tmp))
            {
                this.View.Column1Width = tmp;
            }

            if (this.View.ColumnCount > 1 && Int32.TryParse(this.col2Width.Text, out tmp))
            {
                this.View.Column2Width = tmp;
            }

            if (this.View.ColumnCount > 2 && Int32.TryParse(this.col3Width.Text, out tmp))
            {
                this.View.Column3Width = tmp;
            }

            if (this.View.ColumnCount > 3 && Int32.TryParse(this.col4Width.Text, out tmp))
            {
                this.View.Column4Width = tmp;
            }

            if (this.View.ColumnCount > 4 && Int32.TryParse(this.col5Width.Text, out tmp))
            {
                this.View.Column5Width = tmp;
            }

            if (this.View.ColumnCount > 5 && Int32.TryParse(this.col6Width.Text, out tmp))
            {
                this.View.Column6Width = tmp;
            }
            ViewManager.UpdateView(this.View);
        }
    }

    protected void btnDeleteCol_Click(object sender, CommandEventArgs e)
    {
        try
        {
            if (!Page.IsValid)
            {
                return;
            }
            int deletedCol = int.Parse((string)e.CommandArgument);
            var viewId = View.ViewID;
            if (viewId > 0)
                ViewManager.RemoveColumnFromView(View.ViewID, View.ColumnCount, deletedCol);
            View.ColumnCount = View.ColumnCount - 1;
            ViewManager.UpdateView(View);

            if (viewId == 0)
            {
                RefreshPage();
            }
            else
            {
                this.AllResources.RemoveAll(r => r.Column == deletedCol);
                foreach (ResourceInfo info in this.AllResources)
                    if (info.Column > deletedCol)
                        info.Column -= 1;
                RebuildUI();
            }
        }
        catch (Exception ex)
        {
            _log.Error(string.Format("Delete column for viewID '{0}' has failed.", View.ViewID), ex);
            throw;
        }
    }
}
