<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserWebViews.ascx.cs" Inherits="Orion_Admin_UserWebViews" %>
<%@ Register TagPrefix="orion" TagName="yesnocontrol" Src="~/Orion/Admin/YesNoControl.ascx" %>

<asp:Repeater runat="server" ID="rptWebViews">
    <ItemTemplate>
        <tr>
            <td>
                <input type="checkbox" runat="server" id="chbxWebViewVisible" onclick="togleRowState(this);" />
            </td>
            <td><%# DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.EditAccount_ShowMenu_Label, Eval("DefaultTitle"))) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynViewMenu" />
                <asp:HiddenField runat="server" ID="ViewID" Value='<%# DefaultSanitizer.SanitizeHtml(Eval("ID")) %>' />
            </td>
            <td>&nbsp;</td>
        </tr>
    </ItemTemplate>
</asp:Repeater>
