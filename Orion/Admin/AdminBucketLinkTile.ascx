<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminBucketLinkTile.ascx.cs" Inherits="Orion_Admin_AdminBucketLinkTile" %>
<%@ Import Namespace="System.Drawing" %>


<div class="AdminBucketLinkTile">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="">
        <tr>
            <td width="36" height="100%">
                <img class="BucketIcon" src="<%= DefaultSanitizer.SanitizeHtml(Icon) %>"/>
            </td>
            <td>
                <div class="AdminBucketLinkTileHeader"><%= DefaultSanitizer.SanitizeHtml(Header) %></div>
            </td>
        </tr>
        <tr>
            <td/>
            <td>
                <asp:Repeater ID="primaryButtons" runat="server">
                    <HeaderTemplate>
                        <div class="SpecialSettingsLinkContainer">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink class="sw-btn-primary sw-btn" NavigateUrl="<%# DefaultSanitizer.SanitizeHtml(((LinkModel) Container.DataItem).Href) %>" runat="server" id="href"><%# DefaultSanitizer.SanitizeHtml(((LinkModel) Container.DataItem).Label) %></asp:HyperLink>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td/>
            <td>
                <asp:Repeater ID="links" runat="server" OnItemDataBound="links_OnItemDataBound">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="normalLink" runat="server">
                            <p>
                                <span class="LinkArrow">&#0187;</span>
                                <asp:HyperLink NavigateUrl="<%# DefaultSanitizer.SanitizeHtml(((LinkModel) Container.DataItem).Href) %>"
                                               runat="server" id="href">
                                    <%# DefaultSanitizer.SanitizeHtml(((LinkModel) Container.DataItem).Label) %>
                                </asp:HyperLink>
                            </p>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="summarylink" runat="server" visible="false">

                            <asp:HyperLink CssClass="sw-btn-secondary sw-btn TileSummaryLink" NavigateUrl="<%# DefaultSanitizer.SanitizeHtml(((LinkModel) Container.DataItem).Href) %>"
                                           runat="server" id="HyperLink1">
                                <%# DefaultSanitizer.SanitizeHtml(((LinkModel) Container.DataItem).Label) %>
                            </asp:HyperLink>
                        </asp:PlaceHolder>


                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</div>