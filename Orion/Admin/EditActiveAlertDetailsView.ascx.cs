﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_EditActiveAlertDetailsView : ProfilePropEditUserControl
{
    public override string PropertyValue
    {
        get
        {
            return this.lbxActiveAlertDetails.SelectedValue;
        }
        set
        {
            ListItem item = this.lbxActiveAlertDetails.Items.FindByValue(value);
            if (null != item)
            {
                item.Selected = true;
            }
            else
            {
                // default to "by device type"
                this.lbxActiveAlertDetails.Items.FindByValue("-1").Selected = true;
            }
        }
    }
        
     protected override void OnInit(EventArgs e)
     {
        foreach (ViewInfo info in ViewManager.GetViewsByType("ActiveAlertDetails"))
        {
            ListItem item = new ListItem(info.ViewTitle, info.ViewID.ToString());
            this.lbxActiveAlertDetails.Items.Add(item);
        }

        base.OnInit(e);
     }
}