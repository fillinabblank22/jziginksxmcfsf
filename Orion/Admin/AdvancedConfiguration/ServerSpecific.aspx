<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ServerSpecific.aspx.cs" Inherits="Orion_Admin_AdvancedConfiguration_ServerSpecific" 
    MasterPageFile="~/Orion/Admin/AdvancedConfiguration/AdvancedConfiguration.master" Title="<%$ HtmlEncodedResources: CoreWebContent, AdvancedConfiguration_Title %>"%>

<%@ Register Src="~/Orion/Admin/AdvancedConfiguration/SettingsTable.ascx" TagPrefix="orion" TagName="SettingsTable" %>
<%@ MasterType VirtualPath="~/Orion/Admin/AdvancedConfiguration/AdvancedConfiguration.master"%>

<asp:Content runat="server" ContentPlaceHolderID="settingsContentPlaceholder">
    <orion:Include ID="SettingFilterCssInclude" runat="server" File="Admin/AdvancedConfiguration/styles/SettingsFilter.css"/>
    <orion:Include ID="SettingFilterJsInclude" runat="server" File="Admin/AdvancedConfiguration/js/SettingsFilter.js"/>

    <div class="filterDiv">
        <div class="ddlDiv">
            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AdvancedSettingsServerFilterLabel) %></span>
            <asp:DropDownList runat="server" ID="orionServerDDL" CssClass="ddlStyle"/>
            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AdvancedSettingsSettingFilterLabel) %></span>
        </div>
        <div class="searchDiv">
            <div class="inputDiv">
                <input id="searchText" class="searchInput"/>
				<asp:TextBox id="searchTextCache" class="searchInputCache" runat="server"/>
            </div>
            <div id="searchImgDiv" class="searchImgDiv">
                <span id="searchImg" class="searchImage"></span>
            </div>
        </div>
    </div>
    <orion:SettingsTable runat="server" ID="SettingsTable" DisplayMode="ServerSpecific"/>
</asp:Content>
