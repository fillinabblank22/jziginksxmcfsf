﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controllers;

public partial class Orion_Admin_AdvancedConfiguration_AdvancedConfiguration : System.Web.UI.MasterPage
{
    private JavaScriptSerializer serializer = new JavaScriptSerializer();

    public IList<OrionWindowsService> ServicesToRestart { get; set; }

    public List<string> NotSyncronizedServersNames
    {
        get
        {
            return new ServerSyncInfoController().GetNotSyncronizedServersNames();
        }
    }

    public string GetSerializedData(object data)
    {
        return serializer.Serialize(data);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnResetAllToDefault.Text = Page.AppRelativeVirtualPath
            .Equals("~/Orion/Admin/AdvancedConfiguration/Global.aspx", StringComparison.OrdinalIgnoreCase)
            ? Resources.CoreWebContent.AdvancedConfigurationResetAllBtn
            : Resources.CoreWebContent.AdvancedConfigurationUseGlobalForAllBtn;
    }
}
