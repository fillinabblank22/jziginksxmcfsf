﻿using System;

public partial class Orion_Admin_AdvancedConfiguration_Global : System.Web.UI.Page
{
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        (Master).ServicesToRestart = SettingsTable.ServicesToRestart;
    }
}
