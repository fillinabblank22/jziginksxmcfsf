﻿(function() {
    $(document).keypress(function(e) {
        if (e.which === 13) {
            $("#searchImgDiv").trigger("click", e.keyCode);
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode === 27) {
            $("#searchImgDiv").trigger("click", e.keyCode);
        }
    });

    $(document).ready(function() {
        var searchImgDiv = $("#searchImgDiv");
        var searchText = $("#searchText");
        var searchTextCache = $("[id$='searchTextCache']");
        var searchImage = $("#searchImg");
        var settingsTable = $(".centralizedSettingTable");
        var orionServerDdl = $(".ddlStyle");
        var defaultDdlValue = "0";
        var searchDisabledStateChar = "\u0020";
        var strictSearchFormat = "\"{0}\"";
        var isStrictSearchRegex = /"([^"]*)"/g;

        function getStrictSearchValue(searchTextVal) {
            var matches = searchTextVal.match(isStrictSearchRegex);
            return matches && matches.length === 1
                ? matches[0].replace(/"/g, "")
                : null;
        }

        function isEmptyOrSpaces(str) {
            return str === null || str == undefined || str.match(/^ *$/) !== null;
        }

        var showSettingTable = function() {
            settingsTable.find("tr").each(function () {
                $(this).show();
            });
        }

        var filterSettingTable = function(orionServerDdlValue, searchTextVal, filterByOrionServerOnly) {
            var settingNameSelector = searchTextVal.indexOf(".") === -1
                ? "td.shortNameCell"
                : "td.fullNameCell";

            $((settingsTable.find("tr").toArray()).reverse()).each(function () {
                var tdText = $(this)
                    .find(settingNameSelector)
                    .text();
                var orionServerId = $(this).find("td.orionServerIdCell").text();
                var condition;
                
                if (filterByOrionServerOnly) {
                    condition = orionServerId !== orionServerDdlValue;
                } else {
                    var strictSearchValue = getStrictSearchValue(searchTextVal);
                    var internalCondition = strictSearchValue != null
                        ? tdText.toLowerCase() !== strictSearchValue.toLowerCase()
                        : tdText.toLowerCase().indexOf(searchTextVal.toLowerCase()) < 0;
                    condition = orionServerDdlValue !== defaultDdlValue ?
                    (internalCondition || orionServerId !== orionServerDdlValue) : internalCondition;
                }

                if (condition) {
                    if ($(this).hasClass("mainRow")) {
                        $(this).hide();
                    } else if ($(this).hasClass("groupRow")) {
                        var tr = $(this).nextAll(":visible:first");
                        if (!tr.length || tr.hasClass("groupRow")) {
                            $(this).hide();
                        }
                    }
                }
            });
        }

        var adjustZebra = function() {
            $("tr:visible:odd").css("background-color", "white");
            $("tr:visible:even").css("background-color", "#f9f9f9");
        }

        var getUrlVars = function() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split("=");
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var regainFilterState = function () {
            var selectedVal = orionServerDdl.val();
            var filterByOrionServer = selectedVal !== defaultDdlValue;

            if (filterByOrionServer) {
                filterSettingTable(selectedVal, "", true);
            }

            if (searchTextCache.val() !== searchDisabledStateChar) {
                if (!isEmptyOrSpaces(searchTextCache.val())) {
                    searchText.val(searchTextCache.val());
                    searchImgDiv.click();
                    return;
                } else {
                    var settingNameFromQueryString = getUrlVars()["settingName"];
                    if (!isEmptyOrSpaces(settingNameFromQueryString)) {
                        searchText.val(String.format(strictSearchFormat, settingNameFromQueryString));
                        searchImgDiv.click();
                        return;
                    }
                }
            }

            if (filterByOrionServer) {
                adjustZebra();
            }
        }

        searchImgDiv.on("click", function (eventArgs, keyCode) {
            var searchTextVal = searchText.val();
            var orionServersDdlValue = orionServerDdl.val();

            if (searchImgDiv.attr("search") === "on" && (!keyCode || keyCode === 27 || isEmptyOrSpaces(searchTextVal))) {
                searchImage.css("background-image", "url('/Orion/images/Button.SearchIcon.gif')");
                searchImgDiv.attr("search", "off");
                searchText.val("");
                searchTextCache.val(searchDisabledStateChar);

                orionServerDdl.val(orionServersDdlValue).trigger("change");

            } else if (!isEmptyOrSpaces(searchTextVal) && (!keyCode || keyCode === 13)) {
                searchImage.css("background-image", "url('/Orion/images/Button.SearchCancel.gif')");
                searchImgDiv.attr("search", "on");
                searchTextCache.val(searchTextVal);

                showSettingTable();
                filterSettingTable(orionServersDdlValue, searchTextVal, false);
            }

            adjustZebra();
        });

        orionServerDdl.on("change", function () {
            var selectedVal = $(this).val();

            if (searchImgDiv.attr("search") === "on") {
                searchImgDiv.click();
            }
            if (!isEmptyOrSpaces(searchText.val())) {
                searchText.val("");
            }

            showSettingTable();

            if (selectedVal !== defaultDdlValue) {
                filterSettingTable(selectedVal, "", true);
            }

            adjustZebra();
        });
        
        regainFilterState();
    });
})(window);
