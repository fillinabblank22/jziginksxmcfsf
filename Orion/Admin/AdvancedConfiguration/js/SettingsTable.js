﻿(function () {
    $.fn.exists = function () { return this.length > 0; }

    $.tasksQueue = {
        _timer: null,
        _queue: [],
        add: function (fn, context, time) {
            var setTimer = function(time) {
                $.tasksQueue._timer = setTimeout(function() {
                        time = $.tasksQueue.add();
                        if ($.tasksQueue._queue.length) {
                            setTimer(time);
                        }
                    },
                    time || 2);
            };

            if (fn) {
                $.tasksQueue._queue.push([fn, context, time]);
                if ($.tasksQueue._queue.length === 1) {
                    setTimer(time);
                }
                return null;
            }

            var next = $.tasksQueue._queue.shift();
            if (!next) {
                return 0;
            }
            next[0].call(next[1] || window);
            return next[2];
        },
        clear: function () {
            clearTimeout($.tasksQueue._timer);
            $.tasksQueue._queue = [];
        }
    };

    var isPreparingSettings = false;

    function compareWithEditorValue(editor, valueToCompare) {
        return editor.is("input:checkbox")
            ? editor.prop("checked").toString().toLowerCase() === valueToCompare.toLowerCase()
            : editor.is("select")
            ? editor.val().toLowerCase() === valueToCompare.toLowerCase()
            : editor.val() === valueToCompare;
    }

    function handleChange(initialValue,
        defaultValue,
        restartWarning,
        valueEditor,
        isChangedFlag,
        extraChangeCondition) {

        if (compareWithEditorValue(valueEditor, initialValue) && !extraChangeCondition) {
            restartWarning.css("visibility", "hidden");
            isChangedFlag.prop("checked", false);
        } else {
            restartWarning.css("visibility", "visible");
            isChangedFlag.prop("checked", true);
        }

        var attr = restartWarning.attr("restartNeeded");
        if (typeof attr !== typeof undefined && attr !== false) {
            restartWarning.css("visibility", "visible");
        }

        if (valueEditor.hasClass("advancedTextBox")) {
            SW.Core.AdvancedTextBox.CheckForOverflow(valueEditor.attr("id"));
        }

        if (isPreparingSettings) return;

        var isChangesOccurred = $(".isChangedFlag input:checkbox:checked").exists();

        $("[id$='btnCancel']").text(isChangesOccurred
            ? "@{R=Core.Strings.2;K=WEBJS_OM0_6;E=js}"
            : "@{R=Core.Strings.2;K=WEBJS_PCO_005;E=js}");
    }

    function validationReset(controlId) {
        var validators = window.Page_Validators;

        if (typeof (validators) == "undefined") {
            return;
        }

        if (typeof (controlId) != "undefined") {
            validators = validators.filter(function (v) {
                return v.controltovalidate === controlId;
            });
        }

        for (var i = 0; i < validators.length; i++) {
            var validator = validators[i];
            validator.isvalid = true;
            window.ValidatorUpdateDisplay(validator);
        }
    }

    function setSettingsTableHeight() {
        var heightOffset = $("[id$='DynamicSettingsTablePanel']").offset().top +
            $(".sw-btn-bar-wizard").outerHeight(true) +
            $("#footermark").outerHeight(true) +
            $("#footer").outerHeight(true);

        $("[id$='DynamicSettingsTablePanel']").height($(window).height() - heightOffset);
    }

    function setEditorValue(editor, value) {
        var matchingValue = editor
            .find('option')
            .filter(function () { return this.value.toLowerCase() === value.toLowerCase(); })
            .attr('value');

        editor.val(matchingValue ? matchingValue : value);
        editor.prop("checked", value.toLowerCase() === "true");
    }

    function prepareRowForGlobal(mainRow,
        valueEditor,
        isResetFlag,
        defaultValue,
        changeEvent,
        handleChangeFunc) {

        var resetLink = mainRow.find(".resetLink");

        var handleChangeOnGlobalFunc = function() {
            handleChangeFunc();

            if (compareWithEditorValue(valueEditor, defaultValue)) {
                resetLink.hide();
                isResetFlag.prop("checked", true);
            } else {
                resetLink.show();
                isResetFlag.prop("checked", false);
            }
        };

        valueEditor.on(changeEvent, function () {
            handleChangeOnGlobalFunc();
        });

        resetLink.on("click", function () {
            setEditorValue(valueEditor, defaultValue);

            handleChangeOnGlobalFunc();

            validationReset(valueEditor.attr("id"));
        });

        handleChangeOnGlobalFunc();
    }

    function prepareRowForServerSpecific(mainRow,
        valueEditor,
        isResetFlag,
        isDefault,
        textBox,
        restartWarning,
        initialValue,
        defaultValue,
        changeEvent,
        handleChangeFunc) {

        var useGlobalLink = mainRow.find(".useGlobalLink");
        var overrideButton = mainRow.find(".overrideButton");

        if (textBox.hasClass("advancedTextBox")) {
            /* 
                We have to add IDs of every custom control inside __enabledControlArray generated by asp.net 
                to support post back feature when controls are in disabled state. Build-in controls IDs are already there.
                It works together with 'Form.SubmitDisabledControls = true;' defined inside 
                ServerSpecific.aspx.cs page code behind on Page_Init event handler.
            */
            __enabledControlArray.push(textBox.attr("id"));
        }

        var setEditorStateFunc = function(toGlobal) {
            if (toGlobal) {
                useGlobalLink.hide();
                overrideButton.insertBefore(restartWarning);
                overrideButton.css("visibility", "visible");
                valueEditor.prop("disabled", true);
            } else {
                useGlobalLink.show();
                restartWarning.insertBefore(overrideButton);
                overrideButton.css("visibility", "hidden");
                valueEditor.prop("disabled", false);
            }
        };

        valueEditor.on(changeEvent, function () {
            var extraChangeCondition =
            (isDefault && !isResetFlag.prop("checked")) ||
            (!isDefault && isResetFlag.prop("checked"));

            handleChangeFunc(extraChangeCondition);
        });

        useGlobalLink.on("click", function () {
            setEditorStateFunc(true);
            setEditorValue(valueEditor, defaultValue);

            if (!isDefault) {
                isResetFlag.prop("checked", true);
            }

            handleChangeFunc(!isDefault);

            validationReset(valueEditor.attr("id"));
        });

        overrideButton.on("click", function () {
            setEditorStateFunc(false);
            setEditorValue(valueEditor, initialValue);

            isResetFlag.prop("checked", false);

            handleChangeFunc(isDefault);
        });

        handleChangeFunc();
        setEditorStateFunc(isDefault);
    }

    function prepareRow(rowItem, isServerSpecificMode) {
        var mainRow = $(rowItem);
        var textBox = mainRow.find("input[type='text'][class*='valueEditor']");
        var checkBox = mainRow.find("span.valueEditor input[type='checkbox']");
        var dropDownBox = mainRow.find("select.valueEditor");
        var restartWarning = mainRow.find(".serviceRestartWarningBox");

        var isChangedFlag = mainRow.find(".isChangedFlag input:checkbox");
        var isResetFlag = mainRow.find(".isResetFlag input:checkbox");
        var isDefault = mainRow.find(".isDefaultCell").text().toLowerCase() === "true";

        var initialValue = mainRow.find(".initialValueCell").text();
        var defaultValue = mainRow.find(".defaultValueCell").text();

        var changeEvent;
        var valueEditor;

        if (textBox.exists()) {
            changeEvent = "input";
            valueEditor = textBox;
        } else if(checkBox.exists()) {
            changeEvent = "click";
            valueEditor = checkBox;
        } else if (dropDownBox.exists()) {
            changeEvent = "change";
            valueEditor = dropDownBox;
        }

        var handleChangeFunc = function (extraChangeCondition) {
            handleChange(initialValue,
                defaultValue,
                restartWarning,
                valueEditor,
                isChangedFlag,
                extraChangeCondition);
        };

        if (isServerSpecificMode) {
            prepareRowForServerSpecific(mainRow,
                valueEditor,
                isResetFlag,
                isDefault,
                textBox,
                restartWarning,
                initialValue,
                defaultValue,
                changeEvent,
                handleChangeFunc);
        } else {
            prepareRowForGlobal(mainRow,
                valueEditor,
                isResetFlag,
                defaultValue,
                changeEvent,
                handleChangeFunc);
        }

        restartWarning.mouseover(function (e) {
            var x = String.format("{0}px", e.clientX + 10),
                y = String.format("{0}px", e.clientY + 20);

            var tooltip = $(this).children(".tooltip");

            tooltip[0].style.left = x;
            tooltip[0].style.top = y;
        });
    }

    function showWaitOverlay() {
        $(".centralizedSettingPanel").addClass("disabledPanel");
        $(".waitText").show();

        isPreparingSettings = true;
    }

    function hideWaitOverlay() {
        $(".waitText").hide();
        $(".centralizedSettingPanel").removeClass("disabledPanel");
        $(".centralizedSettingTable").css("opacity", "1");

        isPreparingSettings = false;
    }

    $(document).ready(function () {
        setSettingsTableHeight();

        $(window).resize(setSettingsTableHeight);

        showWaitOverlay();

        var centralizedSettingTable = $(".centralizedSettingTable");
        var mainRows = centralizedSettingTable.find(".mainRow");
        var isServerSpecificMode = centralizedSettingTable.hasClass("serverspecific");

        $.each(mainRows, function (index) {
            $.tasksQueue.add(function () {
                prepareRow(this, isServerSpecificMode);
                if (index === mainRows.length - 1) {
                    $("[id$='btnResetAllToDefault']").click(function () {
                        $(".resetLink:visible").click();
                        $(".useGlobalLink:visible").click();
                        validationReset();
                    });

                    hideWaitOverlay();
                }
            }, this);
        });
    });
})(window);
