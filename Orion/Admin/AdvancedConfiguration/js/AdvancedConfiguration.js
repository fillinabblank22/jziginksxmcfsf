﻿(function(Core) {
    var AdvancedConfiguration = Core.AdvancedConfiguration = Core.AdvancedConfiguration || {};

    var servicesToRestart = [],
        failedToRestartServices = [],

        nonRespondingServers = [],
        settingsSyncCheckTimes = 0;

    function hideSettingsWarningLabels(serviceNames) {
        $.each($(".serviceRestartWarningBox").filter(
                function() {
                    var attr = $(this).attr("restartNeeded");
                    return typeof attr !== typeof undefined && attr !== false;
                }),
            function(index, element) {
                if (!serviceNames.some(function(serviceName) {
                    return $(element).prop("title").indexOf(serviceName.serviceDisplayName) > -1;
                })) {
                    $(element).css("visibility", "hidden");
                    $(element).removeAttr("restartNeeded");
                    $(element).find(".isRestartedFlag input").prop("checked", "true");
                }
            });
    };

    function getWindowsServiceByName(statusResult) {
        for (var i = 0; i < servicesToRestart.length; i++) {
            if (servicesToRestart[i].Name === statusResult.serviceName &&
                servicesToRestart[i].OrionServerId == statusResult.serverId) {
                return servicesToRestart[i];
            }
        }

        return null;
    };

    function alreadyContains(server) {
        var exists = nonRespondingServers.some(function (item) {
            return item === server;
        });

        return exists;
    };

    function addToNonRespondingServers(serversNames) {
        for (var i = 0; i < serversNames.length; i++) {
            var found = alreadyContains(serversNames[i]);

            if (!found) { nonRespondingServers.push(serversNames[i]); }  
        }
    };

    function fitPageContent() {
        $(window).trigger("resize");
    };

    function removeServicesList(id) {
        $(id).children().remove();
    };

    function checkContainerVisibility(servicesList, messageContainer) {
        if (typeof (servicesList) === "undefined" || servicesList.length === 0) {
            messageContainer.hide();
            fitPageContent();

            return false;
        }
        return true;
    };

    function showServices(services) {
        if (!checkContainerVisibility(services, $("#CentrSettingsWarningMessageID"))) {
            return;
        }

        var listContainer = $("#CentrSettingsServicesToRestartID");

        var list = $("<ul />");

        $.each(services, function(i) {
            var li = $("<li/>")
                .appendTo(list);

            var spanTable = $("<span/>")
                .css("display", "table")
                .css("width", "100%")
                .css("margin-top", "-20px")
                .css("margin-left", "20px")
                .appendTo(li);

            $("<span/>")
                .text(services[i].text)
                .css("display", "table-cell")
                .css("vertical-align", "middle")
                .appendTo(spanTable);

            if (services[i].restarting === true) {
                $("<span/>")
                    .text("@{R=Core.Strings.2;K=CentralizedSettingRestartLabel;E=js}")
                    .css("display", "table-cell")
                    .css("font-style", "italic")
                    .css("float", "right")
                    .appendTo(spanTable);
            }
        });
        list.appendTo(listContainer);

        fitPageContent();
    };

    function showErrorList(services) {
        var errorMessageContainer = $("#CentrSettingsErrorMessageID");

        if (!checkContainerVisibility(services, errorMessageContainer)) {
            return;
        }

        var errorListContainer = $("#CentrSettingsServicesWithErrorID");

        var errorList = $("<ul/>");

        $.each(services, function(i) {
            var li = $("<li/>")
                .appendTo(errorList);

            $("<span/>")
                .text(services[i].text)
                .appendTo(li);
        });
        errorList.appendTo(errorListContainer);
        errorMessageContainer.css("display", "block");

        $("#CentrSettingsRestartAllButtonID").css("visibility", "visible");

        fitPageContent();
    };

    function showNotSyncWarningMessage(servers) {
        var notSynchronizedMessageContainer = $("#CentrSettingsNotSynchronizedMessageID");
        if (!checkContainerVisibility(servers, notSynchronizedMessageContainer)) {
            return;
        }

        $("#CentrSettingsNotSynchronizedServersID")
            .text(String.format("@{R=Core.Strings.2;K=CentralizedSettingNotSynchronizedWarningMessage;E=js}", servers.join(", ")));

        notSynchronizedMessageContainer.show();
        fitPageContent();
    };

    function showNotRespondingErrorMessage(servers) {
        var notRespondingMessageContainer = $("#CentrSettingsNotRespondingErrorMessageID");
        if (!checkContainerVisibility(servers, notRespondingMessageContainer)) {
            return;
        }

        $("#CentrSettingsNotRespondingServersID")
            .text(String.format("@{R=Core.Strings.2;K=CentralizedSettingNotRespondingErrorMessage;E=js}", servers.join(", ")));

        notRespondingMessageContainer.show();
        fitPageContent();
    };

    function processStatuses(servicesWithStatuses) {
        servicesToRestart = servicesToRestart.concat(failedToRestartServices);
        failedToRestartServices = [];

        var restartedServiceNames = [];

        $.each(servicesWithStatuses, function (index, result) {
            var service = getWindowsServiceByName(result);

            switch (result.operationResultType) {
                case 0: // NoResult
                case 2: // Success
                    servicesToRestart = $.grep(servicesToRestart, function(item) {
                        return item.Name !== service.Name || item.OrionServerId !== service.OrionServerId;
                    });
                    restartedServiceNames.push(result.serviceName);
                    break;
                case 1: // WaitingForRestart
                    service.restarting = true;
                    break;
                case 3: // Timeout
                case 4: // Failure
                    servicesToRestart = $.grep(servicesToRestart, function (item) {
                        return item.Name !== service.Name || item.OrionServerId !== service.OrionServerId;
                    });
                    service.restarting = false;
                    failedToRestartServices.push(service);
                    break;
            }
        });

        removeServicesList("#CentrSettingsServicesToRestartID");
        removeServicesList("#CentrSettingsServicesWithErrorID");       

        showServices(failedToRestartServices.concat(servicesToRestart));
        showErrorList(failedToRestartServices);

        hideSettingsWarningLabels(restartedServiceNames);

        if (servicesToRestart.length > 0) {
            startServicesStatusCheck();
        } else if (failedToRestartServices.length > 0) {
            $("#CentrSettingsRestartAllButtonID").show();
        }
    };

    function processSyncInfo(serversNames) {
        $("#CentrSettingsNotSynchronizedServersID").empty();
        showNotSyncWarningMessage(serversNames);
    };

    function getServiceNamesToRestartPerServer() {
        var result = {};
        var allServices = servicesToRestart.concat(failedToRestartServices);

        $.each(allServices, function (index, service) {
            if (result[service.OrionServerId] === undefined) { 
                result[service.OrionServerId] = {};

                result[service.OrionServerId].IP = service.ServerIp;
                result[service.OrionServerId].ServerName = service.ServerName;

                result[service.OrionServerId].Services = [];
            }

            result[service.OrionServerId].Services.push(service.Name);
        });

        return result;
    };

    function startServicesStatusCheck() {
        var requests = [];
        var servicesStatuses = [];

        var services = getServiceNamesToRestartPerServer();

        $.each(services, function (serverId) {
            var request = $.ajax({
                url: "/Orion/Admin/AdvancedConfiguration/Services/OrionServicesManagementService.asmx/GetOperationResults",
                type: "POST",
                data: JSON.stringify({
                    serverHostname: services[serverId].ServerName
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    $.each(services[serverId].Services, function (index, serviceName) {
                        var operationResultType = result.d[serviceName];
                        if (operationResultType != undefined) {
                            servicesStatuses.push({
                                operationResultType: operationResultType,
                                serviceName: serviceName,
                                serverId: serverId
                            });
                        }
                    });
                }
            });
            requests.push(request);
        });

        $.when.apply($, requests).always(function () {
            processStatuses(servicesStatuses);
        });
    };

    function settingsSyncCheck() {
        settingsSyncCheckTimes++;

        SW.Core.Services.callController(
            "/api/ServerSyncInfo/GetNotSyncronizedServersNames",
            null,
            function (result) {
                processSyncInfo(result);
                if (result.length !== 0) {
                    if (settingsSyncCheckTimes <= 180) {
                        setTimeout(settingsSyncCheck, 1000);
                    } else {
                        settingsSyncCheckTimes = 0;
                        $("#CentrSettingsNotSynchronizedMessageID").hide();

                        addToNonRespondingServers(result);
                        showNotRespondingErrorMessage(nonRespondingServers);
                    }
                }
            }); 
    };

    AdvancedConfiguration.InitNotSynchronizedMessage = function (notSyncServersNames) {
        processSyncInfo(notSyncServersNames);
        settingsSyncCheck();
    };

    AdvancedConfiguration.InitWarningMessage = function (services) {
        var warningMessageContainer = $("#CentrSettingsWarningMessageID");
        services.length > 0 ? warningMessageContainer.css("display", "block") : warningMessageContainer.css("display", "none");

        var servers = [];
        $.each(services, function (index, service) {
            if (servers.lastIndexOf(service.ServerName) === -1) {
                servers.push(service.ServerName);
            }
        });

        $.each(services, function (index, service) {
            service.text = servers.length > 1
            ? String.format("@{R=Core.Strings.2;K=CentralizedSetting_RemoteServiceNameFormat;E=js}", service.DisplayName, service.ServerName)
            : service.DisplayName;

            service.restarting = false;

            servicesToRestart.push(service);
        });

        showServices(services);
    };

    AdvancedConfiguration.RestartServices = function () {
        $("#CentrSettingsErrorMessageID").hide();
        fitPageContent();

        $("#CentrSettingsRestartAllButtonID").css("visibility", "hidden");

        var services = getServiceNamesToRestartPerServer();
        var requests = [];

        $.each(services, function (serverId) {
            var request = $.ajax({
                url: "/Orion/Admin/AdvancedConfiguration/Services/OrionServicesManagementService.asmx/RestartList",
                type: "POST",
                data: JSON.stringify({
                    serverHostname: services[serverId].ServerName,
                    servicesList: services[serverId].Services
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error: function () {
                    servicesToRestart = $.grep(servicesToRestart, function(service, index) {
                        return service.ServerIp !== services[serverId].IP;
                    });
                    failedToRestartServices = $.grep(failedToRestartServices, function (service, index) {
                        return service.ServerIp !== services[serverId].IP;
                    });

                    addToNonRespondingServers([services[serverId].ServerName]);
                }
            });
            requests.push(request);
        });

        $.when.apply($, requests).always(function () {
            showNotRespondingErrorMessage(nonRespondingServers);
            startServicesStatusCheck();
        });
    };
})(SW.Core);
