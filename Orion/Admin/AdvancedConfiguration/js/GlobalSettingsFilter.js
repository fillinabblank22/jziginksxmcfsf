﻿(function () {
    $(document).keypress(function (e) {
        if (e.which === 13) {
            $("#searchImgDiv").trigger("click", e.keyCode);
        }
    });

    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            $("#searchImgDiv").trigger("click", e.keyCode);
        }
    });

    $(document).ready(function () {
        var searchImgDiv = $("#searchImgDiv");
        var searchText = $("#searchText");
        var searchTextCache = $("[id$='searchTextCache']");
        var searchImage = $("#searchImg");
        var settingsTable = $(".centralizedSettingTable");
        var searchDisabledStateChar = "\u0020";
        var strictSearchFormat = "\"{0}\"";
        var isStrictSearchRegex = /"([^"]*)"/g;

        function getStrictSearchValue(searchTextVal) {
            var matches = searchTextVal.match(isStrictSearchRegex);
            return matches && matches.length === 1
                ? matches[0].replace(/"/g, "")
                : null;
        }

        function isEmptyOrSpaces(str) {
            return !str || str.match(/^ *$/) !== null;
        }

        var showSettingTable = function () {
            settingsTable.find("tr").each(function () {
                $(this).show();
            });
        }

        var filterSettingTable = function (searchTextVal) {
            var settingNameSelector = searchTextVal.indexOf(".") === -1
                ? "td.shortNameCell"
                : "td.fullNameCell";

            $((settingsTable.find("tr").toArray()).reverse()).each(function () {
                var tdText = $(this)
                    .find(settingNameSelector)
                    .text();

                var strictSearchValue = getStrictSearchValue(searchTextVal);
                var condition = strictSearchValue != null
                    ? tdText.toLowerCase() !== strictSearchValue.toLowerCase()
                    : tdText.toLowerCase().indexOf(searchTextVal.toLowerCase()) < 0;

                if (condition) {
                    if ($(this).hasClass("mainRow")) {
                        $(this).hide();
                    } else if ($(this).hasClass("groupRow")) {
                        var tr = $(this).nextAll(":visible:first");
                        if (!tr.length || tr.hasClass("groupRow")) {
                            $(this).hide();
                        }
                    }
                }
            });
        }

        var adjustZebra = function () {
            $("tr:visible:odd").css("background-color", "white");
            $("tr:visible:even").css("background-color", "#f9f9f9");
        }

        var getUrlVars = function () {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split("=");
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var regainFilterState = function () {

            if (searchTextCache.val() !== searchDisabledStateChar) {
                if (!isEmptyOrSpaces(searchTextCache.val())) {
                    searchText.val(searchTextCache.val());
                    searchImgDiv.click();
                    return;
                } else {
                    var settingNameFromQueryString = getUrlVars()["settingName"];
                    if (!isEmptyOrSpaces(settingNameFromQueryString)) {
                        searchText.val(String.format(strictSearchFormat, settingNameFromQueryString));
                        searchImgDiv.click();
                        return;
                    }
                }
            }
        }

        searchImgDiv.on("click", function (eventArgs, keyCode) {
            var searchTextVal = searchText.val();

            if (searchImgDiv.attr("search") === "on" && (!keyCode || keyCode === 27 || isEmptyOrSpaces(searchTextVal))) {
                searchImage.css("background-image", "url('/Orion/images/Button.SearchIcon.gif')");
                searchImgDiv.attr("search", "off");
                searchText.val("");
                searchTextCache.val(searchDisabledStateChar);
                showSettingTable();
            } else if (!isEmptyOrSpaces(searchTextVal) && (!keyCode || keyCode === 13)) {
                searchImage.css("background-image", "url('/Orion/images/Button.SearchCancel.gif')");
                searchImgDiv.attr("search", "on");
                searchTextCache.val(searchTextVal);

                showSettingTable();
                filterSettingTable(searchTextVal);
            }

            adjustZebra();
        });

        regainFilterState();
    });
})(window);
