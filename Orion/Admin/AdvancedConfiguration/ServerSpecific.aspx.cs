﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_AdvancedConfiguration_ServerSpecific : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Form.SubmitDisabledControls = true;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        (Master).ServicesToRestart = SettingsTable.ServicesToRestart;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }

        var centralizedSettingsDal = new CentralizedSettingsDAL();
        var orionServers = new Dictionary<int, string> { { 0, Resources.CoreWebContent.WEBCODE_SO0_8 } };

        foreach (DataRow row in centralizedSettingsDal.GetOrionServers().Rows)
        {
            orionServers.Add((int)row["OrionServerID"], row["HostName"].ToString());
        }

        orionServerDDL.DataSource = orionServers;
        orionServerDDL.DataTextField = "Value";
        orionServerDDL.DataValueField = "Key";
        orionServerDDL.DataBind();
    }
}
