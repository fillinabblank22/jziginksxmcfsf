using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Castle.Core.Internal;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.Model.CentralizedSettings;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Internationalization.Exceptions;

public partial class Orion_Admin_CentralizedSettings_SettingsTable : UserControl
{
    private const string GroupRowCssClass = "groupRow";
    private const string MainRowCssClass = "mainRow";
    private const string SettingEditorIdFormat = "SettingEditorFor_{0}";
    private const string RestartWarningIdFormat = "RestartWarningFor_{0}";
    private const string IsRestartedFlagIdFormat = "IsRestartedFlagIdFor_{0}";
    private const string IsChangedFlagIdFormat = "IsChangedFlagIdFor_{0}";
    private const string IsResetFlagIdFormat = "IsResetFlagIdFor_{0}";
    private const string IsRestartNeededAttribute = "restartNeeded";
    private const string ErrorMessageText = @"*";

    private int _nameCellIndex;
    private int _uriCellIndex;
    private int _serviceDependenciesCellIndex;
    private int _orionServerIdCellIndex;
    private int _initialValueCellIndex;
    private int _isDefaultCellIndex;

    private readonly OrionWindowsServicesDAL _orionWindowsServicesDal = new OrionWindowsServicesDAL();
    private readonly CentralizedSettingsDAL _centralizedSettingsDal = new CentralizedSettingsDAL();
    private readonly Dictionary<string, string> _allSettings = new Dictionary<string, string>();

    protected bool IsGlobalMode
    {
        get { return DisplayMode == ControlDisplayMode.Global; }
    }

    protected bool IsServerSpecificMode
    {
        get { return DisplayMode == ControlDisplayMode.ServerSpecific; }
    }

    protected IEnumerable<TableRow> ChangedSettingsRows
    {
        get
        {
            return DynamicSettingsTable.Rows
                .OfType<TableRow>()
                .Where(r => IsMainRow(r) && IsUpdateRequired(r));
        }
    }

    protected Dictionary<SettingKey, string> SettingsToUpdate
    {
        get
        {
            return ChangedSettingsRows
                .Where(r => !IsDefaultValue(r))
                .ToDictionary(GetSettingKey,
                    r =>
                {
                    var control = r.FindControl(GetControlId(SettingEditorIdFormat, r));
                    return GetControlValue(control);
                });
        }
    }

    protected List<SettingKey> SettingsToReset
    {
        get
        {
            return ChangedSettingsRows
                .Where(IsDefaultValue)
                .Select(GetSettingKey)
                .ToList();
        }
    }

    protected IList<OrionWindowsService> Services { get; set; }

    public IList<OrionWindowsService> ServicesToRestart
    {
        get
        {
            var settingsWithServicesToRestart = DynamicSettingsTable.Rows
                .OfType<TableRow>()
                .Where(r => IsMainRow(r) && IsRestartRequired(r) && IsServicesRestarted(r))
                .ToList();

            return ExtractOrionWindowsServices(settingsWithServicesToRestart);
        }
    }

    public ControlDisplayMode DisplayMode { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        CheckPermissions();

        var settingsData = IsGlobalMode
            ? _centralizedSettingsDal.GetSettings()
            : _centralizedSettingsDal.GetSettingsForOrionServers();

        _allSettings.Clear();

        LoadServices(settingsData);

        BuildDynamicSettingsTable(settingsData);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            return;
        }

        _centralizedSettingsDal.UpdateSettings(SettingsToUpdate);
        _centralizedSettingsDal.ResetSettings(SettingsToReset);

        RefreshDynamicSettingsTableState();
    }

    private bool IsUpdateRequired(TableRow row)
    {
        if (!IsValueChanged(row))
        {
            return false;
        }

        string settingValue = GetControlValue(row.FindControl(GetControlId(SettingEditorIdFormat, row)));
        string settingKey = GetSettingKey(row).ToString();
        string cachedValue;

        return _allSettings.TryGetValue(settingKey, out cachedValue) &&
               !cachedValue.Equals(settingValue, StringComparison.OrdinalIgnoreCase);
    }

    private void CheckPermissions()
    {
        if (Profile.AllowAdmin) return;

        var errorDetails = new ErrorInspectorDetails
        {
            Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323),
            Title = Resources.CoreWebContent.WEBDATA_VB0_567
        };

        OrionErrorPageBase.TransferToErrorPage(Context, errorDetails);
    }

    private void LoadServices(DataTable settingsData)
    {
        var servicesNames = settingsData.Rows
            .OfType<DataRow>()
            .SelectMany(r => r.GetValueOrDefault<string[]>("ServiceRestartDependencies"))
            .Distinct()
            .ToList();

        Services = _orionWindowsServicesDal.GetWindowsServices()
            .Where(s => !s.Status.Equals("Stopped", StringComparison.OrdinalIgnoreCase) && servicesNames.Contains(s.Name))
            .ToList();
    }

    private void BuildDynamicSettingsTable(DataTable settingsData)
    {
        DynamicSettingsTable.AddCssClass(DisplayMode.ToString().ToLowerInvariant());

        var currentGroup = string.Empty;

        foreach (DataRow dataRow in settingsData.Rows)
        {
            BuildGroupRow(dataRow, ref currentGroup);
            BuildMainRow(dataRow);
        }

        SetGroupRowsColspan();
    }

    private void BuildGroupRow(DataRow dataRow, ref string currentGroup)
    {
        var prefix = dataRow.GetValueOrDefault<string>("Prefix");

        if (currentGroup == prefix) return;

        var groupRow = new TableRow {CssClass = GroupRowCssClass};

        DynamicSettingsTable.Rows.Add(groupRow);

        groupRow.Cells.Add(new TableCell {Text = prefix, CssClass = "groupCell"});

        currentGroup = prefix;
    }

    private void BuildMainRow(DataRow dataRow)
    {
        var mainRow = new TableRow {CssClass = MainRowCssClass};

        DynamicSettingsTable.Rows.Add(mainRow);

        BuildInternalCells(dataRow, mainRow);

        BuildHiddenCells(dataRow, mainRow);

        BuildNameCell(dataRow, mainRow);
        BuildValueCell(dataRow, mainRow);
        BuildResetCell(mainRow);
        BuildShowOverridesCell(dataRow, mainRow);
        BuildDescriptionCell(dataRow, mainRow);
    }

    private void BuildInternalCells(DataRow dataRow, TableRow mainRow)
    {
        _uriCellIndex = mainRow.Cells.Add(new TableCell
        {
            Text = dataRow.GetValueOrDefault<string>("Uri"),
            Visible = false
        });

        _serviceDependenciesCellIndex = mainRow.Cells.Add(new TableCell
        {
            Text = string.Join(",", dataRow.GetValueOrDefault<string[]>("ServiceRestartDependencies")),
            Visible = false
        });
    }

    private void BuildHiddenCells(DataRow dataRow, TableRow mainRow)
    {
        _orionServerIdCellIndex = mainRow.Cells.Add(new TableCell
        {
            Text = dataRow.GetValueOrDefault<int>("OrionServerID").ToString(),
            CssClass = "orionServerIdCell"
        });

        _nameCellIndex = mainRow.Cells.Add(new TableCell
        {
            Text = dataRow.GetValueOrDefault<string>("Name"),
            CssClass = "fullNameCell"
        });

        mainRow.Cells.Add(new TableCell
        {
            Text = GetShortSettingName(dataRow),
            CssClass = "shortNameCell"
        });

        mainRow.Cells.Add(new TableCell
        {
            Text = HttpUtility.HtmlEncode(dataRow.GetValueOrDefault<string>("DefaultValue")),
            CssClass = "defaultValueCell"
        });

        _initialValueCellIndex = mainRow.Cells.Add(new TableCell
        {
            Text = HttpUtility.HtmlEncode(dataRow.GetValueOrDefault<string>("Value")),
            CssClass = "initialValueCell"
        });

        _isDefaultCellIndex = mainRow.Cells.Add(new TableCell
        {
            Text = dataRow.GetValueOrDefault<bool>("IsDefault").ToString(),
            CssClass = "isDefaultCell"
        });

        var isChangedCell = new TableCell { CssClass = "isChangedCell" };
        isChangedCell.Controls.Add(new CheckBox
        {
            ID = GetControlId(IsChangedFlagIdFormat, dataRow),
            CssClass = "isChangedFlag"
        });

        mainRow.Cells.Add(isChangedCell);

        var isResetCell = new TableCell { CssClass = "isResetCell" };
        isResetCell.Controls.Add(new CheckBox
        {
            ID = GetControlId(IsResetFlagIdFormat, dataRow),
            CssClass = "isResetFlag"
        });

        mainRow.Cells.Add(isResetCell);
    }


    private void BuildNameCell(DataRow dataRow, TableRow mainRow)
    {
        mainRow.Cells.Add(new TableCell
        {
            Text = GetSettingDisplayName(dataRow),
            CssClass = "nameCell"
        });
    }

    private void BuildValueCell(DataRow dataRow, TableRow mainRow)
    {
        var valueCell = new TableCell {CssClass = "valueCell"};

        mainRow.Cells.Add(valueCell);

        var settingEditorId = GetControlId(SettingEditorIdFormat, dataRow);

        var valueType = Type.GetType(dataRow.GetValueOrDefault<string>("DataType"));
        var valueString = dataRow.GetValueOrDefault<string>("Value");
        var values = dataRow.GetValueOrDefault<string[]>("Values");

        if (valueType == typeof (bool))
        {
            bool value;
            valueCell.Controls.Add(new CheckBox
            {
                ID = settingEditorId,
                Checked = bool.TryParse(valueString, out value)
                    ? value
                    : bool.Parse(dataRow.GetValueOrDefault<string>("DefaultValue")),
                CssClass = "checkBox"
            });
        }
        else if (values.Any())
        {
            var dropDownList = new DropDownList
            {
                ID = settingEditorId,
                CssClass = "dropDownBox"
            };

            dropDownList.DataSource = values;
            dropDownList.DataBind();

            var selectedValue = values.FirstOrDefault(v => v.Equals(valueString, StringComparison.OrdinalIgnoreCase));
            if (selectedValue != null)
            {
                dropDownList.SelectedValue = selectedValue;
            }

            valueCell.Controls.Add(dropDownList);
        }
        else
        {
            if (valueType == typeof (int) || valueType == typeof (double) || valueType == typeof(TimeSpan))
            {
                valueCell.Controls.Add(new TextBox
                {
                    ID = settingEditorId,
                    Text = valueString,
                    CssClass = string.Format("textBox {0}", valueType == typeof(TimeSpan) ? "timeSpanTextBox" : "numericTextBox")
                });

                valueCell.Controls.Add(new RegularExpressionValidator
                {
                    ControlToValidate = settingEditorId,
                    ValidationExpression = GetValidatonExpression(valueType),
                    Display = ValidatorDisplay.Dynamic,
                    ErrorMessage = ErrorMessageText
                });

                valueCell.Controls.Add(new RequiredFieldValidator
                {
                    ControlToValidate = settingEditorId,
                    Display = ValidatorDisplay.Dynamic,
                    ErrorMessage = ErrorMessageText
                });
            }
            else
            {
                var advancedTextBox = new AdvancedTextBox
                {
                    ID = settingEditorId,
                    Text = valueString,
                    CssClass = "advancedTextBox textBox"
                };

                valueCell.Controls.Add(advancedTextBox);
            }
        }

        var settingEditor = (WebControl) valueCell.FindControl(settingEditorId);

        settingEditor.CssClass += " valueEditor";

        if (IsServerSpecificMode)
        {
            var overrideButton = new HyperLink
            {
                Text = Resources.CoreWebContent.WEBDATA_OF0_01,
                CssClass = "overrideButton sw-btn"
            };

            overrideButton.Attributes.CssStyle.Add("visibility", "visible");

            valueCell.Controls.Add(overrideButton);
        }

        var serviceRestartDependencies = dataRow.GetValueOrDefault<string[]>("ServiceRestartDependencies");

        if (serviceRestartDependencies.Any(s => Services.Any(i => i.Name == s)))
        {
            valueCell.Controls.Add(CreateServiceRestartWarningBox(dataRow, serviceRestartDependencies));
        }

        _allSettings[GetSettingKey(dataRow).ToString()] = GetControlValue(settingEditor);
    }

    private HtmlGenericControl CreateServiceRestartWarningBox(DataRow dataRow, string[] serviceRestartDependencies)
    {
        const string breakTag = "<br/>";

        var span = new HtmlGenericControl("span")
        {
            ID = GetControlId(RestartWarningIdFormat, dataRow)
        };

        span.Attributes.Add("class", "serviceRestartWarningBox");
        span.Attributes.CssStyle.Add("visibility", "hidden");

        span.Controls.Add(new Image {ImageUrl = "/Orion/images/warning_16x16.gif", CssClass = "warningImg" });
        span.Controls.Add(new Label { Text = Resources.CoreWebContent.AdvancedConfiguration_SettingTable_RestartServicesLabel });

        var tooltip = new HtmlGenericControl("span");
        tooltip.Attributes.Add("class", "tooltip");
        tooltip.InnerHtml =
            string.Format(Resources.CoreWebContent.AdvancedConfiguration_SettingTable_RestartServicesToolTip,
                breakTag,
                string.Join(breakTag, Services
                    .Where(s => serviceRestartDependencies.Contains(s.Name))
                    .Select(s => s.DisplayName)
                    .Distinct()));
        span.Controls.Add(tooltip);

        CheckBox isRestartedCheckBox;
        span.Controls.Add(isRestartedCheckBox = new CheckBox { ID = GetControlId(IsRestartedFlagIdFormat, dataRow), CssClass = "isRestartedFlag" });
        isRestartedCheckBox.Attributes.CssStyle.Add("display", "none");

        return span;
    }

    private void BuildResetCell(TableRow mainRow)
    {
        var resetCell = new TableCell {CssClass = "resetCell"};

        mainRow.Cells.Add(resetCell);

        var resetLink = new HyperLink();

        if (IsGlobalMode)
        {
            resetLink.Text = Resources.CoreWebContent.AdvancedConfiguration_SettingTable_ResetToDefault;
            resetLink.CssClass = "resetLink";
        }
        else
        {
            resetLink.Text = Resources.CoreWebContent.AdvancedConfiguration_SettingTable_UseGlobal;
            resetLink.CssClass = "useGlobalLink";
        }

        resetLink.Attributes.CssStyle.Add("display", "none");

        resetCell.Controls.Add(resetLink);
    }

    private void BuildShowOverridesCell(DataRow dataRow, TableRow mainRow)
    {
        if (IsServerSpecificMode)
        {
            return;
        }

        var showOverridesCell = new TableCell { CssClass = "showOverridesCell" };

        if (dataRow.GetValueOrDefault<bool>("HasOverrides"))
        {
            const string linkUrlFormat = "~/Orion/Admin/AdvancedConfiguration/ServerSpecific.aspx?settingName={0}";

            showOverridesCell.Controls.Add(new HyperLink
            {
                Text = Resources.CoreWebContent.AdvancedConfiguration_SettingTable_ShowOverrides,
                CssClass = "showOverridesLink",
                NavigateUrl = string.Format(linkUrlFormat, dataRow.GetValueOrDefault<string>("Name"))
            });
        }

        mainRow.Cells.Add(showOverridesCell);
    }

    private static void BuildDescriptionCell(DataRow dataRow, TableRow mainRow)
    {
        mainRow.Cells.Add(new TableCell
        {
            Text = dataRow.GetValueOrDefault<string>("Description"),
            CssClass = "descriptionCell"
        });
    }

    private void SetGroupRowsColspan()
    {
        var dynamicSettingsTableRows = DynamicSettingsTable.Rows.Cast<TableRow>().ToList();
        var colspanAttributeValue = dynamicSettingsTableRows
            .Max(r => r.Cells.Count)
            .ToString(CultureInfo.InvariantCulture);

        dynamicSettingsTableRows
            .Where(r => IsGroupRow(r) && r.Cells.Count == 1)
            .ForEach(r => r.Cells[0].Attributes.Add("colspan", colspanAttributeValue));
    }

    private void RefreshDynamicSettingsTableState()
    {
        DynamicSettingsTable.Rows
            .OfType<TableRow>()
            .Where(IsMainRow)
            .ForEach(r =>
            {
                RefreshRestartWarning(r);
                RefreshInitialValue(r);
                RefreshIsDefaultValue(r);
            });
    }

    private void RefreshRestartWarning(TableRow tableRow)
    {
        var restartWarningBoxId = GetControlId(RestartWarningIdFormat, tableRow);
        var isRestartedBoxId = GetControlId(IsRestartedFlagIdFormat, tableRow);

        var restartWarningBox = (HtmlGenericControl)tableRow.FindControl(restartWarningBoxId);
        var isRestartedBox = (CheckBox)tableRow.FindControl(isRestartedBoxId);

        if (restartWarningBox == null || isRestartedBox == null)
        {
            return;
        }

        if (IsValueChanged(tableRow))
        {
            restartWarningBox.Attributes.Add(IsRestartNeededAttribute, string.Empty);
        }
        else if (isRestartedBox.Checked)
        {
            restartWarningBox.Attributes.Remove(IsRestartNeededAttribute);
        }

        isRestartedBox.Checked = false;
    }

    private void RefreshInitialValue(TableRow tableRow)
    {
        var settingEditor = tableRow.FindControl(GetControlId(SettingEditorIdFormat, tableRow));
        if (settingEditor != null)
        {
            tableRow.Cells[_initialValueCellIndex].Text = HttpUtility.HtmlEncode(GetControlValue(settingEditor));
        }
    }

    private void RefreshIsDefaultValue(TableRow tableRow)
    {
        var isChangedFlag = tableRow.FindControl(GetControlId(IsChangedFlagIdFormat, tableRow));
        var isResetFlag = tableRow.FindControl(GetControlId(IsResetFlagIdFormat, tableRow));

        if (isChangedFlag != null &&
            isResetFlag != null &&
            bool.Parse(GetControlValue(isChangedFlag)))
        {
            tableRow.Cells[_isDefaultCellIndex].Text = (IsGlobalMode
                ? IsDefaultValue(tableRow)
                : bool.Parse(GetControlValue(isResetFlag))).ToString();
        }
    }

    private bool IsGroupRow(TableRow tableRow)
    {
        return tableRow.CssClass.Equals(GroupRowCssClass);
    }

    private bool IsMainRow(TableRow tableRow)
    {
        return tableRow.CssClass.Equals(MainRowCssClass);
    }

    private bool IsDefaultValue(TableRow tableRow)
    {
        var isResetFlagId = GetControlId(IsResetFlagIdFormat, tableRow);
        var isResetFlag = (CheckBox)tableRow.FindControl(isResetFlagId);

        return isResetFlag.Checked;
    }

    private bool IsValueChanged(TableRow tableRow)
    {
        var isChangedFlagId = GetControlId(IsChangedFlagIdFormat, tableRow);
        var isChangedFlag = (CheckBox) tableRow.FindControl(isChangedFlagId);

        return isChangedFlag.Checked;
    }

    private bool IsRestartRequired(TableRow tableRow)
    {
        return !string.IsNullOrWhiteSpace(tableRow.GetValueOrDefault<string>(_serviceDependenciesCellIndex));
    }

    private bool IsServicesRestarted(TableRow tableRow)
    {
        var restartWarningId = GetControlId(RestartWarningIdFormat, tableRow);
        var isRestartedBoxId = GetControlId(IsRestartedFlagIdFormat, tableRow);

        var restartWarning = (HtmlGenericControl)tableRow.FindControl(restartWarningId);
        var isRestartedBox = (CheckBox)tableRow.FindControl(isRestartedBoxId);

        if (restartWarning == null || isRestartedBox == null)
        {
            return false;
        }

        return (!isRestartedBox.Checked && IsValueChanged(tableRow)) ||
               (!isRestartedBox.Checked && restartWarning.Attributes[IsRestartNeededAttribute] != null);
    }

    private IList<OrionWindowsService> ExtractOrionWindowsServices(IList<TableRow> settingsRows)
    {
        IList<OrionWindowsService> orionWindowsServices;

        if (IsGlobalMode)
        {
            var servicesNames = settingsRows.SelectMany(ExtractServicesNames).Distinct();

            orionWindowsServices = Services
                .Where(s => servicesNames.Contains(s.Name))
                .ToList();
        }
        else
        {
            var orionServerIdsAndServicesNames = settingsRows.Select(r => new
            {
                OrionServerId = r.GetValueOrDefault<int>(_orionServerIdCellIndex),
                ServicesNames = ExtractServicesNames(r)
            });

            orionWindowsServices = Services
                .Where(s => orionServerIdsAndServicesNames
                    .Any(sn => sn.OrionServerId == s.OrionServerId && sn.ServicesNames.Contains(s.Name)))
                .ToList();
        }

        return orionWindowsServices;
    }

    private string GetValidatonExpression(Type valueType)
    {
        var result = string.Empty;

        if (valueType == typeof (int))
        {
            result = @"\d+";
        }
        else if (valueType == typeof (double))
        {
            result = @"^(?!0+(\.0+)?$)((\d*\.)?\d+)$";
        }
        else if (valueType == typeof (TimeSpan))
        {
            result = @"^(\d{1,3}\.)?([0-1]?\d|2[0-3]):([0-5]?\d):([0-5]?\d)(\.\d{1,7})?$";
        }

        return result;
    }

    private string GetShortSettingName(DataRow dataRow)
    {
        var name = dataRow["Name"].ToString();
        var prefix = dataRow["Prefix"].ToString();
        int index = name.IndexOf(prefix, StringComparison.Ordinal);
        return index < 0
            ? name
            : name.Remove(index, prefix.Length + 1);
    }

    private string GetSettingDisplayName(DataRow dataRow)
    {
        return string.Format(IsGlobalMode ?  "{0}" : "{0} (ON {1})",
            GetShortSettingName(dataRow),
            dataRow.GetValueOrDefault<string>("HostName"));
    }

    private string[] ExtractServicesNames(TableRow tableRow)
    {
        return tableRow.GetValueOrDefault<string>(_serviceDependenciesCellIndex).Split(',');
    }

    private SettingKey GetSettingKey(TableRow tableRow)
    {
        return SettingKey.Create(tableRow.GetValueOrDefault<string>(_nameCellIndex),
            tableRow.GetValueOrDefault<int>(_orionServerIdCellIndex),
            tableRow.GetValueOrDefault<string>(_uriCellIndex));
    }

    private SettingKey GetSettingKey(DataRow dataRow)
    {
        return SettingKey.Create(dataRow.GetValueOrDefault<string>("Name"),
            dataRow.GetValueOrDefault<int>("OrionServerID"),
            dataRow.GetValueOrDefault<string>("Uri"));
    }

    private static string GetControlValue(Control control)
    {
        var value = string.Empty;

        if (control is CheckBox)
        {
            value = ((CheckBox)control).Checked.ToString();
        }
        else if (control is DropDownList)
        {
            value = ((DropDownList)control).SelectedValue;
        }
        else if (control is TextBox)
        {
            value = ((TextBox)control).Text;
        }

        return value;
    }

    private string GetControlId(string controlIdFormat, TableRow tableRow)
    {
        return GetControlId(controlIdFormat, GetSettingKey(tableRow));
    }

    private string GetControlId(string controlIdFormat, DataRow dataRow)
    {
        return GetControlId(controlIdFormat, GetSettingKey(dataRow));
    }

    private string GetControlId(string controlIdFormat, SettingKey settingKey)
    {
        return string.Format(controlIdFormat, GetIdCompatibleString(settingKey.ToString()));
    }

    private string GetIdCompatibleString(string value)
    {
        return value.Replace('.', '_');
    }

    public enum ControlDisplayMode
    {
        Global = 0,
        ServerSpecific = 1
    }
}
