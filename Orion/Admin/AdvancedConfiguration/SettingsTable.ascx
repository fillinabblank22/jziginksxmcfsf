<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SettingsTable.ascx.cs" Inherits="Orion_Admin_CentralizedSettings_SettingsTable" %>

<orion:Include ID="SettingTableJsInclude" runat="server" File="Admin/AdvancedConfiguration/js/SettingsTable.js" />
<orion:Include ID="SettingTableCssInclude" runat="server" File="Admin/AdvancedConfiguration/styles/SettingsTable.css" />
<orion:Include ID="AdvancedTextBoxHandlersInclude" runat="server" File="AdvancedTextBox/Handlers.js" />
<orion:Include ID="AdvancedTextBoxEditorDialog" runat="server" File="AdvancedTextBox/EditorDialog.js" />

<asp:Panel runat="server" ID="DynamicSettingsTablePanel" Class="centralizedSettingPanel">
    <div class="waitText">
        <img src="/Orion/Admin/Accounts/images/animated/loading_gen_16x16.gif"/>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.CentralizedSettings_WaitText) %>
    </div>
    <asp:Table runat="server" ID="DynamicSettingsTable" CssClass="centralizedSettingTable" CellSpacing="0" Width="100%" />
</asp:Panel>