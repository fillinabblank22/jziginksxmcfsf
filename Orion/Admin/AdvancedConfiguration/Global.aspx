<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Global.aspx.cs" Inherits="Orion_Admin_AdvancedConfiguration_Global" 
    MasterPageFile="~/Orion/Admin/AdvancedConfiguration/AdvancedConfiguration.master" Title="<%$ HtmlEncodedResources: CoreWebContent, AdvancedConfiguration_Title %>"%>

<%@ Register Src="~/Orion/Admin/AdvancedConfiguration/SettingsTable.ascx" TagPrefix="orion" TagName="SettingsTable" %>
<%@ MasterType VirtualPath="~/Orion/Admin/AdvancedConfiguration/AdvancedConfiguration.master"%>

<asp:Content runat="server" ContentPlaceHolderID="settingsContentPlaceholder">
    <orion:Include ID="SettingFilterCssInclude" runat="server" File="Admin/AdvancedConfiguration/styles/SettingsFilter.css"/>
    <orion:Include ID="SettingFilterJsInclude" runat="server" File="Admin/AdvancedConfiguration/js/GlobalSettingsFilter.js"/>

    <div class="filterDiv">
        
        <div class="searchDiv">
            <div class="inputDiv">
                <input id="searchText" class="searchInput"/>
				<asp:TextBox id="searchTextCache" class="searchInputCache" runat="server"/>
            </div>
            <div id="searchImgDiv" class="searchImgDiv">
                <span id="searchImg" class="searchImage"></span>
            </div>
        </div>
    </div>
    <orion:SettingsTable runat="server" ID="SettingsTable" DisplayMode="Global" />
</asp:Content>
