<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewGroupTitleEditor.ascx.cs" Inherits="Orion_Admin_ViewGroupTitleEditor" %>
<%@ Import Namespace="Resources" %>

  <table>
	    <tr>
		    <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_12) %></td>
		    <td><asp:TextBox runat="server" ID="txtViewTitle" MaxLength="200" Columns="40"/></td>
		    <td>
		        <orion:LocalizableButton runat="server" ID="btnUpdateTitle" LocalizedText="CustomText" Text ="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_181 %>" DisplayType="Small" OnClick="UpdateTitle_Click"/>
		    </td>
		    <td>
                <asp:CustomValidator ID="CustomValidator" runat="server" ControlToValidate="txtViewTitle"
                                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_279 %>"
                                    OnServerValidate="TxtViewTitleValidate"
                                    Display="Dynamic"                                    
                                    />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtViewTitle" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_162 %>" runat="server" Display="Dynamic" />
        </td>
	    </tr>
	    <tr>
		    <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_269) %></td>
		    <td><strong> <asp:literal ID="ViewType" runat="server"></asp:literal> </strong></td>
	    </tr>
    </table>