using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_EditDefaultNetObject : System.Web.UI.Page
{
    private ProfileCommon _thisProfile;
    
    protected ProfileCommon ThisProfile
    {
        get 
        {
            if (null == _thisProfile)
            {
                _thisProfile = Profile.GetProfile(Request.QueryString["AccountID"]);
            }
            return _thisProfile; 
        }
    }

    public ViewInfo DefaultView
    {
        get 
        {
            var viewId = Request.QueryString["ViewID"];
            if (string.IsNullOrEmpty(viewId))
            {
                return null;
            }
            return ViewManager.GetViewById(Convert.ToInt32(viewId));
        }
    }

    protected override void OnInit(EventArgs e)
    {
        var defaultView = DefaultView;
        if (null == defaultView || null == defaultView.NetObjectType)
        {
            this.phSelectNetObjectText.Visible = false;
            this.phNoNetObjectNeeded.Visible = true;
        }
        else
        {
            this.phNoNetObjectNeeded.Visible = false;
            this.phSelectNetObjectText.Visible = true;
        }

        if (null != defaultView && null != defaultView.NetObjectType)
        {
            IEnumerable<NetObject> allNetObjects = NetObjectFactory.GetAllObjects(defaultView.NetObjectType);
            SortedList<string, NetObject> sortedNetObjects = new SortedList<string, NetObject>();
            foreach (NetObject obj in allNetObjects)
            {
                sortedNetObjects.Add(string.Format("{0};{1}", obj.Name, obj.NetObjectID), obj);
            }

            this.lstNetObjects.DataSource = sortedNetObjects.Values;
            this.lstNetObjects.DataBind(); 
        }

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    protected void ContinueClick(object sender, EventArgs e)
    {
        var defaultView = DefaultView;
        if (null == defaultView || null == defaultView.NetObjectType)
		{
			ThisProfile.DefaultNetObject = "not selected";
			ThisProfile.DefaultNetObjectID = "";
            var viewId = Request.QueryString["ViewID"];
            var dashboardId = Request.QueryString["DashboardID"];
            if (string.IsNullOrEmpty(viewId) && !string.IsNullOrEmpty(dashboardId))
            {
                ThisProfile.HomePageDashboardID = Convert.ToInt32(dashboardId);
                ThisProfile.HomePageViewID = -1;
            }
            else if (!string.IsNullOrEmpty(viewId))
            {
                ThisProfile.HomePageViewID = Convert.ToInt32(viewId);
                ThisProfile.HomePageDashboardID = -1;
            }
			ThisProfile.Save();
            string SafeUserName = this.Server.UrlEncode(ThisProfile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)
            ReferrerRedirectorBase.Return(string.Format("/Orion/Admin/EditAccount.aspx?AccountID={0}", SafeUserName));
		}
		else
		{
			if (lstNetObjects.Items.Count != 0)
			{
				if (lstNetObjects.SelectedValue != "")
				{
					ThisProfile.DefaultNetObject = lstNetObjects.SelectedItem.Text;
					ThisProfile.DefaultNetObjectID = lstNetObjects.SelectedValue;
					ThisProfile.HomePageViewID = Convert.ToInt32(Request.QueryString["ViewID"]);
                    ThisProfile.HomePageDashboardID = -1;
                    ThisProfile.Save();
                    string SafeUserName = this.Server.UrlEncode(ThisProfile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)
                    ReferrerRedirectorBase.Return(string.Format("/Orion/Admin/EditAccount.aspx?AccountID={0}", SafeUserName));
				}
			}
			else
			{
                string SafeUserName = this.Server.UrlEncode(ThisProfile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)               
                ReferrerRedirectorBase.Return(string.Format("/Orion/Admin/EditAccount.aspx?AccountID={0}", SafeUserName));
            }
		}
    }
}
