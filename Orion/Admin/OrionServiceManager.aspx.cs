﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.DALs;

public partial class Orion_Admin_OrionServiceManager : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        OrionServerDropdown.DataSource = GetOrionServers();
        OrionServerDropdown.DataTextField = "Value";
        OrionServerDropdown.DataValueField = "Key";
        OrionServerDropdown.DataBind();

        SetCurrentServer();
    }

    private static IDictionary<string, string> GetOrionServers()
    {
        var orionServersDictionary = new Dictionary<string, string>();

        var orionSerers = new OrionServerDAL()
            .GetOrionServerNames()
            .Select(s => s.Value);

        foreach (string server in orionSerers)
        {
            try
            {
                var orionServerIp = Dns
                    .GetHostAddresses(server)
                    .FirstOrDefault(i => i.AddressFamily == AddressFamily.InterNetwork);

                if (orionServerIp != null)
                {
                    orionServersDictionary.Add(orionServerIp.ToString(), server);
                }
            }
            catch (SocketException ex)
            {
                log.ErrorFormat("Error resolving hostname {0}. {1}", server, ex);
                orionServersDictionary.Add(server, server);
            }
        }

        return orionServersDictionary;
    }

    private void SetCurrentServer()
    {
        var currentServerItem = OrionServerDropdown.Items.FindByValue(Request.ServerVariables["LOCAL_ADDR"]);
        if (currentServerItem != null)
        {
            currentServerItem.Selected = true;
        }
    }
}
