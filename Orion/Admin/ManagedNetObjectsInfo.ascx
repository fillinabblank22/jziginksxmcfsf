﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagedNetObjectsInfo.ascx.cs" Inherits="Orion_Admin_ManagedNetObjectsInfo" %>
 <div class="managedObjInfoWrapper">   
     <span id="monitoredObjectsInfo" runat="server">
         <asp:Image ID="okImage" runat="server" ImageUrl="~/Orion/images/ok_16x16.gif" CssClass="stateImage"></asp:Image>
         <asp:Image ID="bulbImage" runat="server" ImageUrl="~/Orion/images/lightbulb_tip_16x16.gif" Visible="false" CssClass="stateImage"></asp:Image>
         <span style="font-weight:bold"><%= NumberOfElements %></span>
         <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_163,EntityName)) %></span>
 </div>
