using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_ViewsByDeviceType : System.Web.UI.Page
{
    private Dictionary<string, ListBox> _nodeListBoxes = new Dictionary<string, ListBox>();
    private Dictionary<string, ListBox> _intListBoxes = new Dictionary<string, ListBox>();

    protected override void OnInit(EventArgs e)
    {
		if (!Profile.AllowCustomize)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_317) }); 

        SetupNodeBoxes();
        SetupInterfaceBoxes();

        base.OnInit(e);
    }

    private void SetupNodeBoxes()
    {
        IEnumerable<NetObject> nodes = NetObjectFactory.GetAllObjects(typeof(Node));
        List<string> deviceTypes = new List<string>();
        foreach (NetObject obj in nodes)
        {
            Node myNode = (Node)obj;
            if (!deviceTypes.Contains(myNode.MachineType))
                deviceTypes.Add(myNode.MachineType);
        }

        deviceTypes.Sort();

        _nodeListBoxes.Clear();
        this.rptNodes.DataSource = deviceTypes;
        this.rptNodes.DataBind();
    }

    private void SetupInterfaceBoxes()
    {
        IEnumerable<NetObject> interfaces = NetObjectFactory.GetAllObjects(typeof(Interface));
		if (interfaces == null)
		{
			interfaceWrapper.Visible = false;
			return;
		}
        List<string> deviceTypes = new List<string>();
        foreach (NetObject obj in interfaces)
        {
            Interface myInterface = (Interface)obj;
            if (!deviceTypes.Contains(myInterface.InterfaceTypeDescription))
                deviceTypes.Add(myInterface.InterfaceTypeDescription);
        }

        deviceTypes.Sort();

        _intListBoxes.Clear();
        this.rptInterfaces.DataSource = deviceTypes;
        this.rptInterfaces.DataBind();
    }

    protected void NodeViewsDataBinding(object sender, EventArgs e)
    {
        ListBox lbxViews = (ListBox)sender;
        RepeaterItem container = (RepeaterItem)lbxViews.NamingContainer;
        int currentViewID = ViewsByDeviceTypeDAL.GetViewIDForNodeType(container.DataItem.ToString());

        if (lbxViews.Items.Count == 1)
        {
            foreach (ViewInfo vInfo in ViewManager.GetViewsByType("NodeDetails"))
            {
                ListItem item = new ListItem(vInfo.ViewTitle, vInfo.ViewID.ToString());
                
                if (item.Value.Equals(currentViewID.ToString()))
                    item.Selected = true;

                lbxViews.Items.Add(item);
            }
        }
        _nodeListBoxes.Add(container.DataItem.ToString(), lbxViews);
    }

    protected void InterfaceViewsDataBinding(object sender, EventArgs e)
    {
        ListBox lbxViews = (ListBox)sender;
        RepeaterItem container = (RepeaterItem)lbxViews.NamingContainer;
        int currentViewID = ViewsByDeviceTypeDAL.GetViewIDForInterfaceType(container.DataItem.ToString());

        if (lbxViews.Items.Count == 1)
        {
            foreach (ViewInfo vInfo in ViewManager.GetViewsByType("InterfaceDetails"))
            {
                ListItem item = new ListItem(vInfo.ViewTitle, vInfo.ViewID.ToString());

                if (item.Value.Equals(currentViewID.ToString()))
                    item.Selected = true;

                lbxViews.Items.Add(item);
            }
        }
        _intListBoxes.Add(container.DataItem.ToString(), lbxViews);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        foreach (KeyValuePair<string,ListBox> myPair in _nodeListBoxes)
        {
            ViewsByDeviceTypeDAL.SetViewIDForNodeType(myPair.Key, Convert.ToInt32(myPair.Value.SelectedValue));
        }

        foreach (KeyValuePair<string, ListBox> myPair in _intListBoxes)
        {
            ViewsByDeviceTypeDAL.SetViewIDForInterfaceType(myPair.Key, Convert.ToInt32(myPair.Value.SelectedValue));
        }

        Response.Redirect("/Orion/Admin/");
    }
}
