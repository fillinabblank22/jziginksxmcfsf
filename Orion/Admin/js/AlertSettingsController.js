﻿SW.Core.namespace("SW.Core.Admin").AlertSettingsController = function (config) {
    "use strict";
    // Private methods
    var validHostnameOrIpRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^(?:(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){6})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:::(?:(?:(?:[0-9a-fA-F]{1,4})):){5})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){4})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,1}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){3})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,2}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){2})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,3}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:[0-9a-fA-F]{1,4})):)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,4}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,5}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,6}(?:(?:[0-9a-fA-F]{1,4})))?::))))$";
    var numericStringRegex = /^\d+$/;
    var isPasswordChanged = false;

    function validateConfiguration(configuration) {
        var isValid = true;

        $container.find('[data-form="smtpServerIP"]').removeClass("invalidValue");
        $container.find('[data-form="smtpServerPort"]').removeClass("invalidValue");
        $container.find('[data-form="smtpServerUsername"]').removeClass("invalidValue");
        $container.find('[data-form="smtpServerPasswordConfirm"]').removeClass("invalidValue");

        $container.find('[data-form="emailTo"]').removeClass("invalidValue");
        $container.find('[data-form="emailFrom"]').removeClass("invalidValue");
        $container.find('[data-form="emailMailCc"]').removeClass("invalidValue");

        if (validateEmailsInput(configuration.EmailTo, $container.find('[data-form="emailTo"]').parent().find(".emailInputField")) === false) {
            isValid = false;
        }
        if (validateEmailsInput(configuration.EmailFrom, $container.find('[data-form="emailFrom"]').parent().find(".emailInputField")) === false) {
            isValid = false;
        }
        if (configuration.EmailCC != '') {
            if (validateEmailsInput(configuration.EmailCC, $container.find('[data-form="emailMailCc"]').parent().find(".emailInputField")) === false) {
                isValid = false;
            }
        }
        if (configuration.EmailBCC != '') {
            if (validateEmailsInput(configuration.EmailBCC, $container.find('[data-form="emailBcc"]').parent().find(".emailInputField")) === false) {
                isValid = false;
            }
        }

        if (configuration.EmailSmtpAddress != "") {
            if (configuration.EmailSmtpAddress.match(validHostnameOrIpRegex) == null) {
                isValid = false;
                $container.find('[data-form="smtpServerIP"]').addClass("invalidValue");
            }
        } else {
            isValid = false;
            $container.find('[data-form="smtpServerIP"]').addClass("invalidValue");
        }
        if (configuration.EmailSmtpPort != "" && parseInt(configuration.EmailSmtpPort, 10) != "NaN" && parseInt(configuration.EmailSmtpPort, 10) <= 65535) {
            if (!numericStringRegex.test(configuration.EmailSmtpPort)) {
                isValid = false;
                $container.find('[data-form="smtpServerPort"]').addClass("invalidValue");
            }
        } else {
            isValid = false;
            $container.find('[data-form="smtpServerPort"]').addClass("invalidValue");
        }
        if (configuration.Credentials) {
            if (configuration.Credentials.EmailUserName == "") {
                isValid = false;
                $container.find('[data-form="smtpServerUsername"]').addClass("invalidValue");
            }
            if (configuration.Credentials.EmailPassword != $container.find('[data-form="smtpServerPasswordConfirm"]').val()) {
                isValid = false;
                $container.find('[data-form="smtpServerPasswordConfirm"]').addClass("invalidValue");
            }
        }
        return isValid;
    }

    function isEmail(email) {
        var regex = /.+@.+/;
        return regex.test(email);
    }

    function validateEmailsInput(value, elem) {
        var validationResult = true;
        $.each(value.split(/,|;(?!M=)/), function () {
            if (!isEmail($.trim(this))) {
                validationResult = false;
                elem.addClass("invalidValue");
            }
        });
        return validationResult;
    }

    //Enable/Disable Smtp
    function enableDisableCredentials() {
        var input = $container.find('[data-form="smtpServerUseCredentials"]').find('input[type="checkbox"]');
        if (input.is(':checked')) {
            $container.find('[data-form="smtpServerCredsContainer"]').show();
            $container.find('input[type=password][data-form^="smtpServerPassword"]').focus(function () {
                handlePassword();
            });
        } else {
            $container.find('[data-form="smtpServerCredsContainer"]').hide();
        }
    }

    // after first clicking into pre-filled psw or confirm psw input field its content disappear
    function handlePassword() {
        if (!isPasswordChanged) {
            isPasswordChanged = true;
            $container.find('[data-form="isPswChanged"]').val("true");
            $container.find('[data-form="smtpServerPasswordConfirm"]').val("");
            $container.find('[data-form="smtpServerPassword"]').val("");
        }
    }

    function testSmtpCreadentials() {
        var configuration = createConfiguration();
        $testSmtpSettings(configuration.EmailFrom, configuration.EmailTo, configuration.EmailCC, configuration.EmailBCC);
    };

    // Constructor
    this.init = function () {
        $container = $('#' + config.containerID);
        enableDisableCredentials();
        $testSmtpSettings = SW.Core.SMTPServerSettingsController.testSmtpCreadentials;
        SW.Core.SMTPServerSettingsController.testSmtpCreadentials = testSmtpCreadentials;
    };

    var createConfiguration = function () {
        var configuration = {
            EmailTo: $container.find('[data-form="emailTo"]').val(),
            EmailFrom: $container.find('[data-form="emailFrom"]').val(),
            EmailCC: $container.find('[data-form="emailCc"]').val(),
            EmailBCC: $container.find('[data-form="emailBcc"]').val(),
            EmailSenderName: $container.find('[data-form="emailSenderName"]').val(),
            EmailSmtpAddress: $container.find('[data-form="smtpServerIP"]').val(),
            EmailSmtpPort: $container.find('[data-form="smtpServerPort"]').val(),
            EmailSmtpSSL: $container.find('[data-form="smtpServerEnableSSL"] input[type=checkbox]').is(':checked'),
        };
        if ($container.find('[data-form="smtpServerUseCredentials"] input[type=checkbox]').is(':checked')) {
            configuration.Credentials = {
                EmailUserName: $container.find('[data-form="smtpServerUsername"]').val(),
                EmailPassword: $container.find('[data-form="smtpServerPassword"]').val()
            };
        }

        return configuration;
    };

    // Public methods
    this.validate = function () {
        var configuration = createConfiguration();
        var isValid = validateConfiguration(configuration);
        return isValid;
    };

    var self = this;
    var $container = null;
    var $testSmtpSettings = null;
}
