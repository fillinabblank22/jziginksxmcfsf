Ext.grid.EditorPasteCopyGridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
    clicksToEdit: 'auto',
    stopAutoEditing: true,
    rowContextMenu: null,
    initComponent: function () {
        Ext.grid.EditorPasteCopyGridPanel.superclass.initComponent.call(this);
        this.selModel = new Ext.grid.ExcelCellSelectionModel(); //make sure that selection modal is ExcelCellSelectionModel
        this.addListener('render', this.addKeyMap, this);
        var thisGrid = this;
        rowContextMenu = new Ext.menu.Menu({
            items: [{
                text: '<table><tr><td>@{R=Core.Strings;K=WEBJS_PS0_3;E=js}</td><td style="text-align: right;width:50px;">Ctrl+C</td></tr></table>',
                iconCls: 'menu_copy_icon',
                cls: 'x-menu-item-table',
                handler: function () {
                    this.parentMenu.hide();
                    thisGrid.copyToClipBoard(thisGrid.getSelectionModel().getSelectedCellRange());

                }
            }, {
                text: '<table><tr><td>@{R=Core.Strings;K=WEBJS_SO0_51;E=js}</td><td style="text-align: right;width:50px;">Ctrl+X</td></tr></table>',
                iconCls: 'menu_cut_icon',
                cls: 'x-menu-item-table',
                handler: function () {
                    this.parentMenu.hide();
                    var selection = thisGrid.getSelectionModel().getSelectedCellRange();
                    thisGrid.copyToClipBoard(selection);
                    thisGrid.clearSelected(selection);
                }
            },
             {
                 text: '<table><tr><td>@{R=Core.Strings;K=WEBJS_SO0_52;E=js}</td><td style="text-align: right;width:50px;">Ctrl+V</td></tr></table>',
                 iconCls: 'menu_paste_icon',
                 cls: 'x-menu-item-table',
                 handler: function () {
                     this.parentMenu.hide();
                     thisGrid.updateGridData();
                 }
             }, '-',
            {
                text: '<table><tr><td>@{R=Core.Strings;K=WEBJS_SO0_53;E=js}</td><td style="text-align: right;width:50px;">Delete</td></tr></table>',
                iconCls: 'menu_clear_icon',
                cls: 'x-menu-item-table',
                handler: function () {
                    this.parentMenu.hide();
                    thisGrid.clearSelected(thisGrid.getSelectionModel().getSelectedCellRange());
                }
            }
            ]
        });
        this.addListener('rowcontextmenu', this.showContextMenu, this);
    },
    showContextMenu: function (grid, index, event) {
        var selected = this.getSelectionModel().getSelectedCellRange();
        if (selected.length != 4 || (selected[0] == 0 && selected[1] == 0 && selected[2] == 0 && selected[3] == 0)) return;
        event.stopEvent();
        rowContextMenu.showAt(event.xy);
    },
    formatDateTime: function (value) {
        if (!value || value.length === 0)
            return '';

        var a = this.reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat('s');
        }

        var b = new Date(value);
        if (b) {
            return b.localeFormat('s');
        }

        return '';
    },
    convertToJSONFormat: function (value) {
        var dateTimeParts = value.split("T");
        if (dateTimeParts.length != 2) {
            dateTimeParts = value.split(" ");
        }

        if (dateTimeParts.length == 2) {
            var dateParts = dateTimeParts[0].split("-");
            var timeParts = dateTimeParts[1].split(":");

            if (dateParts.length == 3 && timeParts.length == 3) {
                var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2], timeParts[0], timeParts[1], timeParts[2]);
                return '\/Date(' + date.getTime() + ')\/';
            }
        }
        return value;
    },
    reMsAjax: /^\/Date\((d|-|.*)\)[\/|\\]$/,
    reISO: /^(\d{4})-(\d{2})-(\d{2})T|[ ](\d{2}):(\d{2}):(\d{2})$/,
    addKeyMap: function () {
        var thisGrid = this;
        this.body.on("mouseover", this.onMouseOver, this);
        this.body.on("mouseup", this.onMouseUp, this);
        // map multiple keys to multiple actions by strings and array of codes	 	
        return new Ext.KeyMap(Ext.DomQuery.selectNode('div[class*=x-grid3-scroller]', this.getEl().dom).id, [
        {
            key: "c",
            ctrl: true,
            fn: function () {
                thisGrid.copyToClipBoard(thisGrid.getSelectionModel().getSelectedCellRange());
            }
        },
        {
            key: Ext.EventObject.INSERT,
            ctrl: true,
            fn: function () {
                thisGrid.copyToClipBoard(thisGrid.getSelectionModel().getSelectedCellRange());
            }
        }
        ,
        {
            key: Ext.EventObject.INSERT,
            shift: true,
            fn: function () {
                thisGrid.updateGridData();
            }
        },
        {
            key: "v",
            ctrl: true,
            fn: function () {
                thisGrid.updateGridData();
            }
        },
        {
            key: "x",
            ctrl: true,
            fn: function () {
                var selection = thisGrid.getSelectionModel().getSelectedCellRange();
                thisGrid.copyToClipBoard(selection);
                thisGrid.clearSelected(selection);
            }
        }
        ]);
    },
    onMouseOver: function (e) {
        this.processEvent("mouseover", e);
    },
    onMouseUp: function (e) {
        this.processEvent("mouseup", e);
    },
    setClipboardData: function (format, data) {
        if (window.clipboardData && clipboardData.setData && clipboardData.getData) {
            clipboardData.setData(format, data);
        } else {
            var hiddentextarea = this.getHiddenTextArea();
            hiddentextarea.dom.value = data;
            hiddentextarea.focus();
        }
    },
    getClipboardData: function () {
        if (window.clipboardData && clipboardData.setData && clipboardData.getData) {
            return clipboardData.getData("text").split("\n");
        }
        return (this.hiddentextarea) ? this.hiddentextarea.getValue().split("\n") : [];
    },
    copyToClipBoard: function (rows) {
        this.collectGridData(rows);
        this.setClipboardData("text", this.tsvData);
    },
    collectGridData: function (cr) {
        var row1 = cr[0], col1 = cr[1] - 1, row2 = cr[2], col2 = cr[3] - 1;
        this.tsvData = "";
        var rowTsv = "";
        for (var r = row1; r <= row2; r++) {
            if (r > row1) {
                this.tsvData += "\n";
            }
            rowTsv = "";
            for (var c = col1; c <= col2; c++) {
                if (c > col1) {
                    rowTsv += "\t";
                }
                var cellValue = this.store.getAt(r).get(this.colModel.config[c + 1].dataIndex);
                rowTsv += (cellValue == null ? "" : (this.reMsAjax.test(cellValue) ? this.formatDateTime(cellValue) : ((cellValue instanceof Date) ? cellValue.localeFormat('s') :
                (typeof cellValue == "number" ? String(cellValue).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : cellValue))));
            }
            this.tsvData += rowTsv;
        }
        return this.tsvData;
    },
    updateGridData: function () {
        var tsvData = this.getClipboardData();
        if (tsvData == null || tsvData.length == 0)
            return; //nothing selected to paste to
        var cr = this.getSelectionModel().getSelectedCellRange();
        if (cr == null || cr.length == 0)
            return; //nothing selected to paste to
        var nextIndex = cr[0];
        if (tsvData[0].split("\t").length == 1 && tsvData.length == 1) { //one cell selected
            for (var rowIndex = cr[0]; rowIndex <= cr[2]; rowIndex++) {
                for (var columnIndex = cr[1] - 1; columnIndex <= cr[3] - 1; columnIndex++) {
                    var event = {
                        field: this.colModel.config[columnIndex + 1].dataIndex,
                        value: this.store.getAt(rowIndex).get(this.colModel.config[columnIndex + 1].dataIndex),
                        cancel: false,
                        cellType: null
                    };
                    this.initialConfig.listeners.beforeedit(event);
                    event.value = tsvData[0].trim();
                    this.initialConfig.listeners.validateedit(event);
                    if (!event.cancel) {
                        this.store.getAt(rowIndex).set(this.colModel.config[columnIndex + 1].dataIndex, (event.cellType == 'date' && this.reISO.test(tsvData[0].trim()) ? this.convertToJSONFormat(tsvData[0].trim()) : tsvData[0].trim()));
                        var value = this.store.getAt(rowIndex).get(this.colModel.config[columnIndex + 1].dataIndex);
                        var changedEvent = {
                            grid: this,
                            record: this.store.getAt(rowIndex),
                            field: this.colModel.config[columnIndex + 1].dataIndex,
                            originalValue: null,
                            value: (this.reMsAjax.test(value) ? this.formatDateTime(value) : value),
                            row: rowIndex,
                            column: columnIndex,
                            cellType: event.cellType
                        };
                        this.initialConfig.listeners.afteredit(changedEvent);
                    }
                }
            }
        } else {
            var columns = [];
            var gridTotalRows = this.store.getCount();
            for (var rowIndex = 0; rowIndex < tsvData.length; rowIndex++) {
                columns = tsvData[rowIndex].split("\t");
                if (nextIndex > gridTotalRows - 1)
                    break;
                var pasteColumnIndex = cr[1] - 1;
                for (var columnIndex = 0; columnIndex < columns.length; columnIndex++) {
                    var event = {
                        field: this.colModel.config[pasteColumnIndex + 1].dataIndex,
                        value: this.store.getAt(nextIndex).get(this.colModel.config[pasteColumnIndex + 1].dataIndex),
                        cancel: false,
                        cellType: null
                    };
                    this.initialConfig.listeners.beforeedit(event);
                    event.value = columns[columnIndex].trim();
                    this.initialConfig.listeners.validateedit(event);
                    if (!event.cancel) {
                        this.store.getAt(nextIndex).set(this.colModel.config[pasteColumnIndex + 1].dataIndex, (event.cellType == 'date' && this.reISO.test(columns[columnIndex].trim()) ? this.convertToJSONFormat(columns[columnIndex].trim()) : columns[columnIndex].trim()));
                        var value = this.store.getAt(nextIndex).get(this.colModel.config[pasteColumnIndex + 1].dataIndex);
                        var changedEvent = {
                            grid: this,
                            record: this.store.getAt(rowIndex),
                            field: this.colModel.config[pasteColumnIndex + 1].dataIndex,
                            originalValue: null,
                            value: (this.reMsAjax.test(value) ? this.formatDateTime(value) : value),
                            row: nextIndex,
                            column: pasteColumnIndex,
                            cellType: event.cellType
                        };
                        this.initialConfig.listeners.afteredit(changedEvent);
                    }
                    pasteColumnIndex++;
                }
                nextIndex++;
            }
        }
        if (this.hiddentextarea)
            this.hiddentextarea.blur();
        this.getView().focusEl.focus();
    },
    getHiddenTextArea: function () {
        if (!this.hiddentextarea) {
            this.hiddentextarea = new Ext.Element(document.createElement('textarea'));
            this.hiddentextarea.setStyle('border', '2px solid #ff0000');
            this.hiddentextarea.setStyle('position', 'absolute');
            this.hiddentextarea.setStyle('z-index', '-1');
            this.hiddentextarea.setStyle('width', '100px');
            this.hiddentextarea.setStyle('height', '30px');
            Ext.get(this.getEl().dom.firstChild).appendChild(this.hiddentextarea.dom);
        }
        return this.hiddentextarea;
    }
});
Ext.reg('editorPasteCopyGrid', Ext.grid.EditorPasteCopyGridPanel);