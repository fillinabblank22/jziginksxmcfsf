﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
SW.Orion.SmtpServersManager = function () {

    var selectorModel;
    var pageSizeBox;
    var sortOrder;
    var dataStore;
    var grid;
    var userPageSize;
    var selectedColumns;
    var $container;
    var title;
    var serverType = {
        Main: 0,
        Backup: 1
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;
        var needsToDisable = (selCount === 0);

        map.Edit.setDisabled(selCount != 1);
        map.MakeDefault.setDisabled(selCount != 1);
        map.Delete.setDisabled(needsToDisable);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: "" };

        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    };

    function setMainGridHeight(pageSize) {
        window.setTimeout(function () {
            var mainGrid = Ext.getCmp('mainGrid');
            var maxHeight = calculateMaxGridHeight(mainGrid);
            var calculated = calculateGridHeight(pageSize);
            var height = (calculated > 0) ? Math.min(calculated, maxHeight) : maxHeight;
            setHeightForGrids(height);

            //we need this to add height if horizontal scroll bar is present, with low window width and in chrome
            if (grid.el.child(".x-grid3-scroller").dom.scrollWidth != grid.el.child(".x-grid3-scroller").dom.clientWidth) {
                height = Math.min(height + 20, maxHeight);
                setHeightForGrids(height);
            }
        }, 0);
    }

    function selectCreatedServer(res) {
        var startFrom = 0;
        var objectIndex = res.ind;

        if (objectIndex > -1) {
            var pageNumFl = objectIndex / userPageSize;
            var index = parseInt(pageNumFl.toString());
            startFrom = index * userPageSize;
        }
        refreshGridObjects(startFrom, userPageSize, grid, "", "", "", "", function () {
            if (objectIndex > -1) {
                grid.getView().focusRow(objectIndex % userPageSize);
                var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                rowEl.scrollIntoView(grid.getGridEl().id, false);
                grid.getSelectionModel().selectRow(objectIndex % userPageSize);
            }
        });
    }
    function getServerRowNumber(sId, succeeded) {
        if (!sId) {
            succeeded({ ind: -1 });
            return;
        }

        var sort = "";
        var dir = "";

        if (sortOrder != null) {
            sort = sortOrder[0];
            dir = sortOrder[1];
        }

        SW.Core.Services.callWebServiceSync('/Orion/Services/SMTPServers.asmx', 'GetSMTPServerNumber', { serverId: sId, sort: sort, direction: dir },
            function (result) {
                if (succeeded != null) {
                    succeeded({ ind: result });
                    return;
                }
                // not found
                succeeded({ ind: -1 });
                return;
            });
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp('mainGrid');

        mainGrid.setHeight(Math.max(height, 300));
        mainGrid.doLayout();
    }

    function calculateGridHeight(numberOfRows) {
        if (grid.store.getCount() == 0)
            return 0;
        var rowsHeight = Ext.fly(grid.getView().getRow(0)).getHeight() * (numberOfRows + 1);
        return grid.getHeight() - grid.getInnerHeight() + rowsHeight + 7;
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.getPosition()[1];
        return $(window).height() - gridTop - $('#footer').height() - 25;
    }

    var addSmtpServer = function () {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_SmtpServersManager_AddSMTPServer", this);
            return;
        }

        loadControl({ 'Mode': 'New' });

        title = "@{R=Core.Strings;K=WEBJS_VL0_20; E=js}";

        createButtons(function () {
            if (SW.Core.SMTPServerSettingsController.validate()) {
                var smtpServer = SW.Core.SMTPServerSettingsController.getSmtpServerAsync();
                ORION.callWebService("/Orion/Services/SMTPServers.asmx",
                    "AddSmtpServer", { smtpServer: smtpServer },
                    function (result) {
                        destroyDialog();
                        getServerRowNumber(result, selectCreatedServer);
                    });
            }
        });
    };

    var createButtons = function (saveOnClick) {

        var $buttonsContainer = $('#smtpSettingsButtons', $container);

        $($buttonsContainer).css('text-align', 'right');

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_27;E=js}', { type: 'primary', id: 'createSMTPServer' }))
             .appendTo($buttonsContainer)
             .css('margin', '10px')
             .click(saveOnClick).show();

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_22;E=js}', { type: 'secondary', id: 'cancelCreateSMTPServer' }))
             .appendTo($buttonsContainer)
             .css('margin', '10px 0 10px 10px')
             .on('click', destroyDialog).show();
    };

    var showDialog = function () {
        $("#addItemDialog").dialog({
            resizable: false,
            width: 410,
            modal: true,
            title: title
        });
    };

    var destroyDialog = function () {
        $container.dialog("destroy");
    };

    var loadControl = function (config) {
        $container = $('#addItemDialog');
        config.OnReadyJsCallback = 'SW.Orion.SmtpServersManager.ControlReady';
        $container.accordion('destroy').empty().append("<div class='smtpSettingsControl' ><div id='smtpSettingsHolder' /><div id='smtpSettingsButtons' /></div>");
        SW.Core.Loader.Control('#smtpSettingsHolder', {
            Control: '~/Orion/Controls/SmtpSettingsControl.ascx'
        }, {
            'config': config
        }
        );
    };

    var editSmtpServer = function (type) {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_SmtpServersManager_EditSMTPServer", this);
            return;
        }

        var selectedId = getSelectedSmtpServerIds(grid.getSelectionModel().getSelections(), type)[0];

        loadControl({ 'SMTPServerID': selectedId, 'Mode': 'Edit' });

        title = "@{R=Core.Strings;K=WEBJS_VL0_19; E=js}";
        createButtons(function () {
            if (SW.Core.SMTPServerSettingsController.validate()) {
                var smtpServer = SW.Core.SMTPServerSettingsController.getSmtpServerAsync();
                smtpServer.ServerID = selectedId;
                ORION.callWebService("/Orion/Services/SMTPServers.asmx",
                    "UpdateSmtpServer", { smtpServer: smtpServer },
                    function (result) {
                        if (result) {
                            destroyDialog();
                            getServerRowNumber(selectedId, selectCreatedServer);
                        } 
                    });
            }
        });
    };

    var makeDefaultSmtpServer = function () {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_SmtpServersManager_MakeDefault", this);
            return;
        }
        var selectedId = getSelectedSmtpServerIds(grid.getSelectionModel().getSelections())[0];
        ORION.callWebService("/Orion/Services/SMTPServers.asmx",
            "SetSmtpServerAsDefault", { id: selectedId }, function () { getServerRowNumber(selectedId, selectCreatedServer); });
    };

    var getSelectedSmtpServerIds = function (items, type) {
        var ids = [];
        Ext.each(items, function (item) {
            switch (type) {
                case serverType.Backup:
                    ids.push(item.data.BackupServerID);
                    break;
                case serverType.Main:
                default:
                    ids.push(item.data.SMTPServerID);
                    break;
            }
        });
        return ids;
    };

    var deleteSelectedItems = function (items, onSuccess) {
        var toDelete = getSelectedSmtpServerIds(items);
        var hasDefaultServer = containsDefaultServer(items);
       
        var waitMsg;

        ORION.callWebService("/Orion/Services/SMTPServers.asmx",
            "GetUsedSmtpServers", { ids: toDelete },
            function (result) {
                var smtpAddressList;
                var resultDictionary = JSON.parse(result);
                if (resultDictionary.length > 0) {
                    smtpAddressList = '@{R=Core.Strings;K=WEBJS_TD0_8; E=js}' + "<br/>";
                    if (hasDefaultServer === true) {
                        smtpAddressList = '@{R=Core.Strings;K=WEBJS_TD0_15; E=js}' + "<br/>" + '@{R=Core.Strings;K=WEBJS_TD0_8; E=js}' + "<br/>";
                    }
                    for (var i = 0; i < resultDictionary.length; i++) {
                        smtpAddressList += String.format('@{R=Core.Strings;K=WEBJS_AY0_19; E=js}', resultDictionary[i]);
                        smtpAddressList += "<br/>";
                    }
                    Ext.Msg.show({
                        title: "@{R=Core.Strings;K=WEBJS_TD0_7; E=js}",
                        msg: smtpAddressList,
                        minWidth: 500,
                        buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                        icon: Ext.MessageBox.QUESTION,
                        fn: function (btn) {
                            if (btn == 'yes') {
                                waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TD0_9; E=js}");
                                deleteSmtpServers(toDelete, function (deleteResult) {
                                    waitMsg.hide();
                                    onSuccess(deleteResult);
                                });
                            }
                        }
                    });
                } else if (resultDictionary.length == 0) {
                    var confirmMsg = "@{R=Core.Strings;K=WEBJS_TD0_8; E=js}"
                    if (hasDefaultServer === true) {
                        confirmMsg = '@{R=Core.Strings;K=WEBJS_TD0_15; E=js}' + "<br/>" + '@{R=Core.Strings;K=WEBJS_TD0_8; E=js}';
                    }
                    Ext.Msg.confirm("@{R=Core.Strings;K=WEBJS_TD0_7; E=js}", confirmMsg,
                        function (btn) {
                            if (btn == 'yes') {
                                waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TD0_9; E=js}");
                                deleteSmtpServers(toDelete, function (deleteResult) {
                                    waitMsg.hide();
                                    onSuccess(deleteResult);
                                });
                            }
                        });
                }
            });
    };

    function containsDefaultServer(records) {
        var hasDefaultServer = false;
        Ext.each(records, function (record) {
            if (record.get("IsDefault") === true) {
                hasDefaultServer = true;
                return;
            }
        });
        return hasDefaultServer;
    }

    function deleteSmtpServers(ids, onSuccess) {
        ORION.callWebService("/Orion/Services/SMTPServers.asmx",
            "DeleteSmtpServer", { ids: ids },
            function (result) {
                onSuccess(result);
            });
    }

    var renderBackupServer = function (value, meta, record) {
        if (value == 0) {
            return '@{R=Core.Strings;K=WEBJS_TD0_13; E=js}';
        } else {
            return String.format('<a href="#">{0}</a>', record.data.BackupServerAddress);
        }
    };

    var renderServerName = function (value, meta, record) {
        if (record.data.IsDefault == true)
            value = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', value, '@{R=Core.Strings;K=WEBJS_VL0_33; E=js}');
        return String.format('<a href="#">{0}</a>', value);
    };

    var renderBoolValue = function (value) {
        if (value == 0) {
            return "@{R=Core.Strings;K=WEBJS_TD0_11; E=js}";
        } else {
            return "@{R=Core.Strings;K=WEBJS_TD0_10; E=js}";
        }
    };

    ORION.prefix = 'SMTPServersManager_';
    return {
        ControlReady: function () {
            showDialog();
        },
        init: function () {

            userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);
            sortOrder = ORION.Prefs.load('SortOrder', 'Address, ASC').split(',');
            selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');

            dataStore = new ORION.WebServiceStore(
                                "/Orion/Services/SMTPServers.asmx/GetSMTPServers",
                                [
                                    { name: 'SMTPServerID', mapping: 0 },
                                    { name: 'Address', mapping: 1 },
                                    { name: 'Port', mapping: 2 },
                                    { name: 'CredentialID', mapping: 3 },
                                    { name: 'IsDefault', mapping: 4 },
                                    { name: 'EnabledSSL', mapping: 5 },
                                    { name: 'BackupServerID', mapping: 6 },
                                    { name: 'BackupServerAddress', mapping: 7 }
                                ]);

            dataStore.sortInfo = { field: sortOrder[0], direction: sortOrder[1] };

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                                setMainGridHeight(userPageSize);
                            }
                            else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;

                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                            setMainGridHeight(userPageSize);
                        }
                    }
                }
            });

            var pagingToolbar = new Ext.PagingToolbar(
                { id: 'gridPaging',
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_87; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

            grid = new Ext.grid.GridPanel({
                region: 'center',
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                split: true,
                stripeRows: true,
                trackMouseOver: false,
                columns: [selectorModel,
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'SMTPServerID' },
                    { header: '@{R=Core.Strings;K=WEBJS_TD0_2; E=js}', width: 250, sortable: true, hideable: false, dataIndex: 'Address', renderer: renderServerName },
                    { header: '@{R=Core.Strings;K=WEBJS_TD0_3; E=js}', width: 250, sortable: true, dataIndex: 'Port' },
                    { header: '@{R=Core.Strings;K=WEBJS_TD0_4; E=js}', width: 150, sortable: true, dataIndex: 'EnabledSSL', renderer: renderBoolValue },
                    { header: '@{R=Core.Strings;K=WEBJS_TD0_5; E=js}', width: 150, sortable: true, dataIndex: 'CredentialID', renderer: renderBoolValue },
                    { header: '@{R=Core.Strings;K=WEBJS_TD0_12; E=js}', width: 150, sortable: true, dataIndex: 'BackupServerID', renderer: renderBackupServer }
                ],
                sm: selectorModel, autoScroll: 'true', loadMask: true, width: 750, height: 350,
                tbar: [
                    { id: 'Add', text: '@{R=Core.Strings;K=WEBJS_TD0_6; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TD0_6; E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', handler: addSmtpServer }, '-',
                    { id: 'Edit', text: '@{R=Core.Strings;K=WEBJS_PS0_2; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_PS0_2; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler: editSmtpServer.createCallback(serverType.Main) }, '-',
                    {
                        id: 'MakeDefault', text: '@{R=Core.Strings;K=WEBJS_VL0_32; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_VL0_32; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon',
                        handler: makeDefaultSmtpServer
                    }, '-',
                    { id: 'Delete', text: '@{R=Core.Strings;K=WEBJS_PS0_4; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_PS0_4; E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', cls: 'x-btn-text-icon',
                        handler: function () {
                            if ($("#isOrionDemoMode").length != 0) {
                                demoAction("Core_SmtpServersManager_DeleteSMTPServer", this);
                                return;
                            }
                            deleteSelectedItems(grid.getSelectionModel().getSelections(), function () {
                                refreshGridObjects(0, userPageSize, grid, "", "", "", "", function () { });
                            });
                        }
                    }
                    ],
                bbar: pagingToolbar,
                listeners: {
                    cellclick: function (grid, rowIndex, columnIndex) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                        if (fieldName == 'BackupServerID') {
                            if (grid.getStore().getAt(rowIndex).get(fieldName) != 0) {
                                editSmtpServer(serverType.Backup);
                            }
                        }
                        if (fieldName == 'Address') {
                            editSmtpServer(serverType.Main);
                        }
                    }
                }
            });

            for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                if (selectedColumns.indexOf(grid.getColumnModel().getDataIndex(i)) > -1 && i > 1) {
                    grid.getColumnModel().setHidden(i, false);
                } else {
                    grid.getColumnModel().setHidden(i, true);
                }
            }

            grid.on('sortchange', function (grid, option) {
                var sort = option.field;
                var dir = option.direction;

                if (sort && dir) {
                    sortOrder[0] = sort;
                    sortOrder[1] = dir;
                    ORION.Prefs.save('SortOrder', sort + ',' + dir);
                }
            });

            grid.store.on('load', function () {
                grid.getEl().unmask();
                setMainGridHeight(userPageSize);

            });

            grid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                    if (!grid.getColumnModel().isHidden(i)) {
                        cols += grid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
                ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
            });

            var mainGridPanel = new Ext.Panel({ id: 'mainGrid', region: 'center', split: true, height: 380, layout: 'border', collapsible: false, items: [grid], cls: 'no-border' });
            mainGridPanel.render("SmtpServersManagerGrid");
            refreshGridObjects(0, userPageSize, grid, "", "", "", "", function () { });

            $(window).resize(function () {
                setMainGridHeight(userPageSize);
                mainGridPanel.doLayout();
            });
            updateToolbarButtons();
        }
    };

} ();
Ext.onReady(SW.Orion.SmtpServersManager.init, SW.Orion.SmtpServersManager);
