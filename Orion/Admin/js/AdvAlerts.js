Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

SW.Orion.AdvAlerts = function () {

    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    var isEditDialogShown = false;

    function renderDateTime(str, meta, record) {
        if (!str)
            return '@{R=Core.Strings;K=WEBJS_AK0_67; E=js}';

        a = reMsAjax.exec(str);
        if (a) {
            var b = a[1].split(/[,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        return '@{R=Core.Strings;K=WEBJS_AK0_68; E=js}';
    }

    function DateTimeFromJson(str) {
        if (!str)
            return null;

        a = reMsAjax.exec(str);
        if (a) {
            var b = a[1].split(/[,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortTimePattern);
        }
        return null;
    }

    GridColumnsToJson = function (columnmodel) {
        var cmconfig = columnmodel.config;
        var list = [];
        // skip checkbox and id columns
        for (var i = 2; i < cmconfig.length; i++) {
            var obj = {};
            obj.id = cmconfig[i].dataIndex;
            obj.w = cmconfig[i].width;
            obj.v = cmconfig[i].hidden == true ? 0 : 1;
            list.push(obj);
        }
        var jsonstate = Ext.util.JSON.encode(list);
        return jsonstate;
    };

    function renderImage(status, meta, record) {
        var imPath = "";
        var statVal = "";
        if (status) {
            imPath = "/Orion/images/Check.Green.gif";
            statVal = "@{R=Core.Strings;K=WEBJS_VB0_10; E=js}";
        }
        else {
            imPath = "/Orion/images/failed_16x16.gif";
            statVal = "@{R=Core.Strings;K=WEBJS_VB0_11; E=js}";
        }

        return "<img src='" + imPath + "' alt='' />" + statVal;
    }

    function renderInterval(str, meta, record) {
        if (!str)
            return null;

        return str + '@{R=Core.Strings;K=WEBJS_VB0_12; E=js}'
    }

    function renderName(value, meta, record) {
        return String.format('<a href="#" class="AlertName" value="{0}">{1}</a>', record.data.AlertDefID, Ext.util.Format.htmlEncode(value));
    }

    var getSelectedAlertIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.AlertDefID);
        });

        return ids;
    };

    editSelectedAlert = function (items) {
        var toEdit = getSelectedAlertIds(items);

        if ((!toEdit) || (toEdit.lenght == 0))
            return;

        var id = "";
        id = toEdit[0];

        if (id == "")
            return false;

        editAlert(id, function (results) {
            var row = results.Rows[0];
            fillDialog(results);
            showDialog("@{R=Core.Strings;K=WEBJS_VB0_13; E=js}" + row[1]);
        }, function (error) {
        });

        return false;
    };

    var editAlert = function (id, onSuccess) {
        ORION.callWebService("/Orion/Services/AlertsAdmin.asmx",
                             "GetAdvAlert", { alertDefID: id },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    var deleteSelectedItems = function (items) {
        var toDelete = getSelectedAlertIds(items);
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_VB0_14; E=js}");

        deleteAlerts(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            grid.store.reload();
            selectAll = false;
        });
    };

    var toggleSelectedItems = function (items, val) {
        var toToggle = getSelectedAlertIds(items);
        var waitMsg;
        if (val == true)
            waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_VB0_15; E=js}");
        else
            waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_VB0_16; E=js}");

        toggleAlerts(toToggle, val, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            grid.store.reload();
            selectAll = false;
        });
    };

    var toggleAlerts = function (ids, val, onSuccess) {
        ORION.callWebService("/Orion/Services/AlertsAdmin.asmx",
                             "EnableAdvancedAlerts", { alertDefIds: ids, enable: val, enableAll: selectAll },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    var deleteAlerts = function (ids, onSuccess) {
        ORION.callWebService("/Orion/Services/AlertsAdmin.asmx",
                             "RemoveAdvancedAlerts", { alertDefIds: ids, deleteAll: selectAll },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    gridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridAl = component.getColumnModel();
        var settingName = 'Columns';

        if (gridAl != null && settingName != '') {
            var jsonvalue = GridColumnsToJson(gridAl);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var tPanel = grid.getTopToolbar();
        var map = tPanel.items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.Edit.setDisabled(needsOnlyOneSelected);
        map.Enable.setDisabled(needsAtLeastOneSelected);
        map.Disable.setDisabled(needsAtLeastOneSelected);
        map.Delete.setDisabled(needsAtLeastOneSelected);
        if (selCount === 0 && selectAll) {
            grid.getSelectionModel().selectAll();
            selectAll = true;
            return;
        }

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        }
        else {
            hd.removeClass('x-grid3-hd-checker-on');
            selectAll = false;
        }

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.Enable.setDisabled(true);
            map.Disable.setDisabled(true);
            map.Delete.setDisabled(true);
        }
    };

    //Error handler
    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    ////////////////////////////////////////////////EditAlertDialog//////////////////////////////////////////////////////////
    var fillDialog = function (alertDefs) {
        var alertDef = alertDefs.Rows[0];
        var splitMarker = '/*SplitMarker*/';
        $("#alertGuid").val(alertDef[0]);
        $("#alertName").val(alertDef[1]);
        $("#alertDescription").val(alertDef[2]);
        if (alertDef[6]) {
            $("#enabled [value='1']").attr("selected", "selected");
        } else {
            $("#enabled [value='0']").attr("selected", "selected");
        }

        if (alertDef[9]) {
            $("#ignoreTimeout [value='1']").attr("selected", "selected");
        } else {
            $("#ignoreTimeout [value='0']").attr("selected", "selected");
        }

        var interval = parseInt(alertDef[8]);

        if (interval % 3600 == 0) {
            interval = interval / 3600;
            $("#timeFrame [value='3600']").attr("selected", "selected");
        }
        else if (interval % 60 == 0) {
            interval = interval / 60;
            $("#timeFrame [value='60']").attr("selected", "selected");
        }
        else {
            $("#timeFrame [value='1']").attr("selected", "selected");
        }

        //$("#ignoreTimeout").attr('checked', 'checked');
        $("#executeInterval").val(interval.toString());
        $("#fromDate").val(alertDef[3] == null ? "@{R=Core.Strings;K=StatusDesc_Unknown; E=js}" : DateTimeFromJson(alertDef[3]));
        $("#toDate").val(alertDef[4] == null ? "@{R=Core.Strings;K=StatusDesc_Unknown; E=js}" : DateTimeFromJson(alertDef[4]));

        //readOnlyFields
        $("#triggerQuery").text(alertDef[10].replace(splitMarker, ' ')).attr('readonly', true);
        $("#resetQuery").text(alertDef[11].replace(splitMarker, ' ')).attr('readonly', true);
        $("#suppressionQuery").text(alertDef[12]).attr('readonly', true);

        if (alertDef[5] != null) {
            var days = alertDef[5].split(',');
            for (var i = 1; i < 8; i++) {
                $("#cbDOW" + i).attr('checked', false);
            }
            if (days.length > 0) {
                for (var i = 0; i < days.length; i++) {
                    $("#cbDOW" + days[i]).attr('checked', true);
                }
            }
        }
        var triggerActions = '';
        var taLine = 1;
        var raLine = 1;
        var resetActions = '';

        for (var y = 0; y < alertDefs.Rows.length; ++y) {
            var row = alertDefs.Rows[y];
            if (row[13] != null) {
                if (row[13]) {
                    triggerActions = triggerActions + taLine.toString() + ": " + row[16] + '<br/>';
                    taLine++;
                }
                else {
                    resetActions = resetActions + raLine.toString() + ": " + row[16] + '<br/>';
                    raLine++;
                }
            }
        }

        $("#triggerActions").html(triggerActions);
        $("#resetActions").html(resetActions);
    };

    var showDialog = function (title) {
        var encodedTitle = $("<div/>").text(title).html();
        // fix for FB#18508
        if (!isEditDialogShown) {
            isEditDialogShown = true;
            $("#editAlertDefDialog").show().dialog({
                width: 750, height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle, close: function () { isEditDialogShown = false; }, resizable: false
            }).css('overflow', 'hidden');
        }
    };

    var getDow = function () {
        var dow = "";
        for (var i = 1; i <= 7; i++) {
            if ($("#cbDOW" + i).is(':checked')) {
                if (dow.length > 0) dow += ",";
                dow += i;
            }
        }
        return dow;
    };

    var getEvalTime = function () {
        return $("#executeInterval").val() * $("#timeFrame").val();
    };

    //Ext grid
    ORION.prefix = "Orion_AdvAlerts_";
    var selectorModel;
    var dataStore;
    var grid;
    var selectAll = false;

    return {
        init: function () {

            $(".sw-pg-advdialogcancel-btn").click(function () {
                $("#editAlertDefDialog").dialog('close');
            });

            $(".sw-pg-advdialogdescr-btn").click(function () {
                $("#descrAlertDialog").dialog('close');
            });

            //dialog ok differs in demo mode
            if ($("#isOrionDemoMode").length != 0) {
                $(".sw-pg-advdialogok-btn").click(function () {
                    $("#editAlertDefDialog").dialog('close');
                });
            }
            else {
                $(".sw-pg-advdialogok-btn").click(function () {

                    //jquery dont want to format DT using generics normally for IT lang
                    //so have to workaround it
                    var dt1 = $.timePicker("#fromDate").getTime();
                    var fromTime = dt1.getHours() + ':' + dt1.getMinutes() + ':' + dt1.getSeconds();
                    var dt2 = $.timePicker("#toDate").getTime();
                    var toTime = dt2.getHours() + ':' + dt2.getMinutes() + ':' + dt2.getSeconds();

                    AlertsAdmin.UpdateAdvAlertDefinition($("#alertGuid").val(), $("#alertName").val(), $("#alertDescription").val(), ($("#enabled :selected").val() == 1) ? true : false,
                             getEvalTime(), getDow(), fromTime, toTime, ($("#ignoreTimeout :selected").val() == 1) ? true : false,
                             function () {
                                 $("#editAlertDefDialog").dialog("close");
                                 grid.store.reload();
                             },
                             function (error) {
                                 alert('@{R=Core.Strings;K=WEBJS_VB0_17; E=js}' + error.get_message() + '\n' + error.get_stackTrace());
                             });
                    return false;
                });
            }

            function showDescription(e) {
                var alertId = $(e.target).attr("value");
                editAlert(alertId, function (results) {
                    var row = results.Rows[0];

                    var descr = row[2];
                    var name = row[1];

                    $("#alertDescr")[0].innerHTML = "@{R=Core.Strings;K=WEBJS_VB0_18; E=js}" + descr;

                    $("#descrAlertDialog").show().dialog({
                        width: 480, height: 430, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: name
                    });

                }, function (error) {
                });
            }

            $("#Grid").click(function (e) {
                if ($(e.target).hasClass('AlertName'))
                    editSelectedAlert(grid.getSelectionModel().getSelections());
                return false;
                //if ($(e.target).hasClass('AlertName')) {
                //   showDescription(e);
                //   return false;
                // }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/Services/AlertsAdmin.asmx/GetAdvancedAlerts",
                                [
                                    { name: 'AlertDefID', mapping: 0 },
                                    { name: 'AlertName', mapping: 1 },
                                    { name: 'Enabled', mapping: 3 },
                                    { name: 'LastExecuteTime', mapping: 15 },
                                    { name: 'ExecuteInterval', mapping: 16 },
                                    { name: 'LastErrorTime', mapping: 19 }
                                            ],
                                "AlertName");

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=Core.Strings;K=WEBJS_VB0_19; E=js}',
                emptyMsg: "@{R=Core.Strings;K=WEBJS_VB0_20; E=js}",
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}"
            });

            var refreshObjects = function (property, value, search, callback) {
                grid.store.removeAll();
                grid.store.proxy.conn.jsonData = { property: property, value: value || "", search: search };
                currentSearchTerm = search;
                grid.store.load({ callback: callback });
            };

            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [selectorModel,
                 {header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'AlertDefID'},
                 {header: '@{R=Core.Strings;K=WEBJS_VB0_22; E=js}', width: 400, sortable: true, dataIndex: 'AlertName', renderer: renderName, hideable: false },
                      { header: '@{R=Core.Strings;K=WEBJS_VB0_23; E=js}',
                    //width: 50,
                          sortable: true, dataIndex: 'Enabled', renderer: renderImage},
                     {header: '@{R=Core.Strings;K=WEBJS_VB0_24; E=js}', width: 200, sortable: true, dataIndex: 'LastExecuteTime', renderer: renderDateTime },
                     {header: '@{R=Core.Strings;K=WEBJS_VB0_25; E=js}', width: 150, sortable: true, dataIndex: 'ExecuteInterval', renderer: renderInterval },
                     {header: '@{R=Core.Strings;K=WEBJS_VB0_26; E=js}', width: 200, sortable: true, dataIndex: 'LastErrorTime', renderer: renderDateTime}
                ],

                sm: selectorModel,
                autoScroll: 'true',
                loadMask: true,
                //title:'Manage advanced alerts',

                width: 1150,
                height: 500,
                layout: 'fit',
                stripeRows: true,
                trackMouseOver: false,

                tbar: [{
                    id: 'Edit',
                    text: '@{R=Core.Strings;K=CommonButtonType_Edit; E=js}',
                    tooltip: '@{R=Core.Strings;K=WEBJS_VB0_27; E=js}',
                    icon: '/Orion/Nodes/images/icons/icon_edit.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { editSelectedAlert(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'Enable',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_28; E=js}',
                    tooltip: '@{R=Core.Strings;K=WEBJS_VB0_29; E=js}',
                    icon: '/Orion/images/Check.Green.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { toggleSelectedItems(grid.getSelectionModel().getSelections(), true); }
                }, '-', {
                    id: 'Disable',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_30; E=js}',
                    tooltip: '@{R=Core.Strings;K=WEBJS_VB0_31; E=js}',
                    icon: '/Orion/images/failed_16x16.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { toggleSelectedItems(grid.getSelectionModel().getSelections(), false); }
                }, '-', {
                    id: 'Delete',
                    text: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}',
                    tooltip: '@{R=Core.Strings;K=WEBJS_VB0_32; E=js}',
                    icon: '/Orion/Nodes/images/icons/icon_delete.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        Ext.Msg.confirm("@{R=Core.Strings;K=WEBJS_VB0_33; E=js}", "@{R=Core.Strings;K=WEBJS_VB0_34; E=js}",
                                                    function (btn) {
                                                        if (btn == 'yes')
                                                            deleteSelectedItems(grid.getSelectionModel().getSelections());
                                                    });
                    }
                }, '-', {
                    id: 'SelectAll',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_36; E=js}',
                    tooltip: '@{R=Core.Strings;K=WEBJS_VB0_37; E=js}',
                    icon: '/Orion/images/Icon.SelectAll.gif',
                    cls: 'x-btn-text-icon',
                    //hidden: false,
                    handler: function () {
                        selectorModel.selectAll();
                        selectAll = true;
                    }
                }, '-', {
                    id: 'SelectNone',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_38; E=js}',
                    tooltip: '',
                    icon: '/Orion/images/Icon.SelectNone.gif',
                    cls: 'x-btn-text-icon',
                    //hidden: false,
                    handler: function () {
                        selectorModel.clearSelections();
                        selectAll = false;
                    }
                }
                ],
                bbar: pagingToolbar
            });

            grid.on("statesave", gridColumnChangedHandler);
            grid.render('Grid');

            // Set the width of the grid
            //grid.setWidth($('#gridCell').width());

            //disable refresh button in pagingToolbar
            //pagingToolbar.loading.hideParent = true;
            //pagingToolbar.loading.hide();

            updateToolbarButtons();
            refreshObjects("", "", "");
        }
    };
} ();

Ext.onReady(SW.Orion.AdvAlerts.init, SW.Orion.AdvAlerts);
