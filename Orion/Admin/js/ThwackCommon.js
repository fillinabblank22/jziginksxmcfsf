﻿Ext.namespace('SW.Core');
Ext.namespace('SW.Core.ThwackCommon');

(function (self, $, undefined) {
    var loginDialog;

    function onValidate() {
        loginDialog.buttons[0].setDisabled($("#thwackUsername").val() == "");
        return true;
    }
    function onCancel() {
        loginDialog.hide();
        loginDialog.destroy();
        loginDialog = null;
    }

    function callWebService(serviceUrl, method, username, password, callback) {
        SW.Core.Services.callWebService(
                serviceUrl, method, { username: username, password: password },
                function (result) {
                    if (typeof (callback) == 'function') {
                        if (callback({ username: username, password: password }, result)) {
                            onCancel();
                        }
                    } else {
                        onCancel();
                    }
                },
                function () {
                    Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_TM0_74;E=js}', msg: '@{R=Core.Strings;K=WEBJS_PS1_86;E=js}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    onCancel();
                }
            );
    }

    self.LoginToThwack = function(serviceUrl, method, defUsername, defPassword, callback) {
        loginToThwackInternal(defUsername, defPassword, function(username, password) {
            callWebService(serviceUrl, method, username, password, callback);
        });
    }

    self.LoginToThwackCustom = function(defUsername, defPassword, serviceCaller) {
        loginToThwackInternal(defUsername, defPassword, serviceCaller);
    }

    function loginToThwackInternal(defUsername, defPassword, serviceCaller) {
        function onLogin() {
            onValidate();
            if (loginDialog.buttons[0].disabled) {
                return;
            }
            var username = $("#thwackUsername").val();
            var password = $("#thwackPassword").val();
            serviceCaller(username, password, onCancel);
        }

        loginDialog = new Ext.Window({
            id: 'thwackLoginDlg',
            title: '@{R=Core.Strings;K=WEBJS_PS1_92;E=js}',
            width: 300,
            border: false,
            region: "center",
            layout: "fit",
            modal: true,
            resizable: false,
            bodyStyle: "padding:5px;",
            items: new Ext.form.FormPanel({
                id: 'thwackLoginForm',
                frame: true,
                labelWidth: 100,
                defaultType: 'textfield',
                labelAlign: 'right',
                autoHeight: true,
                items: [{
                    id: "thwackUsername",
                    fieldLabel: '@{R=Core.Strings;K=WEBJS_PS1_93;E=js}',
                    anchor: "100%",
                    selectOnFocus: true,
                    validator: onValidate,
                    value: defUsername,
                    style: "white-space: nowrap;"
                }, {
                    id: "thwackPassword",
                    inputType: "password",
                    fieldLabel: '@{R=Core.Strings;K=WEBJS_PS1_94;E=js}',
                    anchor: "100%",
                    selectOnFocus: true,
                    validator: onValidate,
                    value: defPassword
                }, {
                    xtype: 'label',
                    html: '<hr style="width:auto;color:#666;" />'
                }, {
                    xtype: 'label',
                    id: 'NormalText',
                        html: 'Don\'t have a thwack account? <a href="https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=Create_Thwack_User" target="_blank" rel="noopener noreferrer">» Create it now</a>'
                }]
            }),
            buttons: [{
                text: '@{R=Core.Strings;K=CommonButtonType_Submit;E=js}',
                disabled: true,
                handler: onLogin
            }, {
                text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}',
                handler: onCancel
            }]
        });


        loginDialog.show();
    };
}(SW.Core.ThwackCommon = SW.Core.ThwackCommon || {}, jQuery));