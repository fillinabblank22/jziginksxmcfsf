$(function () {
    var browseWidth = $('#' + getBtnBrowse()).outerWidth();
    $('#browseWrap').width(browseWidth);
    $('#browseWrapNoc').width(browseWidth);

    var uploadInput = $('#' + getFileUploadImage() + ' > div > input[type="file"]');
    uploadInput.width(browseWidth);

    var uploadInputNoc = $('#' + getFileUploadNocImage() + ' > div > input[type="file"]');
    uploadInputNoc.width(browseWidth);

    $('#' + getLogoCheckBoxID()).change(function () {
        if (this.checked) {
            HideShow($('#' + getLogoSectionID()), $('#' + getnoLogoSectionID()));
            HideShow($('#' + getLogoBlockFromUrlSectionID()), $('#' + getnoLogoSectionID()));
        } else {
            HideShow($('#' + getnoLogoSectionID()), $('#' + getLogoSectionID()));
            HideShow($('#' + getnoLogoSectionID()), $('#' + getLogoBlockFromUrlSectionID()));
        }
    });

    $('#' + getNocLogoCheckBoxID()).change(function () {
        if (this.checked) {
            HideShow($('#' + getNocLogoSectionID()), $('#' + getnoNocLogoSectionID()));
            HideShow($('#' + getNocLogoBlockFromUrlSectionID()), $('#' + getnoNocLogoSectionID()));
        } else {
            HideShow($('#' + getnoNocLogoSectionID()), $('#' + getNocLogoSectionID()));
            HideShow($('#' + getnoNocLogoSectionID()), $('#' + getNocLogoBlockFromUrlSectionID()));
        }
    });

    $('#' + getLogoCheckBoxFromUrlID()).change(function () {
        if (this.checked) {
            $('#' + getLogoFromUrInputlID()).css('display', 'block');
        }
        else {
            $('#' + getLogoFromUrInputlID()).css('display', 'none');
        }
    });

    $('#' + getNocLogoCheckBoxFromUrlID()).change(function () {
        if (this.checked) {
            $('#' + getNocLogoFromUrInputlID()).css('display', 'block');
        }
        else {
            $('#' + getNocLogoFromUrInputlID()).css('display', 'none');
        }
    });
});

function HideShow(section1, section2) {
    section1.css('display', 'block');
    section2.css('display', 'none');
}

function checkExtension(sender, args) {
    return checkExtensionGen(sender, args,
                $('#fileFormatError'),
                $('#' + getThrobberUploadID()),
                $('#' + getFileUploadImage() + ' > div > input[type="file"]'),
                $('#browseWrap').find('.sw-btn-t')
            );
}

function checkExtensionNoc(sender, args) {
    return checkExtensionGen(sender, args,
                $('#fileFormatErrorNoc'),
                $('#' + getThrobberUploadNocID()),
                $('#' + getFileUploadNocImage() + ' > div > input[type="file"]'),
                $('#browseWrapNoc').find('.sw-btn-t')
            );
}

function checkExtensionGen(sender, args, errorLbl, throbber, fileUploadImage, browseWrap) {
    var filename = args.get_fileName();
    var ext = filename.substring(filename.lastIndexOf(".") + 1);

    if ($.inArray(ext.toLowerCase(), extentions) <= -1) {
        errorLbl.css('display', 'block');

        throw new Error('@{R=Core.Strings;K=WEBJS_TM0_133;E=js}');
        return false;
    }

    errorLbl.css('display', 'none');
    throbber.css('display', 'block');
    fileUploadImage.css('display', 'none');
    browseWrap.css('color', 'silver');
    return true;
}

function uploadComplete(sender, args) {
    uploadCompleteGen(sender, args, $('#' + getThrobberUploadID()),
              $('#' + getFileUploadImage() + ' > div > input[type="file"]'),
              $('#browseWrap').find('.sw-btn-t'),
              $('#' + getFileNameSiteLogoID()),
              $('#' + getNotificationID())
            );
}

function uploadCompleteNoc(sender, args) {
    uploadCompleteGen(sender, args, $('#' + getThrobberUploadNocID()),
              $('#' + getFileUploadNocImage() + ' > div > input[type="file"]'),
              $('#browseWrapNoc').find('.sw-btn-t'),
              $('#' + getFileNameNocSiteLogoID()),
              $('#' + getNotificationNocID())
            );
}

function uploadCompleteGen(sender, args, throbber, fileUploadImage, browseWrap, filename, notification) {
    throbber.css('display', 'none');
    fileUploadImage.css('display', 'block');
    browseWrap.css('color', 'black');
    filename.text(args.get_fileName());
    notification.css('display', 'block');
}