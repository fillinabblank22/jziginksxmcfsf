﻿Ext.namespace("SW");
Ext.namespace("SW.Orion");
Ext.QuickTips.init();

SW.Orion.ServiceManager = function () {
    var serviceManagerServiceUrl = "/Orion/Admin/AdvancedConfiguration/Services/OrionServicesManagementService.asmx";
    var getServicesServiceName = "GetServices",
        getServicesStatusesServiceName = "GetServicesStatuses",
        startServicesServiceName = "StartList",
        stopServicesServiceName = "StopList",
        restartServicesServiceName = "RestartList";

    var selectAllCkBoxId = "selectAllCkBox_" + Ext.id(),
        startButtonId = "startButton_" + Ext.id(),
        stopButtonId = "stopButton_" + Ext.id(),
        restartButtonId = "restartButton_" + Ext.id();

    var checkBoxCheckedClass = "x-grid3-row-checker-checked",
        hiddenClass = "hidden";

    var notRunningSvcWarningMsgId,
        serviceErrorMsgId;

    var isDemoModeFlag;

    function isDemoMode() {
        return isDemoModeFlag != undefined
            ? isDemoModeFlag
            : $("#isOrionDemoMode").length !== 0;
    }

    function showWaring() {
        var warningMessageIdSelector = String.format("#{0}", notRunningSvcWarningMsgId);
        $(warningMessageIdSelector).removeClass(hiddenClass);
    }

    function hideWaring() {
        var warningMessageIdSelector = String.format("#{0}", notRunningSvcWarningMsgId);
        $(warningMessageIdSelector).addClass(hiddenClass);
    }

    function showError() {
        var errorMessageIdSelector = String.format("#{0}", serviceErrorMsgId);
        $(errorMessageIdSelector).removeClass(hiddenClass);
    }

    function hideError() {
        var errorMessageIdSelector = String.format("#{0}", serviceErrorMsgId);
        $(errorMessageIdSelector).addClass(hiddenClass);
    }

    function displayNameForStatus(status) {
        var displayName;
        switch (status) {
            case 1: // Continuing
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Continuing;E=js}";
                break;
            case 2: // Paused
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Paused;E=js}";
                break;
            case 3: // Pausing
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Pausing;E=js}";
                break;
            case 4: // Running
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Running;E=js}";
                break;
            case 5: // Starting
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Starting;E=js}";
                break;
            case 6: // Stopped
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Stopped;E=js}";
                break;
            case 7: // Stopping
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Stopping;E=js}";
                break;
            default: // Unknown
                displayName = "@{R=Core.Strings.2;K=OrionServiceManager_Status_Unknown;E=js}";
                break;
        }
        return displayName;
    };

    function adjustToolbarButtonsState(checkBoxSelectionModel) {
        var selection = checkBoxSelectionModel.selections.items;
        var startButton = Ext.getCmp(startButtonId),
            stopButton = Ext.getCmp(stopButtonId),
            restartButton = Ext.getCmp(restartButtonId);

        if (!isDemoMode()
			&& selection.length > 0
            && selection.some(function(item) {
                return item.json.Status === 2  // Paused
                    || item.json.Status === 4  // Running
                    || item.json.Status === 6; // Stopped
            })) {
            restartButton.enable();

            if (selection.some(function(item) {
                return item.json.Status === 2  // Paused
                    || item.json.Status === 6; // Stopped
            })) {
                startButton.enable();
            } else {
                startButton.disable();
            }

            if (selection.some(function(item) {
                return item.json.Status === 4; // Running
            })) {
                stopButton.enable();
            } else {
                stopButton.disable();
            }
        } else {
            startButton.disable();
            stopButton.disable();
            restartButton.disable();
        }
    }

    function isNotRunning(item) {
        return item.json.Type !== 1 // Website
            && [1, 3, 4, 7].indexOf(item.json.Status) === -1; // Continuing, Pausing, Running, Stopping
    }

    function createDataStore(serverHostname) {
        var dataStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: String.format("{0}/{1}", serviceManagerServiceUrl, getServicesServiceName),
                method: "POST"
            }),
            reader: new Ext.data.JsonReader(
                {
                    type: "json",
                    root: "d"
                },
                [
                    { name: "DisplayName", mapping: 0 },
                    { name: "Type", mapping: 1 },
                    { name: "operation" }
                ]
            )
        });

        dataStore.proxy.conn.jsonData = { serverHostname: serverHostname };

        return dataStore;
    }

    function createTabPanel(serviceManagerPlaceholderId, dataStore) {
        var servicesTabId = "servicesTab_" + Ext.id(),
            websitesTabId = "websitesTab" + Ext.id(),
            dependenciesTabId = "dependenciesTabId" + Ext.id();

        var tabPanelConfig = {
            width: 674,
            border: false,
            renderTo: serviceManagerPlaceholderId,
            defaults: {
                autoScroll: true
            },
            listeners: {
                "tabchange": function (tabPanel, tab) {
                    var startButton = Ext.getCmp(startButtonId),
                        stopButton = Ext.getCmp(stopButtonId);
                    if (startButton && stopButton) {
                        if (tab.id === websitesTabId || 
							tab.id === dependenciesTabId) {
                            startButton.setVisible(false);
                            stopButton.setVisible(false);
                        } else {
                            startButton.setVisible(true);
                            stopButton.setVisible(true);
                        }
                    }
                    dataStore.filterBy(function (record) {
                        var result = true;
                        switch (tab.id) {
                            case servicesTabId:
                                result = record.json.Type === 0; // Service
                                break;
                            case websitesTabId:
                                result = record.json.Type === 1; // Website
                                break;
                            case dependenciesTabId:
                                result = record.json.Type === 2; // Dependency
                                break;
                        }
                        return result;
                    });
                    $(String.format("#{0}", selectAllCkBoxId)).removeClass(checkBoxCheckedClass);
                }
            }
        };

        var tabPanel = new Ext.TabPanel(tabPanelConfig);

        if (dataStore.data.items.some(function (item) { return item.json.Type === 0 })) { // Service
            tabPanel.add({
                id: servicesTabId,
                title: "@{R=Core.Strings.2;K=OrionServiceManager_Tab_Services;E=js}"
            });
        }

        if (dataStore.data.items.some(function (item) { return item.json.Type === 1 })) { //Website
            tabPanel.add(    {
                id: websitesTabId,
                title: "@{R=Core.Strings.2;K=OrionServiceManager_Tab_Websites;E=js}"
            });
        }

        if (dataStore.data.items.some(function (item) { return item.json.Type === 2 })) { // Dependency
            tabPanel.add({
                id: dependenciesTabId,
                title: "@{R=Core.Strings.2;K=OrionServiceManager_Tab_Dependencies;E=js}"
            });
        }

        tabPanel.setActiveTab(0);

        return tabPanel;
    }

    function createGridPanel(dataStore, serverHostname, serviceManagerPlaceholderId) {
        var gridPanel = null;

        var checkBoxSelectionModel = new Ext.grid.CheckboxSelectionModel(
        {
            checkOnly: true,
            listeners: {
                selectionchange: function() {
                    adjustToolbarButtonsState(checkBoxSelectionModel);
                    var selectAllCkBoxIdSelector = String.format("#{0}", selectAllCkBoxId);
                    if (dataStore.data.items.length === checkBoxSelectionModel.selections.items.length) {
                        $(selectAllCkBoxIdSelector).addClass(checkBoxCheckedClass);
                    } else {
                        $(selectAllCkBoxIdSelector).removeClass(checkBoxCheckedClass);
                    }
                }
            }
        });

        var columnModel = new Ext.grid.ColumnModel([
            checkBoxSelectionModel,
            {
                width: 24,
                height: 50,
                sortable: false,
                menuDisabled: true,
                renderer: function (value, meta, record) {
                    var imgTagFormat = "<img src='/Orion/Images/OrionServiceManager/services/{0}' height='16' width='16'/>";
                    var serviceIcon;
                    switch(record.json.ServiceName) {
                        case "SolarWindsAlertingEngine":
                        case "SolarWindsAlertingServiceV2":
                            serviceIcon = "SolarWindsAlertingEngine.ico";
                            break;
                        case "SolarWindsSyslogService":
                            serviceIcon = "SolarWindsSyslogService.ico";
                            break;
                        case "SolarWindsTrapService":
                            serviceIcon = "SolarWindsTrapService.ico";
                            break;
                        case "OrionModuleEngine":
                            serviceIcon = "OrionModuleEngine.ico";
                            break;
                        default:
                            serviceIcon = "Default.ico";
                            break;
                    }
                    return String.format(imgTagFormat, serviceIcon);
                }
            },
            {
                width: 470,
                height: 50,
                sortable: false,
                menuDisabled: true,
                renderer: function (value, meta, record) {
                    return String.format("<div style='font-weight: bold;'>{0}</div>", record.json.DisplayName);
                }
            },
            {
                width: 114,
                height: 50,
                sortable: false,
                menuDisabled: true,
                align: "right",
                renderer: function (value, meta, record) {
                    var cellHtml;
                    if ([1, 3, 5, 7].indexOf(record.json.Status) !== -1) { // Continuing, Pausing, Starting, Stopping
                        cellHtml = "<div class='loadingStatusBox'>" +
                                       "<div class='loadingImg'>" +
                                           "<img src='/Orion/Images/OrionServiceManager/loading.gif' />" +
                                       "</div>" +
                                       "<div class='loadingText'>{0}</div>" +
                                   "</div>";
                    } else if (record.json.Status === 6) {  // Stopped
                        cellHtml = "<a class='stoppedStatusBox'>{0}</a>";
                    } else {
                        cellHtml = "<div>{0}</div>";
                    }

                    return String.format(cellHtml, displayNameForStatus(record.json.Status));
                }
            }
        ]);

        var callWebService = function (serviceName) {
            var selectedServicesNames = $.map(checkBoxSelectionModel.selections.items,
                function(item) {
                    return item.json.ServiceName;
                });

            ORION.callWebService(
                serviceManagerServiceUrl,
                serviceName,
                {
                    serverHostname: serverHostname,
                    servicesList: selectedServicesNames
                },
                function() {
                    hideError();
                },
                function () {
                    if (serviceName === restartServicesServiceName
                        && checkBoxSelectionModel.selections.items.every(function(item) {
                            return item.json.Type === 1; // Website
                        })) {
                        return;
                    }
                    showError();
                });
        };

        var toolbar = new Ext.Toolbar([
            {
                xtype: "box",
                html: String.format("<div class='checkBoxToolbarButton'>" +
                                        "<div id='{0}' class='x-grid3-row-checker'>&nbsp;</div>" +
                                    "</div>", selectAllCkBoxId)
            },
            new Ext.Toolbar.Button({
                id: startButtonId,
                text: "@{R=Core.Strings;K=ServiceManagerCODE_TM0_8;E=js}",
                handler: function () {
                    $.each(checkBoxSelectionModel.selections.items, function (intex, item) {
						item.data.operation = startServicesServiceName;
                        if (item.json.Status !== 4) { // Running
                            item.json.Status = 5; // Starting
                        }
                    });
                    gridPanel.getView().refresh();
					adjustToolbarButtonsState(checkBoxSelectionModel);
                    callWebService(startServicesServiceName);
                },
                icon: "../images/OrionServiceManager/start.png",
                disabled: true
            }),
            new Ext.Toolbar.Button({
                id: stopButtonId,
                text: "@{R=Core.Strings;K=ServiceManagerCODE_TM0_10;E=js}",
                handler: function () {
                    $.each(checkBoxSelectionModel.selections.items, function (intex, item) {
						item.data.operation = stopServicesServiceName;
                        if (item.json.Status !== 6) { // Stopped
                            item.json.Status = 7; // Stopping
                        }
                    });
                    gridPanel.getView().refresh();
					adjustToolbarButtonsState(checkBoxSelectionModel);
                    callWebService(stopServicesServiceName);
                },
                icon: "../images/OrionServiceManager/stop.png",
                disabled: true
            }),
            new Ext.Toolbar.Button({
                id: restartButtonId,
                text: "@{R=Core.Strings;K=ServiceManagerCODE_TM0_6;E=js}",
                handler: function () {
                    $.each(checkBoxSelectionModel.selections.items, function (intex, item) {
						item.data.operation = restartServicesServiceName;
                        if (item.json.Status === 2 || item.json.Status === 6) { // Paused, Stopped
                            item.json.Status = 5; // Starting
                        } if (item.json.Status === 4) { // Running
                            item.json.Status = 7; // Stopping
                        }
                    });
                    gridPanel.getView().refresh();
					adjustToolbarButtonsState(checkBoxSelectionModel);
                    callWebService(restartServicesServiceName);
                },
                icon: "../images/OrionServiceManager/restart.png",
                disabled: true
            })
        ]);

        var selectAllCkBoxSelector = String.format("#{0}", selectAllCkBoxId);
        $(selectAllCkBoxSelector).live("click", function () {
            var element = $(selectAllCkBoxSelector);
            if (element.hasClass(checkBoxCheckedClass)) {
                element.removeClass(checkBoxCheckedClass);
                checkBoxSelectionModel.clearSelections();
            } else {
                element.addClass(checkBoxCheckedClass);
                checkBoxSelectionModel.selectAll();
            }
        });

        var gridConfig = {
            style: "padding: 10px;",
            enableColumnMove: false,
            width: 672,
            border: false,
            autoscroll: false,
            stripeRows: true,
            loadMask: true,
            maskDisabled: false,
            trackMouseOver: false,
            sort: false,
            store: dataStore,
            hideHeaders: true,
            cm: columnModel,
            sm: checkBoxSelectionModel,
            tbar: toolbar,
            renderTo: serviceManagerPlaceholderId
        };

        gridPanel = new Ext.grid.GridPanel(gridConfig);

        return gridPanel;
    }

    function updateServicesStatuses(gridPanel, serverHostname, notRunningSvcWarningMessageId) {
        ORION.callWebService(
            serviceManagerServiceUrl,
            getServicesStatusesServiceName,
            {
                serverHostname: serverHostname
            },
            function (servicesStatuses) {
                if (servicesStatuses === undefined
                    || servicesStatuses === null
                    || servicesStatuses.servicesStatuses === 0) {
                    showError();
                    return;
                } else {
                    hideError();
                }

                var store = gridPanel.getStore();
                var allRecords = store.snapshot || store.data;

                allRecords.each(function (record) {
                    if ((record.data.operation === stopServicesServiceName
                            && record.json.Status === 7 // Stopping
                            && servicesStatuses[record.json.ServiceName] === 4) // Running
                        || (record.data.operation === startServicesServiceName
                            && record.json.Status === 5 // Starting
                            && servicesStatuses[record.json.ServiceName] === 6)) { // Stopped
                        return;
                    }
                    record.json.Status = servicesStatuses[record.json.ServiceName];
                });

                if (allRecords.items.some(isNotRunning)) {
                    showWaring();
                } else {
                    hideWaring();
                }

                gridPanel.getView().refresh();
                adjustToolbarButtonsState(gridPanel.getSelectionModel());
            },
            function() {
                showError();
            });

        setTimeout(function() {
            updateServicesStatuses(gridPanel, serverHostname, notRunningSvcWarningMessageId);
        }, 1000);
    };

    return {
        init: function (serverHostname,
            serviceManagerPlaceholderId,
            notRunningSvcWarningMessageId,
            serviceErrorMessageId) {

            notRunningSvcWarningMsgId = notRunningSvcWarningMessageId;
            serviceErrorMsgId = serviceErrorMessageId;

            var dataStore = createDataStore(serverHostname);
            dataStore.load({
                callback: function (data, operation, success) {
                    if (!success || dataStore.data.items.length === 0) {
                        showError();
                        return;
                    }

                    if (dataStore.data.items.some(isNotRunning)) {
                        showWaring();
                    }

                    createTabPanel(serviceManagerPlaceholderId, dataStore);

                    var gridPanel = createGridPanel(dataStore, serverHostname, serviceManagerPlaceholderId);

                    updateServicesStatuses(gridPanel, serverHostname, notRunningSvcWarningMessageId);
                }
            });
        }
    };
}();

Ext.onReady(function () {
    var serviceManagerPlaceholderId = "serviceManagerPlaceholder";
    var orionServerDropdownIdPart = "OrionServerDropdown",
        orionServerDropdownSeletor = String.format("[id$='{0}'] option:selected", orionServerDropdownIdPart),
        serverHostname = $(orionServerDropdownSeletor).text();
    var notRunningSvcWarningMessageId = "notRunningSvcWarningMessage",
        serviceErrorMessageId = "serviceErrorMessage";

    SW.Orion.ServiceManager.init(serverHostname,
        serviceManagerPlaceholderId,
        notRunningSvcWarningMessageId,
        serviceErrorMessageId);
}, SW.Orion.ServiceManager);
