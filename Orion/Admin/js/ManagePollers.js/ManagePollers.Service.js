﻿Ext.namespace('SW.Core.ManagePollers');
Ext.namespace('SW.Core.ManagePollers.Service');

(function (self, $, undefined) {
    self.performAction = function (actionName, pollerIds, params, onSuccess, onError) {
        ORION.callWebService("/Orion/Services/ManagePollers.asmx", "PerformAction", { action: actionName, pollerIds: pollerIds, parameters: params }, onSuccess, onError);
    };
    self.finishImport = function (params, onSuccess, onError) {
        ORION.callWebService("/Orion/Services/ManagePollers.asmx", "FinishImport", { parameters: params }, onSuccess, onError);
    };
}(SW.Core.ManagePollers.Service = SW.Core.ManagePollers.Service || {}, jQuery));