﻿Ext.namespace('SW.Core.ManagePollers');
Ext.namespace('SW.Core.ManagePollers.Export');

(function (self, $, undefined) {
    var config = {
        parent: null,
    };

    self.init = init;

    function init(c) {
        // subscriptions
        if (c.parent) {
            c.parent.on("ManagePollers.ExportToThwack", showLoginWindow);
        }

        $.extend(config, c);  
    };
    
    function showLoginWindow() {
        if (thwackUserInfo.name && thwackUserInfo.pass) {
            doExport();
            return;
        }

        SW.Core.ThwackCommon.LoginToThwackCustom(thwackUserInfo.name, thwackUserInfo.pass, function (username, password, onCancel) {
            var params = { thwackUsername: username, thwackPassword: password, credentialCheck: true };
            config.parent.Service.performAction("ExportToThwack", [config.parent.getSingleSelectedId(true)], params, function (result) {
                if (result.Success) {
                    onCancel();
                    thwackUserInfo = { name: username, pass: password };
                    doExport();
                } else {
                    Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_PS1_95;E=js}', msg: '@{R=Core.Strings;K=WEBJS_PS1_96;E=js}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                }
            },
            function (result) {
                onCancel();
                config.parent.errorHandler(result);
            });
        });
    }

    function doExport() {
        var waitMsg = Ext.Msg.wait('@{R=Core.Strings;K=WEBJS_PS1_100;E=js}');
        var params = { credentialCheck: false };
        config.parent.Service.performAction("ExportToThwack", [config.parent.getSingleSelectedId(true)], params, function (result) {
            waitMsg.hide();
            if (result.Success) {
                Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_PS1_98;E=js}', msg: '@{R=Core.Strings;K=WEBJS_PS1_99;E=js}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO });
            } else {
                Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_SO0_24;E=js}', msg: '@{R=Core.Strings;K=WEBJS_PS1_97;E=js}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            }
        },
        function (result) {
            waitMsg.hide();
            config.parent.errorHandler(result);
        });
    }
}(SW.Core.ManagePollers.Export = SW.Core.ManagePollers.Export || {}, jQuery));