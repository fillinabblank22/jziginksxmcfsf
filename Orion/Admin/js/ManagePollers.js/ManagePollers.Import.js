﻿Ext.namespace('SW.Core.ManagePollers');
Ext.namespace('SW.Core.ManagePollers.Import');

(function (self, $, undefined) {
    var uploadWindow;
    var config = {
        parent: null,
    };

    self.init = init;

    function init(c) {
        // subscriptions
        if (c.parent) {
            c.parent.on("ManagePollers.ShowUploadWindow", showUploadWindow);
        }
        
        $.extend(config, c);
        
        var uploadForm = new Ext.form.FormPanel({
            fileUpload: true,
            cls: 'no-border',
            border: false,
            width: 300,
            labelAlign: 'top',
            labelSeparator: ':',
            id: 'uploadForm',
            items: [
                new Ext.form.FileUploadField({
                    id: 'pollerUpload',
                    fieldLabel: '@{R=Core.Strings;K=WEBJS_PS1_72;E=js}',
                    buttonText: '@{R=Core.Strings;K=WEBDATA_PS0_20;E=js}',
                    width: 290,
                    cls: 'pollerUpload'
                })],
            buttons: [new Ext.BoxComponent({
                html: '<a class="sw-btn-primary sw-btn" href="#"><span class="sw-btn-c"><span class="sw-btn-t">@{R=Core.Strings;K=WEBJS_PS1_66;E=js}</span></span></a>',
                listeners: {
                    'afterrender': function (c) {
                        c.el.on('click', function (e) {
                            e.preventDefault();
                            doImport();
                        });
                    }
                }
            }),
                new Ext.BoxComponent({
                    html: '<a class="sw-btn-secondary sw-btn sw-btn-cancel" href="#"><span class="sw-btn-c"><span class="sw-btn-t">@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}</span></span></a>',
                    listeners: {
                        'afterrender': function (c) {
                            c.el.on('click', function (e) {
                                e.preventDefault();
                                uploadWindow.hide();
                            });
                        }
                    }
                })
            ]
        });
        uploadWindow = new Ext.Window({
            modal: true,
            resizable: false,
            id: 'uploadWindow',
            border: false,
            bodyCssClass: 'upload-window-body',
            title: '@{R=Core.Strings;K=WEBJS_PS1_73;E=js}',
            items: uploadForm
        });
    };

    function showUploadWindow() {
        Ext.getCmp('pollerUpload').setValue('');
        uploadWindow.show();
    }

    function doImport() {
        var uploadForm = Ext.getCmp('uploadForm');
        uploadForm.getForm().submit(
            {
                url: '/Orion/Admin/Pollers/PollerImporter.ashx',
                success: function(form, action) {
                    var message = JSON.parse(action.result.message);
                    if (message.eventHandler) {
                        config.parent.trigger(message.eventHandler, message, actionCallback, successfulImport, errorHandler);
                    } else {
                        successfulImport(false);
                    }
                },
                failure: function (form, action) {
                    var message = action.result.message;
                    errorHandler(message);
                }
            }
        );
    }
    
    function actionCallback(params, onSuccess, onError) {
        config.parent.Service.finishImport(params || {}, function() { onSuccess(); }, onError);
    }

    function errorHandler(message) {
        uploadWindow.hide();
        config.parent.errorHandler(message);
    }

    function successfulImport(justHide) {
        uploadWindow.hide();
        if (!justHide) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_PS1_90;E=js}', msg: '@{R=Core.Strings;K=WEBJS_PS1_91;E=js}', minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO });
            config.parent.reload({ groupByGrid: false });
        }
    }
}(SW.Core.ManagePollers.Import = SW.Core.ManagePollers.Import || {}, jQuery));