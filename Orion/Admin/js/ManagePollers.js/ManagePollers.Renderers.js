﻿Ext.namespace('SW.Core.ManagePollers');
Ext.namespace('SW.Core.ManagePollers.Renderers');

(function (self, $, undefined) {

    var config = {
        parent: null,
        pollerStatusEnum: {}
    };

    // local variables
    var selectedGroupByProperty = "";
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;

    var lang = '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}';

    function getImage(onoff, text, onclick) {
        var partPath = '';
        if (lang !== 'en' && lang !== 'is')
            partPath = lang + "/";

        return String.format('<img class="statusSlider" src="/Orion/images/{3}Scan_{0}.png"{2} alt="{1}" title="{1}"/>', onoff, text, onclick, partPath);
    }
    
    var scanNewNodesEnum = {
        disabled: 'Disabled',
        enabled: 'Enabled',
        alwaysDisabled: 'AlwaysDisabled',
        alwaysEnabled: 'AlwaysEnabled',
        notAvailable: 'NotAvailable'
    };

    // subscription handlers
    function selectedGroupChanged(groupName) {
        selectedGroupByProperty = groupName;
    }

    // public methods
    self.renderAssignments = renderAssignments;
    self.renderAuthor = renderAuthor;
    self.renderVendor = renderVendor;
    self.renderAuthorGroup = renderAuthorGroup;
    self.renderString = renderString;
    self.renderGroup = renderGroup;
    self.renderEnabled = renderEnabled;
    self.init = init;
    
    function init(c) {
        // subscriptions
        if (c.parent) {
            c.parent.on("ManagePollers.SelectedGroupByPropertyChanged", selectedGroupChanged);
        }

        $.extend(config, c);
    };
    
    // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function renderFieldType(value) {

        // translation of some special groups
        if (selectedGroupByProperty === "assignments") {
            if (value === "assigned") {
                return "@{R=Core.Strings;K=WEBJS_PS1_4; E=js}";
            }
            if (value === "unassigned") {
                return "@{R=Core.Strings;K=WEBJS_PS1_5; E=js}";
            }
        } else if (selectedGroupByProperty === "tags") {
            if (value === "?") {
                return '@{R=Core.Strings;K=WEBJS_PS1_1; E=js}'; //[None]
            }
        }

        if (!value) {
            return '@{R=Core.Strings;K=WEBJS_PS1_2; E=js}'; //[All]
        }
        else if (value === "?") {
            return '@{R=Core.Strings;K=WEBJS_PS1_3; E=js}'; //[Unknown]
        }

        return Ext.util.Format.htmlEncode(value);
    }

    function renderAssignments(value, meta, record) {

        var caption;
        var pollerStatus = config.pollerStatusEnum.disabled;

        if (!value || value <= 0) {
            // unassigned
            caption = "@{R=Core.Strings;K=WEBJS_PS1_5; E=js}";
        } else {
            caption = value + " " + record.data.AssignmentNetOjectDisplayName;
            pollerStatus = config.pollerStatusEnum.enabled;
        }
        
        if ($.inArray('Assign', eval(record.data.AvailableOperations)) > -1) {
            return '<a href="/Orion/Admin/Pollers/EnableDisablePollers.aspx?poller=' + record.data.Id + '&groupBy=PollerStatus&groupByValue=' + pollerStatus + '">' + caption + '</a>';
        }

        return caption;
    }

    function renderAuthor(value, meta, record) {

        var output = renderString(value, meta, record);

        output = '<span class="' + record.get("AuthorCls") + '">' + output + '</span>';

        return output;
    }

    function renderVendor(value, meta, record) {

        var output = renderString(value, meta, record);

        var vendor = value;
        var vendorIcon = record.get("VendorIcon");
        var vendorIconPath = '/NetPerfMon/images/vendors/';

        var isVendorIconPresent = vendorIcon && (typeof vendorIcon == "string") && vendorIcon.length > 0;

        if (isVendorIconPresent) {
            output = '<img src="' + vendorIconPath + vendorIcon + '" />&nbsp;' + output;
        }

        return output;
    }

    function renderAuthorGroup(value, meta, record) {

        var output = renderString(value, meta, record);

        output = '<span class="'+ record.get("Data") + '">' + output + '</span>';


        return output;
    }

    function renderString(value, meta, record) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    };

    function renderGroup(value, meta, record) {
        var disp;
        a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            disp = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) + " (" + record.data.Cnt + ")";
        }
        else {
            disp = renderFieldType($.trim(String((Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : value))) + " (" + record.data.Cnt + ")";
        }

        // put icons to author
        if (selectedGroupByProperty == "author") {
            disp = renderAuthorGroup(disp, meta, record);
        }
        
        return disp;
    }

    function renderEnabled(value, meta, record) {
        if (value == scanNewNodesEnum.notAvailable) {
            return "@{R=Core.Strings;K=WEBJS_PS1_6}"; // N/A
        }

        var imgTag = "";
        var onoff = "Off";
        if (value == scanNewNodesEnum.enabled || value == scanNewNodesEnum.alwaysEnabled) 
            onoff = "On";

        if (value == scanNewNodesEnum.disabled || value == scanNewNodesEnum.enabled) {
            imgTag = getImage(onoff, "@{R=Core.Strings;K=WEBJS_PS1_8}", " onclick=\"SW.Core.ManagePollers.trigger('ManagePollers.DoAction', 'ChangeScan', { 'newState': " + (value == scanNewNodesEnum.disabled) + "})\""); 
        } else if (value == scanNewNodesEnum.alwaysDisabled || value == scanNewNodesEnum.alwaysEnabled) {
            imgTag = getImage(onoff, "@{R=Core.Strings;K=WEBJS_PS1_7}", '');
        }

        return imgTag;
    }


}(SW.Core.ManagePollers.Renderers = SW.Core.ManagePollers.Renderers || {}, jQuery));