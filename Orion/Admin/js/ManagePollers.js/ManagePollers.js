﻿Ext.namespace('SW.Core.ManagePollers');
Ext.namespace('SW.Core.ManagePollers.Renderers');
Ext.namespace('SW.Core.ManagePollers.Service');
Ext.namespace('SW.Core.ManagePollers.Toolbar');
Ext.namespace('SW.Core.ManagePollers.Import');
Ext.QuickTips.init();

(function (self, $, undefined) {

    var local = self._local = self._local || {};

    var seal = self._seal = self._seal || function() {
        delete self._local;
        delete self._seal;
    };
    var unseal = self._unseal = self._unseal || function() {
        self._local = local;
        self._seal = seal;
        self._unseal = unseal;
    };

    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;

    var pollerStatusEnum = {
        disabled: 'Disabled',
        enabled: 'Enabled',
        notAMatch: 'NotAMatch',
        notScanned: 'NotScanned'
    };
    
    // Ext js components
    var comboArray;
    var pageSizeBox;
    var grid;
    var groupGrid;
    var searchField;

    var selectedColumns;
    var selectorModel;
    var dataStore;
    var userPageSize;

    var isDemo = $('#isDemoMode').length != 0;
    // used to store selected grouping option value
    var isGroupByPropertyinitialized = false;

    local.previousGroupByProperty = '';
    local.selectedGroupByProperty = '';
    local.selectedGroupByValue = '';

    // make this observable
    SW.Core.Observer.makeObserver(self);

    // listeners
    self.on("ManagePollers.SelectedGroupByPropertyChanged", selectedGroupByPropertyChangedHandler);
    self.on("ManagePollers.SelectedGroupByValueChanged", selectedGroupByValueChangedHandler);
    self.on("ManagePollers.DoAction", doAction);
    
    // handlers
    function selectedGroupByPropertyChangedHandler(newGroupName) {
        var isGroupPropertyChanged = true;

        if (!isGroupByPropertyinitialized) {
            local.previousGroupByProperty = newGroupName;
            isGroupByPropertyinitialized = true;
        } else {
            local.previousgroupByproperty = local.selectedGroupByProperty;
            isGroupPropertyChanged = local.previousgroupByproperty != newGroupName;
        }

        local.selectedGroupByProperty = newGroupName;



        reload({ groupByGrid: true, mainGrid: false, isGroupPropertyChanged: isGroupPropertyChanged });
        self.trigger("ManagePollers.SelectedGroupByValueChanged", "", isGroupPropertyChanged);
    }

    function selectedGroupByValueChangedHandler(newGroupGridValue, isGroupPropertyChanged) {
        local.selectedGroupByValue = newGroupGridValue;

        if (!isGroupPropertyChanged) {
            var store = groupGrid.getStore();
            var index = store.findExact("Value", local.selectedGroupByValue);
            if (index > -1) {

                var sm = groupGrid.getSelectionModel();
                sm.suspendEvents(false);
                sm.selectRecords([store.data.items[index]], false);
                sm.resumeEvents();
            }
        }

        reload({ groupByGrid: false, mainGrid: true, isGroupPropertyChanged: isGroupPropertyChanged });
    }

    function doAction(action, parameters) {
        var selectedItems = grid.getSelectionModel().getSelections();
        if (selectedItems) {
            var filteredItems = $.grep(selectedItems, function(elem) {
                return $.inArray(action, eval(elem.data.AvailableOperations)) > -1;
            });
            if (filteredItems.length > 0) {
                grid.getEl().mask('@{R=Core.Strings;K=WEBJS_PS1_89;E=js}', 'x-mask-loading');
                self.Service.performAction(action, getSelectedPollers(filteredItems, true), parameters || {}, success, errorHandler);
            }
        }
    }
    
    function registerHandler(eventName, handlerFunction) {
        if (eventName && handlerFunction && typeof handlerFunction === 'function') {
            self.on(eventName, handlerFunction);
        }
    }
    // local functions
    function success(data) {
        if (data.Success) {
            if (data.RedirectUrl) {
                window.location.href = data.RedirectUrl;
                if (data.RedirectUrl.indexOf('.ashx') != -1) {
                    grid.getEl().unmask();
                }
            } else {
                grid.getEl().unmask();
                SW.Core.ManagePollers.reload({ groupByGrid: false });
            }
        } else {
            grid.getEl().unmask();
            errorHandler('@{R=Core.Strings;K=WEBJS_PS1_86;E=js}'); // An error occured while performing selected action, please see logfile for details
        }
    }

    function getSingleSelectedIdPrivate(encode) {
        var selections = grid.getSelectionModel().getSelections();
        selections = getSelectedPollers(selections, encode);
        if (selections.length == 1) {
            return selections[0];
        } else {
            return undefined;
        }
    }

    var updateToolbarButtons = function () {
        self.Toolbar.updateToolbarButtons(grid);
    };

    var getSelectedPollers = function (items, encode) {
        var ids = [];

        Ext.each(items, function (item) {
            if (encode) {
                ids.push(encodeURIComponent(item.data.Id));
            }
            else {
                ids.push(item.data.Id);
            }
        });

        return ids;
    };

    function refreshObjects(elem, property, value, search, callback) {
        elem.store.removeAll();

        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, value: value || "", search: tmpfilterText };
        elem.store.load({ callback: callback });
    };

    function refreshGridObjects(start, limit, elem, property, value, search, callback) {
        elem.store.removeAll();

        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, value: value || "", search: tmpfilterText };
        elem.store.load({ params: { start: start, limit: limit }, callback: callback });
    };

    //Error handler
    function errorHandler(result) {
        if (result != null ) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_SO0_24;E=js}', msg: result, minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }
    
    // update whatever is needed when search was triggered. Note that search text could be also changed to empty text which means that search criteria is not applied
    function searchStartedHandler(searchText) {
        // refresh just the grouping grid as the main grid is refreshed automatically as it is bound to the same store as searchfield.
        reload({ mainGrid: false });
    }

    function setMainGridHeight() {
        window.setTimeout(function () {
            var mainGrid = Ext.getCmp('mainGrid');
            var maxHeight = calculateMaxGridHeight(mainGrid);
            var height = maxHeight;
            setHeightForGrids(height);
        }, 0);
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp('mainGrid');
        var groupingGrid = Ext.getCmp('groupingGrid');

        mainGrid.setHeight(Math.max(height, 300));
        groupingGrid.setHeight(Math.max(height - 50, 250)); //height without groupByTopPanel

        mainGrid.doLayout();
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.getPosition()[1];
        return $(window).height() - gridTop - $('#footer').height() - 25;
    }

    function reload(options) {

        // default reload options
        var defaults = {
            mainGrid: true,
            groupByGrid: true,
            isGroupPropertyChanged: false
        };

        if (options) {
            $.extend(defaults, options);
        }

        var groupByProperty = (defaults.groupBy) ? '' : local.selectedGroupByProperty;

        // when not changed group by property then try to keep also group by value
        var groupByValue = (defaults.groupByGrid && defaults.isGroupPropertyChanged) ? '' : local.selectedGroupByValue;

        var previousGroupByValue = local.selectedGroupByValue;

        // reloads the grouping grid, cancels any custom selection, just load data for selected group in combobox
        if (defaults.groupByGrid) {
            refreshObjects(groupGrid, groupByProperty, "", filterText, function () {
                // select the same value if groupByProperty was not changed
                if (!defaults.isGroupPropertyChanged) {
                    var store = groupGrid.getStore();
                    var index = store.findExact("Value", previousGroupByValue);
                    if (index > -1) {

                        var sm = groupGrid.getSelectionModel();
                        sm.suspendEvents(false);
                        sm.selectRecords([store.data.items[index]], false);
                        sm.resumeEvents();
                    } else {
                        // show all - as the previsouly selected value is not available anymore
                        self.trigger("ManagePollers.SelectedGroupByValueChanged", "");
                    }
                    //groupGrid.selModel
                }
            });
        }
        // reload data in main grid
        if (defaults.mainGrid) {
            refreshGridObjects(0, userPageSize, grid, groupByProperty, groupByValue, filterText);
        }
    };
    
    //Ext grid
    var allowAdmin = false;

    self.reload = reload;
    self.errorHandler = errorHandler;
    self.registerHandler = registerHandler;
    self.init = function () {

        seal();

        // load user settings
        ORION.prefix = "ManagePollers_";
        userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
        allowAdmin = (ORION.Prefs.load('AllowAdmin', 'false')).toLowerCase() == 'true';
        selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');

        // init submodules
        self.Renderers.init({ parent: self, pollerStatusEnum: pollerStatusEnum });
        self.Toolbar.init({ parent: self, isDemo: isDemo, allowAdmin: allowAdmin });
        self.Import.init({ parent: self });
        self.Export.init({ parent: self });

        // init extjs components and datastores
        selectorModel = new Ext.grid.CheckboxSelectionModel();
        selectorModel.on("selectionchange", updateToolbarButtons);

        dataStore = new ORION.WebServiceStore(
                            "/Orion/Services/ManagePollers.asmx/GetLocalPollers",
                            [
                                { name: 'Technology', mapping: 0 },
                                { name: 'Id', mapping: 1 },
                                { name: 'Name', mapping: 2 },
                                { name: 'Assignments', mapping: 3 },
                                { name: 'Author', mapping: 4 },
                                { name: 'Vendor', mapping: 5 },
                                { name: 'Enabled', mapping: 6 },
                                { name: 'VendorIcon', mapping: 7 },
                                { name: 'AuthorCls', mapping: 8 },
                                { name: 'AvailableOperations', mapping: 9 },
                                { name: 'AssignmentNetOjectDisplayName', mapping: 10 }
                            ],
                                "Name");

        var groupingStore = new ORION.WebServiceStore(
                            "/Orion/Services/ManagePollers.asmx/GetLocalPollersGroupValues",
                            [
                                { name: 'Value', mapping: 0 },
                                { name: 'Cnt', mapping: 1 },
                                { name: 'Data', mapping: 2 }
                            ],
                            "Value", "");

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            enableKeyEvents: true,
            allowNegative: false,
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 100,
            value: userPageSize,
            listeners: {
                scope: this,
                'keydown': function (f, e) {
                    var k = e.getKey();
                    if (k == e.RETURN) {
                        e.stopEvent();
                        var v = parseInt(f.getValue());
                        if (!isNaN(v) && v > 0 && v <= 100) {
                            userPageSize = v;
                            pagingToolbar.pageSize = userPageSize;
                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                        }
                        else {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",//Invalid page size
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                    }
                },
                'focus': function (field) {
                    field.el.dom.select();
                },
                'change': function (f, numbox, o) {
                    var pSize = parseInt(f.getValue());
                    if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                        Ext.Msg.show({
                            title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                            msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",//Invalid page size
                            icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                        });
                        return;
                    }

                    if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                        pagingToolbar.pageSize = pSize;
                        userPageSize = pSize;

                        ORION.Prefs.save('PageSize', userPageSize);
                        pagingToolbar.doLoad(0);
                    }
                }
            }
        });

        var pagingToolbar = new Ext.PagingToolbar(
            {
                store: dataStore,
                pageSize: userPageSize,
                displayInfo: true,
                displayMsg: "@{R=Core.Strings;K=WEBJS_PS1_11; E=js}", // Displaying pollers {0} - {1} of {2}
                emptyMsg: "@{R=Core.Strings;K=WEBJS_PS1_12; E=js}", // No device pollers to display
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                items: [
                '-',
                new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                pageSizeBox
                ]
            }
        );

        var groupingDataStore = new Ext.data.SimpleStore(
        {
            fields: ['Name', 'Value'],
            data: [
                ['@{R=Core.Strings;K=WEBJS_AK0_17; E=js}', ''],
                ['@{R=Core.Strings;K=WEBJS_PS1_13; E=js}', 'author'],
                ['@{R=Core.Strings;K=WEBJS_PS1_14; E=js}', 'technology'],
                ['@{R=Core.Strings;K=WEBJS_PS1_15; E=js}', 'tags'],
                ['@{R=Core.Strings;K=WEBJS_PS1_16; E=js}', 'assignments'],
                ['@{R=Core.Strings;K=WEBJS_PS1_17; E=js}', 'pollerType']
            ]
        });

        comboArray = new Ext.form.ComboBox(
            {
                mode: 'local',
                fieldLabel: 'Name',
                displayField: 'Name',
                valueField: 'Value',
                value: groupingDataStore.getAt(1).data.Value,
                store: groupingDataStore,
                triggerAction: 'all',
                typeAhead: true,
                forceSelection: true,
                selectOnFocus: false,
                multiSelect: false,
                editable: false,
                listeners: {
                    'beforerender': function (control) {
                        self.trigger("ManagePollers.SelectedGroupByPropertyChanged", control.value);
                    },
                    'select': function (control) {
                        self.trigger("ManagePollers.SelectedGroupByPropertyChanged", control.value);
                    }
                }
            });


        searchField = new Ext.ux.form.SearchField({
            store: dataStore,
            width: 200
        });

        searchField.on('searchStarted', searchStartedHandler, this);

        grid = new Ext.grid.GridPanel({
            id: 'pollersGrid',
            region: 'center',
            viewConfig: { forceFit: true, emptyText: "@{R=Core.Strings;K=WEBJS_PS1_12;E=js}" },
            store: dataStore,
            baseParams: { start: 0, limit: userPageSize },
            stripeRows: true,
            trackMouseOver: false,
            columns: [selectorModel,
                {
                    id: 'Id',
                    header: "@{R=Core.Strings;K=WEBJS_PS1_18;E=js}",
                    dataIndex: 'Name',
                    width: 20,
                    sortable: true,
                    hideable: false,
                    renderer: self.Renderers.renderString
                }, {
                    header: "@{R=Core.Strings;K=WEBJS_PS1_13;E=js}",
                    dataIndex: 'Author',
                    width: 17,
                    sortable: true,
                    renderer: self.Renderers.renderAuthor
                }, {
                    id: 'assignments',
                    header: "@{R=Core.Strings;K=WEBJS_PS1_16;E=js}",
                    dataIndex: 'Assignments',
                    width: 13,
                    sortable: false,
                    renderer: self.Renderers.renderAssignments
                }, {
                    header: "@{R=Core.Strings;K=WEBJS_PS1_19;E=js}",
                    dataIndex: 'Enabled',
                    width: 12,
                    sortable: false,
                    resizable: false,
                    id: 'enabled',
                    renderer: self.Renderers.renderEnabled
                }, {
                    header: "@{R=Core.Strings;K=WEBJS_PS1_14;E=js}",
                    dataIndex: 'Technology',
                    width: 13,
                    sortable: true,
                    renderer: self.Renderers.renderString
                }/*, {
                         header: "@{R=Core.Strings;K=WEBJS_PS1_59;E=js}",
                         dataIndex: 'Vendor',
                         width: 25,
                         sortable: true,
                         renderer: self.Renderers.renderVendor
                     }*/
            ],
            sm: selectorModel,
            autoScroll: true,
            loadMask: true,
            height: 500,
            tbar: self.Toolbar.getToolbarButtons(searchField),
            bbar: pagingToolbar
        });



        grid.store.on('beforeload', function() {
             grid.getEl().mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
        });
        grid.store.on('load', function () {
            grid.getEl().unmask();
            setMainGridHeight();
        });

        grid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VB0_133;E=js}',
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });
            grid.getEl().unmask();
        });

        // making columns visible or hidden according to saved configuration
        for (var i = 1; i < grid.getColumnModel().getColumnCount() ; i++) {
            if (selectedColumns.indexOf(grid.getColumnModel().getDataIndex(i)) > -1) {
                grid.getColumnModel().setHidden(i, false);
            } else {
                grid.getColumnModel().setHidden(i, true);
            }
        }

        grid.getColumnModel().on('hiddenchange', function () {
            var cols = '';
            for (var i = 1; i < grid.getColumnModel().getColumnCount() ; i++) {
                if (!grid.getColumnModel().isHidden(i)) {
                    cols += grid.getColumnModel().getDataIndex(i) + ',';
                }
            }
            ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
        });

        groupGrid = new Ext.grid.GridPanel({
            id: 'groupingGrid',
            store: groupingStore,
            cls: 'hide-header',
            columns: [
                { width: 193, editable: false, sortable: false, dataIndex: 'Value', renderer: self.Renderers.renderGroup, id: 'Value' }
            ],
            selModel: new Ext.grid.RowSelectionModel(),
            autoScroll: true,
            loadMask: true,
            listeners: {
                cellclick: function (mygrid, row, cell, e) {
                    var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];

                    a = reMsAjax.exec(val);
                    if (a) {
                        var b = a[1].split(/[-,.]/);
                        var dateVal = new Date(+b[0]);
                        val = dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.SortableDateTimePattern);
                    }

                    self.trigger("ManagePollers.SelectedGroupByValueChanged", val);
                }
            },
            anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
        });

        var groupByTopPanel = new Ext.Panel({
            id: 'groupByTopPanel',
            region: 'center',
            split: false,
            heigth: 50,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76;E=js}" }), comboArray],
            cls: 'panel-no-border panel-bg-gradient'
        });

        var navPanel = new Ext.Panel({
            id: 'groupByPanel',
            region: 'west',
            split: true,
            anchor: '0 0',
            width: 200,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [groupByTopPanel, groupGrid],
            cls: 'panel-no-border'
        });

        navPanel.on("bodyresize", function () {
            groupGrid.setWidth(navPanel.getSize().width);
        });


        var mainGridPanel = new Ext.Panel({
            id: 'mainGrid',
            region: 'center',
            split: true,
            layout: 'border',
            collapsible: false,
            items: [navPanel, grid],
            cls: 'no-border'
        });

        mainGridPanel.render('Grid');


        $(window).resize(function () {
            setMainGridHeight();
            mainGridPanel.doLayout();
        });

        grid.getEl().mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
        
        updateToolbarButtons();
    };

    self.getSingleSelectedId = function (encode) {
        return getSingleSelectedIdPrivate(encode);
    };
}(SW.Core.ManagePollers = SW.Core.ManagePollers || {}, jQuery));

Ext.onReady(window.SW.Core.ManagePollers.init, window.SW.Core.ManagePollers);
