﻿Ext.namespace('SW.Core.ManagePollers');
Ext.namespace('SW.Core.ManagePollers.Toolbar');
Ext.namespace('SW.Core.ManagePollers.Service');

(function (self, $, undefined) {
    // public methods
    self.getToolbarButtons = getToolbarButtons;
    self.init = init;
    self.updateToolbarButtons = updateToolbarButtons;

    var config = {
        parent: null,
        isDemo: false,
        allowAdmin: true,
    };

    var toolbarButtons = [
        { name: 'Edit', id: 'Edit', alwaysEnabled: false },
        { name: 'EditAndDuplicate', id: 'DuplicateAndEdit', alwaysEnabled: false },
        { name: 'Assign', id: 'Assign', alwaysEnabled: false },
        { name: 'ChangeScan', id: 'EnableDisableScanningOnNewNodes', alwaysEnabled: false, allowMultiple: true },
        { name: 'ExportToFile', id: 'Export', subId: 'ExportToFile', alwaysEnabled: false },
        { name: 'ExportToThwack', id: 'Export', subId: 'ExportToThwack', alwaysEnabled: false },
        { name: 'Import', id: 'Import', alwaysEnabled: true },
        { name: 'Delete', id: 'Delete', alwaysEnabled: false, allowMultiple: true }];

    var enumToButtonMap = {
        'Edit': {
            id: 'Edit',
            text: '@{R=Core.Strings;K=WEBJS_PS1_63;E=js}',
            tooltip: '@{R=Core.Strings;K=WEBJS_PS1_63;E=js}',
            iconCls: 'edit',
            handler: function() {
                config.parent.trigger('ManagePollers.DoAction', 'Edit');
            }
        },
        'EditAndDuplicate': {
            id: 'DuplicateAndEdit',
            text: '@{R=Core.Strings;K=WEBJS_PS1_64;E=js}',
            tooltip: '@{R=Core.Strings;K=WEBJS_PS1_64;E=js}',
            iconCls: 'duplicate',
            handler: function() {
                config.parent.trigger('ManagePollers.DoAction', 'EditAndDuplicate');
            }
        },
        'Assign': {
            id: 'Assign',
            text: '@{R=Core.Strings;K=WEBJS_PS1_62;E=js}',
            tooltip: '@{R=Core.Strings;K=WEBJS_PS1_62;E=js}',
            iconCls: 'assign',
            handler: function() {
                config.parent.trigger('ManagePollers.DoAction', 'Assign');
            }
        },
        'ChangeScan': {
            id: 'EnableDisableScanningOnNewNodes',
            text: '@{R=Core.Strings;K=WEBJS_PS1_69;E=js}',
            iconCls: 'enabledisable',
            menu: {
                xtype: 'menu',
                items: [
                    {
                        text: '@{R=Core.Strings;K=WEBJS_VB0_28;E=js}',
                        iconCls: 'enable-scan',
                        tooltip: '@{R=Core.Strings;K=WEBJS_PS1_70;E=js}',
                        handler: function() {
                            config.parent.trigger('ManagePollers.DoAction', 'ChangeScan', { 'newState': true });
                        },
                        listeners: {
                            afterrender: function(thisMenuItem) {
                                Ext.QuickTips.register({
                                    target: thisMenuItem.getEl().getAttribute("id"),
                                    text: thisMenuItem.initialConfig.tooltip
                                });
                            }
                        }
                    },
                    {
                        text: '@{R=Core.Strings;K=WEBJS_VB0_30;E=js}',
                        iconCls: 'disable-scan',
                        tooltip: '@{R=Core.Strings;K=WEBJS_PS1_71;E=js}',
                        handler: function() {
                            config.parent.trigger('ManagePollers.DoAction', 'ChangeScan', { 'newState': false });
                        },
                        listeners: {
                            afterrender: function(thisMenuItem) {
                                Ext.QuickTips.register({
                                    target: thisMenuItem.getEl().getAttribute("id"),
                                    text: thisMenuItem.initialConfig.tooltip
                                });
                            }
                        }
                    }
                ]
            }
        },
        'ExportToFile': {
            id: 'ExportToFile',
            text: '@{R=Core.Strings;K=WEBJS_PS1_84;E=js}',
            tooltip: '@{R=Core.Strings;K=WEBJS_PS1_87;E=js}',
            iconCls: 'export',
            handler: function () {
                config.parent.trigger('ManagePollers.DoAction', 'ExportToFile');
            }
        },
        'ExportToThwack': {
            id: 'ExportToThwack',
            text: '@{R=Core.Strings;K=WEBJS_PS1_85;E=js}',
            tooltip: '@{R=Core.Strings;K=WEBJS_PS1_88;E=js}',
            iconCls: 'export',
            handler: function () {
                config.parent.trigger('ManagePollers.ExportToThwack');
            }
        },
        'Import': {
            id: 'Import',
            text: '@{R=Core.Strings;K=WEBJS_PS1_66;E=js}',
            tooltip: '@{R=Core.Strings;K=WEBJS_PS1_68;E=js}',
            iconCls: 'import',
            handler: function() {
                config.parent.trigger('ManagePollers.ShowUploadWindow');
            }
        },
        'Delete': {
            id: 'Delete',
            text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}',
            tooltip: '@{R=Core.Strings;K=WEBJS_PS1_74;E=js}',
            iconCls: 'delete',
            handler: function () {
                SW.Core.ManagePollers.Extensions.showDialog({
                        title: '@{R=Core.Strings;K=WEBJS_PS1_74;E=js}',
                        msg: '<p><b>@{R=Core.Strings;K=WEBJS_PS1_75;E=js}</b></p><p>@{R=Core.Strings;K=WEBJS_PS1_76;E=js}</p>',
                        buttons: [
                            {
                                type: 'yes',
                                primary: false,
                                text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}'
                            },
                            {
                                type: 'cancel',
                                primary: true,
                                text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}'
                            }
                        ],
                        icon: Ext.Msg.ERROR,
                        width: 450,
                        fn: function(btn) {
                            if (btn == 'yes') {
                                config.parent.trigger('ManagePollers.DoAction', 'Delete');
                            }
                        }
                });
            }
        }
    };
    
    function init(c) {
        $.extend(config, c);
    };
    
    function updateToolbarButtons(grid) {
        var map = grid.getTopToolbar().items.map;
        var selectionModel = grid.getSelectionModel();
        var selCount = selectionModel.getCount();

        if (config.isDemo || !config.allowAdmin) {
            map['Assign'].setDisabled(true);
        }
        var buttonDisabledCommon = config.isDemo || !config.allowAdmin;
        $.each(toolbarButtons, function (index, elem) {
            var buttonDisabled;
            if (elem.allowMultiple) {
                var noElementMatch = true;
                $.each(selectionModel.getSelections(), function(i, e) {
                    if ($.inArray(elem.name, eval(e.data.AvailableOperations)) > -1) {
                        noElementMatch = false;
                        return false;
                    }
                });
                buttonDisabled = selCount == 0 || noElementMatch;
            } else {
                buttonDisabled = selCount != 1 || $.inArray(elem.name, eval(selectionModel.getSelections()[0].data.AvailableOperations)) == -1;
            }
            
            if (map[elem.id]) {
                if (elem.subId && map[elem.id].menu.items.map[elem.subId]) {
                    map[elem.id].menu.items.map[elem.subId].setDisabled(buttonDisabledCommon || buttonDisabled);
                    var disableParent = true;
                    $.each(map[elem.id].menu.items.map, function(i, e) {
                        if (!e.disabled) {
                            disableParent = false;
                        }
                    });
                    buttonDisabled = disableParent;
                }
                map[elem.id].setDisabled(elem.alwaysEnabled ? false : buttonDisabledCommon || buttonDisabled);
                
            } else if (map[elem.subId]) {
                map[elem.subId].setDisabled(elem.alwaysEnabled ? false : buttonDisabledCommon || buttonDisabled);
            }
        });

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        }
        else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    }
    
    function getToolbarButtons(searchField) {
        var buttons = [];

        var anyButtonPresent = false;
        // add create new button
        if (SW.Core.ManagePollers.CreateNewButtons.length > 0) {
            anyButtonPresent = true;
            
            if (SW.Core.ManagePollers.CreateNewButtons.length == 1) {
                var button = SW.Core.ManagePollers.CreateNewButtons[0];
                buttons.push({
                    id: 'Create',
                    text: button.Title,
                    tooltip: button.Tooltip,
                    iconCls: 'create',
                    handler: function() {
                        window.location.href = button.Url;
                    }
                });
            } else {
                buttons.push({
                    id: 'Create',
                    text: '@{R=Core.Strings;K=WEBJS_PS1_60;E=js}',
                    iconCls: 'create',
                    menu: {
                        xtype: 'menu',
                        items: $.map(SW.Core.ManagePollers.CreateNewButtons, function (elem, index) {
                            return {
                                id: 'create-sub' + index,
                                text: elem.Title,
                                iconCls: elem.ButtonCls || 'create',
                                tooltip: elem.Tooltip,
                                handler: function() {
                                    window.location.href = elem.Url;
                                },
                                listeners: {
                                    afterrender: function(thisMenuItem) {
                                        Ext.QuickTips.register({
                                            target: thisMenuItem.getEl().getAttribute("id"),
                                            text: thisMenuItem.initialConfig.tooltip
                                        });
                                    }
                                }
                            };
                        })
                    }
                });
            }
        }

        // add other buttons
        $.each(SW.Core.ManagePollers.AvailableOperations, function(index, elem) {
            if (enumToButtonMap[elem]) {
                if (anyButtonPresent) {
                    buttons.push('-');
                }
                buttons.push(enumToButtonMap[elem]);
                anyButtonPresent = true;
            }
        });
        
        // handle export button
        var exportFileIndex = $.inArray('ExportToFile', $.map(buttons, function (elem, index) {
            if (elem.id) {
                return elem.id;
            }
            return 'space';
        }));
        var exportThwackIndex = $.inArray('ExportToThwack', $.map(buttons, function (elem, index) {
            if (elem.id) {
                return elem.id;
            }
            return 'space';
        }));

        if (exportFileIndex > -1 && exportThwackIndex > -1) {
            buttons.splice(Math.min(exportFileIndex, exportThwackIndex), 3, { // buttons must be consecutive
                id: 'Export',
                text: '@{R=Core.Strings;K=WEBJS_PS1_65;E=js}',
                tooltip: '@{R=Core.Strings;K=WEBJS_PS1_67;E=js}',
                iconCls: 'export',
                menu: {
                    xtype: 'menu',
                    items: [enumToButtonMap['ExportToFile'], enumToButtonMap['ExportToThwack']]
                }
            });
        }

        // at the end add search field
        buttons.push('->', searchField);
        return buttons;
    }
    
    
}(SW.Core.ManagePollers.Toolbar = SW.Core.ManagePollers.Toolbar || {}, jQuery));