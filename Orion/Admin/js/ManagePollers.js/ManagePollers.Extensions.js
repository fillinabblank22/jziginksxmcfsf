﻿Ext.namespace('SW.Core.ManagePollers');

(function (self, $, undefined) {
    self.showDialog = show;
    var messageWindow;

    function getButton(btn, handler) {
        return new Ext.BoxComponent({
            autoEl: {
                tag: 'a',
                href: '#'
            },
            html: '<a class="sw-btn' + (btn.primary ? ' sw-btn-primary' : '')+ '" href="#"><span class="sw-btn-c"><span class="sw-btn-t">' + btn.text+ '</span></span></a>',
            listeners: {
                'afterrender': function(c) {
                    c.el.on('click', function(e) {
                        e.preventDefault();
                        handler(btn.type);
                    });
                }
            }
        });
    };

    function show(cfg) {
        var dialogPanel = new Ext.Panel({
            cls: 'no-border' + (cfg.icon ? ' x-dlg-icon' : ''),
            border: false,
            width: cfg.width || 400,
            resizable: false,
            id: 'dialogPanel',
            items: [
                new Ext.BoxComponent({
                    autoEl: {
                        cls: 'ext-mb-icon ' + (cfg.icon || 'x-hidden')
                    }  
                }),
                new Ext.BoxComponent({
                    autoEl: {
                        cls: 'ext-mb-content',
                        html: '<span class="ext-mb-text">' + cfg.msg + '</span>'
                    }
                    
                })],
            buttons: $.map(cfg.buttons, function(btn) {
                return getButton(btn, function(type) {
                    messageWindow.destroy();
                    cfg.fn(type);
                });
            })
        });

        messageWindow = new Ext.Window({
            cls: 'x-window-dlg',
            modal: true,
            resizable: false,
            id: 'dialogWindow',
            border: false,
            title: cfg.title,
            items: dialogPanel
        });

        messageWindow.show();
    };
    
    

}(SW.Core.ManagePollers.Extensions = SW.Core.ManagePollers.Extensions || {}, jQuery));