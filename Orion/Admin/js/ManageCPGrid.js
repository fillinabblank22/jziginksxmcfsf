Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

SW.Orion.CustomProperties = function () {
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    var comboArray;
    var pageSizeBox;
    var userPageSize;
    var selectorModel;
    var tempSelectedItemsArray;
    var isSelectAllMode = false;

    var dataStore;
    var grid;
    var groupGrid;

    function setFormFieldTooltip(id, tip) {
        var tt = new Ext.ToolTip({
            target: id,
            title: tip,
            plain: true,
            showDelay: 0,
            hideDelay: 0,
            trackMouse: false
        });
    }

    function GetFieldTypeText(sqlType) {
        switch (sqlType) {
            case "nvarchar": return "@{R=Core.Strings;K=WEBJS_SO0_26; E=js}";
            case "varchar": return "@{R=Core.Strings;K=WEBJS_SO0_26; E=js}";
            case "int": return "@{R=Core.Strings;K=WEBJS_SO0_27; E=js}";
            case "real": return "@{R=Core.Strings;K=WEBJS_SO0_28; E=js}";
            case "float": return "@{R=Core.Strings;K=WEBJS_SO0_103; E=js}";
            case "datetime": return "@{R=Core.Strings;K=WEBJS_SO0_29; E=js}";
            case "bit": return "@{R=Core.Strings;K=WEBJS_SO0_30; E=js}";
            case "[All]": return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            default: return sqlType;
        }
    }

    function RenderFieldType(value) {
        if (!value)
            return '@{R=Core.Strings;K=WEBDATA_PS0_0; E=js}';
        return Ext.util.Format.htmlEncode(GetFieldTypeText(value));
    }

    function selectAllTooltip(grid, pageSizeBox, webStore) {
        if (grid.store.data.items.length == grid.getSelectionModel().selections.items.length && grid.store.data.items.length != 0 && grid.store.data.items.length != pageSizeBox.ownerCt.dsLoaded[0].totalLength) {
            var text = String.format("@{R=Core.Strings;K=WEBJS_AB0_41;E=js}", grid.getSelectionModel().selections.items.length);
            $('<div id="selectionTip" style="padding-left: 5px; background-color:#FFFFCC;">' + text + ' </div>').insertAfter(".x-panel-tbar.x-panel-tbar-noheader");
            $("#selectionTip").append($("<span id='selectAllLink' style='text-decoration: underline;color:red;cursor:pointer;'>" + String.format("@{R=Core.Strings;K=WEBJS_AB0_39;E=js}", pageSizeBox.ownerCt.dsLoaded[0].totalLength) + "</span>").click(function () {
                var allGridElementsStore = webStore();
                allGridElementsStore.proxy.conn.jsonData = grid.store.proxy.conn.jsonData;
                allGridElementsStore.load({
                    callback: function () {
                        isSelectAllMode = true;
                        tempSelectedItemsArray = allGridElementsStore.data.items;
                        $("#selectionTip").text("@{R=Core.Strings;K=WEBJS_AB0_40;E=js}").css("background-color", "white");
                        $("#selectAllLink").remove();
                        updateToolbarButtons();
                    }
                });
            }));
            var gridHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(gridHeight + $("#selectionTip").height());
        } else {
            clearSelectAllTooltip();
        }
    }

    function clearSelectAllTooltip() {
        if ($("#selectionTip").length != 0) {
            var rowsContainerHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(rowsContainerHeight - $("#selectionTip").height());
            $("#selectionTip").remove();
            if (isSelectAllMode) {
                isSelectAllMode = false;
            }
        }
    }

    var updateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;
        var selCount = getSelectedItemsArray().length;
        map.Import.setDisabled(disableAssignment(getSelectedItemsArray()));

        if ($("#isOrionDemoMode").length != 0 || !allowAdmin) {
            // The buttons are disabled in demo mode
            map.Add.setDisabled(true);
            map.Edit.setDisabled(true);
            map.Delete.setDisabled(true);
            map.Import.setDisabled(true);
            return;
        }

        map.Edit.setDisabled(selCount != 1);
        map.Delete.setDisabled(selCount == 0);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && grid.getSelectionModel().getCount() == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        }
        else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    var getSelectedCPIds = function (items, encode) {
        var ids = [];

        Ext.each(items, function (item) {
            if (encode) {
                ids.push(encodeURIComponent(item.data.CPID));
            }
            else {
                ids.push(item.data.CPID);
            }
        });

        return ids;
    };

    var disableAssignment = function (items) {
        var table;

        for (var y = 0; y < items.length; y++) {
            if (!table) {
                table = items[y].data.Table;
            }
            else {
                if (table != items[y].data.Table) return true;
            }
        }

        return false;
    };

    var deleteSelectedItems = function (items, onSuccess) {
        var toDelete = getSelectedCPIds(items, false);

        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_SO0_1; E=js}");

        deleteCustomProperties(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            onSuccess(result);
        });
    };

    var deleteCustomProperties = function(ids, onSuccess) {
        ORION.callWebService("/Orion/Services/CPEManagement.asmx",
            "RemoveCP", { ids: ids },
            function(result) {
                onSuccess(result);
            });
        };

    var areCustomPropertiesUsedForActions = function(callback) {
        var items = getSelectedItemsArray();
        var ids = getSelectedCPIds(items, false);
        ORION.callWebService("/Orion/Services/CPEManagement.asmx",
            "AreCustomPropertiesUsedForActions", { ids: ids },
            function(result) {
                if (result) {
                    Ext.Msg.show({
                        title: '@{R=Core.Strings.2;K=WEBJS_VL0_7;E=js}',
                        msg: '@{R=Core.Strings.2;K=WEBJS_VL0_8;E=js}',
                        buttons: { ok: 'OK' },
                        icon: Ext.Msg.WARNING,
                        fn: function(button) {
                            if (button == "ok") {
                                callback();
                            }
                        }
                    });
                } else {
                    callback();
                }
            });
    };

    // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function getSelectedItemsArray() {
        return isSelectAllMode ? tempSelectedItemsArray : grid.getSelectionModel().getSelections();
    }

    function renderYesNo(value) {
        return value ? '@{R=Core.Strings;K=WEBJS_AK0_28;E=js}' : '@{R=Core.Strings;K=WEBJS_AK0_27;E=js}';
    }

    function renderString(value, meta, record) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    };

    function renderGroup(value, meta, record) {
        var disp;
        a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            disp = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) + " (" + record.data.Cnt + ")";
            value = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.SortableDateTimePattern);
        }
        else {
            if (comboArray.getValue().indexOf('Format') > -1) {
                disp = RenderFieldType(value) + " (" + record.data.Cnt + ")";
            }
            else {
                disp = RenderFieldType($.trim(String((Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : value))) + " (" + record.data.Cnt + ")";
            }
        }

        if (value == null) {
            value = "null";
        }

        return disp;
    }

    function refreshObjects(elem, property, type, value, search, callback) {
        elem.store.removeAll();

        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: tmpfilterText };
        elem.store.load({ callback: callback });
    };

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();

        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: tmpfilterText };
        elem.store.load({ params: { start: start, limit: limit }, callback: callback });
    };

    //Error handler
    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({ title: result.Source, msg: result.Msg, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }

    function CPDoQuery(query, succeeded, failed) {
        Information.Query(query, function (result) {
            var table = [];
            for (var y = 0; y < result.Rows.length; ++y) {
                var row = result.Rows[y];
                var tableRow = {};
                for (var x = 0; x < result.Columns.length; ++x) {
                    tableRow[result.Columns[x]] = row[x];
                }
                table.push(tableRow);
            }
            succeeded({ Rows: table });
        }, function (error) {
            if (failed != null)
                failed();
            if (error.get_message() == "Authentication failed.") {
                alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
                window.location.reload();
            }
        });
    };

    function getCPRowNumber(cpId, succeeded) {
        if (!cpId) {
            succeeded({ ind: -1 });
            return;
        }

        var sortField = "Field";
        var query = String.format("SELECT (e.FullName + ':' + cp.Field) as CPID \
FROM Orion.CustomProperty cp \
INNER JOIN Metadata.Relationship re ON re.TargetType=cp.TargetEntity AND re.SourcePropertyName='CustomProperties' AND re.IsInjected = 0\
INNER JOIN Metadata.Entity e ON e.FullName=re.SourceType \
Order BY cp.{0}", sortField);
        CPDoQuery(query, function (result) {
            for (var i = 0; i < result.Rows.length; i++) {
                if (result.Rows[i].CPID == cpId) {
                    if (succeeded != null) {
                        succeeded({ ind: i });
                        return;
                    }
                }
            }
            // not found
            succeeded({ ind: -1 });
            return;
        });
    }

    function postRequest(url, params) {
        if (Ext.isEmpty(url)) return;
        if (Ext.isEmpty(params) || params.length == 0) {
            location.href = url;
            return;
        }
        var stringForm = String.format("<form id='importForm' action='{0}' method='POST'>", url);
        for (var i = 0; i < params.length; i++) {
            stringForm += String.format("<input type='hidden' name='{0}' value='{1}'/>", params[i].name, params[i].value);
        }
        stringForm += "</form>";

        $('body').append(stringForm);
        $('#importForm').submit();
    }

    //Ext grid
    ORION.prefix = "CustomProperties_";
    var allowAdmin = false;
    var selectedCP;

    return {
        init: function () {
            userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));

            var cpIds = $('#sessionInput').val();

            if (cpIds) {
                selectedCP = Ext.decode(cpIds)[0];
                //               alert(selectedCP);
                //                Ext.each(Ext.decode(cpIds), function (op) {
                //                    alert(op);
                //                });

                $.ajax({
                    type: "POST",
                    url: 'Default.aspx/ClearSessionValue',
                    data: "{ sessionVarName : 'CustomPropertyIds' }",
                    contentType: 'application/json; charset=utf-8',
                    failure: function (response) {
                        alert(response);
                    }
                });
            }

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("rowselect", function () {
                selectAllTooltip(grid, pageSizeBox, function () {
                    return new ORION.WebServiceStore(
                        "/Orion/Services/CPEManagement.asmx/GetAllSelectedCustomProperties",
                        [
                            { name: 'CPID', mapping: 0 },
                            { name: 'Table', mapping: 1 },
                            { name: 'SourceEntity', mapping: 2 }
                        ]);
                });
            });

            selectorModel.on("rowdeselect", function (rowIndex, record) {
                clearSelectAllTooltip();
            });

            selectorModel.on("selectionchange", function () {
                updateToolbarButtons();
            });

            allowAdmin = (ORION.Prefs.load('AllowAdmin', 'false')).toLowerCase() == 'true';

            dataStore = new ORION.WebServiceStore(
                                "/Orion/Services/CPEManagement.asmx/GetCustomProperties",
                                [
                                    { name: 'CPID', mapping: 0 },
                                    { name: 'Table', mapping: 1 },
                                    { name: 'Field', mapping: 2 },
                                    { name: 'DataType', mapping: 3 },
                                    { name: 'MaxLength', mapping: 4 },
                                    { name: 'Description', mapping: 5 },
                                    { name: 'DisplayNamePlural', mapping: 6 },
                                    { name: 'TargetEntity', mapping: 7 },
                                    { name: 'SourceEntity', mapping: 8 },
                                    { name: 'Mandatory', mapping: 9 },
                                    { name: 'Default', mapping: 10 },
                                    { name: 'Usage', mapping: 11 }
                                ],
                                    "Field");

            var groupingStore = new ORION.WebServiceStore(
                                "/Orion/Services/CPEManagement.asmx/GetCustomPropertiesGroupValues",
                                [
                                    { name: 'Value', mapping: 0 },
                                    { name: 'DisplayNamePlural', mapping: 1 },
                                    { name: 'Cnt', mapping: 2 }
                                ],
                                "Value", "");

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                            }
                            else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;

                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                        }
                    }
                }
            });

            var pagingToolbar = new Ext.PagingToolbar(
                { store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_SO0_2; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                    '-',
                    new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                    pageSizeBox
                    ]
                }
            );

            var groupingDataStore = new Ext.data.SimpleStore(
                                {
                                    fields: ['Name', 'Value'],
                                    data: [
                                        ['@{R=Core.Strings;K=WEBJS_VB0_76; E=js}', ''],
                                        ['@{R=Core.Strings;K=WEBJS_SO0_20; E=js}', 'Table'],
                                        ['@{R=Core.Strings;K=WEBJS_SO0_21; E=js}', 'DataType'],
                                        ['@{R=Core.Strings;K=WEBJS_JT0_1; E=js}', 'Usage']
                                ]
                                });

            comboArray = new Ext.form.ComboBox(
                {
                    mode: 'local',
                    fieldLabel: 'Name',
                    displayField: 'Name',
                    valueField: 'Value',
                    store: groupingDataStore,
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                    typeAhead: true,
                    forceSelection: true,
                    selectOnFocus: false,
                    multiSelect: false,
                    editable: false,
                    listeners: {
                        'select': function () {
                            var val = this.store.data.items[this.selectedIndex].data.Value;

                            refreshObjects(groupGrid, val, "", "", filterText);
                            if (val == '')
                                refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () {
                                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                        this.tooltip = 'processed';
                                        $.swtooltip(this);
                                    });
                                });
                        }
                    }
                });

            grid = new Ext.grid.GridPanel({
                region: 'center',
                viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}" },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                stripeRows: true,
                trackMouseOver: false,
                columns: [selectorModel,
                    { header: 'CPID', width: 80, hidden: true, hideable: false, dataIndex: 'CPID' },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_19; E=js}', width: 250, sortable: true, hideable: false, dataIndex: 'Field', renderer: renderString },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_20; E=js}', width: 150, sortable: true, dataIndex: 'DisplayNamePlural', renderer: renderString },
                    { header: '@{R=Core.Strings;K=WEBJS_JT0_1; E=js}', width: 200, sortable: false, dataIndex: 'Usage', renderer: renderString },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_21; E=js}', width: 150, sortable: true, dataIndex: 'DataType', renderer: RenderFieldType },
                    { id: 'Description', header: '@{R=Core.Strings;K=WEBJS_SO0_23; E=js}', sortable: true, dataIndex: 'Description', resizable: true, renderer: renderString },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_22; E=js}', width: 50, hidden: true, sortable: true, dataIndex: 'MaxLength' },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_90; E=js}', width: 50, sortable: true, dataIndex: 'Mandatory', renderer: renderYesNo },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_91; E=js}', width: 100, hidden: true, hideable: false, sortable: true, dataIndex: 'Default', renderer: renderString }
                ],
                sm: selectorModel,
                autoScroll: true,
                autoExpandColumn: 'Description',
                loadMask: true,
                width: 795,
                height: 500,
                tbar: [
                    { id: 'Add', text: '@{R=Core.Strings;K=WEBJS_SO0_4; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_SO0_13; E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', handler: function () { location.href = '/Orion/Admin/CPE/Add/Default.aspx'; } }, '-',
                    { id: 'Edit', text: '@{R=Core.Strings;K=WEBJS_SO0_6; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_SO0_37; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler:
                        function () {
                            var editParams = new Array();
                            editParams.push({ name: 'CustomPropertyId', value: getSelectedCPIds(getSelectedItemsArray(), false)[0] });
                            postRequest('/Orion/Admin/CPE/EditCP.aspx', editParams);
                        } 
                    }, '-',
                    { id: 'InlineEdit', text: '@{R=Core.Strings;K=WEBJS_YK0_2; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_YK0_3; E=js}', icon: '/Orion/images/CPE/custom_properties_inline_editor.png', cls: 'x-btn-text-icon', handler:
                        function () {
                            //check if something selected
                            if (grid.getSelectionModel().getCount() < 1) {
                                Ext.Msg.show({
                                    title: '@{R=Core.Strings;K=WEBJS_YK0_2; E=js}',
                                    msg: '@{R=Core.Strings;K=WEBJS_YK0_4; E=js}',
                                    buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                                    icon: Ext.Msg.WARNING
                                });
                                return;
                            }

                            //check if different object CP selected
                            if (disableAssignment(getSelectedItemsArray())) {
                                Ext.Msg.show({
                                    title: '@{R=Core.Strings;K=WEBJS_YK0_2; E=js}',
                                    msg: '@{R=Core.Strings;K=WEBJS_IB0_9; E=js}',
                                    buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                                    icon: Ext.Msg.WARNING
                                });
                                return;
                            }
                            var inlineParams = new Array();
                            inlineParams.push({ name: 'CustomPropertyIds', value: getSelectedCPIds(getSelectedItemsArray(), false).join() });
                            postRequest('/Orion/Admin/CPE/InlineEditor.aspx', inlineParams);
                        }
                    }, '-',
                    { id: 'Export', text: '@{R=Core.Strings;K=WEBJS_SO0_39; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_SO0_17; E=js}', icon: '/Orion/images/CPE/export.png', cls: 'x-btn-text-icon', handler:
                    function () {
                        //check if something selected
                        if (grid.getSelectionModel().getCount() < 1) {
                            Ext.Msg.show({
                                title: '@{R=Core.Strings;K=WEBJS_SO0_39; E=js}',
                                msg: '@{R=Core.Strings;K=WEBJS_IB0_10; E=js}',
                                buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                                icon: Ext.Msg.WARNING
                            });
                            return;
                        }

                        //check if different object CP selected
                        if (disableAssignment(getSelectedItemsArray())) {
                            Ext.Msg.show({
                                title: '@{R=Core.Strings;K=WEBJS_SO0_39; E=js}',
                                msg: '@{R=Core.Strings;K=WEBJS_IB0_9; E=js}',
                                buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                                icon: Ext.Msg.WARNING
                            });
                            return;
                        }

                        $.ajax({
                            type: "POST",
                            url: 'Default.aspx/ClearSessionValue',
                            data: "{ sessionVarName : 'ObjectsIds' }",
                            contentType: 'application/json; charset=utf-8',
                            success: function (response) {
                                var exportParams = new Array();
                                exportParams.push({ name: 'CustomPropertyIds', value: getSelectedCPIds(getSelectedItemsArray(), false).join() });
                                postRequest('/Orion/Admin/CPE/ExportCP.aspx', exportParams);
                            },
                            failure: function (response) {
                                alert(response);
                            }
                        });
                    }
                    }, '-',
                    { id: 'Import', text: '@{R=Core.Strings;K=WEBJS_SO0_38; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_SO0_16; E=js}', icon: '/Orion/images/CPE/import.png', cls: 'x-btn-text-icon', handler: function () {
                        var importParams = new Array();
                        if (grid.getSelectionModel().getCount() > 0) {
                            importParams.push({ name: 'EntityType', value: getSelectedItemsArray()[0].data.SourceEntity });
                        }
                        postRequest('/Orion/Admin/CPE/ImportCP.aspx', importParams);
                    }
                    }, '-',
                    { id: 'Delete', text: '@{R=Core.Strings;K=WEBJS_SO0_9; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_SO0_18; E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', cls: 'x-btn-text-icon',
                        handler: function () {
                            areCustomPropertiesUsedForActions(function() {
                                Ext.Msg.show({
                                    title: '@{R=Core.Strings;K=WEBJS_SO0_10; E=js}',
                                    msg: '@{R=Core.Strings;K=WEBJS_SO0_11; E=js}',
                                    buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                                    icon: Ext.Msg.ERROR,
                                    fn: function(btn) {
                                        if (btn == 'yes') {
                                            deleteSelectedItems(getSelectedItemsArray(), function(result) {
                                                if (result && result != '')
                                                    Ext.Msg.show({ title: "@{R=Core.Strings;K=WEBJS_SO0_34; E=js}", msg: result, minWidth: 500, buttons: Ext.Msg.CANCEL, icon: Ext.MessageBox.ERROR });

                                                if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}") {
                                                    refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function() {});
                                                } else {
                                                    var sell = "";
                                                    if (groupGrid.getSelectionModel().getCount() == 1) {
                                                        sell = groupGrid.getSelectionModel().getSelected().get("Value");
                                                    }

                                                    refreshObjects(groupGrid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, "", "", filterText);
                                                    refreshGridObjects(0, userPageSize, grid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, "", sell, filterText, function() {});
                                                }
                                            });
                                        }
                                    }
                                });
                            });
                        }
                    }
                , ' ', '->',
                new Ext.ux.form.SearchField({
                    store: dataStore,
                    width: 200
                })
                    ],
                bbar: pagingToolbar
            });

            grid.store.on('beforeload', function () { grid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading'); });
            grid.store.on('load', function () {
                $("#selectionTip").remove();
                if (isSelectAllMode) {
                    isSelectAllMode = false;
                    updateToolbarButtons();
                }
                grid.getEl().unmask();
            });

            grid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
                grid.getEl().unmask();
            });

            groupGrid = new Ext.grid.GridPanel({
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [
                    { width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup, id: 'Value' }
                ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: true,
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];

                        a = reMsAjax.exec(val);
                        if (a) {
                            var b = a[1].split(/[-,.]/);
                            var dateVal = new Date(+b[0]);
                            val = dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.SortableDateTimePattern);
                        }

                        if (val == '[All]')
                            val = '';

                        refreshGridObjects(0, userPageSize, grid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, "", val, filterText, function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
            });

            var groupByTopPanel = new Ext.Panel({
                region: 'centre',
                split: false,
                heigth: 50,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), comboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

            var navPanel = new Ext.Panel({
                region: 'west',
                split: true,
                anchor: '0 0',
                width: 200,
                heigth: 500,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [groupByTopPanel, groupGrid],
                cls: 'panel-no-border'
            });

            navPanel.on("bodyresize", function () {
                groupGrid.setSize(navPanel.getSize().width, 450);
            });

            var mainGridPanel = new Ext.Panel({
                region: 'center',
                split: true,
                width: 1420,
                height: 500,
                layout: 'border',
                collapsible: false,
                items: [navPanel, grid],
                cls: 'no-border'
            });

            mainGridPanel.render('CustomPropertiesGrid');

            grid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
            getCPRowNumber(selectedCP, function (res) {
                var startFrom = 0;
                var objectIndex = res.ind;

                if (objectIndex > -1) {
                    var pageNumFl = objectIndex / userPageSize;
                    var index = parseInt(pageNumFl.toString());
                    startFrom = index * userPageSize;
                }

                if (comboArray.getValue() == '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}')
                    refreshGridObjects(startFrom, userPageSize, grid, "", "", "", filterText, function () {
                        if (objectIndex > -1) {
                            grid.getView().focusRow(objectIndex % userPageSize);
                            grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                        }
                    });
                else
                    refreshGridObjects(0, userPageSize, grid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, "", groupGrid.getStore().getAt(groupGrid.getSelectionModel().getSelected()).get("Value"), filterText, function () {
                    });

                updateToolbarButtons();
                //setFormFieldTooltip('searchfield', '@{R=Core.Strings;K=WEBJS_SO0_12; E=js}');
            });
        }
    };
} ();

Ext.onReady(SW.Orion.CustomProperties.init, SW.Orion.CustomProperties);
