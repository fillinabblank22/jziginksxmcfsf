Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

SW.Orion.NOCViews = function () {
    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;
        var needsToDisable = (selCount === 0);

        map.Edit.setDisabled(selCount != 1);
        map.Disable.setDisabled(needsToDisable);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.Add.setDisabled(true);
            map.Edit.setDisabled(true);
            map.Disable.setDisabled(true);
        }
    };

    var getSelectedViewIds = function (items) {
        var ids = { views: [], groups: [] };

        Ext.each(items, function (item) {
            ids.views.push(item.data.ViewID);
            if (!Ext.isEmpty(item.data.ViewGroup))
                ids.groups.push(item.data.ViewGroup);
        });
        return ids;
    };

    var deleteSelectedItems = function (items, onSuccess) {
        var toDelete = getSelectedViewIds(items);
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_SO0_83; E=js}");

        deleteViews(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            onSuccess(result);
        });
    };

    var deleteViews = function (ids, onSuccess) {
        ORION.callWebService("/Orion/Services/NOCViews.asmx",
            "DisableNOCViews", { ids: ids.views, groups: ids.groups },
            function (result) {
                onSuccess(result);
            });
    };

    function renderString(value, meta, record) {
        return String.format(Ext.util.Format.htmlEncode(value));
    }

    function renderViewLink(value, meta, record) {
        return String.format('<a target="blank" href="/Orion/Admin/Preview.aspx?ViewID={0}&isNOCView=true">{1}</a>', record.data.ViewID, value);
    }

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();
        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: search };
        currentSearchTerm = search;
        elem.store.load({ params: { start: start, limit: limit }, callback: callback });
    };

    //Error handler
    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({ title: result.Source, msg: result.Msg, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }

    //Ext grid
    ORION.prefix = "NOCViews_";
    var selectorModel;
    var dataStore;
    var grid;
    var pageSize;

    return {
        init: function () {
            pageSize = 10;

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSize
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/Services/NOCViews.asmx/GetNOCViews",
                                [
                                    { name: 'ViewID', mapping: 0 },
                                    { name: 'ViewTitle', mapping: 1 },
                                    { name: 'ViewType', mapping: 2 },
                                    { name: 'ViewGroup', mapping: 3 }
                                            ]);
            var pagingToolbar = new Ext.PagingToolbar(
                { store: dataStore, pageSize: pageSize/*10*/, displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_AB0_37; E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_AB0_38; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}"
                }
            );
            grid = new Ext.grid.GridPanel({
                region: 'center',
                store: dataStore,
                stripeRows: true,
                trackMouseOver: false,
                split: true,
                columns: [selectorModel,
                    { header: 'ID', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'ViewID'},
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_88; E=js}', width: 350, hideable: false, sortable: true, dataIndex: 'ViewTitle', renderer: renderViewLink },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_89; E=js}', width: 350, sortable: true, dataIndex: 'ViewType', renderer: renderString }
                ],
                sm: selectorModel, autoScroll: 'true', loadMask: true, width: 750, height: 350,
                tbar: [
                    { id: 'Add', text: '@{R=Core.Strings;K=WEBJS_AB0_31; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_AB0_31; E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', handler: function () { location.href = '/Orion/Admin/AddView.aspx?addNOCView=true'; } }, '-',
                    { id: 'Edit', text: '@{R=Core.Strings;K=CommonButtonType_Edit; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_AB0_33; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler: function () { location.href = String.format('/Orion/Admin/CustomizeView.aspx?ViewID={0}&ReturnTo={1}', getSelectedViewIds(grid.getSelectionModel().getSelections()).views[0], $("#ReturnToUrl").val()); } }, '-',
                    { id: 'Disable', text: '@{R=Core.Strings;K=WEBJS_SO0_82; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_SO0_82; E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', cls: 'x-btn-text-icon',
                        handler: function () {
                            Ext.Msg.confirm("@{R=Core.Strings;K=WEBJS_SO0_82; E=js}", "@{R=Core.Strings;K=WEBJS_SO0_84; E=js}",
                                                    function (btn) {
                                                        if (btn == 'yes') {
                                                            deleteSelectedItems(grid.getSelectionModel().getSelections(), function () {
                                                                refreshGridObjects(0, pageSize, grid, "", "", "", "", function () { });
                                                            });
                                                        }
                                                    });
                        }
                    }],
                bbar: pagingToolbar
            });

            var mainGridPanel = new Ext.Panel({ region: 'center', split: true, width: 750, height: 380, layout: 'border', collapsible: false, items: [grid], cls: 'no-border' });

            mainGridPanel.render('NOCViewsGrid');
            refreshGridObjects(0, pageSize, grid, "", "", "", "", function () {
                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                    this.tooltip = 'processed';
                    $.swtooltip(this);
                });
            });
            updateToolbarButtons();
            grid.bottomToolbar.addPageSizer();
        }
    };
} ();

Ext.onReady(SW.Orion.NOCViews.init, SW.Orion.NOCViews);

