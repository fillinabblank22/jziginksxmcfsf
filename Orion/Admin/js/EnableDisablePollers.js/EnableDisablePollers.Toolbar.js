﻿Ext.namespace('SW.Core.EnableDisablePollers');
Ext.namespace('SW.Core.EnableDisablePollers.Toolbar');

(function (self, $, undefined) {
    // public methods
    self.getToolbarButtons = getToolbarButtons;
    self.init = init;

    var config = {
        parent: null,
        isDemo: false,
        allowAdmin: true,
    };

    function init(c) {
        $.extend(config, c);
    };

    function getToolbarButtons(searchField) {
        var buttons = [];

        if (SW.Core.EnableDisablePollers.ScanSupported) {
            buttons.push({
                id: 'Scan',
                text: '@{R=Core.Strings;K=WEBJS_PS1_77;E=js}',
                tooltip: '@{R=Core.Strings;K=WEBJS_PS1_80;E=js}',
                iconCls: 'scan',
                handler: function() {
                    config.parent.trigger("EnableDisablePollers.StartScan");
                }
            }, '-');
        }

        buttons.push({
                id: 'EnablePolling',
                text: '@{R=Core.Strings;K=WEBJS_PS1_78;E=js}',
                tooltip: '@{R=Core.Strings;K=WEBJS_PS1_81;E=js}',
                iconCls: 'enable',
                handler: function() {
                    config.parent.trigger("EnableDisablePollers.EnableDisablePollers", true);
                }
            }, '-',
            {
                id: 'DisablePolling',
                text: '@{R=Core.Strings;K=WEBJS_PS1_79;E=js}',
                tooltip: '@{R=Core.Strings;K=WEBJS_PS1_82;E=js}',
                iconCls: 'disable',
                handler: function() {
                    config.parent.trigger("EnableDisablePollers.EnableDisablePollers", false);
                }
            });
        // at the end add search field
        buttons.push('->', searchField);
        return buttons;
    }


}(SW.Core.EnableDisablePollers.Toolbar = SW.Core.EnableDisablePollers.Toolbar || {}, jQuery));