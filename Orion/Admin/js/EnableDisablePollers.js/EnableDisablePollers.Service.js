﻿Ext.namespace('SW.Core.EnableDisablePollers');
Ext.namespace('SW.Core.EnableDisablePollers.Service');

(function (self, $, undefined) {
    var config = {
        parent: null,
    };

    // public methods
    self.init = function(c) {
        $.extend(config, c);
    };
    
    self.callScanService = function (pollerId, objectIds, allNodes, search, selectedGrouping, selectedGroupBy, onSuccess, onError) {
        var serviceUrl;
        var serviceName;
        var data;
        if (allNodes) {
            var selectInGrid = selectedGrouping ? true : false;
            selectedGrouping = selectedGrouping ? selectedGrouping.data.Value : '';
            var property = selectInGrid ? selectedGroupBy : '';
            var value = selectedGrouping;
            data = {
                technologyPollingId: pollerId,
                property: property,
                value: value,
                search: search
            };

            serviceUrl = '/Orion/Services/ManagePollers.asmx';
            serviceName = 'ScanTechnology';
        } else {
            data = {
                technologyPollingId: pollerId,
                objectIds: objectIds
            };
            serviceUrl = '/Orion/Services/ManagePollers.asmx';
            serviceName = 'ScanTechnologyForNetObjects';
        }
        ORION.callWebService(serviceUrl, serviceName, data, onSuccess, onError);
    };

    self.callEnableDisableService = function (pollerId, enabled, objectIds, allNodes, search, selectedGrouping, selectedGroupBy, onSuccess, onError) {
        var serviceUrl;
        var serviceName;
        var data;
        if (allNodes) {
            var selectInGrid = selectedGrouping ? true : false;
            selectedGrouping = selectedGrouping ? selectedGrouping.data.Value : '';
            var property = selectInGrid ? selectedGroupBy : '';
            var value = selectedGrouping;
            data = {
                technologyPollingId: pollerId,
                status: enabled,
                property: property,
                value: value,
                search: search
            };

            serviceUrl = '/Orion/Services/ManagePollers.asmx';
            serviceName = 'EnableDisableTechnology';
        } else {
            data = {
                technologyPollingId: pollerId,
                status: enabled,
                objectIds: objectIds
            };
            serviceUrl = '/Orion/Services/ManagePollers.asmx';
            serviceName = 'EnableDisableTechnologyForNetObjects';
        }
        
        ORION.callWebService(serviceUrl, serviceName, data, onSuccess, onError);
    };

    self.acknowledgeScanStatus = function(pollerId) {
        ORION.callWebService('/Orion/Services/ManagePollers.asmx', 'AcknowledgeScanStatus', { technologyPollingId: pollerId }, function() {}, function() {});
    };
}(SW.Core.EnableDisablePollers.Service = SW.Core.EnableDisablePollers.Service || {}, jQuery));