﻿Ext.namespace('SW.Core.EnableDisablePollers');
Ext.namespace('SW.Core.EnableDisablePollers.Scanner');

(function (self, $, undefined) {
    // public methods
    self.init = init;

    var config = {
        parent: null,
        pollerId: '',
        pollerName: '',
        technologyName: '',
        pollerStatusEnum: {},
        scanResultEnum: {},
        helpLang: 'en'
    };

    var scanWindow,
        cancelScanButton;

    function init(c) {
        $.extend(config, c);
        
        if (c.parent) {
            c.parent.on("EnableDisablePollers.ChangeScanMode", changeScanMode);
            c.parent.on("EnableDisablePollers.CancelScan", cancelScan);
        }
        
        scanWindow = new Ext.Window({
            id: 'scanWindow',
            closable: false,
            draggable: false,
            header: false,
            plain: true,
            shadow: false,
            resizable: false,
            closeAction: 'hide',
            width: '420',
            height: 'auto',
            modal: true,
            baseCls: 'scan-window',
            bodyCfg: {
                tag: 'div',
                html: '<div class="scan-window-header">' +
                    '<img class="scan-window-header-icon" src="/Orion/images/throbber.gif">' +
                    '@{R=Core.Strings;K=WEBJS_PS1_55;E=js}</div>'
            },
            fbar: [
                      new Ext.BoxComponent({
                          autoEl: {
                              tag: 'a',
                              href: '#',
                              cls: 'sw-btn-primary sw-btn'
                          },
                          id: 'ContinueInBackGround',
                          html: "<span class='sw-btn-c'><span class='sw-btn-scandialog'>@{R=Core.Strings;K=WEBJS_PS1_56;E=js}</span></span>",
                          listeners: {
                              'afterrender': function (c) {
                                  c.el.on('click', function (e) {
                                      e.preventDefault();
                                      c.handler(c, e);
                                  });
                              }
                          },
                          handler: function () {
                              config.parent.trigger("EnableDisablePollers.ChangeScanMode", 'silent', true);
                          }
                      }),
                cancelScanButton = new Ext.BoxComponent({
                    id: 'CancelScan',
                    autoEl: {
                        tag: 'a',
                        href: '#',
                        cls: 'sw-btn-secondary sw-btn sw-btn-cancel'
                    },
                    html: '<span class="sw-btn-c"><span class="sw-btn-scandialog">@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}</span></span>',
                    listeners: {
                        'afterrender': function (c) {
                            c.el.on('click', function (e) {
                                e.preventDefault();
                                c.handler(c, e);
                            });
                        }
                    },
                    handler: function () {
                        config.parent.trigger("EnableDisablePollers.CancelScan", config.pollerId);
                        // update UI
                        config.parent.trigger("EnableDisablePollers.ChangeScanMode", 'normal');
                    }
                })
            ],
            defaultTitle: '@{R=Core.Strings;K=WEBJS_PS1_55;E=js}',
            setTitle: function (title) {

                if (!title) {
                    title = this.defaultTitle;
                }
                var html = '<div class="scan-window-header">' +
                    '<img class="scan-window-header-icon" src="/Orion/images/throbber.gif">' + title + '</div>' +
                    '<div class="scan-window-body">' +
                    '@{R=Core.Strings;K=WEBJS_PS1_54;E=js}'.format({ PollerName: config.pollerName }) +
                    '<p> » <a href="' + SW.Core.HelpDocumentationHelper.getHelpLink('OrionCorePHDeviceStudioAssigningPollersScanningObjects') +'" target="_blank" rel="noopener noreferrer">@{R=Core.Strings;K=WEBJS_GK0_54;E=js}</a></p>' +
                    '</div>';
                
                if (this.body) {
                    this.body.dom.innerHTML = html;
                } else {
                    this.bodyCfg.html = html;
                }
            }

        });
    };

    var updateScanStatusTask = (function () {
        var isRunning = false;

        return {
            run: queryScanStatus,
            interval: 5000, // 5 seconds
            start: function () {
                if (!isRunning) {
                    Ext.TaskMgr.start(this);
                    isRunning = true;
                }
            },
            stop: function () {
                if (isRunning) {
                    Ext.TaskMgr.stop(this);
                    isRunning = false;
                }
            },
            restart: function () {
                this.stop();
                this.start();
            },
            isRunning: function () {
                return isRunning;
            }
        };
    }());

    self.updateScanStatusTask = updateScanStatusTask;
    self.getScanStatusInfo = getScanStatusInfo;
    self.changeScanOptions = changeScanOptions;
    

    var scanOptions = {
        // modes: silent (show progress above grid), normal (blocks ui)
        scanMode: 'silent',
        // initial list of scan statuses
        scanStatuses: [],
        // in order to make sure that we start with the scan window and not the silent mode
        // in case the scan is running for currently viewed poller.
        wasScanModeChangeRequestedByUser: false,
        // false when no scan statuses arrived yet, becomes true as first results come
        scanResultsAcquired: false,
        // keep information whether user hit Scan button
        scanRequested: false,
        // set to true when the grid was refreshed after successfull scan
        gridRefreshedAfterFinish: 0,
        // info about user clicked Cancel scan so UI can react to that and show something like scan canceling scan status message
        cancelScanPending: false
    };

    function changeScanOptions(options) {
        $.extend(scanOptions, options);
    }
    
    function removeScanStatus(scanStatus) {
        var index = _.indexOf(scanOptions.scanStatuses, scanStatus);

        if (index > -1) {
            scanOptions.scanStatuses.splice(index, 1);
        }

        updateScanStatus(scanOptions.scanStatuses);
    }

    function getScanStatusInfo() {

        var isAnyRunning = false;
        var pollerStatus = undefined;

        if (scanOptions.scanStatuses instanceof Array) {
            for (var i = 0; i < scanOptions.scanStatuses.length; i++) {
                var status = scanOptions.scanStatuses[i];
                if (status.ScanStatus == 2) {
                    isAnyRunning = true;
                }
                if (status.PollerId == config.pollerId) {
                    pollerStatus = status;
                }
            }
        }

        return {
            isRunning: isAnyRunning,
            currentPollerStatus: pollerStatus
        };
    }

    function getScanStatusMessageInfo(scanStatus) {
        var infos = [];
        var template, info;

        var getAcknowledgeButton = function (scanStatus) {

            return {
                buttonText: "<img src='/Orion/images/delete_icon_gray_16x16.png' class='enableDisableDeleteIcon'/>",
                buttonAction: function () { removeScanStatus(scanStatus); },
                type: "linkButton"
            };
        };

        var getInfo = function () {
            return {
                message: "",
                buttons: [],
                highlight: false
            };
        };

        switch (scanStatus.ScanStatus) {
            case -1://ui only: waiting for status - when scan started but not status was retrived yet
            case 1: // not started -  scan was scheduled and will start someday
                info = getInfo();
                template = scanStatus.CancelPending ? "@{R=Core.Strings;K=WEBJS_PS1_31;E=js}" : "@{R=Core.Strings;K=WEBJS_PS1_30;E=js}";
                info.message = "<img src='/Orion/images/loading_gen_16x16.gif'/> " + template.format(scanStatus);
                infos.push(info);
                break;
            case 0: //scan for this poller not exists - yet someone request it
                break;
            case 2:// running scan
                info = getInfo();

                if (scanStatus.CancelPending) {
                    template = "@{R=Core.Strings;K=WEBJS_PS1_31;E=js}";
                    info.cssClass = "icon-bar progress";
                    info.message = template.format(scanStatus);
                } else {
                    template = "@{R=Core.Strings;K=WEBJS_PS1_32;E=js} @{R=Core.Strings;K=WEBJS_PS1_39;E=js}";
                    info.cssClass = "icon-bar progress";
                    info.message = template.format(scanStatus);
                    info.buttons = [{
                        buttonText: "@{R=Core.Strings;K=WEBJS_PS1_33;E=js}",
                        buttonAction: function () {
                            cancelScan(scanStatus.PollerId);
                            // update UI
                            changeScanMode('silent');
                        }
                    }];
                }
                infos.push(info);
                break;
            case 3: // success
                info = getInfo();
                if (scanStatus.Enabled > 0) {
                    template = "@{R=Core.Strings;K=WEBJS_PS1_34;E=js}";
                    info.message = template.format(scanStatus);

                    template = "@{R=Core.Strings;K=WEBJS_PS1_35;E=js}";
                    info.message += " " + template.format(scanStatus);
                }
                else {
                    template = "@{R=Core.Strings;K=WEBJS_PS1_83;E=js}";
                    info.message = template.format(scanStatus);
                }

                // show close button only when multiple match does not occured, otherwise is close button part of the following message.
                // we do it this way to not have 2 close buttons for one message
                if (scanStatus.ConflictNetObjects.length === 0) {
                    info.buttons.push(getAcknowledgeButton(scanStatus));
                }
                infos.push(info);

                if (scanStatus.ConflictNetObjects.length > 0) {

                    info = getInfo();

                    template = "@{R=Core.Strings;K=WEBJS_PS1_36;E=js}";
                    var scanStatusExt = $.extend(scanStatus, { TechnologyName: config.technologyName });
                    info.message = template.format(scanStatusExt);
                    info.highlight = true;

                    info.buttons.push({
                        buttonText: "@{R=Core.Strings;K=WEBJS_AK0_28;E=js}",
                        buttonAction: function () { enablePollerOnNetobjects(scanStatus.PollerId, scanStatus.NetObjectType, scanStatus.ConflictNetObjects); removeScanStatus(scanStatus); }
                    });
                    info.buttons.push(getAcknowledgeButton(scanStatus));

                    infos.push(info);
                }


                break;
            case 4: // canceled
                info = getInfo();
                template = "@{R=Core.Strings;K=WEBJS_PS1_37;E=js}";
                info.message = template.format(scanStatus);
                info.buttons.push(getAcknowledgeButton(scanStatus));
                infos.push(info);
                break;
            case 5:  // failed
                info = getInfo();
                template = "@{R=Core.Strings;K=WEBJS_PS1_38;E=js}";
                info.message = template.format(scanStatus);
                info.buttons.push(getAcknowledgeButton(scanStatus));
                infos.push(info);
                break;
        }

        return infos;
    }

    function getScanStatusMessageBar(scanStatus) {
        var m;
        var messageInfos = getScanStatusMessageInfo(scanStatus);
        var messageBars = [];

        for (var infoCounter = 0; infoCounter < messageInfos.length; infoCounter++) {
            var messageInfo = messageInfos[infoCounter];

            var messageBar = new Ext.Panel({
                autoEl: {
                    tag: 'div',
                    cls: 'message-bar' + ((messageInfo.highlight) ? ' sw-suggestion sw-suggestion-noicon' : '')
                },
                baseCls: 'no-border' + ((messageInfo.highlight) ? '' : ' no-border-label')
            });
            var messageBarLabel = new Ext.BoxComponent({
                autoEl: {
                    tag: 'div',
                    cls: 'messageBarLabel ' + (messageInfo.cssClass || '')
                },
                html: messageInfo.message

            });
            messageBar.add(messageBarLabel);

            if (messageInfo.buttons.length > 0) {

                for (var i = 0; i < messageInfo.buttons.length; i++) {
                    var button = messageInfo.buttons[i];

                    if (button.type == "linkButton") {
                        m = new Ext.BoxComponent({
                            autoEl: {
                                tag: 'a',
                                href: '#'
                            },
                            html: button.buttonText,
                            listeners: {
                                'afterrender': function (c) {
                                    c.el.on('click', function (e) {
                                        e.preventDefault();
                                        c.handler(c, e);
                                    });
                                }
                            },
                            handler: button.buttonAction
                        });
                        messageBar.add(m);
                    }
                    else {
                        m = new Ext.BoxComponent({
                            autoEl: {
                                tag: 'span',
                                cls: 'statusbar-button'
                            },
                            html: SW.Core.Widgets.Button(button.buttonText, { type: 'small' }),
                            listeners: {
                                'afterrender': function (c) {
                                    c.el.on('click', function (e) {
                                        e.preventDefault();
                                        c.handler(c, e);
                                    });
                                }
                            },
                            handler: button.buttonAction
                        });
                        messageBar.add(m);
                    }

                }
            }
            messageBar.doLayout();
            messageBars.push(messageBar);
        }

        return messageBars;
    }

    function updateScanStatus(scanStatuses) {
        config.parent.trigger("EnableDisablePollers.UpdateToolbarButtons");
        
        var messageBars;
        var scanPanel = Ext.getCmp('scanPanel');

        switch (scanOptions.scanMode) {
            case 'silent':
                scanWindow.hide();
                scanPanel.show();

                scanPanel.removeAll(true);

                if (scanOptions.scanRequested && !getScanStatusInfo().currentPollerStatus) {
                    messageBars = getScanStatusMessageBar({ ScanStatus: -1, CancelPending: scanOptions.cancelScanPending });
                    scanPanel.items.addAll(messageBars);
                }

                for (var i = 0; i < scanStatuses.length; i++) {
                    messageBars = getScanStatusMessageBar(scanStatuses[i]);
                    scanPanel.items.addAll(messageBars);
                }

                var currentStatus = getScanStatusInfo().currentPollerStatus;
                if (currentStatus && currentStatus.ScanStatus == 2) {
                    scanOptions.gridRefreshedAfterFinish = 1;
                }
                if (currentStatus && currentStatus.ScanStatus >= 3 && scanOptions.gridRefreshedAfterFinish == 1) {
                    config.parent.reload();
                    scanOptions.gridRefreshedAfterFinish = 2;
                }

                scanPanel.doLayout();
                break;
            default:
                scanPanel.hide();

                if (scanOptions.cancelScanPending) {
                    scanWindow.setTitle('@{R=Core.Strings;K=WEBJS_PS1_31;E=js}');
                    cancelScanButton.disable();
                } else {
                    scanWindow.setTitle(scanWindow.defaultTitle);
                    cancelScanButton.enable();
                }
                scanWindow.show();
                break;
        }


        config.parent.trigger("EnableDisablePollers.SetMainGridHeight");
    }


    function queryScanStatusOnSuccess(data) {
        if (!updateScanStatusTask.isRunning() || !data) {
            return;
        }

        scanOptions.scanResultsAcquired = true;

        $.extend(data, { PollerName: config.pollerName });

        scanOptions.scanStatuses = [];

        // ignore acknowledge scan statuses or statuses indicating that there is no scan status for that poller 
        if (!data.StatusAcknowledged && data.ScanStatus > 0) {
            scanOptions.scanStatuses.push(data);
        }


        var s = getScanStatusInfo();

        if (scanOptions.scanRequested && s.currentPollerStatus) {
            scanOptions.scanRequested = false;
        }

        var isScanFinished = s.currentPollerStatus && (s.currentPollerStatus.ScanStatus > 2);
        var hasScanStarted = s.currentPollerStatus && (s.currentPollerStatus.ScanStatus > 0) && (s.currentPollerStatus.ScanStatus <= 2);
        var isScanRunningInNormalMode = scanOptions.scanRequested || (hasScanStarted && (scanOptions.wasScanModeChangeRequestedByUser == false));

        if (!isScanRunningInNormalMode) {
            changeScanMode('silent');
        }

        updateScanStatus(scanOptions.scanStatuses);

        // stop updating when scan not running
        if (isScanFinished && s.currentPollerStatus) {
            // auto-acknowledge any finished scan status (with any result)
            config.parent.Service.acknowledgeScanStatus(s.currentPollerStatus.PollerId);
            updateScanStatusTask.stop();
        }
    };

    function queryScanStatusOnError(err) {
        updateScanStatusTask.stop();
        errorHandler(err);
    }

    // calls a webservice which provides up-to-date status of current scan
    function queryScanStatus() {
        if (updateScanStatusTask.isRunning()) {
            ORION.callWebService("/Orion/Services/ManagePollers.asmx", "GetScanStatus", { technologyPollingId: config.pollerId }, queryScanStatusOnSuccess, queryScanStatusOnError);
        }
    }

    function changeScanMode(mode, requestedByUser) {

        if (requestedByUser) {
            scanOptions.wasScanModeChangeRequestedByUser = true;
        }

        scanOptions.scanMode = mode;
        updateScanStatus(scanOptions.scanStatuses);
    }

    function enablePollerOnNetobjects(pollerId, netObjectType, netObjectIdList) {
        config.parent.Service.callEnableDisableService(pollerId, true, netObjectIdList, false, null, null, null, function() { config.parent.reload(); }, function(result) { errorHandler(result); });
    }

    function cancelScan(pollerId) {

        scanOptions.cancelScanPending = true;
        updateScanStatusTask.stop();

        ORION.callWebService('/Orion/Services/ManagePollers.asmx', 'CancelPollerScan', { technologyPollingId: pollerId }, function () {
            // note that we just know that the request was delivered to the service, but we yet do not know
            // whether was the scan actually canceled, we are inform about that through normal scan Status updating.
            updateScanStatusTask.start();
        }, function () {
            updateScanStatusTask.start();
        });

    }
    
    //Error handler
    function errorHandler(result) {
        if (result != null) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_SO0_24;E=js}', msg: result, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }
}(SW.Core.EnableDisablePollers.Scanner = SW.Core.EnableDisablePollers.Scanner || {}, jQuery));