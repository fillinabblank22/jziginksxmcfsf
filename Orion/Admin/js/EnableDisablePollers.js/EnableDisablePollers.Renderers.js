﻿Ext.namespace('SW.Core.EnableDisablePollers');
Ext.namespace('SW.Core.EnableDisablePollers.Renderers');

(function (self, $, undefined) {
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    var lang = '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}';

    function getImage(onoff, text) {
        var partPath = '';
        if (lang !== 'en' && lang !== 'is')
            partPath = lang + "/";

        return String.format('<img class="statusSlider" src="/Orion/images/{2}Scan_{0}.png" alt="{1}" title="{1}"/>', onoff, text, partPath);
    }

    var config = {
        parent: null,
        isDemo: false,
        pollerStatusEnum: {},
        scanResultEnum: {},
    };
    
    // public methods
    self.init = function init(c) {
        $.extend(config, c);
    };

    self.renderNode = function(value, meta, record) {

        value = value.toSecureString();
        value = renderString(value, meta, record);

        var node = ObjectStringPropertiesToSecureString(record.data);
        node.NodeCaption = value;
        return makeNodeLink(node);
    };

    self.renderPollerStatus = function(value, meta, record) {
        if (value == config.pollerStatusEnum.disabled) {
            return getImage('Off', '@{R=Core.Strings;K=WEBJS_VB0_28; E=js}'); 
        } else if (value == config.pollerStatusEnum.enabled) {
            return getImage('On', '@{R=Core.Strings;K=WEBJS_VB0_30; E=js}');
        } else if (value == config.pollerStatusEnum.notAMatch) {
            return getImage('Off', '@{R=Core.Strings;K=WEBJS_PS1_41; E=js}');
        } else if (value == config.pollerStatusEnum.notScanned) {
            return getImage('Off', '@{R=Core.Strings;K=WEBJS_PS1_40; E=js}');
        }
        return '';
    };

    self.renderScanStatus = function(value, meta, record) {
        var html;
        if (value == config.scanResultEnum.exactMatch) {
            return "<img class=\"scanStatusIcon\" src=\"/Orion/Images/ok_16x16.gif\" alt=\"@{R=Core.Strings;K=WEBJS_PS1_27; E=js}\" title=\"@{R=Core.Strings;K=WEBJS_PS1_27; E=js}\"/> @{R=Core.Strings;K=WEBJS_PS1_27; E=js}";
        }

        if (value == config.scanResultEnum.multipleMatches) {
            var message = "@{R=Core.Strings;K=WEBJS_PS1_42; E=js}";
            var count = record.get("AssignmentsCount") - 1;
            message = message.replace("{num}", count);

            html = "<img class=\"scanStatusIcon\" src=\"/Orion/Images/lightbulb_tip_16x16.gif\" alt=\"@{R=Core.Strings;K=WEBJS_PS1_28; E=js}\" title=\"@{R=Core.Strings;K=WEBJS_PS1_28; E=js}\"/> @{R=Core.Strings;K=WEBJS_PS1_28; E=js}";
            if (!config.isDemo) {
                html += " <span class=\"scanResultLink\">» <span onclick=\"SW.Core.EnableDisablePollers.openListResources(" + record.get("NodeId") + ")\">" + message + "</span></span>";
            }
            return html;
        }
        if (value == config.scanResultEnum.notAMatch) {
            return "<img class=\"scanStatusIcon\" src=\"/Orion/Images/failed_16x16.gif\" alt=\"@{R=Core.Strings;K=WEBJS_PS1_25; E=js}\" title=\"@{R=Core.Strings;K=WEBJS_PS1_25; E=js}\"/> @{R=Core.Strings;K=WEBJS_PS1_25; E=js}";
        }
        if (value == config.scanResultEnum.notScanned) {
            if (record.get("NodeStatus") == 0) {
                html = "<img class=\"scanStatusIcon\" src=\"/Orion/Images/warning_16x16.gif\" alt=\"@{R=Core.Strings;K=WEBJS_PS1_43; E=js}\" title=\"@{R=Core.Strings;K=WEBJS_PS1_43; E=js}\"/> <span class=\"statusExplanation\" title=\"@{R=Core.Strings;K=WEBJS_PS1_44; E=js}\">@{R=Core.Strings;K=WEBJS_PS1_43; E=js}</span>";
            } else {
                html = "<img class=\"scanStatusIcon\" src=\"/Orion/Images/warning_16x16.gif\" alt=\"@{R=Core.Strings;K=WEBJS_PS1_26; E=js}\" title=\"@{R=Core.Strings;K=WEBJS_PS1_26; E=js}\"/> @{R=Core.Strings;K=WEBJS_PS1_26; E=js}";
                if (!config.isDemo) {
                    html += " <span class=\"scanResultLink\">» <span onclick=\"SW.Core.EnableDisablePollers.doScan(" + record.get("NodeId") + ")\">@{R=Core.Strings;K=WEBJS_PS1_45; E=js}</span></span>";
                }
            }

            return html;
        }
        return '<span class="scanNotAvailable" >@{R=Core.Strings;K=WEBJS_PS1_29; E=js}</span>';
    };

    self.renderGroup = function(value, meta, record, comboValue) {
        var disp;
        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            disp = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) + " (" + record.data.Cnt + ")";
        } else {
            var selectedgroupByOption = comboValue;
            // use custom mapping for values of particular columns
            if (selectedgroupByOption == "n.Status") {
                value = getStatusText(value.toString());
            } else if (selectedgroupByOption == "n.ObjectSubType") {
                value = getPollingMethodText(value);
            } else if (selectedgroupByOption == "n.IPAddressType") {
                value = getIPAddressTypeText(value);
            } else if (selectedgroupByOption == "PollerStatus") {
                value = getPollerStatusText(value);
            } else if (selectedgroupByOption == "ScanStatus") {
                value = getScanStatusText(value);
            }

            if (value == null) {
                value = "null";
            }

            if (comboValue.indexOf('Format') > -1) {
                disp = renderFieldType(value) + " (" + record.data.Cnt + ")";
            } else {
                disp = renderFieldType($.trim(String((Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : value))) + " (" + record.data.Cnt + ")";
            }
        }

        return disp;
    };

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    };
    
    function makeNodeLink(node) {
        return '<a href="/Orion/View.aspx?NetObject=N:{NodeId}"><img NetObject=N:{NodeId} class="StatusIcon" src="/Orion/images/StatusIcons/Small-{NodeGroupStatus}" alt="{NodeStatusDescription}"/> <span>{NodeCaption}</span></a>'.format(node);
    };
    
    // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function getStatusText (statusValue) {
        return SW.Core.Status.Get(statusValue).ShortDescription;
    }

    function getIPAddressTypeText(addressType) {
        switch (addressType) {
            case "IPv4": return "@{R=Core.Strings;K=IP_V_4;E=js}";
            case "IPv6": return "@{R=Core.Strings;K=IP_V_6;E=js}";
            default: return addressType;
        }
    }

    function getPollingMethodText(pollingMethod) {
        switch (pollingMethod) {
            case "SNMP": return "@{R=Core.Strings;K=Discovery_NetworkSonarWizard_SNMP;E=js}";
            case "ICMP": return "@{R=Core.Strings;K=WEBJS_TM0_112;E=js}";
            case "WMI": return "@{R=Core.Strings;K=WEBJS_TM0_113;E=js}";
            default: return pollingMethod;
        }
    }
    
    function getPollerStatusText(pollerStatus) {
        switch (pollerStatus) {
            case config.pollerStatusEnum.disabled: return "@{R=Core.Strings;K=WEBJS_PS1_24;E=js}";
            case config.pollerStatusEnum.enabled: return "@{R=Core.Strings;K=WEBJS_PS1_23;E=js}";
            case config.pollerStatusEnum.notAMatch: return "@{R=Core.Strings;K=WEBJS_PS1_25;E=js}";
            case config.pollerStatusEnum.notScanned: return "@{R=Core.Strings;K=WEBJS_PS1_26;E=js}";
            default: return pollerStatus;
        }
    };

    function getScanStatusText(scanStatus) {
        switch (scanStatus) {
            case config.scanResultEnum.exactMatch: return "@{R=Core.Strings;K=WEBJS_PS1_27; E=js}";
            case config.scanResultEnum.multipleMatches: return "@{R=Core.Strings;K=WEBJS_PS1_28; E=js}";
            case config.scanResultEnum.notAMatch: return "@{R=Core.Strings;K=WEBJS_PS1_25; E=js}";
            case config.scanResultEnum.notScanned: return "@{R=Core.Strings;K=WEBJS_PS1_26; E=js}";
            default: return "@{R=Core.Strings;K=WEBJS_PS1_29; E=js}";
        }
    };
    
    function renderFieldType(value) {
        if (!value || value === "") {
            return '@{R=Core.Strings;K=WEBJS_PS1_3; E=js}'; //[Unknown]
        }
        return Ext.util.Format.htmlEncode(value);
    }
    
    function ObjectStringPropertiesToSecureString(obj) {
        if ((obj != null) && typeof obj === 'object') {
            try {
                obj = Object.create(obj); // create copy of object
            } catch (e) { }
        }

        if (typeof obj !== 'undefined') {
            for (var p in obj) {
                if (typeof (p) != 'undefined') {
                    if (typeof (obj[p]) == 'string') {
                        obj[p] = obj[p].toSecureString();
                    }
                }
            }
        }

        return obj;
    };
}(SW.Core.EnableDisablePollers.Renderers = SW.Core.EnableDisablePollers.Renderers || {}, jQuery));