﻿Ext.namespace('SW');
Ext.namespace('SW.Core');
Ext.namespace('SW.Core.EnableDisablePollers');
Ext.QuickTips.init();

String.prototype.toSecureString = function () {
    return this.replace(/</ig, "&lt").replace(/>/ig, "&gt;");
};

String.prototype.format = function (params) {
    var pattern = /{([^}]+)}/g;
    return this.replace(pattern, function ($0, $1) { return params[$1]; });
};

(function (self, $, undefined) {
    var local = self._local = self._local || {};

    var seal = self._seal = self._seal || function () {
        delete self._local;
        delete self._seal;
    };
    var unseal = self._unseal = self._unseal || function () {
        self._local = local;
        self._seal = seal;
        self._unseal = unseal;
    };
    
    var pollerStatusEnum = {
        disabled: 'Disabled',
        enabled: 'Enabled',
        notAMatch: 'NotAMatch',
        notScanned: 'NotScanned'
    };

    var scanResultEnum = {
        exactMatch: 'ExactMatch',
        multipleMatches: 'MultipleMatches',
        notAMatch: 'NotAMatch',
        notScanned: 'NotScanned'
    };

    // Ext js components
    var comboArray,
        pageSizeBox,
        grid,
        groupGrid,
        searchField,
        scanStatusPanel,
        userPageSize,
        selectorModel,
        dataStore,
        pollerId = "",
        pollerName = "",
        technologyName = "",
        allNodesAreSelected = false,
        helpLang = 'en',
        isScanSupported;

    var isDemo = $('#isDemoMode').length != 0;
    var allowAdmin = false;
    
    // make this observable
    SW.Core.Observer.makeObserver(self);

    self.on("EnableDisablePollers.SetMainGridHeight", setMainGridHeight);
    self.on("EnableDisablePollers.EnableDisablePollers", enableDisablePollers);
    self.on("EnableDisablePollers.StartScan", startScan);
    self.on("EnableDisablePollers.UpdateToolbarButtons", updateToolbarButtons);


    // Returns a name of Group by property selected in combobox in case one of the items in Group by grid is selected; otherwise empty string is returned.
    // This solves the case when the main grid needs to refresh but any item in group grid was yet selected - so user want to see all results like there was
    // nothing selected in the group by combobox.
    function getGroupByProperty() {
        var property = '';

        var selectedGroupByOption = comboArray.getValue();
        var isItemSelectedInGroupGrid = groupGrid.selModel.selections.items.length > 0;

        if (selectedGroupByOption && isItemSelectedInGroupGrid) {
            property = selectedGroupByOption;
        }

        return property;
    }

    function updateToolbarButtons() {
        var map = grid.getTopToolbar().items.itemAt(0).items.map;
        var selModel = grid.getSelectionModel();
        var selCount = selModel.getCount();
        var disableScan = true;
        var disableStatusChange = true;

        // handle buttons state by selected lines
        
        if (allNodesAreSelected) {
            disableScan = false;
            disableStatusChange = false;
        }
        else {
            for (var i = 0; i < selCount; i++) {
                var itemData = selModel.getSelections()[i].data;
                if (itemData.NodeStatus == 1) {
                    disableScan = false;
                }
                if (itemData.ScanResult == scanResultEnum.exactMatch || itemData.ScanResult == scanResultEnum.multipleMatches) {
                    disableStatusChange = false;
                }
            }
        }
        if (isDemo || !allowAdmin) {
            // The buttons are disabled in demo mode
            if (isScanSupported) {
                map.Scan.setDisabled(true);
            }
            map.EnablePolling.setDisabled(true);
            map.DisablePolling.setDisabled(true);
        } else {
            // disable scan when some other is running, nothing is selected or this is the native poller
            if (isScanSupported) {
                map.Scan.setDisabled(self.Scanner.getScanStatusInfo().isRunning || selCount == 0 || disableScan);
            }
            map.EnablePolling.setDisabled(selCount === 0 || disableStatusChange);
            map.DisablePolling.setDisabled(selCount === 0 || disableStatusChange);
        }


        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };


    

    function refreshObjects(elem, property, value, search, callback) {
        elem.store.removeAll();

        elem.store.proxy.conn.jsonData = { property: property, value: value || "", search: getSearchString(search) };
        elem.store.load({ params: { pollerId: pollerId }, callback: callback });
    };

    function refreshGridObjects(start, limit, elem, property, value, search, callback) {
        elem.store.removeAll();

        //Cast value to string to prepare for logic operations (e.g. 0 == false, but "0" == true)
        if (value !== undefined || value !== null) {
            value = value.toString();
        }

        elem.store.proxy.conn.jsonData = { property: property, value: value || "", search: getSearchString(search) };
        elem.store.load({ params: { start: start, limit: limit, pollerId: pollerId }, callback: callback });
    };

    function getSearchString(search) {
        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }
        return tmpfilterText;
    }
    //Error handler
    function errorHandler(result) {
        if (result != null) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_SO0_24;E=js}', msg: result, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }

    // update whatever is needed when search was triggered. Note that search text could be also changed to empty text which means that search criteria is not applied
    function searchStartedHandler(searchText) {
        var selectedGrouping = groupGrid.getSelectionModel().getSelected();
        selectedGrouping = selectedGrouping ? selectedGrouping.data.Value : '';
        // refresh just the grouping grid as the main grid is refreshed automatically as it is bound to the same store as searchfield.
        refreshObjects(groupGrid, comboArray.getValue(), "", searchText,  function () { reselectGroupingGrid(selectedGrouping); });
    }

    function setMainGridHeight() {
        window.setTimeout(function () {
            var mainGrid = Ext.getCmp('mainGrid');
            var maxHeight = calculateMaxGridHeight(mainGrid);
            var height = maxHeight;
            setHeightForGrids(height);
        }, 0);
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp('mainGrid');
        var groupingGrid = Ext.getCmp('groupingGrid');

        mainGrid.setHeight(Math.max(height, 300));
        groupingGrid.setHeight(Math.max(height - 50, 250)); //height without groupByTopPanel

        mainGrid.doLayout();
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.getPosition()[1];
        return $(window).height() - gridTop - $('#footer').height() - 40;
    }

    function callScanService(objectIds, allNodes) {
        self.Scanner.updateScanStatusTask.stop();
        
        grid.getEl().mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
        
        self.Service.callScanService(pollerId, objectIds, allNodes, getSearchString(filterText),
            groupGrid.getSelectionModel().getSelected(), comboArray.getValue(),
            function (result) {
                if (result) {
                    var enqueueResult = JSON.parse(result);
                    if (enqueueResult.Success) {
                        grid.getEl().unmask();
                        self.Scanner.changeScanOptions({ scanRequested: true, gridRefreshedAfterFinish: 1, cancelScanPending: false });
                        self.trigger("EnableDisablePollers.ChangeScanMode", 'normal');
                        self.Scanner.updateScanStatusTask.start();
                    } else {
                        if (enqueueResult.Message) {
                            Ext.Msg.show({ title: "@{R=Core.Strings;K=SyslogSeverity_warning;E=js}", msg: enqueueResult.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING });
                        } else {
                            Ext.Msg.show({ title: "@{R=Core.Strings;K=SyslogSeverity_error;E=js}", msg: "@{R=Core.Strings;K=WEBJS_PS1_46;E=js}", buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        }
                        grid.getEl().unmask();
                    }
                }
            },
            function(result) {
                errorHandler(result);
                grid.getEl().unmask();
                self.Scanner.updateScanStatusTask.start();
            });
    }
    
    function callEnableDisableService(enabled, objectIds, allNodes) {
        if (!allNodes && objectIds.length === 0) {
            return;
        }
        grid.getEl().mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
        
        self.Service.callEnableDisableService(pollerId, enabled, objectIds, allNodes, getSearchString(filterText),
            groupGrid.getSelectionModel().getSelected(), comboArray.getValue(),
            function (result) {
                updateToolbarButtons();
                grid.getEl().unmask();
                if (result.length > 0) {
                    reload();
                }
            },
            function() {
                var message = enabled ? "@{R=Core.Strings;K=WEBJS_PS1_47;E=js}" : "@{R=Core.Strings;K=WEBJS_PS1_48;E=js}";
                Ext.Msg.show({ title: "@{R=Core.Strings;K=SyslogSeverity_error;E=js}", msg: message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                grid.getEl().unmask();
            });
    }
    
    function enableDisablePollers(newState) {
        var selection = grid.getSelectionModel().getSelections();
        var objectIds = [];
        $.each(selection, function(index, value) {
            //Only enabled pollers can be disabled.
            if (value.data.PollerStatus == newState ? pollerStatusEnum.disabled : pollerStatusEnum.enabled) {
                objectIds.push(value.data.NodeId);
            }
        });
        callEnableDisableService(newState, objectIds, allNodesAreSelected);
    }
    
    function startScan() {
        var selection = grid.getSelectionModel().getSelections();
        var objectIds = [];
        $.each(selection, function (index, value) {
            //Only scan nodes which are able to be scanned
            if (value.data.NodeStatus == 1) {
                objectIds.push(value.data.NodeId);
            }
        });
        if (!allNodesAreSelected && objectIds.length == 0) {
            return;
        }
        callScanService(objectIds, allNodesAreSelected);
    }
    
    function reload()
    {
        var selectedGrouping = groupGrid.getSelectionModel().getSelected();
        var selectInGrid = selectedGrouping ? true : false;
        selectedGrouping = selectedGrouping ? selectedGrouping.data.Value : '';
        var selectedGroupBy = comboArray.getValue();
        // reloads the grouping grid, cancels any custom selection, just load data for selected group in combobox
        refreshObjects(groupGrid, selectedGroupBy, selectedGrouping, filterText, function () { reselectGroupingGrid(selectedGrouping); });
        // reload data in main grid
        refreshGridObjects(0, userPageSize, grid, selectInGrid ? selectedGroupBy : '', selectedGrouping, filterText);
    }
    
    function reselectGroupingGrid(propertyValue) {
        // select propertyValue if defined
        var store = groupGrid.getStore();

        var foundIndex = -1;
        $.each(store.data.items, function (index, value) {
            var propValue = value.get("Value");
            if (propValue == propertyValue) {
                foundIndex = index;
                return false;
            }
        });

        if (foundIndex > -1) {

            (function () {
                var sm = groupGrid.getSelectionModel();
                sm.suspendEvents(false);
                sm.selectRecords([store.data.items[foundIndex]], false);
                sm.resumeEvents();
            }).defer(1);
        }
    }
    
    //Ext grid
    self.reload = reload;

    self.init = function () {

        seal();

        // NetObject picker overides the orion prefix and thats we need to specify it again so it works for manage device pollers grid and action above the grid
        ORION.prefix = "EnableDisablePollers_";
        pollerId = ORION.Prefs.load('PollerId');
        pollerName = ORION.Prefs.load('PollerName');
        technologyName = ORION.Prefs.load('TechnologyName');
        helpLang = ORION.Prefs.load('HelpLang');
        isScanSupported = SW.Core.EnableDisablePollers.ScanSupported;

        // don't do anything when we don't have the poller id - instead we show error message
        if (!pollerId)
            return;
        
        // init submodules
        self.Toolbar.init({ parent: self, isDemo: isDemo, allowAdmin: allowAdmin });
        self.Scanner.init({ parent: self, pollerName: pollerName, technologyName: technologyName, pollerStatusEnum: pollerStatusEnum, scanResultEnum: scanResultEnum, pollerId: pollerId, helpLang: helpLang });
        self.Service.init({ parent: self });
        self.Renderers.init({ parent: self, isDemo: isDemo, pollerStatusEnum: pollerStatusEnum, scanResultEnum: scanResultEnum });

        userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));

        selectorModel = new Ext.grid.CheckboxSelectionModel();
        selectorModel.on("selectionchange", function () {
            updateToolbarButtons();
            Ext.getCmp('selectAllToolbar').hide();
            if (allNodesAreSelected) {
                allNodesAreSelected = false;
            }
        });

        ORION.prefix = "EnableDisablePollers_";
        allowAdmin = (ORION.Prefs.load('AllowAdmin', 'false')).toLowerCase() == 'true';

        dataStore = new ORION.WebServiceStore(
                            "/Orion/Services/ManagePollers.asmx/GetNodesWithPollerStatus",
                            [
                                { name: 'NodeId', mapping: 0 },
                                { name: 'NodeCaption', mapping: 1 },
                                { name: 'NodeGroupStatus', mapping: 2 },
                                { name: 'NodeStatusDescription', mapping: 3 },
                                { name: 'PollerStatus', mapping: 4 },
                                { name: 'ScanResult', mapping: 5 },
                                { name: 'AssignmentsCount', mapping: 6 },
                                { name: 'NodeStatus', mapping: 7 }
                            ],
                                "NodeCaption");
        dataStore.on('beforeload', function (source, operation) {
            operation.params = Ext.apply(operation.params || {}, {
                pollerId: pollerId
            });
        });

        var groupingStore = new ORION.WebServiceStore(
                            "/Orion/Services/ManagePollers.asmx/GetNodesWithPollerStatusGroupValues",
                            [
                                { name: 'Value', mapping: 0 },
                                { name: 'Cnt', mapping: 1 }
                            ],
                            "Value", "");

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            enableKeyEvents: true,
            allowNegative: false,
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 100,
            value: userPageSize,
            listeners: {
                scope: this,
                'keydown': function (f, e) {
                    var k = e.getKey();
                    if (k == e.RETURN) {
                        e.stopEvent();
                        var v = parseInt(f.getValue());
                        if (!isNaN(v) && v > 0 && v <= 100) {
                            userPageSize = v;
                            pagingToolbar.pageSize = userPageSize;
                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                        }
                        else {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",//Invalid page size
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                    }
                },
                'focus': function (field) {
                    field.el.dom.select();
                },
                'change': function (f, numbox, o) {
                    var pSize = parseInt(f.getValue());
                    if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                        Ext.Msg.show({
                            title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                            msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",//Invalid page size
                            icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                        });
                        return;
                    }

                    if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                        pagingToolbar.pageSize = pSize;
                        userPageSize = pSize;

                        ORION.Prefs.save('PageSize', userPageSize);
                        pagingToolbar.doLoad(0);
                    }
                }
            }
        });

        var pagingToolbar = new Ext.PagingToolbar(
            {
                store: dataStore,
                pageSize: userPageSize,
                displayInfo: true,
                displayMsg: "@{R=Core.Strings;K=WEBJS_VB0_66; E=js}", // Displaying net objects {0} - {1} of {2}
                emptyMsg: "@{R=Core.Strings;K=WEBJS_VB0_65; E=js}", // No net objects to display
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                items: [
                '-',
                new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                pageSizeBox
                ]
            }
        );

        comboArray = new Ext.form.ComboBox(
              {
                  mode: 'local',
                  triggerAction: 'all',
                  typeAhead: true,
                  forceSelection: true,
                  selectOnFocus: false,
                  multiSelect: false,
                  transform: 'groupByProperty',
                  editable: false,
                  listeners: {
                      'select': function () {
                          var val = this.getValue();

                          refreshObjects(groupGrid, val, "", filterText);
                          if (val == '')
                              refreshGridObjects(0, userPageSize, grid, "", "", filterText, null);
                      }
                  }
              });


        searchField = new Ext.ux.form.SearchField({
            store: dataStore,
            width: 200
        });

        searchField.on('searchStarted', searchStartedHandler, this);

        grid = new Ext.grid.GridPanel({
            id: 'assignmentsGrid',
            region: 'center',
            viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_VB0_65; E=js}", markDirty: false },
            store: dataStore,
            baseParams: { start: 0, limit: userPageSize },
            stripeRows: true,
            trackMouseOver: false,
            enableColumnHide: false,
            columns: [selectorModel,
                     {
                         id: 'Id',
                         header: "@{R=Core.Strings;K=WEBJS_AK0_44; E=js}",
                         dataIndex: 'NodeCaption',
                         width: 200,
                         sortable: true,
                         hideable: false,
                         renderer: self.Renderers.renderNode
                     }, {
                         header: "@{R=Core.Strings;K=WEBJS_PS1_50; E=js}",
                         dataIndex: 'ScanResult',
                         width: 380,
                         sortable: false,
                         hideable: false,
                         renderer: self.Renderers.renderScanStatus
                     }, {
                         header: "@{R=Core.Strings;K=WEBJS_PS1_51; E=js}",
                         dataIndex: 'PollerStatus',
                         width: 100,
                         sortable: false,
                         hideable: false,
                         renderer: self.Renderers.renderPollerStatus
                     }
            ],
            listeners: {
                'cellclick': function (object, index, column, event) {
                    if (isDemo || !allowAdmin) {
                        return;
                    }
                    var linkClicked = (event.target.tagName === 'IMG' && event.target.className === 'statusSlider');
                    if (linkClicked) {
                        {
                            event.preventDefault();
                            grid.getEl().mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
                            var record = object.getStore().getAt(index);
                            var pollerStatus = record.get('PollerStatus');
                            var targetStatus = pollerStatus == pollerStatusEnum.disabled ? true : false;
                            var nodeId = record.get('NodeId');

                            callEnableDisableService(targetStatus, [nodeId]);
                        }
                    }
                },
                'headerclick': function (grid, columnIndex, event) {
                    //Check if the check all checkbox was clicked
                    var checkAllClicked = (event.target.tagName === 'DIV' && event.target.className.indexOf('checker') !== -1);
                    if (checkAllClicked) {
                        if (dataStore.getCount() !== dataStore.getTotalCount()) {
                            var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
                            var selectToolbar = Ext.getCmp('selectAllToolbar');
                            if (hd.hasClass('x-grid3-hd-checker-on')) {
                                var label = Ext.getCmp('selectedNodesLabel');
                                label.setText(String.format('@{R=Core.Strings;K=WEBJS_PS1_52; E=js}', dataStore.getCount()));
                                var button = Ext.getCmp('selectAllNodesButton');
                                button.setText(String.format('@{R=Core.Strings;K=WEBJS_PS1_53; E=js}', dataStore.getTotalCount()));
                                selectToolbar.show();
                                setMainGridHeight();
                            } else {
                                selectToolbar.hide();
                                setMainGridHeight();
                            }
                        }
                    }
                }
            },
            sm: selectorModel,
            autoScroll: true,
            loadMask: true,
            width: 795,
            height: 500,
            tbar: new Ext.Panel({
                items: [
                    {
                        xtype: 'toolbar',
                        items: self.Toolbar.getToolbarButtons(searchField)
                    },
                    {
                        xtype: 'toolbar',
                        hidden: true,
                        id: 'selectAllToolbar',
                        items: [
                            {
                                xtype: 'label',
                                id: 'selectedNodesLabel'
                            },
                            {
                                xtype: 'button',
                                id: 'selectAllNodesButton',
                                handler: function () {
                                    allNodesAreSelected = true;
                                    updateToolbarButtons();
                                    Ext.getCmp('selectAllToolbar').hide();
                                    setMainGridHeight();
                                }
                            }
                        ]
                    }]
            }),
            bbar: pagingToolbar
        });

        grid.store.on('beforeload', function () { grid.getEl().mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading'); });
        grid.store.on('load', function () {
            grid.getEl().unmask();
        });

        grid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VB0_133;E=js}',
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });
            grid.getEl().unmask();
        });

        groupGrid = new Ext.grid.GridPanel({
            id: 'groupingGrid',
            store: groupingStore,
            cls: 'hide-header',
            columns: [
                {
                    width: 193,
                    editable: false,
                    sortable: false,
                    dataIndex: 'Value',
                    renderer:
                        function (value, meta, record) {
                            return self.Renderers.renderGroup(value, meta, record, comboArray.getValue());
                        },
                    id: 'Value'
                }
            ],
            selModel: new Ext.grid.RowSelectionModel(),
            autoScroll: true,
            loadMask: true,
            listeners: {
                cellclick: function (mygrid, row, cell, e) {
                    var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                    if (val == '@{R=Core.Strings;K=WEBJS_PS1_2;E=js}')
                        val = '';

                    refreshGridObjects(0, userPageSize, grid, comboArray.getValue(), val, filterText, null);
                }
            },
            anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
        });

        var groupByTopPanel = new Ext.Panel({
            id: 'groupByTopPanel',
            region: 'centre',
            split: false,
            heigth: 50,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76;E=js}" }), comboArray],
            cls: 'panel-no-border panel-bg-gradient'
        });

        var navPanel = new Ext.Panel({
            id: 'groupByPanel',
            region: 'west',
            split: true,
            anchor: '0 0',
            width: 200,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [groupByTopPanel, groupGrid],
            cls: 'panel-no-border'
        });

        navPanel.on("bodyresize", function () {
            groupGrid.setWidth(navPanel.getSize().width);
        });

        scanStatusPanel = new Ext.Panel({
            id: 'scanPanel',
            collapsible: false,
            items: [],
            layout: 'auto',
            align: 'left',
            baseCls: 'no-border',
            width: '100%'

        });


        var mainGridPanel = new Ext.Panel({
            id: 'mainGrid',
            region: 'center',
            split: true,
            layout: 'border',
            collapsible: false,
            items: [navPanel, grid],
            cls: 'no-border'
        });

        mainGridPanel.render('Grid');
        scanStatusPanel.render('StatusBar');
        setMainGridHeight();

        $(window).resize(function () {
            setMainGridHeight();
            mainGridPanel.doLayout();
        });

        grid.getEl().mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');

        var startFrom = 0;

        // schedule scan status updating
        if (isScanSupported) {
            self.Scanner.updateScanStatusTask.start();
        }

        var queryArguments = SW.Core.UriHelper.decodeURIParams(window.location.search);

        var property = getGroupByProperty();
        var propertyValue = "";

        if (queryArguments && queryArguments.groupBy) {
            property = queryArguments.groupBy;
            propertyValue = queryArguments.groupByValue || "";
            comboArray.setValue(property);
        }

        refreshObjects(groupGrid, property, "", "", function() { reselectGroupingGrid(propertyValue); });
        // in case propertyValue is empty we need to emulate show all functionality, thus send empty property to the method...
        refreshGridObjects(startFrom, userPageSize, grid, (propertyValue) ? property : "", propertyValue, filterText, function () { });
        updateToolbarButtons();
    };

    self.doScan = function(netObjectId) {
        var status = self.Scanner.getScanStatusInfo().currentPollerStatus;
        var isRunning = status && status.ScanStatus == 2;
        if (isRunning) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=SyslogSeverity_warning;E=js}', msg: '@{R=Core.Strings;K=WEBJS_PS1_49;E=js}', buttons: Ext.Msg.OK });
        } else {
            callScanService([netObjectId], false);
        }
    };

    self.openListResources = function(netObjectId) {
        var url = "/Orion/Nodes/ListResources.aspx?Nodes={0}&ReturnTo={1}&SubType=SNMP";
        window.location = url.format([netObjectId, $("#EnableDisablePollers_ReturnUrl").val()]);
};


}(SW.Core.EnableDisablePollers = SW.Core.EnableDisablePollers || {}, jQuery));

Ext.onReady(SW.Core.EnableDisablePollers.init, SW.Core.EnableDisablePollers);