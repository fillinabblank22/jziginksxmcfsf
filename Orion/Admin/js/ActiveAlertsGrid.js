﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
Ext.Tip.prototype.maxWidth = 350;

SW.Orion.ActiveAlerts = function () {
	var IsFederationEnabled = false;
	var IsIntegrationInitialized = false;
    var IsDemoServer = false;
	
    // CONSTRUCTOR
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;

    var getModifiedFilterText = function (filterText) {
        var tmpfilterText = "";
        tmpfilterText = filterText;

        if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
            tmpfilterText = "";
        }

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }
        return tmpfilterText;
    };

     //Override onTrigger2Click. Needs for correct search in DataTable Rows
     // According to FB330099
     Ext.override(Ext.ux.form.SearchField, {
         
         //  this is the search icon button in the search field
         onTrigger2Click: function () {

        //set the global variable for the SQL query and regex highlighting
        filterText = this.getRawValue();

        if (filterText.length < 1) {
            this.onTrigger1Click();
            return;
        }

        var tmpfilterText = getModifiedFilterText(filterText);

        // provide search string for Id param when loading the page
        this.store.proxy.conn.jsonData = { property: filterPropertyParam, type: filterTypeParam, value: filterValueParam, search: tmpfilterText, WithoutAcknowledgedAlerts: ORION.Prefs.load('WithoutAcknowledgedAlerts', "false"), FederationEnabled: IsFederationEnabled };

	    var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
        this.store.load({ params: o });

        this.hasSearch = true;
        this.triggers[0].show();

        this.fireEvent('searchStarted', filterText);
         }
     });
    
    Ext.grid.AlertGridView = Ext.extend(Ext.grid.GridView, {
        fitColumns: function (preventRefresh, onlyExpand, omitColumn) {
            var grid = this.grid,
            colModel = this.cm,
            totalColWidth = colModel.getTotalWidth(false),
            gridWidth = this.getGridInnerWidth(),
            extraWidth = gridWidth - totalColWidth,
            columns = [],
            extraCol = 0,
            width = 0,
            colWidth, fraction, i;

            if (gridWidth < 20 || extraWidth === 0) {
                return false;
            }

            var visibleColCount = colModel.getColumnCount(true),
                totalColCount = colModel.getColumnCount(false),
                adjCount = visibleColCount - (Ext.isNumber(omitColumn) ? 1 : 0);

            if (adjCount === 0) {
                adjCount = 1;
                omitColumn = undefined;
            }

            var colWidthCorrection = 0;

            for (i = 0; i < totalColCount; i++) {
                if (!colModel.isFixed(i) && i !== omitColumn) {
                    colWidth = colModel.getColumnWidth(i);
                    if (i == 0)
                        colWidth -= colWidthCorrection;

                    columns.push(i, colWidth);

                    if (!colModel.isHidden(i)) {
                        extraCol = i;
                        width += colWidth;
                    }
                }
            }

            fraction = (gridWidth - colModel.getTotalWidth() + colWidthCorrection) / width;

            var width = 0;
            while (columns.length) {
                colWidth = columns.pop();
                i = columns.pop();
                width = Math.floor(colWidth + colWidth * fraction);
                colModel.setColumnWidth(i, width, true);
            }

            totalColWidth = colModel.getTotalWidth(false);
            totalColWidth -= colWidthCorrection;

            if (totalColWidth > gridWidth) {
                var adjustCol = (adjCount == visibleColCount) ? extraCol : omitColumn,
                    newWidth = Math.max(1, colModel.getColumnWidth(adjustCol) - (totalColWidth - gridWidth));

                colModel.setColumnWidth(adjustCol, newWidth, true);
            }
            else {
                var adjustCol = (adjCount == visibleColCount) ? extraCol : omitColumn;
                
                /*newWidth = Math.max(1, colModel.getColumnWidth(adjustCol) - (totalColWidth - gridWidth)) + 17;
                //newWidth = Math.max(1, width - (totalColWidth - gridWidth)) + 27;
                console.log(newWidth);
                colModel.setColumnWidth(adjustCol, newWidth, true);*/
            }

            if (preventRefresh !== true) {
                this.updateAllColumnWidths();
            }

            setTimeout(function () {
               // $("#activeAlertsGrid .x-grid3-hd-inner:last").width($("#activeAlertsGrid .x-grid3-hd-inner:last").width() + 14);
                $("#activeAlertsGrid .x-grid3-row").width($("#activeAlertsGrid .x-grid3-row").width() + 22);
                //$("#activeAlertsGrid .x-grid3-row").width(gridWidth + 17);
            }, 10);

            return true;
        }
    });

    Ext.grid.FastCheckboxSelectionModel = Ext.extend(Ext.grid.CheckboxSelectionModel, {
         selectAll: function() {
            if (this.locked) return;
            this.selections.clear();
            for (var i = 0, len = this.grid.store.getCount() ; i < len; i++) {
                if (i == (len - 1)) {
                    this.selectRow(i, true);
                }
                else {
                    this.selectRow(i, true, false, true);
                }
            }
        },

        selectRow: function (index, keepExisting, preventViewNotify, dontFireEvents) {
            if (this.locked || (index < 0 || index >= this.grid.store.getCount()) || this.isSelected(index)) return;

            var r = this.grid.store.getAt(index);
            this.selections.add(r);
            this.last = this.lastActive = index;
            if (!preventViewNotify) {
                this.grid.getView().onRowSelect(index);
            }

            this.fireEvent("rowselect", this, index, r);
            this.fireEvent("selectionchange", this);

            if (!dontFireEvents) {
                this.fireEvent("customRowselect", this, index, r);
                this.fireEvent("customSelectionchange", this);
            }
        },

        clearSelections: function(fast)
        {
            if (this.locked)
                return;

            if (fast !== true) {
                var ds = this.grid.store;
                var s = this.selections;
                var length = s.items.length-1;
               
                s.each(function (r, index) {
                    if (index != length) {
                        this.deselectRow(ds.indexOfId(r.id), false, true);
                    } else {
                        this.deselectRow(ds.indexOfId(r.id));
                    }
                }, this);
                s.clear();
            } else {
                this.selections.clear();
            }

            this.last = false;
        },

        deselectRow: function (index, preventViewNotify, dontFireEvents) {
            if (this.locked) return;
            if (this.last == index) {
                this.last = false;
            }

            if (this.lastActive == index) {
                this.lastActive = false;
            }

            var r = this.grid.store.getAt(index);
            if (r) {
                this.selections.remove(r);
                if (!preventViewNotify) {
                    this.grid.getView().onRowDeselect(index);
                }

                this.fireEvent("rowdeselect", this, index, r);
                this.fireEvent("selectionchange", this);

                if (!dontFireEvents)
                {
                    this.fireEvent("customRowdeselect", this, index, r);
                    this.fireEvent("customSelectionchange", this);
                }
            }
        }
    });

    var AlertType = {
        Advanced: 0,
        Basic: 1
    };

    ORION.prefix = "ActiveAlerts_";
    var allowEventClear = false;
    var allowDisableAllActions = false;

    var selectorModel = null;
    var activeAlertsGridPageSize = 20;
    var activeAlertsGridDataStore = null;
    var groupingDataStore = null;
    var groupingStore = null;
    var groupingValue = null;
    var groupByComboBox = null;
    var groupByTopPanel = null;
    var navPanel = null;
    var groupGrid = null;
    var groupGridSelectedRowsIndexes = [];
    var activeAlertsGrid = null;
    var activeAlertsGridFilters = null;
    var pagingToolbar = null;
    var mainGridPanel = null;
    var sortOrder = null;
    var selectedItemsArray = [];

    var withoutAcknowledgedAlerts = false;
    var defaultJsonGroupingValue = "{\"Name\":\"@{R=Core.Strings;K=WEBCODE_PS0_3; E=js}\",\"Value\":\"SeverityText\", \"Type\":\"System.String\", \"SubGroupValue\":\"[All]\"}";

    var activeAlertDataStoreMapping = [
                                        { name: 'AlertObjectID', mapping: 0 },
                                        { name: 'AlertDefID', mapping: 1 },
                                        { name: 'AlertType', mapping: 2 },
                                        { name: 'AlertName', mapping: 3 },
                                        { name: 'AlertMessage', mapping: 4 },
                                        { name: 'TriggeringObject', mapping: 5 },
                                        { name: 'TriggeringObjectEntityName', mapping: 6 },
                                        { name: 'TriggeringObjectStatus', mapping: 7 },
                                        { name: 'TriggeringObjectDetailsUrl', mapping: 8 },
                                        { name: 'TriggeringObjectEntityUri', mapping: 9 },
                                        { name: 'RelatedNode', mapping: 10 },
                                        { name: 'RelatedNodeDetailsUrl', mapping: 11 },
                                        { name: 'Status', mapping: 12 },
                                        { name: 'ActiveTimeDisplay', mapping: 13 },
                                        { name: 'TriggerTime', mapping: 14 },
                                        { name: 'AcknowledgedBy', mapping: 15 },
                                        { name: 'AcknowledgeTime', mapping: 16 },
                                        { name: 'Notes', mapping: 17 },
                                        { name: 'Severity', mapping: 18 },
                                        { name: 'SeverityText', mapping: 19 },
                                        { name: 'Clear', mapping: 20 },
                                        { name: 'ActiveNetObject', mapping: 21 },
                                        { name: 'ObjectType', mapping: 22 },
                                        { name: 'LegacyAlert', mapping: 23 },
										{ name: 'ObjectTriggeredThisAlertDisplayName', mapping: 24 },
                                        { name: 'Canned', mapping: 25 },
                                        { name: 'Category', mapping: 26 },
                                        { name: 'SiteName', mapping: 27 },
                                        { name: 'SiteID', mapping: 28 },
                                        { name: "IncidentNumber", mapping: 29 },
                                        { name: "IncidentUrl", mapping: 30 },
                                        { name: "AssignedTo", mapping: 31 }

    ];
        
    // PRIVATE METHODS
    var updateToolbarButtons = function () {
        var selectedItemsArray = getSelectedItemsArray();
        var selCount = selectedItemsArray.length;
        var map = activeAlertsGrid.getTopToolbar().items.map;
        var needsToDisable = (selCount === 0) || (!allowEventClear);
        var clearNeedsToDisable = needsToDisable;
        
        var acknowledgeNeedsToDisable = false;
        if (selCount > 0) {
            var selItems = getSelectedItemsArray();
            for (var i = 0; i < selItems.length; i++) {
                if (!needsToDisable) {
                    if (selItems[i].get("LegacyAlert") && (selItems[i].get("AlertType") == AlertType.Basic)) {
                        clearNeedsToDisable = true;
                    }
                }

                if (selItems[i].get("AcknowledgedBy") != "") {
                    acknowledgeNeedsToDisable = true;
                    break;
                }

                if (selItems[i].get("AlertType") == AlertType.Basic) {
                    acknowledgeNeedsToDisable = true;
                }
            }
        }

        map.Acknowledge.setDisabled(selCount == 0 || acknowledgeNeedsToDisable);
        map.ViewAlertDetails.setDisabled(selCount != 1 || (selectedItemsArray.length == 0 || selectedItemsArray[0].get("LegacyAlert")));
        map.EditAlertDefinition.setDisabled(selCount != 1 || (selectedItemsArray.length == 0 || selectedItemsArray[0].get("LegacyAlert")));
        map.ClearTriggeredInstanceOfAlert.setDisabled(clearNeedsToDisable);
     
        var hd = Ext.fly(activeAlertsGrid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (activeAlertsGrid.getStore().getCount() > 0 && activeAlertsGrid.getSelectionModel().getCount() == activeAlertsGrid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        }
        else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function selectRow(grid, rowIndex, webStore) {
        // add row into selected items collection
        var item = grid.store.getAt(rowIndex),
            index = getSelectedItemIndex(item);

        if (index == -1)
            selectedItemsArray.push(item);

        if (item.data.LegacyAlert == true) {
            showOldAlertNotification();
        }
    }

    function updateSelectAllTooltip(grid, webStore) {
        var totalLength = grid.store.totalLength,
            pageLength = grid.store.data.items.length,
            pageSelectionLength = grid.getSelectionModel().selections.items.length,
            selectionLength = selectedItemsArray.length;

        hideSelectAllTooltip();

        //If we don't need to display the tooltip
        if (totalLength == 0 || (pageSelectionLength == selectedItemsArray.length && pageSelectionLength != pageLength) || totalLength <= pageLength)
            return;

        $('<div id="selectionTip" style="padding-left: 5px; background-color:#FFE89E;"></div>').insertAfter(".x-panel-tbar.x-panel-tbar-noheader");

        //All active alerts are selected
        if (selectionLength == totalLength) {
            $("#selectionTip").text(SW.Core.String.Format("@{R=Core.Strings;K=WEBJS_AY0_45;E=js} ", totalLength));
            appendDeselectAllLink(grid, totalLength);
        } else {
            $("#selectionTip").text(SW.Core.String.Format("@{R=Core.Strings;K=WEBJS_AY0_44;E=js} ", selectionLength, totalLength));
            $("#selectionTip").append($("<span id='selectAllLink' style='text-decoration: underline;color:#336699;cursor:pointer;'>" + SW.Core.String.Format("@{R=Core.Strings;K=WEBJS_AY0_43;E=js}", totalLength) + "</span>").click(function() {
                var allGridElementsStore = webStore();
                allGridElementsStore.proxy.conn.jsonData = grid.store.proxy.conn.jsonData;
                allGridElementsStore.load({
                    callback: function () {
                        selectedItemsArray = allGridElementsStore.data.items;
                        updateToolbarButtons();
                        grid.getSelectionModel().selectAll();
                    }
                });
            }));

            $("#selectionTip").append("&nbsp;");

            appendDeselectAllLink(grid, selectionLength);
        }

        var gridHeight = $("#selectionTip").closest('.x-panel-body.x-panel-body-noheader').height();
        var tipHeight = $("#selectionTip").height();
        $("#selectionTip").closest('.x-panel-body.x-panel-body-noheader').height(gridHeight + tipHeight);
        $('.x-layout-split.x-layout-split-west.x-splitbar-h').height(gridHeight + tipHeight);
        $("#activeAlertsGrid .x-panel-body.x-panel-body-noheader").height($("#activeAlertsGrid .x-panel-body.x-panel-body-noheader").height() - tipHeight);
        navPanel.setHeight(navPanel.getSize().height + tipHeight);
        groupGrid.setSize(navPanel.getSize().width, groupGrid.getSize().height + tipHeight);
    }
    
    function appendDeselectAllLink(grid, totalLength) {
        $("#selectionTip").append($("<span id='deselectAllLink' style='text-decoration: underline;color:#336699;cursor:pointer;'>" + SW.Core.String.Format("@{R=Core.Strings;K=WEBJS_ED0_1;E=js}", totalLength) + "</span>").click(function () {
            selectedItemsArray = [];
            grid.getSelectionModel().clearSelections();
        }));
    }

    function getSelectedItemIndex(item) {
        var index = -1;

        if (item && item.data) {
            $.each(selectedItemsArray, function(i) {
                if (this.data &&
					item.data.AlertObjectID == this.data.AlertObjectID &&
                    item.data.ActiveNetObject == this.data.ActiveNetObject &&
					item.data.ObjectType == this.data.ObjectType &&
					item.data.AlertDefID == this.data.AlertDefID) {
                    index = i;
                    return false;
                }

                return true;
            });
        }

        return index;
    }

    function deselectRow(grid, rowIndex, webStore) {

        if (selectedItemsArray.length == 1)
            clearNotification('oldalert');

        //remove row from selected collection
        var item = grid.store.getAt(rowIndex),
            index = getSelectedItemIndex(item);
            
        if (index != -1)
            selectedItemsArray.splice(index, 1);
    }

    function hideSelectAllTooltip() {
        if ($("#selectionTip").length != 0) {
            var tipHeight = $("#selectionTip").height();
            navPanel.setHeight(navPanel.getSize().height - tipHeight);
            var rowsContainerHeight = $("#selectionTip").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectionTip").closest('.x-panel-body.x-panel-body-noheader').height(rowsContainerHeight - tipHeight);
            $('.x-layout-split.x-layout-split-west.x-splitbar-h').height(rowsContainerHeight - tipHeight);
            $("#activeAlertsGrid .x-panel-body.x-panel-body-noheader").height($("#activeAlertsGrid .x-panel-body.x-panel-body-noheader").height() + tipHeight);
            $("#selectionTip").remove();
        }
    }

    //Return all selected active alerts
    function getSelectedItemsArray() {
        return selectedItemsArray;
    }
    
    var getSelectedActiveAlertsIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            if (item.data.AlertType == AlertType.Advanced) {
                ids.push({
                    AlertDefID: item.data.AlertDefID,
                    AlertObjectID: item.data.AlertObjectID,
                    ActiveNetObject: item.data.ActiveNetObject,
                    ObjectType: item.data.ObjectType,
                    SiteID: item.data.SiteID
                });
            }
        });

        return ids;
    };

    // Escapes braces (which can be interpreted by extjs as html tags and break showing of tooltip) with whitespaces, eg. '<test>' -> '< test >'
    function escapeWithWhiteSpaces(str) {
        return str.replace(new RegExp("&lt;", 'g'), "&lt; ").replace(new RegExp("&gt;", 'g'), " &gt;");
    }

      // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function renderIncidentNumber(value, meta, record) {
        return '<a href="' + encodeHTML(record.data.IncidentUrl) + '" target="_blank" rel="noopener noreferrer">' + encodeHTML(value) + '</a>';
    }

    function renderString(value, meta, record) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    };

    function renderAlertMessage(value, meta, record) {
        return SW.Core.String.Format("<span ext:qtitle=\"{0}\" ext:qtip=\" \">{1}</span>", removeHtmlTags(value), renderStringOverSeparatedHtmlElements(value));
    }

    // Convert input to jQuery object and calls renderString separately for every text element in HTML nodes tree
    function renderStringOverSeparatedHtmlElements(input) {
        if (!input) {
            return input;
        }

        try {
            var jQueryInputObject = $('<div>' + input + '</div>');
            var maxRecursionDepth = 100;
            renderStringForHtmlElement(jQueryInputObject[0], 1, maxRecursionDepth);
            return Ext.util.Format.htmlDecode(jQueryInputObject[0].innerHTML);
        } catch (err) {
            return input;
        }
    }

    function renderStringForHtmlElement(htmlElement, rsCurrentRecursionDepth, rsMaxRecursionDepth) {
        if (rsCurrentRecursionDepth > rsMaxRecursionDepth) {
            return;
        }

        var childNodes = htmlElement.childNodes;
        for (var i = 0; i < childNodes.length; i++) {
            var childNode = childNodes[i];  
            // replace text node text with highlighted version
            if (childNode.nodeType === Node.TEXT_NODE) {
                childNode.data = renderString(childNode.data);
            }
            // recursive call for nested elements (script elements excluded)
            else if (childNode.nodeType === Node.ELEMENT_NODE && childNode.tagName.toLowerCase() !== 'script') {
                renderStringForHtmlElement(childNode, ++rsCurrentRecursionDepth, rsMaxRecursionDepth);
            }
        }
    }

    function removeHtmlLinkTags(input) {
        return input.replace(/<\s*a\b[^>]*>/i, "").replace(/<\s*\/\s*a\s*>/i, "");
    }

    function removeHtmlTags(input) {
        var inputObj = $('<div>' + input + '</div>');
        return inputObj.text();
    }

     function renderTriggerTime(value, meta, record) {
        return SW.Core.String.Format("<span ext:qtitle=\"{0}\" ext:qtip=\" \">{1}</span>", value, renderString(value));
    }

    function renderAcknowledgeTime(value, meta, record) {
        var acknowledgedBy = record.get("AcknowledgedBy");
        if (acknowledgedBy == '' || acknowledgedBy == null)
            return '@{R=Core.Strings;K=WEBJS_PS0_59; E=js}';
  
        return SW.Core.String.Format("<span ext:qtitle=\"{0}\" ext:qtip=\" \">{1}</span>", value, renderString(value));
    }
     
    function renderActiveTimeDisplay(value, meta, record) {
        return SW.Core.String.Format("<span ext:qtitle=\"{0}\" ext:qtip=\" \">{1}</span>", value, renderString(value));
    }
    
    function federationUrlHelper(siteId, isImage) {
        if (!IsFederationEnabled)
            return "";

        var prefix = SW.Core.String.Format("/Server/{0}", siteId);
        if (isImage)
            prefix += "/ImageHandler";

        return prefix;
    }


    function getFederationLinkTarget() {
        if (IsFederationEnabled)
            return "target = \"_blank\"";

        return "";
    }

    function renderAlertName(value, meta, record) {
        var legacyAlert = record.get("LegacyAlert");
        var msg = "";
        var escapedToolTip = escapeWithWhiteSpaces(encodeHTML(value));
        if (!legacyAlert)
        {
            msg = SW.Core.String.Format("<a ext:qtitle=\" {0}\" ext:qtip=\" \" class=\"NoTip alert-link ui-helper-clearfix\" href=\"{2}/Orion/View.aspx?NetObject=AAT:{1}\" {3} ><span class=\"alert-icon\">", escapedToolTip, record.get("AlertObjectID"),
                federationUrlHelper(record.get("SiteID"), false), getFederationLinkTarget());

            if (record.data.Severity == "0")
            {
                msg += "<img src='/Orion/images/ActiveAlerts/InformationalAlert.png' /> ";
            }
            else if (record.data.Severity == "1")
            {
                msg += "<img src='/Orion/images/ActiveAlerts/Warning.png' /> ";
            }
            else if (record.data.Severity == "2")
            {
                msg += "<img src='/Orion/images/ActiveAlerts/Critical.png' /> ";
            }
             else if (record.data.Severity == "3")
            {
                msg += "<img src='/Orion/images/ActiveAlerts/Serious.png' /> ";
            }
             else if (record.data.Severity == "4")
            {
                msg += "<img src='/Orion/images/ActiveAlerts/Notice.png' /> ";
            }
            msg = msg + "</span><span class=\"alert-name\">" + renderString(value) + "</span></a>";
        }
        else {
            msg = "<span class=\"alert-link ui-helper-clearfix\"><span class=\"alert-icon\">";

            if (record.data.Severity == "0") {
                msg += "<img src='/Orion/images/ActiveAlerts/InformationalAlert.png' /> ";
            }
            else if (record.data.Severity == "1") {
                msg += "<img src='/Orion/images/ActiveAlerts/Warning.png' /> ";
            } else if (record.data.Severity == "2") {
                msg += "<img src='/Orion/images/ActiveAlerts/Critical.png' /> ";
            }
            else if (record.data.Severity == "3"){
                msg += "<img src='/Orion/images/ActiveAlerts/Serious.png' /> ";
            }
            else if (record.data.Severity == "4") {
                msg += "<img src='/Orion/images/ActiveAlerts/Notice.png' /> ";
            }
            msg = msg + "</span><span class=\"alert-name\">" + renderString(value) + "</span></span>";
        }

        return msg;
    }

    function renderObjectTriggeredThisAlert(value, meta, record) {
        var isEmpty = function(str) {
            return (!str || /^\s*$/.test(str));
        };

        var urlPrefix = federationUrlHelper(record.get("SiteID"), true);

        var entityName = (record.data.TriggeringObjectEntityName == "Orion.Container") ? "Orion.Groups" : record.data.TriggeringObjectEntityName;

        var triggeringObjectHtml="";

        if (record.data.TriggeringObjectEntityName == "Orion.SysLog" || record.data.TriggeringObjectEntityName == "Orion.Traps") {
            var replacePattern = record.data.TriggeringObjectEntityName == "Orion.SysLog" ? (/MessageID=[0-9]+,/gi) : (/TrapID=[0-9]+,/gi);
            // getting Node Uri from message uri, like /Orion.Syslog/MessageID=115,NodeID=1 > /Orion.Nodes/NodeID=1.
            var triggerObject = record.data.TriggeringObjectEntityUri.replace(record.data.ObjectType, "Orion.Nodes").replace(replacePattern,""); 
            var objectStatusUri = SW.Core.String.Format("{2}/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;EntityUri='{0}'&amp;size=small&amp;timestamp={1}", encodeURI(triggerObject), $.now(), urlPrefix);
            var objectRef = SW.Core.String.Format("<span ext:qtitle=\"{1}\" ext:qtip=\" \"><a href='{0}' {2}>{1}</a></span>", record.data.RelatedNodeDetailsUrl, renderString(record.data.RelatedNode), getFederationLinkTarget());
            triggeringObjectHtml = SW.Core.String.Format('<img src="{0}"/>{1}',
                objectStatusUri, objectRef);

                return triggeringObjectHtml;
        }

        triggeringObjectHtml = SW.Core.String.Format('<img src="{0}"/><span>{1}</span>',
            SW.Core.String.Format("{5}/Orion/StatusIcon.ashx?entity={0}&amp;EntityUri='{1}'&amp;KeyPropertyValue={2}&amp;status={3}&amp;size=small&amp;timestamp={4}", entityName, encodeURI(record.data.TriggeringObjectEntityUri), record.data.ActiveNetObject, record.data.TriggeringObjectStatus, $.now(), urlPrefix),
            renderString(record.data.TriggeringObject));

        if (record.data.TriggeringObjectDetailsUrl != "") {
            triggeringObjectHtml = SW.Core.String.Format("<span><a href='{0}' {2}>{1}</a></span><span>", record.data.TriggeringObjectDetailsUrl, triggeringObjectHtml, getFederationLinkTarget());
        } else {
            triggeringObjectHtml = SW.Core.String.Format('<span><img src="{0}"/><span ext:qtitle=\"{1}\" ext:qtip=\" \">{1}</span></span><span>',
                SW.Core.String.Format("{5}/Orion/StatusIcon.ashx?entity={0}&amp;EntityUri='{1}'&amp;KeyPropertyValue={2}&amp;status={3}&amp;size=small&amp;timestamp={4}", entityName, encodeURI(record.data.TriggeringObjectEntityUri), record.data.ActiveNetObject, record.data.TriggeringObjectStatus, $.now(), urlPrefix),
                Ext.util.Format.htmlEncode(record.data.TriggeringObject));
            triggeringObjectHtml = SW.Core.String.Format("<span style='position: relative; top: -2px; padding-left: 4px;'>{0}</span>",triggeringObjectHtml);
        }

        if (record.data.RelatedNode.toUpperCase() != record.data.TriggeringObject.toUpperCase()) {
            if (isEmpty(record.data.RelatedNode)) {
                return triggeringObjectHtml;
            }

            triggeringObjectHtml = triggeringObjectHtml + "@{R=Core.Strings; K=TriggeringObjectOnNode; E=js}" + SW.Core.String.Format("</span><span><span ext:qtitle=\"{2}\" ext:qtip=\" \"><a class='clearfix' href='{0}' {3}>{1}</a></span></span>", record.data.RelatedNodeDetailsUrl, renderString(record.data.RelatedNode), Ext.util.Format.htmlEncode(record.data.RelatedNode), getFederationLinkTarget());
        }

        triggeringObjectHtml = SW.Core.String.Format("<div class='alert-trigger'>{0}</div>", triggeringObjectHtml);

        return triggeringObjectHtml;
    }

    function renderAcknowledgedBy(value, meta, record) {
        if (value == '' || value == null) {
            var alertType = record.get("AlertType");
            if (alertType == AlertType.Advanced && allowEventClear) {
                var buttonHtml = "<span class=\" sw-btn  \" style=\"text-transform: none;\" id=\"swgenid-5\" automation=\"Acknowledge\" onclick=\"{0}\" href=\"javascript:void(0);\"><span class=\"sw-btn-c\"><span class=\"sw-btn-t\" style=\"text-transform: none;\">@{R=Core.Strings;K=WEBJS_PS0_38; E=js}</span></span></span>";
                var alertDefId = record.get("AlertDefID");
                var alertObjectId = record.get("AlertObjectID");
                var activeNetObject = record.get("ActiveNetObject");
                var objectType = record.get("ObjectType");
                var siteId = record.get("SiteID");
                var jsCode = "SW.Orion.ActiveAlerts.acknowledgeActiveAlert('{0}', '{1}', '{2}', '{3}', '{4}')";
                jsCode = SW.Core.String.Format(jsCode, alertDefId, alertObjectId, activeNetObject, objectType, siteId);
                buttonHtml = SW.Core.String.Format(buttonHtml, jsCode);
                return buttonHtml;
            } else {
                return "";
            }

        } else { // alert was already acknowledged
            var htmlCode = SW.Core.String.Format("<span ext:qtitle=\"{0}\" ext:qtip=\" \"><img src='/Orion/images/ActiveAlerts/Acknowliedged_icon16x16v1.png'> {1}</span>", value, renderString(value));
            return htmlCode;
        }
    }

    function refreshGroupGridObjects(elem, property, type, value, search, callback) {
        elem.store.removeAll();
        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: search, WithoutAcknowledgedAlerts: withoutAcknowledgedAlerts, FederationEnabled: IsFederationEnabled };
        currentSearchTerm = search;
        elem.store.load({ callback: callback });
    };

    function refreshActiveAlertsGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();

        // Following 3 rows of code solve searching functionality, when search is canceled
        filterPropertyParam = property;
        filterTypeParam = type;
        filterValueParam = value;

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value != null ? value : "", search: search,  WithoutAcknowledgedAlerts: withoutAcknowledgedAlerts, FederationEnabled: IsFederationEnabled };
        elem.store.load({ params: { start: start, limit: limit }, callback: callback });
    };

    function createDataStores() {
        // CREATE DATA STORES
        activeAlertsGridDataStore = new ORION.WebApiStore(
                            "/api/ActiveAlertsGrid/GetAlertTable",
                            activeAlertDataStoreMapping,
                                "Severity"
                            );
        activeAlertsGridDataStore.sortInfo = { field: sortOrder.SortColumn, direction: sortOrder.SortDir };

        activeAlertsGridDataStore.on("beforeload", function (s, params) {
            reconfigureFilterNotifications();
        });
        if (IsFederationEnabled) {
            activeAlertsGridDataStore.on("load", function (store, records, successful) {
                var content = store.reader.jsonData.Metadata;
               if (content.length != 0) {
					var errorMessage = $('<textarea />').html(content).text();
                    $('#SwisFErrorControlPlaceHolder').css('display', 'block');
                    $('#SwisFErrorControlDescription').html(errorMessage);
                }
            });
        }
        groupingDataStore = new ORION.WebApiStore(
                            "/api/ActiveAlertsGrid/GetAlertGroupingValues",
                            [
                                { name: 'Name', mapping: 1 },
                                { name: 'Value', mapping: 0 },
                                { name: 'Type', mapping: 2 }
                            ]);

        groupingStore = new ORION.WebApiStore(
                            "/api/ActiveAlertsGrid/GetAlertGroupValues",
                            [
                                { name: 'Value', mapping: 0 },
                                { name: 'Cnt', mapping: 1 },
                                { name: 'Id', mapping: 2 }
                            ]);
    }

    function getAllSelectedAlerts() {
         var allSelectedAlerts = new ORION.WebApiStore(
                        "/api/ActiveAlertsGrid/GetAlertTable",
                        activeAlertDataStoreMapping,
                        "Severity"
                    );
        allSelectedAlerts.on("beforeload", function(s,  requestParam) {
            //Set limit as totalLength of activeAlertsGrid records 
            //Need for select all alerts in grid not only on current page
            requestParam.params.limit = activeAlertsGrid.store.totalLength;
            reconfigureFilterNotifications();
        });
        return allSelectedAlerts;
    }

    // METHODS FOR LEFT NAVIGATION PANEL
    function createGroupByTopPanel() {
        groupByTopPanel = new Ext.Panel({
            region: 'centre',
            split: false,
            heigth: 50,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [
                groupByComboBox
            ],
            cls: 'panel-no-border panel-bg-gradient'
        });
    }
    function createGroupByComboBox() {
        groupByComboBox = new Ext.form.ComboBox({
            fieldLabel: 'Name',
            hiddenName: 'Value', store: groupingDataStore,
            displayField: 'Name', triggerAction: 'all',
            value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
            typeAhead: true, mode: 'local',
            forceSelection: true, selectOnFocus: false
        });

        groupByComboBox.on('select', function () {
            var val = this.store.data.items[this.selectedIndex].data.Value;
            refreshGroupGridObjects(groupGrid, val, "", "", "");
            var newGroupingValue = getGroupingValue(val);
            newGroupingValue.SubGroupValue = "[All]";
            var modifiedFilterText = getModifiedFilterText(filterText);
            ORION.Prefs.save('GroupingValue', JSON.stringify(newGroupingValue));
            refreshActiveAlertsGridObjects(0, activeAlertsGridPageSize, activeAlertsGrid, "", "", "", modifiedFilterText, function () {
                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                    this.tooltip = 'processed';
                    $.swtooltip(this);
                });
            });
        });
    }

    function renderGroup(value, meta, record) {
        return Ext.util.Format.htmlEncode(value) + " (" + record.data.Cnt + ")";
    }

    function getGroupingValue(val) {
        var groupName = groupByComboBox.selectedIndex != -1 ? groupByComboBox.store.data.items[groupByComboBox.selectedIndex].data.Name : groupingValue.Name;
        var groupValue = groupByComboBox.selectedIndex != -1 ? groupByComboBox.store.data.items[groupByComboBox.selectedIndex].data.Value : groupingValue.Value;
        var groupType = groupByComboBox.selectedIndex != -1 ? groupByComboBox.store.data.items[groupByComboBox.selectedIndex].data.Type : groupingValue.Type;
        return {Name:groupName, Value:groupValue, Type:groupType, SubGroupValue:val};
    }

    // Group grid which is displayed bellow Group By Combo box
    function createGroupGrid() {
        groupGrid = new Ext.grid.GridPanel({
            id: 'groupingGrid',
            store: groupingStore,
            cls: 'hide-header',
            columns: [
                {
                    width: 193,
                    editable: false,
                    sortable: false,
                    dataIndex: 'Value',
                    renderer: renderGroup
                }
            ],
            selModel: new Ext.grid.RowSelectionModel(),
            layout: 'fit',
            autoScroll: 'true',
            loadMask: true,
            listeners: {
                cellclick: function (mygrid, row, cell, e) {
                    selectedItemsArray = []; // we want to clear selected items

                    var val = mygrid.store.data.items[row].data.Id;
                    a = reMsAjax.exec(val);
                    if (a) {
                        var b = a[1].split(/[-,.]/);
                        var dateVal = new Date(+b[0]);
                        val = dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.SortableDateTimePattern);
                    }

                    var newGroupingValue = getGroupingValue(val);
                    ORION.Prefs.save('GroupingValue', JSON.stringify(newGroupingValue));
                    groupingValue = newGroupingValue;
                    
					var tmpfilterText = getModifiedFilterText(filterText);	
                    refreshActiveAlertsGridObjects(0, activeAlertsGridPageSize, activeAlertsGrid, newGroupingValue.Value, newGroupingValue.Type, newGroupingValue.SubGroupValue, tmpfilterText, function () {
                        $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                            this.tooltip = 'processed';
                            $.swtooltip(this);
                        });
                    });
                }
            },
            anchor: '0 0',
            viewConfig: { forceFit: true },
            split: true,
            autoExpandColumn: 'Value'
        });

        groupGrid.store.on('load', function (records, operation, success) {
            var selectionModel = groupGrid.getSelectionModel();
            for (var i = 0; i < groupGridSelectedRowsIndexes.length; i++) {
                selectionModel.selectRow(groupGridSelectedRowsIndexes[i]);
            }

            groupGridSelectedRowsIndexes = []; // we want to clear array
            selectedItemsArray = []; // we want to clear selected items
            updateToolbarButtons();
        });
    }
    function createNavigationPanel() {
        navPanel = new Ext.Panel({
            region: 'west',
            split: true, anchor: '0 0',
            width: 200,
            heigth: 500,
            collapsible: true,
            title: "@{R=Core.Strings; K=WEBJS_PS0_37; E=js}",
            viewConfig: {
                forceFit: true
            },
            items: [
                groupByTopPanel,
                groupGrid
            ],
            cls: 'panel-no-border'
        });

        navPanel.on("bodyresize", function () {
            groupGrid.setSize(navPanel.getSize().width, navPanel.getSize().height - 60);
        });
    }

    // METHODS FOR FILTERING FUNCTIONALITY
    function getGridColumHeader(name, displayName) {
        return SW.Core.String.Format('<img class="filter-icon" onclick="ShowFilter(event, \'{1}\');" src="/Orion/js/extjs/resources/images/default/s.gif" alt="" style="vertical-align: middle; padding-top:1px; height: 16px; width:16px"/><span style="vertical-align: middle;" title="{0}">{0}</span>&nbsp;', (displayName == '') ? name : displayName, name);
    }

    function formatDateValue(value) {
        if (Ext.isEmpty(value))
            return '';
        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
        }

        var b = new Date(value);
        if (b) {
            return b.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
        }
        return '';
    }

    function getActiveAlertsGridFilters() {
        if (activeAlertsGridFilters == null) {
            activeAlertsGridFilters = new Ext.ux.grid.GridFilters({
                menuFilterText: '@{R=Core.Strings;K=WEBJS_SO0_40; E=js}',
                encode: false,
                local: false
            });
        }

        return activeAlertsGridFilters;
    }

    function getFilterNotificationMessage(filter) {
        //var displayName = columnCaptionByColumnName[filter.field] || filter.field;
        var displayName = filter.field;

        if (Ext.isEmpty(filter.data.value)) {
            return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_RB0_2; E=js}', displayName);
        }
        if (filter.data.type == 'numeric') {
            var floatVal = String(filter.data.value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
            if (filter.data.comparison == 'lt')
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_16; E=js}', displayName, floatVal);
            if (filter.data.comparison == 'gt')
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_15; E=js}', displayName, floatVal);
            return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_17; E=js}', displayName, floatVal);
        }
        if (filter.data.type == 'date') {
            var dateVal = formatDateValue(filter.data.value, false);
            if (filter.data.comparison == 'lt')
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_18; E=js}', displayName, dateVal);
            if (filter.data.comparison == 'gt')
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_19; E=js}', displayName, dateVal);
            return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_20; E=js}', displayName, dateVal);
        }
        if (filter.data.type == 'boolean') {
            return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, (filter.data.value ? '@{R=Core.Strings;K=Boolean_true; E=js}' : '@{R=Core.Strings;K=Boolean_false; E=js}'));
        }
        if (filter.data.type.toString().startsWith('list')) {
            return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext.util.Format.htmlEncode(filter.data.value.split('$#').join("; ")));
        }

        return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext.util.Format.htmlEncode(filter.data.value));
    }

    function hasUserRightsToClearEvents() {
        return (ORION.Prefs.load('AllowEventClear', 'false')).toLowerCase() == 'true';
    }

    function hasUserRightsToEditAlerts() { 
        return (ORION.Prefs.load('AllowAlertManagement', 'false')).toLowerCase() == 'true'; 
    } 

    function hasUserRightToDisableAllActions() {
        return (ORION.Prefs.load('AllowDisableAllActions', 'false')).toLowerCase() == 'true';
    }

    function showOldAlertNotification() {
        showNotification("oldalert","@{R=Core.Strings.2;K=WEBJS_TK0_13;E=js} " + "<a href='"+SW.Core.KnowledgebaseHelper.getKBUrlWithLang(66666666)+"' target='_blank'>@{R=Core.Strings.2;K=WEBJS_TK0_14;E=js}</a>");
    }

    function showNotification(id,message) {
        var notificationTemplate = '\
        <div id="Hint" style="position: static;" runat="server"> \
            <div id="{0}" class="sw-notification">{1}</div> \
        </div>';

        var msg = SW.Core.String.Format(notificationTemplate, id, message);

        var $notificationPanel = $("#notifications");
        $notificationPanel.html(msg);
    }

    function clearNotification(identificator) {
        if (identificator) {
            var ntf = $("#notifications #" + identificator);
            if (ntf.length > 0) {
                $("#notifications").html('');
            }
        }
        else
            $("#notifications").html('');
    }

    function reconfigureFilterNotifications() {
        var grid = getActiveAlertsGrid();
        var filterData = grid.filters.getFilterData();
        var notificationTemplate = '\
        <div id="Hint" style="position: static;" runat="server"> \
            <div class="sw-filter"> \
                <table cellpadding="0" cellspacing="0"> \
                    <tr> \
                        <td><span class="sw-filter-icon sw-filter-icon-hint"></span></td> \
                        <td><span style="vertical-align: middle; padding-right: 10px">{0}</span></td> \
                        <td><span><a href="javascript:SW.Orion.ActiveAlerts.removeFilter(\'{1}\',\'{2}\');" ><img id="deleteButton" src="/Orion/Nodes/images/icons/icon_delete.gif" style="cursor: pointer;" /></a></td> \
                    </tr> \
                </table> \
            </div> \
        </div>';

        var pauseActionsOfAllAlertsNotifications = '\
            <div id="Hint"> \
                <div class="sw-filter"> \
                    <table cellpadding="0" cellspacing="0"> \
                        <tr> \
                            <td><span class="sw-warn-icon sw-filter-icon-hint"></span></td> \
                            <td><span style="vertical-align: middle; padding-right: 10px;font-weight: bold;">@{R=Core.Strings;K=WEBJS_PS0_64;E=js}</span></td> \
                            <td style="display: {0}"><span style="vertical-align: middle;"><a href="javascript:SW.Orion.ActiveAlerts.pauseActionsOfAllAlerts(false);" style="color: #336699;">@{R=Core.Strings;K=WEBJS_PS0_65;E=js}</a></span> \
                        </tr> \
                    </table> \
                </div> \
            </div>';

        var autoRefreshPaused = '\
            <div id="Hint"> \
                <div class="sw-filter"> \
                    <table cellpadding="0" cellspacing="0"> \
                        <tr> \
                            <td><span class="sw-warn-icon sw-filter-icon-hint"></span></td> \
                            <td><span style="vertical-align: middle; padding-right: 10px;font-weight: bold;">@{R=Core.Strings;K=WEBJS_BV0_RES_001;E=js}</span></td> \
                            <td style="display: table-cell;"><span style="vertical-align: middle;"><a href="javascript:SW.Orion.ActiveAlerts.enableAutoRefresh();" style="color: #336699;">@{R=Core.Strings;K=WEBJS_BV0_RES_002;E=js}</a></span> \
                        </tr> \
                    </table> \
                </div> \
            </div>';

        $("#filterNotifications").each(function () {
            $(this).empty();

            var filtersConfigToSave = [];
            for (var i = 0; i < filterData.length; i++) {
                var comparison = (filterData[i].data.comparison == undefined) ? "" : filterData[i].data.comparison;
                var notification = $(SW.Core.String.Format(notificationTemplate, getFilterNotificationMessage(filterData[i]), filterData[i].field, comparison));
                $(this).append(notification);

                filtersConfigToSave.push({ColumnName : filterData[i].field, Value:filterData[i].data.value});
            }

            ORION.Prefs.save('LastUsedFilters', JSON.stringify(filtersConfigToSave));

            var autoRefreshEnabled = ORION.Prefs.load('AutoRefreshEnabled', 'true').toLowerCase() == "true";
            if (!autoRefreshEnabled) {
            	$(this).append($(autoRefreshPaused));
            }

            var pauseActionsOfAllAlerts = parseInt($("#ActiveAlerts_PauseActionsOfAllAlerts").val());
            if (pauseActionsOfAllAlerts == 1) {
            	$(this).append($(SW.Core.String.Format(pauseActionsOfAllAlertsNotifications, hasUserRightToDisableAllActions() ? "table-cell" : "none")));
            }
        });
    }

    ShowFilter = function (evt, name) {
        evt = (evt) ? evt : window.event;
        evt.cancelBubble = true;
        var grid = getActiveAlertsGrid();
        for (var i = 0; i < grid.filters.filters.items.length; i++) {
            if (grid.filters.filters.items[i].dataIndex == name) {
                var menu = grid.filters.filters.items[i].menu;
                var event = Ext.EventObject;
                menu.showAt(event.getXY());
            }
        }
        return false;
    };

     function getBoolFilterControl() {
        return {
            type: 'boolean',
            yesText: '@{R=Core.Strings;K=WEBJS_SO0_44; E=js}',
            noText: '@{R=Core.Strings;K=WEBJS_SO0_45; E=js}'
        };
    };
    
    function getFloatFilterControl(restrictedValues) {
        if (restrictedValues.length > 0) {
            var stringArray = new Array();

            for (var i = 0; i < restrictedValues.length; i++) {
                var strVal = String(restrictedValues[i]).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                stringArray.push(strVal);
            }
            return {
                type: 'list',
                options: stringArray,
                getSerialArgs: function () {
                    var args = { type: 'list$system.single', value: this.getValue().join('$#') };
                    return args;
                }
            };
        }
        else {
            return {
                type: 'numeric',
                menuItemCfgs: {
                    emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                    decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                    allowDecimals: true,
                    allowNegative: true,
                    maxValue: 3.40282347E+38,
                    minValue: -3.40282347E+38,
                    decimalPrecision: 7
                }
            };
        }
    };

    function getIntegerFilterControl(restrictedValues) {
        if (restrictedValues.length > 0) {
            var stringArray = new Array();
            for (var y = 0; y < restrictedValues.length; y++) {
                stringArray.push(restrictedValues[y].toString());
            }
            return {
                type: 'list',
                options: stringArray,
                getSerialArgs: function () {
                    var args = { type: 'list$system.int32', value: this.getValue().join('$#') };
                    return args;
                }
            };
        }
        else {
            return {
                type: 'numeric',
                menuItemCfgs: {
                    emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                    allowDecimals: false,
                    allowNegative: true,
                    maxValue: 2147483647,
                    minValue: -2147483648
                }
            };
        }
    }

    function getStringFilterControl(restrictedValues, size) {
        if (restrictedValues.length > 0) {
            var stringArray = new Array();
            for (var i = 0; i < restrictedValues.length; i++) {
                stringArray.push(Ext.util.Format.htmlEncode(restrictedValues[i].toString()));
            }
            return {
                type: 'list',
                options: stringArray,
                getSerialArgs: function () {
                    var args = { type: 'list$system.string', value: Ext.util.Format.htmlDecode(this.getValue().join('$#')) };
                    return args;
                }
            };
        }
        else {
            return {
                type: 'string',
                emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                maxLength: (size > 0) ? size : Number.MAX_VALUE
            };
        }
    }

    function getDateTimeFilterControl() {
        return {
            type: 'dateTime',
            beforeText:'@{R=Core.Strings;K=WEBJS_SO0_41; E=js}',
            afterText: '@{R=Core.Strings;K=WEBJS_SO0_42; E=js}',
            onText: '@{R=Core.Strings;K=WEBJS_SO0_43; E=js}',
            menuItems: ['before', 'after']
        };
    }

    // METHODS FOR MAIN ALERT GRID
    function getActiveAlertsGridSelectorModel() {
        if (selectorModel == null) {
            selectorModel = new Ext.grid.FastCheckboxSelectionModel();
            selectorModel.on("customSelectionchange", updateToolbarButtons);

            selectorModel.on("rowselect", function (sm, rowIndex) {
                selectRow(activeAlertsGrid, rowIndex, function () {
                    return getAllSelectedAlerts();
                });
            });

            selectorModel.on("customRowselect", function (sm, rowIndex) {
                updateSelectAllTooltip(activeAlertsGrid, function () {
                    return getAllSelectedAlerts();
                });
            });

            selectorModel.on("rowdeselect", function (sm, rowIndex) {
                deselectRow(activeAlertsGrid, rowIndex, function () {
                    return getAllSelectedAlerts();
                });
            });

            selectorModel.on("customRowdeselect", function (sm, rowIndex) {
                updateSelectAllTooltip(activeAlertsGrid, function () {
                    return getAllSelectedAlerts();
                });
            });
        }

        return selectorModel;
    };
       
    function getActiveAlertsGridToolbarConfig() {
        var isAckEnabled = hasUserRightsToClearEvents();
        var isEditAlertEnabled = hasUserRightsToEditAlerts(); 

        return [
                {
                    id: 'Acknowledge',
                    text: '@{R=Core.Strings; K=WEBJS_PS0_38; E=js}',
                    icon: '/Orion/images/ActiveAlerts/Acknowledge_icon16x16.png',
                    cls: 'x-btn-text-icon',
                    hidden: !isAckEnabled,
                    handler: function () {
                        var selectedAlerts = getSelectedItemsArray().slice(0);
                        var ids = getSelectedActiveAlertsIds(selectedAlerts);

                        var onSuccess = function(result) {
                            if (result.Acknowledged) {
                                for (var i = 0; i < ids.length; i++) {
                                    var record = getRecordFromMainGrid(ids[i].AlertObjectID, ids[i].ActiveNetObject, ids[i].ObjectType, ids[i].SiteID);
                                    if (record) {
                                        record.set("AcknowledgedBy", result.UserNamePerSite[ids[i].SiteID]);
                                        record.set("AcknowledgeTime", result.AcknowledgeDateTimeInDisplayFormat);
                                        record.set("Notes", result.Notes);
                                        record.commit();
                                    }
                                }

                                selectedItemsArray = [];
                                activeAlertsGrid.getSelectionModel().clearSelections();
                                activeAlertsGrid.getView().refresh();
                            }
                        };
                        var onFail = function(error) {};
                        SW.Orion.Alerts.AcknowledgeAlertDialog.AcknowledgeAlerts(ids, onSuccess, onFail);
                    }
                },
                {
                    id: 'ViewAlertDetails',
                    text: '@{R=Core.Strings; K=WEBJS_PS0_39; E=js}',
                    icon: '/Orion/images/ActiveAlerts/View_Alerts_details_icons16x16.png',
                    cls: 'x-btn-text-icon',
                    handler: function() {
                        var record = activeAlertsGrid.getSelectionModel().getSelected().data;
                        location.href = SW.Core.String.Format('{1}/Orion/View.aspx?NetObject=AAT:{0}', record.AlertObjectID, federationUrlHelper(record.SiteID), false);
                    }
                },
                {
                    id: 'EditAlertDefinition', 
                    text: '@{R=Core.Strings; K=WEBJS_PS0_40; E=js}',
                    icon: '/Orion/images/edit_16x16.gif',
                    cls: 'x-btn-text-icon',
                    hidden: !isEditAlertEnabled,
                    handler: function () {
                        var record = activeAlertsGrid.getSelectionModel().getSelected().data;
                        SW.Core.Alerting.Management.EditAlertDefinition.EditRemote(record.AlertDefID, record.SiteID);
                    }
                },
                {
                    id: 'ClearTriggeredInstanceOfAlert',
                    text: '@{R=Core.Strings; K=WEBJS_PS0_41; E=js}',
                    icon: '/Orion/images/ActiveAlerts/delete_icon16x16.png',
                    cls: 'x-btn-text-icon',
                    hidden: !isAckEnabled,
                    handler: function() {
                        var deleteFn = function () {
                            var ids = getSelectedActiveAlertsIds(getSelectedItemsArray());
                            var onSucc = function(result) {
                                if (result) {
                                    selectedItemsArray = [];
                                    if ((pagingToolbar.store.totalLength - pagingToolbar.cursor - ids.length) > 0) {
                                        pagingToolbar.doRefresh();
                                    } else if (ids.length >= pagingToolbar.store.totalLength) {
                                        pagingToolbar.moveFirst();
                                    } else if (pagingToolbar.store.data.items.length <= ids.length) {
                                        pagingToolbar.movePrevious();
                                    } else {
                                        pagingToolbar.doRefresh();
                                    }

                                    var selectionModel = groupGrid.getSelectionModel();
                                    for (var i = 0; i < groupGrid.store.data.keys.length; i++) {
                                        if (selectionModel.isIdSelected(groupGrid.store.data.keys[i])) {
                                            groupGridSelectedRowsIndexes.push(i);
                                        }
                                    }

                                    groupingStore.reload();
                                }
                            };
                            var onFail = function(result) {};
                            SW.Orion.Alerts.ClearTriggeredAlertsControl.ClearAlerts(ids, onSucc, onFail);
                        };

                        Ext.Msg.confirm(
                            "@{R=Core.Strings; K=WEBJS_PS0_42; E=js}",
                            "@{R=Core.Strings; K=WEBJS_PS0_43; E=js}",
                            function (btn, text) {
                                if (btn == "yes") {
                                    deleteFn();
                                }
                            });
                    }
                },
                '->',
                new Ext.ux.form.SearchField({
                    store: activeAlertsGridDataStore,
                    width: 200,
                    emptyText: '@{R=Core.Strings; K=WEBJS_PS0_44; E=js}',
                    listeners: {
                    	searchStarted: function () { selectedItemsArray = []; },
                    	searchStarting: function (storeJsonData) { storeJsonData["FederationEnabled"] = IsFederationEnabled; }
                    }
                })
        ];
    }

    var colCount = 0;

    function getActiveAlertsGridColumnsModelConfig() {
        var columnsModelConfig = [getActiveAlertsGridSelectorModel(),
                {
                    header: 'AlertObjectID', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'AlertObjectID'
                },
                {
                    header: getGridColumHeader('AlertName', '@{R=Core.Strings; K=WEBJS_PS0_45; E=js}'),
                    width: 250, sortable: true, hideable: false, dataIndex: 'AlertName', filterable: true, filter: getStringFilterControl([]), renderer: renderAlertName
                },
                {
                    header: getGridColumHeader('AlertMessage', '@{R=Core.Strings; K=WEBJS_PS0_46; E=js}'),
                    width: 250, sortable: true, filterable: true, filter: getStringFilterControl([]), dataIndex: 'AlertMessage', renderer: renderAlertMessage
                },
                {
                    header: getGridColumHeader('ObjectTriggeredThisAlertDisplayName', '@{R=Core.Strings; K=WEBJS_PS0_66; E=js}'),
                    width: 250, sortable: true, filtered: true,  filter: getStringFilterControl([]), dataIndex: 'ObjectTriggeredThisAlertDisplayName', renderer: renderObjectTriggeredThisAlert
                },
                {
                    header: getGridColumHeader('ActiveTimeDisplay', '@{R=Core.Strings; K=WEBJS_PS0_49; E=js}'),
                    width: 100, sortable: true, filtered: true, filter: getStringFilterControl([]), dataIndex: 'ActiveTimeDisplay', renderer: renderActiveTimeDisplay
                },
                {
                    header: getGridColumHeader('TriggerTime', '@{R=Core.Strings; K=WEBJS_PS0_50; E=js}'),
                    width: 150, sortable: true, filtered: true, filter: getDateTimeFilterControl(), dataIndex: 'TriggerTime', renderer: renderTriggerTime
                },
                {
                    header: getGridColumHeader('AcknowledgedBy','@{R=Core.Strings; K=WEBJS_PS0_51; E=js}'),
                    width: 150, sortable: true, filtered: true, filter: getStringFilterControl([]), dataIndex: 'AcknowledgedBy',  renderer: renderAcknowledgedBy
                },
                {
                    header: getGridColumHeader('AcknowledgeTime', '@{R=Core.Strings; K=WEBJS_PS0_52; E=js}'),
                    width: 150, sortable: true, filtered: true, filter: getDateTimeFilterControl(), dataIndex: 'AcknowledgeTime', renderer: renderAcknowledgeTime
                },
               /* {
                    header: getGridColumHeader('Notes', '@{R=Core.Strings; K=WEBJS_PS0_53; E=js}'),
                    width: 250,
                    sortable: true,
                    filtered: true,
                    filter: getStringFilterControl([]),
                    dataIndex: 'Notes'
                },*/
                {
                    header: getGridColumHeader('SeverityText', '@{R=Core.Strings; K=WEBJS_PS0_54; E=js}'),
                    width: 150, sortable: true, filtered: true, hidden: true, 
                    filter: getStringFilterControl(['@{R=Core.Strings; K=WEBJS_PS0_57; E=js}', '@{R=Core.Strings; K=WEBJS_JV0_01; E=js}', '@{R=Core.Strings; K=WEBJS_PS0_58; E=js}', '@{R=Core.Strings; K=WEBJS_JV0_02; E=js}', '@{R=Core.Strings; K=WEBJS_ZT0_34; E=js}']),
                    dataIndex: 'SeverityText'
                },
                {
                    header: getGridColumHeader('Category','@{R=Core.Strings; K=WEBJS_SO0_109; E=js}'),
                    width: 250, sortable: true, filtered: true, filter: getStringFilterControl([]), dataIndex: 'Category', renderer: renderString
                },
                {
                    header: getGridColumHeader('SiteName', '@{R=Core.Strings; K=AlertsView_SolarWindsSiteTitle; E=js}'),
                    width: 250, sortable: true, filtered: true, filter: getStringFilterControl([]), dataIndex: 'SiteName', renderer: renderString, hidden: !IsFederationEnabled, hideable: IsFederationEnabled
                },
                {
                    header: getGridColumHeader('IncidentNumber', '@{R=Core.Strings.2; K=AlertsView_IncidentNumberTitle; E=js}'),
                    width: 250, sortable: true, filtered: true, filter: getStringFilterControl([]), dataIndex: 'IncidentNumber', renderer: renderIncidentNumber, hidden: !IsIntegrationInitialized, hideable: IsIntegrationInitialized
                },
                {
                    header: getGridColumHeader('AssignedTo', '@{R=Core.Strings.2; K=AlertsView_IncidentAssignedToTitle; E=js}'),
                    width: 250, sortable: true, filtered: true, filter: getStringFilterControl([]), dataIndex: 'AssignedTo', renderer: renderString, hidden: !IsIntegrationInitialized, hideable: IsIntegrationInitialized
                }
        ];

        colCount = columnsModelConfig.length;

        // Initialize Data stores mapping from Custom properties
        var customPropertiesConfig = getCustomPropertiesConfig();
        for (var i = 0; i < customPropertiesConfig.length; i++) {
            var filterControl = null;
            if (customPropertiesConfig[i].PropertyType == "System.String")
            {
                filterControl = getStringFilterControl([]);
            }
            else if (customPropertiesConfig[i].PropertyType == "System.DateTime")
            {
                filterControl = getDateTimeFilterControl();
            }
            else if (customPropertiesConfig[i].PropertyType == "System.Single")
            {
                filterControl = getFloatFilterControl([]);
            }
            else if (customPropertiesConfig[i].PropertyType == "System.Int32")
            {
                filterControl = getIntegerFilterControl([]);
            }
            else if (customPropertiesConfig[i].PropertyType == "System.Boolean")
            {
                filterControl = getBoolFilterControl();
            }

            columnsModelConfig.push({
                header: getGridColumHeader(SW.Core.String.Format("CP_{0}", customPropertiesConfig[i].PropertyName), customPropertiesConfig[i].PropertyName),
                sortable: true,
                filtered: true,
                hidden: true,
                hideable: false,
                width: 250,
                filter: filterControl,
                dataIndex: SW.Core.String.Format("CP_{0}", customPropertiesConfig[i].PropertyName)
            });
        }

        return columnsModelConfig;
    }

    function getCustomPropertiesConfig() {
        var strCustomPropertiesConfig = $("#ActiveAlerts_CustomPropertiesConfig").val();
        var customPropertiesConfig = [];
        if (typeof strCustomPropertiesConfig != "undefined" && strCustomPropertiesConfig != "") {
            customPropertiesConfig = JSON.parse(strCustomPropertiesConfig);
        }

        return customPropertiesConfig;
    }
    
    function createActiveAlertsGrid() {
        var customPropertiesConfig = getCustomPropertiesConfig();
        var selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');
        var cpMenu = [];
        for (var i = 0; i < customPropertiesConfig.length; i++)
        {
            cpMenu.push({
                text: customPropertiesConfig[i].PropertyName,
                id: SW.Core.String.Format("CP_{0}", customPropertiesConfig[i].PropertyName),
                // loading checked state according to saved columns config in selectedColumns
                checked: $.inArray(SW.Core.String.Format("CP_{0}", customPropertiesConfig[i].PropertyName), selectedColumns) > -1, 
                cellIndex: i,
                handler: function (item, eventArgs) {
                    activeAlertsGrid.getColumnModel().setHidden(colCount + item.cellIndex, item.checked);
                }
            });
        }

        activeAlertsGrid = new Ext.grid.GridPanel({
            id: 'activeAlertsGrid',
            region: 'center',
            store: activeAlertsGridDataStore,
            stripeRows: true,
            trackMouseOver: false,
            split: true,
            layout: {
                type: 'vbox',       // Arrange child items vertically
                align: 'stretch',    // Each takes up full width
                padding: 2
            },
            viewConfig: {
                forceFit: true,
                getRowClass: function (record, index) {
                    if (record.get("Severity") == "0") {
                        return "sw-rowInfo";
                    } else if (record.get("Severity") == "1") {
                        return "sw-rowWarning";
                    } else if (record.get("Severity") == "2") {
                        return "sw-rowError";
                    } else if (record.get("Severity") == "3") {
                        return "sw-rowSerious";
                    } else if (record.get("Severity") == "4") {
                        return "sw-rowNotice";
                    }

                    return "sw-rowClass";
                },
                listeners: {
                    refresh: function (gridview) {
                        setTimeout(function () {
                            $("#activeAlertsGrid .x-grid3-row").width($("#activeAlertsGrid .x-grid3-row").width() + 22);
                        }, 10);
                    }
                }
            },
            columns: getActiveAlertsGridColumnsModelConfig(),
            sm: getActiveAlertsGridSelectorModel(),
            plugins: [getActiveAlertsGridFilters()],
            autoScroll: 'true',
            loadMask: true,
            width: 1295,
            height: 500,
            tbar: getActiveAlertsGridToolbarConfig(),
            bbar: pagingToolbar,
            //enableColumnHide: false,
            listeners: {
                'bodyresize': function () {
  
                },
                filterupdate: function () {
                    selectedItemsArray = [];
                },
                render: function () {
                    var menu = this.getView().hmenu;
                    menu.add([
                        {
                            icon: '/Orion/js/extjs/resources/images/default/s.gif',
                            iconCls: 'x-cols-icon',
                            text: '@{R=Core.Strings;K=WEBJS_PS0_62;E=JS}',
                            handler: function() {
                            },
                            menu: cpMenu
                        }
                    ]);
                }
            }
       });

        // load last used filters for user
        var filterConfig = JSON.parse(ORION.Prefs.load("LastUsedFilters", "[]"));
        for (var i = 0; i < activeAlertsGrid.filters.filters.items.length; i++) {
            for (var j = 0; j < filterConfig.length; j++) {
                if (filterConfig[j].ColumnName === activeAlertsGrid.filters.filters.items[i].dataIndex) {
                    activeAlertsGrid.filters.filters.items[i].setActive(true);
                    activeAlertsGrid.filters.filters.items[i].setValue(filterConfig[j].Value);
                }
            }
        }

        activeAlertsGrid.view = new Ext.grid.AlertGridView(activeAlertsGrid.viewConfig);

        // making columns visible or hidden according to saved configuration in selectedColumns
        if (selectedColumns.length != 1 || selectedColumns[0] != '') {
            var colModel = activeAlertsGrid.getColumnModel();
            for (var index = 1; index < colModel.getColumnCount(); index++) {
                if (selectedColumns.indexOf(colModel.getDataIndex(index)) > -1 && index > 1) {
                    colModel.setHidden(index, false);
                } else {
                    colModel.setHidden(index, true);
                }
            }
        }

        activeAlertsGrid.store.on('load', function (store, records) {
            var grid = activeAlertsGrid;
            var selectedRowIndexes = [];
           $.each(records, function(i) {
               if (getSelectedItemIndex(this) != -1)
                   selectedRowIndexes.push(i);
           });

           var selectionModel = grid.getSelectionModel();
           for (var rowIndex = 1; rowIndex < selectedRowIndexes.length; rowIndex++) {
               selectionModel.selectRow(selectedRowIndexes[rowIndex], true, false, true);
           }

           if (selectedRowIndexes.length > 0)
               selectionModel.selectRow(selectedRowIndexes[0], true);

           updateSelectAllTooltip(grid, function() {
               return getAllSelectedAlerts();
           });
       });

        activeAlertsGrid.on('sortchange', function (store, option) {
                var sort = option.field;
                var dir = option.direction;
                if (sort && dir) {
                    sortOrder.SortColumn = sort;
                    sortOrder.SortDir = dir;
                    ORION.Prefs.save('SortOrder', JSON.stringify(sortOrder));
                }
        });

        activeAlertsGrid.getColumnModel().on('hiddenchange', function () {
            var cols = '';
            var columnModel = activeAlertsGrid.getColumnModel();
            for (var i = 1; i < columnModel.getColumnCount() ; i++) {
                if (!columnModel.isHidden(i)) {
                    cols += columnModel.getDataIndex(i) + ',';
                }
            }
            ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
        });
    }
    function createAndInitializePagingToolbar() {
        pagingToolbar = new Ext.PagingToolbar(
                {
                    store: activeAlertsGridDataStore,
                    pageSize: activeAlertsGridPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VL0_10; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        {
                            xtype: 'label',
                            text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}',
                            style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;'
                        },
                        new Ext.form.ComboBox({
                            regex: /^\d*$/,
                            store: new Ext.data.SimpleStore({
                                fields: ['pageSize'],
                                data: [[10], [20], [25], [30], [50], [100], [150], [200], [250]]
                            }),
                            displayField: 'pageSize',
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus: true,
                            width: 50,
                            editable: false,
                            value: activeAlertsGridPageSize,
                            listeners: {
                                select: function (c, record) {
                                    pagingToolbar.pageSize = record.get("pageSize");
                                    pagingToolbar.cursor = 0;
                                    ORION.Prefs.save('PageSize', pagingToolbar.pageSize);
                                    activeAlertsGridPageSize = pagingToolbar.pageSize;
                                    pagingToolbar.doRefresh();
                                }
                            }
                        })
                    ]
                }
            );
    }

    function getActiveAlertsGrid() {
        if (activeAlertsGrid == null) {
            createActiveAlertsGrid();
        }

        return activeAlertsGrid;
    }

    // MAIN WINDOW
    function createMainGridPanel() {
        mainGridPanel = new Ext.Panel({
            region: 'center',
            split: true,
            width: 1520,
            height: 500,
            layout: 'border',
            collapsible: false,
            items: [
                navPanel,
                activeAlertsGrid
            ],
            cls: 'no-border'
        });
    }

    function getRecordFromMainGrid(alertObjectID, activeNetObject, objectType, siteId) {
        var indexRow = -1;
        var record = null;
        for (var i = 0; i < activeAlertsGridDataStore.data.items.length; i++) {
            record = activeAlertsGridDataStore.data.items[i];
            if ((record.get("AlertObjectID") == alertObjectID) &&
                (record.get("ActiveNetObject") == activeNetObject) &&
                (record.get("ObjectType") == objectType) &&
                (record.get("SiteID") == siteId)) {
                indexRow = i;
                break;
            }
        }

        if (indexRow == -1)
            record = null;

        return record;
    }

    function getIndexOfRecordFromMainGrid(alertObjectID, activeNetObject, objectType) {
        var indexRow = -1;
        for (var i = 0; i < activeAlertsGridDataStore.data.items.length; i++) {
            var record = activeAlertsGridDataStore.data.items[i];
            if ((record.get("AlertObjectID") == alertObjectID) &&
                (record.get("ActiveNetObject") == activeNetObject) &&
                (record.get("ObjectType") == objectType)) {
                indexRow = i;
                break;
            }
        }

        return indexRow;
    }

    function mainPanelResize() {
        mainGridPanel.setWidth(1);
        mainGridPanel.setWidth($('#activeAlertsPlaceholder').width() - 18);
        var height = $('body').height() - $('#activeAlertsPlaceholder')[0].offsetTop - $("div#footer").height() - 50;
        if ($('html').hasClass('sw-is-msie')) {
            height -= 110;
            if ($("#availableUpdates").length > 0) {
                height -= $("#availableUpdates").height();
            }
        }

        if (height < 350)
            height = 350; // min height which has else sence

        if ($('html').hasClass('sw-is-mozilla')) {
            $(".x-panel-bwrap").width($("#alertGrid").width());
        }
       
        $('#activeAlertsPlaceholder').height(height);
        mainGridPanel.setHeight($('#activeAlertsPlaceholder').height());
    }

    // Refresh GroupBy and main grid
    function internalActiveAlertGridRefresh() {

        clearNotification();

        var tmpFilterPropertyParam = filterPropertyParam;
        var tmpFilterTypeParam = filterTypeParam;
        var tmpfilterValueParam = filterValueParam;


        SW.Core.Services.callControllerActionSync("/api/ActiveAlert", "AreActionsOfAllAlertsPaused", null,
               function (success) {
                   var $pauseActionsOfAllAlerts = $("#PauseActionsOfAllAlerts");
                   $("#ActiveAlerts_PauseActionsOfAllAlerts").val(success ? 1 : 0);
                   if (success) {
                       $pauseActionsOfAllAlerts.prop("checked", true);
                   }
                   else {
                       $pauseActionsOfAllAlerts.removeAttr("checked");
                   }
               },
               function (error) { }
        );

        try {
            groupingValue = JSON.parse(ORION.Prefs.load('GroupingValue', defaultJsonGroupingValue));
        } catch (ex) {
            groupingValue = JSON.parse(defaultJsonGroupingValue);
        }

        if (filterPropertyParam == "" && typeof groupingValue != "undefined" && groupingValue.Value != "") {
            filterPropertyParam = groupingValue.Value;
        }

        if (filterTypeParam == "" && typeof groupingValue != "undefined" && groupingValue.Type != "") {
            filterTypeParam = groupingValue.Type;
        }

        refreshGroupGridObjects(groupGrid, filterPropertyParam, filterTypeParam, filterValueParam, "", function () {

            var selectedVal = -1;

            var dtItems = groupGrid.getStore().data.items;
            for (var i = 0; i < dtItems.length; i++) {
                if (dtItems[i].data.Id == groupingValue.SubGroupValue)
                    selectedVal = i;
            }

            if (selectedVal == -1) {
                groupingValue.SubGroupValue = "[All]";
                selectedVal = 0;
            }
            groupGrid.getSelectionModel().selectRow(selectedVal, false);
            
        });

        var tmpfilterText = getModifiedFilterText(filterText);
        if (groupingValue.Name == '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}') {
            groupByComboBox.setValue(groupingValue.Name);
            refreshActiveAlertsGridObjects(0, activeAlertsGridPageSize, activeAlertsGrid, "", "", "", tmpfilterText, function () {
                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function() {
                    this.tooltip = 'processed';
                    $.swtooltip(this);
                });
            });
        } else {
            refreshActiveAlertsGridObjects(0, activeAlertsGridPageSize, activeAlertsGrid, groupingValue.Value,
                groupingValue.Type, groupingValue.SubGroupValue, tmpfilterText, function () {
                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function() {
                    this.tooltip = 'processed';
                    $.swtooltip(this);
                });
            });
        }
        filterPropertyParam = tmpFilterPropertyParam;
        filterTypeParam = tmpFilterTypeParam;
        fiterValueParam = tmpfilterValueParam;
    }

    function setAutoRefresh(timeout) {
        activeAlertGridRefreshTimeout = setTimeout(function () {
            internalActiveAlertGridRefresh();
            activeAlertGridRefreshTimeout = setTimeout(arguments.callee, timeout);
        }, timeout);
    }

    var dontAskForNotesWhenAckDisabled = true;
    function DontAskForNoteWhenAcknowledgeAlertOnChangeHandler(event) {
        var self = this;
        if (IsDemoServer) {
            this.checked = false;
            event.preventDefault();
            demoAction();
        } else {
            setTimeout(function () {
                SW.Core.Services.callController("/api/ActiveAlert/SetShowAcknowledgeAlertDialog", !$(self).prop("checked"), function () { }, function () { });
            }, 0);
        }
    }

    function initMoreMenu() {
        $(".sw-hdr-links table td.sw-preference").on("click", function (event) {
            $(this).toggleClass("sw-preference-open");
            $(".sw-preference-more").toggleClass("sw-preference-more-open");
            $(this).find("div.sw-pref-options").toggleClass("sw-pref-options-hidden");
            $(this).closest("tr").toggleClass("sw-preference-row-open");

            if (dontAskForNotesWhenAckDisabled) {
                dontAskForNotesWhenAckDisabled = false;
                SW.Core.Services.callController("/api/ActiveAlert/IsShowAcknowledgeAlertDialogEnabled", {}, function (success) {
                    if (!success) {
                        $("#DontAskForNoteWhenAcknowledgeAlert").prop("checked", !success);
                    } else {
                        $("#DontAskForNoteWhenAcknowledgeAlert").removeAttr("checked");
                    }

                }, function (failure) { });
            }
        });

        $("body").mousedown(function (event) {
            if ($(event.target).closest(".sw-pref-options").length == 0
                && !$(event.target).hasClass("sw-preference")
                && !$(event.target).hasClass("sw-preference-more")) {
                $("div.sw-pref-options").addClass("sw-pref-options-hidden");
                $(".sw-hdr-links table td.sw-preference").removeClass("sw-preference-open");
                $(".sw-preference-more").removeClass("sw-preference-more-open");
                $(".sw-preference-row-open").removeClass("sw-preference-row-open");
                dontAskForNotesWhenAckDisabled = true;
            }
        });

        $("#HideAcknowledgedAlerts").on("change", function () {
            var self = this;
            setTimeout(function () {
                if ($(self).is(":checked")) {
                    SW.Orion.ActiveAlerts.hideShowAcknowledgedAlerts(true);
                } else {
                    SW.Orion.ActiveAlerts.hideShowAcknowledgedAlerts(false);
                }
            }, 0);
        });

        $("#HideAcknowledgedAlerts").removeAttr('checked');

        $("#DontAskForNoteWhenAcknowledgeAlert").on("change", DontAskForNoteWhenAcknowledgeAlertOnChangeHandler);

        $("#PauseAutoRefresh").on("change", function () {
            var self = this;
            setTimeout(function () {
                if ($(self).is(":checked")) {
                    SW.Orion.ActiveAlerts.disableAutoRefresh();
                } else {
                	SW.Orion.ActiveAlerts.enableAutoRefresh();
                }
            }, 0);
        });

        var pauseActionsForAllAlerts = $("#ActiveAlerts_PauseActionsOfAllAlerts").val();
        if (pauseActionsForAllAlerts == "1") {
            $("#PauseActionsOfAllAlerts").prop("checked", true);
        }

        $("#PauseActionsOfAllAlerts").on("change", function (event) {
            if (IsDemoServer) {
                this.checked = false;
                event.preventDefault();
                demoAction();
            } else {
                SW.Orion.ActiveAlerts.pauseActionsOfAllAlerts($(this).prop("checked"));
            }
        });
         
        // setup initial state of More menu
        ORION.prefix = "ActiveAlerts_";

        if ($("html").hasClass("sw-is-msie")) {
            $("#adminContent .sw-preference").css("background-position-y", "2px");
            $(".sw-preference-more").css("padding-top", "0px");
        }

        var withoutAcknowledgedAlerts = ORION.Prefs.load('WithoutAcknowledgedAlerts', 'false').toLowerCase() == "true";
        $("#HideAcknowledgedAlerts").prop("checked", withoutAcknowledgedAlerts);
        var autoRefreshEnabled = ORION.Prefs.load('AutoRefreshEnabled', 'true').toLowerCase() == "true";
        if (!autoRefreshEnabled) {
            $("#PauseAutoRefresh").prop("checked", true);
            SW.Orion.ActiveAlerts.disableAutoRefresh();
        }

        // Setup left position of more menu
        var marginLeft = $(".sw-pref-options").css("margin-left");
        var $preferenceTextElem = $(".sw-preference span:first");
        var tmpPreferenceText = $preferenceTextElem.text();
        var preferenceCurWidth = $preferenceTextElem.width();
        var preferenceWithMoreTextWidth = $preferenceTextElem.text("More").width();
        $preferenceTextElem.text(tmpPreferenceText);
        var marginLeftNew = marginLeft - (preferenceWithMoreTextWidth - preferenceCurWidth);
        $(".sw-pref-options").css("margin-left", SW.Core.String.Format("{0}px", marginLeftNew));
    }

    // PUBLIC METHODS 
    return {
        init: function () {
		
			IsFederationEnabled = SW.Core.Alerts.IsFederationEnabled;
			IsIntegrationInitialized = SW.Core.Alerts.IsIncidentsIntegrationEnabled;
            IsDemoServer = SW.Core.Alerts.IsDemoServer;
            activeAlertsGridPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            allowDisableAllActions = hasUserRightToDisableAllActions();
            allowEventClear = hasUserRightsToClearEvents();

            try {
                groupingValue = JSON.parse(ORION.Prefs.load('GroupingValue', defaultJsonGroupingValue));
            } catch (ex) {
                groupingValue = JSON.parse(defaultJsonGroupingValue);
            }

            sortOrder = JSON.parse(ORION.Prefs.load('SortOrder', "{\"SortColumn\":\"ActiveTimeDisplay\", \"SortDir\":\"ASC\"}"));
            withoutAcknowledgedAlerts = ORION.Prefs.load('WithoutAcknowledgedAlerts', "false");

            if (!allowDisableAllActions) {
                $("tr.sw-pauseActionsForAllItems").css("display", "none");
            }

            // Initialize Data stores mapping from Custom properties
            var customPropertiesConfig = getCustomPropertiesConfig();
            var mappingStartIndex = activeAlertDataStoreMapping.length;
            for (var i = 0; i < customPropertiesConfig.length; i++) {
                activeAlertDataStoreMapping.push({ name: SW.Core.String.Format("CP_{0}", customPropertiesConfig[i].PropertyName), mapping: mappingStartIndex + i });
            }

            createDataStores();
            createAndInitializePagingToolbar();
            createGroupByComboBox();
            createGroupGrid();
            createActiveAlertsGrid();
            createGroupByTopPanel();
            createNavigationPanel();
            createMainGridPanel();
            refreshGroupGridObjects(groupByComboBox, "", "", "", "");
        
            groupByComboBox.setValue(groupingValue.Name);

            mainGridPanel.render('activeAlertsPlaceholder');
                    
            updateToolbarButtons();
            mainPanelResize();
            $(window).resize(mainPanelResize);

            // auto refresh
            setAutoRefresh(parseInt(ORION.Prefs.load('AutoRefreshInterval')) * 1000 * 60);

            initMoreMenu();

            // Workaround for Chrome browser to show selected grouping value correctly
            setTimeout(internalActiveAlertGridRefresh, 0);
        },
        acknowledgeActiveAlert: function(alertDefId, alertObjectId, activeObject, objectType, siteId) {
            var onSucc = function(result) {
                if (result.Acknowledged) {
                    var record = getRecordFromMainGrid(alertObjectId, activeObject, objectType, siteId);
                    record.set("AcknowledgedBy", result.UserNamePerSite[siteId]);
                    record.set("AcknowledgeTime", result.AcknowledgeDateTimeInDisplayFormat);
                    record.set("Notes", result.Notes);
                    record.commit();
                    activeAlertsGrid.getView().refresh();
                }
            };
            var onFail = function (result) { };
            SW.Orion.Alerts.AcknowledgeAlertDialog.AcknowledgeAlerts([{
                AlertObjectID: alertObjectId,
                AlertDefID: alertDefId,
                ActiveNetObject: activeObject,
                ObjectType: objectType,
                SiteID: siteId
            }], onSucc, onFail);
        },
        clearActiveAlert: function (alertObjectID, activeObject, objectType) {
            var deleteFn = function() {
                var indexRow = getIndexOfRecordFromMainGrid(alertObjectID, activeObject, objectType);
                if (indexRow > -1) {
                    SW.Core.Services.callController('/api/ActiveAlertsGrid/ClearActiveAlert', { AlertObjectID: alertObjectID, ActiveNetObject: activeObject, ObjectType: objectType }, function (result) {
                        if (result) {
                            activeAlertsGridDataStore.removeAt(indexRow);
                        }
                    });
                }
            };

            Ext.Msg.confirm(
                "@{R=Core.Strings; K=WEBJS_PS0_42; E=js}",
                "@{R=Core.Strings; K=WEBJS_PS0_43; E=js}",
                function (btn, text) {
                    if (btn == "yes") {
                        deleteFn();
                    }
                });
        },
        removeFilter: function (name, comparison) {
            var grid = getActiveAlertsGrid();
            for (var i = 0; i < grid.filters.filters.items.length; i++) {
            if (grid.filters.filters.items[i].active && grid.filters.filters.items[i].dataIndex == name) {
                // for one numeric column we're able to define several filters (less then, greater then etc.)
                // so we need to clear filter with exact the same comparison
                switch (grid.filters.filters.items[i].type) {
                    case "numeric":
                        grid.filters.filters.items[i].disableFilter(comparison);
                        break;
                    case "date":
                        grid.filters.filters.items[i].disableFilter(comparison);
                        break;
                    case "dateTime":
                        grid.filters.filters.items[i].disableFilter(comparison);
                    case "list":
                        grid.filters.filters.items[i].disableFilter();
                        break;
                    case "string":
                        grid.filters.filters.items[i].disableFilter();
                        break;
                    default:
                        grid.filters.filters.items[i].active = false;
                        break;
                }
            }
        }
            // after remove filter go to first page
        grid.store.lastOptions.params.start = 0;
            // atrer filter changes we need to reload grid store
        grid.store.reload();
        },
        hideShowAcknowledgedAlerts: function (hide) {
            withoutAcknowledgedAlerts = hide;
            ORION.Prefs.save('WithoutAcknowledgedAlerts', hide.toString());
            internalActiveAlertGridRefresh();
        },
        refresh: function () {
            internalActiveAlertGridRefresh();
        },
        enableAutoRefresh: function() {
            setAutoRefresh(parseInt(ORION.Prefs.load('AutoRefreshInterval'))*1000*60);
            ORION.Prefs.save('AutoRefreshEnabled', 'true');
            reconfigureFilterNotifications();
            internalActiveAlertGridRefresh();
            $("#PauseAutoRefresh").prop("checked", false);
        },
        disableAutoRefresh: function () {
            clearTimeout(activeAlertGridRefreshTimeout);
            ORION.Prefs.save('AutoRefreshEnabled', 'false');
            reconfigureFilterNotifications();
        },
        pauseActionsOfAllAlerts: function (pause) {
            var $pauseActionsOfAllAlerts = $("#PauseActionsOfAllAlerts");
            setTimeout(function () {
                SW.Core.Services.callController("/api/ActiveAlert/PauseActionsOfAllAlerts",
                    pause,
                    function (success) {
                        $("#ActiveAlerts_PauseActionsOfAllAlerts").val(pause ? 1 : 0);
                        if (pause) {
                            $pauseActionsOfAllAlerts.prop("checked", true);
                        }
                        else {
                            $pauseActionsOfAllAlerts.removeAttr("checked");
                        }
                        reconfigureFilterNotifications();
                    },
                    function (failure) { });
            });
        }
    };
} ();

Ext.onReady(SW.Orion.ActiveAlerts.init, SW.Orion.ActiveAlerts);

