﻿
SW.Core.namespace("SW.Core.Web").Integration = function () {

    var urlCustomerPortalLicences = '/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_license&CMP=PRD-IPS-Settings-LicMngmnt-X-NOIP-X';
    var allTicketsUrl = '/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_support&CMP=PRD-IPS-Settings-ManageTickets-X-NOIP-X';
    var submitTicketUrl = '/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_support_ticket&CMP=PRD-IPS-Settings-OpenTicket-X-NOIP-X';
    var forgottenLoginUrl = '/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_forgotten_login&CMP=PRD-IPS-Settings-CPLoginPopup-X-NOIP-X';

    var tokenProxyUrl = '/Orion/Admin/WebIntegrationProxy.aspx?url=';

    var templateLogin = '<div id="sw-logincustomerportal-popup">' +
        '    <div class="sw-logincustomerportal-wrapper">' +
        '       <table>' +
        '           <tr>' +
        '               <td class="sw-logincustomerportal-image">' +
        '                   <img src="/Orion/images/SettingPageIcons/SW_logo_icon32x32.png" style="position:relative;top:-10px" />' +
        '               </td>' +
        '               <td>' +
        '                   <div class="sw-logincustomerportal-content">' +
        '                       <div class="sw-logincustomerportal-text"></div>' +
        '                   </div>' +
        '               </td>' +
        '           </tr>' +
        '           <tr>' +
        '               <td colspan=2>' +
        '                   <div class="sw-logincustomerportal-error" style="display: none;"></div>' +
        '               </td>' +
        '           </tr>' +
        '           <tr>' +
        '               <td>&nbsp;</td>' +
        '               <td>' +
        '                   <div class="sw-logincustomerportal-content">' +
        '                       <div class="sw-logincustomerportal-login"></div>' +
        '                       <div class="sw-logincustomerportal-collapse"></div>' +
        '                       <div class="sw-logincustomerportal-collapsable"></div>' +
        '                   </div>' +
        '               </td>' +
        '           </tr>' +
        '       </table>' +
        '   </div>';

    var fulltemplateLicense = '<div><table>' +
                          '<tr>' +
                             '<td id="sw-LAM-table"></td>' +
                          '</tr>' +
                          '<tr>' +
                             '<td id="sw-LAM-btn"></td>' +
                          '</tr>' +
                       '</table></div>';

    var tableTemplateLicense = '<div><div class="sw-LAM-table">' +
                            '<div onclick="SW.Core.SalesTrigger.ShowMaintenancePopupAsync()" id="sw-LAM-row-expiredMaintenance">' +
                                '<span></span>' +
                                '<span class="sw-LAM-rowpart-name">@{R=Core.Strings;K=WEBJS_JD0_14;E=js}</span>' +
                                '<span class="sw-LAM-rowpart-arrow">»</span>' +
                            '</div>' +
                            '<div onclick="SW.Core.SalesTrigger.ShowMaintenancePopupAsync()" id="sw-LAM-row-expiringMaintenance">' +
                                '<span></span>' +
                                '<span class="sw-LAM-rowpart-name">@{R=Core.Strings;K=WEBJS_JD0_02;E=js}</span>' +
                                '<span class="sw-LAM-rowpart-arrow">»</span>' +
                            '</div>' +
                            '<div onclick="SW.Core.SalesTrigger.ShowEvalPopupAsync()" id="sw-LAM-row-expiringEval">' +
                                '<span></span>' +
                                '<span class="sw-LAM-rowpart-name">@{R=Core.Strings;K=WEBJS_JD0_03;E=js}</span>' +
                                '<span class="sw-LAM-rowpart-arrow">»</span>' +
                            '</div>' +
                            '<div onclick="SW.Core.SalesTrigger.ShowLicensePopupAsync()" id="sw-LAM-row-licenseLimit">' +
                                '<span></span>' +
                                '<span class="sw-LAM-rowpart-name">@{R=Core.Strings;K=WEBJS_JD0_04;E=js}</span>' +
                                '<span class="sw-LAM-rowpart-arrow">»</span>' +
                            '</div>' +
                            '<div onclick="SW.Core.SalesTrigger.ShowUpgradePopupAsync()" id="sw-LAM-row-updates">' +
                                '<span></span>' +
                                '<span class="sw-LAM-rowpart-name">@{R=Core.Strings;K=WEBJS_JD0_05;E=js}</span>' +
                                '<span class="sw-LAM-rowpart-arrow">»</span>' +
                            '</div>' +
                            '<div onclick="window.open(\'{0}\',\'_blank\',\'menubar=yes,statusbar=yes,toolbar=yes\')" id="sw-LAM-row-activeMaintenance">' +
                                '<span></span>' +
                                '<span class="sw-LAM-rowpart-name">@{R=Core.Strings;K=WEBJS_JD0_15;E=js}</span>' +
                                '<span class="sw-LAM-rowpart-arrow">»</span>' +
                            '</div>' +
                        '</div></div>';

    var fulltemplateCases = '<div><table>' +
                          '<tr>' +
                             '<td id="sw-SC-table"></td>' +
                          '</tr>' +
                          '<tr>' +
                             '<td id="sw-SC-btn"></td>' +
                          '</tr>' +
                       '</table></div>';

    var tableTemplateCases = '<div><table class="sw-SC-table">' +
                            '<thead>' +
                                '<tr>' +
                                    '<th>#</th>' +
                                    '<th>@{R=Core.Strings;K=WEBJS_JD0_07;E=js}</th>' +
                                    '<th>@{R=Core.Strings;K=WEBJS_JD0_08;E=js}</th>' +
                                '</tr>' +
                            '<thead>' +
                            '<tbody>' +
                            '</tbody>' +
                        '</table></div>';

    var authToken = null;
    var logInDialog = null;
    var logInPopupHeight = 570;
    var currentUsername = null;

    var SCshowed = false;
    var LAMshowed = false;

    var loginButtonEventRegistered = false;

    var renderWidget = function (enabled) {
        $("#sw-settings-rightbox-inpst-container").hide();
        $("#sw-settings-rightbox-SC-container").hide();
        $("#sw-settings-rightbox-LAM-container").hide();
        if (authToken) {
            $('#sw-settings-cp-username').html(authToken.UserId);
        }
        if (enabled) {
            renderAuthEnabledWidget();
            $('#sw-settings-cp-notenabled').hide('fast');
        } else {
            renderAuthDisabledWidget();
            $('#sw-settings-cp-enabled').hide('fast');
        }
    };

    var renderAuthEnabledWidget = function () {
        SCshowed = false;
        LAMshowed = false;
        FillSupportCasesAsync();
        FillLicenseAndManagementAsync();
        $('#sw-settings-cp-enabled').show('fast');
    };

    var renderAuthDisabledWidget = function() {
        $('#sw-settings-cp-notenabled').show('fast');
    };

    var processLogIn = function (callback) {
        if (!validateLogInFields())
            return false;

        $('#sw-login-customerportal-logging').show(200);
        $('#sw-login-customerportal-loginButton').hide(200);

        currentUsername = $('#sw-logincustomerportal-username').val();
        var password = $('#sw-logincustomerportal-password').val();

        SW.Core.Services.callWebService('/Orion/Services/WebIntegration.asmx', 'EnableWebIntegration', {
            userIdentification: currentUsername,
                userPassword: password
            }, function(r) {
                if (r.success) {
                    authToken = r.token;                    

                    SW.Core.Analytics.TrackEvent('Customer Portal Integration', 'Logged In');

                    if (callback) {
                        callback(r);
                    }

                    // let's do the reload after one second to make sure that analytics value will be processed properly
                    setTimeout(function() { window.location.reload(); }, 1000);
                } else {
                    showLogInError(r.message);

                    var reason = r.failureReason ? r.failureReason : 'Unexpected error';

                    SW.Core.Analytics.TrackEvent('Customer Portal Integration', 'Integration Error', reason);

                    logInDialog.integrationLoginError = reason;
                }
            }, function(e) {
                showLogInError('@{R=Core.Strings;K=WEBJS_JD0_17;E=js}');
            });

        return true;
    };

    var showLogInError = function (message) {
        if (message) {

            var errorBox = $('.sw-logincustomerportal-error');
            errorBox.html(message);

            //if error contains url, append UserName value
            $("a", errorBox).each(function () {
                var url = $(this).attr("href");
                var delimiter = url.indexOf('?') > -1 ? '&' : '?';
                $(this).attr("href", url + delimiter + "userName=" + encodeURIComponent(currentUsername || ""));
            });

            errorBox.show();

            $('#sw-login-customerportal-logging').hide(200);
            $('#sw-login-customerportal-loginButton').show(200);

            var height = logInPopupHeight + errorBox.height();
            logInDialog.animate({ height: height }, 100);
        }
    };

    var hideLogInError = function() {
        var errorBox = $('.sw-logincustomerportal-error');

        if (errorBox.css('display') != 'none') {
            errorBox.hide();

            logInDialog.animate({ height: logInPopupHeight }, 100);
        }
    };

    var validateLogInFields = function() {
        var username = $('#sw-logincustomerportal-username').val();
        var password = $('#sw-logincustomerportal-password').val();

        if (!username || username.length === 0 || !password || password.length === 0) {
            showLogInError('@{R=Core.Strings;K=WEBJS_JD0_18;E=js}');
            return false;
        }

        hideLogInError();

        return true;
    };

    var prepareOutgoingLink = function (url) {
        if (SW.Core.Info.OIPEnabled) {
            return url.replace('NOIP', 'OIP');
        }
        return url;
    };
    
    this.RenderCustomerPortalWidget = function() {
        SW.Core.Services.callWebService('/Orion/Services/WebIntegration.asmx', 'GetUserToken', {}, function(r) {
            authToken = r.token;
            renderWidget(r.enabled);
        });
    };

    this.ExpandCollapse = function() {
        var element = $("#LogInCustomerPortalCollapseImg")[0];
        var expanded = element.src.indexOf("Collapse") >= 0;

        element.src = expanded ? "/Orion/images/Button.Expand.gif" : "/Orion/images/Button.Collapse.gif";

        $('.sw-logincustomerportal-collapsable').css("display", expanded ? "none" : "block");
        $('.sw-logincustomerportal-collapse a:first').css("display", expanded ? "inline-block" : "none");

        return false;
    };

    this.LogOutFromCustomerPortal = function () {
        SW.Core.Services.callWebService('/Orion/Services/WebIntegration.asmx', 'DisableWebIntegration', {}, function(r) {
            authToken = null;
            renderWidget(false);
        });
    };

    this.LogInCustomerPortal = function (callback) {
        var templatelink = $(templateLogin);

        var contentText = '<div>@{R=Core.Strings.2;K=WEBJS_ZS0_1;E=js}</div><ul>' +
                          '<li>@{R=Core.Strings.2;K=WEBJS_ZS0_9;E=js}</li>' +
                          '<li>@{R=Core.Strings.2;K=WEBJS_ZS0_10;E=js}</li>' +
                          '<li>@{R=Core.Strings.2;K=WEBJS_ZS0_11;E=js}</li></ul>';

        $('.sw-logincustomerportal-text', templatelink).html(contentText);

        var loginPart = '<div class="sw-logincustomerportal-input"><div><label id="sw-logincustomerportal-username-label" for="sw-logincustomerportal-username">@{R=Core.Strings.2;K=WEBJS_ZS0_2;E=js}' +
            '<span><img title="@{R=Core.Strings.2;K=WEBJS_JD0_7;E=js}" src="/Orion/images/Info_Icon_MoreDecent_16x16.png" /></span></label>' +
            '<div><input autofocus="autofocus" name="sw-logincustomerportal-username" type="text" id="sw-logincustomerportal-username" tabindex="1" automation="username"></div></div>' +
            '<div><label for="sw-logincustomerportal-password">@{R=Core.Strings.2;K=WEBJS_ZS0_3;E=js}</label>' +
            '<div><input id="sw-logincustomerportal-password" type="password" autocomplete="off" automation="password" tabindex="2" name="sw-logincustomerportal-password"></input></div></div></div>';


        var logInContinueButton = function(options) {
            var defaults = {
                type: 'primary',
                label: '@{R=Core.Strings.2;K=WEBJS_ZS0_4;E=js}',
                cls: 'sw-logincustomerportal-loginbuttons'
            };

            var opt = $.extend({}, defaults, options || {});

            var html;
            html = SW.Core.Widgets.Button(opt.label, {
                type: opt.type,
                id: opt.id || SW.Core.Id(),
                cls: opt.cls
            });

            if (!loginButtonEventRegistered) {
                
                $('body').on('click', '.sw-logincustomerportal-loginbuttons', function() {
                    processLogIn(callback);
                });

                $('body').on('keyup', '#sw-logincustomerportal-username, #sw-logincustomerportal-password', function(event) {
                    if (event.keyCode == 13) {
                        //enter pressed
                        processLogIn(callback);
                    }
                });

                loginButtonEventRegistered = true;
            }

            return html;
        };

        var forgotCredentialsLink = '<div id="sw-login-customerportal-forgottenLink"><a class="sw-link" href="' + prepareOutgoingLink(forgottenLoginUrl) + '" target="_blank" rel="noopener noreferrer" id="sw-logincustomerportal-forgot-credentials-link">&#0187;&nbsp;@{R=Core.Strings.2;K=WEBJS_ZS0_5;E=js}</a></div>';

        var loginButtonLinkSection = '<div id="sw-login-customerportal-loginButton">' + logInContinueButton({ id: 'sw-login-customerportal-submit' }) + '' + forgotCredentialsLink + '</div>';

        loginPart += loginButtonLinkSection;

        var loggingImage = '<div id="sw-login-customerportal-logging" style="display:none"><img src="/Orion/images/animated_loading_sm3_whbg.gif" /><span>@{R=Core.Strings;K=WEBJS_JD0_16;E=js}</span></div>';
        loginPart += loggingImage;

        $('.sw-logincustomerportal-login', templatelink).html(loginPart);
        $('.sw-logincustomerportal-login', templatelink).tooltip();

        var collapsePart = '<img id="LogInCustomerPortalCollapseImg" src="/Orion/images/Button.Expand.gif" alt="Expand/Collapse" onclick="return SW.Core.Web.Integration.ExpandCollapse();" style="cursor: pointer;"/>' +
            '<span onclick="return SW.Core.Web.Integration.ExpandCollapse();" style="cursor: pointer;">@{R=Core.Strings.2;K=WEBJS_ZS0_6;E=js}</span>';

        $('.sw-logincustomerportal-collapse', templatelink).html(collapsePart);

        var collapseablePart = '<p><span>@{R=Core.Strings.2;K=WEBJS_LC0_3;E=js} <a href="mailto:customersales@solarwinds.com" class="sw-link">customersales@solarwinds.com</a></span></p>' +
            '<p><span>@{R=Core.Strings.2;K=WEBJS_LC0_4;E=js}</span> <a href="tel:866 530 8100">866 530 8100</a></p>' +
            '<p><span>@{R=Core.Strings.2;K=WEBJS_LC0_5;E=js}</span> <a href="tel:+353 21 5002900">+353 21 5002900</a></p>' +
            '<p><span>@{R=Core.Strings.2;K=WEBJS_LC0_6;E=js}</span> <a href="tel:+61 2 8412 4900">+61 2 8412 4900</a></p>';

        $('.sw-logincustomerportal-collapsable', templatelink).html(collapseablePart);

        SW.Core.Analytics.TrackEvent('Customer Portal Integration', 'Login Popup Opened');

        // fire up the dialog
        logInDialog = $(templatelink).dialog({
            title: '@{R=Core.Strings.2;K=WEBJS_ZS0_8;E=js}',
            width: 500,
            height: logInPopupHeight,
            dialogClass: 'LogInCustomerPortal',
            modal: true,
            close: function (event, ui) {
                
                // in successful scenario this should not be called at all (the page is reloaded so the popup will not be closed)
                // so we can say that user abandoned the login
                if (logInDialog.integrationLoginError) {
                    SW.Core.Analytics.TrackEvent('Customer Portal Integration', 'Integration Error - Fallout', logInDialog.integrationLoginError);
                } else {
                    SW.Core.Analytics.TrackEvent('Customer Portal Integration', 'Login Popup Abandoned');
                }

                $(this).dialog('destroy').remove();
                logInDialog = null;
            }
        });
    };

    this.FillLicenseAndManagementAsync = function () {

        SW.Core.Services.callWebService('/Orion/Services/WebIntegration.asmx', 'GetLAMInfo', {}, function (r) {
            LAMshowed = true;
            FillLicenseAndManagement(r);
            if (SCshowed) {
                $("#sw-settings-rightbox-inpst-container").show(150);
            }
        });
    };

    this.FillLicenseAndManagement = function (LAMdata) {

        if (!LAMdata.success) {
            $("#sw-settings-rightbox-LAM-container").hide();
            return;
        }

        if (LAMdata.maintenanceExpiredCount == 0 && LAMdata.maintenanceExpiringCount == 0 && LAMdata.evaluationCount == 0
            && LAMdata.licenseLimitCount == 0 && LAMdata.updatesCount == 0 && LAMdata.maintenanceActiveCount == 0) {
            //if there is no data to be displayed - hide all
            $("#sw-settings-rightbox-LAM-container").hide();
            return;
        }
        

        var template = $(SW.Core.String.Format(tableTemplateLicense, prepareOutgoingLink(urlCustomerPortalLicences)));

        var displayedCtn = 0;
        var lastDisplayed = 0;
        //table - display only rows with non-zero values
        if (LAMdata.maintenanceExpiredCount > 0) {
            $('.sw-LAM-table #sw-LAM-row-expiredMaintenance span:first', template).html(LAMdata.maintenanceExpiredCount);
            displayedCtn++;
            lastDisplayed = "sw-LAM-row-expiredMaintenance";
        } else {
            $('.sw-LAM-table #sw-LAM-row-expiredMaintenance', template).hide();
        }
        if (LAMdata.maintenanceExpiringCount > 0) {
            $('.sw-LAM-table #sw-LAM-row-expiringMaintenance span:first', template).html(LAMdata.maintenanceExpiringCount);
            displayedCtn++;
            lastDisplayed = "sw-LAM-row-expiringMaintenance";
        } else {
            $('.sw-LAM-table #sw-LAM-row-expiringMaintenance', template).hide();
        }
        if (LAMdata.evaluationCount > 0) {
            $('.sw-LAM-table #sw-LAM-row-expiringEval span:first', template).html(LAMdata.evaluationCount);
            displayedCtn++;
            lastDisplayed = "sw-LAM-row-expiringEval";
        } else {
            $('.sw-LAM-table #sw-LAM-row-expiringEval', template).hide();
        }
        if (LAMdata.licenseLimitCount > 0) {
            $('.sw-LAM-table #sw-LAM-row-licenseLimit span:first', template).html(LAMdata.licenseLimitCount);
            displayedCtn++;
            lastDisplayed = "sw-LAM-row-licenseLimit";
        } else {
            $('.sw-LAM-table #sw-LAM-row-licenseLimit', template).hide();
        }
        if (LAMdata.updatesCount > 0) {
            $('.sw-LAM-table #sw-LAM-row-updates span:first', template).html(LAMdata.updatesCount);
            displayedCtn++;
            lastDisplayed = "sw-LAM-row-updates";
        } else {
            $('.sw-LAM-table #sw-LAM-row-updates', template).hide();
        }
        if (LAMdata.maintenanceActiveCount > 0) {
            $('.sw-LAM-table #sw-LAM-row-activeMaintenance span:first', template).html(LAMdata.maintenanceActiveCount);
            displayedCtn++;
            lastDisplayed = "sw-LAM-row-activeMaintenance";
        } else {
            $('.sw-LAM-table #sw-LAM-row-activeMaintenance', template).hide();
        }

        if (displayedCtn % 2 > 0) {
            $('.sw-LAM-table #' + lastDisplayed, template).attr("style", "width:431px");
        }

        var tableHtml = template.html();

        //button
        var button1Html = SW.Core.String.Format('<a href="{0}" target="_blank" rel="noopener noreferrer">{1}</a>', prepareOutgoingLink(urlCustomerPortalLicences), '@{R=Core.Strings;K=WEBJS_JD0_06;E=js}');
        var tmp = $(fulltemplateLicense);
        $("#sw-LAM-table", tmp).html(tableHtml);

        $("#sw-settings-rightbox-LAM-header").html('@{R=Core.Strings.2;K=WEBJS_JD0_5;E=js}');
        $("#sw-settings-rightbox-LAM-header-links").html(button1Html);
        $("#sw-settings-rightbox-LAM-content").html(tmp.html());
        $("#sw-settings-rightbox-LAM-container").show();
    };

    this.FillSupportCasesAsync = function () {

        SW.Core.Services.callWebService('/Orion/Services/WebIntegration.asmx', 'GetSupportCases', {}, function (r) {
            FillSupportCases(r);
            SCshowed = true;
            if (LAMshowed) {
                $("#sw-settings-rightbox-inpst-container").show(150);
            }
        });
    };

    this.FillSupportCases = function (SCdata) {

        if (!SCdata.success || SCdata.cases.length == 0) {
            $("#sw-settings-rightbox-SC-content").hide();
            return;
        }

        var template = $(tableTemplateCases);

        //get data
        var content = '';
        var rowFormat = '<tr onclick="window.open(\'{0}\', \'_blank\', \'menubar=yes,statusbar=yes,toolbar=yes\')">' +
                            '<td><a href="#">{1}</a></td>' +
                            '<td title="{2}" class="sw-SC-case-title">{2}</td>' +
                            '<td class="sw-SC-case-date">{3}</td>' +
                        '</tr>';
        var count = 0;
        var maxRows = 3;
        $.each(SCdata.cases, function () {
            if (count < maxRows) {
                content += SW.Core.String.Format(rowFormat, tokenProxyUrl + encodeURIComponent(this.url), this.caseNumber, this.title, this.lastUpdate);
                count++;
            }
        });

        //table
        $('tbody', template).html(content);

        var tableHtml = template.html();

        //buttons
        var linkTemplate = '<a href="{0}" target="_blank" rel="noopener noreferrer">{1}</a>';
        var button1Html = SW.Core.String.Format(linkTemplate, prepareOutgoingLink(submitTicketUrl), '@{R=Core.Strings;K=WEBJS_JD0_11;E=js}');
        var button2Html = SW.Core.String.Format(linkTemplate, prepareOutgoingLink(allTicketsUrl), '@{R=Core.Strings;K=WEBJS_JD0_12;E=js}');

        var tmp = $(fulltemplateCases);
        $("#sw-SC-table", tmp).html(tableHtml);

        $("#sw-settings-rightbox-SC-content").html(tmp.html());

        var casesCount = SCdata.cases.length <= 0 ? "0" :
            SW.Core.String.Format('@{R=Core.Strings.2;K=WEBJS_JD0_6;E=js}', (SCdata.cases.length < maxRows ? SCdata.cases.length : maxRows), SCdata.cases.length);

        $("#sw-settings-rightbox-SC-header").html(SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_JD0_13;E=js}', casesCount));
        $("#sw-settings-rightbox-SC-header-links").html(button2Html + '&nbsp;' + button1Html);
        $("#sw-settings-rightbox-SC-container").show();
    };

    return this; 
}();

