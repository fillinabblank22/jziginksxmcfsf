/// <reference path="../../js/jquery/../jquery/jquery-1.7.1-vsdoc.js" />
/// <reference path="../../js/extjs/3.4/debug/ext-all-debug.js" />

Ext.namespace('SW.Orion');

var listboxID = '';

/// <summary>
///     Constructor of ListViews object
/// </summary>
SW.Orion.ListViews = function () {
};

/// <summary>
///     Method is responsible for creating and rendering toolbar on the page ListViews.aspx
/// </summary>
/// <param name="btnAddUniqueID"  type="String">
///     Unique identifier of btnAdd button on the page.
/// </param>
/// <param name="btnCopyUniqueID" type="String">
///     Unique identifier of btnEdit button on the page.
/// </param>
/// <param name="btnDeleteUniqueID" type="String">
///     Unique identifier of btnDelete button on the page.
/// </param>

var checkForNavigationUssage = function (callback) {
    
    var sel = document.getElementById(listboxID);
    var listLength = sel.options.length;

    var viewName = '';
    var viewID = 0;

    for (var i = 0; i < listLength; i++) {
        if (sel.options[i].selected) {
            viewName = sel.options[i].innerHTML;
            viewID = sel.options[i].value;
        }
    }

    if (viewID == 0)
        callback(true);

    ORION.callWebService("/Orion/Admin/ListViews.aspx",
        "IsViewInUse", { viewID: viewID, viewName: viewName },
        function (result) {
            if (result) {
                Ext.Msg.show({
                    title: '@{R=Core.Strings.2;K=WEBJS_SO0_5; E=js}',
                    msg: '@{R=Core.Strings.2;K=WEBJS_SO0_7; E=js}',
                    buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                    icon: Ext.Msg.WARNING,
                    fn: function (button) {
                        if (button == "yes") {
                            callback(true);
                        }
                    }
                });
            } else {
                Ext.Msg.show({
                    title: '@{R=Core.Strings.2;K=WEBJS_SO0_5; E=js}',
                    msg: '@{R=Core.Strings.2;K=WEBJS_SO0_6; E=js}',
                    buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                    icon: Ext.Msg.QUESTION,
                    fn: function (button) {
                        if (button == "yes") {
                            callback(true);
                        }
                    }
                });
            }
        });
};

SW.Orion.ListViews.prototype.createAndRenderToolbar = function (btnAddUniqueID, btnEditUniqueID, btnCopyUniqueID, btnDeleteUniqueID, lbID) {
    listboxID = lbID;
    var toolbar = new Ext.Toolbar({
        height: 35,
        region: 'north',
        style: { border: '1px solid #d0d0d0' },
        items: [{
            id: 'btnAdd',
            text: '&nbsp;@{R=Core.Strings; K=WEBJS_PS0_1; E=js}',
            iconCls: 'btnAdd',
            xtype: 'button',
            iconAlign: 'left',
            handler: function () {
                var options = new WebForm_PostBackOptions(btnAddUniqueID, "", true, "", "", false, false);
                __doPostBack(options.eventTarget, options.eventArgument);
            }
        },
            {
                xtype: 'tbseparator', height: 27
            },
            {
                id: 'btnEdit',
                text: '&nbsp;@{R=Core.Strings; K=WEBJS_PS0_2; E=js}',
                iconCls: 'btnEdit',
                xtype: 'button',
                iconAlign: 'left',
                handler: function () {
                    var options = new WebForm_PostBackOptions(btnEditUniqueID, "", true, "", "", false, false);
                    __doPostBack(options.eventTarget, options.eventArgument);
                }
            },
            {
                xtype: 'tbseparator', height: 27
            },
            {
                id: 'btnCopy',
                text: '&nbsp;@{R=Core.Strings; K=WEBJS_PS0_3; E=js}',
                iconCls: 'btnCopy',
                xtype: 'button',
                iconAlign: 'left',
                handler: function () {
                    var options = new WebForm_PostBackOptions(btnCopyUniqueID, "", true, "", "", false, false);
                    __doPostBack(options.eventTarget, options.eventArgument);
                }
            },
            {
                xtype: 'tbseparator', height: 27
            },
            {
                id: 'btnDelete',
                text: '&nbsp;@{R=Core.Strings; K=WEBJS_PS0_4; E=js}',
                iconCls: 'btnDelete',
                xtype: 'button',
                iconAlign: 'left',
                handler: function () {
                    checkForNavigationUssage(function(result) {
                        var options = new WebForm_PostBackOptions(btnDeleteUniqueID, "", true, "", "", false, false);
                        __doPostBack(options.eventTarget, options.eventArgument);
                    });
                }
            }]
    });

    toolbar.render("menuToolbar");
};