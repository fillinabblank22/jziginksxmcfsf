Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

SW.Orion.Dependencies = function () {
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    var comboArray;

    function initAutoDependencySetting() {
        if ($("#isOrionDemoMode").length !== 0) return;
        var $toggle = $('.sw-auto-dependency-toggle');
        var onText = '@{R=Core.Strings;K=WEBJS_TM0_144;E=js}';
        var offText = '@{R=Core.Strings;K=LogLevel_off;E=js}';

        ORION.callWebService("/Orion/Services/DependenciesTree.asmx",
            "GetAutoDependencySetting", {},
            function(result) {
                if (ErrorHandler(result)) return;
                $toggle
                    .addClass(result ? 'toggleOn' : 'toggleOff')
                    .text(result ? onText : offText);
                $('.sw-auto-dependency-setting').show();
            });

        $toggle.click(function(e) {
            e.preventDefault();
            var enable = $toggle.hasClass('toggleOff');

            ORION.callWebService("/Orion/Services/DependenciesTree.asmx",
                "SetAutoDependencySetting", { enabled: enable },
                function(result) {
                    if (ErrorHandler(result)) return;
                    $toggle
                        .toggleClass('toggleOn toggleOff')
                        .text(enable ? onText : offText);
                });
        });
    }

    function ignoreAutoDependency() {
        var selectedItems = grid.getSelectionModel().getSelections();
        var toIgnore = getSelectedDependencyIds(selectedItems);

        Ext.Msg.confirm("@{R=Core.Strings;K=Dependencies_Ignore; E=js}", "@{R=Core.Strings;K=Dependencies_ConfirmIgnore; E=js}",
            function (btn) {
                if (btn == 'yes') {
                    var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_VB0_80; E=js}");

                    ORION.callWebService("/Orion/Services/DependenciesTree.asmx",
                        "IgnoreDependencies", { ids: toIgnore },
                        function (result) {
                            waitMsg.hide();
                            ErrorHandler(result);

                            if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}") {
                                refreshGridObjects(0, pageSize, grid, "", "", "", "", function () { });
                            } else {
                                refreshGridObjects(0, pageSize, grid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, comboArray.store.find('Type', comboArray.getValue()), groupGrid.getSelectionModel().getSelected().data.Value, "", function () { });
                            }
                        });
                }
            });
    }

    function renderOriginType(value, meta, record) {
        if (value == 0) {
            return "@{R=Core.Strings;K=Dependencies_UserDefined; E=js}";
        } else {
            return "@{R=Core.Strings;K=Dependencies_Calculated; E=js}";
        }
    }

    function renderDateTime(str, meta, record) {
        if (!str)
            return 'never';
        a = reMsAjax.exec(str);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.format(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.format(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }
        return '@{R=Core.Strings;K=WEBJS_AK0_68; E=js}';
    }

    function DateTimeFromJson(str) {
        if (!str)
            return null;
        a = reMsAjax.exec(str);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.format(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortTimePattern);
        }
        return null;
    }

    function renderStatus(status, meta, record) {
        if (record == null)
            return GetLocalizedObjectStatusText($.trim(status.toString())).toLowerCase();
        return String.format('<img src="/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small"/>&nbsp;{2}', "Orion.Nodes", status, GetLocalizedObjectStatusText($.trim(status.toString())));
    }

    var NodeGrDoQuery = function (query, succeeded) {
        Information.Query(query, function (result) {
            var table = [];
            for (var y = 0; y < result.Rows.length; ++y) {
                var row = result.Rows[y];
                var tableRow = {};
                for (var x = 0; x < result.Columns.length; ++x) {
                    tableRow[result.Columns[x]] = row[x];
                }
                table.push(tableRow);
            }
            succeeded({ Rows: table });
        }, function (error) {
            if (error.get_message() == "@{R=Core.Strings;K=WEBJS_VB0_67; E=js}") {
                alert('@{R=Core.Strings;K=WEBJS_AK0_26; E=js}');
                window.location.reload();
            }
        });
    };

    function canIgnoreItems(items) {
        for (var index = 0; index < items.length; index++) {
            if (!items[index].data.AutoManaged) return false;
        }

        return items.length > 0;
    }

    function canDeleteItems(items) {
        for (var index = 0; index < items.length; index++) {
            if (items[index].data.AutoManaged) return false;
        }

        return items.length > 0;
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var selectedItems = grid.getSelectionModel().getSelections();
        var canDelete = canDeleteItems(selectedItems);
        var canIgnore = canIgnoreItems(selectedItems);

        map.Edit.setDisabled(selCount != 1);
        map.Alerts.setDisabled(selCount != 1);
        map.Ignore.setDisabled(!canIgnore);
        map.Delete.setDisabled(!canDelete);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        }
        else {
            hd.removeClass('x-grid3-hd-checker-on');
        }

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.Add.setDisabled(true);
            map.Alerts.setDisabled(true);
            map.Ignore.setDisabled(true);
            map.Delete.setDisabled(true);
        }

    };

    var getSelectedDependencyIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.DependencyID);
        });

        return ids;
    };

    var deleteSelectedItems = function (items, onSuccess) {
        var toDelete = getSelectedDependencyIds(items);
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_VB0_80; E=js}");

        deleteDependencies(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            onSuccess(result);
        });
    };

    var deleteDependencies = function (ids, onSuccess) {
        ORION.callWebService("/Orion/Services/DependenciesTree.asmx",
                             "RemoveDependencies", { ids: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    function renderDependencyName(value, meta, record) {
        var entity = (record.data.ParentEntity == "Orion.Container") ? "Orion.Groups" : record.data.ParentEntity;
        return String.format('<span class="NodeName"><img src="{0}"/>&nbsp<span style="vertical-align: top;">{1}</span></a>',
            "/Orion/images/dependency_16x16.gif",
            Ext.util.Format.htmlEncode(value));
    }

    function renderParentObjectName(value, meta, record) {
        var entity = (record.data.ParentEntity == "Orion.Container") ? "Orion.Groups" : record.data.ParentEntity;
        var href = "javascript:void(0)";
        var image = String.format("/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small", entity, record.data.ParentStatus);
        if ((record.data.ParentDetailsUrl == null || record.data.ParentDetailsUrl.length == 0) && record.data.ParentEntity == "Orion.NPM.Interfaces") {
            //Temporary fix for Intrfaces due to DetailsUrl is empty for it
            var id = '0';
            var uriParts = record.data.ParentUri.split("/");
            if (uriParts.length > 0) {
                var idParts = uriParts[uriParts.length - 1].split("=");
                if (idParts.length == 2) {
                    id = idParts[1];
                }
            }

            href = String.format('/Orion/NPM/InterfaceDetails.aspx?NetObject=I:{0}&view=InterfaceDetails', id);
        }
        else if (record.data.ParentEntity == null || record.data.ParentEntity.length == 0) {
            href = "#";
            value = "@{R=Core.Strings;K=WEBJS_VB0_81; E=js}";
            image = "/Orion/images/warning_16x16.gif";
        }
        else {
            href = record.data.ParentDetailsUrl;
        }

        return String.format('<a href="{4}" class="NodeName" value="{0}{1}"><img src="{2}"/>&nbsp<span style="vertical-align: top;">{3}</span></a>',
            record.data.ParentNetObjectType,
            record.data.ParentNetObjectID,
            image,
            Ext.util.Format.htmlEncode(value),
			href);
    }

    function renderChildObjectName(value, meta, record) {
        var entity = (record.data.ChildEntity == "Orion.Container") ? "Orion.Groups" : record.data.ChildEntity;
        var href = "javascript:void(0)";
        var image = String.format("/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small", entity, record.data.ChildStatus);
        if ((record.data.ChildDetailsUrl == null || record.data.ChildDetailsUrl.length == 0) && record.data.ChildEntity == "Orion.NPM.Interfaces") {
            //Temporary fix for Intrfaces due to DetailsUrl is empty for it
            var id = '0';
            var uriParts = record.data.ChildUri.split("/");
            if (uriParts.length > 0) {
                var idParts = uriParts[uriParts.length - 1].split("=");
                if (idParts.length == 2) {
                    id = idParts[1];
                }
            }

            href = String.format('/Orion/NPM/InterfaceDetails.aspx?NetObject=I:{0}&view=InterfaceDetails', id);
        }
        else if (record.data.ChildEntity == null || record.data.ChildEntity.length == 0) {
            href = "#";
            value = "@{R=Core.Strings;K=WEBJS_VB0_82; E=js}";
            image = "/Orion/images/warning_16x16.gif";
        }
        else {
            href = record.data.ChildDetailsUrl;
        }

        return String.format('<a href="{4}" class="NodeName" value="{0}{1}"><img src="{2}"/>&nbsp<span style="vertical-align: top;">{3}</span></a>',
            record.data.ChildNetObjectType,
            record.data.ChildNetObjectID,
            image,
            Ext.util.Format.htmlEncode(value),
			href);
    }

    function renderString(value, meta, record) {
        return String.format(Ext.util.Format.htmlEncode(value));
    }

    function renderAlertType(value, meta, record) {
    	if (value == 0)
    		return String.format(Ext.util.Format.htmlEncode('@{R=Core.Strings;K=OrionMessageLabel_AdvAlert_Short; E=js}'));
    	else
    		return String.format(Ext.util.Format.htmlEncode('@{R=Core.Strings;K=OrionMessageLabel_BasicAlert_Short; E=js}'));
    }

    function renderGroup(value, meta, record) {
        var disp;
        a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            disp = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) + " (" + record.data.Cnt + ")";
            value = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.SortableDateTimePattern);
        }
        else {
            disp = ($.trim(String((Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : value))
                    || "[Unknown]") + " (" + record.data.Cnt + ")";

            if (comboArray.getValue().indexOf('Origin') > -1) {
                disp = renderOriginType(value, "", "") + " (" + record.data.Cnt + ")";
            }
			
            if (comboArray.getValue().indexOf('Status') > -1) {
                disp = renderStatus(value, "", "") + " (" + record.data.Cnt + ")";
            }
        }
        if (value == null) {
            value = "null";
        }

        return String.format('<a href="javascript:void(0)" class="NodeGroupName" value="{0}">{1}</a>', value, disp);
    }

    function refreshObjects(elem, property, type, value, search, callback) {
        elem.store.removeAll();
        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: search };
        currentSearchTerm = search;
        elem.store.load({ callback: callback });
    };

    function renderObjectName(value, meta, record) {
        var entity = (record.data.MemberEntityType == "Orion.Container") ? "Orion.Groups" : record.data.MemberEntityType;
        return String.format('<img src="{0}"/>&nbsp<span style="vertical-align: top;">{1}</span>',
            String.format("/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small", entity, record.data.Status),
            Ext.util.Format.htmlEncode(value));
    }

    function renderObjectType(value, meta, record) {
        return String.format('{0} {1}',
            Ext.util.Format.htmlEncode(value),
            renderStatus(record.data.Status.toString(), null, null));
    }

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();
        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value != null ? value : "", search: search };
        currentSearchTerm = search;
        elem.store.load({ params: { start: start, limit: limit }, callback: callback });
    };

    //Error handler
    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({ title: result.Source, msg: result.Msg, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            return true;
        }
    }

    //Ext grid
    ORION.prefix = "Dependencies_";
    var selectorModel;
    var dataStore;
    var grid;
    var pageSize;

    return {
        init: function () {
            initAutoDependencySetting();

            pageSize = parseInt(ORION.Prefs.load('PageSize', '20'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSize
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            var showGroupObjects = function (groupName, groupUri) {
                var detailsGridStore = new Ext.data.JsonStore({
                    root: 'd',
                    totalProperty: 'd.Count',
                    fields: ['Name', 'Status', 'MemberUri', 'MemberEntityType', 'EntityName', 'EntityNamePlural', 'ContainerID', 'MemberAncestorDisplayNames', 'MemberAncestorDetailsUrls', 'MemberPrimaryID'],
                    proxy: new Ext.data.HttpProxy({
                        url: "/Orion/Services/DependenciesTree.asmx/GetContainerMembersByUri",
                        method: "POST",
                        failure: function (xhr, options) { ORION.handleError(xhr); }
                    })
                });

                var detailsGrid = new Ext.grid.GridPanel({
                    store: detailsGridStore,
                    cls: 'hide-header',
                    stripeRows: true,
                    columns: [
                        { width: 250, editable: false, sortable: false, dataIndex: 'Name', renderer: renderObjectName },
                        { width: 120, editable: false, sortable: false, dataIndex: 'EntityName', renderer: renderObjectType }
                    ],
                    selModel: new Ext.grid.RowSelectionModel(), layout: 'fit', autoScroll: 'true', loadMask: true, width: 383, height: 280
                });

                var win = new Ext.Window({ title: String.format('@{R=Core.Strings;K=WEBJS_VB0_72; E=js}', groupName), resizable: false, closable: true, closeAction: 'hide', width: 400, height: 350, plain: true, modal: true,
                    items: [detailsGrid],
                    buttons: [{
                        text: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}',
                        handler: function () { win.hide(); }
                    }]
                });

                win.show($("#DependenciesGrid"));
                detailsGrid.store.removeAll();
                detailsGrid.store.proxy.conn.jsonData = { uri: groupUri };
                detailsGrid.store.load();
            };

            var showAlertsOnChild = function (entityType, entityUri, winHeader) {
                var detailsGridStore = new ORION.WebServiceStore(
                                "/Orion/Services/AlertsAdmin.asmx/GetAlertsForSWISObject",
                                [
                                    { name: 'AlertName', mapping: 0 }, /*also #9*/
                                    { name: 'ActiveAlertType', mapping: 1 }
                                ],
                                "AlertName"
                                );

                var detailsGrid = new Ext.grid.GridPanel({
                    store: detailsGridStore,
                    stripeRows: true,
                    columns: [
                        { width: 30, editable: false, sortable: true, renderer: function () { return '<img src="/NetPerfMon/images/Event-19.gif"/>' } },
                        { header: '@{R=Core.Strings;K=WEBJS_VB0_83; E=js}', width: 278, editable: false, sortable: true, dataIndex: 'AlertName', renderer: renderString },
                        { header: '@{R=Core.Strings;K=WEBJS_VB0_84; E=js}', width: 120, editable: false, sortable: true, dataIndex: 'ActiveAlertType', renderer: renderAlertType }
                    ],
                    selModel: new Ext.grid.RowSelectionModel(), layout: 'fit', autoScroll: 'true', loadMask: true, width: 433, height: 228
                });

                var win = new Ext.Window({ title: winHeader + "<br/><br/><span style='font-weight:normal;'>@{R=Core.Strings;K=WEBJS_VB0_85; E=js}</span>", resizable: false, closable: true, closeAction: 'hide', width: 450, height: 'auto', plain: true, modal: true,
                    items: [detailsGrid],
                    buttons: [{
                        text: '@{R=Core.Strings;K=WEBJS_PCC_4; E=js}',
                        handler: function () { win.hide(); }
                    }]
                });

                win.show($("#DependenciesGrid"));
                refreshObjects(detailsGrid, entityUri, entityType, "", "");
            };

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: "/Orion/Services/DependenciesTree.asmx/GetDependencies",
                    method: "POST",
                    timeout: 120000,
                    success: function (response, options) {
                        if (typeof response.responseText !== 'undefined' && response.status == 200) {
                            var responseJson = JSON.parse(response.responseText);
                            var metadata = responseJson.d.Metadata || false;
                            if (metadata) {
                                if (metadata.ErrorHtml !== null && metadata.ErrorHtml.length > 0) {
                                    //some endpoints are not available
                                    $('#DependenciesGrid-ErrorMessage').html(metadata.ErrorHtml);
                                }
                            }
                        }
                    },
                    failure: function (xhr, options) {
                        SW.Core.Services.handleError(xhr);
                    }
                }),
                reader: new Ext.data.JsonReader({
                    totalProperty: "d.TotalRows",
                    root: "d.DataTable.Rows"
                },
                [
                    { name: 'DependencyID', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'ParentUri', mapping: 3 },
                    { name: 'ChildUri', mapping: 4 },
                    { name: 'ParentStatus', mapping: 5 },
                    { name: 'ParentEntity', mapping: 6 },
                    { name: 'ParentDisplayName', mapping: 7 },
                    { name: 'ChildStatus', mapping: 8 },
                    { name: 'ChildEntity', mapping: 9 },
                    { name: 'ChildDisplayName', mapping: 10 },
                    { name: 'ParentDetailsUrl', mapping: 11 },
                    { name: 'ChildDetailsUrl', mapping: 12 },
					{ name: 'AutoManaged', mapping: 13 }
                ]
            ),
                remoteSort: true,
                sortInfo: {
                    field: "Name",
                    direction: "ASC"
                }
            });

            var groupingDataStore = new ORION.WebServiceStore(
                                "/Orion/Services/DependenciesTree.asmx/GetDependencyGroupingValues",
                                [
                                    { name: 'Name', mapping: 1 },
                                    { name: 'Value', mapping: 0 },
                                    { name: 'Type', mapping: 2 }
                                ]);

            var groupingStore = new ORION.WebServiceStore(
                                "/Orion/Services/DependenciesTree.asmx/GetDependencyGroupValues",
                                [
                                    { name: 'Value', mapping: 0 },
                                    { name: 'Cnt', mapping: 1 }
                                ],
                                "Vendor", "");

            var pagingToolbar = new Ext.PagingToolbar(
                { store: dataStore, pageSize: pageSize/*20*/, displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_VB0_86; E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VB0_87; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}"
                }
            );

            comboArray = new Ext.form.ComboBox(
                { fieldLabel: 'Name', hiddenName: 'Value', store: groupingDataStore, displayField: 'Name', triggerAction: 'all', value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}', typeAhead: true, mode: 'local', forceSelection: true, selectOnFocus: false }
            );

            grid = new Ext.grid.GridPanel({
                region: 'center',
                store: dataStore,
                split: true,
                stripeRows: true,
                trackMouseOver: false,
                columns: [
                    selectorModel,
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'DependencyID' },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_88; E=js}', width: 250, sortable: true, dataIndex: 'Name', renderer: renderDependencyName },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_89; E=js}', width: 250, sortable: true, dataIndex: 'ParentDisplayName', renderer: renderParentObjectName },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_90; E=js}', width: 250, sortable: true, dataIndex: 'ChildDisplayName', renderer: renderChildObjectName },
                    { header: '@{R=Core.Strings;K=Dependencies_Origin; E=js}', width: 230, sortable: true, dataIndex: 'AutoManaged', renderer: renderOriginType }
                ],
                sm: selectorModel,
                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                width: 795,
                height: 500,
                tbar: [
                    { id: 'Add', text: '@{R=Core.Strings;K=WEBJS_VB0_131; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_VB0_131; E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', handler: function() { location.href = '/Orion/Admin/Dependencies/Add/Default.aspx' } }, '-',
                    { id: 'Edit', text: '@{R=Core.Strings;K=CommonButtonType_Edit; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_VB0_91; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler: function() {
                            var selection = grid.getSelectionModel().getSelections();
                            var selectedItem = selection[0]; // when editing we consider only first
                            if (selectedItem.data.AutoManaged) {
                                location.href = '/Orion/Admin/Dependencies/Add/Review.aspx?DependencyID=' + getSelectedDependencyIds(selection)[0];
                            } else {
                                location.href = '/Orion/Admin/Dependencies/Add/Default.aspx?DependencyID=' + getSelectedDependencyIds(selection)[0];
                            }
                        }
                    }, '-',
                    {
                        id: 'Alerts',
                        text: '@{R=Core.Strings;K=WEBJS_VB0_92; E=js}',
                        tooltip: '@{R=Core.Strings;K=WEBJS_VB0_92; E=js}',
                        icon: '/Orion/images/alerting_16x16.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            showAlertsOnChild(grid.getSelectionModel().getSelected().data.ChildEntity,
                                grid.getSelectionModel().getSelected().data.ChildUri,
                                '@{R=Core.Strings;K=WEBJS_VB0_93; E=js}' + Ext.util.Format.htmlEncode(grid.getSelectionModel().getSelected().data.ChildDisplayName));
                        }
                    },
                    '-',
                    { id: 'Ignore', text: '@{R=Core.Strings;K=Dependencies_Ignore; E=js}', tooltip: '@{R=Core.Strings;K=Dependencies_Ignore; E=js}', icon: '/Orion/Nodes/images/icons/ignore_16x16.png', cls: 'x-btn-text-icon', handler: ignoreAutoDependency },
                    '-',
                    {
                        id: 'Delete',
                        text: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}',
                        tooltip: '@{R=Core.Strings;K=WEBJS_VB0_94; E=js}',
                        icon: '/Orion/Nodes/images/icons/icon_delete.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            Ext.Msg.confirm("@{R=Core.Strings;K=WEBJS_VB0_95; E=js}", "@{R=Core.Strings;K=WEBJS_VB0_96; E=js}",
                                function(btn) {
                                    if (btn == 'yes') {
                                        deleteSelectedItems(grid.getSelectionModel().getSelections(), function() {
                                            if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}")
                                                refreshGridObjects(0, pageSize, grid, "", "", "", "", function() {});
                                            else
                                                refreshGridObjects(0, pageSize, grid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, comboArray.store.find('Type', comboArray.getValue()), groupGrid.getSelectionModel().getSelected().data.Value, "", function() {});
                                        });
                                    }
                                });
                        }
                    }
                ],
                bbar: pagingToolbar
            });

            var groupGrid = new Ext.grid.GridPanel({
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [
                    { width: 193, editable: false, sortable: false, dataIndex: 'Value', renderer: renderGroup }
                ],
                selModel: new Ext.grid.RowSelectionModel(),
                layout: 'fit', autoScroll: 'true', loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        a = reMsAjax.exec(val);
                        if (a) {
                            var b = a[1].split(/[-,.]/);
                            var dateVal = new Date(+b[0]);
                            val = dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.SortableDateTimePattern);
                        }
                        //refreshObjects(grid, "", "", "", "");
                        refreshGridObjects(0, pageSize, grid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, comboArray.store.data.items[comboArray.selectedIndex].data.Type, val, "", function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
            });

            var groupByTopPanel = new Ext.Panel({ region: 'centre', split: false, heigth: 50, collapsible: false, viewConfig: { forceFit: true }, items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), comboArray], cls: 'panel-no-border panel-bg-gradient' });
            var navPanel = new Ext.Panel({ region: 'west', split: true, anchor: '0 0', width: 240, heigth: 500, collapsible: false, viewConfig: { forceFit: true }, items: [groupByTopPanel, groupGrid], cls: 'panel-no-border' });

            navPanel.on("bodyresize", function () {
                groupGrid.setSize(navPanel.getSize().width, 450);
            });

            var mainGridPanel = new Ext.Panel({ region: 'center', split: true, width: 1260, height: 500, layout: 'border', collapsible: false, items: [navPanel, grid], cls: 'no-border' });

            comboArray.on('select', function () {
                var val = this.store.data.items[this.selectedIndex].data.Value;
                refreshObjects(groupGrid, val, "", "", "");
                if (val == '')
                    refreshGridObjects(0, pageSize, grid, "", "", "", "", function () {
                        $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                            this.tooltip = 'processed';
                            $.swtooltip(this);
                        });
                    });
            });

            refreshObjects(comboArray, "", "", "", "");
            mainGridPanel.render('DependenciesGrid');
            if (comboArray.getValue() == '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}')
                refreshGridObjects(0, pageSize, grid, "", "", "", "", function () {
                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                        this.tooltip = 'processed';
                        $.swtooltip(this);
                    });
                });
            else
                refreshGridObjects(0, pageSize, grid, comboArray.store.data.items[comboArray.selectedIndex].data.Value, comboArray.store.find('Type', comboArray.getValue()), groupGrid.getStore().getAt(groupGrid.getSelectionModel().getSelected()).get("Value"), "", function () {
                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                        this.tooltip = 'processed';
                        $.swtooltip(this);
                    });
                });

            updateToolbarButtons();
            grid.bottomToolbar.addPageSizer();
        }
    };
} ();

Ext.onReady(SW.Orion.Dependencies.init, SW.Orion.Dependencies);

function GetLocalizedObjectStatusText(statusValue) {
    return SW.Core.Status.Get(statusValue).ShortDescription;
}
