﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.namespace('Ext.ux.grid');

Ext.QuickTips.init();

SW.Orion.CustomPropertyInlineEditor = function () {
    var fm = Ext.form;
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;

    var objectType = 'Orion.Nodes';
    var groupingField = '';
    var groupingFieldType = "System.String";
    var groupingValue = '';

    var grid;
    var selObjTypeCombo;
    var showInColumnsCombo;
    var checkboxSelectionModel;
    var mainGridPanel;
    var searchField;
    var pageSizeBox;
    var userPageSize;
    var pagingToolbar;
    var filters;
    var needToUpdateGridModel = false;
    var isFirstStringColumn = false;
    var isEditDialogShown = false;
    var columnsMenuLoaded = false;
    var firstStringColumnName = '';
    var columnsContainer;
    var lastMainGridParams = null;
    var isSelectAllMode = false;

    var SelectAllParamArray = new Array();
    var ChangedValuesArray = new Array();
    var ColumnsConfigArray = new Array();
    var CpEditorsArray = new Array();
    // container for all available columns
    var AllColumnsArray = new Array();
    // container for shown items
    var ColumnsToShowArray = new Array();

    var default_entityType = 'Orion.Nodes';
    var default_CpColumns = new Array();
    var columnCaptionByColumnName = new Object();

    var cpColumType = {
        'Required': 'req',
        'NonRequired': 'non',
        'All': 'all'
        };
    
    // this store is for loading entities which supports custom properties 
    var selObjTypeStore = new ORION.WebServiceStore(
        "/Orion/Services/CPEManagement.asmx/GetCpAvailableEntities",
        [
            { name: 'Name', mapping: 3 },
            { name: 'Value', mapping: 1 }
        ]);
    // this store used to load entity group by properties
    var groupingDataStore = new ORION.WebServiceStore(
        "/Orion/Services/WebAdmin.asmx/GetFilteredObjectPropertiesForCpe",
        [
            { name: 'Name', mapping: 1 },
            { name: 'Value', mapping: 0 },
            { name: 'Type', mapping: 2 }
        ]);

    // this store used for loading entity grouping values
    var groupingStore = new ORION.WebServiceStore(
        "/Orion/Services/WebAdmin.asmx/GetObjectPropertyValuesCpe",
        [
            { name: 'Value', mapping: 1 },
            { name: 'Cnt', mapping: 2 },
            { name: 'DisplayName', mapping: 0}
        ],
        "Value", "");

    // this store used for loading data into the main grid
    var store = new ORION.WebServiceStore(
        "/Orion/Services/CPEManagement.asmx/GetEntityCustomPropertyTable"
        );

    var showInColumnsStore = new Ext.data.SimpleStore({
            fields: ['Name', 'Value'],
            data: [
                    ['@{R=Core.Strings;K=WEBJS_AY0_31;E=js}', cpColumType.Required],
                    ['@{R=Core.Strings;K=WEBJS_AY0_32;E=js}', cpColumType.NonRequired],
                    ['@{R=Core.Strings;K=WEBJS_AY0_33;E=js}', cpColumType.All]
                  ]
        });
    
    function selectAllTooltip(grid, pageSizeBox) {
        if (grid.store.data.items.length == checkboxSelectionModel.selections.items.length && grid.store.data.items.length != 0 && grid.store.data.items.length != pageSizeBox.ownerCt.store.totalLength) {
            var text = String.format("@{R=Core.Strings;K=WEBJS_VM0_2;E=js}", checkboxSelectionModel.selections.items.length);
            $('<div id="selectionTip" style="padding-left: 5px; background-color:#FFFFCC;">' + text + ' </div>').insertAfter(".x-panel-tbar");
            $("#selectionTip").append($("<span id='selectAllLink' style='text-decoration: underline;color:red;cursor:pointer;'>" + String.format("@{R=Core.Strings;K=WEBJS_VM0_1;E=js}", pageSizeBox.ownerCt.store.totalLength) + "</span>").click(function () {
                isSelectAllMode = true;
                $("#selectionTip").text("@{R=Core.Strings;K=WEBJS_VM0_4;E=js}").css("background-color", "white");
                $("#selectAllLink").remove();
            }));
            var gridHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(gridHeight + $("#selectionTip").height());
        } else {
            clearSelectAllTooltip();
        }
    }
    function clearSelectAllTooltip() {
        if ($("#selectionTip").length != 0) {
            var rowsContainerHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(rowsContainerHeight - $("#selectionTip").height());
            $("#selectionTip").remove();
            if (isSelectAllMode) {
                isSelectAllMode = false;
            }
        }
    }

    function formatBool(value, meta, record) {
        if (!value || value.length === 0 || value.toString() == '0' || value.toString() == 'false')
            return '@{R=Core.Strings;K=WEBJS_IB0_13;E=js}';
        return '@{R=Core.Strings;K=WEBJS_IB0_12;E=js}';
    }

    function formatFloatValue(value, meta, record) {
        if (Ext.isEmpty(value) || value.length === 0)
            return value;
        return formatString(String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator), meta, record);
    }

    function setFilterData(filterData) {
        var count = filterData.length, dateKeys = "before,after,on".split(',');
        var dateFilterFields = new Array();
        var dateFiltersData = new Array();
        for (var j = 0; j < count; j++) {
            var filter = filterData[j], filtersCount = grid.filters.filters.items.length;
            for (var k = 0; k < filtersCount; k++) {
                var gridFilter = grid.filters.filters.items[k];
                if (gridFilter.dataIndex == filter.field) {
                    if (filter.data.type == "numeric") {
                        switch (filter.data.comparison) {
                            case "lt":
                                gridFilter.menu.fields.lt.setValue(filter.data.value);
                                break;
                            case "gt":
                                gridFilter.menu.fields.gt.setValue(filter.data.value);
                                break;
                            default:
                                gridFilter.menu.fields.eq.setValue(filter.data.value);
                                break;
                        }
                    }
                    else if (filter.data.type == "date") {
                        // generate collection of date filters to set values later due to date set value sets only last filter
                        var index = $.inArray(filter.field, dateFilterFields);
                        if (index == -1) {
                            var fieldName = filter.field;
                            dateFilterFields.push(fieldName);
                            dateFiltersData.push({
                                filterCtl: gridFilter,
                                data: {}
                            });
                            index = dateFilterFields.length - 1;
                        }

                        switch (filter.data.comparison) {
                            case "lt":
                                dateFiltersData[index].data['before'] = new Date(filter.data.value);
                                break;
                            case "gt":
                                dateFiltersData[index].data['after'] = new Date(filter.data.value);
                                break;
                            default:
                                dateFiltersData[index].data['on'] = new Date(filter.data.value);
                                break;
                        }
                        continue;
                    }
                    else {
                        gridFilter.setValue(filter.data.value);
                    }
                    gridFilter.setActive(true, false);
                }
            }
        }
        // set date values once for before, after, on filters
        if (dateFiltersData.length > 0) {
            for (var n = 0; n < dateFiltersData.length; n++) {
                dateFiltersData[n].filterCtl.setValue(dateFiltersData[n].data);
                dateFiltersData[n].filterCtl.setActive(true, false);
            }
        }
        // cancel reloading grid
        grid.filters.deferredUpdate.cancel();
    }
    // overrige header Columns menu 
    Ext.override(Ext.grid.GridView, {
        beforeColMenuShow: function () {
            var colModel = this.cm,
                colCount = AllColumnsArray.length,
                colMenu = this.colMenu,
                i;

            if (!columnsMenuLoaded) {
                colMenu.removeAll();
                for (i = 0; i < colCount; i++) {
                    var column = colModel.lookup[AllColumnsArray[i].dataIndex];
                    if (!column || column.hideable !== false) {
                        colMenu.add(new Ext.menu.CheckItem({
                            text: AllColumnsArray[i].header,
                            itemId: 'col-' + AllColumnsArray[i].dataIndex,
                            checked: (!column) ? false : !column.hidden,
                            disabled: (!column) ? false : column.hideable === false,
                            hideOnClick: false
                        }));
                    }
                }
                var parentMenu = colMenu.parentMenu;
                colMenu.on('hide', function () {
                    if (needToUpdateGridModel) {
                        var filterData = grid.filters.getFilterData();
                        ORION.Prefs.save(objectType.replace(/\./gi, '_'), ColumnsToShowArray.join(','));
                        updateGridModel(function () { parentMenu.hide(); }, function () { setFilterData(filterData); });
                    }
                }, this);
                columnsMenuLoaded = true;
            }

        },
        handleHdMenuClickDefault: function (item) {
            var itemIndex = item.getItemId().substr(4); // substr(4) - this magic comes from original method from extjs
            var index = $.inArray(itemIndex, ColumnsToShowArray);
            if (index > -1) {
                if (item.checked) {
                    ColumnsToShowArray.remove(itemIndex);
                    needToUpdateGridModel = true;
                }
            } else if (!item.checked) {
                ColumnsToShowArray.push(itemIndex);
                needToUpdateGridModel = true;
            }
        }
    });

    function formatString(value, meta, record) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0) {
            return (meta.id == firstStringColumnName && !Ext.isEmpty(record.data.Status)) ?
                String.format('<img src="/Orion/StatusIcon.ashx?entity={3}&amp;status={1}&amp;size=small" style="vertical-align: middle; padding-top:1px;"/>&nbsp;<span style="vertical-align: middle;">{2}</span>', 0, $.trim(record.data.Status.toString()), Ext.util.Format.htmlEncode(value), objectType)
                : Ext.util.Format.htmlEncode(value);
        }

        var patternText = filterText;
        patternText = patternText.replace(/\%/g, "*"); // replace any %'s with a *
        patternText = patternText.replace(/\*{2,}/g, "*"); // replace multiple *'s with a single instance
        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' +
                (meta.id == firstStringColumnName && !Ext.isEmpty(record.data.Status) ?
                String.format('<img src="/Orion/StatusIcon.ashx?entity={3}&amp;status={1}&amp;size=small" style="vertical-align: middle; padding-top:1px;"/>&nbsp;<span style="vertical-align: middle;">{2}</span>', 0, $.trim(record.data.Status.toString()), Ext.util.Format.htmlEncode(value), objectType)
                : Ext.util.Format.htmlEncode(value)) +
                    '</span>';
        }
        patternText = patternText.replace(/\\/g, '\\\\'); // replace \ with \\
        patternText = patternText.replace(/\./g, '\\.'); // replace . with \.
        patternText = patternText.replace(/\*/g, '.*');  // replace * with .*

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value.toString(), pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = Ext.util.Format.htmlEncode(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return (meta.id == firstStringColumnName && !Ext.isEmpty(record.data.Status)) ?
            String.format('<img src="/Orion/StatusIcon.ashx?entity={3}&amp;status={1}&amp;size=small" style="vertical-align: middle; padding-top:1px;"/>&nbsp;<span style="vertical-align: middle;">{2}</span>', 0, $.trim(record.data.Status.toString()), fieldValue, objectType)
            : fieldValue;
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function formatDateTime(value, meta, record) {
        if (Ext.isEmpty(value) || value.length === 0)
            return '';

        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        var dateVal = new Date(value);
        if (dateVal && dateVal != 'Invalid Date') {
            return dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        return '';
    }

    function getFilterTextTemp(search) {
        var tmpfilterText;
        if (!search || search.length === 0) {
            tmpfilterText = "";
        }
        else {
            tmpfilterText = search.replace(/\*/g, "%");

            if (tmpfilterText.length > 0) {
                if (!tmpfilterText.match("^%"))
                    tmpfilterText = "%" + tmpfilterText;

                if (!tmpfilterText.match("%$"))
                    tmpfilterText = tmpfilterText + "%";
            }
        }
        return tmpfilterText;
    }

    function refreshMainGridObjects(property, type, value, search, limit, callback, parameters) {
        grid.getView().refresh();
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { property: property, type: type, value: Boolean.isInstanceOfType(value) ? value : value || "", search: getFilterTextTemp(search), extendedParams: "" };
        grid.store.load({ params: (parameters == null) ? parameters : { start: 0, limit: limit }, callback: callback });
    };

    function refreshObjects(elem, property, type, value, search, limit, callback) {
        elem.store.removeAll();
        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: getFilterTextTemp(search) };
        elem.store.load({ params: { start: 0, limit: limit }, callback: callback });
    };

    function getBoolEditorControl() {
        return new fm.ComboBox({
            store: new Ext.data.SimpleStore({
                id: 0,
                fields: ['name', 'value'],
                data: [['@{R=Core.Strings;K=WEBJS_IB0_12;E=js}', 1], ['@{R=Core.Strings;K=WEBJS_IB0_13;E=js}', 0]]
            }),
            editable: false,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'name',
            valueField: 'value',
            listClass: 'x-combo-list-small'
        });
    };

    function getBoolFilterControl() {
        return {
            type: 'boolean',
            yesText: '@{R=Core.Strings;K=WEBJS_SO0_44; E=js}',
            noText: '@{R=Core.Strings;K=WEBJS_SO0_45; E=js}'
        };
    };

    function getDateTimeEditorControl(mandatory, defaultVal) {
        return new Ext.ux.form.DateTime({
            xtype: 'xdatetime',
            fieldLabel: '',
            timeFormat: 'H:i:s',
            timeConfig: {
                altFormats: 'H:i:s',
                allowBlank: true
            },
            dateFormat: 'Y-m-d',
            dateConfig: {
                altFormats: 'Y-m-d|Y-n-d',
                allowBlank: true
            },
            mandatory: mandatory,
            defaultVal: defaultVal,
            validator: function (val) {
                if (Ext.isEmpty(val) && this.mandatory) return false;
                return this.validateValue(val);
            }
        });
    };

    function getDateTimeFilterControl() {
        return {
            type: 'date',
            beforeText: '@{R=Core.Strings;K=WEBJS_SO0_41; E=js}',
            afterText: '@{R=Core.Strings;K=WEBJS_SO0_42; E=js}',
            onText: '@{R=Core.Strings;K=WEBJS_SO0_43; E=js}'
        };
    };

    function isFloat(n) {
        var numberVal = Number(String(n).replace(Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator, "."));
        if (numberVal > 3.40282347E+38 || numberVal < -3.40282347E+38 || isNaN(numberVal))
            return false;

        return (Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator == '.') ? /^-?\d+(\.\d{0,7})?([eE][-+]?[0-9]+)?$/.test(n) : /^-?\d+(\,\d{0,7})?([eE][-+]?[0-9]+)?$/.test(n);
    };

    function isDouble(n) {
        var numberVal = Number(String(n).replace(Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator, "."));
        if (numberVal > 1.7976E+308 || numberVal < -1.7976E+308 || isNaN(numberVal))
            return false;

        return (Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator == '.') ? /^-?\d+(\.\d{0,15})?([eE][-+]?[0-9]+)?$/.test(n) : /^-?\d+(\,\d{0,15})?([eE][-+]?[0-9]+)?$/.test(n);
    };

    function getLatLongEditorControl(minValue, maxValue) {
        return new fm.NumberField({
            allowBlank: true,
            allowDecimals: true,
            allowNegative: true,
            maxValue: maxValue,
            minValue: minValue,
            mandatory: false,
            defaultVal: null,
            decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
            decimalPrecision: 15,
            validator: function (val) {
                var numbval = Number(String(val).replace(Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator, "."));
                return isDouble(val) && numbval <= maxValue && numbval >= minValue;
            }
        });
    };

    function getFloatEditorControl(restrictedValues, mandatory, defaultVal) {
        if (restrictedValues.length > 0) {
            // to bind to ArrayStore properly
            for (var i = 0; i < restrictedValues.length; i++) {
                var strVal = String(restrictedValues[i]).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                restrictedValues[i] = [strVal];
            }
            return new fm.ComboBox({
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['name'],
                    data: restrictedValues
                }),
                triggerAction: 'all',
                mode: 'local',
                displayField: 'name',
                valueField: 'name',
                listClass: 'x-combo-list-small',
                editable: allowAdmin,
                mandatory: mandatory,
                defaultVal: defaultVal,
                updateList: function (val) {
                    if (Ext.isEmpty(val)) {
                        return false;
                    }
                    else {
                        for (var i = 0; i < restrictedValues.length; i++) {
                            if (restrictedValues[i][0] == val)
                                return true;
                        }
                        this.store.add(new this.store.recordType({
                            name: val
                        }));
                        restrictedValues.push([val]);
                    }
                },
                validator: function (val) {
                    if (Ext.isEmpty(val)) {
                        return !this.mandatory;
                    }
                    else {
                        var result = isFloat(val);
                        return result;
                    }
                },
                listeners: {
                    focus: {
                        fn: function () {
                            this.setValue(!Ext.isEmpty(this.value) ? String(this.value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : this.value);
                        }
                    }
                }
            });
        }
        else {
            return new fm.NumberField({
                allowBlank: !mandatory,
                allowDecimals: true,
                allowNegative: true,
                maxValue: 3.40282347E+38,
                minValue: -3.40282347E+38,
                mandatory: mandatory,
                defaultVal: defaultVal,
                decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                decimalPrecision: 7,
                validator: function (val) {
                    if (Ext.isEmpty(val)) {
                        return !this.mandatory;
                    }
                    else {
                        return isFloat(val);
                    }
                }
            });
        }
    };

    function getDoubleEditorControl(restrictedValues, mandatory, defaultVal) {
        if (restrictedValues.length > 0) {
            // to bind to ArrayStore properly
            for (var i = 0; i < restrictedValues.length; i++) {
                var strVal = String(restrictedValues[i]).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                restrictedValues[i] = [strVal];
            }
            return new fm.ComboBox({
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['name'],
                    data: restrictedValues
                }),
                triggerAction: 'all',
                mode: 'local',
                displayField: 'name',
                valueField: 'name',
                listClass: 'x-combo-list-small',
                editable: allowAdmin,
                mandatory: mandatory,
                defaultVal: defaultVal,
                updateList: function (val) {
                    if (Ext.isEmpty(val)) {
                        return false;
                    }
                    else {
                        for (var i = 0; i < restrictedValues.length; i++) {
                            if (restrictedValues[i][0] == val)
                                return true;
                        }
                        this.store.add(new this.store.recordType({
                            name: val
                        }));
                        restrictedValues.push([val]);
                    }
                },
                validator: function (val) {
                    if (Ext.isEmpty(val)) {
                        return !this.mandatory;
                    }
                    else {
                        var result = isFloat(val);
                        return result;
                    }
                },
                listeners: {
                    focus: {
                        fn: function () {
                            this.setValue(!Ext.isEmpty(this.value) ? String(this.value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : this.value);
                        }
                    }
                }
            });
        }
        else {
            return new fm.NumberField({
                allowBlank: !mandatory,
                allowDecimals: true,
                allowNegative: true,
                maxValue: 1.7976E+308,
                minValue: -1.7976E+308,
                mandatory: mandatory,
                defaultVal: defaultVal,
                decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                decimalPrecision: 15,
                validator: function (val) {
                    if (Ext.isEmpty(val)) {
                        return !this.mandatory;
                    }
                    else {
                        return isDouble(val);
                    }
                }
            });
        }
    };

    function getFloatFilterControl(restrictedValues) {
        if (restrictedValues.length > 0) {
            var stringArray = new Array();

            for (var i = 0; i < restrictedValues.length; i++) {
                var strVal = String(restrictedValues[i]).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                stringArray.push(strVal);
            }
            return {
                type: 'list',
                options: stringArray,
                getSerialArgs: function () {
                    var args = { type: 'list$system.single', value: this.getValue().join('$#') };
                    return args;
                }
            };
        }
        else {
            return {
                type: 'numeric',
                menuItemCfgs: {
                    emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                    decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                    allowDecimals: true,
                    allowNegative: true,
                    maxValue: 3.40282347E+38,
                    minValue: -3.40282347E+38,
                    decimalPrecision: 7
                }
            };
        }
    };

    function getDoubleFilterControl(restrictedValues) {
        if (restrictedValues.length > 0) {
            var stringArray = new Array();

            for (var i = 0; i < restrictedValues.length; i++) {
                var strVal = String(restrictedValues[i]).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                stringArray.push(strVal);
            }
            return {
                type: 'list',
                options: stringArray,
                getSerialArgs: function () {
                    var args = { type: 'list$system.double', value: this.getValue().join('$#') };
                    return args;
                }
            };
        }
        else {
            return {
                type: 'numeric',
                menuItemCfgs: {
                    emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                    decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                    allowDecimals: true,
                    allowNegative: true,
                    maxValue: 1.7976E+308,
                    minValue: -1.7976E+308,
                    decimalPrecision: 15
                }
            };
        }
    };

    function getIntegerEditorControl(restrictedValues, mandatory, defaultVal) {
        if (restrictedValues.length > 0) {
            for (var y = 0; y < restrictedValues.length; y++) {
                restrictedValues[y] = [restrictedValues[y]];
            }
            return new fm.ComboBox({
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['name'],
                    data: restrictedValues
                }),
                triggerAction: 'all',
                mode: 'local',
                displayField: 'name',
                valueField: 'name',
                editable: allowAdmin,
                mandatory: mandatory,
                defaultVal: defaultVal,
                listClass: 'x-combo-list-small',
                updateList: function (val) {
                    if (Ext.isEmpty(val)) {
                        return false;
                    }
                    else {
                        var val1 = val * 1;
                        for (var i = 0; i < restrictedValues.length; i++) {
                            if (restrictedValues[i][0] == val1)
                                return true;
                        }
                        this.store.add(new this.store.recordType({
                            name: val1.toString()
                        }));
                        restrictedValues.push([val1]);
                    }
                },
                validator: function (val) {
                    if (Ext.isEmpty(val)) {
                        return !this.mandatory;
                    }
                    else {
                        var result = /^-?\d+$/.test(val) && (val <= 2147483647 && val >= -2147483648);
                        return result;
                    }
                },
                getValue: function () {
                    if (!Ext.isEmpty(this.value)) {
                        var val1 = 1 * this.value;
                        return val1.toString();
                    }
                    return this.value;
                }
            });
        }
        else {
            return new fm.NumberField({
                allowBlank: !mandatory,
                allowDecimals: false,
                allowNegative: true,
                maxValue: 2147483647,
                minValue: -2147483648,
                mandatory: mandatory,
                defaultVal: defaultVal,
                validator: function (val) {
                    if (Ext.isEmpty(val)) {
                        return !this.mandatory;
                    }
                    else {
                        return /^-?\d+$/.test(val) && (val <= 2147483647 && val >= -2147483648);
                    }
                }
            });
        }
    }

    function getIntegerFilterControl(restrictedValues) {
        if (restrictedValues.length > 0) {
            var stringArray = new Array();
            for (var y = 0; y < restrictedValues.length; y++) {
                stringArray.push(restrictedValues[y].toString());
            }
            return {
                type: 'list',
                options: stringArray,
                getSerialArgs: function () {
                    var args = { type: 'list$system.int32', value: this.getValue().join('$#') };
                    return args;
                }
            };
        }
        else {
            return {
                type: 'numeric',
                menuItemCfgs: {
                    emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                    allowDecimals: false,
                    allowNegative: true,
                    maxValue: 2147483647,
                    minValue: -2147483648
                }
            };
        }
    }

    function getStringEditorControl(restrictedValuesArray, size, mandatory, defaultVal) {
        if (restrictedValuesArray.length > 0) {
            var restrictedValues = new Array();
            for (var j = 0; j < restrictedValuesArray.length; j++) {
                restrictedValues.push([Ext.util.Format.htmlEncode(restrictedValuesArray[j])]);
            }
            return new fm.ComboBox({
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['name'],
                    data: restrictedValues
                }),
                triggerAction: 'all',
                mode: 'local',
                displayField: 'name',
                valueField: 'name',
                typeAhead: true,
                editable: allowAdmin,
                mandatory: mandatory,
                defaultVal: defaultVal,
                maxLength: (size > 0) ? size : Number.MAX_VALUE,
                listClass: 'x-combo-list-small',
                updateList: function (val) {
                    if (Ext.isEmpty(val))
                        return false;

                    for (var i = 0; i < restrictedValues.length; i++) {
                        if (Ext.util.Format.htmlDecode(restrictedValues[i][0]) == val)
                            return true;
                    }
                    var val1 = Ext.util.Format.htmlEncode(val);
                    this.store.add(new this.store.recordType({
                        name: val1
                    }));
                    restrictedValues.push([val1]);
                    return true;
                },
                validator: function (val) {
                    if (this.mandatory && Ext.isEmpty(val.trim()))
                        return false;
                    return true;
                },
                listeners: {
                    select: function (comboCrtl) {
                        comboCrtl.setValue(Ext.util.Format.htmlDecode(comboCrtl.value));
                    }
                }
            });
        }
        else {
            return new fm.TextField({
                mandatory: mandatory,
                allowBlank: !mandatory,
                defaultVal: defaultVal,
                maxLength: (size > 0) ? size : Number.MAX_VALUE,
                validator: function (val) {
                    if (this.mandatory && Ext.isEmpty(val.trim()))
                        return false;
                    return true;
                }
            });
        }
    }

    function getStringFilterControl(restrictedValues, size) {
        if (restrictedValues.length > 0) {
            var stringArray = new Array();
            for (var i = 0; i < restrictedValues.length; i++) {
                stringArray.push(Ext.util.Format.htmlEncode(restrictedValues[i].toString()));
            }
            return {
                type: 'list',
                options: stringArray,
                getSerialArgs: function () {
                    var args = { type: 'list$system.string', value: Ext.util.Format.htmlDecode(this.getValue().join('$#')) };
                    return args;
                }
            };
        }
        else {
            return {
                type: 'string',
                emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                maxLength: (size > 0) ? size : Number.MAX_VALUE
            };
        }
    }

    ShowFilter = function (evt, name) {
        evt = (evt) ? evt : window.event;
        evt.cancelBubble = true;

        for (var i = 0; i < grid.filters.filters.items.length; i++) {
            if (grid.filters.filters.items[i].dataIndex == name) {
                var menu = grid.filters.filters.items[i].menu;
                var event = Ext.EventObject;
                menu.showAt(event.getXY());
            }
        }
        return false;
    };

    // returns column config with defined etitor and filter according to column type
    function getColumnConfig(system, editable, name, displayName, type, visible, restrictedValues, size, mandatory, defaultVal) {
        var editor = null;
        var filter;
        var renderMethod;
        switch (type.toString().toLowerCase()) {
            case "system.boolean":
                if (editable)
                    editor = getBoolEditorControl();
                filter = getBoolFilterControl();
                renderMethod = formatBool;
                break;
            case "system.datetime":
                if (editable)
                    editor = getDateTimeEditorControl(mandatory, defaultVal);
                filter = getDateTimeFilterControl();
                renderMethod = formatDateTime;
                break;
            case "system.single":
                if (editable) {
                    if (name == 'WorldMapPoint.Latitude')
                        editor = getLatLongEditorControl(-90, 90);
                    else if (name == 'WorldMapPoint.Longitude')
                        editor = getLatLongEditorControl(-180, 180);
                    else
                        editor = getFloatEditorControl(restrictedValues, mandatory, defaultVal);
                }

                filter = getFloatFilterControl(restrictedValues);
                renderMethod = formatFloatValue;
                break;
            case "system.double":
                if (editable) {
                        editor = getDoubleEditorControl(restrictedValues, mandatory, defaultVal);
                }

                filter = getDoubleFilterControl(restrictedValues);
                renderMethod = formatFloatValue;
                break;
            case "system.int32":
                if (editable)
                    editor = getIntegerEditorControl(restrictedValues, mandatory, defaultVal);
                filter = getIntegerFilterControl(restrictedValues);
                renderMethod = formatString;
                break;
            default:
                if (editable)
                    editor = getStringEditorControl(restrictedValues, size, mandatory, defaultVal);
                filter = getStringFilterControl(restrictedValues, size);
                renderMethod = formatString;
                if (!isFirstStringColumn && visible && system) {
                    isFirstStringColumn = true;
                    firstStringColumnName = name;
                }
                break;
        }

        return {
            id: name,
            header: String.format('<img class="filter-icon" onclick="ShowFilter(event, \'{1}\');" src="/Orion/js/extjs/resources/images/default/s.gif" alt="" style="vertical-align: middle; padding-top:1px; height: 16px; width:16px"/><span style="vertical-align: middle;">{0}</span>&nbsp;', (displayName == '') ? name : displayName, name),
            dataIndex: name,
            width: editable ? (type.toString().toLowerCase() == "system.datetime") ? 200 : 150 : 250,
            editable: editable,
            hidden: !visible,
            sortable: true,
            filterable: true,
            filter: filter,
            editor: editor,
            renderer: renderMethod
        };
    }

    function includeCpEditorIntoPopup(id, name, dispalyName, type, restrictedValues, description, size, mandatory, defaultVal) {
        var editor;
        switch (type.toString().toLowerCase()) {
            case "system.boolean":
                editor = getBoolEditorControl();
                editor.value = '0';
                break;
            case "system.datetime":
                editor = getDateTimeEditorControl(mandatory, defaultVal);
                break;
            case "system.single":
                editor = getFloatEditorControl(restrictedValues, mandatory, defaultVal);
                break;
            case "system.int32":
                editor = getIntegerEditorControl(restrictedValues, mandatory, defaultVal);
                break;
            default:
                editor = getStringEditorControl(restrictedValues, size, mandatory, defaultVal);
                break;
        }

        editor.width = 200;
        editor.disabled = true;
        editor.disabledClass = Ext.isIE ? 'x-item-disabled-ie' : 'x-item-disabled';
        editor.cpConfigData = { id: id, name: name, type: type, restricted: restrictedValues.length > 0 };

        var item = new Ext.form.Checkbox({
            xtype: 'checkbox',
            boxLabel: String.format("<b>{0}&nbsp;{1}</b>", name.indexOf("WorldMapPoint.") != -1 ? dispalyName + '&nbsp;<font color="#646464">@{R=Core.Strings;K=WEBJS_AY0_42; E=js}</font>' : name, mandatory ? '<font color="#646464">@{R=Core.Strings;K=WEBJS_SO0_92; E=js}</font>' : ''),
            name: name,
            inputValue: '0',
            listeners: {
                check: function (checkbox, checked) {
                    // wee need also enable/disable ownerCt due to FB195443
                    if (checked) { editor.enable(); editor.ownerCt.enable(); }
                    else { editor.disable(); editor.ownerCt.disable(); }
                }
            }
        });

        columnsContainer.items.add(item);
        columnsContainer.items.add(new Ext.Panel({
            layout: {
                type: 'table',
                columns: 2,
                tableAttrs: {
                    cls: 'sw-cp-multiedit-control-table'
                }
            },
            cls: 'sw-cp-multiedit-control-panel',
            disabled: true,
            items: [
                    editor,
                    new Ext.form.Label({ text: description, style: { 'color': '#5f5f5f', 'font-size': '10px', 'padding-right': '25px'} })
                ]
        }));
        CpEditorsArray.push(editor);
    }

    function updateGridModel(callback, afterReconfigureCallback) {
        var cm = new Array();
        var mapping = new Array();
        var columnsType = showInColumnsCombo.store.data.items[showInColumnsCombo.selectedIndex].data.Value;
        needToUpdateGridModel = false;
        // get system properties
        getEntityProperties(objectType, function (result) {
            if (result == null || result.d == null) return;

            // add checkboxes as first column
            checkboxSelectionModel.grid = grid;
            cm.push(checkboxSelectionModel);
            // clear columns configuration array
            ColumnsConfigArray = new Array();
            AllColumnsArray = new Array();
            var colNames = ORION.Prefs.load(objectType.replace(/\./gi, '_'), '');
            var index = mapping.length;
            for (var i = 0; i < result.d.Rows.length; i++) {
                var name = result.d.Rows[i][0]; // ColumnName
                var displayName = result.d.Rows[i][1]; //ColumnDisplayName
                var type = result.d.Rows[i][2]; //Type

                var isCustomField = !result.d.Rows[i][3]; // system or custom
                var editable = result.d.Rows[i][5]; // is column editable
                var visible = result.d.Rows[i][4];
                var mandatory = result.d.Rows[i][9];
                var defaultVal = result.d.Rows[i][10];

                var values = new Array();

                if (result.d.Rows[i][6] && result.d.Rows[i][6].length > 0) {
                    values = result.d.Rows[i][6].split("::");
                }
                var description = result.d.Rows[i][7];
                var size = result.d.Rows[i][8];
                var originVisible = visible;
                // hide custom property column if it isn't within the default list
                if (isCustomField && objectType == default_entityType && default_CpColumns.length > 0)
                    visible = $.inArray(name, default_CpColumns) > -1;
                 
                if(isCustomField &&  $('#ShowColumnsFilter').val() == "true") {
                    visible = isCustomFieldVisible(mandatory, columnsType);
                } 
                
                if (columnsMenuLoaded == true) { // if columns menu loaded check if column exists in list
                    var inColumnsArray = $.inArray(name, ColumnsToShowArray) > -1;
                    visible = inColumnsArray;
                }
                else if (!isCustomField && colNames != '') {//if system property and we have column list within the web settings
                    visible = $.inArray(name, colNames.split(',')) > -1;
                }

                // if column is visible but we hide it by unchecking column then just increase index to keep correct ordering
                if (originVisible && !visible) index++;

                // push column config into columns to show array
                if (visible && !inColumnsArray)
                    ColumnsToShowArray.push(name);

                if (mandatory)
                    displayName = displayName + '&nbsp;<font color="#646464">@{R=Core.Strings;K=WEBJS_SO0_92; E=js}</font>';

                if (name == "WorldMapPoint.Latitude")
                    displayName = '@{R=Core.Strings;K=FILEDATA_VS0_41; E=js}' + '&nbsp;<font color="#646464">@{R=Core.Strings;K=WEBJS_AY0_42; E=js}</font>';
                
                if (name == "WorldMapPoint.Longitude")
                    displayName = '@{R=Core.Strings;K=FILEDATA_VS0_43; E=js}' + '&nbsp;<font color="#646464">@{R=Core.Strings;K=WEBJS_AY0_42; E=js}</font>';

                if (visible || name == 'Uri' || name == 'Status' || name == 'DisplayName') {
                    cm.push(getColumnConfig(!isCustomField, editable, name, displayName, type, visible, values, size, mandatory, defaultVal));
                    mapping.push({ name: name, mapping: index });
                    index++;
                }

                columnCaptionByColumnName[name] = displayName;
                AllColumnsArray.push({ dataIndex: name, header: (displayName == '') ? name : displayName });

                if (isCustomField) {
                    ColumnsConfigArray.push({
                        id: i,
                        name: name,
                        type: type,
                        values: values,
                        description: description,
                        editable: editable,
                        size: size
                    });
                }
            }

            isFirstStringColumn = false;
            reconfigureGrid(cm, mapping, callback, afterReconfigureCallback);
        },
            function (error) {
                var err = eval("(" + error.responseText + ")");
                Ext.Msg.show({ title: error.statusText, msg: err.Message, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            });
    }

    function reconfigureFilterNotifications() {
        var filterData = grid.filters.getFilterData();
        var notificationTemplate = '\
        <div id="Hint" style="position: static;" runat="server"> \
            <div class="sw-filter"> \
                <table cellpadding="0" cellspacing="0"> \
                    <tr> \
                        <td><span class="sw-filter-icon sw-filter-icon-hint"></span></td> \
                        <td><span style="vertical-align: middle; padding-right: 10px">{0}</span></td> \
                        <td><span><a href="javascript:SW.Orion.CustomPropertyInlineEditor.RemoveFilter(\'{1}\',\'{2}\');" ><img id="deleteButton" src="/Orion/Nodes/images/icons/icon_delete.gif" style="cursor: pointer;" /></a></td> \
                    </tr> \
                </table> \
            </div> \
        </div>';

        $("#filterNotifications").each(function () {
            $(this).empty();
            for (var i = 0; i < filterData.length; i++) {
                var comparison = (filterData[i].data.comparison == undefined) ? "" : filterData[i].data.comparison;
                var notification = $(String.format(notificationTemplate, getFilterNotificationMessage(filterData[i]), filterData[i].field, comparison));
                $(this).append(notification);
            }
        });
    }

    function isCustomFieldVisible(mandatory, columnsType) {
        if(mandatory && columnsType == cpColumType.Required) {
           return true;
        }
        else if(!mandatory && columnsType == cpColumType.NonRequired) {
            return true;
        }
        return columnsType == cpColumType.All;
    }
    
    function formatDateValue(value) {
        if (Ext.isEmpty(value))
            return '';
        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
        }

        var b = new Date(value);
        if (b) {
            return b.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
        }
        return '';
    }

    function getFilterNotificationMessage(filter) {
        var displayName = columnCaptionByColumnName[filter.field] || filter.field;

        if (Ext.isEmpty(filter.data.value)) {
            return String.format('@{R=Core.Strings;K=WEBJS_RB0_2; E=js}', displayName);
        }
        if (filter.data.type == 'numeric') {
            var floatVal = String(filter.data.value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
            if (filter.data.comparison == 'lt')
                return String.format('@{R=Core.Strings;K=WEBJS_IB0_16; E=js}', displayName, floatVal);
            if (filter.data.comparison == 'gt')
                return String.format('@{R=Core.Strings;K=WEBJS_IB0_15; E=js}', displayName, floatVal);
            return String.format('@{R=Core.Strings;K=WEBJS_IB0_17; E=js}', displayName, floatVal);
        }
        if (filter.data.type == 'date') {
            var dateVal = formatDateValue(filter.data.value, false);
            if (filter.data.comparison == 'lt')
                return String.format('@{R=Core.Strings;K=WEBJS_IB0_18; E=js}', displayName, dateVal);
            if (filter.data.comparison == 'gt')
                return String.format('@{R=Core.Strings;K=WEBJS_IB0_19; E=js}', displayName, dateVal);
            return String.format('@{R=Core.Strings;K=WEBJS_IB0_20; E=js}', displayName, dateVal);
        }
        if (filter.data.type == 'boolean') {
            return String.format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, (filter.data.value ? '@{R=Core.Strings;K=Boolean_true; E=js}' : '@{R=Core.Strings;K=Boolean_false; E=js}'));
        }
        if (filter.data.type.toString().startsWith('list')) {
            return String.format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext.util.Format.htmlEncode(filter.data.value.split('$#').join("; ")));
        }

        return String.format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext.util.Format.htmlEncode(filter.data.value));
    }

    function reconfigureGrid(cmConfig, mapConfig, callback, afterReconfigureCallback) {
        var columnModel = new Ext.grid.ColumnModel(cmConfig);
        var defaultSortColumn = "";

        if (cmConfig.length > 1) {
            for (var i = 1; i < cmConfig.length; i++) {
                if (!cmConfig[i].hidden) {
                    defaultSortColumn = cmConfig[i].dataIndex;
                    break;
                }
            }
        }
        columnModel.defaultSortable = true;

        store = new ORION.WebServiceStore(
            "/Orion/Services/CPEManagement.asmx/GetEntityCustomPropertyTable",
            mapConfig,
            defaultSortColumn, "ASC"
        );
        grid.getView().refresh();
        grid.reconfigure(store, columnModel);

        if (afterReconfigureCallback)
            afterReconfigureCallback();

        searchField.store = store;
        filterTypeParam = objectType; // main grid store requares to have this param always defined

        pagingToolbar.bind(store);

        grid.store.on('beforeload', function (s, params) {
            lastMainGridParams = params.params;
            // we're using jsonData to access web method so all params a going via http GET which has limited length
            s.proxy.conn.jsonData.extendedParams = Ext.urlEncode(params.params);
            // we need to leave these thwo params there do don't brake paging
            if (params.params.sort && params.params.dir) {
                params.params = { start: params.params.start, limit: params.params.limit, sort: params.params.sort, dir: params.params.dir };
            }
            else {
                params.params = { start: params.params.start, limit: params.params.limit };
            }
            grid.getView().refresh();
            if (ChangedValuesArray.length > 0) {
                freeChangedValues(function () {
                    grid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                    groupingStore.reload();
                    // set up actual grid sorting
                    s.sortInfo.field = params.params.sort;
                    s.sortInfo.direction = params.params.dir;
                    // load with the latest params 
                    s.load(params);
                    checkboxSelectionModel.clearSelections();
                });
                return false;
            }
            reconfigureFilterNotifications();
            checkboxSelectionModel.clearSelections();
            grid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
        });
        grid.store.on('load', function () {
            grid.getEl().unmask();
            $("#selectionTip").remove();
            if (isSelectAllMode) {
                isSelectAllMode = false;
            }
        });
        var field = (groupingValue != '') ? groupingField + '$' + groupingFieldType : ''; // for this grid store we need to unite grouping field with its type
        refreshMainGridObjects(field, objectType, groupingValue, filterText, userPageSize, function () {
            if (callback != null)
                callback();
        }, lastMainGridParams);
    }

    function getEntityProperties(entity, success, failure) {
        var methodUrl = '/Orion/Services/CPEManagement.asmx/GetEntityColumns';
        $.ajax({
            type: 'Post',
            url: methodUrl,
            data: "{'entity':" + "'" + entity + "\'}",
            contentType: 'application/json; charset=utf-8',
            timeout: '12000000',
            success: function (result) {
                if (success != null) success(result);
            },
            error: function (error) {
                if (failure != null) failure(error);
            }
        });
    }

    function renderGroup(value, meta, record) {

        if (record.data.DisplayName !== "") {
            value = record.data.DisplayName;
        }

        var disp;
        a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            disp = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) + " (" + record.data.Cnt + ")";
            value = val;
        }
        else {
            disp = ($.trim(String((Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : 
				(Boolean.isInstanceOfType(record.data.Value) ? formatBool(record.data.Value) : value))) || '@{R=Core.Strings;K=WEBDATA_PS0_0;E=js}') + " (" + record.data.Cnt + ")";

            if (value == null) {
                value = "null";
            }
        }
        return String.format('<a href="javascript:void(0)" class="NodeGroupName" value="{0}">{1}</a>', value, Ext.util.Format.htmlEncode(disp));
    }

    function freeChangedValues(callback) {
        if (ChangedValuesArray.length > 0) {
            Ext.MessageBox.show({
                title: '@{R=Core.Strings;K=WEBJS_SO0_47; E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_SO0_48; E=js}',
                buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Save; E=js}', no: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                width: 350,
                fn: function (btn) {
                    if (btn == 'yes') {
                        assignCpValues(callback);
                    }
                    else {
                        ChangedValuesArray = new Array();
                        if (callback != null) {
                            callback();
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }
        else if (callback != null) {
            callback();
        }
    }

    function assignValuesSuccess(callback) {
        for (var i = 0; i < ChangedValuesArray.length; i++) {
            var columnConfig = grid.filters.filters.map[ChangedValuesArray[i].property];
            // if column not found then just skip this 
            if (columnConfig == null || columnConfig == undefined) continue;
            var filterMenu = columnConfig.menu;
            if (filterMenu.type == 'list') {
                var exists = false;
                for (var j = 0; j < filterMenu.items.items.length; j++) {
                    if (ChangedValuesArray[i].value == '' || Ext.util.Format.htmlDecode(filterMenu.items.items[j].itemId) == ChangedValuesArray[i].value) {
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    columnConfig.reconfigureFilters(ChangedValuesArray[i].value);
                }
            }
        }
        ChangedValuesArray = new Array();
        SelectAllParamArray = new Array();
        if (callback != null)
            callback();
    }

    function assignValuesError() {
        if (error.get_statusCode() == 401 || error.get_statusCode() == 403) {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
            window.location.reload();
        } else {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_SO0_49; E=js}', msg: error.get_message(), minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }

    function assignCpValues(callback) {
        if (ChangedValuesArray.length > 0) {
            if (!isSelectAllMode) {
                CPEManagement.AssignCustomPropertyValues(ChangedValuesArray, objectType, function () {
                    assignValuesSuccess(callback);
                }, function () { assignValuesError(); });
            } else {
                CPEManagement.AssignAllCustomPropertyValues(SelectAllParamArray, objectType, store.proxy.conn.jsonData.property, objectType, store.proxy.conn.jsonData.value, getFilterTextTemp(filterText), store.proxy.conn.jsonData.extendedParams, function () {
                    assignValuesSuccess(callback);
                }, function () { assignValuesError(); });
            }
        } else {
            ChangedValuesArray = new Array();
            if (callback != null) {
                callback();
            }
        }
    }

    function showMultiEditPopup() {
        //check if at least 2 items selected
        if (checkboxSelectionModel.getCount() < 2) {
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_IB0_21; E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_IB0_22; E=js}',
                buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                icon: Ext.Msg.WARNING
            });
            return;
        }
        // if no CPs defined for this Entity type then show message
        if (ColumnsConfigArray.length == 0) {
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_IB0_21; E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_SO0_50; E=js}',
                buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                icon: Ext.Msg.WARNING
            });
            return;
        }

        // save if there is something unsaved on the grid view
        freeChangedValues(function () {
            getEntityProperties(objectType, function (result) {
                if (result == null || result.d == null) return;
                // clear columns configuration array
                ColumnsConfigArray = new Array();
                for (var s = 0; s < result.d.Rows.length; s++) {
                    var values = new Array();

                    if (result.d.Rows[s][6] && result.d.Rows[s][6].length > 0) {
                        values = result.d.Rows[s][6].split("::");
                    }
                    var isCustomField = !result.d.Rows[s][3] || result.d.Rows[s][5]===true; // system or custom
                    if (isCustomField) {
                        ColumnsConfigArray.push({
                            id: s,
                            name: result.d.Rows[s][0],
                            displauName: result.d.Rows[s][1],
                            type: result.d.Rows[s][2],
                            values: values,
                            description: result.d.Rows[s][7],
                            editable: result.d.Rows[s][5], // does column editable 
                            size: result.d.Rows[s][8],
                            mandatory: result.d.Rows[s][9],
                            defaultVal: result.d.Rows[s][10]
                        });
                    }
                }

                // clear existing custom property items and editors array
                columnsContainer.removeAll();
                CpEditorsArray = new Array();

                var editableItemsAvailable = false;
                for (var i = 0; i < ColumnsConfigArray.length; i++) {
                    // show editors only for visible columns
                    for (var j = 0; j < grid.colModel.config.length; j++) {
                        if (!grid.colModel.config[j].hidden && grid.colModel.config[j].dataIndex == ColumnsConfigArray[i].name) {
                            editableItemsAvailable = (editableItemsAvailable | ColumnsConfigArray[i].editable);
                            includeCpEditorIntoPopup(ColumnsConfigArray[i].id, ColumnsConfigArray[i].name, ColumnsConfigArray[i].displauName, ColumnsConfigArray[i].type, ColumnsConfigArray[i].values, ColumnsConfigArray[i].description, ColumnsConfigArray[i].size, ColumnsConfigArray[i].mandatory, ColumnsConfigArray[i].defaultVal);
                        }
                    }
                }
                // render panel controls
                columnsContainer.doLayout();
                if (!editableItemsAvailable) {
                    Ext.Msg.show({
                        title: '@{R=Core.Strings;K=WEBJS_IB0_21; E=js}',
                        msg: String.format("@{R=Core.Strings;K=WEBJS_IB0_23; E=js}", selObjTypeCombo.store.data.items[selObjTypeCombo.selectedIndex].data.Name),
                        buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                        icon: Ext.Msg.WARNING
                    });
                    return;
                }

                var encodedTitle = $("<div/>").text('@{R=Core.Strings;K=WEBJS_IB0_21; E=js}').html();
                // fix for FB#18508
                if (!isEditDialogShown) {
                    isEditDialogShown = true;
                    $("#multiEditDialog").show().dialog({
                        width: 500, height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle, close: function () { isEditDialogShown = false; }, resizable: false
                    }).css('overflow', 'hidden');
                }
            });
        });
    }

    ORION.prefix = "CPIEditor_";

    var allowAdmin = false;
    var currentlyEditedField;
    var currentlyEditedValue;

    var updateChangedValuesArray = function (changedValue) {
        var index = -1;
        // check if modified property is already within the changed list
        if (ChangedValuesArray.length > 0) {
            for (var i = 0; i < ChangedValuesArray.length; i++) {
                if (ChangedValuesArray[i].uri == changedValue.uri && ChangedValuesArray[i].property == changedValue.property) {
                    index = i;
                    break;
                }
            }
        }

        if (index > -1) {
            ChangedValuesArray[index] = changedValue;
        }
        else {
            ChangedValuesArray.push(changedValue);
        }
    };

    function storeMultipleChanges(callback) {
        var selectedItems = checkboxSelectionModel.getSelections();
        for (var i = 0; i < CpEditorsArray.length; i++) {
            if (!CpEditorsArray[i].disabled) {
                var val;
                if (CpEditorsArray[i].cpConfigData.type == "System.DateTime") {
                    val = (CpEditorsArray[i].getValue() != '') ? CpEditorsArray[i].getValue().localeFormat('s') : '';
                }
                else if (jQuery.type(CpEditorsArray[i].value) == 'array') {
                    val = CpEditorsArray[i].value[0].toString();
                }
                else {
                    val = CpEditorsArray[i].getValue();
                    if (Ext.isEmpty(val)) val = '';
                }

                if (CpEditorsArray[i].mandatory && val == '') {
                    return;
                }

                // check if all inputs are correct
                if (!CpEditorsArray[i].isValid() && val != '') {
                    // if at least one value is invalid then skip storing
                    return;
                }

                // check if it isn't new value within the restricted list
                if (allowAdmin && val != '' && CpEditorsArray[i].cpConfigData.restricted) {
                    for (var k = 0; k < ColumnsConfigArray.length; k++) {
                        var exists = false;
                        if (ColumnsConfigArray[k].name == CpEditorsArray[i].cpConfigData.name) {
                            var editor = grid.colModel.getColumnById(CpEditorsArray[i].cpConfigData.name).editor;
                            var records = editor.store.getRange();
                            var editorVal = val;
                            if (CpEditorsArray[i].cpConfigData.type == "System.String") {
                                editorVal = Ext.util.Format.htmlEncode(val).replace(/&/gi, "&amp;");
                            }
                            for (var s = 0; s < records.length; s++) {
                                if (records[s].get('name') == editorVal) {
                                    exists = true;
                                    break;
                                }
                            }

                            // add new value also into grid column editor dropdown
                            if (!exists) {
                                editor.store.add(new editor.store.recordType({
                                    name: val
                                }));
                                ColumnsConfigArray[k].values.push(val);
                                break;
                            }
                        }
                    }
                }

                for (var j = 0; j < selectedItems.length; j++) {
                    ChangedValuesArray.push({
                        uri: selectedItems[j].data.Uri,
                        property: CpEditorsArray[i].cpConfigData.name,
                        value: val,
                        columnId: CpEditorsArray[i].cpConfigData.id + 1//+1 due to we also have checkbox column
                    });
                }
                SelectAllParamArray.push({
                    property: CpEditorsArray[i].cpConfigData.name,
                    value: val,
                    columnId: CpEditorsArray[i].cpConfigData.id + 1//+1 due to we also have checkbox column
                });
            }
        }
        assignCpValues(function () {
            CpEditorsArray = new Array();
            groupingStore.reload();
            store.reload();
            if (callback) callback();
        });
    }

    function comboSetter(comboBox, value) {
        var store = comboBox.store;
        var valueField = comboBox.valueField;
        var displayField = comboBox.displayField;

        var recordNumber = store.findExact(valueField, value, 0);
        if (recordNumber == -1)
            return -1;

        var displayValue = store.getAt(recordNumber).data[displayField];
        comboBox.setValue(value);
        comboBox.setRawValue(displayValue);
        comboBox.selectedIndex = recordNumber;
        return recordNumber;
    }

    return {
        init: function () {
            $(".sw-pg-dialogsave-btn").click(function () {
                storeMultipleChanges(function () { $("#multiEditDialog").dialog('close'); });
            });

            $(".sw-pg-dialogcancel-btn").click(function () {
                $("#multiEditDialog").dialog('close');
            });

            $('#Current_EntityName').each(function (index, item) { default_entityType = objectType = item.value; });
            $('#Current_ShowColumns').each(function () {
                if (this.value != '') {
                    default_CpColumns = this.value.split(',');
                }
                else {
                    default_CpColumns = new Array();
                }
            });

            userPageSize = parseInt(ORION.Prefs.load('PageSize', '100'));
            allowAdmin = (ORION.Prefs.load('AllowAdmin', 'false')).toLowerCase() == 'true';

            checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel(
	        {
	            singleSelect: false,
	            sortable: false,
	            checkOnly: false
	        });
            checkboxSelectionModel.on("rowselect", function () {
                selectAllTooltip(grid, pageSizeBox);
            });
            checkboxSelectionModel.on("rowdeselect", function (rowIndex, record) {
                clearSelectAllTooltip();
            });

            var cm = new Ext.grid.ColumnModel([{
                id: 'common',
                header: '',
                dataIndex: 'common',
                width: 220,
                hidden: true,
                editor: new fm.TextField({
                    allowBlank: false
                })
            }]);

            //by default columns are sortable
            cm.defaultSortable = true;

            selObjTypeCombo = new Ext.form.ComboBox({
                valueField: 'Value',
                displayField: 'Name',
                store: selObjTypeStore,
                triggerAction: 'all',
                typeAhead: true,
                mode: 'local',
                forceSelection: true,
                selectOnFocus: false
            });
            
            showInColumnsCombo = new Ext.form.ComboBox({
                valueField: 'Value',
                displayField: 'Name',
                store: showInColumnsStore,
                triggerAction: 'all',
                typeAhead: true,
                mode: 'local',
                forceSelection: true,
                selectOnFocus: false,
                width: 220,
                hidden: $('#ShowColumnsFilter').val() == "true" ? false : true
            });

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 500,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 500) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                            }
                            else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 500) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;
                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                        }
                    }
                }
            });

            searchField = new Ext.ux.form.SearchField({ store: store, width: 200 });

            pagingToolbar = new Ext.PagingToolbar(
                { store: store,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_AK0_54; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox,
                        '->',
                        { xtype: 'tbspacer', width: 9 }
                    ]
                }
            );

            filters = new Ext.ux.grid.GridFilters({
                menuFilterText: '@{R=Core.Strings;K=WEBJS_SO0_40; E=js}',
                encode: false,
                local: false
            });

            grid = new Ext.grid.EditorPasteCopyGridPanel({
                region: 'center',
                store: store,
                baseParams: { start: 0, limit: userPageSize },
                columnLines: true,
                selModel: checkboxSelectionModel,
                cm: cm,
                frame: true,
                bodyStyle: 'padding-right: 9px',
                clicksToEdit: 1,
                height: 400,
                plugins: [filters],
                listeners: {
                    render: function () {
                        // You can also use 'keys' grid conf option to add key map 
                        return new Ext.KeyMap(this.el, {
                            key: Ext.EventObject.DELETE,
                            handler: function () {
                                this.clearSelected(this.getSelectionModel().getSelectedCellRange());
                            },
                            scope: this
                        });
                    },
                    validateedit: function (e) {
                        var column = grid.colModel.getColumnById(e.field);
                        e.cellType = column.filter.type;

                        if (Ext.isEmpty(e.value) && column.editor.mandatory) {
                            e.cancel = true;
                        } else if (!column.editable || (!Ext.isEmpty(e.value) && column.editor.validator && (e.cellType == 'numeric' ? !column.editor.validator(String(e.value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator)) : !column.editor.validator(e.value)))) {
                            e.cancel = true;
                        }
                        else if (e.field == currentlyEditedField && (currentlyEditedValue == e.value || (Ext.isEmpty(currentlyEditedValue) && Ext.isEmpty(e.value)) || (e.cellType == 'date' && (formatDateTime(currentlyEditedValue) == formatDateTime(e.value) || (!Ext.isEmpty(e.value) && Ext.isEmpty(formatDateTime(e.value))))))) {
                            e.cancel = true;
                        }
						else if (!allowAdmin && e.cellType == 'list' && column.editor.store.data.items.length > 0) {
							e.cancel = true;
							for (var i = 0; i < column.editor.store.data.items.length; i++) {
								if (e.value === column.editor.store.data.items[i].id)
								{
									e.cancel = false;
									break;
								}
							}
                        }
                        else if (column.editor.maxLength && e.value.length > column.editor.maxLength) {
                            e.cancel = true;
                        }

                        if (!e.cancel && e.cellType == 'list' && column.editor.updateList) {
                            column.editor.updateList(e.value);
                        }
                    },
                    afteredit: function (changed) {
                        var changedValue = {
                            uri: changed.record.store.data.items[changed.row].data['Uri'],
                            property: changed.field,
                            value: (jQuery.type(changed.value) == 'date') ? changed.value.localeFormat('s') : changed.value,
                            columnId: changed.column
                        };

                        if (changed.cellType == 'boolean') {
                            changedValue.value = !!changedValue.value;
                        }

                        updateChangedValuesArray(changedValue);
                    },
                    beforeedit: function (e) {
                        /// the Fires before cell editing is triggered. The edit event object has the following properties
                        //    * grid - This grid
                        //    * record - The record being edited
                        //    * field - The field name being edited
                        //    * value - The value for the field being edited.
                        //    * row - The grid row index
                        //    * column - The grid column index
                        //    * cancel - Set this to true to cancel the edit or return false from your handler.
                        currentlyEditedField = e.field;
                        currentlyEditedValue = e.value;
                    },
                    filterupdate: function () {
                        // when we change filter state we need to go to the first grid page and with no grouping
                        store.lastOptions = { params: { start: 0, limit: userPageSize} };
                        store.proxy.conn.jsonData = { property: "", type: objectType, value: "", search: getFilterTextTemp(filterText) };
                    }
                },
                clearSelected: function (rows) {
                    var row1 = rows[0], col1 = rows[1] - 1, row2 = rows[2], col2 = rows[3] - 1;
                    for (var r = row1; r <= row2; r++) {
                        for (var c = col1; c <= col2; c++) {
                            var editor = this.colModel.config[c + 1].editor;
                            if (this.colModel.config[c + 1].editable && !editor.mandatory) { // +1 due to added checkbox column from selection model
                                var val = this.store.getAt(r).get(this.colModel.config[c + 1].dataIndex);
                                var isBollean = (jQuery.type(val) == 'boolean');
                                if (!Ext.isEmpty(val) && !(isBollean && !val)) {
                                    var changedValue = {
                                        uri: this.store.data.items[r].data['Uri'],
                                        property: this.colModel.config[c + 1].dataIndex,
                                        value: (isBollean) ? 0 : '',
                                        columnId: c
                                    };
                                    updateChangedValuesArray(changedValue);
                                    this.store.getAt(r).set(this.colModel.config[c + 1].dataIndex, changedValue.value);
                                }
                            }
                        }
                    }
                },
                onHdMouseDown: function (e, t) {
                    if (t.className == 'x-grid3-hd-checker') {
                        e.stopEvent();
                        var hd = Ext.fly(t.parentNode);
                        var isChecked = hd.hasClass('x-grid3-hd-checker-on');
                        if (isChecked) {
                            hd.removeClass('x-grid3-hd-checker-on');
                            checkboxSelectionModel.clearSelections();
                        } else {
                            hd.addClass('x-grid3-hd-checker-on');
                            checkboxSelectionModel.selectAll();
                        }
                    }
                },
                tbar: [
                        { id: 'MultiEdit', text: '@{R=Core.Strings;K=WEBJS_IB0_21; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_IB0_21; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler: function () { showMultiEditPopup(); } }, '-',
                        { id: 'AddCostumProperty', text: '@{R=Core.Strings;K=WEBJS_SO0_13; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_SO0_13; E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', handler: function () { location.href = '/Orion/Admin/CPE/Add/Default.aspx'; } },
                        '->',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_AY0_34; E=js}', hidden: $('#ShowColumnsFilter').val() == "true" ? false : true}),
                        '&nbsp;',
                        showInColumnsCombo,
                        '&nbsp;',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_IB0_11;E=js}'}),
                        '&nbsp;',
                        selObjTypeCombo,
                        '&nbsp;&nbsp;',
                        searchField,
                        { xtype: 'tbspacer', width: 9 }
                    ],
                bbar: pagingToolbar
            });

            var groupGrid = new Ext.grid.GridPanel({
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [{
                    width: 190,
                    editable: false,
                    sortable: false,
                    dataIndex: 'Value',
                    renderer: renderGroup
                }],
                selModel: new Ext.grid.RowSelectionModel(),
                layout: 'fit',
                autoScroll: 'true',
                loadMask: false,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        groupingValue = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        refreshMainGridObjects(groupingField + '$' + groupingFieldType, // for this grid store we need to unite grouping field with its type
                                objectType, groupingValue, filterText, userPageSize);
                    }
                },
                anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
            });

            var comboArray = new Ext.form.ComboBox({
                fieldLabel: 'Name',
                hiddenName: 'Value',
                displayField: 'Name',
                store: groupingDataStore,
                triggerAction: 'all',
                value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                typeAhead: true,
                mode: 'local',
                forceSelection: true,
                selectOnFocus: false
            });

            var groupBy = new Ext.Panel({
                region: 'center',
                cls: 'panel-bg-gradient panel-no-border',
                split: false,
                viewConfig: { forceFit: true },
                collapsible: false,
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), comboArray]
            });

            var nav = new Ext.Panel({ region: 'west', split: true, width: 300, viewConfig: { forceFit: true }, collapsible: false, items: [groupBy, groupGrid] });
            mainGridPanel = new Ext.Panel({ region: 'center', split: true, height: 580, layout: 'border', collapsible: false, items: [nav, grid], cls: 'no-border' });

            nav.on('bodyresize', function () {
                groupGrid.setSize(nav.getSize().width, 530);
            });

            mainGridPanel.render('editor-grid');
            selObjTypeCombo.on('select', function () {
                var newObjType = this.store.data.items[this.selectedIndex].data.Value;
                // if nothing changed then no need to reload
                if (newObjType != objectType) {
                    objectType = newObjType;
                    groupingField = '';
                    groupingValue = '';
                    mainGridPanel.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                    refreshObjects(comboArray, "", objectType, "", "", userPageSize, function () {
                        comboArray.setValue('@{R=Core.Strings;K=WEBJS_VB0_76; E=js}');
                        groupingField = "";
                        refreshObjects(groupGrid, groupingField, objectType, "", filterText, userPageSize, function () {
                            mainGridPanel.getEl().unmask();
                            ColumnsToShowArray = new Array(); // swich to new object than we need to clear this array
                            columnsMenuLoaded = false;
                            lastMainGridParams = null;
                            updateGridModel();
                        });
                    });
                }
            });

            showInColumnsCombo.on('select', function () {
                updateGridModel();
            });
            
            comboArray.on('select', function () {
                var newGroupingField = this.store.data.items[comboArray.selectedIndex].data.Value;
                // if nothing changed then no need to reload
                if (newGroupingField != groupingField) {
                    groupingField = newGroupingField;
                    groupingValue = '';
                    groupingFieldType = this.store.data.items[comboArray.selectedIndex].data.Type;
                    mainGridPanel.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                    refreshObjects(groupGrid, groupingField, objectType, "", filterText, userPageSize, function () {
                        mainGridPanel.getEl().unmask();
                        refreshMainGridObjects("", objectType, "", filterText, userPageSize, function () { });
                    });
                }
            });

            Ext.fly(grid.getView().innerHd).on('mousedown', grid.onHdMouseDown, grid);

            //trigger the data store load
            mainGridPanel.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
            refreshObjects(selObjTypeCombo, "", "", "", "", userPageSize, function () {
                // we need to set objecttype within the combobox there after it's store already loaded
                comboSetter(selObjTypeCombo, objectType);
                comboSetter(showInColumnsCombo, cpColumType.Required);
                
                refreshObjects(comboArray, "", objectType, "", "", userPageSize, function () {
                    refreshObjects(groupGrid, groupingField, objectType, "", filterText, userPageSize, function () {
                        mainGridPanel.getEl().unmask();
                        updateGridModel();
                    });
                });
            });

            $(window).resize(function () {
                // resize grid
                mainGridPanel.doLayout();
            });

            columnsContainer = new Ext.Panel({
                id: 'columns-container',
                renderTo: 'selCustomPropertiesList',
                region: 'center',
                width: 430,
                autoScroll: true,
                cls: 'no-border-no-background',
                style: { 'height': 'auto', 'max-height': '350px', 'overflow': 'auto' }
            });

        },
        StoreChanges: function (callback) {
            assignCpValues(function () {
                groupingStore.reload();
                store.reload();
                if (callback != null)
                    callback();
            });
        },
        RemoveFilter: function (name, comparison) {
            for (var i = 0; i < grid.filters.filters.items.length; i++) {
                if (grid.filters.filters.items[i].active && grid.filters.filters.items[i].dataIndex == name) {
                    // for one numeric column we're able to define several filters (less then, greater then etc.)
                    // so we need to clear filter with exact the same comparison
                    switch (grid.filters.filters.items[i].type) {
                        case "numeric":
                            grid.filters.filters.items[i].disableFilter(comparison);
                            break;
                        case "date":
                            grid.filters.filters.items[i].disableFilter(comparison);
                            break;
                        case "list":
                            grid.filters.filters.items[i].disableFilter();
                            break;
                        case "string":
                            grid.filters.filters.items[i].disableFilter();
                            break;
                        default:
                            grid.filters.filters.items[i].active = false;
                            break;
                    }
                }
            }
            // after remove filter go to first page
            store.lastOptions.params.start = 0;
            // atrer filter changes we need to reload grid store
            store.reload();
        }
    };
} ();

Ext.onReady(SW.Orion.CustomPropertyInlineEditor.init, SW.Orion.CustomPropertyInlineEditor);
