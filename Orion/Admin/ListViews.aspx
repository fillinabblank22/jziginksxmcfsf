<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListViews.aspx.cs" 
    Inherits="Orion_Admin_ListViews" 
    MasterPageFile="~/Orion/Admin/CustomizeView.master" 
    Title="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_AK0_6 %>" %>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
    <link rel="stylesheet" href="../styles/ListViews.css" />
    <script type="text/javascript" src="js/ListViews.js"></script>
    <script type="text/javascript">
        Ext.onReady(function () {
            var listViews = new SW.Orion.ListViews();
            listViews.createAndRenderToolbar('<%= btnAdd.UniqueID %>', '<%= btnEdit.UniqueID %>', '<%= btnCopy.UniqueID %>', '<%= btnDelete.UniqueID %>', '<%= lbxViews.ClientID %>');
        });
    </script>

    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <div id="menuToolbar"></div>
    
    <table>
        <tr>
            <td>
                <asp:ListBox Width="600px" runat="server" ID="lbxViews" Rows="20" SelectionMode="single" DataTextField="ViewTitle" DataValueField="ViewID" />
            </td>
            <td valign="top">
                <asp:ImageButton runat="server" ID="btnEdit" Visible="False" ImageUrl="/Orion/images/Button.Edit.gif" OnClick="EditClick" />
                <br />
                <br />
                <asp:ImageButton runat="server" ID="btnAdd" Visible="False" ImageUrl="/Orion/images/Button.Add.gif" OnClick="AddClick" />
                <br />
                <asp:ImageButton runat="server" ID="btnCopy" Visible="False" ImageUrl="/Orion/images/Button.Copy.gif" OnClick="CopyClick" />
                <br />
                <asp:ImageButton runat="server" ID="btnDelete" Visible="False" ImageUrl="/Orion/images/Button.Delete.gif" OnClick="DeleteClick" />
            </td>
        </tr>
    </table>
    
    <script type="text/javascript">
        function AdjustControlProperties () {
            $('#menuToolbar').css({ width: '' + $(window).width() * 0.75 + 'px' });
            $('#<%= lbxViews.ClientID %>').css({ width: $('#menuToolbar').css("width") });
        }

        $(document).ready(function () {
            AdjustControlProperties();
            $(window).resize(function () {
                AdjustControlProperties();
            });
        });
    </script>
    
    <div id="statusMessage">
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="lbxViews" 
                                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_267 %>" Display="Static" EnableClientScript="false" />
        
        <asp:PlaceHolder runat="server" ID="phErrorMessage" />
    </div>
</asp:Content>
