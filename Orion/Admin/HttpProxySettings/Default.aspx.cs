using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using SolarWinds.Logging;
using Newtonsoft.Json;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using InformationServiceProxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy;

public partial class Orion_Admin_HttpProxySettings_Default : System.Web.UI.Page
{
    private const string CredentialOwnerTag = "Orion.HttpProxy";
    private const string UseDefaultUriString = "system://default";
    private const string DisallowUseUriString = "system://disabled";
    private const string PasswordGooid = "{gooid}";
    private const int NoCredentialSetValue = 0;

    private const string uriName = "Orion.HttpProxy.Uri";
    private const string credIdName = "Orion.HttpProxy.CredentialId";

    private readonly string textCredentialError = Resources.CoreWebContent.HttpProxySettings_DB_InvalidCredential;
    private readonly string textUriError = Resources.CoreWebContent.HttpProxySettings_DB_InvalidUri;
    private readonly string textInvalidTestUrl = Resources.CoreWebContent.HttpProxySettings_Test_InvalidUrl;
    private readonly string textProxyAuthRequired = Resources.CoreWebContent.HttpProxySettings_Test_CredentialRequired;
    private readonly string textRawHttpErrorDetail = Resources.CoreWebContent.HttpProxySettings_Test_HttpError;
    private readonly string textRawHttpSuccessDetail = Resources.CoreWebContent.HttpProxySettings_Test_HttpSuccess;
    private readonly string textWrongCredential = Resources.CoreWebContent.HttpProxySettings_Test_WrongCredential;

    // --

    private static Log log = new Log();

    public enum HttpProxyConfigMode
    {
        Default = 0,
        Disabled = 1,
        Specific = 2
    }

    public class HttpProxyConfigModelAuth
    {
        public bool Enabled { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class HttpProxyConfigTest
    {
        public string Url { get; set; }
        public string Fail { get; set; }
        public string Success { get; set; }
    }

    public class HttpProxyConfigModel
    {
        public HttpProxyConfigModelAuth Authentication { get; set; }
        public HttpProxyConfigTest Test { get; set; }

        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public HttpProxyConfigMode Mode { get; set; }
        public string Protocol { get; set; }
        public string Address { get; set; }
        public int Port { get; set;}

        public HttpProxyConfigModel()
        {
            Mode = HttpProxyConfigMode.Default;
        }
    }

    public class HttpProxySettingResult
    {
        public HttpProxyConfigModel Model { get; set; }
        public Dictionary<string,string> Errors { get; set; }
    }

    // --

    protected void Page_Load(object sender, EventArgs e)
    {
        var op = GetOp();

        if(op != null)
        {
            op(HttpContext.Current);
            return;
        }

        Page.Title = Resources.CoreWebContent.HttpProxySettings_Title;

    	var setting = GetGlobalSetting(true);

    	var emit = JsonConvert.SerializeObject(setting);

		var file = OrionInclude.CoreFile("Admin/HttpProxySettings/HttpProxySettings.js", OrionInclude.Section.Bottom);

        var returnUrl = ReferrerRedirectorBase.GetSafeReferrerUrlOrDefault("/Orion/Admin");

        var emit2 = JsonConvert.SerializeObject( new { returnUrl = returnUrl });

		file.AddJsInit("$(function(){ SW.Core.HttpProxyConfig.Init(" + emit + "," +  emit2 + "); });");
    }

    // --

    private Func<HttpContext,bool> GetOp()
    {
        string op = Request.QueryString["op"] ?? string.Empty;
        
        if (op.StartsWith("/")) 
            op = op.Substring(1);

        if (string.IsNullOrWhiteSpace(op) ||
            "POST".Equals(Request.HttpMethod, StringComparison.OrdinalIgnoreCase) == false)
            return null;

        if(string.Equals(op, nameof(Test), StringComparison.OrdinalIgnoreCase))
            return (context) => DoRequest<HttpProxyConfigModel>(context, Test);

        if(string.Equals(op, nameof(Save), StringComparison.OrdinalIgnoreCase))
            return (context) => DoRequest<HttpProxyConfigModel>(context, Save);

        return null;
    }

    private static bool DoRequest<T>(HttpContext _context, Func<T, object> body)
    {
        T p;

        using (var sr = new System.IO.StreamReader(_context.Request.InputStream))
        {
            string txt = sr.ReadToEnd();
            var o = JsonConvert.DeserializeObject(txt, typeof(T));
            p = o == null ? default(T) : (T)o;
        }

        var ret = body(p);
        var json = JsonConvert.SerializeObject(ret);

        _context.Response.ContentType = "application/json; charset=utf-8";
        _context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        _context.Response.Write("{\"d\":" + json + "}");
        _context.Response.End();
        return true;
    }

    // -- test

    private IWebProxy AsWebProxy(HttpProxyConfigModel model)
    {
        if (model.Mode == HttpProxyConfigMode.Disabled)
            return null;

        if (model.Mode == HttpProxyConfigMode.Default)
            return WebRequest.DefaultWebProxy ?? WebRequest.GetSystemWebProxy();

        var uri = new Uri( (model.Protocol ?? "http") + "://" + model.Address + ":" + model.Port );

        var current = SolarWinds.Orion.Core.Common.Configuration.HttpProxySettings.Instance;

        var ret = new WebProxy(uri, current.BypassOnLocal, current.BypassOnLocalExceptions);

        if (model.Authentication?.Enabled != true)
        {
            ret.UseDefaultCredentials = true;
            return ret;
        }

        try
        {
            string password = model.Authentication.Password;

            if( password == PasswordGooid)
            {
                var existing = GetGlobalSetting(false)?.Model;
                password = existing.Authentication?.Password;
            }

            var c = new NetworkCredential(model.Authentication.Username, password);
            ret.Credentials = c;
        }
        catch (Exception ex)
        {
            log.Error("unexpected error while fetching http proxy credential. Not supplying credential.", ex);
        }

        return ret;
    }

    public HttpProxyConfigTest Test(HttpProxyConfigModel model)
    {
        var ret = new HttpProxyConfigTest();

        var url = model?.Test?.Url;

        if( string.IsNullOrWhiteSpace(url) )
        {
            ret.Url = "https://www.solarwinds.com/";
        }
        else if(url.StartsWith("http://", StringComparison.OrdinalIgnoreCase) == false && 
            url.StartsWith("https://", StringComparison.OrdinalIgnoreCase) == false)
        {
            ret.Url = "http://" + url;
        }
        else
        {
            ret.Url = url;
        }

        try
        {
            var req = System.Net.WebRequest.Create(ret.Url);
            req.Proxy = AsWebProxy(model);
            req.Method = "HEAD";
            var res = req.GetResponse() as HttpWebResponse;
            var code = (int) res.StatusCode;
            var length = res.ContentLength;
            ret.Success = string.Format( textRawHttpSuccessDetail, code, length);
        }
        catch( System.UriFormatException )
        {
            ret.Fail = textInvalidTestUrl;
        }
        catch( WebException ex)
        {
            var exs = ex.InnerException as System.Net.Sockets.SocketException;

            if( exs != null )
            {
                log.Debug($"Socket error seen while testing http proxy against {ret.Url}.", exs);
                ret.Fail = exs.Message;
            }
            else
            {
                log.Debug($"Error seen while testing http proxy against {ret.Url}.", ex);

                var res = ex.Response as HttpWebResponse;
                if( res != null )
                {
                    var code = (int) res.StatusCode;

                    if( code == 407 )
                    {
                        ret.Fail = model.Authentication?.Enabled == true ? textWrongCredential : textProxyAuthRequired;
                    }
                    else
                    {
                        ret.Fail = string.Format(textRawHttpErrorDetail, code, res.StatusDescription ?? res.StatusCode.ToString() );
                    }
                }
                else
                {
                    ret.Fail = string.Format("Unexpected error fetching details on {0}. {1}", ret.Url, ex.Message );
                }
            }
        }

        return ret;
    }

    // -- load

    public HttpProxySettingResult GetGlobalSetting( bool obfuscate )
    {
        string swql = $"SELECT s.Name, s.Value FROM Orion.Setting s WHERE s.Name IN ('{uriName}','{credIdName}') ORDER by s.Name";

        var ret = new HttpProxySettingResult { Model = new HttpProxyConfigModel() };

        var creator = InformationServiceProxy.CreateV3Creator();

        using (var swisProxy = creator.Create())
        using (var table = swisProxy.Query(swql))
        {
            string uri = null;
            int? credId = null;

            foreach(var row in table.Rows.Cast<DataRow>())
            {
                string name = (string) row.ItemArray[0];

                if(string.Equals( name, uriName, StringComparison.OrdinalIgnoreCase ))
                {
                    uri = (string) row.ItemArray[1];
                    continue;
                }

                if( string.Equals( name, credIdName, StringComparison.OrdinalIgnoreCase ) == false)
                    continue;

                string v = (string) row.ItemArray[1];

                if(string.IsNullOrEmpty(v)) continue;

                credId = int.Parse( v );
            }

            uri = uri ?? UseDefaultUriString;


            if(string.Equals( uri, UseDefaultUriString )) 
                return ret;

            if(string.Equals( uri, DisallowUseUriString )) 
            {
                ret.Model.Mode = HttpProxyConfigMode.Disabled;
                return ret;
            }

            var model = ret.Model;

            try 
            {
                model.Mode = HttpProxyConfigMode.Specific;
                var parsed = new Uri(uri);
                model.Port = parsed.Port; // expect to use http/https, where this is set correctly if absent
                model.Address = parsed.Host;
                model.Protocol = parsed.Scheme.ToLowerInvariant();
            }
            catch(Exception)
            {
                ret.Errors = new Dictionary<string,string>();
                ret.Errors["uri"] = string.Format(textUriError, uri);
            }

            if(credId.HasValue && credId.Value != NoCredentialSetValue)
            {
                model.Authentication = new HttpProxyConfigModelAuth { Enabled = true };

                try
                {
                    var manager = new CredentialManager();
                    var cred = manager.GetCredential<UsernamePasswordCredential>(CredentialOwnerTag, credId.Value, false) as UsernamePasswordCredential;

                    model.Authentication.Username = cred?.Username;
                    model.Authentication.Password = cred?.Password;
                }
                catch(Exception ex)
                {
                    log.Error("Unexpected error handling previously saved credential", ex);
                }

                if( model.Authentication.Password == null)
                {
                    ret.Errors = new Dictionary<string,string>();
                    ret.Errors["credential"] = string.Format(textCredentialError, credId.Value);
                }
                else if( obfuscate)
                {
                    model.Authentication.Password = PasswordGooid;
                }
            }

            return ret;
        }
    }

    // -- save

    public string Save(HttpProxyConfigModel model)
    {
        // need to return something that translates to 'object'
        return SaveGlobalSetting(model).ToString();
    }

    public bool SaveGlobalSetting(HttpProxyConfigModel model)
    {
        if( model == null)
            return false;

        string uriSetting = null;
        string credIdSetting = NoCredentialSetValue.ToString(CultureInfo.InvariantCulture);
        bool ignoreCredId = false;

        if( model.Mode == HttpProxyConfigMode.Default) uriSetting = UseDefaultUriString;
        else if( model.Mode == HttpProxyConfigMode.Disabled) uriSetting = DisallowUseUriString;
        else uriSetting = (model.Protocol ?? "http") + "://" + model.Address + ":" + model.Port;

        if( model.Mode == HttpProxyConfigMode.Specific)
        {
            bool authEnabled = model.Authentication?.Enabled ?? false;

            if( authEnabled )
            {
                var existing = GetGlobalSetting(false)?.Model;

                // look for gooid. if so, upgrade pw.

                string password = string.Equals(model.Authentication?.Password, PasswordGooid) ?
                    existing.Authentication?.Password : 
                    model.Authentication.Password;

                bool resetCred = string.Equals(existing.Authentication?.Username, model.Authentication.Username) == false ||
                    string.Equals(existing.Authentication?.Password, password) == false;

                if(resetCred)
                {
                    var newCred = new UsernamePasswordCredential();
                    newCred.Username = model.Authentication.Username;
                    newCred.Password = password;
                    newCred.Name = "HTTP proxy credential";
                    newCred.Description = "HTTP proxy credential";
                    
                    var manager = new CredentialManager();
                    manager.AddCredential(CredentialOwnerTag, newCred);
                    credIdSetting = newCred.ID.Value.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    ignoreCredId = true;
                }
            }
        }

        // -- reset both settings

        var creator = InformationServiceProxy.CreateV3Creator();
        using (var swisProxy = creator.Create())
        {
            string swql = $"SELECT s.Name, s.Uri, s.Value FROM Orion.Setting s WHERE s.Name IN ('{uriName}','{credIdName}') ORDER by s.Name";

            using (var table = swisProxy.Query(swql))
            {
                string uri = null;
                string uriValueUri = null;
                string credId = null;
                string credIdValueUri = null;

                foreach(var row in table.Rows.Cast<DataRow>())
                {
                    string name = (string) row.ItemArray[0];

                    if(string.Equals( name, uriName, StringComparison.OrdinalIgnoreCase ))
                    {
                        uriValueUri = (string) row.ItemArray[1];
                        uri = (string) row.ItemArray[2];
                        continue;
                    }

                    if( string.Equals( name, credIdName, StringComparison.OrdinalIgnoreCase ))
                    {
                        credIdValueUri = (string) row.ItemArray[1];
                        credId = (string) row.ItemArray[2];
                    }
                }

                if( uriValueUri == null)
                {
                    log.Warn("Http proxy (Uri) setting is missing from manifest. Not Saving.");
                }
                else
                {
                    swisProxy.Update(uriValueUri, new Dictionary<string, object>{ {"Value", uriSetting }});
                }

                if( credIdValueUri == null)
                {
                    log.Warn("Http proxy (CredentialId) setting is missing from manifest. Not Saving.");
                }
                else if(ignoreCredId == false)
                {
                    swisProxy.Update(credIdValueUri, new Dictionary<string, object>{ {"Value", credIdSetting }});
                }
            }

            // remove old credentials no longer referenced.

            using( var table = swisProxy.Query( $@"
SELECT Value FROM Orion.SettingOverride Where Name = '{credIdName}'
UNION ( SELECT Value FROM Orion.Setting Where Name = '{credIdName}' )"))
            {
                HashSet<int> inUse = new HashSet<int>();

                foreach( var txtId in table.Rows.Cast<DataRow>().Select( x => x.ItemArray[0] ) )
                {
                    if( txtId == null || txtId == DBNull.Value )
                        continue;

                    int inUseCredId;

                    if( int.TryParse( (string) txtId, out inUseCredId ) )
                        inUse.Add(inUseCredId);
                }

                var manager = new CredentialManager();

                foreach( var foundCredId in manager.GetCredentialNames<UsernamePasswordCredential>(CredentialOwnerTag).Keys )
                {
                    if( inUse.Contains(foundCredId))
                        continue;

                    manager.DeleteCredential(CredentialOwnerTag, foundCredId);
                }
            }
        }

        return true;
    }
}
