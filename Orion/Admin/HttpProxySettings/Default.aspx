<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" CodeFile="Default.aspx.cs" Inherits="Orion_Admin_HttpProxySettings_Default" Title="Proxy Settings" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
	<orion:Include runat="server" File="Admin/HttpProxySettings/HttpProxySettings.css" />
	<%-- see default.aspx.cs <orion:Include runat="server" File="Admin/HttpProxySettings/HttpProxySettings.js" /> --%>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder" automation="HttpProxyConfig">

<div class="hpc-panel" hpc-scope="HttpProxyConfig" id="proxyPanel">

	<h1 automation="HttpProxyConfig_Title"><%: Resources.CoreWebContent.HttpProxySettings_Title %></h1>

	<div class="hpc-control-section" id="proxySection">
		<label automation="HttpProxyConfig_Proxy_Section_Label"><%: Resources.CoreWebContent.HttpProxySettings_GlobalHeader %></label>

		<div class="hpc-top-message">
			<%: Resources.CoreWebContent.HttpProxySettings_PageHint %>
		</div>
		
		<div id="ProxyEnabled" class="hpc-control-group">
			<label>&nbsp;</label>
			<div>
				<select id="hpcmode"
						autocomplete="off"
						automation="HttpProxyConfig_Mode"
						x-bind="Mode">
						<option value="Default"><%: Resources.CoreWebContent.HttpProxySettings_Mode_Default %></option>
						<option value="Disabled"><%: Resources.CoreWebContent.HttpProxySettings_Mode_Disabled %></option>
						<option value="Specific"><%: Resources.CoreWebContent.HttpProxySettings_Mode_Specific %></option>
				</select>
			</div>
		</div>

		<div id="ProxyProtocol" class="hpc-control-group hpc-cloak" x-cloak-parent>
			<label automation="HttpProxyConfig_Protocol_Label"><%: Resources.CoreWebContent.HttpProxySettings_ProtocolLabel %></label>
			<div>
				<select id="hpcprotocol"
						autocomplete="off"
						automation="HttpProxyConfig_Protocol"
						x-bind="Protocol"
						class="hpc-cloak">
						<option value="http"><%: Resources.CoreWebContent.HttpProxySettings_Protocol_Http %></option>
						<option value="https"><%: Resources.CoreWebContent.HttpProxySettings_Protocol_Https %></option>
				</select>
			</div>
		</div>

		<div id="ProxyAddress" class="hpc-control-group">
			<label automation="HttpProxyConfig_Address_Label"><%: Resources.CoreWebContent.HttpProxySettings_AddressLabel %></label>
			<div>
				<input id="hpcaddress"
					   type="text"
					   disabled
					   x-bind="Address"
					   automation="HttpProxyConfig_Address"
					   />
				<p automation="HttpProxyConfig_Address_Description"><%: Resources.CoreWebContent.HttpProxySettings_AddressDesc %></p>
				<p class="hpc-validation-message hpc-cloak" x-validation="Address" x-message="isEmpty" automation="HttpProxyConfig_Address_EmptyMessage">
					<%: Resources.CoreWebContent.HttpProxySettings_AddressValidation_IsEmpty %>
				</p>

				<div x-cloak-parent class="hpc-error hpc-cloak"><span x-bind="errors.uri" class="hpc-cloak"></span></div>
			</div>
		</div>

		<div id="ProxyPort" class="hpc-control-group">
			<label automation="HttpProxyConfig_Port_Label"><%: Resources.CoreWebContent.HttpProxySettings_PortLabel %></label>
			<div>
				<input id="httpport"
					   type="text"
					   disabled
					   x-bind="Port"
					   automation="HttpProxyConfig_Port"
					   />
				<p class="hpc-validation-message hpc-cloak" x-validation="Port" x-message="isEmpty" automation="HttpProxyConfig_Port_EmptyMessage">
					<%: Resources.CoreWebContent.HttpProxySettings_PortValidation_IsEmpty %>
				</p>
				<p class="hpc-validation-message hpc-cloak" x-validation="Port" x-message="isInvalid" automation="HttpProxyConfig_Port_InvalidMessage">
					<%: Resources.CoreWebContent.HttpProxySettings_PortValidation_IsInvalid %>
				</p>
			</div>
		</div>

		<div id="ProxyAuthenticationEnabled" class="hpc-control-group">
			<label>&nbsp;</label>
			<div>
				<p>
					<input id="hpcauthenticationenabled"
						   type="checkbox" 
						   value="true"
						   disabled
						   x-bind="Authentication.Enabled"
						   automation="HttpProxyConfig_ProxyAuthentication_Enabled_Input"
						   />
					<label for="hpcauthenticationenabled"><%: Resources.CoreWebContent.HttpProxySettings_Authentication_EnabledDesc %></label>
				</p>
			</div>
		</div>

		<div id="ProxyUsername" class="hpc-control-group">
			<label automation="HttpProxyConfig_Proxy_Username_Label"><%: Resources.CoreWebContent.HttpProxySettings_UsernameLabel %></label>
			<div>
				<input id="httpusername"
					   type="text"
					   disabled
					   x-bind="Authentication.Username" 
					   automation="HttpProxyConfig_Username"
					   />
				<p class="hpc-validation-message hpc-cloak" x-validation="Authentication.Username" x-message="isEmpty" automation="HttpProxyConfig_Username_EmptyMessage">
					<%: Resources.CoreWebContent.HttpProxySettings_UsernameValidation_IsEmpty %>
				</p>
			</div>
		</div>

		<div id="ProxyPassword" class="hpc-control-group">
			<label automation="HttpProxyConfig_Proxy_Password_Label"><%: Resources.CoreWebContent.HttpProxySettings_PasswordLabel %></label>
			<div>
				<input id="proxyPassword"
					   type="password" 
					   disabled
					   x-bind="Authentication.Password" 
					   automation="HttpProxyConfig_Proxy_Password"
					   />
				<p class="hpc-validation-message hpc-cloak" x-validation="Authentication.Password" x-message="isEmpty" automation="HttpProxyConfig_Password_EmptyMessage">
					<%: Resources.CoreWebContent.HttpProxySettings_PasswordValidation_IsEmpty %>
				</p>

				<div x-cloak-parent class="hpc-error hpc-cloak"><span x-bind="errors.credential" class="hpc-cloak"></span></div>
			</div>
		</div>
	</div>

	<div class="hpc-control-section" id="testSection">
		<label automation="HttpProxyConfig_Proxy_Section_Label"><%: Resources.CoreWebContent.HttpProxySettings_TestHeader %></label>
		
		<div id="InstanceUrl" class="hpc-control-group">
			<label automation="HttpProxyConfig_ServiceNowInstance_Url_Label"><%: Resources.CoreWebContent.HttpProxySettings_TestUrlLabel %></label>
			<div>
				<input type="text" x-bind="test.url" automation="HttpProxyConfig_TestUrl" />
				<p class="hpc-validation-message hpc-cloak" hpc-validate="test.url" x-message="isEmpty" automation="HttpProxyConfig_TestUrl_EmptyMessage">
					<%: Resources.CoreWebContent.HttpProxySettings_TestUrlValidation_IsEmpty %>
				</p>
				<p class="hpc-validation-message hpc-cloak" hpc-validate="test.url" x-message="isInvalid" automation="HttpProxyConfig_TestUrl_InvalidMessage">
					<%: Resources.CoreWebContent.HttpProxySettings_TestUrlValidation_IsInvalid %>
				</p>
			</div>
		</div>

		<div id="InstanceTest2" class="hpc-control-group" x-cloak-parent>
			<label>&nbsp;</label>
			<div>
				<ul class="hpc-horizontal-stack">

				 <li x-cloak-parent><a x-bind="test.start" href="#" class="hpc-btn hpc-btn-secondary" automation="HttpProxyConfig_Test">
				 	<%: Resources.CoreWebContent.HttpProxySettings_TestButton %></a>
				</li>
				 
				 <li x-cloak-parent>
					 <ul class="hpc-horizontal-stack" x-cloak-parent>
						<li> 
							<span x-bind="test.indicator" class="hpc-loading-indicator hpc-cloak" automation="HttpProxyConfig_TestConnection_LoadingIndicator">
								<asp:Image runat="server" ID="Image1" ImageUrl="~/Orion/images/loading_gen_small.gif" />
								<span><%: Resources.CoreWebContent.HttpProxySettings_TestIndicator %></span>
							</span>
						</li>
						<li><a x-bind="test.cancel" href="#" class="hpc-btn hpc-btn-tertiary hpc-cloak">
							<%: Resources.CoreWebContent.HttpProxySettings_TestCancelButton %></a>
						</li>
					 </ul>
				 </li>
				</ul>

				<ul class="hpc-vertical-stack" class="hpc-cloak" x-cloak-parent>
					<li class="hpc-cloak" x-cloak-parent automation="HttpProxyConfig_Test_TestSuccess">
						<span x-bind="test.success" class="hpc-success hpc-cloak"><%: Resources.CoreWebContent.HttpProxySettings_TestResult_Success %></span>
					</li>
					<li class="hpc-cloak" x-cloak-parent automation="HttpProxyConfig_Test_TestFailure"><div class="hpc-error"><span x-bind="test.fail" class="hpc-cloak"></span></div></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="hpc-bottom-section">
		<ul class="hpc-horizontal-stack">
			<li><a x-bind="save" href="#" class="hpc-btn hpc-btn-primary" automation="HttpProxyConfig_Save"><%: Resources.CoreWebContent.HttpProxySettings_SaveButton %></a></li>
			<li><a x-bind="discard" href="#" class="hpc-btn hpc-btn-tertiary" automation="HttpProxyConfig_Cancel"><%: Resources.CoreWebContent.HttpProxySettings_CancelButton %></a></li>
		</ul>
	</div>

</div>

<script type="text/javascript">
(function(){

var config = SW.Core.namespace('SW.Core.HttpProxyConfig');

config.Init = function(payload, options) {

	options = options || {};
	options.returnUrl = options.returnUrl || "/Orion/Admin/";

	var model = payload.Model;
	var errors = payload.Errors || {};
	var visibility = {};

	for( var key in errors )
	{
		var val = errors[key];
		if( typeof(val) != "string") continue;
		model.errors = model.errors || {};
		model.errors[key] = val;
		visibility['errors.' + key] = 'visible';
	}

	config.viewModel = config.Bind( model, 'proxyPanel', visibility );
	config.viewModel.options = options;
};

})();

</script>
</asp:Content>
