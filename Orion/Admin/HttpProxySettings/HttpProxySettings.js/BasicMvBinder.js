// basic data/viewmodel binder

function createViewModel(_model, _validation, _options){ 
    
    _options = _options || { cssPrefix: "x-" };

    var prefix = _options.cssPrefix || "x-";
    var cssClassCloak = prefix + "cloak";
    var attrBind = "x-bind";
    var selectorCloakParent = "[x-cloak-parent]";
    var selectorBind = "[x-bind]";

    var dom = null; // $('#' + _domId);
    var viewModel = null;
    var validation = _validation;
    var events = SW.Core.Observer.makeCancellableObserver();

    function makeValidationSelector(path){
        return '[x-validation="' + path + '"]';
    }

    function makeMessageSelector(message){
        if( !message ) return null;
        return '[x-message="' + message + '"]';
    }

    function genericClick(e) {
        var path = $(e.target).attr(attrBind);

        if(!path)
            return;

        // force change detection to occur before click handlers
        window.setTimeout( function(){ events.trigger('click', { viewModel: viewModel, id: path, e: e }); }, 0);
    }

    function cloak(doms, except) {
        doms = doms || [];
        var vis = [];

        for( var i = 0, imax = doms.length; i < imax; ++i) {

            var src = $(doms[i]);

            if(except && src.is(except)) {
                vis.push(doms[i]);
                continue;
            }

            src.addClass(cssClassCloak);
            src.parents(selectorCloakParent).addClass(cssClassCloak);
        }

        uncloak(vis);
    }

    function uncloak(doms) {
        doms = doms || [];

        for( var i = 0, imax = doms.length; i < imax; ++i) {
            var src = $(doms[i]);
            src.removeClass(cssClassCloak);
            src.parents(selectorCloakParent).removeClass(cssClassCloak);
        }
    }

    function getProperty(path) {

        var ref = viewModel.model;
        var parts = path.split('.');

        for(var i = 0; i < parts.length-1; ++i )
            ref = ref[parts[i]] || {};

        return {
            set: function(value) { 
                ref[parts[parts.length-1]] = value; },
            get: function() { 
                return ref[parts[parts.length-1]]; }
        };
    }

    function setControlValue(src,val) {
        var isFormElement = src.is('select,input');

        if( isFormElement && src.is(':checkbox') ) {
            if( val ) $(src).attr('checked','checked');
            else $(src).removeAttr('checked');
        }
        else if( isFormElement ) {
            src.val(val);
        }
        else if( val !== undefined) {
            src.text(val);
        }
    }

    function changeDetection(e) {
        var src = $(e.target);
        var v = src.val();
        var path = src.attr(attrBind);
        var validationInfo = (validation[path] || validation['default'])(viewModel.model, v, src);
        var property = getProperty(path);
        var exist = property.get();

        cloak( dom.find(makeValidationSelector(path)), makeMessageSelector(validationInfo.failure) );

        var skipEnable = exist == validationInfo.result;

        if( !skipEnable ) {
            if( v == "" && exist == null ) ; // intentional
            else if( events.trigger('changed', { viewModel: viewModel, id: path, value: v, old: exist, validation: validationInfo }) == false)
                return;
        }

        property.set(validationInfo.result);

        if(validationInfo.result !== v)
            setControlValue(src, validationInfo.result);

        if( !skipEnable)
            enableControls();
    }

    function enableControls() {
        var doms = dom.find(selectorBind);

        for( var i = 0, imax = doms.length; i < imax; ++i) {
            var src = $(doms[i]);
            var v = src.val();
            var path = src.attr(attrBind);
            var validationInfo = (validation[path] || validation['default'])(viewModel.model, v, src);

            if( validationInfo.result !== v )
                setControlValue( src, validationInfo.result );

            if( validationInfo.disabled === true )
            {
                src.val(validationInfo.result);
                src.attr('disabled','disabled');
                cloak( dom.find(makeValidationSelector(path)) );
            }
            else
            {
                src.removeAttr('disabled');
            }
        }
    }

    function validateControls(doAll){
        var ok = true;
        var doms = dom.find(selectorBind);

        for( var i = 0, imax = doms.length; i < imax; ++i) {
            var src = $(doms[i]);
            var v = src.val();
            var path = src.attr(attrBind);
            var validationInfo = (validation[path] || validation['default'])(viewModel.model, v, src);
            if( validationInfo.ok ) continue;

            cloak( dom.find(makeValidationSelector(path)), makeMessageSelector(validationInfo.failure) );

            ok = false;
            if(!doAll) break;
        }

        return ok;
    }

    function updateControls() {
        var doms = dom.find(selectorBind);
        var toCloak = [];
        var toUncloak = [];

        for( var i = 0, imax = doms.length; i < imax; ++i) {
            var src = $(doms[i]);
            var path = src.attr(attrBind);
            var property = getProperty(path);
            var val = property.get();

            setControlValue(src, val);

            // -- use visibility settings

            var vis = (viewModel.visibility || {})[path];

            if( vis == 'visible' )
                toUncloak.push(src[0]);
            else if( vis == 'hidden' )
                toCloak.push(src[0]);
        }

        cloak(toCloak);
        uncloak(toUncloak);
        enableControls();
    }

    function initialize(domId) {

        dom = $('#' + domId);
        updateControls();

        dom.find(selectorBind).change(changeDetection);
        dom.find('input' + selectorBind + ', select' + selectorBind).blur(changeDetection);
        $(document).on('click', '#' + dom[0].id + ' ' + selectorBind, genericClick);
    }

    function unhookControls() {

        dom.find(selectorBind).unbind('change',changeDetection);
        dom.find('input' + selectorBind + ', select' + selectorBind).unbind('blur',changeDetection);
        $(document).off('click', '#' + dom[0].id + ' ' + selectorBind, genericClick);
        events.trigger('unhook', { viewModel: viewModel });
    }

    // ==

    viewModel = {

        model: _model,

        visibility: {},

        events: events,

        GetProperty: getProperty,

        IsValid: validateControls,

        Update: updateControls,

        Unhook: unhookControls
    };

    return { viewModel: viewModel, Initialize: initialize };
};
