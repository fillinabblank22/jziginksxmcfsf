// -- data model (validation / disable) logic

var visibleText = {
    unknownServerError: "@{R=Core.Strings.2;K=HttpProxySettings_TestUnknownError;E=js}",
    successfulTestComplete: "@{R=Core.Strings.2;K=HttpProxySettings_TestSuccess;E=js}",
    emptyTestURL: "@{R=Core.Strings.2;K=WEBJS_SO0_17;E=js}",
    confirmationMessage: "@{R=Core.Strings.2;K=WEBJS_SO0_16;E=js}"
};

var blValidationLogic = (function(){ 

	function authValue(model,v) {

		if( model.Mode == 'Default' || model.Mode == 'Disabled' || !model.Authentication.Enabled)
			return { ok: true, result: null, disabled: true };

		var p = v.trim();

		if(p == '') return { ok: false, result: v, failure: 'isEmpty' };

		return { ok: true, result: p };
	}

	return {

		'default': function(model,v)  {
				return { ok: true, result: v };
		},

		'Protocol': function(model,v) {
			if( model.Mode == 'Default' || model.Mode == 'Disabled')
				return { ok: true, result: v, disabled: true };

			return { ok: true, result: v };
		},

		'Address': function(model,v) {
			if( model.Mode == 'Default' || model.Mode == 'Disabled')
				return { ok: true, result: null, disabled: true };

			var p = v.trim();

			if(p == '') return { ok: false, result: v, failure: 'isEmpty' };

			return { ok: true, result: p };
		},

		'Port': function(model,v) {
			if( model.Mode == 'Default' || model.Mode == 'Disabled')
				return { ok: true, result: null, disabled: true };

			var p = v.trim();

			if(p == '') return { ok: false, result: v, failure: 'isEmpty' };

			p = parseInt(p, 10);

			if( isNaN(p) || p < 1 || p > 65535) return { ok: false, result: v, failure: 'isInvalid' };

			return { ok: true, result: p };
		},

		'Authentication.Enabled': function(model,v,target) {
			if( model.Mode == 'Default' || model.Mode == 'Disabled')
				return { ok: true, result: false, disabled: true };

			var p = target == null ? v : '' + ( target.attr("checked") != null );

			return { ok: true, result: p == 'true' };
        },

        'Authentication.Username': authValue,
		'Authentication.Password': authValue
	};

})();

function loginRedirTest(obj)
{
	if(!obj || obj.status != 200 || typeof(obj.responseText) != "string" ) return;
	if( obj.responseText.indexOf('loginsig(09470EE361E54F3DB730000176BA16B2)') >= 0 )
		window.location = '/orion/login.aspx?ReturnUrl=' + encodeURIComponent(window.location.pathname + window.location.search);
}

function testSucceeded(viewModel, result, next, optional)
{
	if( !viewModel )
		return;

	var data = result && result.d;

	viewModel.visibility['test.start'] = 'visible';
	viewModel.visibility['test.cancel'] = 'hidden';
	viewModel.visibility['test.indicator'] = 'hidden';

	if( !data.Success)
    {
        viewModel.visibility['test.fail'] = 'visible';
        viewModel.model.test.fail = data.Fail;
        viewModel.Update();

        if (!optional || !confirm(visibleText.confirmationMessage)) {
            return;
        }
    }

    if (data.Success) {
        viewModel.visibility['test.fail'] = 'hidden';
        viewModel.visibility['test.success'] = 'visible';
        viewModel.model.test.success = visibleText.successfulTestComplete;
    }
    viewModel.model.test.verified = true;
    viewModel.Update();
	if(next) next();
}

function testFailed(viewModel)
{
	if( !viewModel )
		return;

	viewModel.visibility['test.start'] = 'visible';
	viewModel.visibility['test.cancel'] = 'hidden';
	viewModel.visibility['test.indicator'] = 'hidden';
	viewModel.visibility['test.fail'] = 'visible';
	viewModel.visibility['test.success'] = 'hidden';
	viewModel.model.test.fail = visibleText.unknownServerError;
	viewModel.model.test.verified = false;
	viewModel.Update();
}

// -- event handling

var testState = {};
var testStateCounter = 0;

function startProxyTest(viewModel, next, optional) {
	if( viewModel.IsValid(true) == false)
        return;

    if (!optional && (!viewModel.model.test.url || viewModel.model.test.url.trim() == '')) {
        viewModel.visibility['test.fail'] = 'visible';
        viewModel.visibility['test.success'] = 'hidden';
        viewModel.model.test.fail = visibleText.emptyTestURL;
        viewModel.model.test.verified = false;
        viewModel.Update();
        return;
    }

    var id = '' + (testStateCounter++);
	testState = {};
	testState[id] = viewModel;

	viewModel.visibility['test.start'] = 'hidden';
	viewModel.visibility['test.cancel'] = 'visible';
	viewModel.visibility['test.indicator'] = 'visible';
	viewModel.visibility['test.fail'] = 'hidden';
	viewModel.visibility['test.success'] = 'hidden';
    viewModel.Update();

    var request = $.ajax({
        url: "/Orion/Admin/HttpProxySettings/Default.aspx?Op=Test",
        type: "POST",
        data: JSON.stringify(viewModel.model),
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (result) {
            testSucceeded(testState[id], result, next, optional);
        },

        error: function (obj, reason, detail) {
            loginRedirTest(obj);
            testFailed(testState[id]);
        }
    });
};

function stopProxTest(viewModel) {
	testState = {};
	viewModel.visibility['test.start'] = 'visible';
	viewModel.visibility['test.cancel'] = 'hidden';
	viewModel.visibility['test.indicator'] = 'hidden';
	viewModel.visibility['test.fail'] = 'hidden';
	viewModel.visibility['test.success'] = 'hidden';
	viewModel.Update();
}

function startSave(viewModel) {
	if( viewModel.IsValid(true) == false)
		return;

	if( !viewModel.model.test.verified )
    {
            var vm = viewModel;
            startProxyTest(vm, function() { startSave(vm); }, true); //optional
            return;
    }

    var request = $.ajax({
        url: "/Orion/Admin/HttpProxySettings/Default.aspx?Op=Save",
        type: "POST",
        data: JSON.stringify(viewModel.model),
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (result) {
        	window.location = viewModel.options.returnUrl;
        },

        error: function(obj) {
        	loginRedirTest(obj);

        	if( window.console && window.console.log )
        		window.console.log({ 'save': 'failed',  viewModel: viewModel, args: arguments });
        }
    });
}

function blOnClick(args) {
	switch( args.id ) {
	    case 'save':
	        if ($("#isOrionDemoMode").length != 0) {
	            demoAction("Core_ProxySettings_Save", this);
	            return false;
	        }

			startSave(args.viewModel);
			break;

		case 'discard':
			window.location = args.viewModel.options.returnUrl;
			break;

	    case 'test.start':
	        if ($("#isOrionDemoMode").length != 0) {
	            demoAction("Core_ProxySettings_Test", this);
	            return false;
	        }

			startProxyTest(args.viewModel);
			break;

		case 'test.cancel':
			stopProxTest(args.viewModel);
			break;
	}

	return true;
}

function blOnChange(args) {
	
	if( args.Id != 'test.url')
		args.viewModel.model.test.verified = false;

	switch( args.id ) {
		case 'Mode':
			var have = args.viewModel.visibility['Protocol'];
			if( have === undefined ) break;
			var want = args.value == 'disabled' || args.value == 'default' ? 'hidden' : 'visible';
			if( want == have) return;
			args.viewModel.visibility['Protocol'] = want;
			args.viewModel.Update();
			break;

		case 'Address':
			var have = args.viewModel.visibility['errors.uri'];
			if( have !== 'visible') return;
			args.viewModel.visibility['errors.uri'] = 'hidden';
			args.viewModel.Update();
			break;

		case 'Authentication.Username':
		case 'Authentication.Password':
			var have = args.viewModel.visibility['errors.credential'];
			if( have !== 'visible') break;
			args.viewModel.visibility['errors.credential'] = 'hidden';
			args.viewModel.Update();
			break;

		case 'Protocol':
			if( args.value == 'http' && args.viewModel.model.Port == '443') {
				window.setTimeout(function(){ 
					args.viewModel.model.Port = '80';
					args.viewModel.Update(); }, 0);
			}
			else if( args.value == 'https' && args.viewModel.model.Port == '80') {
				window.setTimeout(function(){ 
					args.viewModel.model.Port = '443';
					args.viewModel.Update(); }, 0);
			}
			break;
	}

	return true;
}

function bind(model, id, visibility) {

	function detatch(args){
		args.viewModel.events.off('click', blOnClick);
		args.viewModel.events.off('changed', blOnClick);
		args.viewModel.events.off('unhook', detatch);
	}

	model.Authentication = model.Authentication || {};
	model.Mode = model.Mode || 'Default';
	model.test = model.test || {};
	model.test.url = model.test.url;
	model.test.verified = true;

	var obj = createViewModel(model, blValidationLogic, { cssPrefix: "hpc-" } );
	var viewModel = obj.viewModel;
	viewModel.events.on('click', blOnClick);
	viewModel.events.on('changed', blOnChange);
	viewModel.events.on('unhook', detatch);
	viewModel.visibility = visibility || viewModel.visibility;

	if(model.Protocol == 'https')
		viewModel.visibility['Protocol'] = 'visible';

	obj.Initialize(id);
	return viewModel;
};

SW.Core.namespace('SW.Core.HttpProxyConfig').Bind = bind;
