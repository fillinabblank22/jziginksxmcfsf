<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Admin/OrionNotificationsPage.master" AutoEventWireup="true" CodeFile="GenericNotifications.aspx.cs" Inherits="Orion_Admin_GenericNotifications" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/GenericNotificationsControl.ascx" TagPrefix="orion" TagName="Notifications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitlePlaceHolder" Runat="Server">
    <%= DefaultSanitizer.SanitizeHtml(SelectedNotificationItemType.Description) %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="HelpIcon" runat="server" HelpUrlFragment="OrionCorePHGeneralNotifications" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageInfoPlaceHolder" Runat="Server">
    List of important Orion notification messages.
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="NotificationsContentPlaceHolder" Runat="Server">
    <orion:Notifications runat="server" ID="NotificationsControl" />
</asp:Content>

