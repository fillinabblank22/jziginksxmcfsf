﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="TokenConvert.aspx.cs" Inherits="Orion_Admin_TokenConvert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <p>Enter the text to be converted and click convert</p>
    <asp:Button runat="server" ID="ConvertButton" Text="Convert" OnClick="ConvertButton_OnClick"/>
    
    <table>
        <tr>
            <td>Source:</td>
            <td>Converted:</td>
        </tr>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="Source" TextMode="MultiLine" Rows="20" Columns="50"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="Dest" TextMode="MultiLine" Rows="20" Columns="50"></asp:TextBox>
            </td>
        </tr>
    </table>

</asp:Content>

