using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Orion_Admin_YesNoControl : System.Web.UI.UserControl
{
    public string ListBoxClientId
    {
        get { return listBox.ClientID; }
    }

    public bool Value
    {
        get
        {
            bool ret;
            if (bool.TryParse(this.listBox.SelectedValue, out ret))
                return ret;
            return false;
        }
        set
        {
            this.listBox.Items.FindByValue(value ? "true" : "false").Selected = true;
            this.listBox.Items.FindByValue(value ? "false" : "true").Selected = false;
        }
    }

}
