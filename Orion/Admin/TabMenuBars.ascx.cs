﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_TabMenuBars : UserControl
{
	private static readonly SolarWinds.Logging.Log Log = new SolarWinds.Logging.Log();

	private static ITabManager manager = new TabManager();
	public int itemCounter;

    protected string GetTabText(string key)
    {
        const string managerid = "Core.Strings";
        const string prefix = "NavigationTab";

        ResourceManagerRegistrar registrar = ResourceManagerRegistrar.Instance;
        string lookupkey = registrar.CleanResxKey(prefix, key);
        return registrar.SearchAll(lookupkey, new[] { managerid }) ?? key; // look up core last, and default to the incoming key.
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        var accountID = Request.QueryString["AccountID"];
        var tabs = new List<Tab>(manager.GetTabs(accountID, false, true, true));
        tabBars.DataSource = tabs;
        tabBars.DataBind();
    }

	protected void TabsDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			var td = e.Item.FindControl("td");
            td.Visible = false; // itemCounter < 1; // FB 18433
		}
		itemCounter++;
	}

	public void AssignBarsToTabs(string accountId)
	{
		if (accountId == null)
		{
			Log.Error("Couldn't assign bar to tab due to AccountId is null");
			return;
		}

		var list = new List<ProfilePropEditUserControl>();
		FindControls(this, list);

		foreach (var item in list)
		{
			if (item.PropertyValue.Equals("none", StringComparison.InvariantCultureIgnoreCase))
			{
				manager.UnassignMenuBar(accountId, Convert.ToInt32(item.ID));
			}
			else
			{
				manager.AssignBarToTab(accountId, Convert.ToInt32(item.ID), item.PropertyValue);
			}
		}
	}

	public void SetUpValues(string accountId)
	{
		if (accountId == null)
		{
			Log.Error("Couldn't set up bar due to AccountId is null");
			return;
		}

		if (!IsPostBack)
		{
			var list = new List<ProfilePropEditUserControl>();
			FindControls(this, list);

			foreach (var control in list)
			{
				control.PropertyValue = manager.GetCurrentBarName(accountId, Convert.ToInt32(control.ID));
			}
		}
	}

	protected void TabsDataBound(object sender, EventArgs e)
	{
		var placeholder = (PlaceHolder)sender;
		var propControl = (UserControl)LoadControl("/Orion/Admin/MenuBars.ascx");
		propControl.ID = DataBinder.Eval(placeholder.NamingContainer, "DataItem.TabId").ToString();
		placeholder.Controls.Add(propControl);
	}

	private void FindControls(Control parent, ICollection<ProfilePropEditUserControl> list)
	{
		if (parent is ProfilePropEditUserControl)
		{
			list.Add((ProfilePropEditUserControl)parent);
		}
		else
		{
			if (parent.Controls != null)
			{
				foreach (Control child in parent.Controls)
				{
					FindControls(child, list);
				}
			}
		}
	}
}
