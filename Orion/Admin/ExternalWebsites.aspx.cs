using System;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_ExternalWebsites : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		lbMenuBars.DataSource = MenuBar.GetAllMenuBars();
		lbMenuBars.DataBind();
        dialogOk.DataBind();
        dialogCancel.DataBind();
	}
}
