﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_CPE_InlineEditor : System.Web.UI.Page
{
    protected string EntityName
    {
        get
        {
            if (ViewState["EntityName"] == null)
            {
                ViewState["EntityName"] = "Orion.Nodes";
            }
            return ViewState["EntityName"].ToString();
        }
        set { ViewState["EntityName"] = value; }
    }

    protected string ColumnsList
    {
        get
        {
            if (ViewState["ColumnsList"] == null)
            {
                ViewState["ColumnsList"] = string.Empty;
            }
            return ViewState["ColumnsList"].ToString();
        }
        set { ViewState["ColumnsList"] = value; }
    }

    protected List<string> AvailableEntities
    {
        get
        {
            if (ViewState["AvailableEntities"] == null)
            {
                ViewState["AvailableEntities"] = GetAvailableEntities();
            }
            return (List<string>)ViewState["AvailableEntities"];
        }
    }

    private List<string> GetAvailableEntities()
    {
        DataTable dt;
        var list = new List<string>();
        using (var dal = new CustomPropertyDAL())
        {
            dt = dal.GetAvailableEntities();
        }

        if (dt != null && dt.Rows.Count > 0)
        {
            list.AddRange(from DataRow row in dt.Rows select row["SourceType"].ToString());
        }
        return list;
    }

    private bool? _isNodeFunctionalityAllowed;
    /// <summary>
    /// (get) Return True if licence allow to add/manage nodes. False in the case, that licence doesn't allow it (for example user has just installed SRM module).
    /// </summary>
    protected bool IsNodeFunctionalityAllowed
    {
        get
        {
            if (!_isNodeFunctionalityAllowed.HasValue)
            {
                var featureManager = new SolarWinds.Orion.Core.Common.FeatureManager();
                int maxNodes = featureManager.GetMaxElementCount(SolarWinds.Orion.Core.Common.WellKnownElementTypes.Nodes);
                _isNodeFunctionalityAllowed = maxNodes != 0;
            }

            return _isNodeFunctionalityAllowed.Value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        
        if (IsPostBack) return;

        var customProperties = WebSecurityHelper.SanitizeHtmlV2(Request["CustomPropertyIds"]);
        var sourceEntity = "Orion.Nodes";
        var columnList = new List<string>();

        if (customProperties != null)
        {
            var ids = customProperties.Split(',');
            foreach (var propVals in ids.Select(id => id.Split(':')).Where(propVals => propVals.Length == 2))
            {
                sourceEntity = propVals[0];
                columnList.Add(propVals[1]);
            }
        }

        if (columnList.Count>0)
        {
            ColumnsList = string.Join(",", columnList.ToArray());
        }

        EntityName = sourceEntity;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Cancel_Click(object source, EventArgs e)
    {
        ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");
    }
}