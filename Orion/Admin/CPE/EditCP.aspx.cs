using System;
using System.Globalization;
using System.Web;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.CPE;

public partial class Orion_CPE_EditCP : System.Web.UI.Page
{
	private static readonly Log _log = new Log();

	protected int PropertySize
	{
		get
		{
			if (ViewState["EditCP_PropertySize"] == null)
			{
				ViewState["EditCP_PropertySize"] = 0;
			}
			return (int)ViewState["EditCP_PropertySize"];
		}
		set
		{
			ViewState["EditCP_PropertySize"] = value;
		}
	}

	protected override void OnInit(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		Response.Cache.SetCacheability(HttpCacheability.NoCache);
		base.OnInit(e);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			if (Request["CustomPropertyId"] != null)
			{
				string[] customPropertyId = Request["CustomPropertyId"].Split(':');

			    var sourceEntity = customPropertyId[0];
                var targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(sourceEntity);

			    EditCPControl.SourceEntityName = sourceEntity;
			    EditCPControl.TargetEntityName = targetEntity;

				EditCPControl.IsEdit = true;
			    
				CustomProperty customProperty = CustomPropertyMgr.GetCustomPropertyByEntity(targetEntity, customPropertyId[1]);

				if (customProperty.DatabaseColumnType.Equals("nvarchar", StringComparison.OrdinalIgnoreCase))
				{
					PropertySize = (customProperty.DatabaseColumnSize == -1) ? int.MaxValue : customProperty.DatabaseColumnSize;
				}	

				// handle Back behaviour
				if (!string.IsNullOrEmpty(customProperty.PropertyName) && !string.IsNullOrEmpty(customProperty.DatabaseColumnType))
				{
					EditCPControl.UpdateFields(customProperty.PropertyName, customProperty.Description, customProperty.DatabaseColumnType, customProperty.DatabaseColumnLength, customProperty.Mandatory, customProperty.Default, customProperty.Values, customProperty.UsageFlags, true);
				}
			}
			if (Request["NewRestrictedValues"] != null)
			{
				string[] restrictedValuesToAdd = ((string)Request["NewRestrictedValues"]).Split(';');

				if (restrictedValuesToAdd.Length>0) 
					EditCPControl.AppendNewField(restrictedValuesToAdd);
			}
		}
	}

	[WebMethod(EnableSession = true)]
	public static string GetAffectedObjectsNumber(string entityName, string propertyName, string value)
	{
		return CustomPropertyHelper.GetCountBySource(entityName, propertyName, value).ToString(CultureInfo.CurrentCulture);
	}

    [WebMethod(EnableSession = true)]
    public static object GetNumberAffectedValues(string entityName, string propertyName, int columnSize)
    {
        return CustomPropertyHelper.GetNumberAffectedValues(entityName, propertyName, columnSize);
    }

	protected void Submit_Click(object source, EventArgs e)
	{
		if (Page.IsValid)
		{
			_log.DebugFormat("Updating custom property '{0}' for '{1}'.", EditCPControl.PropertyName, EditCPControl.SourceEntityName);
			try
			{
                CustomProperty cp = new CustomProperty();
                cp.TargetEntity = EditCPControl.TargetEntityName;
                cp.PropertyName = EditCPControl.PropertyName;
                cp.Description = EditCPControl.PropertyDescription;
                cp.DatabaseColumnType = EditCPControl.DatabaseColumnType;
			    cp.Mandatory = EditCPControl.Mandatory;
                cp.Values = EditCPControl.EnumerationValues != null ? EditCPControl.EnumerationValues.ToArray() : new string[0];
                cp.UsageFlags = EditCPControl.UsageFlags;
			    cp.Default = EditCPControl.Default;
                int propertySize = EditCPControl.PropertySize;
			    cp.DatabaseColumnLength = cp.DatabaseColumnType.Equals("NVARCHAR", StringComparison.OrdinalIgnoreCase)
			        ? propertySize == 0 ? Int32.MaxValue : propertySize
			        : propertySize;

			    CustomPropertyHelper.ModifyCustomProperty(cp);
			}
            catch (Exception ex)
            {
                _log.Error(string.Format("ModifyCustomProperty failed, prop name '{0}', prop source '{1}'", EditCPControl.PropertyName, EditCPControl.SourceEntityName), ex);
            }

			ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");
		}
	}

	protected void Cancel_Click(object source, EventArgs e)
	{
		ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");
	}
}
