<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CPE/Add/AddCPWizard.master"
    AutoEventWireup="true" CodeFile="ChooseProperties.aspx.cs" Inherits="Orion_CPE_Add_ChooseProperties"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_15 %>" %>

<%@ Register Src="~/Orion/Admin/CPE/Controls/DefineCPControl.ascx" TagPrefix="orion"
    TagName="DefineCPControl" %>
<asp:Content ContentPlaceHolderID="adminHeadPlaceholder2" runat="server">
    <style type="text/css">
        .error
        {
            border: 2px solid red;
        }
        #gridCell div#Grid
        {
            width: 100%;
        }
        .no-border .x-table-layout-ct, .panel-no-border .x-panel-body-noheader
        {
            border: none !important;
        }
        .panel-border
        {
            border: solid 1px #D0D0D0 !important;
        }
        .hide-header .x-grid3-header
        {
            display: none;
        }
        .x-grid3-row-alt
        {
            background-color: #EBF3FD;
        }
        .x-tip-body
        {
            white-space: nowrap !important;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <br />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_14) %>
            <div>
                <orion:DefineCPControl runat="server" ID="DefineCPControl" TargetEntity="<%$ HtmlEncodedCode: TargetEntity %>" SourceEntity="<%$ HtmlEncodedCode: SourceEntity %>" />
            </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="LocalizableButton1" OnClick="Back_Click"
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
