﻿using System;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_CPE_Add_ChooseProperties : AddCustomPropertyWizardBase
{
    [Obsolete("deprecated per SWIS entity usage")]
	protected string TableName
	{
		get { return Workflow.CustomProperty != null ? Workflow.CustomProperty.Table : string.Empty; }
	}

    protected string TargetEntity
    {
        get { return Workflow.CustomProperty != null ? Workflow.CustomProperty.TargetEntity : string.Empty; }
    }

    protected string SourceEntity
    {
        get { return Workflow.CustomProperty != null ? Workflow.CustomProperty.SourceEntity : string.Empty; }
    }

    protected void Page_Load(object sender, EventArgs e)
	{
        if (((AddCustomPropertyWorkflow)Workflow).CurrentStepType != AddCustomPropertyWorkflow.StepType.ChooseProperties)
		{
			GotoFirstStep();
		}

		var customProperty = Workflow.CustomProperty;
		// handle Back behaviour
		if (!IsPostBack && !string.IsNullOrEmpty(customProperty.PropertyName)
			&& !string.IsNullOrEmpty(customProperty.DatabaseColumnType))
		{
			string cleanName = WebSecurityHelper.SanitizeAngular(customProperty.PropertyName);
	        string cleanDesc = WebSecurityHelper.SanitizeAngular(customProperty.Description);
			DefineCPControl.UpdateEditCPFields(
            cleanName, cleanDesc, customProperty.DatabaseColumnType, customProperty.Size ?? 0, customProperty.Mandatory, customProperty.Default, customProperty.Values, customProperty.UsageFlags);
		}
	}

	protected override bool Next()
	{
		string cleanName = WebSecurityHelper.SanitizeAngular(DefineCPControl.PropertyName);
		string cleanDesc = WebSecurityHelper.SanitizeAngular(DefineCPControl.PropertyDescription);
		Workflow.CustomProperty.PropertyName = cleanName;
		Workflow.CustomProperty.Description = cleanDesc;
		Workflow.CustomProperty.DatabaseColumnType = DefineCPControl.DatabaseColumnType;
		Workflow.CustomProperty.DatabaseColumnLength = DefineCPControl.PropertySize;
        Workflow.CustomProperty.Mandatory = DefineCPControl.Mandatory;
        Workflow.CustomProperty.Default = DefineCPControl.Default;
		Workflow.CustomProperty.Values = 
			DefineCPControl.EnumerationValues != null ? DefineCPControl.EnumerationValues.ToArray() : new string[0];
	    Workflow.CustomProperty.UsageFlags = DefineCPControl.UsageFlags;
		return base.Next();
	}
}