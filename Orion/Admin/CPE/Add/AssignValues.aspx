<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CPE/Add/AddCPWizard.master"
    AutoEventWireup="true" CodeFile="AssignValues.aspx.cs" Inherits="Orion_Admin_CPE_Add_AssignValues"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_15 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Web.CPE" %>

<%@ Register Src="~/Orion/Admin/CPE/Controls/AssignValue.ascx" TagPrefix="orion"
    TagName="AssignValue" %>
<asp:Content ContentPlaceHolderID="adminHeadPlaceholder2" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <br />
            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IB0_126, SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular(PropertyName) , Description, CustomPropertyHelper.GetSourceEntityDisplayName(TargetEntity))) %>
            <br />
            <br />
            <orion:AssignValue runat="server" ID="AssignValues" EnumerationValues="<%$ HtmlEncodedCode: Values %>" />
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="LocalizableButton1" OnClick="Back_Click"
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Submit"
                DisplayType="Primary" ValidationGroup="EditCpValue" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
