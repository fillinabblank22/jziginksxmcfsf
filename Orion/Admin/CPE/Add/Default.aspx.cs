using System;
using System.Data;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_CPE_Add_Default : AddCustomPropertyWizardBase
{
    protected override void OnInit(EventArgs e)
    {
        Workflow.FirstStep();
        base.OnInit(e);

        var availableEntities = Session["Orion_CPE_AvailableEntities"] as DataTable;
        if (availableEntities == null)
        {
            using (var dal = new CustomPropertyDAL())
            {
                availableEntities = dal.GetAvailableEntities();
            }
        }

        Entities.DataSource = availableEntities;

        Entities.DataTextField = "DisplayNamePlural";
        Entities.DataValueField = "SourceType";

        Entities.DataBind();

        // we should select correct value after press Back on next scene
        if (Workflow.CustomProperty != null && !string.IsNullOrEmpty(Workflow.CustomProperty.TargetEntity))
        {
            Entities.SelectedValue = Workflow.CustomProperty.SourceEntity;
        }
        else
        {
            // select Nodes by default
            Entities.SelectedValue = "Orion.Nodes";
        }

        Session["Orion_CPE_AvailableEntities"] = availableEntities;
    }

    protected override bool Next()
    {
        var availableEntities = (DataTable)Session["Orion_CPE_AvailableEntities"];

        string source = null;
	    string target = null;
	    foreach (DataRow row in availableEntities.Rows)
	    {
            var type = row["SourceType"].ToString();
            if (type.Equals(Entities.SelectedValue, StringComparison.OrdinalIgnoreCase))
            {
                target = row["Type"].ToString();
                source = type;
                break;
            }
	    }
	    Workflow.CustomProperty = new CustomProperty
	    {
            SourceEntity = source, 
            TargetEntity = target,
	    };
		return base.Next();
	}
}
