﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.CPE;

public partial class Orion_Admin_CPE_Add_AssignValues : AddCustomPropertyWizardBase
{
	private static readonly Log _log = new Log();
	#region properties
	protected string PropertyName
	{
		get
		{
			return (Workflow.CustomProperty != null) ? Workflow.CustomProperty.PropertyName : string.Empty;
		}
	}
	protected string Description
	{
		get
		{
			return (Workflow.CustomProperty != null) ? Workflow.CustomProperty.Description : string.Empty;
		}
	}
    protected string TargetEntity
    {
        get
        {
            return (Workflow.CustomProperty != null) ? Workflow.CustomProperty.TargetEntity : string.Empty;
        }
    }
    protected string SourceEntity
    {
        get
        {
            return (Workflow.CustomProperty != null) ? Workflow.CustomProperty.SourceEntity : string.Empty;
        }
    }
    [Obsolete("Table usage deprecated per TargetEntity")]
    protected string Table
	{
		get
		{
			return (Workflow.CustomProperty != null) ? Workflow.CustomProperty.Table : string.Empty;
		}
	}
	protected string[] Values
	{
		get
		{
			return (Workflow.CustomProperty != null) ? Workflow.CustomProperty.Values : new string[0];
		}
	}
	#endregion


	protected void Page_Load(object sender, EventArgs e)
	{
        if (((AddCustomPropertyWorkflow)Workflow).CurrentStepType != AddCustomPropertyWorkflow.StepType.AssignValues)
		{
			GotoFirstStep();
		}

		if (!IsPostBack)
		{
			AssignValues.SetPropertyAssignments(null, Workflow.CustomProperty);
			AssignValues.PropertyType = Workflow.CustomProperty.PropertyType;
            AssignValues.DatabaseColumnType = Workflow.CustomProperty.DatabaseColumnType;
			AssignValues.PropertySize = Workflow.CustomProperty.DatabaseColumnLength;
			AssignValues.PropertyName = Workflow.CustomProperty.PropertyName;
		    AssignValues.Mandatory = Workflow.CustomProperty.Mandatory;
            AssignValues.Default = Workflow.CustomProperty.Default;
		}
	}

	protected override bool Next()
	{
		var customProperty = Workflow.CustomProperty;
		if (customProperty != null)
		{
			_log.DebugFormat("Creating custom property '{0}' for '{1}'.", customProperty.PropertyName, customProperty.SourceEntity);
		    try
		    {
		        var assignments = AssignValues.GetPropertyAssignments();
		        if (customProperty.Values != null && customProperty.Values.Length > 0)
		        {
		            // add new restricted values if defined
		            var list = new List<string>(customProperty.Values);
		            foreach (var propVal in assignments.Keys)
		            {
		                // don't add new value if:
		                // 1) value already exists;
		                // 2) value haven't any assignments
		                if (!list.Contains(propVal) && assignments[propVal] != null && assignments[propVal].Count > 0)
		                {
		                    list.Add(propVal);
		                }
		            }
		            customProperty.Values = list.ToArray();
		        }

		        CustomPropertyHelper.CreateCustomProperty(customProperty);

		        var formattedCpName = CommonHelper.FormatCustomPropertyName(customProperty.PropertyName);
		        Session["CustomPropertyIds"] =
		            CustomPropertyHelper.FormatArray(new string[] {customProperty.SourceEntity + ":" + formattedCpName});

		        // add CP to updated CP list
                if (assignments != null)
                {
                    if (assignments.Any(val => val.Value != null && val.Value.Count > 0))
                    {
                        // before value assignemnt, we need wait for schema 'refresh'
                        bool isCpReloaded = false;
                        int tryCnt = 0;
                        do
                        {
                            System.Threading.Thread.Sleep(500);
                            isCpReloaded = CustomPropertyHelper.IsEntityProperty(customProperty.TargetEntity, formattedCpName);
                            tryCnt++;
                        } while (!isCpReloaded && tryCnt < 20);

                        CustomPropertyHelper.AssignCpValuesForEntity(formattedCpName, assignments, customProperty.TargetEntity);
                        Session["AssignedPropertyIds"] = new[] { string.Format("{0}:{1}", customProperty.SourceEntity, formattedCpName) };
                    }
                }
		    }
		    catch (Exception ex)
		    {
		        _log.ErrorFormat(
		            string.Format("CreateCustomProperty failed, property name '{0}', property entity '{1}'",
		                customProperty.PropertyName, customProperty.SourceEntity), ex);

		        string script =
		            string.Format(
		                "Ext.Msg.show({{ title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR }});",
		                Resources.CoreWebContent.WEBCODE_AK0_1,
		                Resources.CoreWebContent.WEBCODE_PF0_13);

		        ScriptManager.RegisterStartupScript(this, GetType(), "ErrorMessage", script, true);
		        return false;
		    }
		}

		return base.Next();
	}
}