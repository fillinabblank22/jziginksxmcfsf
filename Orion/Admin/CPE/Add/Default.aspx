<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CPE/Add/AddCPWizard.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_CPE_Add_Default"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_15 %>" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder2" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <br />
            <div style="padding: 10px 0px 10px 0px;">
                <h1>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_13) %>
                    <asp:DropDownList runat="server" ID="Entities">
                    </asp:DropDownList>
                </h1>
            </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
