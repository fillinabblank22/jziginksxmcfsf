﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.InformationService;


public partial class Orion_Admin_CPE_AssignCPValues : System.Web.UI.Page
{
	[Serializable]
	public class BaseCpInfo
	{
		public string Name;
		public string Description;
		public Type Type;
		public int Size;
        [Obsolete("deprecated and incorrect - use TargetEntity")]
		public string Table;
	    public string TargetEntity;
		public string[] EnumValues = new string[0];
		public Dictionary<string, List<string>> AssignedValues;
	}
	private static readonly Log _log = new Log();
	#region properties
	protected string[] Values
	{
		get { return new string[0]; }
	}

	protected List<BaseCpInfo> CustomProperties
	{
		get
		{
			if (ViewState["CustomProperties"] == null)
			{
				ViewState["CustomProperties"] = new List<BaseCpInfo>();
			}
			return (List<BaseCpInfo>)ViewState["CustomProperties"];
		}
		set
		{
			ViewState["CustomProperties"] = value;
		}

	}

	protected string EntityName
	{
		get
		{
			if (ViewState["EntityName"] == null)
			{
				ViewState["EntityName"] = "Orion.Nodes";
			}
			return (string)ViewState["EntityName"];
		}
		set
		{
			ViewState["EntityName"] = value;
		}
	}


	#endregion

	protected override void OnInit(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		Response.Cache.SetCacheability(HttpCacheability.NoCache);

		base.OnInit(e);
	}

	private static int? GetSetting(string name)
	{
		int val;

		if (int.TryParse(ConfigurationManager.AppSettings[name], out val))
			return val;

		return null;
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			int maxNumber = GetSetting("CPMaxNumberToAssign") ?? 10;
			var customProperties = Request["CustomPropertyIds"];
			if (customProperties != null)
			{
				var list = new List<BaseCpInfo>();
				var ids = customProperties.Split(',');
				foreach (var propVals in ids.Select(id => id.Split(':')).Where(propVals => propVals.Length == 2))
				{
					// currently we support here only CPs for the same entity
					// e.g. all CPs should for nodes or all for volumes
					EntityName = propVals[0];

				    var targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(EntityName);

					var cp = CustomPropertyMgr.GetCustomPropertyByEntity(targetEntity, propVals[1]);
					var size = 0;

					if (cp.DatabaseColumnType.Equals("nvarchar", StringComparison.OrdinalIgnoreCase))
					{
						size = (cp.DatabaseColumnSize == -1) ? int.MaxValue : cp.DatabaseColumnSize;
					}

					list.Add(new BaseCpInfo
					{
						Name = cp.PropertyName,
						Description = cp.Description,
						Type = cp.PropertyType,
						Size = size,
						EnumValues = cp.Values,
						Table = cp.Table,
						TargetEntity = cp.TargetEntity,
						// we don't load existing assignments on page start
						// AssignedValues = CustomPropertyHelper.GetCpAssignedValues(cp.Table, cp.PropertyName)
					});
				}

				CustomProperties = new List<BaseCpInfo>(list.Take(maxNumber));

				// setup assign value control
				var assignments = new List<CpItemInfo>();
				foreach (var prop in CustomProperties)
				{
					assignments.Add(new CpItemInfo
					{
						Name = prop.Name,
						Type = prop.Type,
						EnumValues = prop.EnumValues,
						Value = string.Empty
					});
				}
			    var assignmentList = new List<CpAssignmentItemInfo>
			    {
			        new CpAssignmentItemInfo
			        {
			            EntityName = CustomProperties[0].TargetEntity,
			            EntityDisplayName = CustomPropertyHelper.GetSourceEntityDisplayName(CustomProperties[0].TargetEntity),
			            CpAssignments = assignments,
			            ObjectUris = new string[0]
			        }
			    };

				AssignValues.SetPropertyAssignments(assignmentList, CustomProperties[0].TargetEntity);
			}
		}
	}

	private List<string> GetRemovedObjectAssignments(Dictionary<string,List<string>> assignmentsBefore, Dictionary<string,List<string>> assignmentsAfter)
	{
		var list = new List<string>();

		if (assignmentsBefore!=null && assignmentsBefore.Count > 0)
		{
			var listAfter = assignmentsAfter.SelectMany(item => item.Value).ToList();

			list.AddRange(from item in assignmentsBefore from uri in item.Value where !listAfter.Contains(uri) select uri);
		}

		return list;
	}

	protected void Submit_Click(object source, EventArgs e)
	{
		if (Page.IsValid)
		{

			try
			{
				var assignments = AssignValues.GetPropertyAssignments();
				var updatedCPs = new List<string>();
				foreach (var assignment in assignments)
				{
				    var assign = assignment;
					foreach (var cp in CustomProperties.Where(cp => cp.Name.Equals(assign.Key)))
					{
						var customProperty = CustomPropertyMgr.GetCustomPropertyByEntity(cp.TargetEntity, cp.Name);
						var list = new List<string>();

						// don't add new value if:
						// 1) value already exists;
						// 2) value haven't any assignments
						foreach (var propVal in assignment.Value.Keys.Where(propVal => !list.Contains(propVal)))
						{
						    if (assignment.Value[propVal] != null && assignment.Value[propVal].Count > 0)
						    {
						        list.Add(propVal);
						    }
						}

						var formattedCpName = CommonHelper.FormatCustomPropertyName(cp.Name);
						// add new restricted values if defined
						CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty, list.ToArray());
						// assign CP values
						CustomPropertyHelper.AssignCpValuesForEntity(formattedCpName, assignment.Value, cp.TargetEntity);
						
						// add CP to updated CP list
					    if (assignment.Value != null)
					    {
					        if (assignment.Value.Any(val => val.Value != null && val.Value.Count > 0))
					        {
					            var targetEntity = CustomPropertyHelper.GetCustomPropertySource(cp.TargetEntity);
					            updatedCPs.Add(string.Format("{0}:{1}", targetEntity, formattedCpName));
					        }
					    }
					}
				}

				// add updated properties into session to show validation message on Manage Custom Properties Page
				if (updatedCPs.Count>0)
					Session["AssignedPropertyIds"] = updatedCPs.ToArray();
			}
			catch (Exception ex)
			{
				_log.Error(string.Format("Assign Custom Properties failed, for prop table '{0}'. ", CustomProperties[0].Table), ex);
			}

			ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");

		}
	}

	protected void Cancel_Click(object source, EventArgs e)
	{
		ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");
	}
}