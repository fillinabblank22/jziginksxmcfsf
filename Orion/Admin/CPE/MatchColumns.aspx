<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CPE/ImportCPWizard.master" AutoEventWireup="true"
    CodeFile="MatchColumns.aspx.cs" Inherits="Orion_Admin_CPE_MatchColumns" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_12 %>" %>

<%@ Register Src="~/Orion/Admin/CPE/Controls/MatchColumnsControl.ascx" TagPrefix="orion"
    TagName="MatchColumns" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder2" runat="server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
    <style type="text/css">
        .errorDescription
        {
            color: red;
        }
        .errorDescription ul
        {
            list-style-type: disc;
            margin-left: 1em;
        }
        #validate
        {
            background-color: white;
            border-top: 1px solid #D1D1D1;
            clear: both;
            margin: 0;
            padding-bottom: 10px;
            padding-top: 10px;
            vertical-align: middle;
        }
        #validate tr td
        {
            padding-left: 7px !important;
        }
        .sw-suggestion-pass
        {
            border: 1px solid #00A000;
        }
        .sw-suggestion-icon-hint
        {
            background-image: url(/orion/images/warning_16x16.gif) !important;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="imgbNext">
        <div class="GroupBox" style="padding-top: 0px;">
            <h2 style="margin-top: 10px;">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_138) %></h2>
            <p style="width: auto!important;">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_139) %>
                <span class="LinkArrow">&raquo;</span> <a id="downloadLink" style="text-decoration: underline;
                    color: #336699;" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(4322)) %>">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_140) %></a></p>
            <div id="Hint" style="position: static; width: 100%;" runat="server">
                <table>
                    <tr>
                        <td>
                            <div class="sw-suggestion">
                                <span class="sw-suggestion-icon sw-suggestion-icon-hint"></span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_141) %>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div runat="server" id="ErrorAdvice" class="sw-suggestion sw-suggestion-fail" visible="False">
                                <span class="sw-suggestion-icon"></span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_40) %>
                            </div>
                            <div runat="server" id="ValidationOk" class="sw-suggestion sw-suggestion-pass" visible="False">
                                <span class="sw-suggestion-icon"></span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_48) %>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <%--<asp:CheckBox ID="CheckBox1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_IB0_142 %>" />--%>
            <orion:MatchColumns runat="server" ID="MatchColumns" />
            <div id="validate">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="cp_validate_leftcolumn" style="vertical-align: middle;">
                            <asp:CheckBox ID="skipValidation" CssClass="spaced_checkbox" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_42 %>" />
                        </td>
                        <td style="padding-top: 4px;">
                            <orion:LocalizableButton ID="ValidateInput" runat="server" DisplayType="Small" LocalizedText="CustomText"
                                Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_41 %>" OnClick="Validate_Clicked" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <asp:CustomValidator ID="Validator" runat="server" ErrorMessage="" OnServerValidate="ValidateUploadFile"
            Display="Dynamic"></asp:CustomValidator>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="LocalizableButton1" OnClick="Back_Click"
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Import"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
