<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/LimitedAdminPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Orion_CPE_Default" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_4 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <style type="text/css">
        .error
        {
            border: 2px solid red;
        }
        #gridCell div#Grid
        {
            width: 100%;
        }
        #adminContent td
        {
            padding: 0px 0px 0px 0px !important;
            vertical-align: middle;
        }
        .no-border .x-table-layout-ct, .panel-no-border .x-panel-body-noheader
        {
            border: none !important;
        }
        .panel-border
        {
            border: solid 1px #D0D0D0 !important;
        }
        .hide-header .x-grid3-header
        {
            display: none;
        }
        .x-tip {
            width: auto !important;
        }
        .x-tip-body 
        {
            white-space: nowrap !important;
            width: auto !important;
        }
        .searchfield
         {
         	background-image:url(/Orion/images/search_button.gif) !important; 
         }
        .sw-suggestion-pass
        {
            border: 1px solid #00A000;
        }
        .sw-suggestion-pass a
        {
            font-weight: normal;
            color: #336699;
            text-decoration: underline; 
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionAGCustomProperties" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 style="font-size:large!important;"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div style="padding: 1em 0 1em 0;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_5) %>
     </div>
     <div id="Hint" style="position: static; width: 100%;" runat="server">
                <table>
                    <tr>
                        <td>
                            <div runat="server" id="ValidationOk" class="sw-suggestion sw-suggestion-pass" visible="false" >
                                <span class="sw-suggestion-icon"></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
            <asp:ServiceReference Path="/Orion/Services/WebAdmin.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <orion:Include runat="server" File="Admin/js/ManageCPGrid.js" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <input type="hidden" name="sessionInput" id="sessionInput" value='<%= DefaultSanitizer.SanitizeHtml(Session["CustomPropertyIds"]) %>' />
    <input type="hidden" name="CustomProperties_MaxCPToAssign" id="CustomProperties_MaxCPToAssign" value='<%= DefaultSanitizer.SanitizeHtml(ConfigurationManager.AppSettings["CPMaxNumberToAssign"].ToString()) %>' />
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" style="padding-top:4px!important">
                <input type="hidden" name="CustomProperties_PageSize" id="CustomProperties_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("CustomProperties_PageSize")) %>' />
                <input type="hidden" name="CustomProperties_AllowAdmin" id="CustomProperties_AllowAdmin" value='<%= this.Profile.AllowAdmin %>' />
                <div id="CustomPropertiesGrid">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
