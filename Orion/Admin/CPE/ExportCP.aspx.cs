using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Logging;

public partial class Orion_CPE_ExportCP : System.Web.UI.Page
{
    private static Log log = new Log();

    protected List<string> ObjectsIds
    {
        get
        {
            if (ViewState["ObjectsIds"] == null)
            {
                ViewState["ObjectsIds"] = new List<string>();
            }
            return ViewState["ObjectsIds"] as List<string>;
        }
        set
        {
            ViewState["ObjectsIds"] = value;
        }
    }

    protected string EntityDisplayName
    {
        get
        {
            if (ViewState["EntityDisplayName"] == null)
            {
                ViewState["EntityDisplayName"] = string.Empty;
            }
            return (string)ViewState["EntityDisplayName"];
        }
        set
        {
            ViewState["EntityDisplayName"] = value;
        }
    }

    private string PropertyEntityType
    {
        get
        {
            if (ViewState["PropertyEntityType"] == null)
            {
                ViewState["PropertyEntityType"] = "Orion.Nodes";
            }
            return (string)ViewState["PropertyEntityType"];
        }
        set
        {
            ViewState["PropertyEntityType"] = value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            // We need to rebuild the dynamically added ancestory nodes on every post back.
            parentObjectColumnList.Ancestors = LoadAncestoryNodes(PropertyEntityType);

            return;
        }

        var customProperties = Request["CustomPropertyIds"];

        string sourceEntity = null;

        List<string> columnList = null;
        List<string> netObjectsIds = null;

        if (customProperties != null)
        {
            columnList = new List<string>();
            var ids = customProperties.Split(',');
            foreach (var propVals in ids.Select(id => id.Split(':')).Where(propVals => propVals.Length == 2))
            {
                sourceEntity = propVals[0];
                columnList.Add(propVals[1]);
            }
        }
        else
        {
            sourceEntity = Request["EntityType"] ?? "Orion.Nodes";

            var ids = Request["NetObjectsIds"];
            if (!string.IsNullOrEmpty(ids))
            {
                netObjectsIds = ids.Split(',').ToList();
            }
        }

        PropertyEntityType = sourceEntity;

        var dp = CustomPropertyHelper.GetCPExportColumns(sourceEntity);
        var ep = CustomPropertyHelper.GetEntityProperties(sourceEntity);
        if (WorldMapDAL.AvailableEntities.ContainsKey(sourceEntity))
        {
            ep.Rows.Add("WorldMapPoint.Latitude", string.Format("{0} {1}", Resources.CoreWebContent.WEBDATA_PCC_WM_LATITIDE, Resources.CoreWebContent.WEBDATA_AY0_89), typeof(Single), true);
            ep.Rows.Add("WorldMapPoint.Longitude", string.Format("{0} {1}", Resources.CoreWebContent.WEBDATA_PCC_WM_LONGITUDE, Resources.CoreWebContent.WEBDATA_AY0_89), true);
        }
        EntityDisplayName = CustomPropertyHelper.GetEntityDisplayName(sourceEntity);

        parentObjectColumnList.Ancestors = LoadAncestoryNodes(sourceEntity);
        parentObjectColumnList.DefaultColumnsList = dp;
        parentObjectColumnList.DataSource = ep;

        customPropertiesColumnList.SelectedCP = columnList;
        customPropertiesColumnList.DataSource = CustomPropertyHelper.GetCustomPropertiesBySource(sourceEntity);

        selObject.ParentRowId = 0;
        selObject.EntityType = PropertyEntityType;
        selObject.EntityDisplayName = EntityDisplayName;

        ObjectsIds = new List<string>(GetMembers(PropertyEntityType, netObjectsIds));

        selObject.Assignments = ObjectsIds;
    }

    private static IDictionary<string, IList<NodeProperty>> LoadAncestoryNodes(string entity)
    {
        var parentEntity = EntityHelper.GetSwisEntityParent(entity);
        if (parentEntity == entity)
        {
            return new Dictionary<string, IList<NodeProperty>>();
        }

        var ancestors = LoadAncestoryNodes(parentEntity);

        var strippedEntity = EntityHelper.GetEntityName(parentEntity);

        ancestors.Add(strippedEntity, LoadNodeProperties(strippedEntity, CustomPropertyHelper.GetEntityProperties(parentEntity)));

        return ancestors;
    }

    private static IList<NodeProperty> LoadNodeProperties(string entity, DataTable entityProperties)
    {
        return (from DataRow dataRow in entityProperties.Rows select new NodeProperty { ColumnDisplayName = dataRow["ColumnDisplayName"].ToString(), ColumnName = string.Format("{0}.{1}", entity, dataRow["ColumnName"]), Entity = entity, Type = dataRow["Type"].ToString() }).ToList();
    }

    private IEnumerable<string> GetMembers(string entityName, IEnumerable<string> ids = null)
    {
        log.Debug("Getting members for entity " + entityName);
        var whereCondition = string.Empty;

        if (ids != null)
        {
            var idsString = string.Empty;
            var coma = string.Empty;
            var idColumnName = GetIdColumn(entityName);
            if (!string.IsNullOrEmpty(idColumnName))
            {
                foreach (var id in ids)
                {
                    idsString += coma + id;
                    coma = ",";
                }
                whereCondition = string.Format(" Where {0} in ({1})", idColumnName, idsString);
            }
        }

        string queryString = string.Format(@"SELECT Uri FROM {0} {1}", entityName, whereCondition);

        DataTable uriDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                log.DebugFormat("Getting members with query: {0}", queryString);
                uriDataTable = service.Query(queryString);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);

            throw;
        }

        if (uriDataTable == null || uriDataTable.Rows.Count == 0)
        {
            return new string[0];
        }

        string[] arr = new string[uriDataTable.Rows.Count];
        for (int i = 0; i < uriDataTable.Rows.Count; i++)
        {
            arr[i] = uriDataTable.Rows[i][0].ToString();
        }

        return arr;
    }

    private static string GetIdColumn(string entityName)
    {
        var sql = string.Format(@"SELECT Top 1 OrionIdColumn From {0}", entityName);

        try
        {
            using (var service = InformationServiceProxy.CreateV3())
            {
                var res = service.Query(sql);
                return res.Rows.Count > 0 ? res.Rows[0][0].ToString() : string.Empty;
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    protected void Cancel_Click(object source, EventArgs e)
    {
        ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");
    }

    protected void selObject_SelectObjectChanged(List<string> assignments, int rowId)
    {
        ObjectsIds = assignments;
    }

    protected void RequiredNetObjectsValidation(object source, ServerValidateEventArgs args)
    {
        args.IsValid = !(ObjectsIds == null || ObjectsIds.Count == 0);
    }

    protected void Export_Click(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        IList<string> parentColumns = parentObjectColumnList.GetSelectedColumns().ToList(); // nested entity properties, IP_Address or Caption for example
        string parentEntity = EntityHelper.GetSwisEntityParent(PropertyEntityType);
        if (parentEntity != PropertyEntityType && parentObjectColumnList.AnythingFromParent) // PropertyEntityType its entity, Orion.Nodes for example
        {
            // yes, we have parent, and something is checked from it - so we should add keycollumns
            string parentName = EntityHelper.GetEntityName(parentEntity);

            DataTable keyProperties = CustomPropertyHelper.GetEntityKeyProperties(parentEntity);
            if (keyProperties != null && keyProperties.Rows.Count > 0)
            {
                foreach (DataRow dataRow in keyProperties.Rows)
                {
                    string parentColumnName = parentName + "." + dataRow[0];
                    if (!parentColumns.Contains(parentColumnName))
                        parentColumns = new[] { parentColumnName }.Concat(parentColumns).ToList();
                }
            }
        }
        IList<string> cpColumns = customPropertiesColumnList.GetSelectedColumns();

        StringBuilder sb = new StringBuilder("Select o.Uri, ");
        foreach (var cpColumn in parentColumns)
        {
            if (cpColumn.StartsWith("WorldMapPoint.", StringComparison.OrdinalIgnoreCase))
                sb.AppendLine("o." + cpColumn + " AS [" + cpColumn + "],");
            else
                sb.AppendLine("o." + cpColumn + ",");
        }

        foreach (var cpColumn in cpColumns)
        {
            sb.AppendLine("o.CustomProperties." + cpColumn + ",");
        }

        sb.Remove(sb.ToString().LastIndexOf(","), 1);
        sb.AppendLine("from " + PropertyEntityType + " o");

        DataTable cpDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                log.DebugFormat("Getting custom properties DataTable with query: {0}", sb.ToString());
                cpDataTable = service.Query(sb.ToString());

                // [pc] patch column names to match query. if we're including 'parent' properties, we could have collisions

                for (int i = 0, imax = parentColumns.Count; i < imax; ++i)
                {
                    var col = cpDataTable.Columns[i + 1];
                    col.ColumnName = parentColumns[i];
                }
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        // todo: this is inefficient. We're selecting all entities, processing them, and then filtering them. filtering should come as part of the selection.
        var detector = new CustomPropertyChangeDetector();
        detector.AddChangeDetectionHash(cpDataTable, parentColumns.Count + 1 /* add 1 for the Uri */, cpColumns);

        ExportTableToFile(cpDataTable, exportFormat.SelectedFormat);
        ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");
    }

    // CSV rules: http://en.wikipedia.org/wiki/Comma-separated_values#Basic_rules
    // From the rules:
    // 1. if the data has quote, escape the quote in the data
    // 2. if the data contains the delimiter (in our case ','), double-quote it
    // 3. if the data contains the new-line, double-quote it.

    private string GetCSVStream(DataTable dt, char separator)
    {
        log.DebugFormat("Bulding string from datatable with {0} rows ...", dt.Rows.Count);
        var regex = new Regex(@"\r\n?|\n");
        var sep = separator.ToString();
        StringBuilder sb = new StringBuilder();

        var columnNames = (dt.Columns.Cast<DataColumn>().Where(column => column.ColumnName != "Uri").Select(column => column.ColumnName)).ToArray();
        sb.AppendLine(string.Join(sep, columnNames));

        foreach (DataRow row in dt.Rows)
        {
            //string keyColl = row["Uri"].ToString();
            //if (ObjectsIds.Contains(keyColl))
            //{
            //skip first column == Uri
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                string value = string.Format(CultureInfo.InvariantCulture, "{0}", row[i]);
                value = regex.Replace(value, " ");
                bool containsSlash = value.Contains("\"");
                bool mustQuote = (value.Contains(sep) || containsSlash);

                if (containsSlash)
                    value = value.Replace("\"", "\"\"");

                string append = mustQuote
                               ? string.Format("\"{0}\"", value)
                               : value;

                sb.Append(append);

                if (i == dt.Columns.Count - 1)
                    sb.AppendLine("");
                else
                    sb.Append(sep);
                //}
            }
        }
        return sb.ToString();
    }

    private DataTable GetFilteredDataTable(DataTable original, List<string> selectedObjectsList)
    {
        if (original == null || original.Rows.Count == 0)
            return original;

        //nothing selected
        if (selectedObjectsList == null || selectedObjectsList.Count == 0)
            return original.Clone();

        DataTable result = original.Clone();
        bool all = original.Rows.Count == selectedObjectsList.Count;

        foreach (DataRow dr in original.Rows)
        {
            string keyColl = dr["Uri"].ToString();

            if (all || selectedObjectsList.Contains(keyColl))
            {
                result.Rows.Add(dr.ItemArray);
            }
        }

        result.Columns.Remove("Uri");

        return result;
    }

    private void ExportTableToFile(DataTable dt, string selectedFormat)
    {
        if (dt == null)
            return;

        log.Debug("Export started...");
        string fileName = "exportCP.";
        string stream = string.Empty;
        string contextType = null;
        switch (selectedFormat)
        {
            case "0":
                fileName += "csv";
                stream = GetCSVStream(GetFilteredDataTable(dt, ObjectsIds), ',');
                contextType = "text/csv";
                break;
            case "1":
                fileName += "txt";
                stream = GetCSVStream(GetFilteredDataTable(dt, ObjectsIds), '\t');
                contextType = "text/plain";
                break;
            case "2":
                fileName += "html";
                stream = GetExcelStream(GetFilteredDataTable(dt, ObjectsIds));
                contextType = "text/html";
                break;
            case "3":
                fileName += "xls";
                var dataTable = GetFilteredDataTable(dt, ObjectsIds);
                var fileBytes = CustomPropertyHelper.GetExcelBytes(dataTable);
                if (fileBytes != null)
                {
                    WriteBytesToResponse(fileBytes, fileName);
                    return;
                }
                else
                {
                    stream = GetExcelStream(dataTable);
                    contextType = "application/vnd.ms-excel";
                    break;
                }
        }

        log.DebugFormat("Got stream...Pushing to reponse. String lenght: {0}", string.IsNullOrEmpty(stream) ? 0 : stream.Length);

        if (string.IsNullOrEmpty(stream))
        {
            //throw something?;
            return;
        }

        Response.Clear();
        // add the header that specifies the default filename for the Download/SaveAs dialog 
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
        //Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentType = contextType;

        Response.ContentEncoding = Encoding.Unicode;
        Response.BinaryWrite(Encoding.Unicode.GetPreamble());

        // send the file stream to the client
        //Response.TransmitFile("exportCP.csv");
        Response.Write(stream);
        Response.Flush();
        Response.End();
    }

    private void WriteBytesToResponse(byte[] bytes, string fileName)
    {
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
        Response.ContentType = "text/plain";

        Response.ContentEncoding = Encoding.Unicode;
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
    }


    public string GetExcelStream(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            StringWriter tw = new StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
            DataGrid dgGrid = new DataGrid();
            dgGrid.DataSource = dt;
            dgGrid.DataBind();

            //Get the HTML for the control.
            dgGrid.RenderControl(hw);
            //Write the HTML back to the browser.

            return tw.ToString();
        }

        return null;
    }
}