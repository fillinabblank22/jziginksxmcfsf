using System;
using System.Data;
using System.Web;
using System.IO;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_CPE_ImportCP : ImportCustomPropertyWizardBase
{
	protected override void OnInit(EventArgs e)
	{
		Workflow.FirstStep();
		base.OnInit(e);
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		Response.Cache.SetCacheability(HttpCacheability.NoCache);

		using (var dal = new CustomPropertyDAL())
		{
			Entities.DataSource = dal.GetAvailableEntities();
		}

		Entities.DataTextField = "DisplayNamePlural";
		Entities.DataValueField = "SourceType";

		Entities.DataBind();
		Entities.SelectedValue = "Orion.Nodes";

		if (!IsPostBack)
		{
			if (Workflow.ImportFileDataTable != null && !string.IsNullOrEmpty(Workflow.ImportFilePath))
			{
				txtFile.Value = Workflow.ImportFilePath;
				Entities.SelectedValue = Workflow.SelectedEntity;
			}
			else
			{
				var entityType = Request["EntityType"];
				if (!string.IsNullOrEmpty(entityType))
				{
					Entities.SelectedValue = entityType;
				}
			}
		}
	}

	protected override bool Next()
	{
		//first run, try to get table from selected file
		DataTable dt;
		if (ValidateFile(out dt))
		{
			Workflow.ImportFileDataTable = dt;
			Workflow.ImportFilePath = txtFile.Value;
			Workflow.SelectedEntity = Entities.SelectedValue;
			return base.Next();
		}

		// clicked forth and back few times - so stream already lost but maybe we have stored before?
		if (Workflow.ImportFileDataTable != null && Workflow.ImportFilePath.Equals(txtFile.Value))
		{
			// we've already have all data within the Workflow - Next/Back behaviour
			Workflow.SelectedEntity = Entities.SelectedValue;
			return base.Next();
		}

		hiddenData.Value = Resources.CoreWebContent.WEBDATA_YK0_18;
		return false;
	}

	protected override void Cancel()
	{
		Workflow.Reset();
		ReferrerRedirectorBase.Return("/Orion/Admin/CPE/Default.aspx");
	}

	private bool ValidateFile(out DataTable dt)
    {
        _log.Debug("Validating file " + fileDialog.FileName);

        dt = null;
        var extension = Path.GetExtension(fileDialog.FileName);

	    var parser = new CustomPropertyFileParser();

        switch (extension)
        {
            case ".txt":
                dt = parser.GetTableFromTextFormat(fileDialog.FileContent, '\t');
                break;
            case ".csv":
                dt = parser.GetTableFromTextFormat(fileDialog.FileContent, ',');
                break;
            case ".xls":
            case ".xlsx":
                dt = CustomPropertyHelper.GetExcelData(fileDialog.FileContent);
                break;
            default:
                return false; // what whichcraft is this? magic file extension?
        }

        bool ret = dt != null && dt.Rows.Count > 0;

        if (ret && cbRemoveUnchanged.Checked)
        {
            var detector = new CustomPropertyChangeDetector();
            detector.RemoveUnchangedRows(dt);
        }
        else
        {
            _log.WarnFormat("Validating file {0} failed!", fileDialog.FileName);
        }

        return ret;
	}
}
