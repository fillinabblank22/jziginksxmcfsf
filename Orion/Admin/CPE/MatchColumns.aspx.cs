﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.CPE;

public partial class Orion_Admin_CPE_MatchColumns : ImportCustomPropertyWizardBase
{
	#region properties
	protected static DataTable MatchingTable
	{
		get;
		set;
	}
	protected static DataTable ImportsTable
	{
		get;
		set;
	}
	#endregion

	protected override void OnInit(EventArgs e)
	{
		if (Workflow.CurrentStepType != ImportCustomPropertyWorkflow.StepType.MatchColumns)
		{
			GotoFirstStep();
		}

		base.OnInit(e);

        MatchColumns.SourceEntityName = Workflow.SelectedEntity;
		MatchColumns.DataSource = Workflow.ImportFileDataTable;
	}

	protected override void Next_Click(object sender, EventArgs e)
	{
        if (Request["__EVENTARGUMENT"].Equals("IMPORT_ERROR", StringComparison.OrdinalIgnoreCase))
        {
            Session["AssignedPropertyIds"] = null;
            base.Next_Click(sender, e);
        }
        else if (Request["__EVENTARGUMENT"].Equals("IMPORT_FINISHED", StringComparison.OrdinalIgnoreCase))
		{
			var updatedCps = new List<string>();
			foreach (DataRow row in ImportsTable.Rows)
			{
				updatedCps.Add(string.Format("{0}:{1}", Workflow.SelectedEntity, row["TargetColumn"]));
			}

			if (updatedCps.Count > 0)
				Session["AssignedPropertyIds"] = updatedCps.ToArray();
			

			base.Next_Click(sender, e);
		}
		else if (Page.IsValid)
		{
			MatchingTable = MatchColumns.GetColumnMappingTable(true);
			ImportsTable = MatchColumns.GetColumnMappingTable(false);
			_log.Debug("Import data started...");

			var scriptBlock = @"
$(function () {
				Ext.MessageBox.show({
                    msg: '" + Resources.CoreWebContent.WEBDATA_IB0_149 + @"',
                    progressText: String.format('" + Resources.CoreWebContent.WEBDATA_IB0_167 + @"', 0),
                    width: 300,
                    progress: true,
                    closable: false
                });

                var fnShowError = function(){
				                Ext.MessageBox.hide();
				                Ext.Msg.show({
					                title: '" + Resources.CoreWebContent.WEBDATA_IB0_151 + @"',  
					                msg: errorMessages.join('<br />'),  
					                width: 400,
					                buttons: Ext.Msg.OK,  
					                icon: Ext.Msg.INFO, // <- customized icon  
					                fn: function(){
						                __doPostBack('" + imgbNext.UniqueID + @"', !success ? 'IMPORT_ERROR' : 'IMPORT_FINISHED');
					                }
				                });
                    };

                var fnHandleError = function(error){
                        errorMessages[fail] = error.ExceptionMessage ? error.ExceptionMessage : error.get_message();
                        fail++;
                        index++;

                        if (success + fail >= count) {
                            fnShowError();
                            return;
                        }

                        importData();
                    };

                var count = Math.min(100, " + Workflow.ImportFileDataTable.Rows.Count + @");
                var success=0;
                var fail=0;
                var errorMessages=new Array();
                var index = 0;
                var importData = function() {
                    PageMethods.ImportData(index, function(result){
		                if (!result.Success)
		                {
			                fnHandleError(result);
			                return;
		                }

		                success++;
		                if (success + fail >= count) {
			                if(fail>0){
                                fnShowError();
                                return;
			                }
			                else{
				                __doPostBack('" + imgbNext.UniqueID + @"', 'IMPORT_FINISHED');
			                }
		                } else {
			                if (count != 0) {
			                    var p = success / count;
			                    Ext.MessageBox.updateProgress(p, String.format('" + Resources.CoreWebContent.WEBDATA_IB0_167 + @"', success));
                                index++;
                                importData();
			                }
		                }
	                }, function(error){
		                fnHandleError(error);
                    });
                };
                importData();
});
";

			ClientScript.RegisterStartupScript(GetType(), "progressDlg", scriptBlock, true);
		}
	}

	protected void ValidateUploadFile(object source, ServerValidateEventArgs args)
	{
		bool isValid = true;
		ResetErrorControls();

		_log.Debug("Checking if we can parse data...");//stage1 - check we can parse data
		List<ImportCPColumnsInfo> listSelected = MatchColumns.GetSelectedColumns();
		if (!TryParseSelected(listSelected))
		{
			ErrorAdvice.Visible = true;
			isValid = false;
		}

		//stage2 - checking if we are trying to add some new restricted values
		_log.Debug("Checking if we are trying to add some new restricted values...");
		if (!CheckRestrictedValues(listSelected))
		{
			ErrorAdvice.Visible = true;
			isValid = false;
		}

		if (!isValid)
			MatchColumns.UpdateRepeaterWithErrors(_errors);

		args.IsValid = isValid || skipValidation.Checked;
	}

	private bool CheckRestrictedValues(List<ImportCPColumnsInfo> selected)
	{
		if (selected == null || selected.Count == 0)
			return false;

		bool result = true;
		string targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(Workflow.SelectedEntity);
		
		foreach (ImportCPColumnsInfo matchInfo in selected)
		{
			if (matchInfo.MatchType)
				continue;

		    ImportCPColumnsInfo info = matchInfo;
		    string[] strValues = Workflow.ImportFileDataTable.AsEnumerable().Select(s => s.Field<string>(info.SourceColumnName)).Distinct().ToArray();

			if (strValues.Length ==0)
				continue;

            CustomProperty cp = CustomPropertyMgr.GetCustomPropertyByEntity(targetEntity, matchInfo.TargetColumnName);

			if (cp == null || cp.Values == null || cp.Values.Length == 0)
				continue;

			List<string> listOfMissedValues = new List<string>();

			foreach (string strValue in strValues)
			{
				if (string.IsNullOrEmpty(strValue))
					continue;

				if (!cp.Values.Contains(strValue))
				{
					object cValue = null;

					if (!matchInfo.TargetColumnType.Equals("System.String"))
					{
						if (!_converters.ContainsKey(matchInfo.TargetColumnType))
							_converters[matchInfo.TargetColumnType] = TypeDescriptor.GetConverter(Type.GetType(matchInfo.TargetColumnType));

						try
						{
							cValue = _converters[matchInfo.TargetColumnType].ConvertFromInvariantString(strValue);

							if (cValue != null && cp.Values.Contains(_converters[matchInfo.TargetColumnType].ConvertToInvariantString(cValue)))
								continue;
						}
						catch (Exception ex)
						{
							//cannot convert it
						}
					}

					listOfMissedValues.Add(cValue == null ? strValue : cValue.ToString());
				
					result = false;
				}
            }

            StringBuilder sb = new StringBuilder();
			string href = null;
			if (listOfMissedValues.Count > 0)
			{
				href = string.Format("<span class=\"LinkArrow\" style=\"color: #336699;\">&raquo;</span>&nbsp;<a id=\"downloadLink\" style=\"text-decoration: underline;color: #336699;\" href=\"/Orion/Admin/CPE/EditCP.aspx?CustomPropertyId={0}:{1}&NewRestrictedValues={4}\" target=\"_blank\">{2}</a>&nbsp;<span style=\"font-size:11px; color:#979797\">{3}</span>", Workflow.SelectedEntity, matchInfo.TargetColumnName, Resources.CoreWebContent.WEBDATA_SO0_45, Resources.CoreWebContent.WEBDATA_SO0_44, HttpUtility.UrlEncode(string.Join(";", listOfMissedValues.ToArray())));
				sb.AppendLine(string.Format("<li>{0}</li>", string.Join("</li><li>", listOfMissedValues.ToArray())));
			}

			if (!result && sb.Length > 0)
			{
				_errors[matchInfo.SourceColumnName] = string.Format("<div class=\"errorDescription\">{0}<ul>{1}</ul>{2}</div>", Resources.CoreWebContent.WEBDATA_SO0_43, sb.ToString(), href);

				_skipedValues[matchInfo.SourceColumnName] = listOfMissedValues;
			}
		}

		return result;
	}

    private bool TryParseSelected(List<ImportCPColumnsInfo> selected)
    {
        if (selected == null || selected.Count == 0)
            return false;

        bool result = true;
        var targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(Workflow.SelectedEntity);
        foreach (ImportCPColumnsInfo matchInfo in selected)
        {
            var cp = CustomPropertyMgr.GetCustomPropertyByEntity(targetEntity, matchInfo.TargetColumnName);

            if (matchInfo.TargetColumnType.Equals("System.String") && (cp == null || cp.DatabaseColumnLength == -1))
                continue;

            StringBuilder sb = new StringBuilder();
            ImportCPColumnsInfo info = matchInfo;
            string[] strValues =
                Workflow.ImportFileDataTable.AsEnumerable()
                    .Select(s => s.Field<string>(info.SourceColumnName))
                    .ToArray();
            int i = 0;

            foreach (string val in strValues)
            {
                i++;

                if (cp != null && cp.Mandatory && string.IsNullOrEmpty(val))
                {
                    result = false;

                    if (!_skipedValues.ContainsKey(matchInfo.SourceColumnName))
                    {
                        _skipedValues[matchInfo.SourceColumnName] = new List<string>();
                    }
                    if (!_skipedValues[matchInfo.SourceColumnName].Contains(val))
                    {
                        _skipedValues[matchInfo.SourceColumnName].Add(val);
                        sb.AppendLine(string.Format("<li>{0}</li>", Resources.CoreWebContent.WEBDATA_OM0_5));
                    }
                }

                if (string.IsNullOrEmpty(val)) //having DBNull
                    continue;

                if (!ValidateDataValue(val, matchInfo.TargetColumnType))
                {
                    sb.AppendLine(
                        string.Format("<li>" +
                                      string.Format(Resources.CoreWebContent.WEBDATA_SO0_46, i, val,
                                          MapTypeToDisplayString(matchInfo.TargetColumnType))) + "</li>");
                    result = false;

                    if (!_skipedValues.ContainsKey(matchInfo.SourceColumnName))
                        _skipedValues[matchInfo.SourceColumnName] = new List<string>();

                    if (!_skipedValues[matchInfo.SourceColumnName].Contains(val))
                        _skipedValues[matchInfo.SourceColumnName].Add(val);
                }

                if (cp != null && matchInfo.TargetColumnType.Equals("System.String") &&
                    val.Length > cp.DatabaseColumnLength)
                {
                    sb.AppendLine(
                        string.Format("<li>" +
                                      string.Format(Resources.CoreWebContent.WEBDATA_IB0_194, i, val,
                                          cp.DatabaseColumnLength)) + "</li>");
                    result = false;

                    if (!_skipedValues.ContainsKey(matchInfo.SourceColumnName))
                        _skipedValues[matchInfo.SourceColumnName] = new List<string>();

                    if (!_skipedValues[matchInfo.SourceColumnName].Contains(val))
                        _skipedValues[matchInfo.SourceColumnName].Add(val);
                }

                if (matchInfo.TargetColumnName.StartsWith("WorldMapPoint.", StringComparison.OrdinalIgnoreCase))
                {
                    //Row {0} "{1}" value from spreadsheet for{2} should be between {3} and {4}
                    if (matchInfo.TargetColumnName.Equals("WorldMapPoint.Latitude", StringComparison.OrdinalIgnoreCase) && !ValidateLimitedFloatValue(val, -90, 90))
                    {
                        result = false;
                        sb.AppendLine("<li>" + string.Format(Resources.CoreWebContent.WEBDATA_SO0_181, i, val, "-90", "90") + "</li>");
                        if (!_skipedValues.ContainsKey(matchInfo.SourceColumnName))
                            _skipedValues[matchInfo.SourceColumnName] = new List<string>();

                        if (!_skipedValues[matchInfo.SourceColumnName].Contains(val))
                            _skipedValues[matchInfo.SourceColumnName].Add(val);
                    }
                    else if (
                        matchInfo.TargetColumnName.Equals("WorldMapPoint.Longitude", StringComparison.OrdinalIgnoreCase) && !ValidateLimitedFloatValue(val, -180, 180))
                    {
                        result = false;
                        sb.AppendLine("<li>" + string.Format(Resources.CoreWebContent.WEBDATA_SO0_181, i, val, "-180", "180") + "</li>");
                        if (!_skipedValues.ContainsKey(matchInfo.SourceColumnName))
                            _skipedValues[matchInfo.SourceColumnName] = new List<string>();

                        if (!_skipedValues[matchInfo.SourceColumnName].Contains(val))
                            _skipedValues[matchInfo.SourceColumnName].Add(val);
                    }
                }
            }

            if (!result && sb.Length > 0)
                _errors[matchInfo.SourceColumnName] = "<div class=\"errorDescription\"><ul>" + sb.ToString() + "</ul></div>";
        }

        return result;
    }

    private readonly Dictionary<string, string> _errors = new Dictionary<string, string>();
	private readonly static Dictionary<string, List<string>> _skipedValues = new Dictionary<string, List<string>>();
	private readonly Dictionary<string, TypeConverter> _converters = new Dictionary<string, TypeConverter>();

    private bool ValidateLimitedFloatValue(string o, float minValue, float maxValue)
    {
        float res;
        if (!float.TryParse(o, out res))
            return false;

        return res >= minValue && res <= maxValue;
    }

	private bool ValidateDataValue(string o, string targetColumnType)
	{
		if (targetColumnType.Equals("System.String"))
			return true;

		object cValue;
		try
		{
			//var cValue = Convert.ChangeType(o, Type.GetType(targetColumnType));
			
			if (!_converters.ContainsKey(targetColumnType))
				_converters[targetColumnType] = TypeDescriptor.GetConverter(Type.GetType(targetColumnType));

			cValue = _converters[targetColumnType].ConvertFromInvariantString(o);
			return true;
		}
		catch (Exception)
		{
			try 
			{
				// something we cannot parse - it can be nonInvariant string or Boolean yes/no from old CPE
				if (targetColumnType.Equals("System.Boolean"))
				{
					if (o.Equals("Yes", StringComparison.OrdinalIgnoreCase))
						return true;
					else if (o.Equals("No", StringComparison.OrdinalIgnoreCase))
						return true;
				}
				else
					cValue = Convert.ChangeType(o.Trim('"'), Type.GetType(targetColumnType), CultureInfo.InstalledUICulture);

				return true;
			}
			catch (Exception) {
				
			}
		}

		return false;
	}

	protected void ResetErrorControls()
	{
		ErrorAdvice.Visible = false;
		ValidationOk.Visible = false;
		_errors.Clear();
		_skipedValues.Clear();
	}

    public class CPEImportResult
    {
        public CPEImportResult(bool success, string exceptionMessage)
        {
            Success = success;
            ExceptionMessage = exceptionMessage;
        }

        public bool Success { get; }
        public string ExceptionMessage { get; }
    }


    [ScriptMethod]
    [WebMethod(EnableSession = true)]
    public static CPEImportResult ImportData(int index)
    {
        var successResult = new CPEImportResult(true, null);
        DataTable mTable;
        DataTable iTable;

        if (_skipedValues.Count > 0)
        {
            mTable = MatchingTable.Clone();
            iTable = ImportsTable.Clone();

            var columnsToIgnore = new List<string>();
            foreach (KeyValuePair<string, List<string>> skipedColumn in _skipedValues)
            {
                if (skipedColumn.Value.Contains(Workflow.ImportFileDataTable.Rows[index][skipedColumn.Key].ToString()))
                {
                    columnsToIgnore.Add(skipedColumn.Key);
                }
            }

            // ignore matching or importing column value if there is an error
            foreach (var row in MatchingTable.Rows.Cast<DataRow>().Where(row => !columnsToIgnore.Contains(row["SourceColumn"].ToString())))
            {
                mTable.Rows.Add(row["SourceColumn"], row["TargetColumn"], row["TargetColumnType"]);
            }

            // if no columns to match then skip importing of this row
            if (mTable.Rows.Count == 0)
                return successResult;

            foreach (var row in ImportsTable.Rows.Cast<DataRow>().Where(row => !columnsToIgnore.Contains(row["SourceColumn"].ToString())))
            {
                iTable.Rows.Add(row["SourceColumn"], row["TargetColumn"], row["TargetColumnType"]);
            }

            // if no columns to import then skip importing of this row
            if (iTable.Rows.Count == 0) return successResult;
        }
        else
        {
            mTable = MatchingTable;
            iTable = ImportsTable;
        }

        var rowsCount = Workflow.ImportFileDataTable.Rows.Count;
        if (Workflow.ImportFileDataTable.Rows.Count == 0)
        {
            // todo: tell the user which rows were modified on success. requires changing the return type of this method.
            return new CPEImportResult (false, Resources.CoreWebContent.WEBDATA_YK0_93);
        }

        if (rowsCount <= 100)
        {
            CustomPropertyHelper.ImportCPDataRow(Workflow.ImportFileDataTable.Rows[index], mTable, iTable,
                                                 Workflow.SelectedEntity);
        }
        else
        {
            var step = rowsCount/100;
            var start = index*step;
            var end = (index != 99) ? start + step : Workflow.ImportFileDataTable.Rows.Count;
            for (int i = start; i < end; i++)
            {
                CustomPropertyHelper.ImportCPDataRow(Workflow.ImportFileDataTable.Rows[i], mTable, iTable,
                                                     Workflow.SelectedEntity);
            }
        }

        return successResult;
    }

    protected void Validate_Clicked(object sender, EventArgs e)
	{
		if (Page.IsValid)
			ValidationOk.Visible = true;
		else 
			ValidationOk.Visible = false;
	}

	private static string MapTypeToDisplayString(string typeName)
	{
		switch (typeName)
		{
			case "System.DateTime":
				return Resources.CoreWebContent.WEBCODE_SO0_32;
			case "System.Boolean":
				return Resources.CoreWebContent.WEBCODE_SO0_33;
			case "System.Int64":
			case "System.Int32":
			case "System.Int16":
				return Resources.CoreWebContent.WEBCODE_SO0_30;
			case "System.Double":
			case "System.Single":
				return Resources.CoreWebContent.WEBCODE_SO0_31;
			case "System.String":
				return Resources.CoreWebContent.WEBCODE_SO0_29;
			default:
				return typeName.Replace("System.", string.Empty);
		}
	}
}
