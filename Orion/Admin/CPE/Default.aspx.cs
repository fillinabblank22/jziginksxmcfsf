using System;
using System.Linq;
using System.Text;
using System.Web;

using System.Web.Services;
using System.Web.UI;

public partial class Orion_CPE_Default : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
		// in case we have "AssignedPropertyIds" value within the session
		// we're building and show validation message
		if (Session["AssignedPropertyIds"] != null)
		{
			var assignedPropertyIds = (string[])Session["AssignedPropertyIds"];

			if (assignedPropertyIds != null && assignedPropertyIds.Length > 0)
			{
				var entityName = "Orion.Nodes";
				var separator = string.Empty;
				var columnsString = string.Empty;
				// build updated column names list 
				foreach (var parts in assignedPropertyIds.Select(assignProp => assignProp.Split(':')).Where(parts => parts.Length == 2)) {
					
					entityName = parts[0];
					columnsString = string.Format("{0}{1}{2}", columnsString, separator, parts[1]);
					separator = Resources.CoreWebContent.String_Separator_1_VB0_59;
				}

				var validationMessage = new StringBuilder();
				validationMessage.AppendFormat(Resources.CoreWebContent.WEBDATA_IB0_157, columnsString);

				var nodeMngLink = "/Orion/Nodes/Default.aspx";
				// add grouping paramenter in case we have only one CP updated
				if (assignedPropertyIds.Length == 1)
					nodeMngLink = string.Format("{0}?CustomPropertyId={1}", nodeMngLink, HttpUtility.UrlEncode(assignedPropertyIds[0]));

				// in case we updated Nodes or Interfaces CPs we're adding "Show assignments link"
				if (entityName.Equals("Orion.Nodes", StringComparison.OrdinalIgnoreCase)
					|| entityName.Equals("Orion.NPM.Interfaces", StringComparison.OrdinalIgnoreCase))
					validationMessage.AppendFormat("&nbsp;<a href='{0}'><span class='LinkArrow'>&raquo;</span>&nbsp;{1}</a>&nbsp;", nodeMngLink, Resources.CoreWebContent.WEBDATA_IB0_158);

				ValidationOk.Controls.Add(new LiteralControl(validationMessage.ToString()));
				ValidationOk.Visible = true;
			}

			ClearSessionValue("AssignedPropertyIds");
		}
        base.OnInit(e);
    }

	[WebMethod(EnableSession = true)]
	public static void ClearSessionValue(string sessionVarName)
	{
		HttpContext.Current.Session[sessionVarName] = null;
	}
}
