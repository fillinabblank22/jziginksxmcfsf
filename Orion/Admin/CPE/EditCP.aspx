<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="EditCP.aspx.cs" Inherits="Orion_CPE_EditCP" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_10 %>" %>

<%@ Register Src="~/Orion/Admin/CPE/Controls/EditCPControl.ascx" TagPrefix="orion"
    TagName="EditCPControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include ID="Include1" runat="server" File="CustomPropertyEditor.css" />
    <style type="text/css">
        #adminContentTable td {
            padding: 10px 10px 10px 10px;
        }

        #inside_table td {
            padding: 5px;
            vertical-align: middle;
        }
        #inside_table td.Property img {
            width: 15px;
            height: 15px;
        }
        #inside_table .header td {
            padding-left: 0;
        }

        .footer_buttons {
            margin: 10px 0 0 0;
        }
        #adminContent h2 {
            margin: 0;
            font-weight: normal;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 style="font-size: large!important;">
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <asp:Panel runat="server" DefaultButton="bt_Submit">
        <div style="width: 830px;">
            <table id="adminContentTable" cellpadding="0" cellspacing="0" style="width: 100%!important;">
                <tr>
                    <td>
                        <orion:EditCPControl runat="server" ID="EditCPControl" />
                    </td>
                </tr>
            </table>
            <div class="sw-btn-bar-wizard" width="100%">
                <orion:LocalizableButton ID="bt_Submit" runat="server" DisplayType="Primary" LocalizedText="Submit"
                    OnClick="Submit_Click" />
                <orion:LocalizableButton ID="bt_Cancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel"
                    OnClick="Cancel_Click" CausesValidation="false" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
