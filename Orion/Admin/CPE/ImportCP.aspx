<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CPE/ImportCPWizard.master" AutoEventWireup="true"
    CodeFile="ImportCP.aspx.cs" Inherits="Orion_CPE_ImportCP" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_12 %>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder2" runat="server">
    <orion:Include ID="Include4" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <style type="text/css">
        div.file-uploader
        {
            position: relative;
            width: 450px;
        }
        
        div.fakefile
        {
            position: absolute;
            top: 0px;
            left: 0px;
            z-index: 1;
        }
        
        input.file
        {
            position: relative;
            -moz-opacity: 0;
            width: 20px;
            cursor: pointer;
            filter: alpha(opacity: 0);
            opacity: 0;
            z-index: 2;
        }
    </style>
    <orion:Include ID="Include5" runat="server" File="OrionCore.js" />
    <script type="text/javascript">
        var hash = {
            '.txt': 1,
            '.csv': 1,
            '.xls': 1,
            '.xlsx': 1
        };

        function fileSelected(thisEl) {
            var re = /\..+$/;
            var ext = thisEl.value.match(re);
            if (hash[ext]) {
                $("#<%=txtFile.ClientID%>").val(thisEl.value);
                AfterResultReceived();

                return true;
            } else {
                $("#<%=txtFile.ClientID%>").val('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_12) %>');
                Ext.Msg.show({
                    width: 400,
                    title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_13) %>',
                    msg: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_14) %> <br /> <br /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_15) %>',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                BeforeResultReceived();
                return false;
            }
        }

        function BeforeResultReceived() {
            $(function () {
                $("#<%=imgbNext.ClientID%>").addClass('sw-btn-disabled');
            });
        }

        function AfterResultReceived() {
            $(function () {
                $("#<%=imgbNext.ClientID%>").removeClass('sw-btn-disabled');
            });
        }

        $(function () {
            if ($("#<%=txtFile.ClientID %>").val() == "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_12) %>") {
                BeforeResultReceived();
            }

            if ($("#<%=hiddenData.ClientID%>").val() != "") {
                Ext.Msg.show({
                    width: 400,
                    title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_17) %>',
                    msg: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_18) %> <br /> <br /><span class="LinkArrow">&raquo;</span><a id="downloadLink" style="text-decoration: underline; color: #336699;" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_16) %></a>',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }

            $("#<%=imgbNext.ClientID%>").click(function (e) {
                if ($(this).is('.sw-btn-disabled')) {
                    e.preventDefault();
                }
            });

            $("#<%=fileUpload.ClientID%>").mousemove(function (e) {
                $("#<%=fileDialog.ClientID %>").css({
                    'left': e.pageX - $("div.fakefile").offset().left - 10,
                    'top': e.pageY - $("div.fakefile").offset().top - 5
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <h2>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_4) %></h2>
            <br />
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <div style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_5) %></div>
                                </td>
                                <td>
                                    <div class="file-uploader">
                                        <asp:FileUpload ID="fileDialog" runat="server" onchange="fileSelected(this)" CssClass="file" />
                                        <div class="fakefile" style="white-space: nowrap;">
                                            <input runat="server" type="text" value="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_12 %>"
                                                id="txtFile" readonly="readonly" style="width: 330px;" />
                                            <orion:LocalizableButton runat="server" ID="fileUpload" LocalizedText="CustomText"
                                                Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_11 %>" />
                                        </div>
                                    </div>
                                    <input runat="server" id="hiddenData" type="hidden" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="padding-bottom: 20px; color: #336699;">
                                        <span class="LinkArrow">&raquo;</span>&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_16) %>&nbsp;
                                        <a id="A1" style="text-decoration: underline; color: #336699;" target="_blank" href="/Orion/Admin/CPE/ImportExamples/ImportExample.csv">csv</a>
                                        &nbsp;
                                        <a id="A2" style="text-decoration: underline; color: #336699;" target="_blank" href="/Orion/Admin/CPE/ImportExamples/ImportExample.txt">txt</a>
                                        &nbsp;
                                        <a id="A3" style="text-decoration: underline; color: #336699;" target="_blank" href="/Orion/Admin/CPE/ImportExamples/ImportExample.xls">xls</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_6) %></div>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="Entities" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding-bottom: 10px;">
                        <div id="Hint" style="position: static; width: 100%;" runat="server">
                                <div class="sw-pg-hint-inner">
                                    <div class="sw-pg-hint-body">
                                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_7) %></b>
                                        <ul class="cphint-ul">
                                            <li class="cphint-li"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_9) %></li>
                                            <li class="cphint-li"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_8) %></li>
                                            <li class="cphint-li"><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_YK0_10, "<br />")) %></li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 5px;">
                        <asp:CheckBox runat="server" ID="cbRemoveUnchanged" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_192 %>" Checked="True" CssClass="checkboxspace"/>
                    </td>
                    <td>
                        <div id="Info" style="width: 100%;" runat="server">
                            <div class="sw-pg-hint-blue">
                                <div class="sw-pg-hint-body-info">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_208) %>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br />
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
