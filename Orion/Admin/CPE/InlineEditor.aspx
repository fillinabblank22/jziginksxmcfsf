﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true"
    CodeFile="InlineEditor.aspx.cs" Inherits="Orion_Admin_CPE_InlineEditor" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_19 %>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headplaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" File="Admin.css" />
    <script type="text/javascript">        window.IsOrionAdminPage = 1;</script>

    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <%--include filter extentions--%>
    <%--todo: use Include control there  --%>
    <link rel="stylesheet" type="text/css" href="/Orion/js/extjs/3.4/ux/gridfilters/css/GridFilters.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/js/extjs/3.4/ux/gridfilters/css/RangeMenu.css" />
    <orion:Include runat="server" File="CustomPropertyEditor.css" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/menu/RangeMenu.js" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/menu/ListMenu.js" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/GridFilters.js" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/filter/Filter.js" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/filter/StringFilter.js" />
    <orion:Include runat="server" File="Admin/js/ExcelCellSelectionModel.js" />
    <orion:Include runat="server" File="Admin/js/EditorPasteCopyGrid.js" /> 
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/filter/DateFilter.js" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/filter/ListFilter.js" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/filter/NumericFilter.js" />
    <orion:Include runat="server" File="extjs/3.4/ux/gridfilters/filter/BooleanFilter.js" />
    <%--include filter extentions--%>
    <orion:Include runat="server" File="Admin/js/DateTimeEditor.js" />
    <orion:Include runat="server" File="Admin/js/CpInlineEditorGrid.js" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <style type="text/css">
        #adminContent td { padding: 0px 0px 0px 0px !important;  vertical-align: middle; }
        .no-border .x-table-layout-ct, .panel-no-border .x-panel-body-noheader { border: none !important; }
        .panel-border { border: solid 1px #D0D0D0 !important; }
        .x-menu-item-table {padding-right: 5px!important;}
        .x-menu-item-table td {font-size:11px;}
        .x-menu-item-table table {width:100px;}
        .x-grid3-cell 
        {
            border-right: 1px Solid #EDEDED!important;
            border-bottom: 1px Solid #EDEDED!important;
        } 
        div.x-grid3-row 
        {
            border-top: 0px none!important;
            border-bottom: 0px none!important;
            border-right: 0px none!important;
        }
        
        .x-grid3-dirty-cell { 
    background-color: #ffa; 
    background-image: url(/Orion/images/CPE/bullet_green.png); 
    background-position: right center; 
    color: #090; 
}
        .hide-header .x-grid3-header { display: none; }
        .searchfield { background-image: url(/Orion/images/search_button.gif) !important; }
        .x-grid3-hd-row td.ux-filtered-column img.filter-icon { background-image: url(/Orion/images/CPE/filter_applied.png); }
        .x-grid3-hd-row td.ux-filtered-column img.filter-icon:hover { background-image: url(/Orion/images/CPE/filter_applied_hover.png); }
        .x-grid3-hd-row img.filter-icon { background-image: url(/Orion/images/CPE/filter.png); }
        .x-grid3-hd-row img.filter-icon:hover { background-image: url(/Orion/images/CPE/filter_hover.png); }
        .ext-ie7 .sw-cp-multiedit-control-table .x-form-text { margin: 0px !important; }
        #multiEditDialog { display: none; background-color: white;}
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
   <div class="sw-hdr-links">   
    <a href="/Orion/Admin/CPE/Default.aspx" style="background: transparent url(/Orion/Nodes/images/icons/icon_edit.gif) scroll no-repeat left center;
            padding: 2px 0px 2px 20px;" target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_4) %></a>
    <% if (IsNodeFunctionalityAllowed)
     { %>
    <span class="IconHelpButton">
        <%--todo: change icon for link to manage nodes page--%>
        <a class="HelpText" href="/Orion/Nodes/Default.aspx" style="background: transparent url(/Orion/images/CPE/manage_nodes.gif) scroll no-repeat left center;
            padding: 2px 0px 2px 20px;" target="_blank">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcesAll_ManageNodes) %></a> </span>
  <% } %>
   </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="adminContent">
    <h1 style="font-size: large!important;">
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div style="padding: 1em 0 1em 0;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_20) %>
    </div>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="/Orion/Services/CPEManagement.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <div id="filterNotifications" class="sw-filter-notifications"></div>
    
    <div id="editor-grid" class="editor-grid">
    </div>
    <input type="hidden" name="CPIEditor_PageSize" id="CPIEditor_PageSize"
        value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "CPIEditor_PageSize", "100")) %>' />
        <% foreach(var entity in AvailableEntities)
           {%>
        <input type="hidden" name="CPIEditor_<%= DefaultSanitizer.SanitizeHtml(entity.Replace('.', '_')) %>" id="CPIEditor_<%= DefaultSanitizer.SanitizeHtml(entity.Replace('.', '_')) %>"
        value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, string.Format("CPIEditor_{0}", entity.Replace('.', '_')), "")) %>' />
        <% } %>
    <input type="hidden" name="CPIEditor_AllowAdmin" id="CPIEditor_AllowAdmin" value='<%= this.Profile.AllowAdmin %>' />
    <input type="hidden" id="Current_EntityName" value="<%= DefaultSanitizer.SanitizeHtml(EntityName) %>" />
    <input type="hidden" id="Current_ShowColumns" value="<%= DefaultSanitizer.SanitizeHtml(ColumnsList) %>" />
    <!--TODO: temporary added <%= DefaultSanitizer.SanitizeHtml(Request["ShowColumnsFilter"]) %> for QA testing-->
    <input type="hidden" id="ShowColumnsFilter" value="<%= DefaultSanitizer.SanitizeHtml(Request["ShowColumnsFilter"]) %>"/>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="CustomText"
            Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_159 %>" DisplayType="Primary"
            OnClientClick="SW.Orion.CustomPropertyInlineEditor.StoreChanges(); return false;" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
            DisplayType="Secondary" CausesValidation="false" />
    </div>

    <div id="multiEditDialog">
        <div>
        <h1><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_160) %></b></h1>
            <div id="selCustomPropertiesList"></div>
        </div>
        <div class="sw-btn-bar-wizard" >
            <orion:LocalizableButtonLink CssClass="sw-pg-dialogsave-btn" ID="dialogOk" runat="server" LocalizedText="CustomText" DisplayType="Primary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_159 %>" style="cursor: pointer;"  />
            <orion:LocalizableButtonLink CssClass="sw-pg-dialogcancel-btn" ID="dialogCancel" LocalizedText="Cancel" DisplayType="Secondary" runat="server" style="cursor: pointer;" />
        </div>
    </div>
    </div>
</asp:Content>
