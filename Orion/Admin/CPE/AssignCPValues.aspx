<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/LimitedAdminPage.master" AutoEventWireup="true"
    CodeFile="AssignCPValues.aspx.cs" Inherits="Orion_Admin_CPE_AssignCPValues" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_129 %>" %>

<%@ Register Src="~/Orion/Admin/CPE/Controls/AssignMultipleValues.ascx" TagPrefix="orion"
    TagName="AssignValue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include3" runat="server" File="CustomPropertyEditor.css" />
    <style type="text/css">
        #adminContentTable td
        {
            padding: 10px 10px 10px 10px;
        }
        #inside_table td
        {
            padding: 5px;
            vertical-align: middle;
        }
        #inside_table td.Property img
        {
            width: 15px;
            height: 15px;
        }
        #inside_table .header td
        {
            padding-left: 0px;
        }
        .footer_buttons
        {
            margin: 10px 0px 0px 0px;
        }
        #adminContent h2
        {
            margin: 0px 0px;
            font-weight: normal;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 style="font-size: large!important;">
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <asp:Panel ID="Panel1" runat="server" DefaultButton="bt_Submit">
        <div>
            <table id="adminContentTable" cellpadding="0" cellspacing="0" style="width: 100%!important;">
                <tr>
                    <td>
                           <orion:AssignValue runat="server" ID="AssignValues" EnumerationValues="<%$ HtmlEncodedCode: Values %>" />
                    </td>
                </tr>
            </table>
            <div class="sw-btn-bar-wizard" width="100%">
                <orion:LocalizableButton ID="bt_Submit" runat="server" DisplayType="Primary" LocalizedText="Submit"
                    OnClick="Submit_Click" />
                <orion:LocalizableButton ID="bt_Cancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel"
                    OnClick="Cancel_Click" CausesValidation="false" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
