<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/LimitedAdminPage.master" AutoEventWireup="true"
    CodeFile="ExportCP.aspx.cs" Inherits="Orion_CPE_ExportCP" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_11 %>" %>

<%@ Register Src="~/Orion/Admin/CPE/Controls/ExportFormatControl.ascx" TagPrefix="orion"
    TagName="ExportFormat" %>
<%@ Register Src="~/Orion/Admin/CPE/Controls/ColumnListControl.ascx" TagPrefix="orion"
    TagName="ColumnList" %>
<%@ Register Src="~/Orion/Admin/CPE/Controls/SelectCPAssignments.ascx" TagPrefix="orion"
    TagName="SelectCPAssignments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <style type="text/css">
        .error
        {
            border: 2px solid red;
        }
        #adminContent td
        {
            padding: 3px 3px 3px 3px !important;
            vertical-align: middle;
            font-family: Arial, Verdana, Helvetica, sans-serif;
        }
        .no-border .x-table-layout-ct, .panel-no-border .x-panel-body-noheader
        {
            border: none !important;
        }
        .panel-border
        {
            border: solid 1px #D0D0D0 !important;
        }
        .x-tip-body
        {
            white-space: nowrap !important;
        }
        .cp_table_leftcolumn
        {
            width: 300px;
        }
    </style>
    <orion:Include ID="Include1" runat="server" File="ValueAssignments.css" />
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div style="padding: 1em 0 1em 0;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_33) %>
    </div>
    <asp:Panel runat="server" DefaultButton="bt_Export">
        <div style="width: 700px;">
            <table id="adminContentTable" cellpadding="0" cellspacing="0" style="width: 100%!important;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="cp_table_leftcolumn">
                                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_34) %></b>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <orion:SelectCPAssignments runat="server" ID="selObject" EntityDisplayName='<%# DefaultSanitizer.SanitizeHtml(Eval("EntityDisplayName")) %>'
                                                OnSelectObjectChanged="selObject_SelectObjectChanged" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                     <asp:CustomValidator ID="RequiredNetObjects" runat="server" OnServerValidate="RequiredNetObjectsValidation"
                                                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_SO0_37 %>" EnableClientScript="false"
                                                Display="Dynamic">
                                            </asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_30) %> </b>
                        <orion:ColumnList runat="server" ID="parentObjectColumnList" Title="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO0_31 %>"></orion:ColumnList>
                        <orion:ColumnList runat="server" ID="customPropertiesColumnList" Title="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO0_32 %>"></orion:ColumnList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <orion:ExportFormat runat="server" ID="exportFormat"></orion:ExportFormat>
                    </td>
                </tr>
            </table>
            <div class="sw-btn-bar-wizard" width="100%">
                <orion:LocalizableButton ID="bt_Export" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO0_35 %>" OnClick="Export_Click" />
                <orion:LocalizableButton ID="bt_Cancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="Cancel_Click" CausesValidation="false" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>