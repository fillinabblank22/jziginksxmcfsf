<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleEditCpValuesControl.ascx.cs"
    Inherits="Orion_Admin_CPE_Controls_MultipleEditCpValuesControl" %>
<%@ Register TagPrefix="orion" TagName="EditCpValueControl" Src="~/Orion/Controls/EditCpValueControl.ascx" %>
<asp:Repeater runat="server" ID="repeater">
    <HeaderTemplate>
        <table>
            <tr>
    </HeaderTemplate>
    <ItemTemplate>
        <td>
            <orion:EditCpValueControl runat="server" ID="ValueTextBox"  PropertyType='<%# DefaultSanitizer.SanitizeHtml(Eval("Type")) %>'
                EnumerationValues='<%# DefaultSanitizer.SanitizeHtml(Eval("EnumValues")) %>' Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %>' />
        </td>
    </ItemTemplate>
    <FooterTemplate>
        </tr></table>
    </FooterTemplate>
</asp:Repeater>
