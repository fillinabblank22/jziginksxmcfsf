<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignMultipleValues.ascx.cs"
    Inherits="Orion_Admin_CPE_Controls_AssignMultipleValues" %>
<%@ Register Src="~/Orion/Admin/CPE/Controls/SelectCPAssignments.ascx" TagPrefix="orion"
    TagName="SelectCPAssignments" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<%@ Register Src="~/Orion/Admin/CPE/Controls/MultipleEditCpValuesControl.ascx" TagPrefix="orion"
    TagName="EditCpControl" %>
<%@ Reference Control="~/Orion/Admin/CPE/Controls/SelectCPAssignments.ascx" %>
<orion:Include ID="Include1" runat="server" File="ValueAssignments.css" />
<asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
    <ContentTemplate>
            <asp:GridView ID="valuesGrid" runat="server" automation="valueslist" AutoGenerateColumns="False"
                ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="ValuesGrid_RowCommand"
                OnDataBound="ValuesGrid_OnDataBound" OnRowDataBound="ValuesGrid_OnRowDataBound">
                <AlternatingRowStyle CssClass="assignItemHolder assignItemHolderAlter" />
                <RowStyle CssClass="assignItemHolder" />
                <FooterStyle CssClass="footerItemHolder" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table class="assign-items-table">
                                <tr runat="server" id="ColumnsTR">
                                    <td class="leftCol">
                                        <orion:SelectCPAssignments runat="server" ID="selObject" EntityDisplayName='<%# DefaultSanitizer.SanitizeHtml(Eval("EntityDisplayName")) %>'
                                            OnSelectObjectChanged="selObject_SelectObjectChanged" />
                                    </td>
                                    <td class="rightCol" style="padding-left: 0px!important;">
                                        <orion:EditCpControl runat="server" ID="editCpControl" Items='<%# DefaultSanitizer.SanitizeHtml(Eval("CpAssignments")) %>'
                                            ObjectUris='<%# DefaultSanitizer.SanitizeHtml(Eval("ObjectUris")) %>' />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <FooterTemplate>
                            <table style="width: 100%;">
                                <tr runat="server" id="footerRow">
                                    <td style="padding: 0px 0px 0px 0px!important; width: 300px; ">
                                        <div class="sw-hatchbox">
                                            <orion:LocalizableButton ID="AddButton" runat="server" DisplayType="Small" LocalizedText="CustomText"
                                                CausesValidation="false" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_170 %>"
                                                OnClick="AddButton_Clicked" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/button_X_04.gif"
                        Text="x" ControlStyle-CssClass="delete-button" CommandName="DeleteRow" />
                </Columns>
            </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
