using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.Controls;

public partial class Orion_Controls_ColumnListControl : UserControl
{
    public DataTable DataSource
    {
        set
        {
            if (value != null && DefaultColumnsList != null && DefaultColumnsList.Count > 0)
            {
                DefaultColumns = value.Clone();
                AdvancedColumns = value.Clone();

                for (var i = 0; i < value.Rows.Count; i++)
                {
                    if (DefaultColumnsList.Contains(value.Rows[i]["ColumnName"].ToString()))
                    {
                        DefaultColumns.Rows.Add(value.Rows[i]["ColumnName"], value.Rows[i]["ColumnDisplayName"], value.Rows[i]["Type"]);
                    }
                    else
                    {
                        AdvancedColumns.Rows.Add(value.Rows[i]["ColumnName"], value.Rows[i]["ColumnDisplayName"], value.Rows[i]["Type"]);
                    }
                }
                AdvancedColumnList.DataSource = AdvancedColumns;
                AdvancedColumnList.DataBind();
            }
            else
            {
                DefaultColumns = value;
                AdvancedPanel.Visible = false;

                phAncestors.Visible = false;
            }

            ColumnList.DataSource = DefaultColumns;
            ColumnList.DataBind();
        }
    }

    public IDictionary<string, IList<NodeProperty>> Ancestors { private get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    public List<string> SelectedCP { get; set; }

    public List<string> DefaultColumnsList { get; set; }

    public List<string> GetSelectedColumns()
    {
        var ilist = GetRepeaterItemsChecked(ColumnList.Items);

        foreach (var node in phAncestors.Controls.OfType<AncestoryPanel>())
        {
            ilist.AddRange(node.GetSelectedColumns());
        }

        ilist.AddRange(GetRepeaterItemsChecked(AdvancedColumnList.Items));

        return ilist;
    }

    public bool AnythingFromParent
    {
        get
        {
            foreach (var node in phAncestors.Controls.OfType<AncestoryPanel>())
            {
                if (node.GetSelectedColumns().Count > 0)
                    return true;
            }

            return false;
        }
    }

    private List<string> GetRepeaterItemsChecked(IEnumerable items)
    {
        return (from RepeaterItem rpItem in items
                select rpItem.FindControl("chkbx") as CheckBox into chkbx
                where chkbx != null && chkbx.Checked
                select chkbx.Attributes["ColumnName"]).ToList();
    }

    protected DataTable DefaultColumns
    {
        get
        {
            if (ViewState[string.Format("DefaultColumns_{0}", ClientID)] == null)
            {
                ViewState[string.Format("DefaultColumns_{0}", ClientID)] = new DataTable();
            }

            return (DataTable)ViewState[string.Format("DefaultColumns_{0}", ClientID)];
        }

        set
        {
            ViewState[string.Format("DefaultColumns_{0}", ClientID)] = value;
        }
    }

    protected DataTable AdvancedColumns
    {
        get
        {
            if (ViewState[string.Format("AdvancedColumns_{0}", ClientID)] == null)
            {
                ViewState[string.Format("AdvancedColumns_{0}", ClientID)] = new DataTable();
            }

            return (DataTable)ViewState[string.Format("AdvancedColumns_{0}", ClientID)];
        }

        set
        {
            ViewState[string.Format("AdvancedColumns_{0}", ClientID)] = value;
        }
    }

    protected void ColumnList_ItemDataBound(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

        var cb = (CheckBox)e.Item.FindControl("chkbx");
        var columnName = ((DataRowView)e.Item.DataItem)["ColumnName"].ToString();
        cb.Attributes.Add("ColumnName", columnName);
        cb.Attributes["onclick"] = string.Format("setUpRootCb($(\"#{0}\"));", ColumnListContainer.ClientID);

        var columnDisplayName = ((DataRowView)e.Item.DataItem)["ColumnDisplayName"].ToString();
        cb.Text = string.IsNullOrEmpty(columnDisplayName) ? columnName : columnDisplayName;

        if (SelectedCP != null)
        {
            cb.Checked = SelectedCP.Contains(columnName);
        }
    }

    protected void ColumnsValidation(object source, ServerValidateEventArgs args)
    {
        args.IsValid = GetSelectedColumns().Count > 0;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Ancestors == null) return;

        foreach (var ancestor in Ancestors)
        {
            var ancestoryControl = (AncestoryPanel)LoadControl("AncestoryPanel.ascx");
            ancestoryControl.NodeTitle = ancestor.Key;
            ancestoryControl.CheckboxOnClick = string.Format("setUpRootCb($(\"#{0}\"));", ColumnListContainer.ClientID);
            ancestoryControl.Ancestors = ancestor.Value;

            phAncestors.Controls.Add(ancestoryControl);
        }
    }
}