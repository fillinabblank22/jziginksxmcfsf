﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web.CPE;

public partial class Orion_Admin_CPE_Controls_MultipleEditCpValuesControl : System.Web.UI.UserControl
{
	#region Properties
	[PersistenceMode(PersistenceMode.Attribute)]
	public string EntityDisplayName
	{
		get
		{
			if (ViewState[string.Format("EntityDisplayName_{0}", ClientID)] == null)
			{
				ViewState[string.Format("EntityDisplayName_{0}", ClientID)] = string.Empty;
			}
			return (string)ViewState[string.Format("EntityDisplayName_{0}", ClientID)];
		}
		set
		{
			ViewState[string.Format("EntityDisplayName_{0}", ClientID)] = value;
		}
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public string EntityType
	{
		get
		{
			if (ViewState[string.Format("EntityType_{0}", ClientID)] == null)
			{
				ViewState[string.Format("EntityType_{0}", ClientID)] = string.Empty;
			}
			return (string)ViewState[string.Format("EntityType_{0}", ClientID)];
		}
		set
		{
			ViewState[string.Format("EntityType_{0}", ClientID)] = value;
		}
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public string[] ObjectUris
	{
		get
		{
			if (ViewState[string.Format("ObjectUris_{0}", ClientID)] == null)
			{
				ViewState[string.Format("ObjectUris_{0}", ClientID)] = new string[0];
			}
			return (string[])ViewState[string.Format("ObjectUris_{0}", ClientID)];
		}
		set
		{
			ViewState[string.Format("ObjectUris_{0}", ClientID)] = value;
		}
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public List<CpItemInfo> Items
	{
		get
		{
			if (ViewState[string.Format("CpItems_{0}", ClientID)] == null)
			{
				ViewState[string.Format("CpItems_{0}", ClientID)] = new List<CpItemInfo>();
			}
			return (List<CpItemInfo>)ViewState[string.Format("CpItems_{0}", ClientID)];
		}
		set
		{
			ViewState[string.Format("CpItems_{0}", ClientID)] = value;
		}
	}
	#endregion

	protected void Page_Load(object sender, EventArgs e)
	{
		Update();
	}

	public void DataBind()
	{
		repeater.DataSource = Items;
		repeater.DataBind();
	}

	private void Update()
	{
		for (var i = 0; i < repeater.Items.Count; i++)
		{
			Control control = repeater.Items[i];
			if (i > Items.Count) break;

			var tbx = (Orion_Controls_EditCpValueControl)control.FindControl("ValueTextBox");
			tbx.ObjectIds = ObjectUris;
			tbx.Configure(false, Items[i].Type, Items[i].EnumValues);
			Items[i].Value = tbx.Text;
		}
	}
}