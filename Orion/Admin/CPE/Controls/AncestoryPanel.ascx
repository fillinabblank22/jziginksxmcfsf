<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AncestoryPanel.ascx.cs" Inherits="AncestoryPanel" %>

<asp:Panel runat="server" ID="NodesPanel" CssClass="advanced-columns-panel">
    <img src="/Orion/images/Button.Expand.gif" style="cursor: pointer;" OnClick="toggleElement(this, 'AncestoryNodeColumns');" alt="" />
    <%= DefaultSanitizer.SanitizeHtml(NodeTitle) %>
    <div id="AncestoryNodeColumns" runat="server" style="display: none; margin-left: 15px;">
        <asp:Repeater ID="AncestoryColumnList" runat="server" OnItemDataBound="AncestoryPanel_ItemDataBound">
            <HeaderTemplate>
                <table style="width: 100% !important;">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="text-align: left;">
                        <asp:CheckBox ID="chkbx" runat="server" CssClass="spaced_checkbox" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>