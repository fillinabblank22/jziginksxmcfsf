﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_CPE_Controls_MatchColumnsControl : UserControl
{
	#region Properties
	public DataTable DataSource
	{
		get; set; 
	}

    public string SourceEntityName
    {
        get
        {
            if (ViewState[string.Format("SourceEntityName_{0}", ClientID)] == null)
            {
                ViewState[string.Format("SourceEntityName_{0}", ClientID)] = "Orion.Nodes";
            }
            return (string)ViewState[string.Format("SourceEntityName_{0}", ClientID)];
        }
        set
        {
            ViewState[string.Format("SourceEntityName_{0}", ClientID)] = value;
        }
    }

	protected Dictionary<string, string> SystemProperties
	{
		get
		{
			if (ViewState[string.Format("SystemProperties_{0}", ClientID)] == null)
			{
				ViewState[string.Format("SystemProperties_{0}", ClientID)] = LoadProperties(false);
			}
			return (Dictionary<string, string>)ViewState[string.Format("SystemProperties_{0}", ClientID)];
		}
	}

	protected Dictionary<string, string> CustomProperties
	{
		get
		{
			if (ViewState[string.Format("CustomProperties_{0}", ClientID)] == null)
			{
				ViewState[string.Format("CustomProperties_{0}", ClientID)] = LoadProperties(true);
			}
			return (Dictionary<string, string>)ViewState[string.Format("CustomProperties_{0}", ClientID)];
		}
		set
		{
			ViewState[string.Format("CustomProperties_{0}", ClientID)] = value;
		}
	}

    private string LatitudeStr = string.Format("{0} {1}", Resources.CoreWebContent.WEBDATA_PCC_WM_LATITIDE, Resources.CoreWebContent.WEBDATA_AY0_89);
    private string LongitudeStr = string.Format("{0} {1}", Resources.CoreWebContent.WEBDATA_PCC_WM_LONGITUDE, Resources.CoreWebContent.WEBDATA_AY0_89);

	#endregion

	private Dictionary<string, string> LoadProperties(bool custom)
	{
		var dictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

	    DataTable dt;
	    if (custom)
	    {
	        dt = CustomPropertyHelper.GetCustomPropertiesBySource(SourceEntityName);
            if (WorldMapDAL.isEntityAvailable(SourceEntityName))
            {
                dt.Rows.Add("WorldMapPoint.Latitude", LatitudeStr, typeof(Single));
                dt.Rows.Add("WorldMapPoint.Longitude", LongitudeStr, typeof(Single));
            }
	    }
	    else
	    {
	        dt = CustomPropertyHelper.GetEntityProperties(SourceEntityName);

            // now checking if anything should be added from parent
            var parentEntity = EntityHelper.GetSwisEntityParent(SourceEntityName);
	        if (!parentEntity.Equals(SourceEntityName) && IsAnythingFromParent(EntityHelper.GetEntityName(parentEntity)))
	        {
                var parProps = CustomPropertyHelper.GetEntityProperties(parentEntity);
	            string parentName = EntityHelper.GetEntityName(parentEntity);
                foreach (DataColumn col in DataSource.Columns)
	            {
	                if (col.ColumnName.StartsWith(parentName + "."))
	                {
                        DataRow[] foundRows = parProps.Select("ColumnName = '" + col.ColumnName.Split('.')[1] + "'"); //taking column name from Node.NodeID => NodeId
                        if (foundRows.Length == 1)
                            dt.Rows.Add(parentName + "." + foundRows[0]["ColumnName"], foundRows[0]["ColumnDisplayName"], foundRows[0]["Type"], foundRows[0]["CanUpdate"]);
	                }
	            }
	        }
	    }

		if (dt != null && dt.Rows.Count > 0)
		{
			foreach (DataRow row in dt.Rows)
			{
				dictionary.Add(row["ColumnName"].ToString(), (string)row["Type"]);
			}
		}

		return dictionary;
	}

    private bool IsAnythingFromParent(string getEntityName)
    {
        if (DataSource == null || DataSource.Columns.Count == 0)
            return false;

        foreach (DataColumn col in DataSource.Columns)
        {
            if (col.ColumnName.StartsWith(getEntityName + "."))
                return true;
        }

        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
        {
            var hashColumnNameMatcher = CustomPropertyChangeDetector.HashColumnMatcher();

            var dataSource = DataSource.Columns.Cast<DataColumn>().Where(x => hashColumnNameMatcher.IsMatch(x.ColumnName) == false).ToArray();

            foreach (var dataColumn in dataSource)
            {
                if (dataColumn.ColumnName == "WorldMapPoint.Latitude")
                    dataColumn.Caption = LatitudeStr;

                if (dataColumn.ColumnName == "WorldMapPoint.Longitude")
                    dataColumn.Caption = LongitudeStr;
            }

            ColumnsRepeater.DataSource = dataSource;
            ColumnsRepeater.DataBind();
		}
		else
		{
			var customProperties = LoadProperties(true);
			var needsToReLoad = customProperties.Any(prop => !CustomProperties.ContainsKey(prop.Key));
			
			if (customProperties.Count != CustomProperties.Count)
			{
				// some custom property can be removed 
				needsToReLoad = true;
			}

			// reload dropdowns with custom properties in case new cp was added or removed
			if (needsToReLoad)
			{
				CustomProperties = customProperties;
				ReloadCustomPropertyDropdowns();
			}
		}

		// trick for FB167115: CPE : match column is shown ugly on 'Import CP' page
		// ie9 browser add Text - "Empty Text Node" beatween <td> elements within the table 
		// it breaks table layout after updatepanel update - so we're removing these empty nodes
		ScriptManager.RegisterStartupScript(Page, GetType(), "normalize tr elements", @"
if ($.browser.msie && $.browser.version=='9.0') {
	$('.matches-table tr').each(function () {
		for (var i = 0; i < this.childNodes.length; i++) {
			if ($.trim(this.childNodes[i].outerHTML) == '') {
				this.removeChild(this.childNodes[i]);
			}
		} 
	});
}", true);
	}

	private void ReloadCustomPropertyDropdowns()
	{
		foreach (RepeaterItem rpItem in ColumnsRepeater.Items)
		{
			var matchControl = (DropDownList)rpItem.FindControl("MatchType");
			// in case "matches" selected we aren't show custom properties then skip this;
			if (matchControl.SelectedIndex != 1) continue;

			var assignControl = (HtmlSelect)rpItem.FindControl("MatchColumns");
			var createUrlControl = (HtmlContainerControl)rpItem.FindControl("createUrl");

			// remember old selected value
			var selectedValue = assignControl.Items[assignControl.SelectedIndex].Value;

			assignControl.Items.Clear();
			assignControl.Items.Add(Resources.CoreWebContent.WEBDATA_IB0_148);

			foreach (var prop in CustomProperties)
			{
				assignControl.Items.Add(prop.Key);
			}

            var colName = ResolveColumnName((Label)rpItem.FindControl("colName"));

			// try to select previous value
			if (!string.IsNullOrEmpty(selectedValue) && CustomProperties.ContainsKey(selectedValue))
			{
				foreach (var listItemVar in assignControl.Items.Cast<ListItem>().Where(listItemVar => listItemVar.Value.Equals(selectedValue, StringComparison.OrdinalIgnoreCase))) {
					listItemVar.Selected = true;
				}
			}
			else
			{
				// if previous value is not longer exists then try to match with column name from file
				if (CustomProperties.ContainsKey(colName.Text))
				{
					foreach (var listItemVar in assignControl.Items.Cast<ListItem>().Where(listItemVar => listItemVar.Value.Equals(colName.Text, StringComparison.OrdinalIgnoreCase))) {
						listItemVar.Selected = true;
					}
				}
			}
			// hide create new custom property link if needed (e.g. column with the same name was already added)
			createUrlControl.Visible = !SystemProperties.ContainsKey(colName.Text) && !CustomProperties.ContainsKey(colName.Text);
		}
	}

	public List<ImportCPColumnsInfo> GetSelectedColumns()
	{
		List<ImportCPColumnsInfo> selectedResult = new List<ImportCPColumnsInfo>();

		foreach (RepeaterItem rpItem in ColumnsRepeater.Items)
		{
			HtmlSelect assignControl = (HtmlSelect)rpItem.FindControl("MatchColumns");

			if (assignControl.SelectedIndex == 0)
				continue;

			DropDownList matchControl = (DropDownList)rpItem.FindControl("MatchType");

            var columnName = ResolveColumnName((Label)rpItem.FindControl("colName"));

			ImportCPColumnsInfo stru = new ImportCPColumnsInfo { SourceColumnName = columnName.Text, TargetColumnName = assignControl.Value, MatchType = matchControl.SelectedIndex == 0 };

			if ((SystemProperties.ContainsKey(assignControl.Value)))
				stru.TargetColumnType = SystemProperties[assignControl.Value];
			else
				stru.TargetColumnType = CustomProperties[assignControl.Value];

			selectedResult.Add(stru);
		}

		return selectedResult;
	}

	protected void ColumnsValidation(object source, ServerValidateEventArgs args)
	{
		// any matching and import selected!!!!
		bool anyMatch = false;
		bool anyImport = false;

		List<string> checkImportDuplicates = new List<string>();
		List<int> selectedValues = new List<int>();

		foreach (RepeaterItem rpItem in ColumnsRepeater.Items)
		{
			HtmlSelect assignControl = (HtmlSelect)rpItem.FindControl("MatchColumns");
			DropDownList matchControl = (DropDownList)rpItem.FindControl("MatchType");

			if (matchControl.SelectedIndex == 0 && assignControl.SelectedIndex != 0)
			{
				anyImport = true;
			}

			if (matchControl.SelectedIndex == 1 && assignControl.SelectedIndex != 0)
			{
				anyMatch = true;
				if (!selectedValues.Contains(assignControl.SelectedIndex))
					selectedValues.Add(assignControl.SelectedIndex);
				else
					checkImportDuplicates.Add(assignControl.Value);
			}
		}

		if (!anyMatch || !anyImport)
		{
			args.IsValid = false;
			((CustomValidator)source).ErrorMessage = Resources.CoreWebContent.WEBDATA_SO0_39;
			return;
		}

		if (checkImportDuplicates.Count > 0)
		{
			args.IsValid = false;
			((CustomValidator)source).ErrorMessage = string.Format(Resources.CoreWebContent.WEBDATA_SO0_47, string.Join("\",\"", checkImportDuplicates.ToArray()));
			return;
		}

		args.IsValid = true;
	}

	protected void ColumnsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item ||
			e.Item.ItemType == ListItemType.AlternatingItem)
		{
			var assignControl = (HtmlSelect)e.Item.FindControl("MatchColumns");
			var matchControl = (DropDownList)e.Item.FindControl("MatchType");
			var columnName = DataBinder.Eval(e.Item.DataItem, "ColumnName").ToString();

            if (DataBinder.Eval(e.Item.DataItem, "ColumnName").ToString().Equals(LatitudeStr))
            {
                columnName = "WorldMapPoint.Latitude";
            }
            if (DataBinder.Eval(e.Item.DataItem, "ColumnName").ToString().Equals(LongitudeStr))
            {
                columnName = "WorldMapPoint.Longitude";
            }
		    var createUrlControl = (HtmlContainerControl)e.Item.FindControl("createUrl");

			if (assignControl != null)
			{
				assignControl.Items.Add(Resources.CoreWebContent.WEBDATA_IB0_148);

				if (SystemProperties.ContainsKey(columnName))
				{
					foreach (var prop in SystemProperties)
					{
						assignControl.Items.Add(prop.Key);
					}

					foreach (ListItem listItemVar in assignControl.Items)
					{
						if (listItemVar.Value.Equals(columnName, StringComparison.OrdinalIgnoreCase))
							listItemVar.Selected = true;
					}

					if (matchControl != null)
					{
						matchControl.SelectedIndex = 0;
					}
				}
				else
				{
					foreach (var prop in CustomProperties)
					{
                        if (prop.Key.Equals("WorldMapPoint.Latitude"))
					    {
                            assignControl.Items.Add(new ListItem(LatitudeStr, prop.Key));
					        continue;
					    }
                        if (prop.Key.Equals("WorldMapPoint.Longitude"))
                        {
                            assignControl.Items.Add(new ListItem(LongitudeStr, prop.Key));
                            continue;
                        }
                        assignControl.Items.Add(new ListItem(prop.Key));
                    }

					if (CustomProperties.ContainsKey(columnName))
					{
						foreach (ListItem listItemVar in assignControl.Items)
						{
							if (listItemVar.Value.Equals(columnName, StringComparison.OrdinalIgnoreCase))
								listItemVar.Selected = true;
						}
					}
					else
					{
						createUrlControl.Visible = true;
					}
				}
			}
		}
	}

	protected void MatchType_SelectedIndexChanged(Object sender, EventArgs e)
	{
		var matchDdlControl = (DropDownList)sender;
		if (matchDdlControl == null) return;

        var columnName = ResolveColumnName((Label)matchDdlControl.Parent.FindControl("colName"));
		if (columnName == null) return;

		var columnsDdlControl = (HtmlSelect)matchDdlControl.Parent.FindControl("MatchColumns");
		if (columnsDdlControl == null) return;

		columnsDdlControl.Items.Clear();
		columnsDdlControl.Items.Add(Resources.CoreWebContent.WEBDATA_IB0_148);

		if (matchDdlControl.SelectedValue.EndsWith("match", StringComparison.OrdinalIgnoreCase))
		{
			foreach (var prop in SystemProperties)
			{
				columnsDdlControl.Items.Add(prop.Key);
			}
		}
		else
		{
			foreach (var prop in CustomProperties)
			{
				columnsDdlControl.Items.Add(prop.Key);
			}
		}

		foreach (ListItem listItemVar in columnsDdlControl.Items)
		{
			if (listItemVar.Value.Equals(columnName.Text, StringComparison.OrdinalIgnoreCase))
				listItemVar.Selected = true;
		}
	}

	public DataTable GetColumnMappingTable(bool matches)
	{
		var dataTable = new DataTable();
		dataTable.Columns.Add("SourceColumn", typeof(string));
		dataTable.Columns.Add("TargetColumn", typeof(string));
		dataTable.Columns.Add("TargetColumnType", typeof(string));

		foreach (RepeaterItem rpItem in ColumnsRepeater.Items)
		{
			var assignControl = (HtmlSelect)rpItem.FindControl("MatchColumns");
			var matchControl = (DropDownList)rpItem.FindControl("MatchType");
            var columnName = ResolveColumnName((Label)rpItem.FindControl("colName"));

			if (matches)
			{
				if (matchControl.SelectedIndex == 0 && assignControl.SelectedIndex != 0)
				{
					dataTable.Rows.Add(columnName.Text, assignControl.Items[assignControl.SelectedIndex].Value, SystemProperties[assignControl.Value]);
				}
			}
			else
			{
				if (matchControl.SelectedIndex == 1 && assignControl.SelectedIndex != 0)
				{
					dataTable.Rows.Add(columnName.Text, assignControl.Items[assignControl.SelectedIndex].Value, CustomProperties[assignControl.Value]);
				}
			}
		}

		return dataTable;
	}

	public void UpdateRepeaterWithErrors(Dictionary<string, string> errors)
	{
		foreach (RepeaterItem rpItem in ColumnsRepeater.Items)
		{
			var errorPanel = (Panel)rpItem.FindControl("ErrorsPanel");
            var columnName = ResolveColumnName((Label)rpItem.FindControl("colName"));

			if (errors.ContainsKey(columnName.Text))
			{
				errorPanel.Controls.Add(new LiteralControl(errors[columnName.Text]));
				errorPanel.Visible = true;
			}
		}
	}

    private Label ResolveColumnName(Label control)
    {
        if (control.Text.Equals(LatitudeStr))
        {
            control.Text = "WorldMapPoint.Latitude";
            return control;
        }

        if (control.Text.Equals(LongitudeStr))
        {
            control.Text = "WorldMapPoint.Longitude";
            return control;
        }
        return control;
    }
}