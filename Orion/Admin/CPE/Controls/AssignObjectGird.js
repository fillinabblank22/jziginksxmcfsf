﻿Ext.namespace('SW');
Ext.namespace('SW.Core');

SW.Core.assignments = function () {
    ORION.prefix = "Core_Assignments_";

    var batchSize = 100;
    var batchDelay = 10;
    var gridPageSize = 15;

    var record = Ext.data.Record.create([
            { name: 'Definition' },
            { name: 'Name', sortType: Ext.data.SortTypes.asUCString },
            { name: 'FullName' },
            { name: 'MemberStatus' },
            { name: 'Entity' }
        ]);
    var definitionIndex = 0;
    var nameIndex = 1;
    var fullNameIndex = 2;
    var memberStatusIndex = 3;
    var entityIndex = 4;

    var selectorModel;
    var searchText = '';
    var dataStore;
    var grid;
    var gridPanel;
    var tree;
    var treePanel;
    var itemsToDelete;
    var itemsToAdd;
    var mask;
    var someAlreadyExist;
    var groupByValue;

    var entityType;
    var entityDisplayName;
    var isManagedEntityType;
    var addToGroupButtonClientID = '';
    var removeFromGroupButtonClientID = '';
    var initialData = new Array();
    var headerText;

    var nodesToRemove; // remove these nodes at the end of the last batch

    function renderName(value, meta, record) {
        if (isManagedEntityType)
            return String.format('<span class="StatusIcon"><img src="/Orion/StatusIcon.ashx?entity={0}&amp;id={1}&amp;status={2}&amp;size=small" /></span> {3} ', record.data.Entity, record.data.Definition.split('=').pop(), record.data.MemberStatus, value);
        return value;
    }

    GetGridState = function () {
        // get all definitions that are used in grid
        var gridState = [];
        for (var i = 0; i < grid.store.arraySource.length; i++) {
            gridState.push(grid.store.arraySource[i][definitionIndex]);
        }

        return gridState;
    };

    ShowAddMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_32;E=js}"
        });
        mask.show();
    };

    ShowRemoveMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_33;E=js}"
        });
        mask.show();
    };

    HideMask = function () {
        grid.store.resumeEvents();
        grid.store.fireEvent('datachanged');
        mask.hide();

        RefreshTreeIfEmpty();
    };

    RefreshTreeIfEmpty = function () {
        // if last item in tre is "There are XX more items" (entity attribute is empty), relaod tree
        if ((tree.root.childNodes.length == 1) && (!tree.root.childNodes[0].attributes.entity)) {
            LoadGroups();
        }
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;
    };

    LoadGroupByProperties = function (selectValue) {
        var groupBySelect = $("#groupBySelect");
        ORION.prefix = 'CustomProperties_' + entityType + '_';
        groupBySelect.unbind('change').change(function () {
            groupByValue = this.options[this.selectedIndex].value;
            CancelSearch(false);
            ORION.Prefs.save('Grouping', groupByValue);
            LoadGroupsData(groupByValue);
        });

        groupByValue = groupBySelect.val();
        LoadGroupsData(groupByValue);
    };

    GetGroups = function (itemType, groupBy, searchValue, onSuccess) {
        //ToDo: all entity related methods should be moved to some common Entity web service
        ORION.callWebService("/Orion/Services/CPEManagement.asmx", "GetCustomPropertyEntityGroups", { entityType: itemType, groupByProperty: groupBy, searchValue: searchValue, excludeDefinitions: GetGridState() }, onSuccess);
    };

    // Changes icon on gived TreeNode in runtime
    SetNodeIcon = function (node, icon) {
        if ((node.ui) && (node.ui.iconNode)) {
            node.ui.iconNode.src = icon;
        }
    };

    CancelSearch = function (reloadTree) {
        $('[id=searchBox]').val('');
        searchText = '';

        if (reloadTree)
            LoadGroups();
    };

    DoSearch = function () {
        LoadGroups();
    };

    GetLabelForNode = function (name, count) {
        return String.format("{0} ({1})", Ext.util.Format.htmlEncode(name), count);
    };

    UpdateGroupItemsCount = function (node, newCount) {
        node.attributes.groupCount = newCount;
        node.setText(GetLabelForNode(node.attributes.groupName, node.attributes.groupCount));
    };

    CheckChange = function (node, checked) {
        if (checked) {
            node.getUI().addClass('x-tree-selected');
        } else {
            node.getUI().removeClass('x-tree-selected');
            if ((node.parentNode) && (node.attributes.propagateCheck)) {
                node.parentNode.attributes.propagateCheck = false;
                node.parentNode.getUI().removeClass('x-tree-selected');
                node.parentNode.getUI().toggleCheck(false);
                node.parentNode.attributes.propagateCheck = true;
            }
        }
    };

    LoadEntitiesGrouped = function (itemType, groupBy, searchValue, result, notInitialLoad, parentNode) {
        var bInitialLoad = true;
        if (typeof notInitialLoad != "undefined") {
            bInitialLoad = !notInitialLoad;
        }

        if (bInitialLoad) {
            ClearTree();
        }

        var myLoader = null;
        for (var i = 0; i < result.length; i++) {
            var item = result[i];
            if (item.Count > 0) {

                myLoader = new Ext.tree.TreeLoader({
                    dataUrl: '/Orion/Admin/CPE/Controls/AssignmentObjectsTreeProvider.ashx',
                    listeners: {
                        // init load event handler
                        beforeload: {
                            fn: function (treeLoader, node) {
                                this.baseParams.entityType = itemType;
                                this.baseParams.groupBy = groupBy;
                                this.baseParams.searchValue = searchValue;
                                if (bInitialLoad) {
                                    this.baseParams.value = node.attributes.value;
                                }
                                else {
                                    this.baseParams.value = parentNode.attributes.value;
                                }

                                this.baseParams.excludeDefinitions = GetGridState();
                                this.baseParams.initialLoad = bInitialLoad;
                                SetNodeIcon(node, '/Orion/images/AJAX-Loader.gif', false);
                            }
                        },
                        // after load event handler
                        load: {
                            fn: function (treeLoader, node, response) {
                                node.expanded = true;
                                SetNodeIcon(node, node.attributes.icon);

                                if (bInitialLoad) {
                                    Ext.each(node.childNodes, function (child) {
                                        if (((typeof child.attributes.attributes) != "undefined") && child.attributes.attributes.loadNextItems) {
                                            child.on("click", function () { node.removeChild(child); LoadEntitiesGrouped(itemType, groupBy, searchValue, result, true, node); });
                                        }
                                    });
                                }
                                else {
                                    var nresult = Ext.util.JSON.decode(response.responseText);
                                    for (var y = 0; y < nresult.length; y++) {
                                        var newNode = new Ext.tree.TreeNode(nresult[y]);
                                        parentNode.appendChild(newNode);
                                    }
                                }
                            }
                        }
                    }
                });

                var node = new Ext.tree.AsyncTreeNode({
                    id: 'tree-node-' + (i),
                    text: GetLabelForNode(item.Name, item.Count),
                    value: item.Name,
                    groupCount: item.Count,
                    groupName: item.Name,
                    icon: isManagedEntityType ? String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small', itemType, item.Status) : Ext.BLANK_IMAGE_URL,
                    allowDrag: true,
                    isGroup: true,
                    leaf: false,
                    checked: false,
                    propagateCheck: true,
                    listeners: {
                        'checkchange': function (node, checked) {
                            if (node.attributes.propagateCheck) {
                                node.eachChild(function (n) {
                                    n.attributes.propagateCheck = false;
                                    n.checked = checked;
                                    n.getUI().toggleCheck(checked);
                                    n.attributes.propagateCheck = true;
                                });
                            }

                            if (checked) {
                                node.getUI().addClass('x-tree-selected');
                            } else {
                                node.getUI().removeClass('x-tree-selected');
                            }
                        }
                    },
                    loader: myLoader
                });

                node.attributes.loader.baseParams.icon = node.attributes.icon;

                if (bInitialLoad) {
                    tree.root.appendChild(node);
                }
            }
            else {
                tree.root.appendChild(new Ext.tree.TreeNode({
                    text: item.Name,
                    allowDrag: false,
                    icon: isManagedEntityType ? '/Orion/StatusIcon.ashx?entity=&status=&size=small' : Ext.BLANK_IMAGE_URL
                }));
            }
        }

        if (!bInitialLoad) {
            myLoader.load(new Ext.tree.TreeNode(), null, null);
        }
    };

    LoadGroups = function () {
        var itemType = GetItemType();
        var groupBy = GetGroupBy();
        var searchValue = GetSearchValue();
        var loadText = "@{R=Core.Strings;K=WEBJS_VB0_1;E=js}";

        if (searchValue) {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchCancel.gif');
            $('[id=searchButton]').unbind('click');
            $('[id=searchButton]').click(function () { CancelSearch(true); });
            loadText = "@{R=Core.Strings;K=WEBJS_TM0_34;E=js}";
        } else {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchIcon.gif');
            $('[id=searchButton]').unbind('click');
            $('[id=searchButton]').click(DoSearch);
        }

        ClearTree();
        tree.root.appendChild({ text: loadText, icon: '/Orion/images/AJAX-Loader.gif', expandable: false, leaf: true });

        if ((itemType) && (groupBy)) {
            GetGroups(itemType, groupBy, searchValue, function (result) { LoadEntitiesGrouped(itemType, groupBy, searchValue, result, false); });
        } else if (itemType) {
            // no grouping - load entites directly
            LoadEntitiesNonGrouped(itemType, searchValue);
        }
    };

    LoadGroupsData = function (groupBy) {
        var itemType = GetItemType();
        //var groupBy = GetGroupBy();
        var searchValue = GetSearchValue();
        var loadText = "@{R=Core.Strings;K=WEBJS_VB0_1;E=js}";

        if (searchValue) {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchCancel.gif');
            $('[id=searchButton]').unbind('click');
            $('[id=searchButton]').click(function () { CancelSearch(true); });
            loadText = "@{R=Core.Strings;K=WEBJS_TM0_34;E=js}";
        } else {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchIcon.gif');
            $('[id=searchButton]').unbind('click');
            $('[id=searchButton]').click(DoSearch);
        }

        ClearTree();
        tree.root.appendChild({ text: loadText, icon: '/Orion/images/AJAX-Loader.gif', expandable: false, leaf: true });

        if ((itemType) && (groupBy)) {
            GetGroups(itemType, groupBy, searchValue, function (result) { LoadEntitiesGrouped(itemType, groupBy, searchValue, result, false); });
        } else if (itemType) {
            // no grouping - load entites directly
            LoadEntitiesNonGrouped(itemType, searchValue);
        }
    };

    LoadEntitiesNonGrouped = function (itemType, searchValue, notInitialLoad) {
        var bInitialLoad = true;
        if (typeof notInitialLoad != "undefined") {
            bInitialLoad = !notInitialLoad;
        }

        var loader = new Ext.tree.TreeLoader({
            dataUrl: '/Orion/Admin/CPE/Controls/AssignmentObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, node) {
                        this.baseParams.entityType = itemType;
                        this.baseParams.searchValue = searchValue;
                        this.baseParams.excludeDefinitions = GetGridState();
                        this.baseParams.initialLoad = bInitialLoad;
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, node, response) {
                        if (bInitialLoad) {
                            ClearTree();
                        }

                        var result = Ext.util.JSON.decode(response.responseText);
                        for (var i = 0; i < result.length; i++) {
                            var child = new Ext.tree.TreeNode(result[i]);
                            if (bInitialLoad) {
                                if (((typeof child.attributes.attributes) != "undefined") && child.attributes.attributes.loadNextItems) {
                                    child.on("click", function () { tree.root.removeChild(child); LoadEntitiesNonGrouped(itemType, searchValue, true); });
                                }
                            }

                            tree.root.appendChild(child);
                        }
                    }
                }
            }
        });
        loader.load(new Ext.tree.TreeNode(), null, null);
    };

    ClearTree = function () {
        while (tree.root.firstChild) {
            tree.root.removeChild(tree.root.firstChild);
        }
    };

    GetItemsData = function () {
        var data = '[';
        var first = true;
        for (var i = 0; i < grid.store.arraySource.length; i++) {
            if (!first) {
                data += ",";
            }
            data += Ext.util.JSON.encode({
                Definition: grid.store.arraySource[i][definitionIndex],
                Name: grid.store.arraySource[i][nameIndex],
                FullName: grid.store.arraySource[i][fullNameIndex],
                MemberStatus: grid.store.arraySource[i][memberStatusIndex],
                Entity: grid.store.arraySource[i][entityIndex]
            });
            first = false;
        }
        data += ']';
        return data;
    };

    RemoveBatch = function () {
        if (isSelectAllMode) {
            grid.store.arraySource = [];
            itemsToDelete = [];
            isSelectAllMode = false;
        }
        else if (itemsToDelete.length > 0) {
            for (var i = itemsToDelete.length - 1; i >= 0; i--) {
                if (i < 0)
                    break;
                var index = 0;
                for (var j = 0; j < grid.store.arraySource.length; j++) {
                    if (grid.store.arraySource[j][definitionIndex] == itemsToDelete[i].data.Definition) {
                        index = j;
                        break;
                    }
                }
                grid.store.arraySource.splice(index, 1);
                itemsToDelete.remove(itemsToDelete[i]);
            }
        }

        grid.setTitle(String.format(headerText, grid.getStore().arraySource.length));
        clearSelectAllTooltip();
        grid.store.reload();

        if (itemsToDelete.length > 0) {
            setTimeout(RemoveBatch, batchDelay);
        }
        else {
            LoadGroups();
            HideMask();
        }
    };

    RemoveSelectedItems = function (items) {
        ShowRemoveMask();
        itemsToDelete = items;
        RemoveBatch();
    };

    RemoveCheckedNodesFromGrid = function () {
        if (selectorModel.getCount() > 0) {
            Ext.Msg.confirm(
				"@{R=Core.Strings;K=WEBJS_SO0_32;E=js}", "@{R=Core.Strings;K=WEBJS_SO0_33;E=js}",
				function (btn, text) {
				    if (btn == "yes") {
				        RemoveSelectedItems(selectorModel.getSelections());
				    }
				}
			);
        }
    };

    ReloadTree = function () {
        var itemType = GetItemType();
        var groupBy = GetGroupBy();
        if (groupBy) {
            $.each(tree.root.childNodes, function (index, node) {
                if (node.loaded) {
                    node.attributes.loader.baseParams.expandAfterLoad = node.expanded;
                    node.reload(function () { }, node);
                }
            });
        } else {
            LoadEntitiesNonGrouped(itemType);
        }
    };

    GetItemType = function () {
        return entityType;
    };

    GetGroupBy = function () {
        return groupByValue;
    };

    GetSearchValue = function () {
        return searchText;
    };

    InitDragDrop = function () {
        nodesToRemove = [];

        var gridDropTargetEl = grid.getView().el.dom.childNodes[0].childNodes[1];
        var gridDropTarget = new Ext.dd.DropTarget(gridDropTargetEl, {
            ddGroup: 'treeDDGroup',
            copy: false,
            notifyDrop: function (ddSource, e, data) {
                if (data.node.attributes.isGroup) {
                    AddChildNodesToGrid(data.node);
                } else {
                    var parentNode = data.node.parentNode;

                    AddNodeToGrid(data.node);
                    data.node.remove(true);

                    if (parentNode != null && parentNode.childNodes.length == 0) {
                        parentNode.remove(true);
                    } else if (parentNode != null) {
                        UpdateGroupItemsCount(parentNode, parentNode.attributes.groupCount - 1);
                    }

                    RefreshTreeIfEmpty();
                }

                return true;
            }
        });
    };

    ExpandAndAdd = function (node, callback) {
        if (typeof itemsToAdd === 'undefined') {
            itemsToAdd = [];
        }
        var loader = new Ext.tree.TreeLoader({
            dataUrl: '/Orion/Admin/CPE/Controls/AssignmentObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, n) {
                        this.baseParams.entityType = GetItemType();
                        this.baseParams.groupBy = GetGroupBy();
                        this.baseParams.searchValue = GetSearchValue();
                        this.baseParams.value = n.attributes.value;
                        this.baseParams.excludeDefinitions = GetGridState();
                        this.baseParams.initialLoad = true;
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, n, response) {
                        var result = Ext.util.JSON.decode(response.responseText);
                        for (var i = 0; i < result.length; i++) {
                            var child = new Ext.tree.TreeNode(result[i]);
                            itemsToAdd.push(child);
                        }
                        if (itemsToAdd.length > 0) {
                            if (callback)
                                AddBatch(callback);
                            else
                                AddBatch();
                            node.remove(this);
                        } else {
                            node.remove(true);

                            if (callback)
                                callback();
                        }
                    }
                }
            }
        });
        loader.load(node, null, null);
    };

    AddBatch = function (callback) {
        ShowAddMask();
        if (itemsToAdd.length > 0) {
            for (var i = itemsToAdd.length - 1, j = 0; i >= 0, j < batchSize; i--, j++) {
                if (i < 0)
                    break;
                var child = itemsToAdd[i];

                if (!child.attributes.entity) {
                    itemsToAdd.remove(child);
                    continue;
                }

                var parentNode = child.parentNode;
                if (findExact(child.attributes.id) != -1) {
                    someAlreadyExist = true;
                } else {
                    grid.store.arraySource.push([
                            child.attributes.id,
                            child.attributes.text,
                            child.attributes.fullName,
                            child.attributes.status,
                            child.attributes.entity
                        ]);
                    child.remove();

                    if (parentNode != null) {
                        UpdateGroupItemsCount(parentNode, parentNode.attributes.groupCount - 1);
                    }
                }

                itemsToAdd.remove(child);

                if ((parentNode) && (!parentNode.hasChildNodes())) {
                    nodesToRemove.push(parentNode);
                }
            }
            grid.setTitle(String.format(headerText, grid.getStore().arraySource.length));
            grid.store.arraySource.sort(dataStoreCompare);
            grid.store.reload();
        }


        if (itemsToAdd.length > 0) {
            setTimeout(function () { AddBatch(callback); }, batchDelay);
        }
        else {
            if (callback) {
                callback();
            }

            if (nodesToRemove != null) {
                $.each(nodesToRemove, function (index, node) { node.remove(true); });
                nodesToRemove = [];
            }
        }
    };

    AddCheckedNodesToGrid = function () {
        someAlreadyExist = false;
        itemsToAdd = [];
        nodesToRemove = [];
        var anythingSelected = false;
        ShowAddMask();
        var responses = 0;
        var toExpand = [];
        for (var i = 0; i < tree.root.childNodes.length; i++) {
            var n = tree.root.childNodes[i];
            if (n.attributes.isGroup) {
                if (n.attributes.checked) {
                    anythingSelected = true;
                    toExpand.push(n);
                } else {
                    n.eachChild(function (child) {
                        if (child.attributes.checked) {
                            anythingSelected = true;
                            itemsToAdd.push(child);
                        }
                    });
                }
            } else if (n.attributes.checked) {
                anythingSelected = true;
                itemsToAdd.push(n);
            }
        }

        for (var i = 0; i < toExpand.length; i++) {
            ExpandAndAdd(toExpand[i], function () {
                responses++;
                // we need to hide load mask when get last resporse from ExpandAndAdd
                if (toExpand.length == responses)
                    HideMask();
            });
        }

        if (!anythingSelected)
            HideMask();

        if (itemsToAdd.length > 0)
            AddBatch(HideMask);
    };

    AddChildNodesToGrid = function (node) {
        someAlreadyExist = false;
        ShowAddMask();
        ExpandAndAdd(node, HideMask);
    };

    function findExact(id) {
        for (var i = 0; i < grid.store.arraySource.lenght; i++) {
            if (id == grid.store.arraySource[definitionIndex]) {
                return i;
            }
        }
        return -1;
    }

    AddNodeToGrid = function (node) {
        if (findExact(node.attributes.id) != -1) {
            Ext.Msg.alert("@{R=Core.Strings;K=WEBJS_TM0_40;E=js}", "@{R=Core.Strings;K=WEBJS_SO0_35;E=js}");
            return;
        }

        grid.store.arraySource.push([
                node.attributes.id,
                node.attributes.text,
                node.attributes.fullName,
                node.attributes.status,
                node.attributes.entity
            ]);

        grid.setTitle(String.format(headerText, grid.getStore().arraySource.length));
        grid.store.arraySource.sort(dataStoreCompare);
        grid.store.reload();
    };

    ToggleAllCheckboxes = function (value) {
        tree.root.eachChild(function (n) {
            n.attributes.propagateCheck = false;
            n.checked = value;
            n.getUI().toggleCheck(value);
            n.attributes.propagateCheck = true;

            if (n.attributes.isGroup) {
                n.eachChild(function (child) {
                    child.attributes.propagateCheck = false;
                    child.checked = value;
                    child.getUI().toggleCheck(value);
                    child.attributes.propagateCheck = true;
                });
            }
        });
    };

    SelectAllInTree = function () {
        ToggleAllCheckboxes(true);
    };

    SelectNoneInTree = function () {
        ToggleAllCheckboxes(false);
    };

    InitTree = function (headerText) {
        selectPanel = new Ext.Container({
            applyTo: 'GroupItemsSelector',
            region: 'north',
            height: 137,
            layout: 'fit'
        });

        var toolbar = new Ext.Toolbar({
            height: 27,
            region: 'north',
            items: [{
                id: 'SelectAllButton',
                text: '@{R=Core.Strings;K=WEBJS_VB0_36;E=js}',
                iconCls: 'selectAllButton',
                handler: function () {
                    SelectAllInTree();
                }
            }, '-', {
                id: 'SelectNoneButton',
                text: '@{R=Core.Strings;K=WEBJS_VB0_38;E=js}',
                iconCls: 'selectNoneButton',
                handler: function () {
                    SelectNoneInTree();
                }
            }]
        });

        tree = new Ext.tree.TreePanel({
            id: 'TreeExtPanel',
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDrag: true,
            height: 330, // border layout somehow doesn't work so we have to set height manually
            region: 'center',
            root: {
                text: '',
                id: 'root'
            },
            rootVisible: false,
            ddGroup: "treeDDGroup"
        });

        treePanel = new Ext.Panel({
            title: headerText,
            frame: true,
            region: 'west',
            width: 400,
            split: true,
            items: [
                selectPanel,
                toolbar,
                tree
            ]
        });
    };

    function dataStoreCompare(a, b) {
        if (a[nameIndex] < b[nameIndex])
            return -1;
        if (a[nameIndex] > b[nameIndex])
            return 1;
        return 0;
    };

    var isSelectAllMode = false;
    var originGridHeight;
    function selectAllTooltip() {
        if (!originGridHeight)
            originGridHeight = grid.getHeight();
        if (grid.store.data.items.length == grid.getSelectionModel().selections.items.length && grid.store.data.items.length != 0 && grid.store.data.items.length != grid.store.getTotalCount()) {
            var text = String.format("@{R=Core.Strings;K=WEBJS_IB0_42; E=js}", entityDisplayName, grid.getSelectionModel().selections.items.length);
            $('<div id="selectionTip" style="padding-left: 5px; background-color:#FFFFCC;">' + text + ' </div>').insertAfter(".x-panel-tbar");
            $("#selectionTip").append($("<br/><span id='selectAllLink' style='text-decoration: underline;color:red;cursor:pointer;'>" + String.format("@{R=Core.Strings;K=WEBJS_IB0_43; E=js}", entityDisplayName, grid.store.getTotalCount()) + "</span>").click(function () {
                isSelectAllMode = true;
                $("#selectionTip").text(String.format("@{R=Core.Strings;K=WEBJS_IB0_44; E=js}", entityDisplayName)).css("background-color", "white");
                $("#selectAllLink").remove();
                grid.setHeight(originGridHeight - $("#selectionTip").outerHeight());
            }));

            grid.setHeight(originGridHeight - $("#selectionTip").outerHeight());

        } else {
            clearSelectAllTooltip();
        }
    }
    function clearSelectAllTooltip() {
        if ($("#selectionTip").length != 0) {
            if (!originGridHeight)
                originGridHeight = grid.getHeight();
            $("#selectionTip").remove();
            if (isSelectAllMode) {
                isSelectAllMode = false;
            }
            grid.setHeight(originGridHeight);
        }
    }

    InitGrid = function (initFieldClientID, headerText) {
        var arrayReader = new Ext.data.ArrayReader({
            idIndex: definitionIndex
        },
        record);

        dataStore = new Ext.data.Store({
            reader: arrayReader,
            arraySource: Ext.decode($('#' + initFieldClientID).val()).sort(dataStoreCompare),
            arrayParams: {}, // this property will be shared with paging toolbar
            // override original method to make client-side paging working
            load: function (options) {
                if (!options)
                    options = { params: { start: 0, limit: gridPageSize} };
                options = Ext.apply({}, options);
                this.storeOptions(options);
                try {
                    // setup this property to make paging toolbar working without requst to server 
                    this.arrayParams = options.params;
                    // load data from array
                    var r = this.reader.readRecords(this.arraySource.slice(options.params.start, options.params.start + options.params.limit));
                    this.loadRecords(r, {}, true);
                } catch (e) {
                    this.handleException(e);
                    return false;
                }
            },
            reload: function (options) {
                // check if current page still exists, if no then load previous
                if (this.lastOptions.params.start >= this.getTotalCount()) {
                    this.lastOptions.params.start = Math.max(0, this.getTotalCount() - this.lastOptions.params.limit);
                }
                this.load(Ext.applyIf(options || {}, this.lastOptions));
            },
            getTotalCount: function () {
                return this.arraySource.length || 0;
            }
        });

        dataStore.load();

        selectorModel = new Ext.grid.CheckboxSelectionModel();
        selectorModel.on("rowselect", function () {
            selectAllTooltip();
        });
        selectorModel.on("rowdeselect", function () {
            clearSelectAllTooltip();
        });

        var pagingToolbar = new Ext.PagingToolbar(
                { store: dataStore,
                    pageSize: gridPageSize,
                    displayInfo: false,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_SO0_2; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    // override this method to use store.arrayParams 
                    onLoad: function (store, r, o) {
                        if (!this.rendered) {
                            this.dsLoaded = [store, r, o];
                            return;
                        }
                        selectorModel.clearSelections();
                        clearSelectAllTooltip()
                        var p = this.getParams();
                        this.cursor = (this.store.arrayParams && this.store.arrayParams[p.start]) ? this.store.arrayParams[p.start] : 0;
                        var d = this.getPageData(), ap = d.activePage, ps = d.pages;
                        this.afterTextItem.setText(String.format(this.afterPageText, d.pages));
                        this.inputItem.setValue(ap);
                        this.first.setDisabled(ap == 1);
                        this.prev.setDisabled(ap == 1);
                        this.next.setDisabled(ap == ps);
                        this.last.setDisabled(ap == ps);
                        this.refresh.enable();
                        this.updateInfo();
                        this.fireEvent('change', this, d);
                    }
                }
            );
        grid = new Ext.grid.GridPanel({
            store: dataStore,
            columns: [
                    selectorModel,
                    { id: 'FullName', header: '@{R=Core.Strings;K=WEBJS_TM0_42;E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'FullName', renderer: renderName }
                ],
            autoExpandColumn: 'FullName',
            sm: selectorModel,
            viewConfig: { forceFit: false },
            border: true,
            frame: true,
            hideHeaders: true,
            stripeRows: true,
            trackMouseOver: false,
            ddGroup: 'treeDDGroup',
            region: 'center',
            title: Ext.util.Format.htmlEncode(String.format(headerText, dataStore.arraySource.length)),
            loadMask: { msg: '@{R=Core.Strings;K=WEBJS_TM0_35;E=js}' },
            tbar: ['-',
                    {
                        text: '@{R=Core.Strings;K=WEBJS_VB0_36;E=js}',
                        iconCls: 'selectAllButton',
                        handler: function () {
                            selectorModel.selectAll();
                        }
                    }, '-', {
                        text: '@{R=Core.Strings;K=WEBJS_VB0_38;E=js}',
                        iconCls: 'selectNoneButton',
                        handler: function () {
                            selectorModel.clearSelections();
                            clearSelectAllTooltip();
                        }
                    }],
            bbar: pagingToolbar
        });

        var addButtonPanel = new Ext.Panel({
            border: false,
            region: 'west',
            width: 95,
            contentEl: 'AddButtonPanel'
        });

        gridPanel = new Ext.Panel({
            id: 'AddButtonPanelExtPanel',
            region: 'center',
            layout: 'border',
            border: false,
            items: [addButtonPanel, grid]
        });

        // grid.getSelectionModel().on("selectionchange", UpdateToolbarButtons);
    };

    return {
        SetAddToGroupButtonClientID: function (id) {
            addToGroupButtonClientID = id;
        },
        SetRemoveFromGroupButton: function (id) {
            removeFromGroupButtonClientID = id;
        },
        AddToInitialData: function (item) {
            initialData.push(item);
        },
        SelectButtonClick: function (thisBtn) {
            isSelectAllMode = false;
            var initFieldClientID = $(thisBtn).next().next().find('[id*="initialGridItems"]')[0].id;
            var selectObjectText = $(thisBtn).next().next().find('[id*="popupTitle"]').val();
            var treeHeaderText = $(thisBtn).next().next().find('[id*="groupingGridTitle"]').val();
            entityType = $(thisBtn).next().next().find('[id*="entityTypeData"]').val();
            entityDisplayName = $(thisBtn).next().next().find('[id*="entityDisplayName"]').val();
            isManagedEntityType = $(thisBtn).next().next().find('[id*="isManagedEntityType"]').val().toLowerCase() == "true";
            searchText = '';

            headerText = $(thisBtn).next().next().find('[id*="gridTitle"]').val();

            InitGrid(initFieldClientID, headerText);
            InitTree(treeHeaderText);

            $('#AddButtonPanel').height($('#AssignmentsTable').height());
            $('#' + addToGroupButtonClientID).click(AddCheckedNodesToGrid);
            $('#' + removeFromGroupButtonClientID).click(RemoveCheckedNodesFromGrid);
            $('#searchButton').click(DoSearch);
            $('#searchBox').keydown(function (event) {
                if (event.keyCode == '13') {
                    event.preventDefault();
                    DoSearch();
                }
            });

            // fix for $('#searchBox').val always return ""
            $('[id=searchBox]').keyup(function (event) {
                searchText = $(this).val();
            });

            grid.on('render', function () {
                InitDragDrop();
            });

            var win = new Ext.Window({
                title: selectObjectText, resizable: false, closable: true, closeAction: 'close', width: 800, height: 550, plain: true, modal: true, layout: 'border', items: [treePanel, gridPanel],
                buttons: [{
                    text: selectObjectText,
                    cls: 'x-btn-default',
                    handler: function () {
                        __doPostBack(thisBtn.id, GetItemsData());
                        win.close();
                    }
                },
                {
                    text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}', handler: function () { win.close(); }
                }]
            });

            LoadGroupByProperties("");
            win.show(this);
        },
        init: function () {
        }
    };
} ();

//Ext.onReady(SW.Core.assignments.init, SW.Core.assignments);
