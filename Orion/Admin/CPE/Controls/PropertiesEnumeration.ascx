<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PropertiesEnumeration.ascx.cs" Inherits="Orion_Admin_CPE_Controls_PropertiesEnumeration" %>
<orion:Include ID="Include1" runat="server" File="Enumeration.css" />
<orion:Include ID="Include2" runat="server" File="Admin/CPE/Controls/EditCP.js" />

<table>
<tr>
<td class="Enumerations enumerationGrid" valign="top" id="enumerationListTd" runat="server">
    <asp:UpdatePanel runat="server" id="UpdatePanel" updatemode="Conditional">
        <ContentTemplate>
        <asp:GridView ID="PropertiesEnumerationGrid" runat="server" automation="enumerationrangelist" AutoGenerateColumns="False" ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="PropertiesEnumerationGrid_RowCommand" OnRowDataBound="GridView_RowDataBound" OnDataBound="PropertiesEnumerationGrid_OnDataBound" >
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <label><%# DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_SO0_28, Container.DataItemIndex + 1)) %></label>
                        <asp:TextBox ID="EnumerationTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input enumerationItem" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Value").ToString()) %>' Enabled='<%# DefaultSanitizer.SanitizeHtml(Eval("Editable")) %>'></asp:TextBox>
                        <asp:ImageButton ID="CloseCross" ImageAlign="Top" Width="20px" Height="18px" runat="server" ImageUrl="~/Orion/Discovery/images/button_X_04.gif"
                        CommandName="DeleteRow" CommandArgument='<%# Container.DataItemIndex %>'/><br/>
                        <asp:CustomValidator
                            ID="EnumerationValidator" 
                            runat="server" 
                            ControlToValidate="EnumerationTextBox"
                            OnServerValidate="EnumerateValidation"
                            ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBCODE_VB0_27 %>" 
                            EnableClientScript="false"
                            Display="Dynamic" ValidateEmptyText="true">
                        </asp:CustomValidator>
                        <asp:CustomValidator
                            ID="DuplicityValidator" 
                            runat="server" 
                            ControlToValidate="EnumerationTextBox"
                            OnServerValidate="DuplicityValidation"
                            ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_YK0_3 %>" 
                            EnableClientScript="false"
                            Display="Dynamic">
                        </asp:CustomValidator>
                    </ItemTemplate>
                    <FooterTemplate>
                    <div class="sw-hatchbox addButton">
                        <orion:LocalizableButton id="AndButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_127  %>"  OnClick="AndButton_Clicked"/>
                    </div>
                    </FooterTemplate>
                </asp:TemplateField>
           </Columns>
        </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
<div>
    <br /><br />
</div>
</td>
</tr>
</table>
