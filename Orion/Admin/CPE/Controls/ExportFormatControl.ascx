<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExportFormatControl.ascx.cs"
    Inherits="ExportFormatControl" %>
<table>
    <tr>
        <td class="cp_table_leftcolumn" style="text-align:left;">
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_29) %></b>
        </td>
        </tr>
        <tr>
        <td>
            <asp:DropDownList runat="server" ID="exportFormats">
                <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO0_25 %>" Value="0" Selected="true"></asp:ListItem>
                <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO0_26 %>" Value="1"></asp:ListItem>
                <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO0_27 %>" Value="2"></asp:ListItem>
                <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO0_28 %>" Value="3"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
</table>