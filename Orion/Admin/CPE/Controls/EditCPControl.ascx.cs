﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_CPE_Controls_EditCPControl : System.Web.UI.UserControl
{
    private const string UsageHtmlAttribute = "usage";
    private static readonly Log _log = new Log();
    private bool _usageCheckboxesLoaded = false;

    protected const int DefaultCPSize = 400;

	#region public properties 
	public string PropertyName
	{
		get { return cpName.Text; }
	}

	public string PropertyDescription
	{
		get { return cpDescription.Text; }
	}

	public string DatabaseColumnType
	{
		get { return cpFormat.SelectedValue; }
	}

	public List<string> EnumerationValues
	{
		get
		{
			if (restrictInputs.Visible && cpRestrictValues.Checked)
			{
				return PropertiesEnumeration.GetEnumerationValues();
			}
			return null;
		}
	}

    public int PropertySize
    {
        get
        {
            if (cpFormat.SelectedValue.Equals("nvarchar", StringComparison.OrdinalIgnoreCase))
            {
                if (string.IsNullOrEmpty(hfCpPropertySize.Value)) return DefaultCPSize;
                int size;
                if (int.TryParse(hfCpPropertySize.Value, out size))
                    return size;
            }
            return 0;
        }
    }

    public bool Mandatory
    {
        get { return requiredCP.Checked; }
    }

    public string Default
    {
        get { return string.Empty; } // EditCpValueControl.Text; }
    }

    public IDictionary<string, bool> UsageFlags
    {
        get
        {
            if (cpUsageTableRow.Visible)
            {
                Dictionary<string, bool> usageFlags = new Dictionary<string, bool>();
                foreach (RepeaterItem rpItem in usagesRepeater.Items)
                {
                    CheckBox checkBox = (CheckBox)rpItem.FindControl("usageCheckBox");
                    usageFlags[checkBox.Attributes[UsageHtmlAttribute]] = checkBox.Checked;
                }
                return usageFlags;
            }

            return null;
        }
    }

    [Obsolete("deprecated per SWIS entity usage, use TargetEntityName and SourceEntityName", true)]
	[PersistenceMode(PersistenceMode.Attribute)]
	public string TableName
	{
		get
		{
			if (ViewState["EditCP_TableName"] == null)
			{
				ViewState["EditCP_TableName"] = string.Empty;
			}
			return (string)ViewState["EditCP_TableName"];
		}
		set
		{
			ViewState["EditCP_TableName"] = value;
		}
	}

    [PersistenceMode(PersistenceMode.Attribute)]
    public string TargetEntityName
    {
        get
        {
            if (ViewState["EditCP_TargetEntityName"] == null)
            {
                ViewState["EditCP_TargetEntityName"] = string.Empty;
            }
            return (string)ViewState["EditCP_TargetEntityName"];
        }
        set
        {
            ViewState["EditCP_TargetEntityName"] = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string SourceEntityName
    {
        get
        {
            if (ViewState["EditCP_SourceEntityName"] == null)
            {
                ViewState["EditCP_SourceEntityName"] = string.Empty;
            }
            return (string)ViewState["EditCP_SourceEntityName"];
        }
        set
        {
            ViewState["EditCP_SourceEntityName"] = value;
        }
    }

	public bool IsEdit
	{
		get
		{
			if (ViewState["EditCP_IsEdit"] == null)
			{
				ViewState["EditCP_IsEdit"] = false;
			}
			return (bool)ViewState["EditCP_IsEdit"];
		}
		set
		{
			ViewState["EditCP_IsEdit"] = value;
		}
	}
	#endregion 

	protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["CustomPropertyId"] != null)
        {
            hfCustomPropertyId.Value = Request["CustomPropertyId"];
        }
	    hfCpSizeDialogOk.Value = cpSizeDialogOk.UniqueID;
	    if (!IsPostBack)
        {
            UpdateDataFormatHelpfullText();
            EnsureUsageCheckboxesAreLoaded();
            //if (string.IsNullOrEmpty(tbPropertySize.Text))
            //    tbPropertySize.Text = DefaultCPSize.ToString();
        }

	    PropertiesEnumeration.PropertyType = GetDataFormatType();
	    PropertiesEnumeration.PropertySize = PropertySize;
	    PropertiesEnumeration.PropertiesEnumerationChanged += PropertiesEnumerationChanged;
    }

    private bool SupportsUsages
    {
        get { return CustomPropertyHelper.IsCustomPropertyUsageSupported(SourceEntityName); }
    }

    private void EnsureUsageCheckboxesAreLoaded()
    {
        if (_usageCheckboxesLoaded)
            return;

        if (SupportsUsages)
        {
            usagesRepeater.DataSource = CustomPropertyHelper.GetCustomPropertyUsages(SourceEntityName);
            usagesRepeater.DataBind();
            cpUsageTableRow.Visible = true;
        }
        else
        {
            cpUsageTableRow.Visible = false;
        }

        _usageCheckboxesLoaded = true;
    }

    protected void usagesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item ||
          e.Item.ItemType == ListItemType.AlternatingItem)
      {
          CustomPropertyUsage usage = (CustomPropertyUsage) e.Item.DataItem;
          CheckBox checkBox = (CheckBox)e.Item.FindControl("usageCheckBox");
          checkBox.Attributes.Add(Orion_Admin_CPE_Controls_EditCPControl.UsageHtmlAttribute, usage.Name);
          checkBox.Text = usage.Label;
          checkBox.Checked = CustomPropertyUsage.DefaultUsages.Contains(usage.Name);

          if (!string.IsNullOrEmpty(usage.HelpText))
          {
              Literal helpfulText = (Literal) e.Item.FindControl("helpfulText");
              helpfulText.Text = usage.HelpText;
              Panel helpfulTextPanel = (Panel)e.Item.FindControl("helpfulTextPanel");
              helpfulTextPanel.Visible = true;
          }
      }
    }

	protected void cpFormat_SelectedIndexChanged(object sender, EventArgs e)
	{
        UpdateRequiredCPType(string.Empty);
		UpdateDataFormatHelpfullText();
		UpdateRestrictValues();
        //UpdateColumnSizeControl(DefaultCPSize);
	}

    protected void requiredCP_CheckedChanged(object sender, EventArgs e)
    {
        UpdateRequiredCPType(Default);
	}

    private void UpdateRequiredCPType(string defaultValue)
    {
        return; // remove this when we will have default value 
        requiredCPPlaceHolder.Visible = requiredCP.Checked;
        if (requiredCP.Checked)
        {
            CustomProperty cp = new CustomProperty
            {
                DatabaseColumnType = DatabaseColumnType,
                DatabaseColumnLength = PropertySize,
                Values = PropertiesEnumeration.Visible ? PropertiesEnumeration.GetEnumerationValues().ToArray() : new string[0],
                Mandatory = true,
                Default = defaultValue,
            };
            EditCpValueControl.Configure(true, cp, true);
            if (!string.IsNullOrEmpty(defaultValue))
                EditCpValueControl.Text = string.Empty;
            requiredCpUpdatePanel.Update();
        }
    }

    protected void cpRestrictValues_CheckedChanged(object sender, EventArgs e)
    {
        PropertiesEnumeration.Visible = cpRestrictValues.Checked;
        UpdateRequiredCPType(Default);
    }

    protected void PropertiesEnumerationChanged()
    {
        return; //remove this when default for mandatory occurs
        string defa = this.Default;
        EditCpValueControl.Clear();
        UpdateRequiredCPType(defa);
    }

    protected void ValidatePropertyName(object source, ServerValidateEventArgs args)
	{
		// column name couldn't be empty
		if (string.IsNullOrEmpty(args.Value))
		{
			args.IsValid = false;
			PropertyNameValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_27;
			return;
		}
		// column name can't contain spaces
		if (args.Value.Contains(" "))
		{
			args.IsValid = false;
			PropertyNameValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_SO0_19;
			return;
		}
		var propName = CommonHelper.FormatCustomPropertyName(args.Value);
		try
		{
		    var validationResult = CustomPropertyHelper.ValidateCustomProperty(
                new CustomProperty
                    {
                        PropertyName = propName, 
                        TargetEntity = TargetEntityName, 
                        SourceEntity = SourceEntityName,
                        Description = PropertyDescription,
                        DatabaseColumnType = DatabaseColumnType,
                        Values = (EnumerationValues==null)? new string[0] : EnumerationValues.ToArray(),
                        UsageFlags = UsageFlags
                    });
            switch (validationResult.Status)
            {
                case CustomPropertyValidationStatus.IsSystem:
                    args.IsValid = false;
                    PropertyNameValidator.ErrorMessage = string.Format(Resources.CoreWebContent.WEBCODE_SO0_20, propName,
                        string.Format(
                            "<span class='LinkArrow' style='color: #336699;'>&raquo;</span>&nbsp;<a href='{0}' target='_blank' style='text-decoration: underline; color: #336699;'>",
                            KnowledgebaseHelper.GetKBUrl(4136)),
                            "</a>");
                    return;
                case CustomPropertyValidationStatus.IsReserved:
                    args.IsValid = false;
                    PropertyNameValidator.ErrorMessage = string.Format(Resources.CoreWebContent.WEBCODE_SO0_21, propName,
                        string.Format(
                            "<span class='LinkArrow' style='color: #336699;'>&raquo;</span>&nbsp;<a href='{0}' target='_blank' style='text-decoration: underline; color: #336699;'>",
                            KnowledgebaseHelper.GetKBUrl(4135)),
                            "</a>");
                    return;
                case CustomPropertyValidationStatus.Error:
                    args.IsValid = false;
                    PropertyNameValidator.ErrorMessage = validationResult.ErrorMessage;
                    return;
                case CustomPropertyValidationStatus.Exists:
                    if (!IsEdit)
                    {
                        args.IsValid = false;
                        PropertyNameValidator.ErrorMessage = string.Format(Resources.CoreWebContent.WEBCODE_SO0_22, propName);
                        return;
                    }
                    break;
            }
		}
		catch (Exception e)
		{
			_log.ErrorFormat("Validating Custom property name '{0}' for '{1}' failed with exception: {2}", propName, TargetEntityName, e);
			throw;
		}

		args.IsValid = true;
	}

    public void UpdateFields(string propName, string description, string type, int size, bool mandatory, string defaultValue, string[] values, IDictionary<string, bool> usageFlags, bool readOnly)
    {
        EditCPFlag.Value = readOnly.ToString();

        if (!string.IsNullOrEmpty(type))
        {
            cpName.Text = propName;
            cpName.ReadOnly = readOnly;
            cpDescription.Text = description;
        }
        else
        {
            cpName.Text = string.Empty;
            cpDescription.Text = string.Empty;
        }

        cpFormat.Enabled = !readOnly;
        SetDataFormat(type, size);

        PropertiesEnumeration.PropertyKey = SourceEntityName + ":" + propName;
        PropertiesEnumeration.PropertySize = size;

        if (values != null && values.Length > 0)
        {
            cpRestrictValues.Checked = true;
            PropertiesEnumeration.Visible = true;
            PropertiesEnumeration.PropertyType = GetDataFormatType();
            PropertiesEnumeration.SetEnumerationValues(values, !readOnly);
        }
        // when on edit page we check this check box 
        // we should get all existing custom property values to propose them as restricted values
        else if (IsEdit)
        {
            using (var dal = new CustomPropertyDAL())
            {
                var list = dal.GetCustomPropertyObjectValuesBySource(PropertyName, SourceEntityName);
                if (list.Count > 0)
                {
                    PropertiesEnumeration.PropertyType = GetDataFormatType();
                    PropertiesEnumeration.SetEnumerationValues(list.ToArray(), false);
                }
            }
        }

        if (mandatory)
        {
            requiredCP.Checked = mandatory;
            UpdateRequiredCPType(defaultValue);
        }

        // this method can be called before Page_Load is executed so usage checkboxes must be loaded here
        EnsureUsageCheckboxesAreLoaded();

        foreach (RepeaterItem rpItem in usagesRepeater.Items)
        {
            CheckBox checkBox = (CheckBox)rpItem.FindControl("usageCheckBox");
            bool allowed;
            if (usageFlags.TryGetValue(checkBox.Attributes[UsageHtmlAttribute], out allowed))
            {
                checkBox.Checked = allowed;
            }

            if (IsEdit && checkBox.Checked)
            {
                checkBox.Enabled = false;
            }
        }
    }

	public void UpdateFields(string propName, string description, string type, string[] values, IDictionary<string, bool> usageFlags, bool readOnly)
	{
        UpdateFields(propName, description, type, -1, false, string.Empty, values, usageFlags, readOnly);
	}

    public void UpdateFields(string propName, string description, string type, string[] values, IDictionary<string, bool> usageFlags)
	{
        UpdateFields(propName, description, type, values, usageFlags, false);
	}

	private void SetDataFormat(string type, int size)
	{
	    hfCpPropertySize.Value = (size == -1 ? 0 : size).ToString();
		cpFormat.SelectedValue = type;
		UpdateDataFormatHelpfullText();
		UpdateRestrictValues();
	}

    private void UpdateDataFormatHelpfullText()
	{
		switch (cpFormat.SelectedValue)
		{
			case "nvarchar":
                formatHelpfulText.InnerHtml = string.Format(Resources.CoreWebContent.WEBDATA_VL0_61,
                    PropertySize == 0 ? Resources.CoreWebContent.WEBDATA_VL0_62 : string.Format(Resources.CoreWebContent.WEBDATA_VL0_63, PropertySize), 
                    "<a href='javascript:propertySizeDlg();'>", "</a>");
		        break;
			case "int":
				formatHelpfulText.InnerText = Resources.CoreWebContent.WEBCODE_SO0_24;
				break;
			case "real":
				formatHelpfulText.InnerText = Resources.CoreWebContent.WEBCODE_SO0_25;
				break;
			case "datetime":
				formatHelpfulText.InnerText = Resources.CoreWebContent.WEBCODE_SO0_26;
				break;
			case "bit":
				formatHelpfulText.InnerText = Resources.CoreWebContent.WEBCODE_SO0_27;
				break;
		}
	}

	private void UpdateRestrictValues()
	{
		switch (cpFormat.SelectedValue)
		{
			case "datetime": 
			case "bit": 
				restrictLabel.Visible = false;
				restrictInputs.Visible = false;
				break;
			default:
				restrictLabel.Visible = true;
				restrictInputs.Visible = true;
				break;
		}
	}

	private Type GetDataFormatType()
	{
		return CustomPropertyHelper.GetPropertySystemTypeFromString(cpFormat.SelectedValue);
	}

	public void AppendNewField(string[] restrictedValuesToAdd) 
	{
		PropertiesEnumeration.AppendEnumerationValues(restrictedValuesToAdd);
	}
}