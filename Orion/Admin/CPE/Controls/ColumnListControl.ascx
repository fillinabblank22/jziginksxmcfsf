<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColumnListControl.ascx.cs" Inherits="Orion_Controls_ColumnListControl" %>
<%@ Reference Control="~/Orion/Admin/CPE/Controls/AncestoryPanel.ascx" %>

<script type="text/javascript">
    function selectAll(invoker) {
        var rp = $(invoker).parents('[id*=ColumnListContainer]')[0];
        var inputElements = rp.getElementsByTagName('input');

        for (var i = 0; i < inputElements.length; i++) {
            var myElement = inputElements[i];

            // Filter through the input types looking for checkboxes
            if (myElement.type === "checkbox") {
                // Use the invoker (our calling element) as the reference 
                //  for our checkbox status
                myElement.checked = invoker.checked;
            }
        }
    }

    function setUpRootCb(container) {
        var selectedAll = true;
        container.find('[id*=chkbx]').each(function () {
            selectedAll &= this.checked;
        });

        container.find('[id*=rootCB]').each(function () {
            this.checked = selectedAll;
        });
    }

    $(function () {
        setUpRootCb($("#<%= ColumnListContainer.ClientID %>"));
    });

        function toggleElement(elem, elemName) {
            var node = $(elem).siblings("div[id*=" + elemName + "]")[0];

            if (node) {
                $(node).toggle();
                if (elem.src.indexOf("Button.Expand.gif") > 0) {
                    elem.src = "/Orion/images/Button.Collapse.gif";
                } else {
                    elem.src = "/Orion/images/Button.Expand.gif";
                }
            }
        }
</script>

<orion:Include ID="Include1" runat="server" File="CustomPropertyEditor.css" />
<div runat="server" id="ColumnListContainer">
    <table style="width: 100%;">
        <tr>
            <td class="cp_table_leftcolumn" style="vertical-align: top; text-align: left;">
                <div class="GroupBox" style="padding: 0 0 0 0;">
                    <asp:Repeater ID="ColumnList" runat="server" OnItemDataBound="ColumnList_ItemDataBound">
                        <HeaderTemplate>
                            <table style="width: 100%!important">
                                <tr class="alternateRow">
                                    <td>
                                        <asp:CheckBox runat="server" Checked="true" CssClass="spaced_checkbox" ID="rootCB" OnClick="selectAll(this);" Text="<%# DefaultSanitizer.SanitizeHtml(Title) %>" />
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:CheckBox ID="chkbx" runat="server" Checked="true" CssClass="spaced_checkbox" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:PlaceHolder runat="server" ID="phAncestors"></asp:PlaceHolder>
                    <asp:Panel runat="server" ID="AdvancedPanel" CssClass="advanced-columns-panel">
                        <img src="/Orion/images/Button.Expand.gif" style="cursor: pointer;" onclick="toggleElement(this, 'AdvancedDiv');" alt="" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_216) %>
                        <div id="AdvancedDiv" runat="server" style="display: none; margin-left: 15px;">
                            <asp:Repeater ID="AdvancedColumnList" runat="server" OnItemDataBound="ColumnList_ItemDataBound">
                                <HeaderTemplate>
                                    <table style="width: 100%!important">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: left;">
                                            <asp:CheckBox ID="chkbx" runat="server" CssClass="spaced_checkbox" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                </div>
            </td>
            <td>
                <asp:CustomValidator ID="RequiredColumn" runat="server" OnServerValidate="ColumnsValidation" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_SO0_36 %>" EnableClientScript="false" Display="Dynamic"></asp:CustomValidator>
            </td>
        </tr>
    </table>
</div>