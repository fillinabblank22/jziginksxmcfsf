<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCPControl.ascx.cs"
    Inherits="Orion_Admin_CPE_Controls_EditCPControl" %>
<%@ Register Src="~/Orion/Admin/CPE/Controls/PropertiesEnumeration.ascx" TagPrefix="orion"
    TagName="PropertiesEnumeration" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<orion:Include ID="Include2" runat="server" File="Admin/CPE/Controls/EditCP.js" />
<style type="text/css">
    .requiredCPPlaceHolder {padding: 10px !important; vertical-align: middle;}
</style>
<div class="contentBlock">
    <asp:HiddenField ID="EditCPFlag" runat="server" Value="false" />
    <table class="editcptable" cellpadding="0" cellspacing="0">
        <tr>
            <td class="leftLabelColumn">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_7) %>
            </td>
            <td class="rightInputColumn">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding: 0px 0px 0px 0px;">
                            <asp:TextBox runat="server" ID="cpName" Width="200"></asp:TextBox>
                            <br />
                            <span class="helpfulText">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_6) %></span>
                        </td>
                        <td style="padding: 0px 0px 0px 5px;">
                            <asp:CustomValidator ID="PropertyNameValidator" runat="server" ControlToValidate="cpName"
                                OnServerValidate="ValidatePropertyName" ErrorMessage="*" EnableClientScript="false"
                                Display="Dynamic" ValidateEmptyText="true" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_19) %>
            </td>
            <td class="rightInputColumn">
                <asp:TextBox runat="server" ID="cpDescription" Width="520"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_20) %>
            </td>
            <td class="rightInputColumn">
                <asp:DropDownList runat="server" ID="cpFormat" AutoPostBack="true" OnSelectedIndexChanged="cpFormat_SelectedIndexChanged">
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_29 %>" Value="nvarchar"></asp:ListItem>
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_30 %>" Value="int"></asp:ListItem>
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_31 %>" Value="real"></asp:ListItem>
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_32 %>" Value="datetime"></asp:ListItem>
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_33 %>" Value="bit"></asp:ListItem>
                </asp:DropDownList>
                <span runat="server" id="formatHelpfulText" class="helpfulText" style="display: inline;"></span>
                <asp:HiddenField runat="server" ID="hfCpPropertySize" />
                <asp:HiddenField runat="server" ID="hfCustomPropertyId" />
                <asp:HiddenField runat="server" ID="hfCpSizeDialogOk" />
                <div id="cpSizeDialog" style="display: none;">
                    <div class="cpSizeDialog">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_57) %>&nbsp;<input id="propertySize" type="text" style="width: 50px;"/>&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_58) %>
                        <span id="validationMessage" style="color: red; display: none;"><br/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_193) %></span>
                        <div class="maxSize">
                            <input type="checkbox" id="useMaxSize" checked="checked"/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_59) %>
                        </div>
                        <div class="sw-suggestion" id="maxSizeWarn"><span class="sw-suggestion-icon">
                            </span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_60) %>
                        </div>
                    </div>
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton ID="cpSizeDialogOk" runat="server" LocalizedText="Save" DisplayType="Primary" OnClick="cpFormat_SelectedIndexChanged" CausesValidation="false"  />
                        <orion:LocalizableButton ID="cpSizeDialogCancel" LocalizedText="Cancel" DisplayType="Secondary" runat="server" OnClientClick="return false;" />
                    </div>
                </div>
                <div id="cpWarningDialog" style="display: none">
                    <div class="cpSizeDialog">
                        <div id="cpWarningDialogText"></div><br/>
                        <div id="cpWarningDialogAffected" style="padding-top: 10px">
                            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_81) %></span>
                            <ul id="cpValuesList" style="list-style: square; padding-left: 16px"></ul>
                            <span id="cpMoreValues"></span>
                        </div>
                    </div>
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton ID="cpWarningDialogTrim" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_78 %>" DisplayType="Primary" runat="server" OnClientClick="return false"  />
                        <orion:LocalizableButton ID="cpWarningDialogEdit" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_79 %>" DisplayType="Secondary" runat="server" OnClientClick="return false;" />
                        <orion:LocalizableButton ID="cpWarningDialogKeep" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_80 %>" runat="server" OnClientClick="return false;" />
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <asp:PlaceHolder runat="server" ID="PlaceHolder3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_170) %></asp:PlaceHolder>
            </td>
            <td class="rightInputColumn">
                <asp:PlaceHolder runat="server" ID="PlaceHolder4">
                    <asp:CheckBox runat="server" ID="requiredCP" AutoPostBack="true" OnCheckedChanged="requiredCP_CheckedChanged"
                        Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_202 %>" /><br />
                    <asp:PlaceHolder runat="server" ID="requiredCPPlaceHolder" Visible="false">
                        <table style="background-color: #f8f8f8; width: 520px;" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td class="requiredCPPlaceHolder">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_171) %>
                                </td>
                                <td class="requiredCPPlaceHolder">
                                    <asp:UpdatePanel ID="requiredCpUpdatePanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <orion:EditCpValueControl ID="EditCpValueControl" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
  
                                </td>
                            </tr>
                        </table>
                    </asp:PlaceHolder>
                </asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <asp:PlaceHolder runat="server" ID="restrictLabel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_9) %></asp:PlaceHolder>
            </td>
            <td class="rightInputColumn">
                <asp:PlaceHolder runat="server" ID="restrictInputs">
                    <asp:CheckBox runat="server" ID="cpRestrictValues" AutoPostBack="true" OnCheckedChanged="cpRestrictValues_CheckedChanged"
                        Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_18 %>" />
                    <orion:PropertiesEnumeration ID="PropertiesEnumeration" runat="server" Visible="false" OnPropertiesEnumerationChanged="PropertiesEnumerationChanged" />
                </asp:PlaceHolder>
            </td>
        </tr>
        <tr runat="server" ID="cpUsageTableRow" Visible="False">
            <td class="leftLabelColumn">
                <asp:PlaceHolder runat="server" ID="PlaceHolder1">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JT0_1) %></asp:PlaceHolder>
            </td>
            <td class="rightInputColumn">
                <asp:PlaceHolder runat="server" ID="PlaceHolder2">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JT0_2) %></asp:PlaceHolder>
                <asp:Repeater runat="server" ID="usagesRepeater" OnItemDataBound="usagesRepeater_ItemDataBound">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="usageCheckbox"></div><asp:CheckBox runat="server" ID="usageCheckBox" /></div>
                            <asp:Panel runat="server" CssClass="helpfulText checkboxHelpfulText" ID="helpfulTextPanel" Visible="False"><asp:Literal  runat="server" ID="helpfulText"></asp:Literal></asp:Panel>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</div>
