<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefineCPControl.ascx.cs"
    Inherits="Orion_Admin_CPE_Controls_DefineCPControl" %>
<%@ Register Src="~/Orion/Admin/CPE/Controls/EditCPControl.ascx" TagPrefix="orion"
    TagName="EditCPControl" %>
<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include2" runat="server" File="OrionCore.js" />
<orion:Include ID="Include3" runat="server" File="Admin/CPE/Controls/DefineCustomProperty.js" />
<div class="contentBlock">
    <table class="define_cp_table" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_16) %> </b>
            </td>
            <td>
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_17) %> </b>
            </td>
        </tr>
        <tr>
            <td class="define_cp_holder_left">
                <div id="ChoosePropertyGridPlaceHolder">
                </div>
            </td>
            <td class="define_cp_holder_right">
                <asp:UpdatePanel runat="server" ID="editCPUpdatePanel">
                    <ContentTemplate>
                        <asp:HiddenField runat="server" ID="hfPropertyName" />
                        <asp:HiddenField runat="server" ID="hfPropertyDescription" />
                        <asp:HiddenField runat="server" ID="hfPropertyFormat" />
                        <orion:EditCPControl runat="server" ID="EditCPControl" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <input type="hidden" id="cpTargetEntity" value="<%= DefaultSanitizer.SanitizeHtml(TargetEntity) %>" />
    <input type="hidden" id="hfPropertyNameRef" value="<%= hfPropertyName.ClientID %>" />
</div>
