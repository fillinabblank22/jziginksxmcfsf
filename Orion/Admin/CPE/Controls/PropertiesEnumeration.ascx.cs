using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.CPE;

public delegate void PropertiesEnumerationChangedHandler();

public partial class Orion_Admin_CPE_Controls_PropertiesEnumeration : UserControl
{
    public event PropertiesEnumerationChangedHandler PropertiesEnumerationChanged;

	[Serializable]
	public struct EnumerationInfo
	{
		public string Value { get; set; }
		public bool Editable { get; set; }
	}

	public List<EnumerationInfo> Enumerations
    {
        get
        {
            if (ViewState["EnumerationsCollection"] == null) 
            {
				ViewState["EnumerationsCollection"] = new List<EnumerationInfo>();
            }
			return (List<EnumerationInfo>)ViewState["EnumerationsCollection"];
        }
        set
        {
            ViewState["EnumerationsCollection"] = value;
        }
    }

    public Type PropertyType
    {
        get
        {
            if (ViewState["EnumerationsPropertyType"] == null)
            {
                ViewState["EnumerationsPropertyType"] = typeof(string);
            }
            return (Type)ViewState["EnumerationsPropertyType"];
        }
        set
        {
            ViewState["EnumerationsPropertyType"] = value;
        }
    }

    public int PropertySize
    {
        get
        {
            if (ViewState["EnumerationsPropertySize"] == null)
            {
                ViewState["EnumerationsPropertySize"] = -1;
            }
            return (int)ViewState["EnumerationsPropertySize"];
        }
        set
        {
            ViewState["EnumerationsPropertySize"] = value;
        }
    }

    public bool Visible
    {
        get { return enumerationListTd.Visible; }
        set { enumerationListTd.Visible = value; }
    }

	public string PropertyKey
	{
		get
        {
			if (ViewState["PropertyKey"] == null)
            {
				ViewState["PropertyKey"] = string.Empty;
            }
			return (string)ViewState["PropertyKey"];
        }
        set
        {
			ViewState["PropertyKey"] = value;
        }
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            BindData();
        }
		Update();
    }

    protected void PropertiesEnumerationGrid_OnDataBound(object sender, EventArgs e)
    {
		if (Enumerations.Count == 1)
		{
			//var rows = ((GridView)sender).Rows;
			Control item = PropertiesEnumerationGrid.Rows[0];

			var enumerationImageButton = (ImageButton)(item.FindControl("CloseCross"));

			if (enumerationImageButton != null)
				enumerationImageButton.Visible = false;
		}
    }

    private void BindData()
    {
		if (Enumerations.Count == 0)
		{
			Enumerations.Add(new EnumerationInfo { Value = string.Empty, Editable = true });
		}

		PropertiesEnumerationGrid.DataSource = Enumerations;
        PropertiesEnumerationGrid.DataBind();
    }

    private void Update()
    {
		//needed to store all changes used did in grid
        for (var i = 0; i < PropertiesEnumerationGrid.Rows.Count; i++)
        {
            if (i >= Enumerations.Count)
				break;

            Control item = PropertiesEnumerationGrid.Rows[i];
            if (item != null) 
            {
                var enumerationTextBox = (TextBox)item.FindControl("EnumerationTextBox");
                if (enumerationTextBox != null)
					Enumerations[i] = new EnumerationInfo {
					Value = enumerationTextBox.Text,
					Editable = enumerationTextBox.Enabled
				};
            }
        }
    }

    private bool ValidateDuplicity(int rowIndex)
    {
        return !Enumerations.Where((t, index) => index != rowIndex && rowIndex < Enumerations.Count && t.Value.Equals(Enumerations[rowIndex].Value)).Any();
    }

    protected void PropertiesEnumerationGrid_RowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (index < Enumerations.Count)
            {
                Enumerations.RemoveAt(index);
            }
            BindData();
            PropertiesEnumerationChanged();
        }
    }

    protected void AndButton_Clicked(object sender, EventArgs e)
    {
		Enumerations.Add(new EnumerationInfo { Value = string.Empty, Editable = true });
        BindData();
        PropertiesEnumerationChanged();
    }

	protected void EnumerateValidation(object source, ServerValidateEventArgs args)
	{
		var cv = (CustomValidator)source;
		int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;

		if (rowIndex >= Enumerations.Count)
		{
			args.IsValid = true;
			return;
		}

		var value = Enumerations[rowIndex].Value;

		if (string.IsNullOrEmpty(value))
		{
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_27;
			args.IsValid = false;
			return;
		}

		if (PropertyType == typeof(float))
		{
			float dNumber;
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Float;
			args.IsValid = float.TryParse(value, NumberStyles.Float, CultureInfo.CurrentCulture, out dNumber);
		}
		else if (PropertyType == typeof(int))
		{
			int iNumber;
			args.IsValid = int.TryParse(value, out iNumber);
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Integer;
		}
		else if (PropertyType == typeof(DateTime))
		{
			DateTime dt;
			args.IsValid = DateTime.TryParse(value, out dt);
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_SO0_5;
		}
		else if (PropertyType == typeof(bool))
		{
			int iValue;
			if (int.TryParse(value, out iValue))
			{
				args.IsValid = (iValue == 0 || iValue == 1);
			}
			else
			{
				bool bValue;
				args.IsValid = bool.TryParse(value, out bValue);
			}
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_SO0_4;
		}
        else if (PropertyType == typeof(string))
        {
            if (string.IsNullOrEmpty(value) || value.Length <= (PropertySize > 0 ? PropertySize : int.MaxValue))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
                cv.ErrorMessage = string.Format(Resources.CoreWebContent.WEBDATA_IB0_195,
                                                (PropertySize == int.MaxValue) ? int.MaxValue : PropertySize );
            }
        }
		else
		{
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_27;
			args.IsValid = !string.IsNullOrEmpty(value);
		}
	}

    protected void DuplicityValidation(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        args.IsValid = ValidateDuplicity(rowIndex);
    }

	public void SetEnumerationValues(string[] values, bool editable)
	{
		if (values == null || values.Length == 0) return;

		var list = new List <EnumerationInfo>();
		foreach (var val in values)
		{
			// convert value to current culture to show on web site
			list.Add(new EnumerationInfo { Value = CustomPropertyHelper.GetCurrentCultureString(val, PropertyType), Editable = editable });
		}

		Enumerations = list;
	}

	public List<string> GetEnumerationValues()
	{
		// convert value to invariant culture when get value
		return Enumerations.Select(enumItem => CustomPropertyHelper.GetInvariantCultureString(enumItem.Value, PropertyType)).ToList();
	}

	protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e) 
	{
		if (e.Row.RowType == DataControlRowType.DataRow)
		{
			if (!Enumerations[e.Row.RowIndex].Editable)
			{
				var enumerationImageButton = (ImageButton)(e.Row.FindControl("CloseCross"));
				if (enumerationImageButton != null)
				{
					enumerationImageButton.AlternateText = this.PropertyKey;
					// convert value to invariant culture to correctly get number of existing assignments
					enumerationImageButton.Attributes.Add("onClick", "javascript:return deleteValue(this.name, this.alt, '" + CustomPropertyHelper.GetInvariantCultureString(Enumerations[e.Row.RowIndex].Value, PropertyType) + "');return false;");
				}
			}
		}
	}

	public void AppendEnumerationValues(string[] restrictedValuesToAdd)
	{
		if (restrictedValuesToAdd == null || restrictedValuesToAdd.Length==0) return;

		foreach (var val in restrictedValuesToAdd)
		{
			// convert value to current culture to show on web site
			var ei = new EnumerationInfo { Value = CustomPropertyHelper.GetCurrentCultureString(val, PropertyType), Editable = true };
			Enumerations.Add(ei);
		}
	}
}
