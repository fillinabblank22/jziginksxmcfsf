using System;
using System.Web.UI.WebControls;

public partial class ExportFormatControl : System.Web.UI.UserControl
{
	public string SelectedFormat
	{
		get { return exportFormats.SelectedValue; }
		set { exportFormats.SelectedValue = value; }
	}
}
