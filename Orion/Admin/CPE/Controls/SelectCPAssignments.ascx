<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectCPAssignments.ascx.cs"
    Inherits="Orion_Admin_CPE_Controls_SelectCPAssignments" %>
<orion:Include ID="Include3" runat="server" File="Admin/Containers/Containers.css" />
<orion:Include ID="Include4" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include5" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" runat="server" File="Admin/CPE/Controls/AssignObjectGird.js" />
<orion:LocalizableButton ID="btnSelected" runat="server" CssClass="sw-cpe-selectorionobject"
    OnClientClick="SW.Core.assignments.SelectButtonClick(this)" DisplayType="Small"
    CausesValidation="false" LocalizedText="CustomText" Text="<%$ HtmlEncodedCode: ButtonText %>" />
<asp:Label ID="objectMsg" runat="server" Text="<%$ HtmlEncodedCode: DefaultLabelText %>" Font-Bold="true"></asp:Label>
<div id="hiddenData" runat="server" class="assignGroupHide">
    <asp:HiddenField ID="gridTitle" runat="server" />
    <asp:HiddenField ID="groupingGridTitle" runat="server" />
    <asp:HiddenField ID="popupTitle" runat="server" />
    <asp:HiddenField ID="initialGridItems" runat="server" />
    <asp:HiddenField ID="entityTypeData" runat="server" />
    <asp:HiddenField ID="isManagedEntityType" runat="server" />
    <asp:HiddenField ID="entityDisplayName" runat="server" />
</div>
<div class="assignGroupHide">
    <div id="GroupItemsSelector">
        <div class="GroupSection">
            <label for="groupBySelect">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_5) %></label>
            <select id="groupBySelect">
                <% foreach (var entity in GetGroupByProperties(entityTypeData.Value))
                   {
                       if (isDefaultGroupBy(entityTypeData.Value, entity.Key))
                       {
                %>
                <option value="<%= DefaultSanitizer.SanitizeHtml(entity.Key) %>" selected="selected"><%= DefaultSanitizer.SanitizeHtml(entity.Value) %></option>
                <% }
                       else
                       {
                %>
                <option value="<%= DefaultSanitizer.SanitizeHtml(entity.Key) %>"><%= DefaultSanitizer.SanitizeHtml(entity.Value) %></option>
                <%  
                    }
                   } %>
            </select>
        </div>
        <div class="GroupSection">
            <label for="searchBox">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_225) %></label>
            <table>
                <tr>
                    <td>
                        <input type="text" class="searchBox" id="searchBox" name="searchBox" />
                    </td>
                    <td>
                        <a href="javascript:void(0);" id="searchButton">
                            <img src="/Orion/images/Button.SearchIcon.gif" /></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="AssignmentsTable" style="width: 99%;">
    </div>
    <div id="AddButtonPanel" style="text-align: center; font-size: 11px;">
        <a href="javascript:void(0);" id="AddToGroupButton" style="top: 44%; position: relative;">
            <img src="/Orion/images/AddRemoveObjects/arrows_add_32x32.gif" /><br />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_21) %></a><br />
        <a href="javascript:void(0);" id="RemoveFromGroupButton" style="top: 46%; position: relative;">
            <img src="/Orion/images/AddRemoveObjects/arrows_remove_32x32.gif" /><br />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_227) %></a>
    </div>
</div>
<script type="text/javascript">
    // <![CDATA[

    SW.Core.assignments.SetAddToGroupButtonClientID('AddToGroupButton');
    SW.Core.assignments.SetRemoveFromGroupButton('RemoveFromGroupButton');
    
    // ]]>
</script>
