﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MatchColumnsControl.ascx.cs"
    Inherits="Orion_Admin_CPE_Controls_MatchColumnsControl" %>
<orion:Include ID="Include1" runat="server" File="CustomPropertyEditor.css" />
<div>
    <asp:UpdatePanel runat="server" >
        <ContentTemplate>
            <asp:Repeater ID="ColumnsRepeater" runat="server" OnItemDataBound="ColumnsRepeater_ItemDataBound">
                <HeaderTemplate>
                    <table border="0" cellspacing="0" cellpadding="0" class="matches-table">
                        <tr>
                            <th class="cp_validate_leftcolumn">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_143) %>
                            </th>
                            <th style="width: 100px;">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_144) %>
                            </th>
                            <th style="width: 270px;">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_145) %>
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr style="<%# Container.ItemIndex%2 == 0 ? "background-color: rgb(228, 241, 248);" : "" %>">
                        <td class="cp_validate_leftcolumn">
                            <asp:Label ID="colName" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Caption").ToString()) %>'></asp:Label>
                            <asp:Panel ID="ErrorsPanel" runat="server" CssClass="errorDescription">
                            </asp:Panel>
                        </td>
                        <td style="width: 100px;">
                            <asp:DropDownList runat="server" ID="MatchType" AutoPostBack="true" OnSelectedIndexChanged="MatchType_SelectedIndexChanged"
                                CausesValidation="false">
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_146 %>" Value="match"></asp:ListItem>
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_147 %>" Value="import"
                                    Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 200px;">
                            <select runat="server" id="MatchColumns" style="width: 100%">
                            </select>
                        </td>
                        <td style="<%# Container.ItemIndex % 2 == 0 ? "background-color: white!important;" : "" %>">
                            <div id="createUrl" runat="server" visible="False">
                                <span class="LinkArrow">&raquo;</span>&nbsp;<a id="downloadLink" style="text-decoration: underline;
                                    color: #336699;" href="/Orion/Admin/CPE/Add/Default.aspx" target="_blank">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_38) %></a>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:CustomValidator ID="RequiredColumn" runat="server" OnServerValidate="ColumnsValidation"
        ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_SO0_39 %>" EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
</div>
