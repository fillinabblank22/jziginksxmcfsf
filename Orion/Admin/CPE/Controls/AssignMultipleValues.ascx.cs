﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_CPE_Controls_AssignMultipleValues : System.Web.UI.UserControl
{
	private static Log _log = new Log(); 
	#region properties
	public List<CpAssignmentItemInfo> AssignmentValues
	{
		get
		{
			if (ViewState["AssignmentValuesCollection"] == null)
			{
				ViewState["AssignmentValuesCollection"] = new List<CpAssignmentItemInfo>();
			}
			return (List<CpAssignmentItemInfo>)ViewState["AssignmentValuesCollection"];
		}
		set
		{
			ViewState["AssignmentValuesCollection"] = value;
		}
	}

	private string PropertyEntityType
	{
		get
		{
			if (ViewState["PropertyEntityType"] == null)
			{
				ViewState["PropertyEntityType"] = "Orion.Nodes";
			}
			return (string)ViewState["PropertyEntityType"];
		}
		set
		{
			ViewState["PropertyEntityType"] = value;
		}
	}

	protected string EntityDisplayName
	{
		get
		{
			if (ViewState["EntityDisplayName"] == null)
			{
				ViewState["EntityDisplayName"] = string.Empty;
			}
			return (string)ViewState["EntityDisplayName"];
		}
		set
		{
			ViewState["EntityDisplayName"] = value;
		}
	}

	public List<string> PropertyNames
	{
		get
		{
			var list = new List<string>();
			if (AssignmentValues.Count > 0)
			{
				foreach (var val in AssignmentValues.First().CpAssignments)
				{
					list.Add(val.Name);
				}
			}
			return list;
		}
	}
	#endregion

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			BindData();
		}
		Update();
		SetUpGridHeaders();
	}

	private void Update()
	{
		for (var i = 0; i < valuesGrid.Rows.Count; i++)
		{
			if (i >= AssignmentValues.Count) break;

			Control item = valuesGrid.Rows[i];
			if (item != null)
			{
				var enumerationTextBox = (Orion_Admin_CPE_Controls_MultipleEditCpValuesControl)item.FindControl("editCpControl");
				
				if (enumerationTextBox != null)
				{
					var ids = AssignmentValues[i].ObjectUris;
					enumerationTextBox.ObjectUris = ids;
					enumerationTextBox.EntityDisplayName = EntityDisplayName;

					AssignmentValues[i].CpAssignments = enumerationTextBox.Items;
				}
			}
		}
	}

	private void SetUpGridHeaders()
	{
		var table = new StringBuilder(
			string.Format("<table class=\"assign-items-table\"><tr><td style=\"width: 300px!important;\"><b>{0}</b></td>",
					string.Format(Resources.CoreWebContent.WEBDATA_IB0_131, EntityDisplayName)));

		table.Append("<td class=\"rightCol\" style=\"padding-left: 0px!important;\"><table><tr>");
		foreach (var column in AssignmentValues.First().CpAssignments)
		{
			table.Append(string.Format("<td><div style=\"width: 256px!important;\"><b>{0}</b></div></td>", column.Name));
		}
		table.Append("</tr></table></td></tr></table>");
		
		if (valuesGrid.HeaderRow != null && valuesGrid.HeaderRow.Cells.Count > 0)
		{
			valuesGrid.HeaderRow.Cells[0].Controls.Add(new LiteralControl(table.ToString()));
		}
		if (valuesGrid.FooterRow != null && valuesGrid.FooterRow.Cells.Count > 0)
		{
			var row = (HtmlTableRow)valuesGrid.FooterRow.Cells[0].FindControl("footerRow");
			if (row != null)
			{
				var cell = new HtmlTableCell();
				cell.Style["padding"] = "0px 0px 0px 0px!important;";
				cell.Controls.Add(new LiteralControl("<table><tr>"));
				foreach (var column in AssignmentValues.First().CpAssignments)
				{
					cell.Controls.Add(new LiteralControl("<td style=\"padding: 0px 0px 0px 12px!important;\"><div class='sw-hatchbox' style=\"width: 252px!important;\"></div></td>"));
				}
				cell.Controls.Add(new LiteralControl("</tr></table>"));
				row.Controls.Add(cell);
			}
		}
	}


	private void BindData()
	{
		if (AssignmentValues.Count == 0)
		{
			AssignmentValues.Add(new CpAssignmentItemInfo
			{
				EntityName = PropertyEntityType,
				EntityDisplayName = EntityDisplayName,
				ObjectUris = new string[0],
				CpAssignments = new List<CpItemInfo>()
			});
		}
		valuesGrid.FooterStyle.CssClass = (AssignmentValues.Count % 2 == 0) ? "footerItemHolder" : "footerItemHolder assignItemHolderAlter";

		valuesGrid.DataSource = AssignmentValues;
		valuesGrid.DataBind();

		for (var i = 0; i < valuesGrid.Rows.Count; i++)
		{
			if (i >= AssignmentValues.Count) break;

			Control item = valuesGrid.Rows[i];
			if (item == null) continue;

			var enumerationTextBox = (Orion_Admin_CPE_Controls_MultipleEditCpValuesControl)item.FindControl("editCpControl");
			if (enumerationTextBox != null) enumerationTextBox.DataBind();
		}
	}

	protected void AddButton_Clicked(object sender, EventArgs e)
	{
		var list = new List<CpItemInfo>();
		foreach (var prop in AssignmentValues.First().CpAssignments)
		{
			list.Add(new CpItemInfo
			{
				EnumValues = prop.EnumValues,
				Name = prop.Name,
				Type = prop.Type
			});
		}

		AssignmentValues.Add(new CpAssignmentItemInfo
		{
			ObjectUris = new string[0],
			EntityName = PropertyEntityType,
			EntityDisplayName = EntityDisplayName,
			CpAssignments = list
		});

		BindData();
		SetUpGridHeaders();
	}

	protected void ValuesGrid_RowCommand(object sender, GridViewCommandEventArgs e)
	{
		if (e.CommandName == "DeleteRow")
		{
			int index = Convert.ToInt32(e.CommandArgument);
			if (index < AssignmentValues.Count)
			{
				AssignmentValues.RemoveAt(index);
			}
		}
		BindData();
		SetUpGridHeaders();
	}

	protected void ValuesGrid_OnDataBound(object sender, EventArgs e)
	{
		valuesGrid.Columns[1].Visible = (AssignmentValues.Count > 1);
	}


	public Dictionary<string, Dictionary<string, List<string>>> GetPropertyAssignments()
	{
		var result = new Dictionary<string, Dictionary<string, List<string>>>();
		foreach (var item in AssignmentValues)
		{
			foreach (var cp in item.CpAssignments)
			{
				if (!result.ContainsKey(cp.Name))
					result.Add(cp.Name, new Dictionary<string, List<string>>());

				if (!result[cp.Name].ContainsKey(cp.Value))
					result[cp.Name].Add(cp.Value, new List<string>(item.ObjectUris));
				else
					result[cp.Name][cp.Value].AddRange(item.ObjectUris);
			}
		}
		return result;
	}

	public void SetPropertyAssignments(List<CpAssignmentItemInfo> items, string propertyEntityName)
	{
		EntityDisplayName = CustomPropertyHelper.GetSourceEntityDisplayName(propertyEntityName);
		if (items != null)
		{
			AssignmentValues = items;
		}
	}

	protected void ValuesGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
	{
		if (e.Row.RowType == DataControlRowType.DataRow)
		{
			var assignControl = (Orion_Admin_CPE_Controls_SelectCPAssignments)e.Row.FindControl("selObject");

			if (assignControl != null)
			{
				assignControl.ParentRowId = e.Row.RowIndex;
				assignControl.EntityType = PropertyEntityType;
				assignControl.Assignments = new List<string>(AssignmentValues[e.Row.RowIndex].ObjectUris);
			}
		}
	}

	protected void selObject_SelectObjectChanged(List<string> assignments, int rowId)
	{
		AssignmentValues[rowId].ObjectUris = assignments.ToArray();
		var needToBind = false;
		foreach (var cpInfo in AssignmentValues[rowId].CpAssignments)
		{
			try
			{
				//try to find an existing assignment
				var assignValues = CustomPropertyHelper.GetCpAssignedValuesByTarget(AssignmentValues[rowId].EntityName, cpInfo.Name);

				var count = 0;
				var values = new List<string>();
				foreach (var uri in AssignmentValues[rowId].ObjectUris)
				{
				    var u = uri;
					foreach (var assign in assignValues.Where(assign => assign.Value.Contains(u))) {
						if (!values.Contains(assign.Key))
							values.Add(assign.Key);
						count++;
					}
				}
				// we've found eisting assignment
				if (count == AssignmentValues[rowId].ObjectUris.Length && values.Count == 1)
				{
					cpInfo.Value = values[0];
					needToBind = true;
				}
			}
			catch (Exception e)
			{
				_log.Warn("Exception durring getting existing assignments", e);
			}
		}

		if (needToBind)
		{
			Control item = valuesGrid.Rows[rowId];
			if (item == null) return;

			var enumerationTextBox = (Orion_Admin_CPE_Controls_MultipleEditCpValuesControl)item.FindControl("editCpControl");
			if (enumerationTextBox != null)
			{
				enumerationTextBox.Items = AssignmentValues[rowId].CpAssignments;
				enumerationTextBox.DataBind();
				
			}
		}
	}
}
