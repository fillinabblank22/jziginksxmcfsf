﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.CPE;

public delegate void SelectObjectChangedHandler(List<string> assignments, int rowId);

public partial class Orion_Admin_CPE_Controls_SelectCPAssignments : System.Web.UI.UserControl
{
	public event SelectObjectChangedHandler SelectObjectChanged;

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Page.IsPostBack)
		{
			if (Request.Params["__EVENTTARGET"] != null && Request.Params["__EVENTTARGET"].Equals(btnSelected.ClientID, StringComparison.OrdinalIgnoreCase))
			{
				string arg = Request.Params["__EVENTARGUMENT"];
				var itemsData = arg;
				if (!string.IsNullOrEmpty(itemsData))
				{
					var serializer = new DataContractJsonSerializer(typeof(DataGridRecord[]));
					var records = (DataGridRecord[])serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(itemsData)));

					Assignments = records.Select(record => record.Definition.ToString()).ToList();
				}
				SelectObjectChanged(Assignments, ParentRowId);
			}
		}

		if (string.IsNullOrEmpty(initialGridItems.Value))
			initialGridItems.Value = GridObjects;

		//if (!_defaultGroupKeys.ContainsKey(entityTypeData.Value) || string.IsNullOrEmpty(_defaultGroupKeys[entityTypeData.Value]))
		//{
		//try to get settings for current entity
		string groping = WebUserSettingsDAL.Get("CustomProperties_" + entityTypeData.Value + "_Grouping");
		if (groping != null)
			_defaultGroupKeys[entityTypeData.Value] = groping;
		//}

	}

	protected string Escape(string input)
	{
		if (input == null)
			return string.Empty;

		return input.Replace(@"\", @"\\").Replace("'", @"\'");
	}

	private static readonly IDictionary<string, string> _defaultGroupKeys = new Dictionary<string, string>();
	private static Dictionary<string, string> _properties;

	public Dictionary<string, string> GetGroupByProperties(string entityType)
	{
		if (!Page.IsPostBack)
		{
			var dal = new ContainersMetadataDAL(); //ToDo: all entity related methods should be moved to some common EntityDal
			_properties = new Dictionary<string, string>();
			_properties.Add(string.Empty, Resources.CoreWebContent.WEBCODE_AK0_70);
			foreach (var property in dal.GetEntityProperties(entityType, ContainersMetadataDAL.PropertyType.GroupBy))
			{
				_properties.Add(property.FullyQualifiedName, UIHelper.Escape(property.DisplayName));

				if ((!_defaultGroupKeys.ContainsKey(entityType) || string.IsNullOrEmpty(_defaultGroupKeys[entityType])) && property.FullyQualifiedName.EndsWith(".Status"))
					_defaultGroupKeys[entityType] = property.FullyQualifiedName;
			}

			//todo
			//when in SWIS will be implemented "Default grouping" property for entities - the this should be reworked

			if ((!_defaultGroupKeys.ContainsKey(entityType) || string.IsNullOrEmpty(_defaultGroupKeys[entityType])) && _properties.Count > 1)
				_defaultGroupKeys[entityType] = _properties.Keys.ElementAt(1); //0 - will be no grouping, but we need to select any 
		}
		return _properties;
	}

	public bool isDefaultGroupBy(string entityType, string property)
	{
		if (!_defaultGroupKeys.ContainsKey(entityType) || string.IsNullOrEmpty(_defaultGroupKeys[entityType]))
		{
			_defaultGroupKeys[entityType] = property;
			return true;
		}

		return _defaultGroupKeys[entityType].Equals(property);
	}

	public List<string> Assignments
	{
		get
		{
			if (ViewState[string.Format("Assignments_{0}", ClientID)] == null)
			{
				ViewState[string.Format("Assignments_{0}", ClientID)] = new List<string>();
			}
			return (List<string>)ViewState[string.Format("Assignments_{0}", ClientID)];
		}
		set
		{
			objectMsg.Text = (value.Count == 0) ?
				string.Format(Resources.CoreWebContent.WEBDATA_IB0_132, EntityDisplayName) :
				string.Format(Resources.CoreWebContent.WEBDATA_IB0_133, value.Count, EntityDisplayName);
			btnSelected.Text = ButtonText;


			gridTitle.Value = string.Format(Resources.CoreWebContent.WEBDATA_IB0_134, "{0}", EntityDisplayName);
			groupingGridTitle.Value = string.Format(Resources.CoreWebContent.WEBDATA_IB0_135, EntityDisplayName);
			popupTitle.Value = ButtonText;
		    entityDisplayName.Value = EntityDisplayName;

			ViewState[string.Format("Assignments_{0}", ClientID)] = value;
			initialGridItems.Value = GridObjects;
		}
	}

	public string GridObjects
	{
		get
		{
			var res = "[{0}]";
			StringBuilder item = new StringBuilder();

			if (Assignments != null && Assignments.Count > 0)
			{
				DataTable dt = null;
				var coma = string.Empty;
				bool all = false;

				var isManagedEntity = SwisEntityHelper.IsManagedEntity(entityTypeData.Value);
				using (var dal = new CustomPropertyDAL())
				{
					if (Assignments.Count < 50)
					{
						all = true;
						dt =
							dal.GetEntityNamesWithStatus(
								string.Format("Select DisplayName, {0} AS Status, Uri from {1} Where Uri IN ('{2}')",
											  (isManagedEntity) ? "Status" : "0",
											  entityTypeData.Value, String.Join("','", Assignments.ToArray())));
					}
					else
					{
						dt =
							dal.GetEntityNamesWithStatus(
								string.Format("Select DisplayName, {0} AS Status, Uri from {1}",
											  (isManagedEntity) ? "Status" : "0", entityTypeData.Value));

						all = (dt != null && dt.Rows != null && dt.Rows.Count != 0) && Assignments.Count == dt.Rows.Count;
					}
				}

				if (dt == null || dt.Rows == null || dt.Rows.Count == 0)
				{
					res = string.Format(res, string.Empty);
				}
				else
				{
					string entityType = entityTypeData.Value;
					foreach (DataRow row in dt.Rows)
					{
						var uri = row["Uri"].ToString();
						if (all || Assignments.Contains(uri))
						{
							var caption = Escape(row["DisplayName"].ToString());
							item.AppendFormat("{5}['{0}', '{1}', '{2}', '{3}', '{4}']", uri, caption, caption, row["Status"], entityType, coma);
							coma = ",";
						}
					}
				}
			}
			else
			{
				res = string.Format(res, string.Empty);
			}

			return string.Format(res, item);
		}
	}

	public int ParentRowId
	{
		get
		{
			if (ViewState[string.Format("ParentRowId_{0}", ClientID)] == null)
			{
				ViewState[string.Format("ParentRowId_{0}", ClientID)] = 0;
			}
			return (int)ViewState[string.Format("ParentRowId_{0}", ClientID)];
		}
		set
		{
			ViewState[string.Format("ParentRowId_{0}", ClientID)] = value;
		}
	}

	public string EntityType
	{
		set
		{
			entityTypeData.Value = value;
            isManagedEntityType.Value = SwisEntityHelper.IsManagedEntity(value).ToString();
		}
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public string EntityDisplayName
	{
		get
		{
			if (ViewState[string.Format("EntityDisplayName_{0}", ClientID)] == null)
			{
				ViewState[string.Format("EntityDisplayName_{0}", ClientID)] = string.Empty;
			}
			return (string)ViewState[string.Format("EntityDisplayName_{0}", ClientID)];
		}
		set
		{
			ViewState[string.Format("EntityDisplayName_{0}", ClientID)] = value;
		}
	}

	protected string ButtonText
	{
		get { return string.Format(Resources.CoreWebContent.WEBDATA_IB0_131, EntityDisplayName); }
	}

	protected string DefaultLabelText
	{
		get { return string.Format(Resources.CoreWebContent.WEBDATA_IB0_132, EntityDisplayName); }
	}

	[Serializable]
	public class DataGridRecord
	{
		public string Definition = null;
		public string Name = null;
		public string FullName = null;
		public int MemberStatus = 0;
		public string Entity = null;
	}
}