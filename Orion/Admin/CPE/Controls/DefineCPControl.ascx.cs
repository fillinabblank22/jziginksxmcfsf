﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web.CPE;

public partial class Orion_Admin_CPE_Controls_DefineCPControl : System.Web.UI.UserControl
{
	#region public properties 
	public string PropertyName
	{
		get { return EditCPControl.PropertyName; }
	}

	public string PropertyDescription
	{
		get { return EditCPControl.PropertyDescription; }
	}

	public string DatabaseColumnType
	{
		get { return EditCPControl.DatabaseColumnType; }
	}

	public List<string> EnumerationValues
	{
		get { return EditCPControl.EnumerationValues; }
	}

    public int PropertySize
    {
        get { return EditCPControl.PropertySize; }
    }

    public bool Mandatory
    {
        get { return EditCPControl.Mandatory; }
    }

    public string Default
    {
        get { return EditCPControl.Default; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
	public string TableName
	{
		get;
		set;
	}

    [PersistenceMode(PersistenceMode.Attribute)]
    public string TargetEntity
    {
        get;
        set;
    }


    [PersistenceMode(PersistenceMode.Attribute)]
    public string SourceEntity
    {
        get;
        set;
    }

    public IDictionary<string, bool> UsageFlags
    {
        get { return EditCPControl.UsageFlags; }
    }

    #endregion 

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        EditCPControl.TargetEntityName = TargetEntity;
	    EditCPControl.SourceEntityName = SourceEntity;
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		if (Request.Params["__EVENTTARGET"] != null 
			&& Request.Params["__EVENTTARGET"].Equals(hfPropertyName.ClientID, StringComparison.OrdinalIgnoreCase))
		{
			UpdateEditCPFields(hfPropertyName.Value, hfPropertyDescription.Value, CustomPropertyHelper.GetPropertyDbType(hfPropertyFormat.Value), new string[0], new Dictionary<string, bool>());
		}
    }

    public void UpdateEditCPFields(string name, string description, string dataType, int size, string[] values, IDictionary<string, bool> usageFlags)
    {
        EditCPControl.UpdateFields(name, description, dataType, size, false, string.Empty, values, usageFlags, false);
    }

    public void UpdateEditCPFields(string name, string description, string dataType, int size, bool mandatory, string defaultValue, string[] values, IDictionary<string, bool> usageFlags)
    {
        EditCPControl.UpdateFields(name, description, dataType, size, mandatory, defaultValue, values, usageFlags, false);
    }

	public void UpdateEditCPFields(string name, string description, string dataType, string[] values, IDictionary<string, bool> usageFlags)
	{
        UpdateEditCPFields(name, description, dataType, 0, values, usageFlags);
	}
}