<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignValue.ascx.cs" Inherits="Orion_Admin_CPE_Controls_AssignValue" %>
<%@ Register Src="~/Orion/Admin/CPE/Controls/SelectCPAssignments.ascx" TagPrefix="orion"
    TagName="SelectCPAssignments" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<%@ Reference Control="~/Orion/Admin/CPE/Controls/SelectCPAssignments.ascx" %>
<orion:Include ID="Include1" runat="server" File="ValueAssignments.css" />
<table class="headerItemHolder">
    <tr>
        <td class="headerColumn leftCol">
            <b>
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IB0_131, EntityDisplayName)) %></b>
        </td>
        <td class="headerColumn rigntCol">
            <b>
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IB0_130, SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular(PropertyName), EntityDisplayName)) %></b>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="valuesGrid" valign="top" id="assigningListTd" runat="server">
            <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView ID="valuesGrid" runat="server" automation="valueslist" AutoGenerateColumns="False"
                        ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="ValuesGrid_RowCommand"
                        OnDataBound="ValuesGrid_OnDataBound" OnRowDataBound="ValuesGrid_OnRowDataBound">
                        <AlternatingRowStyle CssClass="assignItemHolder assignItemHolderAlter" />
                        <RowStyle CssClass="assignItemHolder" />
                        <FooterStyle CssClass="footerItemHolder" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <table class="assignItemTable">
                                        <tr>
                                            <td class="leftCol">
                                                <orion:SelectCPAssignments runat="server" ID="selObject" EntityDisplayName='<%# DefaultSanitizer.SanitizeHtml(Eval("EntityDisplayName")) %>'
                                                    OnSelectObjectChanged="selObject_SelectObjectChanged" />
                                            </td>
                                            <td class="rigntCol">
                                                <orion:EditCpValueControl runat="server" ID="ValueTextBox"/>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="padding: 0px 5px 0px 0px!important;">
                                                <div class="sw-hatchbox">
                                                    <orion:LocalizableButton ID="AddButton" runat="server" DisplayType="Small" LocalizedText="CustomText"
                                                        Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_170 %>" OnClick="AddButton_Clicked" ValidationGroup="EditCpValue" />
                                                </div>
                                            </td>
                                            <td style="padding: 0px 0px 0px 0px!important;" class="addButton">
                                                <div class="sw-hatchbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/button_X_04.gif" Text="x" CommandName="DeleteRow" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
