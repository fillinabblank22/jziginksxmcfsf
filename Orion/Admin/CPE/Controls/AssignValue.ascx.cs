﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.CPE;

public partial class Orion_Admin_CPE_Controls_AssignValue : System.Web.UI.UserControl
{
    [Serializable]
    public class PropertyAssignmentItem
    {
        private List<string> objectUris = new List<string>();
        private string propValue = string.Empty;
		private string[] enumerationValues = new string[0];
		private string entityDisplayName = string.Empty;
		private Type type = typeof(string);

        public string Value
        {
            get { return propValue; }
            set { propValue = value; }
        }
		public string[] EnumValues
		{
			get { return enumerationValues; }
			set { enumerationValues = value; }
		}
        public List<string> ObjectsIds
        {
			get { return objectUris; }
			set { objectUris = value; }
        }
		public string[] ObjectsIdsArray
		{
			get { return objectUris.ToArray(); }
		}
		public string EntityDisplayName
		{
			get { return entityDisplayName; }
			set { entityDisplayName = value; }
		}
		public Type Type
		{
			get { return type; }
			set { type = value; }
		}
    }
	#region properties
	public List<PropertyAssignmentItem> AssignmentValues
    {
        get
        {
            if (ViewState["AssignmentValuesCollection"] == null)
            {
                ViewState["AssignmentValuesCollection"] = new List<PropertyAssignmentItem>();
            }
            return (List<PropertyAssignmentItem>)ViewState["AssignmentValuesCollection"];
        }
        set
        {
            ViewState["AssignmentValuesCollection"] = value;
        }
    }

    public Type PropertyType
    {
        get
        {
            if (ViewState["CustomPropertyType"] == null)
            {
                ViewState["CustomPropertyType"] = typeof(string);
            }
            return (Type)ViewState["CustomPropertyType"];
        }
        set
        {
            ViewState["CustomPropertyType"] = value;
        }
    }

    public string DatabaseColumnType
    {
        get
        {
            if (ViewState["DatabaseColumnType"] == null)
            {
                ViewState["DatabaseColumnType"] = string.Empty;
            }
            return (string)ViewState["DatabaseColumnType"];
        }
        set
        {
            ViewState["DatabaseColumnType"] = value;
        }
    }

    public bool Mandatory
    {
        get
        {
            if (ViewState["Mandatory"] == null)
            {
                ViewState["Mandatory"] = false;
            }
            return (bool)ViewState["Mandatory"];
        }
        set
        {
            ViewState["Mandatory"] = value;
        }
    }

    public string Default
    {
        get
        {
            if (ViewState["Default"] == null)
            {
                ViewState["Default"] = string.Empty;
            }
            return (string)ViewState["Default"];
        }
        set
        {
            ViewState["Default"] = value;
        }
    }

	[PersistenceMode(PersistenceMode.Attribute)]
	public string[] EnumerationValues
	{
		get
		{
			if (ViewState["EnumValues"] == null)
			{
				ViewState["EnumValues"] = typeof(string[]);
			}
			return (string[])ViewState["EnumValues"];
		}
		set
		{
			ViewState["EnumValues"] = value;
		}
	}

    private string PropertySourceEntityType
    {
        get
        {
            if (ViewState["PropertySourceEntityType"] == null)
            {
                ViewState["PropertySourceEntityType"] = "Orion.Nodes";
            }
            return (string)ViewState["PropertySourceEntityType"];
        }
        set
        {
            ViewState["PropertySourceEntityType"] = value;
        }
    }

    private string PropertyTargetEntityType
    {
        get
        {
            if (ViewState["PropertyTargetEntityType"] == null)
            {
                ViewState["PropertyTargetEntityType"] = "Orion.NodesCustomProperties";
            }
            return (string)ViewState["PropertyTargetEntityType"];
        }
        set
        {
            ViewState["PropertyTargetEntityType"] = value;
        }
    }

	protected string EntityDisplayName
	{
		get
		{
			if (ViewState["EntityDisplayName"] == null)
			{
				ViewState["EntityDisplayName"] = string.Empty;
			}
			return (string)ViewState["EntityDisplayName"];
		}
		set
		{
			ViewState["EntityDisplayName"] = value;
		}
	}

	public string PropertyName
	{
		get
		{
			if (ViewState["PropertyName"] == null)
			{
				ViewState["PropertyName"] = string.Empty;
			}
			return (string)ViewState["PropertyName"];
		}
		set
		{
			ViewState["PropertyName"] = value;
		}
	}

    public int PropertySize
    {
        get
        {
            if (ViewState["PropertySize"] == null)
            {
                ViewState["PropertySize"] = -1;
            }
            return (int)ViewState["PropertySize"];
        }
        set
        {
            ViewState["PropertySize"] = value;
        }
    }
	#endregion

	protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            BindData();
        }
        Update();
    }

    private void Update()
    {
        for (var i = 0; i < valuesGrid.Rows.Count; i++)
        {
            if (i >= AssignmentValues.Count) break;
            Control item = valuesGrid.Rows[i];
            if (item != null)
            {
				var enumerationTextBox = (Orion_Controls_EditCpValueControl)item.FindControl("ValueTextBox");
                if (enumerationTextBox != null)
                {
                    CustomProperty cp = new CustomProperty
                    {
                        DatabaseColumnType = DatabaseColumnType,
                        DatabaseColumnLength = PropertySize,
                        Values = EnumerationValues ?? new string[0],
                        Mandatory = Mandatory,
                        Default = Default,
                    };

                    var ids = AssignmentValues[i].ObjectsIds;
                    enumerationTextBox.ObjectIds = ids.ToArray();
                    enumerationTextBox.Configure(false, cp);

                    AssignmentValues[i] = new PropertyAssignmentItem()
                    {
                        Value = enumerationTextBox.Text,
                        ObjectsIds = ids,
                        EnumValues = EnumerationValues,
                        EntityDisplayName = EntityDisplayName,
                        Type = PropertyType
                    };
                }
            }
        }
    }

    private void BindData()
    {
        if (AssignmentValues.Count == 0)
        {
			AssignmentValues.Add(new PropertyAssignmentItem { EnumValues = EnumerationValues, EntityDisplayName = EntityDisplayName, Type = PropertyType });
        }
		valuesGrid.FooterStyle.CssClass = (AssignmentValues.Count % 2 == 0) ? "footerItemHolder" : "footerItemHolder assignItemHolderAlter";

        valuesGrid.DataSource = AssignmentValues;
        valuesGrid.DataBind();
    }

    protected void AddButton_Clicked(object sender, EventArgs e)
    {
		AssignmentValues.Add(new PropertyAssignmentItem { Value = string.Empty, EnumValues = EnumerationValues, EntityDisplayName = EntityDisplayName, Type = PropertyType });
        BindData();
    }

    protected void ValuesGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (index < AssignmentValues.Count) 
            {
                AssignmentValues.RemoveAt(index);
            }
            BindData();
        }
    }

    protected void ValuesGrid_OnDataBound(object sender, EventArgs e)
    {
        valuesGrid.Columns[1].Visible = (AssignmentValues.Count > 1);
    }

    public Dictionary<string, List<string>> GetPropertyAssignments()
    {
        var result = new Dictionary<string, List<string>>();
        foreach (var item in AssignmentValues)
        {
            if (!result.ContainsKey(item.Value))
                result.Add(item.Value, item.ObjectsIds);
            else
                result[item.Value].AddRange(item.ObjectsIds);
        }
        return result;
    }

    public void SetPropertyAssignments(Dictionary<string, List<string>> items, CustomProperty customProperty)
    {
        PropertySourceEntityType = customProperty.SourceEntity;
        PropertyTargetEntityType = customProperty.TargetEntity;
        EntityDisplayName = CustomPropertyHelper.GetEntityDisplayName(PropertySourceEntityType);

        if (items != null)
        {
            foreach (var item in items)
            {
				AssignmentValues.Add(new PropertyAssignmentItem { Value = item.Key, ObjectsIds = item.Value, EnumValues = EnumerationValues, EntityDisplayName = EntityDisplayName, Type = PropertyType });
            }
        }
    }

    protected void ValueValidation(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;

        if (rowIndex >= AssignmentValues.Count)
        {
            args.IsValid = true;
            return;
        }

		if (AssignmentValues.Count > 0)
		{
			var isEmpty = true;
			foreach (var objects in AssignmentValues) 
			{
				if (objects.ObjectsIds != null && objects.ObjectsIds.Count > 0) 
				{
					isEmpty = false;
					break;
				}
			}

			// if no objects for assignment then don't validate
			if (isEmpty)
			{
				args.IsValid = true;
				return;
			}
		}

        var value = AssignmentValues[rowIndex];
        if (PropertyType == typeof(float))
        {
            float dNumber;
            args.IsValid = float.TryParse(value.Value, out dNumber);
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Float;
        }
        else if (PropertyType == typeof(int))
        {
            int iNumber;
            args.IsValid = int.TryParse(value.Value, out iNumber);
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Integer;
        }
		else if (PropertyType == typeof(DateTime))
		{
			DateTime dt;
			args.IsValid = DateTime.TryParse(value.Value, out dt);
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_SO0_5;
		}
		else if (PropertyType == typeof(bool))
		{
			int iValue;
			if (int.TryParse(value.Value, out iValue))
			{
				args.IsValid = (iValue == 0 || iValue == 1);
			}
			else {
				bool bValue;
				args.IsValid = bool.TryParse(value.Value, out bValue);
			}
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_SO0_4;
		}
		else
		{
			args.IsValid = true;
		}
    }

    protected void ValuesGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var assignControl = (Orion_Admin_CPE_Controls_SelectCPAssignments)e.Row.FindControl("selObject");

            if (assignControl != null)
            {
                assignControl.ParentRowId = e.Row.RowIndex;
                assignControl.EntityType = PropertySourceEntityType;
                assignControl.Assignments = AssignmentValues[e.Row.RowIndex].ObjectsIds;
            }

            var enumerationTextBox = (Orion_Controls_EditCpValueControl)e.Row.FindControl("ValueTextBox");
            CustomProperty cp = new CustomProperty
            {
                DatabaseColumnType = DatabaseColumnType,
                DatabaseColumnLength = PropertySize,
                Values = EnumerationValues ?? new string[0],
                Mandatory = Mandatory,
                Default = Default,
            };

            enumerationTextBox.Configure(false, cp);
            enumerationTextBox.ObjectIds = AssignmentValues[e.Row.RowIndex].ObjectsIdsArray;
            enumerationTextBox.Text = AssignmentValues[e.Row.RowIndex].Value;
        }
    }

    protected void selObject_SelectObjectChanged(List<string> assignments, int rowId)
    {
        AssignmentValues[rowId].ObjectsIds = assignments;
    }
}