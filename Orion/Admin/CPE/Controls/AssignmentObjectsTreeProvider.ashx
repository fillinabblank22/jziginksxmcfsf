﻿<%@ WebHandler Language="C#" Class="AssignmentObjectsTreeProvider" %>

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;
using Resources;
using SolarWinds.Orion.Web.Containers;

public class AssignmentObjectsTreeProvider : IHttpHandler
{
    [Serializable]
    private class TreeNode
    {
        public string Uri { get; set; }
        public string FullName { get; set; }
        public string FullNameWithoutHighlight { get; set; }
        public string Entity { get; set; }
        public int Status { get; set; }
        public bool LoadNextItems { get; set; }

        protected string Escape(string input)
        {
            if (input == null)
                return string.Empty;

            return input.Replace(@"\", @"\\").Replace("'", @"\'");
        }
        
        public override string ToString()
        {
            var str = new System.Text.StringBuilder();
            str.Append("{");
            str.AppendFormat("id: '{0}',", Uri);
            str.AppendFormat("text: '{0}',", Escape(FullName));
            str.AppendFormat("entity: '{0}',", Entity);
            str.AppendFormat("status: '{0}',", Status);
            str.AppendFormat("fullName: '{0}',", Escape(FullNameWithoutHighlight));
            if (Entity == null)
            {
                str.Append("allowDrag: false,");
            }
            else
            {
                // no checkbox for "There are XX more items... message
                str.Append("checked: false,");
                str.Append("propagateCheck: true,");
            }

            str.Append("attributes: {loadNextItems: " + LoadNextItems.ToString().ToLower() + "},");
            if (Entity != null && SolarWinds.Orion.Web.SwisEntityHelper.IsManagedEntity(Entity))
            {
                str.AppendFormat("icon: '/Orion/StatusIcon.ashx?entity={0}&amp;id={1}&amp;status={2}&amp;size=small',",
                                 Entity, (Uri ?? "").Split('=').Last(), Status);
            }
            else
            {
                str.Append("icon: Ext.BLANK_IMAGE_URL,");
            }
            str.Append("listeners: { 'checkchange': CheckChange },");
            str.Append("expandable: false,");
            str.Append("leaf: true");
            str.Append("}");
            return str.ToString();
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        var entityType = context.Request.Params["entityType"];
        var property = context.Request.Params["groupBy"];
        var propertyValue = context.Request.Params["value"];
        var searchValue = context.Request.Params["searchValue"];
        var excludeDefinitionsStr = context.Request.Params["excludeDefinitions"];

        var initialLoad = context.Request.Params["initialLoad"];
        var bInitialLoad = true;
        Boolean.TryParse(initialLoad, out bInitialLoad);

        var excludeDefinitions = new HashSet<string>(excludeDefinitionsStr.Split(','));

        IEnumerable<SolarWinds.Orion.Web.Containers.Entity> entities;
        using (var dal = new SolarWinds.Orion.Web.DAL.CustomPropertyDAL())
        {
            entities = dal.GetCustomPropertyEntities(entityType, property, propertyValue, searchValue,
                                                         excludeDefinitions);
        }
        var response = new System.Text.StringBuilder("[");
        var first = true;
        var count = 0;
        var maxCount = 300;

        var startIndex = 0;
        if (!bInitialLoad)
        {
            startIndex = maxCount;
        }

        foreach (var entity in entities.Skip(startIndex))
        {
            if (excludeDefinitions.Contains(EntityHelper.GetIdFromUri(entity.Uri).ToString()))
                continue;
                
            TreeNode node;
            if ((count++ == maxCount) && bInitialLoad)
            {
                var remainingCount = entities.Count() - count + 1;
                node = new TreeNode()
                {
                    FullName = (remainingCount > 1) ? String.Format(CoreWebContent.WEBCODE_VB1_20, remainingCount) : String.Format(CoreWebContent.WEBCODE_VB1_21, remainingCount),
                    LoadNextItems = true
                };
            }
            else
            {
                var name = entity.FullNameWithoutLinks;
                if (!String.IsNullOrEmpty(searchValue))
                {
                    // highlight search text
                    name = Regex.Replace(
                        name,
                        String.Format("({0})", Regex.Escape(searchValue)),
                        "<span class=\"searchHighlight\">$1</span>",
                        RegexOptions.IgnoreCase);
                }

                node = new TreeNode()
                {
                    Uri = entity.Uri,
                    Entity = entity.EntityType,
                    Status = entity.Status,
                    FullName = name,
                    FullNameWithoutHighlight = entity.FullNameWithoutLinks,
                    LoadNextItems = false
                };
            }

            if (!first)
                response.Append(",");
            response.Append(node);

            first = false;

            if ((count > maxCount) && bInitialLoad) break;
        }
        response.Append("]");
        context.Response.Write(response.ToString());
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}