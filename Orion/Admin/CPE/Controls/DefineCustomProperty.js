﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

SW.Orion.DefineCustomProperty = function () {

    function renderString(value, meta, record) {
        return String.format(Ext.util.Format.htmlEncode(value));
    }

    //Error handler
    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({ title: result.Source, msg: result.Msg, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }

    //Ext grid
    ORION.prefix = "DefineCustomProperty_";
    var dataStore;
    var grid;
    var targetEntity;

    return {
        init: function () {

            targetEntity = $("#cpTargetEntity")[0].value;

            dataStore = new Ext.data.JsonStore({
                root: 'd',
                totalProperty: 'd.Count',
                fields: ['Name', 'Table', 'Advanced', 'Description', 'Type', 'Size'],
                proxy: new Ext.data.HttpProxy({
                    url: "/Orion/Services/CPEManagement.asmx/GetCustomPropertyTemplates",
                    method: "POST",
                    failure: function (xhr, options) { ORION.handleError(xhr); }
                }),
                listeners: {
                    load: function (store) {

                        grid.getView().focusRow(0);
                        grid.getSelectionModel().selectRow(0);
                    },
                    delay: 1
                }
            });

            grid = new Ext.grid.GridPanel({
                id: 'chooseTemplateGrid',
                region: 'center',
                cls: 'hide-header',
                store: dataStore,
                split: true,
                autoExpandColumn: 'Name',
                viewConfig: { forceFit: true },
                columns: [
                    { id: 'Name', editable: false, sortable: false, dataIndex: 'Name' }
                ],
                sm: new Ext.grid.RowSelectionModel({ singleSelect: true }),
                loadMask: true, width: 220, height: 400,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var type = mygrid.store.data.items[row].data.Type;
                        var description = mygrid.store.data.items[row].data.Description;
                        $("[name*='hfPropertyDescription']")[0].value = (description == null) ? "" : description;
                        $("[name*='hfPropertyFormat']")[0].value = (type == null) ? "" : type;
                        $("[name*='hfPropertyName']")[0].value = (type == null) ? "" : mygrid.store.data.items[row].data.Name

                        __doPostBack($("#hfPropertyNameRef")[0].value, "");
                    }
                },
                anchor: '0 0'
            });

            grid.render('ChoosePropertyGridPlaceHolder');

            grid.store.removeAll();
            grid.store.proxy.conn.jsonData = { targetEntity: targetEntity, includeAdvanced: true };
            grid.store.load();
        }
    };
} ();

Ext.onReady(SW.Orion.DefineCustomProperty.init, SW.Orion.DefineCustomProperty);

