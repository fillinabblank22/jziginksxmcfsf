(function (Core) {
    deleteValue = function (buttonId, id, value) {
        var cpKey = id.split(":");
        var dataString = JSON.stringify({ entityName: cpKey[0], propertyName: cpKey[1], value: value });

        $.ajax({
            type: "POST",
            url: 'EditCP.aspx/GetAffectedObjectsNumber',
            data: dataString,
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (dataResponse) {
                var number = parseInt(dataResponse.d);
                if (number == 0) {
                    __doPostBack(buttonId, '');
                    return false;
                }
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_IB0_5; E=js}',
                    msg: Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_7; E=js}', value, cpKey[1], number, '<a class="RenewalsLink" href="/Orion/Nodes/Default.aspx" target="_blank">', '</a>'),
                    buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                    icon: Ext.Msg.ERROR,
                    fn: function (btn) {
                        if (btn == 'yes')
                            __doPostBack(buttonId, '');
                    }
                });

                return false;
            },
            failure: function (response) {
                alert(response);
            }
        });

        return false;
    };
    deleteValues = function (button, id) {
        var cpKey = id.split(":");
        var dataString = JSON.stringify({ entityName: cpKey[0], propertyName: cpKey[1], value: '' });

        if (button.checked) return true;

        $.ajax({
            type: "POST",
            url: 'EditCP.aspx/GetAffectedObjectsNumber',
            data: dataString,
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (dataResponse) {
                var number = parseInt(dataResponse.d);
                if (number == 0) {
                    button.checked = !button.checked;
                    __doPostBack(button.name, '');
                    return true;
                }

                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_IB0_6; E=js}',
                    msg: Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_8; E=js}', cpKey[1], number, '<a class="RenewalsLink" href="/Orion/Nodes/Default.aspx" target="_blank">', '</a>'),
                    buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                    icon: Ext.Msg.ERROR,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            button.checked = !button.checked;
                            __doPostBack(button.name, '');
                        }
                    }
                });

                return false;
            },
            failure: function (response) {
                alert(response);
            }
        });
        return false;
    };

    propertySizeDlg = function () {
        var propertySize = $("[id*=hfCpPropertySize]").val();
        if (propertySize == '') propertySize = 400;
        $('#validationMessage').hide();
        $('#useMaxSize').change(function () {
            if (this.checked) { $('#maxSizeWarn').show(); $('#propertySize').prop('disabled', true); $('#validationMessage').hide(); }
            else { $('#maxSizeWarn').hide(); $('#propertySize').prop('disabled', false); }
        });

        if (propertySize == 0) { $('#propertySize').val('400').prop('disabled', true); $('#useMaxSize').attr('checked', 'checked').change(); }
        else { $('#propertySize').val(propertySize).prop('disabled', false); $('#useMaxSize').removeAttr('checked').change(); }

        $('[id*=cpSizeDialogOk]').click(function () {
            if ($('#useMaxSize').attr('checked')) { $("[id*=hfCpPropertySize]").val(0); $("#cpSizeDialog").dialog('close'); return; }
            if (validateSize()) {

                var oldPropertySize = parseInt($("[id*=hfCpPropertySize]").val());
                var newPropertySize = parseInt($('#propertySize').val());

                if (newPropertySize < oldPropertySize) {
                    var temp = $('[id*=hfCustomPropertyId]').val();
                    if (temp == null || temp == '') {
                        $("[id*=hfCpPropertySize]").val($('#propertySize').val());
                        __doPostBack($('[id*=hfCpSizeDialogOk]').val(), '');
                        return true;
                    }
                    var cpKey = temp.split(':');

                    SW.Core.Services.callWebService('EditCP.aspx', 'GetNumberAffectedValues', { entityName: cpKey[0], propertyName: cpKey[1], columnSize: newPropertySize }, function (dataResponse) {
                        var number = parseInt(dataResponse.Count);
                        var values = dataResponse.TopValues;

                        if (number == 0) {
                            $("[id*=hfCpPropertySize]").val($('#propertySize').val());
                            __doPostBack($('[id*=hfCpSizeDialogOk]').val(), '');
                            return true;
                        }

                        $('#cpWarningDialogText').html(Core.String.Format('@{R=Core.Strings;K=WEBJS_AY0_27; E=js}', number, cpKey[1], newPropertySize));

                        $('#cpValuesList').empty();
                        $('#cpMoreValues').html('');
                        $.each(values, function () {
                            $('#cpValuesList').append("<li>" + this + "</li>");
                        });

                        if (number > 5)
                            $('#cpMoreValues').html(Core.String.Format('@{R=Core.Strings;K=WEBJS_AY0_29; E=js}', number - 5));

                        $("#cpWarningDialog").show().dialog({
                            width: 600, height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: '@{R=Core.Strings;K=WEBJS_AY0_26; E=js}', close: function () { }, resizable: false
                        }).css("background-color", "white");

                        $('[id*=cpWarningDialogEdit]').click(function () {
                            SW.Core.Services.postToTarget('/Orion/Admin/CPE/InlineEditor.aspx', { 'CustomPropertyIds': $('[id*=hfCustomPropertyId]').val() }, false, true);
                        });

                        $('[id*=cpWarningDialogTrim]').click(function () {
                            $("[id*=hfCpPropertySize]").val($('#propertySize').val());
                            __doPostBack($('[id*=hfCpSizeDialogOk]').val(), ''); return true;
                        });
                        return false;
                    });
                }
                else {
                    $("[id*=hfCpPropertySize]").val($('#propertySize').val());
                    $("#cpSizeDialog").dialog('close'); return;
                }
            }
            else if (!validateSize()) {
                $('#validationMessage').show();
            }
            return false;
        });

        $('[id*=cpSizeDialogCancel]').click(function () {
            $("#cpSizeDialog").dialog('close');
        });

        $('[id*=cpWarningDialogKeep]').click(function () {
            $("#cpWarningDialog").dialog('close');
        });

        $("#cpSizeDialog").show().dialog({
            width: 500, height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: "@{R=Core.Strings;K=WEBJS_VL0_28; E=js}", close: function () { }, resizable: false
        }).css("background-color", "white");
    };

    function validateSize() {
        if ($('#propertySize').prop('disabled') == true) return true;
        var val = $('#propertySize').val();
        var intValue = parseInt(val);
        if (val != '' && val == intValue)
            return intValue > 0 && intValue <= 4000;
        else
            return false;
    }
})(SW.Core);