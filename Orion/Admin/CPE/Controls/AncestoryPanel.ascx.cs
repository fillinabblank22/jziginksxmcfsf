﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Controls;

public partial class AncestoryPanel : UserControl
{
    public string NodeTitle { get; set; }
    public string CheckboxOnClick { get; set; }
    public IList<NodeProperty> Ancestors { private get; set; }

    public AncestoryPanel() : this(string.Empty, string.Empty, null)
    {
    }

    public AncestoryPanel(string nodeTitle, string checkboxOnClickScript, IList<NodeProperty> ancestors)
    {
        NodeTitle = nodeTitle;
        CheckboxOnClick = checkboxOnClickScript;
        Ancestors = ancestors;
    }

    public List<string> GetSelectedColumns()
    {
        return (from RepeaterItem rpItem in AncestoryColumnList.Items
                select rpItem.FindControl("chkbx") as CheckBox into chkbx
                where chkbx != null && chkbx.Checked
                select chkbx.Attributes["ColumnName"]).ToList();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        AncestoryColumnList.DataSource = Ancestors;
        AncestoryColumnList.DataBind();
    }

    protected void AncestoryPanel_ItemDataBound(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

        var nodeProperty = (NodeProperty)e.Item.DataItem;

        var cb = (CheckBox)e.Item.FindControl("chkbx");
        var columnName = nodeProperty.ColumnName;
        cb.Attributes.Add("ColumnName", columnName);
        cb.Attributes["onclick"] = CheckboxOnClick;

        var columnDisplayName = nodeProperty.ColumnDisplayName;
        cb.Text = string.IsNullOrEmpty(columnDisplayName) ? columnName : columnDisplayName;
    }
}