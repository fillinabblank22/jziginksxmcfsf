﻿using System;
using System.Web.UI.WebControls;

public partial class Orion_Admin_ProductsBlog : System.Web.UI.Page
{
	protected override void OnLoadComplete(EventArgs e)
	{
		base.OnLoadComplete(e);
		productBlog.LoadControls();
	}
}
