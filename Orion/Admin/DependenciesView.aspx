<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/LimitedAdminPage.master" AutoEventWireup="true"
    CodeFile="DependenciesView.aspx.cs" Inherits="Orion_Admin_DependenciesView" Title="<%$ HtmlEncodedResources: CoreWebContent, ResourcesAll_ManageDependencies %>" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" File="Admin/Dependencies/Dependencies.css" />
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <style type="text/css">
        .error
        {
            border: 2px solid red;
        }
        #gridCell div#Grid
        {
            width: 100%;
        }
        #adminContent td
        {
            padding: 0px 0px 0px 0px !important;
            vertical-align: middle;
        }
        .no-border .x-table-layout-ct, .panel-no-border .x-panel-body-noheader
        {
            border: none !important;
        }
        .hide-header .x-grid3-header
        {
            display: none;
        }
        .x-tip-body 
        {
            white-space: nowrap !important;
        }
        .createtip-primary {
            font-weight: bold;
            margin-right: 5px;
        }
        .createtip-secondary {
            
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 style="font-size:large!important;"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
            <asp:ServiceReference Path="/Orion/Services/WebAdmin.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <orion:Include runat="server" File="Admin/js/DependenciesGrid.js" />

    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
			    <div style="padding-top: 10px;">
                    <orion:HelpHint  runat="server" >
                        <HelpContent>
                            <%= DefaultSanitizer.SanitizeHtml(String.Format(CoreWebContent.WEBDATA_VB0_527,"<a class=\"coloredLink\" href='"+ SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(1828) +"'>","</a>")) %>
                        </HelpContent>
                    </orion:HelpHint>
			    </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="padding: 15px 0;">
                    <span class="createtip-primary"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.Dependencies_CreateTipPrimary) %></span><span class="createtip-secondary"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.Dependencies_CreateTipSecondary) %></span>
                </div>
                <div <%= (!allowTopology) ? "style='display:none;'" : "class='sw-auto-dependency-setting'" %> ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Dependencies_AutoCalculateSettingLabel) %>:<a href="#" class="sw-auto-dependency-toggle"></a></div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="DependenciesGrid-ErrorMessage" style="width: 957px"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top:4px!important">
                <orion:NavigationTabBar ID="DependenciesTabBar" runat="server" Visible="true">
                    <orion:NavigationTabItem Name="<%$HtmlEncodedResources:CoreWebContent,ResourcesAll_ManageDependencies%>" Url="~/Orion/Admin/DependenciesView.aspx" />
                    <orion:NavigationTabItem Name="<%$HtmlEncodedResources:CoreWebContent,ResourcesAll_ManageIgnoredDependencies%>" Url="~/Orion/Admin/IgnoredDependenciesView.aspx" />
               </orion:NavigationTabBar>

                <input type="hidden" name="Dependencies_PageSize" id="Dependencies_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("Dependencies_PageSize")) %>' />

                <div id="DependenciesGrid">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
