using System;
using System.Text;
using System.Web.UI;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_SelectLimitationType : System.Web.UI.Page
{
    private ProfileCommon _thisProfile;
    private ViewInfo _thisView;

    protected ProfileCommon ThisProfile
    {
        get
        {
            if (null == _thisProfile)
            {
                _thisProfile = Profile.GetProfile(Request.QueryString["AccountID"]);
            }

            return _thisProfile;
        }
    }

    protected ViewInfo ThisView
    {
        get 
        {
            if (null == _thisView)
            {
                try
                {
                    _thisView = ViewManager.GetViewById(Convert.ToInt32(Request.QueryString["ViewID"]));
                }
                catch
                {
                    // do nothing
                }
            }

            return _thisView;
        }
    }

    protected int LimitationIndex
    {
        get
        {
            return Convert.ToInt32(Request.QueryString["Index"]);
        }
    }

    protected bool IsAccountBased
    {
        get
        {
            return !((null == this.ThisProfile) || 
                    string.IsNullOrEmpty(this.ThisProfile.UserName) || 
                    this.ThisProfile.IsAnonymous);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (!this.IsAccountBased)
        {
            if (null == this.ThisView)
                this.OnError(e);

            this.paraAccountMessage.Visible = false;
            this.h1Account.Visible = false;
            this.paraViewMessage.Visible = true;
            this.h1View.Visible = true;
        }
        else
        {
            this.paraAccountMessage.Visible = true;
            this.h1Account.Visible = true;
            this.paraViewMessage.Visible = false;
            this.h1View.Visible = false;
        }

        this.rptTypes.DataSource = LimitationType.GetAllLimitationTypes();
        this.rptTypes.DataBind();

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    protected override void OnError(EventArgs e)
    {
        OrionErrorPageBase.TransferToErrorPage(Context, 
            new ErrorInspectorDetails {
                Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_322, Server.GetLastError()),
               
            }); 
    }

    protected void ContinueClick(object sender, EventArgs e)
    {
        if ((!this.IsAccountBased) && (null == this.ThisView))
            this.OnError(e);

        
        if (!string.IsNullOrEmpty(Request.Form["LimitationType"]))
        {
            int limitationTypeID;
            
            if(Int32.TryParse(Request.Form["LimitationType"], out limitationTypeID))
            {
                var uri = new StringBuilder(@"/Orion/Admin/EditLimitation.aspx");
                if (IsAccountBased)
                {
                    uri.AppendFormat(@"?AccountID={0}&Index={1}", this.Server.UrlEncode(ThisProfile.UserName), LimitationIndex);
                }
                else
                {
                    uri.AppendFormat(@"?ViewID={0}", ThisView.ViewID);
                }
                uri.AppendFormat(@"&LimitationTypeID={0}", limitationTypeID)
                    .Append(@"&ReturnTo=")
                    .Append(ReferrerRedirectorBase.GetReferrerUrl());

                Response.Redirect(uri.ToString());                                                       
            }
        }
        
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Return(string.Format(
            @"/Orion/Admin/Accounts/EditAccount.aspx?ReturnTo={0}&AccountID={1}", 
            ReferrerRedirectorBase.GetReferrerUrl(),
            this.Server.UrlEncode(ThisProfile.UserName)));
    }
}
