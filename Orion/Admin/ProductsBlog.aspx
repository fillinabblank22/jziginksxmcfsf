<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="ProductsBlog.aspx.cs" 
    Inherits="Orion_Admin_ProductsBlog" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_TM0_47 %>" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/ProductBlogControl.ascx" TagPrefix="orion" TagName="ProductBlog" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <style type="text/css">
	    #adminContent p, #adminContent ul, #adminContent ol { border: none; font-size: 13px; line-height: 150%;}
		#adminContent img { border: 1px solid #DFDED7 ; padding: 0; }
	    #adminContent a { color: #336699; }
	    #adminContent a:hover {color:orange;}
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionProductBlog" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
<table width="100%">
    <tr>
        <td colspan="2">
            <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="font-size: 8pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_160) %></td>
    </tr>
</table>
<%if ((this.Profile.AllowAdmin) || (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
  {%>
    <orion:ProductBlog runat="server" ID="productBlog" />
<%} %>
</asp:Content>