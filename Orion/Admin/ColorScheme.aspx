<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_9 %>" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="SolarWinds.Internationalization.Tokens" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.i18n" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        if (!this.IsPostBack)
        {
            string curScheme = WebSettingsDAL.StyleSheet;
            
            foreach (string filename in Directory.GetFiles(Server.MapPath("/WebEngine/Resources"), "*.css"))
            {
                FileInfo myFile = new FileInfo(filename);
                using (var datain = File.OpenText(filename))
                using (var dataout = new StringWriter())
                {
                    TokenSubstitution.Parse(datain, dataout, TokenSubstitutionSequence.DisallowDeferrals);
                    using (var sr = new StringReader(dataout.ToString()))
                    {
                        string title = sr.ReadLine().Trim();
                        if (title.StartsWith("/*") && title.EndsWith("*/"))
                            title = title.Substring(2, title.Length - 4).Trim();
                        else
                            title = myFile.Name; // First line wasn't a comment. Just display the filename instead.

                        this.lstSchemes.Items.Add(new ListItem(title, myFile.Name));
                    }
                }
            }

            if (!string.IsNullOrEmpty(curScheme))
                lstSchemes.Items.FindByValue(curScheme).Selected = true;
        }
        
        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.lstSchemes.SelectedValue))
        {
            WebSettingsDAL.StyleSheet = this.lstSchemes.SelectedValue;
            Response.Redirect("/Orion/");
        }
    }  
</script>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(this.Page.Title) %></h1>
    
    <p>
        <div>
            <asp:RadioButtonList runat="server" ID="lstSchemes" RepeatLayout="Table" />
            <asp:RequiredFieldValidator runat="server" ID="rbListRfv" ControlToValidate="lstSchemes" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_125 %>" EnableClientScript="false" Display="static" />
        </div>
        <div class="sw-btn-bar">
            <orion:LocalizableButton ID="LocalizableButton1" runat="server" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
        </div>
    </p>
</asp:Content>

