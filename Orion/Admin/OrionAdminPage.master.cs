using System;
using System.Collections.Generic;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_OrionAdminPage : System.Web.UI.MasterPage
{
    private static readonly List<string> _allowedGuestPages = new List<string>
	        {
                "/Orion/Admin/Default.aspx",

                "/Orion/Admin/Accounts/Accounts.aspx",
                "/Orion/Admin/Accounts/AccountGroups.aspx",
                "/Orion/Admin/Accounts/AdvancedADSettings.aspx",

                "/Orion/Admin/ProductsBlog.aspx",
                "/Orion/Admin/Containers/Default.aspx",
                "/Orion/Admin/DependenciesView.aspx",
                "/Orion/Admin/AdvAlerts.aspx",

                "/Orion/Admin/ModuleConfig.aspx",
                "/Orion/DPI/Admin/Default.aspx",
                "/Orion/DPI/Admin/ManageApplications.aspx",
                "/Orion/DPI/Admin/Applications/Add/Default.aspx",
                "/Orion/DPI/Admin/Applications/Add/ConfigureApplication.aspx",
                "/Orion/DPI/Admin/Applications/Add/ConfigureDataCollection.aspx",
                "/Orion/DPI/Admin/Applications/Add/Summary.aspx",
                "/Orion/Admin/SmtpServersManager.aspx"
	        };

    protected bool IsDemoMode
    {
        get { return SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if( !Profile.AllowAdmin && !(this.Page is IBypassAccessLimitation) )
        {
            if (IsDemoMode
                && _allowedGuestPages.Exists(s => String.Equals(s, Request.Path, StringComparison.InvariantCultureIgnoreCase)))
            {
                return;
            }

            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });
            
        }

        if ((CommonWebHelper.IsProductBlogDisabled && Request.Path.Equals("/Orion/Admin/ProductsBlog.aspx", StringComparison.InvariantCultureIgnoreCase))
            || (CommonWebHelper.IsMaintenanceRenewalsDisabled && Request.Path.Equals("/Orion/Admin/MaintenanceRenewals.aspx", StringComparison.InvariantCultureIgnoreCase)))
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_324) });
    }

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            string viewID = (Request["ViewID"] != null) ? Request["ViewID"].ToString() : null;
            string returnTo = (Request["ReturnTo"] != null) ? Request["ReturnTo"].ToString() : null;
            string accountID = (Request["AccountID"] != null) ? Request["AccountID"].ToString() : null;
            var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
            OrionSiteMapPath.SetUpRenderer(renderer);
        }
    }
}
