using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Converters;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_EditLimitation : System.Web.UI.Page
{
    protected LimitationType LimitationType { get; private set; }
    protected Limitation Limitation { get; private set; }

    protected override void OnInit(EventArgs e)
    {
            var limId = Request.QueryString["LimitationID"];
            if (! string.IsNullOrEmpty(limId))
            {
                Limitation = Limitation.GetLimitationByID(Convert.ToInt32(limId));
                LimitationType = Limitation.Type;
            }
            else
            {
                var limTypeId = Request.QueryString["LimitationTypeID"];
                if (! string.IsNullOrEmpty(limTypeId))
                {
                    LimitationType = LimitationType.GetLimitationTypeById(Convert.ToInt32(limTypeId));
                }
            }

            this.phTypeDesc.Controls.Add(new LiteralControl(WebSecurityHelper.HtmlEncode(LimitationType.Description)));
            this.phTypeName.Controls.Add(new LiteralControl(WebSecurityHelper.HtmlEncode(LimitationType.Name)));

            var items = Limitation != null ? Limitation.Items : new string[0];
            double val;

        using (var swisErrorsContext = new SwisErrorsContext())
        {
            switch (this.LimitationType.Method)
            {
                case LimitationMethod.Pattern:
                    this.lstNetObjects.Visible = false;
                    this.cbxLstObjects.Visible = false;

                    if ((null != items) && (items.Length == 1))
                    {
                        txtPattern.Text = WebSecurityHelper.HtmlEncode(items[0]).ToString();
                    }

                    break;
                case LimitationMethod.Checkbox:
                    this.lstNetObjects.Visible = false;
                    this.txtPattern.Visible = false;

                    this.cbxLstObjects.DataSource = Limitation != null
                        ? SanitizeDictionary(Limitation.GetNameValuePairs())
                        : SanitizeDictionary(LimitationType.GetNameValuePairs());
                    this.cbxLstObjects.DataBind();

					var converter = new LimitationInputValueConverter();
                    if (null != items)
                    {
                        foreach (string value in items)
                        {
                            if (value.Equals("1"))
                            {
                                ListItem itemTrue = this.cbxLstObjects.Items.FindByValue("True");
                                if (itemTrue != null)
                                {
                                    itemTrue.Selected = true;
                                }
                            }
                            if (value.Equals("0"))
                            {
                                ListItem itemTrue = this.cbxLstObjects.Items.FindByValue("False");
                                if (itemTrue != null)
                                {
                                    itemTrue.Selected = true;
                                }
                            }
                            if (value.Equals("NULL"))
                            {
                                ListItem itemTrue = this.cbxLstObjects.Items.FindByValue(string.Empty);
                                if (itemTrue != null)
                                {
                                    itemTrue.Selected = true;
                                }
                            }

							var convertedValue = converter.Convert(value);
                            ListItem item =
                                this.cbxLstObjects.Items.FindByValue(convertedValue);
                            if (null != item)
                            {
                                item.Selected = true;
                            }
                        }
                    }
                    break;
                case LimitationMethod.Selection:
                    this.cbxLstObjects.Visible = false;
                    this.txtPattern.Visible = false;

                    this.lstNetObjects.DataSource = Limitation != null
                        ? SanitizeDictionary(Limitation.GetNameValuePairs())
                        : SanitizeDictionary(LimitationType.GetNameValuePairs());
                    this.lstNetObjects.DataBind();

                    if (null != items && items.Length == 1)
                    {
                        this.lstNetObjects.SelectedValue = double.TryParse(items[0], NumberStyles.Float,
                            CultureInfo.InvariantCulture, out val)
                            ? val.ToString(CultureInfo.CurrentCulture)
                            : items[0];
                    }
                    break;
            }

            var swisErrorMessages = swisErrorsContext.GetErrorMessages();
            if (swisErrorMessages.Any())
            {
                SwisfErrorControl.SetError(swisErrorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList());
                SwisfErrorControl.Visible = true;
            }
        }

        base.OnInit(e);
    }

    private IDictionary<string, string> SanitizeDictionary(Dictionary<string, string> inputDict)
    {
        IDictionary<string, string> result = new Dictionary<string, string>();
        foreach (KeyValuePair<string, string> pair in inputDict)
        {
            result.Add(WebSecurityHelper.HtmlEncode(pair.Key), WebSecurityHelper.HtmlEncode(pair.Value));
        }

        return result;
    }

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    private string DecodeAndCorrectSQLString(string value)
    {
        if (string.IsNullOrEmpty(value))
            return value;

        return WebSecurityHelper.HtmlDecode(value).Replace("'", "''");
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        string[] limitationItems = null;
        var converter = new LimitationInputValueConverter();

        switch (LimitationType.Method)
        {
            case LimitationMethod.Selection:
            {
                if (string.IsNullOrWhiteSpace(lstNetObjects.SelectedValue))
                {
                    throw new InvalidOperationException(CoreWebContent.WEBDATA_VS0_6);
                }

                var convertedValue = converter.Convert(DecodeAndCorrectSQLString(lstNetObjects.SelectedValue));

                limitationItems = new[] { convertedValue };

                break;
            }
            case LimitationMethod.Checkbox:
            {
                var values = new List<string>();

                foreach (ListItem item in cbxLstObjects.Items)
                {
                    if (item.Selected)
                    {
                        if (string.IsNullOrWhiteSpace(item.Value))
                        {
                            continue;
                        }

                        var convertedValue = converter.Convert(DecodeAndCorrectSQLString(item.Value));

                        values.Add(convertedValue);
                    }
                }
                if (values.Count == 0)
                {
                    throw new InvalidOperationException(CoreWebContent.WEBDATA_VS0_6);
                }
                limitationItems = values.ToArray();
                break;
            }
            case LimitationMethod.Pattern:
            {
                var pattern = txtPattern.Text.Trim();
                if (string.IsNullOrEmpty(pattern))
                {
                    throw new InvalidOperationException(CoreWebContent.WEBDATA_VS0_6);
                }
                limitationItems = new[] { DecodeAndCorrectSQLString(pattern) };
                break;
            }
        }

        var limitation = Limitation;
        if (limitation == null)
        {
            limitation = Limitation.CreateNewLimitation(LimitationType, limitationItems);
            Limitation = limitation;
        }
        else
        {
            limitation.Update(limitationItems);
        }

        var accountID = Request.QueryString["AccountID"];
        if (! string.IsNullOrWhiteSpace(accountID))
        {
            SubmitAccountLimitation(accountID);
        }
        else
        {
            SubmitViewLimitation();
        }

        var returnTo = Request.QueryString["ReturnTo"];
        if (string.IsNullOrWhiteSpace(returnTo))
        {
            ReferrerRedirectorBase.Return("/Orion/Admin/Default.aspx");
        }
        else
        {
            ReferrerRedirectorBase.Return(returnTo);
        }
    }

    private void SubmitAccountLimitation(string accountID)
    {
        var profile = Profile.GetProfile(accountID);
        if (profile == null)
        {
            throw new InvalidOperationException(string.Format(CoreWebContent.WEBDATA_VS0_7, accountID));
        }
        var index = Request.QueryString["Index"];
        if (! string.IsNullOrWhiteSpace(index))
        {
            switch (Convert.ToInt32(index))
            {
                case 1:
                    profile.LimitationID1 = Limitation.LimitationID;
                    break;
                case 2:
                    profile.LimitationID2 = Limitation.LimitationID;
                    break;
                case 3:
                    profile.LimitationID3 = Limitation.LimitationID;
                    break;
                default:
                    throw new InvalidOperationException(string.Format(CoreWebContent.WEBDATA_VS0_8, index));
            }
        }
        profile.Save();
        AccountManagementDAL.UpdateGroupUsersLimitations(profile.UserName,
                                                         profile.AccountType,
                                                         profile.LimitationID1,
                                                         profile.LimitationID2,
                                                         profile.LimitationID3);
    }

    private void SubmitViewLimitation()
    {
        var viewID = Request.QueryString["ViewID"];
        if (string.IsNullOrWhiteSpace(viewID))
        {
            throw new InvalidOperationException(CoreWebContent.WEBDATA_VS0_9);
        }

        var view = ViewManager.GetViewById(Convert.ToInt32(viewID));
        if (view == null)
        {
            throw new InvalidOperationException(string.Format(CoreWebContent.WEBDATA_VS0_10, viewID));
        }

        view.LimitationID = Limitation.LimitationID;

        ViewManager.UpdateViewGroupLimitation(view);
    }
}
