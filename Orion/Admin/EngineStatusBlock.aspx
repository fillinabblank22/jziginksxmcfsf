﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EngineStatusBlock.aspx.cs" Inherits="Orion_Admin_EngineStatusBlock" EnableViewState="false" %>

<%--This is intended to be displayed inline in Orion/Admin/Details/Engines.aspx.--%>
<table>
    <asp:placeholder runat="server" id="engineInfoPlugins" />
</table>

