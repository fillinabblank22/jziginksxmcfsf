<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceEditor.ascx.cs"
    Inherits="Orion_Admin_ResourceEditor" %>
<%@ Register TagPrefix="orion" TagName="ResourcePickerLoader" Src="~/Orion/Controls/ResourcePicker/ResourcePickerLoader.ascx" %>

<%--Column 1--%>
<div style="float:left;clear:both;white-space: nowrap;">
    <div  class="divColumn">
        <div class="colHeader">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0">
                        <h2>
                           <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_130) %></h2>
                    </td>
                    <td style="padding: 0; text-align: right;">
                        <asp:ImageButton CssClass="imgButton" ID="ImageButton1" runat="server" ImageUrl="/Orion/images/delete_icon_white16x16.png"
                            OnCommand="btnDeleteCol_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" CommandArgument="1"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" OnClientClick="DeleteColumn(this); return false;" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="resSpan"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_129) %></b></div>
        <table class="resList">
            <tr style="vertical-align: top">
                <td>
                    <asp:ListBox runat="server" ID="lbxColumn1" Width="200px" Height="215px" Rows="14" DataValueField="ID"
                        DataTextField="Title" SelectionMode="multiple"></asp:ListBox>
                </td>
                <td class="viewColumnButtons">
                    <asp:ImageButton ID="btnAdd1" runat="server" ImageUrl="~/orion/images/SubViewImages/Add.png"
                        OnClick="btnAdd_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_282 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_282 %>" /><br />
                    <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="~/orion/images/SubViewImages/delete_16x16.png"
                        OnClick="btnDelete_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_283 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_283 %>" /><br />
                    <asp:ImageButton ID="btnClone1" runat="server" ImageUrl="~/orion/images/SubViewImages/Copy.png"
                        OnClick="btnClone_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_284 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_284 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveTop1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTop.png"
                        OnClick="btnMoveTop_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_285 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_285 %>" /><br />
                    <asp:ImageButton ID="btnMoveUp1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveUp.png"
                        OnClick="btnMoveUp_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_286 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_286 %>" /><br />
                    <asp:ImageButton ID="btnMoveDown1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveDown.png"
                        OnClick="btnMoveDown_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_287 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_287 %>" /><br />
                    <asp:ImageButton ID="btnMoveBottom1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveBottom.png"
                        OnClick="btnMoveBottom_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_288 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_288 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveRight1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveRight.png"
                        OnClick="LateralRightMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_289 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_289 %>" /><br />
                    <asp:ImageButton ID="btnMoveLeft1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveLeft.png"
                        OnClick="LateralLeftMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_290 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_290 %>" />
                </td>
            </tr>
            <tr>
                <td>
                <table cellpadding="0" cellspacing="0" class="wtable">
                    <tr>
                        <td>
                            <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_75) %></b></div>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="col1Width" OnTextChanged="columnWidth_TextChanged" Rows="1" Width="40" Columns="1"/>                           
                        </td>
                        <td>                           
                            <div ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_PX) %></div> 
                            <asp:RequiredFieldValidator Display="Dynamic" ID="col1WidthRequiredFieldValidator" runat="server" ControlToValidate="col1Width" ErrorMessage="*" />                          
                            <asp:RangeValidator Display="Dynamic" ID="col1WidthRangeValidator" runat="server" Type="Integer" MinimumValue="0" MaximumValue="1920" ControlToValidate="col1Width" ErrorMessage="*" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divMoveTab1" onclick="ChooseDestinationClickHandler(buttonMoveTab1CallBack);return false;"
                        runat="server" visible="False" style="cursor: pointer; width: 200px">
                        <asp:ImageButton ID="btnMoveTab" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTab.png"
                            OnClick="btnMoveTab_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_48) %>
                    </div>
                </td>
            </tr>
        </table>
    </div>
   
    <div runat="server" id="divColumn2" class="divColumn">
        <div class="colHeader">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0">
                        <h2>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_131) %></h2>
                    </td>
                    <td style="padding: 0; text-align: right;">
                        <asp:ImageButton CssClass="imgButton" ID="col2Delete" runat="server" ImageUrl="/Orion/images/delete_icon_white16x16.png"
                            OnCommand="btnDeleteCol_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" CommandArgument="2"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" OnClientClick="DeleteColumn(this); return false;"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="resSpan"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_129) %></b></div>
        <table class="resList">
            <tr style="vertical-align: top;">
                <td>
                    <asp:ListBox runat="server" ID="lbxColumn2" Width="200px" Height="215px" Rows="14" DataValueField="ID"
                        DataTextField="Title" SelectionMode="multiple"></asp:ListBox>
                </td>
                <td class="viewColumnButtons">
                    <asp:ImageButton ID="btnAdd2" runat="server" ImageUrl="~/orion/images/SubViewImages/Add.png"
                        OnClick="btnAdd_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_292 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_292 %>" /><br />
                    <asp:ImageButton ID="btnDelete2" runat="server" ImageUrl="~/orion/images/SubViewImages/delete_16x16.png"
                        OnClick="btnDelete_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_293 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_293 %>" /><br />
                    <asp:ImageButton ID="btnClone2" runat="server" ImageUrl="~/orion/images/SubViewImages/Copy.png"
                        OnClick="btnClone_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_294 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_294 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveTop2" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTop.png"
                        OnClick="btnMoveTop_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_295 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_295 %>" /><br />
                    <asp:ImageButton ID="btnMoveUp2" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveUp.png"
                        OnClick="btnMoveUp_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_296 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_296 %>" /><br />
                    <asp:ImageButton ID="btnMoveDown2" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveDown.png"
                        OnClick="btnMoveDown_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_297 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_297 %>" /><br />
                    <asp:ImageButton ID="btnMoveBottom2" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveBottom.png"
                        OnClick="btnMoveBottom_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_298 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_298 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveRight2" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveRight.png"
                        OnClick="LateralRightMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_299 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_299 %>" /><br />
                    <asp:ImageButton ID="btnMoveLeft2" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveLeft.png"
                        OnClick="LateralLeftMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_300 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_300 %>" />
                </td>
            </tr>
            <tr>
                <td>
                <table cellpadding="0" cellspacing="0" class="wtable">
                    <tr>
                        <td>
                            <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_75) %></b></div>
                        </td>
                        <td>
                            <asp:TextBox runat="server" OnTextChanged="columnWidth_TextChanged" ID="col2Width" Rows="1" Width="40" Columns="1"/>
                        </td>
                        <td>
                            <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_PX) %></div>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="col2WidthRequiredFieldValidator" runat="server" ControlToValidate="col2Width" ErrorMessage="*" />                         
                            <asp:RangeValidator Display="Dynamic" ID="col2WidthRangeValidator" runat="server" Type="Integer" MinimumValue="0" MaximumValue="1920" ControlToValidate="col2Width" ErrorMessage="*" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divMoveTab2" onclick="ChooseDestinationClickHandler(buttonMoveTab2CallBack);return false;"
                        runat="server" visible="False" style="cursor: pointer; width: 200px">
                        <asp:ImageButton ID="btnMoveTab2" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTab.png"
                            OnClick="btnMoveTab_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_48) %>
                    </div>
                </td>
            </tr>
        </table>
    </div>
   
    <div runat="server" id="divColumn3" class="divColumn">
        <div class="colHeader">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0">
                        <h2>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_132) %></h2>
                    </td>
                    <td style="padding: 0; text-align: right;">
                        <asp:ImageButton CssClass="imgButton" ID="ImageButton2" runat="server" ImageUrl="/Orion/images/delete_icon_white16x16.png"
                            OnCommand="btnDeleteCol_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" CommandArgument="3"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" OnClientClick="DeleteColumn(this); return false;"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="resSpan"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_129) %></b></div>
        <table class="resList">
            <tr style="vertical-align: top">
                <td>
                    <asp:ListBox runat="server" ID="lbxColumn3" Width="200px" Height="215px" Rows="14" DataValueField="ID"
                        DataTextField="Title" SelectionMode="multiple"></asp:ListBox>
                </td>
                <td class="viewColumnButtons">
                    <asp:ImageButton ID="btnAdd3" runat="server" ImageUrl="~/orion/images/SubViewImages/Add.png"
                        OnClick="btnAdd_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_303 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_303 %>" /><br />
                    <asp:ImageButton ID="btnDelete3" runat="server" ImageUrl="~/orion/images/SubViewImages/delete_16x16.png"
                        OnClick="btnDelete_Click" AlternateText="Delete Selected Resources From Column 3"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_304 %>" /><br />
                    <asp:ImageButton ID="btnClone3" runat="server" ImageUrl="~/orion/images/SubViewImages/Copy.png"
                        OnClick="btnClone_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_305 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_305 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveTop3" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTop.png"
                        OnClick="btnMoveTop_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_306 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_306 %>" /><br />
                    <asp:ImageButton ID="btnMoveUp3" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveUp.png"
                        OnClick="btnMoveUp_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_307 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_307 %>" /><br />
                    <asp:ImageButton ID="btnMoveDown3" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveDown.png"
                        OnClick="btnMoveDown_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_308 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_308 %>" /><br />
                    <asp:ImageButton ID="btnMoveBottom3" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveBottom.png"
                        OnClick="btnMoveBottom_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_309 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_309 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveRight3" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveRight.png"
                        OnClick="LateralRightMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_25 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_25 %>" /><br />
                    <asp:ImageButton ID="btnMoveLeft3" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveLeft.png"
                        OnClick="LateralLeftMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_26 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_26 %>" />
                </td>
            </tr>
            <tr>
                <td>
                <table cellpadding="0" cellspacing="0" class="wtable">
                    <tr>
                        <td>
                            <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_75) %></b></div>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="col3Width" OnTextChanged="columnWidth_TextChanged" Rows="1" Width="40" Columns="1"/>
                        </td>
                        <td>
                            <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_PX) %></div>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="col3WidthRequiredFieldValidator" runat="server" ControlToValidate="col3Width" ErrorMessage="*" />                          
                            <asp:RangeValidator Display="Dynamic" ID="col3WidthRangeValidator" runat="server" Type="Integer" MinimumValue="0" MaximumValue="1920" ControlToValidate="col3Width" ErrorMessage="*" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divMoveTab3" runat="server" onclick="ChooseDestinationClickHandler(buttonMoveTab3CallBack);return false;"
                        visible="false" style="cursor: pointer; width: 200px">
                        <asp:ImageButton ID="btnMoveTab3" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTab.png"
                            OnClick="btnMoveTab_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_48) %>
                    </div>
                </td>
            </tr>
        </table>
    </div>
   
    <div runat="server" id="divColumn4" class="divColumn">
        <div class="colHeader">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0">
                        <h2>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_133) %></h2>
                    </td>
                    <td style="padding: 0; text-align: right;">
                        <asp:ImageButton CssClass="imgButton" ID="ImageButton3" runat="server" ImageUrl="/Orion/images/delete_icon_white16x16.png"
                            OnCommand="btnDeleteCol_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" CommandArgument="4"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" OnClientClick="DeleteColumn(this); return false;"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="resSpan"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_129) %></b></div>
        <table class="resList">
            <tr style="vertical-align: top">
                <td>
                    <asp:ListBox runat="server" ID="lbxColumn4" Width="200px" Height="215px" Rows="14" DataValueField="ID"
                        DataTextField="Title" SelectionMode="multiple"></asp:ListBox>
                </td>
                <td class="viewColumnButtons">
                    <asp:ImageButton ID="btnAdd4" runat="server" ImageUrl="~/orion/images/SubViewImages/Add.png"
                        OnClick="btnAdd_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_0 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_0 %>" /><br />
                    <asp:ImageButton ID="btnDelete4" runat="server" ImageUrl="~/orion/images/SubViewImages/delete_16x16.png"
                        OnClick="btnDelete_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_1 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_1 %>" /><br />
                    <asp:ImageButton ID="btnClone4" runat="server" ImageUrl="~/orion/images/SubViewImages/Copy.png"
                        OnClick="btnClone_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_2 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_2 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveTop4" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTop.png"
                        OnClick="btnMoveTop_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_3 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_3 %>" /><br />
                    <asp:ImageButton ID="btnMoveUp4" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveUp.png"
                        OnClick="btnMoveUp_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_4 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_4 %>" /><br />
                    <asp:ImageButton ID="btnMoveDown4" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveDown.png"
                        OnClick="btnMoveDown_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_5 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_5 %>" /><br />
                    <asp:ImageButton ID="btnMoveBottom4" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveBottom.png"
                        OnClick="btnMoveBottom_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_6 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_6 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveRight4" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveRight.png"
                        OnClick="LateralRightMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_7 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_7 %>" /><br />
                    <asp:ImageButton ID="btnMoveLeft4" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveLeft.png"
                        OnClick="LateralLeftMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_8 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_8 %>" />
                </td>
            </tr>
            <tr>
                <td>
                <table cellpadding="0" cellspacing="0" class="wtable">
                    <tr>
                        <td>
                            <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_75) %></b></div>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="col4Width" OnTextChanged="columnWidth_TextChanged" Rows="1" Width="40" Columns="1"/>
                        </td>
                        <td>
                            <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_PX) %></div>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="col4WidthRequiredFieldValidator" runat="server" ControlToValidate="col4Width" ErrorMessage="*" />                           
                            <asp:RangeValidator Display="Dynamic" ID="col4WidthRangeValidator" runat="server" Type="Integer" MinimumValue="0" MaximumValue="1920" ControlToValidate="col4Width" ErrorMessage="*" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divMoveTab4" runat="server" onclick="ChooseDestinationClickHandler(buttonMoveTab4CallBack);return false;"
                        visible="false" style="cursor: pointer; width: 200px">
                        <asp:ImageButton ID="btnMoveTab4" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTab.png"
                            OnClick="btnMoveTab_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_48) %>
                    </div>
                </td>
            </tr>
        </table>
    </div>
   
    <div runat="server" id="divColumn5" class="divColumn">
         <div class="colHeader">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0">
                        <h2>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_134) %></h2>
                    </td>
                    <td style="padding: 0; text-align: right;">
                        <asp:ImageButton CssClass="imgButton" ID="ImageButton4" runat="server" ImageUrl="/Orion/images/delete_icon_white16x16.png"
                            OnCommand="btnDeleteCol_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" CommandArgument="5"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" OnClientClick="DeleteColumn(this); return false;"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="resSpan"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_129) %></b></div>
        <table class="resList">
            <tr style="vertical-align: top">
                <td>
                    <asp:ListBox runat="server" ID="lbxColumn5" Width="200px" Height="215px" Rows="14" DataValueField="ID"
                        DataTextField="Title" SelectionMode="multiple"></asp:ListBox>
                </td>
                <td class="viewColumnButtons">
                    <asp:ImageButton ID="btnAdd5" runat="server" ImageUrl="~/orion/images/SubViewImages/Add.png"
                        OnClick="btnAdd_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_9 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_9 %>" /><br />
                    <asp:ImageButton ID="btnDelete5" runat="server" ImageUrl="~/orion/images/SubViewImages/delete_16x16.png"
                        OnClick="btnDelete_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_10 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_10 %>" /><br />
                    <asp:ImageButton ID="btnClone5" runat="server" ImageUrl="~/orion/images/SubViewImages/Copy.png"
                        OnClick="btnClone_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_11 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_11 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveTop5" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTop.png"
                        OnClick="btnMoveTop_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_12 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_12 %>" /><br />
                    <asp:ImageButton ID="btnMoveUp5" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveUp.png"
                        OnClick="btnMoveUp_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_13 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_13 %>" /><br />
                    <asp:ImageButton ID="btnMoveDown5" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveDown.png"
                        OnClick="btnMoveDown_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_14 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_14 %>" /><br />
                    <asp:ImageButton ID="btnMoveBottom5" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveBottom.png"
                        OnClick="btnMoveBottom_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_15 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_15 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveRight5" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveRight.png"
                        OnClick="LateralRightMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_16 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_16 %>" /><br />
                    <asp:ImageButton ID="btnMoveLeft5" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveLeft.png"
                        OnClick="LateralLeftMove_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_17 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_17 %>" />
                </td>
            </tr>
            <tr>
                <td>
                <table cellpadding="0" cellspacing="0" class="wtable">
                    <tr>
                        <td>
                            <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_75) %></b></div>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="col5Width" OnTextChanged="columnWidth_TextChanged" Rows="1" Width="40" Columns="1"/>
                        </td>
                        <td>
                            <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_PX) %></div>
                             <asp:RequiredFieldValidator Display="Dynamic" ID="col5WidthRequiredFieldValidator" runat="server" ControlToValidate="col5Width" ErrorMessage="*" />                           
                            <asp:RangeValidator Display="Dynamic" ID="col5WidthRangeValidator" runat="server" Type="Integer" MinimumValue="0" MaximumValue="1920" ControlToValidate="col5Width" ErrorMessage="*" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divMoveTab5" runat="server" onclick="ChooseDestinationClickHandler(buttonMoveTab5CallBack);return false;"
                        visible="false" style="cursor: pointer; width: 200px">
                        <asp:ImageButton ID="btnMoveTab5" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTab.png"
                            OnClick="btnMoveTab_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_48) %>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
    <div runat="server" id="divColumn6" class="divColumn">
         <div class="colHeader">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0">
                        <h2>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_135) %></h2>
                    </td>
                    <td style="padding: 0; text-align: right;">
                        <asp:ImageButton CssClass="imgButton" ID="ImageButton5" runat="server" ImageUrl="/Orion/images/delete_icon_white16x16.png"
                            OnCommand="btnDeleteCol_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" CommandArgument="6"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_12 %>" OnClientClick="DeleteColumn(this); return false;"/>
                    </td>
                </tr>
            </table>
        </div>
        <div class="resSpan"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_129) %></b></div>
        <table class="resList">
            <tr style="vertical-align: top">
                <td>
                    <asp:ListBox runat="server" ID="lbxColumn6" Width="200px" Height="215px" Rows="14" DataValueField="ID"
                        DataTextField="Title" SelectionMode="multiple"></asp:ListBox>
                </td>
                <td class="viewColumnButtons">
                    <asp:ImageButton ID="btnAdd6" runat="server" ImageUrl="~/orion/images/SubViewImages/Add.png"
                        OnClick="btnAdd_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_18 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_18 %>" /><br />
                    <asp:ImageButton ID="btnDelete6" runat="server" ImageUrl="~/orion/images/SubViewImages/delete_16x16.png"
                        OnClick="btnDelete_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_19 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_19 %>" /><br />
                    <asp:ImageButton ID="btnClone6" runat="server" ImageUrl="~/orion/images/SubViewImages/Copy.png"
                        OnClick="btnClone_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_20 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_20 %>" /><br />
                    <br />
                    <asp:ImageButton ID="btnMoveTop6" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTop.png"
                        OnClick="btnMoveTop_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_21 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_21 %>" /><br />
                    <asp:ImageButton ID="btnMoveUp6" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveUp.png"
                        OnClick="btnMoveUp_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_22 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_22 %>" /><br />
                    <asp:ImageButton ID="btnMoveDown6" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveDown.png"
                        OnClick="btnMoveDown_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_23 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_23 %>" /><br />
                    <asp:ImageButton ID="btnMoveBottom6" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveBottom.png"
                        OnClick="btnMoveBottom_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_24 %>"
                        ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_24 %>" /><br />
                </td>
            </tr>
            <tr>
                <td>
                <table cellpadding="0" cellspacing="0" class="wtable">
                    <tr>
                        <td>
                            <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_75) %></b></div>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="col6Width" OnTextChanged="columnWidth_TextChanged" Rows="1" Width="40" Columns="1"/>
                        </td>
                        <td>
                            <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_PX) %></div>
                             <asp:RequiredFieldValidator Display="Dynamic" ID="col6WidthRequiredFieldValidator" runat="server" ControlToValidate="col6Width" ErrorMessage="*" />                           
                            <asp:RangeValidator Display="Dynamic" ID="col6WidthRangeValidator" runat="server" Type="Integer" MinimumValue="0" MaximumValue="1920" ControlToValidate="col6Width" ErrorMessage="*" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divMoveTab6" runat="server" onclick="ChooseDestinationClickHandler(buttonMoveTab4CallBack);return false;"
                        visible="false" style="cursor: pointer; width: 200px">
                        <asp:ImageButton ID="btnMoveTab6" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveTab.png"
                            OnClick="btnMoveTab_Click" AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>"
                            ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_47 %>" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_48) %>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div runat="server" id="addColumnBox" class="addDivColumn">
        <div style="color: darkgrey; padding: 8px 0 5px 5px; border-bottom: 1px dashed darkgrey; text-align: center">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_41) %></div>
        <table style="width: 240px; height: 300px;">
            <tr style="vertical-align: central">
                <td style="text-align: center; vertical-align: middle;">
                    <orion:localizablebutton runat="server" id="addColumn" onclick="AddColumn" 
                        localizedtext="CustomText" displaytype="Primary" causesvalidation="false" text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_40 %>" />
                </td>
            </tr>
        </table>
    </div>
    </div>  

<asp:HiddenField ID="selectedViewID" runat="server" />
<asp:HiddenField ID="selectedColumn" runat="server" />
<div id="chooseDestinationTabDialog" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_55) %>"
    style="display: none;">
    <p>
        <b>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_49) %></b></p>
    <div style="background-color: White; padding: 5px; width: auto;">
        <asp:Repeater ID="rptTabs" runat="server">
            <ItemTemplate>
                    <div>
                    <div><%# DefaultSanitizer.SanitizeHtml(((ViewInfo)Container.DataItem).ViewShortTitle) %></div>
                    <table width="100%" cellpadding="5" style="padding: 5px 5px 10px 5px">
                        <tr>
                            <td>
                    <asp:RadioButton runat="server" class="jsCustomizeView-columnSelection" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_50 %>"
                        data-viewid='<%# ((ViewInfo)Container.DataItem).ViewID %>' data-columnNum='1' />
                        </td>
                        <td>
                    <asp:RadioButton runat="server" class="jsCustomizeView-columnSelection" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_51 %>"
                        data-viewid='<%# ((ViewInfo)Container.DataItem).ViewID %>' data-columnNum='2'
                        Visible='<%# ((ViewInfo)Container.DataItem).ColumnCount > 1 %>' />
                        </td>
                        <td>
                    <asp:RadioButton runat="server" class="jsCustomizeView-columnSelection" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_52 %>"
                        data-viewid='<%# ((ViewInfo)Container.DataItem).ViewID %>' data-columnNum='3'
                        Visible='<%# ((ViewInfo)Container.DataItem).ColumnCount > 2 %>' />
                        </td>
                        <td>
                    <asp:RadioButton runat="server" class="jsCustomizeView-columnSelection" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_120 %>"
                        data-viewid='<%# ((ViewInfo)Container.DataItem).ViewID %>' data-columnNum='4'
                        Visible='<%# ((ViewInfo)Container.DataItem).ColumnCount > 3 %>' />
                        </td>
                        <td>
                    <asp:RadioButton runat="server" class="jsCustomizeView-columnSelection" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_121 %>"
                        data-viewid='<%# ((ViewInfo)Container.DataItem).ViewID %>' data-columnNum='5'
                        Visible='<%# ((ViewInfo)Container.DataItem).ColumnCount > 4 %>' />
                        </td>
                        <td>
                    <asp:RadioButton runat="server" class="jsCustomizeView-columnSelection" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_122 %>"
                        data-viewid='<%# ((ViewInfo)Container.DataItem).ViewID %>' data-columnNum='6'
                        Visible='<%# ((ViewInfo)Container.DataItem).ColumnCount > 5 %>' />
                        </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="sw-btn-bar" style="white-space: nowrap; text-align: right;">
        <orion:localizablebuttonlink id="btnDialogOk" runat="server" displaytype="Primary"
            localizedtext="CustomText" text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_53 %>"
            tooltip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_54 %>" />
        <orion:localizablebuttonlink id="btnDialogCancel" runat="server" displaytype="Secondary"
            localizedtext="Cancel" tooltip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>" />
    </div>
</div>
<orion:resourcepickerloader runat="server" id="ResourcePickerDialog" oncreatedjs="ResourcePickerLoaderCreated"
    onselectedjs="SaveSelectedResources"></orion:resourcepickerloader>
<script type="text/javascript">
     var buttonMoveTab1CallBack = function(){<%= Page.ClientScript.GetPostBackEventReference(btnMoveTab, null) %>};
     var buttonMoveTab2CallBack = function(){<%= Page.ClientScript.GetPostBackEventReference(btnMoveTab2, null) %>};
     var buttonMoveTab3CallBack = function(){<%= Page.ClientScript.GetPostBackEventReference(btnMoveTab3, null) %>};
     var buttonMoveTab4CallBack = function(){<%= Page.ClientScript.GetPostBackEventReference(btnMoveTab4, null) %>};
     var buttonMoveTab5CallBack = function(){<%= Page.ClientScript.GetPostBackEventReference(btnMoveTab5, null) %>};
     var buttonMoveTab6CallBack = function(){<%= Page.ClientScript.GetPostBackEventReference(btnMoveTab6, null) %>};
        
     $(function() {
         $('.jsCustomizeView-columnSelection input').attr('name', 'MoveResource');
     });

      function DeleteColumn(el) {     
      if($(el).closest("div").parent().find('.resList select option').length>0)      
            Ext.MessageBox.show({ 
                title: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBJS_SO0_01) %>",
                msg: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBJS_SO0_02) %>", 
                buttons: { yes: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_JP1_4) %>",
                 no: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_JP1_5) %>" },
                width: 350,
                fn: function (btn) {
                    if (btn == 'yes') {
                        __doPostBack(el.name, null);
                    }
                    else {
                        return false;
                    }
                },
                icon: Ext.MessageBox.ERROR
            });
            else
             __doPostBack(el.name, null);

    }
         
     function ChooseDestinationClickHandler( serverMethodCallBack ) {
         //bind btnDialogOK
         $('#<%=btnDialogOk.ClientID %>').unbind('click').click( function () {
             var selectedCheck = $('.jsCustomizeView-columnSelection input:checked');
             if( selectedCheck.length == 0)
                 alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_57) %>");
             else
             {
                 var selectedViewId = selectedCheck.parent().data('viewid');
                 var selectedColumn = selectedCheck.parent().data('columnnum');
                 $('#<%=selectedViewID.ClientID %>').val( selectedViewId );
                 $('#<%=selectedColumn.ClientID %>').val( selectedColumn );
                 serverMethodCallBack();   
             }
         });
         //bind btnDialogCancel
         $('#<%=btnDialogCancel.ClientID %>').unbind('click').click( function () {
             $("#chooseDestinationTabDialog").dialog( "close" );
         });
         //open dialog
         $("#chooseDestinationTabDialog").dialog({
             width: 490,
             modal: true,
             open: function(event, ui) { 
                 var btnSubmit = $(".ui-dialog-buttonpane button:eq(0)");
                 btnSubmit.removeClass().focus();
             }
         });
         return false;
     }

     function ResourcePickerLoaderCreated(loader) {
        $('#' + '<% = btnAdd1.ClientID %>').click(function() {
            loader.AddResources(1);
            return false;
        });

         $('#' + '<% = btnAdd2.ClientID %>').click(function() {
            loader.AddResources(2);
            return false;
        });
        
          $('#' + '<% = btnAdd3.ClientID %>').click(function() {
            loader.AddResources(3);
            return false;
        });

         $('#' + '<% = btnAdd4.ClientID %>').click(function() {
            loader.AddResources(4);
            return false;
        });

         $('#' + '<% = btnAdd5.ClientID %>').click(function() {
            loader.AddResources(5);
            return false;
        });

         $('#' + '<% = btnAdd6.ClientID %>').click(function() {
            loader.AddResources(6);
            return false;
        });
    }

     function SaveSelectedResources(loader) {
        var viewId = "<%= View.ViewID %>",
            viewGroup = "<%= View.ViewGroup %>",
            selectedResources = loader.GetSelectedResources();
        SW.Core.Services.callWebService(SW.Core.Pickers.RESOURCE_PICKER_WEB_SERVICE_URL,
                                        'AddResourcesToView',
                                        {   resourcePaths: _.pluck(selectedResources.resources, 'path'),
                                            viewId: viewId,
                                            viewGroup: viewGroup,
                                            columnNumber: selectedResources.selectedColumn
                                        }, 
                                        function (returnViewid) {
                                            SW.Core.UriHelper.navigateWithURIParams({ ViewID: returnViewid });
                                        }, 
                                        function (errorMessage) {
                                            Ext.Msg.show({
                                                title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_11) %>',
                                                msg: errorMessage,
                                                icon: Ext.Msg.ERROR,
                                                buttons: Ext.Msg.OK
                                            });
                                    });
    };
</script>
