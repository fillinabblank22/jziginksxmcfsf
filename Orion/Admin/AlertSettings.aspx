<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlertSettings.aspx.cs" 
    Inherits="Orion_Admin_AlertSettings" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AB0_96 %>" %>    
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" TagName="SmtpSettings" Src="~/Orion/Controls/SmtpSettingsControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EmailsInput" Src="~/Orion/Controls/EmailsInput.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="AdminHeadPlaceholder">
<orion:Include runat="server" File="Admin/js/AlertSettingsController.js" />

<style type="text/css">        
    .emailPickerContainer {width: 395px;}
    .email-settings { padding-left: 20px; width: 340px; }
    .alert-settings { padding-top: 15px }
    .alert-settings-btn { text-align: right; margin-top: 15px; padding-top: 15px; border-top: solid 1px #abadb3; }    
    #alertSettingsContainer { width: 440px; } 
    .smtp-table { width: 420px; }
    .smtp-table input[data-form="smtpServerIP"] { width: 397px !important; }
    .smtp-settings-description { margin-bottom: 15px; }
    .smtp-settings-top-section { margin-top: 10px; }
    .smtp-settings-top-section span { font-weight: bold; }
    .smtp-settings-bottom-section { margin-bottom: 10px; }
    .smtp-settings-bottom-section span { font-weight: bold; }
    .smtp-settings-replyaddress { width:397px; border: solid 1px #abadb3; }    
</style>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHAlertSettings" />    
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="adminContentPlaceholder">
<h1 style="font-size: large; padding-bottom: 10px;">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_96) %>
</h1>
<div id="alertSettingsContainer">
    <div class="smtp-settings-description">
        <span>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_93) %>
        </span>
    </div>
    <div class="smtp-settings-top-section smtp-settings-bottom-section">
        <span> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_94) %> <font color="A0A0A0"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_168) %></font></span>
    </div>
    <div class="section email-settings">
        <div>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_28) %>
        </div>
        <orion:EmailsInput ID="txtEmailTo" runat="server" DataForm="emailTo" ToolTip="${DefaultEmailTo}" MultipleMode="True" />
    </div>
    <div class="section email-settings">
        <div>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_31) %>
        </div>
        <orion:EmailsInput ID="txtEmailCc" runat="server" DataForm="emailCc" ToolTip="${DefaultEmailCC}" MultipleMode="True" />
    </div>
    <div class="section email-settings">
        <div>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_32) %>
        </div>
        <orion:EmailsInput ID="txtEmailBcc" runat="server" DataForm="emailBcc" ToolTip="${DefaultEmailBCC}" MultipleMode="True" />
    </div>
    <div class="smtp-settings-top-section smtp-settings-bottom-section">
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_95) %></span>
    </div>
    <div class="section email-settings smtp-settings-bottom-section">
        <div>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_83) %>
        </div>
        <asp:TextBox runat="server" class="smtp-settings-replyaddress" ID="txtEmailSenderName" DataForm="emailSenderName" ToolTip="${DefaultEmailSenderName}"></asp:TextBox>
    </div>    
    <div class="section email-settings">
        <div>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_82) %>
        </div>
        <orion:EmailsInput ID="txtEmailFrom" runat="server" DataForm="emailFrom" ToolTip="${DefaultEmailFrom}"/>
    </div>
    <div class="smtp-settings-top-section">
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_97) %></span>
    </div>
    <div class="smtp-settings-description">
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Configure_SMTP_Applies_To_Entire_Product_Warning) %></span>
    </div>
    <div class="section email-settings" style="padding-top: 10px;">
        <asp:CheckBox runat="server" ID="cbSupportTls" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_334 %>" />
    </div>
    <orion:SmtpSettings runat="server" ID="SmtpSettings" Mode="AlertSettingsEdit" />
    <div class="alert-settings-btn">
        <orion:LocalizableButton ID="btnAlertSettingsSave" CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AB0_98 %>" ToolTip="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VM0_10 %>" OnClientClick="validateAndSave(); return false;" />
        <orion:LocalizableButton ID="btnAlertSettingsCancel"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelAlertSettings" />
    </div>
    <br/>
</div>  
<script type="text/javascript">
    $(function () {
        SW.Core.AlertSettingsController = new SW.Core.Admin.AlertSettingsController({ containerID: 'alertSettingsContainer' });
        SW.Core.AlertSettingsController.init();
    });

    function validateAndSave() {
        if (SW.Core.AlertSettingsController.validate() == true) {
            __doPostBack('SaveSettings', '');
        }
    };
</script>
</asp:Content>

