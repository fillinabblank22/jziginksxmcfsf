using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Logging;
using Resources;
using System.Text.RegularExpressions;


public partial class Orion_Admin_ViewGroupTitleEditor : BaseViewGroupControl
{
    private static readonly Log _log = new Log();
    private const string AlowedCharsPattern = @"^[-\w.() ]*$";
   
    protected override void RebuildUI()
    {
        if (this.View != null)
        {
            this.Page.Title = string.Format(CoreWebContent.WEBCODE_VB0_225, this.View.ViewTitle);
            txtViewTitle.Text = this.View.ViewGroupName;
            ViewType.Text = GetViewTypeName(this.View.ViewType);
        }
    }

    protected string GetViewTypeName(string viewType)
    {
        try
        {
            return ViewManager.GetFriendlyViewTypeName(viewType);
        }
        catch (Exception ex)
        {
            _log.WarnFormat("Unable to fing friendly view type name for '{0}'. {1}", viewType, ex);
            return viewType;
        }
    }

    protected void UpdateTitle_Click(object sender, EventArgs e)
    {
        if (CustomValidator.IsValid)
        {
           
                if (this.View.ViewGroup != 0)
                {
                    this.View.ViewGroupName = txtViewTitle.Text;
                    ViewManager.UpdateViewGroupName(this.View);
                }
                else
                {
                    this.View.ViewTitle = txtViewTitle.Text;
                    ViewManager.UpdateView(this.View);
                }
                RefreshPage();
        }
    }


    protected void TxtViewTitleValidate(object source, ServerValidateEventArgs args)
    {
        string title = SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular(txtViewTitle.Text);

        if (Regex.IsMatch(title, AlowedCharsPattern))
        {
            if (this.View.ViewGroup != 0)
            {
                args.IsValid = ViewManager.IsGroupTitleUnique(title, this.View.ViewGroup);
            }
            else
            {
                args.IsValid = ViewManager.IsTitleUnique(string.Empty, title, this.View.ViewID);
            }
            if (!args.IsValid) CustomValidator.ErrorMessage = string.Format(CoreWebContent.WEBCODE_TM0_51, title);
        }
        else
        {
            args.IsValid = false;
            txtViewTitle.Text = title;
            CustomValidator.ErrorMessage = CoreWebContent.WEBDATA_VB0_279;
        }
    }
}
