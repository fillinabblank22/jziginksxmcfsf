﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Orion.Core.Common.Interfaces.ManagePollers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Pollers_ManagePollers : Page
{
    private static readonly JavaScriptSerializer JsonSerializer = new JavaScriptSerializer();

    protected string CreateNewPollerButtons;
    protected string AvailableOperations;
    protected string ThwackUserInfo = JsonSerializer.Serialize(new { name = String.Empty, pass = String.Empty });

    protected override void OnLoad(EventArgs e)
    {
        if (ManagePollersHelper.OldDeviceStudioRedirect)
        {
            var redirectUrl = new StringBuilder("/Orion/DeviceStudio/Admin/ManageDevicePollers.aspx");
            if (Request.QueryString.Count > 0)
            {
                redirectUrl.AppendFormat("?{0}", string.Join("&", Request.QueryString
                    .ToEnumerable()
                    .Select(qs => string.Format("{0}={1}", qs.Key, qs.Value))));
            }
            Response.Redirect(redirectUrl.ToString());
        }

        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IEnumerable<IManagePollersPlugin> plugins = new[]
        {
            ManagePollersHelper.NativePollersPlugin
        };
        plugins = plugins.Concat(OrionModuleManager.GetManagePollersPlugins());
        var toolbarPlugins = plugins.OfType<IManagePollersToolbarPlugin>().ToList();

        var createNewButtons = toolbarPlugins.SelectMany(p => p.CreateNewPollerButtons);
        var availableOps = toolbarPlugins.SelectMany(p => p.SupportedActions).Distinct().OrderBy(p => (int)p).Select(p => p.ToString());
        
        CreateNewPollerButtons = JsonSerializer.Serialize(createNewButtons);
        AvailableOperations = JsonSerializer.Serialize(availableOps);

        var nCred = Session["ThwackCredential"] as NetworkCredential;
        if (nCred != null)
        {
            var uiCred = new
            {
                name = nCred.UserName,
                pass = nCred.Password
            };
            ThwackUserInfo = JsonSerializer.Serialize(uiCred);
        }
    }
}