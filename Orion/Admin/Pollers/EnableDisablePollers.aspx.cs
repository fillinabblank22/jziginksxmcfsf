﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Interfaces.ManagePollers;
using SolarWinds.Orion.Core.Common.Models.ManagePollers;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Pollers_EnableDisablePollers : Page
{
    private static readonly JavaScriptSerializer JsonSerializer = new JavaScriptSerializer();

    private static readonly Log log = new Log();
    protected string HintMessage;
    protected string PollerId;
    protected string PollerName;
    protected string TechnologyName;
    protected string ToolbarButtons;
    protected string ScanSupported;

    protected override void OnLoad(EventArgs e)
    {
        if (ManagePollersHelper.OldDeviceStudioRedirect)
        {
            var redirectUrl = new StringBuilder("/Orion/DeviceStudio/Admin/EnableDisablePollers.aspx");
            if (Request.QueryString.Count > 0)
            {
                redirectUrl.AppendFormat("?{0}", string.Join("&", Request.QueryString
                    .ToEnumerable()
                    .Select(qs => string.Format("{0}={1}", qs.Key, qs.Value))));
            }
            Response.Redirect(redirectUrl.ToString());
        }
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PollerName = null;
        TechnologyName = null;
        
        var targetEntity = string.Empty;
        var pollerId = Request["poller"];
        var scanSupported = false;
        var toolbarButtons = new AssignmentsToolbarButton[0] as IEnumerable<AssignmentsToolbarButton>;
        if (!string.IsNullOrEmpty(pollerId))
        {
            var result = new TechnologyDAL().GetPollerDetails(pollerId);
            if (result != null && result.Rows.Count == 1)
            {
                PollerName = result.Rows[0]["PollerName"].ToString();
                TechnologyName = result.Rows[0]["TechnologyName"].ToString();
                targetEntity = result.Rows[0]["TargetEntity"].ToString();
            }
        }

        if (!string.IsNullOrWhiteSpace(PollerName))
        {
            PollerId = pollerId;
            Title = string.Format(CoreWebContent.WEBDATA_PS1_7, PollerName, EntityHelper.GetEntityNamePlural(targetEntity) ?? targetEntity);
            HintMessageBox.Visible = true;

            var plugin = ManagePollersHelper.GetManagePollersPluginWithType<IAssignmentsPagePlugin>(pollerId);
            if (plugin != null)
            {
                toolbarButtons = plugin.AssignmentsToolbarButtons;
                scanSupported = plugin.PollersScanSupported;
            }
        }
        else
        {
            log.ErrorFormat("Invalid poller with id='{0}' was supplied to enable/disable (assign) poller to nodes page. The poller id was missing, invalid or for particular id there exists no poller.", pollerId ?? string.Empty);

            Title = CoreWebContent.WEBDATA_PS1_9;
            HintMessage = CoreWebContent.WEBDATA_PS1_8;
            HintMessageBox.Visible = true;
            MainContent.Visible = false;
        }

        ScanSupported = scanSupported.ToString().ToLower();
        ToolbarButtons = JsonSerializer.Serialize(toolbarButtons);
    }
}