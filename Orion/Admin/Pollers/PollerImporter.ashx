﻿<%@ WebHandler Language="C#" Class="PollerImporter" %>

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Resources;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Core.Common.Interfaces.ManagePollers;
using SolarWinds.Orion.Web.Helpers;

public class PollerImporter : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count == 1)
        {
            var suitablePlugins = ManagePollersHelper.GetManagePollersPluginsWithType<IManagePollersToolbarPlugin>()
                .Where(p => p.SupportedActions.Contains(ToolbarButtonEnum.Import))
                .ToArray();

            if (suitablePlugins.Length == 0)
            {
                BuildSerializedJsonResponse(context, false, CoreWebContent.WEBCODE_PS1_2);
                return;
            }

            byte[] fileBytes;
            var buffer = new byte[1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = context.Request.Files[0].InputStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                fileBytes = ms.ToArray();
            }

            var importParameters = new Dictionary<string, object>
            {
                {"justCheck", true},
                {"fileContent", fileBytes},
                {"fileName", context.Request.Files[0].FileName}
            };

            foreach (var suitablePlugin in suitablePlugins)
            {
                string redirectUrl;
                var result = suitablePlugin.PerformAction(ToolbarButtonEnum.Import, null, importParameters, out redirectUrl);
                if (result)
                {
                    BuildSerializedJsonResponse(context, true, redirectUrl);
                    return;
                }
            }

            BuildSerializedJsonResponse(context, false, CoreWebContent.WEBCODE_PS1_3);
        }
        else
        {
            BuildSerializedJsonResponse(context, false, CoreWebContent.WEBCODE_PS1_1);
        }
    }

    public bool IsReusable { get { return false; } }

    private static void BuildSerializedJsonResponse(HttpContext context, bool successP, string messageP)
    {
        var response = new { success = successP, message = messageP };
        var serialized = new JavaScriptSerializer().Serialize(response);
        context.Response.ContentType = "text/html";
        context.Response.StatusCode = 200;
        context.Response.Write(serialized);
    }
}