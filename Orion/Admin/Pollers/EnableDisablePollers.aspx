<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" 
    CodeFile="EnableDisablePollers.aspx.cs" Inherits="Orion_Admin_Pollers_EnableDisablePollers" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common"%>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include2" runat="server" File="Admin/js/SearchField.js" />
    <orion:Include ID="Include3" runat="server" File="Admin/js/EnableDisablePollers.js" />
    <orion:Include ID="Include4" runat="server" File="EnableDisablePollers.css" />
    <orion:Include ID="Include5" runat="server" File="ManagePollers.css" />
    <script type="text/javascript">
        SW.Core.namespace("SW.Core.EnableDisablePollers").ToolbarButtons = <%= ToolbarButtons %>;
        SW.Core.namespace("SW.Core.EnableDisablePollers").ScanSupported = <%= ScanSupported %>;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <br />
    <div class="tableWidth">
        <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
        <div class="subtitle"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PS1_3) %></div>

        <div runat="server" id="HintMessageBox" class="hintMessage">
            <%= DefaultSanitizer.SanitizeHtml(HintMessage) %>
        </div>

        <div id="MainContent" runat="server">
            <select id="groupByProperty" style="display:none">
                <option value=""><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBCODE_AK0_70) %></option>
                <option value="n.Vendor"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBCODE_VB0_124) %></option>
                <option value="n.MachineType"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_16) %></option>
                <option value="n.ObjectSubType"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_BV0_0047) %></option>
                <option value="n.SNMPVersion"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBCODE_VB0_340) %></option>
                <option value="n.Status"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_14) %></option>
                <option value="n.Location"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_128) %></option>
                <option value="n.Contact"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_129) %></option>
                <option value="n.Community"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBCODE_VB0_190) %></option>
                <option value="n.RWCommunity"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBCODE_VB0_339) %></option>
                <option value="n.IPAddressType"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_AK0_352) %></option>
                <option value="PollerStatus"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PS1_4) %></option>
                <option value="ScanStatus"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PS1_5) %></option>
                <% foreach (string prop in CustomPropertyMgr.GetGroupingPropNamesForTable("Nodes", false))
                  { %>
                <option value="n.CustomProperties.[<%= DefaultSanitizer.SanitizeHtml(prop) %>]" propertytype="<%= DefaultSanitizer.SanitizeHtml(CustomPropertyMgr.GetTypeForProp("NodesCustomProperties", prop)) %>"><%= DefaultSanitizer.SanitizeHtml(prop) %></option>
                <%}%>
            </select>

            <div id="StatusBar" class="scan-status"></div>

            <div id="tabPanel" class="tab-top">
                
                <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2">
                            <input type="hidden" name="EnableDisablePollers_HelpLang" id="EnableDisablePollers_HelpLang" value='<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.CurrentHelpLanguage) %>' />
                            <input type="hidden" name="EnableDisablePollers_PollerId" id="EnableDisablePollers_PollerId" value='<%= DefaultSanitizer.SanitizeHtml(PollerId) %>' />
                            <input type="hidden" name="EnableDisablePollers_PollerName" id="EnableDisablePollers_PollerName" value='<%= DefaultSanitizer.SanitizeHtml(PollerName ?? string.Empty) %>' />
                            <input type="hidden" name="EnableDisablePollers_TechnologyName" id="EnableDisablePollers_TechnologyName" value='<%= DefaultSanitizer.SanitizeHtml(TechnologyName ?? string.Empty) %>' />
                            <input type="hidden" name="EnableDisablePollers_PageSize" id="EnableDisablePollers_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("EnableDisablePollers_PageSize")) %>' />
                            <input type="hidden" name="EnableDisablePollers_AllowAdmin" id="EnableDisablePollers_AllowAdmin" value='<%= this.Profile.AllowAdmin %>' />
                            <input type="hidden" name="EnableDisablePollers_ReturnUrl" id="EnableDisablePollers_ReturnUrl" value='<%= DefaultSanitizer.SanitizeHtml(ReferrerRedirectorBase.GetReturnUrl()) %>'/>

                            <div id="Grid" class="sw-is-locale-<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.CurrentHelpLanguage) %>"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <span class="managePollers"><a href="ManagePollers.aspx"> <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PS1_6) %></a></span>
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionCorePHDeviceStudioEnableDisablePollers" />
</asp:Content>
