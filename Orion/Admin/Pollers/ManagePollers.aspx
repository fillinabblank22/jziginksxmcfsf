<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" 
    CodeFile="ManagePollers.aspx.cs" Inherits="Orion_Admin_Pollers_ManagePollers" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS1_2 %>"%>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="orion" TagName="TopPageItems" Src="~/Orion/Controls/ManagePollersTopPageControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include2" runat="server" File="Admin/js/ManagePollers.js" />
    <orion:Include ID="Include3" runat="server" File="Admin/js/SearchField.js" />
    <orion:Include ID="Include5" runat="server" File="Admin/js/FileUploadField.js" />
    <orion:Include ID="Include4" runat="server" File="ManagePollers.css" />
    <orion:Include ID="Include6" runat="server" File="Admin/js/ThwackCommon.js" />
    <script type="text/javascript">
        SW.Core.namespace("SW.Core.ManagePollers").CreateNewButtons = <%= CreateNewPollerButtons %>;
        SW.Core.namespace("SW.Core.ManagePollers").AvailableOperations = <%= AvailableOperations %>;
        var thwackUserInfo = <%= ThwackUserInfo  %>;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <br />
    <div class="tableWidth" id="mainContent">
        <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
        <!-- place for top page items -->
        <orion:TopPageItems runat="server" />

        <div id="tabPanel" class="tab-top">
            <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="ManagePollers_PageSize" id="ManagePollers_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("ManagePollers_PageSize")) %>' />
                        <input type="hidden" name="ManagePollers_AllowAdmin" id="ManagePollers_AllowAdmin" value='<%= this.Profile.AllowAdmin %>' />
                        <input type="hidden" name="ManagePollers_SelectedColumns" id="ManagePollers_SelectedColumns" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ManagePollers_SelectedColumns", ManagePollersHelper.DefaultGridColumns)) %>' />
                        <div id="Grid"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <%if (OrionConfiguration.IsDemoServer || !Profile.AllowNodeManagement) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionCorePHDeviceStudioManagingPollers" />
</asp:Content>