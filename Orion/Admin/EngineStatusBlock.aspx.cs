﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Admin.Engines;
using System.Web.UI;

public partial class Orion_Admin_EngineStatusBlock : Page
{
    private static readonly Log _log = new Log();
    private static volatile IEnumerable<EngineInfoPlugin> _plugins;
    private static readonly object _mutex = new object();
   

    public bool HasEngineID
    {
        get
        {
            int tempValue;
            return Request["EngineID"] != null && int.TryParse(Request["EngineID"], out tempValue);
        }
    }

    public int EngineID
    {
        get
        {
            return Int32.Parse(Request["EngineID"]);
        }
    }

    public static IEnumerable<EngineInfoPlugin> Plugins
    {
        get
        {
            if(_plugins == null)
            {
                lock (_mutex)
                {
                    if (_plugins == null)
                    {
                        _plugins = LoadPluginsFromModulesConfigFiles();
                    }
                }
            }
            return _plugins;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (HasEngineID)
        {
            try
            {
                LoadEngineInfoPluginsControls();
            }
            catch (Exception ex)
            {
                _log.Error(string.Format("Error loading engine info plugins for EngineID {0}.", EngineID), ex);
            }
        }
    }


    private void LoadEngineInfoPluginsControls()
    {
        engineInfoPlugins.Controls.Clear();

        foreach (var plugin in Plugins.Where(info => info.Visible).OrderBy(info => info.Order))
        {
            try
            {
                var engineInfoPluginControl = LoadControl(plugin.UserControlPath) as EngineInfoBaseControl;

                if (engineInfoPluginControl != null)
                {
                    engineInfoPluginControl.EngineID = EngineID;
                    engineInfoPlugins.Controls.Add(engineInfoPluginControl);
                }
                else
                {
                    _log.DebugFormat("Engine info plugin control {0} does not inherit from base usercontrol class: EngineInfoBaseControl", plugin.UserControlPath);
                }

            }
            catch (Exception ex)
            {
                _log.ErrorFormat("Failed to load plugin control for engine info plugin {0} from path {1}. Details: {2}",
                    plugin.Name, plugin.UserControlPath, ex.ToString());
            }
        }
    }

    private static IEnumerable<EngineInfoPlugin> LoadPluginsFromModulesConfigFiles()
    {
        var plugins = new List<EngineInfoPlugin>();

        foreach (OrionModule module in OrionModuleManager.GetModules())
        {
            var cfgFile = GetConfigPath(module);
            var moduleOrFilename = module.Name;
            var currentPlugins = GetPluginsFromXmlFile(cfgFile, moduleOrFilename);

            plugins.AddRange(currentPlugins);
        }

        // adding support for packages
        plugins.AddRange(OrionModuleManager.GetIndependentEnginePlugins());

        return plugins;
    }

    private static IEnumerable<EngineInfoPlugin> GetPluginsFromXmlFile(string cfgFile, string moduleOrFilename)
    {
        var currentPlugins = new List<EngineInfoPlugin>();

        /* we expect xml format like this:
            * <engineInfoPlugins>                
            *    <engineInfoPlugin 
            *      name="some_unique_name"
             *     controlPath="/Orion/Module/Controls/PluginControl.ascx"
             *     visible="true"
             *     order="int_value" > 
             *   </engineInfoPlugin>
            * </engineInfoPlugins>
            */
        var xmlDoc = new XmlDocument();
        TokenSubstitution.LoadXml(xmlDoc, cfgFile);

        foreach (XmlNode node in xmlDoc.DocumentElement.SelectNodes("engineInfoPlugins/engineInfoPlugin"))
        {
            try
            {
                var engineInfoPlugin = new EngineInfoPlugin();

                engineInfoPlugin.Name = node.Attributes["name"].Value;
                engineInfoPlugin.UserControlPath = node.Attributes["controlPath"].Value;
                engineInfoPlugin.Visible = node.Attributes["visible"] == null ? true : bool.Parse(node.Attributes["visible"].Value);
                engineInfoPlugin.Order = node.Attributes["order"] == null ? Int32.MaxValue : Int32.Parse(node.Attributes["order"].Value);

                currentPlugins.Add(engineInfoPlugin);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("Failed to load engine info plugin for module {0}. Details: {1}", moduleOrFilename, ex.ToString());
            }
        }
        return currentPlugins;
    }

    private static string GetConfigPath(OrionModule module)
    {
        return string.Format("{0}\\{1}\\{2}.config", HttpContext.Current.Server.MapPath("~/Orion/"), module.Name, module.Name);
    }

}
