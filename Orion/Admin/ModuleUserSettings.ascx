<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModuleUserSettings.ascx.cs" Inherits="Orion_Admin_ModuleUserSettings" %>

<asp:Repeater runat="server" ID="rptModules">
    <HeaderTemplate><table width="100%" cellpadding="0" cellspacing="0"></HeaderTemplate>
    <FooterTemplate></table></FooterTemplate>
    <ItemTemplate>
        <tr class="section-containerChbx">
            <td style="vertical-align: text-top; width: 13px;">
                <input type="checkbox" runat="server" id="chbxModuleSection" class="section-parentChbx" onclick="togleRowState(this);" />
            </td>
            <td>
                <orion:CollapsePanel runat="server" DataSource='<%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>' Collapsed="true" >
                    <TitleTemplate>
                        <h2 style="display: inline; ">&nbsp;<%# DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_VB1_65, Eval("Title"))) %></h2>
                    </TitleTemplate>
                    <BlockTemplate>
                        <table class="accountDetails">
                            <asp:Repeater runat="server" 
                                          DataSource='<%# DefaultSanitizer.SanitizeHtml(OrionModuleManager.GetModuleUserPropertyInfo(Eval("Name").ToString())) %>'>
                                <ItemTemplate>
                                    <tr>
                                        <td><input type="checkbox" runat="server" id="chbxModuleSetting" class="section-childChbx" onclick="togleRowState(this);" /></td>
                                        <td><%# DefaultSanitizer.SanitizeHtml(Eval("FriendlyName")) %></td>
                                        <td><asp:PlaceHolder runat="server" OnDataBinding="PropControlPlaceHolder_DataBind" /> </td>
                                        <td><%# DefaultSanitizer.SanitizeHtml(Eval("Description")) %></td>
                                    </tr>
                            
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>

                    </BlockTemplate>
                </orion:CollapsePanel>
            </td>
        </tr>    
    </ItemTemplate>
</asp:Repeater>