<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_33 %>" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="DiscoveryCentral.aspx.cs" Inherits="Orion_Admin_DiscoveryCentral" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
<script type="text/javascript" language="javascript">
    $(document).ready(function () {        
        if (!$(".highlightButton,.sw-btn-primary","#adminContent").length){
            $('#<%= orionHomeLink.ClientID %>').addClass('sw-btn-primary');
        }      
    });  
</script>
<style type="text/css">
    #adminContent { padding: 10px 10px 10px 20px;}
    .sw-pg-iconbox-discovery { background: url(/Orion/images/scan.svg) center left no-repeat; padding-left: 41px; }
    .sw-hdr-title-iconbox { margin-bottom: 25px;}   
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionPHDiscoveryCentral" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
<div id="discoveryCentral">
    
    <div class="sw-hdr-title-iconbox sw-pg-iconbox-discovery">
         <h3 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h3>
         <div class="sw-hdr-subtitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_35) %></div>
    </div>
    
    <div style="box-shadow: 0 0 25px rgba(0, 0, 0, 0.25); padding: 15px;">
    <div style="background-color: #dfe0e1;">
        <div runat="server" id="pluginContainer" style="border: 1px solid #DBDCD7;">
        </div>
    </div>
    </div>

    <div class="sw-btn-bar" style="padding-left: 75px;">
        <orion:LocalizableButton runat="server" ID="orionHomeLink" PostBackUrl="~/Orion/Default.aspx" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_34 %>" />
    </div>
</div>    
</asp:Content>
