using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using Limitation = SolarWinds.Orion.Web.Limitation;

public partial class Orion_Admin_AccountList : System.Web.UI.Page
{
    protected const string trueImg = "/Orion/images/Check.Green.gif";
    protected const string falseImg = "/Orion/images/Check.None.gif";
    protected const string lockedImg = "/Orion/images/Check.Lock.gif";

    private const string AccountPermissionsAllowNodeManagement = "AllowNodeManagement";
    private const string AccountPermissionsToolsetIntegration = "ToolsetIntegration";
    
    private static ITabManager manager = new TabManager();
    private static HashSet<String> _hiddenAccountPermissions = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.AccountsHidePermission)).Select(c => c.Key));

    protected override void OnInit(EventArgs e)
    {
        ProfileInfoCollection pInfoList = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All);
        List<ProfileCommon> profiles = new List<ProfileCommon>();
        foreach (ProfileInfo info in pInfoList)
        {
            profiles.Add(Profile.GetProfile(info.UserName));
        }

        AccountWorkflowManager.CurrentStep = AccountWorkflowManager.AddAccountStep.SelectType;
        this.AccountTable.Controls.Add(this.CreateAccountTable(profiles));

        base.OnInit(e);
    }

    private HtmlTable CreateAccountTable(List<ProfileCommon> profiles)
    {
        HtmlTable table = new HtmlTable();
        table.CellSpacing = 0;
        table.Attributes["class"] = "AccountList";
        // create the rows
        HtmlTableRow headerRow = new HtmlTableRow();
        HtmlTableRow enabledRow = new HtmlTableRow();
        HtmlTableRow expirationRow = new HtmlTableRow();
        HtmlTableRow adminRightsRow = new HtmlTableRow();


        HtmlTableRow manageAlertsRightsRow = new HtmlTableRow();
        HtmlTableRow allowDisableAlertRow = new HtmlTableRow();
        HtmlTableRow allowDisableAllActionsRow = new HtmlTableRow();
        HtmlTableRow allowDisableActionRow = new HtmlTableRow();
        HtmlTableRow allowNodeMgmtRow = new HtmlTableRow();
        HtmlTableRow allowMapMgmtRow = new HtmlTableRow();
        HtmlTableRow allowOrionMapsManagementRow = new HtmlTableRow();
        HtmlTableRow allowUploadImagesToOrionMapsRow = new HtmlTableRow();
        HtmlTableRow unmanageRightsRow = new HtmlTableRow();
        HtmlTableRow manageReportsRightsRow = new HtmlTableRow();

        HtmlTableRow allowViewCustRow = new HtmlTableRow();
        HtmlTableRow allowManageDashboardsRow = new HtmlTableRow();
        HtmlTableRow allowEventClrRow = new HtmlTableRow();
        HtmlTableRow toolsetIntRow = new HtmlTableRow();
        HtmlTableRow lastLoginRow = new HtmlTableRow();
        HtmlTableRow accountLimitRow = new HtmlTableRow();
        HtmlTableRow defaultNetworkDeviceRow = new HtmlTableRow();
        HtmlTableRow reportFolderRow = new HtmlTableRow();
        HtmlTableRow alertCategoryRow = new HtmlTableRow();
        var tabRows = new List<HtmlTableRow>();
        // add the rows to the table
        table.Rows.Add(headerRow);
        table.Rows.Add(enabledRow);
        table.Rows.Add(expirationRow);
        table.Rows.Add(adminRightsRow);
        table.Rows.Add(manageAlertsRightsRow);
        table.Rows.Add(allowDisableAlertRow);
        table.Rows.Add(allowDisableAllActionsRow);
        table.Rows.Add(allowDisableActionRow);
        if (!_hiddenAccountPermissions.Contains(AccountPermissionsAllowNodeManagement))
        {
            table.Rows.Add(allowNodeMgmtRow);
        }
        table.Rows.Add(allowMapMgmtRow);
        table.Rows.Add(allowOrionMapsManagementRow);
        table.Rows.Add(allowUploadImagesToOrionMapsRow);
        table.Rows.Add(allowViewCustRow);
        table.Rows.Add(allowManageDashboardsRow);
        table.Rows.Add(unmanageRightsRow);
        table.Rows.Add(manageReportsRightsRow);
        table.Rows.Add(allowEventClrRow);
        if (!_hiddenAccountPermissions.Contains(AccountPermissionsToolsetIntegration))
        {
            table.Rows.Add(toolsetIntRow);
        }
        table.Rows.Add(lastLoginRow);
        table.Rows.Add(accountLimitRow);
        table.Rows.Add(defaultNetworkDeviceRow);

        foreach (var tab in manager.GetAllTabs())
        {
            var tabRow = new HtmlTableRow {ID = tab.TabId.ToString()};
            tabRows.Add(tabRow);
            AddHeader(tabRow, string.Format(Resources.CoreWebContent.WEBCODE_TM0_63, GetLocalizedProperty("NavigationTab", tab.TabName)));
            table.Rows.Add(tabRow);
        }

        table.Rows.Add(reportFolderRow);
        table.Rows.Add(alertCategoryRow);
        // add the first column - headers
        AddHeader(headerRow, Resources.CoreWebContent.WEBCODE_TM0_64);
        AddHeader(enabledRow, Resources.CoreWebContent.WEBCODE_TM0_65);
        AddHeader(expirationRow, Resources.CoreWebContent.WEBCODE_TM0_66);
        AddHeader(adminRightsRow, Resources.CoreWebContent.WEBCODE_TM0_67);
        AddHeader(allowNodeMgmtRow, Resources.CoreWebContent.WEBCODE_TM0_68);
        AddHeader(allowMapMgmtRow, Resources.CoreWebContent.WEBCODE_MG0_1);
        AddHeader(allowOrionMapsManagementRow, Resources.CoreWebContent.AllowOrionMapsManagement);
        AddHeader(allowUploadImagesToOrionMapsRow, Resources.CoreWebContent.AllowUploadImagesToOrionMaps);
        AddHeader(allowViewCustRow, Resources.CoreWebContent.WEBCODE_TM0_69);
        AddHeader(allowManageDashboardsRow, Resources.CoreWebContent.AllowDashboardManagement);
        AddHeader(manageReportsRightsRow, Resources.CoreWebContent.WEBDATA_SO0_54);
        AddHeader(manageAlertsRightsRow, Resources.CoreWebContent.WEBDATA_SO0_185);
        AddHeader(unmanageRightsRow, Resources.CoreWebContent.WEBDATA_ED0_1);
        AddHeader(allowDisableActionRow, Resources.CoreWebContent.WEBDATA_JV0_1);
        AddHeader(allowDisableAlertRow, Resources.CoreWebContent.WEBDATA_JV0_3);
        AddHeader(allowDisableAllActionsRow, Resources.CoreWebContent.WEBDATA_JV0_5);
        AddHeader(allowEventClrRow, Resources.CoreWebContent.WEBCODE_TM0_70);
        AddHeader(toolsetIntRow, Resources.CoreWebContent.WEBCODE_TM0_71);
        AddHeader(lastLoginRow, Resources.CoreWebContent.WEBCODE_TM0_72);
        AddHeader(accountLimitRow, Resources.CoreWebContent.WEBCODE_TM0_73);
        AddHeader(defaultNetworkDeviceRow, Resources.CoreWebContent.WEBCODE_TM0_74);

        AddHeader(reportFolderRow, Resources.CoreWebContent.WEBCODE_TM0_75);
        AddHeader(alertCategoryRow, Resources.CoreWebContent.AccountList_AlertLimitationCategory_Header);
        // add a column for each user
        foreach (ProfileCommon profile in profiles)
        {
            if (profile.AccountType == (int) GroupAccountType.WindowsVirtual)
            {
                continue;
            }
            HtmlAnchor link = new HtmlAnchor();
            link.InnerText = profile.UserName;
            link.Attributes["class"] = "AccountLink";
            link.HRef = String.Format("/Orion/Admin/Accounts/EditAccount.aspx?AccountID={0}&ReturnTo={1}&AccountType=Edit",
                Server.UrlEncode(profile.UserName), ReferrerRedirectorBase.GetReturnUrl());
            AddCell(headerRow, link);
            AddImageButton(enabledRow, "AccountEnabled", profile.UserName, profile.AccountEnabled);
            AddCell(expirationRow, profile.Expires > DateTime.Now.AddYears(50) ? Resources.CoreWebContent.WEBCODE_VB0_242 : profile.Expires.ToString());

            AddImageButton(adminRightsRow, "AllowAdmin", profile.UserName, profile.AllowAdmin);

            AddImageButtonWithHierarchy(allowNodeMgmtRow, profile, "AllowNodeManagement");
            AddImageButtonWithHierarchy(manageAlertsRightsRow, profile, "AllowAlertManagement");
            AddImageButtonWithHierarchy(unmanageRightsRow, profile, "AllowUnmanage");
            AddImageButtonWithHierarchy(allowDisableActionRow, profile, "AllowDisableAction");
            AddImageButtonWithHierarchy(allowDisableAlertRow, profile, "AllowDisableAlert");
            AddImageButtonWithHierarchy(allowDisableAllActionsRow, profile, "AllowDisableAllActions");
            AddImageButtonWithHierarchy(allowViewCustRow, profile, "AllowCustomize");
            AddImageButtonWithHierarchy(allowManageDashboardsRow, profile, "AllowManageDashboards");

            AddImageButton(allowMapMgmtRow, "AllowMapManagement", profile.UserName, profile.AllowMapManagement);
            AddImageButton(allowOrionMapsManagementRow, "AllowOrionMapsManagement", profile.UserName, profile.AllowOrionMapsManagement);
            AddImageButton(allowUploadImagesToOrionMapsRow, "AllowUploadImagesToOrionMaps", profile.UserName, profile.AllowUploadImagesToOrionMaps);
            AddImageButton(manageReportsRightsRow, "AllowReportManagement", profile.UserName, profile.AllowReportManagement);
            AddImageButton(allowEventClrRow, "AllowEventClear", profile.UserName, profile.AllowEventClear);
            AddImageButton(toolsetIntRow, "ToolsetIntegration", profile.UserName, profile.ToolsetIntegration);

            AddCell(lastLoginRow, profile.LastLogin < DateTime.Now.AddYears(-50) ? Resources.CoreWebContent.WEBCODE_VB0_242 : FormatDate(profile.LastLogin));
            StringBuilder limitations = new StringBuilder();
            foreach (int i in new int[] {profile.LimitationID1, profile.LimitationID2, profile.LimitationID3})
            {
                try
                {
                    Limitation lim = Limitation.GetLimitationByID(i);
                    if (lim != null)
                    {
                        if (limitations.Length > 0)
                        {
                            limitations.Append("<br />");
                        }
                        limitations.Append(lim.Type.Name.Replace(" ", "&nbsp;"));
                    }
                }
                catch
                {
                    // do nothing
                }
            }
            AddCell(accountLimitRow, limitations.ToString());

            string defaultNetObject = profile.DefaultNetObject;
            if (profile.DefaultNetObject == null || profile.DefaultNetObject.Replace(" ", "").Contains("notselected"))
            {
                defaultNetObject = Resources.CoreWebContent.WEBCODE_TM0_77;
            }
            AddCell(defaultNetworkDeviceRow, defaultNetObject);

            foreach (var tabRow in tabRows)
            {
                var barName = manager.GetCurrentBarName(profile.UserName, Convert.ToInt32(tabRow.ID));
                AddCell(tabRow, (string.IsNullOrEmpty(barName)) ? Resources.CoreWebContent.WEBCODE_SO0_9 : string.Format(Resources.CoreWebContent.WEBCODE_TM0_76, barName));
            }

            string folder = profile.ReportFolder ?? "";
            if (string.IsNullOrEmpty(folder) || folder.Replace(" ", "").Contains("NoReports"))
            {
                folder = Resources.CoreWebContent.WEBCODE_TM0_78;
            }
            else if (folder.Equals(CoreConstants.ReportDefaultFolder, StringComparison.OrdinalIgnoreCase))
            {
                folder = Resources.CoreWebContent.WEBCODE_TM0_116;
            }
            else if (folder.Equals(CoreConstants.ReportNoLimitationCategory, StringComparison.OrdinalIgnoreCase))
            {
                folder = Resources.CoreWebContent.EditAccount_AlertLimitation_NoLimitation;
            }

            AddCell(reportFolderRow, folder);

            AddCell(alertCategoryRow, string.IsNullOrEmpty(profile.AlertCategory) ? Resources.CoreWebContent.AccountList_AlertCategoryLimitation_NoLimitation : profile.AlertCategory);
        }
        return table;
    }

    private static void AddCell(HtmlTableRow row, string contents)
    {
        if (String.IsNullOrEmpty(contents))
        {
            contents = "&nbsp;";
        }
        AddCell(row, new LiteralControl(contents));
    }

    private static void AddCell(HtmlTableRow row, Control contents)
    {
        HtmlTableCell cell = new HtmlTableCell();
        cell.Controls.Add(contents);
        row.Cells.Add(cell);
    }

    private static void AddHeader(HtmlTableRow row, string contents)
    {
        HtmlTableCell cell = new HtmlTableCell();
        cell.Attributes["class"] = "header";
        cell.Controls.Add(new LiteralControl(contents));
        row.Cells.Add(cell);
    }

    private static string FormatDate(DateTime date)
    {
        return date.ToString("d") + "<br />" + date.ToString("t");
    }

    private void AddImageButton(HtmlTableRow row, string property, string userName, bool value)
    {
        var button = new ImageButton();
        button.ImageUrl = value ? trueImg : falseImg;
        button.CommandName = property;
        button.CommandArgument = userName;
        button.Click += ToggleButton_Click;
        AddCell(row, button);
    }

    
    private string GetHigherLevelPropertyName(string propertyName)
    {
        switch (propertyName)
        {
            case "AllowNodeManagement":
            case "AllowAlertManagement":
                return "AllowAdmin";
            case "AllowDisableAllActions":
            case "AllowDisableAlert":
                return "AllowAlertManagement";
            case "AllowUnmanage":
                return "AllowNodeManagement";
            case "AllowDisableAction":
                return "AllowDisableAllActions";
            case "AllowManageDashboards":
                return "AllowCustomize";

            default:
                return "AllowAdmin";
        }
    }

    private string GetHigherLevelPropertyDescription(string propertyName)
    {
        switch (propertyName)
        {
            case "AllowNodeManagement":
            case "AllowAlertManagement":
            case "AllowCustomize":
                return Resources.CoreWebContent.WEBCODE_TM0_67;
            case "AllowDisableAllActions":
            case "AllowDisableAlert":
                return Resources.CoreWebContent.WEBDATA_SO0_185;
            case "AllowUnmanage":
                return Resources.CoreWebContent.WEBCODE_TM0_68;
            case "AllowDisableAction":
                return Resources.CoreWebContent.WEBDATA_JV0_5;
            case "AllowManageDashboards":
                return Resources.CoreWebContent.WEBCODE_TM0_69;

            default:
                return string.Empty;
        }
    }

    private void AddImageButtonWithHierarchy(HtmlTableRow row, ProfileCommon profile, string propertyName)
    {
        var button = new ImageButton();

        var propertyValue = (bool)profile.GetPropertyValue(propertyName);
        string higherLevelPropertyName = GetHigherLevelPropertyName(propertyName);
        var higherLevelPropertyValue = (bool)profile.GetPropertyValue(higherLevelPropertyName);
        string higherLevelPropertyDescription = GetHigherLevelPropertyDescription(propertyName);

        if (higherLevelPropertyValue)
        {
            if (!propertyValue)
            {
                // This can happen if user upgrade to new version of Orion and on his
                // older version was no logic which automatically checked related properties
                ChangePropertyValue(profile, propertyName, true);
            }

            BlockButton(button, string.Format(Resources.CoreWebContent.WEBDATA_JV0_7, higherLevelPropertyDescription));
        }
        else
        {
            button.ImageUrl = propertyValue ? trueImg : falseImg;
        }

        button.CommandName = propertyName;
        button.CommandArgument = profile.UserName;
        button.Click += ToggleButton_Click;
        AddCell(row, button);
    }

    private void BlockButton(ImageButton button, string toolTipWithHigherLevelAccountRight)
    {
        button.ImageUrl = lockedImg;
        button.ToolTip = toolTipWithHigherLevelAccountRight;
        button.OnClientClick = "return false;";
        button.Style.Add("cursor", "default");
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    protected void ToggleButton_Click(object sender, EventArgs e)
    {
        ImageButton button = (ImageButton) sender;
        string propertyName = button.CommandName;
        string username = button.CommandArgument;
        ProfileCommon thisProfile = Profile.GetProfile(username);
        bool allowed = button.ImageUrl.Equals(trueImg);

        ChangePropertyValue(thisProfile, propertyName, !allowed);

        button.ImageUrl = allowed ? falseImg : trueImg;
        Response.Redirect(Request.Url.AbsolutePath, true);
    }

    private void ChangePropertyValue(ProfileCommon profile, string propertyName, bool allow)
    {
        profile.SetPropertyValue(propertyName, allow);
        ProcessAccountRightsHierarchy(propertyName, profile);
        profile.Save();

        // Added according to CORE-3866
        if (propertyName.Equals("AccountEnabled") && !allow)
        {
            AccountManagementDAL accountMgmtDal = new AccountManagementDAL();
            accountMgmtDal.ClearAlertUsers(profile.UserName);

            if (profile.AccountType == (int)GroupAccountType.WindowsGroup)
            {
                var virtualAccountIds = AccountManagementDAL.GetUsersForGroup(profile.UserName);
                accountMgmtDal.ClearAlertUsersForWindowsVirtualAccount(virtualAccountIds);
            }
        }
    }

    private void ProcessAccountRightsHierarchy(string propertyName, ProfileCommon thisProfile)
    {
        if (thisProfile.AllowAdmin && propertyName.Equals("AllowAdmin", StringComparison.OrdinalIgnoreCase))
        {
            PropagateAdminHierarchy(thisProfile);
        }
        else if (thisProfile.AllowNodeManagement &&
                 propertyName.Equals("AllowNodeManagement", StringComparison.OrdinalIgnoreCase))
        {
            SetPropertyNodeManagerHierarchy(thisProfile);
        }
        else if (thisProfile.AllowAlertManagement &&
                 propertyName.Equals("AllowAlertManagement", StringComparison.OrdinalIgnoreCase))
        {
            PropagateAlertManagerHierarchy(thisProfile);
        }
        else if (thisProfile.AllowDisableAllActions &&
                 propertyName.Equals("AllowDisableAllActions", StringComparison.OrdinalIgnoreCase))
        {
            PropagateDisableAllActionsHierarchy(thisProfile);
        }
        else if (thisProfile.AllowCustomize &&
                 propertyName.Equals("AllowCustomize", StringComparison.OrdinalIgnoreCase))
        {
            PropagateCustomizeViewsHierarchy(thisProfile);
        }
    }

    private void PropagateAdminHierarchy(ProfileCommon profile)
    {
        profile.AllowAlertManagement = true;
        profile.AllowNodeManagement = true;
        PropagateAlertManagerHierarchy(profile);
        SetPropertyNodeManagerHierarchy(profile);
    }

    private void PropagateAlertManagerHierarchy(ProfileCommon profile)
    {
        profile.AllowDisableAlert = true;
        profile.AllowDisableAllActions = true;
        PropagateDisableAllActionsHierarchy(profile);
    }

    private void SetPropertyNodeManagerHierarchy(ProfileCommon profile)
    {
        profile.AllowUnmanage = true;
    }

    private void PropagateDisableAllActionsHierarchy(ProfileCommon profile)
    {
        profile.AllowDisableAction = true;
    }

    private void PropagateCustomizeViewsHierarchy(ProfileCommon profile)
    {
        profile.AllowManageDashboards = true;
    }
}
