using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_SMTPServer : System.Web.UI.UserControl
{
    /// <summary>
    /// Load SMTP server settings from database.
    /// </summary>
    public new void Load()
    {
        txtServerAddress.Text = WebSettingsDAL.Get("SMTPServerAddress", string.Empty);
        txtPortNumber.Text = WebSettingsDAL.Get("SMTPServerPort", "25");
        chUseSSL.Checked = Convert.ToBoolean(WebSettingsDAL.Get("SMTPUseSSL", "false"));
        ddlAuthList.SelectedValue = WebSettingsDAL.Get("SMTPUseAuth", "true");
        txtUsername.Text = WebSettingsDAL.Get("SMTPUserName", string.Empty);
        if (!string.IsNullOrEmpty(WebSettingsDAL.Get("SMTPPassword", string.Empty)))
        {
            txtPassword.Attributes["value"] = GetHiddenPassword();
        }
        else
        {
            txtPassword.Text = string.Empty;
        }
    }

    /// <summary>
    /// Save SMTP server settings into database.
    /// </summary>
    public void Save()
    {
        WebSettingsDAL.Set("SMTPServerAddress", txtServerAddress.Text);
        WebSettingsDAL.Set("SMTPServerPort", txtPortNumber.Text);
        WebSettingsDAL.Set("SMTPUseSSL", chUseSSL.Checked.ToString().ToLowerInvariant());
        WebSettingsDAL.Set("SMTPUseAuth", ddlAuthList.SelectedValue);
        if (Convert.ToBoolean(ddlAuthList.SelectedValue))
        {
            WebSettingsDAL.Set("SMTPUserName", txtUsername.Text);
            if (!txtPassword.Text.Equals(GetHiddenPassword()))
            {
                WebSettingsDAL.Set("SMTPPassword", txtPassword.Text);
            }
        }
    }

    /// <summary>
    /// Use to hide password in html response.
    /// </summary>
    private string GetHiddenPassword()
    {
        return new String(' ', WebSettingsDAL.Get("SMTPPassword", string.Empty).Length);
    }
}
