<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" Title="Untitled Page" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Extensions" %>

<script runat="server">

    private ViewInfo _view;
    protected ViewInfo View
    {
        get
        {
            if (null == this._view)
                this._view = ViewManager.GetViewById(Convert.ToInt32(this.Request.QueryString["ViewID"]));

            return this._view;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        var queryStrings = Request.QueryString.ToEnumerable()
            .Merge(new[] {new KeyValuePair<string, string>("ViewID", this.View.ViewID.ToString())});

        var url = SolarWinds.Orion.Web.Helpers.UrlHelper.ReplaceQueryString("/Orion/View.aspx", queryStrings);

        if (!string.IsNullOrEmpty(Request["isNOCView"]))
            url = SolarWinds.Orion.Web.Helpers.UrlHelper.UrlAppendUpdateParameter(url, "isNOCView", Request["isNOCView"]);

        if (null != this.View.NetObjectType)
        {
            if (!string.IsNullOrEmpty(Request["NetObject"]))
            {
                Response.Redirect(SolarWinds.Orion.Web.Helpers.UrlHelper.UrlAppendUpdateParameter(url, "NetObject", Request["NetObject"]));
            }
            else
            {
                string netObjectParam = string.Empty;

                Limitation lim = null;
                if (this.View.LimitationID > 0)
                    lim = Limitation.GetLimitationByID(this.View.LimitationID);

                NetObject myObject = NetObjectFactory.GetPreviewObjectByType(this.View.NetObjectType, lim);

                if (null == myObject)
                {
                    foreach (NetObject obj in NetObjectFactory.GetAllObjects(this.View.NetObjectType))
                    {
                        if (null != lim && (!obj.PassesLimitation(lim)))
                        {
                            continue;
                        }

                        netObjectParam = obj.NetObjectID;
                        break;
                    }
                }
                else
                {
                    netObjectParam = myObject.NetObjectID;
                }

                if (!string.IsNullOrEmpty(netObjectParam))
                {
                    Response.Redirect(SolarWinds.Orion.Web.Helpers.UrlHelper.UrlAppendUpdateParameter(url, "NetObject", netObjectParam));
                }
            }
        }
        else
        {
            Response.Redirect(url);
        }

        base.OnInit(e);
    }
</script>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <div class="StatusMessage">
        Could not locate a network device for view <%= DefaultSanitizer.SanitizeHtml(this.View.ViewTitle) %>.  You may have
        created a view limitation that is too restrictive.
    </div>
</asp:Content>

