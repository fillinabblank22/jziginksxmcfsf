using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_ModuleUserSettings : System.Web.UI.UserControl
{
    private static SolarWinds.Logging.Log myLog = new SolarWinds.Logging.Log();

    public void SetPropertyValuesToForm(ProfileCommon currentProfile)
    {
        if (!this.IsPostBack)
        {
            // enumerate child controls and populate from currentProfile
            List<ProfilePropEditUserControl> myList = new List<ProfilePropEditUserControl>();

            FindPropEditControls(this, myList, false);

            foreach (ProfilePropEditUserControl editControl in myList)
            {
                myLog.InfoFormat("Populating ModuleUserSettings control: username: {0}; property: {1}:{2}", currentProfile.UserName, editControl.PropertyGroupName, editControl.PropertyName);
                editControl.PropertyValue = currentProfile.GetProfileGroup(editControl.PropertyGroupName).GetPropertyValue(editControl.PropertyName).ToString();
            }
        }
    }

    public List<string> GetPropertyNamesForEdit()
    {
        var myList = new List<ProfilePropEditUserControl>();
        FindPropEditControls(this, myList, true);

        return myList.Select(editControl => editControl.PropertyGroupName + "." + editControl.PropertyName).ToList();
    }

    public void SetPropertyValuesToProfile(ProfileCommon currentProfile)
    {
        // enumerate child controls and set currentProfile's properties to the controls' values
        List<ProfilePropEditUserControl> myList = new List<ProfilePropEditUserControl>();
        FindPropEditControls(this, myList, true);

        foreach (ProfilePropEditUserControl editControl in myList)
        {
            string groupedPropName = editControl.PropertyGroupName + "." + editControl.PropertyName;
            Type propType = ProfileCommon.Properties[groupedPropName].PropertyType;
            object propValue = Convert.ChangeType(editControl.PropertyValue, propType);

            myLog.InfoFormat("Saving property {0} with value {1}.", groupedPropName, propValue);
            currentProfile.SetPropertyValue(groupedPropName, propValue);
        }

        currentProfile.Save();
    }

    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("Admin/Accounts/js/EditAccounts.js");

        var modules = SolarWinds.Orion.Web.OrionModuleManager.GetPropertyOwners().ToList();
        var modulesToDelete = new List<PropertyInfoOwner>();
        foreach (var modul in modules)
        {
            PropertyInfoCollection colP = OrionModuleManager.GetModuleUserPropertyInfo(modul.Name);
            if (!colP.Any())
            {
                modulesToDelete.Add(modul);
            }
        }

        modulesToDelete.ForEach(item => modules.Remove(item));
        rptModules.DataSource = modules;
        rptModules.DataBind();

        base.OnInit(e);
    }


    protected void PropControlPlaceHolder_DataBind(object sender, EventArgs e)
    {
        myLog.Debug("Dynamically adding module-specific property edit control.");
        Control placeholder = (PlaceHolder)sender;
        string controlPath = DataBinder.Eval(placeholder.NamingContainer, "DataItem.ControlPath").ToString();
        myLog.DebugFormat("\tControlPath = \"{0}\"", controlPath);
        string moduleID = DataBinder.Eval(placeholder.NamingContainer, "DataItem.ModuleID").ToString();
        myLog.DebugFormat("\tModuleID = \"{0}\"", moduleID);
        string propName = DataBinder.Eval(placeholder.NamingContainer, "DataItem.Name").ToString();
        myLog.DebugFormat("\tProperty Name = \"{0}\"", propName);
        
        ProfilePropEditUserControl propControl = (ProfilePropEditUserControl)LoadControl(controlPath);

        propControl.PropertyGroupName = moduleID;
        propControl.PropertyName = propName;

        placeholder.Controls.Add(propControl);
    }

    // EGAD! Recursion!  Pay attention!
    private void FindPropEditControls(Control parent, List<ProfilePropEditUserControl> list, bool onlyChecked)
    {
        if (parent is ProfilePropEditUserControl)
        {
            var cb = (HtmlInputCheckBox)parent.Parent.Parent.FindControl("chbxModuleSetting");
            if (!onlyChecked || !cb.Visible || cb.Checked)
                list.Add((ProfilePropEditUserControl) parent);
        }
        else
        {
            if (parent.Controls != null)
            {
                foreach (Control child in parent.Controls)
                {
                    FindPropEditControls(child, list, onlyChecked);
                }
            }
        }
    }
}
