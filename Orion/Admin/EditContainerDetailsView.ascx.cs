using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Admin_EditContainerDetailsView : ProfilePropEditUserControl
{
    public override string PropertyValue
    {
        get
        {
            return this.lbxNodeDetails.SelectedValue;
        }
        set
        {
            ListItem item = this.lbxNodeDetails.Items.FindByValue(value);
            if (item != null)
            {
                item.Selected = true;
            }
            else
            {
                // default to "by device type"
                this.lbxNodeDetails.Items.FindByValue("-1").Selected = true;
            }
                
        }
    }

    protected override void OnInit(EventArgs e)
    {
        foreach (ViewInfo info in ViewManager.GetViewsByType("GroupDetails"))
        {
            ListItem item = new ListItem(info.ViewTitle, info.ViewID.ToString());
            this.lbxNodeDetails.Items.Add(item);
        }

        base.OnInit(e);
    }

    
}
