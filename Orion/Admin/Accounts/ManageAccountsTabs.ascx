<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageAccountsTabs.ascx.cs" Inherits="Orion_Admin_ManageAccountsTabs" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>

<orion:NavigationTabBar ID="NavigationTabBar1" runat="server" Visible="true">
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_451 %>" Url="~/Orion/Admin/Accounts/Accounts.aspx" />
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_452 %>" Url="~/Orion/Admin/Accounts/AccountGroups.aspx" />
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_488 %>" Url="~/Orion/Admin/Accounts/SamlGroups.aspx" />
</orion:NavigationTabBar>