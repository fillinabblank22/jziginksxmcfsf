﻿using System;
using System.DirectoryServices.Protocols;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Model.LDAPAuthentication;

public partial class Orion_Admin_Accounts_AdvancedADSettings : Page
{
    private const string DefaultReturnUrl = "~/Orion/Admin";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadLDAPAuthSettings();

            // blocks save AdvancedADSettings of demo mode
            if (!Profile.AllowAdmin && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                btnSettingsSave.OnClientClick = "demoAction('Core_AdvancedADSettings_SaveSettings', this); return false;";
            }
        }
    }

    private void LoadLDAPAuthSettings()
    {
        var settings = LDAPAuthHelper.GetLDAPAuthSettings();
        SetupLDAPSettingsEnabled(settings.Enabled);
        tbServerAddress.Value = settings.Host;
        tbPort.Value = settings.Port.ToString();
        tbDomainDN.Value = settings.DomainDN;
        ddAuthType.Value = settings.AuthType.ToString();
        cbUseSsl.Checked = settings.UseSSL;
        ldapVerified.Value = bool.TrueString;

        if(cbUseSsl.Checked)
            portSuggestion.Style["display"] = "inline";
    }

    private void SaveLDAPAuthSettings()
    {
        if (ldapEnableVal.Value.Equals(bool.FalseString))
        {
            LDAPAuthHelper.EnableLDAPAuthentication(false);
            return;
        }
        var settings = new LDAPAuthSettings
        {
            Enabled = ldapEnableVal.Value.Equals(bool.TrueString),
            Host = tbServerAddress.Value,
            Port = int.Parse(tbPort.Value),
            DomainDN = tbDomainDN.Value,
            AuthType = (AuthType)
                    Enum.Parse(typeof(AuthType),
                        ddAuthType.Value, true),
            UseSSL = cbUseSsl.Checked
        };
       
        LDAPAuthHelper.SetLDAPAuthSettings(settings);
    }

    private void SetupLDAPSettingsEnabled(bool enabled)
    {
        if (enabled)
        {
            ldapEnabled.InnerText = Resources.CoreWebContent.WEBDATA_YP0_20;
            ldapEnabled.Attributes["class"] = "toggleOn";
            ldapEnableVal.Value = bool.TrueString;
            ldapSettings.Style["display"] = "block";
        }
        else
        {
            ldapEnabled.InnerText = Resources.CoreWebContent.WEBDATA_SO0_153;
            ldapEnabled.Attributes["class"] = "toggleOff";
            ldapEnableVal.Value = bool.FalseString;
            ldapSettings.Style["display"] = "none";
        }
    }

    protected void SaveBtnClick(object sender, EventArgs e)
    {
        SaveLDAPAuthSettings();
        ReferrerRedirectorBase.Return(DefaultReturnUrl);
    }

    protected void CancelBtnClick(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Return(DefaultReturnUrl);
    }
}