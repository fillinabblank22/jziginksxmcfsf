<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Accounts.aspx.cs" Inherits="Orion_Admin_Accounts_Accounts" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBCODE_TM0_43 %>" %>

<%@ Register Src="~/Orion/Admin/Accounts/ManageAccountsTabs.ascx" TagPrefix="orion" TagName="ManageAccountTabs" %>

<asp:Content ID="accountsHeader" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">

    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Admin/Accounts/js/SearchField.js" />
    <orion:Include runat="server" File="Admin/Accounts/js/Accounts.js" />

    <style type="text/css">
        span.Label {
            white-space: nowrap;
        }

        .smallText {
            color: #979797;
            font-size: 9px;
        }

        .add {
            background-image: url(/Orion/Admin/Accounts/images/icons/add_16x16.gif) !important;
        }

        .edit {
            background-image: url(/Orion/Admin/Accounts/images/icons/icon_edit.gif) !important;
        }

        .changePassword {
            background-image: url(/Orion/Admin/Accounts/images/icons/credential.gif) !important;
        }

        .searchAccounts {
            background-image: url(/Orion/Admin/Accounts/images/search/search_button.gif) !important;
        }

        .clearAccounts {
            background-image: url(/Orion/Admin/Accounts/images/search/clear_button.gif) !important;
        }

        .del {
            background-image: url(/Orion/Admin/Accounts/images/icons/icon_delete.gif) !important;
        }

        .accountsFilter {
            top: 0px !important;
        }

        #delDialog li {
            max-width: 320px;
            overflow: hidden;
            text-overflow: ellipsis;
            list-style-position: inside;
            white-space: nowrap;
        }

        #delDialog ul {
            margin-left: 20px;
            list-style-type: square;
        }

        #delDialog td.x-btn-mc {
            text-align: center !important;
        }

        #Grid td {
            padding-right: 0;
            padding-bottom: 0;
            vertical-align: middle;
        }

        .x-grid3-scroller {
            height: 270px;
        }

        #delDialog .dialogBody {
            padding-left: 60px;
            margin: 20px;
            background: url('/Orion/images/NotificationImages/notification_warning.png') top left no-repeat
        }

        #delDialog .dialogItem {
            margin-bottom: 10px;
        }

        #delDialogAgentText {
            margin-left: 5px;
        }

        .dialogBox {
            display: none;
        }

        .x-window-footer.x-panel-btns {
            padding-bottom: 10px;
        }

        .pageIntroduction {
            padding: 1em 0 1em 0;
        }

        .fullWidth {
            width: 100%;
        }

        h1 {
            font-size: large;
        }

        .groupTable td {
            padding: 0;
        }

        .groupTable tr {
            vertical-align: top;
            align: left;
        }

        .groupTable {
            border-spacing: 0;
            border-collapse: collapse;
        }

        #delDialog table.sw-btn-primary {
            background: #297994 !important;
            border-color: #297994 !important;
            color: #ffffff !important;
        }

        #delDialog table:hover.sw-btn-primary {
            background: #204a63 !important;
            border-color: #204a63 !important;
            color: #ffffff !important;
        }
    </style>
</asp:Content>

<asp:Content ID="accountsContent" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <input type="hidden" name="Accounts_PageSize" id="Accounts_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("Accounts_PageSize")) %>' />
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div class="pageIntroduction"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_398) %></div>
    <% if (ContainsSamlAccounts && !IsIdpConfigured)
        {
    %>
    <span class="xui">
        <xui-message name="samlConfiguredMsg" type="info">
            <b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_MB0_1, $"<a href=\"/ui/account/saml/config\">{Resources.CoreWebContent.WEBDATA_MB0_0}</a>")) %></b>
        </xui-message>
        <%
            }
        %>
    </span>
    <div <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable()) %>>
        <div class="fullWidth">
            <orion:ManageAccountTabs ID="ManageAccountTabs1" runat="server" />
            <div id="tabPanel" class="tab-top">
                <table class="GroupTable fullWidth">
                    <tr>
                        <td id="gridCell">
                            <div id="Grid"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="delDialog" class="dialogBox common-dialog">
            <div class="x-window-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_399) %></div>
            <div id="delDialogBody" class="dialogBody">
                <div id="delDialogDescription" class="dialogItem"></div>
                <div id="delDialogAgent" class="dialogItem">
                    <ul id="accounts"></ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>