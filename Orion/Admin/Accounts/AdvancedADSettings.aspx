<%@ Page Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_IB0_208 %>" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="AdvancedADSettings.aspx.cs" Inherits="Orion_Admin_Accounts_AdvancedADSettings" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <style type="text/css">
        .ad-setting-table .column-left {padding-right: 100px;}
        .ad-setting-table .column-right {padding-left: 90px !important; border-left: solid 1px #eaeaea;}
        .ad-setting-table, .ad-setting-table div, .ad-settings-controls {padding: 10px 0 10px 0;}
        .ad-settings-suggestion {width: 680px;}
        .ad-settings-message-text { font-weight: 600;font-size: 11px;}
        .ad-settings-button-disable {color: grey; pointer-events: none;}
        .ad-settings-button { height: 28px; outline: none;}
        .column-left input[type=text], .column-left select {width: 480px;height: 24px;}         
        .input-width{width: 379px!important;}
        .loadinggif {background:url('/Orion/images/AJAX-Loader.gif') no-repeat left center; background-position: 10px; }
        #validationDialog tr td { vertical-align: top;}
        #validationDialog table{ padding-bottom: 10px;margin-bottom: 10px; border-bottom:solid 1px #eaeaea; }
        #validationDialog .ad-settings-controls{ width: 100%;text-align: right;} 
        .helpfulText {  text-align: left; display:inline-block; }
    </style>
    <script type="text/javascript">
        // <![CDATA[
        var defaultPort = 389;
        var defaultSslPort = 636;

        function toggleOnOff() {
            var validators = [document.getElementById("<%= AddressValidator.ClientID %>"), document.getElementById("<%= PortValidator.ClientID %>"), document.getElementById("<%= PortValidatorRange.ClientID %>"), document.getElementById("<%= DomainDNValidator.ClientID %>")];
            var div = $("#<%= ldapEnabled.ClientID %>");
            if (div.hasClass("toggleOff")) {
                div.removeClass('toggleOff');
                div.addClass('toggleOn');
                div[0].innerHTML = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_20) %>';
                $("#<%= ldapEnableVal.ClientID %>").val("True");
                $("#<%= ldapVerified.ClientID %>").val("False");
                $("#<%= ldapSettings.ClientID %>").show();
                // enable validators
                $.each(validators, function (index, validator) {
                    validator.enabled = true;
                });
            } else {
                div.removeClass('toggleOn');
                div.addClass('toggleOff');
                div[0].innerHTML = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_153) %>';
                $("#<%= ldapEnableVal.ClientID %>").val("False");
                $("#<%= ldapVerified.ClientID %>").val("True");
                $("#<%= ldapSettings.ClientID %>").hide();
                // disable validators
                $.each(validators, function (index, validator) {
                    validator.enabled = false;
                    validator.isValid = true;
                });
            }
        }

        function adSettingsChanged() {
            if (!Page_ClientValidate("LdapSettings")) {
                $("#notificationHolder").hide();
                $("#btnTestSettings").addClass('sw-btn-disabled');
            } else {
                $("#btnTestSettings").removeClass('sw-btn-disabled');
            }

            if ($("#<%= tbServerAddress.ClientID %>").val() === "") {
                $("#btnGetDomainDN").addClass('ad-settings-button-disable');
            } else {
                $("#btnGetDomainDN").removeClass('ad-settings-button-disable');
            }

            $("#<%= ldapVerified.ClientID %>").val("False");
        }

        function sslChanged() {
            adSettingsChanged();

            if ($("#<%= cbUseSsl.ClientID %>").is(":checked")) {
                if ($("#<%= tbPort.ClientID %>").val() == defaultPort) {
                    $("#<%= tbPort.ClientID %>").val(defaultSslPort);
                }
                else
                    $("#<%= portSuggestion.ClientID %>").css("display", "inline");
            } else {
                if ($("#<%= tbPort.ClientID %>").val() == defaultSslPort) {
                    $("#<%= tbPort.ClientID %>").val(defaultPort);
                }
                $("#<%= portSuggestion.ClientID %>").css("display", "none");
            }
        }

        function ValidateSettings() {
            if (!Page_ClientValidate("LdapSettings") || !Page_ClientValidate("")) return false;
            if ($("#<%= ldapVerified.ClientID %>").val() === "False") {
                $("#validationDialog").dialog({
                    title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_226) %>',
                    resizable: false,
                    width: 460,
                    modal: true
                });
                return false;
            }
            return true;
        }

        function testAdSettings() {

            if ($("#isOrionDemoMode").length != 0) {
                demoAction("Core_AdvancedADSettings_Test", this);
                return;
            }

            var username = $("#<%= tbUserName.ClientID %>").val();
            var password = $("#<%= tbPassword.ClientID %>").val();

            if (!Page_ClientValidate("LdapCreds")) return false;
            if ($("#<%= tbDomainDN.ClientID %>").val() === "" && !Page_ClientValidate("")) return false;

            $("#notificationHolder").show();
            showWaitMessage();

            var ldapSettings = {
                Host: $("#<%= tbServerAddress.ClientID %>").val(),
                Port: $("#<%= tbPort.ClientID %>").val(),
                DomainDN: $("#<%= tbDomainDN.ClientID %>").val(),
                AuthType: $("#<%= ddAuthType.ClientID %>").val(),
                UseSSL: $("#<%= cbUseSsl.ClientID %>").is(":checked")
            }

            SW.Core.Services.callWebService("/Orion/Services/AccountManagement.asmx", "TestLDAPServerConnection", { ldapServerSettigs: JSON.stringify(ldapSettings), userName: username, password: password },
                function (response) {
                    if (response && response.result) {
                        $('#iconResult').attr("src", "/Orion/Admin/Accounts/images/icons/icon_success.png");
                        $("#primaryMessage").html('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_110) %>');
                        $("#descriptionMessage").html('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_111) %>');
                        $("#<%= ldapVerified.ClientID %>").val("True");
                    } else {
                        if (response && response.message) {
                            showFailureMessage(response.message);
                        } else {
                            showFailureMessage(String.format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_113) %>',
                                ldapSettings.Host,
                                ldapSettings.Port));
                        }
                    }
                }, function (error) {
                    showFailureMessage(error);
                    return false;
                });
        }

        function showFailureMessage(message) {
            $('#iconResult').attr("src", "/Orion/Admin/Accounts/images/icons/icon_failure.png");
            $("#primaryMessage").html('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_112) %>');
            $("#descriptionMessage").html(message);
        }

        function showWaitMessage() {
            $('#iconResult').attr("src", "/Orion/images/AJAX-Loader.gif");
            $("#primaryMessage").html('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_108) %>');
            $("#descriptionMessage").html('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_109) %>');
        }

        function getDomainDN() {

            if ($("#isOrionDemoMode").length != 0) {
                demoAction("Core_AdvancedADSettings_DiscoverDN", this);
                return;
            }

            var ldapServer = $("#<%= tbServerAddress.ClientID %>").val();

            if (ldapServer === '') {
                return;
            }
            $("#<%= tbDomainDN.ClientID %>").val(null);
            $("#<%= tbDomainDN.ClientID %>").attr("disabled", "disabled");
            $("#<%= tbDomainDN.ClientID %>").addClass('loadinggif');
            $("#btnGetDomainDN").addClass('ad-settings-button-disable');

            SW.Core.Services.callWebService("/Orion/Services/AccountManagement.asmx", "GetDomainDN", { ldapServer: ldapServer }, function (result) {
                if (result) {
                    $("#<%= tbDomainDN.ClientID %>").removeAttr("disabled", "disabled");
                    $("#<%= tbDomainDN.ClientID %>").removeClass('loadinggif');
                    $("#btnGetDomainDN").removeClass("ad-settings-button-disable");
                    $("#<%= tbDomainDN.ClientID %>").val(result);
                    adSettingsChanged();
                }
            }, function (error) {
                $("#<%= tbDomainDN.ClientID %>").removeAttr("disabled", "disabled");
                $("#<%= tbDomainDN.ClientID %>").removeClass("loadinggif");
                    $("#btnGetDomainDN").removeClass("ad-settings-button-disable");
                    adSettingsChanged();
                    return false;
                });
        }

        $(function () {
            $(':text').bind('keydown', function (e) {
                if (e.keyCode === 13)
                    e.preventDefault();
            });
            $(':password').bind('keydown', function (e) {
                if (e.keyCode === 13)
                    e.preventDefault();
            });
        });
        // ]]>
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div class="sw-suggestion sw-suggestion-info ad-settings-suggestion">
        <span class="sw-suggestion-icon"></span>
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_209) %></span>
    </div>
    <div class="ad-settings-controls">
        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_210) %></b><br />
        <a id="ldapEnabled" runat="server" onclick="toggleOnOff()" href="#" class="toggleOn"></a>
        <asp:HiddenField ID="ldapEnableVal" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="ldapVerified" ClientIDMode="Static" runat="server" />
    </div>
    <table runat="server" id="ldapSettings" class="ad-setting-table">
        <tr>
            <td class="column-left">
                <div>
                    <p><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_211) %></b></p>
                    <input runat="server" id="tbServerAddress" type="text" onchange="adSettingsChanged()" />
                    <asp:RequiredFieldValidator ID="AddressValidator" ValidationGroup="LdapSettings" runat="server" ControlToValidate="tbServerAddress" Display="Static" ErrorMessage="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>"></asp:RequiredFieldValidator><br />
                    <span class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_212) %></span><br />
                </div>
                <div>
                    <p><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_213) %></b></p>
                    <input runat="server" id="tbPort" type="text" onchange="adSettingsChanged()" />
                    <asp:RequiredFieldValidator ID="PortValidator" ValidationGroup="LdapSettings" runat="server" ControlToValidate="tbPort" Display="Static" ErrorMessage="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator ID="PortValidatorRange" ValidationGroup="LdapSettings" runat="server" ControlToValidate="tbPort" Type="Integer" MinimumValue="0" MaximumValue="65535" Display="Dynamic" ErrorMessage="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_225%>" />
                </div>
                <div>
                    <p>
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_214) %></b>
                    </p>
                    <input runat="server" class="input-width" id="tbDomainDN" type="text" onchange="adSettingsChanged()" />
                    <asp:Button ID="btnGetDomainDN" CausesValidation="false" CssClass="ad-settings-button sw-btn-secondary sw-btn" ClientIDMode="Static" runat="server" Text="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_114 %>" OnClientClick="getDomainDN(); return false;" />
                    <asp:RequiredFieldValidator ID="DomainDNValidator" runat="server" ControlToValidate="tbDomainDN" Display="Static" ErrorMessage="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>"></asp:RequiredFieldValidator><br />
                    <span class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_215) %></span><br>
                </div>
                <div>
                    <p><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_216) %></b></p>
                    <select runat="server" id="ddAuthType" onchange="adSettingsChanged()">
                        <option value="Basic" title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_IB0_218%>"></option>
                        <option value="Negotiate" title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_IB0_219%>"></option>
                        <option value="Ntlm" title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_IB0_220%>"></option>
                        <option value="Kerberos" title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_IB0_221%>"></option>
                    </select>
                </div>
                <div>
                    <input type="checkbox" runat="server" id="cbUseSsl" onchange="sslChanged()" /><asp:Literal runat="server" Text="<%$HtmlEncodedResources: CoreWebContent,WEBDATA_AK1_6%>" />
                    <span id="portSuggestion" runat="server" class="sw-suggestion" style="margin: 7px 2px 1px 5px; display: none"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_115) %></span>
                </div>
            </td>
            <td class="column-right">
                <p><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_222) %></b></p>
                <p><span class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_223) %></span></p>
                <div>
                    <p><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_224) %></b></p>
                    <asp:TextBox runat="server" ID="tbUserName" onchange="adSettingsChanged()" autocomplete="new-password" Width="290" Height="24"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="tbUserNameValidator" ValidationGroup="LdapCreds" runat="server" ControlToValidate="tbUserName" Display="Static" ErrorMessage="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>"></asp:RequiredFieldValidator><br />
                </div>
                <div>
                    <p><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_8) %></b></p>
                    <asp:TextBox runat="server" ID="tbPassword" onchange="adSettingsChanged()" TextMode="Password" autocomplete="new-password" Width="290" Height="24"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="tbPasswordValidator" ValidationGroup="LdapCreds" runat="server" ControlToValidate="tbPassword" Display="Static" ErrorMessage="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>"></asp:RequiredFieldValidator><br />
                </div>
                <div class="ad-settings-controls">
                    <orion:LocalizableButton ID="btnTestSettings" CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Test" OnClientClick="testAdSettings(); return false;" />
                </div>
                <div id="notificationHolder" style="display: none">
                    <table>
                        <tr>
                            <td>
                                <img id="iconResult" src="" alt="" style="margin-right: 5px; vertical-align: top;" />
                            </td>
                            <td>
                                <p id="primaryMessage" class="ad-settings-message-text"></p>
                                <p style="width: 290px"><span id="descriptionMessage" class="helpfulText"></span></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <div id="validationDialog" title="" style="display: none;">
        <table>
            <tr>
                <td>
                    <img src="/Orion/images/NotificationImages/notification_warning.png" alt="" style="margin-right: 5px; vertical-align: top;" /></td>
                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_227) %></td>
            </tr>
        </table>
        <div class="ad-settings-controls">
            <orion:LocalizableButton CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$HtmlEncodedResources:CoreWebContent,WEBDATA_IB0_159 %>" OnClick="SaveBtnClick" />
            <orion:LocalizableButton CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="Cancel" OnClientClick="$('#validationDialog').dialog('close'); return false;" />
        </div>
    </div>
    <div class="ad-settings-controls">
        <orion:LocalizableButton ID="btnSettingsSave" CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="Save" OnClientClick="return ValidateSettings();" OnClick="SaveBtnClick" />
        <orion:LocalizableButton ID="btnSettingsCancel" CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelBtnClick" />
    </div>
</asp:Content>

