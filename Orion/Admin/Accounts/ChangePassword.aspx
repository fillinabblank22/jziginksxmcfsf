<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" 
         CodeFile="ChangePassword.aspx.cs" Inherits="Orion_Admin_Accounts_ChangePassword" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_181 %>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_TM0_79, HttpUtility.HtmlEncode(this.UserName))) %></h1>
    
    <p runat="server" id="paraPasswordBoxes">
        <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_184) %></strong><br />
        <asp:TextBox runat="server" ID="txtPassword" TextMode="password" Rows="1" Columns="25" autocomplete="off" />
        <br /><br />
        <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_182) %></strong><br />
        <asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="password" Rows="1" Columns="25" autocomplete="off" /><br />
        
    </p>
    
    <asp:PlaceHolder runat="server" ID="phSuccessMessage" Visible="false"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_183) %></asp:PlaceHolder>
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnChangePassword" OnClick="ChangePasswordClick" LocalizedText="CustomText"
            Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_181 %>" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="CancelContinueClick" 
            LocalizedText="Cancel" DisplayType="Secondary" />
    </div>
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnContinue"  Visible="false" OnClick="CancelContinueClick"
            LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_186 %>" DisplayType="Primary" />
    </div>    
    <div id="statusMessage">
        <asp:PlaceHolder runat="server" ID="phErrorMessage" />
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtPassword" ControlToCompare="txtConfirmPassword"
                                      Display="static" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_185 %>" 
                                      EnableClientScript="false" />
    </div>
</asp:Content>

