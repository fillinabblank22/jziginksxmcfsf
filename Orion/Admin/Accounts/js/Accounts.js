﻿Ext.namespace('SW');
Ext.namespace('SW.NPM');

SW.NPM.Accounts = function () {

    ORION.prefix = "Accounts_";

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        map.EditButton.setDisabled(count === 0);
        map.DeleteButton.setDisabled(count === 0);

        //check whether selected item is a Windows Account or SAML Account
        if (count == 1 && (selItems.getSelected().get("Accounts.AccountType") == "2" || selItems.getSelected().get("Accounts.AccountType") == "5")) {
            // is Windows Account, so disable the Change Password button
            map.ChangePasswordButton.setDisabled(true);
        }
        else {
            // not a Windows account or more than one record selected, so set button accordingly
            map.ChangePasswordButton.setDisabled(count != 1);
        }

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
            map.ChangePasswordButton.setDisabled(true);
        }
    };

    function gridResize() {
        // need to set grid width to small width first as it appears setWidth only resizes upwards! (bug in Ext maybe?)
        grid.setWidth(1);
        grid.setWidth($('#gridCell').width());
    }

    function setFormFieldTooltip(id, tip) {
        var tt = new Ext.ToolTip({
            target: id,
            title: tip,
            plain: true,
            showDelay: 0,
            hideDelay: 0,
            trackMouse: false
        });
    }

    // Encoding value function
    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // Column rendering function
    function renderAccountField(value) {
        return encodeHTML(value);
    };

    // rendering AccountID (Name) column applying highlighting to search text matches
    function renderAccountName(value) {

        var accName = value;
        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(accName) + '</span>'; }


        //check for trailing wildcard * on end of search string
        //var x = patternText.toString().substring(patternText.length - 1);
        //if (x == "*") { patternText = patternText.toString().substring(0, (patternText.length - 1)); }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = accName,

        pattern = new RegExp(x, "gi"),
        //replaceWith = '<span style=\"background-color: #FFE89E\">$1</span>';

        //NOTE: we need to add literal SPAN 'markers' because otherwise the actual HTML span tags would get encoded as literal
        replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex repleace
        accName = content.replace(pattern, replaceWith);

        //ensure highlighted content gets encoded
        accName = encodeHTML(accName);

        // now replace the literal SPAN markers with the HTML span tags
        accName = accName.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        accName = accName.replace(/{SPAN-END-MARKER}/g, '</span>');

        return accName;
    };

    // render AccountType column
    function renderAccountType(value) {
        if (value == '2') {
            var at = '<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/windows.gif" /> Windows</div>';
        }
        else if (value == '5') {
            var at = '<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/saml_icon.png" /> SAML</div>';
        }
        else {
            var at = '<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/orion_generic_icon_orange.gif" /> Orion</div>';
        }
        return at;
    };

    // render the data columns which contain enabled/disabled icons
    function renderYesNo(value, meta, record) {
        if (value == 'Y') {
            var yn = String.format('<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/ok_enabled.png" /> {0}</div>', '@{R=Core.Strings;K=WEBJS_AK0_28;E=js}');
        }
        else {
            var yn = String.format('<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/disable.png" /> {0}</div>', '@{R=Core.Strings;K=WEBJS_AK0_27;E=js}');
        }
        return yn;
    };

    function addNewAccounts() {
        // 
        window.location = "/Orion/Admin/Accounts/Add/SelectAccountType.aspx";
    };

    function editAccounts() {
        var ids = [];
        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["Accounts.AccountID"]);
        });
        var result;
        ORION.callWebService("/Orion/Services/AccountManagement.asmx", "EditWindowsAccount", { accountIDs: ids }, function (result) {
            document.location.href = result;
        });
    };

    // Change password for selected account
    function changePassword() {
        var rec;
        grid.getSelectionModel().each(function (record) {
            rec = record;
        });
        var qs = "?AccountID=" + rec.data["Accounts.AccountID"];
        var url = "/Orion/Admin/Accounts/ChangePassword.aspx";
        document.location.href = url + qs //+ referrer;
    };

    // Delete selected accounts function.  Pops a delete confirmation dialog and call on webservice to perform delete.
    function delAccounts() {
        // get accountIds of selected records in the grid
        var ids = [];
        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["Accounts.AccountID"]);
        });

        var layout = $("#delDialogDescription");

        if (ids.length > 5) {
            layout.text(String.format('@{R=Core.Strings;K=WEBJS_AK0_77;E=js}', ids.length));
            $("#accounts").empty();
        }
        else {
            layout.text('@{R=Core.Strings;K=WEBJS_AK0_78;E=js}');
            var accounts = $("#accounts");
            accounts.empty();
            $(ids).each(function(i,v) {
                $(String.format('<li>{0}</li>', encodeHTML(''+v))).appendTo(accounts);
            });
        }

        if (!delDialog) {
            delDialog = new Ext.Window({
                applyTo: 'delDialog',
                layout: 'fit',
                width: 420,
                height: 205,
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'delDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
        {
            text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}',
            handler: function () {

                var ids = [];

                var curPageRecordCount = grid.store.getCount();
                var selectedRecordCount = grid.getSelectionModel().getCount();

                grid.getSelectionModel().each(function (rec) {
                    ids.push(rec.data["Accounts.AccountID"]);
                });

                ORION.callWebService("/Orion/Services/AccountManagement.asmx", "DeleteAccount", { accountIDs: ids }, function (result) {

                    // Note: .reload() only reloads the current page.  If deleting the last records from the current page, force a load of 
                    // all data using .load() to prevent paging from going out of whack.  
                    if (curPageRecordCount > selectedRecordCount) {
                        grid.getStore().reload();
                    }
                    else {
                        grid.getStore().load();
                    }
                });

                delDialog.hide();
            }
        },
        {
            text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}',
            cls: 'sw-btn-primary',
            style: 'margin-left: 5px;',
            handler: function () {
                delDialog.hide();
            }
        }]
            });
        }

        // Set the location
        delDialog.alignTo(document.body, "c-c");
        delDialog.show();

        return false;
    };

    var selectorModel;
    var dataStore;
    var grid;
    var delDialog;
    var pageSizeNum;

    return {
        init: function () {
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                "/Orion/Services/AccountManagement.asmx/GetAccounts",
                [
                { name: 'Accounts.AccountID', mapping: 0 },
                { name: 'Accounts.AccountType', mapping: 1 },
                { name: 'Accounts.AccountEnabled', mapping: 2 },
                { name: 'Accounts.Expires', mapping: 3 },
                { name: 'Accounts.LastLogin', mapping: 4 },
                { name: 'Accounts.LimitationID', mapping: 5 },
                { name: 'Accounts.AllowAdmin', mapping: 6 },
                { name: 'Accounts.AllowNodeManagement', mapping: 7 },
                { name: 'Accounts.AllowMapManagement', mapping: 8 },
                { name: 'Accounts.AllowCustomize', mapping: 9 },
                { name: 'Accounts.AllowManageDashboards', mapping: 10 },
                { name: 'Accounts.AllowReportManagement', mapping: 11 },
                { name: 'Accounts.AllowAlertManagement', mapping: 12 },
                { name: 'Accounts.AllowUnmanage', mapping: 13 },
                { name: 'Accounts.AllowDisableAction', mapping: 14 },
                { name: 'Accounts.AllowDisableAlert', mapping: 15 },
                { name: 'Accounts.AllowDisableAllActions', mapping: 16 },
                { name: 'Accounts.AllowOrionMapsManagement', mapping: 17 },
                { name: 'Accounts.AllowUploadImagesToOrionMaps', mapping: 18 }
                ],
                "Accounts.AccountID");

            // pass an empty string to accountId parameter of GetAccount web method.
            //dataStore.proxy.conn.jsonData = { accountId: '' };
            //dataStore.load();

            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
                        selectorModel,
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_9;E=js}', width: 200, sortable: true, dataIndex: 'Accounts.AccountID', renderer: renderAccountName },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_81;E=js}', width: 150, sortable: true, dataIndex: 'Accounts.AccountType', renderer: renderAccountType },
                        { header: '@{R=Core.Strings;K=WEBJS_VB0_23;E=js}', width: 105, sortable: true, dataIndex: 'Accounts.AccountEnabled', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_82;E=js}', width: 175, sortable: true, dataIndex: 'Accounts.Expires', renderer: renderAccountField },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_83;E=js}', width: 175, sortable: true, dataIndex: 'Accounts.LastLogin', renderer: renderAccountField },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_84;E=js}', width: 205, sortable: true, dataIndex: 'Accounts.LimitationID', renderer: renderAccountField },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_85;E=js}', width: 130, sortable: true, dataIndex: 'Accounts.AllowAdmin', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_86;E=js}', width: 130, sortable: true, dataIndex: 'Accounts.AllowNodeManagement', renderer: renderYesNo },
                        { header: '@{R=Core.Strings.2;K=WEBJS_MG0_1;E=js}', width: 130, sortable: true, dataIndex: 'Accounts.AllowMapManagement', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=WEBJS_TM0_120;E=js}', width: 130, sortable: true, dataIndex: 'Accounts.AllowReportManagement', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=WEBJS_SO0_99;E=js}', width: 130, sortable: true, dataIndex: 'Accounts.AllowAlertManagement', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=XMLDATA_ED0_2;E=js}', width: 185, sortable: true, dataIndex: 'Accounts.AllowUnmanage', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=XMLDATA_JV0_1;E=js}', width: 175, sortable: true, dataIndex: 'Accounts.AllowDisableAction', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=XMLDATA_JV0_2;E=js}', width: 175, sortable: true, dataIndex: 'Accounts.AllowDisableAlert', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=XMLDATA_JV0_3;E=js}', width: 205, sortable: true, dataIndex: 'Accounts.AllowDisableAllActions', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_87;E=js}', width: 130, sortable: true, dataIndex: 'Accounts.AllowCustomize', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=DashboardMgmt; E=js}', width: 175, sortable: true, dataIndex: 'Accounts.AllowManageDashboards', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=OrionMapsMgmt;E=js}', width: 175, sortable: true, dataIndex: 'Accounts.AllowOrionMapsManagement', renderer: renderYesNo },
                        { header: '@{R=Core.Strings;K=OrionMapsUploadImages;E=js}', width: 205, sortable: true, dataIndex: 'Accounts.AllowUploadImagesToOrionMaps', renderer: renderYesNo }
                ],
                sm: selectorModel,
                viewConfig: {
                    //set to true if you want to resize each column with the window resize.  False maintains existing column sizes.
                    forceFit: false,
                    emptyText: '@{R=Core.Strings;K=WEBJS_AK0_88;E=js}'
                },
                //width: 700,
                height: 350,
                stripeRows: true,
                trackMouseOver: false,
                // top toobar on grid with buttons and search field
                tbar: [
                {
                    id: 'AddButton',
                    text: '@{R=Core.Strings;K=WEBJS_AK0_89;E=js}',
                    iconCls: 'add',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        addNewAccounts();
                    }
                }, '-',
                {
                    id: 'EditButton',
                    text: '@{R=Core.Strings;K=CommonButtonType_Edit;E=js}',
                    iconCls: 'edit',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        editAccounts();
                    }
                }, '-',
                {
                    id: 'ChangePasswordButton',
                    text: '@{R=Core.Strings;K=WEBJS_AK0_90;E=js}',
                    iconCls: 'changePassword',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        changePassword();
                    }
                }, '-',
                {
                    id: 'DeleteButton',
                    text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}',
                    iconCls: 'del',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        delAccounts();
                    }
                }, ' ',

                //'->',  // removed the right align
                ' ', '-', ' ', ' ',
                // account search field and trigger buttons
                new Ext.ux.form.SearchField({
                    store: dataStore,
                    width: 260
                }), ' '
                ],

                // bottom toolbar
                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_AK0_91;E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_AK0_88;E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.render('Grid');
            UpdateToolbarButtons();

            // add a tooltip to the Search control
            setFormFieldTooltip('searchAccounts', '@{R=Core.Strings;K=WEBJS_AK0_92;E=js}');

            grid.bottomToolbar.addPageSizer();
            // resize the grid
            gridResize();

            // provide an empty string for accountId param when loading the page
            grid.store.proxy.conn.jsonData = { accountId: '' };
            // load the grid with data
            grid.store.load();

            $("form").submit(function() { return false; });

            // update the Ext grid size when window size is changed
            $(window).resize(gridResize);
        }
    };

} ();


Ext.onReady(SW.NPM.Accounts.init, SW.NPM.Accounts);
