﻿Ext.namespace('SW');
Ext.namespace('SW.NPM');

SW.NPM.AccountGroups = function () {

    ORION.prefix = "SamlAccountGroups_";

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        map.EditButton.setDisabled(count === 0);
        map.DeleteButton.setDisabled(count === 0);
        map.MoveDown.setDisabled(count === 0);
        map.MoveUp.setDisabled(count === 0);
        checkMe = [];

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
            map.MoveDown.setDisabled(true);
            map.MoveUp.setDisabled(true);
        }
    };

    function gridResize() {
        // need to set grid width to small width first as it appears setWidth only resizes upwards! (bug in Ext maybe?)
        grid.setWidth(1);
        grid.setWidth($('#gridCell').width());
    }

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // Column rendering functions
    function renderCredential(value, meta, record) {
        return encodeHTML(value);
    };

    // render columns with Yes/No icons
    function renderYesNO(value, meta, record) {
        if (value == 'Y') {
            var a = '<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/ok_enabled.png" /> @{R=Core.Strings;K=WEBJS_AK0_28; E=js}</div>';
        }
        else {
            var a = '<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/disable.png" /> @{R=Core.Strings;K=WEBJS_AK0_27; E=js}</div>';
        }
        return a;
    };

    //will sort an array numerically lowest to highest 
    //used in the functions to move rows up and down
    function sortNumeric(a, b) {
        return (a - b);
    }

    //move rows up
    function moveUp(checkMe) {
        var recIDs = [];
        var rec;
        var ids = [];
        var Priorities = [];
        //using each returns the records in the order they were selected rather than in priority order so we have
        //to go a little bit round the houses to get them in priority order, which is required for the swapping process
        // to work correctly
        grid.getSelectionModel().each(function (rec) {
            recIDs.push(grid.store.indexOf(rec));
        });

        recIDs.sort(sortNumeric);
        //recIDs now holds a sorted list of the selected row numbers so we can iterate through these
        //and get the values we want and put them in the correct order into variable ids
        for (var i = 0; i < recIDs.length; i++) {
            rec = grid.getStore().getAt(recIDs[i]);
            ids.push(rec.data["Accounts.AccountID"]);
            var Priority = rec.data["Accounts.GroupPriority"];
            Priorities.push(Priority);
            checkMe.push(recIDs[i]);
        }

        ORION.callWebService("/Orion/Services/AccountManagement.asmx", "MoveUpGroupAccount", { accountIDs: ids, groupAccountType: 6}, function (result) {
            grid.getStore().reload();
            //we need to decrease all the checkme values so the correct 'moved' rows remain ticked
            //but we only do this if the swapping was succesful 
            if (result) {
                for (var i = 0; i < checkMe.length; i++) { checkMe[i] = checkMe[i] - 1; }
            }
        });
    };

    //move rows down
    function moveDown(checkMe, pageSizeNum) {
        var recIDs = [];
        var rec;
        var ids = [];
        var topRowNumber = -1;
        var Priorities = [];
        //using each returns the records in the order they were selected rather than in priority order so we have
        //to go a little bit round the houses to get them in priority order, which is required for the swapping process
        // to work correctly
        grid.getSelectionModel().each(function (rec) {
            recIDs.push(grid.store.indexOf(rec));
        });
        recIDs.sort(sortNumeric);
        recIDs.reverse();
        //recIDs now holds a sorted list of the selected row numbers so we can iterate through these
        //and get the values we want and put them in the correct order into variable ids
        for (var i = 0; i < recIDs.length; i++) {
            rec = grid.getStore().getAt(recIDs[i]);
            ids.push(rec.data["Accounts.AccountID"]);
            var Priority = rec.data["Accounts.GroupPriority"];
            Priorities.push(Priority);
            checkMe.push(recIDs[i]);
            //we can't move the rows 'down' if the 'bottom-most' row has been selected so we check here
            //for selection of the 'bottom-most' row and set a flag appropriately
            if (recIDs[i] > topRowNumber || topRowNumber == -1) {
                topRowNumber = recIDs[i];
            }
        }
        //only do this if the bottommost record is not selected
        if (topRowNumber < pageSizeNum - 1 && topRowNumber < grid.store.totalLength - 1) {
            var result;
            ORION.callWebService("/Orion/Services/AccountManagement.asmx", "MoveDownGroupAccount", { accountIDs: ids, groupAccountType:6 }, function (result) {
                grid.getStore().reload();
                //we need to decrease all the checkme values so the correct 'moved' rows remain ticked
                //only do this if the swapping was succesful 
                if (result) {
                    for (var i = 0; i < checkMe.length; i++) { checkMe[i] = checkMe[i] + 1; }
                }
            });
        }
    };

    function enableButtons(dialog) {
        dialog.buttons[0].enable();   // ok button
        dialog.buttons[1].enable();   // cancel button
    };

    function disableButtons(dialog) {
        dialog.buttons[0].disable();  // ok button
        dialog.buttons[1].disable();  // cancel button  
    };


    function addGroup() {
        document.location.href = "/Orion/Admin/Accounts/Add/SamlAccount.aspx?AccountType=SamlGroup";
    }

    function updateGroup() {
        var ids = [];
        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["Accounts.AccountID"]);
        });
        ORION.callWebService("/Orion/Services/AccountManagement.asmx", "EditGroupAccount", { accountIDs: ids }, function (result) {
            window.location.href = result;
        });
    };

    //delete the selected accounts
    function delGroup() {
        var ids = [];
        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["Accounts.AccountID"]);
        });

        var layout = $("#delDialogDescription");

        if (ids.length > 5) {
            layout.text(String.format('@{R=Core.Strings;K=WEBJS_VB0_45; E=js}', ids.length));
            $("#accounts").empty();
        }
        else {
            layout.text('@{R=Core.Strings;K=WEBJS_VB0_44; E=js}');
            var accounts = $("#accounts");
            accounts.empty();
            $(ids).each(function (i, v) {
                $(String.format('<li>{0}</li>', encodeHTML('' + v))).appendTo(accounts);
            });
        }

        if (!delDialog) {
            delDialog = new Ext.Window({
                applyTo: 'delDialog',
                layout: 'fit',
                width: 420,
                height: 205,
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'delDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
                    {
                        text: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}',
                        handler: function () {

                            var ids = [];
                            var curPageRecordCount = grid.store.getCount();
                            var selectedRecordCount = grid.getSelectionModel().getCount();

                            grid.getSelectionModel().each(function (rec) {
                                ids.push(rec.data["Accounts.AccountID"]);
                            });

                            ORION.callWebService("/Orion/Services/AccountManagement.asmx", "DeleteGroupAccount", { accountIDs: ids, groupAccountType: 6}, function (result) {
                                //    grid.getStore().reload();

                                // Note: .reload() only reloads the current page.  If deleting the last records from the current page, force a load of 
                                // all data using .load() to prevent paging from going out of whack.  
                                if (curPageRecordCount > selectedRecordCount) {
                                    grid.getStore().reload();
                                }
                                else {
                                    grid.getStore().load();
                                }

                            });
                            delDialog.hide();
                        }
                    },
                    {
                        text: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            delDialog.hide();
                        }
                    }]
            });
        }

        // Set the location
        delDialog.alignTo(document.body, "c-c");
        delDialog.show();

        return false;
    };

    var selectorModel;
    var dataStore;
    var grid;
    var delDialog;
    var pageSizeNum;
    var checkMe = [];

    return {
        init: function () {
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        pageSizeNum = this.pageSize;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                "/Orion/Services/AccountManagement.asmx/GetAccountGroups?groupType=6",
                [
                    { name: 'Accounts.GroupPriority', mapping: 0 },
                    { name: 'Accounts.AccountID', mapping: 1 },
                    { name: 'Accounts.AccountEnabled', mapping: 2 },
                    { name: 'Accounts.Expires', mapping: 3 },
                    { name: 'Accounts.LastLogin', mapping: 4 },
                    { name: 'Accounts.Limitation', mapping: 5 },
                    { name: 'Accounts.AllowAdmin', mapping: 6 },
                    { name: 'Accounts.AllowNodeManagement', mapping: 7 },
                    { name: 'Accounts.AllowMapManagement', mapping: 8 },
                    { name: 'Accounts.AllowCustomize', mapping: 9 },
                    { name: 'Accounts.AllowManageDashboards', mapping: 10 },
                    { name: 'Accounts.AllowReportManagement', mapping: 11 },
                    { name: 'Accounts.AllowAlertManagement', mapping: 12 },
                    { name: 'Accounts.AllowUnmanage', mapping: 13 },
                    { name: 'Accounts.AllowDisableAction', mapping: 14 },
                    { name: 'Accounts.AllowDisableAlert', mapping: 15 },
                    { name: 'Accounts.AllowDisableAllActions', mapping: 16 },
                    { name: 'Accounts.AllowOrionMapsManagement', mapping: 17 },
                    { name: 'Accounts.AllowUploadImagesToOrionMaps', mapping: 18 }
                ], "Accounts.GroupPriority");
            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
                    selectorModel,
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_48; E=js}', width: 60, sortable: true, dataIndex: 'Accounts.GroupPriority', renderer: renderCredential },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_9; E=js}', width: 175, sortable: true, dataIndex: 'Accounts.AccountID', renderer: renderCredential },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_23; E=js}', width: 165, sortable: true, dataIndex: 'Accounts.AccountEnabled', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_49; E=js}', width: 150, sortable: true, dataIndex: 'Accounts.Expires', renderer: renderCredential },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_50; E=js}', width: 150, sortable: true, dataIndex: 'Accounts.LastLogin', renderer: renderCredential },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_51; E=js}', width: 175, sortable: true, dataIndex: 'Accounts.Limitation', renderer: renderCredential },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_52; E=js}', width: 90, sortable: true, dataIndex: 'Accounts.AllowAdmin', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_53; E=js}', width: 90, sortable: true, dataIndex: 'Accounts.AllowNodeManagement', renderer: renderYesNO },
                    { header: '@{R=Core.Strings.2;K=WEBJS_MG0_1;E=js}', width: 90, sortable: true, dataIndex: 'Accounts.AllowMapManagement', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=WEBJS_TM0_120; E=js}', width: 90, sortable: true, dataIndex: 'Accounts.AllowReportManagement', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=WEBJS_SO0_99; E=js}', width: 90, sortable: true, dataIndex: 'Accounts.AllowAlertManagement', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=XMLDATA_ED0_2;E=js}', width: 185, sortable: true, dataIndex: 'Accounts.AllowUnmanage', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=XMLDATA_JV0_1;E=js}', width: 155, sortable: true, dataIndex: 'Accounts.AllowDisableAction', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=XMLDATA_JV0_2;E=js}', width: 155, sortable: true, dataIndex: 'Accounts.AllowDisableAlert', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=XMLDATA_JV0_3;E=js}', width: 205, sortable: true, dataIndex: 'Accounts.AllowDisableAllActions', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_54; E=js}', width: 175, sortable: true, dataIndex: 'Accounts.AllowCustomize', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=DashboardMgmt; E=js}', width: 175, sortable: true, dataIndex: 'Accounts.AllowManageDashboards', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=OrionMapsMgmt;E=js}', width: 155, sortable: true, dataIndex: 'Accounts.AllowOrionMapsManagement', renderer: renderYesNO },
                    { header: '@{R=Core.Strings;K=OrionMapsUploadImages;E=js}', width: 205, sortable: true, dataIndex: 'Accounts.AllowUploadImagesToOrionMaps', renderer: renderYesNO }
                ],
                sm: selectorModel,
                viewConfig: {
                    forceFit: false,
                    emptyText: '@{R=Core.Strings;K=WEBJS_VB0_55; E=js}'
                },
                //width: 700,
                height: 350,
                stripeRows: true,
                tbar: [
                    {
                        id: 'AddButton',
                        text: '@{R=Core.Strings;K=WEBJS_VB0_56; E=js}',
                        iconCls: 'add',
                        handler: function () {
                            addGroup();
                        }
                    }, '-',
                    {
                        id: 'EditButton',
                        text: '@{R=Core.Strings;K=CommonButtonType_Edit; E=js}',
                        iconCls: 'edit',
                        handler: function () {
                            updateGroup();
                        }
                    }, '-',
                    {
                        id: 'MoveUp',
                        text: '@{R=Core.Strings;K=WEBJS_VB0_57; E=js}',
                        iconCls: 'up',
                        handler: function () {
                            moveUp(checkMe);
                        }
                    }, '-',

                    {
                        id: 'MoveDown',
                        text: '@{R=Core.Strings;K=WEBJS_VB0_58; E=js}',
                        iconCls: 'down',
                        handler: function () {
                            moveDown(checkMe, pageSizeNum);
                        }
                    }, '-',
                    {
                        id: 'DeleteButton',
                        text: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}',
                        iconCls: 'del',
                        handler: function () {
                            delGroup();
                        }
                    }],
                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_VB0_59; E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VB0_55; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                if (checkMe.length > 0) {
                    grid.getSelectionModel().selectRows(checkMe);
                    checkMe = [];
                }
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.render('Grid');
            UpdateToolbarButtons();
            grid.bottomToolbar.addPageSizer();

            // resize the grid
            gridResize();
            grid.store.proxy.conn.jsonData = {};
            grid.store.load();

            $("form").submit(function () { return false; });
            // update the EXt grid size when window size is changed
            $(window).resize(gridResize);
        }
    };

}();
Ext.onReady(SW.NPM.AccountGroups.init, SW.NPM.AccountGroups);
