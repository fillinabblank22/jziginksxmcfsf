﻿Ext.namespace('SW');
Ext.namespace('SW.NPM');
Ext.ns('Ext.ux.form');

var filterText=''; //global search value

Ext.ux.form.SearchField = Ext.extend(Ext.form.TwinTriggerField, {
    initComponent: function () {
        Ext.ux.form.SearchField.superclass.initComponent.call(this);
        this.triggerConfig = {
            tag: 'span', cls: 'x-form-twin-triggers', cn: [
            { tag: "img", src: "/Orion/Admin/Accounts/images/search/clear_button.gif", cls: "x-form-trigger " + this.trigger1Class },
            { tag: "img", src: "/Orion/Admin/Accounts/images/search/search_button.gif", cls: "x-form-trigger " + this.trigger2Class }
        ]
        };

        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        // set search field text styles
        this.on('focus', function () {
            this.el.applyStyles('font-style:normal;');
        }, this);

        // reset search field text style
        this.on('blur', function () {
            if (Ext.isEmpty(this.el.dom.value) || this.el.dom.value == this.emptyText) {
                this.el.applyStyles('font-style:italic;');
            }
        }, this);
    },


    id: 'searchAccounts',
    validationEvent: false,
    validateOnBlur: false,

    trigger1Class: 'x-form-clear-trigger',
    trigger2Class: 'x-form-search-trigger',

    // default search field text style
    style: 'font-style: italic',
    emptyText: '@{R=Core.Strings;K=WEBJS_AK0_93;E=js}',

    hideTrigger1: true,
    width: 180,
    hasSearch: false,

    // this is the clear (X) icon button in the search field
    onTrigger1Click: function () {
        if (this.hasSearch) {
            this.el.dom.value = '';
            filterText = this.getRawValue();
            this.store.proxy.conn.jsonData = { accountId: '' };
            this.store.load();

            this.triggers[0].hide();
            this.hasSearch = false;
        }
    },

    //  this is the search icon button in the search field
    onTrigger2Click: function () {

        //set the global variable for the SQL query and regex highlighting (renderAccountName)
        filterText = this.getRawValue();

        if (filterText.length < 1) {
            this.onTrigger1Click();
            return;
        }

        // convert search string to SQL format (i.e. replace * with %)
        var tmpfilterText = "";
        tmpfilterText = filterText.replace(/\*/g, "%");

        // provide search string for accountId param when loading the page
        this.store.proxy.conn.jsonData = { accountId: tmpfilterText };
        this.store.load();

        this.hasSearch = true;
        this.triggers[0].show();
    }
});