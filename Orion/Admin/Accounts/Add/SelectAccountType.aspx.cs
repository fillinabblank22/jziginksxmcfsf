﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_Accounts_Add_SelectAccontType : System.Web.UI.Page
{

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AccountWorkflowManager.Init();
            //clear anny existing accounts in case we've previously added some then returned here via the back button
            if (!(AccountWorkflowManager.Accounts == null))
            {
                AccountWorkflowManager.Accounts.Clear();
            }
            if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["AccountType"]))
            {
                if (Request.QueryString["AccountType"].Equals("group", StringComparison.InvariantCultureIgnoreCase))
                {
                    radioWinGroup.Checked = true;
                    Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.AddWindowsGroups));
                }
                else if (Request.QueryString["AccountType"].Equals("SamlGroup", StringComparison.InvariantCultureIgnoreCase))
                {
                    radioSamlGroup.Checked = true;
                    Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.AddSamlGroups));
                }
            }
            else
            {
                radioOrion.Checked = true;
            }
        }
        else
        {
            radioOrion.Checked = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnNext_Click(object sender, EventArgs e)
    {

        if (radioOrion.Checked)
        {
            Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.AddSqlUser));
        }

        if (radioWinIndividual.Checked)
        {
            Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.AddWindowsUsers));
        }

        if (radioWinGroup.Checked)
        {
            Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.AddWindowsGroups));
        }
        if (radioSamlIndividual.Checked)
        {
            Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.AddSamlUsers));
        }
        if (radioSamlGroup.Checked)
        {
            Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.AddSamlGroups));
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(AccountWorkflowManager.Cancel());
    }
}
