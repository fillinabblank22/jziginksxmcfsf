﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Accounts_AddNewIndividualOrionAccount : System.Web.UI.Page
{
	protected void Page_Init(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			AccountWorkflowManager.CurrentStep = AccountWorkflowManager.AddAccountStep.AddSqlUser;
		}
	}

	protected override void OnLoad(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);

		createWizard.CreateUserStep.Title = string.Empty;

		if (AccountWorkflowManager.Accounts != null && AccountWorkflowManager.Accounts.Count > 0)
		{
			createWizard.UserName = AccountWorkflowManager.Accounts[0];

			MembershipUserCollection users = Membership.FindUsersByName(createWizard.UserName);
			if (users != null && users.Count > 0)
				Membership.DeleteUser(createWizard.UserName); // just if temp user somehow was not yet deleted.
		}
		base.OnLoad(e);
	}

	protected void CreatedUser(object sender, EventArgs e)
	{
		// set up initial views for account
		ProfileCommon thisProfile = Profile.GetProfile(createWizard.UserName);

	    AccountManagement.FillDefaultViewsForAccount(thisProfile);

	    AccountWorkflowManager.Accounts = new List<string>() { thisProfile.UserName };
	    Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.SetProperties));
    }

    protected void imgbBack_Click(object sender, EventArgs e)
	{
		Page.Response.Redirect(AccountWorkflowManager.Back());
	}

	protected void imgbCancel_Click(object sender, EventArgs e)
	{
		Page.Response.Redirect(AccountWorkflowManager.Cancel());
	}

	protected void Custom_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
	{
		//OK, this may be a little bit around the houses way of achieving this but I couldn't get another method to work, see below.
		//Origionally we were using the default CreateUserWizard validators. We then had a requirement to add a custom validator to
		//check whether the username contained a / . This was created in addition to the existing validators but the custom validator only
		//worked intermittently, seeming somehow overridden by the password validators. So after much frustration I decided to bin the other
		//validators and do all the checking myself from the one custom validator.

		args.IsValid = true;

		//username validation
		Label lbUserName = (Label)WizardStepID.ContentTemplateContainer.FindControl("UserNameError");
		lbUserName.Text = "";

		if (String.IsNullOrEmpty(createWizard.UserName))
		{
			lbUserName.Text = Resources.CoreWebContent.WEBCODE_AK0_158;
		}

		//FB25296 - do not allow leading whitespace characters
		else if (createWizard.UserName.IndexOf(" ", 0, 1) == 0)
		{
			lbUserName.Text = Resources.CoreWebContent.WEBCODE_AK0_157;
		}

		else if (createWizard.UserName.Contains("\\"))
		{
			lbUserName.Text = Resources.CoreWebContent.WEBCODE_AK0_156;
		}

		else if (createWizard.UserName.Contains("/"))
		{
			lbUserName.Text = Resources.CoreWebContent.WEBCODE_AK0_155;
		}

		//FB25296 - do not allow trailing whitespace characters
		else if (createWizard.UserName.LastIndexOf(" ", (createWizard.UserName.Length - 1), 1) == createWizard.UserName.Length - 1)
		{
			lbUserName.Text = Resources.CoreWebContent.WEBCODE_AK0_154;
		}

		// set label visibility
		if (String.IsNullOrEmpty(lbUserName.Text))
			lbUserName.Visible = false;
		else
		{
			lbUserName.Visible = true;
			args.IsValid = false;
		}
		//end username validation


		//password validation
		Label lbPassword = (Label)WizardStepID.ContentTemplateContainer.FindControl("PasswordError");
		lbPassword.Text = "";

		if (String.IsNullOrEmpty(createWizard.Password))
		{
			lbPassword.Text = Resources.CoreWebContent.WEBCODE_AK0_153;
		}

		else if (createWizard.Password != createWizard.ConfirmPassword)
		{
			lbPassword.Text = Resources.CoreWebContent.WEBCODE_AK0_152;
		}

		// set label visibility
		if (String.IsNullOrEmpty(lbPassword.Text))
			lbPassword.Visible = false;
		else
		{
			lbPassword.Visible = true;
			args.IsValid = false;
		}

		//confirm password validation
		Label lbConfirm = (Label)WizardStepID.ContentTemplateContainer.FindControl("ConfirmPasswordError");

		if (String.IsNullOrEmpty(createWizard.ConfirmPassword))
		{
			lbConfirm.Text = Resources.CoreWebContent.WEBCODE_AK0_151;
			lbConfirm.Visible = true;
			args.IsValid = false;
		}
		else
		{
			lbConfirm.Visible = false;
		}

		//end password validation
	}
}