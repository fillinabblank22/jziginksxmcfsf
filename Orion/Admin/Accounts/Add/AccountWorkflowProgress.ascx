﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountWorkflowProgress.ascx.cs" 
    Inherits="Orion_Admin_Accounts_AccountWorkflowProgress" %>

<orion:Include runat="server" File="ProgressIndicator.css" />

<div class="ProgressIndicator">
    <asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder>
</div>