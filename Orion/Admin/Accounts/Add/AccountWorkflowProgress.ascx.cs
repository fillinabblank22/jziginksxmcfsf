﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Accounts_AccountWorkflowProgress : System.Web.UI.UserControl
{
    private const string indicatorImageFolderPath = "~/orion/images/nodemgmt_art/progress_indicator/background/";

    protected void Page_Load(object sender, EventArgs e)
    {
        Reload();
    }

    public void Reload()
    {
        RenderProgressImages();
    }

    private void RenderProgressImages()
    {
        phPluginImages.Controls.Clear();

        Image[] separators = new Image[] {new Image(), new Image(), new Image()};
        Label[] texts = new Label[] {
            new Label(){Text = Resources.CoreWebContent.WEBCODE_AK0_148, CssClass = "PI_off"}, 
            new Label() {Text = Resources.CoreWebContent.WEBCODE_AK0_149, CssClass = "PI_off"}, 
            new Label(){Text = Resources.CoreWebContent.WEBCODE_AK0_150, CssClass = "PI_off"}};
        
        texts[0].Style.Add("padding-left", "10px;"); // to move text a bit to the right

        switch (AccountWorkflowManager.CurrentStep)
        {
            case AccountWorkflowManager.AddAccountStep.SelectType:
                texts[0].CssClass = "PI_on";
                separators[0].ImageUrl = string.Format("{0}pi_sep_on_off.gif", indicatorImageFolderPath);
                separators[1].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
                separators[2].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
                break;

            case AccountWorkflowManager.AddAccountStep.AddSqlUser:
            case AccountWorkflowManager.AddAccountStep.AddWindowsUsers:
            case AccountWorkflowManager.AddAccountStep.AddWindowsGroups:
            case AccountWorkflowManager.AddAccountStep.AddSamlUsers:
            case AccountWorkflowManager.AddAccountStep.AddSamlGroups:
                texts[1].CssClass = "PI_on";
                separators[0].ImageUrl = string.Format("{0}pi_sep_off_on.gif", indicatorImageFolderPath);
                separators[1].ImageUrl = string.Format("{0}pi_sep_on_off.gif", indicatorImageFolderPath);
                separators[2].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
                break;

            case AccountWorkflowManager.AddAccountStep.SetProperties:
                texts[2].CssClass = "PI_on";
                separators[0].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
                separators[1].ImageUrl = string.Format("{0}pi_sep_off_on.gif", indicatorImageFolderPath);
                separators[2].ImageUrl = string.Format("{0}pi_sep_on_off.gif", indicatorImageFolderPath);
                break;
        }

        for (int i = 0; i < 3; i++)
        {
            phPluginImages.Controls.Add(texts[i]);
            phPluginImages.Controls.Add(separators[i]);
        }
    }
}
