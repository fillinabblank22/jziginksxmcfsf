<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountsList.ascx.cs" Inherits="Orion_Admin_Accounts_Add_AccountsList" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="2.2" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Admin/Accounts/Add/AccountList.js" />
  
<style type="text/css">
    .x-tool { float: left; }
</style>       


<%-- div that holds the main table list of accounts--%>
<div id="contentDivImg" style="border:0; margin:0; width:<%= DefaultSanitizer.SanitizeHtml(this.width) %>px ">
</div>




