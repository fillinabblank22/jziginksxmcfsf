using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using System.Web.UI.HtmlControls;

public partial class Orion_Admin_Accounts_Add_AccountsList : System.Web.UI.UserControl
{

    private string _displayText = Resources.CoreWebContent.WEBCODE_VB0_295;
    public string displayText
    {
        get { return _displayText; }
        set { _displayText = value; }
    }

    private string _mode=String.Empty;
    public string mode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    private string _width = "270";
    public string width
    {
        get { return _width; }
        set { _width = value; }
    }

    private bool _collapsedState = true;
    public bool collapsedState
    {
        get { return _collapsedState; }
        set { _collapsedState = value; }
    }
    
    private int _itemCount = 0;
    public int itemCount
    {
        get { return _itemCount; }
        set { _itemCount = value; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (_mode.ToLowerInvariant()=="readonly")
        {
            if (this.Visible)
            {
                string updateScript = "SW.NPM.AccountList.Readonly = true;";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script_" + System.Guid.NewGuid().ToString(), updateScript, true);
            }
        }

        if ( !String.IsNullOrEmpty(_displayText))
        {
            if (this.Visible)
            {
                string updateScript = string.Format("SW.NPM.AccountList.displayText='{0}';", ControlHelper.EncodeJsString(displayText));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script_" + System.Guid.NewGuid().ToString(), updateScript, true);
            }
        }


            if (this.Visible)
            {
                if (_collapsedState == true)
                {

                    string updateScript = "SW.NPM.AccountList.collapsedState = true;";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script_" + System.Guid.NewGuid().ToString(), updateScript, true);
                }
                else
                {
                        string updateScript = "SW.NPM.AccountList.collapsedState = false;";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script_" + System.Guid.NewGuid().ToString(), updateScript, true);
                }

             }


    }





}
