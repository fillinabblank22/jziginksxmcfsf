﻿Ext.namespace('SW');
Ext.namespace('SW.NPM');

SW.NPM.AccountList = function () {

    Readonly: false;
    displayText: null;
    collapsedState: true;

    var dataStore;
    var accListGrid;

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };


    // render Account name    
    function renderAccountName(value) {
        // encode the Account name 
        return encodeHTML(value);
    }
    

    function renderDelete() {
        if (!SW.NPM.AccountList.Readonly) {
            var a = '<div><image style="cursor:hand;" src="/Orion/Admin/Accounts/images/icons/icon_delete.gif" /></div>';
        } else {
            var a = '<div></div>';
        }
        return a;
    };

    //used to diplay the grid contents in the tool tip
    Ext.override(Ext.ToolTip, {
        onTargetOver: function (e) {
            if (this.disabled || e.within(this.target.dom, true)) {
                return;
            }
            var t = e.getTarget(this.delegate);
            if (t) {
                this.triggerElement = t;
                this.clearTimer('hide');
                this.targetXY = e.getXY();
                this.delayShow();
            }
        },
        onMouseMove: function (e) {
            var t = e.getTarget(this.delegate);
            if (t) {
                this.targetXY = e.getXY();
                if (t === this.triggerElement) {
                    if (!this.hidden && this.trackMouse) {
                        this.setPagePosition(this.getTargetXY());
                    }
                } else {
                    this.hide();
                    this.lastActive = new Date(0);
                    this.onTargetOver(e);
                }
            } else if (!this.closable && this.isVisible()) {
                this.hide();
            }
        },
        hide: function () {
            this.clearTimer('dismiss');
            this.lastActive = new Date();
            delete this.triggerElement;
            Ext.ToolTip.superclass.hide.call(this);
        }
    });




    return {
        init: function () {
            dataStore = new ORION.WebServiceStore(
                          "/Orion/Services/AccountManagement.asmx/GetWorkflowManagerAccounts",
                [
                { name: 'account', mapping: 0 },
                { name: 'delete', mapping: 1 }
                ], "account");

            // SW.NPM.AccountList.collapsedState = false; // maintains the state of the grid so that when we re-create it will remain expanded
            // create the Grid
            if (accListGrid) {
                SW.NPM.AccountList.collapsedState = accListGrid.collapsed;
                accListGrid.destroy();
            }
            accListGrid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
        { resizable: false, fixed: false, editable: false, groupable: false, menuDisabled: true, width: 173, sortable: false, dataIndex: 'account', renderer: renderAccountName },
        { resizable: false, fixed: true, editable: false, menuDisabled: true, groupable: false, width: 40, sortable: false, dataIndex: 'delete', renderer: renderDelete }
        ],
                viewConfig: {
                    autoFill: true,
                    forceFit: true
                },
                stripeRows: true,
                trackMouseOver: false,
                height: 180,
                header: true,
                collapsed: SW.NPM.AccountList.collapsedState,
                collapsible: true,
                collapseFirst: true,
                enableColumnMove: false,
                enableColumnResize: false,
                enableHdMenu: false,
                hideHeaders: true
            });

            //checks if the delete icon was clicked and delete as appropriate
            accListGrid.on('cellclick', function (grid, rowIndex, columnIndex, e) {
                if (columnIndex == 1 && !SW.NPM.AccountList.Readonly ) {
                    var record = grid.getStore().getAt(rowIndex);
                    var accountId = record.get('account');
                    // call webservice to delete account
                    ORION.callWebService("/Orion/Services/AccountManagement.asmx", "WorkflowManagerDeleteAccount", { accountID: accountId }, function (result) {
                        accListGrid.getStore().reload();
                    });
                }
            });

            //Set title from accListGrid.onLoad (wire-up onLoad listener)
            accListGrid.store.on('load', function () { accListGrid.setTitle("&nbsp;" + accListGrid.store.getCount() + "&nbsp;" + SW.NPM.AccountList.displayText); SW.NPM.AccountList.itemCount = accListGrid.store.getCount(); }, accListGrid);
            //originally we had to add the below line to cope with some odd behaviour in one of the hosting pages
            //I belive it is no longer required but have left it commented in the issue resurfaces in QA tests
            //accListGrid.setTitle("&nbsp;" + accListGrid.store.getCount() + "&nbsp;" + SW.NPM.AccountList.displayText);

            //render the tool tips
            accListGrid.on('render', function () {
                accListGrid.tip = new Ext.ToolTip({
                    view: accListGrid.getView(),
                    target: accListGrid.getView().mainBody,
                    delegate: '.x-grid3-row',
                    trackMouse: true,
                    renderTo: document.body,
                    listeners: {
                        beforeshow: function updateTipBody(tip) {
                            var record = accListGrid.getStore().getAt(tip.view.findRowIndex(tip.triggerElement));
                            if (record) {
                                var accountId = record.get('account');
                                tip.body.dom.innerHTML = encodeHTML(accountId);
                            }
                        }
                    }
                });
            });

            if( !$('#contentDivImg')[0] ) return; // avoid error if dom element isn't on the page

            //render the grid to the specified div in the page
            accListGrid.render('contentDivImg');
            accListGrid.store.proxy.conn.jsonData = {};

            // set the item count
            SW.NPM.AccountList.itemCount = accListGrid.store.getCount();

            accListGrid.setTitle("&nbsp;" + accListGrid.store.getCount() + "&nbsp;" + SW.NPM.AccountList.displayText);
            accListGrid.store.load();
        }
    };

} ();


    Ext.onReady(SW.NPM.AccountList.init, SW.NPM.AccountList);

    
