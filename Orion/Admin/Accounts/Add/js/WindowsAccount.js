﻿Ext.namespace('SW');
Ext.namespace('SW.NPM');

// global variable list object to retain selected accounts within grid
var prevAccountList = [];

// compare contents of two arrays
Array.prototype.subtract = function(array2) {
    var array1 = this;

    if (array1.length == 0) return array1;

    var s = []; 
    for (i = 0; i < array1.length; i++) {
        var found = false;
        for (j = 0; j < array2.length; j++) {
            if (array1[i] == array2[j]) {
                found = true;
                break;
            }
        }
        if (!found) s.push(array1[i]);
    }
    return s;
};


function showHideElement(id, isVisible) {
    var el = new String();

    el = document.getElementById(id).style.visibility;

    if (isVisible == false) {
        document.getElementById(id).style.visibility = "hidden";
        document.getElementById(id).style.display = "none";
    }
    else {
        document.getElementById(id).style.visibility = "visible";
        document.getElementById(id).style.display = "block";
    }
};

function hasAccountsSelected() {

    // get the AccountList control item count 
    var accCount = SW.NPM.AccountList.itemCount;

    // show warning if account list has no items and cancel postback
    if (accCount < 1) {
        updateErrorTextField(String.format("@{R=Core.Strings;K=WEBJS_AK0_94;E=js}", globVarButtonText), "error", "search");
        return false;
    }
    else {
        updateErrorTextField('', '', '');
        return true;
    }
};

function getQsValueByItem(qsItem) {
    qsItem = qsItem.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + qsItem + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    //var results = regex.exec(document.URL);
    if (results == null)
        return "";
    else
        return results[1];
}


// global variable used to set grid 'Add select ...' button text
var globVarButtonText = (getQsValueByItem('AccountType').toLowerCase()=='group')? "@{R=Core.Strings;K=WEBJS_AK0_96;E=js}": "@{R=Core.Strings;K=WEBJS_AK0_95;E=js}";


// update the error text field with messages back from the webservice call
function updateErrorTextField(errMsg, errPriority, errType, append) {
    
    var divSrchErrText = $('#divSrchErrorText');
    var divCredErrText = $('#InerWindowsAccountDiv .explicit-credentials .message-summary');
    var divAuthErrText = $('#InerWindowsAccountDiv .process-credentials .message-summary');

	var divErrText;

    // determine the message type and so the correct error message div element is changed
    switch (errType.toLowerCase()) {
		case 'search':
			divErrText = divSrchErrText;
			break;
		case 'credentials':
			divErrText = divCredErrText;
			break;
		case 'authentication':
			divErrText = divAuthErrText;
			break;
		// neither type was found (i.e. an empty string we use to clear all messages from the web page).
		default:
			divErrText = divSrchErrText.add(divCredErrText).add(divAuthErrText);
			break;
	}

	if (!append) {
		divErrText.find('.message').not('.static').remove();
	}

	// check if we actually received an error message; if not then don't show any error message divs.
    if (errMsg != '') {
	    var message = $('<div />')
		    .addClass('message')
		    .addClass(errPriority)
		    .html(errMsg);

	    divErrText.append(message);
	    message.show();
    }
};


// go do the Search
function goSearch() {

    // if a search is already in progress, prevent any further ones from occurring
    if (SW.NPM.WindowsAccount.searchInProgress) return;
    
    
    var search = document.getElementById('txtSearchAccount').value;
    var user = document.getElementById(txtUsernameID).value;
    var password = document.getElementById('txtPassword').value;

    // locate the start of the querystring from within the URL
    var accounttype = getQsValueByItem('AccountType');

    updateErrorTextField('', '', '');

	var explicitOption = $('#InerWindowsAccountDiv .explicit-credentials .credentials-option :radio'),
		isExplicitSelected = explicitOption.is(':checked');

    // user name is mandatory if user chose to provide credentials
    if (isExplicitSelected && user == '') {
	    updateErrorTextField("@{R=Core.Strings;K=WEBJS_AK0_98;E=js}", "warning", "credentials");
    } else {

	    // check whether a search string has been entered
	    if (search == '') {
		    updateErrorTextField("@{R=Core.Strings;K=WEBJS_AK0_97;E=js}", "warning", "search");
	    } else {

		    //add implicit wildcard * to end of string
		    var x = search.toString().substring(search.length - 1);
		    if (x != "*") { search += "*"; }

		    //replace all multiple *'s to a single * instance
		    search = search.replace(/\*{2,}/, "*");

		    // new search being performed, so flush the prevAccountList array (otherwise we'll end up removing all existing items from the AccountList control!)
		    prevAccountList = [];

		    // reinitialize the search grid and acount list controls
		    SW.NPM.WindowsAccount.init(search, isExplicitSelected ? user : '', isExplicitSelected ? password : '', accounttype);
		    SW.NPM.AccountList.init();
	    }
    }

};

function testAuthentication() {
	var container = $('.process-credentials .test-credentials'),
		button = container.find('.sw-btn');

	if (button.is('.sw-btn-disabled')) {
		return;
	}

	updateErrorTextField('', '', '');

	var accounttype = getQsValueByItem('AccountType');

	SW.NPM.WindowsAccount.testAuthentication(accounttype);
}


SW.NPM.WindowsAccount = function () {

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // column rendering functions
    function renderCredential(value) {
        return encodeHTML(value);
    };


    // update the AccountList control with selected/deselected records
    function updateAccountList(selectionModel) {

        // add the accountId of the selected items from the selectionModel, to the curAccountList array
        var curAccountList = [];
        grid.getSelectionModel().each(function (rec) {
            curAccountList.push(rec.data["account"]);
        });

        var addedIds;
        var deletedIds;

        // compare lists to find new selected items
        addedIds = curAccountList.subtract(prevAccountList);
        // compare lists to find deselected items which were previoulsy selected
        deletedIds = prevAccountList.subtract(curAccountList);
        // set the previous account list selection to the current account list selection
        prevAccountList = curAccountList;

        // call webservice to add/delete accounts in AccountList
        ORION.callWebService("/Orion/Services/AccountManagement.asmx", "WorkflowManagerAddDeleteAccounts", { addAccountIds: addedIds, deleteAccountIds: deletedIds }, function (result) {
            SW.NPM.AccountList.init();
        });
    };


    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;

    // default the search in progress flag
    searchInProgress: false;

    return {
        init: function (searchstring, user, password, accounttype) {

            pageSizeNum = 20; //fix the record count displayed in the grid to 20 items.

            selectorModel = new Ext.grid.CheckboxSelectionModel();

            // pass the selection
            selectorModel.on("selectionchange", updateAccountList, this, { buffer: 500 });

            dataStore = new ORION.WebServiceStore(
                            "/Orion/Services/AccountManagement.asmx/GetWindowsAccounts",
                            [
                            { name: 'account', mapping: 0 }
                            ],
                            "account");

            dataStore.autoDestroy = true;


            if (grid) {
                //always destroy grid if it exists otherwise mulitple grids will appear with each subsequent search.
                grid.destroy();
            }


            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                                selectorModel,
                                { header: '@{R=Core.Strings;K=WEBJS_AK0_99;E=js}', sortable: true, dataIndex: 'account', renderer: renderCredential }
                            ],

                sm: selectorModel,

                viewConfig: {
                    forceFit: false
                },

                width: 355,
                autoExpandColumn: true,
                height: 300,
                stripeRows: true,
                trackMouseOver: false,

                bbar: new Ext.Toolbar(
                            {
                                height: 30,

                                items: [

                                    // begin using the right-justified button container
                                    '->', // same as {xtype: 'tbfill'}, // Ext.Toolbar.Fill
                                    {
                                        xtype: 'tbtext',
                                        id: 'recordCount',
                                        text: ' ',
                                        style: {
                                            marginTop: '10px'
                                        }
                                    },
                                    ' '
                                ]
                            })
            });

            // refresh the bottom toolbar record count when the grid store loads/reloads
            grid.store.on('load', function () {

                with (grid.store.reader.jsonData.d) {

                    // check the number of records returned (Note: this is NOT the number of data rows returned, its numberOfRecords within the webservice)
                    if (TotalRows > 250) {
                        grid.getBottomToolbar().items.items[1].el.innerHTML = "@{R=Core.Strings;K=WEBJS_AK0_100;E=js}";
                    }
                    else {
                        grid.getBottomToolbar().items.items[1].el.innerHTML = String.format("@{R=Core.Strings;K=WEBJS_AK0_101;E=js}", grid.store.getCount());
                    }

                    // check message data being returned from webservice to update the error message div elelments 
                    updateErrorTextField(EmptyText, MessagePriority, MessageType);
                }

                // search ends - hide overlay element reset the search in progress flag to allow further searches
                showHideElement('Searching', false);
                showHideElement('Grid', true);
                SW.NPM.WindowsAccount.searchInProgress = false;
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.render('Grid');
            
            if (!searchstring == '') {
                grid.getBottomToolbar().items.items[1].el.innerHTML = "@{R=Core.Strings;K=WEBJS_AK0_102;E=js}";
                SW.NPM.AccountList.displayText = String.format("@{R=Core.Strings;K=WEBJS_AK0_103;E=js}", globVarButtonText);
                grid.store.proxy.conn.jsonData = { searchstring: searchstring, username: user, password: password, accounttype: accounttype };

                //search begins - set flag to prevent further searches from occurring while this one is running 
                SW.NPM.WindowsAccount.searchInProgress = true;
                showHideElement('Grid', false);
                showHideElement('Searching', true);

                grid.store.load();

            }

            $("form").submit(function () { return false; });
        },

		onLoad: function () {
			// call init w\out params to initialize the Account Grids
        	SW.NPM.WindowsAccount.init();

        	var onChange = function () {
				// we always want to clear messages when we switch workflow
        		updateErrorTextField('', '', '');

		        var process = $('#InerWindowsAccountDiv .process-credentials'),
			        explicit = $('#InerWindowsAccountDiv .explicit-credentials'),
		        	processOption = process.find('.credentials-option :radio');

	        	var isProcessSelected = processOption.is(':checked');
	        	if (isProcessSelected) {
	        		// enable the button
					var button = process.find('.test-credentials .sw-btn');
					button.removeClass('sw-btn-disabled aspNetDisabled');

					// disable name & pwd inputs
					explicit.find(':text, :password').prop('disabled', 'disabled');
			        
		        } else {
					// disable the button
					var button = process.find('.test-credentials .sw-btn');
					button.addClass('sw-btn-disabled aspNetDisabled');

	        		// enable name & pwd inputs
			        explicit.find(':text, :password').prop('disabled', '');
		        }
	        };

	        $('#InerWindowsAccountDiv .credentials-option :radio').change(onChange);

			// trigger the handler to enable \ disable controls according to current option
	        onChange();
        },

        testAuthentication: function (accounttype) {
	        var container = $('#InerWindowsAccountDiv .process-credentials .test-credentials'),
				button = container.find('.sw-btn'),
		        indicator = container.find('.progress-indicator');
	        var options = $('#InerWindowsAccountDiv .credentials-option :radio');

	        var showProgress = function() {
		        button.addClass('sw-btn-disabled aspNetDisabled');
		        indicator.show();

		        options.prop('disabled', 'disabled');
	        };
			var hideProgress = function() {
				button.removeClass('sw-btn-disabled aspNetDisabled');
				indicator.hide();

				options.prop('disabled', '');
			}

	        showProgress();

			ORION.callWebService(
				"/Orion/Services/AccountManagement.asmx", "TestProcessAuthentication", {accounttype: accounttype},
				function (result) {
					hideProgress();

					$.each(result, function() {
						updateErrorTextField(this.Message, this.MessageType, 'authentication', true);
					})
				},
				function (error) {
					hideProgress();

					updateErrorTextField(error, 'error', 'authentication');
				});
		}
    };

} ();

Ext.onReady(SW.NPM.WindowsAccount.onLoad, SW.NPM.WindowsAccount);
