<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SamlAccount.aspx.cs" Inherits="Orion_Admin_Accounts_Add_SamlAccount" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_400 %>" %>

<%@ Register Src="~/Orion/Admin/Accounts/Add/AccountWorkflowProgress.ascx" TagPrefix="orion" TagName="AccountWorkflowProgress" %>
<%@ Register Src="~/Orion/Admin/Accounts/Add/AccountsList.ascx" TagPrefix="orion" TagName="AccountsList" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
	<orion:Include runat="server" File="NodeMNG.css" />
	<orion:Include runat="server" File="Admin.css" />
	<orion:Include runat="server" File="ProgressIndicator.css" />
	<orion:Include runat="server" File="Nodes/js/TimeoutHandling.js" />
	<orion:Include runat="server" Framework="Ext" FrameworkVersion="2.2" />
	<orion:Include runat="server" File="OrionCore.js" />

	<style type="text/css">
		.sw-validation-error {
			padding-left: 6px;
		}
	</style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
	<table width="100%" id="breadcrumb">
		<tr>
			<td>
				<h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
			</td>
		</tr>
	</table>
	<asp:UpdatePanel runat="server" ID="UpdatePanel1">
		<ContentTemplate>
			<div class="GroupBoxAccounts" id="SamlAccountDiv" style="min-width: 480px;">
				<orion:AccountWorkflowProgress ID="AccountWorkflowProgress2" runat="server" />
				<div id="InerSamlAccountDiv" style="padding: 10px 10px 5px 10px;">
					<h2>
						<asp:Literal runat="server" ID="litPageHeader"></asp:Literal></h2>
					<asp:CreateUserWizard runat="server" LabelStyle-Width="150"
						TextBoxStyle-Width="200" Width="100%" RequireEmail="False" OnCreatedUser="CreatedUser"
						LoginCreatedUser="False" ID="createWizard" AutoGeneratePassword="True"
						Style="margin-left: 15px;">
						<TextBoxStyle Width="200px" />
						<LabelStyle Width="150px" />
						<WizardSteps>
							<asp:CreateUserWizardStep ID="WizardStepID" runat="server">
								<ContentTemplate>
                                    <table style="font-size: 100%; width: 100%;">
                                        <tr>
                                            <td align="right" style="width: 150px;">
                                                <asp:Label ID="NameIDLabel" runat="server" AssociatedControlID="UserName"><asp:Literal runat="server" ID="litPageNameID"></asp:Literal></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="UserName" runat="server" Width="200px" onkeypress="if (event.keyCode==13) {return false;}"></asp:TextBox>
												<asp:Label ID="NameIDError" Visible="false" runat="server" CssClass="sw-suggestion sw-suggestion-fail sw-validation-error"></asp:Label>
											</td>
										</tr>
										<tr>
											<td align="center" colspan="2">
												<asp:CustomValidator ID="Custom" runat="server" OnServerValidate="Custom_ServerValidate" Display="None"
													ValidationGroup="createWizard"></asp:CustomValidator>
											</td>
										</tr>
										<tr>
											<td align="center" colspan="2" style="color: Red;">
												<asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
											</td>
										</tr>
									</table>
								</ContentTemplate>
								<CustomNavigationTemplate>
									<table width="98%" cellpadding="0" cellspacing="0">
										<tr>
											<td class="leftLabelColumn">&nbsp;
											</td>
											<td style="width: 100%;">
												<div class="sw-btn-bar-wizard">
													<orion:LocalizableButton ID="imgbBack" runat="server" DisplayType="Secondary" LocalizedText="Back" OnClick="imgbBack_Click" />
													<orion:LocalizableButton ID="ImageButton1" runat="server" DisplayType="Primary" LocalizedText="Next" CommandName="MoveNext" ValidationGroup="createWizard" OnClientClick="clickAndDisable(this)" />
													<orion:LocalizableButton ID="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
												</div>
											</td>
										</tr>
									</table>
								</CustomNavigationTemplate>
							</asp:CreateUserWizardStep>
							<asp:CompleteWizardStep runat="server">
							</asp:CompleteWizardStep>
						</WizardSteps>
					</asp:CreateUserWizard>
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript">
		function clickAndDisable(link) {
			// disable subsequent clicks
			link.onclick = function (event) {
				event.preventDefault();
			}
		}
	</script>
</asp:Content>
