﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Accounts_Add_SamlAccount : System.Web.UI.Page
{
    private static readonly Log _log = new Log();
    private bool _isGroupAccountType;

    protected void Page_Load(object sender, EventArgs e)
    {
        ValidateQueryString();
        if (!IsPostBack)
        {
            AccountWorkflowManager.CurrentStep = _isGroupAccountType
                ? AccountWorkflowManager.AddAccountStep.AddSamlGroups
                : AccountWorkflowManager.AddAccountStep.AddSamlUsers;
        }
        if (_isGroupAccountType)
        {
            if (WizardStepID.Controls[0].FindControl("litPageNameID") != null) ((Literal)WizardStepID.Controls[0].FindControl("litPageNameID")).Text = Resources.CoreWebContent.WEBDATA_AK0_533;
            if (litPageHeader != null) litPageHeader.Text = Resources.CoreWebContent.WEBDATA_AK0_534;
        }
        else
        {
            if (WizardStepID.Controls[0].FindControl("litPageNameID") != null)
                ((Literal)WizardStepID.Controls[0].FindControl("litPageNameID")).Text =
                    Resources.CoreWebContent.WEBDATA_AK0_531;
            if (litPageHeader != null) litPageHeader.Text = Resources.CoreWebContent.WEBDATA_AK0_532;
        }
    }

    protected void CreatedUser(object sender, EventArgs e)
    {
        // set up initial views for account
        ProfileCommon thisProfile = Profile.GetProfile(createWizard.UserName);

        thisProfile.AccountType =
            _isGroupAccountType ? (int) GroupAccountType.SamlGroup : (int) GroupAccountType.SamlUser;
        _log.Info($"Creating user of type {thisProfile.AccountType} with name: {thisProfile.UserName}");

        AccountManagement.FillDefaultViewsForAccount(thisProfile);

        AccountWorkflowManager.Accounts = new List<string>() { thisProfile.UserName };
        
        Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.SetProperties));
    }

    protected void Custom_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        Label lbUserName = (Label)WizardStepID.ContentTemplateContainer.FindControl("NameIDError");
        lbUserName.Text = "";

        _log.Debug($"Validating user: {createWizard.UserName} which has {(_isGroupAccountType ? "group":"individual")} account.");
        lbUserName.Text = _isGroupAccountType 
            ? GetErrorMessageGroupAccount(createWizard.UserName) 
            : GetErrorMessageIndividualAccount(createWizard.UserName);

        // set label visibility
        if (String.IsNullOrEmpty(lbUserName.Text))
            lbUserName.Visible = false;
        else
        {
            lbUserName.Visible = true;
            args.IsValid = false;
        }
    }
    
    protected void imgbBack_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(AccountWorkflowManager.Back());
    }

    protected void imgbCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(AccountWorkflowManager.Cancel());
    }

    private void ValidateQueryString()
    {
        if (string.IsNullOrEmpty(Request.QueryString["AccountType"]))
            throw new NotImplementedException("Querystring item for AccountType required.");

        if (Request.QueryString["AccountType"].Equals("samlgroup", StringComparison.InvariantCultureIgnoreCase))
        {
            _isGroupAccountType = true;
        }
        else if (Request.QueryString["AccountType"].Equals("samlindividual", StringComparison.InvariantCultureIgnoreCase))
        {
            _isGroupAccountType = false;
        }
        else
        {
            throw new NotImplementedException(String.Format("QueryString value for AccountType: {0} is not supported.",
                Request.QueryString["AccountType"]));
        }
    }

    private static string GetErrorMessageIndividualAccount(string username)
    {
        if (String.IsNullOrEmpty(username))
        {
            return Resources.CoreWebContent.WEBCODE_AK0_539;
        }
        //FB25296 - do not allow leading whitespace characters
        if (username.IndexOf(" ", 0, 1) == 0)
        {
            return Resources.CoreWebContent.WEBCODE_AK0_538;
        }

        if (username.Contains("\\"))
        {
            return Resources.CoreWebContent.WEBCODE_AK0_537;
        }

        if (username.Any(Char.IsWhiteSpace))
        {
            return Resources.CoreWebContent.WEBCODE_AK0_536;
        }

        //FB25296 - do not allow trailing whitespace characters
        return username.LastIndexOf(" ", (username.Length - 1), 1) == username.Length - 1
            ? Resources.CoreWebContent.WEBCODE_AK0_535
            : "";
    }

    private static string GetErrorMessageGroupAccount(string username)
    {
        if (String.IsNullOrEmpty(username))
        {
            return Resources.CoreWebContent.WEBCODE_AK0_534;
        }

        //FB25296 - do not allow leading or trailing whitespace characters
        if (username.IndexOf(" ", 0, 1) == 0)
        {
            return Resources.CoreWebContent.WEBCODE_AK0_533;
        }

        return username.LastIndexOf(" ", (username.Length - 1), 1) == username.Length - 1
            ? Resources.CoreWebContent.WEBCODE_AK0_532
            : "";
    }
}