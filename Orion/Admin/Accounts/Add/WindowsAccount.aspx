<%@ page language="C#" autoeventwireup="true" codefile="WindowsAccount.aspx.cs" inherits="Orion_Admin_Accounts_Add_WindowsAccount" masterpagefile="~/Orion/Admin/OrionAdminPage.master" title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_400 %>" %>

<%@ register src="~/Orion/Admin/Accounts/Add/AccountWorkflowProgress.ascx" tagprefix="orion" tagname="AccountWorkflowProgress" %>
<%@ register src="~/Orion/Admin/Accounts/Add/AccountsList.ascx" tagprefix="orion" tagname="AccountsList" %>
<%@ register src="~/Orion/Controls/IconHelpButton.ascx" tagprefix="orion" tagname="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:include runat="server" file="NodeMNG.css" />
    <orion:include runat="server" file="Admin.css" />
    <orion:include runat="server" file="ProgressIndicator.css" />
    <orion:include runat="server" file="Nodes/js/TimeoutHandling.js" />
    <orion:include runat="server" framework="Ext" frameworkversion="2.2" />
    <orion:include runat="server" file="OrionCore.js" />
    <orion:include runat="server" file="Admin/Accounts/Add/js/WindowsAccount.js" />
    <script language="javascript" type="text/javascript">
        var txtUsernameID = '<%=txtUsername.ClientID%>';
    </script>

    <style type="text/css">
        span.Label {
            white-space: nowrap;
        }

        .smallText {
            color: #979797;
            font-size: 9px;
        }

        .add {
            background-image: url(/Orion/Admin/Accounts/images/icons/add_16x16.gif) !important;
        }

        #adminContent .dialogBody table td {
            padding-top: 5px;
            padding-left: 10px;
        }

        #adminContent .dialogBody ul {
            margin-left: 20px;
            list-style-type: square;
        }

        #Grid td {
            padding-right: 0;
            padding-bottom: 0;
            vertical-align: middle;
        }

        .message-summary .message {
            font-size: 11px;
            border-radius: 0;
            display: inline-block;
            position: relative;
            zoom: 1;
            padding: 5px 15px 6px 5px;
            display: none;
        }

        .message-summary .warning {
            background: #FCF8e0;
            border: 1px solid #D1BC6B;
            color: #8e6708;
        }

        .message-summary .success {
            background: #E7F4DF;
            color: #1D781D;
            font-weight: bold;
            border: 1px solid #30B230;
        }

        .message-summary .error {
            background: #FFE4E0;
            color: #D50000;
            border: 1px solid #D50000;
            font-weight: bold;
        }

        .message-summary .static {
            display: block;
        }

        .process-credentials .message-summary,
        .explicit-credentials .message-summary {
            padding-left: 17px;
        }

        .form-entry {
            padding: 4px 7px 4px 7px;
        }

            .form-entry .form-control {
                overflow: hidden;
            }

                .form-entry .form-control label {
                    display: block;
                    float: left;
                    padding-left: 10px;
                }

                .form-entry .form-control input {
                    display: block;
                    margin-left: 11%;
                }

            .form-entry .helpfulText {
                padding-left: 11%;
            }

        .credentials-option {
            padding-left: 0;
        }

            .credentials-option input {
                margin-right: 4px;
            }

        .progress-indicator {
            font-weight: bold;
            padding-left: 20px;
            background: url('/orion/Admin/Accounts/images/animated/loading_gen_16x16.gif') top left no-repeat;
            display: none;
        }

        .process-credentials .test-credentials .progress-indicator {
            margin-left: 7px;
        }
    </style>

</asp:Content>


<asp:content id="Content2" contentplaceholderid="adminContentPlaceholder" runat="Server">
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1" >
        <ContentTemplate>
            <div class="GroupBoxAccounts" id="WindowsAccountDiv" style="min-width: 480px;">
                <orion:AccountWorkflowProgress ID="ProgressIndicator" runat="server" />
                <div id="InerWindowsAccountDiv" style="padding: 10px 10px 5px 10px;">
                    <h1><font style="font-size:large"><asp:Literal runat="server" id="litPageSubTitle"></asp:Literal></font></h1>
                    <br />

                    <div id="altCredentials" style="width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="1%" rowspan="12">&nbsp;</td>
                                <td colspan="2">&nbsp;</td>
                            </tr>

                            <!-- user credentials section -->
                            <tr>
                                <td colspan="2" style="height:25px">
                                   <h3><font style="font-size:small; color:#5f5f5f;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_411) %></font></h3>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div class="process-credentials">
                                        <div class="form-entry credentials-option">
                                            <span class="radio">
                                                <asp:RadioButton runat="server" ID="rbProcessCredentials" 
                                                    GroupName="credentials-option"
                                                    Checked="True" />
                                            </span>
                                            
                                            <orion:IconHelpButton runat="server" HelpUrlFragment="OrionCoreAGCreatingNewAccounts" />
                                        </div>
                                        
                                        <div class="form-entry test-credentials">
                                            <orion:LocalizableButton runat="server" ID="btnTestAuthentication"
                                                DisplayType="Small" 
                                                LocalizedText="CustomText" 
                                                Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_535 %>"                                                
                                                OnClientClick="testAuthentication(); return false;" />
                                            
                                            <span class="progress-indicator"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_538) %></span>                                        
                                        </div>
                                        
                                        <div class="message-summary">
                                            <asp:PlaceHolder runat="server" id="phCantGetProcessIdentity" Visible="False">
                                                <div class="message error static">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_536) %>                                                
                                                </div>
                                            </asp:PlaceHolder>
                                        </div>
                                    </div>
                                    
                                    <div class="explicit-credentials">
                                         <div class="form-entry credentials-option">
                                             <span>
                                                <asp:RadioButton runat="server" ID="rbExplicitCredentials"
                                                    GroupName="credentials-option"
                                                    Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_537 %>"/>                                             
                                             </span>
                                         </div>

                                        <div class="form-entry">
                                            <div class="form-control">
                                                <label for="<%= DefaultSanitizer.SanitizeHtml(txtUsername.ClientID) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_73) %></label>
                                                <input type="text" id="txtUsername" runat="server"
                                                       style="width: 250px;"
                                                       value=""
                                                       title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_412 %>"
                                                       onkeypress="if (event.keyCode==13) {return false;}"/>
                                            </div>

                                            <div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_413) %></div>
                                        </div>
                                    
                                        <div class="form-entry">
                                            <div class="form-control">
                                                <label for="txtPassword"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></label>
                                        
                                                <input type="password" id="txtPassword" 
                                                    style="width:250px;" 
                                                    value="" 
                                                    title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_414) %>" 
                                                    onkeypress="if (event.keyCode==13) {return false;}"
                                                    autocomplete="off">
                                            </div>
                                        
                                            <div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_415) %></div>
                                        </div>
                                    
                                        <div class="message-summary">
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <!-- search for account/group section -->
                             <tr>
                                <td colspan="2" style="height:40px">
                                     &nbsp;
                                </td>                
                            </tr>  
                            <tr>
                                <td colspan="2" style="height:25px">
                                     <h3><font style="font-size:small; color:#5f5f5f;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_417) %></font></h3>
                                </td>                
                            </tr>
                           
                            <tr>
                                <td style="text-align: left; width: 11%">
                                    <asp:Literal runat="server" id="litNameLabelText"></asp:Literal>&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td>
                                    <input type="text" id="txtSearchAccount" value="" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_418) %>" style="width:250px;" onkeypress="if (event.keyCode==13) {goSearch(); return false;}">
                                    <orion:LocalizableButton id="btnSearch" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_131 %>" OnClientClick="goSearch(); return false;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="helpfulText">
                                    <asp:Literal runat="server" id="litFormatLabelText"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="text-align:left">
                                    <div class="message-summary" id="divSrchErrorText">
                                        <asp:PlaceHolder runat="server" id="phSearchError" Visible="False">
                                            <div class="message error static">
                                                <%: Resources.CoreWebContent.WEBCODE_AK0_169 %>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                               </td>
                            </tr>
                        </table>     
                    </div>

                    <!-- add search results Ext grid -->
                    <div id="tabPanel" style="width:100%">
                        <table cellpadding="0" cellspacing="0" width="100%">
	                        <tr>
                                <td width="1%" rowspan="8">&nbsp;</td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height:25px">
                                     <h3><font style="font-size:small; color:#5f5f5f;"><asp:Literal runat="server" id="litAccountLabelText"></asp:Literal></font></h3>
                                </td>                
                            </tr>
                            <tr>
                                <td width="11%" style="text-align:left;">
		                           <asp:Literal runat="server" id="litResultsLabelText"></asp:Literal>
                                </td>
                                <td>
	                                <table border="0" cellspacing="0" cellpadding="0">
                                        <tr valign="top" align="left">
                                            <td width="48%" id="gridCell" style="padding-right: 0px;">
                                                <div>
                                                    <div id="Searching" style="visibility:hidden; display:none; width:355px">
                                                    <img src="/Orion/Admin/Accounts/images/animated/loading_gen_16x16.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_419) %>"/>
                                                    &nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_420) %></b></div>
                                                    <div id="Grid" style="width:355px"></div>
                                                </div>
		                                    </td>
		                                    <td width="2%">
                                                &nbsp;
                                            </td>
                                            <td width="50%" align="left" style="padding-right: 0px;">
                                                <orion:AccountsList ID="AccountsList" runat="server"/>
                                            </td>
	                                    </tr>
	                                </table>
                                </td>                
                            </tr>
                        </table>
	                </div>
    
                    <!-- add navigation buttons -->
                    <table width="98%" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td width="99%">
                                <div class="sw-btn-bar-wizard">
                                    <orion:LocalizableButton id="imgbBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="imgbBack_Click"/>
                                    <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next" OnClick="imgbNext_Click" OnClientClick="return hasAccountsSelected();" />
                                    <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
                                </div>
                            </td>
                        </tr>
                    </table>     
                </div>    
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   
</asp:content>


