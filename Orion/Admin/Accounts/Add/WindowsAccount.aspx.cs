﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Security.Principal;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Accounts_Add_WindowsAccount : System.Web.UI.Page
{
    private static Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["AccountType"]))
        {
            if (Request.QueryString["AccountType"].Equals("group", StringComparison.InvariantCultureIgnoreCase))
                AccountWorkflowManager.CurrentStep = AccountWorkflowManager.AddAccountStep.AddWindowsGroups;
            else AccountWorkflowManager.CurrentStep = AccountWorkflowManager.AddAccountStep.AddWindowsUsers;
        }
        //else
        //{
        //    //set the error msg div message text and styles to display the error.
        //    divSrchErrorText.InnerHtml =
        //        "<img style='vertical-align:middle; padding: 7px 0px 7px 0px;' src='/Orion/Admin/Accounts/images/icons/disable.png'/>&nbsp;&nbsp;No accounts selected, please use the search to find accounts.&nbsp;";
        //    divSrchErrorText.Style["display"] = "block";
        //}
    }
   
    protected void Page_Load(object sender, EventArgs e)
    {
        AccountsList.collapsedState = false;

        // get the user account name if logged in as WindowsUser or VirtualUser and prepopulate the username field.
        IIdentity wi = (IIdentity) HttpContext.Current.Session["CurrentWindowsIdentity"];
        
        //// if no CurrentWindowsIdentity available, then must be a forms auth account try Request.LogonUserIdentity
        //if (wi == null)
        //{
        //    wi = HttpContext.Current.Request.LogonUserIdentity;  // no good, as it returns NTAuthority on autologin!
        //} 
        
        // we have an identity so pre-populate the username credential control
        if (wi != null)
        {
            txtUsername.Value = wi.Name;
        }

        //else
        //{
        //    txtUsername.Value = "WI is null!!!";//Profile.UserName;
        //}



        // check the querystring AccountType value to set the web page text controls accordingly
        if (!string.IsNullOrEmpty(Request.QueryString["AccountType"]))
        {

            switch (Request.QueryString["AccountType"].ToLower(CultureInfo.InvariantCulture))
            {
                case "group":
                    if (litPageSubTitle != null) litPageSubTitle.Text = Resources.CoreWebContent.WEBCODE_AK0_159;
                    if (litNameLabelText != null) litNameLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_160;
                    if (litFormatLabelText != null) litFormatLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_161;
                    if (litResultsLabelText != null) litResultsLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_162;
                    if (litAccountLabelText != null) litAccountLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_163;
                    //AccountType = "group";
                    break;

                case "individual":
                    if (litPageSubTitle != null) litPageSubTitle.Text = Resources.CoreWebContent.WEBCODE_AK0_164;
                    if (litNameLabelText != null) litNameLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_165;
                    if (litFormatLabelText != null) litFormatLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_166;
                    if (litResultsLabelText != null) litResultsLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_167;
                    if (litAccountLabelText != null) litAccountLabelText.Text = Resources.CoreWebContent.WEBCODE_AK0_168;
                    break;

                default:
                    throw new NotImplementedException(String.Format("QueryString value for AccountType: {0} is not supported.",
                                                                    Request.QueryString["AccountType"]));
            }
        }
        else
        {
            throw new NotImplementedException("Querystring item for AccountType required.");
        }

        DisplayAuthenticationOptions();
    }

    private void DisplayAuthenticationOptions()
    {
        WindowsIdentity currentIdentity = GetCurrentIdentity();
        if (currentIdentity == null)
        {
            // this should not happen
            // in case we can't even access current Identity, disable this option entirely

            rbProcessCredentials.Text = string.Format(Resources.CoreWebContent.WEBDATA_VK0_04, "IIS Application Pool Identity");
            rbProcessCredentials.Enabled = false;
            btnTestAuthentication.Enabled = false;
            phCantGetProcessIdentity.Visible = true;
            rbExplicitCredentials.Checked = true;

            return;
        }

        if (LDAPAuthHelper.IsLDAPAuthenticationEnabled())
        {
            //we need explicit credentials to connect to LDAP server 
            rbProcessCredentials.Enabled = false;
            rbExplicitCredentials.Checked = true;
        }

        AccountName name = AccountSearchHelper.ParseAccountName(currentIdentity.Name);
        rbProcessCredentials.Text = string.Format(Resources.CoreWebContent.WEBDATA_VK0_04, name.GetFullName());
    }

    private static WindowsIdentity GetCurrentIdentity()
    {
        try
        {
            return WindowsIdentity.GetCurrent();
        }
        catch (Exception exception)
        {
            _log.WarnFormat("Could not access current Windows user account. Verify that IIS Application Pool" +
                            " account has ControlPrincipal permission. Details:\r\n{0}", exception);
        }
        return null;
    }

    protected void imgbNext_Click(object sender, EventArgs e)
    {
               
       // Assign selected users to workflow manager
       // !!! Page must validate that at least one user selected. If not, ask user to select !!!
        if (AccountWorkflowManager.Accounts != null)
        {
            if (AccountWorkflowManager.Accounts.Count() > 0)
            {
                Page.Response.Redirect(AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.SetProperties));
            }
        }
        
        // We shouldn't really get here as the OnClientClick call to javascript function will check the AccountList control has at least one item.
        // ... but just to be on the safe side, lets set the error msg div message text and styles to display the error.
        
        // NOTE: there appears to be a rendering issue on the Ext objects after a PostBack occurs. The Search Results Grid and AccountList control fail 
        // to display, however they then appear OK when a new search is actioned - so this is not a show stopper!
        phSearchError.Visible = true;
    }

    protected void imgbBack_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(AccountWorkflowManager.Back());
    }

    protected void imgbCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(AccountWorkflowManager.Cancel());
    }

}
