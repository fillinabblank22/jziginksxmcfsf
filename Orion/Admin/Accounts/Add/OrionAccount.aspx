<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrionAccount.aspx.cs" Inherits="Orion_Admin_Accounts_AddNewIndividualOrionAccount" 
MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_400 %>"%>

<%@ Register Src="~/Orion/Admin/Accounts/Add/AccountWorkflowProgress.ascx" TagPrefix="orion" TagName="AccountWorkflowProgress" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="NodeMNG.css" />
    <orion:Include runat="server" File="Admin.css" />
    <orion:Include runat="server" File="ProgressIndicator.css" />
    <orion:Include runat="server" File="Nodes/js/TimeoutHandling.js" />
    <style type="text/css">
        .sw-validation-error {
            padding-left: 6px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    
    <h1><%= DefaultSanitizer.SanitizeHtml(this.Title) %></h1>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
    
    <div class="GroupBoxAccounts" id="OrionAccountDiv" style="min-width: 480px; ">
	<orion:AccountWorkflowProgress ID="AccountWorkflowProgress1" runat="server" />
	<div id="InerOrionAccountDiv" style="padding: 10px 10px 5px 10px;">
            
        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_409) %></h2>
                 
        <asp:CreateUserWizard runat="server" LabelStyle-Width="150" 
            TextBoxStyle-Width="200" Width="100%" RequireEmail="False" 
            LoginCreatedUser="False" ID="createWizard" OnCreatedUser="CreatedUser" 
            style="margin-left: 15px;" >
             <TextBoxStyle Width="200px" />
             <LabelStyle Width="150px" />
             <WizardSteps>
                <asp:CreateUserWizardStep ID="WizardStepID" runat="server">
                    <ContentTemplate>
                        <table style="font-size:100%;width:100%;">
                            <tr>
                                <td align="right" style="width:150px;">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_73) %></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="UserName" runat="server" Width="200px" onkeypress="if (event.keyCode==13) {return false;}"></asp:TextBox>
                                    <asp:Label ID="UserNameError" Visible="false" runat="server" CssClass="sw-suggestion sw-suggestion-fail sw-validation-error" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="width:150px;">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="200px" onkeypress="if (event.keyCode==13) {return false;}" autocomplete="off"></asp:TextBox>                                                                
                                    <asp:Label ID="PasswordError" Visible="false" runat="server" CssClass="sw-suggestion sw-suggestion-fail sw-validation-error" ></asp:Label>
                                </td> 
                            </tr>
                            <tr>
                                <td align="right" style="width:150px;">
                                    <asp:Label ID="ConfirmPasswordLabel" runat="server" style="white-space:nowrap;" 
                                        AssociatedControlID="ConfirmPassword" autocomplete="off"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_410) %></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Width="200px" onkeypress="if (event.keyCode==13) {return false;}" autocomplete="off"></asp:TextBox>
                                    <asp:Label ID="ConfirmPasswordError" Visible="false" runat="server" CssClass="sw-suggestion sw-suggestion-fail sw-validation-error" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                        <asp:CustomValidator id="Custom" runat="server" onservervalidate="Custom_ServerValidate" Display="None"
                                         ValidationGroup="createWizard"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="color:Red;">
                                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <CustomNavigationTemplate>
                      <table width="98%" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td class="leftLabelColumn">
                                &nbsp;
                            </td>
                            <td style="width: 100%;">
                                <div class="sw-btn-bar-wizard">
                                    <orion:LocalizableButton id="imgbBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="imgbBack_Click"/>
                                    <orion:LocalizableButton id="ImageButton1" runat="server" DisplayType="Primary" LocalizedText="Next" automation="Next" CommandName="MoveNext" ValidationGroup="createWizard"/>
                                    <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
                                </div>
                            </td>
                        </tr>
                    </table>             
                
                    </CustomNavigationTemplate>
                </asp:CreateUserWizardStep>
                <asp:CompleteWizardStep runat="server">
                </asp:CompleteWizardStep>
            </WizardSteps>
        </asp:CreateUserWizard>            
    </div>
    </div>     
    </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

