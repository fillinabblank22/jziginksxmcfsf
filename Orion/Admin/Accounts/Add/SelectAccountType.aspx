<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelectAccountType.aspx.cs" Inherits="Orion_Admin_Accounts_Add_SelectAccontType"
    MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_400 %>"%>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
    
<%@ Register Src="~/Orion/Admin/Accounts/Add/AccountWorkflowProgress.ascx" TagPrefix="orion" TagName="AccountWorkflowProgress" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="NodeMNG.css" />
    <orion:Include runat="server" File="Admin.css" />
    <orion:Include runat="server" File="ProgressIndicator.css" />
    <orion:Include runat="server" File="Nodes/js/TimeoutHandling.js" />
    <script language="javascript" type="text/javascript">
        var radioOrionID = '<%=radioOrion.ClientID%>';
        var radioWinIndividualID = '<%=radioWinIndividual.ClientID%>';
        var radioWinGroupID = '<%=radioWinGroup.ClientID%>';
        var radioSamlIndividualID = '<%=radioSamlIndividual.ClientID%>';
        var radioSamlGroupID = '<%=radioSamlGroup.ClientID%>';

        function selectOption(id) {
            var el = document.getElementById(id);
            el.checked = true;
        }
    </script>

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    
	<table width="100%" id="Table1">
		<tr>
			<td>
				<h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
			</td>
		</tr>
	</table>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>

            <div class="GroupBoxAccounts" id="SelectTypeDiv" style="min-width: 480px;">
            <orion:AccountWorkflowProgress ID="ProgressIndicator" runat="server" />
            <div id="InerSelectTypeDiv" style="padding: 10px 10px 5px 10px;">
            
                <br />
                <h1 style="padding-left: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_401) %></h1>
                
                <table width="97%" cellpadding="0" cellspacing="0" style="margin-left:10px; margin-right:10px;" >
                    <tr style="background-color:#f8f8f8; height:20px;">
                        <td>&nbsp;</td>
                        <td><input type="radio" id="radioOrion" name="radioButtons" value="OrionAccount" style="margin-top:10px; cursor:pointer;" runat="server" /></td>                        
                        <td width="95%" style="padding-top:10px; font-weight:bolder;"><div style="cursor:pointer;" onclick="selectOption(radioOrionID);"><img src="/Orion/Admin/Accounts/images/icons/orion_generic_icon_orange.gif"/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_402) %></div></td>
                    </tr>
                    <tr style="background-color:#f8f8f8; height:20px;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>                        
                        <td width="95%" style="font-weight:lighter; font-size:small">&nbsp;&nbsp;&nbsp;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_403) %>&nbsp;<a href='<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCorePHAccountTypesOrionIndividual")) %>' style="text-decoration:underline; cursor:pointer;" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_404) %></a></td>
                    </tr>             
                     <tr style="height:20px;">
                        <td>&nbsp;</td>
                        <td><input type="radio" id="radioWinIndividual" name="radioButtons" value="WindowsAccount" style="margin-top:10px; cursor:pointer;" runat="server"  /></td>
                        <td width="95%" style="padding-top:10px; font-weight:bolder"><div style="cursor:pointer;" onclick="selectOption(radioWinIndividualID);"><img src="/Orion/Admin/Accounts/images/icons/windows.gif"/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_405) %></div></td>
                    </tr>
                    <tr style="height:20px;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>                        
                        <td width="95%" style="font-weight:lighter; font-size:small">&nbsp;&nbsp;&nbsp;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_407) %>&nbsp;<a href='<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCorePHAccountTypesWindowsIndividual")) %>' style="text-decoration:underline; cursor:pointer;" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_404) %></a></td>
                    </tr>   
                    <tr style="background-color:#f8f8f8; height:20px;">
                        <td>&nbsp;</td>
                        <td><input type="radio" id="radioWinGroup" name="radioButtons" value="WindowsGroup" style="margin-top:10px; cursor:pointer;" runat="server"/></td>
                        <td width="95%" style="padding-top:10px; font-weight:bolder"><div style="cursor:pointer;" onclick="selectOption(radioWinGroupID);"><img src="/Orion/Admin/Accounts/images/icons/windows.gif"/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_406) %></div></td>
                    </tr>
                    <tr style="background-color:#f8f8f8; height:20px;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>                        
                        <td width="95%" style="font-weight:lighter; font-size:small">&nbsp;&nbsp;&nbsp;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_408) %>&nbsp;<a href='<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCorePHAccountTypesWindowsGroup")) %>' style="text-decoration:underline; cursor:pointer;" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_404) %></a></td>
                    </tr>    
                    
                    
                    

                    <tr style="height:20px;">
                        <td>&nbsp;</td>
                        <td><input type="radio" id="radioSamlIndividual" name="radioButtons" value="SamlAccount" style="margin-top:10px; cursor:pointer;" runat="server" /></td>                        
                        <td width="95%" style="padding-top:10px; font-weight:bolder;"><div style="cursor:pointer;" onclick="selectOption(radioSamlIndividualID);"><img src="/Orion/Admin/Accounts/images/icons/saml_icon.png"/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_527) %></div></td>
                    </tr>
                    <tr style="height:20px;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>                        
                        <td width="95%" style="font-weight:lighter; font-size:small">&nbsp;&nbsp;&nbsp;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_528) %>&nbsp;<a href='<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCorePHAccountTypesSamlIndividual")) %>' style="text-decoration:underline; cursor:pointer;" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_404) %></a></td>
                    </tr>         
                    
                    <tr style="background-color:#f8f8f8; height:20px;">
                        <td>&nbsp;</td>
                        <td><input type="radio" id="radioSamlGroup" name="radioButtons" value="SamlGroup" style="margin-top:10px; cursor:pointer;" runat="server" /></td>                        
                        <td width="95%" style="padding-top:10px; font-weight:bolder;"><div style="cursor:pointer;" onclick="selectOption(radioSamlGroupID);"><img src="/Orion/Admin/Accounts/images/icons/saml_icon.png"/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_529) %></div></td>
                    </tr>
                    <tr style="background-color:#f8f8f8; height:20px;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>                        
                        <td width="95%" style="font-weight:lighter; font-size:small">&nbsp;&nbsp;&nbsp;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_530) %>&nbsp;<a href='<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCorePHAccountTypesSamlGroup")) %>' style="text-decoration:underline; cursor:pointer;" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_404) %></a></td>
                    </tr>         
                    
                    


                    <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                         <td>&nbsp;</td>
                        <td style="text-align:right">
                            <div class="sw-btn-bar-wizard">
                                <orion:LocalizableButton id="btnNext" runat="server" DisplayType="Primary" LocalizedText="Next"  automation="Next" OnClick="btnNext_Click"/>
                                <orion:LocalizableButton id="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="btnCancel_Click" CausesValidation="false"/>
                            </div>
                        </td>
                    </tr>
                </table>             
            </div>
            </div>
            

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


