﻿using System;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_Accounts_Accounts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // enable AngularJS for page content so that SAML identity provider info message works
        AngularJsHelper.EnableAngularJsForPageContent();
    }

    protected bool ContainsSamlAccounts
        => AccountManagementDAL.GetSamlAccountsCount() > 0;

    protected bool IsIdpConfigured
        => AccountManagementDAL.IsIdpConfigured();
}
