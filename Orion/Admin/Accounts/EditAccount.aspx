<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditAccount.aspx.cs" Inherits="Orion_Admin_Accounts_EditAccount"
    MasterPageFile="~/Orion/Admin/OrionAdminPage.master" %>

<%@ Register TagPrefix="orion" TagName="yesnocontrol" Src="~/Orion/Admin/YesNoControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="moduleusersettings" Src="~/Orion/Admin/ModuleUserSettings.ascx" %>
<%@ Register TagPrefix="orion" TagName="userwebviews" Src="~/Orion/Admin/UserWebViews.ascx" %>
<%@ Register TagPrefix="orion" TagName="AccountLimitationEditor" Src="~/Orion/Admin/AccountLimitationEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="TabMenuBars" Src="~/Orion/Admin/TabMenuBars.ascx" %>
<%@ Register TagPrefix="orion" TagName="TabsEditor" Src="~/Orion/Controls/TabsEditorControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="AccountWorkflowProgress" Src="~/Orion/Admin/Accounts/Add/AccountWorkflowProgress.ascx" %>
<%@ Register TagPrefix="orion" TagName="AccountsList" Src="~/Orion/Admin/Accounts/Add/AccountsList.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <orion:Include runat="server" File="Admin.css" />
    <orion:Include runat="server" File="ProgressIndicator.css" />
    <style type="text/css">
        .accountDetails { width: 100%; }
        .accountDetails tr td,  #newAlertCategory
        {
            font-size: 9pt;
            padding-right: 2em;
            padding-bottom: .5em;
            vertical-align: top;
        }
        .cellDisabled { color: GrayText; cursor: default; }
        input[disabled] { cursor: default; }
    </style>
    
    <script type="text/javascript">
        function ValidateBreadcrumbItemsCount(source, arguments) {
            if (arguments.Value != "" && arguments.Value > -1 && arguments.Value < 2147483648)
                arguments.IsValid = true;
            else
                arguments.IsValid = false;
        }

        $(window).load(function() {
            var allowAdmin = $('#<%= ynAdminRights.ListBoxClientId %>');
            var allowNodeMgmt = $('#<%= ynAllowNodeManagement.ListBoxClientId %>');
            var allowMapMgmt = $('#<%= ynAllowMapManagement.ListBoxClientId %>');
            var allowAlertMgmt = $('#<%= ynAllowMngAlerts.ListBoxClientId %>');
            var allowUnmanage = $('#<%= ynAllowUnmanage.ListBoxClientId %>');
            var allowDisableAction = $('#<%= ynAllowDisableAction.ListBoxClientId %>');
            var allowDisableAlert = $('#<%= ynAllowDisableAlert.ListBoxClientId %>');
            var allowDisableAllActions = $('#<%= ynAllowDisableAllActions.ListBoxClientId %>');
            var allowOrionMapsManagement = $('#<%= ynAllowOrionMapsManagement.ListBoxClientId %>');
            var allowUploadImagesToOrionMaps = $('#<%= ynAllowUploadImagesToOrionMaps.ListBoxClientId %>');
            var allowCustomize = $('#<%= ynCustomizeViews.ListBoxClientId %>');
            var allowManageDashboards = $('#<%= ynAllowManageDashboards.ListBoxClientId %>');
            
            var nodeMgmtChangeEvent = function() {
                var nodeMgmtValue = allowNodeMgmt.find('option:selected').val();
                if (nodeMgmtValue == "true") {
                    allowUnmanage.val(nodeMgmtValue);
                    allowUnmanage.find('option:not(:selected)').attr('disabled', true);
                } else {
                    allowUnmanage.find('option').removeAttr('disabled');
                }
            };

            var allowDisableAllActionsChangeEvent = function() {
                var allowDisableAllActionsValue = allowDisableAllActions.find('option:selected').val();
                if (allowDisableAllActionsValue == 'true') {
                    allowDisableAction.val(allowDisableAllActionsValue);
                    allowDisableAction.find('option:not(selected)').attr('disabled', true);
                } else {
                    allowDisableAction.find('option').removeAttr('disabled');
                }
            }

            var alertMgmtChangeEvent = function() {
                var alertMgmtValue = allowAlertMgmt.find('option:selected').val();
                if (alertMgmtValue == 'true') {
                    allowDisableAction.val(alertMgmtValue);
                    allowDisableAlert.val(alertMgmtValue);
                    allowDisableAllActions.val(alertMgmtValue);
                    allowDisableAction.find('option:not(:selected)').attr('disabled', true);
                    allowDisableAlert.find('option:not(:selected)').attr('disabled', true);
                    allowDisableAllActions.find('option:not(:selected)').attr('disabled', true);
                } else {
                    allowDisableAction.find('option').removeAttr('disabled');
                    allowDisableAlert.find('option').removeAttr('disabled');
                    allowDisableAllActions.find('option').removeAttr('disabled');
                }
            };

            var allowCustomizeChangeEvent = function () {
                var allowCustomizeValue = allowCustomize.find('option:selected').val();
                if (allowCustomizeValue == "true") {
                    allowManageDashboards.val(allowCustomize);
                    allowManageDashboards.find('option:not(:selected)').attr('disabled', true);
                } else {
                    allowManageDashboards.find('option').removeAttr('disabled');
                }
            };

            var adminChangeEvent = function() {
                var adminValue = allowAdmin.find('option:selected').val();
                if (adminValue == "true") {
                    allowNodeMgmt.val(adminValue);
                    allowNodeMgmt.find('option:not(:selected)').attr('disabled', true);
                    allowAlertMgmt.val(adminValue);
                    allowAlertMgmt.find('option:not(:selected)').attr('disabled', true);
                    allowMapMgmt.val(adminValue);
                    allowMapMgmt.find('option:not(:selected)').attr('disabled', true);
                    allowOrionMapsManagement.val(adminValue);
                    allowOrionMapsManagement.find('option:not(:selected)').attr('disabled', true);
                    allowUploadImagesToOrionMaps.val(adminValue);
                    allowUploadImagesToOrionMaps.find('option:not(:selected)').attr('disabled', true);
                    allowCustomize.val(adminValue);
                    allowCustomize.find('option:not(:selected)').attr('disabled', true);
                    allowManageDashboards.val(adminValue);
                    allowManageDashboards.find('option:not(:selected)').attr('disabled', true);
                } else {
                    allowNodeMgmt.find('option').removeAttr('disabled');
                    allowAlertMgmt.find('option').removeAttr('disabled');
                    allowMapMgmt.find('option').removeAttr('disabled');
                    allowOrionMapsManagement.find('option').removeAttr('disabled');
                    allowUploadImagesToOrionMaps.find('option').removeAttr('disabled');
                    allowCustomize.find('option').removeAttr('disabled');
                    allowManageDashboards.find('option').removeAttr('disabled');
                }
                nodeMgmtChangeEvent();
                alertMgmtChangeEvent();
                allowCustomizeChangeEvent();
            };

            var allowOrionMapsManagementChangeEvent = function () {
                var allowOrionMapsManagementValue = allowOrionMapsManagement.find('option:selected').val();
                if (allowOrionMapsManagementValue == "false") {
                    allowUploadImagesToOrionMaps.val(allowOrionMapsManagementValue);
                    allowUploadImagesToOrionMaps.find('option:not(:selected)').attr('disabled', true);
                } else {
                    allowUploadImagesToOrionMaps.find('option').removeAttr('disabled');
                }
            };

            allowAdmin.change(adminChangeEvent);
            allowNodeMgmt.change(nodeMgmtChangeEvent);
            allowAlertMgmt.change(alertMgmtChangeEvent);
            allowOrionMapsManagement.change(allowOrionMapsManagementChangeEvent);
            allowCustomize.change(allowCustomizeChangeEvent);

            adminChangeEvent();
            nodeMgmtChangeEvent();
            alertMgmtChangeEvent();
            allowOrionMapsManagementChangeEvent();
            allowCustomizeChangeEvent();

            var validator = document.getElementById("<%=newCategoryValidator.ClientID %>");
            $("#<%= lbAlertCategory.ClientID %>").change(function () {
                if (this.value === "<%= AddNewCategoryValue%>") {
                    $("#newAlertCategory").show();
                    validator.enabled = true;
                } else {
                    $("#newAlertCategory").hide();
                    validator.enabled = false;
                }
            });
            $("#<%= lbAlertCategory.ClientID %>").change();
        });
    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <input type="hidden" id="checkboxesVisible" name="checkboxesVisible" value="hidden" runat="server" />

    <h1 style="font-size: large; padding-left: 0px; padding-bottom: 10px; padding-top: 15px;">
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %>
    </h1>

    <div class="GroupBoxAccounts" id="AllPropertiesBox" style="min-width: 800px; ">
	<orion:AccountWorkflowProgress ID="ProgressIndicator" runat="server" />
	<div id="InerPropertyBox" style="padding: 0px 10px 5px 10px;">
	
	<h2 style="margin-top: 10px; "><asp:Label ID="lblSubTitle" Visible="false" runat="server" /></h2>	
	
	<div visible="true" runat="server" id="divAccountsList" style="border-bottom: #dbdcd7 1px solid; padding-bottom: 0px; width: 100%;">
	    <table >
	        <tr>
	            <td style="padding-right: 15px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_453) %></td>
	            <td><orion:AccountsList ID="accountsList" mode="readonly" runat = "server" width="480" /></td>
	        </tr>	    
	    </table>
    </div>

    <table class="accountDetails" id="accountPropTabel" >
        <tr>
            <td><input type="checkbox" runat="server" ID="chbxAccountEnabled" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_TM0_65) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAccountEnabled" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_454) %></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAccountExpires" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_455) %></td>
            <td>
                <asp:TextBox runat="server" Columns="20" ID="txtAccountExpires" />
                <asp:ImageButton ImageUrl="images\buttons\Calendar_Control.gif" ID="ExpiresCalendarButton" runat="server" style="vertical-align:bottom;" OnClick="ExpiresCalendarButton_Click" /></td>
            <td>
               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_456) %></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:Calendar ID="ExpiresCalendar" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" OnSelectionChanged="ExpiresCalendar_SelectionChanged" Visible="False" Width="200px">
                <SelectedDayStyle BackColor="#696969" Font-Bold="True" ForeColor="White" />
                <SelectorStyle BackColor="#CCCCCC" />
                <WeekendDayStyle BackColor="#FFFFCC" />
                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                <OtherMonthDayStyle ForeColor="#808080" />
                <NextPrevStyle VerticalAlign="Bottom" />
                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxSessionTimeout" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_457) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynDisableSessionTimeout" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_458) %></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAdmin" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_459) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAdminRights" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_460) %></td>
        </tr>
        <% if (!_hideEditAccountRows.Contains("NodeManagementRight"))
                   {%>
                <tr >
                    <td>
                        <input type="checkbox" runat="server" id="chbxNodeMng" onclick="togleRowState(this);" /></td>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_461) %></td>
                    <td>
                        <orion:yesnocontrol runat="server" ID="ynAllowNodeManagement" />
                    </td>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_462) %></td>
                </tr>
         <%}%>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxMapMngt" onclick="togleRowState(this);" /></td>
            <td>
                <% = Resources.CoreWebContent.WEBDATA_OS0_1%>
            </td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowMapManagement"/>
            </td>
            <td>
                 <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OS0_2) %>
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxOrionMapsMangt" onclick="togleRowState(this);" /></td>
            <td>
                <% = Resources.CoreWebContent.AllowOrionMapsManagement%>
            </td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowOrionMapsManagement"/>
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AllowOrionMapsManagementDescription) %>
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxOrionMapsUploadImages" onclick="togleRowState(this);" /></td>
            <td>
                <% = Resources.CoreWebContent.AllowUploadImagesToOrionMaps%>
            </td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowUploadImagesToOrionMaps"/>
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AllowUploadImagesToOrionMapsDescription) %>
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxCustomizeViews" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_463) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynCustomizeViews" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_464) %></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAllowManageDashboards" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AllowManageDashboards) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowManageDashboards" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AllowManageDashboardsDescription) %></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3"><h2><br/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.EditAccount_ReportsHeader) %></h2></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxMngReports" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_265) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowMngReports" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_266) %></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxReportFolder" onclick="togleRowState(this);" /></td>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_TM0_75) %></td>
            <td>
                <asp:ListBox runat="server" ID="lbxReportFolder" Rows="1">
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_479 %>" Value="" />
                </asp:ListBox>
            </td>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_480) %><br />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3"><h2><br/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.EditAccount_AlertsHeader) %></h2></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxMngAlerts" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_186) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowMngAlerts" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_187) %></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbAlertCategory" onclick="togleRowState(this);" /></td>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.EditAccount_AlertLimitationCategory) %></td>
            <td>
                <asp:ListBox runat="server" ID="lbAlertCategory" Rows="1" />
                <div id="newAlertCategory" style="display: none;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertWizard_Default_Category_New) %><br />
                    <asp:TextBox runat="server" ID="tbNewCategory" Width="200px" MaxLength="255" /><br />
                    <asp:RequiredFieldValidator ID="newCategoryValidator" runat="server" ControlToValidate="tbNewCategory" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,AlertWizard_Default_Category_ValidatorWarning %>" Display="Dynamic" />
                </div>
            </td>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.EditAccount_AlertLimitationCategoryDescription) %></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3"><h2><br/></h2></td>
        </tr>
        <% if (!_hideEditAccountRows.Contains("UnmanageObjectRight"))
           { %>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAllowUnmanage" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ED0_1) %>
            </td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowUnmanage" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ED0_2) %>
            </td>
        </tr>
        <% } %>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAllowDisableAction" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_1) %>
            </td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowDisableAction" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_2) %>
            </td>
        </tr>
        
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAllowDisableAlert" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_3) %>
            </td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowDisableAlert" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_4) %>
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAllowDisableAllActions" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_5) %>
            </td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynAllowDisableAllActions" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_6) %>
            </td>
        </tr>

        <tr>
            <td><input type="checkbox" runat="server" id="chbxClearEvents" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_465) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynClearEvents" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_466) %>
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxBrowserIntegration" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_467) %></td>
            <td>
                <orion:yesnocontrol runat="server" ID="ynBrowserIntegration" />
            </td>
            <td>
               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_468) %></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxBreadcrumb" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_471) %>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbBreadcrumbItems" Width="72px"></asp:TextBox>
                <asp:CustomValidator runat="server" ControlToValidate="tbBreadcrumbItems" ValidateEmptyText="true" ClientValidationFunction="ValidateBreadcrumbItemsCount" Display="Dynamic" ErrorMessage="*"></asp:CustomValidator>
             </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_472) %>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_483) %></h2></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxAccountLim" onclick="togleRowState(this);" /></td>
            <td colspan="3">
                <orion:AccountLimitationEditor runat="server" ID="accountLimitations" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_473) %></h2></td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxTabMenuBars" onclick=" togleTabBarState(this);" /></td>
            <td colspan="3">
            
            <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_289, "<a style=\"color: Blue; text-decoration: underline\" href=\"/Orion/Admin/SelectMenuBar.aspx\" target=\"_blank\" >", "</a>")) %>
            </td>
        </tr>
        <orion:TabMenuBars id="tabBars" runat="server" />
        
        <orion:userwebviews id="userwebviews" runat="server" />

        <tr>
            <td><input type="checkbox" runat="server" id="chbxTabEditor" onclick="togleRowState(this);" /></td>
            <orion:TabsEditor runat="server" ID="tabsEditor"/>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxHomePageView" onclick="togleRowState(this);" /></td>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_474) %></td>
            <td>
                <asp:ListBox runat="server" ID="lbxHomePageView" Rows="1" SelectionMode="single" DataTextField="ViewTitle" DataValueField="ViewID" />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_475) %>
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxNetworkDevice" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_TM0_74) %>
                <br /><br />
            </td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(this.DefaultNetObjectCaption) %>
            </td>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_476) %><br />
                <orion:localizableButton runat="server" ID="btnEditNetObject" LocalizedText="Edit" DisplayType="Small" OnClick="EditNetObject" />
                <orion:LocalizableButton runat="server" ID="btnDeleteNetObject" LocalizedText="Delete" DisplayType="Small" OnClick="DeleteNetObject" />
            </td>
        </tr>
        <tr>
            <td><input type="checkbox" runat="server" id="chbxSummaryView" onclick="togleRowState(this);" /></td>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_477) %>
            </td>
            <td>
                <asp:ListBox runat="server" ID="lbxSummaryView" Rows="1" SelectionMode="single" DataTextField="ViewTitle" DataValueField="ViewID" />
            </td>
            <td>
               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_478) %>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <orion:moduleusersettings runat="server" ID="moduleSettings" />   
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">
                <asp:CustomValidator ID="SubmitValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VB0_482 %>"
                OnServerValidate="SubmitValidator_ServerValidate" Font-Size="Large"></asp:CustomValidator>
            </td>
        </tr>
    </table>    

	<br />
    <div class="sw-btn-bar">
	<orion:LocalizableButton runat="server" ID="btnBack" LocalizedText="Back" DisplayType="Secondary"
        OnClick="btnBack_Click" CausesValidation="False" />
    <orion:LocalizableButton runat="server" ID="submitButton" LocalizedText="Submit" automation="Submit" DisplayType="Primary"
        OnClick="SubmitClick" />
    &nbsp;&nbsp;&nbsp;
    <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary"
        OnClick="btnCancel_Click" CausesValidation="False" />
        </div>
    </div>        
    </div>    
</asp:Content>
