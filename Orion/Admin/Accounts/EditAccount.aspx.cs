using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Data;
using Resources;

using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Extensions;

public partial class Orion_Admin_Accounts_EditAccount : System.Web.UI.Page
{
    private static Log myLog = new Log();
    private const string ViewIdentifier = "V";
    private const string DashboardIdentifier = "D";
    private const int UnknownID = -1;
    private const string AccountIdQueryStringKey = "AccountID";
    private const string AccountTypeQueryStringKey = "AccountType";

    private bool _editAccountMode;
    private List<Control> _checkboxes;

    internal ProfileCommon _profile;

    //Random unique string
    protected string AddNewCategoryValue = "7AD37FE3-D0E7-41C5-8467-8C6B70DAEE30";

    public static List<String> _hideEditAccountRows = OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.EditAccounHideTableRow)).Select(c => c.Key).ToList();

    protected string DefaultNetObjectCaption
    {
        get
        {
            if (!_profile.DefaultNetObject.Equals("not selected", StringComparison.InvariantCultureIgnoreCase))
            {
                return _profile.DefaultNetObject;
            }
            return Resources.CoreWebContent.WEBCODE_TM0_77;
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        ValidateAccount(Request.QueryString[AccountIdQueryStringKey], Request.QueryString[AccountTypeQueryStringKey]);

        if (!IsPostBack)
        {
            //check if AccountWorkflowManager initiated. If not - redirect to default accounts root page
            AccountWorkflowManager.VerifySession();
        }

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        DetectProcessEditType();

        this.ynDisableSessionTimeout.Value = _profile.DisableSessionTimeout;
        this.ynAccountEnabled.Value = _profile.AccountEnabled;
        this.ynAdminRights.Value = _profile.AllowAdmin;
        this.ynAllowNodeManagement.Value = _profile.AllowNodeManagement;
        this.ynAllowMapManagement.Value = _profile.AllowMapManagement;
        this.ynAllowMngReports.Value = _profile.AllowReportManagement;
        this.ynAllowMngAlerts.Value = _profile.AllowAlertManagement;
        this.ynCustomizeViews.Value = _profile.AllowCustomize;
        this.ynAllowUnmanage.Value = _profile.AllowUnmanage;
        this.ynAllowDisableAction.Value = _profile.AllowDisableAction;
        this.ynAllowDisableAlert.Value = _profile.AllowDisableAlert;
        this.ynAllowDisableAllActions.Value = _profile.AllowDisableAllActions;
        this.ynClearEvents.Value = _profile.AllowEventClear;
        this.ynBrowserIntegration.Value = _profile.ToolsetIntegration;
        this.ynAllowOrionMapsManagement.Value = _profile.AllowOrionMapsManagement;
        this.ynAllowUploadImagesToOrionMaps.Value = _profile.AllowUploadImagesToOrionMaps;
        this.ynAllowManageDashboards.Value = _profile.AllowManageDashboards;

        this.accountLimitations.UserProfile = _profile;

        // Somehow, I doubt this software will be around for 50 years; I think we're safe here.
        if (DateTime.Now.AddYears(50) < _profile.Expires)
        {
            this.txtAccountExpires.Text = Resources.CoreWebContent.WEBCODE_VB0_242;
        }
        else
        {
            this.txtAccountExpires.Text = _profile.Expires.ToShortDateString();
        }

        SetupHomePageViewSelections();
        lbxHomePageView.SelectedValue = (_profile.HomePageDashboardID > 0) ?
            $"{DashboardIdentifier}:{_profile.HomePageDashboardID}" : $"{ViewIdentifier}:{GetDefaultHomePageViewID()}";

        SetupDefaultNetObjectSelections(_profile);

        SetupSummaryViewSelections();
        lbxSummaryView.SelectedValue = (_profile.SummaryDashboardID > 0) ?
            $"{DashboardIdentifier}:{_profile.SummaryDashboardID}" : $"{ViewIdentifier}:{_profile.SummaryViewID}";

        SetupReportFolderSelections();
        this.lbxReportFolder.SelectedValue = _profile.ReportFolder;
        SetupAlertCategorySelections();
        this.lbAlertCategory.SelectedValue = _profile.AlertCategory;

        this.moduleSettings.SetPropertyValuesToForm(_profile);
        this.tabBars.SetUpValues(_profile.UserName);
        this.userwebviews.SetUpValues(_profile.UserName);

        this.tbBreadcrumbItems.Text = WebUserSettingsDAL.Get(_profile.UserName, "Breadcrumb_NetOjectsCount", "50");

        base.OnInit(e);
    }

    private void DetectProcessEditType()
    {
        // if "AccountID" is missing, redirect to manager
        string accountID = Request.QueryString[AccountIdQueryStringKey];
        if (string.IsNullOrEmpty(accountID))
            this.Response.Redirect("/Orion/Admin/Accounts/Accounts.aspx");

        string accountType = Request.QueryString[AccountTypeQueryStringKey];

        _profile = Profile.GetProfile(accountID);

        if (string.IsNullOrEmpty(accountType))
        {   // Existing single account edit
            if (!IsPostBack)
                AccountWorkflowManager.Cancel(); // just clear possible prev state

            this.Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_VB0_288, HttpUtility.HtmlEncode(accountID));
            btnCancel.Visible = false;
            ProgressIndicator.Visible = false;
            accountsList.Visible = false;
            divAccountsList.Visible = false;
            ManageCheckboxes(false, null);
            return;
        }

        if (!IsPostBack)
        {
            AccountWorkflowManager.CurrentStep = AccountWorkflowManager.AddAccountStep.SetProperties;
            if (AccountWorkflowManager.Accounts?.Any() != true)
            {
                AccountWorkflowManager.Accounts = new List<string>() { accountID };
            }
        }

        _editAccountMode = false;
        switch (accountType.ToLower(CultureInfo.InvariantCulture))
        {
            case "orion":
                this.Page.Title = Resources.CoreWebContent.WEBCODE_VB0_282;
                lblSubTitle.Text = string.Format(Resources.CoreWebContent.WEBCODE_VB0_281, HttpUtility.HtmlEncode(_profile.UserName));
                lblSubTitle.Visible = true;
                ProgressIndicator.Visible = true;
                divAccountsList.Visible = false;
                accountsList.Visible = false;

                ManageCheckboxes(false, null);
                _profile.AccountType = (int)GroupAccountType.Sql;
                break;

            case "individual":
                this.Page.Title = Resources.CoreWebContent.WEBCODE_VB0_282;
                lblSubTitle.Text = Resources.CoreWebContent.WEBCODE_VB0_283;
                lblSubTitle.Visible = true;
                ProgressIndicator.Visible = true;
                accountsList.displayText = AccountWorkflowManager.Accounts.Count > 1 ?
                     Resources.CoreWebContent.WEBDATA_TM0_100 :
                     Resources.CoreWebContent.WEBCODE_TM0_64;

                ManageCheckboxes(false, null);
                _profile.AccountType = (int)GroupAccountType.WindowsUser;
                break;

            case "group":
                this.Page.Title = Resources.CoreWebContent.WEBCODE_VB0_282;
                lblSubTitle.Text = Resources.CoreWebContent.WEBCODE_VB0_284;
                lblSubTitle.Visible = true;
                ProgressIndicator.Visible = true;
                if (AccountWorkflowManager.Accounts.Count > 1)
                    accountsList.displayText = Resources.CoreWebContent.WEBDATA_VB0_452; // Windows Groups, bcs Windows accounts are stores in AWM.Accounts
                else accountsList.displayText = Resources.CoreWebContent.WEBCODE_VB0_285; // Groups (SAML)

                ManageCheckboxes(false, null);
                _profile.AccountType = (int)GroupAccountType.WindowsGroup;
                break;
            case "samlindividual":
                ProgressIndicator.Visible = true;
                ManageCheckboxes(false, null);
                _profile.AccountType = (int)GroupAccountType.SamlUser;
                break;
            case "samlgroup":
                ManageCheckboxes(false, null);
                _profile.AccountType = (int)GroupAccountType.SamlGroup;
                break;
            case "edit":
                lblSubTitle.Visible = false;
                ProgressIndicator.Visible = false;
                btnCancel.Visible = false;
                _editAccountMode = true;

                // Either display list of accounts to be edited or a single account
                if (AccountWorkflowManager.Accounts.Count > 1)
                {
                    this.Page.Title = Resources.CoreWebContent.WEBCODE_VB0_286; // Edit account(s)
                    accountsList.displayText = Resources.CoreWebContent.WEBCODE_VB0_287; // Account(s)

                    OrionInclude.CoreFile("Admin/Accounts/js/EditAccounts.js");

                    if (Page.Session["activeCheckboxes"] == null)
                    { // set of checkboxes checked by default
                        Page.Session["activeCheckboxes"] = new List<string>();
                    }
                    ManageCheckboxes(true, (List<string>)Page.Session["activeCheckboxes"]);
                }
                else
                {
                    this.Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_VB0_288, HttpUtility.HtmlEncode(accountID)); // Edit [accountID] account
                    accountsList.Visible = false;
                    divAccountsList.Visible = false;

                    ManageCheckboxes(false, null);
                }
                break;
        }
    }

    protected void ManageCheckboxes(bool visible, List<string> checkedNames)
    {
        // set value used by client type js (EditAccount.js)
        checkboxesVisible.Value = visible ? "visible" : "hidden";

        // Get list of all checkboxes on this page
        _checkboxes = GetAllControls(this, typeof(HtmlInputCheckBox));

        // Set Visible for every checkbox
        foreach (HtmlInputCheckBox chbx in _checkboxes) chbx.Visible = visible;

        // Manage checked state
        if (checkedNames != null)
        {
            foreach (string name in checkedNames)
                foreach (HtmlInputCheckBox chbx in _checkboxes)
                    if (chbx.Name == name)
                        chbx.Checked = true;
        }
    }

    #region == Helper methods ==
    private List<Control> GetAllControls(Control parent, Type type)
    {
        List<Control> list = new List<Control>();
        AddControlToList(parent, type, ref list);
        return list;
    }
    private void AddControlToList(Control parent, Type type, ref List<Control> list)
    {
        foreach (Control c in parent.Controls)
        {
            if (c.GetType() == type) list.Add(c);
            else AddControlToList(c, type, ref list);
        }
    }
    #endregion

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);

        if (IsPostBack && _checkboxes != null)
        {
            // Save state of all checked checkboxes into session variable
            List<string> list = new List<string>();
            foreach (HtmlInputCheckBox chbx in _checkboxes)
            {
                if (chbx.Checked)
                    list.Add(chbx.Name);
            }
            Page.Session["activeCheckboxes"] = list;
        }

        if (!IsPostBack)
        {
            string toggleScript = "$(function() { if(typeof(SetDefaultState)=='function') {SetDefaultState('" + checkboxesVisible.ClientID + "','" + chbxTabMenuBars.ClientID + "')}});";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script_" + System.Guid.NewGuid().ToString(), toggleScript, true);
        }
    }

    private void SetupHomePageViewSelections()
    {
        this.lbxHomePageView.DataSource = GetDashboardsAndViews(ViewManager.GetAllViews());
        this.lbxHomePageView.DataBind();
    }

    private string GetDefaultHomePageViewID()
    {
        if (_profile.HomePageViewID == 0)
        {
            int? defaultHomePageViewID = ViewManager.GetDefaultHomePageViewID();
            return defaultHomePageViewID.HasValue ? defaultHomePageViewID.Value.ToString() : _profile.SummaryViewID.ToString();
        }
        else
        {
            return _profile.HomePageViewID.ToString();
        }
    }

    private void SetupDefaultNetObjectSelections(ProfileCommon profile)
    {
        ViewInfo defaultView = ViewManager.GetViewById(profile.HomePageViewID);
        if ((null == defaultView) ||
            ((null == defaultView.NetObjectType) && !string.IsNullOrEmpty(profile.DefaultNetObjectID)))
        {
            profile.DefaultNetObjectID = string.Empty;
            profile.DefaultNetObject = "not selected";
            profile.Save();
        }

        if (string.IsNullOrEmpty(profile.DefaultNetObjectID))
        {
            this.btnDeleteNetObject.Visible = false;
        }
        else
        {
            this.btnDeleteNetObject.Visible = true;
        }
    }

    private void SetupSummaryViewSelections()
    {
        IEnumerable<ViewInfo> summaryViews = ViewManager.GetViewsByType("Summary");

        // Check if there is custom default summary page set
        int? defaultSummaryViewId = ViewManager.GetDefaultSummaryViewId();
        if (defaultSummaryViewId != null)
        {
            if (!summaryViews.Any(x => x.ViewID.Equals(defaultSummaryViewId)))
            {
                // Add to the list if missing
                var viewInfo = ViewManager.GetViewById(defaultSummaryViewId.Value);
                summaryViews = new[] { viewInfo }.Concat(summaryViews);
            }
        }

        this.lbxSummaryView.DataSource = GetDashboardsAndViews(summaryViews);
        this.lbxSummaryView.DataBind();
    }

    private void SetupAlertCategorySelections()
    {
        this.lbAlertCategory.Items.Add(new ListItem(Resources.CoreWebContent.EditAccount_AlertLimitation_NoLimitation, string.Empty));
        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            var categories =
                swis3.Query(
@"SELECT DISTINCT AlertCategory FROM
(SELECT AlertCategory FROM Orion.Accounts
 UNION (SELECT Category AS AlertCategory FROM Orion.AlertConfigurations))
WHERE (AlertCategory IS NOT NULL) AND (AlertCategory <> '') ORDER BY AlertCategory");
            foreach (var row in categories.Rows.Cast<DataRow>())
            {
                var category = Convert.ToString(row[0]);
                this.lbAlertCategory.Items.Add(new ListItem(category, category));
            }
        }
        this.lbAlertCategory.Items.Add(new ListItem(Resources.CoreWebContent.AlertWizard_Default_Category_AddNew, AddNewCategoryValue));
    }

    private void SetupReportFolderSelections()
    {
        var categories = OrionReportHelper.GetAllLimitationCategories();
        this.lbxReportFolder.Items.Add(new ListItem(CoreWebContent.EditAccount_AlertLimitation_NoLimitation, CoreConstants.ReportNoLimitationCategory));
        this.lbxReportFolder.Items.Add(new ListItem(CoreWebContent.WEBCODE_TM0_116, CoreConstants.ReportDefaultFolder));

        foreach (string cat in categories)
        {
            if (!cat.Equals(CoreConstants.ReportDefaultFolder, StringComparison.OrdinalIgnoreCase))
                this.lbxReportFolder.Items.Add(new ListItem(cat, cat));
        }
    }

    //private int CloneLimitation(int limitationId)
    //{
    //    //A more performant version belongs in DAL but this is a quicker solution and current usage performance is not a large factor
    //    if (limitationId == 0)
    //        return limitationId;
    //    else
    //    {
    //        var oldLimitation = SolarWinds.Orion.Web.Limitation.GetLimitationByID(limitationId);
    //        var newLimitation = SolarWinds.Orion.Web.Limitation.CreateNewLimitation(oldLimitation.Type, oldLimitation.Items);
    //        return newLimitation.LimitationID;
    //    }
    //}

    private string[] GetAccounts(string accountType, List<string> accounts)
    {
        return (string.IsNullOrEmpty(accountType) || accounts == null || accounts.Count == 0)
            ? new string[] { _profile.UserName }
            : accounts.ToArray();
    }

    protected void SubmitClick(Object sender, EventArgs e)
    {
        if (!SubmitValidator.IsValid)
        {
            SubmitValidator.ErrorMessage = SubmitValidator.ErrorMessage;
            // to get submit button and error message into view
            ScriptManager.RegisterStartupScript(
                this,
                this.GetType(),
                "ScrollEditPageToBottom",
                "window.scrollTo(0, 1600);",
                true
            );
            return;
        }

        string accountType = Request.QueryString[AccountTypeQueryStringKey];
        string[] accounts = GetAccounts(accountType, AccountWorkflowManager.Accounts);

        int maxGroupPriority = -1;
        int maxSamlGroupPriority = -1;

        if (!string.IsNullOrEmpty(accountType))
        {
            accountType = accountType.ToLower(CultureInfo.InvariantCulture);
            if (accountType == "group")
            {
                maxGroupPriority = new AccountManagementDAL().GetMaxGroupPriority();
            }
            if (accountType == "samlgroup")
            {
                maxSamlGroupPriority = new AccountManagementDAL().GetMaxGroupPriority(GroupAccountType.SamlGroup);
            }
        }

        foreach (string account in accounts)
        {
            myLog.DebugFormat("Saving profile properties for user account \"{0}\"", account);

            if (!_editAccountMode)
            {
                ValidateAccount(account, accountType);
            }

            ProfileCommon profile = Profile.GetProfile(account);

            if (!chbxSessionTimeout.Visible || chbxSessionTimeout.Checked)
                profile.DisableSessionTimeout = ynDisableSessionTimeout.Value;
            if (!chbxAccountEnabled.Visible || chbxAccountEnabled.Checked)
                profile.AccountEnabled = ynAccountEnabled.Value;
            if (!chbxAdmin.Visible || chbxAdmin.Checked)
                profile.AllowAdmin = ynAdminRights.Value;
            if (!chbxNodeMng.Visible || chbxNodeMng.Checked)
                profile.AllowNodeManagement = ynAllowNodeManagement.Value;
            if (!chbxMapMngt.Visible || chbxMapMngt.Checked)
                profile.AllowMapManagement = ynAllowMapManagement.Value;
            if (!chbxMngReports.Visible || chbxMngReports.Checked)
                profile.AllowReportManagement = ynAllowMngReports.Value;
            if (!chbxMngAlerts.Visible || chbxMngAlerts.Checked)
                profile.AllowAlertManagement = ynAllowMngAlerts.Value;
            if (!chbxAllowUnmanage.Visible || chbxAllowUnmanage.Checked)
                profile.AllowUnmanage = ynAllowUnmanage.Value;
            if (!chbxAllowDisableAction.Visible || chbxAllowDisableAction.Checked)
                profile.AllowDisableAction = ynAllowDisableAction.Value;
            if (!chbxAllowDisableAlert.Visible || chbxAllowDisableAlert.Checked)
                profile.AllowDisableAlert = ynAllowDisableAlert.Value;
            if (!chbxAllowDisableAllActions.Visible || chbxAllowDisableAllActions.Checked)
                profile.AllowDisableAllActions = ynAllowDisableAllActions.Value;
            if (!chbxCustomizeViews.Visible || chbxCustomizeViews.Checked)
                profile.AllowCustomize = ynCustomizeViews.Value;
            if (!chbxClearEvents.Visible || chbxClearEvents.Checked)
                profile.AllowEventClear = ynClearEvents.Value;
            if (!chbxBrowserIntegration.Visible || chbxBrowserIntegration.Checked)
                profile.ToolsetIntegration = ynBrowserIntegration.Value;
            if (!chbxOrionMapsMangt.Visible || chbxOrionMapsMangt.Checked)
                profile.AllowOrionMapsManagement = ynAllowOrionMapsManagement.Value;
            if (!chbxOrionMapsUploadImages.Visible || chbxOrionMapsUploadImages.Checked)
                profile.AllowUploadImagesToOrionMaps = ynAllowUploadImagesToOrionMaps.Value;
            if (!chbxAllowManageDashboards.Visible || chbxAllowManageDashboards.Checked)
                profile.AllowManageDashboards = ynAllowManageDashboards.Value;

            switch (accountType)
            {
                case "orion": profile.AccountType = (int)GroupAccountType.Sql; break;
                case "individual":
                    profile.AccountType = (int)GroupAccountType.WindowsUser;
                    SetRandomPassword(account);
                    AccountWorkflowManager.TryToAddSidToAccount(profile.UserName);
                    break;
                case "group":
                    profile.AccountType = (int)GroupAccountType.WindowsGroup;
                    profile.GroupPriority = ++maxGroupPriority;
                    SetRandomPassword(account);
                    AccountWorkflowManager.TryToAddSidToAccount(profile.UserName);
                    break;
                case "samlindividual":
                    profile.AccountType = (int)GroupAccountType.SamlUser;
                    break;
                case "samlgroup":
                    profile.GroupPriority = ++maxSamlGroupPriority;
                    profile.AccountType = (int)GroupAccountType.SamlGroup;
                    break;
            }

            WebUserSettingsDAL.Set(profile.UserName, "WhatIsNewInUIDialogDontShow", true.ToString());

            if (!chbxBreadcrumb.Visible || chbxBreadcrumb.Checked)
            {
                WebUserSettingsDAL.Set(profile.UserName, "Breadcrumb_NetOjectsCount", this.tbBreadcrumbItems.Text);
            }

            if (!chbxAccountExpires.Visible || chbxAccountExpires.Checked)
            {
                DateTime DefaultAccountExpirationDate = new DateTime(2100, 2, 1);
                if (!txtAccountExpires.Text.Equals(Resources.CoreWebContent.WEBCODE_VB0_242, StringComparison.InvariantCultureIgnoreCase))
                {
                    DateTime tmpExpires;
                    if (DateTime.TryParse(txtAccountExpires.Text, out tmpExpires))
                    {
                        profile.Expires = tmpExpires;
                    }
                    else
                    {
                        profile.Expires = DefaultAccountExpirationDate;
                    }
                }
                else
                {
                    profile.Expires = DefaultAccountExpirationDate;
                }
            }

            if (!chbxHomePageView.Visible || chbxHomePageView.Checked)
            {
                string identifier;
                var selectedHomePageId = GetParsedListValue(lbxHomePageView.SelectedValue, out identifier);
                if (identifier.Equals(ViewIdentifier))
                {
                    profile.HomePageDashboardID = UnknownID;
                    profile.HomePageViewID = selectedHomePageId;
                }
                else if (identifier.Equals(DashboardIdentifier))
                {
                    profile.HomePageDashboardID = selectedHomePageId;
                    profile.HomePageViewID = UnknownID;
                }
            }

            if (!chbxSummaryView.Visible || chbxSummaryView.Checked)
            {
                string identifier;
                var selectedSummaryId = GetParsedListValue(lbxSummaryView.SelectedValue, out identifier);
                if (identifier.Equals(ViewIdentifier))
                {
                    profile.SummaryDashboardID = UnknownID;
                    profile.SummaryViewID = selectedSummaryId;
                }
                else if (identifier.Equals(DashboardIdentifier))
                {
                    profile.SummaryDashboardID = selectedSummaryId;
                    profile.SummaryViewID = UnknownID;
                }
            }

            if (!chbxReportFolder.Visible || chbxReportFolder.Checked)
                profile.ReportFolder = lbxReportFolder.SelectedValue;
            if (!chbAlertCategory.Visible || chbAlertCategory.Checked)
            {
                profile.AlertCategory = (lbAlertCategory.SelectedValue == AddNewCategoryValue) ?
                    tbNewCategory.Text :
                    lbAlertCategory.SelectedValue;
            }

            if (!chbxTabMenuBars.Visible || chbxTabMenuBars.Checked)
            {
                tabBars.AssignBarsToTabs(profile.UserName);
            }

            userwebviews.StoreValues(profile.UserName);

            if (!chbxTabEditor.Visible || chbxTabEditor.Checked)
            {
                tabsEditor.ReorderTabs();
            }

            // clone other setting set for profile which had been edited in UI to other profiles
            if (profile.UserName != _profile.UserName)
            {
                if (!chbxNetworkDevice.Visible || chbxNetworkDevice.Checked)
                {
                    profile.DefaultNetObjectID = _profile.DefaultNetObjectID;
                    profile.DefaultNetObject = _profile.DefaultNetObject;
                }

                if (!chbxAccountLim.Visible || chbxAccountLim.Checked)
                {
                    profile.LimitationID1 = SolarWinds.Orion.Web.Limitation.CloneLimitation(_profile.LimitationID1);
                    profile.LimitationID2 = SolarWinds.Orion.Web.Limitation.CloneLimitation(_profile.LimitationID2);
                    profile.LimitationID3 = SolarWinds.Orion.Web.Limitation.CloneLimitation(_profile.LimitationID3);
                }

                profile.MenuName = _profile.MenuName;

                try
                {
                    var editproperties = this.moduleSettings.GetPropertyNamesForEdit();

                    if (editproperties.Contains("NetPerfMon.NodeDetailsViewID"))
                        profile.NetPerfMon.NodeDetailsViewID = _profile.NetPerfMon.NodeDetailsViewID;
                    if (editproperties.Contains("NetPerfMon.ContainerDetailsViewID"))
                        profile.NetPerfMon.ContainerDetailsViewID = _profile.NetPerfMon.ContainerDetailsViewID;
                    if (editproperties.Contains("NetPerfMon.VolumeDetailsViewID"))
                        profile.NetPerfMon.VolumeDetailsViewID = _profile.NetPerfMon.VolumeDetailsViewID;
                    if (editproperties.Contains("Interfaces.InterfacesPackageInterfaceDetailsViewID"))
                        profile.NPM.InterfaceDetailsViewID = _profile.NPM.InterfaceDetailsViewID;
                }
                catch (Exception ex)
                {
                    myLog.DebugFormat("Exception while trying to clone profile: {0}", ex.Message +
                        (ex.InnerException == null ? string.Empty : ": " + ex.InnerException.Message));
                }
            }

            ViewInfo defaultView = ViewManager.GetViewById(profile.HomePageViewID);
            if ((defaultView == null && profile.HomePageDashboardID > 0) || null == defaultView.NetObjectType)
            {
                this.moduleSettings.SetPropertyValuesToProfile(profile);
            }
            else
            {
                if (profile.DefaultNetObjectID != "")
                {
                    this.moduleSettings.SetPropertyValuesToProfile(profile);
                }
            }

            profile.Save();

            // Added according to CORE-3866
            if (!profile.AccountEnabled)
            {
                AccountManagementDAL accountMgmtDal = new AccountManagementDAL();
                accountMgmtDal.ClearAlertUsers(account);

                if (profile.AccountType == (int)GroupAccountType.WindowsGroup)
                {
                    var virtualAccountIds = AccountManagementDAL.GetUsersForGroup(profile.UserName);
                    accountMgmtDal.ClearAlertUsersForWindowsVirtualAccount(virtualAccountIds);
                }
            }

            AccountManagementDAL.UpdateGroupUsersLimitations(profile.UserName, profile.AccountType, profile.LimitationID1, profile.LimitationID2, profile.LimitationID3);
        }

        if (!_editAccountMode)
        {
            AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.Completed);
        }

        MenuItemsHelper.ClearMenuCache();
        ReferrerRedirectorBase.Return("/Orion/Admin/Accounts/Accounts.aspx");
    }

    protected void EditNetObject(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(lbxHomePageView.SelectedValue))
        {
            string identifier;
            var selectedHomePageID = GetParsedListValue(lbxHomePageView.SelectedValue, out identifier);
            string SafeUserName = this.Server.UrlEncode(_profile.UserName); //081014{Andy} - When user name includes non safe characters (such as + # $ and so on)
            Response.Redirect(string.Format("/Orion/Admin/EditDefaultNetObject.aspx?AccountID={0}&{3}={1}&Title={4}&ReturnTo={2}", SafeUserName, selectedHomePageID, ReferrerRedirectorBase.GetReturnUrl(),
                identifier.Equals(DashboardIdentifier) ? "DashboardID" : "ViewID", lbxHomePageView.SelectedItem.Text));
        }
    }

    protected void DeleteNetObject(object sender, EventArgs e)
    {
        ViewInfo defaultView = ViewManager.GetViewById(_profile.HomePageViewID);

        _profile.DefaultNetObjectID = string.Empty;
        _profile.DefaultNetObject = "not selected";

        if ((null != defaultView) && (defaultView.NetObjectType != null))
        {
            _profile.HomePageViewID = 0;
        }

        if (ViewManager.GetViewById(_profile.HomePageViewID) == null)
        {
            _profile.Save();
        }

        SetupDefaultNetObjectSelections(_profile);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (_editAccountMode)
        {
            Page.Response.Redirect(AccountWorkflowManager.Cancel());
        }
        else
        {
            myLog.DebugFormat("Deleting profile properties for user account \"{0}\"", _profile.UserName);
            Membership.DeleteUser(_profile.UserName);

            Page.Response.Redirect(AccountWorkflowManager.Back());
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        // Called only as part of "add new account" flow
        if (!_editAccountMode)
        {
            myLog.DebugFormat("Deleting profile properties for user account \"{0}\"", _profile.UserName);
            Membership.DeleteUser(_profile.UserName);
        }

        Page.Response.Redirect(AccountWorkflowManager.Cancel());
    }

    private IEnumerable<dynamic> GetDashboardsAndViews(IEnumerable<ViewInfo> views)
    {
        IEnumerable<dynamic> dashboardDataSource = new List<dynamic>();
        try
        {
            DataTable dashboardTabl = null;
            using (var dashboardDal = new DashboardDAL())
            {
                dashboardTabl = dashboardDal.GetPublicDashboards();
            }

            dashboardDataSource = from dashboardRow in dashboardTabl.AsEnumerable()
                                  select new
                                  {
                                      ViewID = $"{DashboardIdentifier}:{dashboardRow.Field<int>("DashboardID")}",
                                      ViewTitle = dashboardRow.Field<string>("DisplayName")
                                  };
        }
        catch (Exception ex)
        {
            myLog.Warn("Couldn't get dashboards data!", ex);
        }

        var viewsDataSource = from view in views
                              select new
                              {
                                  ViewID = $"{ViewIdentifier}:{view.ViewID}",
                                  ViewTitle = view.ViewTitle
                              };

        return viewsDataSource.Concat(dashboardDataSource).OrderBy(x => x.ViewTitle);
    }

    private int GetParsedListValue(string value)
    {
        string identifier;
        return GetParsedListValue(value, out identifier);
    }

    private int GetParsedListValue(string value, out string identifier)
    {
        var valueParts = value.Split(':');
        if (valueParts.Length == 2)
        {
            identifier = valueParts[0];
            return Convert.ToInt32(valueParts[1]);
        }
        throw new ArgumentException("Error parsing selected value!");
    }

    private void SetRandomPassword(string accountID)
    {
        MembershipUser user = Membership.GetUser(accountID);
        if (user != null)
        {
            user.ChangePassword(
                "1", // should be just not null
                "0x" + new Random().Next().ToString("X")
                );
        }
    }

    /// <summary>
    /// Validates that accountID matches the one used created in the wizard (OrionAccount.aspx).
    /// </summary>
    /// <param name="accountID"><c>AccountID</c> to check the existence for.</param>
    /// <param name="accountType"><c>accountType</c> to check if the account from db matched given type.</param>
    private void ValidateAccount(string accountID, string accountType)
    {
        if (!Profile.AllowAdmin)
        {
            myLog.Debug($"Cannot edit [{accountID}]. Page restricted for Admininstrator accounts only.");
            OrionErrorPageBase.TransferToErrorPage(Context,
                new ErrorInspectorDetails
                {
                    Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323),
                    Title = Resources.CoreWebContent.WEBDATA_VB0_567
                });
        }

        myLog.Debug($"Selecting account: [{accountID}] from database.");
        int accType;

        if (!AccountManagementDAL.TryGetAccountType(accountID, out accType))
        {
            if (!AccountWorkflowManager.Accounts.Contains(accountID))
            {
                myLog.Warn("Attempting to get the EditAccount.aspx page for non-existing user.");
                OrionErrorPageBase.TransferToErrorPage(Context,
                    new ErrorInspectorDetails
                    {
                        Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_580),
                        Title = Resources.CoreWebContent.WEBDATA_VB0_581
                    });
                return;
            }

            if (accountType.Equals(GroupAccountType.WindowsGroup.GetQueryStringName(), StringComparison.InvariantCultureIgnoreCase) ||
                accountType.Equals(GroupAccountType.WindowsUser.GetQueryStringName(), StringComparison.InvariantCultureIgnoreCase))
            {
                myLog.Debug($"Creating account membership for windows account: [{accountID}]");
                CreateWindowsAccount(accountID);
                return;
            }
        }

        GroupAccountType dalAccountType = (GroupAccountType)accType;
        myLog.Debug($"Comparing whether account type for [{accountID}] from database: [{dalAccountType}] matches the one from querystring: [{accountType}].");
        if (accountType.Equals("edit", StringComparison.InvariantCultureIgnoreCase) ||
            accountType.Equals(dalAccountType.GetQueryStringName(), StringComparison.InvariantCultureIgnoreCase))
            return;

        myLog.Warn("Attempting to change the account type in EditAccount.aspx.");
        OrionErrorPageBase.TransferToErrorPage(Context,
            new ErrorInspectorDetails
            {
                Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBDATA_VB0_577),
                Title = Resources.CoreWebContent.WEBDATA_VB0_579
            });
    }

    private void CreateWindowsAccount(string accountID)
    {
        MembershipUser user = Membership.CreateUser(accountID, "1");
        user.ResetPassword(); // if account was created  here, what normally happens only if multiple accounts are being added, it must have empty password 

        // Assign default value to new user 

        ProfileCommon thisProfile = Profile.GetProfile(accountID);
        thisProfile.AllowEventClear = true;
        thisProfile.AllowOrionMapsManagement = true;
        thisProfile.AllowManageDashboards = true;

        foreach (string viewType in ViewManager.GetViewTypes())
        {
            string propName = ViewManager.GetUserPropertyNameByViewType(viewType);

            if (viewType.Equals("Summary") && ViewManager.GetDefaultSummaryViewId() != null)
            {
                thisProfile.SetPropertyValue(propName, ViewManager.GetDefaultSummaryViewId());
            }
            else
            {
                foreach (ViewInfo view in ViewManager.GetViewsByType(viewType))
                {
                    if (view.IsStaticSubview)
                        continue;
                    thisProfile.SetPropertyValue(propName, view.ViewID);
                    break;
                }
            }

            // TT #7375 Node Details view = By Device Type as default for new accounts 
            if (propName == "NetPerfMon.NodeDetailsViewID")
                thisProfile.SetPropertyValue(propName, -1);
        }

        //var reportsFolderDir = ReportHelper.GetReportsRootFolderDir(); 
        //if (reportsFolderDir.Exists) 
        thisProfile.SetPropertyValue("ReportFolder", CoreConstants.ReportDefaultFolder);
        thisProfile.Save();

        //Assign menubars for new created user 
        ITabManager manager = new TabManager();

        IList<Tab> tabs = manager.GetAllTabs();
        foreach (var tab in tabs)
        {
            if (IsHomeTab(tab.TabName))
            {
                //Assign Default menubar to Home tab for new created user 
                manager.AssignBarToTab(accountID, tab.TabId, thisProfile.MenuName);
            }
            else
            {
                //Assign menubars for other modules 
                string menuName = manager.GetCurrentBarName(accountID, tab.TabId);
                //assign the menu of the current user 
                //if no current user then get default 
                if (string.IsNullOrEmpty(menuName))
                    menuName = manager.GetDefaultBarForTab(tab.TabName);

                //Assign the numbar value 
                if (!string.IsNullOrEmpty(menuName))
                    manager.AssignBarToTab(accountID, tab.TabId, menuName);
            }
        }
        return;
    }

    protected void SubmitValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chbxHomePageView.Visible || chbxHomePageView.Checked)
        {
            string identifier;
            int viewID = GetParsedListValue(lbxHomePageView.SelectedValue, out identifier);

            if (identifier.Equals(DashboardIdentifier))
            {
                // no need to DefaultNetObject validate
                args.IsValid = true;
                return;
            }

            ViewInfo defaultView = ViewManager.GetViewById(viewID);

            if ((null != defaultView) && (null != defaultView.NetObjectType))
            {
                args.IsValid = (false == string.IsNullOrEmpty(_profile.DefaultNetObjectID));
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    /**
     * added by PJOHN
     * BugFix #6073
     * Displays/Hide calendar for selecting Account Expiration Date
     */
    protected void ExpiresCalendarButton_Click(object sender, EventArgs e)
    {
        ExpiresCalendar.Visible = !ExpiresCalendar.Visible;
        if (ExpiresCalendar.Visible)
        {
            DateTime tmpExpires;
            if (DateTime.TryParse(txtAccountExpires.Text, out tmpExpires))
            {
                ExpiresCalendar.SelectedDate = tmpExpires;
                ExpiresCalendar.VisibleDate = tmpExpires;
            }
        }
    }

    /**
     * added by PJOHN
     * BugFix #6073
     * Put selected Expiration date into AccountExpires Text field
     */
    protected void ExpiresCalendar_SelectionChanged(object sender, EventArgs e)
    {
        ExpiresCalendar.Visible = false;
        txtAccountExpires.Text = ExpiresCalendar.SelectedDate.ToShortDateString();
    }

    private static bool IsHomeTab(string tabName)
    {
        return tabName == "Home" || OrionModuleManager.GetCustomizations().Any(c => c.Action.Equals(Customization.CustomHomeTab) && c.Key.Equals(tabName));
    }

    private class UserWebView
    {
        public string AccountId { get; set; }
        public int WebViewId { get; set; }
        public string Uri { get; set; }

    }
}
