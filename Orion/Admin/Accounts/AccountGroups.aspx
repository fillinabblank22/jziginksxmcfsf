<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccountGroups.aspx.cs" Inherits="Orion_Admin_AccountGroups" 
MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_448 %>"%>

<%@ Register Src="~/Orion/Admin/Accounts/ManageAccountsTabs.ascx" TagPrefix="orion" TagName="ManageAccountTabs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Admin/Accounts/js/AccountGroups.js" />

    <style type="text/css">
        span.Label { white-space:nowrap; }
        .smallText {color:#979797;font-size:9px;}
        .add { background-image:url(/Orion/Admin/Accounts/images/icons/add_16x16.gif) !important; }
        .edit { background-image:url(/Orion/Admin/Accounts/images/icons/icon_edit.gif) !important; }
        .up { background-image:url(/Orion/Admin/Accounts/images/icons/icon_up.gif) !important; }
        .down { background-image:url(/Orion/Admin/Accounts/images/icons/icon_down.gif) !important; }     
        .del { background-image:url(/Orion/Admin/Accounts/images/icons/icon_delete.gif) !important; }
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #delDialog .dialogBody table td { padding-right: 10px; }
        #delDialog li { max-width: 320px; overflow: hidden; text-overflow: ellipsis; list-style-position: inside; white-space: nowrap; }
        #addDialog td.x-btn-center, #delDialog td.x-btn-center, #updateDialog td.x-btn-center { text-align: center !important; }
        #addDialog td, #delDialog td, #updateDialog td { padding-right: 0; padding-bottom: 0; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }      
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align: middle; }
        .x-grid3-scroller { height: 270px; }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <input type="hidden" name="AccountGroups_PageSize" id="AccountGroups_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("AccountGroups_PageSize")) %>' />
    <h1 style="font-size:large"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div style="padding: 1em 0 1em 0;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_449) %></div>
    <div style="width:100%">	
        <orion:ManageAccountTabs ID="ManageAccountTabs1" runat="server" />
        <div id="tabPanel" class="tab-top">
            <table class="GroupTable" cellpadding="0" cellspacing="0" width="100%" >
	            <tr valign="top" align="left">
		            <td id="gridCell" style="padding-right: 0px;">
                        <div id="Grid"></div>
		            </td>
	            </tr>
            </table>
	    </div>
    </div>  	
    <div id="delDialog" class="x-hidden">
	    <div class="x-window-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_450) %></div>
	    <div id="delDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="delDialogDescription"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="accounts" style = "white-space: nowrap;"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>
   </asp:Content>
