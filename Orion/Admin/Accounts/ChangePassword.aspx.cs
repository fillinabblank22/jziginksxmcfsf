﻿using System;
using System.Web.Security;
using System.Web.UI;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_Accounts_ChangePassword : System.Web.UI.Page
{
	protected string UserName
	{
		get
		{
			return string.IsNullOrEmpty(Request.QueryString["AccountID"]) ? User.Identity.Name : Request.QueryString["AccountID"];
		}
	}

	protected override void OnLoad(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		base.OnLoad(e);
	}

	protected void ChangePasswordClick(object sender, EventArgs e)
	{
		MembershipUser myUser = Membership.GetUser(this.UserName);

		if (null != myUser)
		{
			if (Page.IsValid)
			{
				try
				{
					// If password is blank, we use the ResetPassword() method, 
					//  which is a back-door to setting a blank password.
					if (string.IsNullOrEmpty(txtPassword.Text))
					{
						myUser.ResetPassword();
						this.DisplayStep2();
					}
					else
					{
						// fill in old password with a dummy entry
						if (myUser.ChangePassword("dummy", txtPassword.Text))
						{
							this.DisplayStep2();
						}
						else
						{
							phErrorMessage.Controls.Clear();
							phErrorMessage.Controls.Add(new LiteralControl(Resources.CoreWebContent.WEBCODE_TM0_80));
						}
					}
				}
				catch
				{
					phErrorMessage.Controls.Clear();
					phErrorMessage.Controls.Add(new LiteralControl(Resources.CoreWebContent.WEBCODE_TM0_80));
				}
			}
		}
	}

	protected void CancelContinueClick(object sender, EventArgs e)
	{
		ReferrerRedirectorBase.Return("/Orion/Admin/Accounts/Accounts.aspx");
	}

	private void DisplayStep2()
	{
		this.paraPasswordBoxes.Visible = false;
		this.btnChangePassword.Visible = false;
		this.btnContinue.Visible = true;
		this.phSuccessMessage.Visible = true;
		this.btnCancel.Visible = false;
	}
}
