<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="ExternalWebsites.aspx.cs" Inherits="Orion_Admin_ExternalWebsites" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_TM0_44 %>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
<orion:Include runat="server" File="ExternalWebsites.css" />
<script type="text/javascript">
//<![CDATA[
    $(function () {
        var fillDialog = function (site) {
            WebAdmin.GetMenuBarByExternalWebsite(site.ID, function (result) {
                if (site.ID > 0)
                    $('#<%= this.lbMenuBars.ClientID %>')[0].value = result;
                else
                    $('#<%= this.lbMenuBars.ClientID %>')[0].value = $('#<%= this.lbMenuBars.ClientID %>')[0][0].value;
            }, reportError);

            $("#fullTitle").val(site.FullTitle);
            $("#shortTitle").val(site.ShortTitle);
            $("#url").val(decodeURI(site.URL));
        };

        var fillSite = function (site) {
            site.FullTitle = $("#fullTitle").val();
            site.ShortTitle = $("#shortTitle").val();
            site.URL = encodeURI($("#url").val());
        };

        var showDialog = function (title) {
            var encodedTitle = $("<div/>").text(title).html();
            $("#editSiteDialog").show().dialog({
                width: 625,  modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle
            });
        };

        var reportError = function (error) {
            alert(error.get_message() + '\n' + error.get_stackTrace());
        };

        var trim = function (s) {
            return s.replace(/^\s+|\s+$/, '');
        };

        var validateSite = function (site) {
		    site.ShortTitle = trim(site.ShortTitle).replace(/{/g, "{\u2063");
            site.FullTitle = trim(site.FullTitle).replace(/{/g, "{\u2063");
            site.URL = trim(site.URL);
            if (site.ShortTitle.length == 0) {
                alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_1) %>');
                return false;
            }
            
            return true;
        };

        function isValidHttpUrl(url) {
            url = encodeURI(trim(url));

            if (url.length == 0) {
                alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_2) %>');
                return false;
            }

            let uri;
            try {
                uri = new URL(url);
            } catch (_) {
                alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_270) %>');
                return false;
            }

            if (!(uri.protocol === "http:" || uri.protocol === "https:")) {
                alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_270) %>');
                return false;
            }

            return true;
        }

        var addSite = function (site) {
            var row = $("#siteRowTemplate").clone().appendTo($("#websites tbody"));
            row.find(".siteShortTitle").text(site.ShortTitle);
            if (site.FullTitle)
                row.find(".siteFullTitle").text(site.FullTitle);
            else
                row.find(".siteFullTitle").html("<em><%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_3) %></em>");
            row.find(".siteUrl").text(site.URL);

            WebAdmin.GetMenuBarByExternalWebsite(site.ID, function (result) {
                row.find(".menuBarName").text(result);
            }, reportError);

            row.find("#<%= this.locBLEdit.ClientID %>").click(function () {
                fillDialog(site);
                $("#<%= this.dialogOk.ClientID %>").unbind().click(function () {
                if (isValidHttpUrl($("#url").val()))
                {
                    fillSite(site);
                    if (validateSite(site)) {
                        var menuBar = $('#<%= this.lbMenuBars.ClientID %>')[0];
                        WebAdmin.UpdateExternalWebsite(site, menuBar.value, function () {
                            $("#editSiteDialog").dialog("close");
                            window.location.reload(true); // reload page to pick up menubar changes						
                        }, reportError);
                    }
                }
            });
                var title = String.format('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_4) %>', site.ShortTitle);
                showDialog(title);
                return false;
            });
            row.find("#<%= this.locBLPreview.ClientID %>").attr('href', "/Orion/External.aspx?Site=" + site.ID);
            row.find("#<%= this.locBLDelete.ClientID %>").click(function () {
                var title = String.format('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_5) %>', site.ShortTitle);
                if (confirm(title)) {
                    WebAdmin.DeleteExternalWebsite(site.ID, function () {
                        window.location.reload(true); // reload page to pick up menubar changes
                    }, reportError);
                }
                return false;
            });
        };

        var refreshSites = function () {
            $("#websites tbody").empty();
            $("#noItemsRow").hide();
            $("#loading").show();
            WebAdmin.GetExternalWebsites(function (sites) {
                $("#loading").hide();
                if (sites.length == 0) $("#noItemsRow").show();
                $(sites).each(function () { addSite(this); });
                if (typeof setFooter == "function") setFooter();
            }, function (error) {
                $("#loading").hide();
                alert(error.get_message() + '\n' + error.get_stackTrace());
            });
        };

        $("#<%= this.dialogCancel.ClientID %>").click(function () {
            $("#editSiteDialog").dialog('close');
        });

        $("#<%= this.createWebsite.ClientID %>").click(function () {
            var site = { ID: 0, ShortTitle: '', FullTitle: '', URL: '' };
            fillDialog(site);
            $("#<%= this.dialogOk.ClientID %>").unbind().click(function () {
                if (isValidHttpUrl($("#url").val())) {
                    fillSite(site);

                    if (validateSite(site)) {
                        var menuBar = $('#<%= this.lbMenuBars.ClientID %>')[0];
                        WebAdmin.CreateExternalWebsite(site, menuBar.value, function () {
                            $("#editSiteDialog").dialog("close");
                            window.location.reload(true); // reload page to pick up menubar changes
                        }, reportError);
                    }
                }
            });
            showDialog("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_TM0_132) %>");
            return false;
        });

        refreshSites();
    });
//]]>
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	<Services>
		<asp:ServiceReference path="../Services/WebAdmin.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

<table id="websites" cellspacing="0">
<thead>
	<tr>
		<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_126) %></th>
		<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_127) %></th>
		<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_128) %></th>
		<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_129) %></th>
		<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_130) %></th>
	</tr>
</thead>
<tbody></tbody>
<tfoot>
<tr id="loading" style="display:none;">
	<td style="text-align:center;" colspan="4"><img src="/Orion/images/AJAX-Loader.gif" alt="" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %></td>
</tr>
<tr id="noItemsRow" style="display:none;">
	<td style="text-align:center;font-style:italic;" colspan="4"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_131) %></td>
</tr>
</tfoot>
</table>

<br />

<div class="sw-btn-bar">
    <orion:LocalizableButtonLink class="siteEdit" ID="createWebsite" NavigateUrl="#" 
        DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_138 %>" runat="server" />
</div>

<div id="editSiteDialog" style="display:none;">
	<table width="100%">
		<tbody>
			<tr>
				<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_133) %></th>
				<td><input type="text" id="shortTitle" /></td>
			</tr>
			<tr>
				<td></td>
				<td class="help"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_134) %></td>
			</tr>
			<tr>
				<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_135) %></th>
				<td><input type="text" id="fullTitle" style="width: 95%" /></td>
			</tr>
			<tr>
				<td></td>
				<td class="help"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_136) %></td>
			</tr>
			<tr>
				<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_137) %></th>
				<td><input type="text" id="url" style="width: 95%" /></td>
			</tr>
			<tr>
			    <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_26) %></th>
			    <td><asp:ListBox runat="server" ID="lbMenuBars" SelectionMode="Single" Rows="1" DataTextField="Name" DataValueField="Name"></asp:ListBox></td>
			</tr>
            <tr>
                <td></td>
                <td>
                    <div class="sw-btn-bar">
                        <orion:LocalizableButtonLink ID="dialogOk" DisplayType="Primary" LocalizedText="Ok" runat="server" />
                        <orion:LocalizableButtonLink ID="dialogCancel" DisplayType="Secondary" LocalizedText="Cancel" runat="server" />
                    </div>
                </td>
            </tr>
		</tbody>
	</table>

	<%--<div style="text-align: right; padding: 10px;">
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButtonLink ID="dialogOk" DisplayType="Primary" LocalizedText="Ok" runat="server" />
            <orion:LocalizableButtonLink ID="dialogCancel" DisplayType="Secondary" LocalizedText="Cancel" runat="server" />
        </div>
	</div>--%>
</div>

<table style="display:none;">
<tbody>
<tr id="siteRowTemplate">
	<td class="siteShortTitle"></td>
	<td class="siteFullTitle"></td>
	<td class="siteUrl"></td>
	<td class="menuBarName"></td>
	<td>
        <div class="sw-btn-bar">
            <orion:LocalizableButtonLink class="siteEdit" ID="locBLEdit" NavigateUrl="#" 
                DisplayType="Small" LocalizedText="Edit" runat="server" />
            <orion:LocalizableButtonLink target="_blank" class="siteTest" ID="locBLPreview" NavigateUrl="#" 
                DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_141 %>" runat="server" />
            <orion:LocalizableButtonLink class="siteDelete" ID="locBLDelete" NavigateUrl="#" 
                DisplayType="Small" LocalizedText="Delete" runat="server" />
        </div>
	</td>
</tr>
</tbody>
</table>

</asp:Content>
