﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Admin;

/// <summary>
/// NOTE: this control is not used in voltron.  It is only here so modules (like NPM)
/// have something to compile against.  The class that is actually used is <see cref="Orion_Admin_VoltronAdminBucket"/>
/// </summary>
[Obsolete("Use of this control should be replaced by Orion_Admin_VoltronAdminBucket")]
public partial class Orion_Admin_AdminBucket : System.Web.UI.UserControl
{
	private static readonly Log Log = new Log();

	private int _columnCount = 3;
    private static HashSet<String> _hiddenLinks = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.SettingsHideLink)).Select(c => c.Key));

    public static HashSet<String> _hiddenSections = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.SettingsHideSection)).Select(c => c.Key));

    private AdminLinkConditionsProvider _linkConditionsProvider = new AdminLinkConditionsProvider();

	public int ColumnCount
	{
		get { return _columnCount; }
		set { _columnCount = value; }
	}
	public string Header { get; set; }
	public string Icon { get; set; }
	public ITemplate Description { get; set; }


    public bool NewStyle { get; set; }
    public bool WithoutHeader { get; set; }

	// for simplicity allow single custom column template (put it in first row)
	public ITemplate CustomColumn { get; set; }
	public int CustomColumnIndex { get; set; }

	internal class LinkSpec
	{
		internal string Href;
		internal string Label;
		internal int Order;
		internal bool IsCustom;
        internal string Description;
        internal string Target;

		internal LinkSpec(string href, string label, int order)
		{
			Href = href;
			Label = label;
			Order = order;
			IsCustom = false;
		}

	    internal LinkSpec(string href, string label, int order, string description): this(href,label,order)
	    {
            Description = string.IsNullOrEmpty(description) ? string.Empty : description;
		}

	    internal LinkSpec(string href, string label, int order, string description, string target)
	        : this(href, label, order, description)
	    {
            Target = string.IsNullOrEmpty(target) ? string.Empty : target;
	    }

		internal LinkSpec()
		{
			IsCustom = true;
		}
	}

	internal class RowLinks
	{
		internal LinkSpec[] Columns;
		internal RowLinks(int cnt)
		{
			Columns = new LinkSpec[cnt];
		}
	}

	protected int ColumnWidth
	{
		get { return 100 / ColumnCount; }
	}

    private List<LinkSpec> links = new List<LinkSpec>();

    public void AddLink(string href, string label, int order, string description, string target)
    {
        // Add link to the bucket if not marked to be hidden. Matches using "contains" (e.g. partial) rather than "equals" pattern.
        // Such approach allows to match absolute and relative paths using the same pattern.
        if (!_hiddenLinks.Any(href.Contains))
        {
            links.Add(new LinkSpec(href, label, order, description, target));
        }
    }

	public void AddLink(string href, string label, int order, string description)
	{
        AddLink(href, label, order, description, null);
	}

	public void AddLink(string href, string label, int order)
	{
	    AddLink(href, label, order, null);
	}

    public void AddAdminLinks(AdminLink[] adminLinks)
    {
        if (adminLinks == null)
            return;

        var currentGroupLinks = adminLinks.Where(a => a.TargetGroup == ID);
        foreach (AdminLink adminLink in currentGroupLinks)
        {
            var condition = _linkConditionsProvider[adminLink.Condition];
            if (condition == null && adminLink.Condition != null)
            {
                Log.Error($"Implementation for condition '{adminLink.Condition}' not found");
            }

            if (condition == null || condition.LinkShouldBeShown(adminLink))
            {
                AddLink(adminLink.Href, adminLink.Label, adminLink.Order);
            }
        }
    }

	public void RemoveLink(string label) // For Demo Server customization
    {
        var linkToRemove = links.SingleOrDefault(i => i.Label.Equals(label));
        if(linkToRemove != null)
            links.Remove(linkToRemove);
    }

	private List<RowLinks> MakeRowLinks()
	{
		links.Sort(OrderComparer);

		int linkCount = links.Count;
		if (CustomColumn != null) linkCount++;
		int rowCount = linkCount / ColumnCount;
		if (linkCount % ColumnCount > 0) rowCount++;
		List<RowLinks> result = new List<RowLinks>(rowCount);
		int idx = 0;
		for (int row = 0; row < rowCount; row++)
		{
			RowLinks rLinks = new RowLinks(ColumnCount);
			for (int clm = 0; clm < ColumnCount; clm++)
			{
				// 1. Custom column - allow in first row only
				if (row == 0 && CustomColumn != null && CustomColumnIndex == clm)
				{
					rLinks.Columns[clm] = new LinkSpec();
				}
				// 2. Standard column
				else if (idx < links.Count)
				{
					rLinks.Columns[clm] = links[idx];
					idx++;
				}
				// 3. Empty column
				else
				{
					rLinks.Columns[clm] = null;
				}
			}
			result.Add(rLinks);
		}
		return result;
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Description != null)
		{
			Description.InstantiateIn(placeDescription);
		}

		rptRows.DataSource = MakeRowLinks();
		rptRows.DataBind();
	}

	protected void RowDataBound(object sender, RepeaterItemEventArgs e)
	{
		RowLinks row = (RowLinks)e.Item.DataItem;
		Repeater r = (Repeater)e.Item.FindControl("rptCols");
		r.DataSource = row.Columns;
		r.DataBind();
	}

	protected void ColumnDataBound(object sender, RepeaterItemEventArgs e)
	{
		MultiView container = (MultiView)e.Item.FindControl("container");
		LinkSpec col = (LinkSpec)e.Item.DataItem;
		if (col == null)
		{
			container.ActiveViewIndex = 1;
		}
		else if (col.IsCustom)
		{
			container.ActiveViewIndex = 2;
			CustomColumn.InstantiateIn(container.GetActiveView());
		}
		else
		{
			container.ActiveViewIndex = 0;
			HyperLink href = (HyperLink)container.FindControl("href");
		    Label description = (Label) container.FindControl("description");
			href.NavigateUrl = col.Href;
			href.Text = col.Label;
		    href.Target = col.Target;
		    description.Text = col.Description;
		}
	}

	private int OrderComparer(LinkSpec ls1, LinkSpec ls2)
	{
		int o1 = (ls1 == null || ls1.IsCustom ? int.MaxValue : ls1.Order);
		int o2 = (ls2 == null || ls2.IsCustom ? int.MaxValue : ls2.Order);

		if (o1 > o2) { return 1; }
		if (o1 < o2) { return -1; }
		return 0;
	}

}
