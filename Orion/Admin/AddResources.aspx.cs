using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;
using Infragistics.WebUI.UltraWebNavigator;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_AddResources : System.Web.UI.Page
{
	protected int Column;
	protected ViewInfo View;

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

	protected void ResourceTemplateTree_Init(object sender, EventArgs e)
	{
		int viewId = int.Parse(Request["ViewID"]);
		View = ViewManager.GetViewById(viewId);

		Column = int.Parse(Request["Column"]);
		Title = string.Format(Resources.CoreWebContent.WEBCODE_VB0_310, View.ViewTitle, Column);

		IEnumerable<ResourceTemplateCategory> categories = ResourceTemplateManager.GetCategoriesForViewType(View.ViewType);

		foreach (ResourceTemplateCategory category in categories)
		{
            var fullTitle = String.IsNullOrEmpty(category.SubTitle) ? 
                String.Format("{0}", Server.HtmlEncode(category.Title)) 
                : string.Format("{0} - {1}", Server.HtmlEncode(category.Title), Server.HtmlEncode(category.SubTitle));
		
			Node catNode = ResourceTemplateTree.Nodes.Add(fullTitle, category.Title);
			foreach (ResourceTemplate template in category.ResourceTemplates)
			{
				if (template.Title != null && template.Title != BaseResourceControl.notAvailable && template.IsInternal == false )
                {
                    Node templateNode = new Node();
                    templateNode.Text = Server.HtmlEncode(template.Title);
                    templateNode.Tag = template.TemplatePath;
                    templateNode.CheckBox = string.IsNullOrEmpty(template.Title) ? CheckBoxes.False : CheckBoxes.True;
                    catNode.Nodes.Add(templateNode);
                }
			}
		}
	}

	protected void Submit_Click(object sender, EventArgs e)
	{
		// append the selected resources to the view/column and redirect to CustomizeView.aspx?ViewID=xx
		ProcessNodeList(ResourceTemplateTree.Nodes);
        ReferrerRedirectorBase.Return(string.Format("CustomizeView.aspx?ViewID={0}", View.ViewID));
	}

	private void ProcessNodeList(Nodes nodes)
	{
		foreach (Node node in nodes)
		{
			if (node.Checked)
			{
				ResourceTemplate template;
				string path = (string)node.Tag;

                template = ResourceTemplateManager.CreateFromVirtualPath(path);

				ResourceInfo info = ResourceManager.CreateFromTemplate(template, View);
				info.Column = Column;
				ResourceManager.Insert(info);
			}

			ProcessNodeList(node.Nodes);
		}
	}
}
