using System;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;
using SolarWinds.Orion.Security;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_AlertSettings : System.Web.UI.Page
{
    private const string DefaultReturnUrl = "~/Orion/Admin";

    private const string DefaultEmailSupportTls = "DefaultEmailSupportTls";
    private const string DefaultEmailSmtpAddress = "DefaultEmailSmtpAddress";
    private const string DefaultEmailSmtpPort = "DefaultEmailSmtpPort";
    private const string DefaultEmailSmtpSSL = "DefaultEmailSmtpSSL";
    private const string DefaultEmailUserName = "DefaultEmailUserName";
    private const string DefaultEmailPassword = "DefaultEmailPassword";
    private static Log log = new Log();

    private readonly CryptoHelper cryptoHelper = new CryptoHelper();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (Request.Params.Get("__EVENTTARGET") == "SaveSettings")
            {
                SaveSettings();
            }
            return;
        }

        InitSettings();

        // block saving alert settings in case of demo mode
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            btnAlertSettingsSave.OnClientClick = "demoAction('Core_Alerting_SaveAlertSettings', this); return false;";
        }
    }

    private void InitSettings()
    {
        var emailTo = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailTo, string.Empty);
        txtEmailTo.Value = emailTo;

        var emailFrom = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailFrom, string.Empty);
        txtEmailFrom.Value = emailFrom;

        var emailSenderName = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailSenderName, string.Empty);
        txtEmailSenderName.Text = emailSenderName;

        var emailCc = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailCc, string.Empty);
        txtEmailCc.Value = emailCc;

        var emailBcc = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailBcc, string.Empty);
        txtEmailBcc.Value = emailBcc;

        cbSupportTls.Checked = WebSettingsDAL.GetValue(DefaultEmailSupportTls, false);

        var smtpServer = new SmtpServer();
        var smtpAddress = WebSettingsDAL.GetValue(DefaultEmailSmtpAddress, string.Empty);
        smtpServer.Address = smtpAddress;

        var smtpPort = WebSettingsDAL.GetValue(DefaultEmailSmtpPort, string.Empty);
        int smtpPortValue;
        smtpServer.Port = int.TryParse(smtpPort, out smtpPortValue) ? smtpPortValue : 25;

        var enableSmtpSSL = WebSettingsDAL.GetValue(DefaultEmailSmtpSSL, string.Empty);
        bool enableSmtpSSLValue;
        if (bool.TryParse(enableSmtpSSL, out enableSmtpSSLValue))
        {
            smtpServer.EnableSSL = enableSmtpSSLValue;
        }

        var userName = WebSettingsDAL.GetValue(DefaultEmailUserName, string.Empty);
        var password = WebSettingsDAL.GetValue(DefaultEmailPassword, string.Empty);

        if (!string.IsNullOrEmpty(userName) || !string.IsNullOrEmpty(password))
        {
            password = cryptoHelper.Decrypt(password);
            smtpServer.Credentials = new SmtpServerCredential { Username = userName, Password = password };
        }

        SmtpSettings.SmtpServer = smtpServer;
    }

    private void SaveSettings()
    {
        var emailTo = (string)DefaultSanitizer.SanitizeHtml(txtEmailTo.Value);
        WebSettingsDAL.SetValue(EmailConstants.DefaultEmailTo, emailTo);

        var emailFrom = (string)DefaultSanitizer.SanitizeHtml(txtEmailFrom.Value);
        WebSettingsDAL.SetValue(EmailConstants.DefaultEmailFrom, emailFrom);

        var emailSenderName = (string)DefaultSanitizer.SanitizeHtml(txtEmailSenderName.Text);
        WebSettingsDAL.SetValue(EmailConstants.DefaultEmailSenderName, emailSenderName);

        var emailCc = (string)DefaultSanitizer.SanitizeHtml(txtEmailCc.Value);
        WebSettingsDAL.SetValue(EmailConstants.DefaultEmailCc, emailCc);

        var emailBcc = (string)DefaultSanitizer.SanitizeHtml(txtEmailBcc.Value);
        WebSettingsDAL.SetValue(EmailConstants.DefaultEmailBcc, emailBcc);

        WebSettingsDAL.SetValue(DefaultEmailSupportTls, cbSupportTls.Checked);

        var smtpServer = SmtpSettings.SmtpServer;

        if (SmtpSettings.IsPasswordChanged && smtpServer.Credentials?.Password != null)
        {
            smtpServer.Credentials.Password = cryptoHelper.Encrypt(smtpServer.Credentials.Password);
        }

        WebSettingsDAL.SetSmtpServerAsDefault(smtpServer, SmtpSettings.IsPasswordChanged);

        try
        {
            using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
            {
                int newDefaultSmtpServerID;
                var avalibleServerList = proxy.GetAvailableSmtpServers();
                var newDefaultSmtpServer = avalibleServerList.FirstOrDefault(x => x.Equals(SmtpSettings.SmtpServer));
                newDefaultSmtpServerID = newDefaultSmtpServer == null ?
                    proxy.InsertSmtpServer(SmtpSettings.SmtpServer) :
                    newDefaultSmtpServer.ServerID;

                proxy.SetSmtpServerAsDefault(newDefaultSmtpServerID);
            }
        }
        catch (Exception ex)
        {
            log.Error("Couldn't set as default SMTPServer.", ex);
            throw;
        }

        Response.Redirect(DefaultReturnUrl);
    }

    protected void CancelAlertSettings(object sender, EventArgs e)
    {
        Response.Redirect(DefaultReturnUrl);
    }
}
