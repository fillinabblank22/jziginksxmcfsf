﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_GenericNotifications : BaseNotificationsPage
{
    protected NotificationItemType SelectedNotificationItemType { get; set; }
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        NotificationsControl.RefreshContent();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        try
        {
            // Get selected NotificationItemType ID:
            var typeId = new Guid(Request.Params["TypeId"]);

            // Load selected type from DB:
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                SelectedNotificationItemType = proxy.GetNotificationItemTypeById(typeId);
                if (SelectedNotificationItemType == null)
                    throw new Exception();
            }
        }
        catch
        {
            throw new Exception("Can't obtain Notification Type specified by URL parameter.");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = SelectedNotificationItemType.Description;
        NotificationsControl.SelectedNotificationItemType = SelectedNotificationItemType;
    }
}
