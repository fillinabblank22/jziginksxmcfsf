<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"  
        Title="Orion SWIS Invoke" EnableViewState="false" %>
<%-- disable ViewState on this page, we really don't need to maintain anything across postbacks --%>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.2.0"/>

<style type="text/css">
    #contents
    {
        width:80%;
    }
    
    h1 
    {
        font-size: 12pt;
    }

    #textareaDiv textarea
    {
        width: 100%;
        border: 1px solid #8FB3D4;
        font-family: Monaco, 'Courier New', Courier, monospace;
        font-size: 11px;
        padding: 2px;
        width: 100%;        
    }
    #textareaDiv
    {
        padding: 10px 0px;
    }
    
    #results 
    {
        padding-top: 20px;
    }
    
    .x-grid3-row-alt 
    {
        background-color: #f8f8f8;
    }
    

</style>

<script language="javascript" type="text/javascript">
    $().ready(function () {
        var itemsForSwis = [];

        function htmlEscape(str) {
            return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
        }

        function onSubmitCompleted(rv) {
            $('#result').html('<pre>' + htmlEscape(rv) + '</pre>');
        }
        function onSubmitFailed(rv) {
            $('#result').html('<b>Error: ' + rv._message + '</b>');
        }
        function submitSwis() {
            $('#result').html('Working.......');
            var params = [];
            for (var i = 0; i < itemsForSwis.length; i++) {
                var itm = $(itemsForSwis[i]);
                if (itm.attr('type') == 'checkbox') {
                    params.push(itm.attr('checked') == 'checked' ? 'true' : 'false');
                }
                else {
                    params.push(itm.val());
                }
            }
            Information.Invoke($('#entites').val(), $('#verbs').val(), params, onSubmitCompleted, onSubmitFailed);
        }

        function createForType(row) {
            var rv = $(document.createElement('input'));
            if (row.type == 'System.String' || row.type.indexOf('System.Int') == 0 || row.type == 'System.DateTime') {
                rv.attr('type', 'text');
            }
            else if (row.type == 'System.Boolean') {
                rv.attr('type', 'checkbox');
            }
            else {
                return 'unsupported type';
            }
            if (row.type.indexOf('[]') != -1) {
                var cnt = $(document.createElement('span'));
                cnt.append(rv);
                cnt.append('Insert items, separated by `;`');
                rv = cnt;
            }
            return rv;
        }
        function loadVerbParams(result) {
            itemsForSwis = [];

            var p = $('#params');
            p.children().remove();
            p.append('<tr><th>Parameter name</th><th>Type</th><th>Value</th></tr>');
            var canBeCalled = true;

            for (var i = 0; i < result.length; i++) {
                var tr = $(document.createElement('tr'));
                tr.append('<td>' + result[i].name + '</td>');
                tr.append('<td>' + result[i].type + '</td>');
                var td = $(document.createElement('td'));
                td.append(createForType(result[i]));
                var inputForMe = td.find('input');
                if (inputForMe.length == 0) {
                    canBeCalled = false;
                }
                else {
                    itemsForSwis[result[i].position] = inputForMe[0];
                }
                tr.append(td);
                p.append(tr);
            }
            
            var callButton = $('#callButton');
            if (canBeCalled) {
                callButton.removeAttr('disabled');
                callButton.attr('value', 'Call');
            }
            else {
                callButton.attr('disabled', 'disabled');
                callButton.attr('value', 'Cannot be called - unsupported types');
            }
        }
        function loadEntityVerbs(result) {
            var verbs = $('#verbs');

            verbs.children('option').remove();
            verbs.append('<option>Select one</option>');
            for (var i = 0; i < result.length; i++) {
                verbs.append('<option>' + result[i] + '</option>');
            }
            verbs.width($('#entites').width());

            verbs.change(function () {
                if (verbs.val() == 'Select one' || verbs.val() == '') {
                    $('#paramsTr').css('display', 'none');
                }
                else {
                    $('#paramsTr').css('display', '');
                    Information.GetVerbParams($('#entites').val(), verbs.val(), loadVerbParams);
                }
            });
        }

        Information.GetEntitiesWithVerbs(
            function (result) {
                var entities = $('#entites');
                entities.children('option').remove();
                entities.append('<option>Select one</option>');
                for (var i = 0; i < result.length; i++) {
                    entities.append('<option>' + result[i] + '</option>');
                }

                entities.change(function () {
                    $('#verbs').unbind('change');

                    if (entities.val() == 'Select one') {
                        $('#verb').css('display', 'none');
                        $('#paramsTr').css('display', 'none');
                    }
                    else {
                        $('#verb').css('display', '');

                        $('#paramsTr').css('display', 'none');
                        $('params').children().remove();

                        Information.GetEntityVerbs(entities.val(), loadEntityVerbs);
                    }
                });
            });

        $('#callButton').click(submitSwis);
    });
</script>


</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<asp:ScriptManagerProxy ID="ScriptManager1" runat="server" >
		<Services>
			<asp:ServiceReference path="../Services/Information.asmx" />
		</Services>
	</asp:ScriptManagerProxy>

    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <table>
        <tr>
            <td style="width: 9em;">
                Entity:
            </td>
            <td>
                <select id="entites">
                    <option>Loading...</option>
                </select>
            </td>
        </tr>
        <tr id="verb" style='display: none'>
            <td>
                Verb:
            </td>
            <td><select id="verbs">
                    <option>Loading...</option>
                </select></td>
        </tr>
        <tr id="paramsTr" style='display: none'>
            <td colspan="2">
                <table id="params">
                </table>
                <input type="button" value="Call" id="callButton" />
            </td>
        </tr>
        <tr><td id="result" colspan="2"></td></tr>
    </table>
    
</asp:Content>