﻿using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Extensions;
using System.Web;
using System;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_ViewNOCEnabler : BaseViewGroupControl
{
	public string NocViewEnableChangedJs { get; set; }

	protected string NOCViewUrl
	{
		get
		{
            var newParams = new Dictionary<string, string>()
                {
                    {"ViewID", this.View.ViewID.ToString()},
                    {"isNOCView", "true"}
                };

            if (!string.IsNullOrEmpty(Request["NetObject"]))
            {
                newParams["NetObject"] = Request["NetObject"];
            }


            var returnToUrl = ReferrerRedirectorBase.GetDecodedReferrerUrl();
            var returnToParams = UrlHelper.ParseQueryString(returnToUrl).Merge(newParams);
            return UrlHelper.ReplaceQueryString("/Orion/Admin/Preview.aspx", returnToParams);
		}
	}

	protected override void RebuildUI()
	{
		chkEnableNOCView.Checked = this.View.NOCView;
        if (this.View.NOCView)
        {
			if (this.View.NOCViewRotationInterval >= 60 && (this.View.NOCViewRotationInterval % 60) == 0) 
			{
				rotateTime.Value = (this.View.NOCViewRotationInterval / 60).ToString();
				rotateTimeType.Value = Resources.CoreWebContent.WEBDATA_IB0_48;
				return;
			}

            rotateTime.Value = this.View.NOCViewRotationInterval.ToString();
        }
	}
}