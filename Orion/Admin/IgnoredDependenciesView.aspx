<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/LimitedAdminPage.master" AutoEventWireup="true"
    CodeFile="IgnoredDependenciesView.aspx.cs" Inherits="Orion_Admin_IgnoredDependenciesView" Title="<%$HtmlEncodedResources:CoreWebContent,ResourcesAll_ManageIgnoredDependencies%>" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <style type="text/css">
        .error
        {
            border: 2px solid red;
        }
        #gridCell div#Grid
        {
            width: 100%;
        }
        #adminContent td
        {
            padding: 0px 0px 0px 0px !important;
            vertical-align: middle;
        }
        .no-border .x-table-layout-ct, .panel-no-border .x-panel-body-noheader
        {
            border: none !important;
        }
        .hide-header .x-grid3-header
        {
            display: none;
        }
        .x-tip-body 
        {
            white-space: nowrap !important;
        }
        .createtip-primary {
            font-weight: bold;
            margin-right: 5px;
        }
        .createtip-secondary {
        }

    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 style="font-size:large!important;"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
            <asp:ServiceReference Path="/Orion/Services/WebAdmin.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <orion:Include runat="server" File="Admin/js/IgnoredDependenciesGrid.js" />

    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div style="padding: 15px 0;">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="DependenciesGrid-ErrorMessage" style="width: 957px"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top:4px!important">
                <orion:NavigationTabBar ID="IgnoredDependenciesTabBar" runat="server" Visible="true">   
                    <orion:NavigationTabItem Name="<%$HtmlEncodedResources:CoreWebContent,ResourcesAll_ManageDependencies%>" Url="~/Orion/Admin/DependenciesView.aspx" />
                    <orion:NavigationTabItem Name="<%$HtmlEncodedResources:CoreWebContent,ResourcesAll_ManageIgnoredDependencies%>" Url="~/Orion/Admin/IgnoredDependenciesView.aspx" />
               </orion:NavigationTabBar>

                <input type="hidden" name="Dependencies_PageSize" id="Dependencies_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("Dependencies_PageSize")) %>' />

                <div id="DependenciesGrid">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
