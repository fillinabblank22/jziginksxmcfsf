<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccountManager.aspx.cs" Inherits="Orion_Admin_AccountManager" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="Account Manager" %>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <table>
        <tr>
            <td>
                <asp:ListBox runat="server" ID="accountList" SelectionMode="Single" Rows="20" />
            </td>
            <td valign="top">
                <asp:ImageButton runat="server" ID="editButton" ImageUrl="~/Orion/images/Button.Edit.gif" OnClick="EditButtonClick" /><br />
                <asp:ImageButton ID="changePasswordButton" runat="server" ImageUrl="~/Orion/images/Button.ChangePassword.gif" OnClick="ChangePasswordButtonClick" /><br />
                <br />
                <asp:ImageButton runat="server" ID="addButton" ImageUrl="~/Orion/images/Button.Add.gif" OnClick="AddAccountClick" /><br />
                <asp:ImageButton runat="server" ID="deleteButton" ImageUrl="~/Orion/images/Button.Delete.gif" OnClick="DeleteAccountClick" />
            </td>
        </tr>
    </table>
    <div id="statusMessage">
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="accountList" 
                                    ErrorMessage="Please select an account." Display="Static" EnableClientScript="false" />
        <asp:PlaceHolder runat="server" ID="phErrorMessage" />
    </div>
    
</asp:Content>