﻿using System;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_ViewGroupAddTab : System.Web.UI.Page
{
    protected ViewInfo View;

    protected override void OnInit(EventArgs e)
    {
        int viewId;
        int viewGroup;
        int parentViewId = 0;

        if (!int.TryParse(Request["ViewID"], out viewId))
            this.Response.Redirect("ListViews.aspx");
        if (!int.TryParse(Request["ViewGroup"], out viewGroup))
            this.Response.Redirect("ListViews.aspx");

        if (viewId == 0)
        {
            ViewInfo parentView;
            // reserved for "Add tab" link
            if (viewGroup == 0)
            {
                // Sub nav was not enabled yet
                if (!int.TryParse(Request["ParentView"], out parentViewId) || parentViewId == 0)
                {
                    throw new Exception("ParentView URL parameter (> 0) must be provided for \"Add tab\" link.");
                }
                parentView = ViewManager.GetViewById(parentViewId);
                ViewManager.EnableViewGroup(parentView);
            }
            else
            {
                // Sub nav was already enabled
                parentView = ViewManager.GetFirstViewInGroup(viewGroup);
            }

            if (parentView == null)
            {
                throw new Exception("ParentView not defined: ViewGroup=" + viewGroup + " ParentViewId=" + parentViewId);
            }
            View = ViewManager.GetEmptyView(parentView);
        }
        else
            // common situation
            View = ViewManager.GetViewById(viewId);

        if (this.IsPostBack)
            this.phErrorMessage.Controls.Clear();

        UpdateViewColumnEditors();
    }

    protected override void OnLoad(EventArgs e)
    {
        Preview.DataBind();
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);

        AngularJsHelper.EnableAngularJsForPageContent();
    }

    private void UpdateViewColumnEditors()
    {
        //If we are creating a new view this could be null.
        if (View != null)
        {

            PageHeader.Text = Resources.CoreWebContent.WEBCODE_OJ0_2;
           
            myResourceEditor.View = View;
            myResourceColumnWidthEditor.View = View;
            myViewGroupEditor.View = View;

            if (View.IsStaticSubview)
            {
                myResourceEditor.Visible = false;
                myResourceColumnWidthEditor.Visible = false;
            }

            myViewGroupSubNav.IsEdit = true;
            myViewGroupSubNav.View = View;
            myViewGroupSubNav.IsActive = false;

            if (View.ViewID != 0)
            {
                Preview.NavigateUrl = "/Orion/Admin/Preview.aspx?ViewID=" + this.View.ViewID;
                Preview.Visible = true;
            }
        }
    }

    protected void DoneClick(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Return("ListViews.aspx");
    }
}