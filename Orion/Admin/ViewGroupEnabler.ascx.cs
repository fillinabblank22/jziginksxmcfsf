using System;
using System.Linq;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_ViewGroupEnabler : BaseViewGroupControl
{
    protected override void RebuildUI()
    {
        if (View.ViewGroup != 0)
        {
        	if(!this.Page.IsPostBack || Request.Form[chkEnableLeftNav.UniqueID] == null)
        	{
            	// if left nav was enabled on the background by something else and checkbox to enable was clicked right after, 
                // this setting would clear the checkbox event and that's not desired, hence the condition above
            	this.chkEnableLeftNav.Checked = true;    
            }
        }
    }

    private void EnableSubNav()
    {
    	if (View.ViewGroup == 0)
        {
        	ViewManager.EnableViewGroup(View);
        }
        RefreshPage();
    }

    private void DisableSubNav(bool moveResources)
    {
        ViewInfo ParentView = ViewManager.GetFirstViewInGroup(View.ViewGroup);
        ViewManager.RemoveViewGroup(View, moveResources);
        RefreshPage(ParentView.ViewID);
    }

    protected void chkEnableLeftNav_CheckedChanged(object sender, EventArgs e)
    {
        var removeResources = string.Equals(Request["__EVENTARGUMENT"], true.ToString(), StringComparison.InvariantCultureIgnoreCase);

        if (chkEnableLeftNav.Checked)
            EnableSubNav();
        else
            DisableSubNav(!removeResources);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.Visible = this.LeftNavAllowed();
    }

    private bool LeftNavAllowed()
    {
        // Disable left navigation for APM application details for APM < 5.3 if it wasn't enabled yet.
        if (View.ViewType.Trim().Equals("APM Application Details") && !chkEnableLeftNav.Checked)
        {
            var apm = OrionModuleManager.GetModules().FirstOrDefault(module => module.ShortTitle.Equals("APM"));
            if (apm != null && apm.Version < new Version(5,3))
            {
                return false;
            }
        }
        return true;
    }
}
