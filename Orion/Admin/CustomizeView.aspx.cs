using System;
using System.Web;
using System.Text;
using System.Web.Services;

using Resources;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.Helpers;
using System.Collections.Generic;
using System.Linq;

public partial class Orion_Admin_CustomizeView : System.Web.UI.Page
{
    private ViewInfo _view;
	private ViewInfo _parentView;
	protected ResourceInfoCollection Resources;
    public static HashSet<String> _hideViewLimitation = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.CustomizeViewHideSection)).Select(c => c.Key));
    protected ViewInfo View
	{
		get
		{
            if (_view == null)
            {
                int viewId = 0;
                if (!int.TryParse(Request["ViewID"], out viewId))
                    this.Response.Redirect("ListViews.aspx");

                if (viewId == 0)
                    _view = ViewManager.GetEmptyView(ParentView);
                else
                    _view = ViewManager.GetViewById(viewId);
            }
            return _view;
		}
	}

	protected ViewInfo ParentView
	{
		get
		{
			if (_parentView == null)
				_parentView = ViewManager.GetFirstViewInGroup(this.ViewGroup);
			return _parentView;
		}
	}

	private int ViewGroup
	{
		get
		{
			int viewGroup = 0;

			if (this._view != null)
				viewGroup = this._view.ViewGroup;
			else if (!int.TryParse(Request["ViewGroup"], out viewGroup))
				this.Response.Redirect("ListViews.aspx");

			return viewGroup;
		}
	}

	protected override void OnInit(EventArgs e)
	{
		if (this.IsPostBack)
			this.phErrorMessage.Controls.Clear();
		UpdateViewColumnEditors();
	}

	protected override void OnLoad(EventArgs e)
	{
		Preview.DataBind();
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
	    base.OnLoad(e);

        AngularJsHelper.EnableAngularJsForPageContent();
    }

	private void UpdateViewColumnEditors()
	{
		//If we are creating a new view this could be null.
		if (View != null)
		{
			PageHeader.Text = String.Format(CoreWebContent.WEBCODE_VB0_225, View.ViewGroupName);
			myResourceTitleEditor.View = View;

			myResourceEditor.View = View;
			myViewGroupEnabler.View = View;
			myViewNOCEnabler.View = View;
		    btnGoToNOC.Style["display"] = (View.NOCView) ? "inline-block" : "none";

			myViewGroupEditor.View = View;

            if (View.IsStaticSubview)
            {
                myResourceEditor.Visible = false;
            }

			if (View.ViewGroup != 0)
			{
				myViewGroupSubNav.IsEdit = true;
				myViewGroupSubNav.IsActive = false;
				myViewGroupSubNav.View = View;
			}
			else
			{
				myViewGroupEditor.Visible = false;
				myViewGroupSubNav.Visible = false;
			}

			if (View.ViewID == 0)
			{
				ViewLimitationEditor1.Visible = false;
				emptyTabLimitation.Visible = true;
			}
			else
			{

                var newParams = new Dictionary<string, string>()
                {
                    {"ViewID", this.View.ViewID.ToString()},
                    {"NetObject",  (!string.IsNullOrEmpty(Request["NetObject"]) ? Request["NetObject"] : "")}
                };


                var returnToUrl = ReferrerRedirectorBase.GetDecodedReferrerUrl();
                var returnToParams = UrlHelper.ParseQueryString(returnToUrl).Merge(newParams);

                Preview.NavigateUrl = UrlHelper.ReplaceQueryString("/Orion/Admin/Preview.aspx", returnToParams);
				Preview.Visible = true;
			}
		}
	}

	private void CheckValidReturnView()
	{
		string returnUrl = ReferrerRedirectorBase.GetDecodedReferrerUrl();

		if ((returnUrl.IndexOf("?", StringComparison.OrdinalIgnoreCase) >= 0) &&
			(returnUrl.IndexOf("ViewID", StringComparison.OrdinalIgnoreCase) >= 0))
		{
			string returnQueryString = returnUrl.Split('?')[1];

			var returnQueryStringValues = HttpUtility.ParseQueryString(returnQueryString);
			int returnViewID = 0;
			if ((returnQueryStringValues["ViewID"] == null) ||
				(!Int32.TryParse(returnQueryStringValues["ViewID"], out returnViewID)))
			{ // error ViewID is not provided
				return;
			}

			var returnView = ViewManager.GetViewById(returnViewID);
			var returnToUpdated = String.Empty;
			if (returnView == null)
			{//Return view has been deleted.
				returnUrl = HttpContext.Current.Request.Url.AbsoluteUri;
				if ((returnUrl.IndexOf("?", StringComparison.OrdinalIgnoreCase) >= 0) &&
					(returnUrl.IndexOf("ViewID", StringComparison.OrdinalIgnoreCase) >= 0) &&
					(returnUrl.IndexOf("ReturnTo", StringComparison.OrdinalIgnoreCase) >= 0))
				{
					returnQueryString = returnUrl.Split('?')[1];

					returnQueryStringValues = HttpUtility.ParseQueryString(returnQueryString);
					if ((returnQueryStringValues["ViewID"] == null) ||
						(!Int32.TryParse(returnQueryStringValues["ViewID"], out returnViewID)))
					{ // error ViewID is not provided
						return;
					}

					var encodedURL = returnQueryStringValues["ReturnTo"].Replace('-', '+').Replace('_', '/');
					byte[] input = Convert.FromBase64String(encodedURL);
					var returnTo = Encoding.UTF8.GetString(input);

					if (returnTo.IndexOf("?", StringComparison.OrdinalIgnoreCase) < 0)
					{ // error wrong ReturnTo url
						return;
					}

					returnToUpdated = UrlHelper.UrlAppendUpdateParameter(returnTo, "ViewID", returnViewID.ToString());
					returnView = ViewManager.GetViewById(returnViewID);
				}

				if (returnView != null)
				{
					ReferrerRedirectorBase.SetReferrerUrl(returnToUpdated);
				}
			}
		}
	}

	protected void DoneClick(object sender, EventArgs e)
	{
		CheckValidReturnView();
        myResourceEditor.UpdateColumnsWidth();

		string returnUrl = ReferrerRedirectorBase.GetDecodedReferrerUrl();
		if (returnUrl.IndexOf("isNOCView", StringComparison.OrdinalIgnoreCase) > -1 && !this.View.NOCView)
		{
			returnUrl = UrlHelper.UrlRemoveParameter(returnUrl, "isNOCView");
			ReferrerRedirectorBase.SetReferrerUrl(returnUrl);
		}

		ReferrerRedirectorBase.Return("ListViews.aspx");
	}

    protected void DoneAndGoToNocClick(object sender, EventArgs e)
    {
        CheckValidReturnView();
        myResourceEditor.UpdateColumnsWidth();

        var newParams = new Dictionary<string, string>()
                {
                    {"ViewID", this.ParentView != null ? this.ParentView.ViewID.ToString() : this.View.ViewID.ToString()},
                    {"NetObject",  (!string.IsNullOrEmpty(Request["NetObject"]) ? Request["NetObject"] : "")},
                    {"isNOCView", "true"}
                };


        var returnToUrl = ReferrerRedirectorBase.GetDecodedReferrerUrl();
		var returnToParams = UrlHelper.ParseQueryString(returnToUrl).Merge(newParams);
        returnToParams = returnToParams.Where(kvp => !kvp.Key.Equals("ReturnTo", StringComparison.OrdinalIgnoreCase));
        Response.Redirect(UrlHelper.ReplaceQueryString("/Orion/Admin/Preview.aspx", returnToParams));
        Preview.Visible = true;
    }

    [WebMethod]
    public static void SetViewNOCMode(bool isEnable, int viewID, int viewGroup)
    {
        var view = (viewID > 0) ? ViewManager.GetViewById(viewID) : ViewManager.GetFirstViewInGroup(viewGroup);
        if (view == null) return;
        view.NOCView = isEnable;
        view.NOCViewRotationInterval = (isEnable) ? 15 : 0;
        ViewManager.UpdateViewGroupNocData(view);
    }

    [WebMethod]
    public static void SetViewNOCRotateTime(int rotateTime, int viewID, int viewGroup)
    {
        var view = (viewID > 0) ? ViewManager.GetViewById(viewID) : ViewManager.GetFirstViewInGroup(viewGroup);
        if (view == null) return;
        view.NOCViewRotationInterval = rotateTime;
        ViewManager.UpdateViewGroupNocData(view);
    }
}
