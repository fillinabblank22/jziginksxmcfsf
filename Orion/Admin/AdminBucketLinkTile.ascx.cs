﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Admin_AdminBucketLinkTile : UserControl
{
    public Orion_Admin_AdminBucketLinkTile()
    {
        PrimaryLinks = new List<LinkModel>();
        Links = new List<LinkModel>();
    }

    public string Header { get; set; }
    public string Icon { get; set; }

    public IList<LinkModel> PrimaryLinks { get; set; }
    public IList<LinkModel> Links { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        primaryButtons.DataSource = PrimaryLinks != null ? PrimaryLinks.Where(x => x == null || x.IsVisible) : null;
        primaryButtons.DataBind();

        links.DataSource = Links != null ? Links.Where(x => x == null || x.IsVisible) : null;
        links.DataBind();
    }

    protected void links_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        LinkModel item = e.Item.DataItem as LinkModel;
        if (item == null)
            return;

        Control normalLinkControl = e.Item.FindControl("normalLink");
        Control summaryLinkControl = e.Item.FindControl("summarylink");


        if (item.IsSummaryLink)
        {
            normalLinkControl.Visible = false;
            summaryLinkControl.Visible = true;
        }
        else
        {
            normalLinkControl.Visible = true;
            summaryLinkControl.Visible = false;
        }
    }
}

public class LinkModel
{
    public LinkModel()
    {
        IsVisible = true;
    }

    public string Href { get; set; }
    public string Label { get; set; }
    public bool IsSummaryLink { get; set; }
    public bool IsVisible { get; set; }
}