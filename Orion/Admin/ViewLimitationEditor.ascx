<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewLimitationEditor.ascx.cs" Inherits="Orion_Admin_ViewLimitationEditor" %>
<%@ Register TagPrefix="orion" TagName="SwisfErrorControl" Src="~/Orion/SwisfErrorControl.ascx" %>

<table>
    <tr>
        <td colspan="2">
            <orion:SwisfErrorControl runat="server" ID="SwisfErrorControl" Visible="False" />
        </td>
    </tr>
    <tr>
        <td>
            <div style=" margin-bottom: 10px; margin-top: 10px;">
            <span class="highlight"><asp:PlaceHolder runat="server" ID="phNoLimitation">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_310) %>
            </asp:PlaceHolder></span>
            <asp:PlaceHolder runat="server" ID="phType" />
            <br />
            <span><asp:PlaceHolder runat="server" ID="phDetail" /></span>
            </div>
        </td>
        <td valign="middle">
            <div class="sw-btn-bar" >
            <orion:LocalizableButton runat="server" ID="btnEditLimitation" LocalizedText="CustomText" Text = "<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_28 %>" DisplayType="Small"  OnClick="EditLimitation_Click" />
            <orion:localizableButton runat="server" ID="btnDeleteLimitation" Visible="<%$ HtmlEncodedCode: this.HasLimitation %>" LocalizedText="Delete" DisplayType="Small" OnClick="DeleteLimitation_Click" />
            </div>
        </td>
    </tr>
</table>