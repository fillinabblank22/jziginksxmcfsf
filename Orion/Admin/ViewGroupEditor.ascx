  <%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewGroupEditor.ascx.cs" Inherits="Orion_Admin_ViewGroupEditor" %>
<%@ Register TagPrefix="orion" TagName="viewGroupSubNav" Src="~/Orion/ViewGroupSubNav.ascx" %>

    <orion:Include ID="Include1" runat="server" File="SubViews.css" />
 
    <table>
        <tr>
		    <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_42) %></td>
		    <td><asp:TextBox runat="server" ID="txtViewTitle" MaxLength="200" Columns="40"/></td>
		    <td>
		        <orion:LocalizableButton runat="server" ID="btnUpdateName" LocalizedText="CustomText" Text ="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_181 %>" DisplayType="Small" OnClick="UpdateViewTitle_Click"/>
		    </td>
            <td>
               <asp:CustomValidator runat="server" ControlToValidate="txtViewTitle"
                                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_160 %>"
                                    OnServerValidate="TxtViewTitleValidate"
                                    Display="Dynamic"                                    
                                    />
               <asp:CustomValidator runat="server" ControlToValidate="txtViewTitle"                                    
                                    OnServerValidate="TxtViewTitleUniqueValidate"
                                    Display="Dynamic"                                    
                                    />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtViewTitle" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_161 %>" runat="server" Display="Dynamic" />
                 <span class="SubNav-Error"><asp:PlaceHolder runat="server" ID="phStatusMessage" /></span>
            </td>
	    </tr>
	    <tr>
		    <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_43) %></td>
		    <td>
            <img id="imgViewGroupIcon" runat="server" alt=""/>
            <orion:LocalizableButton runat="server" ID="btnUpdateIcon" OnClientClick="return BrowseIconClickHandler();" LocalizedText="CustomText" Text ="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_41 %>" DisplayType="Small" OnClick="UpdateIcon_Click"/>
            </td>
		    <td>
		       </td>
	    </tr>
    </table>

     <asp:HiddenField runat="server" ID="ViewGroupSelectedIcon" />
     <div id="iconDialog" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_61) %>" style="display:none;">
	    <p><b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_VB1_44, View.ViewShortTitle)) %></b></p>
        <div style="background-color:White; padding:5px;">
            <asp:Repeater ID="rptIcons" runat="server">
                <ItemTemplate>
                    <div class="browseIcons-Icon" data-filepath='<%# DefaultSanitizer.SanitizeHtml(((IconInfo)Container.DataItem).FileName) %>'>
                        <a><img src='<%# DefaultSanitizer.SanitizeHtml(((IconInfo)Container.DataItem).WebImagePath) %>' alt='<%# DefaultSanitizer.SanitizeHtml(((IconInfo)Container.DataItem).WebImagePath) %>' /></a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div style="clear: both;"></div>
        </div>
         <div class="sw-btn-bar">
            <orion:LocalizableButtonLink ID="btnDialogOk" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_45 %>"  ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_46%>"/>
            <orion:LocalizableButtonLink ID="btnDialogCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel"  ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>"/>
        </div>
    </div>

<div id="conditionDialog" style="display: none;">
    <table>
        <tr>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VT0_1) %></td>
            <td>
                <input type="radio" id="displayAlways" name="conditionOn" />
                <label for="displayAlways"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VT0_2) %></label></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="radio" id="displayWhenMet" name="conditionOn" />
                <label for="displayWhenMet"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VT0_3) %></label>
                <ul id="conditionList">
                </ul>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">
        $(function() {
           $('.browseIcons-Icon').click(function(){
                $('.browseIcons-SelectedIcon').removeClass('browseIcons-SelectedIcon');
                $(this).addClass('browseIcons-SelectedIcon');
           });
            
            <%
                if (View.IsSubView)
                { // do not try to show the dialog on not subview
            %>

            SW.Core.Services.callWebService(
                "/Orion/Services/ViewConditions.asmx",
                "GetPossibleConditionsForView",
                {
                    viewId: <%= this.View.ViewID %>
                },
                function(viewConditions) {
                    if (viewConditions.length > 0) {
                        var loadConditions = function() {
                            var condList = $('#conditionList');

                            $.each(viewConditions, function(index, condition) {
                                var textSpan = $("<label />")
                                  .attr('for', 'viewConditionCheckbox' + index)
                                  .text(condition.Name);

                                $("<li />")
                                    .append(
                                        $("<input type='checkbox' />")
                                            .attr('checked', condition.Selected)
                                            .attr('id', 'viewConditionCheckbox' + index)
                                            .on('change', function(ev) {
                                                SW.Core.Services.callWebService(
                                                    "/Orion/Services/ViewConditions.asmx",
                                                    "SetViewCondition",
                                                    {
                                                        viewId: <%= this.View.ViewID %>,
                                                        condition: condition.Type,
                                                        enable: ev.target.checked
                                                    }, 
                                                    function () {});

                                            })
                                    )
                                    .append(textSpan)
                                    .appendTo(condList);

                            });
                        };

                        $('#conditionDialog').show();
                        if (<%= ViewManager.GetViewConditions(this.View).Length %> > 0) {
                            $('#displayWhenMet').attr('checked', true);
                            loadConditions();
                        } else {
                            $('#displayAlways').attr('checked', true);
                        }                        

                        $('#displayWhenMet').on('change', loadConditions);
                        
                        $('#displayAlways').on('change', function() {
                            $('#conditionList').html('');
                            
                            $.each(viewConditions, function(index, condition) {
                                condition.Selected = false;
                            });
                            
                            SW.Core.Services.callWebService(
                                "/Orion/Services/ViewConditions.asmx",
                                "ShowViewAlways",
                                {
                                    viewId: <%= this.View.ViewID %>
                                },
                                function () {});
                        });
                    }   
                }
            );
            <% } %>
        });

        function BrowseIconClickHandler() {
              //Bind btnOK
              $('#<%=btnDialogOk.ClientID %>').unbind('click').click( function () {
                                        var selectedIcon = $('.browseIcons-SelectedIcon');
                  
                                        if( !selectedIcon.data('filepath'))
                                            alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_55) %>");
                                        else
                                        {
                                            $('#<%=ViewGroupSelectedIcon.ClientID %>').val( selectedIcon.data('filepath') );
					                        <%= Page.GetPostBackEventReference(btnUpdateIcon) %>
                                        }
            });
            //Bind btnCancel
            $('#<%=btnDialogCancel.ClientID %>').unbind('click').click( function () {
                 $("#iconDialog").dialog( "close" );
            });
            //Open dialog
            $("#iconDialog").dialog({
                title:"<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_56) %>",
                modal: true
            });
            return false;
        }
</script>
