<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceColumnWidthEditor.ascx.cs" Inherits="Orion_Admin_ResourceColumnWidthEditor" %>
<%@ Import Namespace="Resources" %>

    <table runat="server" id="tblColumnView" enableviewstate="false">
        <%-- This empty row makes the edit mode line up better with the view mode --%>
        <tr><td colspan="3">&nbsp;</td></tr>
	    <tr>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_270) %></td>
	        <td><strong><asp:PlaceHolder runat="server" ID="phCol1" EnableViewState="false" /></strong></td>
	        <td rowspan="6" style="vertical-align: middle; padding-left: 24px;">
	            <orion:LocalizableButton runat="server" ID="btnEditColumnWidths" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_28 %>" DisplayType="Small" OnClick="EditColumnsClick" />
	        </td>
	    </tr>
	    <tr runat="server" id="trWidth2">
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_271) %></td>
	        <td><strong><asp:PlaceHolder runat="server" ID="phCol2" EnableViewState="false" /></strong></td>
	    </tr>
	    <tr runat="server" id="trWidth3">
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_272) %></td>
	        <td><strong><asp:PlaceHolder runat="server" ID="phCol3" EnableViewState="false" /></strong></td>
	    </tr>
        <tr runat="server" id="trWidth4">
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_114) %></td>
	        <td><strong><asp:PlaceHolder runat="server" ID="phCol4" EnableViewState="false" /></strong></td>
	    </tr>
        <tr runat="server" id="trWidth5">
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_115) %></td>
	        <td><strong><asp:PlaceHolder runat="server" ID="phCol5" EnableViewState="false" /></strong></td>
	    </tr>
        <tr runat="server" id="trWidth6">
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_116) %></td>
	        <td><strong><asp:PlaceHolder runat="server" ID="phCol6" EnableViewState="false" /></strong></td>
	    </tr>
    </table>

    <table runat="server" id="tblColumnEdit" visible="false" enableviewstate="false">
        <tr>
            <td colspan="2">&nbsp;</td>
            <th colspan="2"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_278) %></th>
        </tr>
        <tr>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_270) %></td>
	        <td>
	            <asp:TextBox runat="server" ID="txtColumn1Width" Columns="6" EnableViewState="false" />
	        </td>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_273) %></td>
	        <td><asp:RadioButton runat="server" ID="radioColumn1" GroupName="columnCount" /></td>
	        <td rowspan="6" style="vertical-align: middle; padding-left: 24px;">
	            <orion:LocalizableButton runat="server" ID="btnSaveColumnWidths" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitColumnsClick" />
	        </td>
	    </tr>
	    <tr>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_271) %></td>
	        <td>
	            <asp:TextBox runat="server" ID="txtColumn2Width" Columns="6" EnableViewState="false" />
	        </td>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_274) %></td>
	        <td><asp:RadioButton runat="server" ID="radioColumn2" GroupName="columnCount" /></td>
	    </tr>
	    <tr>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_272) %></td>
	        <td>
	            <asp:TextBox runat="server" ID="txtColumn3Width" Columns="6" EnableViewState="false" />
	        </td>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VB0_275) %></td>
	        <td><asp:RadioButton runat="server" ID="radioColumn3" GroupName="columnCount" /></td>
	    </tr>
        <tr>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_114) %></td>
	        <td>
	            <asp:TextBox runat="server" ID="txtColumn4Width" Columns="6" EnableViewState="false" />
	        </td>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_117) %></td>
	        <td><asp:RadioButton runat="server" ID="radioColumn4" GroupName="columnCount" /></td>
	    </tr>
        <tr>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_115) %></td>
	        <td>
	            <asp:TextBox runat="server" ID="txtColumn5Width" Columns="6" EnableViewState="false" />
	        </td>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_118) %></td>
	        <td><asp:RadioButton runat="server" ID="radioColumn5" GroupName="columnCount" /></td>
	    </tr>
         <tr>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_116) %></td>
	        <td>
	            <asp:TextBox runat="server" ID="txtColumn6Width" Columns="6" EnableViewState="false" />
	        </td>
	        <td><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_119) %></td>
	        <td><asp:RadioButton runat="server" ID="radioColumn6" GroupName="columnCount" /></td>
	    </tr>
    </table>