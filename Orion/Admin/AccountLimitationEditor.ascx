<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountLimitationEditor.ascx.cs" Inherits="Orion_Admin_AccountLimitationEditor" %>
<%@ Register TagPrefix="orion" TagName="SwisfErrorControl" Src="~/Orion/SwisfErrorControl.ascx" %>

<orion:SwisfErrorControl runat="server" ID="SwisfErrorControl" Visible="False" />
<table class="accountDetails">
    <tr runat="server" id="trNoLimsMessage">
        <td colspan="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_484) %>
        </td>
    </tr>
    <tr runat="server" id="trLimitation1">
        <td><asp:PlaceHolder ID="phLim1Type" runat="server" /></td>
        <td><asp:PlaceHolder ID="phLim1Detail" runat="server" /></td>
        <td>
            <div class="sw-btn-bar">
            <orion:localizableButton ID="btnLim1Edit" runat="server" 
                LocalizedText="Edit" DisplayType="Small" OnClick="EditClick1" 
                CausesValidation="False" />
            <orion:LocalizableButton ID="btnLim1Delete" runat="server" 
                LocalizedText="Delete" DisplayType="Small" OnClick="DeleteClick1" 
                CausesValidation="False" />
            </div>
        </td>
    </tr>
    <tr runat="server" id="trLimitation2">
        <td><asp:PlaceHolder ID="phLim2Type" runat="server" /></td>
        <td><asp:PlaceHolder ID="phLim2Detail" runat="server" /></td>
        <td>
        <div class="sw-btn-bar">
            <orion:LocalizableButton ID="btnLim2Edit" runat="server" 
                LocalizedText="Edit" DisplayType="Small" OnClick="EditClick2" 
                CausesValidation="False" />
            <orion:LocalizableButton ID="btnLim2Delete" runat="server" 
                LocalizedText="Delete" DisplayType="Small" OnClick="DeleteClick2" 
                CausesValidation="False" />
        </div>
        </td>
    </tr>
    <tr runat="server" id="trLimitation3">
        <td><asp:PlaceHolder ID="phLim3Type" runat="server" /></td>
        <td><asp:PlaceHolder ID="phLim3Detail" runat="server" /></td>
        <td>
            <div class = "sw-btn-bar">
            <orion:LocalizableButton ID="btnLim3Edit" runat="server" 
                LocalizedText="Edit" DisplayType="Small"  OnClick="EditClick3" 
                CausesValidation="False" />
            <orion:LocalizableButton ID="btnLim3Delete" runat="server" LocalizedText="Delete" DisplayType="Small" OnClick="DeleteClick3" />
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
        <div class="sw-btn-bar">
            <orion:LocalizableButton ID="btnNewLimitation" runat="server" 
                LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_485 %>" DisplayType="Small" OnClick="AddClick" 
                CausesValidation="False" />
        </div>
        </td>
    </tr>
</table>