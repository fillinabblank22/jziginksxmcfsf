<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" AutoEventWireup="true" 
    CodeFile="SelectLimitationType.aspx.cs" Inherits="Orion_Admin_SelectLimitationType" 
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_190 %>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    
    <h1 runat="server" id="h1Account">
        <%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(string.Format(Resources.CoreWebContent.WEBCODE_TM0_81, Request.QueryString["AccountID"]))) %>
    </h1>
    
    <h1 runat="server" id="h1View" visible="false">
        <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_TM0_82, this.ThisView.ViewTitle)) %>
    </h1>
    
    <p runat="server" id="paraAccountMessage">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_188) %>
    </p>
    
    <p runat="server" id="paraViewMessage" visible="false">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_189) %>
    </p>
    
    <table style="font-size: 8pt;">
        <asp:Repeater runat="server" ID="rptTypes">
            <ItemTemplate>
                <tr>
                    <td>
                        <input type="radio" name="LimitationType" value='<%# DefaultSanitizer.SanitizeHtml(Eval("LimitationTypeID")) %>' />
                    </td>
                    
                    <th style="text-align: left; padding-right: 1em; white-space: nowrap; vertical-align: top;">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %>
                    </th>
                    
                    <td><%# DefaultSanitizer.SanitizeHtml(Eval("Description")) %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnContinue" OnClick="ContinueClick" LocalizedText="CustomText"
            Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_186 %>" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="CancelClick" 
            LocalizedText="Cancel" DisplayType="Secondary" />
    </div>
</asp:Content>

