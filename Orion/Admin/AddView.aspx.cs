using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using System.Text.RegularExpressions;

public partial class Orion_Admin_AddView : Page
{
    protected override void OnInit(EventArgs e)
    {
        lbxViewTypes.DataSource = ViewManager.GetCreatableViewTypes();
        lbxViewTypes.DataBind();

        foreach (ListItem item in lbxViewTypes.Items)
        {
            item.Text = ViewManager.GetFriendlyViewTypeName(item.Value);
        }

        rptViewTypes.DataSource = ViewManager.GetCreatableViewTypes();
        rptViewTypes.DataBind();

        base.OnInit(e);
    }

	protected override void OnLoad(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		base.OnLoad(e);
	}

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (IsValid)
        {
            ViewInfo info;
            try
            {
                bool isNOC;
                bool.TryParse(Request["addNOCView"], out isNOC);
                info = ViewManager.CreateNewView(txtViewTitle.Text, lbxViewTypes.SelectedValue, 2, 500, 400, 0, 0, 0, 0, isNOC, 0);
                
				if (lbxViewTypes.SelectedValue.Equals("summary", StringComparison.OrdinalIgnoreCase))
				{
					MenuBar.CreateCustomMenuItem(txtViewTitle.Text, txtViewTitle.Text, string.Format("/Orion/View.aspx?ViewID={0}", info.ViewID), false);
				}

				Response.Redirect(string.Format("/Orion/Admin/CustomizeView.aspx?ViewID={0}&ReturnTo={1}", info.ViewID, ReferrerRedirectorBase.GetReferrerUrl()));
            }
            catch (ArgumentException)
            {
                // specifically catching the argument exception, it means that the view title is not unique
                phStatusMessage.Controls.Clear();
                phStatusMessage.Controls.Add(new LiteralControl(string.Format(Resources.CoreWebContent.WEBCODE_TM0_51, txtViewTitle.Text)));
            }
        }
    }

    private const string AlowedCharsPattern = @"^[-\w.() ]*$";
    protected void TxtViewTitleValidate(object source, ServerValidateEventArgs args)
    {
        string title = SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular(txtViewTitle.Text);
        args.IsValid = Regex.IsMatch(title, AlowedCharsPattern);

        if (!args.IsValid)
        {
            txtViewTitle.Text = title;
        }
    }
}
