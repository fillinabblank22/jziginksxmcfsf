﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_MaintenanceRenewals : System.Web.UI.Page
{
	private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected DateTime LastUpdateCheck;
	protected DateTime NextUpdateCheck;
	protected string message;

    protected bool UpdateNeverChecked
    {
        get { return LastUpdateCheck == DateTime.MinValue; }
    }

    protected bool NextUpdateUnknown
    {
        get { return NextUpdateCheck == DateTime.MinValue; }
    }

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	protected override void  OnLoadComplete(EventArgs e)
	{
 		 base.OnLoadComplete(e);
		 LoadControls();
	}

	protected void Page_Load(object sender, EventArgs e)
	{
        downloadSelected.DataBind();
		if (!IsPostBack)
		{
			cbShowIgnored.Checked = (SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("MaintenanceRenewals-ShowIgnored").SettingValue == 1) ? true : false;
		}

        litLinkToMatrix.Text = string.Format(Resources.CoreWebContent.WEBDATA_ZT0_56, string.Format("<a class='link-to-matrix' target='_blank' href='{0}'>", KnowledgebaseHelper.GetKBUrl(1888)), "</a>");

	}

	private void LoadControls()
	{
		cbAutoCheck.Checked = (SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("MaintenanceRenewals-Check").SettingValue == 1) ? true : false;
		if (Profile.AllowAdmin)
		{
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
			{
			    var renewals = proxy.GetMaintenanceRenewalNotificationItems(cbShowIgnored.Checked);
                message = (renewals.Count > 0) ? Resources.CoreWebContent.WEBCODE_VB0_276 : Resources.CoreWebContent.WEBCODE_VB0_277;
                UpdatesTable.DataSource = renewals;
                UpdatesTable.DataBind();

			    var status = proxy.GetMaintenanceRenewalsCheckStatus();
				if (status != null)
				{
				    LastUpdateCheck = status.LastUpdateCheck.HasValue ? status.LastUpdateCheck.Value : DateTime.MinValue;
				    NextUpdateCheck = status.NextUpdateCheck.HasValue ? status.NextUpdateCheck.Value : DateTime.MinValue;
				}
                else
				{
				    LastUpdateCheck = DateTime.MinValue;
                    NextUpdateCheck = DateTime.MinValue;
				}
			}
		}
	}

	protected void AcknowledgeSelected(object sender, EventArgs e)
	{
		var updateKeys = new List<Guid>();
		foreach (RepeaterItem item in UpdatesTable.Items)
		{
			CheckBox cb = (CheckBox)item.FindControl("cbUpdate");
			if (cb != null)
			{
				if (cb.Checked && cb.Enabled)
				{
					HiddenField hf = (HiddenField)item.FindControl("hfUpdateKey");
					if (hf != null)
					{
						updateKeys.Add(new Guid(hf.Value));
					}
				}
			}
		}
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			proxy.IgnoreNotificationItems(updateKeys);
		}

		LoadControls();
	}

	protected void Acknowledge(object sender, EventArgs e)
	{
		LocalizableButton imgButton = (LocalizableButton)sender;
		if (imgButton != null)
		{
			Control parent = imgButton.Parent;
			HiddenField hf = (HiddenField)parent.FindControl("hfUpdateKey");
			if (hf != null)
			{
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				{
					proxy.IgnoreNotificationItem(new Guid(hf.Value));
				}

				LoadControls();
			}
		}
	}

	protected void ForceUpdateCheck(object sender, EventArgs e)
	{
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
		    proxy.ForceMaintenanceRenewalsCheck();
		}

		LoadControls();
	}

	protected void SaveSettings(object sender, EventArgs e)
	{
		SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("MaintenanceRenewals-Check", (cbAutoCheck.Checked) ? 1 : 0);
		SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("MaintenanceRenewals-ShowIgnored", (cbShowIgnored.Checked) ? 1 : 0);
		LoadControls();
	}

	protected void RevertSettings(object sender, EventArgs e)
	{
		cbAutoCheck.Checked = (SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("MaintenanceRenewals-Check").SettingValue == 1) ? true : false;
		cbShowIgnored.Checked = (SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("MaintenanceRenewals-ShowIgnored").SettingValue == 1) ? true : false;
		LoadControls();
	}

	protected void Refresh(object sender, EventArgs e)
	{
		LoadControls();
	}
}
