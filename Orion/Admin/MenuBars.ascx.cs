﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Admin_MenuBars : ProfilePropEditUserControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		List<MenuBar> bars = new List<MenuBar>();
		bars.AddRange(MenuBar.GetAllMenuBars());
        menuBars.Items.Add(new ListItem( Resources.CoreWebContent.WEB_JS_CODE_TM0_3,"none"));
	    foreach (MenuBar menuBar in bars)
	    {
            menuBars.Items.Add(new ListItem(menuBar.Name));	        
	    }
	}

	public string Id
	{
		get
		{
			return this.ID;
		}
	}

	public override string PropertyValue
	{
		get
		{
			return menuBars.SelectedValue;
		}
		set
		{
			if (value != null)
			{
				var item = this.menuBars.Items.FindByValue(value.Trim());
				if (item != null)
				{
					item.Selected = true;
				}
				else
				{
					menuBars.Items.FindByValue("none").Selected = true;
				}
			}
			else
			{
				menuBars.Items.FindByValue("none").Selected = true;
			}
		}
	}
}
