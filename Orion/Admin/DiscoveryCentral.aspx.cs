﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Discovery.Contract;
using SolarWinds.Orion.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using System.Data;
using XmlHelper = SolarWinds.Orion.Web.Helpers.XmlHelper;

public partial class Orion_Admin_DiscoveryCentral : System.Web.UI.Page
{
    private static Log log = new Log();

    private bool? _isNodeFunctionalityAllowed;
    /// <summary>
    /// (get) Return True if licence allow to add/manage nodes. False in the case, that licence doesn't allow it (for example user has just installed SRM module).
    /// </summary>
    private bool IsNodeFunctionalityAllowed
    {
        get
        {
            if (!_isNodeFunctionalityAllowed.HasValue)
            {
                var featureManager = new SolarWinds.Orion.Core.Common.FeatureManager();
                int maxNodes = featureManager.GetMaxElementCount(SolarWinds.Orion.Core.Common.WellKnownElementTypes.Nodes);
                _isNodeFunctionalityAllowed = maxNodes != 0;
            }

            return _isNodeFunctionalityAllowed.Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int nodeCount=0;        
        bool areNodeRequired = false;

        var otherPluginControls = new Dictionary<int, Control>();
        var corePluginControls = new Dictionary<int, Control>();
        Dictionary<int, Control> pluginControls = null;  

        // load plugin controls from config
        XmlDocument xmlDoc = new XmlDocument();

        // get number of Nodes
        try
        {
            using (WebDAL webDAL = new WebDAL())
            {
                nodeCount = webDAL.GetNodeCount();                
            }
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Cannot get number of managed nodes. Details: {0}", ex.ToString());
        }        

        // independent plugins
        var pluginLoader = new DiscoveryPluginLoader();
        var cfgFiles = pluginLoader.GetIndependentDiscoveryPluginFiles(OrionConfiguration.InstallPath);

        foreach (var cfgFile in cfgFiles)
        {
            try
            {
                var xdoc = XDocument.Load(cfgFile);
                xmlDoc = SolarWinds.Orion.Web.Helpers.XmlHelper.ToXmlDocument(xdoc);
                var xmlNodes = xmlDoc.DocumentElement.SelectNodes("discoveryCentralPlugins/discoveryCentralPlugin");

                var currentControls = new Dictionary<int, Control>();

                foreach (XmlNode xmlNode in xmlNodes)
                {
					int order = int.Parse(xmlNode.Attributes["order"].Value);
					Control control = this.LoadControl(xmlNode.Attributes["control"].Value);

					if (currentControls.ContainsKey(order))
					{
						log.WarnFormat("Independent plugin config [{3}]:Resource [{1}] with order [{0}] already added to current controls dictionary - existing: [{2}] ", order, xmlNode.Attributes["control"].Value, currentControls[order], cfgFile);
						continue;
					}

                    if (xmlNode.Attributes["requireNodesLicence"] != null)
                    {
                        bool requireNodesLicence = false;
                        if (bool.TryParse(xmlNode.Attributes["requireNodesLicence"].Value, out requireNodesLicence))
                        {
                            if (requireNodesLicence && !IsNodeFunctionalityAllowed)
                                continue;
                        }
                    }

                    currentControls.Add(order, control);

                    if (xmlNode.Attributes["requireNodes"] != null)
                        areNodeRequired = Boolean.Parse(xmlNode.Attributes["requireNodes"].Value) || areNodeRequired;

                    bool redirectToDiscovery = false;

                    if (areNodeRequired && nodeCount <= 1)
                    {
                        if (nodeCount == 1) //only do this when really needed = to detect if our single node is a DPI probe
                        {
                            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                            {
                                try
                                {
                                    DataTable result = service.Query("SELECT n.IPAddress from Orion.Nodes n JOIN Orion.Engines e ON e.IP = n.IPAddress");
                                    if (result != null)
                                    {
                                        redirectToDiscovery = result.Rows.Count > 0; //The only node in system has the same IP as engine. 
                                        //That means it's probably a DPI probe and we still want to behave as "no ndoes"
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.Warn("Error querying SWIS", ex);
                                    redirectToDiscovery = false; //something wrong happened.. assume there are some nodes
                                }
                            }
                        }
                        else
                        {
                            //no nodes
                            redirectToDiscovery = true;
                        }
                        
                    }

                    if (redirectToDiscovery)
                    {
                        DiscoveryCentralHelper.Leave();
                        Response.Redirect("~/Orion/Discovery/Default.aspx");
                    }
                }

                // independent plugins are always non-Core
                foreach (var item in currentControls)
                {
                    otherPluginControls.Add(item.Key, item.Value);
                }
            }
            catch (System.Threading.ThreadAbortException threadEx)
            {
                log.DebugFormat("Thread aborted while loading from file {0}, possibly a side-effect of Response.Redirect. Details: {1}", cfgFile, threadEx.ToString());
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error while loading discovery central plugin from file {0}. Details: {1}", cfgFile, ex.ToString());
            }
        }

        // plugins from modules
        foreach (var module in OrionModuleManager.GetModules())
        {
            try
            {
                xmlDoc = XmlHelper.ToXmlDocument(module.GetModuleConfiguration());

                var xmlNodes = xmlDoc.DocumentElement.SelectNodes("discoveryCentralPlugins/discoveryCentralPlugin");
                
                if (xmlNodes.Count != 0)
                {
                    var currentControls = new Dictionary<int, Control>();  

                    foreach (XmlNode xmlNode in xmlNodes)
                    {
						int order = int.Parse(xmlNode.Attributes["order"].Value);
						Control control = this.LoadControl(xmlNode.Attributes["control"].Value);

						if (currentControls.ContainsKey(order))
						{
							log.WarnFormat("Module config [{3}]:Resource [{1}] with order [{0}] already added to current controls dictionary - existing: [{2}] ", order, xmlNode.Attributes["control"].Value, currentControls[order], module.ConfigFilePath);
							continue;
						}

                        currentControls.Add(order, control);

                        if (xmlNode.Attributes["requireNodes"] != null)
                            areNodeRequired = Boolean.Parse(xmlNode.Attributes["requireNodes"].Value) || areNodeRequired;

                        if (areNodeRequired && nodeCount == 0)
                        {
                            DiscoveryCentralHelper.Leave();
                            Response.Redirect("~/Orion/Discovery/Default.aspx");
                        }
                    }

                    // hardcoded distinguishing of Core plugins
                    if (String.Compare(module.Name, "NetPerfMon", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        pluginControls = corePluginControls;
                    }
                    else
                    {
                        pluginControls = otherPluginControls;
                    }

                    foreach (var item in currentControls)
                    {

						if (pluginControls.ContainsKey(item.Key))
						{
							log.WarnFormat("Module config [{1}]:Resource with order [{0}] already added to current controls dictionary.", item.Key, module.ConfigFilePath);
							continue;
						}

                        pluginControls.Add(item.Key, item.Value);
                    }
                }
                else
                {
                    //setup fallback plugins for modules that are installed, but don't have plugins defined
                    if (String.Compare(module.Name, "voip", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
						AddControlToDict("/Orion/Discovery/Controls/DiscoveryCentralDefault/IPSLADiscoveryPlugin.ascx", 60, otherPluginControls, module.Name, ref areNodeRequired);
                    }
                    else if (String.Compare(module.Name, "TrafficAnalysis", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
						AddControlToDict("/Orion/Discovery/Controls/DiscoveryCentralDefault/NTADiscoveryPlugin.ascx", 80, otherPluginControls, module.Name, ref areNodeRequired);
                    }
                    else if (String.Compare(module.Name, "APM", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
						AddControlToDict("/Orion/Discovery/Controls/DiscoveryCentralDefault/ApplicationDiscoveryPlugin.ascx", 30, otherPluginControls, module.Name, ref areNodeRequired);
                    }
                    else if (String.Compare(module.Name, "VIM", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
						AddControlToDict("/Orion/Discovery/Controls/DiscoveryCentralDefault/VIMDiscoveryPlugin.ascx", 20, otherPluginControls, module.Name, ref areNodeRequired);
                    }
                    else if (String.Compare(module.Name, "NPM", StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
						AddControlToDict("/Orion/Discovery/Controls/DiscoveryCentralDefault/NPMDiscoveryPlugin.ascx", 10, otherPluginControls, module.Name, ref areNodeRequired);
                    }

                    if (areNodeRequired && nodeCount == 0)
                    {
                        DiscoveryCentralHelper.Leave();
                        Response.Redirect("~/Orion/Discovery/Default.aspx");
                    }
                }
            }
            catch (System.Threading.ThreadAbortException threadEx)
            {
                log.DebugFormat("Thread aborted while loading module {0}, possibly a side-effect of Response.Redirect. Details: {1}", module.Name, threadEx.ToString());
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error while loading discovery central plugin for module {0}. Details: {1}", module.Name, ex.ToString());
            }
        }
        
        if (areNodeRequired)
        {
            foreach (var item in corePluginControls)
            {
                otherPluginControls[item.Key] = item.Value;
            }           
        }

        // arrange plugins by "order"
        var orderedPluginControls = otherPluginControls.OrderBy(p => p.Key);

        // Add loaded plugin controls on the page
        pluginContainer.Controls.Clear();
		foreach (var control in orderedPluginControls)
		{
			Panel wrapper = new Panel();
            wrapper.Controls.Add(new LiteralControl("<div style=\"clear: both;\"></div>"));
            wrapper.Controls.Add(control.Value);               
            pluginContainer.Controls.Add(wrapper);

			wrapper.CssClass = control.Value.Visible ? "pluginWrapper" : String.Empty;
		}
    }

	private void AddControlToDict(string path, int order, Dictionary<int, Control> otherPluginControls, string moduleName, ref bool areNodeRequired)
	{
		if (otherPluginControls.ContainsKey(order))
		{
			log.WarnFormat("Module [{1}]:Default resource with order [{0}] already added to current controls dictionary.", order, moduleName);
			return;
		}

		otherPluginControls.Add(order, LoadControl(path));
		areNodeRequired = true;
	}
}
