using System;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_ListViews : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        ViewInfoCollection views = ViewManager.GetAllViews();
        ViewInfo[] sortedViews = new ViewInfo[views.Count];

        int i = 0;
        foreach (ViewInfo info in views)
        {
            sortedViews[i] = info;
            i++;
        }

        Array.Sort(sortedViews, new ViewInfoTitleComparer());

        this.lbxViews.DataSource = sortedViews;
        this.lbxViews.DataBind();

        

        base.OnInit(e);
    }

    protected void EditClick(object sender, EventArgs e)
    {
        if (null != lbxViews.SelectedItem)
        {
            Response.Redirect(string.Format("/Orion/Admin/CustomizeView.aspx?ViewID={0}&ReturnTo={1}", lbxViews.SelectedValue, ReferrerRedirectorBase.GetReturnUrl()));
        }
    }

    protected void AddClick(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("/Orion/Admin/AddView.aspx?ReturnTo={0}", ReferrerRedirectorBase.GetReturnUrl()));
    }

    protected void CopyClick(object sender, EventArgs e)
    {
        if (null != lbxViews.SelectedItem)
        {
            var info = ViewManager.GetViewById(Convert.ToInt32(lbxViews.SelectedValue));
            var copiedInfo = info.ViewGroup != 0 ? ViewManager.CopyViewWithSubViews(info).FirstOrDefault() : ViewManager.CloneView(info);

            if (copiedInfo != null && copiedInfo.ViewType.Equals("Summary", StringComparison.OrdinalIgnoreCase))
            {
                var copiedTitle = (info.ViewGroup != 0) ? copiedInfo.ViewGroupTitle : copiedInfo.ViewTitle;
                MenuBar.CreateCustomMenuItem(copiedTitle, copiedTitle, string.Format("/Orion/View.aspx?ViewID={0}", copiedInfo.ViewID), false);
            }
            Response.Redirect(Request.Url.ToString());
        }
    }

    [WebMethod]
    public static bool IsViewInUse(int viewID, string viewName)
    {
        return MenuBar.CheckIfViewMenuIteminUse(viewID, viewName);
    }

    protected void DeleteClick(object sender, EventArgs e)
    {
        if (null != lbxViews.SelectedItem)
        {
            ViewInfo info = ViewManager.GetViewById(Convert.ToInt32(lbxViews.SelectedValue));

            if (info.IsSystem)
            {
                this.phErrorMessage.Controls.Clear();
                this.phErrorMessage.Controls.Add(
                    new LiteralControl(string.Format(Resources.CoreWebContent.WEBCODE_VB0_224, info.ViewTitle))
                    );
            }
            else
            {
                ViewManager.DeleteView(info);
                lbxViews.Items.Remove(lbxViews.SelectedItem);
            } 
        }
    }
}
