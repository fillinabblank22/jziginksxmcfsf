<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SMTPServer.ascx.cs" Inherits="Orion_Admin_SMTPServer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script type="text/javascript">
    function pageLoad() 
    {
        var sValue = $('#<%=ddlAuthList.ClientID%>').val();
        ShowHideAuthContent(sValue);
    }
    
    function ddlAuthListSelectIndexChange(obj)
    {
        var sValue = obj.options[obj.selectedIndex].value;
        ShowHideAuthContent(sValue);
    }
    
    function ShowHideAuthContent(sValue)
    {
        if(sValue =='false')
        {
            $('#AuthContent').hide();
        }
        else
        {
            $('#AuthContent').show();
        }
    }
</script>

<style type="text/css">
    .column1
    {
        width:auto;
        padding:5px 0px 5px 0px !important;
    }
    
    .column2
    {
        width:auto;
        padding:5px 0px 5px 0px !important;
    }
    
    #AuthContent td
    {
        padding:5px 0px 5px 0px !important;
        vertical-align:middle;
    }
    
    .AuthList
    {
        width: 200px;
        height: 20px;
        border: 1px solid #89A6C0;
    }
</style>

<div>
    <table width="auto">
        <tr>
            <td class="column1"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_1) %></b></td>
            <td class="column2">
                <asp:TextBox ID="txtServerAddress" runat="server" BorderColor="#89A6C0" BorderWidth="1px" Width="200px" Height="18px" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvServerAddress" runat="server" Display="Dynamic" ControlToValidate="txtServerAddress" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td class="column1"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_2) %></b></td>
            <td class="column2">
                <asp:TextBox ID="txtPortNumber" runat="server" BorderColor="#89A6C0" BorderWidth="1px" Width="200px" Height="18px" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvPortNumber" runat="server" Display="Dynamic" ControlToValidate="txtPortNumber" ErrorMessage="*"></asp:RequiredFieldValidator>
                <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtPortNumber" FilterType="Numbers" />
            </td>
        </tr>
        <tr>
            <td class="column1"></td>
            <td class="column2">
                <asp:CheckBox ID="chUseSSL" runat="server" Text="<%$HtmlEncodedResources:CoreWebContent,WEBDATA_AK1_6%>"/>
            </td>
        </tr>
        <tr>
            <td class="column1" style="vertical-align:top;"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_3) %></b></td>
            <td class="column2">
                <div>
                    <asp:DropDownList ID="ddlAuthList" runat="server" onchange="ddlAuthListSelectIndexChange(this);" CssClass="AuthList">
                        <asp:ListItem Text="<%$HtmlEncodedResources:CoreWebContent,WEBDATA_AK1_7%>" Value="false"></asp:ListItem>
                        <asp:ListItem Text="<%$HtmlEncodedResources:CoreWebContent,WEBDATA_AK1_8%>" Value="true"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <table id="AuthContent">
                    <tr>
                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_4) %></td>
                        <td>
                            <asp:TextBox ID="txtUsername" runat="server" BorderColor="#89A6C0" BorderWidth="1px" Width="200px" Height="18px" MaxLength="4000"/>
                        </td>
                    </tr>
                    <tr>
                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_5) %></td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" BorderColor="#89A6C0" BorderWidth="1px" Width="200px" Height="18px" TextMode="Password" MaxLength="4000"/>
                        </td>                    
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>        