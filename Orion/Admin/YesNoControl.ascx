<%@ Control Language="C#" AutoEventWireup="true" CodeFile="YesNoControl.ascx.cs" Inherits="Orion_Admin_YesNoControl" %>


<asp:ListBox ID="listBox" runat="server" Rows="1" SelectionMode="Single" >
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_124 %>" Value="true" />
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_125 %>" Value="false" />
</asp:ListBox>