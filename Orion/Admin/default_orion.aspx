<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="Untitled Page" %>

<%@ Import Namespace="System.ServiceModel" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.PackageManager" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Register Src="~/Orion/Admin/AdminBucket.ascx" TagPrefix="orion" TagName="AdminBucket" %>


<script runat="server">
    private const string ProductIntegrationVisibilitySettingName = "FreeProductIntegrationOnAdmin";
	private bool _displayFreeProductIntegration;
    private readonly SolarWinds.Orion.Core.Common.FeatureManager _featureManager = new SolarWinds.Orion.Core.Common.FeatureManager();

    private bool? _isNodeFunctionalityAllowed;
    /// <summary>
    /// (get) Return True if licence allow to add/manage nodes. False in the case, that licence doesn't allow it (for example user has just installed SRM module).
    /// </summary>
    private bool IsNodeFunctionalityAllowed
    {
        get
        {
            if (!_isNodeFunctionalityAllowed.HasValue)
            {
                int maxNodes = _featureManager.GetMaxElementCount(SolarWinds.Orion.Core.Common.WellKnownElementTypes.Nodes);
                _isNodeFunctionalityAllowed = maxNodes != 0;
            }

            return _isNodeFunctionalityAllowed.Value;
        }
    }

    private IEnumerable<string> GetEnabledFeatures()
    {
        return (_featureManager.EnabledFeatures).Concat(_featureManager.ValidElementTypes);
    }

    protected override void OnLoad(EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Page.Title = Resources.CoreWebContent.WEBCODE_TM0_34;
        base.OnLoad(e);
    }
    protected void Cross_Click(object sender, EventArgs e)
    {
        WebUserSettingsDAL.Set(HttpContext.Current.Profile.UserName, ProductIntegrationVisibilitySettingName, Boolean.FalseString);
        _displayFreeProductIntegration = false;
    }

    protected override void OnInit(EventArgs e)
    {
		var adminLinks = OrionModuleManager.FilterLinksByFeatureDependencies(OrionModuleManager.GetAdminLinks().ToArray(), GetEnabledFeatures());
        
        if (!IsNodeFunctionalityAllowed)
        {
            adminLinks = adminLinks.Where(item =>
            string.Compare(item.Href, "/Orion/AgentManagement/Admin/ManageAgents.aspx") != 0
            && string.Compare(item.Href, "/Orion/AgentManagement/Admin") != 0)
            .ToArray();
        }

        gettingStarted.AddLink("/Orion/Admin/DiscoveryCentral.aspx", Resources.CoreWebContent.WEBCODE_TM0_41, 1);
        if (IsNodeFunctionalityAllowed)
        {
            gettingStarted.AddLink("../Discovery/Default.aspx?origUrl=Admin", Resources.CoreWebContent.WEBDATA_VB0_31, 10);
            gettingStarted.AddLink("../Nodes/Add/Default.aspx", Resources.CoreWebContent.WEBCODE_TM0_42, 20);
        }

        gettingStarted.AddAdminLinks(adminLinks);

        if (IsNodeFunctionalityAllowed)
        {
            nodeAndGroupManagement.AddLink("../Nodes/Default.aspx", Resources.CoreWebContent.ResourcesAll_ManageNodes, 10);
        }

        nodeAndGroupManagement.AddLink("/Orion/Admin/DependenciesView.aspx", Resources.CoreWebContent.ResourcesAll_ManageDependencies, 30);
        //commented two items same as in original page
        //nodeAndGroupManagement.AddLink("../Discovery/ScheduledDiscovery.aspx", "Scheduled Network Discovery");
        //nodeAndGroupManagement.AddLink("../Discovery/ScheduledDiscoveryResults.aspx", "Scheduled Network Discovery Results");
        nodeAndGroupManagement.AddLink("/Orion/Admin/Containers/Default.aspx", Resources.CoreWebContent.ResourcesAll_ManageGroups, 35);
        nodeAndGroupManagement.AddLink("/Orion/Admin/CPE/Default.aspx", Resources.CoreWebContent.WEBDATA_SO0_4, 36);

        if (IsDeviceStudioInstalled)
        {
            nodeAndGroupManagement.AddLink("/Orion/Admin/Pollers/ManagePollers.aspx", Resources.CoreWebContent.WEBDATA_PS1_2, 5000);
        }

        nodeAndGroupManagement.AddAdminLinks(adminLinks);
        nodeAndGroupManagement.AddLink("/Orion/WorldMap/Manage.aspx", Resources.CoreWebContent.WEBDATA_PCC_50, 5000);
        nodeAndGroupManagement.Header = GetBundleHeader(nodeAndGroupManagement.ID, Resources.CoreWebContent.WEBCODE_TM0_36);

        accountManagement.AddLink("/Orion/Admin/Accounts/Accounts.aspx", Resources.CoreWebContent.WEBCODE_TM0_43, 10);
        //FB 18218 kill 'account views' page. (CL 103867)
        //accountManagement.AddLink("AccountViews.aspx", "Account Views");
        accountManagement.AddLink("/Orion/Admin/AccountList.aspx", Resources.CoreWebContent.WEBDATA_AK0_4, 15);
        accountManagement.AddLink("/Orion/Admin/Accounts/AdvancedADSettings.aspx", Resources.CoreWebContent.WEBDATA_IB0_207, 20);
        accountManagement.AddAdminLinks(adminLinks);
        credentias.AddLink("/Orion/Admin/Credentials/CredentialManager.aspx", Resources.CoreWebContent.WEBCODE_VB0_312, 1);
        credentias.AddLink("/Orion/Admin/Credentials/SNMPCredentialManager.aspx", Resources.CoreWebContent.WEBCODE_SO0_53, 2);
		credentias.AddAdminLinks(adminLinks);
        
        customize.AddLink("/Orion/Admin/SelectMenuBar.aspx", Resources.CoreWebContent.WEBDATA_AK0_8, 10);
        customize.AddLink("/Orion/Admin/ColorScheme.aspx", Resources.CoreWebContent.WEBDATA_AK0_9, 15);
        customize.AddLink("/Orion/Admin/ExternalWebsites.aspx", Resources.CoreWebContent.WEBCODE_TM0_44, 20);
        customize.AddAdminLinks(adminLinks);

        alertManagement.AddLink("/Orion/Alerts/Default.aspx", Resources.CoreWebContent.WEBCODE_YK0_26, 11);

        alertManagement.AddLink("/Orion/Reports/Default.aspx", Resources.CoreWebContent.WEBDATA_SO0_52, 12);
        alertManagement.AddLink("/Orion/Admin/SmtpServersManager.aspx", Resources.CoreWebContent.WEBDATA_TD0_1, 13);
        alertManagement.AddLink("/Orion/Admin/AlertSettings.aspx", Resources.CoreWebContent.WEBDATA_AB0_96, 20);
        alertManagement.AddAdminLinks(adminLinks);

        viewManagement.Visible = Profile.AllowCustomize;
        if (viewManagement.Visible)
        {
            if (Profile.AllowManageDashboards)
            {
                viewManagement.AddLink("/apps/platform/dashboard/manage", Resources.CoreWebContent.WebSettings_ManageDashboards, 10);
            }
            viewManagement.AddLink("AddView.aspx", Resources.CoreWebContent.WEBCODE_TM0_48, 15);
            viewManagement.AddLink("/Orion/Admin/ViewsByDeviceType.aspx", Resources.CoreWebContent.WEBDATA_AK0_7, 25);
            viewManagement.AddLink("/Orion/Admin/NOCView.aspx", Resources.CoreWebContent.WEBDATA_AB0_38, 20);
            viewManagement.AddAdminLinks(adminLinks);
        }

        settings.AddLink("/Orion/Admin/Settings.aspx", Resources.CoreWebContent.WEBDATA_AK0_10, 10);
        settings.AddLink("/Orion/Admin/OrionServiceManager.aspx", Resources.CoreWebContent.OrionServiceManager_Title, 10);
        settings.AddAdminLinks(adminLinks);
        settings.AddLink("/Orion/Admin/HttpProxySettings/Default.aspx", Resources.CoreWebContent.HttpProxySettings_Title, 20);

        thresholdsAndPolling.AddLink("/Orion/Admin/PollingSettings.aspx", Resources.CoreWebContent.WEBCODE_TM0_49, 10);
        thresholdsAndPolling.AddLink("/apps/settings/node-child-status-participation", Resources.CoreWebContent.AdminSettings_NodeChildStatusParticipation, 20);
        thresholdsAndPolling.AddAdminLinks(adminLinks);

        foreach (var module in OrionModuleManager.GetModules())
        {
            if (module.Name.Equals("EOC", StringComparison.OrdinalIgnoreCase) && !SwisFederationInfo.IsFederationEnabled)
                continue;
                
            var configPages = OrionModuleManager.FilterLinksByFeatureDependencies(OrionModuleManager.GetConfigPagesByModule(module.Name), GetEnabledFeatures());        

            if (!(configPages == null || configPages.Count == 0))
            {

                settings.AddLink("/Orion/Admin/ModuleConfig.aspx?ModuleID=" + module.Name, module.DefaultConfigPageTitle, 10);
            }
        }

        details.AddLink("/Orion/Admin/Details/DatabaseDetails.aspx", Resources.CoreWebContent.WEBDATA_AK0_12, 10);
        details.AddLink("/Orion/Admin/Details/Engines.aspx", Resources.CoreWebContent.WEBDATA_AK0_14, 15);
        details.AddLink("/Orion/Admin/Details/OrionCoreDetails.aspx", Resources.CoreWebContent.WEBCODE_TM0_50, 20);
        details.AddLink("/ui/ha/summary", Resources.CoreWebContent.WEBDATA_MC0_1, 23);
        details.AddLink("/Orion/Admin/Details/ModulesDetailsHost.aspx", Resources.CoreWebContent.WEBDATA_AK0_13, 25);
        details.AddAdminLinks(adminLinks);

        _displayFreeProductIntegration = (Boolean.TrueString).Equals(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, ProductIntegrationVisibilitySettingName, Boolean.TrueString));

        base.OnInit(e);
    }

    private static bool IsDeviceStudioInstalled
    {
        get
        {
            bool result;

            try
            {
                result = !PackageManager.Instance.IsPackageInstalled("Orion.DeviceStudio")
                         || PackageManager.Instance.GetPackageInfo("Orion.DeviceStudio").ParseVersion() >= new Version(2, 0, 0);
            }
            catch(EndpointNotFoundException)
            {
                result = false;
            }

            return result;
        }
    }

    private string GetBundleHeader(string bundleId, string defaultValue)
    {
        return GetUpdatedBundleValue(Customization.SettingsUpdateSectionHeader, bundleId, defaultValue);
    }

    private string GetBundleDescription(string bundleId, string defaultValue)
    {
        return GetUpdatedBundleValue(Customization.SettingsUpdateSectionDescription, bundleId, defaultValue);
    }

    private string GetUpdatedBundleValue(string action, string bucketId, string defaultValue)
    {
        var customValue = OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(action) && c.Key.Equals(bucketId)).Select(c => c.Value).ToArray().FirstOrDefault();
        return String.IsNullOrWhiteSpace(customValue) ? defaultValue : customValue;
    }
</script>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
<orion:Include runat="server" File="Admin/js/WebIntegration.js" />
<orion:Include runat="server" File="WebIntegrationLogin.css" />


    <style type="text/css">
    .sw-hdr-breadbox { display: none; } /* no breadcrumbs on admin page. */
    #adminContent{background-color: #dfe0e1;}  
	#adminContent p { width: auto; padding: 0; }
   
    #adminContent * .sw-hdr-title{font-weight: 700;font-size: 18px;text-transform: capitalize;line-height: 15px;padding: 15px;margin-right: 15px;background: #ffffff;box-shadow: 0 1px 5px 0 #b9b9b9;}
    .sw-is-locale-de .sw-hdr-title {text-transform: none!important;}
    #adminContent * h2.sw-hdr-title{border-bottom: white 1px solid;margin-top: 0;}
	#adminContent * .column1of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left;}
	#adminContent * .column2of2 {padding: 0; vertical-align:top; text-align: left; width: 500px; background-color: #ffffff;box-shadow: 0 1px 5px 0 #b9b9b9;}
    #adminContent * .column1of2 * img{ width: 75px;height: 75px;} 


    #adminContent * .blockHeader{ font-weight: 700;font-size: 16px;margin-top: 0;}
    #adminContent * td.column2of2 * .iconContainer{ padding-right: 10px;vertical-align: top;}
    #adminContent * .sw-settings-rightbox-content * .paragraphHeader{font-weight: 700;margin-bottom: 5px;}
    #adminContent * .sw-settings-rightbox-content p {margin-top: 0;margin-bottom: 0;}
    #adminContent * .sw-settings-rightbox-content .subParagraph{ margin-top: 20px;}
    #adminContent * .sw-settings-rightbox-content .collapseParagraph { margin-bottom: 5px;}
    #adminContent * .sw-settings-rightbox-content .collapseParagraph > span, #adminContent * .sw-settings-rightbox-content .collapseParagraph > img { cursor: pointer;}
    #adminContent * .sw-settings-rightbox-content .subParagraph > a{ cursor: pointer;display: block;margin-bottom: 5px;}
    #adminContent * .sw-settings-delimiter { border-top: 1px solid #fff;}

    #helpFromHumans * .collapseParagraph > div {margin: 10px 0px 15px 18px;display: none;}
    #helpFromHumans * .collapseParagraph > div > p > span{color:#888;}
    #helpFromHumans * .collapseParagraph > img {vertical-align: top;}

    #productEducation * .NewContentBucket { box-shadow: none;padding: 0;margin: 0;}
    #productEducation * .NewBucketLinkContainer { margin: 0;padding: 0;}
    #productEducation * .LinkColumn { padding-top: 0;}
    </style>
    
    <script type="text/javascript">
        $(document).ready(function () {
            if (SW.Core.Info.OIPEnabled) {
                $('a.sw-settings-link-outgoing').each(function () {
                    var href = $(this).attr('href');
                    $(this).attr('href', href.replace('NOIP', 'OIP'));
                });
            }
        });

        var ExpandCollapseOnHelpFromHumansWidget = function (colapseBlock) {
            var element = $('img:first', colapseBlock.parentElement)[0];
            var expanded = element.src.indexOf("Collapse") >= 0;

            element.src = expanded ? "/Orion/images/Button.Expand.gif" : "/Orion/images/Button.Collapse.gif";

            $("div:first", colapseBlock.parentElement).css("display", expanded ? "none" : "block");

            return false;
        };

    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks">
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">       

	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="settingsTable">
		<tbody>
		  <tr>
			<td class="column1of2">
			    <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

                <orion:AdminBucket ID="gettingStarted" runat="server" NewStyle="True"
				    Header="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_TM0_35 %>" 
				    Icon="../images/SettingPageIcons/signpost.svg">				    
				    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_91) %></Description>                    
				</orion:AdminBucket>

				<orion:AdminBucket ID="nodeAndGroupManagement" runat="server" NewStyle="True" 
				    Icon="../images/SettingPageIcons/management.svg">				    
				    <Description><%= DefaultSanitizer.SanitizeHtml(GetBundleDescription("nodeAndGroupManagement", Resources.CoreWebContent.WEBDATA_TM0_92)) %></Description>                  			    
				</orion:AdminBucket>
                
                <orion:AdminBucket ID="alertManagement" runat="server" NewStyle="True"
				    Header="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_53 %>"
				    Icon="/Orion/images/SettingPageIcons/allerts-reports.svg">
                    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_052) %></Description>		
				</orion:AdminBucket> 	

                <orion:AdminBucket ID="settings" runat="server" NewStyle="True"
			        Header="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZS0_023 %>"
			        Icon="../images/SettingPageIcons/product-specific.svg">
                    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_051) %></Description>		  
			    </orion:AdminBucket>                                                               			
                
                <orion:AdminBucket ID="thresholdsAndPolling" runat="server" NewStyle="True"
			        Header="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZS0_025 %>" 
			        Icon="../images/SettingPageIcons/thresholds-polling.svg">
                    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_053) %></Description>		   
			    </orion:AdminBucket>

                <orion:AdminBucket ID="credentias" runat="server" NewStyle="True"
				    Header="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_54 %>" 
				    Icon="../images/SettingPageIcons/key.svg">
                    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_056) %></Description>	
				</orion:AdminBucket>                 
                
				<orion:AdminBucket ID="accountManagement" runat="server" NewStyle="True"
				    Header="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_99 %>" 
				    Icon="../images/SettingPageIcons/users.svg">
				    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_055) %></Description>			
				</orion:AdminBucket>

                <% if (viewManagement.Visible)
                   { %>
                    <orion:AdminBucket ID="viewManagement" runat="server" NewStyle="True"
			            Header="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_104 %>" 
			            Icon="../images/SettingPageIcons/layout_small.svg">
                        <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_054) %></Description>		
			        </orion:AdminBucket>
                <% } %>                           

				<orion:AdminBucket ID="customize" runat="server" NewStyle="True"
				    Header="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_98 %>" 
				    Icon="../images/SettingPageIcons/customize.svg">
				    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_057) %></Description>	
				</orion:AdminBucket>     

                 <orion:AdminBucket ID="details" runat="server" NewStyle="True"
			        Header="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_105 %>" 
			        Icon="../images/SettingPageIcons/info.svg">
                    <Description><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_058) %></Description>		
			    </orion:AdminBucket>            
			</td>
			<!-- Begin 2nd Column -->
			<td class="column2of2">
               <%-- <h2 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_066) %></h2>--%>
                <orion:SettingsPageControlHost runat="server"/>
                 <div id="productEducation" class="sw-settings-rightbox sw-settings-delimiter">
                    <div class="sw-settings-icon sw-settings-icon-swintegration">&nbsp;</div>
                    <div class="sw-settings-rightbox-content">
                        <div class="blockHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_073) %></div>
                        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_074) %></span>
                        <div class="subParagraph">
                            <p class="paragraphHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_075) %></p>
                            <a class="sw-link" href="https://support.solarwinds.com/SuccessCenter/s/solarwinds-academy-training-classes" onclick="SW.Core.Analytics.TrackEvent('Settings Page Interaction', 'Transition', 'Virtual Classrooms');" target="_blank">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_076) %></a>
                        </div>                    
                    </div>
                </div>       
                <div id="helpFromHumans" class="sw-settings-rightbox sw-settings-delimiter">
                    <div class="sw-settings-icon sw-settings-icon-swhelpfromhuman">&nbsp;</div>
                    <div class="sw-settings-rightbox-content">
                        <div class="blockHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_067) %></div>
                        <div class="subParagraph">
                            <p class="paragraphHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.HelpFromHumans_SupportHeader) %></p>
                            <div id="contactdetails" class="collapseParagraph">
                                <img src="/Orion/images/Button.Expand.gif" alt="Expand/Collapse" onclick="return ExpandCollapseOnHelpFromHumansWidget(this);" />
                                <span onclick="return ExpandCollapseOnHelpFromHumansWidget(this);"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.HelpFromHumans_SupportContactDetailsHeader) %></span>
                                <div>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_030) %></span> <a href="tel:866-530-8040">866-530-8040</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_031) %></span> <a href="tel:+353 21 5002900">+353 21 5002900</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_032) %></span> <a href="tel:+65 6593 7600">+65 6593 7600</a></p>
                                </div>
                            </div>
                            <a class="sw-link" target="_blank" rel="noopener noreferrer" href="https://customerportal.solarwinds.com/support/submit-a-ticket/">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.HelpFromHumans_SubmitATicketLink) %></a>
                        </div>
                        <div class="subParagraph">
                            <p class="paragraphHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_068) %></p>
                            <div id="needaquote" class="collapseParagraph">
                                <img src="/Orion/images/Button.Expand.gif" alt="Expand/Collapse" onclick="return ExpandCollapseOnHelpFromHumansWidget(this);"/>
                                <span onclick="return ExpandCollapseOnHelpFromHumansWidget(this);"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_069) %></span>
                                <div>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_029) %></span><a href="mailto:customersales@solarwinds.com" class="sw-link"> customersales@solarwinds.com</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_030) %></span> <a href="tel:866 530 8100">866 530 8100</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_031) %></span> <a href="tel:+353 21 5002900">+353 21 5002900</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_032) %></span> <a href="tel:+61 2 8412 4900">+61 2 8412 4900</a></p>
                                </div>
                            </div>
                            <div id="renewalquestions" class="collapseParagraph">
                                <img src="/Orion/images/Button.Expand.gif" alt="Expand/Collapse" onclick="return ExpandCollapseOnHelpFromHumansWidget(this);"/>
                                <span onclick="return ExpandCollapseOnHelpFromHumansWidget(this);"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_070) %></span>
                                <div>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_029) %></span><a href="mailto:renewals@solarwinds.com" class="sw-link"> renewals@solarwinds.com</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_030) %></span> <a href="tel:866 530 8100">866 530 8100</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_031) %></span> <a href="tel:+353 21 5002900">+353 21 5002900</a></p>
                                    <p><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_032) %></span> <a href="tel:+61 2 8412 4900">+61 2 8412 4900</a></p>
                                </div>                                      
                            </div>
                        </div>
                        <div class="subParagraph">
                            <p class="paragraphHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_072) %></p>
                            <a class="sw-link" target="_blank" rel="noopener noreferrer" href="https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=maintenance_card">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_040) %></a>
                            <a class="sw-link" target="_blank" rel="noopener noreferrer" href="https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=thwack_product">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_059) %></a>
                            <a class="sw-link" target="_blank" rel="noopener noreferrer" href="https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=thwack_feature">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_060) %></a>
                        </div>
                        <div class="subParagraph">
                            <p class="paragraphHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_026) %></p>                                                        
                            <a class="sw-link sw-settings-link-outgoing" target="_blank" rel="noopener noreferrer" href="http://solarwinds.participate-in-solarwinds-ux-feedback.sgizmo.com/s3/">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZS0_061) %></a>
                        </div>            
                    </div>           
                </div>
			</td>
		  </tr>
		</tbody>
	</table>
</asp:Content>
