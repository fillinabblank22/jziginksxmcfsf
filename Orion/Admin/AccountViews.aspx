<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="AccountViews.aspx.cs" 
    Inherits="Orion_Admin_AccountViews" Title="Account Views" %>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <asp:PlaceHolder runat="server" ID="AccountTable" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
<orion:Include runat="server" File="AccountList.css" />
</asp:Content>