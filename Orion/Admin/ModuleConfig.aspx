<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="ModuleConfig.aspx.cs" Inherits="Orion_Admin_ModuleConfig" Title="Untitled Page" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <h1><%= DefaultSanitizer.SanitizeHtml(this.Title) %></h1>
        
    <asp:Repeater runat="server" ID="rptConfigPages">
        <ItemTemplate>
            <h2><a style="color: Blue; text-decoration: underline;" href='<%# DefaultSanitizer.SanitizeHtml(Eval("Path")) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("Title")) %></a></h2>
            
            <p><%# DefaultSanitizer.SanitizeHtml(Eval("Description")) %></p>
        </ItemTemplate>
    </asp:Repeater>

</asp:Content>

