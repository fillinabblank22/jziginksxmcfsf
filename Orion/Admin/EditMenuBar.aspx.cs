using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_EditMenuBar : System.Web.UI.Page
{
	#region Properties
	protected string MenuName
	{
		get
		{
            return this.Request.QueryString["Menu"];
		}
	}

	protected bool IsNew
	{
		get
		{
			return String.IsNullOrEmpty(this.MenuName);
		}

	}

	private MenuBar menuBar;

	protected MenuBar MenuBar
	{
		get
		{
			if (this.IsNew)
			{
				return null;
			}
			if (this.menuBar == null)
			{
				this.menuBar = new MenuBar(this.MenuName);
			}
			return this.menuBar;
		}
	}

	protected List<int> CurrentMenu { get; set; }

	#endregion

	#region Events
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		this.newMenuBar.Visible = this.IsNew;
		this.Page.Title = Server.HtmlEncode(string.Format(
            Resources.CoreWebContent.WEBCODE_TM0_76,
            (!this.IsNew) ? $"{Resources.CoreWebContent.WEBCODE_VB1_15} {this.MenuName}" : Resources.CoreWebContent.WEBDATA_AK0_138
        ));
	}

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		if (this.IsPostBack)
		{
			this.DeserializeSelectedItems(this.selectedMenuItems.Value);
		}
		this.PopulateMenuItems();
	}

    protected void SubmitClick(object sender, EventArgs e)
	{
	    if (Page.IsValid)
	    {
	        MenuBar menu;
	        if (this.IsNew)
	        {
	            menu = new MenuBar(WebSecurityHelper.SanitizeHtmlV2(this.newName.Text));
	            if (menu.Count > 0)
	            {
	                this.errorMessage.Controls.Add(new LiteralControl(Resources.CoreWebContent.WEBCODE_VB1_16));
	                this.errorMessage.Visible = true;
	                return;
	            }
	        }
	        else
	        {
	            menu = this.MenuBar;
	        }

	        if (this.CurrentMenu.Count == 0 && menu.IsSystemMenu)
	        {
	            this.errorMessage.Controls.Add(new LiteralControl(Resources.CoreWebContent.WEBCODE_VB1_17));
	            this.errorMessage.Visible = true;
	            return;
	        }
	        menu.SetMenuItems(this.CurrentMenu);

            this.Response.Redirect("SelectMenuBar.aspx");
	    }
	}

	protected void DeleteCustomMenuItemClick(object sender, EventArgs e)
	{
		ImageButton button = sender as ImageButton;
		if (sender != null)
		{
			int id = Convert.ToInt32(button.CommandArgument);
			MenuBar.DeleteMenuItem(id);
			if (this.CurrentMenu.Contains(id))
			{
				this.CurrentMenu.Remove(id);
			}
			this.PopulateMenuItems();
		}
	}

	protected void EditCustomItemClick(object sender, EventArgs e)
	{
        int id = Convert.ToInt32(this.editCustomItemID.Value);
		string title = this.editCustomTitle.Value.Trim();
		string description = this.editCustomDescription.Value.Trim();
		string url = this.editCustomURL.Value.Trim();

        if (!IsValidURL(url))
        {
            return;
        }
            

        bool newWindow;
		Boolean.TryParse(this.editCustomOpenNewWindow.Value, out newWindow);
		if (id == 0)
		{
			MenuBar.CreateCustomMenuItem(title, description, url, newWindow);
		}
		else
		{
			MenuBar.UpdateCustomMenuItem(id, title, description, url, newWindow);
		}
		this.PopulateMenuItems();
	}
	#endregion

	#region private methods
	private void PopulateMenuItems()
	{
		IEnumerable<SolarWinds.Orion.Web.MenuItem> selected;
		IEnumerable<SolarWinds.Orion.Web.MenuItem> available;

		if (this.IsPostBack)
		{
			IEnumerable<SolarWinds.Orion.Web.MenuItem> all = MenuBar.GetAllMenuItems();

			selected = this.CurrentMenu.Select(id => all.First(item => item.MenuItemID == id));
			available = all.Except(selected);
		}
		else if (!this.IsNew)
		{
			selected = this.MenuBar;
			available = this.MenuBar.GetAvailableMenuItems();
		}
		else
		{
			selected = new List<SolarWinds.Orion.Web.MenuItem>();
			available = MenuBar.GetAllMenuItems();
		}
		this.menuItems.DataSource = selected;
		this.menuItems.DataBind();

		this.availableMenuItems.DataSource = available;
		this.availableMenuItems.DataBind();
	}

	private void DeserializeSelectedItems(string selectedItems)
	{
		this.CurrentMenu = new List<int>();
		if (!String.IsNullOrEmpty(selectedItems))
		{
			foreach (var id in selectedItems.Split(','))
			{
				this.CurrentMenu.Add(Convert.ToInt32(id));
			}
		}
	}

    /// <summary>
    /// Validates if input is valid URI. In case of absolute it ensures that is http(s) schema. 
    /// Stuff like 'javascript:prompt(document.domain)' would fail.
    /// </summary>    
    private bool IsValidURL(string url)
    {
        try
        {
            Uri result;
            if (Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out result))
            {
                if (!result.IsAbsoluteUri)
                    return true;

                if (result.Scheme.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                    return true;
            }

            return false;
        }
        catch (Exception)
        {
            return false;
        }
    }

    protected void newNameCustomValidator_Validate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = WebSecurityHelper.SanitizeHtmlV2(this.newName.Text) == this.newName.Text;

    }
	#endregion

	#region Ajax web methods
	[WebMethod]
	public static SolarWinds.Orion.Web.MenuItem GetCustomItem(int id)
	{
		return MenuBar.GetMenuItem(id);
	}
	#endregion
}
