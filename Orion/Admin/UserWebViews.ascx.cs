﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.InformationService.Linq.Plugins.Core.Orion.Web;
using SolarWinds.Orion.Content.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_UserWebViews : System.Web.UI.UserControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private RestrictableWebViewsDAL dal = new RestrictableWebViewsDAL();
       
    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("Admin/Accounts/js/EditAccounts.js");
        try
        {
            rptWebViews.DataSource = dal.RestrictableWebViewsViews();
            rptWebViews.DataBind();
        }
        catch (Exception ex)
        {
            log.Error("Error while loading restrictable views.", ex);
        }
        base.OnInit(e);
    }

    public void SetUpValues(string account)
    {
        if (!IsPostBack)
        {
            try
            {
                SetBlackListedViews(dal.GetAccountBlackListedViews(account));
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error while loading view restrictions for account {0}. {1}", account, ex);
            }
        }
    }

    public void StoreValues(string account)
    {
        try
        {
            dal.SetAccountBlackListedViews(account, GetBlackListedViews());
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Error while storing restricted views for account {0}. {1}", account, ex);
        }
    }

    private Dictionary<int, bool> GetBlackListedViews()
    {
        Dictionary<int, bool> viewRestrictions = new Dictionary<int, bool>();
        foreach (RepeaterItem item in rptWebViews.Items)
        {
            try
            {
                var cb = (HtmlInputCheckBox)item.FindControl("chbxWebViewVisible");
                if (!cb.Visible || cb.Checked)
                {
                    var hf = (HiddenField)item.FindControl("ViewID");
                    int webViewID = int.Parse(hf.Value);

                    var ynControl = (Orion_Admin_YesNoControl)item.FindControl("ynViewMenu");

                    viewRestrictions[webViewID] = ynControl.Value;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while getting blacklisted views for account.", ex);
            }
        }
        return viewRestrictions;
    }

    private void SetBlackListedViews(List<int> restrictedViews)
    {
        foreach (RepeaterItem item in rptWebViews.Items)
        {
            try {
                var hf = (HiddenField)item.FindControl("ViewID");
                int webViewID = int.Parse(hf.Value);

                var ynControl = (Orion_Admin_YesNoControl)item.FindControl("ynViewMenu");

                ynControl.Value = !restrictedViews.Contains(webViewID);
            }
            catch (Exception ex) {
                log.Error("Error while setting account restricted views.", ex);
            }
        }
    }

}