using System;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using Resources;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_ViewGroupEditor : BaseViewGroupControl
{
    private const string AlowedCharsPattern = @"^[-\w.() ]*$";
    private const string AlowedFileNamePattern = @"^[a-zA-Z0-9_\-\.]+$";

    protected IconInfo CurrentIconInfo {
        get {
            
            return new IconInfo(this.View);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        if (this.IsPostBack)
            View.ViewTitle = WebSecurityHelper.SanitizeHtmlV2(txtViewTitle.Text);//Make sure title is updated when other controls post back.
        base.OnLoad(e);
    }

    protected override void RebuildUI()
    {
        this.Page.Title = string.Format(CoreWebContent.WEBCODE_VB0_225, View.ViewTitle);
        
        txtViewTitle.Text = View.ViewShortTitle;
        ViewGroupSelectedIcon.Value = View.ViewIcon;

        imgViewGroupIcon.Src = CurrentIconInfo.WebImagePath;
        imgViewGroupIcon.Visible = !string.IsNullOrEmpty(this.View.ViewIcon);

        BindIconGalleryImages();
    }

    private void BindIconGalleryImages()
    {
        string[] serverFileNames = System.IO.Directory.GetFiles(Server.MapPath( IconInfo.iconImagesFolderLocation));
        List<IconInfo> webFileNames = new List<IconInfo>();
        foreach (var f in serverFileNames)
        {
            System.IO.FileInfo info = new System.IO.FileInfo(f);
            if (!info.Name.ToLower().StartsWith("internal"))
            {
                IconInfo i = new IconInfo(info.Name);
                webFileNames.Add(i);
            }
        }

        rptIcons.DataSource = webFileNames;
        rptIcons.DataBind();
    }

    protected void UpdateIcon_Click(object sender, EventArgs e)
    {
        try
        {
            // XSS mitigation CORE-16399. 
            if (!Regex.IsMatch(ViewGroupSelectedIcon.Value, AlowedFileNamePattern))
            {
                throw new ArgumentException("Unsupported icon name format");
            }

            this.View.ViewIcon = WebSecurityHelper.SanitizeHtmlV2(ViewGroupSelectedIcon.Value);
            ViewGroupSelectedIcon.Value = View.ViewIcon;
            imgViewGroupIcon.Src = CurrentIconInfo.WebImagePath;
            imgViewGroupIcon.Visible = !string.IsNullOrEmpty(this.View.ViewIcon);
            ViewManager.UpdateView(this.View);
            RefreshPage();
        }
        catch (ArgumentException)
        {
            ShowTitleValidationError();
        }
    }

    protected void UpdateViewTitle_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }

        this.View.ViewTitle = WebSecurityHelper.SanitizeHtmlV2(txtViewTitle.Text);
        if (!string.IsNullOrEmpty(ViewGroupSelectedIcon.Value))
        {
            this.View.ViewIcon = WebSecurityHelper.SanitizeHtmlV2(ViewGroupSelectedIcon.Value);
        }
        ViewManager.UpdateView(this.View);
        RefreshPage();
    }

    private void ShowTitleValidationError()
    {
        // specifically catching the argument exception, it means that the view title is not unique
        phStatusMessage.Controls.Clear();
        phStatusMessage.Controls.Add(
            new LiteralControl(string.Format(Resources.CoreWebContent.WEBCODE_TM0_51, txtViewTitle.Text)));
    }

    protected void TxtViewTitleUniqueValidate(object source, ServerValidateEventArgs args)
    {
        if (args.IsValid)
        {
            foreach (var view in ViewManager.GetViewsByGroup(this.View.ViewGroup))
            {
                if (view.ViewShortTitle.Equals(txtViewTitle.Text, StringComparison.OrdinalIgnoreCase) && view.ViewID != this.View.ViewID)
                {
                    args.IsValid = false;
                    ShowTitleValidationError();
                    return;
                }
            }
        }
    }

    protected void TxtViewTitleValidate(object source, ServerValidateEventArgs args)
    {
        string title = WebSecurityHelper.SanitizeAngular(txtViewTitle.Text);        
        args.IsValid = Regex.IsMatch(title, AlowedCharsPattern);

        if (!args.IsValid)
        {
            txtViewTitle.Text = title;
        }
    }
}

