<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/AdminPage.master" AutoEventWireup="true" 
    CodeFile="WebIntegrationProxy.aspx.cs" Inherits="Orion_Admin_WebIntegrationProxy"  Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_JD0_13 %>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    
    <div>
        <p style="width: 300px!important;white-space: nowrap">
            <img src="/Orion/images/stop_32x32.gif" />
            <span style="line-height: 32px;vertical-align: 31%;padding-left: 10px;font-weight: bold;font-size: 120%">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JD0_14) %>
            </span>
        </p>
        <br />
        <p><orion:LocalizableButton ID="LocalizableButton1" LocalizedText="Back" DisplayType="Secondary" runat="server" OnClientClick="history.go(-1)" /></p>
    </div>
    
</asp:Content>