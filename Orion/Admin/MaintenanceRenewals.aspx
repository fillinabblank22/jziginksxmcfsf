<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="MaintenanceRenewals.aspx.cs" 
    Inherits="Orion_Admin_MaintenanceRenewals" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_103 %>" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="linksContent" runat="server" ContentPlaceHolderID="TopRightPageLinks">
        <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionProductUpdates" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
<style type="text/css">
    #adminContentTable td
    {
    	padding: 10px 10px 10px 10px;
    }
    
    #adminContentTable
    {
    	margin: 10px 10px 0px 0px;
    }

    #hint-to-matrix .link-to-matrix 
    {
            color: #369; text-decoration: underline;
    }

   
</style>

<script type="text/javascript">
    function DownloadSelected() {
        $('.cbUpdate').each(function() {
            if (this.childNodes[0].checked) {
                window.open(this.title, "_blank");
            }
        });
    }
</script>

<table width="100%">
    <tr>
        <td>
            <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
        </td>
        <tr>
        <td colspan="2" style="font-size: 8pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_411) %></td>
    </tr>
</table>
    
<div id="hint-to-matrix" class="sw-suggestion">
    <span class="sw-suggestion-icon"></span>
    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZT0_55) %></b>
    <asp:HyperLink runat="server" id="litLinkToMatrix" />
</div>

<table id="adminContentTable" width="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr>
        <td style="font-size: 8pt;">
            <table class="UpdatesSettingsTable" cellpadding=0 cellspacing=0>
                <tr>
                    <td>
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_412) %></b> <%= DefaultSanitizer.SanitizeHtml(UpdateNeverChecked ? Resources.CoreWebContent.WEBCODE_VB0_242 : LastUpdateCheck.ToLocalTime().ToString(System.Globalization.CultureInfo.CurrentCulture)) %>&nbsp;
                    </td>
                    <td>
                        <%if (SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("MaintenanceRenewals-Check").SettingValue == 1)
                          { %>
                            
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_413) %></b> <%= DefaultSanitizer.SanitizeHtml(NextUpdateUnknown ? Resources.CoreWebContent.WEBCODE_AK0_6 : NextUpdateCheck.ToLocalTime().ToString(System.Globalization.CultureInfo.CurrentCulture)) %>&nbsp;
                        <%} %>
                    </td>
                    <td>
                        <orion:LocalizableButton ID="ImageButton2" LocalizableText="CustomText" DisplayType="Small" runat="server" OnClick="ForceUpdateCheck" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_417 %>" />
                    </td>
                </tr>
				<tr>
					<td colspan="3">
					<br />
					<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_414) %>.
					</td>
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <div class="UpdatesSettingsTable" >
                <asp:CheckBox 
                    ID="cbAutoCheck" 
                    runat="server" 
                    AutoPostBack="false" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_415 %>"
                    /> 
                <br />
                <asp:CheckBox ID="cbShowIgnored" runat="server" AutoPostBack="false" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_416 %>" />
                <br />
                <br />
                <div class="sw-btn-bar">
                <orion:LocalizableButton ID="btnSave" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_418 %>" DisplayType="Primary" runat="server" OnClick="SaveSettings" />
                <orion:LocalizableButton ID="btnCancel" Localizedtext="Cancel" DisplayType="Secondary" runat="server" OnClick="RevertSettings" />
                <orion:LocalizableButton ID="btnRefresh" Localizedtext="Refresh  " DisplayType="Secondary" runat="server" OnClick="Refresh" />     
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="UpdatesItems">
            <asp:Repeater runat="server" ID="UpdatesTable">
                <HeaderTemplate>
                    <div class="ProductUpdates"><%= DefaultSanitizer.SanitizeHtml(this.message) %></div>
                    <table id="inside_table" cellpadding="0" cellspacing="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr><td colspan="2" style="padding: 0px 0px 0px 0px!important; height: 10px;"></td></tr>
                    <tr class="UpdatesItem">
                        <td style="width: 10px;">
                            <span class="cbUpdate" title='<%# DefaultSanitizer.SanitizeHtml(Eval("Url").ToString()) %>'><asp:CheckBox runat="server" ID="cbUpdate" /></span>
                        </td>
                        <td class="UpdateDescription">
                            <b><%# DefaultSanitizer.SanitizeHtml(Eval("Title")) %></b>
                            <br />
                            <div class="sw-btn-bar">
                                <orion:LocalizableButtonLink CssClass="DownloadUpdatesLink" runat="server" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_410 %>" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("Url").ToString()) %>' DisplayType="Small" />
                                <orion:LocalizableButton LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_42 %>" DisplayType="Small"
                                    runat="server" 
                                    OnClick="Acknowledge" 
                                    Visible='<%# DefaultSanitizer.SanitizeHtml(!Convert.ToBoolean(Eval("Ignored"))) %>' />
                                <asp:HiddenField runat="server" ID="hfUpdateKey" Value='<%# DefaultSanitizer.SanitizeHtml(Eval("Id").ToString()) %>' />
                                <asp:HiddenField runat="server" ID="hfUpdateLink" Value='<%# DefaultSanitizer.SanitizeHtml(Eval("Url").ToString()) %>' />
                            </div>
                            <br />
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Description").ToString().Replace("* ", "<br />• ")) %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
        </td>
    </tr>
</table>
<br />
<div class="sw-btn-bar">
<orion:LocalizableButtonLink ID="downloadSelected" style="cursor:pointer;" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_419 %>" runat="server" DisplayType="Primary" onclick="DownloadSelected();" />
<orion:LocalizableButton ID="ImageButton1" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_420 %>" DisplayType="Secondary" runat="server" OnClick="AcknowledgeSelected"/>
</div>
<br />   
</asp:Content>