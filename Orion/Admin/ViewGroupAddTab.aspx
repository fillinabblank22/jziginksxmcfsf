<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" AutoEventWireup="true"
    CodeFile="ViewGroupAddTab.aspx.cs" Inherits="Orion_Admin_ViewGroupAddTab" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_201 %>" %>

<%@ Register TagPrefix="orion" TagName="resourceEditor" Src="~/Orion/Admin/ResourceEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="resourceColumnWidthEditor" Src="~/Orion/Admin/ResourceColumnWidthEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="ViewGroupEditor" Src="~/Orion/Admin/ViewGroupEditor.ascx" %>
<%@ Register TagPrefix="orion" TagName="viewGroupSubNav" Src="~/Orion/ViewGroupSubNav.ascx" %>
<%@ Import Namespace="Resources" %>
<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <style type="text/css">
        .viewColumnButtons input
        {
            vertical-align: top;
            margin-bottom: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1>
        <asp:Literal runat="server" ID="PageHeader"></asp:Literal></h1>
    
    <!-- This Markup is similar to /Admin/CustomizeView and should most likly be kept in sync -->
    <table>
        <tr>
            <td class="subNavWrapperAdmin">
                <orion:viewGroupSubNav runat="server" ID="myViewGroupSubNav" />
            </td>
            <td>
                <orion:ViewGroupEditor runat="server" ID="myViewGroupEditor" />
                <orion:resourceColumnWidthEditor runat="server" ID="myResourceColumnWidthEditor" />
                <orion:resourceEditor runat="server" ID="myResourceEditor" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <div class="sw-btn-bar">
                    <orion:LocalizableButton ID="LocalizableButton1" runat="server" OnClick="DoneClick"
                        DisplayType="Primary" LocalizedText="Done" />
                    <orion:LocalizableButtonLink ID="Preview" runat="server" DisplayType="Secondary"
                        Target="_blank" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_280 %>"
                        Visible="false" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_281 %>" />
                </div>
                <div class="StatusMessage">
                    <asp:PlaceHolder runat="server" ID="phErrorMessage" />
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
