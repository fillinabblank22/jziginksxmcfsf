using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Dependencies_Add_Dependent : DependencyWizardBase
{
    public string ReturnUrl
    {
        get
        {
            return string.Format("?returnUrl={0}", UrlHelper.ToSafeUrlParameter(string.Format("{0}{1}", Request.AppRelativeCurrentExecutionFilePath,
                Request["DependencyID"] != null ? "?DependencyID=" + Request["DependencyID"] : string.Empty)));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Request["DependencyID"] != null ? Resources.CoreWebContent.WEBCODE_VB0_297 : Resources.CoreWebContent.WEBDATA_VB0_514;

        if (!IsPostBack)
        {
            if (Workflow.ChildDependencyObject != null)
            {
                Grid.SelectedObject = Workflow.ChildDependencyObject.Uri;
                SetParentObject(false);
                if (Workflow.ChildData.Count > 0)
                {
                    Grid.SetGridData(Workflow.ChildData);
                }
                return;
            }

            int dependencyId;
            bool isEdit = false;

            if (Request["DependencyID"] != null && int.TryParse(Request["DependencyID"], out dependencyId))
            {
                isEdit = true;
                using (var dal = new DependenciesDAL())
                {
                    var dt = dal.GetDependency(dependencyId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        dependencyName.Text = (string.IsNullOrEmpty(Workflow.DependencyName)) ? dt.Rows[0]["Name"].ToString() : Workflow.DependencyName;
                        Grid.SelectedObject = dt.Rows[0]["ChildUri"].ToString();
                    }
                }
            }

            SetParentObject(isEdit);
        }
    }

    // temp workaround to get entity type from uri
    // note: when you touch this, also touch Review.aspx.cs
    private string GetSWISEntityTypeByUri(string uri)
    {
        string entityType = UriHelper.GetUriType(uri);
        if (entityType.StartsWith("interfaces", StringComparison.OrdinalIgnoreCase))
            return string.Format("Orion.NPM.{0}", entityType);
        else if (entityType.StartsWith("application", StringComparison.OrdinalIgnoreCase))
            return string.Format("Orion.APM.{0}", entityType);
        return entityType;
    }

    private void SetParentObject(bool isEdit)
    {
        var parentObject = Workflow.ParentDependencyObject;
        if (parentObject != null)
        {
            DataTable objectTable = null;
            using (var dal = new DependenciesDAL())
            {
                var isInterfacesAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");
                objectTable = dal.GetObjectFromUri(parentObject.EntityName, parentObject.Uri);
            }
            var name = (objectTable != null && objectTable.Rows.Count > 0) ? objectTable.Rows[0]["Name"].ToString() : string.Empty;
            if (!isEdit)
            {
                dependencyName.Text = (string.IsNullOrEmpty(Workflow.DependencyName)) ? string.Format(Resources.CoreWebContent.WEBCODE_VB0_207, name) : Workflow.DependencyName;
            }

            int depStatus = (objectTable != null && objectTable.Rows.Count > 0) ? Convert.ToInt32(objectTable.Rows[0]["Status"]) : 0;
            int itemId = (objectTable != null && objectTable.Rows.Count > 0) ? Convert.ToInt32(objectTable.Rows[0]["ID"]) : 0;
            StatusIcon.ImageUrl = string.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small&id={2}", parentObject.EntityName, depStatus, itemId);
            Label.Text = HttpUtility.HtmlEncode(name);
        }
    }

    protected void OnObjectChanged()
    {
        var selObj = Grid.SelectedObject;
        DependencyObjectEntityUri childDependencyObject = new DependencyObjectEntityUri();
        childDependencyObject.EntityName = Grid.EntityNameValue;
        childDependencyObject.Uri = selObj;
        Workflow.ChildDependencyObject = childDependencyObject;
        Workflow.ChildData = Grid.GetGridData();
    }

    protected override bool Next()
    {
        if (!string.IsNullOrEmpty(Grid.SelectedObject))
        {
            DependencyObjectEntityUri childDependencyObject = new DependencyObjectEntityUri();
            childDependencyObject.EntityName = Grid.EntityNameValue;
            childDependencyObject.Uri = Grid.SelectedObject;
            Workflow.ChildDependencyObject = childDependencyObject;
            Workflow.DependencyName = dependencyName.Text;
        }

        return true;
    }

    
    protected void ValidateDependency(object source, ServerValidateEventArgs args)
    {
        int dependencyId = 0;

        if (Request["DependencyID"] != null) int.TryParse(Request["DependencyID"], out dependencyId);


        if (string.IsNullOrEmpty(Grid.SelectedObject))
        {
            DependencyValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_302;
            args.IsValid = false;
            return;
        }

        if (Workflow.ParentDependencyObject.Uri.Equals(Grid.SelectedObject))
        {
            DependencyValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_303;
            args.IsValid = false;
            return;
        }

        if (UriHelper.GetUriType(Workflow.ParentDependencyObject.Uri).Equals("Orion.Groups", StringComparison.OrdinalIgnoreCase))
        {
            using (var dal = new ContainersDAL())
            {
                int groupId = UriHelper.GetId(Workflow.ParentDependencyObject.Uri);
                if (groupId > 0 && DependencyCache.IsGroupMember(dal, groupId, groupId, Grid.SelectedObject))
                {
                    DependencyValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_304;
                    args.IsValid = false;
                    return;
                }
            }
        }

        if (UriHelper.GetUriType(Grid.SelectedObject).Equals("Orion.Groups", StringComparison.OrdinalIgnoreCase))
        {
            using (var dal = new ContainersDAL())
            {
                int groupId = UriHelper.GetId(Grid.SelectedObject);
                if (groupId > 0 && DependencyCache.IsGroupMember(dal, groupId, groupId, Workflow.ParentDependencyObject.Uri))
                {
                    DependencyValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_305;
                    args.IsValid = false;
                    return;
                }
            }
        }

        using (var dal = new DependenciesDAL())
        {
            var duplicates = dal.GetDependencyDuplicates(Workflow.ParentDependencyObject.Uri, Grid.SelectedObject, dependencyName.Text.Replace("'", "''"), dependencyId);
            if (duplicates.Rows.Count > 0)
            {
                var message = string.Empty;

                foreach (DataRow row in duplicates.Rows)
                {
                    if (row["Name"].ToString().Equals(dependencyName.Text, StringComparison.OrdinalIgnoreCase))
                    {
                        message = Resources.CoreWebContent.WEBCODE_VB0_306;
                        break;
                    }

                    var rowParentUri = row["ParentUri"].ToString();
                    var rowChildUri = row["ChildUri"].ToString();

                    if (SwisUriComparer.SwisUriEquals(Workflow.ParentDependencyObject.Uri, rowParentUri) &&
                        SwisUriComparer.SwisUriEquals(Grid.SelectedObject, rowChildUri))
                    {
                        message = Resources.CoreWebContent.WEBCODE_VB0_307;
                        break;
                    }
                }
                DependencyValidator.ErrorMessage = message;
                args.IsValid = false;
                
            }
        }

    }
}
