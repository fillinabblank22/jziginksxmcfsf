using System;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Models.Alerts;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using WebLimitation = SolarWinds.Orion.Web.Limitation;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Admin_Dependencies_Add_Review : DependencyWizardBase
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
	private ActiveAlertPage _alertsOnChild = null;
    
	int _dependencyId {
		get 
		{			
			return (int)(ViewState["_dependencyId"]??0);
		}
		
		set 
		{
			ViewState["_dependencyId"] = value;
		}
	}
	
	bool _automanaged {
		get 
		{			
			return (bool)(ViewState["_automanaged"]??false);
		}
		
		set 
		{
			ViewState["_automanaged"] = value;
		}
	}
	
	bool _foundAsAutomanaged {
		get 
		{			
			return (bool)(ViewState["_foundAsAutomanaged"]??false);
		}
		
		set 
		{
			ViewState["_foundAsAutomanaged"] = value;
		}
	}
	
	string _owner {
		get 
		{			
			return ViewState["_owner"] as string;
		}
		
		set 
		{
			ViewState["_owner"] = value;
		}
	}
	
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Request["DependencyID"] != null ? Resources.CoreWebContent.WEBCODE_VB0_297 : Resources.CoreWebContent.WEBDATA_VB0_514;

        if (!IsPostBack)
        {
            int dependencyId;
            bool isEdit = false;
		    _alertsOnChild = null;

            if (Request["DependencyID"] != null && int.TryParse(Request["DependencyID"], out dependencyId))
            {
                _dependencyId = dependencyId;
                isEdit = true;
                using (var dal = new DependenciesDAL())
                {
                    var dt = dal.GetDependency(dependencyId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        dependencyName.Text = (string.IsNullOrEmpty(Workflow.DependencyName)) ? dt.Rows[0]["Name"].ToString() : Workflow.DependencyName;

                        dependencyDescription.Text = dt.Rows[0]["Description"].ToString();

                        var includeInStatusCalculation = (bool) dt.Rows[0]["IncludeInStatusCalculation"];
                        ChildObjectAffected.Checked = includeInStatusCalculation;
                        ChildObjectNotAffected.Checked = !includeInStatusCalculation;

                        if (Request["ReturnUrl"] == null)
                        {
                            // it indicate that there is no return URL so most likely directly ended here
                            Workflow.Reset();

                            Workflow.ParentDependencyObject = new DependencyObjectEntityUri()
                            {
                                Uri = dt.Rows[0]["ParentUri"].ToString(),
                                EntityName = dt.Rows[0]["ParentEntityType"].ToString()
                            };

                            Workflow.ChildDependencyObject = new DependencyObjectEntityUri()
                            {
                                Uri = dt.Rows[0]["ChildUri"].ToString(),
                                EntityName = dt.Rows[0]["ChildEntityType"].ToString()
                            };

                            // proceed indicator to last step
                            // just to prevent infinite loop in case of some serios problem, do not allow to move more then 10 steps
                            int counter = 0;
                            while (!Workflow.IsLastStep)
                            {
                                if (counter >= 10)
                                {
                                    break;
                                }

                                Workflow.NextStep();
                                counter++;
                            }

                            UpdateProgressIndicator();
                            imgbBack.Enabled = false; // do not allow to click Back button
                        }

                        // load fields which we will just passthrough (we are not changing those)
                        _automanaged = (bool)dt.Rows[0]["AutoManaged"];
                        _foundAsAutomanaged = (bool) dt.Rows[0]["FoundAsAutoManaged"];
                        _owner = dt.Rows[0]["Owner"].ToString();
                    }
                }
            }

            SetDependencyObjects(isEdit);
        }

        // block saving dependency in case of demo mode
        if (!Profile.AllowNodeManagement && !(this.Page is IBypassAccessLimitation) && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            imgbNext.OnClientClick = "demoAction('Core_Dependencies_SaveDependency', this); return false;";
        }
    }

    private void SetDependencyObjects(bool isEdit)
    {
        var parentObject = Workflow.ParentDependencyObject;
        var childObject = Workflow.ChildDependencyObject;

        DataTable parentObjectTable = null;
        DataTable childObjectTable = null;
        using (var dal = new DependenciesDAL())
        {
            bool allowInterfaces = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");

            parentObjectTable = dal.GetObjectFromUri(parentObject.EntityName, parentObject.Uri);
            childObjectTable = dal.GetObjectFromUri(childObject.EntityName, childObject.Uri);
        }
        if (!isEdit || string.IsNullOrEmpty(dependencyName.Text))
        {
            dependencyName.Text = (string.IsNullOrEmpty(Workflow.DependencyName)) ?
                string.Format(Resources.CoreWebContent.WEBCODE_VB0_207, (parentObjectTable != null && parentObjectTable.Rows.Count > 0) ? parentObjectTable.Rows[0]["Name"].ToString() : string.Empty) :
                Workflow.DependencyName;
        }

        int depStatus;
        int itemId;

        if (parentObject != null)
        {
            depStatus = (parentObjectTable != null && parentObjectTable.Rows.Count > 0) ? Convert.ToInt32(parentObjectTable.Rows[0]["Status"]) : 0;
            itemId = (parentObjectTable != null && parentObjectTable.Rows.Count > 0) ? Convert.ToInt32(parentObjectTable.Rows[0]["ID"]) : 0;
            ParentStatusIcon.ImageUrl = string.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small&id={2}", parentObject.EntityName, depStatus, itemId);
            ParentObjectName.Text = (parentObjectTable != null && parentObjectTable.Rows.Count > 0) ? HttpUtility.HtmlEncode(parentObjectTable.Rows[0]["Name"].ToString()) : string.Empty;
        }

        if (childObject != null)
        {
            depStatus = (childObjectTable != null && childObjectTable.Rows.Count > 0) ? Convert.ToInt32(childObjectTable.Rows[0]["Status"]) : 0;
            itemId = (childObjectTable != null && childObjectTable.Rows.Count > 0) ? Convert.ToInt32(childObjectTable.Rows[0]["ID"]) : 0;
            ChildStatusIcon.ImageUrl = string.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small&id={2}", childObject.EntityName, depStatus, itemId);
            ChildObjectName.Text = (childObjectTable != null && childObjectTable.Rows.Count > 0) ? HttpUtility.HtmlEncode(childObjectTable.Rows[0]["Name"].ToString()) : string.Empty;
        }

        using (var proxy = _blProxyCreator.Create(ex => _log.Error(ex)))
            try
            {
	            var request = new PageableActiveAlertRequest();

	            request.FilterStatement =
		            string.Format("(AcknowledgedBy IS NULL OR AcknowledgedBy = '') AND TriggeringObjectEntityUri = '{0}'",
			            childObject.Uri);
				request.LoadTriggeringObjectEntityUriForLegacyAlerts = true;
				request.SecondaryFilters = new List<ActiveAlertFilter>();
				request.LimitationIDs = new int[] { WebLimitation.GetCurrentViewLimitationID() };
				request.OrderByClause = "AlertName";
	            request.StartRow = 0;
	            request.PageSize = 100;
				_alertsOnChild = proxy.GetPageableActiveAlerts(request);
				repAlertsOnChild.DataSource = _alertsOnChild.ActiveAlerts;
                repAlertsOnChild.DataBind();
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("AlertsOnChild: Error during getting advanced alerts - {0}", ex.ToString());
                _alertsOnChild = null;
            }
    }

    protected ActiveAlertPage AlertsOnChild
    {
        get
        {
            return _alertsOnChild;
        }
    }

    protected int AlertsOnChildCount
    {
        get
        {
	        if (_alertsOnChild != null)
		        return _alertsOnChild.TotalRow;
            return 0;
        }
    }

    // temp workaround to get entity type from uri
    // note: when you touch this, also touch Dependent.aspx.cs
    private string GetSWISEntityTypeByUri(string uri)
    {
        string entityType = UriHelper.GetUriType(uri);
        if (entityType.StartsWith("interfaces", StringComparison.OrdinalIgnoreCase))
            return string.Format("Orion.NPM.{0}", entityType);
        else if (entityType.StartsWith("application", StringComparison.OrdinalIgnoreCase))
            return string.Format("Orion.APM.{0}", entityType);
        return entityType;
    }

    protected override bool Next()
    {
        var parentUri = Workflow.ParentDependencyObject.Uri;
        var childUri = Workflow.ChildDependencyObject.Uri;
        Workflow.DependencyName = dependencyName.Text;

        Dependency dependency = new Dependency
        {
            AutoManaged = _automanaged,
            FoundAsAutoManaged = _foundAsAutomanaged,
            Owner = _owner,
            ChildUri = childUri,
            ParentUri = parentUri,
            Name = dependencyName.Text,
            Description = dependencyDescription.Text,
            IncludeInStatusCalculation = ChildObjectAffected.Checked,
            LastUpdateUTC = DateTime.Now.ToUniversalTime(),
            Id = _dependencyId
        };

        using (var proxy = new DependencyProxy(ex => _log.Error(ex)))
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                // don't allow user to change dependencies
            }
            else
            {
                proxy.UpdateDependency(dependency);
            }

            _dependencyId = 0;
        }

        return true;
    }

    protected void ValidateDependency(object source, ServerValidateEventArgs args)
    {

        if (string.IsNullOrEmpty(args.Value))
        {
            DependencyValidator.ErrorMessage = Resources.CoreWebContent.WEBDATA_VB0_518;
            args.IsValid = false;
            return;
        }

        int dependencyId = 0;
        if (Request["DependencyID"] != null) int.TryParse(Request["DependencyID"], out dependencyId);
        
        using (var dal = new DependenciesDAL())
        {
            var duplicates = dal.GetDependencyDuplicates(Workflow.ParentDependencyObject.Uri, Workflow.ChildDependencyObject.Uri, dependencyName.Text.Replace("'", "''"), dependencyId);
            if (duplicates.Rows.Count > 0)
            {
                foreach (DataRow row in duplicates.Rows)
                {
                    if (row["Name"].ToString().Equals(dependencyName.Text, StringComparison.OrdinalIgnoreCase))
                    {
                        DependencyValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_306;
                        args.IsValid = false;
                        break;
                    }
                }
            }
        }

    }

    protected override bool Back()
    {
        Workflow.DependencyName = dependencyName.Text;
        return true;
    }
}