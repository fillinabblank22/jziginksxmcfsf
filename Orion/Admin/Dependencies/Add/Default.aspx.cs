using System;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Dependencies_Add_Default : DependencyWizardBase
{

    public string ReturnUrl
    {
        get
        {
            return string.Format("?returnUrl={0}", UrlHelper.ToSafeUrlParameter(string.Format("{0}{1}", Request.AppRelativeCurrentExecutionFilePath, 
                Request["DependencyID"] != null ? "?DependencyID=" + Request["DependencyID"] : string.Empty)));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Request["DependencyID"] != null ? Resources.CoreWebContent.WEBCODE_VB0_297 : Resources.CoreWebContent.WEBDATA_VB0_514;
        if (Request["DependencyID"] == null && Request["ReturnUrl"] == null)
        {
            Workflow.Reset();
            UpdateProgressIndicator();
        }

        if (!IsPostBack)
        {
            if (Workflow.ParentDependencyObject != null)
            {
                Grid.SelectedObject = Workflow.ParentDependencyObject.Uri;
                if (Workflow.ParentData.Count > 0)
                {
                    Grid.SetGridData(Workflow.ParentData);
                }
                return;
            }

            int dependencyId;
            if (Request["DependencyID"] != null && int.TryParse(Request["DependencyID"], out dependencyId))
            {
                using (var dal = new DependenciesDAL())
                {
                    var dt = dal.GetDependency(dependencyId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        Grid.SelectedObject = dt.Rows[0]["ParentUri"].ToString();
                    }
                }
            }
        }
    }

    protected void OnObjectChanged()
    {
        var selObj = Grid.SelectedObject;
        DependencyObjectEntityUri parentDependencyObject = new DependencyObjectEntityUri();
        parentDependencyObject.Uri = selObj;
        parentDependencyObject.EntityName = Grid.EntityNameValue;
        Workflow.ParentDependencyObject = parentDependencyObject;
        Workflow.ParentData = Grid.GetGridData();
    }

    protected override bool Next()
    {
        if (!string.IsNullOrEmpty(Grid.SelectedObject))
        {
            DependencyObjectEntityUri parentDependencyObject = new DependencyObjectEntityUri();
            parentDependencyObject.Uri = Grid.SelectedObject;
            parentDependencyObject.EntityName = Grid.EntityNameValue;
            Workflow.ParentDependencyObject = parentDependencyObject;
        }

        if (Workflow.ParentDependencyObject == null)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "myalert",String.Format("alert('{0}');",Resources.CoreWebContent.WEBCODE_VB0_298), true);
            return false;
        }

        if (Workflow.ChildDependencyObject != null && Workflow.ParentDependencyObject.Uri.Equals(Workflow.ChildDependencyObject.Uri))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "myalert", String.Format("alert(\"{0}\");",Resources.CoreWebContent.WEBCODE_VB0_300), true);
            Grid.SelectedObject = null;
            Workflow.ParentDependencyObject = null;
            return false;
        }

        if (Workflow.ChildDependencyObject != null && UriHelper.GetUriType(Workflow.ChildDependencyObject.Uri).Equals("Orion.Groups", StringComparison.OrdinalIgnoreCase))
        {
            using (var dal = new ContainersDAL())
            {
                int groupId = UriHelper.GetId(Workflow.ChildDependencyObject.Uri);
                if (groupId > 0 && DependencyCache.IsGroupMember(dal, groupId, groupId, Workflow.ParentDependencyObject.Uri))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", String.Format("alert(\"{0}\");",Resources.CoreWebContent.WEBCODE_VB0_299), true);
                    Grid.SelectedObject = null;
                    Workflow.ParentDependencyObject = null;
                    return false;
                }
            }
        }

        if (Workflow.ChildDependencyObject != null && UriHelper.GetUriType(Workflow.ParentDependencyObject.Uri).Equals("Orion.Groups", StringComparison.OrdinalIgnoreCase))
        {
            using (var dal = new ContainersDAL())
            {
                int groupId = UriHelper.GetId(Workflow.ParentDependencyObject.Uri);
                if (groupId > 0 && DependencyCache.IsGroupMember(dal, groupId, groupId, Workflow.ChildDependencyObject.Uri))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", String.Format("alert(\"{0}\");",Resources.CoreWebContent.WEBCODE_VB0_301), true);
                    Grid.SelectedObject = null;
                    Workflow.ParentDependencyObject = null;
                    return false;
                }
            }
        }
        return true;
    }
}
