<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_514 %>" Language="C#" MasterPageFile="~/Orion/Admin/Dependencies/Add/DependencyAddWizard.master"
    AutoEventWireup="true" CodeFile="Review.aspx.cs" Inherits="Orion_Admin_Dependencies_Add_Review" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <script type="text/javascript">
        function collapseAlertsOnChild(elem) { 
            var div = document.getElementById('AlertsOnChildDiv');
            if (div.style.display == "none") {
                div.style.display = "block";
                elem.src = "/Orion/images/Button.Collapse.gif";
            }
            else {
                div.style.display = "none";
                elem.src = "/Orion/images/Button.Expand.gif";
            }
        }
    </script>
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_520) %></h1>
    <br />
    <div style="width: 1006px;">
        <table id="dependencyInfo" style="padding: 10px;" width="100%" cellpadding="0" cellspacing="0">
            <tr style="background-color: #EBF3FD;">
                <td style="width: 15%; padding: 10px;" automation="dependencyNameLabel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_517) %>
                </td>
                <td style="width: 85%; padding: 10px;" automation="dependencyNameValue">
                    <asp:TextBox ID="dependencyName" runat="server" Width="200px" />
                    <asp:CustomValidator ID="DependencyValidator" runat="server" ControlToValidate="dependencyName" OnServerValidate="ValidateDependency" ErrorMessage="" ValidateEmptyText="True" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td style="width: 15%; padding: 10px;" automation="dependencyDescriptionLavel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_572) %>
                </td>
                <td style="width: 85%; padding: 10px;" automation="dependencyDescriptionValue">
                    <asp:TextBox ID="dependencyDescription" runat="server" Width="100%" TextMode="MultiLine" Rows="3" />
                </td>
            </tr>            
            <tr style="background-color: #EBF3FD;">
                <td style="width: 15%; padding: 10px;" automation="parentObjectLabel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_521) %>
                </td>
                <td style="width: 85%; padding: 10px;" automation="parentObjectValue">
                    <asp:Image ID="ParentStatusIcon" href="#" runat="server" class="StatusIcon" />&nbsp;
                    <asp:Label ID="ParentObjectName" runat="server" Style="vertical-align: top;" />
                </td>
            </tr>
            <tr>
                <td style="width: 15%; padding: 10px;" automation="childObjectLabel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_522) %>
                </td>
                <td style="width: 85%; padding: 10px;" automation="childObjectValue">
                    <asp:Image ID="ChildStatusIcon" href="#" runat="server" class="StatusIcon" />&nbsp;
                    <asp:Label ID="ChildObjectName" runat="server" Style="vertical-align: top;" />
                </td>
            </tr>
            <tr style="background-color: #EBF3FD;">
                <td style="width: 15%; padding: 10px; word-wrap: break-word;" automation="parentGoesDownLabel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_573) %>
                </td>
                <td style="width: 85%; padding: 10px;" automation="parentGoesDownValue">
                    <div style="padding: 5px;" automation="parentGoesDownAffected">
                        <asp:RadioButton runat="server" ID="ChildObjectAffected" GroupName="ChildStatus" Checked="True"/>
                        <asp:Label runat="server" AssociatedControlID="ChildObjectAffected" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VB0_574 %>"/>
                    </div>
                    <div style="padding: 5px;" automation="parentGoesDownNotAffected">
                        <asp:RadioButton runat="server" ID="ChildObjectNotAffected" GroupName="ChildStatus"/>
                        <asp:Label runat="server" AssociatedControlID="ChildObjectNotAffected" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VB0_575 %>"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 15%; padding: 10px;" automation="affectsOnChildLabel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_523) %>
                </td>
                <td style="width: 85%; padding: 10px;" automation="affectsOnChildValue">
                    <img src="/Orion/images/Button.Expand.gif" style="cursor:pointer;" onclick="collapseAlertsOnChild(this);" />
                    <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_VB0_524,AlertsOnChildCount)) %>
                    <div id="AlertsOnChildDiv" style="display:none; margin-left: 15px;">
                        <asp:Repeater runat="server" ID="repAlertsOnChild">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <img src="/NetPerfMon/images/Event-19.gif" />
                                    &nbsp;<%# DefaultSanitizer.SanitizeHtml(Eval("Name").ToString()) %>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;"></td>
                <td style="width: 80%; padding-left: 12px; font-size: 8pt; color: Gray;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_525) %></td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton runat="server" LocalizedText="Back" automation="Back" DisplayType="Secondary" ID="imgbBack"
                            OnClick="Back_Click" CausesValidation="false" />
                        <orion:LocalizableButton LocalizedText="Submit" automation="Submit" DisplayType="Primary" runat="server"
                            ID="imgbNext" OnClick="Next_Click" />
                        &nbsp;
                        <orion:LocalizableButton LocalizedText="Cancel" automation="Cancel" DisplayType="Secondary" runat="server" ID="imgbCancel"
                            OnClick="Cancel_Click" CausesValidation="false" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
