<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_514 %>" Language="C#" MasterPageFile="~/Orion/Admin/Dependencies/Add/DependencyAddWizard.master"
    AutoEventWireup="true" CodeFile="Dependent.aspx.cs" Inherits="Orion_Admin_Dependencies_Add_Dependent" %>

<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>
<%@ Register Src="~/Orion/Controls/NetObjectsGrid.ascx" TagPrefix="orion" TagName="NetObjectsGrid" %>
<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_515) %>
    </h1>
    <p style="padding-bottom:5px;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_516) %>  <br />
    </p>
    <orion:HelpHint  runat="server" >
            <HelpContent>
                <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_VB0_526 , String.Format("<a class=\"coloredLink\" href=\"/Orion/Admin/Containers/Add/Default.aspx{0}\" id=\"link\">", ReturnUrl), "</a>")) %>
            </HelpContent>
    </orion:HelpHint>
    <br />
    <div style="width: 1076px;">
        <table id="dependencyInfo" style="background-color: #EBF3FD; padding: 10px;" width="100%">
            <tr>
                <td style="width: 20%;white-space:nowrap;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_517) %>
                </td>
                <td style="width: 80%;">
                    <asp:TextBox ID="dependencyName" runat="server" Width="200px" />
                    <asp:RequiredFieldValidator ID="validator1" ControlToValidate="dependencyName" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_518 %>"
                        runat="server" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td style="width: 20%;white-space:nowrap;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_519) %>
                </td>
                <td style="width: 80%;">
                    <asp:Image ID="StatusIcon" href="#" runat="server" class="StatusIcon" />&nbsp;
                    <asp:Label ID="Label" runat="server" Style="vertical-align: top;" />
                </td>
            </tr>
        </table>
        <asp:CustomValidator ID="DependencyValidator" runat="server" ControlToValidate="dependencyName" OnServerValidate="ValidateDependency" ErrorMessage="" EnableClientScript="false" Display="Dynamic" />
        <br />
        <orion:NetObjectsGrid ID="Grid" runat="server" OnObjectChanged="OnObjectChanged"
            IsParentDependency="false" />
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" LocalizedText="Back" DisplayType="Secondary" ID="imgbBack"
                OnClick="Back_Click" CausesValidation="false" />
            <orion:LocalizableButton DisplayType="Primary" LocalizedText="Next" runat="server"
                ID="imgbNext" OnClick="Next_Click" />
            &nbsp;
            <orion:LocalizableButton DisplayType="Secondary" LocalizedText="Cancel" runat="server" ID="imgbCancel"
                OnClick="Cancel_Click" CausesValidation="false" />
        </div>
    </div>
</asp:Content>
