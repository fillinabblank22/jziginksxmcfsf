<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_514 %>" Language="C#" MasterPageFile="~/Orion/Admin/Dependencies/Add/DependencyAddWizard.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Admin_Dependencies_Add_Default" %>

<%@ Register Src="~/Orion/Controls/NetObjectsGrid.ascx" TagPrefix="orion" TagName="NetObjectsGrid" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>
<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_512) %></h1>
    <p style="padding-bottom:5px;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_513) %><br />
    </p>
        <orion:HelpHint  runat="server" >
            <HelpContent>
                <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_296,String.Format("<a class=\"coloredLink\" href=\"../../Containers/Add/Default.aspx{0}\" id=\"link\">",ReturnUrl), "</a>")) %>
            </HelpContent>
        </orion:HelpHint>
    <div style="width: 1076px;padding-top:20px;">
        <orion:NetObjectsGrid ID="Grid" runat="server" OnObjectChanged="OnObjectChanged"
            IsParentDependency="true" />
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary" runat="server"
                ID="imgbNext" OnClick="Next_Click" />
            &nbsp;
            <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" ID="imgbCancel"
                OnClick="Cancel_Click" CausesValidation="false" />
        </div>
    </div>
</asp:Content>
