<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Orion_Admin_ResetPassword" MasterPageFile="~/Orion/OrionMasterPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_552 %>" %>

<asp:Content runat="server" ContentPlaceHolderID="ResourcesStyleSheetPlaceHolder">
    <orion:Include runat="server" File="Admin.css" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="adminContent">
        <h1><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_TM0_79, this.User.Identity.Name)) %></h1>
        
        <p runat="server" id="paraPasswordBoxes">
            <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_549) %></strong><br />
            <asp:TextBox runat="server" ID="txtOldPassword" TextMode="password" Rows="1" Columns="25" />
            <br /><br />
            <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_184) %></strong><br />
            <asp:TextBox runat="server" ID="txtPassword" TextMode="password" Rows="1" Columns="25" />
            <br /><br />
            <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_182) %></strong><br />
            <asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="password" Rows="1" Columns="25" /><br />
        </p>
        
        <p runat="server" id="paraRestrictAccount">
            <br /><br />
            <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_550) %></strong>
            <br />
        </p>

        <asp:PlaceHolder runat="server" ID="phSuccessMessage" Visible="false"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_183) %></asp:PlaceHolder>
        <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID ="btnChangePassword" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TM0_181 %>" DisplayType="Primary" OnClick="ChangePasswordClick" Enabled="<%$ HtmlEncodedCode: !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin%>" /><br />
        <orion:LocalizableButton ID="btnContinue" runat="server" Visible="false" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TM0_186 %>" OnClick="ContinueClick" />
        </div>
        <div id="statusMessage">
            <asp:PlaceHolder runat="server" ID="phErrorMessage" />
            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="false" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_551 %>"
                OnServerValidate="ValidateOldPassword" /><br />
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtPassword" ControlToCompare="txtConfirmPassword"
                                  Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TM0_185 %>" 
                                  EnableClientScript="false" />
        </div>
    </div>
</asp:Content>