using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Common;
using System.Globalization;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.Core.Common;

public partial class Orion_Admin_HistoricalStatistics : System.Web.UI.Page
{
    // add new HTML row from cells
    private HtmlTableRow NewRow(params HtmlTableCell[] cells)
    {
        HtmlTableRow r = new HtmlTableRow();

        foreach (HtmlTableCell cell in cells)
            r.Cells.Add(cell);

        return r;
    }

    // creates new cell with text
    private HtmlTableCell NewCell(string text)
    {
        HtmlTableCell cell = new HtmlTableCell();
        cell.InnerHtml = text;
        return cell;
    }

    // creates new cell with cell width
    private HtmlTableCell NewCell(string text, int width)
    {
        HtmlTableCell cell = NewCell(text);
        cell.Style["Width"] = String.Format("{0}px", width);
        return cell;
    }

    // creates new cell with cell width and text algin
    private HtmlTableCell NewCell(string text, int width, string textAlgin)
    {
        HtmlTableCell cell = NewCell(text, width);
        cell.Style["text-align"] = textAlgin;
        return cell;
    }

    // creates new cell with cell width and text algin
    private HtmlTableCell NewCell(string text, string URL)
    {
        Literal txt = new Literal();
        txt.Text = text;

        HtmlAnchor link = new HtmlAnchor();
        link.HRef = URL;
        link.Controls.Add(txt);

        HtmlTableCell cell = new HtmlTableCell();
        cell.Controls.Add(link);

        return cell;
    }

    // days formating
    private string FormatDays(object dateBegin, object dateEnd)
    {
        DateTime begin = new DateTime();
        DateTime end = new DateTime();

        try
        {
            begin = (DateTime)dateBegin;
            end = (DateTime)dateEnd;
        }
        catch
        {
            return Resources.CoreWebContent.WEBCODE_AK0_6;
        }

        var daysFromBegin = (new TimeSpan(DateTime.Now.Ticks - begin.Ticks)).Days;
        var daysFromEnd = (new TimeSpan(DateTime.Now.Ticks - end.Ticks)).Days;

        if (daysFromEnd == 1)
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_TM0_103, daysFromBegin, daysFromEnd);
        }
        else
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_TM0_104, daysFromBegin, daysFromEnd);
        }
    }

    // adds not node row
    private void AddRow(DataRow row, HtmlTable htmlTable)
    {
        var typeOfStatistic = String.Empty;
        int archive = 0;

        if (row["Archive"] is System.Byte)
            archive = (System.Byte) row["Archive"];
        else
            archive = (System.Int32) row["Archive"];

        switch (archive)
        {
            case 0: typeOfStatistic = Resources.CoreWebContent.WEBCODE_TM0_105; break;
            case 1: typeOfStatistic = Resources.CoreWebContent.WEBCODE_TM0_106; break;
            case 2: typeOfStatistic = Resources.CoreWebContent.WEBCODE_TM0_107; break;
        }
        
        htmlTable.Rows.Add(NewRow(
                                    NewCell(String.Format(typeOfStatistic, row["Total"]), 210),
                                    NewCell(String.Format(Resources.CoreWebContent.WEBCODE_TM0_110,
                                                          Utils.FormatDateTime((DateTime)row["Oldest"], true),
                                                          Utils.FormatDateTime((DateTime)row["Newest"], true)),
                                           
                                            330),
                                    NewCell("&nbsp;", 20),
                                    NewCell(FormatDays(row["Newest"], row["Oldest"]))
                                   )
                           );
    }

    // adds node row
    private void AddRow(DataRow row, HtmlTable htmlTable, bool byNode)
    {
        if (!byNode)
        {
            AddRow(row, htmlTable);
            return;
        }

        var rec = Resources.CoreWebContent.WEBCODE_TM0_108;

        try
        {
            if ((int)row["Total"] == 1) rec = Resources.CoreWebContent.WEBCODE_TM0_109;
        }
        catch { }

        htmlTable.Rows.Add(NewRow(
                                    NewCell(row["Caption"].ToString(), String.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", row["NodeId"])),
                                    NewCell(String.Format(rec, row["Total"])),
                                    NewCell(String.Format(Resources.CoreWebContent.WEBCODE_TM0_110, 
                                                          Utils.FormatDateTime((DateTime)row["Oldest"], true), 
                                                          Utils.FormatDateTime((DateTime)row["Newest"], true))),
                                    NewCell("&nbsp;", 20),
                                    NewCell(FormatDays(row["Newest"], row["Oldest"]))
                                   )
                           );
    }

    // adds rows in table to a HTMLtable
    private void DoTable(WebDAL dal, string tableName, HtmlTable htmlTable, bool byNode, bool archiveColumnExist = false)
    {
        DataTable table = dal.GetStatisticRange(tableName, byNode, archiveColumnExist);
        
        foreach (DataRow r in table.Rows)
            AddRow(r, htmlTable, byNode);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        using (WebDAL dal = new WebDAL())
        {
            bool byNode = false;

            if (Request.QueryString["ByNode"] != null && Request.QueryString["ByNode"].Trim().ToUpperInvariant() == Boolean.TrueString.ToUpperInvariant())
                byNode = true;
            
            DoTable(dal, "CiscoBuffers", this.CiscoBuffers, byNode);
            DoTable(dal, "CPULoad", this.CPULoad, byNode);
            DoTable(dal, "ResponseTime", this.ResponseTime, byNode);
                        
            if(PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces"))
            {
                DoTable(dal, "NPM.InterfaceTraffic", this.InterfaceTraffic, byNode, true);
                DoTable(dal, "NPM.InterfaceErrors", this.InterfaceErrors, byNode, true);
            }
        }
    }
}
