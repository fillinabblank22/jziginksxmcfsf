<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"  
        Title="Orion SWIS Query" EnableViewState="false" %>
<%-- disable ViewState on this page, we really don't need to maintain anything across postbacks --%>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.2.0"/>

<style type="text/css">
    #contents
    {
        width:80%;
    }
    
    h1 
    {
        font-size: 12pt;
    }

    #textareaDiv textarea
    {
        width: 100%;
        border: 1px solid #8FB3D4;
        font-family: Monaco, 'Courier New', Courier, monospace;
        font-size: 11px;
        padding: 2px;
        width: 100%;        
    }
    #textareaDiv
    {
        padding: 10px 0px;
    }
    
    #results 
    {
        padding-top: 20px;
    }
    
    .x-grid3-row-alt 
    {
        background-color: #f8f8f8;
    }
    
    #entities {
        width: 200px;
    }
    
    #entityList {
        margin-top: 10px; 
    }

</style>

<script type="text/javascript">
DoQuery = function (query, succeeded) {
    Information.Query(query, function (result) {
        succeeded(result);
        $('#lastErrorMessage').hide();
    }, function (error) {
        if (error.get_message() == "Authentication failed.") {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('Your session expired.');
            window.location.reload();
        } else {
            $('#lastErrorMessage').text("Server error: " + error.get_message()).show();
            //			$("#originalQuery").text(query);
            //			$("#test").text(error.get_message());
            //			$("#stackTrace").text(error.get_stackTrace());
        }
    });
};

DoQueryWithServer = function (query, server, succeeded) {
    Information.QueryWithServerDetailedError(query, server, function (result) {
        succeeded(result);
    }, function (error) {
        if (error.get_message() == "Authentication failed.") {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('Your session expired.');
            window.location.reload();
        } else {
            $('#lastErrorMessage').text("Server error: " + error.get_message()).show();
            //			$("#originalQuery").text(query);
            //			$("#test").text(error.get_message());
            //			$("#stackTrace").text(error.get_stackTrace());
        }
    });
};

FormatException = function(exception) {
    var errorMsg = exception.LocalizedMessage;
    if (exception.InnerExceptionMessage != null && exception.InnerException != null) {
        errorMsg = errorMsg.concat("\n<b>", exception.InnerExceptionMessage, "\n</b>",
            exception.InnerException);
    }
    return errorMsg;
};

$().ready(function () {

    $('#executeBtn').click(function () {

        var swql = TextAreaHelper.GetSelectedText('queryEdit');
        var server = $('#engines').val();

        DoQueryWithServer(swql, server, function (result) {
            if (result.Data == null && result.QueryError != null) {
                $('#resultset').empty();
                $('#lastErrorMessage').html("<pre>Server error: " + FormatException(result.QueryError) + "</pre>").show();
                return;
            }

            var store = new Ext.data.SimpleStore({
                data: result.Data.Rows,
                fields: result.Data.Columns
            });

            var columns = [];
            for (var x = 0; x < result.Data.Columns.length; ++x) {
                columns.push({
                    header: result.Data.Columns[x],
                    width: 160,
                    sortable: true,
                    dataIndex: result.Data.Columns[x]
                });
            }

            var grid = new Ext.grid.GridPanel({
                store: store,
                columns: columns,
                stripeRows: true,
                autoHeight: true,
                viewConfig: {
                //forceFit: true
                //autoFill: true
                },

                height: 350
            });

            $('#lastErrorMessage').hide();
            $('#resultset').empty();

            grid.setAutoScroll(true);
            grid.render('resultset');
        });
    });

    var entitiesManager = new EntitiesManager();
    entitiesManager.Init();

    var engines = new Engines();
    engines.Init();
});

var Engines = function () {

    this.Init = function () {
        this.LoadEngines();
    };

    this.LoadEngines = function () {
        var selectServersSwql = "SELECT ServerName FROM Orion.Engines";

        DoQuery(selectServersSwql, function (result) {

            var $selectWithEngines = $('#engines');
            $selectWithEngines.html('');

            for (var i = 0, end = result.Rows.length; i < end; i++) {
                var entityRow = result.Rows[i];

                var entityOption = $("<option />");
                entityOption.attr("value", entityRow[0]);
                entityOption.text(entityRow[0]);

                $selectWithEngines.append(entityOption);
            }
        });
    };
};


var EntitiesManager = function () {
    
    // Public methods
    
    this.Init = function () {
      
        this.LoadAllEntities();
        this.InitGeneratorBtn();
    };

    this.LoadAllEntities = function () {
        var selectAllEntitiesSwql = "SELECT FullName FROM Metadata.Entity WHERE IsAbstract = 'False' ORDER BY FullName";

        DoQuery(selectAllEntitiesSwql, function (result) {

            var $selectWithEntities = $('#entities');
            $selectWithEntities.html('');

            for (var i = 0, end = result.Rows.length; i < end; i++) {
                var entityRow = result.Rows[i];

                var entityOption = $("<option />");
                entityOption.attr("value", entityRow[0]);
                entityOption.text(entityRow[0]);

                $selectWithEntities.append(entityOption);
            }
        });
    };

 
    this.InitGeneratorBtn = function() {
        $('#generateEntitySwqlBtn').click(function () {
            var selectedEntityName = $('#entities').val();
            if (selectedEntityName !== '') {
                listAllColumnsForEntity(selectedEntityName);
            }
        });
    };

    // Private methods
    
    var listAllColumnsForEntity = function (entityName) {
        var selectAllEntityColumnsSwql = "SELECT Name FROM Metadata.Property WHERE EntityName = '" + entityName + "' AND IsNavigable = 'False' ORDER BY Name";

        DoQuery(selectAllEntityColumnsSwql, function (result) {
            if (result.Rows.length > 0) {
                var $queryEdit = $('#queryEdit');

                var swqlForListingColumns = generateColumnsListSwql(entityName, result.Rows);
                var newSwqlQuery = appendQuery($queryEdit.val(), swqlForListingColumns);
                
                $queryEdit.val(newSwqlQuery);
            }
        });
    };

    var generateColumnsListSwql = function (entityName, columns) {
        var swql = "SELECT ";
        swql = swql + columns.join(", ");
        swql = swql + " FROM ";
        swql = swql + entityName;

        return swql;
    };

    var appendQuery = function(currentQuery, queryToAppend) {
        var newQuery;
        currentQuery = $.trim(currentQuery);

        if (currentQuery === '') {
            newQuery = queryToAppend;
        } else {
            newQuery = currentQuery + "\n\n" + queryToAppend;
        }
        
        return newQuery + '\n';
    };
};

var TextAreaHelper = {
    GetSelectedText: function (elementId) {
        var text = $('#' + elementId).val();
        try {
            var selection = TextAreaHelper.GetSelection(elementId);

            if (typeof (selection) !== 'undefined' && selection.text !== '') {
                text = selection.text;
            }
        }
        catch(e) {
            // we dont care about selection, we will use origin text from textarea.
        }
        return text;
    },

    GetSelection: function (elementId) {
        var e = document.getElementById(elementId);

        // Mozilla and DOM 3.0
        if ('selectionStart' in e) {
            var l = e.selectionEnd - e.selectionStart;
            return { start: e.selectionStart, end: e.selectionEnd, length: l, text: e.value.substr(e.selectionStart, l) };
        }
        // IE
        else if (document.selection) {
            e.focus();
            var r = document.selection.createRange();
            var tr = e.createTextRange();
            var tr2 = tr.duplicate();
            tr2.moveToBookmark(r.getBookmark());
            tr.setEndPoint('EndToStart', tr2);
            if (r == null || tr == null) return { start: e.value.length, end: e.value.length, length: 0, text: '' };
            var textPpart = r.text.replace(/[\r\n]/g, '.'); //for some reason IE doesn't always count the \n and \r in the length
            var textWhole = e.value.replace(/[\r\n]/g, '.');
            var theStart = textWhole.indexOf(textPpart, tr.text.length);
            return { start: theStart, end: theStart + textPpart.length, length: textPpart.length, text: r.text };
        }
        // Browser not supported
        else return { start: e.value.length, end: e.value.length, length: 0, text: '' };

    }
};

</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<asp:ScriptManagerProxy ID="ScriptManager1"  runat="server" >
		<Services>
			<asp:ServiceReference path="../Services/Information.asmx" />
		</Services>
	</asp:ScriptManagerProxy>

    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <div id="contents" >
        <div id="entityList">
           <b>Select entity:</b> <select id="entities"></select> 
           <b>Engine:</b> <select id="engines"></select> 
           <input type="button" id="generateEntitySwqlBtn" value=" Generate Select Query " />  
        </div>
        <div id="query">
            <div id="textareaDiv">
                <textarea id="queryEdit" rows="5">SELECT StatusId, StatusName, ShortDescription, RollupType, Ranking, Color, IconPostfix, ChildStatusMap, DefaultIconName FROM Orion.StatusInfo
                </textarea>
            </div>
            <input type="button" id="executeBtn" value=" Execute Query "></input>
        </div>
        <div id="results">
            <div id="lastErrorMessage"></div>
            <div id="resultset"></div>
        </div>
    </div>
    
</asp:Content>

