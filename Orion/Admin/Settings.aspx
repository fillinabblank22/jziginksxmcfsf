<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Orion_Admin_Settings"
    MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_10 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50927.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionCoreAGChangingWebConsoleChartSettings" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <orion:Include ID="Include1" runat="server" File="ajaxupload.css" />
    <orion:Include ID="Include2" runat="server" File="Admin/js/LogoUpload.js" />
    <orion:Include runat="server" File="NodeMNG.css" />
    <script type="text/javascript">
        var extentions = ['png', 'jpeg', 'jpg', 'gif', 'bmp'];

        function getThrobberUploadID() { return '<%=throbberUpload.ClientID %>'; }
        function getFileUploadImage() { return '<%= fileUploadImage.ClientID %>'; }
        function getBtnBrowse() { return '<%= btnBrowse.ClientID %>'; }

        function getThrobberUploadNocID() { return '<%=throbberUploadNoc.ClientID %>'; }
        function getFileUploadNocImage() { return '<%= fileUploadImageNoc.ClientID %>'; }
        function getBtnBrowseNoc() { return '<%= btnBrowseNoc.ClientID %>'; }

        function getFileNameSiteLogoID() { return '<%= logoFileName.ClientID %>'; }
        function getFileNameNocSiteLogoID() { return '<%= nocLogoFileName.ClientID %>'; }

        function getNotificationID() { return '<%= notification.ClientID %>'; }
        function getNotificationNocID() { return '<%= nocNotification.ClientID %>'; }

        function getLogoSectionID() { return '<%= LogoBlock.ClientID %>'; }
        function getnoLogoSectionID() { return '<%= noLogoBlock.ClientID %>'; }

        function getNocLogoSectionID() { return '<%= NocLogoBlock.ClientID %>'; }
        function getnoNocLogoSectionID() { return '<%= noNocLogoBlock.ClientID %>'; }

        function getNocLogoCheckBoxID() { return '<%= nocLogoCb.ClientID %>'; }
        function getLogoCheckBoxID() { return '<%= logoCb.ClientID %>'; }

        function getLogoBlockFromUrlSectionID() { return '<%= LogoBlockFromUrl.ClientID%>'; }
        function getLogoCheckBoxFromUrlID() { return '<%= logoFromUrlCb.ClientID %>'; }
        function getLogoFromUrInputlID() { return '<%= LogoFromUrlInput.ClientID%>'; }

        function getNocLogoBlockFromUrlSectionID() { return '<%= nocLogoBlockFromUrl.ClientID%>'; }
        function getNocLogoCheckBoxFromUrlID() { return '<%= nocLogoFromUrlCb.ClientID %>'; }
        function getNocLogoFromUrInputlID() { return '<%= nocLogoFromUrlInput.ClientID%>'; }
    </script>
    
    <script type="text/javascript">
//<![CDATA[
        var _migration = function (verb, message, callback) {

            var invokeMigration = function (swisVerb) {
                var promise = $.Deferred();
                SW.Core.Services.callController(
                    '/api2/swis/invokeToString',
                    { entity: 'Orion.Resources', verb: swisVerb, parameters: [] },
                    function (result) {
                        var success = /true/gi.test(result);
                        promise.resolve(success);
                    },
                    function (error) {
                        promise.reject(error);
                    });
                return promise;
            };

            var getConfirmButtonConfig = function (afterMigration, failedMigration) {
                return {
                    id: 'confirmMigration',
                    html: SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_MigrateCharts_Dialog_Yes) %>', { type: 'primary', id: 'confirmMigration' }),
                    handler: function (msgbox) {
                        invokeMigration(verb)
                            .done(function (success) {
                                msgbox.dialog('close');
                                afterMigration(success);
                            })
                            .fail(function (error) {
                                msgbox.dialog('close');
                                failedMigration(error);
                            });
                    }
                };
            };

            var rejectButtonConfig = {
                id: 'rejectMigration',
                html: SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_MigrateCharts_Dialog_No) %>', { type: 'secondary', id: 'rejectMigration' }),
                handler: function (msgbox) {
                    msgbox.dialog('close');
                }
            };

            return function () {
                var confirmButtonConfig = getConfirmButtonConfig(function (success) {
                    console.log("Resource migration result: " + success);
                    callback();
                }, function () {
                    console.log("Failed to migrate resources." + error);
                });

                SW.Core.MessageBox.Dialog({
                    title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_MigrateCharts_Dialog_Title) %>',
                    modal: true,
                    html: message,
                    buttons: [ confirmButtonConfig, rejectButtonConfig ]
                });
                return false;
            }
        };

        var showMigrationDialog = _migration(
            'MigrateClassicToModernResources',
            '<br/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_MigrateCharts_Dialog_Text) %><br/><br/><br/>',
            function () {
                $('#<%= promptChartsMigration.ClientID %>').hide();
                $('#<%= promptChartsRollback.ClientID %>').show();
            }
        );

        var showRollbackDialog = _migration(
            'MigrateModernToClassicResources',
            '<br/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_RollbackCharts_Dialog_Text) %><br/><br/><br/>',
            function () {
                $('#<%= promptChartsMigration.ClientID %>').show();
                $('#<%= promptChartsRollback.ClientID %>').hide();
            }
        );
//]]>    
</script>

    <style type="text/css">
        .notification {
            font-family: Arial,Verdana,Helvetica,sans-serif;
            font-size: 8pt;
            font-weight: normal;
        }
        .alignCbForUrl.notification  {
            margin-left: -3px; 
        }
        .x-form-file{ top: 0;height: auto!important;}
        .x-form-file div input{ cursor: pointer;}
        .notificationGray {
            color: gray;
        }
        div.ui-dialog-titlebar { 
            margin: 0 !important;
        }
    </style>
    <table border="0" width="100%">
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td><h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1></td>
                        <td style="text-align: right;">
                            <orion:LocalizableButtonLink runat="server" DisplayType="Primary" NavigateUrl="/sapi/installer/get" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, Engines_DownloadInstallerButtonText %>" />
                        </td>
                    </tr>
                </table>

                <table id="adminContentTable" border="0" cellpadding="3" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_314) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbSessionTimeout" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="tbSessionTimeout" Operator="DataTypeCheck"
                                ID="CompareValidator1" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbSessionTimeout" ID="RequiredFieldValidator1"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp;
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                            <asp:RangeValidator ControlToValidate="tbSessionTimeout" ID="SessionRangeValidator"
                                runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_315 %>"
                                Display="Dynamic" MinimumValue="1" MaximumValue="1000" Type="Integer" />
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_316) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_317) %>
                        </td>
                        <td class="Property">
                            <asp:DropDownList runat="server" ID="ddlWindowsAccountLogin">
                                <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VB0_318 %>" Value="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VB0_319 %>" Value="False"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_320) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_321) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbAutoRefresh" Columns="5" MaxLength="4"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="tbAutoRefresh" Operator="DataTypeCheck"
                                ID="CompareValidator2" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbAutoRefresh" ID="RequiredFieldValidator2"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp;
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_322) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                           <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_RV0_5) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbWidgetRefresherTime" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="tbWidgetRefresherTime" Operator="DataTypeCheck"
                                ID="CompareValidator3" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbWidgetRefresherTime" ID="RequiredFieldValidator4"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp;
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                            <asp:RangeValidator ControlToValidate="tbWidgetRefresherTime" ID="RangeValidator3"
                                runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_RV0_6 %>"
                                Display="Dynamic" MinimumValue="1" MaximumValue="86400" Type="Integer" />
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_RV0_7) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <asp:CheckBox runat="server" ID="logoCb" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_323 %>" />
                        </td>
                        <td class="Property" style="text-align: left; vertical-align: middle;">
                        <div runat="server" id = "LogoBlock" style="display: none;" class="notification">
                            <asp:Label runat="server" ID="logoFileName" EnableViewState="false" />
                            <div id="browseWrap" class="x-form-field-wrap x-form-file-wrap" style="display: inline; padding-left: 3px; overflow: hidden;">
                                <ajaxtoolkit:AsyncFileUpload ID="fileUploadImage" runat="server" UploaderStyle="Traditional"
                                    ThrobberID="throbberUpload" OnClientUploadStarted="checkExtension" OnClientUploadComplete="uploadComplete"
                                    OnUploadedComplete="AsyncFileUpload_UploadeComplete" CssClass="x-form-file" />
                                <orion:LocalizableButton CssClass="x-form-file-btn" runat="server" ID="btnBrowse"
                                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_41 %>" OnClientClick="return false;"
                                    DisplayType="Small" />
                                <span id="fileFormatError" class="upload-error" style="display: none;">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_269) %>
                                </span>
                                <asp:Label runat="server" ID="throbberUpload" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_267 %>" CssClass="notification"
                                    Style="display: none;" />
                            </div>
                            <div class="notification">
                                <asp:CustomValidator ID="LogoValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_TM0_338 %>"
                                    Display="Dynamic" OnServerValidate="CustomLogo_ServerValidate" EnableClientScript="False"></asp:CustomValidator>
                            </div>
                            <div class="notification notificationGray">
                                <asp:Label runat="server" ID="notification" Text="* Logo will be updated after clicking 'Submit' button..."
                                    EnableViewState="false" Style="display: none;" />
                            </div>
                            </div>
                            <div runat="server" id = "noLogoBlock" style="display: none;" class="notification notificationGray">
                                No logo will be displayed
                            </div>
                            <div runat="server" id="LogoBlockFromUrl" style="display: none">
                            <div class="alignCbForUrl" style="margin-left: -3px">
                                <asp:CheckBox runat="server" Checked="False" ID="logoFromUrlCb" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_AY0_98 %>" />
                            </div>
                            <div runat="server" id="LogoFromUrlInput" style="display: none">
                                <asp:TextBox runat="server" ID="logoPath" Width="290"></asp:TextBox>
                            </div>
                            <div>
                                <asp:CustomValidator ID="LogoFromUrlValidator" runat="server" ErrorMessage="*"
                                    Display="Dynamic" OnServerValidate="CustomLogoFromUrl_ServerValidate" EnableClientScript="False"></asp:CustomValidator>
                            </div>
                            </div>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_VB0_324, "/NetPerfMon/images/SolarWinds.Logo.png")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <asp:CheckBox runat="server" ID="nocLogoCb" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_50 %>" />
                        </td>
                        <td class="Property" style="text-align: left; vertical-align: middle;">
                        <div runat="server" id = "NocLogoBlock" style="display: none;" class="notification">
                            <asp:Label runat="server" ID="nocLogoFileName" EnableViewState="false" />
                            <div id="browseWrapNoc" class="x-form-field-wrap x-form-file-wrap" style="display: inline; padding-left: 3px; overflow: hidden;">
                                <ajaxtoolkit:AsyncFileUpload ID="fileUploadImageNoc" runat="server" UploaderStyle="Traditional"
                                    ThrobberID="throbberUploadNoc" OnClientUploadStarted="checkExtensionNoc" OnClientUploadComplete="uploadCompleteNoc"
                                    OnUploadedComplete="AsyncFileUpload_UploadeCompleteNoc" CssClass="x-form-file" />
                                <orion:LocalizableButton CssClass="x-form-file-btn" runat="server" ID="btnBrowseNoc"
                                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_41 %>" OnClientClick="return false;"
                                    DisplayType="Small" />
                                <span id="fileFormatErrorNoc" class="upload-error" style="display: none;">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_269) %>
                                </span>
                                <asp:Label runat="server" ID="throbberUploadNoc" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_267 %>"
                                    Style="display: none;" CssClass="notification" />
                            </div>
                            <div class="notification">
                            <asp:CustomValidator ID="NocLogoValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_TM0_338 %>"
                                Display="Dynamic" OnServerValidate="CustomNocLogo_ServerValidate" EnableClientScript="False"></asp:CustomValidator>
                            </div>
                            <div class="notification notificationGray">
                            <asp:Label runat="server" ID="nocNotification" Text="* Logo will be updated after clicking 'Submit' button..."
                                EnableViewState="false" Style="display: none;" />
                            </div>
                            </div>
                            <div runat="server" id = "noNocLogoBlock" style="display: none;" class="notification notificationGray">
                                No logo will be displayed
                            </div>
                            <div runat="server" id="nocLogoBlockFromUrl" style="display: none">
                            <div class="alignCbForUrl" style="margin-left: -3px">
                                <asp:CheckBox runat="server" Checked="False" ID="nocLogoFromUrlCb" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_AY0_98 %>" />
                            </div>
                            <div runat="server" id="nocLogoFromUrlInput" style="display: none">
                                <asp:TextBox runat="server" ID="nocLogoPath" Width="290"></asp:TextBox>
                            </div>
                            <div>
                                <asp:CustomValidator ID="nocLogoFromUrlValidator" runat="server" ErrorMessage="*"
                                    Display="Dynamic" OnServerValidate="CustomNocLogoFromUrl_ServerValidate" EnableClientScript="False"></asp:CustomValidator>
                            </div>
                        </div>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_VL0_51, "/Orion/images/SW_NOClogo.png")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_325) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbSiteLoginText" Columns="30" Rows="4" TextMode="MultiLine"></asp:TextBox>
                            <%-- MultiLine asp:TextBoxes ignore the MaxLength property, so use a regex validator instead --%>
                            <asp:RegularExpressionValidator runat="server" ID="SiteLoginTextLengthValidator"
                                ControlToValidate="tbSiteLoginText" ValidationExpression="^[\s\S]{0,3500}$" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_326 %>"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_327) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_158) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbOrionWebServerFQDN" Columns="45"></asp:TextBox>
                        </td>
                        <td class="Property"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_157) %></td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_HSTS_PropertyLabel) %>
                        </td>
                        <td class="Property">
                            <asp:CheckBox runat="server" ID="chbEnableHstsPolicy" />
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_HSTS_HintText) %>
                        </td>
                    </tr>

                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_328) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbHelpServer" Columns="45"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="tbHelpServer" ID="RequiredFieldValidator8"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_329) %>
                        </td>
                    </tr>
                    <% if (!_hiddenWebConsoleSettings.Contains("StatusRollupModeSection"))
                       { %>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_352) %>
                        </td>
                        <td class="Property">
                            <asp:DropDownList runat="server" ID="ddlRollupWorstStatus">
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_330 %>" Value="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_331 %>" Value="False"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_332) %>
                        </td>
                    </tr>
                    <% } %>

                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_336) %>
                        </td>
                        <td class="Property">
                            <asp:DropDownList runat="server" ID="ddlNodeChildStatusDisplayMode">
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_337 %>" Value="NoBlink"></asp:ListItem>
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_338 %>" Value="Blinking"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_339) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LK0_4) %>
                        </td>
                        <td class="Property">
                            <asp:DropDownList runat="server" ID="ddlTipsIntegration">
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_LK0_6 %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_LK0_7 %>" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Property">
                            <asp:Literal ID="lTipsIntegrationText" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_AutocompleteUsernameField_PropertyLabel) %>
                        </td>
                        <td class="Property">
                            <asp:CheckBox runat="server" ID="UseAutocompleteInUsernameField" />
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_AutocompleteUsernameField_HintText) %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <% if (!_hiddenWebConsoleSettings.Contains("AuditingSettingsSection"))
                       {
                    %>
                    <tr>
                        <td colspan="3">
                            <h2>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VS0_3) %></h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VS0_4) %>
                        </td>
                        <td class="Property">
                            <asp:CheckBox runat="server" ID="cbAuditingTrails" />
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VS0_5) %>
                        </td>
                    </tr>
                    <% } %>

                    <% if (!_hiddenWebConsoleSettings.Contains("ChartSettingsSection"))
                       {
                    %>
                    <tr>
                         <td colspan="3">
                            <h2 style="display: inline !important;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_340) %></h2>&nbsp;
                            <span class="sw-pg-hint-body-info">
                                    <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreclassiccharts")) %>" class="sw-link" target="_blank" rel="noopener noreferrer">
                                        <span class="sw-pg-hint-text"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_245) %></span>
                                    </a>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_341) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbChartAspect" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Double" ControlToValidate="tbChartAspect" Operator="DataTypeCheck"
                                ID="CompareValidator6" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbChartAspect" ID="RequiredFieldValidator9"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_342) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_343) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbThumbnailAspect" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Double" ControlToValidate="tbThumbnailAspect" Operator="DataTypeCheck"
                                ID="CompareValidator7" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbThumbnailAspect" ID="RequiredFieldValidator10"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_344) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_345) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbPercentile" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="tbPercentile" Operator="DataTypeCheck"
                                ID="CompareValidator8" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbPercentile" ID="RequiredFieldValidator11"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_346) %>
                            <a href="<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionPH95th")) %>" target="_blank" rel="noopener noreferrer">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_347) %></a>.
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_7) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbMaximumSeries" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="tbMaximumSeries" Operator="DataTypeCheck"
                                ID="CompareValidator4" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbMaximumSeries" ID="RequiredFieldValidator3"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ControlToValidate="tbMaximumSeries" ID="RangeValidator1" runat="server"
                                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_315 %>" Display="Dynamic"
                                MinimumValue="1" MaximumValue="1000" Type="Integer" />
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_8) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_PdfChartsExport_RenderAsImage_PropertyLabel) %>
                        </td>
                        <td class="Property">
                            <asp:DropDownList runat="server" ID="ddlPdfChartsRenderAsImage">
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WebSettings_PdfChartsExport_RenderAsImage_Disable %>" Value="0"></asp:ListItem>
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WebSettings_PdfChartsExport_RenderAsImage_Enable %>" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_PdfChartsExport_RenderAsImage_HintText) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_MigrateCharts_Text) %>
                        </td>
                        <td class="Property">
                            <orion:LocalizableButton ID="promptChartsMigration" ClientIDMode="Static" runat="server" DisplayType="Small" LocalizedText="CustomText" 
                                Text="<%$ HtmlEncodedResources: CoreWebContent, WebSettings_MigrateCharts_Button_Migrate %>" OnClientClick="return showMigrationDialog(this);"/>
                            <orion:LocalizableButton ID="promptChartsRollback" ClientIDMode="Static" runat="server" DisplayType="Small" LocalizedText="CustomText" 
                                Text="<%$ HtmlEncodedResources: CoreWebContent, WebSettings_MigrateCharts_Button_Revert %>" OnClientClick="return showRollbackDialog(this);"/>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_MigrateCharts_Hint) %>  
                            <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreaddmoderncharts")) %>" class="sw-link" target="_blank" rel="noopener noreferrer">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WebSettings_MigrateCharts_LearnMore) %>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"> </td>
                    </tr>

                    <% } %>
                    
                    <% if (!_hiddenWebConsoleSettings.Contains("ChartSettingsSection"))
                       {
                    %>
                    <tr>
                        <td colspan="3">
                            <h2 style="display: inline !important;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_405) %></h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_406) %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="tbChartRefresh" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="tbChartRefresh" Operator="DataTypeCheck"
                                                  ID="CompareValidator5" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="tbChartRefresh" ID="RequiredFieldValidator5"
                                                        runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_407) %>
                        </td>
                    </tr>
                    
                    <% } %>

                    <% if (!_hiddenWebConsoleSettings.Contains("DiscoverySettingsSection"))
                       { %>
                    <tr>
                        <td colspan="3">
                            <h2>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_134) %></h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_350) %>
                        </td>
                        <td class="Property">
                            <asp:CheckBox runat="server" ID="cbNotifyRemovable" />
                        </td>
                        <td class="Property">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_351) %>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td colspan="3">
                            <h2>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_174) %>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                           <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_175) %>
                        </td>
                        <td class="Property">
                            <asp:CheckBox runat="server" ID="cbAutomaticGeolocation" />
                        </td>
                        <td class="Property">
                           <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_176) %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <h2>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_138) %>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_137) %></td>
                        <td class="Property">
                            <asp:TextBox runat="server" Columns="5" MaxLength="4" ID="tbAutoRefreshActiveAlerts"></asp:TextBox>
                            <asp:CompareValidator runat="server" Type="Integer" ControlToValidate="tbAutoRefreshActiveAlerts" Operator="DataTypeCheck" ErrorMessage="*" ID="AutoRefreshActiveAlertsCompareValidator"></asp:CompareValidator>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tbAutoRefreshActiveAlerts" ID="AutoRefreshActiveAlertsRequiredFieldValidator" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp;
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                        </td>
                        <td class="Propery"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_136) %></td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <br />
                            <orion:LocalizableButton runat="server" ID="imgbtnSubmit" LocalizedText="Submit"
                                DisplayType="Primary" OnClick="imgbtnSubmit_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
