using System;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_AdvAlerts : System.Web.UI.Page
{
    protected string ReturnUrl
	{
		get { return ReferrerRedirectorBase.GetReturnUrl(); }
	}

    protected override void OnInit(EventArgs e)
    {
            Response.Redirect("/Orion/Alerts/Default.aspx");
    }
}
