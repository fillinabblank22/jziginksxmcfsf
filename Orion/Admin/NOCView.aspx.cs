using System;
using System.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_NOCView : Page
{
    protected override void OnInit(EventArgs e)
    {
        Page.Title = Resources.CoreWebContent.WEBDATA_AB0_39;
        base.OnInit(e);
    }

	protected override void OnLoad(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		base.OnLoad(e);
	}
}
