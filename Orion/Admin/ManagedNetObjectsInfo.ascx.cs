﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Admin_ManagedNetObjectsInfo : System.Web.UI.UserControl
{
    public int NumberOfElements
    {
        get;
        set;
    }

    public string EntityName
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (NumberOfElements == 0)
        {
            monitoredObjectsInfo.Attributes["class"] = "noObjectsStyle";
            bulbImage.Visible = true;
            okImage.Visible = false;
        }
    }
}