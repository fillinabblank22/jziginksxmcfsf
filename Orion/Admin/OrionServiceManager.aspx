<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="OrionServiceManager.aspx.cs" 
        Inherits="Orion_Admin_OrionServiceManager" Title="<%$ HtmlEncodedResources: CoreWebContent, OrionServiceManager_Title %>" EnableViewState="false" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="IncludeContent" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Admin/js/OrionServiceManager.js" />
    <orion:Include runat="server" File="Admin/styles/OrionServiceManager.css" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1 class="page-title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.OrionServiceManager_Title) %></h1>
    <hr align="left">
    <div class="orionServerDropdownPanel">
        <asp:Label runat="server" Text="Orion Server:" Font-Bold="True" />
        <asp:DropDownList runat="server" ID="OrionServerDropdown" CssClass="orionServerDropdown" AutoPostBack="True"/>
    </div>
    <hr align="left">
     <div id="serviceErrorMessage" class="sw-suggestion sw-suggestion-fail errorMessageContainer hidden">
        <img src="/Orion/images/NotificationImages/notification_error.gif" class="errorImg" />
        <div class="errorText">
            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.OrionServiceManager_ErrorMessage, 
                              OrionServerDropdown.SelectedItem.Text, 
                              SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(25241), 
                              CultureInfo.InvariantCulture)) %>
        </div>
    </div>
    <div id="notRunningSvcWarningMessage" class="sw-suggestion warningMessageContainer hidden">
        <img src="/Orion/images/StatusIcons/Warning.gif" class="warningImg" />
        <div class="warningText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.OrionServiceManager_WarningMessage) %></div>
    </div>
    <div id="serviceManagerPlaceholder" class="serviceManagerTabs"></div>
</asp:Content>
