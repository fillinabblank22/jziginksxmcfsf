﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_SelectMenuBar : System.Web.UI.Page
{
	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

        //This will 'trick' ASP.NET into generating __doPostBack function
        ClientScript.GetPostBackEventReference(this, String.Empty);

		if (!this.IsPostBack)
		{
			this.PopulateData();
		}
	}

	private void PopulateData()
	{
		this.menuBars.DataSource = MenuBar.GetAllMenuBars();
		this.menuBars.DataBind();
	}

	protected void MenuBarsDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			Repeater menuItems = e.Item.FindControl("menuItems") as Repeater;
			if (menuItems != null)
			{
				menuItems.DataSource = e.Item.DataItem as MenuBar;
				menuItems.DataBind();
			}
		}
	}

	protected bool IsSystemMenuBar(string name)
	{
		return name.Equals("admin", StringComparison.InvariantCultureIgnoreCase);
	}

    protected void DeleteMenuBar(object sender, EventArgs e)
	{
        var menuName = Request.Form.Get("__EVENTARGUMENT") ?? "";

        if (string.IsNullOrEmpty(menuName) || menuName.Equals("admin", StringComparison.OrdinalIgnoreCase))
            return;

        MenuBar.DeleteMenu(menuName);
		this.PopulateData();
	}
}
