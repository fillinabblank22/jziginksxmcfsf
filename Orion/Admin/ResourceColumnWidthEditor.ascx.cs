using System;
using System.Web.UI;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_ResourceColumnWidthEditor : BaseViewGroupControl
{
	protected override void RebuildUI()
	{
		trWidth2.Visible = this.View.ColumnCount > 1;
		trWidth3.Visible = this.View.ColumnCount > 2;
		trWidth4.Visible = this.View.ColumnCount > 3;
		trWidth5.Visible = this.View.ColumnCount > 4;
		trWidth6.Visible = this.View.ColumnCount > 5;

		radioColumn1.Checked = this.View.ColumnCount == 1;
		radioColumn2.Checked = this.View.ColumnCount == 2;
		radioColumn3.Checked = this.View.ColumnCount == 3;
		radioColumn4.Checked = this.View.ColumnCount == 4;
		radioColumn5.Checked = this.View.ColumnCount == 5;
		radioColumn6.Checked = this.View.ColumnCount == 6;

		txtColumn1Width.Text = this.View.Column1Width.ToString();
		txtColumn2Width.Text = this.View.Column2Width.ToString();
		txtColumn3Width.Text = this.View.Column3Width.ToString();
		txtColumn4Width.Text = this.View.Column4Width.ToString();
		txtColumn5Width.Text = this.View.Column5Width.ToString();
		txtColumn6Width.Text = this.View.Column6Width.ToString();

		this.phCol1.Controls.Clear();
		this.phCol1.Controls.Add(new LiteralControl(this.View.Column1Width.ToString()));
		this.phCol2.Controls.Clear();
		this.phCol2.Controls.Add(new LiteralControl(this.View.Column2Width.ToString()));
		this.phCol3.Controls.Clear();
		this.phCol3.Controls.Add(new LiteralControl(this.View.Column3Width.ToString()));
		this.phCol4.Controls.Clear();
		this.phCol4.Controls.Add(new LiteralControl(this.View.Column4Width.ToString()));
		this.phCol5.Controls.Clear();
		this.phCol5.Controls.Add(new LiteralControl(this.View.Column5Width.ToString()));
		this.phCol6.Controls.Clear();
		this.phCol6.Controls.Add(new LiteralControl(this.View.Column6Width.ToString()));
	}

	protected void EditColumnsClick(object sender, EventArgs e)
	{
		tblColumnEdit.Visible = true;
		tblColumnView.Visible = false;
	}

	protected void SubmitColumnsClick(object sender, EventArgs e)
	{
		if (radioColumn1.Checked)
		{
			this.View.ColumnCount = 1;
		}
		else if (radioColumn2.Checked)
		{
			this.View.ColumnCount = 2;
		}
		else if (radioColumn3.Checked)
		{
			this.View.ColumnCount = 3;
		}
		else if (radioColumn4.Checked)
		{
			this.View.ColumnCount = 4;
		}
		else if (radioColumn5.Checked)
		{
			this.View.ColumnCount = 5;
		}
		else
		{
			this.View.ColumnCount = 6;
		}

		int tmp;

		if (Int32.TryParse(this.txtColumn1Width.Text, out tmp))
		{
			this.View.Column1Width = tmp;
		}

		if (Int32.TryParse(this.txtColumn2Width.Text, out tmp))
		{
			this.View.Column2Width = tmp;
		}

		if (Int32.TryParse(this.txtColumn3Width.Text, out tmp))
		{
			this.View.Column3Width = tmp;
		}

		if (Int32.TryParse(this.txtColumn4Width.Text, out tmp))
		{
			this.View.Column4Width = tmp;
		}

		if (Int32.TryParse(this.txtColumn5Width.Text, out tmp))
		{
			this.View.Column5Width = tmp;
		}

		if (Int32.TryParse(this.txtColumn6Width.Text, out tmp))
		{
			this.View.Column6Width = tmp;
		}

		ViewManager.UpdateView(this.View);
		this.RefreshPage();

		tblColumnEdit.Visible = false;
		tblColumnView.Visible = true;
	}
}