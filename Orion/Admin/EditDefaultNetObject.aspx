<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="EditDefaultNetObject.aspx.cs" 
    Inherits="Orion_Admin_EditDefaultNetObject" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_192 %>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <p style="width: auto;">
        <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_TM0_83, 
            "<b>" + ThisProfile.UserName + "</b>", "<b>" + Request.QueryString["Title"] + "</b>")) %>
    
        <br />
        <br />
        
        <asp:PlaceHolder runat="server" ID="phNoNetObjectNeeded" >
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_193) %>
        </asp:PlaceHolder>
        
        <asp:PlaceHolder runat="server" ID="phSelectNetObjectText">
            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_TM0_84,
                "<b>" + NetObjectFactory.GetFriendlyNameForNetObjectType(DefaultView.NetObjectType) + "</b>" )) %>
        </asp:PlaceHolder>
    </p>
    
    <asp:RadioButtonList runat="server" ID="lstNetObjects" DataTextField="Name" DataValueField="NetObjectID" />
    
    <div id="statusMessage">
        <asp:RequiredFieldValidator runat="server" EnableClientScript="false" ControlToValidate="lstNetObjects"
                                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_194 %>" Display="static" />
    </div>
    
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnContinue" OnClick="ContinueClick" LocalizedText="CustomText" 
            Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_186 %>" DisplayType="Primary" />
    </div>

</asp:Content>

