<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoltronAdminBucket.ascx.cs" Inherits="Orion_Admin_VoltronAdminBucket" %>
<%@ Register Src="~/Orion/Admin/AdminBucketLinkTile.ascx" TagPrefix="orion" TagName="LinkTile" %>

<%--NOTE: this is a voltron specific variation of AdminBucket--%>
<%--It is used for for the wilbur-based settings page --%>

<div class="NewContentBucket">
    <div class="NewBucketHeader"><%= DefaultSanitizer.SanitizeHtml(Header) %></div>

    <table class="NewBucketLinkContainer">
        <tr>
            <asp:Repeater ID="rptColumns" runat="server" OnItemDataBound="ColumnDataBound">
                <ItemTemplate>
                    <td width="<%= DefaultSanitizer.SanitizeHtml(ColumnWidth) %>%" class="BucketColumn">
                        <asp:Repeater ID="rptTiles" runat="server" OnItemDataBound="TileDataBound">
                            <ItemTemplate>
                                <orion:LinkTile ID="tile" runat="server"/>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </table>
</div>
