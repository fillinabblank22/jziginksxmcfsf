<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="SelectMenuBar.aspx.cs" Inherits="Orion_Admin_SelectMenuBar"
Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_8 %>" %>
<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <style type="text/css">
        #adminContent .x-toolbar td { padding: 0; }
        #adminContent *:first-child h1 { font-size: 18px; padding-bottom: 10px; }
        .sw-pg-menutitle { font-size: small; font-weight: bold; padding-bottom: 5px; clear: both; }
        .sw-pg-breadcrumb-subheading { font-size: 8pt; }
        .sw-extcmd-delete { background-image:url(/Orion/images/delete_16x16.gif) !important; }
        .sw-extcmd-edit { background-image:url(/Orion/images/edit_16x16.gif) !important; }
        .sw-extcmd-delete, .sw-extcmd-edit { padding-left: 20px !important; }
    </style>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.2.0" />
<orion:Include runat="server" File="NavTabs.css" />

<script type="text/javascript">
Ext.onReady(function(){
    $('.MenuEditToolbar').each(function(i,o){
        o.className = '';
        o.id = 'menuedit-toolbar-' + i;

        var config = {
                renderTo: o.id,
                items: [{ 
                text: '<%= SolarWinds.Orion.Web.ControlHelper.EncodeJsString( Resources.CoreWebContent.WEBDATA_AK0_28 ) %>', 
                iconCls: 'sw-extcmd-edit',
                handler: function(me) { window.location = "/Orion/Admin/EditMenuBar.aspx?Menu=" + encodeURIComponent(me.ownerCt.initialConfig.toolbarname); }
            }],
            toolbarname: $(o.parentNode).attr('rel')
        };

        if( config.toolbarname.toLowerCase() != 'admin' ){
            config.items.push('-');
            config.items.push({ text: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_31) %>', iconCls: 'sw-extcmd-delete', handler: function(me) {
              
                var tbName = me.ownerCt.initialConfig.toolbarname;

                if (confirm(String.format('<%= SolarWinds.Orion.Web.ControlHelper.EncodeJsString( Resources.CoreWebContent.WEBDATA_PCC_22 ) %>', tbName))) {
                    __doPostBack('<%= DeleteBtn.UniqueID %>', tbName);
                }
              } 
            });
        }

        var tb = new Ext.Toolbar(config);
    });
});
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <table width="100%">
    <tr>
        <td>
            <h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_24) %></h1>
        </td>
    </tr>
     <tr>
        <td class="sw-pg-breadcrumb-subheading"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_25) %></td>
    </tr>
    </table>

    <asp:Repeater runat="server" ID="menuBars" OnItemDataBound="MenuBarsDataBound">
        <ItemTemplate><div automation="toolbar-section" rel="<%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular( HttpUtility.HtmlAttributeEncode( (string)Eval("Name") ) ) %>" style="margin-bottom: 16px;">

            <div class="sw-pg-menutitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_26) %> <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Eval("Name").ToString()) %></div>

            <div class="MenuEditToolbar"></div>

            <div class="sw-mainnav-box"><div class="sw-mainnav-bars">
            <ul>
			    <asp:Repeater ID="menuItems" runat="server">
		            <ItemTemplate>
                        <li><a href='<%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Eval("LinkTarget").ToString())%>' <%# Convert.ToBoolean(Eval("OpenInNewWindow"))?"target='_blank'":"" %>><%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Eval("Title").ToString()) %></a></li>
                	</ItemTemplate>
			    </asp:Repeater>
            </ul>
            <div style="clear: both;"><!-- --></div></div></div>
        </div></ItemTemplate>
    </asp:Repeater>
    <br />

    <orion:LocalizableButton runat="server" id="DeleteBtn" Visible="false" DisplayType="Small" LocalizedText="Delete" OnClick="DeleteMenuBar" /><%-- intentionally not visible, ever. --%>
    <orion:LocalizableButtonLink runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_27 %>" NavigateUrl="/Orion/Admin/EditMenuBar.aspx" />

</asp:Content>
