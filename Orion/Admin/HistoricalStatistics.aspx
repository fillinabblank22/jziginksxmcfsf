<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="HistoricalStatistics.aspx.cs" Inherits="Orion_Admin_HistoricalStatistics"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_246 %>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style type="text/css">
        .ViewHeader { border: solid 1px #DBDCD7; padding-top: 8px; padding-bottom: 8px; }
    </style>

    <h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_354) %></h1>
           
    <table class="ViewHeader" style="width: 100%; " border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="padding-left: 20px; padding-top:10px;">
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_247) %></b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px">
                <table runat="server" id="CiscoBuffers">
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px; padding-top:10px;">
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_248) %> </b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px">
                <table runat="server" id="CPULoad">
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px; padding-top:10px;">
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_249) %> </b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px">
                <table runat="server" id="ResponseTime">
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px; padding-top:10px;">
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_250) %> </b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px">
                <table runat="server" id="InterfaceTraffic">
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px; padding-top:10px;">
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_251) %> </b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px">
                <table runat="server" id="InterfaceErrors">
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
