<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddRemoveObjectsControl.ascx.cs"
Inherits="Orion_Admin_Containers_AddRemoveObjectsControl" %>

<orion:Include runat="server" File="Admin/Containers/DynamicQueryPreview.js" />
<orion:Include runat="server" File="Admin/Containers/AddRemoveObjects.js" />

<div class="swis-error-wrapper" style="width:99%;"></div>

<div id="GroupItemsSelector">
    <div class="GroupSection">
		<label for="itemTypeSelect"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_224) %></label>
		<select id="itemTypeSelect">
		<% foreach (KeyValuePair<String, String> entity in AvailableEntities) { %>
			<option value="<%= DefaultSanitizer.SanitizeHtml(entity.Key) %>"><%= DefaultSanitizer.SanitizeHtml(entity.Value) %></option>
		<% } %>
		</select>
	</div>
	<div class="GroupSection">
		<label for="groupBySelect"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_5) %></label>
		<select id="groupBySelect">
			<option value="test"/>
		</select>
	</div>
    <div class="GroupSection">
		<label for="searchBox"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_225) %></label>
        <table>
            <tr>
            <td><input type="text" class="searchBox" id="searchBox" name="searchBox" /></td>
            <td><a href="javascript:void(0);" id="searchButton"><img src="/Orion/images/Button.SearchIcon.gif" /></a></td>
            </tr>
        </table>
	</div>
</div>

<div id="ContainerMembersTable" style="width:99%;">
</div>

<div id="AddButtonPanel" style="text-align: center; font-size:11px;">
    <a href="javascript:void(0);" id="AddToGroupButton" style="top: 44%; position: relative;"><img src="/Orion/images/AddRemoveObjects/arrows_add_32x32.gif" /><br/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_226) %></a><br/>
    <a href="javascript:void(0);" id="RemoveFromGroupButton" style="top: 46%; position: relative;"><img src="/Orion/images/AddRemoveObjects/arrows_remove_32x32.gif" /><br/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_227) %></a>
</div>

<asp:HiddenField ID="gridItems" runat="server" />
<asp:HiddenField ID="currentItem" runat="server" />

<input type="hidden" name="Core_Containers_ItemType" id="Core_Containers_ItemType" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("Core_Containers_ItemType")) %>' />

<script type="text/javascript">
    // <![CDATA[
    SW.Core.containers.SetCurrentContainerID(<%= Container.Id %>);
    SW.Core.containers.SetParentContainerID(<%= (Container.Parent != null)? Container.Parent.Id : 0 %>);
    SW.Core.containers.SetMemberIds(<%= MemberIds %>);
    SW.Core.containers.SetCircledGroupName('<%= Escape(GetCircledGroupName()) %>');
    SW.Core.containers.SetParentContainerUri('<%= (Container.Parent != null)? Container.Parent.Uri : null %>');
    SW.Core.containers.SetMaxGroupNestedLevel(<%= MaxGroupNestedLevel %>);
    SW.Core.containers.SetCurrentContainerUri('<%= Container.Uri %>');
    SW.Core.containers.SetCurrentNestedLevel(<%= CurrentNestedLevel %>);
    SW.Core.containers.SetGroupName('<%= Escape(Container.Name) %>');
    SW.Core.containers.SetGridItemsFieldClientID('<%= GridItemsFieldClientID %>');
    SW.Core.containers.SetCurrentItemFieldClientID('<%= CurrentItemFieldClientID %>');
    SW.Core.containers.SetAddToGroupButtonClientID('AddToGroupButton');
    SW.Core.containers.SetRemoveFromGroupButton('RemoveFromGroupButton');
    <% foreach (SolarWinds.Orion.Core.Common.Models.ContainerMemberDefinition definition in Definitions) { %>
    SW.Core.containers.AddToInitialData(['<%= Escape(definition.Definition) %>','<%=Escape(definition.MemberName) %>','<%=Escape(definition.FullMemberName) %>',<%=definition.MemberStatus %>,'<%=Escape(definition.Entity) %>',<%= (definition.IsDynamicQuery) ? '1' : '0' %>]);
    <% } %>
    SW.Core.containers.SetAddDynamicQueryPostBack(function() { <%= AddDynamicQueryPostBack %> });
    SW.Core.containers.SetEditDynamicQueryPostBack(function() { <%= EditDynamicQueryPostBack %> });
    // ]]>
</script>