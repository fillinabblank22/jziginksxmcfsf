<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditProperties.ascx.cs" Inherits="Orion_Admin_Containers_EditProperties" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register Src="~/Orion/Nodes/Controls/CustomProperties.ascx" TagPrefix="orion" TagName="CustomProperties" %>
<%@ Register TagPrefix="orion" TagName="SwisfErrorControl" Src="~/Orion/SwisfErrorControl.ascx" %>

<style type="text/css">
    .helpfulText {
    color: #5F5F5F;
    font-size: 10px;
    vertical-align: middle;
}
.contentBlockHeader {
    font-weight: bold;
}
</style>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Admin/Containers/DynamicQueryPreview.js" />

<script type="text/javascript">
    //<![CDATA[
    var changesMade = false;

    function setChanges() {
        changesMade = true;
    }
    
    function postFrequencyValidate() {
         $("#<%= RangeValidator1.ClientID %>").is(':hidden') ? $("#secondsLabel").removeClass("sw-validation-error") : $("#secondsLabel").addClass("sw-validation-error"); ;    
    }

    $(document).ready(function () {
        $("#<%= frequencyBox.ClientID %>").bind('change', function () { postFrequencyValidate(); });
        $("#<%= frequencyBox.ClientID %>").keypress(function (e) {
            if (e.which == 13) { postFrequencyValidate();}
        });
    });

    function checkChanges() {
        if (changesMade) {
            Ext.Msg.confirm("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_7) %>",
                                "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_8) %>",
                                function(btn, text) {
                                    if (btn == 'yes') {
                                        $('#<%= saveChangesField.ClientID %>').val(1);
                                    }
                                    changesMade = 0;
                                    $('#<%= addRemoveButton.ClientID %>').click();
                                });
            return false;
        }
    }

    function ToggleContainerContents(button) {
        var div = $("#containerContentDiv");
        div.toggleClass("hidden");
        if (div.hasClass("hidden")) {
            button.setAttribute("src", "/Orion/images/Button.Expand.gif");
        } else {
            button.setAttribute("src", "/Orion/images/Button.Collapse.gif");
        }
    }

    function ToggleAdvanced(button) {
        var div = $("#containerAdvancedDiv");
        div.toggleClass("hidden");
        if (div.hasClass("hidden")) {
            button.setAttribute("src", "/Orion/images/Button.Expand.gif");
        } else {
            button.setAttribute("src", "/Orion/images/Button.Collapse.gif");
        }
    }
    //]]>
</script>

<% if (!String.IsNullOrEmpty(Title))
   { %>
   <h1><%= DefaultSanitizer.SanitizeHtml(Title) %></h1>
<% } %>

<% if (!String.IsNullOrEmpty(ContainerParentName)) { %>
<div id="nestedGroupNotification">
    <img alt="" src="/Orion/images/NotificationImages/hint_message_icon_16x16.png" /> <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_211, ContainerParentName)) %>
</div>
<% } %>

<orion:SwisfErrorControl runat="server" ID="SwisfErrorControl" Visible="False" />

<table id="containerDetailsTable">
    <tr>
        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_14) %></td>
        <td>
            <asp:TextBox runat="server" ID="nameBox" CssClass="NameBox" onchange="setChanges();" />
            <asp:RequiredFieldValidator ControlToValidate="nameBox" ID="valNameBoxRequired" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_518 %>" runat="server" Display="Dynamic" />
            <asp:CustomValidator ID="valNameUnique" ControlToValidate="nameBox"  runat="server" OnServerValidate="ValidateNameUnique" />
        </td>
    </tr>
    <tr>
        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_136) %></td>
        <td><asp:TextBox runat="server" ID="descriptionBox" Rows="3" CssClass="DescriptionBox" TextMode="MultiLine" onchange="setChanges();" /></td>
    </tr>
    <% if (DisplayMembers)
       { %>
    <tr>
        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_212) %></td>
        <td>
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif" OnClientClick="ToggleContainerContents(this); return false;" CausesValidation="false" />
            <asp:Label runat="server" ID="contentsSummaryLabel" />

            <orion:LocalizableButton runat="server" ID="addRemoveButton" OnClick="AddRemoveObjects_Click" OnClientClick="return checkChanges();"
                LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_218 %>" DisplayType="Small" />

            <asp:HiddenField ID="saveChangesField" Value="0" runat="server" />
            
            <div id="containerContentDiv" class="hidden">
                <asp:Repeater runat="server" ID="contentsRepeater">
                    <HeaderTemplate>
                        <table id="ContainerMembersPreviewTable">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
			                    <orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" 
                                    Entity="<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Core.Common.Models.ContainerMemberDefinition)Container.DataItem).EntityOrDynamicQuery) %>" 
                                    EntityId="<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Core.Common.Models.ContainerMemberDefinition)Container.DataItem).Definition.Split('=').Last()) %>" 
                                    Status="<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Core.Common.Models.ContainerMemberDefinition)Container.DataItem).MemberStatus) %>" />
					            <%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Core.Common.Models.ContainerMemberDefinition)Container.DataItem).FullMemberName) %>
			                </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </td>
    </tr>
    <% } %>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="2">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif" OnClientClick="ToggleAdvanced(this); return false;" CausesValidation="false" />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_216) %>
            
            <div id="containerAdvancedDiv" class="hidden">
                <table>
                    <tr>
                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_213) %></td>
                        <td>
                            <asp:DropDownList runat="server" ID="rollupComboBox" CssClass="RollupBox" onchange="setChanges();" />
                            <a href="#" id="rollupHelpLink" runat="server" class="formHelpfulText" target="_blank" rel="noopener noreferrer"><u><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_214) %></u></a>
                        </td>
                    </tr>
                    <tr>
                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_215) %></td>
                        <td>
                            <asp:TextBox runat="server" ID="frequencyBox" CssClass="FrequencyBox" onchange="setChanges();" />
                            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="frequencyBox" 
                                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_217 %>" 
                                MinimumValue="60" MaximumValue="1000000"
                                Type="Integer"
                                runat="server" Display="Dynamic" />
                            <span id="secondsLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %></span>
                        </td>
                    </tr>
                    <tr>
                    <td style="vertical-align: middle; padding: 5px;" colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <orion:customproperties runat="server" id="groupCps" netobjecttype="ContainerCustomProperties"
                                    helpfullhinttext="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_106 %>" custompropertiestable="ContainerCustomProperties" />
                                <br />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
