﻿using System;
using System.Linq;

using SolarWinds.InformationService.Contract2;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using ContainerMemberDefinition = SolarWinds.Orion.Core.Common.Models.ContainerMemberDefinition;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Containers_Add_Members : ContainerWizardPageBase, IBypassAccessLimitation
{
    private void CheckNodeRole()
    {
        if (!Profile.AllowNodeManagement)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_180) }); 
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckNodeRole();

        ((Orion_Admin_Containers_Add_ContainerWizard)this.Master).HelpFragment = "OrionCorePHGroups_AddNewGroup";

        if (Workflow.CurrentStepType != ContainerWorkflow.StepType.Members)
        {
            GotoFirstStep();
        }

        addRemoveObjectsControl.Container = ContainerModel;
    }

    protected override bool Back()
    {
        SaveState();
        return true;
    }

    protected override bool Next()
    {
        SaveState();

        ContainersDAL dal = new ContainersDAL();
        int id = dal.CreateContainer(ContainerModel);

        SaveStateAfterCreation(id);

        foreach (var cp in ContainerModel.CustomProperties.Where(cp => cp.Value != null))
		{
			if (id != 0 || (cp.Value != null && !string.IsNullOrEmpty(cp.Value.ToString()))) //no need to run update query for empty cps
			{
				CustomPropertyMgr.SetCustomProp("ContainerCustomProperties", cp.Key, id, cp.Value);

                var changedProperties = new PropertyBag
                                                    {
                                                        {"InstanceType", "Orion.GroupCustomProperties"},
                                                        {"ContainerID", id},
                                                        {cp.Key, cp.Value}
                                                    };
                IndicationPublisher.CreateV3().ReportIndication(
                    IndicationHelper.GetIndicationType(IndicationType.System_InstanceModified),
                    IndicationHelper.GetIndicationProperties(),
                    changedProperties);

				// update restricted values if needed
				var customProperty = CustomPropertyMgr.GetCustomProperty("ContainerCustomProperties", cp.Key);
				CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty,
					new[] { CustomPropertyHelper.GetInvariantCultureString((cp.Value != null) ? cp.Value.ToString() : string.Empty, customProperty.PropertyType) });
			}
		}

        return true;
    }
    
    protected void SaveState()
    {
        ContainerModel.MemberDefinitions.Clear();
        foreach (ContainerMemberDefinition definition in addRemoveObjectsControl.MemberDefinitions)
        {
            ContainerModel.MemberDefinitions.Add(definition);
        }
    }

    protected void SaveStateAfterCreation(int containerId)
    {
        ContainerModel.Id = containerId;
    }
}
