﻿using System;

public partial class Orion_Admin_Containers_Add_ContainerWizard : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public string HelpFragment
    {
        get 
        { 
            return ((Orion_Admin_Containers_ContainerAdminPage)this.Master).HelpFragment; 
        }
        set 
        { 
            ((Orion_Admin_Containers_ContainerAdminPage)this.Master).HelpFragment = value; 
        }
    }
}
