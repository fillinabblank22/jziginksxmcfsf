<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/Containers/Add/ContainerWizard.master" AutoEventWireup="true" 
CodeFile="Default.aspx.cs" Inherits="Orion_Admin_Containers_Add_Default"
Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_219 %>" %>

<%@ Register Src="~/Orion/Admin/Containers/EditProperties.ascx" TagPrefix="orion" TagName="EditContainerPropertiesControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:EditContainerPropertiesControl ID="editPropertiesControl" runat="server" DisplayMembers="false" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_220 %>" />
    
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" 
            LocalizedText="Next" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click"
            LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
    </div>
</asp:Content>

