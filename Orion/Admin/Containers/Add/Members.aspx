<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/Containers/Add/ContainerWizard.master" AutoEventWireup="true" 
CodeFile="Members.aspx.cs" Inherits="Orion_Admin_Containers_Add_Members" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_219 %>"%>

<%@ Register Src="~/Orion/Admin/Containers/AddRemoveObjectsControl.ascx" TagName="AddRemoveObjectsControl" TagPrefix="orion" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_221) %></h1>
    <p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_222) %>
    </p>
    
    <orion:AddRemoveObjectsControl ID="addRemoveObjectsControl" runat="server" />
    
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" 
            LocalizedText="Back" DisplayType="Secondary" />
        <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" 
            LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_223 %>" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" 
            LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
    </div>
</asp:Content>

