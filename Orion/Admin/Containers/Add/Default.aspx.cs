﻿using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.UI;
using ContainerModel = SolarWinds.Orion.Core.Common.Models.Container;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Containers_Add_Default : ContainerWizardPageBase, IBypassAccessLimitation
{
	private void CheckNodeRole()
	{
		if (!Profile.AllowNodeManagement)
		{
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_180) }); 
		}
	}
	
	protected void Page_Load(object sender, EventArgs e)
    {
		CheckNodeRole();
		
		((Orion_Admin_Containers_Add_ContainerWizard)this.Master).HelpFragment = "OrionCorePHGroups_AddNewGroup";

        if (Request["parent"] != null)
        {
            Int32 parentId;
            Int32.TryParse(Request["parent"], out parentId);
            if (parentId != 0)
            {
                ContainersDAL dal = new ContainersDAL();
                this.ContainerModel.Parent = dal.GetContainer(parentId, true);
            }
        }
        editPropertiesControl.ContainerModel = ContainerModel;
    }

    protected override bool ValidateUserInput()
    {
        Page.Validate("EditCpValue");

        if (Page.IsValid)
            return base.ValidateUserInput();

        if (this.editPropertiesControl.ContainerModel != null && string.IsNullOrEmpty(this.editPropertiesControl.ContainerModel.Name))
        {
            return false;
        }

        if (!ClientScript.IsClientScriptBlockRegistered("ExpandScript"))
        {
            String cstext1 = "<script type=\"text/javascript\">" +
                "ToggleAdvanced($('[id*=ImageButton2]')[0]); </" + "script>";
            ClientScript.RegisterStartupScript(this.GetType(), "ExpandScript", cstext1);
        }

        return false;
    }
}
