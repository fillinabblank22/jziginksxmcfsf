﻿<%@ WebHandler Language="C#" Class="ContainerObjectsTreeProvider" %>

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.Helpers;

public class ContainerObjectsTreeProvider : IHttpHandler {
    [Serializable]
    private class TreeNode
    {
        public string Uri { get; set; }
        public string FullName { get; set; }
        public string FullNameWithoutHighlight { get; set; }
        public string Entity { get; set; }
        public int Status { get; set; }
        public int NestedLevel { get; set; }
        public bool LoadNextItems { get; set; }
        public override string ToString()
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            str.Append("{");
            str.AppendFormat("id: '{0}',", Uri);
            str.AppendFormat("text: '{0}',", HttpUtility.JavaScriptStringEncode(FullName));
            str.AppendFormat("entity: '{0}',", Entity);
            str.AppendFormat("status: '{0}',", Status);
            str.AppendFormat("nestedLevel: '{0}',", NestedLevel);
            str.AppendFormat("fullName: '{0}',", HttpUtility.JavaScriptStringEncode(FullNameWithoutHighlight));
            if (Entity == null)
            {
                str.Append("allowDrag: false,");
            }
            else
            {
                // no checkbox for "There are XX more items... message
                str.Append("checked: false,");
                str.Append("propagateCheck: true,");   
            }

            str.Append("attributes: {loadNextItems: " + LoadNextItems.ToString().ToLower() + "},");
			str.AppendFormat("icon: '/Orion/StatusIcon.ashx?entity={0}&amp;id={1}&amp;status={2}&amp;size=small',", Entity, (Uri ?? "").Split('=').Last(), Status);
            str.Append("listeners: { 'checkchange': function (node, checked) {");
            str.Append("if (checked) {");
            str.Append("   node.getUI().addClass('x-tree-selected');");
            str.Append("} else {");
            str.Append("  node.getUI().removeClass('x-tree-selected');");
            str.Append("  if ((node.parentNode) && (node.attributes.propagateCheck)) {");
            str.Append("    node.parentNode.attributes.propagateCheck = false;");
            str.Append("    node.parentNode.getUI().removeClass('x-tree-selected');");
            str.Append("    node.parentNode.getUI().toggleCheck(false);");
            str.Append("    node.parentNode.attributes.propagateCheck = true;");
            str.Append("  }");
            str.Append("} } },");
            str.Append("expandable: false,");
            str.Append("leaf: true");
            str.Append("}");
            return str.ToString();
        }
    }

    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        String entityType = context.Request.Params["entityType"];
        String property = context.Request.Params["groupBy"];
        String propertyValue = context.Request.Params["value"];
        String searchValue = context.Request.Params["searchValue"];
        String excludeDefinitionsStr = context.Request.Params["excludeDefinitions"];
        String memberDefinitions = context.Request.Params["memberDefinitions"];
        String currentContainerIdStr = context.Request.Params["currentContainerID"];
        String parentContainerIdStr = context.Request.Params["parentContainerID"];

        String initialLoad = context.Request.Params["initialLoad"];
        bool bInitialLoad = true;
        Boolean.TryParse(initialLoad, out bInitialLoad);

        int currentContainerId = 0;
        int.TryParse(currentContainerIdStr, out currentContainerId);

        int parentContainerId = 0;
        int.TryParse(parentContainerIdStr, out parentContainerId);

        HashSet<string> excludeDefinitions = new HashSet<string>(JsonConvert.DeserializeObject<string[]>(excludeDefinitionsStr));

        using (var swisErrorsContext = new SwisErrorsContext())
        {
            SolarWinds.Orion.Web.DAL.ContainersMetadataDAL dal = new SolarWinds.Orion.Web.DAL.ContainersMetadataDAL();
            System.Collections.Generic.IEnumerable<SolarWinds.Orion.Web.Containers.Entity> entities
                = dal.GetEntities(entityType, property, propertyValue, searchValue, excludeDefinitions);

            Dictionary<int, IList<int>> membersIds = null;
            if (!string.IsNullOrEmpty(memberDefinitions))
            {
                try
                {
                    membersIds = JsonConvert.DeserializeObject<Dictionary<int, IList<int>>>(memberDefinitions);
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    throw ex;
                }
            }

            System.Text.StringBuilder response = new System.Text.StringBuilder("{");
            bool first = true;
            int count = 0;
            int maxCount = ContainerHelper.GetManageGroupsMaxObjectTreeItems();

            int startIndex = 0;
            if (!bInitialLoad)
            {
                startIndex = maxCount;
            }

            var swisErrorMessages = swisErrorsContext.GetErrorMessages();
            if (swisErrorMessages.Any())
            {
                var control = new System.Web.UI.UserControl();
                var swisfErrorControl = control.LoadControl("~/Orion/SwisfErrorControl.ascx") as SolarWinds.Orion.Web.BaseSwisfErrorControl;
                string swisError = SolarWinds.Orion.Web.BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(
                    swisErrorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList(), swisfErrorControl);

                response.AppendFormat("errorControl: '{0}',", HttpUtility.JavaScriptStringEncode(swisError));
            }

            response.Append("data:[");

            foreach (SolarWinds.Orion.Web.Containers.Entity entity in entities.Skip(startIndex))
            {
                TreeNode node;
                if ((count++ == maxCount) && bInitialLoad)
                {
                    int remainingCount = entities.Count() - count + 1;
                    node = new TreeNode()
                    {
                        FullName = (remainingCount > 1) ? String.Format(CoreWebContent.WEBCODE_VB1_20, remainingCount) : String.Format(CoreWebContent.WEBCODE_VB1_21, remainingCount),
                        LoadNextItems = true
                    };
                }
                else
                {
                    string name = entity.FullNameWithoutLinks;
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        // highlight search text
                        name = Regex.Replace(
                            name,
                            String.Format("({0})", Regex.Escape(searchValue)),
                            "<span class=\"searchHighlight\">$1</span>",
                            RegexOptions.IgnoreCase);
                    }

                    node = new TreeNode()
                    {
                        Uri = entity.Uri,
                        Entity = entity.EntityType,
                        Status = entity.Status,
                        FullName = name,
                        FullNameWithoutHighlight = entity.FullNameWithoutLinks,
                        LoadNextItems = false
                    };

                    if (entity.EntityType.Equals("Orion.Groups") && membersIds != null)
                    {
                        var nestedLevel = 0;
                        var grouId = UriHelper.GetId(entity.Uri);

                        //Get group's nested level (of sub groups)
                        ContainerHelper.GetNestedLevelDown(currentContainerId == 0 && parentContainerId > 0 ? parentContainerId : currentContainerId, grouId, membersIds, ref nestedLevel);
                        node.NestedLevel = nestedLevel;
                    }
                }

                if (!first)
                    response.Append(",");
                response.Append(node);

                first = false;

                if ((count > maxCount) && bInitialLoad) break;
            }
            response.Append("]}");
            context.Response.Write(response.ToString());            
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
