﻿<%@ WebHandler Language="C#" Class="PropertyAutocompleteProvider" %>

using System;
using System.Web;

public class PropertyAutocompleteProvider : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        String entityName = context.Request.Params["entityName"];
        String property = context.Request.Params["propertyName"];
        String propertyValue = context.Request.Params["q"];
        int limit = Convert.ToInt32(context.Request.Params["limit"]);

        SolarWinds.Orion.Web.DAL.ContainersMetadataDAL dal = new SolarWinds.Orion.Web.DAL.ContainersMetadataDAL();
        System.Collections.Generic.IEnumerable<string> values
            = dal.GetPropertyValues(entityName, property, propertyValue, limit, true);

        System.Text.StringBuilder response = new System.Text.StringBuilder();
        foreach (string val in values)
        {
            response.AppendLine(val);
        }
        context.Response.Write(response.ToString());
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}