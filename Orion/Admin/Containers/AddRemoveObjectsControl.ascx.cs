﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common.Models;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Linq;
using System.Text;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Admin_Containers_AddRemoveObjectsControl : System.Web.UI.UserControl
{
    private IEnumerable<KeyValuePair<string, string>> entities;
    private readonly string addDynamicQueryArgument = "AddDynamicQuery";
    private readonly string editDynamicQueryArgument = "EditDynamicQuery";
    private readonly string membersSessionIdFormat = "cm{0}";
    private string sessionKey;
    private IEnumerable<ContainerMemberDefinition> definitionsFromSession;
    private Dictionary<int, IList<int>> memberIds;
    private int circledGroupId;
    private int currentNestedLevel;

    protected void Page_Load(object sender, EventArgs e)
    {
        sessionKey = String.Format(membersSessionIdFormat, Container.Id);

        if (Session[sessionKey] != null)
        {
            definitionsFromSession = (IEnumerable<ContainerMemberDefinition>)Session[sessionKey];
            Session.Remove(sessionKey);
        }

        AddDynamicQueryPostBack = Page.ClientScript.GetPostBackEventReference(this, addDynamicQueryArgument);
        EditDynamicQueryPostBack = Page.ClientScript.GetPostBackEventReference(this, editDynamicQueryArgument);

        if (Page.IsPostBack)
        {
            if (Request["__EVENTARGUMENT"] == addDynamicQueryArgument)
            {
                Session[sessionKey] = MemberDefinitions;
                Response.Redirect(
                    String.Format("/Orion/Admin/Containers/EditDynamicQuery.aspx?sid={0}&returnUrl={1}", 
                        sessionKey,
                        UrlHelper.ToSafeUrlParameter(Request.RawUrl)));
            }
            else if (Request["__EVENTARGUMENT"] == editDynamicQueryArgument)
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DataGridRecord));
                DataGridRecord record = (DataGridRecord)serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(currentItem.Value)));

                Session[sessionKey] = MemberDefinitions;
                Response.Redirect(
                    String.Format("/Orion/Admin/Containers/EditDynamicQuery.aspx?name={0}&def={1}&sid={2}&returnUrl={3}",
                        HttpUtility.UrlEncode(record.Name),
                        HttpUtility.UrlEncode(record.Definition),
                        sessionKey,
                        UrlHelper.ToSafeUrlParameter(Request.RawUrl)));
            }
        }
        else
        {
            using (var dal = new ContainersDAL())
            {
                memberIds = dal.GetMemberIds();
            }
            var groupId = Container.Id;
            if (groupId != 0 && memberIds.Count > 0)
            {
                var nestedlevel = 0;
                circledGroupId = ContainerHelper.GetNestedLevelDown(groupId, groupId, memberIds, ref nestedlevel);
                currentNestedLevel = nestedlevel;
            }
        }
    }

    public Container Container
    {
        get;
        set;
    }

    public int MaxGroupNestedLevel
    {
        get
        {
            int val;

            if (int.TryParse(ConfigurationManager.AppSettings["MaxGroupNestedLevel"], out val))
                return val;

            return int.MaxValue;
        }
    }

    protected String AddDynamicQueryPostBack { get; set; }
    protected String EditDynamicQueryPostBack { get; set; }

    public IList<ContainerMemberDefinition> MemberDefinitions
    {
        get
        {
            List<ContainerMemberDefinition> definitions = new List<ContainerMemberDefinition>();
            String itemsData = gridItems.Value;

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DataGridRecord[]));
            DataGridRecord[] records = (DataGridRecord[])serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(itemsData)));

            foreach (DataGridRecord record in records) 
            {
                ContainerMemberDefinition definition = new ContainerMemberDefinition() 
                {
                    Definition = record.Definition,
                    Entity = record.Entity,
                    MemberName = record.Name,
                    FullMemberName = record.FullName,
                    MemberStatus = record.MemberStatus
                }; 
                
                definitions.Add(definition);
            }

            definitions.AddRange(HiddenDefinitions);

            return definitions;
        }
    }

    protected String GridItemsFieldClientID
    {
        get
        {
            return gridItems.ClientID;
        }
    }
    protected String CurrentItemFieldClientID
    {
        get
        {
            return currentItem.ClientID;
        }
    }

    protected int CurrentNestedLevel
    {
        get
        {
            return currentNestedLevel;
        }
    }

    protected IEnumerable<KeyValuePair<string, string>> AvailableEntities
    {
        get
        {
            if (entities == null)
            {
                ContainersMetadataDAL dal = new ContainersMetadataDAL();
                entities = dal.GetAvailableContainerMemberEntities(true).OrderBy(x => x.Value);
            }

            return entities;
        }
    }

    protected IEnumerable<ContainerMemberDefinition> Definitions
    {
        get
        {
            if (definitionsFromSession != null)
            {
                return definitionsFromSession.Where(def => def.IsVisible); ;
            }
            else
            {
                ContainersDAL dal = new ContainersDAL();
                return dal.GetMemberDefinitions(Container.Id, ContainersDAL.MemberDefinitionLoadMethod.Members).Where(def => def.IsVisible);
            }
        }
    }

    protected IEnumerable<ContainerMemberDefinition> HiddenDefinitions
    {
        get
        {
            if (definitionsFromSession != null)
            {
                return definitionsFromSession.Where(def => !def.IsVisible); ;
            }
            else
            {
                ContainersDAL dal = new ContainersDAL();
                return dal.GetMemberDefinitions(Container.Id, ContainersDAL.MemberDefinitionLoadMethod.Members).Where(def => !def.IsVisible);
            }
        }
    }

    protected string MemberIds
    {
        get
        {
            return memberIds.Count > 0 ? JsonConvert.SerializeObject(memberIds) : string.Empty;
        }
    }

    protected string Escape(string input)
    {
        if (input == null)
            return string.Empty;

        return input.Replace(@"\", @"\\").Replace("'", @"\'");
    }

    protected string GetCircledGroupName()
    {
        if (circledGroupId != 0)
        {
            using (var dal = new ContainersDAL())
            {
                return dal.GetContainer(circledGroupId).Name;
            }
        }
        return string.Empty;
    }

    // when changing this record, change also record for grid in AddRemoveObjects.js
    [Serializable]
    private class DataGridRecord
    {
        public string Definition = null;
        public string Name = null;
        public string FullName = null;
        public int MemberStatus = 0;
        public string Entity = null;
        public string IsDynamicQuery = null;
    }
}
