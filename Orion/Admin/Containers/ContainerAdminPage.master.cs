﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Admin_Containers_ContainerAdminPage : System.Web.UI.MasterPage
{
    internal bool _isOrionDemoMode = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        _isOrionDemoMode = (!Profile.AllowNodeManagement && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer);

        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }
    }

    public string HelpFragment { get; set; }
}
