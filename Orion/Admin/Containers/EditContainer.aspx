<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/Containers/ContainerAdminPage.master" AutoEventWireup="true"
 CodeFile="EditContainer.aspx.cs" Inherits="Orion_Admin_Containers_EditContainer" %>

<%@ Register Src="~/Orion/Admin/Containers/EditProperties.ascx" TagPrefix="orion" TagName="EditContainerPropertiesControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
	<div class="ContentBucket">
	    <orion:EditContainerPropertiesControl ID="editPropertiesControl" runat="server" DisplayMembers="true" />
	</div>
	
	<div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="submitButton" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
        <orion:LocalizableButton runat="server" ID="cancelButton" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" CausesValidation="False" />
	</div>
</asp:Content>
