using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using ContainerModel = SolarWinds.Orion.Core.Common.Models.Container;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Containers_AddRemoveObjects : System.Web.UI.Page, IBypassAccessLimitation
{
    private ContainerModel containerModel;
    private ContainersDAL dal;

    private void CheckNodeRole()
    {
        if (!Profile.AllowNodeManagement)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_180) }); 
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckNodeRole();

        ((Orion_Admin_Containers_ContainerAdminPage)this.Master).HelpFragment = "OrionCorePHGroups_AddRemoveOrionObjects";

        dal = new ContainersDAL();
        containerModel = dal.GetContainer(Convert.ToInt32(Request.QueryString["id"]));

        this.Title = String.Format(Resources.CoreWebContent.WEBCODE_TM0_95, UIHelper.Escape(containerModel.Name));

        addRemoveObjectsControl.Container = containerModel;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        containerModel.MemberDefinitions.Clear();
        foreach (ContainerMemberDefinition definition in addRemoveObjectsControl.MemberDefinitions)
        {
            containerModel.MemberDefinitions.Add(definition);
        }

        dal.SetDefinitions(containerModel);

        ReturnBack();
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        ReturnBack();
    }

    protected void ReturnBack()
    {
        string url = "~/Orion/Admin/Containers/Default.aspx";
        if (Request["returnUrl"] != null)
            url = UrlHelper.FromSafeUrlParameter(Request["returnUrl"]);

        this.Response.Redirect(url);
    }
}
