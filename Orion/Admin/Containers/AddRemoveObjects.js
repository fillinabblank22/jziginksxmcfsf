Ext.namespace('SW');
Ext.namespace('SW.Core');

SW.Core.containers = function () {
    ORION.prefix = "Core_Containers_";

    var batchSize = 100;
    var batchDelay = 10;

    var selectorModel;
    var dataStore;
    var grid;
    var gridPanel;
    var initialized;
    var defaultLoad = true;
    var tree;
    var treePanel;
    var itemsToDelete;
    var itemsToAdd;
    var cycleDependentItems;
    var exceedNestedLevelItems;
    var memberIds;
    var circledGroupName;
    var mask;
    var someAlreadyExist;

    var gridItemsFieldClientID = '';
    var currentItemFieldClientID = '';
    var addToGroupButtonClientID = '';
    var removeFromGroupButtonClientID = '';
    var currentContainerID;
    var parentContainerID;
    var reloadGroups = false;
    var currentContainerUri;
    var parentContainerUri;
    var currentContainerNestedLevel = 0;
    var currentContainerNestedLevelDown = 0;
    var groupName = '';
    var initialData = new Array();
    var addDynamicQueryPostBack;
    var editDynamicQueryPostBack;

    var maxGroupNestedLevel = 5;

    var nodesToRemove; // remove these nodes at the end of the last batch

    function renderName(value, meta, record) {
        if (record.data.IsDynamicQuery) {
            return String.format('<span class="entityIconBox"><img src="/Orion/images/Icon.DynamicQuery.gif" /></span> {0}', value);
        } else {
            return String.format('<span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={0}&amp;id={1}&amp;status={2}&amp;size=small" /></span> {3} ', record.data.Entity, record.data.Definition.split('=').pop(), record.data.MemberStatus, value);
        }
    }

    function createDataTreeLoader(config) {
        var defaultConfig = {
            handleResponse: function (response) {
                this.transId = false;
                var a = response.argument;
                var responseObject = Ext.decode(response.responseText);

                if (responseObject.errorControl && responseObject.errorControl != '') {
                    showSwisError(responseObject.errorControl);
                }

                response.responseData = responseObject.data;

                this.processResponse(response, a.node, a.callback, a.scope);
                this.fireEvent("load", this, a.node, response);
            }
        };

        var treeLoader = new Ext.tree.TreeLoader($.extend({}, config, defaultConfig));

        return treeLoader;
    }

    var showSwisError = function (errorControlHtml) {
        var swisErrorWrapper = $('.swis-error-wrapper');
        if (swisErrorWrapper.length > 0) {
            swisErrorWrapper.html(errorControlHtml);
            swisErrorWrapper.show();
        }
    };

    var hideSwisError = function () {
        var swisErrorWrapper = $('.swis-error-wrapper');
        if (swisErrorWrapper.length > 0) {
            swisErrorWrapper.hide();
        }
    };

    GetGridState = function () {
        // get all definitions (excluding dynamic query) that are used in grid
        var gridState = [];
        grid.store.each(function (record) { if (!record.data.IsDynamicQuery) gridState.push(record.data.Definition); });

        if (currentContainerUri)
            gridState.push(currentContainerUri);
        else if (currentContainerID)
            gridState.push('swis://./Orion/Orion.Groups/ContainerID=' + currentContainerID);
        if (currentContainerID === 0 && parentContainerUri)
            gridState.push(parentContainerUri);

        return gridState;
    };

    ShowAddMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_32;E=js}"
        });
        mask.show();

        grid.store.suspendEvents(false);
    };

    ShowRemoveMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_33;E=js}"
        });
        mask.show();

        grid.store.suspendEvents(false);
    };

    HideMask = function () {
        grid.store.sort('Name', 'ASC');
        grid.store.resumeEvents();
        grid.store.fireEvent('datachanged');
        mask.hide();

        RefreshTreeIfEmpty();
    };

    RefreshTreeIfEmpty = function () {
        // if last item in tre is "There are XX more items" (entity attribute is empty), relaod tree
        if ((tree.root.childNodes.length == 1) && (!tree.root.childNodes[0].attributes.entity)) {
            LoadGroups();
        }
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        var isQuery = false;
        if (count == 1) {
            isQuery = selItems.selections.items[0].data.IsDynamicQuery;
        }

        map.EditQueryButton.setDisabled(!isQuery);
        map.DynamicQueryResultsButton.setDisabled(!isQuery);
    };

    GetGroupByProperties = function (itemType, onSuccess) {
        ORION.callWebService("/Orion/Services/Containers.asmx", "GetGroupByProperties", { entityType: itemType }, onSuccess);
    };

    LoadGroupByProperties = function (selectValue) {
        CancelSearch(false);
        var itemType = $("#itemTypeSelect option:selected").val();
        ORION.Prefs.save('ItemType', itemType);

        if (itemType) {
            GetGroupByProperties(itemType, function (result) {
                var groupBySelect = $("#groupBySelect");
                groupBySelect.empty();

                for (var name in result) {
                    if (name == selectValue && defaultLoad)
                        groupBySelect.append('<option value="' + name + '" selected="selected">' + result[name] + '</option>');
                    else
                        groupBySelect.append('<option value="' + name + '">' + result[name] + '</option>');
                }
                defaultLoad = false;

                groupBySelect.unbind('change').change(function () {
                    CancelSearch(false);
                    LoadGroups();
                });
                groupBySelect.change();
            });
        }
    };

    GetGroups = function (itemType, groupBy, searchValue, onSuccess) {
        ORION.callWebService("/Orion/Services/Containers.asmx", "GetEntityGroupsAppendErrors", { entityType: itemType, groupByProperty: groupBy, searchValue: searchValue, excludeDefinitions: GetGridState() }, onSuccess);
    };

    GetCurrentContainerNestedLevel = function (memberIds, grouId, onSuccess) {
        SW.Core.Services.callWebService("/Orion/Services/Containers.asmx", "GetContainerNestedLevel", { memberIds: memberIds, grouId: grouId }, onSuccess);
    };

    // Changes icon on gived TreeNode in runtime
    SetNodeIcon = function (node, icon) {
        if ((node.ui) && (node.ui.iconNode)) {
            node.ui.iconNode.src = icon;
        }
    };

    CancelSearch = function (reloadTree) {
        $('#searchBox').val('');

        if (reloadTree)
            LoadGroups();
    };

    DoSearch = function () {
        LoadGroups();
    };

    GetLabelForNode = function (name, count) {
        return String.format("{0} ({1})", Ext.util.Format.htmlEncode(name), count);
    };

    UpdateGroupItemsCount = function (node, newCount) {
        node.attributes.groupCount = newCount;
        node.setText(GetLabelForNode(node.attributes.groupName, node.attributes.groupCount));
    };

    LoadEntitiesGrouped = function (itemType, groupBy, searchValue, result, notInitialLoad, parentNode) {
        hideSwisError();

        if (result.IsError) {
            showSwisError(result.ErrorControl);
        }

        var bInitialLoad = true;
        if (typeof notInitialLoad != "undefined") {
            bInitialLoad = !notInitialLoad;
        }

        if (bInitialLoad) {
            ClearTree();
        }

        var myLoader = null;
        for (var i = 0; i < result.Groups.length; i++) {
            var item = result.Groups[i];
            if (item.Count > 0) {

                myLoader = createDataTreeLoader({
                    dataUrl: '/Orion/Admin/Containers/ContainerObjectsTreeProvider.ashx',
                    listeners: {
                        // init load event handler
                        beforeload: {
                            fn: function (treeLoader, node) {
                                this.baseParams.entityType = itemType;
                                this.baseParams.groupBy = groupBy;
                                this.baseParams.searchValue = searchValue;
                                if (bInitialLoad) {
                                    this.baseParams.value = node.attributes.value;
                                }
                                else {
                                    this.baseParams.value = parentNode.attributes.value;
                                }

                                this.baseParams.excludeDefinitions = JSON.stringify(GetGridState());
                                this.baseParams.currentContainerID = currentContainerID;
                                this.baseParams.parentContainerID = parentContainerID;
                                if (memberIds)
                                    this.baseParams.memberDefinitions = memberIds;

                                this.baseParams.initialLoad = bInitialLoad;
                                SetNodeIcon(node, '/Orion/images/AJAX-Loader.gif', false);
                            }
                        },
                        // after load event handler
                        load: {
                            fn: function (treeLoader, node, response) {
                                node.expanded = true;
                                SetNodeIcon(node, node.attributes.icon);

                                if (bInitialLoad) {
                                    Ext.each(node.childNodes, function (child) {
                                        if (((typeof child.attributes.attributes) != "undefined") && child.attributes.attributes.loadNextItems) {
                                            child.on("click", function () { node.removeChild(child); LoadEntitiesGrouped(itemType, groupBy, searchValue, result, true, node); });
                                        }
                                    });
                                }
                                else {
                                    var nresult = response.responseData;
                                    for (var i = 0; i < nresult.length; i++) {
                                        var newNode = new Ext.tree.TreeNode(nresult[i]);
                                        parentNode.appendChild(newNode);
                                    }
                                }
                            }
                        }
                    }
                });

                var node = new Ext.tree.AsyncTreeNode({
                    id: 'tree-node-' + (i),
                    text: GetLabelForNode(item.Name, item.Count),
                    value: item.Name,
                    groupCount: item.Count,
                    groupName: item.Name,
                    icon: String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small', itemType, item.Status),
                    allowDrag: true,
                    isGroup: true,
                    leaf: false,
                    checked: false,
                    propagateCheck: true,
                    listeners: {
                        'checkchange': function (node, checked) {
                            if (node.attributes.propagateCheck) {
                                node.eachChild(function (n) {
                                    n.attributes.propagateCheck = false;
                                    n.checked = checked;
                                    n.getUI().toggleCheck(checked);
                                    n.attributes.propagateCheck = true;
                                });
                            }

                            if (checked) {
                                node.getUI().addClass('x-tree-selected');
                            } else {
                                node.getUI().removeClass('x-tree-selected');
                            }
                        }
                    },
                    loader: myLoader
                });

                node.attributes.loader.baseParams.icon = node.attributes.icon;

                if (bInitialLoad) {
                    tree.root.appendChild(node);
                }
            }
            else {
                tree.root.appendChild(new Ext.tree.TreeNode({
                    text: item.Name,
                    allowDrag: false,
                    icon: '/Orion/StatusIcon.ashx?entity=&status=&size=small'
                }));
            }
        }

        if (!bInitialLoad) {
            myLoader.load(new Ext.tree.TreeNode(), null, null);
        }
    };

    LoadGroups = function () {
        hideSwisError();
        var itemType = GetItemType();
        var groupBy = GetGroupBy();
        var searchValue = GetSearchValue();
        var loadText = "@{R=Core.Strings;K=WEBJS_VB0_1;E=js}";

        if (searchValue) {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchCancel.gif');
            $('#searchButton').unbind('click');
            $('#searchButton').click(function () { CancelSearch(true) });
            loadText = "@{R=Core.Strings;K=WEBJS_TM0_34;E=js}";
        } else {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchIcon.gif');
            $('#searchButton').unbind('click');
            $('#searchButton').click(DoSearch);
        }

        ClearTree();
        tree.root.appendChild({ text: loadText, icon: '/Orion/images/AJAX-Loader.gif', expandable: false, leaf: true });

        if ((itemType) && (groupBy)) {
            GetGroups(itemType, groupBy, searchValue, function (result) { LoadEntitiesGrouped(itemType, groupBy, searchValue, result, false) });
        } else if (itemType) {
            // no grouping - load entites directly
            LoadEntitiesNonGrouped(itemType, searchValue);
        }
    };

    LoadEntitiesNonGrouped = function (itemType, searchValue, notInitialLoad) {
        hideSwisError();
        var bInitialLoad = true;
        if (typeof notInitialLoad != "undefined") {
            bInitialLoad = !notInitialLoad;
        }

        var loader = createDataTreeLoader({
            dataUrl: '/Orion/Admin/Containers/ContainerObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, node) {
                        this.baseParams.entityType = itemType;
                        this.baseParams.searchValue = searchValue;
                        this.baseParams.excludeDefinitions = JSON.stringify(GetGridState());
                        this.baseParams.currentContainerID = currentContainerID;
                        this.baseParams.parentContainerID = parentContainerID;
                        if (memberIds)
                            this.baseParams.memberDefinitions = memberIds;
                        this.baseParams.initialLoad = bInitialLoad;
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, node, response) {
                        if (bInitialLoad) {
                            ClearTree();
                        }

                        var result = response.responseData;
                        for (var i = 0; i < result.length; i++) {
                            var child = new Ext.tree.TreeNode(result[i]);
                            if (bInitialLoad) {
                                if (((typeof child.attributes.attributes) != "undefined") && child.attributes.attributes.loadNextItems) {
                                    child.on("click", function () { tree.root.removeChild(child); LoadEntitiesNonGrouped(itemType, searchValue, true); });
                                }
                            }

                            tree.root.appendChild(child);
                        }
                    }
                }
            }
        });
        loader.load(new Ext.tree.TreeNode(), null, null);
    };

    ClearTree = function () {
        while (tree.root.firstChild) {
            tree.root.removeChild(tree.root.firstChild);
        }
    };

    RefreshItemsData = function () {
        var data = '[';
        var first = true;
        grid.store.each(function (record) {
            if (!first) {
                data += ",";
            }
            data += Ext.util.JSON.encode(record.data);
            first = false;
        });
        data += ']';
        $('#' + gridItemsFieldClientID).val(data);
    };

    AddMember = function (store, records, index) {
        RefreshItemsData();
    };

    RemoveMember = function (store, records, index) {
        RefreshItemsData();
    };

    RemoveBatch = function () {
        if (itemsToDelete.length > 0) {
            for (var i = itemsToDelete.length - 1, j = 0; i >= 0, j < batchSize; i-- , j++) {
                if (i < 0)
                    break;
                grid.store.remove(itemsToDelete[i]);
                itemsToDelete.remove(itemsToDelete[i]);
            }
        }

        if (itemsToDelete.length > 0) {
            setTimeout(RemoveBatch, batchDelay);
        }
        else {
            LoadGroups();
            HideMask();
        }
    };

    RemoveSelectedItems = function (items) {
        ShowRemoveMask();
        itemsToDelete = items;
        RemoveBatch();
    };

    RemoveCheckedNodesFromGrid = function () {
        if (selectorModel.getCount() > 0) {
            Ext.Msg.confirm(
                "@{R=Core.Strings;K=WEBJS_TM0_37;E=js}", "@{R=Core.Strings;K=WEBJS_TM0_36;E=js}",
                function (btn, text) {
                    if (btn == "yes") {
                        RemoveSelectedItems(selectorModel.getSelections());
                    }
                }
            );
        }
    };

    ReloadTree = function () {
        var itemType = GetItemType();
        var groupBy = GetGroupBy();

        if (groupBy) {
            $.each(tree.root.childNodes, function (index, node) {
                if (node.loaded) {
                    node.attributes.loader.baseParams.expandAfterLoad = node.expanded;
                    node.reload(function () { }, node);
                }
            });
        } else {
            LoadEntitiesNonGrouped(itemType);
        }
    };


    GetItemType = function () {
        return $("#itemTypeSelect option:selected").val();
    };

    GetGroupBy = function () {
        return $("#groupBySelect option:selected").val();
    };

    GetSearchValue = function () {
        return $("#searchBox").val();
    };

    AddDynamicQuery = function () {
        addDynamicQueryPostBack();
    };

    EditDynamicQuery = function (item) {
        $('#' + currentItemFieldClientID).val(Ext.util.JSON.encode(item.data));
        editDynamicQueryPostBack();
    };


    InitDragDrop = function () {
        nodesToRemove = [];

        var gridDropTargetEl = grid.getView().el.dom.childNodes[0].childNodes[1];
        var gridDropTarget = new Ext.dd.DropTarget(gridDropTargetEl, {
            ddGroup: 'treeDDGroup',
            copy: false,
            notifyDrop: function (ddSource, e, data) {
                cycleDependentItems = []; exceedNestedLevelItems = [];
                if (data.node.attributes.isGroup) {
                    reloadGroups = true;
                    AddChildNodesToGrid(data.node);
                } else {
                    var parentNode = data.node.parentNode;

                    AddNodeToGrid(data.node);
                    data.node.remove(true);

                    if (parentNode != null && parentNode.childNodes.length == 0) {
                        parentNode.remove(true);
                    } else if (parentNode != null) {
                        UpdateGroupItemsCount(parentNode, parentNode.attributes.groupCount - 1);
                    }

                    RefreshTreeIfEmpty();
                }

                return true;
            }
        });
    };

    ExpandAndAdd = function (node, callback) {
        itemsToAdd = [];
        var loader = createDataTreeLoader({
            dataUrl: '/Orion/Admin/Containers/ContainerObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, n) {
                        this.baseParams.entityType = GetItemType();
                        this.baseParams.groupBy = GetGroupBy();
                        this.baseParams.searchValue = GetSearchValue();
                        this.baseParams.value = n.attributes.value;
                        this.baseParams.excludeDefinitions = JSON.stringify(GetGridState());
                        this.baseParams.currentContainerID = currentContainerID;
                        this.baseParams.parentContainerID = parentContainerID;
                        if (memberIds)
                            this.baseParams.memberDefinitions = memberIds;
                        this.baseParams.initialLoad = true;
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, n, response) {
                        var result = response.responseData;
                        for (var i = 0; i < result.length; i++) {
                            var child = new Ext.tree.TreeNode(result[i]);
                            if (cycleDependentItems.length > 0 && jQuery.inArray(child.attributes.text, cycleDependentItems) !== -1) {
                                //skip items that were marked as cycle dependent
                                continue;
                            }
                            itemsToAdd.push(child);
                        }
                        if (itemsToAdd.length > 0) {
                            AddBatch(function () { ExpandAndAdd(node, callback); });
                        } else {
                            node.remove(true);

                            if (callback)
                                callback();
                        }
                    }
                }
            }
        });
        loader.load(node, null, null);
    };

    AddBatch = function (callback) {
        ShowAddMask();
        if (itemsToAdd.length > 0) {
            var blankRecord = Ext.data.Record.create(grid.store.fields);

            if (itemsToAdd.length > 0) {
                for (var i = itemsToAdd.length - 1, j = 0; i >= 0, j < batchSize; i-- , j++) {
                    if (i < 0)
                        break;
                    var child = itemsToAdd[i];

                    if (!child.attributes.entity) {
                        itemsToAdd.remove(child);
                        continue;
                    }

                    var record = new blankRecord({
                        Definition: child.attributes.id,
                        Name: child.attributes.text,
                        FullName: child.attributes.fullName,
                        MemberStatus: child.attributes.status,
                        Entity: child.attributes.entity,
                        IsDynamicQuery: 0
                    });

                    var parentNode = child.parentNode;
                    if (grid.store.findExact("Definition", record.data.Definition) != -1) {
                        someAlreadyExist = true;
                    } else {
                        var childContainerNestedlevel = parseInt(child.attributes.nestedLevel);
                        if (childContainerNestedlevel === -1) {
                            //Found cycle dependency
                            itemsToAdd.remove(child);
                            cycleDependentItems.push(child.attributes.text);
                            continue;
                        }

                        if (child.attributes.entity === 'Orion.Groups' && maxGroupNestedLevel > 0 && currentContainerNestedLevel + childContainerNestedlevel + 1 > maxGroupNestedLevel) {
                            exceedNestedLevelItems.push(child.attributes.text);
                        }

                        grid.store.add(record);
                        child.remove();

                        if (parentNode != null) {
                            UpdateGroupItemsCount(parentNode, parentNode.attributes.groupCount - 1);
                        }
                    }

                    itemsToAdd.remove(child);

                    if ((parentNode) && (!parentNode.hasChildNodes())) {
                        nodesToRemove.push(parentNode);
                    }
                }
            }
        }
        if (itemsToAdd.length > 0) {
            setTimeout(function () { AddBatch(callback) }, batchDelay);
        }
        else {
            if (callback) {
                callback();
            }

            if (nodesToRemove != null) {
                $.each(nodesToRemove, function (index, node) { node.remove(true); });
                nodesToRemove = [];
            }
        }
    };

    VerifyAndDisplayNestedLevelMessage = function () {
        if (cycleDependentItems.length > 0 && exceedNestedLevelItems.length > 0) {
            Ext.MessageBox.show({
                title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_11;E=js}", cycleDependentItems.join()) + "<br>" +
                    String.format("@{R=Core.Strings.2;K=WEBJS_YK0_12;E=js}", exceedNestedLevelItems.join(), maxGroupNestedLevel),
                icon: Ext.MessageBox.WARNING,
                buttons: Ext.MessageBox.OK

            });
        }
        else if (cycleDependentItems.length > 0) {
            Ext.MessageBox.show({
                title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_11;E=js}", cycleDependentItems.join()),
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.MessageBox.OK

            });
        }
        else if (exceedNestedLevelItems.length > 0) {
            Ext.MessageBox.show({
                title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_12;E=js}", exceedNestedLevelItems.join(), maxGroupNestedLevel),
                icon: Ext.MessageBox.WARNING,
                buttons: Ext.MessageBox.OK

            });
        }
    };

    AddCheckedNodesToGrid = function () {
        someAlreadyExist = false;
        itemsToAdd = [];
        nodesToRemove = [];
        var anythingSelected = false;

        ShowAddMask();

        tree.root.eachChild(function (n) {
            if (n.attributes.isGroup) {
                if (n.attributes.checked) {
                    anythingSelected = true;
                    ExpandAndAdd(n, AddCheckedNodesToGrid);
                } else {
                    n.eachChild(function (child) {
                        if (child.attributes.checked) {
                            anythingSelected = true;
                            itemsToAdd.push(child);
                        }
                    });
                }
            } else if (n.attributes.checked) {
                anythingSelected = true;
                itemsToAdd.push(n);
            }
        });

        if (!anythingSelected)
            AddObjectsResultsHandler();

        if (itemsToAdd.length > 0)
            AddBatch(AddObjectsResultsHandler);
    };

    AddChildNodesToGrid = function (node) {
        someAlreadyExist = false;

        ShowAddMask();

        grid.store.suspendEvents(false);

        ExpandAndAdd(node, AddObjectsResultsHandler);
    };

    AddObjectsResultsHandler = function () {
        HideMask();
        if (reloadGroups) {
            VerifyAndDisplayNestedLevelMessage();
            if (cycleDependentItems.length > 0)
                LoadGroups();
            reloadGroups = false;
        }
    };

    AddNodeToGrid = function (node) {
        var blankRecord = Ext.data.Record.create(grid.store.fields);

        var record = new blankRecord({
            Definition: node.attributes.id,
            Name: node.attributes.text,
            FullName: node.attributes.fullName,
            MemberStatus: node.attributes.status,
            Entity: node.attributes.entity,
            IsDynamicQuery: 0
        });
        if (grid.store.findExact("Definition", record.data.Definition) != -1) {
            Ext.Msg.alert("@{R=Core.Strings;K=WEBJS_TM0_40;E=js}", "@{R=Core.Strings;K=WEBJS_TM0_39;E=js}");
            return;
        }

        var childContainerNestedlevel = parseInt(node.attributes.nestedLevel);
        if (childContainerNestedlevel === -1) {
            Ext.MessageBox.show({
                title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_11;E=js}", node.attributes.text),
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.MessageBox.OK

            });

            LoadGroups();
            return;
        }

        if (node.attributes.entity === 'Orion.Groups' && maxGroupNestedLevel > 0 && currentContainerNestedLevel + childContainerNestedlevel + 1 > maxGroupNestedLevel) {
            Ext.MessageBox.show({
                title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_12;E=js}", node.attributes.text, maxGroupNestedLevel),
                icon: Ext.MessageBox.WARNING,
                buttons: Ext.MessageBox.OK

            });
        }
        grid.store.addSorted(record);
    };

    ToggleAllCheckboxes = function (value) {
        tree.root.eachChild(function (n) {
            n.attributes.propagateCheck = false;
            n.checked = value;
            n.getUI().toggleCheck(value);
            n.attributes.propagateCheck = true;

            if (n.attributes.isGroup) {
                n.eachChild(function (child) {
                    child.attributes.propagateCheck = false;
                    child.checked = value;
                    child.getUI().toggleCheck(value);
                    child.attributes.propagateCheck = true;
                });
            }
        });
    };

    SelectAllInTree = function () {
        ToggleAllCheckboxes(true);
    };

    SelectNoneInTree = function () {
        ToggleAllCheckboxes(false);
    };

    InitTree = function () {
        selectPanel = new Ext.Container({
            applyTo: 'GroupItemsSelector',
            region: 'north',
            height: 137,
            layout: 'fit'
        });

        var toolbar = new Ext.Toolbar({
            height: 27,
            region: 'north',
            items: [{
                id: 'SelectAllButton',
                text: '@{R=Core.Strings;K=WEBJS_VB0_36;E=js}',
                iconCls: 'selectAllButton',
                handler: function () {
                    //if (IsDemoMode()) return DemoModeMessage();
                    SelectAllInTree();
                }
            }, '-', {
                id: 'SelectNoneButton',
                text: '@{R=Core.Strings;K=WEBJS_VB0_38;E=js}',
                iconCls: 'selectNoneButton',
                handler: function () {
                    //if (IsDemoMode()) return DemoModeMessage();
                    SelectNoneInTree();
                }
            }]
        });

        tree = new Ext.tree.TreePanel({
            id: 'TreeExtPanel',
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDrag: true,
            height: 300, // border layout somehow doesn't work so we have to set height manually
            region: 'center',
            root: {
                text: '',
                id: 'root'
            },
            rootVisible: false,
            ddGroup: "treeDDGroup"
        });

        treePanel = new Ext.Panel({
            title: '@{R=Core.Strings;K=WEBJS_TM0_31;E=js}',
            frame: true,
            region: 'west',
            width: 280,
            split: true,
            items: [
                selectPanel,
                toolbar,
                tree
            ]
        });
    };

    InitGrid = function () {
        // when changing this record, change also DataGridRecord in AddRemoveObjectsControl.ascx.cs
        record = Ext.data.Record.create([
            { name: 'Definition' },
            { name: 'Name', sortType: Ext.data.SortTypes.asUCString },
            { name: 'FullName' },
            { name: 'MemberStatus' },
            { name: 'Entity' },
            { name: 'IsDynamicQuery' }
        ]);
        arrayReader = new Ext.data.ArrayReader({
                idIndex: 0
            },
            record);

        dataStore = new Ext.data.Store({
            reader: arrayReader,
            sortInfo: { field: 'Name', direction: 'ASC' }
        });

        dataStore.loadData(initialData);

        dataStore.on("add", AddMember);
        dataStore.on("remove", RemoveMember);
        dataStore.on('datachanged', RefreshItemsData);

        selectorModel = new Ext.grid.CheckboxSelectionModel();

        grid = new Ext.grid.GridPanel({
            store: dataStore,

            columns: [
                selectorModel,
                { id: 'FullName', header: '@{R=Core.Strings;K=WEBJS_TM0_42;E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'FullName', renderer: renderName }
            ],
            autoExpandColumn: 'FullName',

            sm: selectorModel,

            viewConfig: {
                forceFit: false
            },

            border: true,
            frame: true,
            hideHeaders: true,
            stripeRows: true,
            trackMouseOver: false,
            ddGroup: 'treeDDGroup',
            region: 'center',
            title: (groupName) ? Ext.util.Format.htmlEncode(groupName) : '@{R=Core.Strings;K=WEBJS_TM0_41;E=js}',
            loadMask: { msg: '@{R=Core.Strings;K=WEBJS_TM0_35;E=js}' },

            tbar: [{
                id: 'AddQueryButton',
                text: '@{R=Core.Strings;K=WEBJS_TM0_28;E=js}',
                tooltip: '@{R=Core.Strings;K=WEBJS_TM0_38;E=js}',
                iconCls: 'addDynamicQuery',
                handler: function () {
                    //if (IsDemoMode()) return DemoModeMessage();
                    AddDynamicQuery();
                }
            }, '-', {
                id: 'EditQueryButton',
                text: '@{R=Core.Strings;K=WEBJS_TM0_29;E=js}',
                tooltip: '@{R=Core.Strings;K=WEBJS_TM0_30;E=js}',
                iconCls: 'editDynamicQuery',
                handler: function () {
                    //if (IsDemoMode()) return DemoModeMessage();
                    EditDynamicQuery(grid.getSelectionModel().getSelected());
                }
            }, '-', {
                id: 'DynamicQueryResultsButton',
                text: '@{R=Core.Strings;K=WEBJS_TM0_14;E=js}',
                tooltip: '@{R=Core.Strings;K=WEBJS_TM0_15;E=js}',
                iconCls: 'viewDynamicQueryObjects',
                handler: function () {
                    //if (IsDemoMode()) return DemoModeMessage();
                    var item = grid.getSelectionModel().getSelected();
                    DynamicQueryPreview(item.data.Name, item.data.Definition); // from external file DynamicQueryPreview.js
                }
            }, '-', {
                text: '@{R=Core.Strings;K=WEBJS_VB0_36;E=js}',
                iconCls: 'selectAllButton',
                handler: function () {
                    selectorModel.selectAll();
                }
            }, '-', {
                text: '@{R=Core.Strings;K=WEBJS_VB0_38;E=js}',
                iconCls: 'selectNoneButton',
                handler: function () {
                    selectorModel.clearSelections();
                }
            }]
        });

        var addButtonPanel = new Ext.Panel({
            border: false,
            region: 'west',
            width: 95,
            contentEl: 'AddButtonPanel'
        });

        gridPanel = new Ext.Panel({
            id: 'AddButtonPanelExtPanel',
            region: 'center',
            layout: 'border',
            border: false,
            items: [addButtonPanel, grid]
        });

        grid.getSelectionModel().on("selectionchange", UpdateToolbarButtons);
    };

    InitLayout = function () {
        var panel = new Ext.Container({
            id: 'MainExtPanel',
            renderTo: 'ContainerMembersTable',
            height: 500,
            layout: 'border',
            items: [treePanel, gridPanel]
        });
        $(window).bind('resize', function () {
            panel.doLayout();
        });
    };

    return {
        SetGridItemsFieldClientID: function (id) {
            gridItemsFieldClientID = id;
        },
        SetCurrentItemFieldClientID: function (id) {
            currentItemFieldClientID = id;
        },
        SetAddToGroupButtonClientID: function (id) {
            addToGroupButtonClientID = id;
        },
        SetRemoveFromGroupButton: function (id) {
            removeFromGroupButtonClientID = id;
        },
        SetCurrentContainerID: function (id) {
            currentContainerID = id;
        },
        SetParentContainerID: function (id) {
            parentContainerID = id;
        },
        SetCurrentContainerUri: function (uri) {
            currentContainerUri = uri;
        },
        SetCurrentNestedLevel: function (level) {
            currentContainerNestedLevelDown = level;
        },
        SetParentContainerUri: function (uri) {
            parentContainerUri = uri;
        },
        SetMaxGroupNestedLevel: function (maxNestedLevel) {
            maxGroupNestedLevel = maxNestedLevel;
        },
        SetGroupName: function (name) {
            groupName = name;
        },
        SetMemberIds: function (members) {
            memberIds = JSON.stringify(members);
        },
        SetCircledGroupName: function (crclGroupName) {
            circledGroupName = crclGroupName;
        },
        AddToInitialData: function (item) {
            initialData.push(item);
        },
        SetAddDynamicQueryPostBack: function (fn) {
            addDynamicQueryPostBack = fn;
        },
        SetEditDynamicQueryPostBack: function (fn) {
            editDynamicQueryPostBack = fn;
        },
        ValidateGrid: function () {
            var isSameGroup = false;
            var currentGroupName;
            var isCircledGroupInMembers = false;
            grid.store.each(function (record) {
                if (record.id == currentContainerUri) {
                    isSameGroup = true;
                    currentGroupName = record.data.Name;
                    return false;
                }
                if (circledGroupName && circledGroupName.length > 0 && record.data.Name === circledGroupName) {
                    isCircledGroupInMembers = true;
                    return false;
                }
            });
            if (isSameGroup) {
                //group is added to itself
                Ext.MessageBox.show({
                    title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                    msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_14;E=js}", Ext.util.Format.htmlEncode(currentGroupName)),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.MessageBox.OK
                });
                return false;
            }
            else if (circledGroupName && circledGroupName.length > 0 && !(!isCircledGroupInMembers && currentContainerNestedLevelDown === -1)) {
                //circkled group is in current group's member tree
                //like 1->2->3->4->5->4 or 6->7->6
                Ext.MessageBox.show({
                    title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                    msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_15;E=js}", Ext.util.Format.htmlEncode(groupName), Ext.util.Format.htmlEncode(circledGroupName)),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.MessageBox.OK
                });
                return false;
            }
            else
                return true;
        },
        init: function () {
            if (initialized)
                return;

            initialized = true;

            if (memberIds != null) {
                GetCurrentContainerNestedLevel(memberIds,
                    currentContainerID === 0 ? parentContainerID : currentContainerID,
                    function (result) {
                        currentContainerNestedLevel = result;
                        if (currentContainerID === 0 && parentContainerID > 0) {
                            //adding new group inside existing one (parent)
                            //current nested level should be increased
                            currentContainerNestedLevel = currentContainerNestedLevel + 1;
                        }
                    });
            }

            InitTree();
            InitGrid();
            InitLayout();
            RefreshItemsData();
            InitDragDrop();

            UpdateToolbarButtons();
            $("#itemTypeSelect").val(ORION.Prefs.load("ItemType", "Orion.Nodes")).change(function (evt) {
                var grpProp = "Orion.Nodes.Vendor";
                if (evt != null && evt.target != null && evt.target.value != "Orion.Nodes") grpProp = evt.target.value + ".Status";
                defaultLoad = true;
                LoadGroupByProperties(grpProp);
            });

            $("#itemTypeSelect").change();
            $('#AddButtonPanel').height($('#ContainerMembersTable').height());
            $('#' + addToGroupButtonClientID).click(function () {
                reloadGroups = true; cycleDependentItems = []; exceedNestedLevelItems = []; AddCheckedNodesToGrid();
            });
            $('#' + removeFromGroupButtonClientID).click(RemoveCheckedNodesFromGrid);
            $('#searchButton').click(DoSearch);
            $('#searchBox').keydown(function (event) {
                if (event.keyCode == '13') {
                    event.preventDefault();
                    DoSearch();
                } else {
                    $('#searchButton img').attr('src', '/Orion/images/Button.SearchIcon.gif');
                    $('#searchButton').unbind('click');
                    $('#searchButton').click(DoSearch);
                }
            });
        }
    };
}();

Ext.onReady(SW.Core.containers.init, SW.Core.containers);
