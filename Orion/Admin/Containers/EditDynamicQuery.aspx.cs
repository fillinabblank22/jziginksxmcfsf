using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Shared;
using ContainerMemberDefinition = SolarWinds.Orion.Core.Common.Models.ContainerMemberDefinition;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Containers_EditDynamicQuery : System.Web.UI.Page, IBypassAccessLimitation
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log(); 
    private const string DefaultReturnUrl = "~/Orion/Admin/Containers/Default.aspx";

    private bool fisrtCondition = true;
    private List<DynamicQueryCondition> conditions;
    private List<EntityProperty> entityProperties;
    private ContainersMetadataDAL dal;
    private string returnUrl;
    private string originalDefinition;

    public bool FirstCondition 
    {
        get
        {
            if (this.fisrtCondition)
            {
                this.fisrtCondition = false;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public bool NoValidatorIsShown { get; set; }

    private void CheckNodeRole()
    {
        if (!Profile.AllowNodeManagement)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_180) }); 
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckNodeRole();

        ((Orion_Admin_Containers_ContainerAdminPage)this.Master).HelpFragment = "OrionCorePHGroups_BuildDynamicQuery";

        this.NoValidatorIsShown = true;

        if (Session.IsNewSession)
        {
            Response.Redirect(DefaultReturnUrl);
        }
        
        conditions = new List<DynamicQueryCondition>();
        entityProperties = new List<EntityProperty>();
        dal = new ContainersMetadataDAL();

        if (this.Request["returnUrl"] != null)
        {
            this.returnUrl = UrlHelper.FromSafeUrlParameter(this.Request["returnUrl"]);
        }
        else
        {
            this.returnUrl = DefaultReturnUrl;
        }

        originalDefinition = (Request["def"] != null) ? HttpUtility.UrlDecode(Request["def"]) : string.Empty;

        if (!IsPostBack)
        {
            entityBox.DataTextField = "Value";
            entityBox.DataValueField = "Key";

            var containerDerivedEntities = dal.GetContainerSubEntities().Keys;
            entityBox.DataSource = dal.GetAvailableContainerMemberEntities().Where(pair => !containerDerivedEntities.Contains(pair.Key)).OrderBy(x => x.Value); 
            entityBox.DataBind();

            dynamicQueryPreviewPanel.Visible = false;

            if (!String.IsNullOrEmpty(originalDefinition))
            {
                // editing dynamic query
                nameBox.Text = WebSecurityHelper.SanitizeAngular(HttpUtility.UrlDecode(Request["name"]));

                entityBox.SelectedValue = DynamicQueryHelper.GetEntity(originalDefinition);
                IEnumerable<DynamicQueryCondition> tmp = DynamicQueryHelper.GetConditions(originalDefinition);
                if (tmp != null)
                {
                    conditions.AddRange(tmp);
                }
            }
            else
            {
                var item = entityBox.Items.FindByValue("Orion.Nodes");
                if (item != null)
                {
                    entityBox.SelectedIndex = entityBox.Items.IndexOf(item);
                }
            }
        }

        entityProperties.AddRange(dal.GetEntityProperties(entityBox.SelectedValue, ContainersMetadataDAL.PropertyType.FilterBy));

        if (IsPostBack)
        {
            foreach (RepeaterItem item in ConditionsRepeater.Items)
            {
                DropDownList propertyBox = item.FindControl("propertyBox") as DropDownList;
                DropDownList operatorBox = item.FindControl("operatorBox") as DropDownList;
                // this is required to allow client side modification of operators
                HiddenField operatorBoxValue = item.FindControl("operatorBoxValue") as HiddenField;
                TextBox valueBox = item.FindControl("valueBox") as TextBox;

                DynamicQueryCondition condition = new DynamicQueryCondition()
                {
                    Property = entityProperties.FirstOrDefault(p => p.FullyQualifiedName == propertyBox.SelectedValue),
                    Operator = DynamicQueryOperator.Operators.FirstOrDefault(op => op.Value == operatorBoxValue.Value)
                };
                if (condition.Property != null)
                {
                    string valueBox_Text = WebSecurityHelper.SanitizeAngular(valueBox.Text);
                    if (condition.Property.IsDateTime)
                    {
                        condition.SetDateTime(valueBox_Text, DateTimeKind.Local);
                    }
                    else
                    {
                        condition.Value = valueBox_Text;
                    }

                    // set operators to select box according to current condition
                    IEnumerable<DynamicQueryOperator> operators = DynamicQueryOperator.Operators;
                    if ((condition.Property.IsStatus) || (condition.Property.IsBoolean))
                        operators = DynamicQueryOperator.Operators.Where(o => o.IsForEnum);
                    else if ((condition.Property.IsNumeric) || (condition.Property.IsDateTime))
                        operators = DynamicQueryOperator.Operators.Where(o => o.IsForNumber);
                    else if (condition.Property.IsString)
                        operators = DynamicQueryOperator.Operators.Where(o => o.IsForString);

                    operatorBox.DataSource = operators.ToArray();
                    operatorBox.DataBind();
                    operatorBox.SelectedValue = condition.Operator;
                }
                conditions.Add(condition);
            }
        }
        else
        {
            foreach (DynamicQueryCondition condition in conditions)
            {
                condition.Property = entityProperties.FirstOrDefault(p => p.FullyQualifiedName == condition.Property.FullyQualifiedName);
            }
            RefreshRepeater();
        }
    }

    protected void AddCondition(object sender, EventArgs e)
    {
        conditions.Add(new DynamicQueryCondition());
        RefreshRepeater();
    }

    protected void EntityChanged(object sender, EventArgs e)
    {
        conditions.Clear();
        RefreshRepeater();
    }

    protected void Preview(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        dynamicQueryPreviewPanel.Visible = true;

        int maxRecords = ContainerHelper.GetContainerPreviewMaxCount();

        String filterDefinition = string.Empty;
        try
        {
            filterDefinition = DynamicQueryHelper.GetFilterDefinition(entityBox.SelectedValue, conditions);
            DataTable dt = SWISVerbsHelper.InvokeV3<DataTable>("Orion.ContainerMemberDefinition", "GetFirstNMembers", filterDefinition, maxRecords + 1);

            if (dt.Rows.Count > maxRecords)
            {
                if (dt.Rows.Count > 0)
                {
                    dt.Rows[dt.Rows.Count - 1].Delete();
                }

                this.MoreItemsLabel.Text = String.Format(Resources.CoreWebContent.WEBCODE_TM0_86, dt.Rows.Count);
                this.MoreItemsLabel.Visible = true;
            }
            else
            {
                this.MoreItemsLabel.Visible = false;
            }

            List<ContainerMember> members = new List<ContainerMember>();

            foreach (DataRow row in dt.Rows)
            {
                members.Add(new ContainerMember()
                {
                    Entity = row["MemberEntityType"].ToString(),
                    FullName = FullNameHelper.GenerateFullName(
                        (string[])row["MemberAncestorDisplayNames"],
                        (string[])row["MemberAncestorDetailsUrls"]),
                    MemberPrimaryID = EntityHelper.GetIdFromUri((string)row["MemberPrimaryIDUri"]),
                    Status = StatusInfo.GetStatus(Convert.ToInt32(row["Status"]))
                });
            }

            if (members.Count == 0)
            {
                members.Add(new ContainerMember()
                {
                    FullName = Resources.CoreWebContent.WEBCODE_VB0_308
                });
            }

            RefreshPreview(members);
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            log.ErrorFormat(
                "Error displaying dynamic query preview. Parameters: filterDefinition={0}", filterDefinition);
            log.Error(e);

            throw new Exception(String.Format(Resources.CoreWebContent.WEBCODE_VB0_309, ex.Detail.Message));
        }
    }

    protected void Save(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        ContainerMemberDefinition definition = new ContainerMemberDefinition()
        {
            MemberName = (string)DefaultSanitizer.SanitizeHtml(nameBox.Text),
            Definition = DynamicQueryHelper.GetFilterDefinition(entityBox.SelectedValue, conditions)
        };

        // add query to session if available
        if (Request["sid"] != null)
        {
            IList<ContainerMemberDefinition> members = (IList<ContainerMemberDefinition>)Session[Request["sid"]];
            if (members != null)
            {
                if (!String.IsNullOrEmpty(originalDefinition))
                {
                    members.Remove(members.FirstOrDefault(md => md.Definition == originalDefinition));
                }
                members.Add(definition);
                Session[Request["sid"]] = members;
            }
            else
            {
                GoBack();
            }
        }
        else if (Request["id"] != null)
        {
            // save query directly
            ContainersDAL cDal = new ContainersDAL();
            cDal.UpdateDefinition(Convert.ToInt32(Request["id"]), new MemberDefinitionInfo(definition.Definition, definition.MemberName));
        }

        GoBack();
    }

    protected void Cancel(object sender, EventArgs e)
    {
        GoBack();
    }

    private void GoBack()
    {
        this.Response.Redirect(returnUrl ?? DefaultReturnUrl);
    }

    protected void ConditionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem is DynamicQueryCondition)
        {
            DynamicQueryCondition condition = e.Item.DataItem as DynamicQueryCondition;

            DropDownList propertyBox = e.Item.FindControl("propertyBox") as DropDownList;
            DropDownList operatorBox = e.Item.FindControl("operatorBox") as DropDownList;
            // this is required to allow client side modification of operators
            HiddenField operatorBoxValue = e.Item.FindControl("operatorBoxValue") as HiddenField;
            TextBox valueBox = e.Item.FindControl("valueBox") as TextBox;

            propertyBox.DataTextField = "DisplayName";
            propertyBox.DataValueField = "FullyQualifiedName";
            propertyBox.DataSource = entityProperties;
            propertyBox.DataBind();

            IEnumerable<DynamicQueryOperator> operators = DynamicQueryOperator.Operators;

            if (condition.Property == null)
            {
                condition.Property = entityProperties.FirstOrDefault();
            }

            string value = WebSecurityHelper.SanitizeAngular(condition.Value);

            if (condition.Property != null)
            {
                propertyBox.SelectedValue = condition.Property.FullyQualifiedName;

                if ((condition.Property.IsStatus) || (condition.Property.IsBoolean))
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForEnum);
                else if ((condition.Property.IsNumeric) || (condition.Property.IsDateTime))
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForNumber);
                else if (condition.Property.IsString)
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForString);

                if (condition.Property.IsDateTime)
                {
                    value = condition.GetDateTime(DateTimeKind.Local);
                }
                else if (condition.Property.IsDecimal && value != null)
                {
                    // trim possible .0 that was added to definition for save
                    if (value.EndsWith(".0"))
                        value = value.Substring(0, value.Length - 2);
                }
            }

            operatorBox.DataTextField = "Label";
            operatorBox.DataValueField = "Value";
            operatorBox.DataSource = operators.ToArray();
            operatorBox.DataBind();
            operatorBox.SelectedValue = condition.Operator;
            // this is required to allow client side modification of operators
            operatorBoxValue.Value = condition.Operator.Value;

            valueBox.Text = value;
        }
    }

    protected void ConditionsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            conditions.RemoveAt(e.Item.ItemIndex);
            RefreshRepeater();
        }
    }

    protected void RefreshRepeater()
    {
        ConditionsRepeater.DataSource = conditions;
        ConditionsRepeater.DataBind();
    }

    protected void RefreshPreview(IEnumerable<ContainerMember> members)
    {
        PreviewRepeater.DataSource = members;
        PreviewRepeater.DataBind();
    }

    protected void ConditionsValidation(object source, ServerValidateEventArgs args)
    {
        if (this.conditions.Count == 0)
        {
            args.IsValid = false;
        }
    }

    protected void ConditionValidation(object source, ServerValidateEventArgs args)
    {
        // find current validator in repeater and process it
        for (int i = 0; i < ConditionsRepeater.Items.Count; i++)
        {
            CustomValidator validator = ConditionsRepeater.Items[i].FindControl("ConditionValidator") as CustomValidator;
            if (validator != source as CustomValidator)
                continue;

            validator.Text = string.Empty;

            if (!DynamicQueryHelper.ValidateCondition(conditions[i]))
            {
                validator.Text = conditions[i].Error;
                args.IsValid = false;
                this.NoValidatorIsShown = false;
                return;
            }
        }
    }
}
