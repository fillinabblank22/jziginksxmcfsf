Ext.namespace('SW');
Ext.namespace('SW.Core');

SW.Core.containers = function () {
    var selectorModel;
    var dataStore;
    var groupBox, viewModeBox;
    var initialGroupBy = 'none', initialViewMode = 'all';
    var initialPage = 1;
    var pagingToolbar;
    var grid;
    var initialized;
    var expandedItems = new Array();
    var availableEntities = new Array();
    var customPropertyNames = [];

    var currentItemFieldClientID = '';
    var currentPageFieldClientID = '';
    var currentGroupByFieldClientID = '';
    var addItemPostBack;
    var editItemPostBack;
    var addRemoveObjectsPostBack;

    var maxGroupNestedLevel = 5;
    var memberIds;

    var pageSize = 1;
    var userName = "";

    var loadingMask = null;

    function renderBool(value) {
        if (jQuery.type(value) === "null" || jQuery.type(value) === "undefined" || value.length === 0)
            return '';

        if (!value || value.toString() == '0' || value.toString() == 'false')
            return '@{R=Core.Strings;K=WEBJS_IB0_13;E=js}';
        return '@{R=Core.Strings;K=WEBJS_IB0_12;E=js}';
    }

    function renderFloat(value) {
        if (Ext.isEmpty(value) || value.length === 0)
            return value;
        return renderString(String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator));
    }

    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    function renderDateTime(value) {
        if (Ext.isEmpty(value) || value.length === 0)
            return '';

        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        var dateVal = new Date(value);
        if (dateVal && dateVal != 'Invalid Date') {
            return dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;
        return encodeHTML(value);
    }

    // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: "@{R=Core.Strings;K=WEBJS_TM0_1;E=js}"
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = { groupBy: groupBox.getValue(), viewMode: viewModeBox.getValue() };
        grid.store.baseParams = { start: (initialPage - 1) * pageSize, limit: pageSize };
        grid.store.on('load', function () {
            UpdateExpandStates();
            loadingMask.hide();
        });
        grid.store.on('datachanged', function () {
            jQuery("#myGridPanel div.x-grid3-row").parent().width('auto');
            jQuery("#myGridPanel div.x-grid3-row").width('auto');
        });
        grid.store.load();
    };

    // marks all expander icons with the right class and fills expandedItems array with expanded records ids
    UpdateExpandStates = function () {
        var clollapsedExpanders = $("img:visible[class=tree-grid-expander-collapse]").removeClass('tree-grid-expander-collapse').addClass('tree-grid-expander-expand');
        var previousRecord = null;
        var expanders = $("img:visible[class=tree-grid-expander-expand]");
        var expanderIndex = -1;

        grid.store.data.each(function (record, index, length) {
            if (record != null && record.data.IsExpandable == 1) {
                delete expandedItems[record.id];
            }

            if (previousRecord != null && previousRecord.data.Level < record.data.Level) {
                expandedItems[previousRecord.id] = true;
                expanders[expanderIndex].className = 'tree-grid-expander-collapse';
                changed = true;
            }

            previousRecord = record;

            if (record.data.IsExpandable == 1) {
                expanderIndex++;
            }
        });
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        var isGroup = false;
        var isQuery = false;
        if (count == 1) {
            isGroup = selItems.selections.items[0].data.IsGroup;
            isQuery = selItems.selections.items[0].data.IsQuery;
        }

        map.AddButton.setDisabled(true);
        map.EditButton.setDisabled(true);
        map.AddRemoveObjectsButton.setDisabled(true);
        map.DeleteButton.setDisabled(true);
        map.DynamicQueryResultsButton.setDisabled(true);

        if (count === 0) {
            map.AddButton.setDisabled(false);
        } else if (count === 1) {
            if (isGroup) {
                map.AddButton.setDisabled(false);
                map.EditButton.setDisabled(false);
                map.AddRemoveObjectsButton.setDisabled(false);
                map.DeleteButton.setDisabled(false);
            } else if (isQuery) {
                map.EditButton.setDisabled(false);
                map.DynamicQueryResultsButton.setDisabled(false);
            }
        } else {
            var items = grid.getSelectionModel().getSelections();
            var enableDelete = false;
            Ext.each(items, function (item) {
                if (item.data.IsGroup) {
                    enableDelete = true;
                }
            });
            if (enableDelete) {
                map.DeleteButton.setDisabled(false);
            }
        }

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.AddRemoveObjectsButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
        }
    };

    AddContainer = function (item) {
        if (item) {
            $('#' + currentItemFieldClientID).val(Ext.util.JSON.encode(item.data));
            var grouId = item.data.ID;
            if (grouId && memberIds) {
                var containerNestedLevel;
                SW.Core.Services.callWebServiceSync("/Orion/Services/Containers.asmx", "GetContainerNestedLevel", { memberIds: JSON.stringify(memberIds), grouId: grouId }, function (result) { containerNestedLevel = result; });
                if (maxGroupNestedLevel > 0 && containerNestedLevel >= maxGroupNestedLevel) {
                    Ext.MessageBox.show({
                        title: "@{R=Core.Strings;K=WEBJS_TM0_12;E=js}",
                        msg: String.format("@{R=Core.Strings.2;K=WEBJS_YK0_13;E=js}", item.data.PlainName, maxGroupNestedLevel),
                        icon: Ext.MessageBox.WARNING,
                        buttons: Ext.MessageBox.YESNO,
                        fn: function (btn) {
                            if (btn == 'yes') {
                                SendAddItemPostBack();
                            }
                            else {
                                return;
                            }
                        }
                    });
                }
                else {
                    SendAddItemPostBack();
                }
            }
            else {
                SendAddItemPostBack();
            }
        }
        else {
            $('#' + currentItemFieldClientID).val('');
            SendAddItemPostBack();
        }
    };

    SendAddItemPostBack = function () {
        $('#' + currentPageFieldClientID).val(getCurrentPageNumber());
        $('#' + currentGroupByFieldClientID).val(groupBox.getValue());
        addItemPostBack();
    };

    EditContainer = function (item) {
        $('#' + currentItemFieldClientID).val(Ext.util.JSON.encode(item.data));
        $('#' + currentPageFieldClientID).val(getCurrentPageNumber());
        $('#' + currentGroupByFieldClientID).val(groupBox.getValue());
        editItemPostBack();
    };

    AddRemoveObjects = function (item) {
        $('#' + currentItemFieldClientID).val(Ext.util.JSON.encode(item.data));
        $('#' + currentPageFieldClientID).val(getCurrentPageNumber());
        $('#' + currentGroupByFieldClientID).val(groupBox.getValue());
        addRemoveObjectsPostBack();
    };

    DeleteContainers = function (ids, onSuccess) {
        ORION.callWebService("/Orion/Services/Containers.asmx",
                         "DeleteContainers", { containerIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    DeleteSelectedItems = function (items) {
        var toDelete = [];

        Ext.each(items, function (item) {
            if (item.data.IsGroup) {
                toDelete.push(item.data.ID);
            }
        });

        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TM0_2;E=js}");

        DeleteContainers(toDelete, function () {
            waitMsg.hide();
            grid.store.reload();
        });
    };

    GetLoadingRecord = function (level) {
        var blankRecord = Ext.data.Record.create(dataStore.fields);
        var loadingRecord = new blankRecord({
            ID: -1,
            Level: level + 1,
            Name: "@{R=Core.Strings;K=WEBJS_TM0_3;E=js}"
        });
        return loadingRecord;
    };

    GetCellID = function (item) {
        return item.data.Level + '-' + item.data.Entity + '-' + item.data.ID + '-' + item.id;
    };

    ToggleRow = function (expander, recordId) {
        var item = dataStore.getById(recordId);
        var itemID = item.data.Path;

        if (expandedItems[recordId]) {
            CollapseRow(expander, recordId);
            ORION.callWebService("/Orion/Services/Containers.asmx", "ChangeTree", { path: itemID, expand: false }, function (results) { });
        } else {
            loadingMask.show();
            var items = [];
            items.push(itemID);
            //ExpandRowWithFullName(items, GetRowByCellID(itemID).data.Level);
            ExpandRow(expander, recordId);
            ORION.callWebService("/Orion/Services/Containers.asmx", "ChangeTree", { path: itemID, expand: true }, function (results) { });
        }
    };

    ExpandRow = function (expander, recordId, callback, items) {
        var rowRecord = dataStore.getById(recordId);
        // when changing this, change DataGridRecord accordingly in Default.aspx.cs

        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'ObjectType', mapping: 2 },
            { name: 'Description', mapping: 3 },
            { name: 'RollupType', mapping: 4 },
            { name: 'IsExpandable', mapping: 5 },
            { name: 'IsGroup', mapping: 6 },
            { name: 'IsQuery', mapping: 7 },
            { name: 'Entity', mapping: 8 },
            { name: 'Status', mapping: 9 },
            { name: 'Definition', mapping: 10 },
            { name: 'PlainName', mapping: 11 },
            { name: 'Level', mapping: 17 },
            { name: 'Path', mapping: 18 },
            { name: 'ParentPath', mapping: 19 }
        ];

        var index = gridStoreColumns[gridStoreColumns.length - 1].mapping + 1;
        for (var j = 0; j < customPropertyNames.length; j++) {
            var columnName = customPropertyNames[j][0];
            gridStoreColumns.push({ name: columnName, mapping: index });
            index++;
        }

        var store = new ORION.WebServiceStore(
                            "/Orion/Services/Containers.asmx/GetChildren",
                            gridStoreColumns,
                            "Name");

        store.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: "@{R=Core.Strings;K=WEBJS_TM0_4;E=js}",
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        store.proxy.conn.jsonData = { itemId: rowRecord.data.ID, level: parseInt(rowRecord.data.Level) + 1, parentPath: rowRecord.data.Path, viewMode: viewModeBox.getValue() };

        var insertIndex = dataStore.indexOfId(recordId) + 1;
        dataStore.insert(insertIndex, GetLoadingRecord(parseInt(rowRecord.data.Level)));

        store.addListener("load", function (store) {
            expandedItems[recordId] = true;
            expander.className = 'tree-grid-expander-collapse';

            var toAdd = new Array();

            store.each(function (record) {
                toAdd.unshift(record);
            });

            dataStore.removeAt(insertIndex); // remove "loading" record
            if (toAdd.length > 0) {
                dataStore.insert(insertIndex, toAdd);
            }

            UpdateExpandStates();
            loadingMask.hide();
        });
        store.load();
    };

    CollapseRow = function (expander, recordId) {
        var rowRecord = dataStore.getById(recordId);
        var index = dataStore.indexOfId(recordId);
        var limit = dataStore.getCount();
        var toRemove = new Array();

        // remove all records in a branch under current record, use Level to check this
        for (var i = index + 1; i < limit; i++) {
            var record = dataStore.getAt(i);
            if (record.data.Level > rowRecord.data.Level) {
                toRemove.push(record);
            } else {
                break;
            }
        }

        dataStore.remove(toRemove);
        delete expandedItems[recordId];
        expander.className = 'tree-grid-expander-expand';
    };

    function renderName(value, meta, record) {
        var id = record.data.ID;
        var level = record.data.Level;
        var expandable = record.data.IsExpandable;
        var entity = record.data.Entity;
        var status = record.data.Status;

        var result = '';
        for (var i = 0; i < level; i++) {
            result += '<span class="tree-grid-indent-block">';
        }

        if (expandable) {
            result += String.format('<span><img src="/Orion/images/Pixel.gif" class="tree-grid-expander-expand" onclick="SW.Core.containers.toggleRow(this, \'{0}\');">', record.id);
        } else {
            result += '<span class="tree-grid-expander-dummy">';
        }

        if (id === -2) {// Max childrens cut off record
            result = String.format('{0} <span style="margin-left:15px; font-size:12px;">{1}</span>', result, value); ;
        }
        else if (id === -1) { // "loading" record
            result = String.format('{0} <span class="entityIconBox"><img src="/Orion/images/AJAX-Loader.gif" /></span> {1}', result, value);
        } else if (record.data.IsQuery) {
            result = String.format('{0} <span class="entityIconBox"><img src="/Orion/images/Icon.DynamicQuery.gif" /></span> {1}', result, value);
        } else {
            result = String.format('{0} <span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={1}&amp;id={2}&amp;status={3}&amp;size=small" /></span> {4}', result, entity, id, status, value);
        }

        result += '</span>';
        for (var i = 0; i < level; i++) {
            result += '</span>';
        }

        return result;
    }

    function getCurrentPageNumber() {
        return Math.ceil((pagingToolbar.cursor + pagingToolbar.pageSize) / pagingToolbar.pageSize);
    }

    ORION.prefix = "Core_Containers_";

    return {
        toggleRow: ToggleRow,
        SetPageSize: function (value) {
            pageSize = value;
        },
        SetCurrentItemFieldClientID: function (id) {
            currentItemFieldClientID = id;
        },
        SetCurrentPageFieldClientID: function (id) {
            currentPageFieldClientID = id;
        },
        SetCurrentGroupByFieldClientID: function (id) {
            currentGroupByFieldClientID = id;
        },
        SetAddItemPostBack: function (fn) {
            addItemPostBack = fn;
        },
        SetAddRemoveObjectsPostBack: function (fn) {
            addRemoveObjectsPostBack = fn;
        },
        SetEditItemPostBack: function (fn) {
            editItemPostBack = fn;
        },
        SetMaxGroupNestedLevel: function (maxNestedLevel) {
            maxGroupNestedLevel = maxNestedLevel;
        },
        SetMemberIds: function (members) {
            memberIds = members;
        },
        AddToAvailableEntities: function (item) {
            availableEntities.push(item);
        },
        SetInitialGroupBy: function (groupBy) {
            if (groupBy)
                initialGroupBy = groupBy;
        },
        SetInitialPage: function (page) {
            if (page)
                initialPage = page;
        },
        SetUserName: function (user) {
            if (user)
                userName = user;
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;
            var objectTypeColumnHidden = getCookie(userName + '_ObjectType');
            var descriptionColumnHidden = getCookie(userName + '_Description');
            var rollupTypeColumnHidden = getCookie(userName + '_RollupType');
            if (rollupTypeColumnHidden == false)
                rollupTypeColumnHidden = 'H';

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            var gridStoreColumns = [
                { name: 'ID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'ObjectType', mapping: 2 },
                { name: 'Description', mapping: 3 },
                { name: 'RollupType', mapping: 4 },
                { name: 'IsExpandable', mapping: 5 },
                { name: 'IsGroup', mapping: 6 },
                { name: 'IsQuery', mapping: 7 },
                { name: 'Entity', mapping: 8 },
                { name: 'Status', mapping: 9 },
                { name: 'Definition', mapping: 10 },
                { name: 'PlainName', mapping: 11 }
            ];

            var gridColumnsModel = [
                        selectorModel,
                        { header: '@{R=Core.Strings;K=WEBJS_VB0_21;E=js}', width: parseInt('@{R=Core.Strings;K=UIDATA_TM0_1;E=js}'), hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_9;E=js}', width: parseInt('@{R=Core.Strings;K=UIDATA_TM0_2;E=js}'), hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                        { header: '@{R=Core.Strings;K=WEBJS_TM0_7;E=js}', width: parseInt('@{R=Core.Strings;K=UIDATA_TM0_3;E=js}'), hidden: objectTypeColumnHidden == 'H', sortable: true, dataIndex: 'ObjectType' },
                        { header: '@{R=Core.Strings;K=WEBJS_TM0_8;E=js}', width: parseInt('@{R=Core.Strings;K=UIDATA_TM0_4;E=js}'), hidden: rollupTypeColumnHidden == 'H', sortable: true, dataIndex: 'RollupType' },
                        { id: 'Description', header: '@{R=Core.Strings;K=WEBJS_TM0_21;E=js}', width: parseInt('@{R=Core.Strings;K=UIDATA_TM0_5;E=js}'), hidden: descriptionColumnHidden == 'H', sortable: true, dataIndex: 'Description' }
                    ];

            var index = gridStoreColumns[gridStoreColumns.length - 1].mapping + 3;
            // get list of custom properties to push them into datastore mapping and grid columns
            SW.Core.Services.callWebServiceSync('/Orion/Services/Containers.asmx', 'GetContainerCustomProperties', {}, function (result) {
                if (result && result.Rows) {
                    for (var j = 0; j < result.Rows.length; j++) {
                        var renderer;
                        // setup correct renderer according to CP type
                        switch (result.Rows[j][2].toString().toLowerCase()) {
                            case "system.datetime":
                                renderer = renderDateTime;
                                break;
                            case "system.int32":
                            case "system.single":
                                renderer = renderFloat;
                                break;
                            case "system.boolean":
                                renderer = renderBool;
                                break;
                            default:
                                renderer = renderString;
                                break;
                        }
                        var columnName = String.format('CustomProperties.{0}', result.Rows[j][0]);
                        customPropertyNames.push([columnName, result.Rows[j][0]]);
                        gridStoreColumns.push({ name: columnName, mapping: index });
                        gridColumnsModel.push({ header: result.Rows[j][1], width: 150, sortable: true, hidden: getCookie(userName + '_' + columnName) == 'H' || getCookie(userName + '_' + columnName) == false, dataIndex: columnName, renderer: renderer });
                        index++;
                    }
                }
            });

            gridStoreColumns.push({ name: 'Level', mapping: index + 3 });
            gridStoreColumns.push({ name: 'Path', mapping: index + 4 });
            gridStoreColumns.push({ name: 'ParentPath', mapping: index + 5 });

            // when changing this, change DataGridRecord accordingly in Default.aspx.cs
            dataStore = new ORION.WebServiceStore("/Orion/Services/Containers.asmx/GetItemsPaged", gridStoreColumns, "Name");

            dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: "@{R=Core.Strings;K=WEBJS_TM0_4;E=js}",
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });

                loadingMask.hide();
            });

            function resizeToFitContent(combo) {
                if (!combo.elMetrics) {
                    combo.elMetrics = Ext.util.TextMetrics.createInstance(combo.getEl());
                }

                var m = combo.elMetrics;
                var width = 0;
                var el = combo.el;
                var si = combo.getSize();
                var minWidth = si.width;
                combo.store.each(function (r) {
                    var text = r.get(combo.displayField);
                    width = Math.max(width, m.getWidth(text));
                }, combo);

                if (el) {
                    width += el.getBorderWidth('lr');
                    width += el.getPadding('lr');
                }

                if (combo.trigger) {
                    width += combo.trigger.getWidth();
                }

                si.width = Math.max(width, minWidth);
                combo.setSize(si);
                combo.store.on({
                    'datachange': function () { resizeToFitContent(combo); },
                    'add': function () { resizeToFitContent(combo); },
                    'remove': function () { resizeToFitContent(combo); },
                    'load': function () { resizeToFitContent(combo); },
                    'update': function () { resizeToFitContent(combo); },
                    buffer: 10,
                    scope: combo
                });
            }

            groupBox = new Ext.form.ComboBox({
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: ['groupBy', 'label'],
                    data: [['none', '@{R=Core.Strings;K=WEBJS_TM0_24;E=js}'], ['status', '@{R=Core.Strings;K=WEBJS_AK0_6;E=js}']].concat(availableEntities)//.concat(customPropertyNames)
                }),
                displayField: 'label',
                valueField: 'groupBy',
                mode: 'local',
                width: parseInt('@{R=Core.Strings;K=UIDATA_TM0_6;E=js}'),
                editable: false,
                triggerAction: 'all',
                listeners: {
                    select: {
                        fn: function () {
                            setCookie("ManageGroups_GroupBy", groupBox.getValue(), "months", 1);
                            initialPage = 1;
                            ORION.callWebService("/Orion/Services/Containers.asmx", "ClearTreeState", {}, function (results) { });
                            RefreshObjects();
                        }
                    },
                    render: function (combo) {
                        resizeToFitContent(combo);
                    }
                }
            });

            //groupBox.on('render', resizeToFitContent, groupBox);

            viewModeBox = new Ext.form.ComboBox({
                store: new Ext.data.ArrayStore({
                    fields: ["mode", "label"], data: [["all", "@{R=Core.Strings;K=CFGDATA_VB0_7;E=js}"], ["root", "@{R=Core.Strings;K=WEBJS_TM0_22;E=js}"], ["flat", "@{R=Core.Strings;K=WEBJS_TM0_23;E=js}"]]
                }),
                displayField: "label", valueField: "mode", mode: "local",
                editable: false,
                triggerAction: "all",
                width: parseInt('@{R=Core.Strings;K=UIDATA_TM0_7;E=js}'),
                listeners: {
                    select: function (combo, record, index) {
                        setCookie("ManageGroups_ViewMode", viewModeBox.getValue(), "months", 1);
                        initialPage = 1;
                        ORION.callWebService("/Orion/Services/Containers.asmx", "ClearTreeState", {}, function () { });
                        RefreshObjects();
                    },
                    render: function (combo) {
                        resizeToFitContent(combo);
                    }
                }
            });

            var toolbar = new Ext.Toolbar({
                id: 'myGridToolbar',
                style: { overflow: 'visible', width: '100%' },
                items: [
                        "@{R=Core.Strings;K=WEBJS_AK0_76;E=js}", groupBox, '-',
                        "@{R=Core.Strings;K=WEBJS_TM0_19;E=js}", viewModeBox, '-',
                        {
                            id: 'AddButton',
                            text: '@{R=Core.Strings;K=WEBJS_TM0_9;E=js}',
                            tooltip: '@{R=Core.Strings;K=WEBJS_TM0_10;E=js}',
                            iconCls: 'addContainer',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();
                                AddContainer(grid.getSelectionModel().getSelected());
                            }
                        }, '-', {
                            id: 'EditButton',
                            text: '@{R=Core.Strings;K=WEBJS_TM0_20;E=js}',
                            tooltip: '@{R=Core.Strings;K=WEBJS_TM0_11;E=js}',
                            iconCls: 'editContainer',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();
                                EditContainer(grid.getSelectionModel().getSelected());
                            }
                        }, '-', {
                            id: 'AddRemoveObjectsButton',
                            text: '@{R=Core.Strings;K=WEBJS_TM0_12;E=js}',
                            tooltip: '@{R=Core.Strings;K=WEBJS_TM0_13;E=js}',
                            iconCls: 'addremoveContainer',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();
                                AddRemoveObjects(grid.getSelectionModel().getSelected());
                            }
                        }, '-', {
                            id: 'DynamicQueryResultsButton',
                            text: '@{R=Core.Strings;K=WEBJS_TM0_14;E=js}',
                            tooltip: '@{R=Core.Strings;K=WEBJS_TM0_15;E=js}',
                            iconCls: 'viewDynamicQueryObjects',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();
                                var item = grid.getSelectionModel().getSelected();
                                DynamicQueryPreview(item.data.Name, item.data.Definition); // from external file DynamicQueryPreview.js
                            }
                        }, '-', {
                            id: 'DeleteButton',
                            text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}',
                            tooltip: '@{R=Core.Strings;K=WEBJS_TM0_16;E=js}',
                            iconCls: 'deleteContainer',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();

                                Ext.Msg.confirm("@{R=Core.Strings;K=WEBJS_TM0_17;E=js}",
                                    "@{R=Core.Strings;K=WEBJS_TM0_18;E=js}",
                                    function (btn, text) {
                                        if (btn == 'yes') {
                                            DeleteSelectedItems(grid.getSelectionModel().getSelections());
                                        }
                                    });
                            }
                        }
                    ]
            });

            pagingToolbar = new Ext.PagingToolbar({
                id: 'myPagingToolbar',
                store: dataStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=Core.Strings;K=WEBJS_TM0_5;E=js}',
                emptyMsg: "@{R=Core.Strings;K=WEBJS_TM0_6;E=js}",
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                listeners: {
                    change: function () {
                        if (this.displayItem) {
                            var count = 0;
                            this.store.each(function (record) {
                                if (record.data.Level == 0)
                                    count++;
                            });

                            var msg = count == 0 ?
                            this.emptyMsg :
                            String.format(
                                this.displayMsg,
                                this.cursor + 1, this.cursor + count, this.store.getTotalCount()
                            );
                            this.displayItem.setText(msg);
                        }
                    }
                }
            });

            grid = new Ext.grid.GridPanel({
                id: 'myGridPanel',
                store: dataStore,
                columns: gridColumnsModel,
                sm: selectorModel,
                height: 500,
                layout: 'fit',
                autoscroll: true,
                stripeRows: true,
                trackMouseOver: false,
                autoExpandColumn: 'Description',
                //loadMask: { msg: 'Loading groups ...' },
                style: { overflow: 'visible' },
                tbar: toolbar,
                bbar: pagingToolbar
            });

            grid.getColumnModel().on('hiddenchange', function (cm, index, hidden) {
                if ($("#isOrionDemoMode").length == 0)
                    setCookie(userName + '_' + cm.getDataIndex(index), hidden ? 'H' : 'V', 'days', 1);
            });

            grid.render('Grid');

            if (getCookie("ManageGroups_GroupBy")) {
                initialGroupBy = getCookie("ManageGroups_GroupBy");
            } else {
                setCookie("ManageGroups_GroupBy", initialGroupBy, "months", 1);
            }
            groupBox.setValue(initialGroupBy);

            if (getCookie("ManageGroups_ViewMode")) {
                initialViewMode = getCookie("ManageGroups_ViewMode");
            } else {
                setCookie("ManageGroups_ViewMode", initialViewMode, "months", 1);
            }
            viewModeBox.setValue(initialViewMode);

            RefreshObjects();

            UpdateToolbarButtons();

            // Set the width of the grid
            //grid.setWidth($('#gridCell').width());

            $("form").submit(function () { return false; });
        }
    };
} ();

Ext.onReady(SW.Core.containers.init, SW.Core.containers);

Ext.onReady(function() {
    var myGridToolbar = jQuery("#myGridToolbar");
    var tmpWidth = myGridToolbar.width();
    var tmpWidthParent = myGridToolbar.parent().width();
    myGridToolbar.width('auto');
    myGridToolbar.parent().width('auto');

    if (myGridToolbar.width() < tmpWidth) {
        myGridToolbar.width(tmpWidth);
        myGridToolbar.parent().width(tmpWidthParent);
    } else {
        var myPagingToolbar = jQuery("#myPagingToolbar");
        myPagingToolbar.width('auto');
        myPagingToolbar.parent().width('auto');
        var aspNetFormWidth = jQuery("#aspnetForm").width();
        jQuery("#pageHeader").width(aspNetFormWidth);
        jQuery("#footer").width(aspNetFormWidth);
    }
});