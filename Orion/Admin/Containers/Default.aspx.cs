using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using Newtonsoft.Json;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Containers_ManageContainers : System.Web.UI.Page, IBypassAccessLimitation
{
    private const string addItemArgument = "AddItem";
    private const string editItemArgument = "EditItem";
    private const string addRemoveObjectsArgument = "AddRemoveObjects";
    private Dictionary<String, String> usedEntities = new Dictionary<String, String>();

    public int PageSize { get { return ContainerHelper.GetManageGroupsPageSize(); } }

    // All modifications of containers are disabled in demo mode. Buttons are disabled using JavaScript
    // but this is server side check.
    private void CheckDemoMode()
    {
        if (!Profile.AllowNodeManagement && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_180) }); 
        }
    }

	private void CheckNodeRole()
	{
		if (!Profile.AllowNodeManagement && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_180) }); 
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		CheckNodeRole();

        ((Orion_Admin_Containers_ContainerAdminPage)this.Master).HelpFragment = "OrionCorePHGroups_ManageGroups";

        AddItemPostBack = Page.ClientScript.GetPostBackEventReference(this, addItemArgument);
        EditItemPostBack = Page.ClientScript.GetPostBackEventReference(this, editItemArgument);
        AddRemoveObjectsPostBack = Page.ClientScript.GetPostBackEventReference(this, addRemoveObjectsArgument);

        ContainersMetadataDAL dal = new ContainersMetadataDAL();
        usedEntities = dal.GetUsedContainerMemberEntities();

        if (Page.IsPostBack)
        {
            ContainerWorkflow.Instance.Reset();

            switch (Request["__EVENTARGUMENT"])
            {
                case addItemArgument:
                    {
                        CheckDemoMode();

                        DataGridRecord record = null;
                        if (!String.IsNullOrEmpty(currentItem.Value))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DataGridRecord));
                            record = (DataGridRecord)serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(currentItem.Value)));
                        }

                        StringBuilder url = new StringBuilder();
                        url.AppendFormat("/Orion/Admin/Containers/Add/Default.aspx?returnUrl={0}", GetReturnUrlParameter());
                        if (record != null)
                        {
                            url.AppendFormat("&parent={0}", record.ID);
                        }

                        Response.Redirect(url.ToString());
                    }
                    break;
                case editItemArgument:
                    {
                        CheckDemoMode();

                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DataGridRecord));
                        DataGridRecord record = (DataGridRecord)serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(currentItem.Value)));

                        if (record.IsQuery)
                        {
                            Response.Redirect(
                                String.Format("/Orion/Admin/Containers/EditDynamicQuery.aspx?name={0}&def={1}&id={2}&returnUrl={3}",
                                    HttpUtility.UrlEncode(record.PlainName),
                                    HttpUtility.UrlEncode(record.Definition),
                                    record.ID,
                                    GetReturnUrlParameter()));
                        }
                        else
                        {
                            Response.Redirect(String.Format("/Orion/Admin/Containers/EditContainer.aspx?id={0}&returnUrl={1}",
                                record.ID,
                                GetReturnUrlParameter()));
                        }
                    }
                    break;
                case addRemoveObjectsArgument:
                    {
                        CheckDemoMode();

                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DataGridRecord));
                        DataGridRecord record = (DataGridRecord)serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(currentItem.Value)));

                        Response.Redirect(String.Format("/Orion/Admin/Containers/AddRemoveObjects.aspx?id={0}&returnUrl={1}",
                                record.ID,
                                GetReturnUrlParameter()));
                    }
                    break;
            }
        }
    }

    protected String AddItemPostBack { get; set; }
    protected String EditItemPostBack { get; set; }
    protected String AddRemoveObjectsPostBack { get; set; }

    protected String CurrentItemFieldClientID
    {
        get { return currentItem.ClientID; }
    }
    protected String CurrentPageFieldClientID
    {
        get { return currentPage.ClientID; }
    }
    protected String CurrentGroupByFieldClientID
    {
        get { return currentGroupBy.ClientID; }
    }

    protected Dictionary<string, string> UsedEntities
    {
        get
        {
            return usedEntities;
        }
    }

    protected string GroupBy
    {
        get
        {
            if (Request["groupBy"] != null)
                return HttpUtility.HtmlEncode(Request["groupBy"]);
            else
                return null;
        }
    }

    protected Int32 PageNumber
    {
        get
        {
            int page = 0;
            if (Request["page"] != null)
                Int32.TryParse(Request["page"], out page);

            return page;
        }
    }

    private string GetReturnUrlParameter()
    {
        return UrlHelper.ToSafeUrlParameter(
            String.Format("{0}?page={1}&groupBy={2}", 
            Request.AppRelativeCurrentExecutionFilePath, 
            currentPage.Value, 
            currentGroupBy.Value));
    }

    public string GetMemberIds()
    {
        Dictionary<int, IList<int>> members;
        using (var dal = new ContainersDAL())
        {
            members = dal.GetMemberIds();
        }
        return members.Count > 0 ? JsonConvert.SerializeObject(members) : string.Empty;
    }

    public int MaxGroupNestedLevel
    {
        get
        {
            int val;

            if (int.TryParse(ConfigurationManager.AppSettings["MaxGroupNestedLevel"], out val))
                return val;

            return int.MaxValue;
        }
    }

    protected string Escape(string input)
    {
        if (input == null)
            return string.Empty;

        return input.Replace(@"\", @"\\").Replace("'", @"\'");
    }

    // if you change this, you must change also row definition in ContainersGrid.js
    [Serializable]
    private class DataGridRecord
    {
        public int ID = 0;
        public string Name = null;
        public string ObjectType = null;
        public string Description = null;
        public string RollupType = null;
        public bool IsExpandable = false;
        public bool IsGroup = false;
        public bool IsQuery = false;
        public string Definition = null;
        public string PlainName = null;
        public int Level = 0;
        public string Path = null;
        public string ParentPath = null;
    }
}
