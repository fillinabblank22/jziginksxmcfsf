<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/Containers/ContainerAdminPage.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Admin_Containers_ManageContainers"
    Title="<%$ HtmlEncodedResources: CoreWebContent, ResourcesAll_ManageGroups %>" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="Admin/Containers/DynamicQueryPreview.js" />
    <orion:Include runat="server" File="Admin/Containers/ContainersGrid.js" />
    <style type="text/css">
    .x-grid3-row-selected
    {
	    background-color:#DFE8F6 !important;
	}
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <p>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_210) %></p>
    <table width="97%">
        <tr>
            <td id="gridCell">
                <div id="Grid">
                </div>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="currentItem" runat="server" />
    <asp:HiddenField ID="currentPage" runat="server" />
    <asp:HiddenField ID="currentGroupBy" runat="server" />

    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>

    <script type="text/javascript">
        // <![CDATA[
        SW.Core.containers.SetPageSize(<%= this.PageSize %>);
        SW.Core.containers.SetCurrentItemFieldClientID('<%= CurrentItemFieldClientID %>');
        SW.Core.containers.SetCurrentPageFieldClientID('<%= CurrentPageFieldClientID %>');
        SW.Core.containers.SetCurrentGroupByFieldClientID('<%= CurrentGroupByFieldClientID %>');
        SW.Core.containers.SetAddItemPostBack(function() { <%= AddItemPostBack %> });
        SW.Core.containers.SetEditItemPostBack(function() { <%= EditItemPostBack %> });
        SW.Core.containers.SetMaxGroupNestedLevel(<%= MaxGroupNestedLevel %>);
        SW.Core.containers.SetMemberIds(<%= GetMemberIds() %>);
        SW.Core.containers.SetAddRemoveObjectsPostBack(function() { <%= AddRemoveObjectsPostBack %> });
        <% foreach (KeyValuePair<String, String> entity in UsedEntities) { %>
        SW.Core.containers.AddToAvailableEntities(['<%= Escape(entity.Key) %>','<%=Escape(entity.Value) %>']);
        <% } %>
        SW.Core.containers.SetInitialGroupBy('<%= GroupBy %>');
        SW.Core.containers.SetInitialPage(<%= PageNumber %>);
        SW.Core.containers.SetUserName("<%=Context.User.Identity.Name.Trim().Replace("\\", "\\\\")%>");
        // ]]>
    </script>

</asp:Content>
