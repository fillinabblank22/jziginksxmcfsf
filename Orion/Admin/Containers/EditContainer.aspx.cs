using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_Containers_EditContainer : System.Web.UI.Page, IBypassAccessLimitation
{
	private const string DefaultReturnUrl = "~/Orion/Admin/Containers/Default.aspx";

	private Container containerModel;
	private ContainersDAL dal;
    private Dictionary<string, object> customProperties; 

	private void CheckNodeRole()
	{
		if (!Profile.AllowNodeManagement)
		{
			throw new Exception(Resources.CoreWebContent.WEBCODE_AK0_180);
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		CheckNodeRole();

		((Orion_Admin_Containers_ContainerAdminPage)this.Master).HelpFragment = "OrionCorePHGroups_EditGroup";

		dal = new ContainersDAL();
		containerModel = dal.GetContainer(Convert.ToInt32(Request.QueryString["id"]));

		this.Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_179, UIHelper.Escape(containerModel.Name));

		// getting cp values
		var properties = CustomPropertyMgr.GetCustomPropertiesForEntity("Orion.GroupCustomProperties");
		containerModel.CustomProperties = new Dictionary<string, object>();

		if (properties != null)
		{
			var cpColumns = new StringBuilder();

			foreach (var customProperty in properties)
			{
				cpColumns.AppendFormat(", c.CustomProperties.[{0}]", customProperty.PropertyName);
			}

			DataTable cDataTable;
			using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
			{
				string selectQuery = String.Format(
						@"SELECT ContainerID {0}                            
                      FROM {1} c
                      WHERE  ContainerID = {2}", cpColumns.ToString(), ContainersDAL.Entity, Convert.ToInt32(Request.QueryString["id"]));

				cDataTable = service.Query(selectQuery);
			}

			foreach (var customProperty in properties)
			{
				containerModel.CustomProperties.Add(customProperty.PropertyName, cDataTable.Rows[0][customProperty.PropertyName]);
			}

            if (IsPostBack)
            {
                customProperties = containerModel.CustomProperties;
            }
		}

		editPropertiesControl.ContainerModel = containerModel;
		editPropertiesControl.OnAddRemoveObjects
			+= new Orion_Admin_Containers_EditProperties.AddRemoveObjectsHandler(editPropertiesControl_OnAddRemoveObjects);
	}

	void editPropertiesControl_OnAddRemoveObjects(object sender, AddRemoveObjectsEventArgs e)
	{
		bool redirect = true;
		if (e.SaveChanges)
		{
			redirect = SaveChanges();
		}

		if (redirect)
		{
			this.Response.Redirect(String.Format("~/Orion/Admin/Containers/AddRemoveObjects.aspx?id={0}&returnUrl={1}",
				containerModel.Id,
				UrlHelper.ToSafeUrlParameter(Request.RawUrl)));
		}
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		if (SaveChanges())
		{
			ReturnBack();
		}
	}

	private bool SaveChanges()
	{
	    Page.Validate("EditCpValue");

        if (Page.IsValid)
		{
			dal.UpdateContainer(editPropertiesControl.ContainerModel);

			foreach (var cp in containerModel.CustomProperties.Where(cp => cp.Value != null))
			{
				if (containerModel.Id != 0 || (cp.Value != null && !string.IsNullOrEmpty(cp.Value.ToString()))) //no need to run update query for empty cps
				{
					CustomPropertyMgr.SetCustomProp("ContainerCustomProperties", cp.Key, containerModel.Id, cp.Value);

                    var cpValue = customProperties.Where(x => x.Key == cp.Key).Select(x => x.Value).FirstOrDefault();
                    if (!(cp.Value.ToString() == "" ? DBNull.Value : cp.Value).Equals(cpValue))
                    {
                        var changedProperties = new PropertyBag
                                                    {
                                                        {"InstanceType", "Orion.GroupCustomProperties"},
                                                        {"ContainerID", containerModel.Id},
                                                        {cp.Key, cp.Value}
                                                    };
                        IndicationPublisher.CreateV3().ReportIndication(
                            IndicationHelper.GetIndicationType(IndicationType.System_InstanceModified),
                            IndicationHelper.GetIndicationProperties(),
                            changedProperties);

                    }
					// update restricted values if needed
					var customProperty = CustomPropertyMgr.GetCustomProperty("ContainerCustomProperties", cp.Key);
					CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty,
						new[] { CustomPropertyHelper.GetInvariantCultureString((cp.Value != null) ? cp.Value.ToString() : string.Empty, customProperty.PropertyType) });
				}
			}

			return true;
		}

        if (!ClientScript.IsClientScriptBlockRegistered("ExpandScript"))
        {
            String cstext1 = "<script type=\"text/javascript\">" +
                "ToggleAdvanced($('[id*=ImageButton2]')[0]); </" + "script>";
            ClientScript.RegisterStartupScript(this.GetType(), "ExpandScript", cstext1);
        }
		return false;
	}

	protected void CancelClick(object sender, EventArgs e)
	{
		ReturnBack();
	}

	protected void ReturnBack()
	{
		if (Request["returnUrl"] != null)
		{
			this.Response.Redirect(UrlHelper.FromSafeUrlParameter(Request["returnUrl"]));
		}
		else
		{
			this.Response.Redirect(DefaultReturnUrl);
		}
	}
}
