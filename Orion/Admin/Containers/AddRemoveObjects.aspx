<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Admin/Containers/ContainerAdminPage.master"
 CodeFile="AddRemoveObjects.aspx.cs" Inherits="Orion_Admin_Containers_AddRemoveObjects" %>
 
<%@ Register Src="~/Orion/Admin/Containers/AddRemoveObjectsControl.ascx" TagName="AddRemoveObjectsControl" TagPrefix="orion" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <p>
	    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_222) %>
	    </p>
	    
	<orion:AddRemoveObjectsControl ID="addRemoveObjectsControl" runat="server" />
	
	<div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="submitButton" LocalizedText="Submit" OnClick="SubmitClick" OnClientClick="return SW.Core.containers.ValidateGrid();" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="cancelButton" LocalizedText="Cancel" OnClick="CancelClick" DisplayType="Secondary" />
	</div>
</asp:Content>
