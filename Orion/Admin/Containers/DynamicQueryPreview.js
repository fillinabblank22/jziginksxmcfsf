﻿DynamicQueryPreview = function(name, definition) {
    GetLoadingRecord = function() {
        var blankRecord = Ext.data.Record.create(store.fields);
        var loadingRecord = new blankRecord({
            Name: '@{R=Core.Strings;K=WEBJS_TM0_3;E=js}',
            Entity: 'Loading'
        });
        return loadingRecord;
    }

    var dialog;
    var store = new ORION.WebServiceStore(
                                "/Orion/Services/Containers.asmx/GetQueryResults",
                                [
                                    { name: 'Name', mapping: 0 },
                                    { name: 'Entity', mapping: 1 },
                                    { name: 'EntityName', mapping: 2 },
                                    { name: 'Status', mapping: 3 },
                                    { name: 'StatusName', mapping: 4 },
                                    { name: 'MemberPrimaryID', mapping: 5 },
                                ],
                                "Name");
    store.proxy.conn.jsonData = { definition: definition };
    store.add(GetLoadingRecord());
    store.load();
    
    var listView = new Ext.list.ListView({
        store: store,
        multiSelect: true,
        emptyText: '@{R=Core.Strings;K=WEBJS_TM0_26;E=js}',
        reserveScrollOffset: true,
        hideHeaders: true,
        cls: 'dynamicQueryPreviewList',
        tpl: new Ext.XTemplate(
                '<tpl>',
                    '<table class="dynamicQueryPreviewListTable">',
                        '<tpl for="rows">',
                            '<tr class="{[xindex % 2 === 0 ? "ZebraStripe" :  ""]}">',
                                '<tpl if="Entity === \'Loading\'">',
                                '<td class="dynamicQueryPreviewListItem"><img src="/Orion/images/AJAX-Loader.gif" /> {Name}</td>',
                                '</tpl>',
                                '<tpl if="Entity !== \'Loading\'">',
                                '<tpl if="EntityName !== \'\'">',
                                '<td class="dynamicQueryPreviewListItem"><img src="/Orion/StatusIcon.ashx?id={MemberPrimaryID}&amp;entity={Entity}&amp;status={Status}&amp;size=small" /> {Name}</td>',
                                '</tpl>',
                                '<tpl if="EntityName == \'\'">',
                                '<td class="dynamicQueryPreviewListItem">&nbsp;{Name}</td><td>&nbsp;</td>',
                                '</tpl>',
                                '<tpl if="EntityName !== \'\'">',
                                '<td class="dynamicQueryPreviewListItem">@{R=Core.Strings;K=WEBJS_TM0_27;E=js}</td>',
                                '</tpl>',
                                '</tpl>',
                            '</tr>',
                        '</tpl>',
                    '</table>',
                '</tpl>'
            ),
        columns: [{
            header: '@{R=Core.Strings;K=WEBJS_AK0_9;E=js}'
}]
        });

        function onClose() { if (dialog) { dialog.close(); dialog = null; } };

        dialog = new Ext.Window({
            id: "queryResultsDialog",
            title:  String.format("@{R=Core.Strings;K=WEBJS_TM0_25;E=js}", name),
            width: @{R=Core.Strings;K=UIDATA_TM0_8;E=js},
            height: 400,
            border: false,
            region: "center",
            layout: "fit",
            modal: true,
            resizable: true,
            items: [listView],
            buttons: [
	    { text: "@{R=Core.Strings;K=WEBJS_PCC_4;E=js}", disabled: false, handler: onClose }
    ]
        });

        dialog.show();
    }