<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditDynamicQuery.aspx.cs" 
MasterPageFile="~/Orion/Admin/Containers/ContainerAdminPage.master" Inherits="Orion_Admin_Containers_EditDynamicQuery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="js/jquery/jquery.autocomplete.css" />
    <orion:Include runat="server" File="jquery/jquery.autocomplete.js" />
    
    <script type="text/javascript">
        //<![CDATA[
        function createAutoCompleteFields() {
            var entityBox = $(".dynamicQueryTable .entityBox");

            $('.conditionRow').each(function(index, element) {
                var propertyBox = $(element).find('.propertyColumn select');
                var valueBox = $(element).find('.valueColumn input');

                propertyBox.change(function() {
                    $(valueBox).flushCache();
                });

                $(valueBox).autocomplete("/Orion/Admin/Containers/PropertyAutocompleteProvider.ashx",
                {
                    cacheLength: 10,
                    max: 100,
                    matchCase: false,
                    matchContains: true,
                    scroll: true,
                    selectFirst: true,
                    minChars: 0,
                    highlight: function(value, term)
                    {
                        value = Ext.util.Format.htmlEncode(value);
                        return value.replace(term, '<b>' + term + '</b>');
                    },
                    extraParams: {
                        entityName: function() {
                            return entityBox.val();
                        },
                        propertyName: function() {
                            return propertyBox.val();
                        }
                    }
                });
            });
        }

        function refreshOperators(propertyBox) {
            var entityBox = $(".dynamicQueryTable .entityBox");
            var operatorBox = $(propertyBox).parents('.conditionRow').find('.operatorColumn select');

            $.ajax({
                url: '/Orion/Admin/Containers/PropertyOperatorProvider.ashx',
                dataType: 'json',
                data: {
                    entityName: entityBox.val(),
                    propertyName: $(propertyBox).val()
                },
                success: function (data) {
                    operatorBox.children().remove();
                    for (var i = 0; i < data.length; i++) {
                        operatorBox.append('<option value="' + data[i].value + '">' + data[i].label + '</option>');
                    }
                    operatorBox.width('150px');
                    storeOperatorValue(operatorBox);
                }
            });
        }

        function storeOperatorValue(operatorBox) {
            var operatorValueBox = $(operatorBox).siblings('input[type=hidden]');
            operatorValueBox.val($(operatorBox).val());
        }
        //]]>
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_528) %></h1>
    
    <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_529) %></p>
    
	<div class="ContentBucket">
	    <div class="dynamicQueryEditorPanel">
	        <table id="dynamicQueryEditorTable">
                <tr>
                    <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_530) %></td>
                    <td colspan="4">
                        <asp:TextBox runat="server" ID="nameBox" CssClass="NameBox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="nameBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_531 %>" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_532) %></td>
                    <td>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <table width="100%" class="dynamicQueryTable">
                                    <tr>
                                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_533) %></td>
                                        <td class="operatorColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_534) %></td>
                                        <td class="valueColumn">
                                            <asp:DropDownList class="entityBox" ID="entityBox" runat="server" OnSelectedIndexChanged="EntityChanged" AutoPostBack="true" />
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <asp:Repeater ID="ConditionsRepeater" runat="server" OnItemDataBound="ConditionsRepeater_ItemDataBound" OnItemCommand="ConditionsRepeater_ItemCommand">
                                        <ItemTemplate>
                                            <tr class="conditionRow">
                                                <td class="propertyColumn"><asp:DropDownList ID="propertyBox" runat="server" onchange="refreshOperators(this);" /></td>
                                                <td class="operatorColumn">
                                                    <asp:DropDownList ID="operatorBox" runat="server" onchange="storeOperatorValue(this);" />
                                                    <asp:HiddenField ID="operatorBoxValue" runat="server" />
                                                </td>
                                                <td class="valueColumn"><asp:TextBox ID="valueBox" runat="server" /></td>
                                                <td class="deleteColumn">
                                                    <asp:ImageButton ID="removeConditionButton" ImageUrl="~/Orion/images/Button.RemoveCondition.gif" 
                                                    runat="server" CommandName="Remove" CausesValidation="false" />
                                                </td>
                                                <td>
                                                <asp:CustomValidator ID="ConditionValidator" runat="server" Display="Dynamic" OnServerValidate="ConditionValidation" />
                                                <span style="padding-left:15px;">&nbsp;</span>
                                                <%if (this.FirstCondition && this.NoValidatorIsShown)
                                                  { %>
                                                <asp:Label ID="HintLablel" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_535 %>" ></asp:Label>
                                                <%} %>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                     <tr>
                                        <td class="dynamicQueryBuilderColumn">
                                            <orion:LocalizableButton ID="addConditionButton" LocalizedText="CustomText" automation="Add condition" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_536 %>" DisplayType="Small" runat="server" CausesValidation="false" OnClick="AddCondition" />
                                        </td>
                                        <td class="dynamicQueryBuilderColumn">&nbsp;</td>
                                        <td class="dynamicQueryBuilderColumn">&nbsp;</td>
                                    </tr>
	                            </table>
	                        </ContentTemplate>
	                    </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:CustomValidator ID="ConditionsValidator" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_537 %>" runat="server" Display="Dynamic" OnServerValidate="ConditionsValidation" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="sw-btn-bar">
                            <orion:LocalizableButton ID="previewButton" LocalizedText="CustomText" automation="Preview" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_280 %>" DisplayType="Secondary" runat="server" OnClick="Preview" />
                            <orion:LocalizableButton ID="saveButton" LocalizedText="Save" automation="Save" DisplayType="Primary" runat="server" OnClick="Save" />
                            <orion:LocalizableButton ID="cancelButton" LocalizedText="Cancel" automation="Cancel" DisplayType="Secondary" runat="server" CausesValidation="false" OnClick="Cancel" />
                        </div>
                    </td>
                </tr>
            </table>
	    </div>
	    <div id="dynamicQueryPreviewPanel" class="dynamicQueryPreviewPanel" runat="server">
	        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_539) %></p>
	        <asp:Repeater ID="PreviewRepeater" runat="server">
	            <HeaderTemplate>
	                <table id="dynamicQueryPreviewTable">
	            </HeaderTemplate>
	            <ItemTemplate>
	                <tr class="ZebraStripe">
	                    <td>
	                        <img src="/Orion/StatusIcon.ashx?entity=<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).Entity) %>&amp;id=<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).MemberPrimaryID) %>&amp;status=<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).Status.StatusId) %>&amp;size=small" />
	                        <%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).FullName) %>
	                    </td>
	                </tr>
	            </ItemTemplate>
	            <AlternatingItemTemplate>
	                <tr>
	                    <td>
	                        <img src="/Orion/StatusIcon.ashx?entity=<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).Entity) %>&amp;id=<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).MemberPrimaryID) %>&amp;status=<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).Status.StatusId) %>&amp;size=small" />
	                        <%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Web.Containers.ContainerMember)Container.DataItem).FullName) %>
	                    </td>
	                </tr>
	            </AlternatingItemTemplate>
	            <FooterTemplate>
	                
	            </FooterTemplate>
	        </asp:Repeater>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="MoreItemsLabel" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_538 %>" Style="margin-left:10px;"></asp:Label>
                    </td>
                </tr>
            </table>
	    </div>
	</div>
	<script type="text/javascript">
	    //<![CDATA[
	    createAutoCompleteFields();
	    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(createAutoCompleteFields);
	    //]]>
	</script>
</asp:Content>
