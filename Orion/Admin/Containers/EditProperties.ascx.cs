﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Shared;
using SolarWinds.Orion.Web.DAL;
using System.Text;
using SolarWinds.Orion.Web.Helpers;

public class AddRemoveObjectsEventArgs : EventArgs
{
    public bool SaveChanges { get; set; }
}

public partial class Orion_Admin_Containers_EditProperties : System.Web.UI.UserControl
{
    public delegate void AddRemoveObjectsHandler(object sender, AddRemoveObjectsEventArgs e);
    public event AddRemoveObjectsHandler OnAddRemoveObjects;

    private Container containerModel;
    public Container ContainerModel
    {
        get
        {
            return containerModel;
        }
        set { containerModel = value; }
    }

    public bool DisplayMembers
    {
        get;
        set;
    }

    public string Title
    {
        get;
        set;
    }

    public string ContainerParentName
    {
        get
        {
            if (containerModel.Parent != null)
            {
                StringBuilder str = new StringBuilder();
                Container c = containerModel.Parent;

                while (c != null)
                {
                    if (str.Length > 0)
                        str.Insert(0, " &gt; ");
                    str.Insert(0, UIHelper.Escape(c.Name));
                    c = c.Parent;
                }

                return str.ToString();
            }

            return null;
        }
    }

	protected override void OnInit(EventArgs e)
	{
		if (!IsPostBack && Request.QueryString["id"] != null)
		{
			groupCps.EntityIds = new List<int>() { Convert.ToInt32(Request.QueryString["id"]) };
		}
	}

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!IsPostBack)
        {
            groupCps.CustomProperties = containerModel.CustomProperties;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        rollupHelpLink.HRef = HelpHelper.GetHelpUrl("OrionCoreAGManagingDisplayGroupStatus");
        using (var swisErrorsContext = new SwisErrorsContext())
        {
            if (!IsPostBack)
            {
                rollupComboBox.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_TM0_94,
                    EvaluationMethod.Best.ToString()));
                rollupComboBox.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_TM0_87,
                    EvaluationMethod.Mixed.ToString()));
                rollupComboBox.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_TM0_88,
                    EvaluationMethod.Worst.ToString()));

                nameBox.Text = containerModel.Name;
                descriptionBox.Text = containerModel.Description;
                rollupComboBox.SelectedValue = ((EvaluationMethod) containerModel.StatusCalculator).ToString();
                frequencyBox.Text = containerModel.Frequency.ToString();

                if (DisplayMembers)
                {
                    ContainersDAL dal = new ContainersDAL();
                    IEnumerable<ContainerMemberDefinition> definitions
                        = dal.GetMemberDefinitions(containerModel.Id,
                            ContainersDAL.MemberDefinitionLoadMethod.Members |
                            ContainersDAL.MemberDefinitionLoadMethod.EntityNames);
                    foreach (ContainerMemberDefinition definition in definitions)
                    {
                        if (definition.IsDynamicQuery)
                        {
                            definition.FullMemberName =
                                SolarWinds.Orion.Web.Containers.FullNameHelper.GenerateDynamicQueryFullName(
                                    definition.MemberName, definition.Definition);
                        }
                    }

                    var visibleDefinitions = new List<ContainerMemberDefinition>(definitions).Where(def => def.IsVisible);

                    contentsRepeater.DataSource = visibleDefinitions;
                    contentsRepeater.DataBind();

                    contentsSummaryLabel.Text = GetMembersString(visibleDefinitions);
                }
            }
            else
            {
                UpdateContainer();
            }

            var swisErrorMessages = swisErrorsContext.GetErrorMessages();
            if (swisErrorMessages.Any())
            {
                SwisfErrorControl.SetError(swisErrorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList());
                SwisfErrorControl.Visible = true;
            }
        }
    }

    private void UpdateContainer()
    {
        containerModel.Name = WebSecurityHelper.SanitizeHtmlV2(nameBox.Text);
        containerModel.Description = WebSecurityHelper.SanitizeHtmlV2(descriptionBox.Text);
        containerModel.StatusCalculator = (int)Enum.Parse(typeof(EvaluationMethod), rollupComboBox.SelectedValue);
        Int32 frequency = 0;
        Int32.TryParse(frequencyBox.Text, out frequency);
        containerModel.Frequency = frequency;
		containerModel.CustomProperties = groupCps.CustomProperties;
    }

    protected void AddRemoveObjects_Click(object sender, EventArgs e)
    {
        if (OnAddRemoveObjects != null)
        {
            OnAddRemoveObjects(this, new AddRemoveObjectsEventArgs() { SaveChanges = (saveChangesField.Value == "1") });
        }
    }

    private string GetMembersString(IEnumerable<ContainerMemberDefinition> definitions)
    {
        Dictionary<string, int> counters = new Dictionary<string, int>();
        foreach (ContainerMemberDefinition def in definitions)
        {
            if (!counters.ContainsKey(def.EntityOrDynamicQuery))
                counters[def.EntityOrDynamicQuery] = 0;

            counters[def.EntityOrDynamicQuery]++;
        }

        StringBuilder membersStr = new StringBuilder();
        bool first = true;
        foreach (KeyValuePair<string, int> pair in counters)
        {
            ContainerMemberDefinition definition;
            if (pair.Key == ContainerMemberDefinition.DynamicQueryEntity)
            {
                definition = ContainerMemberDefinition.DynamicQueryDefinition;
            }
            else
            {
                definition = definitions.First(d => d.Entity == pair.Key);
            }

            if (!first)
                membersStr.Append(", ");

            membersStr.Append(pair.Value);
            if (pair.Value == 1)
            {
                membersStr.AppendFormat(" {0}", definition.EntityName);
            }
            else
            {
                membersStr.AppendFormat(" {0}", definition.EntityNamePlural);
            }

            first = false;
        }

        return membersStr.ToString();
    }

    protected void ValidateNameUnique(object source, ServerValidateEventArgs args)
    {
		using (ContainersDAL dal = new ContainersDAL()) 
		{
			var groups = dal.GetContainers(string.Format("Name = '{0}'", nameBox.Text.Replace("'", "''")));
			args.IsValid = (groups.Count() == 0 || groups.Count(item => item.Id == ContainerModel.Id) == 1);
			if (!args.IsValid)
			{
				valNameUnique.Text = string.Format(Resources.CoreWebContent.WEBCODE_TM0_89, HttpUtility.HtmlEncode(nameBox.Text));
			}
		}

        if (args.IsValid)
        {
            if (string.IsNullOrEmpty(WebSecurityHelper.SanitizeHtmlV2(nameBox.Text)))
            {
                valNameBoxRequired.Text = Resources.CoreWebContent.WEBCODE_PS0_23;
                valNameBoxRequired.Visible = true;
                valNameBoxRequired.IsValid = false;
            }
        }
    }
}
