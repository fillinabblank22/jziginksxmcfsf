<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="AccountList.aspx.cs" 
        Inherits="Orion_Admin_AccountList" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_180 %>" EnableViewState="false" %>
<%-- disable ViewState on this page, we really don't need to maintain anything across postbacks --%>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <asp:PlaceHolder runat="server" ID="AccountTable" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
<orion:Include runat="server" File="AccountList.css" />
</asp:Content>
