<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PollingSettings.aspx.cs"
    Inherits="Orion_Admin_PollingSettings" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_486 %>" %>

<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="App_Code" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHPollingSettings" />
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <style type="text/css">
        #adminContentTable td {
            padding: 10px;
        }

        #inside_table td {
            padding: 5px;
            vertical-align: middle;
        }

            #inside_table td.Property img {
                width: 15px;
                height: 15px;
            }
        #inside_table .header td {
            padding-left: 0;
        }

        .footer_buttons {
            margin: 10px 0 0 0;
        }

        #adminContent h2 {
            margin: 0;
            font-weight: normal;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            var CounterRollover_Method1 = '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_9) %>';
            var CounterRollover_Method2 = '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_485) %>';

            var CounterRollover = $("#<%= SWNetPerfMon_Settings_Counter_Rollover.ClientID %>");
            var CounterRolloverDescription = $(".CounterRolloverDescription")[0];

            if ($(CounterRollover).val() == 'Method 1') $(CounterRolloverDescription).text(CounterRollover_Method1);
            else $(CounterRolloverDescription).text(CounterRollover_Method2);

            $(CounterRollover).change(function () {
                if ($(CounterRollover).val() == 'Method 1') $(CounterRolloverDescription).text(CounterRollover_Method1);
                else $(CounterRolloverDescription).text(CounterRollover_Method2);
            });

            $('#inside_table > #inside_table_body > tr:nth-child(even)').not('tr.header').not('tr.applyButton').addClass('alternateRow');
        });

        function ClientRetentionValidate(sender, args) {
            var detail = parseInt($("#<%= SWNetPerfMon_Settings_Retain_Detail.TextBoxId %>").val());
            var hourly = parseInt($("#<%= SWNetPerfMon_Settings_Retain_Hourly.TextBoxId %>").val());
            var daily = parseInt($("#<%= SWNetPerfMon_Settings_Retain_Daily.TextBoxId %>").val());

            args.IsValid = detail < hourly && hourly < daily;
        }
    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <asp:ImageButton runat="server" ID="bug_button" CausesValidation="true" ImageUrl="/Orion/images/Pixel.gif"
        OnClientClick="return false" />
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table class="PageHeader" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <h1>
                                <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
                        </td>
                    </tr>
                </table>
                
                <table id="adminContentTable" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody id="inside_table_body">
                                <tr class="header">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_422) %></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_427) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Node Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Node_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_498 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_428) %>
                                    </td>
                                </tr>
                                <tr id="InterfacePollIntervalRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_429) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Interface Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Interface_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_498 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_428) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_430) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Volume Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Volume_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_498 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_428) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_431) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Rediscovery Interval"
                                             ID="SWNetPerfMon_Settings_Default_Rediscovery_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_499 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_432) %>
                                    </td>
                                </tr>
                                <tr runat="server" id="AssetInventoryPollIntervalDaysRow">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VT0_4) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="AssetInventory-PollIntervalDays"
                                             ID="AssetInventory_PollIntervalDays" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VT0_5 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VT0_6) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_433) %>
                                    </td>
                                     <td><asp:CheckBox runat="server" ID="cbLockCustomValues" AutoPostBack="false" Checked="true"/>&nbsp;</td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_434) %>
                                    </td>
                                </tr>
                                <tr class="applyButton">
                                    <td>&nbsp;</td>
                                    <td colspan="2">
                                        <orion:LocalizableButton id="bt_Polling" runat="server" DisplayType="Small" LocalizedText="CustomText" automation="ReApplyPollingIntervals" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_494 %>" OnClick="Polling_Click" OnClientClick='<%# DefaultSanitizer.SanitizeHtml(string.Format("return confirm(\"{0}\");", ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_10))) %>'/>
                                    </td>
                                </tr>
                                <tr class="header">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_423) %></h2>
                                    </td>
                                </tr>
                                <tr runat="server" id="TopologyPollIntervalRow">
                                    <td class="PropertyHeader">
										<asp:CheckBox runat="server" ID="IsDefaultNodeTopologyPollIntervalEnabled" AutoPostBack="false" Checked="True"/>&nbsp;
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_1) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Node Topology Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Node_Topology_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_MZ0_2 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_3) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_4) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
										<asp:CheckBox runat="server" ID="IsDefaultNodeStatsPollIntervalEnabled" AutoPostBack="false" Checked="True"/>&nbsp;
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_435) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Node Stat Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Node_Stat_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_500 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_436) %>
                                    </td>
                                </tr>
                                <tr id="InterfaceStatsPollIntervalRow" runat="server">
                                    <td class="PropertyHeader">
										<asp:CheckBox runat="server" ID="IsDefaultInterfaceStatsPollIntervalEnabled" AutoPostBack="false" Checked="True"/>&nbsp;
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_437) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Interface Stat Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Interface_Stat_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_500 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_438) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
										<asp:CheckBox runat="server" ID="IsDefaultVolumeStatsPollIntervalEnabled" AutoPostBack="false" Checked="True"/>&nbsp;
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_439) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Volume Stat Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Volume_Stat_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText='<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_500 %>' />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_440) %>
                                    </td>
                                </tr>
                                <tr class="applyButton">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="2">
                                        <orion:LocalizableButton id="bt_Statistics" runat="server" DisplayType="Small" LocalizedText="CustomText" automation="ReApplyPollingStatisticsIntervals" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_495 %>" OnClick="Statistics_Click" OnClientClick='<%# DefaultSanitizer.SanitizeHtml(string.Format("return confirm(\"{0}\");", ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_497))) %>'/>
                                    </td>
                                </tr>

                                <tr class="header">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_EnhancedNodeStatusCalculation_Label) %></h2>
                                    </td>
                                </tr>
                                <tr />
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_EnhancedNodeStatusCalculation_Property) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:RadioButtonList runat="server" ID="EnhancedNodeStatusCalculationSetting" automation="EnhancedNodeStatusCalculationSetting">
                                            <asp:ListItem Value="0" Text="<%$ HtmlEncodedResources:CoreWebContent,PollingSettings_EnhancedNodeStatusCalculation_ClassicStatus %>" automation="ClassicStatus" />
                                            <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources:CoreWebContent,PollingSettings_EnhancedNodeStatusCalculation_EnhancedStatus %>" automation="EnhancedStatus" />
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="Property">
                                         <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_EnhancedNodeStatusCalculation_Hint) %>
                                    </td>
                                </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_EnhancedInterfaceStatusCalculation_Property) %>
                                </td>
                                <td class="Property" align="left">
                                    <asp:RadioButtonList runat="server" ID="EnhancedInterfaceStatusCalculationSetting" automation="EnhancedInterfaceStatusCalculationSetting">
                                        <asp:ListItem Value="0" Text="<%$ HtmlEncodedResources:CoreWebContent,PollingSettings_EnhancedInterfaceStatusCalculation_ClassicStatus %>" automation="ClassicStatus" />
                                        <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources:CoreWebContent,PollingSettings_EnhancedInterfaceStatusCalculation_EnhancedStatus %>" automation="EnhancedStatus" />
                                    </asp:RadioButtonList>
                                </td>
                                <td class="Property">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_EnhancedInterfaceStatusCalculation_Hint) %>
                                </td>
                            </tr>

                                <tr class="header">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_386) %></h2>
                                    </td>
                                </tr>
                                <tr />
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_441) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:RadioButtonList runat="server" ID="SWNetPerfMon_Settings_Default_IP_Address_Resolution" class="CounterRollover">
                                            <asp:ListItem Value="4" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_389 %>" />
                                            <asp:ListItem Value="6" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_390 %>" />
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_388) %>
                                    </td>
                                </tr>
                                <tr class="applyButton">
                                    <td>&nbsp;</td>
                                    <td colspan="2">
                                        <orion:LocalizableButton id="bt_IPResolution" runat="server" DisplayType="Small" LocalizedText="CustomText" automation="ReApplyResolutionPreference" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_496 %>" OnClick="IPResolution_Click" OnClientClick='<%# DefaultSanitizer.SanitizeHtml(string.Format("return confirm(\"{0}\");", ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_11))) %>'/>
                                    </td>
                                </tr>
                                <tr class="header">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_424) %></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_442) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:TextBox runat="server" ID="Archive_Time" Text="<%# new DateTime(1900,1,1,0,15,0).ToShortTimeString() %>" Columns="5"></asp:TextBox>
                                        <asp:CustomValidator ControlToValidate="Archive_Time" ID="Archive_Time_Validator"
                                            ErrorMessage='<%# DefaultSanitizer.SanitizeHtml(string.Format("<img src=\"/Orion/images/Small-Down.gif\" title=\"{0}\"/>", Resources.CoreWebContent.WEBDATA_AK0_508)) %>'
                                            ValidateEmptyText="true" Display="Dynamic" runat="server" OnServerValidate="TimeValidator"></asp:CustomValidator>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_443) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LV0_01) %>
                                    </td>
                                    <td class="Property">
                                        <asp:CheckBox runat="server" ID="cbIndexDefragmentationEnabled" AutoPostBack="false" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LV0_02) %>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LV0_03) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="dbm-defragmentation-timeout" Text="3600"
                                            ID="tbIndexDefragmentationTimeout" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_LV0_04 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LV0_03) %>
                                    </td>
                                </tr>
                    
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LV0_05) %>
                                    </td>
                                    <td class="Property">
                                        <asp:CheckBox runat="server" ID="cbAutoResetLogLevelsEnabled" AutoPostBack="false" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LV0_06) %>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_15) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Auditing Trails" 
                                            ID="SWNetPerfMon_Settings_Retain_Auditing_Trails" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_16) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_444) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Detail" 
                                            ID="SWNetPerfMon_Settings_Retain_Detail" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>"
                                            UseCustomValidator="true" CustomClientValidation="ClientRetentionValidate"/>
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_445) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_446) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Hourly" 
                                            ID="SWNetPerfMon_Settings_Retain_Hourly" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>"
                                            UseCustomValidator="true" CustomClientValidation="ClientRetentionValidate"
                                            CustomValidatorMessage="<%$ HtmlEncodedResources: CoreWebContent,PollingSettings_Retention_Validator %>"/>
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_447) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_448) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Daily" 
                                            ID="SWNetPerfMon_Settings_Retain_Daily" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" 
                                            UseCustomValidator="true" CustomClientValidation="ClientRetentionValidate"
                                            CustomValidatorMessage="<%$ HtmlEncodedResources: CoreWebContent,PollingSettings_Retention_Validator %>"/>
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_449) %>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_223) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Container Detail" 
                                            ID="SWNetPerfMon_Settings_Retain_Container_Detail" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_224) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_225) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Container Hourly" 
                                            ID="SWNetPerfMon_Settings_Retain_Container_Hourly" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_226) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_227) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Container Daily" 
                                            ID="SWNetPerfMon_Settings_Retain_Container_Daily" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_228) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_1) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Baseline Collection Duration" 
                                            ID="SWNetPerfMon_Settings_Baseline_Collection_Duration" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"   
                                            UseCustomValidator="true" OnServerValidate="CollectionDuration_ServerValidate" CustomValidatorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_TK0_4 %>"
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_TK0_3 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_2) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LL0_1) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:DropDownList runat="server" ID="SWNetPerfMon_Settings_Interface_Baseline_Calculation_Frequency">
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_2 %>" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_3 %>" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_4 %>" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_5 %>" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_6 %>" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_7 %>" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_8 %>" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_9 %>" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_LL0_10 %>" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Property">
                                         <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LL0_11) %> 
                                         <a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionBaselineDataCalculation")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_51) %></a> <%-- » Learn more --%>
                                    </td>
                                </tr>
                                <tr id="InterfaceAvailabilityDetailRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_553) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_InterfaceAvailability_Retain_Detail" 
                                            ID="NPM_Settings_InterfaceAvailability_Retain_Detail" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_554 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_555) %>
                                    </td>
                                </tr>
                                <tr id="InterfaceAvailabilityHourlyRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_556) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_InterfaceAvailability_Retain_Hourly" 
                                            ID="NPM_Settings_InterfaceAvailability_Retain_Hourly" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_554 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_557) %>
                                    </td>
                                </tr>
                                <tr id="InterfaceAvailabilityDailyRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_558) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_InterfaceAvailability_Retain_Daily" 
                                            ID="NPM_Settings_InterfaceAvailability_Retain_Daily" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_559 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_560) %>
                                    </td>
                                </tr>
                                <tr id="InterfacesStaleRemovalEnabledRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Interfaces_RemoveStale_Enable) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="NPM_Settings_StaleInterfaces_RemovalEnabled" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Interfaces_RemoveStale_Enable_Description) %>
                                    </td>
                                </tr>
                                <tr id="InterfacesStaleRemovalIntervalDaysRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Interfaces_RemoveStale_DaysInterval) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_StaleInterfaces_RemovalIntervalDays" 
                                            ID="NPM_Settings_StaleInterfaces_RemovalIntervalDays" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_559 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Interfaces_RemoveStale_DaysInterval_Description) %>
                                    </td>
                                </tr>
                                <tr id="VolumesStaleRemovalEnabledRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Volumes_RemoveStale_Enable) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="SWNetPerfMon_Settings_StaleVolume_RemovalEnabled" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Volumes_RemoveStale_Enable_Description) %>
                                    </td>
                                </tr>
                                <tr id="VolumesStaleRemovalIntervalRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Volumes_RemoveStale_DaysInterval) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <orion:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-StaleVolume-RemovalIntervalDays" 
                                                                ID="SWNetPerfMon_Settings_StaleVolume_RemovalIntervalDays" Columns="5" Type="Integer" runat="server"
                                                                UseRequiredValidator="true" UseRangeValidator="true"
                                                                ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_559 %>"/>
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Volumes_RemoveStale_DaysInterval_Description) %>
                                    </td>
                                </tr>
                                <tr id="WirelessDetailRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_561) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_Wireless_Retain_Detail" 
                                            ID="NPM_Settings_Wireless_Retain_Detail" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_554 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_562) %>
                                    </td>
                                </tr>
                                <tr id="WirelessHourlyRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_563) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_Wireless_Retain_Hourly" 
                                            ID="NPM_Settings_Wireless_Retain_Hourly" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_554 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_564) %>
                                    </td>
                                </tr>
                                <tr id="WirelessDailyRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_565) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_Wireless_Retain_Daily" 
                                            ID="NPM_Settings_Wireless_Retain_Daily" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_559 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_566) %>
                                    </td>
                                </tr>
                                <tr id="UnDPDetailRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZB0_1) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_UnDP_Retain_Detail" 
                                            ID="NPM_Settings_UnDP_Retain_Detail" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_554 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZB0_4) %>
                                    </td>
                                </tr>
                                <tr id="UnDPHourlyRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZB0_2) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_UnDP_Retain_Hourly" 
                                            ID="NPM_Settings_UnDP_Retain_Hourly" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_554 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZB0_5) %>
                                    </td>
                                </tr>
                                <tr id="UnDPDailyRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZB0_3) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_UnDP_Retain_Daily" 
                                            ID="NPM_Settings_UnDP_Retain_Daily" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_559 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZB0_6) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_450) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Events" 
                                            ID="SWNetPerfMon_Settings_Retain_Events" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_451) %>
                                    </td>
                                </tr>
                                 <tr id="SyslogMessagesRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_452) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SysLog-MaxMessageAge" 
                                            ID="SysLog_MaxMessageAge" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_453) %>
                                    </td>
                                </tr>
                                 <tr id="TrapMessagesRetentionRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_454) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="Trap-MaxMessageAge" 
                                            ID="Trap_MaxMessageAge" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_455) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_456) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="AlertEngine-MaxAlertExecutionTimeout" 
                                            ID="Alert_MaxAlertExecutionTime" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_503 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_457) %>
                                    </td>
                                </tr>
								<tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_458) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="AlertEngine-AlertAcknowledgeUrlText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_492 %>"
                                            ID="Alert_AlertAcknowledgeUrlText" Type="Text" runat="server" UseRequiredValidator="false"
                                            MaxLength="150" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_459) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_83) %>
                                    </td>
                                     <td><asp:CheckBox runat="server" ID="SWNetPerfMon_Settings_AllowAlertActionsForUnmanaged" AutoPostBack="false" />&nbsp;</td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_84) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_460) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Retain Discovery"
                                             ID="SWNetPerfMon_Settings_Retain_Discovery" Columns="5" Type="Integer"
                                            runat="server" UseRequiredValidator="true" UseRangeValidator="true" 
                                             ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_461) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BI0_0) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon_Downtime_History_Retention"
                                             ID="SWNetPerfMon_Downtime_History_Retention" Columns="5" Type="Integer"
                                            runat="server" UseRequiredValidator="true" UseRangeValidator="true" 
                                             ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BI0_1) %>
                                    </td>
                                </tr>
                                <tr id="InterfaceObsoleteDataEnabledRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_6) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox runat="server" ID="ifObsoleteCb" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_7) %>
                                    </td>
                                </tr>
                                <tr id="InterfaceObsoleteDataPollCountRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Interfaces_ObsoleteData_PollCount) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_Interfaces_ObsoleteDataPoolCount"
                                                              ID="SWNetPerfMon_Settings_Default_Interface_Obsolete_Data_Poll_Count" Columns="5"
                                                              Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_LH0_8 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_490) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingSettings_Interfaces_ObsoleteData_PollCount_Description) %>
                                    </td>
                                </tr>
                                <tr class="header">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_425) %></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_462) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-ICMP Timeout" 
                                            ID="SWNetPerfMon_Settings_ICMP_Timeout" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_504 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_151) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_463) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_464) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-ICMP Data" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_493 %>"
                                            ID="SWNetPerfMon_Settings_ICMP_Data" Type="Text" runat="server" UseRequiredValidator="false"
                                            MaxLength="150" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_465) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_466) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-SNMP Timeout" 
                                            ID="SWNetPerfMon_Settings_SNMP_Timeout" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_505 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_151) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_467) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_468) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-SNMP Retries" 
                                            ID="SWNetPerfMon_Settings_SNMP_Retries" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_506 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_490) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_469) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_539) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:DropDownList runat="server" ID="SNMPEncodingDropDown">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_540) %>
                                    </td>
                                </tr>

                                  <tr id="UCSAPITimeoutRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_472) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-UCS API Timeout" 
                                            ID="SWNetPerfMon_Settings_UCS_API_Timeout" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_500 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_473) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_474) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="SWNetPerfMon_Settings_DHCP_EnableRDNS" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_475) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP1_10) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="SWNetPerfMon_Settings_EnableDowntimeMonitoring" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP1_11) %>
                                    </td>
                                </tr>
                                
                                <tr style="border-collapse:collapse;display:none;">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_9) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="Topology_IgnoreUnknownPorts" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_10) %>
                                    </td>
                                </tr>

                                <tr class="header">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_426) %></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_476) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:DropDownList runat="server" ID="SWNetPerfMon_Settings_Availability_Calculation">
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB0_122 %>" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TM0_88 %>" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_477) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_478) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="SWNetPerfMon_Settings_Baseline_Calculation" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_479) %>
                                    </td>
                                </tr>
                                <tr id="AutoDepEnabledRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AutoDependency_Enabled_Header) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="SWNetPerfMon_Settings_AutoDependency_Enabled" Checked="false" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AutoDependency_Enabled_Description) %>
                                    </td>
                                </tr>								
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_480) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="SWNetPerfMon_Settings_Baseline_AllowSecureData" Checked="false"
                                            runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_481) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_482) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="SWNetPerfMon-Settings-Default Fast Poll Interval"
                                             ID="SWNetPerfMon_Settings_Default_Fast_Poll_Interval" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                              ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_500 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_483) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_484) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:DropDownList runat="server" ID="SWNetPerfMon_Settings_Counter_Rollover" class="CounterRollover">
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_487 %>" Value="Method 1"></asp:ListItem>
                                            <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_488 %>" Value="Method 2" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Property CounterRolloverDescription">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_485) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_0) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:TextBox ID="SWNetPerfMon_Settings_Invalid_Dynamic_IP" runat="server" Enabled="true" autocomplete="off" MaxLength="16" />
                                        <orion:IPAddressValidator runat="server" ID="SWNetPerfMon_Settings_Invalid_Dynamic_IP_Validator" ControlToValidate="SWNetPerfMon_Settings_Invalid_Dynamic_IP" 
                                            ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_148 %>"/>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_3) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_4) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="SWNetPerfMon_Settings_VulnerabilityCheckDisabled" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_7) %>
                                    </td>
                                </tr>

                                <!--Hardware health section-->
                                <tr class="header" id="HardwareHealth_SectionHeader" runat="server">
                                    <td colspan="3">
                                        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_01) %></h2>
                                    </td>
                                </tr>
                                <tr id="HardwareHealth_StatisticsPollInterval_Control" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_6) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="HardwareHealth-StatisticsPollInterval" 
                                            ID="HardwareHealth_StatisticsPollInterval" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_3) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_7) %>
                                    </td>
                                </tr>
                                <tr id="HardwareHealth_RetainDetail_Control" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_444) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="HardwareHealth-RetainDetail" 
                                            ID="HardwareHealth_RetainDetail" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_3) %>
                                    </td>
                                </tr>
                                <tr id="HardwareHealth_RetainHourly_Control" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_446) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="HardwareHealth-RetainHourly" 
                                            ID="HardwareHealth_RetainHourly" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_4) %>
                                    </td>
                                </tr>
                                <tr id="HardwareHealth_RetainDaily_Control" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_448) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="HardwareHealth-RetainDaily" 
                                            ID="HardwareHealth_RetainDaily" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_502 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_489) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_5) %>
                                    </td>
                                </tr>
                                <tr id="HardwareHealth_PreferredCiscoMibRow" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_02) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:DropDownList runat="server" ID="HardwareHealth_PreferredCiscoMib">
                                            <asp:ListItem Text="CISCO-ENTITY-SENSOR-MIB" Value="CISCO-ENTITY-SENSOR-MIB"></asp:ListItem>
                                            <asp:ListItem Text="CISCO-ENVMON-MIB" Value="CISCO-ENVMON-MIB"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_03) %>
                                    </td>
                                </tr>

                                <!--Multicast section-->
                                <tr class="header" id="Multicast_SectionHeader" runat="server">
                                    <td colspan="3">
                                        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_8) %></h2>
                                    </td>
                                </tr>
                                <tr id="Multicast_RouteTablePollInterval_Control" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_9) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_MulticastRouting_MulticastRouteTable_PollInterval" 
                                            ID="Multicast_RouteTablePollInterval" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_3) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_10) %>
                                    </td>
                                </tr>

                                <!--Routing section-->
								<tr class="header" id="NPM_RoutingSection" runat="server">
                                    <td colspan="3">
                                        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_11) %></h2>
                                    </td>
                                </tr>
                                <tr id="NPM_RoutingNeighborPollIntervalControl" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_12) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_Routing_Neighbor_PollInterval" 
                                            ID="NPM_RoutingNeighborPollInterval" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_3) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_13) %>
                                    </td>
                                </tr>
                                <tr id="NPM_RoutingRouteTablePollIntervalControl" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_14) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_Routing_RouteTable_PollInterval" 
                                            ID="NPM_RoutingRouteTablePollInterval" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_3) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_15) %>
                                    </td>
                                </tr>
                                <tr id="NPM_RoutingVRFPollIntervalControl" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_16) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_Routing_VRF_PollInterval" 
                                            ID="NPM_RoutingVRFPollInterval" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_501 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MZ0_3) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_17) %>
                                    </td>
                                </tr>

								<!--Wireless section-->
								<tr class="header" id="WirelessSection" runat="server">
                                    <td colspan="3">
                                        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OC0_04) %></h2>
                                    </td>
                                </tr>
                                <tr id="WirelessRDNSTimeout" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OC0_02) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="WLP_Settings_ClientRdnsTimeout" 
                                            ID="WLP_Settings_ClientRdnsTimeout" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_500 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OC0_03) %>
                                    </td>
                                </tr>
                                <tr id="WirelessRogueAp" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BI0_2) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:CheckBox ID="WLP_Settings_PollRogues" Checked="true" runat="server" />
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BI0_3) %>
                                    </td>
                                </tr>
								
								<!--Wireless Heatmaps section-->
                                <tr class="header" id="WirelessHeatMapSection" runat="server">
                                    <td colspan="3">
                                        <h2>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LL0_21) %></h2>
                                    </td>
                                </tr>
                                <tr id="MapGeneration_Time_Row" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LL0_22) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <asp:TextBox runat="server" ID="MapGeneration_Time" Text="<%# new DateTime(1900,1,1,0,45,0).ToShortTimeString() %>" Columns="7"></asp:TextBox>
                                        <asp:CustomValidator ControlToValidate="MapGeneration_Time" ID="MapGeneration_Time_Validator"
                                            ErrorMessage='<%# DefaultSanitizer.SanitizeHtml(string.Format("<img src=\"/Orion/images/Small-Down.gif\" title=\"{0}\"/>", Resources.CoreWebContent.WEBDATA_AK0_508)) %>'
                                            ValidateEmptyText="true" Display="Dynamic" runat="server" OnServerValidate="MapGenerationTimeValidator"></asp:CustomValidator>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LL0_23) %>
                                    </td>
                                </tr>
								<tr id="ClientLocation_Poll_Interval_Row" runat="server">
                                    <td class="PropertyHeader">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LL0_24) %>
                                    </td>
                                    <td class="Property" align="left">
                                        <web:ValidatedTextBox CustomStringValue="NPM_Settings_WLHM_ClientLocation_PollInterval" 
                                            ID="NPM_Settings_WLHM_ClientLocation_PollInterval" Columns="5" Type="Integer" runat="server"
                                            UseRequiredValidator="true" UseRangeValidator="true"  
                                            ValidatorText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_500 %>" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                                    </td>
                                    <td class="Property">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LL0_25) %>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="sw-btn-bar">
                    <orion:LocalizableButton ID="bt_Submit" runat="server" DisplayType="Primary" LocalizedText="Submit" automation="Submit" OnClick="Submit_Click"/>
                    <orion:LocalizableButton ID="bt_Cancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" automation="Cancel" OnClick="Cancel_Click" CausesValidation="false"/>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
