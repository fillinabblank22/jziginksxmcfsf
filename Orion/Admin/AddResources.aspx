<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/CustomizeView.master" AutoEventWireup="true" CodeFile="AddResources.aspx.cs" Inherits="Orion_Admin_AddResources" %>
<%@ Assembly Name="Infragistics2.WebUI.Shared.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igNav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics2.WebUI.UltraWebNavigator.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"%>


<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
<h1><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_311,String.Format("<a href=\"/Orion/View.aspx?ViewID={0}\">{1}</a>",View.ViewID,View.ViewTitle),Column )) %></h1>

<ignav:UltraWebTree ID="ResourceTemplateTree" runat="server" DefaultImage="" Indentation="20" OnInit="ResourceTemplateTree_Init" EnableViewState="false">
	<Levels>
		<ignav:Level Index="0" LevelCheckBoxes="False" ColumnName="Title" RelationName="ResourceTemplates" />
		<ignav:Level Index="1" LevelCheckBoxes="True" ColumnName="Title" />
	</Levels>
</ignav:UltraWebTree>
	<br />
	<orion:LocalizableButton ID="Submit" LocalizedText="Submit" runat="server" OnClick="Submit_Click" DisplayType="Primary" />
	 <script language="javascript" type="text/javascript">
	     $("div[id^='ctl00ctl00ContentPlaceHolder1adminContentPlaceholderResourceTemplateTree_']").addClass("AddResourcesPadding");
    </script>
</asp:Content>

