﻿<%@ Page Title="Hubble Data Viewer" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Hubble" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.2.0"/>

<style type="text/css">
    #contents
    {
        width:80%;
    }
    
    h1 
    {
        font-size: 12pt;
    }

    #logViewer 
    {
        padding: 5px 20px;
    }

    #cachedHubbleResults
    {
        padding: 5px 20px;
    }
    
    #textareaDiv textarea
    {
        width: 100%;
        border: 1px solid #8FB3D4;
        font-family: Monaco, 'Courier New', Courier, monospace;
        font-size: 11px;
        padding: 2px;
        width: 100%;        
    }
    
    #textareaDiv
    {
        padding: 10px 0px;
    }
    
    #cacheContents 
    {
        border-spacing: 0;
        width: 100%;
        padding: 10px 0px;
        table-layout: fixed;
    }
    
    #cacheContents thead th
    {
        font-weight: bold;
        padding: 10px 5px;
        border-bottom: 1px solid black;      
        text-decoration: underline;
        cursor: pointer;         
    }
    
    #cacheContents .odd
    {
        background-color: #f8f8f8;
    }
    
    #cacheContents tbody td
    {
        padding: 10px 5px;
    }
    
    #cacheContents tbody td a
    {
        text-decoration: underline;
    }
    
    #cacheContents .url 
    {
        word-wrap: break-word;                
    }   
    

</style>


<script type="text/javascript">

    $().ready(function () {

        $('#applyButton').click(function () {
            var input = $('#hubbleEntry').val();
            var prefix = "JSON Data:";

            if (input.substr(0, prefix.length) === prefix) {
                input = input.substr(prefix.length);
            }

            var hubbleData = Ext.decode(input);
            Hubble.loadFromObject(hubbleData, true);
        });

        if (jQuery.tablesorter)
            $("#cacheContents").tablesorter({ widgets: ['zebra'] });


    });

    function showHubbleResults(id) {
        Hubble.loadFromId(id, true);
        return false;
    }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">


    <div id="contents" >
        <h1>Cached Hubble Data</h1>

        <div id="cachedHubbleResults">
            Below are the Hubble results that have been seen across the entire website for the current user
            in the last few minutes.  Click on "Load Hubble Results" to view the Hubble results for that 
            page (instead of this page's results).

            <table id="cacheContents" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th width="40%">Page Name</th>
                        <th>Generated Date</th>
                        <th>Duration (ms)</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        bool isOdd = false;
                        foreach (var item in HubbleCachedItem.LoadAll().Where(x => x.User == User.Identity.Name))
                        {
                            isOdd = !isOdd;
                        %>
                            <tr class="<%= isOdd ? "odd" : "even" %>">
                                <td class="url"><%= DefaultSanitizer.SanitizeHtml(item.Name) %></td>
                                <td><%= item.Timestamp.ToString() %></td>
                                <td><%= item.RequestDuration %> </td>
                                <td><a href="#" onclick="return showHubbleResults('<%= item.RequestId %>')">Load Hubble Results</a></td>
                            </tr>
                    <% } %>

                </tbody>
            </table>

        </div>

        <br/>


        <h1>Hubble Log Data Viewer</h1>
        <div id="logViewer">
            You can use this page to view Hubble Data from the Hubble logfile (Hubble.log).  
            Just copy the line that begins with "JSON Data:" into the textbox below and click Apply.
            The Hubble UI (the red box in the upper left of the page) will then display this new data.   

            <div id="textareaDiv">
                <textarea id="hubbleEntry"rows="5"></textarea>
            </div>
            <input type="button" id="applyButton" value="Apply"/>
        </div>
    </div>
    

</asp:Content>

