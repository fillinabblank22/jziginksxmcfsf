using System;
using System.Linq;

using SolarWinds.Orion.Web;

public partial class Orion_Admin_ModuleConfig : System.Web.UI.Page
{
    private OrionModule _module;

    protected OrionModule Module
    {
        get 
        {
			if (_module == null)
            {
                string moduleID = this.Request.QueryString["ModuleID"];
                if (string.IsNullOrEmpty(moduleID))
                    return null;

                foreach (OrionModule myModule in OrionModuleManager.GetModules())
                {
                    if (moduleID.Equals(myModule.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        _module = myModule;
                    }
                } 
            }

            return _module;
        }
    }

    protected override void OnInit(EventArgs e)
    {
		if (!string.IsNullOrEmpty(this.Module.DefaultConfigPage))
			Response.Redirect(this.Module.DefaultConfigPage);

		if (this.Module.Version == null)
			this.Title = string.Format("Configuration Settings for {0}", this.Module.Title);
		else
			this.Title = string.Format("Configuration Settings for {0} {1}", this.Module.Title, this.Module.Version);

        var featureManager = new SolarWinds.Orion.Core.Common.FeatureManager();
        var enabledFeatures = (featureManager.EnabledFeatures).Concat(featureManager.ValidElementTypes);

        this.rptConfigPages.DataSource =
            OrionModuleManager.FilterLinksByFeatureDependencies(
                OrionModuleManager.GetConfigPagesByModule(this.Module.Name), enabledFeatures);
        this.rptConfigPages.DataBind();

        base.OnInit(e);
    }
}
