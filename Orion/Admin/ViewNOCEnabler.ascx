<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewNOCEnabler.ascx.cs"
    Inherits="Orion_Admin_ViewNOCEnabler" %>
<%@ Import Namespace="Resources" %>
<div style="padding-top: 20px;">
    <h2 style="display: inline;"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VL0_2) %></h2>
    <span class="LinkArrow coloredLink">&#187;</span>
    <a href="/Orion/Admin/NOCView.aspx" class="coloredLink"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VL0_5) %></a>
</div>
<div><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VL0_3) %></div>
<asp:HiddenField ID="rotateTimeType" runat="server" />
<asp:HiddenField ID="rotateTime" runat="server" />
<asp:CheckBox ID="chkEnableNOCView" CssClass="chkEnableNOCView" onclick="EnableNOCView(this.checked)"
   runat="server" Text=" <%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_6 %>" />
<div class="viewNOCMode">
    <div class="viewMultiplePages" style="display:block">
        <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VL0_7) %>&nbsp;<a class="coloredLink" href="<%= DefaultSanitizer.SanitizeHtml(NOCViewUrl) %>" target="_blank"><%= DefaultSanitizer.SanitizeHtml(string.Format(CoreWebContent.WEBDATA_IB0_185, View.ViewGroupName)) %></a>
        </div>   
    
    <div id="viewMultiplePages" class="viewMultiplePages" >
        <span style="float: left;"> <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VL0_10) %></span>
        <div id="timeValue" >
        </div>
        <div id="timeTypeValue" >
        </div>
        <span style="float: left;"> <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_IB0_187) %></span>
        <div style="clear: both;"></div>
    </div>
    <div id="multiplePagesMessage" class="viewMultiplePages" >
        <span style="float: left;">
            <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_VL0_9) %>
        </span>        
        <div style="clear: both;"></div>
    </div>
</div>
<script type="text/javascript">
    function ConvertTime(time, timeType) {
        if (timeType == '<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_IB0_48) %>') {
            if (new RegExp('^[0-9]+$').test(time)) {
                var endTime=time * 60;
                $('#<%=rotateTime.ClientID%>').attr('value',endTime );
                SetViewNOCRotateTime(endTime);
            }
        } else {
            if (new RegExp('^[0-9]+$').test(time)) {
                $('#<%=rotateTime.ClientID%>').attr('value', RotateTimeCombo.getValue());
                SetViewNOCRotateTime(time);
            }
        }
    };
    function getViewID(){ return '<%=View.ViewID %>';};
    function getViewGroup(){ return '<%=View.ViewGroup %>';};
    function getRelatedViews(){return '<%=this.RelatedViews.Count %>';};
    function SetViewNOCRotateTime(rotateTime,success)
    {
        SW.Core.Services.callWebService('/Orion/Admin/CustomizeView.aspx', 'SetViewNOCRotateTime', { rotateTime: rotateTime, viewID: getViewID(), viewGroup: getViewGroup() }, function(res) { if (success) success(res); });
    }
    var onNocEnableChanged = <%=(!string.IsNullOrEmpty(NocViewEnableChangedJs))? NocViewEnableChangedJs: "function(){}"%> ;
    
    var RotateTimeCombo = new Ext.form.ComboBox(
                {
                    mode: 'local',
                    triggerAction: 'all',
                    width: 50,
                    typeAhead: false,
                    renderTo: 'timeValue',
                    store: ['5', '10', '15', '20', '25', '30', '35'],
                    id: 'rotationTimeCombo',
                    xtype: 'numberfield',
                    forceSelection: false,
                    selectOnFocus: true,
                    validator: function (val) {
                        var result = new RegExp('^[0-9]+$').test(val);
                        if (result)
                            return result;
                        else {                       
                            return '<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_AK0_508) %>';
                        }
                    },
                    listeners: {
                        'select': function () {
                            ConvertTime(this.getValue(), RotateTimeType.getValue());                           
                        },
                        'blur': function () {
                            ConvertTime(this.getValue(), RotateTimeType.getValue());                            
                        }
                    }
                });               

                var RotateTimeType = new Ext.form.ComboBox(
                {
                    mode: 'local',
                    triggerAction: 'all',
                    width: 90,
                    typeAhead: true,
                    renderTo: 'timeTypeValue',
                    store: ['<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_IB0_46) %>', '<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_IB0_48) %>'],
                    id: 'rotationTimeTypeCombo',
                    xtype: 'numberfield',
                    editable: false,
                    forceSelection: false,
                    selectOnFocus: true,
                    listeners: {
                        'select': function () {
                            ConvertTime( RotateTimeCombo.getValue(),this.getValue());
                            $('#<%=rotateTimeType.ClientID%>').attr('value', this.getValue());
                        }
                    }
                });
                     

    if ($('#<%=rotateTimeType.ClientID%>').val()) {
        RotateTimeType.setValue($('#<%=rotateTimeType.ClientID%>').val());
    } else {
        $('#<%=rotateTimeType.ClientID%>').val(RotateTimeType.getStore().getAt(0).data.field1);
        RotateTimeType.setValue(RotateTimeType.getStore().getAt(0).data.field1);
    }

    if ($('#<%=rotateTime.ClientID%>').val()>0) {
        RotateTimeCombo.setValue($('#<%=rotateTime.ClientID%>').val());
    } else {
        $('#<%=rotateTime.ClientID%>').val(15);
        if ($('.chkEnableNOCView input:first-child').prop('checked') & getRelatedViews()>2)  
        SetViewNOCRotateTime(15);
        RotateTimeCombo.setValue(15);
    }

    $(document).ready(function () {
         if ($('.chkEnableNOCView input:first-child').prop('checked'))           
                $('.viewNOCMode').css('display', 'block');            
                 else        
                   $('.viewNOCMode').css('display', 'none');
        if (getViewGroup()!=0 & getRelatedViews()>2) {
            $('#viewMultiplePages').css('display', 'block');
            }
            else
            {
            $('#multiplePagesMessage').css('display', 'block');
        }
          onNocEnableChanged($('.chkEnableNOCView input:first-child').prop('checked'));
    });  

    function EnableNOCView(elem) {
        SW.Core.Services.callWebService('/Orion/Admin/CustomizeView.aspx', 'SetViewNOCMode', { isEnable: elem, viewID: getViewID(), viewGroup: getViewGroup() }, function() {
            $('.viewNOCMode').css('display', (elem) ? 'block' : 'none');
        });
        if(elem)
        RotateTimeCombo.setValue(15);
        onNocEnableChanged(elem);        
    };

</script>
