using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_ResetPassword : System.Web.UI.Page
{
    internal ProfileCommon _profile;
    
    protected override void OnInit(EventArgs e)
    {
        // get profile of current logged in user
        _profile = Profile.GetProfile(this.User.Identity.Name);

        // reset password NOT permissable for Windows account types
        if (_profile.AccountType == (int)GroupAccountType.Sql)
            SetupFormControls(true);
        else
            SetupFormControls(false);

        base.OnInit(e);
    }
    
    private void SetupFormControls(Boolean showControl)            
    {
        this.paraPasswordBoxes.Visible = showControl;
        this.btnChangePassword.Visible = showControl;

        this.paraRestrictAccount.Visible = !showControl;
        this.btnContinue.Visible = !showControl;
    }
    
    protected void ChangePasswordClick(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !Profile.AllowAdmin)
            return;

		MembershipUser myUser = Membership.GetUser(this.User.Identity.Name);

		if (myUser != null && this.Page.IsValid)
		{
			try
			{
				// If password is blank, we use the ResetPassword() method, 
				//  which is a back-door to setting a blank password.
				if (string.IsNullOrEmpty(this.txtPassword.Text))
				{
					myUser.ResetPassword();
				}
				else
				{
					// fill in old password with a dummy entry
					// because we don't care about old password in the ChangePassword method
					if (!myUser.ChangePassword("dummy", txtPassword.Text))
					{
						throw new Exception();
					}
				}
				this.DisplayStep2();
			}
			catch
			{
				phErrorMessage.Controls.Clear();
				phErrorMessage.Controls.Add(new LiteralControl(Resources.CoreWebContent.WEBCODE_TM0_80));
			}
		}
	}

	protected void ValidateOldPassword(object sender, ServerValidateEventArgs e)
	{
		e.IsValid = Membership.ValidateUser(User.Identity.Name, this.txtOldPassword.Text);
	}

	protected void ContinueClick(object sender, EventArgs e)
	{
		Response.Redirect("/");
	}

	private void DisplayStep2()
	{
		this.paraPasswordBoxes.Visible = false;
		this.btnChangePassword.Visible = false;
		this.btnContinue.Visible = true;
		this.phSuccessMessage.Visible = true;
	}
}
