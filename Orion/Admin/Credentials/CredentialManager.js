﻿Ext.namespace('SW');
Ext.namespace('SW.Core');

SW.Core.CredentialManager = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;
    var brokenExists = false;

    ORION.prefix = "CredentialManager_";

    UpdateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;

        if ($("#isDemoMode").length) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
        }
        else {
            var selItems = grid.getSelectionModel();
            var count = selItems.getCount();
            var editButtonDisabled = (count != 1);
            var deleteButtonDisabled = false;

            selItems.each(function (rec) {
                if ((rec.data["NodesAssigned"] != "0") || ((rec.data["ActionsAssigned"]) != "0")) {
                    deleteButtonDisabled = true;
                }
            });

            map.EditButton.setDisabled(editButtonDisabled);
            map.DeleteButton.setDisabled(count === 0 || deleteButtonDisabled);
        }
    };

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // Column rendering functions
    function renderCredential(value, meta, record) {
        return encodeHTML(value);
    };

    // Column rendering functions
    function renderUserName(value, meta, record) {
        return encodeHTML(value);
    };

    function renderNodeCount(value, meta, record) {
        var actionCount = record.get('ActionsAssigned');
        var result = "";

        if (value == 1)
            result += value + " " + "@{R=Core.Strings;K=WEBJS_VB0_98; E=js}";
        else if (value > 1)
            result += value + " " + "@{R=Core.Strings;K=WEBJS_VB0_99; E=js}";

        if ((value > 0) && (actionCount > 0))
            result += ", ";

        if (actionCount == 1)
            result += actionCount + "@{R=Core.Strings.3;K=CredManager_Action; E=js}";
        else if (actionCount > 1)
            result += actionCount + "@{R=Core.Strings.3;K=CredManager_Actions; E=js}";

        if ((actionCount == 0) && (value == 0))
            return "";

        return result;
    };

    function renderActionCount(value, meta, record) {
        if (value === 1)
            return value + " " + "@{R=Core.Strings.3;K=CredManager_Action; E=js}";
        else
            return value + " " + "@{R=Core.Strings.3;K=CredManager_Actions; E=js}";
    };

    function editSelecetedCredential() {
        var selItems = grid.getSelectionModel();
        brokenExists = false;
        selItems.each(function (rec) {
            eval('SW.Core.CredentialManager.' + rec.data["Control"] + '.Show(' + rec.data["CredentialID"] + ');');
            return;
        });
    }

    function deleteSelecetedCredential() {
        if (!confirm("@{R=Core.Strings;K=WEBJS_VB0_100; E=js}"))
            return;

        var ids = [];

        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["CredentialID"]);
        });

        ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "DeleteCredentials", { credentialIDs: ids }, function (result) {
            grid.getStore().reload();
        });
    }

    return {
        reload: function () { grid.getStore().reload(); },
        init: function () {

            // load default page size from Orion settings
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);						
                        this.doRefresh();
                    }, this);
                }
            });


            // set selection model to checkbox
            selectorModel = new Ext.grid.CheckboxSelectionModel();

            selectorModel.on("selectionchange", UpdateToolbarButtons);

            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/Services/CredentialManagerService.asmx/GetCredentials",
                [
                { name: 'CredentialID', mapping: 0 },
                { name: 'CredentialName', mapping: 1 },
                { name: 'Username', mapping: 2 },
                { name: 'NodesAssigned', mapping: 3 },
                { name: 'ActionsAssigned', mapping: 4 },
                { name: 'Control', mapping: 5 },
                { name: 'IsBroken', mapping: 6 }
                ],
                "Name");


            // define grid panel
            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
                    selectorModel,
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'CredentialID' },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_101; E=js}', width: 200, sortable: true, dataIndex: 'CredentialName', renderer: renderUserName },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_102; E=js}', width: 200, sortable: true, dataIndex: 'Username', renderer: renderUserName },
                    { header: '@{R=Core.Strings.3;K=CredManager_AssignedToEntities; E=js}', width: 200, sortable: true, dataIndex: 'NodesAssigned', renderer: renderNodeCount }
                ],
                sm: selectorModel,
                viewConfig: {
                    forceFit: true,
                    getRowClass: function (record) {
                        if (record.get('IsBroken') === true) {
                            brokenExists = true;
                            return 'broken-row';
                        }
                    }
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 830,
                height: 300,
                stripeRows: true,
                trackMouseOver: false,
                tbar: [
                {
                    id: 'AddButton',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_104; E=js}',
                    iconCls: 'add',
                    // TODO: add menu when pluggable for other credential types
                    handler: function () { eval('SW.Core.CredentialManager.UsernamePasswordCredential.Show(null);'); }
                }, '-',
                {
                    id: 'EditButton',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_105; E=js}',
                    iconCls: 'edit',
                    handler: function () { editSelecetedCredential(); }
                }, '-',
                {
                    id: 'DeleteButton',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_106; E=js}',
                    iconCls: 'del',
                    handler: function () { deleteSelecetedCredential(); }
                }],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_VB0_107; E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VB0_108; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');

                if (brokenExists === true) {
                    $('#suggestionFail')
                        .html('<div class="sw-suggestion sw-suggestion-fail" style="margin-bottom: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings.2;K=WEBJS_SO0_14;E=js}</div>');
                    $('#suggestionFail').show();
                } else {
                    $('#suggestionFail').hide();
                }
            });

            grid.render('Grid');
            UpdateToolbarButtons();
            grid.bottomToolbar.addPageSizer();
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = {};
            grid.store.load({ params: { start: 0, limit: pageSizeNum } });
        }
    };

} ();


Ext.onReady(SW.Core.CredentialManager.init, SW.Core.CredentialManager);