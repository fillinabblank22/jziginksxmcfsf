﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="SNMPCredentialManager.aspx.cs" 
    Inherits="Orion_Admin_Credentials_SNMPCredentialManager" Title="<%$HtmlEncodedResources:CoreWebContent,WEBCODE_SO0_53%>" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="/Admin/Credentials/js/SNMPCredentialManager.js" />
     <input type="hidden" name="SNMPCredentialManager_PageSize" id="SNMPCredentialManager_PageSize"
        value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "SNMPCredentialManager_PageSize", "20")) %>' />

    <style type="text/css">
        span.Label { white-space:nowrap; margin-left:5px;}
        .smallText {color:#979797;font-size:9px;}
        .add { background-image:url(/Orion/images/add_16x16.gif) !important; }
        .edit { background-image:url(/Orion/images/edit_16x16.gif) !important; }
        .del { background-image:url(/Orion/images/delete_16x16.gif) !important; }
        #Grid td { padding-right: 0px; padding-bottom: 0px; vertical-align: middle; }
        #Grid { padding-top: 12px; }
        #CredManagerDialogFrame {height:auto !important;}
        #CredManagerDialogBody {height:auto !important;}
        .dialogBody td { padding: 5px; padding-bottom: 5px; vertical-align: middle; }
        .x-btn td {text-align: center !important;}
        .sw-btn-primary { background: #297994 none repeat scroll 0 0; border-color: #297994; color: #ffffff !important; }
        a.sw-btn-primary:hover { background: #204a63 none repeat scroll 0 0; border-color: #204a63; color: #ffffff !important; }
        .helpfulText {color: #5F5F5F;font-size: 10px;text-align: left;}
        .broken-row {  background: #FFE4E0; color: #D50000; }
</style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <%-- Render the DIV control only in demo mode --%>
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    
    <h1 style="font-size:large"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

     <div style="padding-top: 10px;" id="suggestionFail">
    </div>
    <div id="gridPanel" style="max-width:870px;">	
        <div id="Grid"></div>
    </div>
    
     <div id="SNMPCredManagerDialogFrame" class="x-hidden">
        <div class="x-window-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_233) %></div>
        <div id="SNMPCredManagerDialogBody" class="x-panel-body dialogBody" >
           <table>
             <tr>
                 <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_521) %></span></td>
                 <td><input ID="credName" maxlength="50"></input></td>
             </tr>
             <tr>
                <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_72) %></span></td>
                <td><input ID="credUsername" maxlength="50"></input></td>
             </tr>
             <tr>
                 <td style="padding-bottom: 2px;"><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_36) %></span></td>
                 <td style="padding-bottom: 2px;"><input ID="credContext" maxlength="50"></input></td>
             </tr>
               <tr>
                   <td style="padding-top: 0px;"></td>
                   <td style="padding-left: 5px; padding-top: 0px;"><div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_234) %></div></td>
                   </tr>
               <tr>
                 <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_75) %></span></td>
                 <td>
                      <select ID="authTypeSelect">
                            <option value="None"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_9) %></option>
                          <% if (!fipsModeEnabled)
                             { %>                          
                          <option value="MD5"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_75) %></option>
                          <% } %>>
                          <option value="SHA1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_76) %></option>
                      </select> 
                 </td>
             </tr>
             <tr>
                 <td style="padding-bottom: 2px;"><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></span></td>
                 <td style="padding-bottom: 2px;"><input ID="credAuthPassword" maxlength="50" type="password"></input></td>
             </tr>
               <tr>
                   <td style="padding-bottom: 2px;"></td>
                   <td style="padding-bottom: 2px;"><input ID="authPassAsKey" type="checkbox"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_57) %> <br></td>
               </tr>
                <tr>
                    <td style="padding-top: 0px;"></td>
                   <td style="padding-left: 5px; padding-top: 0px;"><div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_235) %></div></td>
                   </tr>
               <tr>
                 <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_77) %></span></td>
                 <td>
                      <select ID="encTypeSelect">
                            <option value="None"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_9) %></option>
                          <% if (!fipsModeEnabled)
                             { %>
                            <option value="DES56"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_77) %></option>
                          <% } %>>
                            <option value="AES128"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_78) %></option>
                            <option value="AES192"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_85) %></option>
                            <option value="AES256"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_86) %></option> 
                      </select> 
                 </td>
             </tr>
             <tr>
                 <td style="padding-bottom: 2px;"><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></span></td>
                 <td style="padding-bottom: 2px;"><input ID="credEncPassword" maxlength="50" type="password"></input></td>
             </tr>
               <tr>
                   <td style="padding-bottom: 2px;"></td>
                   <td style="padding-bottom: 2px;">
                        <input ID="encPassAsKey" type="checkbox"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_57) %>      
                   </td>
               </tr>
                <tr>
                   <td style="padding-top: 0px;"></td>
                   <td style="padding-left: 5px; padding-top: 0px; padding-bottom: 0px;"><div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_236) %></div></td>
                   </tr>
             <tr>
                 <td colspan="2" style="color: red; padding-bottom: 2px;"><span id="errorDiv"></span></td>
             </tr>
         </table>
        </div>
    </div>
</asp:Content>


