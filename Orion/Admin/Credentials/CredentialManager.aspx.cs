using System;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using System.IO;
using SolarWinds.Logging;

public partial class Orion_Admin_Credentials_CredentialManager : System.Web.UI.Page, IBypassAccessLimitation
{
    private const string PLUGIN_PATH_FORMAT = "/Orion/Admin/Credentials/EditPlugins/{0}";
    private static readonly Log log = new Log();

    protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement)
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", HttpUtility.UrlEncode(Resources.CoreWebContent.WEBCODE_VB0_3)));
        }

        try
        {
            // load edit plugins
            foreach (string script in Directory.GetFiles(Server.MapPath("/Orion/Admin/Credentials/EditPlugins/"), "*.js"))
            {
                string filename = Path.GetFileName(script);
                log.DebugFormat("Including edit plugin {0}", filename);
                //ClientScript.RegisterClientScriptInclude(filename, String.Format(PLUGIN_PATH_FORMAT, filename));
                OrionInclude.CoreFile(String.Format("Admin/Credentials/EditPlugins/{0}", filename));
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when loading edit plugins", ex);
        }

        //((Orion_VIM_Admin_VIMAdminPage)this.Master).HelpFragment = "OrionVIMAG_VMwareCredentialsLibrary";
	}
}
