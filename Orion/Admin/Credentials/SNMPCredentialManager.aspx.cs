using System;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Admin_Credentials_SNMPCredentialManager : System.Web.UI.Page, IBypassAccessLimitation
{
     protected static bool fipsModeEnabled = EnginesDAL.IsFIPSModeEnabledOnAnyEngine();

    protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);

        if (!OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement)
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", HttpUtility.UrlEncode(Resources.CoreWebContent.WEBCODE_VB0_3)));
        }
	}
}
