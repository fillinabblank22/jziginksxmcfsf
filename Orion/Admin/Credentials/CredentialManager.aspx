﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="CredentialManager.aspx.cs" 
    Inherits="Orion_Admin_Credentials_CredentialManager" Title="<%$HtmlEncodedResources:CoreWebContent,WEBCODE_VB0_312%>" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="/Admin/Credentials/CredentialManager.js" />
    
    <input type="hidden" name="CredentialManager_PageSize" id="CredentialManager_PageSize"
        value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "CredentialManager_PageSize", "20")) %>' />

    <style type="text/css">
        span.Label { white-space:nowrap; margin-left:5px;}
        .smallText {color:#979797;font-size:9px;}
        .add { background-image:url(/Orion/images/add_16x16.gif) !important; }
        .edit { background-image:url(/Orion/images/edit_16x16.gif) !important; }
        .del { background-image:url(/Orion/images/delete_16x16.gif) !important; }
        #Grid td { padding-right: 0px; padding-bottom: 0px; vertical-align: middle; }
        #Grid { padding-top: 12px; }
        #CredManagerDialogFrame {height:auto !important;}
        #CredManagerDialogBody {height:auto !important;}
        .dialogBody td { padding: 5px; padding-bottom: 5px; vertical-align: middle; }
        .x-btn td {text-align: center !important;}
        .sw-btn-primary { background: #297994 none repeat scroll 0 0; border-color: #297994; color: #ffffff !important; }
        a.sw-btn-primary:hover { background: #204a63 none repeat scroll 0 0; border-color: #204a63; color: #ffffff !important; }
        .broken-row {  background: #FFE4E0; color: #D50000; }
</style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <%-- Render the DIV control only in demo mode --%>
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    
    <h1 style="font-size:large"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

    <div style="padding-top: 10px;" id="suggestionFail">
    </div>

    <div id="gridPanel" style="max-width:870px;">	
        <div id="Grid"></div>
    </div>

    <div id="CredManagerDialogFrame" class="x-hidden">
        <div class="x-window-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_546) %></div>
        <div id="CredManagerDialogBody" class="x-panel-body dialogBody" >
           <table>
             <tr>
                 <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_521) %></span></td>
                 <td><input ID="credDescription" maxlength="50"></input></td>
             </tr>
             <tr>
                <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_73) %></span></td>
                <td><input ID="credUsername" maxlength="50"></input></td>
             </tr>
             <tr>
                 <td></td>
                 <td style="max-width:130px;">
                     <div class="smallText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_547) %></div>
                     <span class="LinkArrow">»</span>
                     <orion:HelpLink runat="server" HelpUrlFragment="OrionPHWhatPrivilegesNeed" HelpDescription="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_516 %>" CssClass="sw-link sw-text-reset-small" />
                 </td>
             </tr>
             <tr>
                 <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></span></td>
                 <td><input ID="credPassword" maxlength="50" type="password"></input></td>
             </tr>
             <tr>
                 <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_182) %></span></td>
                 <td><input ID="credConfirm" maxlength="50" type="password"></input></td>
             </tr>
             <tr>
                 <td colspan="2" style="color: red;"><span id="errorDiv"></span></td>
             </tr>
         </table>
        </div>
    </div>
</asp:Content>


