﻿Ext.namespace('SW');
Ext.namespace('SW.Core');

SW.Core.SNMPCredentialManager = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var dialog;
    var pageSizeNum;
    var editedCredentialId;
    var brokenExists = false;

    ORION.prefix = "SNMPCredentialManager_";

    UpdateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;

        if ($("#isDemoMode").length) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
        }
        else {
            var selItems = grid.getSelectionModel();
            var count = selItems.getCount();
            var editButtonDisabled = (count != 1);
            var deleteButtonDisabled = false;

            selItems.each(function (rec) {
                if (rec.data["NodesAssigned"] != "0") {
                    deleteButtonDisabled = true;
                }
            });

            map.EditButton.setDisabled(editButtonDisabled);
            map.DeleteButton.setDisabled(count === 0 || deleteButtonDisabled);
        }
    };

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // Column rendering functions
    function renderCredential(value, meta, record) {
        return encodeHTML(value);
    };

    // Column rendering functions
    function renderUserName(value, meta, record) {
        return encodeHTML(value);
    };

    function renderNodeCount(value, meta, record) {
        if (value == 1)
            return value + " " +"@{R=Core.Strings;K=WEBJS_VB0_98; E=js}";
        else
            return value + " "+"@{R=Core.Strings;K=WEBJS_VB0_99; E=js}";
    };

    function addEditCredential(credentialId, nameVal, usernameVal, contextVal, authPasswordVal, encPasswordVal, authenticationTypeVal, privacyTypeVal, authPassAsKeyVal, encPassAsKeyVal) {
        ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "InsertUpdateSNMPCredential",
        {
                         credentialId: credentialId,
                         name: nameVal,
                         username: usernameVal,
                         context: contextVal,
                         authPassword: authPasswordVal,
                         encPassword: encPasswordVal,
                         authenticationType: authenticationTypeVal,
                         privacyType: privacyTypeVal,
                         authPassAsKey: authPassAsKeyVal,
                         encPassAsKey: encPassAsKeyVal
                     },
                     function (result) {

                         // credential name exists in db    
                         if (result == false) {
                             $("#errorDiv").text('@{R=Core.Strings;K=WEBJS_VB0_119; E=js}');
                             return;
                         }

                         dialog.hide();
                         SW.Core.SNMPCredentialManager.reload();
                     });
    }

    function checkCredentials(nameVal, usernameVal) {
        if ($.trim(String(nameVal)) == '') {
            return "@{R=Core.Strings;K=WEBJS_VB0_115; E=js}";
        }

        if ($.trim(String(usernameVal)) == '') {
            return "@{R=Core.Strings;K=WEBJS_VB0_116; E=js}";
        }

        return '';
    };

    function addEditNewCredential(credentialId) {
        editedCredentialId = credentialId;
        if (!dialog) {
            dialog = new Ext.Window({
                applyTo: 'SNMPCredManagerDialogFrame',
                id: 'dialog',
                layout: 'fit',
                width: 480,
                height: 420,
                modal: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'SNMPCredManagerDialogBody'
                }),
                buttons: [
                {
                    text: '@{R=Core.Strings;K=CommonButtonType_Save; E=js}',
                    cls: 'sw-btn-primary',
                    handler: function () {
                        brokenExists = false;
                        var nameVal = $("#credName").val();
                        var usernameVal = $("#credUsername").val();
                        var contextVal = $("#credContext").val();
                        var authPasswordVal = $("#credAuthPassword").val();
                        var encPasswordVal = $("#credEncPassword").val();
                        var authenticationTypeVal = $("#authTypeSelect").val();
                        var privacyTypeVal = $("#encTypeSelect").val();
                        var authPassAsKeyVal = $("#authPassAsKey").is(":checked");
                        var encPassAsKeyVal = $("#encPassAsKey").is(":checked");

                        var error = checkCredentials(nameVal, usernameVal);

                        if (error != '') {
                            $("#errorDiv").text(error);
                            return;
                        } else {
                            $("#errorDiv").text('');
                        }

                        // detect if there are any nodes / profiles using edited credentials
                        if (editedCredentialId != null) {
                            ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "GetNumberOfNodesUsingCredentials", { credentialId: editedCredentialId, types: ["ROSNMPCredentialID", "RWSNMPCredentialID"] },
                                function(result) {
                                    if (result && result.length == 2 && (result[0] > 0 || result[1] > 0)) {

                                        var listPart = '';
                                        if (result[0] > 0)
                                            listPart += String.format("@{R=Core.Strings.2;K=WEBJS_SO0_11; E=js}", result[0]);

                                        if (result[1] > 0) {
                                            if (listPart.length > 0) listPart += ", ";
                                            listPart += String.format("@{R=Core.Strings.2;K=WEBJS_SO0_12; E=js}", result[1]);
                                            }

                                        if (!confirm(String.format("@{R=Core.Strings.2;K=WEBJS_SO0_10; E=js}", listPart))) {
                                            dialog.hide();
                                            return;
                                        }
                                    }

                                    addEditCredential(editedCredentialId, nameVal, usernameVal, contextVal, authPasswordVal, encPasswordVal, authenticationTypeVal, privacyTypeVal, authPassAsKeyVal, encPassAsKeyVal);
                                });
                        }
                        else
                            addEditCredential(editedCredentialId, nameVal, usernameVal, contextVal, authPasswordVal, encPasswordVal, authenticationTypeVal, privacyTypeVal, authPassAsKeyVal, encPassAsKeyVal);
                        }
                },
                {
                    text: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        dialog.hide();
                    }
                }]
            });
        }

        $("#errorDiv").text('');

        if (credentialId != null) {
            ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "GetSNMPCredential", { credentialId: credentialId },
            function (result) {
                dialog.show();

                var credNameInput = $("#credName");

                credNameInput.val(result.Name);
                credNameInput.attr('disabled', 'disabled');

                $("#credUsername").val(result.UserName);
                $("#credContext").val(result.Context);
                $("#credAuthPassword").val(result.AuthenticationPassword);
                $("#credEncPassword").val(result.PrivacyPassword);

                if (!result.AuthenticationKeyIsPassword)
                    $("#authPassAsKey").prop('checked', 'checked');
                else
                    $("#authPassAsKey").removeAttr('checked');

                if (!result.PrivacyKeyIsPassword)
                    $("#encPassAsKey").prop('checked', 'checked');
                else
                    $("#encPassAsKey").removeAttr('checked');

                switch (result.AuthenticationType) {
                    case 0:
                        $("#authTypeSelect").val('None');
                        break;
                    case 1:
                        $("#authTypeSelect").val('MD5');
                        break;
                    case 2:
                        $("#authTypeSelect").val('SHA1');
                        break;
                    default:
                        $("#authTypeSelect").val('None');
                } 

                switch (result.PrivacyType) {
                    case 0:
                        $("#encTypeSelect").val('None');
                        break;
                    case 1:
                        $("#encTypeSelect").val('DES56');
                        break;
                    case 2:
                        $("#encTypeSelect").val('AES128');
                        break;
                    case 3:
                        $("#encTypeSelect").val('AES192');
                        break;
                    case 4:
                        $("#encTypeSelect").val('AES256');
                        break;
                    default:
                        $("#encTypeSelect").val('None');
                }
            });
        }
        else {
            dialog.show();

            var credNameInput = $("#credName");

            credNameInput.val('');
            credNameInput.removeAttr('disabled');

            $("#credUsername").val('');
            $("#credContext").val('');
            $("#credAuthPassword").val('');
            $("#credEncPassword").val('');
            $("#authPassAsKey").removeAttr('checked');
            $("#encPassAsKey").removeAttr('checked');
            $("#encTypeSelect").val('None');
            $("#authTypeSelect").val('None');
        }
        return;
    };

    function editSelecetedCredential() {
        var selItems = grid.getSelectionModel();

        selItems.each(function (rec) {
            addEditNewCredential(rec.data["CredentialID"]);
            });
    }

    function deleteSelecetedCredential() {
        if (!confirm("@{R=Core.Strings;K=WEBJS_VB0_100; E=js}"))
            return;

        var ids = [];

        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["CredentialID"]);
        });

        // detect if there are any profiles using edited credentials
        ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "GetNumberOfDiscoveryProfilesUsingCredentialsList",
            {
                credentialIds: ids
            },
            function(result) {
                if (result > 0) {
                    if (!confirm(String.format("@{R=Core.Strings.2;K=WEBJS_SO0_13; E=js}", result))) {
                        dialog.hide();
                        return;
                    }
                }

                ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "DeleteSNMPCredentials", { credentialIDs: ids }, function (result) {
                    grid.getStore().reload();
                    });
        });
    }

    return {
        reload: function () { grid.getStore().reload(); },
        init: function () {

            // load default page size from Orion settings
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '20'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);						
                        this.doRefresh();
                    }, this);
                }
            });

            // set selection model to checkbox
            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/Services/CredentialManagerService.asmx/GetSNMPCredentials",
                [
                { name: 'CredentialID', mapping: 0 },
                { name: 'CredentialName', mapping: 1 },
                { name: 'Username', mapping: 2 },
                { name: 'NodesAssigned', mapping: 3 },
                { name: 'IsBroken', mapping: 4 },
                ],
                "CredentialName");

            // define grid panel
            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
                    selectorModel,
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'CredentialID' },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_101; E=js}', width: 200, sortable: true, dataIndex: 'CredentialName', renderer: renderUserName },
                    { header: '@{R=Core.Strings;K=WEBJS_VB0_102; E=js}', width: 200, sortable: true, dataIndex: 'Username', renderer: renderUserName },
			        { header: '@{R=Core.Strings;K=WEBJS_VB0_103; E=js}', width: 200, sortable: true, dataIndex: 'NodesAssigned', renderer: renderNodeCount }
                ],
                sm: selectorModel,
                viewConfig: {
                    forceFit: true,
                    getRowClass: function (record) {
                        if (record.get('IsBroken') === true) {
                            brokenExists = true;
                            return 'broken-row';
                        }
                    }
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 830,
                height: 300,
                stripeRows: true,
                trackMouseOver: false,

                tbar: [
                {
                    id: 'AddButton',
                    text: '@{R=Core.Strings.2;K=WEBJS_SO0_9; E=js}',
                    iconCls: 'add',
                    handler: function () { addEditNewCredential(null); }
                }, '-',
                {
                    id: 'EditButton',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_105; E=js}',
                    iconCls: 'edit',
                    handler: function () { editSelecetedCredential(); }
                }, '-',
                {
                    id: 'DeleteButton',
                    text: '@{R=Core.Strings;K=WEBJS_VB0_106; E=js}',
                    iconCls: 'del',
                    handler: function () { deleteSelecetedCredential(); }
                }],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_VB0_107; E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VB0_108; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');

                if (brokenExists === true) {
                    $('#suggestionFail')
                        .html('<div class="sw-suggestion sw-suggestion-fail" style="margin-bottom: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings.2;K=WEBJS_SO0_14;E=js}</div>');
                    $('#suggestionFail').show();
                } else {
                    $('#suggestionFail').hide();
                }
            });

            grid.render('Grid');
            UpdateToolbarButtons();

            grid.bottomToolbar.addPageSizer();
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = {};
            grid.store.load({ params: { start: 0, limit: pageSizeNum } });
        }
    };
} ();

Ext.onReady(SW.Core.SNMPCredentialManager.init, SW.Core.SNMPCredentialManager);