﻿SW.Core.CredentialManager.UsernamePasswordCredential = function () {
    var dialog;
    var editedCredentialId;
    var oldPassword;
    var oldUserName;

    
    function checkCredentials(description, username, password, confirm) {
        if ($.trim(String(description)) == '') {
            return "@{R=Core.Strings;K=WEBJS_VB0_115; E=js}";
        }

        if ($.trim(String(username)) == '') {
            return "@{R=Core.Strings;K=WEBJS_VB0_116; E=js}";
        }

        if (!(password === confirm) || $.trim(String(password)) == '') {
            return "@{R=Core.Strings;K=WEBJS_VB0_117; E=js}";
        }

        return "";
    };

    return {
        Show: function (credentialId) {
            editedCredentialId = credentialId;

            if (!dialog) {
                dialog = new Ext.Window({
                    applyTo: 'CredManagerDialogFrame',
                    id: 'dialog',
                    layout: 'fit',
                    width: 360,
                    height: 'auto',
                    modal: true,
                    closeAction: 'hide',
                    plain: true,
                    resizable: false,
                    items:new Ext.BoxComponent({
                        applyTo: 'CredManagerDialogBody'
                    }),
                    buttons: [
                    {
                        text: '@{R=Core.Strings;K=CommonButtonType_Save; E=js}',
                        cls: 'sw-btn-primary',
                        handler: function () {
                            var description = $("#credDescription").val();
                            var username = $("#credUsername").val();
                            var password = $("#credPassword").val();
                            var confirmPassword = $("#credConfirm").val();

                            if (password == confirmPassword && password == oldPassword && username == oldUserName) {
                                // user pressed ok but changed nothing
                                dialog.hide();
                                return;
                            }

                            var error = checkCredentials(description, username, password, confirmPassword, true);

                            if (error != '') {
                                $("#errorDiv").text(error);
                                return;
                            }
                            else {
                                $("#errorDiv").text('');
                            }

                            // detect if there are any profiles using edited credentials
                            ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "GetNumberOfEntitiesUsingCredentials",
                            {
                                credentialId: editedCredentialId, types: ["WMICredential"]
                            },
                            function (result) {

                                if (result && result.length == 3 && (result[0] > 0 || result[1] > 0 || result[2] > 0)) {

                                    var listPart = '';
                                    if (result[0] > 0)
                                        listPart += String.format("@{R=Core.Strings.2;K=WEBJS_SO0_11; E=js}", result[0]);

                                    if ((result[0] > 0) && (result[1] > 0))
                                        listPart += ", ";

                                    if (result[1] > 0) 
                                        listPart += String.format("@{R=Core.Strings.2;K=WEBJS_SO0_55; E=js}", result[1]);

                                    if (result[2] > 0) {
                                        if (listPart.length > 0) listPart += ", ";
                                        listPart += String.format("@{R=Core.Strings.2;K=WEBJS_SO0_12; E=js}", result[2]);
                                    }

                                    if (!confirm(String.format("@{R=Core.Strings.2;K=WEBJS_SO0_10; E=js}", listPart))) {
                                        dialog.hide();
                                        return;
                                    }
                                }

                                ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "InsertUpdateWindowsCredential",
                                {
                                    credentialId: editedCredentialId,
                                    name: description,
                                    username: username,
                                    password: password
                                },
                                function (result) {

                                    // credential name exists in db    
                                    if (result == false) {
                                        $("#errorDiv").text('@{R=Core.Strings;K=WEBJS_VB0_119; E=js}');
                                        return;
                                    }

                                    dialog.hide();
                                    SW.Core.CredentialManager.reload();
                                });
                            });
                        }
                    },
                    {
                        text: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            dialog.hide();
                        }
                    }]
                });
            }

            $("#errorDiv").text('');

            if (credentialId != null) {
                ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "GetWindowsCredential", { credentialId: credentialId },
                function (result) {
                    dialog.show();

                    var descriptionInput = $("#credDescription");

                    descriptionInput.val(result.Name);
                    descriptionInput.attr('disabled', 'disabled');

                    $("#credUsername").val(result.Username);
                    $("#credPassword").val(result.Password);
                    $("#credConfirm").val(result.Password);

                    // store old password even if it is a fake one (buch of stars)
                    oldPassword = result.Password;
                    oldUserName = result.Username;
                });
            }
            else {
                dialog.show();

                var descriptionInput = $("#credDescription");

                descriptionInput.val('');
                descriptionInput.removeAttr('disabled');

                $("#credUsername").val('');
                $("#credPassword").val('');
                $("#credConfirm").val('');
            }
        }
    };
} ();