using SolarWinds.Orion.AccountManagement.Models;
using SolarWinds.Orion.AccountManagement.Saml;
using SolarWinds.Orion.AccountManagement.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Saml;
using SolarWinds.Orion.Web.Services;
using System;
using Resx = Resources.AccountManagementWebContent;

namespace SolarWinds.Orion.AccountManagement.LegacyWebSite
{
    public partial class Orion_SamlLogin : SamlPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageInit();

            try
            {
                // check if is SAML authentication enabled
                if (!samlManager.IsSamlEnabled())
                    HandleLoginException(Resx.SamlDisabledTitle, Resx.SamlDisabledMessage);

                if (relayState.IsTestMode)
                {
                    samlTestResult.SamlLoginLoaded = DateTime.Now;
                    SaveTestResult();
                }

                // configure SAML library
                samlManager.ConfigureSaml(Request);

                // extract SAML response
                var response = samlManager.ReceiveSSO(Request);

                // Let's validate the key.
                if (relayState.IsTestMode
                && samlManager.VerifyTestKey(relayState.TestKey))
                {
                    WriteObserverDataToResult();
                    WriteSamlResponseToResult(response);
                    SaveTestResult();
                }

                // check for empty NameID
                if (string.IsNullOrEmpty(response.UserName))
                    HandleLoginException(Resx.SamlUnknownUserErrorTitle, Resx.SamlUnknownUserErrorMessage);

                // authorize
                var groups = samlManager.ExtractGroups(response);
                var result = authManager.Authorize(response.UserName, groups, Request.UserHostAddress);

                if (relayState.IsTestMode)
                {
                    WriteAuthorizationResult(result, response.UserName);
                    SaveTestResult();

                    // redirect user to saml test result page
                    log.Debug("Redirecting user to test result page");
                    Response.Redirect(SamlWebHelper.GetTestResultUrl(relayState), false);
                }
                else
                {
                    if (result.Status == AuthorizationStatus.Authorized)
                    {
                        // give user authorization and CSRF cookies
                        foreach (var cookie in result.AuthCookies)
                            Response.Cookies.Add(cookie);

                        ResourcePickerWebServices.PreBuildCache(result.XsrfToken);

                        // redirect authorized user to requested location in return URL
                        log.Debug("Redirectiong user to default page");
                        RedirectUser(response.UserName, relayState.ReturnUrl);
                    }
                    else
                    {
                        HandleAuthorizationError(result, relayState.ReturnUrl);
                    }
                }

                if (samlTestResult != null)
                {
                    samlTestResult.SamlLoginFinished = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);

                if (relayState.IsTestMode)
                {
                    WriteObserverDataToResult();
                    WriteExceptionToTestResult(ex);
                    Response.Redirect(SamlWebHelper.GetTestResultUrl(relayState), true);
                }
                else
                {
                    HandleLoginException(Resx.SamlAuthErrorTitle, Resx.SamlAuthErrorMessage);
                }
            }
        }

        protected void RedirectUser(string userName, string returnUrl)
        {
            returnUrl = returnUrl ?? SamlConstants.DefaultPage;

            if (OrionMembershipProvider.IsNewApiUser(userName))
            {
                string passThroughUrl = ResolveUrl(SamlConstants.LoginApiPage);
                log.DebugFormat(
                    "API-added account needs additional processing on first login. Redirecting to pass-through url '{0}', then continuing to '{1}'.",
                    passThroughUrl, returnUrl);

                returnUrl = UrlHelper.UrlAppendUpdateParameter(passThroughUrl, "ReturnUrl", returnUrl);
            }

            Response.Redirect(returnUrl, false);
        }

        private void HandleAuthorizationError(AuthorizationResult result, string returnUrl)
        {
            switch (result.Status)
            {
                case AuthorizationStatus.NotMatched:
                case AuthorizationStatus.LockedOut:
                case AuthorizationStatus.Expired:
                    AddLoginErrorToSession(Resx.SamlUnknownUserErrorTitle, Resx.SamlUnknownUserErrorMessage);
                    break;
                case AuthorizationStatus.Error:
                default:
                    AddLoginErrorToSession(Resx.SamlAuthErrorTitle, Resx.SamlAuthErrorMessage);
                    break;
            }

            var urlWithReturn = UrlHelper.UrlAppendUpdateParameter(SamlConstants.LoginPage, "ReturnUrl", returnUrl ?? SamlConstants.DefaultPage);

            log.DebugFormat("Authorization failed. Redirecting to '{0}'", urlWithReturn);
            Response.Redirect(urlWithReturn, false);
        }

        private void WriteAuthorizationResult(AuthorizationResult result, string userName)
        {
            switch (result.Status)
            {
                case AuthorizationStatus.Authorized:
                    samlTestResult.TestResult = SamlTestResultType.Success;
                    break;

                case AuthorizationStatus.NotMatched:
                    samlTestResult.TestResult = SamlTestResultType.Warning;
                    samlTestResult.Title = Resx.SamlTestResultWarning;
                    samlTestResult.Message = String.Format(Resx.SamlNotMatched, userName);
                    break;

                case AuthorizationStatus.Expired:
                    samlTestResult.TestResult = SamlTestResultType.Warning;
                    samlTestResult.Title = Resx.SamlTestResultWarning;
                    samlTestResult.Message = String.Format(Resx.SamlExpired, userName);
                    break;

                case AuthorizationStatus.LockedOut:
                    samlTestResult.TestResult = SamlTestResultType.Warning;
                    samlTestResult.Title = Resx.SamlTestResultWarning;
                    samlTestResult.Message = String.Format(Resx.SamlLockedOut, userName);
                    break;

                case AuthorizationStatus.Error:
                    samlTestResult.TestResult = SamlTestResultType.Error;
                    break;
            }

            // If the claim is missing we will add a warning
            if (samlTestResult.IsGroupClaimMissing)
            {
                if (samlTestResult.TestResult == SamlTestResultType.Success)
                {
                    samlTestResult.TestResult = SamlTestResultType.Warning;
                    samlTestResult.Title = Resx.SamlTestResultSuccess;
                }

                samlTestResult.Message += String.Format(Resx.SamlTestResultClaimMessage, SamlConstants.SamlGroupsClaimName);
                samlTestResult.Message = samlTestResult.Message.Trim();
            }

            SaveTestResult();
        }
    }
}