﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PCU.Web.Helpers;
using SolarWinds.PCU.Web.Models;

public partial class Orion_PowerControlUnit_Admin_EditViews_PCUTemperatureUnit : ProfilePropEditUserControl
{
    public override String PropertyValue { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int temperatureUnit = (int)PCUTemperatureUnit.Fahrenheit;
            if (!String.IsNullOrEmpty(PropertyValue))
            {
                TryConvertStringToTemperatureUnit(PropertyValue, out temperatureUnit);
            }
            ctrTemperatureUnit.ClearSelection();
            PCUHtmlHelper.SetListSelectedValue(ctrTemperatureUnit, temperatureUnit.ToString(CultureInfo.InvariantCulture), ((int)PCUTemperatureUnit.Fahrenheit).ToString(CultureInfo.InvariantCulture));
        }
        PropertyValue = ctrTemperatureUnit.SelectedValue;
    }
    protected static bool TryConvertStringToTemperatureUnit(string value, out int unit)
    {
        unit = (int)PCUTemperatureUnit.Fahrenheit;
        try
        {
            unit = (int)Enum.Parse(typeof(PCUTemperatureUnit), value);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
}