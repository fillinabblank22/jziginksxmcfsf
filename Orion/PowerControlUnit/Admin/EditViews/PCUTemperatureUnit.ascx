﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PCUTemperatureUnit.ascx.cs" Inherits="Orion_PowerControlUnit_Admin_EditViews_PCUTemperatureUnit" %>

<asp:ListBox ID="ctrTemperatureUnit" SelectionMode="Single" Rows="1" runat="server">
	<asp:ListItem Text="<%$ Resources: PCUWebContent, PCUFahrenheit %>" Selected="True" Value="0"/>
	<asp:ListItem Text="<%$ Resources: PCUWebContent, PCUCelsius %>" Value="1"/>
	<asp:ListItem Text="<%$ Resources: PCUWebContent, PCUKelvin %>" Value="2"/>
</asp:ListBox>
