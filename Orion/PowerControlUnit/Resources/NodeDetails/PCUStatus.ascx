﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PCUStatus.ascx.cs" Inherits="Orion_PowerControlUnit_Resources_PCUStatus" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <% if (NoData) 
                {
        %>
            <div><%= Resources.CoreWebContent.WEBDATA_VB0_443 %></div>
        <%      }
        else    {
        %>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUBasicBatteryStatus %></td>
		        <td class="Property"><img class="StatusIcon" src=<%= GetUPSStatusIcon() %>></td>
                <td class="Property"><%= GetUPSStatusString() %></td>
            </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUOutputStatus %></td>
		        <td class="Property"><img class="StatusIcon" src=<%= GetBatteryStatusIcon() %>></td>
                <td class="Property"><%= GetOutputStatusString() %></td>
            </tr>
        <% if (PCUInfoModel.Name != "<null>") 
        {
        %>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUName %></td>
                <td class="Property">&nbsp;</td>
                <td class="Property"><%= PCUInfoModel.Name %></td>
            </tr>
        <% } %>
            <tr>
			    <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUSerialNumber %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= PCUInfoModel.SerialNumber %></td>
			</tr>
			<tr>
			    <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUModel %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= PCUInfoModel.Model %></td>
			</tr>
			<tr>
			    <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCULastFailCause %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= GetLastFailCause() %></td>
			</tr>
        <% if (PCUInfoModel.BatteryPackCount != UnknownBatteryPackCount) 
        {
        %>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUBatteryPackCount %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= PCUInfoModel.BatteryPackCount %></td>
            </tr>
        <% } %>
			<tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.UPSOutputLoad %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= PCUInfoModel.OutputPercentLoad %><span> %</span></td>
            </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUBatteryCapacity %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= PCUInfoModel.BatteryCapacity %><span> %</span></td>
            </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUBatteryTemperature %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= GetBatteryTemperatureConverted() %><span> </span><%= TemperatureUnit %></td>
            </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUTimeOnBattery %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= PrintTimeSpan(PCUInfoModel.TimeOnBattery) %></td>
            </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCURunTimeRemaining %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= PrintTimeSpan(PCUInfoModel.RunTimeRemaining) %></td>
            </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle"><%= Resources.PCUWebContent.PCUReplaceInicator %></td>
		        <td class="Property">&nbsp;</td>
                <td class="Property"><%= GetReplacementIndicatorString() %></td>
            </tr>
        </table>
        <% } %>
    </Content>
</orion:ResourceWrapper>