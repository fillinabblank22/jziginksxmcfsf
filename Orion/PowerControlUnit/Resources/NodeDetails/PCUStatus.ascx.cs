﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using SolarWinds.Data;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.PCU.Web;
using SolarWinds.PCU.Web.Helpers;
using SolarWinds.PCU.Web.Models;
using SolarWinds.PCU.Web.UI;
using SolarWinds.PCU.Contract.DataModels;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_PowerControlUnit_Resources_PCUStatus : PCUResourceControl
{
    public PCUInfoModel PCUInfoModel = new PCUInfoModel();
    public bool NoData = false;
	protected const int UnknownBatteryPackCount = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider?.Node == null || nodeProvider.Node.NodeID == 0)
        {
			ResourceWrapper.Visible = false;
            return;
        }

        var NodeID = nodeProvider.Node.NodeID;

        PCUInfoModel = PCUResourceHelper.GetPCUStatistics(NodeID);

		if ((AutoHide) && (PCUInfoModel == null))
        {
            ResourceWrapper.Visible = false;
			return;
        }

        if ((!AutoHide) &&  (PCUInfoModel == null)) 
        {
            NoData = true;
            return;
        }
        
        if (PCUInfoModel.OutputUPSStatus < 0)
        {
            ResourceWrapper.Visible = false;
            PCUInfoModel = null;
			return;
        }
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override string HelpLinkFragment
    {
        get { return "orioncorepowercontrolunit"; }
    }
    
    public override string EditControlLocation
    {
        get
        {
            return "/Orion/PowerControlUnit/Controls/EditResourceControls/EditPCUResource.ascx";
        }
    }

    protected string TemperatureUnit
    {
        get
        {
            switch (PCURoleAccessor.TemperatureUnit)
            {
                case 0:
                    return Resources.PCUWebContent.PCUFahrenheitUnit;
                case 1:
                    return Resources.PCUWebContent.PCUCelsiusUnit;
                case 2:
                    return Resources.PCUWebContent.PCUKelvinUnit;
                default:
                    return Resources.PCUWebContent.PCUFahrenheitUnit;
            }
        }
    }

    protected float GetBatteryTemperatureConverted()
    {
        var unit = PCURoleAccessor.TemperatureUnit;
        PCUTemperatureUnit resultUnit = PCUTemperatureUnit.Fahrenheit;
        switch (unit)
        {
            case 0:
                resultUnit = PCUTemperatureUnit.Fahrenheit;
                break;
            case 1:
                resultUnit = PCUTemperatureUnit.Celsius;
                break;
            case 2:
                resultUnit = PCUTemperatureUnit.Kelvin;
                break;
        }

        return TemperatureConverter.Convert(PCUInfoModel.BatteryTemperature, PCUTemperatureUnit.Celsius, resultUnit);
    }

    protected string imagesPath
    {
        get { return "/Orion/PowerControlUnit/Images/"; }
    }

    protected string GetUPSStatusIcon()
    {
        switch (PCUInfoModel.OutputUPSStatus)
        {
            case BasicOutputStatus.Unknown:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-Unknown.png");
            case BasicOutputStatus.Online:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-Online.png");
            case BasicOutputStatus.OnBattery:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-OnBattery.png");
            case BasicOutputStatus.OnSmartBoost:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-OnSmartBoost.png");
            case BasicOutputStatus.TimedSleeping:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-TimedSleeping.png");
            case BasicOutputStatus.SoftwareBypass:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-SoftwareBypass.png");
            case BasicOutputStatus.Off:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-Off.png");
            case BasicOutputStatus.Rebooting:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-Rebooting.png");
            case BasicOutputStatus.SwitchedBypass:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-SwitchedBypass.png");
            case BasicOutputStatus.HardwareFailureBypass:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-HardwareFailureBypass.png");
            case BasicOutputStatus.SleepingUntilPowerReturn:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-SleepingUntilPowerReturn.png");
            case BasicOutputStatus.OnSmartTrim:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-OnSmartTrim.png");
            case BasicOutputStatus.EcoMode:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-EcoMode.png");
            case BasicOutputStatus.HotStandBy:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-HotStandBy.png");
            case BasicOutputStatus.OnBatteryTest:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-OnBatteryTest.png");
            default:
                return string.Format("{0}{1}", imagesPath, "UPSOutputStatus-Unknown.png"); ;
        }
    }
    protected string GetBatteryStatusIcon()
    {
        switch (PCUInfoModel.OutputBatteryStatus)
        {
            case BasicBatteryStatus.Unknown:
                return string.Format("{0}{1}", imagesPath, "UPSBatteryStatus-Unknown.png");
            case BasicBatteryStatus.BatteryNormal:
                return string.Format("{0}{1}", imagesPath, "UPSBatteryStatus-BatteryNormal.png");
            case BasicBatteryStatus.BatteryLow:
                return string.Format("{0}{1}", imagesPath, "UPSBatteryStatus-BatteryLow.png");
            case BasicBatteryStatus.BatteryInFaultCondition:
                return string.Format("{0}{1}", imagesPath, "UPSBatteryStatus-BatteryInFaultCondition.png");
            default:
                return string.Format("{0}{1}", imagesPath, "UPSBatteryStatus-Unknown.png"); ;

        }
    }

    protected string GetUPSStatusString()
    {
        switch (PCUInfoModel.OutputUPSStatus)
        {
            case BasicOutputStatus.Unknown:
                return Resources.PCUWebContent.PCUStatusUnknown;
            case BasicOutputStatus.Online:
                return Resources.PCUWebContent.PCUStatusOnLine;
            case BasicOutputStatus.OnBattery:
                return Resources.PCUWebContent.PCUStatusOnBattery;
            case BasicOutputStatus.OnSmartBoost:
                return Resources.PCUWebContent.PCUStatusOnSmartBoost;
            case BasicOutputStatus.TimedSleeping:
                return Resources.PCUWebContent.PCUStatusTimedSleeping;
            case BasicOutputStatus.SoftwareBypass:
                return Resources.PCUWebContent.PCUStatusSoftwareBypass;
            case BasicOutputStatus.Off:
                return Resources.PCUWebContent.PCUStatusOff;
            case BasicOutputStatus.Rebooting:
                return Resources.PCUWebContent.PCUStatusRebooting;
            case BasicOutputStatus.SwitchedBypass:
                return Resources.PCUWebContent.PCUStatusSwitchedBypass;
            case BasicOutputStatus.HardwareFailureBypass:
                return Resources.PCUWebContent.PCUStatusHardwareFailureBypass;
            case BasicOutputStatus.SleepingUntilPowerReturn:
                return Resources.PCUWebContent.PCUStatusSleepingUntilPowerReturn;
            case BasicOutputStatus.OnSmartTrim:
                return Resources.PCUWebContent.PCUStatusOnSmartTrim;
            case BasicOutputStatus.EcoMode:
                return Resources.PCUWebContent.PCUStatusEcoMode;
            case BasicOutputStatus.HotStandBy:
                return Resources.PCUWebContent.PCUStatusHotStandby;
            case BasicOutputStatus.OnBatteryTest:
                return Resources.PCUWebContent.PCUStatusOnBatteryTest;

            default:
                return String.Empty;
        }
    }

    protected string GetOutputStatusString()
    {
        switch (PCUInfoModel.OutputBatteryStatus)
        {
            case BasicBatteryStatus.Unknown:
                return Resources.PCUWebContent.BatteryStatusUnknown;
            case BasicBatteryStatus.BatteryNormal:
                return Resources.PCUWebContent.BatteryStatusNormal;
            case BasicBatteryStatus.BatteryLow:
                return Resources.PCUWebContent.BatteryStatusLow;
            case BasicBatteryStatus.BatteryInFaultCondition:
                return Resources.PCUWebContent.BatteryStatusInFaultCondition;

            default:
                return String.Empty;
        }
    }

    protected string GetReplacementIndicatorString()
    {
        switch (PCUInfoModel.ReplaceIndicator)
        {
            case BatteryReplaceIndicator.NoBatteryNeedsReplacing:
                return Resources.PCUWebContent.BatteryDoesntNeedReplacement;
            case BatteryReplaceIndicator.BatteryNeedsReplacing:
                return string.Format("<span style =\"color: red\"><b>{0}</b></span>", Resources.PCUWebContent.BatteryNeedsReplacement);

            default:
                return string.Empty;
        }
    }

    protected string GetLastFailCause()
    {
        switch (PCUInfoModel.LastFailCause)
        {
            case LastFailCause.NoTransfer:
                return Resources.PCUWebContent.FailCauseNoTransfer;
            case LastFailCause.HighLineVoltage:
                return Resources.PCUWebContent.FailCauseHighLineVoltage;
            case LastFailCause.Brownout:
                return Resources.PCUWebContent.FailCauseBrownout;
            case LastFailCause.Blackout:
                return Resources.PCUWebContent.FailCauseBlackout;
            case LastFailCause.SmallMomentarySag:
                return Resources.PCUWebContent.FailCauseSmallMomentarySag;
            case LastFailCause.DeepMomentarySag:
                return Resources.PCUWebContent.FailCauseDeepMomentarySag;
            case LastFailCause.SmallMomentarySpike:
                return Resources.PCUWebContent.FailCauseSmallMomentarySpike;
            case LastFailCause.LargeMomentarySpike:
                return Resources.PCUWebContent.FailCauseLargeMomentarySpike;
            case LastFailCause.SelfTest:
                return Resources.PCUWebContent.FailCauseSelfTest;
            case LastFailCause.RateOfVoltageChange:
                return Resources.PCUWebContent.FailCauseRateOfVoltageChange;
            default: return string.Empty;
        }
    }

    protected bool AutoHide
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
				return false;
			return Resource.Properties["AutoHide"].Equals("1");
		}
	}
	
	protected string PrintTimeSpan(TimeSpan t)
	{
	   string result;
		if (t.TotalSeconds == 0)
		{
			result = "0";
		} 
		else if (t.TotalMinutes < 1.0)
		{
			result = String.Format("{0}s", t.Seconds);
		}
		else if (t.TotalHours < 1.0)
		{
			result = String.Format("{0}m:{1:D2}s", t.Minutes, t.Seconds);
		}
		else // more than 1 hour
		{
			result = String.Format("{0}h:{1:D2}m:{2:D2}s", (int)t.TotalHours, t.Minutes, t.Seconds);
		}

	   return result;
	}


}
