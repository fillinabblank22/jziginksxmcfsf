﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.PCU.Web;
using SolarWinds.PCU.Web.Models;

public partial class Orion_PowerControlUnit_Controls_SelectPCUTemperature : System.Web.UI.UserControl
{
    private PCUTemperatureUnit temperatureUnit;

    private PCUTemperatureUnit GetTemperatureUnitFromInt(int selectedValue)
    {
        switch (selectedValue)
        {
            case 0: return PCUTemperatureUnit.Fahrenheit;
            case 1: return PCUTemperatureUnit.Celsius;
            case 2: return PCUTemperatureUnit.Kelvin;

            default:    return PCUTemperatureUnit.Fahrenheit;
        }
    }
    //default F
    public PCUTemperatureUnit SelectedTemperatureUnit
    {
        get
        {
            return GetTemperatureUnitFromInt(Convert.ToInt32(RadioPCUTemperatureUnit.SelectedValue));
        }
        set
        {
            PCUTemperatureUnit temperature = value;

            if (temperature != PCUTemperatureUnit.Celsius && temperature != PCUTemperatureUnit.Fahrenheit && temperature != PCUTemperatureUnit.Kelvin)
                temperature = PCUTemperatureUnit.Fahrenheit;

            switch (temperature)
            {
                case PCUTemperatureUnit.Fahrenheit:
                    RadioPCUTemperatureUnit.Items[0].Selected = true;
                    break;
                case PCUTemperatureUnit.Celsius:
                    RadioPCUTemperatureUnit.Items[1].Selected = true;
                    break;
                case PCUTemperatureUnit.Kelvin:
                    RadioPCUTemperatureUnit.Items[2].Selected = true;
                    break;
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        RadioPCUTemperatureUnit.Items.Add(new ListItem(Resources.PCUWebContent.PCUFahrenheit, ((int)PCUTemperatureUnit.Fahrenheit).ToString(CultureInfo.InvariantCulture)));
        RadioPCUTemperatureUnit.Items.Add(new ListItem(Resources.PCUWebContent.PCUCelsius, ((int)PCUTemperatureUnit.Celsius).ToString(CultureInfo.InvariantCulture)));
        RadioPCUTemperatureUnit.Items.Add(new ListItem(Resources.PCUWebContent.PCUKelvin, ((int)PCUTemperatureUnit.Kelvin).ToString(CultureInfo.InvariantCulture)));
    }
    
    public void SaveProperties(Dictionary<string, object> properties)
    {
        var profile = HttpContext.Current.Profile.GetProfileGroup("PowerControlUnit");
        profile.SetPropertyValue(PCURoleAccessor.KeyTemperatureUnit, (int)SelectedTemperatureUnit);
        Profile.Save();
    }
}