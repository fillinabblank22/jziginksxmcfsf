﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PCU.Web;
using SolarWinds.PCU.Web.Models;

public partial class Orion_PowerControlUnit_Controls_EditResourceControls_EditPCUResource : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add("AutoHide", this.autoHide.AutoHideValue);
            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

}