﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditPCUResource.ascx.cs" Inherits="Orion_PowerControlUnit_Controls_EditResourceControls_EditPCUResource" %>
<%@ Register Src="~/Orion/PowerControlUnit/Controls/SelectPCUTemperature.ascx" TagPrefix="pcu" TagName="SelectPCUTemperature" %>
<%@ Register TagPrefix="orion" TagName="AutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>

<orion:AutoHide runat="server" ID="autoHide" InLine="true" Description="<%$ Resources: CoreWebContent,WEBDATA_AK0_309 %>" />
