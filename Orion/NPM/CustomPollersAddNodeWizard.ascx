<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollersAddNodeWizard.ascx.cs" Inherits="Orion_NPM_CustomPollers" %>
<%@ Register Src="~/Orion/NPM/Controls/CustomPollers.ascx" TagName="CustomPollers" TagPrefix="orion"  %>

<div class="GroupBox">
    <span class="ActionName"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_1%>
    <asp:Literal runat="server" ID="litNodeIP"></asp:Literal></span>
    <br />
    <span><%=Resources.NPMWebContent.NPMWEBDATA_VB0_2%></span>                
    <br /> <br />          
    <orion:CustomPollers runat="server" ID="NodeCustomPollers" NetworkObjectType="Node" />
    <br /> 
</div>
