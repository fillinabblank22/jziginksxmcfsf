using System;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_InternalInterfaceList : BaseResourceControl
{
	protected void InterfaceTable_Init(object sender, EventArgs e)
	{
		InterfaceTable.DataBind();
	}

	public object DataSource
	{
		get { return InterfaceTable.DataSource; }
		set { InterfaceTable.DataSource = value; }
	}

    protected override string DefaultTitle
    {
        get { return "Interface List"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
