﻿using System;
using System.Linq;
using System.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using Interface = SolarWinds.Orion.NPM.Web.Interface;

public partial class Orion_NPM_InterfacePopup : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	protected void Page_Load(object sender, EventArgs e)
	{
		// if limitation exists interface won't be displayed
		// so, we don't need limitation here
		this.Interface = NetObjectFactory.Create(Request["NetObject"], true) as Interface;
        this.PercentUtilTxRow.Visible = this.Interface.OutPercentUtil.Value != -2;
		this.PercentUtilRxRow.Visible = this.Interface.InPercentUtil.Value != -2;
		this.BpsTxRow.Visible = this.Interface.OutBps.Value != -2;
		this.BpsRxRow.Visible = this.Interface.InBps.Value != -2;

		this.PercentUtilizationRx.Value = (short)this.Interface.InPercentUtil.Value;
		SetupBar(this.PercentUtilizationRxBar, this.Interface.InPercentUtil.Value, 100,
			Thresholds.IfPercentUtilizationWarning.SettingValue, Thresholds.IfPercentUtilizationWarning.SettingValue);
		this.PercentUtilizationTx.Value = (short)this.Interface.OutPercentUtil.Value;
		SetupBar(this.PercentUtilizationTxBar, this.Interface.OutPercentUtil.Value, 100,
			Thresholds.IfPercentUtilizationWarning.SettingValue, Thresholds.IfPercentUtilizationWarning.SettingValue);

		if (this.HasOverviewStyle(InterfaceOverviewStyle.ErrorsAndDiscardsThisHour))
		{
			float err = (float)Thresholds.ErrorsDiscardsError.SettingValue;  //use error level as max value for bars (?)
			float warn = (float)Thresholds.ErrorsDiscardsWarning.SettingValue;
			this.ErrorsRxLabel.Text = this.ErrorsTxLabel.Text = Resources.NPMWebContent.NPMWEBCODE_VB0_23;
			this.ErrorsRx.Value = (short)this.Interface.InErrorsThisHour;
			this.ErrorsTx.Value = (short)this.Interface.OutErrorsThisHour;
			this.SetupBar(this.ErrorsRxBar, this.Interface.InErrorsThisHour, err, warn, err);
			this.SetupBar(this.ErrorsTxBar, this.Interface.OutErrorsThisHour, err, warn, err);
			this.DiscardsRxLabel.Text = this.DiscardsTxLabel.Text = Resources.NPMWebContent.NPMWEBCODE_VB0_22;
			this.DiscardsRx.Value = (short)this.Interface.InDiscardsThisHour;
			this.DiscardsTx.Value = (short)this.Interface.OutDiscardsThisHour;
			this.SetupBar(this.DiscardsRxBar, this.Interface.InDiscardsThisHour, err, warn, err);
			this.SetupBar(this.DiscardsTxBar, this.Interface.OutDiscardsThisHour, err, warn, err);

			this.ErrorsTxRow.Visible = this.Interface.OutErrorsThisHour != -2;
			this.ErrorsRxRow.Visible = this.Interface.InErrorsThisHour != -2;
			this.DiscardsTxRow.Visible = this.Interface.OutDiscardsThisHour != -2;
			this.DiscardsRxRow.Visible = this.Interface.InDiscardsThisHour != -2;
		}
		else if (this.HasOverviewStyle(InterfaceOverviewStyle.ErrorsAndDiscardsToday))
		{
			float err = (float)Thresholds.ErrorsDiscardsError.SettingValue;  //use error level as max value for bars (?)
			float warn = (float)Thresholds.ErrorsDiscardsWarning.SettingValue;
			this.ErrorsRxLabel.Text = this.ErrorsTxLabel.Text = Resources.NPMWebContent.NPMWEBDATA_VB0_67;
			this.ErrorsRx.Value = (short)this.Interface.InErrorsToday;
			this.ErrorsTx.Value = (short)this.Interface.OutErrorsToday;
			this.SetupBar(this.ErrorsRxBar, this.Interface.InErrorsToday, err, warn, err);
			this.SetupBar(this.ErrorsTxBar, this.Interface.OutErrorsToday, err, warn, err);
			this.DiscardsRxLabel.Text = this.DiscardsTxLabel.Text = Resources.NPMWebContent.NPMWEBDATA_VB0_68;
			this.DiscardsRx.Value = (short)this.Interface.InDiscardsToday;
			this.DiscardsTx.Value = (short)this.Interface.OutDiscardsToday;
			this.SetupBar(this.DiscardsRxBar, this.Interface.InDiscardsToday, err, warn, err);
			this.SetupBar(this.DiscardsTxBar, this.Interface.OutDiscardsToday, err, warn, err);

			this.ErrorsTxRow.Visible = this.Interface.OutErrorsToday != -2;
			this.ErrorsRxRow.Visible = this.Interface.InErrorsToday != -2;
			this.DiscardsTxRow.Visible = this.Interface.OutDiscardsToday != -2;
			this.DiscardsRxRow.Visible = this.Interface.InDiscardsToday != -2;
		}

		this.TxRow.Visible = this.PercentUtilTxRow.Visible || this.BpsTxRow.Visible || this.ErrorsTxRow.Visible || this.DiscardsTxRow.Visible;
		this.RxRow.Visible = this.PercentUtilRxRow.Visible || this.BpsRxRow.Visible || this.ErrorsRxRow.Visible || this.DiscardsRxRow.Visible;

		this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
	}

	protected Interface Interface { get; set; }

	protected string SplitOnComma(string str)
	{
		return str.Replace(",", ".<br/>");
	}

	private void SetupBar(InlineBar bar, float value, float max, double warning, double error)
	{
		bar.Percentage = Math.Min(100.0 * value / max, 100);
		if (value >= error)
			bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Error;
		else if (value > warning)
			bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Warning;
		else
			bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Normal;
	}

	protected bool HasOverviewStyle(params InterfaceOverviewStyle[] styles)
	{								 
		int styleValue;
		return Int32.TryParse(this.Request.QueryString["InterfaceOverviewStyle"], out styleValue) && styles.Any(s => (int)s == styleValue);
	}

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
}