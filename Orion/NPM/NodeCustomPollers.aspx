<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true" CodeFile="NodeCustomPollers.aspx.cs" Inherits="Orion_Nodes_NodeCustomPollers" Title="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_74%>" %>
<%@ Register Src="~/Orion/NPM/Controls/CustomPollers.ascx" TagPrefix="orion" TagName="CustomPollers" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="../NPM/styles/NodeCustomPollers.css" />
</asp:Content>
<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <span class="createDevicePoller"><a href="../DeviceStudio/Admin/CreatePollerWizard/Default.aspx"> <%= Resources.NPMWebContent.NPMWEBCODE_ZB0_12 %></a></span>
    <orion:IconHelpButton HelpUrlFragment="OrionPHNodeManagementAssignPollersNodes" ID="NodesHelpButton1" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
   	<h1 class="sw-hdr-title"><%=Page.Title%></h1>
    
    <div class="subtitle">
        <b><%=Resources.NPMWebContent.NPMWEBCODE_ZB0_13%></b>&nbsp;<%=Resources.NPMWebContent.NPMWEBCODE_ZB0_14%>
        <a href="/Orion/Admin/Pollers/ManagePollers.aspx"><span>&raquo;&nbsp;</span><%=Resources.NPMWebContent.NPMWEBCODE_ZB0_15%></a>
        <br />
        <b><%=Resources.NPMWebContent.NPMWEBCODE_ZB0_16%></b>&nbsp;<%=Resources.NPMWebContent.NPMWEBCODE_ZB0_17%>&nbsp;<b><%=Resources.NPMWebContent.NPMWEBCODE_ZB0_18%></b>
    </div>
    
    <asp:LinkButton ID="RedirectLbt" runat="server" OnClick="DoRedirectLbt" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress DynamicLayout="false" ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <img runat="server" alt="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_75%>" src="../images/AJAX-Loader.gif" /><%=Resources.NPMWebContent.NPMWEBCODE_VB0_55%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="node-groupbox1">
                <table width="100%">
                    <tr>
                        <td>
                            <%=Resources.NPMWebContent.NPMWEBDATA_TM0_71%></td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                <div>
                    <asp:BulletedList runat="server" ID="blNodes">
                    </asp:BulletedList>
                </div>
                <orion:CustomPollers ID="NodeCustomPollers" runat="server" NetworkObjectType="Node" />
            </div>
            <br />
            <div class="sw-btn-bar">
                <orion:LocalizableButton runat="server" ID="imbtnSubmit" OnClick="imbtnSubmit_Click" LocalizedText="Submit" DisplayType="Primary" />
                <orion:LocalizableButton runat="server" ID="imbtnCancel" OnClick="imbtnCancel_Click" LocalizedText="Cancel" DisplayType="Secondary" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
