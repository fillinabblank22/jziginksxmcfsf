using System;
using SolarWinds.NPM.Web.DAL;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI.Localizer;

public partial class Orion_NPM_PrintableOverview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        NodeOverviewStyle nodeStyle;
        InterfaceOverviewStyle interfaceStyle;
        try
        {
            nodeStyle = (NodeOverviewStyle)Enum.Parse(typeof(NodeOverviewStyle), this.Request.QueryString["nodes"]);
            interfaceStyle = (InterfaceOverviewStyle)Enum.Parse(typeof(InterfaceOverviewStyle), this.Request.QueryString["interfaces"]);
        }
        catch
        {
            nodeStyle = NodeOverviewStyle.ResponseTime;
            interfaceStyle = InterfaceOverviewStyle.PercentUtilization;
        }

		this.nodeStyleLabel.Text = OverviewDAL.GetStyleDescription<NodeOverviewStyle>(nodeStyle);
		this.interfaceStyleLabel.Text = OverviewDAL.GetStyleDescription<InterfaceOverviewStyle>(interfaceStyle);

        this.overviewTable.NodeStyle = nodeStyle;
        this.overviewTable.InterfaceStyle = interfaceStyle;
    }
}
