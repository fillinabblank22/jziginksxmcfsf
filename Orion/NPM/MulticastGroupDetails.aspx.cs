﻿using System;
using System.Globalization;
using Resources;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;

public partial class Orion_NPM_MulticastGroupDetails : OrionView
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public new string Header { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var group = this.NetObject as MulticastGroup;

        int nodeId;
        string header;
        if (group.NodeID.HasValue)
        {
            // Node Details - NodeCaptionWithStatus - Multicast Group GroupName
            header = NPMWebContent.NPMWEBCODE_ZB0_5;

            var node = (Node)NetObjectFactory.Create("N:" + group.NodeID.Value);
            if (node != null)
            {
                nodeInfo.Visible = true;

                nodeLink.Node = node;

                statusIconControl.StatusLEDImageSrc = node.Status.ToString("smallled", null);
                statusIconControl.EntityId = node.NodeID.ToString(CultureInfo.InvariantCulture);
                statusIconControl.ViewLimitationId = ViewInfo.LimitationID;

                groupName.Text = group.Name;
            }
        }
        else
        {
            // Multicast Group Details - GroupName
            header = string.Format("{0} - {1}", NPMWebContent.NPMWEBCODE_PD0_1, group.Name);
        }

        this.resHost.Group = group;
        this.Header = header;

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
    }


    protected void NPMSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new SolarWinds.NPM.Web.UI.NPMSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("NetObject", Request["NetObject"] ?? string.Empty));
            NPMSiteMapPath.SetUpRenderer(renderer);
        }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    public override string ViewType
    {
        get { return "MulticastGroupDetails"; }
    }
}
