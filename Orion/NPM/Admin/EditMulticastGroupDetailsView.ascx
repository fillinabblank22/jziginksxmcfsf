<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMulticastGroupDetailsView.ascx.cs" Inherits="Orion_NPM_Admin_EditMulticastGroupDetailsView" %>

<asp:ListBox runat="server" ID="lbxMulticastGroupDetails" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$Resources:NPMWebContent,NPMWEBDATA_AK1_3%>" Value="0" />
    <asp:ListItem Text="<%$Resources:NPMWebContent,NPMWEBDATA_AK1_4%>" Value="-1" />
    
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>