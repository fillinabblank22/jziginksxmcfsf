<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditVSANDetailsView.ascx.cs" Inherits="Orion_NPM_Admin_EditVSANDetailsView" %>

<asp:ListBox runat="server" ID="lbxVSANDetails" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$Resources:NPMWebContent,NPMWEBDATA_AK1_5%>" Value="0" />
    <asp:ListItem Text="<%$Resources:NPMWebContent,NPMWEBDATA_AK1_6%>" Value="-1" />
    
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>