﻿<%@ Page Title="<%$ Resources:NPMWebContent,NPMWEBCODE_PD0_25 %>" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="UnDPThresholdsManager.aspx.cs" Inherits="Orion_NPM_Admin_UnDP_UnDPThresholdsManager" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionPHUnDPThresholds" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ID="HeadContent" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="Include1" File="../NPM/styles/AdminPages.css" runat="server" />
    <orion:Include ID="Include2" File="OrionCore.js" runat="server" />
    <orion:Include ID="Include3" File="NestedExpressionBuilder.js" runat="server" />
    <orion:Include ID="Include4" File="../NPM/js/neb.input.field.undpstatus.js" runat="server" />
    <orion:Include ID="Include5" File="NestedExpressionBuilder.css" runat="server" />
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <h1 class="sw-hdr-title"><%= Page.Title %></h1>
    <p><%= Resources.NPMWebContent.NPMWEBCODE_PD0_26 %></p>
    <table id="sw_undpthr_wrapper">
        <tr>
            <td class="sw_undpthr_wrapper_sidebar">
                <label for="sw_undpthr_search_input"><%= Resources.NPMWebContent.NPMWEBCODE_PD0_27 %></label>
                <div class="sw_undpthr_search">
                    <input type="text" id="sw_undpthr_search_input" />
                    <img id="sw_undpthr_searchButton" src="/Orion/NPM/images/icons/icon-search.png" alt="Search" />
                </div>
                <div id="sw_undpthr_itemlist_placeholder"></div>
            </td>
            <td class="sw_undpthr_wrapper_main">
                <h2 id="sw_undpthr_pollerTitle"><span><img src="/Orion/images/Icon.Info.gif" alt="" /><%= Resources.NPMWebContent.NPMWEBCODE_PD0_28 %></span></h2>
                <div id="sw_undpthr_valueTypeSelector" class="sw_undpthr_hidden">
                    <%= Resources.NPMWebContent.NPMWEBCODE_PD0_29 %>
                    <label>
                        <input type="radio" name="UnDP" id="Status" value="<%= SolarWinds.NPM.Common.Models.CustomPollerStatistic.STR_FIELD %>" />
                        <%= Resources.NPMWebContent.NPMWEBCODE_PD0_30 %>
                    </label>
                    <label>
                        <input type="radio" name="UnDP" id="RawStatus" value="<%= SolarWinds.NPM.Common.Models.CustomPollerStatistic.NUM_FIELD %>" />
                        <%= Resources.NPMWebContent.NPMWEBCODE_PD0_31 %>
                    </label>
                </div>
                <div id="sw_neb_undp_warning"></div>
                <div class="sw_undpthr_clear"></div>
                <div id="sw_neb_undp_critical"></div>
            </td>
        </tr>
    </table>

    <div class="sw-btn-bar">
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Primary" ID="saveChangesButton" Text="<%$ Resources:NPMWebContent,NPMWEBCODE_PD0_39 %>" />
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Secondary" ID="saveAndContinueButton" Text="<%$ Resources:NPMWebContent,NPMWEBCODE_PD0_35 %>" />
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Secondary" ID="cancelChangesButton" Text="<%$ Resources:NPMWebContent,NPMWEBDATA_JF0_34 %>" />
        <div id="sw_undpthr_notification">&nbsp;</div>
    </div>

    <script type="text/javascript">

        SW = SW || {};
        SW.NPM = SW.NPM || {};
        SW.NPM.UnDP = SW.NPM.UnDP || {};
        SW.NPM.UnDP.NEBGroupOpers = <%= Newtonsoft.Json.JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetGroupOperators().ToArray()) %>;
        SW.NPM.UnDP.NEBLogicalOpers = <%= Newtonsoft.Json.JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetLogicalOperators().ToArray()) %>;
        SW.NPM.UnDP.NEBFieldConstant = JSON.parse("{\"NodeType\":0,\"Value\":null,\"Child\":[{\"NodeType\":1,\"Value\":null,\"Child\":null},{\"NodeType\":2,\"Value\":null,\"Child\":null}]}");
        SW.NPM.UnDP.NEBAddConditionTitle = "<%= Resources.NPMWebContent.NPMWEBCODE_PD0_34 %>";
        SW.NPM.UnDP.NUM_FIELD = "<%= SolarWinds.NPM.Common.Models.CustomPollerStatistic.NUM_FIELD %>";
        SW.NPM.UnDP.STR_FIELD = "<%= SolarWinds.NPM.Common.Models.CustomPollerStatistic.STR_FIELD %>";
        SW.NPM.UnDP.ValueType = SW.NPM.UnDP.STR_FIELD; //setting default to Text
        SW.NPM.UnDP.SelectedPollerId = "";
        SW.NPM.UnDP.SelectedPollerName = "";
        SW.NPM.UnDP.SearchValue = "";
        SW.NPM.UnDP.WarningNEB = $("#sw_neb_undp_warning");
        SW.NPM.UnDP.CriticalNEB = $("#sw_neb_undp_critical");
        SW.NPM.UnDP.WarningTitle = "<%= Resources.NPMWebContent.NPMWEBCODE_PD0_32 %>";
        SW.NPM.UnDP.CriticalTitle = "<%= Resources.NPMWebContent.NPMWEBCODE_PD0_33 %>";
        SW.NPM.UnDP.NotificationWrapper = $("#sw_undpthr_notification");
        SW.NPM.UnDP.NotificationSuccess = $("<p class='sw-suggestion sw-suggestion-pass'><span class='sw-suggestion-icon'></span><%= Resources.NPMWebContent.NPMWEBCODE_PD0_36 %></p>");
        SW.NPM.UnDP.NotificationFail = $("<p class='sw-suggestion sw-suggestion-fail'><span class='sw-suggestion-icon'></span><%= Resources.NPMWebContent.NPMWEBCODE_PD0_37 %></p>");
        SW.NPM.UnDP.NotificationValidationFail = $("<p class='sw-suggestion sw-suggestion-fail'><span class='sw-suggestion-icon'></span><%= Resources.NPMWebContent.NPMWEBCODE_PD0_38 %></p>");
        SW.NPM.UnDP.IsDemo = ("<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") ? true : false;

        $(function() {

            // CREATE LIST + ATTACH HANDLERS
            SW.NPM.UnDP.CreateCustomPollersList = function(data, placeholderID){
                var placeholderCheck = document.getElementById(placeholderID);
                if (placeholderCheck && typeof placeholderCheck !== "undefined") {

                    var placeholder = $("#"+placeholderID);

                    // process the underscore template
                    var temp = $("#template_CustomPollersList").html();
                    var newHtml = _.template(temp, {items: data});
                    placeholder.html(newHtml);

                    // attach event handler to list items
                    placeholder.on("click", ".sw_undpthr_itemlist a", function(e){

                        e.preventDefault();
                        var self = $(this);
                        var selectedItemClassname = "sw_undpthr_itemselected";
                        SW.NPM.UnDP.SelectedPollerId = self.attr("data-id");
                        SW.NPM.UnDP.SelectedPollerName = self.text().trim();

                        // switch the 'selected' css class
                        if(!self.hasClass(selectedItemClassname)){
                            $("#" + placeholderID + " .sw_undpthr_itemlist a." + selectedItemClassname).removeClass(selectedItemClassname);
                            self.addClass(selectedItemClassname);
                        }

                        // get the valueType and init both NEBs on success
                        SW.Core.Services.callWebService(
                            "/Orion/NPM/Services/UnDPThresholds.asmx",
                            "GetValueTypeById",
                            {
                                customPollerId: SW.NPM.UnDP.SelectedPollerId
                            },
                            function(result) {

                                // change the area title
                                $("#sw_undpthr_pollerTitle").html(SW.NPM.UnDP.SelectedPollerName);

                                // show value type switch
                                $("#sw_undpthr_valueTypeSelector").removeClass("sw_undpthr_hidden");

                                // switch value type switch
                                SW.NPM.UnDP.ReselectRadio(result);

                                // set global value type
                                SW.NPM.UnDP.ValueType = result === "" ? SW.NPM.UnDP.STR_FIELD : result;

                                // hide hanging notification (when some settings are invalid and another poller is selected instead of correcting)
                                SW.NPM.UnDP.NotificationWrapper.children().hide();

                                // if the valueType is empty, there are no expressions for selected poller
                                if (result === "") {
                                    SW.NPM.UnDP.InitNEB(SW.NPM.UnDP.WarningNEB, result, SW.NPM.UnDP.WarningTitle, "#FFFDCC");
                                    SW.NPM.UnDP.InitNEB(SW.NPM.UnDP.CriticalNEB, result, SW.NPM.UnDP.CriticalTitle, "#FACECE");
                                }
                                else {
                                    // render both NEBs
                                    SW.NPM.UnDP.RenderNEBInstances(SW.NPM.UnDP.SelectedPollerId);
                                }
                            },
                            function(){}
                        );
                    });
                }
            };

            // RENDER BOTH NEB INSTANCES
            SW.NPM.UnDP.RenderNEBInstances = function(pollerId){
                SW.Core.Services.callWebService(
                    "/Orion/NPM/Services/UnDPThresholds.asmx",
                    "GetThresholdExpression",
                    {
                        customPollerId: pollerId,
                        valueType: SW.NPM.UnDP.ValueType,
                        expressionType: "Warning"
                    },
                    function(result) {
                        SW.NPM.UnDP.InitNEB(SW.NPM.UnDP.WarningNEB, result, SW.NPM.UnDP.WarningTitle, "#FFFDCC");
                    },
                    function(){}
                );
                SW.Core.Services.callWebService(
                    "/Orion/NPM/Services/UnDPThresholds.asmx",
                    "GetThresholdExpression",
                    {
                        customPollerId: pollerId,
                        valueType: SW.NPM.UnDP.ValueType,
                        expressionType: "Critical"
                    },
                    function(result) {
                        SW.NPM.UnDP.InitNEB(SW.NPM.UnDP.CriticalNEB, result, SW.NPM.UnDP.CriticalTitle, "#FACECE");
                    },
                    function(){}
                );
            };

            SW.NPM.UnDP.InitNEB = function(target, expression, instanceTitle, color){
                var DOMtitle, coloredBar;
                target.empty();
                var nebConfig = {
                    allowGroups: true,
                    groupOperators: SW.NPM.UnDP.NEBGroupOpers,
                    logicalOperators: SW.NPM.UnDP.NEBLogicalOpers,
                    ruleTypes: [
                            { displayName: SW.NPM.UnDP.NEBAddConditionTitle, expr: SW.NPM.UnDP.NEBFieldConstant }
                    ],
                    fieldInput: SW.Core.NestedExpressionBuilder.Input.Field.UnDPStatus,
                    disableAutocomplete: true,
                    undpValueType: SW.NPM.UnDP.ValueType,
                    decimal_separator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                    preferenceOptions: []
                };
                if (expression && expression !== "") {
                    // NEB WITH EXPRESSION
                    nebConfig.expr = JSON.parse(expression);
                }
                target.nested_expression_builder(nebConfig);
                // add title and some eye candy
                DOMTitle = $("<h4>").html(instanceTitle).prependTo(target);
                coloredBar = $("<div class='sw_undpthr_coloredBar'></div>").attr("style","background-color:"+color).prependTo(target);
            };

            // RESELECT RADIO - change only visually!
            SW.NPM.UnDP.ReselectRadio = function(value) {
                var radioText = $("#sw_undpthr_valueTypeSelector #"+SW.NPM.UnDP.STR_FIELD)[0];
                var radioNumber = $("#sw_undpthr_valueTypeSelector #"+SW.NPM.UnDP.NUM_FIELD)[0];
                switch (value) {
                    case SW.NPM.UnDP.STR_FIELD: radioText.checked = true;
                        break;
                    case SW.NPM.UnDP.NUM_FIELD: radioNumber.checked = true;
                        break;
                    default: radioText.checked = true;
                        break;
                }
            };

            // RENDER LIST WITH POLLERS
            SW.NPM.UnDP.RenderPollersList = function() {
                SW.Core.Services.callWebService(
                    "/Orion/NPM/Services/UnDPThresholds.asmx",
                    "GetPollers",
                    {
                        searchValue: SW.NPM.UnDP.SearchValue
                    },
                    function(result){ SW.NPM.UnDP.CreateCustomPollersList(JSON.parse(result), "sw_undpthr_itemlist_placeholder"); },
                    function(){}
                );
            };

            // REMEMBER CURRENT EXPRESSIONS
            SW.NPM.UnDP.RememberExpressions = function(successCallback){
                if (SW.NPM.UnDP.Validate()) {
                    var warningExpression = SW.NPM.UnDP.WarningNEB.nested_expression_builder('getExpression');
                    var criticalExpression = SW.NPM.UnDP.CriticalNEB.nested_expression_builder("getExpression");
                    SW.Core.Services.callWebService(
                        "/Orion/NPM/Services/UnDPThresholds.asmx",
                        "SetThresholdExpression",
                        {
                            customPollerId: SW.NPM.UnDP.SelectedPollerId,
                            valueType: SW.NPM.UnDP.ValueType,
                            expressionType: "Warning",
                            expression: warningExpression
                        },
                        function(){
                            if (successCallback && typeof successCallback !== "undefined") successCallback();
                        },
                        function(){}
                    );
                    SW.Core.Services.callWebService(
                        "/Orion/NPM/Services/UnDPThresholds.asmx",
                        "SetThresholdExpression",
                        {
                            customPollerId: SW.NPM.UnDP.SelectedPollerId,
                            valueType: SW.NPM.UnDP.ValueType,
                            expressionType: "Critical",
                            expression: criticalExpression
                        },
                        function(){},
                        function(){}
                    );
                } else {
                    SW.NPM.UnDP.NotificationWrapper.append(SW.NPM.UnDP.NotificationValidationFail.show());
                }
            };

            // VALIDATION
            SW.NPM.UnDP.Validate = function(){
                var warningIsValid = SW.NPM.UnDP.WarningNEB.nested_expression_builder("validateConstantValues");
                var criticalIsValid = SW.NPM.UnDP.CriticalNEB.nested_expression_builder("validateConstantValues");
                if (warningIsValid && criticalIsValid) {
                    SW.NPM.UnDP.NotificationWrapper.children().hide();
                    return true;
                }
                else return false;
            };

            // HEARTBEAT
            SW.NPM.UnDP.Heartbeat = function(){
                SW.Core.Services.callWebService(
                        "/Orion/NPM/Services/UnDPThresholds.asmx",
                        "Heartbeat",
                        {},
                        function(){},
                        function(){}
                    );
            };

            // LOSING FOCUS -> REMEMBER DATA
            SW.NPM.UnDP.WarningNEB.on("focusout", function(){
                SW.NPM.UnDP.RememberExpressions();
            });
            SW.NPM.UnDP.CriticalNEB.on("focusout", function(){
                SW.NPM.UnDP.RememberExpressions();
            });

            // SWITCHING THE VALUE TYPE
            $("#sw_undpthr_valueTypeSelector").on("click", "input[type='radio']", function(e){
                SW.NPM.UnDP.ValueType = e.target.value;
                SW.Core.Services.callWebService(
                    "/Orion/NPM/Services/UnDPThresholds.asmx",
                    "SetValueType",
                    {
                        customPollerId: SW.NPM.UnDP.SelectedPollerId,
                        valueType: SW.NPM.UnDP.ValueType
                    },
                    function(){ SW.NPM.UnDP.RenderNEBInstances(SW.NPM.UnDP.SelectedPollerId); },
                    function(){}
                );
            });

            // SEARCHING
            $("#sw_undpthr_searchButton").on("click", function(e){
                var searchValue = $(this).prev().val();
                SW.NPM.UnDP.SearchValue = searchValue;
                SW.NPM.UnDP.RenderPollersList();
                $("input#sw_undpthr_search_input").val(SW.NPM.UnDP.SearchValue);
            });

            // REMAP ENTER KEY ON SEARCH
            $("#sw_undpthr_search_input").on("keypress", function(e){
                var key = e.keyCode || e.which;
                if (key == 13) {
                    e.preventDefault();
                    $("#sw_undpthr_searchButton").click();
                }
            });

            // SAVING
            $("#saveChangesButton").on("click", function(e){
                e.preventDefault();
                // disable in demo mode
                if (SW.NPM.UnDP.IsDemo) {
                    demoAction("NPM_UnDPThresholds_Save", this);
                    return false;
                }
                // get actual state and save on success
                SW.NPM.UnDP.RememberExpressions(function(){
                    SW.Core.Services.callWebService(
                        "/Orion/NPM/Services/UnDPThresholds.asmx",
                        "SaveData",
                        {},
                        function(){
                            SW.NPM.UnDP.NotificationWrapper.append(SW.NPM.UnDP.NotificationSuccess.show());
                            window.location.href = "/Orion/Admin";
                        },
                        function(){
                            SW.NPM.UnDP.NotificationWrapper.append(SW.NPM.UnDP.NotificationFail.show());
                            setTimeout(function(){
                                SW.NPM.UnDP.NotificationWrapper.children().fadeOut(600);
                            }, 2000);
                        }
                    );
                });
            });

            // SAVE and CONTINUE
            $("#saveAndContinueButton").on("click", function(e){
                e.preventDefault();
                // disable in demo mode
                if (SW.NPM.UnDP.IsDemo) {
                    demoAction("NPM_UnDPThresholds_SaveAndContinue", this);
                    return false;
                }
                // get actual state and save on success
                SW.NPM.UnDP.RememberExpressions(function(){
                    SW.Core.Services.callWebService(
                        "/Orion/NPM/Services/UnDPThresholds.asmx",
                        "SaveData",
                        {},
                        function(){
                            SW.NPM.UnDP.NotificationWrapper.append(SW.NPM.UnDP.NotificationSuccess.show());
                            setTimeout(function(){
                                SW.NPM.UnDP.NotificationWrapper.children().fadeOut(600);
                            }, 2000);
                        },
                        function(){
                            SW.NPM.UnDP.NotificationWrapper.append(SW.NPM.UnDP.NotificationFail.show());
                            setTimeout(function(){
                                SW.NPM.UnDP.NotificationWrapper.children().fadeOut(600);
                            }, 2000);
                        }
                    );
                });
            });

            // CANCEL CHANGES
            $("#cancelChangesButton").on("click", function(e){
                e.preventDefault();
                SW.Core.Services.callWebService(
                    "/Orion/NPM/Services/UnDPThresholds.asmx",
                    "ResetData",
                    {},
                    function(){ window.location.href = "/Orion/Admin"; },
                    function(){}
                );
            });

            // LOAD DATA WHEN PAGE LOADS
            SW.Core.Services.callWebService(
                "/Orion/NPM/Services/UnDPThresholds.asmx",
                "LoadData",
                {},
                function(){
                    SW.NPM.UnDP.RenderPollersList();
                },
                function(){}
            );

        });
    </script>
    <script id="template_CustomPollersList" type="text/x-template">
        {# if (_.size(items) > 0) { #}
            <table class="sw_undpthr_itemlist" cellspading="0" cellpadding="0">
            {# _.each(items, function(item) { #}
                <tr>
                    <td>
                        <a href="#" data-id="{{ item.Id }}" class="sw_undpthr_item">
                            <img class="sw_undpthr_itemicon" src="{{ item.IconPath }}" alt="" /><span>{{ item.Name}}</span>
                        </a>
                    </td>
                </tr>
            {# }); #}
            </table>
        {# } else { #}
            <em style="color:#333"><%= Resources.NPMWebContent.NPMWEBDATA_LH0_5 %></em>
        {# } #}
    </script>
</asp:Content>
