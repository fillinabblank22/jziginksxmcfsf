﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NPM.Common;

public partial class Orion_NPM_Admin_NpmModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo("NPM");
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error while displaying details for Orion Core module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            NpmDetails.Name = module.ProductDisplayName;

            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_41, module.LicenseInfo);
            values.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_37, module.ProductName);
            values.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_38, module.Version);
            values.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_40, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.NPMWebContent.NPMWEBCODE_AK0_39 : module.HotfixVersion);
            AddNpmInfo(values);

            NpmDetails.DataSource = values;
			break; // we support only one "Primary" engine
        }
    }

    private void AddNpmInfo(Dictionary<string, string> values)
    { 
        try
        {
            int maxInterfaces = new FeatureManager().GetMaxElementCount(WellKnownElementTypes.Interfaces);

            int interfaceCount = 0;
            using (var npmProxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                interfaceCount = npmProxy.Api.GetLicensedInterfacesCount();
            }

            values.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_44, interfaceCount.ToString());
            values.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_43, maxInterfaces > 1000000 ? Resources.NPMWebContent.NPMWEBCODE_AK0_42 : maxInterfaces.ToString());
        }
        catch (Exception ex)
        {
            _logger.Error("Cannot get number of licensed elements.", ex);
        }
    }
}