﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Common.Models.DBMaintenance;
using SolarWinds.NPM.Common;
using SolarWinds.Logging;

public partial class Orion_Admin_MaintenanceOverdue : System.Web.UI.Page
{
    private readonly static Log Log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<DBMaintenanceInfo> maintenanceInfoList = LoadData();

            Repeater.DataSource = maintenanceInfoList;
            Repeater.DataBind();
        }
    }

    private List<DBMaintenanceInfo> LoadData()
    {
        using (var businessLayer = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                return businessLayer.Api.GetMaintenanceStatusInfo();
            }
            catch(Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
    }

    private void HandleException(Exception ex)
    {
        Log.Error(ex);
        throw ex;
    }
}