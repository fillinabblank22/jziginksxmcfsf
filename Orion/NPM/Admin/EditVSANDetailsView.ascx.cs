using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Admin_EditVSANDetailsView : ProfilePropEditUserControl
{
    #region ProfilePropEditUserControl members
    public override string PropertyValue
    {
        get
        {
            return this.lbxVSANDetails.SelectedValue;
        }
        set
        {
            ListItem item = this.lbxVSANDetails.Items.FindByValue(value);
            if (null != item)
            {
                item.Selected = true;
            }
            else
            {
                // default to "by device type"
                this.lbxVSANDetails.Items.FindByValue("-1").Selected = true;
            }
        }
    } 
    #endregion

    protected override void OnInit(EventArgs e)
    {
        foreach (ViewInfo info in ViewManager.GetViewsByType("VSANDetails"))
        {
            ListItem item = new ListItem(info.ViewTitle, info.ViewID.ToString());
            this.lbxVSANDetails.Items.Add(item);
        }

        base.OnInit(e);
    }
}
