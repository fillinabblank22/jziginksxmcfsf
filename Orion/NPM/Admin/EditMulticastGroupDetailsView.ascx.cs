using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Admin_EditMulticastGroupDetailsView : ProfilePropEditUserControl
{
    #region ProfilePropEditUserControl members
    public override string PropertyValue
    {
        get
        {
            return this.lbxMulticastGroupDetails.SelectedValue;
        }
        set
        {
            ListItem item = this.lbxMulticastGroupDetails.Items.FindByValue(value);
            if (null != item)
            {
                item.Selected = true;
            }
            else
            {
                // default to "by device type"
                this.lbxMulticastGroupDetails.Items.FindByValue("-1").Selected = true;
            }
        }
    } 
    #endregion

    protected override void OnInit(EventArgs e)
    {
        foreach (ViewInfo info in ViewManager.GetViewsByType("MulticastGroupDetails"))
        {
            ListItem item = new ListItem(info.ViewTitle, info.ViewID.ToString());
            this.lbxMulticastGroupDetails.Items.Add(item);
        }

        base.OnInit(e);
    }
}
