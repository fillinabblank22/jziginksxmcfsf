﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/AdminPage.master" AutoEventWireup="true"
    CodeFile="MaintenanceOverdue.aspx.cs" Inherits="Orion_Admin_MaintenanceOverdue" Title="<%$ Resources:NPMWebContent,WEBDATA_LH0_1%>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>


<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <style type="text/css">
        #maintenanceOverdueTable{
            width: 100%;
        }

        #maintenanceOverdueTable {
            border:1px solid #bbb;
            border-collapse:collapse;
        }
        #maintenanceOverdueTable td,#maintenanceOverdueTable th {
            border:1px solid #ccc;
            border-collapse:collapse;
            padding:5px;
            text-align: left;
        }
        #maintenanceOverdueTable th{
            background-color: #ffd800;
        }

        #adminContent td[align=right]{
            text-align:right !important;
        }

    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table width="100%" id="breadcrumb" style="width: 100%;">
        <tr>
            <td style="border-bottom-width: 0px;">
                <h1><%=Page.Title%></h1>
            </td>
            <td align="right" style="vertical-align: top; border-bottom-width: 0px;">
                <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHDatabaseMaintenanceOverdue" />
            </td>
        </tr>
    </table>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />

    <asp:Repeater ID="Repeater" runat="server">
        <HeaderTemplate>
            <table border="1" id="maintenanceOverdueTable">
                <thead>
                    <tr>
                        <th><%=Resources.NPMWebContent.WEBDATA_LH0_2%></th>
                        <th><%=Resources.NPMWebContent.WEBDATA_LH0_3%></th>
                        <th><%=Resources.NPMWebContent.WEBDATA_LH0_4%></th>
                        <th><%=Resources.NPMWebContent.WEBDATA_LH0_5%></th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# DataBinder.Eval(Container.DataItem, "StatisticTable")%></td>
                <td><%# DataBinder.Eval(Container.DataItem, "OldestEntry")%></td>
                <td><%# DataBinder.Eval(Container.DataItem, "Overdue", String.Format(Resources.CoreWebContent.LIBCODE_VB0_6, @"{0:%d}"))%></td>
                <td><%# DataBinder.Eval(Container.DataItem, "PluginName")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
