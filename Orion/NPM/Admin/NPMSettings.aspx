<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="NPMSettings.aspx.cs" 
    Inherits="Orion_NPM_Admin_NPMSettings" Title="<%$ Resources: NPMWebContent, WEBDATA_PCC_1%>" %>
<%@ Register TagPrefix="NetPerfMon" TagName="ThresholdSetting" Src="~/Orion/NetPerfMon/Admin/ThresholdSetting.ascx" %>
<%@ Register TagPrefix="NetPerfMon" TagName="UseAvgOrPeak" Src="~/Orion/NetPerfMon/Admin/UseAvgOrPeak.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionPHNetworkPerformanceMonitorThresholds" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <table class="PageHeader HeaderTable" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <h1><%=Page.Title %></h1>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
    <%=Resources.NPMWebContent.WEBDATA_PCC_2%>
    
    <table cellspacing="0">
        
        <tr><td colspan="4"><h2><%=Resources.NPMWebContent.NPMWEBDATA_TM0_5%></h2></td></tr>
        <NetPerfMon:ThresholdSetting ID="stgBufferMissHigh" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="1000000" />
        <NetPerfMon:ThresholdSetting ID="stgBufferMissWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="1000000" />
        
        <tr><td colspan="4"><h2><%=Resources.NPMWebContent.NPMWEBDATA_TM0_6%></h2></td></tr>
        <NetPerfMon:ThresholdSetting ID="stgErrorsDiscardsError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="50000" /> 
        <NetPerfMon:ThresholdSetting ID="stgErrorsDiscardsWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="50000" /> 
        
        <tr><td colspan="4"><h2><%=Resources.NPMWebContent.NPMWEBDATA_TM0_7%></h2></td></tr>
        <NetPerfMon:ThresholdSetting ID="stgIfUtilError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" /> 
        <NetPerfMon:ThresholdSetting ID="stgIfUtilWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" />

        <NetPerfMon:UseAvgOrPeak ID="useAvgOrPeakInInterfaceUtilization" runat="server" MetricName="Forecast.Metric.InInterfacePercentUtilization" />
        <tr><td colspan="4"><h2><%=Resources.NPMWebContent.NPMWEBCODE_VT0_39%></h2></td></tr>
        <%--useAvgOrPeakOutInterfaceUtilization can be used when InterfaceUtilization will be splitted --%> 
        <NetPerfMon:UseAvgOrPeak ID="useAvgOrPeakOutInterfaceUtilization" runat="server" MetricName="Forecast.Metric.OutInterfacePercentUtilization" Visible="False" />
        <NetPerfMon:ThresholdSetting ID="stgFlapsError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="50000" /> 
        <NetPerfMon:ThresholdSetting ID="stgFlapsWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="50000" />
        
    </table>
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="imgSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
    </div> 
</asp:Content>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
<style type="text/css">

.HeaderTable tr td
{
	border-bottom-style: none!important;
}

#adminContent table tr th, #adminContent table tr td
{
	border-bottom: thin solid #dddddd;
	padding-bottom: .1em;
	padding-top: .2em;
}

#adminContent table tr td h2
{
	margin-bottom: 0em;
	padding-bottom: 0em;
}

#adminContent table
{
    width: 100%;
}

#adminContent table tr th, #adminContent table tr td
{
    border-bottom: thin solid #dddddd;
}

#adminContent table tr td
{
    font-size: 8pt;
}

#adminContent table tr th
{
    text-align: left;
    font-size: 9pt;
}

#adminContent table tr th, #adminContent table tr td
{
    white-space: nowrap;
    padding-right: .5em;
    font-size: 8pt;
}

#adminContent table tr td.allowWrap
{
    white-space: normal;
}
</style>
</asp:Content>
