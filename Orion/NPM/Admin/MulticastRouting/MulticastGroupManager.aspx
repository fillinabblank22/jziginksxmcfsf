﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="MulticastGroupManager.aspx.cs" Inherits="Orion_NPM_Admin_MulticastGroups"
    Title="<%$ Resources:NPMWebContent,NPMWEBDATA_LH0_1 %>" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <script type="text/javascript">
        SW.Core.namespace("SW.NPM").IsDemo = ("<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") ? true : false;
    </script>
    <orion:Include ID="Include2" runat="server" File="/NPM/Admin/MulticastRouting/MulticastGroupManager.js" />

    <style type="text/css">
        span.Label { white-space:nowrap; margin-left:5px;}
        .smallText {color:#979797;font-size:9px;}
        .edit { background-image:url(/Orion/images/edit_16x16.gif) !important; }
        #Grid td { padding-right: 0px; padding-bottom: 0px; vertical-align: middle; }
        #Grid { padding-top: 12px; }
        .x-btn td { text-align: center !important; }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1 class="sw-hdr-title"><%= Page.Title %></h1>

    <div id="gridPanel" style="max-width:870px;">
        <div id="Grid"/>
    </div>
    <div class="sw-btn-bar" >
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Primary" ID="saveChangesButton" Text="<%$ Resources:NPMWebContent,NPMWEBDATA_LH0_4 %>"/>
    </div>
</asp:Content>


