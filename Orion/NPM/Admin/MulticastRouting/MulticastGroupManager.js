﻿Ext.namespace('SW');
Ext.namespace('SW.NPM');

SW.NPM.MulticastGroupManager = function () {

    var defaults = {
        getUrl: '/Orion/NPM/Services/MulticastRouting.asmx/GetGroupsNames',
        setUrl: '/Orion/NPM/Services/MulticastRouting.asmx/SetGroupsNames'
    };
    var grid, dataStore, dialog;

    function editSelectedMulticastGroup() {
        var selectionModel = grid.getSelectionModel();
        // start editing only when a row (a cell within a row) is selected
        if (selectionModel.selection) {
            // get position of selected row (ie. 0= 1st row, 1=2nd row, etc)
            var selectedRow = selectionModel.selection.cell[0];
            // start edit the value in the second column which is Name
            grid.startEditing(selectedRow, 1);
        }
    }

    // performs the ajax post to the server (saves the multicast group names)
    function saveItems(items, success) {
        var request = {
            names: items
        }

        var paramString = JSON.stringify(request);
        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: defaults.setUrl,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                success();
            }
        });
    }

    // reloads the data back from server
    function reload() {
        grid.store.load();
    }

    return {
        init: function () {
            dataStore = new ORION.WebServiceStore(
                defaults.getUrl,
                [
                    { name: 'GroupIPAddress', mapping: 0 },
                    { name: 'GroupName', mapping: 1 }
                ],
                'GroupIPAddress'
            );

            // define grid panel
            grid = new Ext.grid.EditorGridPanel({

                store: dataStore,

                columns: [{
                    id: 'groupIPAddress',
                    header: "@{R=NPM.Strings;K=NPMWEBJS_LH0_1;E=js}",
                    dataIndex: 'GroupIPAddress',
                    sortable: true,
                    width: 200
                }, {
                    header: "@{R=NPM.Strings;K=NPMWEBJS_LH0_2;E=js}",
                    dataIndex: 'GroupName',
                    sortable: true,
                    width: 271,
                    editor: new Ext.form.TextField({
                        allowBlank: true
                    }),
                    renderer: 'htmlEncode'
                }],
                tbar: [
                {
                    id: 'EditButton',
                    text: "@{R=NPM.Strings;K=NPMWEBJS_LH0_3;E=js}",
                    iconCls: 'edit',
                    handler: function () { editSelectedMulticastGroup(); }
                }],

                viewConfig: {
                    forceFit: false
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 500,
                height: 300,
                enableColumnResize: true,
                enableColumnHide: false,
                stripeRows: true
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });


            grid.render('Grid');
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = {};
            grid.store.load();
        },

        save: function () {
            // use underscore lib to transform items of array, extract "data" field of item
            // which actually contains required information.
            var items = _.map(dataStore.data.items, function (item) { return item.data; });
            saveItems(items, function () { reload(); });
        }
    }
} ();



Ext.onReady(SW.NPM.MulticastGroupManager.init, SW.NPM.MulticastGroupManager);
$(function () {
    $("#saveChangesButton").on("click", function () {
        if (SW.NPM.IsDemo) {
            demoAction('NPM_MulticastGroupsSetup_Save', this);
            return false;
        } else {
            SW.NPM.MulticastGroupManager.save();
        }
    });
});