using System;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using System.Collections.Generic;

public partial class Orion_NPM_Admin_NPMSettings : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    protected override void OnInit(EventArgs e)
    {
        var allPeakSettings = ForecastingCapacityDAL.GetAllUsePeakValues();

        useAvgOrPeakInInterfaceUtilization.UsePeak = allPeakSettings[useAvgOrPeakInInterfaceUtilization.MetricName];
        useAvgOrPeakOutInterfaceUtilization.UsePeak = allPeakSettings[useAvgOrPeakOutInterfaceUtilization.MetricName];

        stgBufferMissHigh.Setting = Thresholds.BufferMissError;
        stgBufferMissWarning.Setting = Thresholds.BufferMissWarning;
        stgErrorsDiscardsError.Setting = Thresholds.ErrorsDiscardsError;
        stgErrorsDiscardsWarning.Setting = Thresholds.ErrorsDiscardsWarning;
        stgIfUtilError.Setting = Thresholds.IfPercentUtilizationError;
        stgIfUtilWarning.Setting = Thresholds.IfPercentUtilizationWarning;
	    stgFlapsError.Setting = Thresholds.RoutingFlapsError;
		stgFlapsWarning.Setting = Thresholds.RoutingFlapsWarning;
        
        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        Dictionary<string, bool> dict = new Dictionary<string, bool>();
        dict.Add(useAvgOrPeakInInterfaceUtilization.MetricName, useAvgOrPeakInInterfaceUtilization.UsePeak);
        //usePeak same as at InInterfaceUtilization
        dict.Add(useAvgOrPeakOutInterfaceUtilization.MetricName, useAvgOrPeakInInterfaceUtilization.UsePeak);

        ForecastingCapacityDAL.SetAllUsePeakValues(dict);

        stgBufferMissHigh.Setting.SaveSetting(Convert.ToDouble(stgBufferMissHigh.Value));
        stgBufferMissWarning.Setting.SaveSetting(Convert.ToDouble(stgBufferMissWarning.Value));
        stgErrorsDiscardsError.Setting.SaveSetting(Convert.ToDouble(stgErrorsDiscardsError.Value));
        stgErrorsDiscardsWarning.Setting.SaveSetting(Convert.ToDouble(stgErrorsDiscardsWarning.Value));
        stgIfUtilError.Setting.SaveSetting(Convert.ToDouble(stgIfUtilError.Value));
        stgIfUtilWarning.Setting.SaveSetting(Convert.ToDouble(stgIfUtilWarning.Value));
		stgFlapsError.Setting.SaveSetting(Convert.ToDouble(stgFlapsError.Value));
		stgFlapsWarning.Setting.SaveSetting(Convert.ToDouble(stgFlapsWarning.Value));

        ReferrerRedirectorBase.Return("/Orion/Admin/");
    }
}
