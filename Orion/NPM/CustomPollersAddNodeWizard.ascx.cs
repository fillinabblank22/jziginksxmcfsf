using System;
using System.Collections.Generic;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Web;
using Node=SolarWinds.Orion.Core.Common.Models.Node;

public partial class Orion_NPM_CustomPollers : System.Web.UI.UserControl, IAddCoreNodePlugin
{
	private readonly string CustomPollersKey = "CustomPollers";

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			ValidatePage();
			litNodeIP.Text = SolarWinds.Orion.Web.NodeWorkflowHelper.Node.Caption;

			NodeCustomPollers.LoadCustomPollers();
		}
	}

	private void ValidatePage()
	{
		if (SolarWinds.Orion.Web.NodeWorkflowHelper.Node == null)
			Response.Redirect("Default.aspx");
	}

	public void Cancel()
	{
		NodeWorkflowHelper.Reset();
		Response.Redirect("../Default.aspx", true);
	}

	public void Next()
	{
		if (!Page.IsValid) { return; }

		NodeCustomPollers.UpdateInMemoryNodeInformation();

		foreach (Guid id in ((Dictionary<Guid, CustomPoller>)NodeWorkflowHelper.NewNodeInfo[CustomPollersKey]).Keys)
		{
			CustomPoller cpoller = new CustomPoller();
			cpoller.CustomPollerID = id;
			NodeWorkflowHelper.Node.CustomPollers.Add(cpoller);
		}
	}

	public void Previous()
	{
	}

	public void Save(Node node)
	{
		
	}

	public string Title
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VB0_1; }
	}

	public bool ValidateInput()
	{
		return true;
	}
}
