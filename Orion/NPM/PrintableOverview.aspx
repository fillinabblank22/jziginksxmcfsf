<%@ Page Title="<%$ Resources: NPMWebContent, NPMWEBDATA_AK0_2 %>" Language="C#" AutoEventWireup="true" CodeFile="PrintableOverview.aspx.cs" Inherits="Orion_NPM_PrintableOverview" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="orion" TagName="OverviewReport" Src="~/Orion/NPM/Controls/OverviewReportControl.ascx"  %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <form id="form1" runat="server" class="sw-app-region">
        <h1><%=Resources.NPMWebContent.NPMWEBDATA_AK0_2%></h1>
        <div style="margin:10px;">
            <b><%=Resources.NPMWebContent.NPMWEBDATA_AK0_7%>:</b> <asp:Label runat="server" ID="nodeStyleLabel" /><br />
            <b><%=Resources.NPMWebContent.NPMWEBDATA_AK0_8%>:</b> <asp:Label runat="server" ID="interfaceStyleLabel" /><br /><br />
            <orion:OverviewReport runat="server" Printable="true" id="overviewTable" />
        </div>
    </form>
</asp:Content>
