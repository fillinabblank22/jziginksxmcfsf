<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InternalInterfaceList.ascx.cs" Inherits="Orion_NPM_InternalInterfaceList" %>
<%@ Register Src="~/Orion/NPM/Controls/InterfaceLink.ascx" TagPrefix="npm" TagName="InterfaceLink" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:Repeater ID="InterfaceTable" runat="server" OnInit="InterfaceTable_Init">
			<HeaderTemplate>
				<table class="NeedsZebraStripes" border="0" cellPadding="3" cellSpacing="0" width="100%">
					<thead>
						<tr>
							<td colspan="2">STATUS</td>
							<td></td>
							<td>INTERFACE</td>
							<td colspan="2">TRANSMIT</td>
							<td colspan="2">RECEIVE</td>
						</tr>
					</thead>
			</HeaderTemplate>

			<ItemTemplate>
				<tr>
					<td>
						<npm:InterfaceLink runat="server" InterfaceID='<%# Eval("NetObjectID") %>'>
							<Content>
								<img src="<%#Eval("Status", "{0:smallled}") %>">
							</Content>
						</npm:InterfaceLink>
					</td>
					<td>
						<npm:InterfaceLink ID="InterfaceLink1" runat="server" InterfaceID='<%# Eval("NetObjectID") %>'>
							<Content>
								<%#Eval("Status") %>
							</Content>
						</npm:InterfaceLink>
					</td>
					<td>
						<npm:InterfaceLink ID="InterfaceLink2" runat="server" InterfaceID='<%# Eval("NetObjectID") %>'>
							<Content>
								<img src="/NetPerfMon/images/Interfaces/<%#Eval("InterfaceType") %>.gif" border="0">
							</Content>
						</npm:InterfaceLink>
					</td>
					<td>
						<npm:InterfaceLink ID="InterfaceLink3" runat="server" InterfaceID='<%# Eval("NetObjectID") %>'>
							<Content>
								<%#Eval("Caption") %>
							</Content>
						</npm:InterfaceLink>
					</td>
					<td class="<%#Eval("OutPercentUtil", "{0:class}") %>">
						<a href='/Orion/NetPerfMon/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject=<%# Eval("NetObjectID") %>'><%#Eval("OutPercentUtil") %></a>
					</td>
					<td></td>
					<td class="<%#Eval("InPercentUtil", "{0:class}") %>">
						<a href='/Orion/NetPerfMon/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject=<%# Eval("NetObjectID") %>'><%#Eval("InPercentUtil") %></a>
					</td>
					<td></td>
				</tr>
			</ItemTemplate>

			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</Content>
</orion:resourceWrapper>
