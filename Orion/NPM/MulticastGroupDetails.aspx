﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
         CodeFile="MulticastGroupDetails.aspx.cs" Inherits="Orion_NPM_MulticastGroupDetails" 
         Title="Multicast Group Details"%>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.NPM.Web.UI" Assembly="SolarWinds.NPM.Web" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
               <br />
	            <div style="margin-left: 10px" >
                    <orion:DropDownMapPath ID="NPMSiteMapPath" runat="server" Provider="NPMSitemapProvider" OnInit="NPMSiteMapPath_OnInit">
                    <RootNodeTemplate>
                        <a href="<%# Eval("url") %>" ><u> <%# Eval("title") %></u> </a>
                    </RootNodeTemplate>
                    <CurrentNodeTemplate>
				        <a href="<%# Eval("url") %>" ><%# Eval("title") %> </a>
				    </CurrentNodeTemplate>
                    </orion:DropDownMapPath>
				</div>
				<%} %>
	<h1>
	    <%=this.Header%>
        <asp:PlaceHolder runat="server" ID="nodeInfo" Visible="False">
            <asp:Label runat="server" ID="groupName" /> 
            &nbsp;<%= Resources.NPMWebContent.NPMWEBDATA_YLY_0 %>
            <orion:StatusIconControl ID="statusIconControl" runat="server" />
            <npm:NodeLink ID="nodeLink" runat="server" />
        </asp:PlaceHolder>
	</h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:MulticastGroupResourceHost runat="server" ID="resHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:MulticastGroupResourceHost>
</asp:Content>
