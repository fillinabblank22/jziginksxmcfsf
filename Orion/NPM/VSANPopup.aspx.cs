﻿using System;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Web;
using SolarWinds.NPM.Common;

public partial class Orion_NPM_VSANPopup : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected SolarWinds.NPM.Common.Models.VSAN VSAN { get; set; }

    protected string VsanState
    {
        get
        {
            if (VSAN != null)
            {
                switch (VSAN.AdminState)
                {
                    case SolarWinds.NPM.Common.Models.VsanAdminState.Active:
                        return "Up";

                    default:
                        return "Unknown";
                }
            }

            return String.Empty;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        var VSANNetObject = NetObjectFactory.Create(Request["NetObject"], true) as VSAN;

        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create()) 
        {
            try
            {
                this.VSAN = proxy.Api.GetVSAN(VSANNetObject.ID);
            }
            catch(Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }
        }

        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
	}

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }
}
