﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VSANPopup.aspx.cs" Inherits="Orion_NPM_VSANPopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>

<%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>

<h3 class="Status<%=this.VsanState%>"><%=this.VSAN.Name%> (ID <%=this.VSAN.ID%>)</h3>

<div class="NetObjectTipBody">
    <p class="StatusDescription"><%= String.Format(Resources.NPMWebContent.NPMWEBDATA_TM0_70, GetLocalizedProperty("AdminState", this.VSAN.AdminState.ToString())) %></p>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th style="font-weight:bold"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_69%></th>
			<td colspan="2"><%= GetLocalizedProperty("VsanMediaType", this.VSAN.MediaType.ToString()) %></td>
		</tr>
		<tr>
			<th style="font-weight:bold"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_68%></th>
			<td colspan="2"><%= GetLocalizedProperty("VsanLoadBalancingType", this.VSAN.LoadBalancingType.ToString()) %></td>
		</tr>
        </table>
</div>
