<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="InterfaceCustomPollers.aspx.cs" Inherits="Orion_Nodes_InterfaceCustomPollers" Title="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_73%>" %>

<%@ Register Src="~/Orion/NPM/Controls/CustomPollers.ascx" TagPrefix="orion" TagName="CustomPollers" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionPHNodeManagementAssignPollersInterfaces" ID="NodesHelpButton1" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%=Page.Title%></h1>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress DynamicLayout="false" ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <img runat="server" alt="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_75%>" src="../images/AJAX-Loader.gif" /><%=Resources.NPMWebContent.NPMWEBCODE_VB0_55%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="node-groupbox1">
                <table width="100%">
                    <tr>
                        <td>
                            <%=Resources.NPMWebContent.NPMWEBDATA_TM0_72%></td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                <div>
                    <asp:BulletedList runat="server" ID="blInterfaces">
                    </asp:BulletedList>
                </div>
                <orion:CustomPollers ID="InterfaceCustomPollers" runat="server" NetworkObjectType="Interface" />
            </div>
            <br />
            <div class="sw-btn-bar">
                <orion:LocalizableButton runat="server" ID="imbtnSubmit" OnClick="imbtnSubmit_Click" LocalizedText="Submit" DisplayType="Primary" />
                <orion:LocalizableButton runat="server" ID="imbtnCancel" OnClick="imbtnCancel_Click" LocalizedText="Cancel" DisplayType="Secondary" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
