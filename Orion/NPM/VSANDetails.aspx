<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
         CodeFile="VSANDetails.aspx.cs" Inherits="Orion_NPM_VSANDetails" 
         Title="VSAN Details"%>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="npm" Assembly="SolarWinds.NPM.Web" Namespace="SolarWinds.Orion.NPM.Web.UI" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
               <br />
	            <div style="margin-left: 10px" >
                    <orion:DropDownMapPath ID="NPMSiteMapPath" runat="server" Provider="NPMSitemapProvider" OnInit="NPMSiteMapPath_OnInit">
                    <RootNodeTemplate>
                        <a href="<%# Eval("url") %>" ><u> <%# Eval("title") %></u> </a>
                    </RootNodeTemplate>
                    <CurrentNodeTemplate>
				        <a href="<%# Eval("url") %>" ><%# Eval("title") %> </a>
				    </CurrentNodeTemplate>
                    </orion:DropDownMapPath>
				</div>
				<%} %>
    <h1>
	    <%=this.Title%>
	</h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<npm:VSANResourceHost runat="server" ID="ifHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</npm:VSANResourceHost>
</asp:Content>
