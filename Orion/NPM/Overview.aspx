<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="Overview.aspx.cs" Inherits="Orion_NPM_Overview" Title="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_2%>" %>
<%@ Register TagPrefix="orion" TagName="HelpButton" Src="~/Orion/Controls/HelpButton.ascx" %>
<%@ Register TagPrefix="orion" TagName="OverviewReport" Src="~/Orion/NPM/Controls/OverviewReportControl.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <asp:LinkButton ID="btnPrintable" runat="server" CssClass="printablePageLink" OnClick="Printable_Click" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_246 %>" />
    <orion:IconHelpButton HelpUrlFragment="OrionPHOverview" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">
    <div class="titleTable">
        <h1><%=Page.Title %></h1>
    </div>
    <div style="margin-left: 10px; margin-right: 10px; border: 1px solid #DFDED7; background-color: white; padding: 10px;">
        <table style="width: 100%;">
            <tr>
                <td><%=Resources.NPMWebContent.NPMWEBDATA_AK0_7%>:</td>
                <td><%=Resources.NPMWebContent.NPMWEBDATA_AK0_8%>:</td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList runat="server" ID="NodeStyle" DataTextField="Value" DataValueField="Key" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="InterfaceStyle" DataTextField="Value" DataValueField="Key" />
                </td>
                <td><%=Resources.NPMWebContent.NPMWEBDATA_AK0_5%></td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5" style="padding: 10px 0px 10px 0px;">
                    <orion:LocalizableButton ID="ButtonRefresh" runat="server" DisplayType="Primary" LocalizedText="Refresh" />
                </td>
            </tr>
            <tr>
            	<td colspan="5" class="Text">
            	    <div id="Details">
            	        <%=Resources.NPMWebContent.NPMWEBDATA_AK0_6%>
            	    </div>
            	</td>
            </tr>
        </table>
        <orion:OverviewReport runat="server" ID="overviewTable" Printable="false"/>
    </div>
</asp:Content>

