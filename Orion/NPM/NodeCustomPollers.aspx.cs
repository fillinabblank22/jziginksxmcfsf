using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.NPM.Common.Models;
using SolarWinds.NPM.Common;
using System.Collections.Generic;
using Node=SolarWinds.Orion.Core.Common.Models.Node;
using SolarWinds.Orion.Core.Common;

public partial class Orion_Nodes_NodeCustomPollers : System.Web.UI.Page
{
	private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
	private readonly string CustomPollersKey = "CustomPollers";
	private readonly string CustomPollersKeyOld = "CustomPollersOld";

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			NodeWorkflowHelper.FillNetObjectIds("Nodes");
			ValidatePage();
			LoadSelectedNodes();
			NodeCustomPollers.LoadCustomPollers();			
		}
	}

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

	protected void imbtnCancel_Click(object sender, EventArgs e)
	{
		NodeWorkflowHelper.Reset();
        ReferrerRedirectorBase.Return("Default.aspx");	
	}

	protected void DoRedirectLbt(object sender, EventArgs e)
	{
        ReferrerRedirectorBase.Return("Default.aspx");
    }

	protected void imbtnSubmit_Click(object sender, EventArgs e)
	{
		string mes = Resources.NPMWebContent.NPMWEBCODE_TM0_36;
		try
		{
			NodeCustomPollers.AssignCustomPollers(NodeWorkflowHelper.MultiSelectedNodeIds);
		}
		catch(Exception ex)
		{
			if (!string.IsNullOrEmpty(NodeCustomPollers.LastBLErrorText))
				mes = NodeCustomPollers.LastBLErrorText;
			else
				mes = ex.Message;
		}
		
		NodeWorkflowHelper.MultiSelectedNodeIds.Clear();
		NodeWorkflowHelper.Reset();

		ScriptManager.RegisterStartupScript(this, this.GetType(), "CusomPollerAssignmentSucceded", string.Format("alert('" + mes + "');location.href='{0}'", ReferrerRedirectorBase.GetDecodedReferrerUrl()), true);
		//ScriptManager.RegisterStartupScript(this, this.GetType(), "CusomPollerAssignmentSucceded", this.Page.ClientScript.GetPostBackEventReference(RedirectLbt, (new Guid()).ToString()) + "; alert('"+mes+"');", true);
	}

	private void ValidatePage()
	{
		if (NodeWorkflowHelper.MultiSelectedNodeIds.Count == 0)
            ReferrerRedirectorBase.Return("Default.aspx");
	}

	private void LoadSelectedNodes()
	{
		if (!SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo.ContainsKey(CustomPollersKey))
			SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo.Add(CustomPollersKey, new Dictionary<Guid, CustomPoller>());

		if (!SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo.ContainsKey(CustomPollersKeyOld))
			SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo.Add(CustomPollersKeyOld, new Dictionary<Guid, CustomPoller>());		

		using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
		{
			ListItem item;
			Dictionary<Guid, int> repeatIndicator = new Dictionary<Guid, int>();			
			foreach (int id in NodeWorkflowHelper.MultiSelectedNodeIds)
			{
				item = new ListItem();
				Node node = null;
				using (ICoreBusinessLayer coreProxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
					node = coreProxy.GetNodeWithOptions(id, false, false);

				item.Text = node.Caption;
				blNodes.Items.Add(item);

                CustomPollerAssignments assignments;
                try
                {
                    assignments = proxy.Api.GetCustomPollerAssignmentsForNodeOnly(id);
                }
                catch(Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }


                foreach (CustomPollerAssignment assignment in assignments)
				{					
					if (!repeatIndicator.ContainsKey(assignment.CustomPollerID))
						repeatIndicator[assignment.CustomPollerID] = 0;

					repeatIndicator[assignment.CustomPollerID]++;
				}				
			}

			foreach (KeyValuePair<Guid, int> keyValue in repeatIndicator)
			{
				if (keyValue.Value == NodeWorkflowHelper.MultiSelectedNodeIds.Count)
				{
					((Dictionary<Guid, CustomPoller>)SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo[CustomPollersKey])[keyValue.Key] = null;

					((Dictionary<Guid, CustomPoller>)SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo[CustomPollersKeyOld])[keyValue.Key] = null;
				}
			}			
		}
	}

	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}
}

