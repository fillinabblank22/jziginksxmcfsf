﻿var GroupIPTree = {

    Succeeded: function (result, context) {
        context.after(result);
    },

    Failed: function (error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = String.format("@{R=Core.Strings;K=WEBJS_AK0_1;E=js}", error.get_message());
    },

    Click: function (rootId, resourceId, nodeId, viewLimitationId) {
        return GroupIPTree.HandleClick(rootId, function (contentDiv) {
            MulticastRouting.GetGroupIPTreeSection(resourceId, rootId, nodeId, viewLimitationId, GroupIPTree.Succeeded, GroupIPTree.Failed, contentDiv);
        }, function (contentDiv) {
            GroupIPTree.Collapse(rootId);
            MulticastRouting.RemoveGroupIP(resourceId, rootId);
        });
    },

    LoadRoot: function (treeDiv, resourceId, nodeId, viewLimitationId) {
        $('tr', treeDiv).not('.ReportHeader').remove();
        MulticastRouting.GetGroupIPTreeSection(resourceId, '', nodeId, viewLimitationId, GroupIPTree.Succeeded, GroupIPTree.Failed, treeDiv.find('tr'));
    },

    HandleClick: function (rootId, expandCallback, collapseCallback) {
        var toggleImg = $('#' + rootId + '-toggle');
        var actual = $('#' + rootId);

        if (toggleImg.attr('src') == '/Orion/images/Button.Expand.gif') {
            toggleImg.attr('src', '/Orion/images/Button.Collapse.gif');
            expandCallback(actual);
        }
        else {
            toggleImg.attr('src', '/Orion/images/Button.Expand.gif');
            collapseCallback(actual);
        }
        return false;
    },

    Collapse: function (rootId) {
        var contentElement = $('#' + rootId).nextAll('#'+rootId+'-content');
        contentElement.remove();
    }

};