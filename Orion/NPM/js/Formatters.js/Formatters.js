SW = SW || {};
SW.NPM = SW.NPM || {};
SW.NPM.Formatters = SW.NPM.Formatters || {};

(function (ns) {
    ns.formatMacAddress = function (address) {
        return SW.Core.Formatters.insertPerChunks(address, 2, ':');
    };
})(SW.NPM.Formatters);