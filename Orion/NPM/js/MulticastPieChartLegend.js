﻿SW = SW || {};
SW.NPM = SW.NPM || {};
SW.NPM.Charts = SW.NPM.Charts || {};
SW.NPM.Charts.MulticastPieChartLegend = SW.NPM.Charts.MulticastPieChartLegend || {};
(function (legend) {

    legend.createStandardLegend = function (chart, legendContainerId) {
        var table = $('#' + legendContainerId);

        // we need to clean the table, because of async refresh
        table.empty();
        
        // there's only one serie, slices are represented by the data points
        var serie = chart.series[0];

        // custom data are in data point, check first one to find out which template to use
        var templateId = chart.series[0].data[0].templateId;
        var source = $('#'+templateId).html();
        var newHtml = _.template(source, serie);
        table.append(newHtml);
    };
    
    // rectangle with appropriate color matching the chart
    legend.createLegendSymbol = function (point) {
        var container = $('<td />');
        SW.Core.Charts.Legend.addLegendSymbol(point, container);
        return container.html();
    };
    
    legend.createLegendLabel = function(point) {
        if (point.id > 0)
            return String.format("<a href='/Orion/View.aspx?NetObject=MCG:{0};N:{1}'>{2}</a>", point.id, point.nodeid, point.name);
        else
            return point.name;
    }
}(SW.NPM.Charts.MulticastPieChartLegend));