﻿SW = SW || {};
SW.NPM = SW.NPM || {};


SW.NPM.ManageNodeDialog = function(engineId, ipAddress) {

    var dialog;

    function onClose() {
        if (dialog) {
            dialog.dialog("close");
            dialog = {};
        }
    }

    ;

    function onManage() {
        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }

        var url = "/Orion/Nodes/Add/Default.aspx?EngineID=" + engineId + "&IPAddress=" + encodeURIComponent(ipAddress);

        window.location = url;
    }

    ;

    var textHtml =
        '<div style="height:70px; background-color:white; display:block; padding:10px;">' +
            '<table>' +
            '<tr>' +
            '<td style="width:40px;">' +
            '<img src="/Orion/Images/Info-Notification.gif" alt="" />' +
            '</td>' +
            '<td>' +
            '<b>@{R=NPM.Strings;K=NPMWEBJS_VT0_3;E=js}</b><br />' +
            '@{R=NPM.Strings;K=NPMWEBJS_VT0_4;E=js}' +
            '</td>' +
            '</tr>' +
            '</table> {0}' +
            '{1}' +
            '</div>';

    var dialogButtons = '<br /><div style="float: right">';
    dialogButtons += SW.Core.Widgets.Button("@{R=NPM.Strings;K=NPMWEBJS_VT0_5;E=js}", { type: 'primary', id: 'btnManage' });
    dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=NPM.Strings;K=NPMWEBJS_VT0_6;E=js}", { type: 'secondary', id: 'btnClose' });
    dialogButtons += '</div>';

    textHtml = String.format(textHtml, "", dialogButtons);


    var $dialog = $(textHtml);
    $dialog.find("#btnManage").click(function() { onManage(); });
    $dialog.find("#btnClose").click(function() { onClose(); });
    dialog = $dialog.dialog({
        width: 390,
        title: "@{R=NPM.Strings;K=NPMWEBJS_VT0_7;E=js}",
        modal: true,
        resizable: false
    });
};
