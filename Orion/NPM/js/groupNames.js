﻿Ext.namespace("SW.NPM.Resources");

/* Object is responsible for rendering Group Names dialog UI. 
 *
 * @param {Object} config 
 *  config.renderTo {String} id of the element where will be resource picker rendered.
 *  config.selectedItemsRenderTo {String} id of the element where will be rendered selected resources.
 *  config.selectionMode {String}  determine how many items can be selected. Valid options are: Single | Multiple.
 *  config.onCreated {Function} callback function which will be called when is resource picker created. To callback is passed instance of picker.
 *  config.onSelected {Function} callback function which will be called when filds are picked. To callback is passed instance of picker and customData.
 */
SW.NPM.Resources.GroupNames = function (config) {
    var defaults = {
        widths: {
            dialog: isIEVersion7andLess() ? '1080' : 'auto'
        }
    };

    config = jQuery.extend(true, defaults, config);

    var self = this,
        rendered = false,
        $dialogElement = $('#' + config.dialog.renderTo);

    var gridItems = null;

    var gridDataStore;

    var groupsConfig = {
        url: '/Orion/NPM/Services/MulticastRouting.asmx/GetGroupsNames'
    };

    config = jQuery.extend(true, config, {
        groups: groupsConfig
    });

    initDialogButtons();

    function initDialogButtons() {
        $(config.dialog.btnOk).click(selected);
        $(config.dialog.btnCancel).click(hideDialog);
    }

    function selected() {
        hideDialog();
        var newData = [];
        $.each(gridDataStore.data.items, function (i, item) {
            newData.push(item.data);
        });

        var data = {
            names: newData
        };

        callWebService('/Orion/NPM/Services/MulticastRouting.asmx/SetGroupsNames', data, self);
    }

    function callWebService(url, params, self) {
        var paramString = JSON.stringify(params);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: url,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                self.Updated();
            }
        });
    }

    function hideDialog() {
        $dialogElement.dialog('close');
    }

    function created() {
        if (config.onCreated && jQuery.isFunction(config.onCreated)) {
            config.onCreated(self);
        }
    }

    function isIEVersion7andLess() {
        if (jQuery.browser.msie) {
            if (parseFloat(jQuery.browser.version) < 8) {

                return true;
            }
        }
        return false;
    }

    var initializeGridDataStore = function () {
        gridDataStore = new ORION.WebServiceStore(
            config.groups.url,
            [
                { name: 'GroupIPAddress', mapping: 0 },
                { name: 'GroupName', mapping: 1 }
            ],
            'GroupIPAddress'
        );

        setGetGroupsRequest();
        gridDataStore.load();
    };

    var setGetGroupsRequest = function () {
        var request = _.extend(config.requestParams, {});

        gridDataStore.proxy.conn.jsonData = {
            request: request
        };
    };

    var createAndInitializeDataUIControls = function () {
        gridItems = new Ext.grid.EditorGridPanel({
            id: 'groupGridItems',
            region: 'center',
            store: gridDataStore,
            viewConfig: { forceFit: true },
            columns: [{
                id: 'groupIPAddress',
                header: 'Group IP Address',
                dataIndex: 'GroupIPAddress',
                width: 200
            }, {
                header: 'Name',
                dataIndex: 'GroupName',
                width: 271,
                editor: new Ext.form.TextField({
                    allowBlank: true
                }),
                renderer: 'htmlEncode'
            }],
            renderTo: config.renderTo,
            autoScroll: true,
            enableColumnResize: true,
            enableColumnHide: false,
            clicksToEdit: 1,
            height: config.dialog.height - 100,
            width: config.dialog.width - 25
        });
    };

    //PUBLIC methods
    this.Updated = function () {
        if (config.onUpdated && jQuery.isFunction(config.onUpdated)) {
            config.onUpdated(self);
        }
    };


    this.Render = function () {
        initializeGridDataStore();
        createAndInitializeDataUIControls();
    };

    /* 
    * Render group names and show it in dialog.
    */
    this.ShowInDialog = function () {

        if (!rendered) {
            self.Render();
            rendered = true;
        }
        else {
            gridDataStore.load();
        }

        $dialogElement.parent().removeClass("groupNamesOffPage");
        $dialogElement.dialog({
            width: config.dialog.width,
            height: config.dialog.height,
            title: config.dialog.title,
            modal: true
        });
    };

    created();
};