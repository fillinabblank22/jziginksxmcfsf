﻿if (typeof (SW) == "undefined") {
    SW = {};
}

if (typeof (SW.NPM) == "undefined") {
    SW.NPM = {};
}

SW.NPM.ListOfVrfs = {
    SetupListOfVrfs: function (resourceName) {
        this.setupResourceInternal(resourceName, "GetAllVrfs", "GetAllVrfsCount", "GetVrfNodes", 0);
    },

    SetupListOfVrfsOnNode: function (resourceName, nodeId) {
        this.setupResourceInternal(resourceName, "GetListOfVrfsOnNode", "GetVrfOnNodeCount", "GetVrfInterfaces", nodeId);
    },
    
    setupResourceInternal: function (resourceName, getAllService, getTotalCountService, getItemsService, nodeId) {
        var _nodeId = nodeId;
        var totalRows = 0;
        var subTreeItems = 10;
        var PageManager = function (pageIndex, pageSize) {
            this.currentPageIndex = pageIndex;
            this.rowsPerPage = pageSize;
            this.totalRowsCount = 0;
            this.startItem = function () {
                return (this.rowsPerPage * this.currentPageIndex) + 1;
            };
            this.lastItem = function () {
                // Ask for rowsPerPage + 1 rows so we can detect if this is the last page.
                return (this.rowsPerPage * (this.currentPageIndex + 1)) + 1;
            };
            this.numberOfPages = function () {
                return Math.ceil(this.totalRowsCount / this.rowsPerPage);
            };
            this.isLastPage = function (rowCount) {
                // We asked for rowsPerPage + 1 rows. If more than rowsPerPage rows
                // got returned, we know this isn't the last page.
                return rowCount <= this.rowsPerPage;
            };
        };
        var getPager = function (uniqueId) {
            return $('#Pager-' + uniqueId);
        };

        var getCurrentPageSize = function (uniqueId) {
            var pager = getPager(uniqueId);
            var pageSize = pager.find('.pageSize').val();
            if (typeof (pageSize) === "undefined" || pageSize == '' || pageSize <= 0) {
                pageSize = 1;
            }
            return pageSize;
        };

        var updatePagerControls = function (uniqueId, pageManager, rowCount) {
            var pager = getPager(uniqueId);
            var pageIndex = pageManager.currentPageIndex;
            var html = [];
            var showAllText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_SHOWALL;E=js}'; //Show all
            var displayingObjectsText = '@{R=Core.Strings;K=WEBJS_AK0_54;E=js}'; //Displaying objects {0} - {1} of {2}
            var pageXofYText = '@{R=Core.Strings;K=WEBJS_JT0_2;E=js}'; //Page {0} of {1}
            var itemsOnPageText = '@{R=Core.Strings;K=WEBJS_JT0_3;E=js}'; // Items on page
            var style = 'style="vertical-align:middle"';
            var firstImgRoot = '/Orion/images/Arrows/button_white_paging_first';
            var previousImgRoot = '/Orion/images/Arrows/button_white_paging_previous';
            var nextImgRoot = '/Orion/images/Arrows/button_white_paging_next';
            var lastImgRoot = '/Orion/images/Arrows/button_white_paging_last';
            var showAll = showAllText;
            var haveLinks = false;
            var startHtml;
            var endHtml;
            var contents;
            if (pageIndex > 0) {
                startHtml = '<a href="#" class="firstPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', firstImgRoot, style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<a href="#" class="previousPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', previousImgRoot, style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                haveLinks = true;
            } else {
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', firstImgRoot, style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', previousImgRoot, style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
            }
            startHtml = String.format(pageXofYText, '<input type="text" class="pageNumber SmallInput" value="' + (pageManager.currentPageIndex + 1) + '" />', pageManager.numberOfPages());
            html.push(startHtml);
            html.push(' | ');
            if (!pageManager.isLastPage(rowCount)) {
                startHtml = '<a href="#" class="nextPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', nextImgRoot, style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<a href="#" class="lastPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', lastImgRoot, style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                haveLinks = true;
            } else {
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', nextImgRoot, style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', lastImgRoot, style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
            }
            contents = itemsOnPageText;
            endHtml = '<input type="text" class="pageSize SmallInput" value="' + pageManager.rowsPerPage + '" />';
            html.push(contents + endHtml);
            html.push(' | ');
            html.push('<a href="#" class="showAll NoTip">' + showAll + '</a>');
            html.push(' | ');
            html.push('<div class="ResourcePagerInfo">');
            startHtml = String.format(displayingObjectsText, pageManager.startItem(), Math.min(pageManager.lastItem() - 1, pageManager.totalRowsCount), pageManager.totalRowsCount);
            html.push(startHtml);
            html.push('</div>');
            pager.empty().append(html.join(' '));
            var method = haveLinks ? 'show' : 'hide';
            pager[method]();
            pager.find('.firstPage').click(function () {
                createTableFromQuery(uniqueId, 0, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.previousPage').click(function () {
                createTableFromQuery(uniqueId, pageIndex - 1, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.nextPage').click(function () {
                createTableFromQuery(uniqueId, pageIndex + 1, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.lastPage').click(function () {
                createTableFromQuery(uniqueId, pageManager.numberOfPages() - 1, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.showAll').click(function () {
                // We don't have a good way to show all. We'll show 1 million and
                // accept that there's an issue if there are more than that :)
                createTableFromQuery(uniqueId, 0, 1000000);
                return false;
            });
            var changePageSize = function () {
                createTableFromQuery(uniqueId, 0, getCurrentPageSize(uniqueId));
            };
            pager.find('.pageSize').change(function () {
                changePageSize();
            });
            pager.find('.pageSize').keydown(function (e) {
                if (e.keyCode == 13) {
                    changePageSize();
                    return false;
                }
                return true;
            });
            var changePageNumber = function () {
                var pageNumber = pager.find('.pageNumber').val();
                if (pageNumber <= 0) {
                    pageNumber = 1;
                } else if (pageNumber > pageManager.numberOfPages()) {
                    pageNumber = pageManager.numberOfPages();
                }
                createTableFromQuery(uniqueId, pageNumber - 1, pageManager.rowsPerPage);
            };
            pager.find('.pageNumber').change(function () {
                changePageNumber();
            });
            pager.find('.pageNumber').keyup(function (e) {
                if (e.keyCode == 13) {
                    changePageNumber();
                    return false;
                }
                return true;
            });
        };

        var toggleExpanderImage = function (button, showCb, hideCb) {
            //Get the row where the expander resides.
            var currentItem = button.parent().parent().parent().next();
            if (button.attr('src').indexOf('Expand') != -1) {
                button.attr('src', '/Orion/Images/Button.Collapse.gif');
                showCb(currentItem);
            } else {
                button.attr('src', '/Orion/Images/Button.Expand.gif');
                hideCb(currentItem);
            }
        };

        var showRows = function (result, currentItem) {
            var readMoreRow = currentItem.find('.showMoreRow');
            var rowsToOutput = result.slice(0, subTreeItems);
            $.each(rowsToOutput, function (index, value) {
                var source = $('#listOfVrfsRowTemplate-' + resourceName).html();
                var newHtml = _.template(source, { item: value });
                readMoreRow.before(newHtml);
            });
            currentItem.data('startFrom', currentItem.data('startFrom') + subTreeItems);
            if (result.length > subTreeItems) {
                readMoreRow.show();
            } else {
                readMoreRow.hide();
            }
        };

        var callGetItemsService = function (currentItem, callback) {
            var startFrom = currentItem.data('startFrom');
            var rowId = currentItem.data('rowId');
            currentItem.find('.loadingRow').show();
            SW.Core.Services.callWebService(
                "/Orion/NPM/Services/Vrf.asmx",
                getItemsService,
                {
                    rowId: rowId,
                    startFrom: startFrom,
                },
                callback
            );
        };

        var showCallback = function (currentItem) {
            currentItem.show('fade', 200);
            if (!currentItem.data('loaded')) {
                currentItem.data('startFrom', 1);
                callGetItemsService(currentItem, (function (result) {
                    currentItem.data('loaded', true);
                    currentItem.find('.loadingRow').hide();
                    showRows(result, currentItem);
                }));
                currentItem.find('.showMoreLink a').click(function () {
                    callGetItemsService(currentItem, (function (result) {
                        currentItem.find('.loadingRow').hide();
                        showRows(result, currentItem);
                    }));
                    return false;
                });
            }
        };

        var hideCallback = function (currentItem) {
            currentItem.hide('fade', 200);
        };
        
        var processList = function (parentId) {
            $('#Content-' + parentId).find('.vrfAccordion').each(function() {
                $(this).find('.vrfExpander').click(function() {
                    toggleExpanderImage($(this), showCallback, hideCallback);
                });
            });
        };
        
        var createTableFromQuery = function (uniqueId, pageIndex, pageSize) {
            // 0 means to keep whatever page size is set in page size text box
            if (pageSize == 0) {
                pageSize = getCurrentPageSize(uniqueId);
            }
            var pageManager = new PageManager(pageIndex, pageSize);

            var processResults = function (result) {
                pageManager.totalRowsCount = totalRows;
                var rowsToOutput = result.slice(0, pageManager.rowsPerPage);
                var source = $('#listOfVrfsTemplate-' + resourceName).html();
                var newHtml = _.template(source, { vrfs: rowsToOutput });
                $('#Content-' + resourceName).html(newHtml);
                processList(resourceName);
                updatePagerControls(uniqueId, pageManager, result.length);
            };

            SW.Core.Services.callWebService(
                "/Orion/NPM/Services/Vrf.asmx",
                getAllService,
                {
                    startItem: pageManager.startItem(),
                    lastItem: pageManager.lastItem(),
                    nodeId: _nodeId
                },
                processResults
            );
        };

        var createTable = function (result) {
            totalRows = result;
            createTableFromQuery(resourceName, 0, 10);
        };
        var refresh = function () {
            SW.Core.Services.callWebService(
                "/Orion/NPM/Services/Vrf.asmx",
                getTotalCountService,
                {
                    nodeId: _nodeId
                },
                createTable
            );
        };
        SW.Core.View.AddOnRefresh(refresh, "Content-" + resourceName);
        refresh();
    }
};
