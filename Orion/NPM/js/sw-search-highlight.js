﻿SW = SW || {};
SW.NPM = SW.NPM || {};
SW.NPM.Highlighting = SW.NPM.Highlighting || {};

(function (obj) {

	obj.Highlight = function fnHighlight(sContainer, sTerm){
		var oContainer = document.getElementById(sContainer);
		var aNodes = oContainer.childNodes;
		fnClearHighlight(aNodes);
		if (!sTerm || sTerm === "") return;
		fnHighlightInner(aNodes, sTerm);
	};
	
	obj.HighlightQuery = function fnHighlightQuery(sXpathQuery, sTerm){
		var aNodes = $(sXpathQuery);
		fnClearHighlight(aNodes);
		if (!sTerm || sTerm === "") return;
		fnHighlightInner(aNodes, sTerm);	
	};
	
	function fnHighlightInner(aNodes, sTerm) {
		for (var i = 0, iLen = aNodes.length; i < iLen; i++){
			var oNode = aNodes[i];
			if (oNode.nodeType === 3) {
				// is a text node, let's find out if it has what we want
				var sNodeValue = oNode.nodeValue;
				var sNodeValueLow = sNodeValue.toLowerCase();
				if (sNodeValueLow.indexOf(sTerm.toLowerCase()) >= 0) {
					var oSpan = document.createElement("span");
					oNode.parentNode.replaceChild(oSpan, oNode);
					var iIndex;
					while ((iIndex = sNodeValueLow.indexOf(sTerm.toLowerCase())) !== -1){	
						oSpan.appendChild(document.createTextNode(sNodeValue.substr(0, iIndex)));
						oSpan.appendChild(fnWrapTerm(sNodeValue.substr(iIndex,sTerm.length)));
						sNodeValue = sNodeValue.substr(iIndex + sTerm.length);
						sNodeValueLow = sNodeValueLow.substr(iIndex + sTerm.length);
					}
					oSpan.appendChild(document.createTextNode(sNodeValue));
				}
			}
			else {
				// not a text node, let's dig deeper
				fnHighlightInner(oNode.childNodes, sTerm);
			}
		}
		
		function fnWrapTerm(sTermToHighlight){
			var oWrapper = document.createElement("span");
			oWrapper.setAttribute("class","");
			oWrapper.attributes["class"].value = "sw-highlighted";
			oWrapper.appendChild(document.createTextNode(sTermToHighlight));
			return oWrapper;
		}
	}
	
	function fnClearHighlight(aNodes){
		for (var i = 0; i < aNodes.length; i++){
			var oNode = aNodes[i];
			if (oNode.attributes && oNode.attributes["class"] && oNode.attributes["class"].value === "sw-highlighted") {
				oNode.parentNode.parentNode.replaceChild(document.createTextNode(oNode.parentNode.innerHTML.replace(/<[^>]+>/g, "")), oNode.parentNode);
				return;
			}
			else if (oNode.nodeType !== 3) {
				fnClearHighlight(oNode.childNodes);
			}
		}
	}
})(SW.NPM.Highlighting);