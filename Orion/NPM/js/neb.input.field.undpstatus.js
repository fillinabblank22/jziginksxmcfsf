﻿SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Field").UnDPStatus = function (config) {

    // CONSTANTS
    var DEFAULT_CONFIG = {
        enabledWhenEmpty: false,
        dataTypeFilter: 0
    };

    // PUBLIC METHODS
    this.getExpr = function () {
        return {
            Child: null,
            NodeType: 1,
            Value: labelHolder.attr("data-value")
        };
    };

    this.validate = function () {
		return true;
    };

    this.setFilter = function (masterField) {
    };

    // PRIVATE METHODS
    var showItems = function () {
        if (!expr.Value) {
            labelHolder.attr("data-value", options.globalOptions.undpValueType);
        }
        else {
            labelHolder.attr("data-value", expr.Value);
        }
        setTimeout(propagateSet, 0);
    };

    var propagateSet = function () {
        if (typeof options.onSet === 'function') {
            options.onSet({
                DataTypeInfo: {
                    DeclType: labelHolder.attr("data-value") == "Status" ? 5 : 4
                    // 4 : FLOAT
                    // 5 : TEXT
                }
            });
        }
    };

    // CONSTRUCTOR
    var self = this;
    var options = $.extend({}, DEFAULT_CONFIG, config);
    var expr = options.expr;

    var labelHolder = $("<span data-value=''>@{R=NPM.Strings;K=NPMWEBJS_LH0_4;E=js}</span>");
    options.renderTo.append(labelHolder);

    showItems();

    return this;
};