﻿(function () {

    var svg;

    var variables = {};

    var sizeConstants = {
        rectWidth: 140,
        rectHeight: 30,
        lineSize: 40,
        fontSize: 10,
        imgRoleWidth: 24,
        imgRoleHeight: 24,
        imgStatusWidth: 24,
        imgStatusHeight: 24
    }

    var createVariables = function (resourceId, resourceConfig, data) {
        var countOfSwitchesOnLine = getCountOfSwitchesOnLine(resourceId, data.length);

        var vars = {
            resourceId: resourceId,
            resourceConfig: resourceConfig,
            data: data,
            countOfSwitchesOnLine: countOfSwitchesOnLine,
            countOfSwitchLines: getCountOfSwitchLines(countOfSwitchesOnLine, data.length),
            X: data.length == 1 ? 0 : getShiftX(countOfSwitchesOnLine), // power stack may have only one member
            Y: 0,
            posX: 0,
            posY: 0,
            direction: 'right'
        };
        variables[resourceId] = vars;
    }

    var updateVariables = function (resourceId) {
        var vars = variables[resourceId];
        var countOfSwitchesOnLine = getCountOfSwitchesOnLine(resourceId, vars.data.length);

        var vars = {
            resourceId: resourceId,
            resourceConfig: vars.resourceConfig,
            data: vars.data,
            countOfSwitchesOnLine: countOfSwitchesOnLine,
            countOfSwitchLines: getCountOfSwitchLines(countOfSwitchesOnLine, vars.data.length),
            X: vars.data.length == 1 ? 0 : getShiftX(countOfSwitchesOnLine), // power stack may have only one member
            Y: 0,
            posX: 0,
            posY: 0,
            direction: 'right'
        };
        variables[resourceId] = vars;
    }

    var getShiftX = function (countOfSwitchesOnLine) {
        return countOfSwitchesOnLine == 1 ? sizeConstants.lineSize / 2 + sizeConstants.imgStatusWidth / 2 : 0;
    }

    var calculateXY = function (resourceId) {
        var vars = variables[resourceId];
        switch (vars.direction) {
            case 'right':
                if (vars.countOfSwitchesOnLine > vars.posX) {
                    if (vars.posX != 0) {
                        vars.X = vars.X + sizeConstants.lineSize + sizeConstants.rectWidth;
                    }

                    vars.posX++;
                    if (vars.countOfSwitchesOnLine == vars.posX) {
                        vars.direction = 'down';
                        vars.posY++;
                    }
                }
                break;
            case 'down':
                if (vars.countOfSwitchLines > vars.posY) {

                    vars.Y = vars.Y + sizeConstants.lineSize + sizeConstants.rectHeight;

                    vars.posY++;
                    if (vars.countOfSwitchLines == vars.posY) {
                        vars.direction = 'left';
                        vars.posX--;
                    }
                }
                break;
            case 'up':
                if (vars.posY > 0) {
                    vars.Y = vars.Y - sizeConstants.lineSize - sizeConstants.rectHeight;

                    vars.posY--;
                    if (vars.posY == 0) {
                        vars.direction = 'right';
                        vars.posX--;
                    }
                }
                break;
            case 'left':
                if (vars.posX > 0) {
                    vars.X = vars.X - sizeConstants.lineSize - sizeConstants.rectWidth;

                    vars.posX--;
                    if (vars.posX == 0) {
                        vars.posY++;
                        vars.direction = 'up';
                    }
                }
                break;
        }
    }

    var getContentContainer = function (resourceId) {
        return $('#Content-' + resourceId);
    }

    var getResourceWidth = function (resourceId) {
        var parentDiv = getContentContainer(resourceId);
        var resourceContent = parentDiv.closest('.ResourceContent');
        var resourceWrapper = resourceContent.closest('.ResourceWrapper');
        return resourceWrapper.width() - parseInt(resourceContent.css('padding-left')) - parseInt(resourceContent.css('padding-right')) - parseInt(parentDiv.css('padding-left')) - parseInt(parentDiv.css('padding-right'));
    }

    var getCountOfSwitchesOnLine = function (resourceId, membersLength) {
        var resourceWidth = getResourceWidth(resourceId);

        if (membersLength == 1) {
            // power stack may have only one member
            return 1;
        }

        if (resourceWidth < (sizeConstants.rectWidth + sizeConstants.lineSize / 2 + sizeConstants.imgStatusWidth / 2)) {
            return 1;
        }
        else {
            if (resourceWidth < (sizeConstants.rectWidth + sizeConstants.lineSize + sizeConstants.rectWidth)) {
                return 1;
            }
            else {
                var count = 2;
                var width = sizeConstants.rectWidth;
                for (var i = count; i <= membersLength; i++) {
                    width = width + sizeConstants.lineSize + sizeConstants.rectWidth;
                    if (resourceWidth < width)
                        break;

                    count = i;
                }
                return count;
            }
        }
    }

    var getCountOfSwitchLines = function (countOfSwitchesOnLine, membersLength) {
        if (countOfSwitchesOnLine == 1) {
            return membersLength;
        }
        else {
            var fullSwitchLines = 2;
            var countOfSwitchesInOrphanedLine = 2;
            var orphanedLines = (membersLength - countOfSwitchesOnLine * fullSwitchLines) / countOfSwitchesInOrphanedLine;
            if (orphanedLines > 0) {
                return Math.ceil(orphanedLines) + fullSwitchLines;
            }
            else {
                return Math.ceil(membersLength / countOfSwitchesOnLine);
            }
        }
    }

    var getSvgWidth = function (resourceId) {
        var vars = variables[resourceId];
        if (vars.data.length == 1) {
            // power stack may have only one member
            return sizeConstants.rectWidth;
        }

        var countOfSwitchesOnLine = vars.countOfSwitchesOnLine;
        if (countOfSwitchesOnLine == 1) {
            return sizeConstants.rectWidth + sizeConstants.lineSize / 2 + sizeConstants.imgStatusWidth / 2;
        }
        else {
            var width = sizeConstants.rectWidth + sizeConstants.lineSize + sizeConstants.rectWidth;
            for (var i = 2; i < countOfSwitchesOnLine; i++) {
                width = width + sizeConstants.lineSize + sizeConstants.rectWidth;
            }
            return width;
        }
    }

    var getSvgHeight = function (resourceId) {
        var vars = variables[resourceId];
        if (vars.data.length == 1) {
            // power stack may have only one member
            return sizeConstants.rectHeight;
        }

        var countOfSwitchLines = vars.countOfSwitchLines;
        if (countOfSwitchLines == 1) {
            return sizeConstants.rectHeight + sizeConstants.lineSize / 2 + sizeConstants.imgStatusHeight / 2;
        }
        else {
            var height = sizeConstants.rectHeight + sizeConstants.lineSize + sizeConstants.rectHeight;
            for (var i = 2; i < countOfSwitchLines; i++) {
                height = height + sizeConstants.lineSize + sizeConstants.rectHeight;
            }
            return height;
        }
    }

    var prepareSvgContainer = function (resourceId) {
        svg = d3.select('#SwitchStackRing-' + resourceId)
            .attr('width', getSvgWidth(resourceId))
            .attr('height', getSvgHeight(resourceId));

        var rectLayer = svg.select('.sw-switch-stack-rect-layer').remove();
        if (!rectLayer.empty()) {
            rectLayer.remove();
        }

        var connLayer = svg.select('.sw-switch-stack-conn-layer').remove();
        if (!connLayer.empty()) {
            connLayer.remove();
        }
    }

    var createRectLayer = function () {
        var rectLayer = svg.select('.sw-switch-stack-rect-layer');
        if (rectLayer.empty()) {
            rectLayer = svg.append('g')
                            .attr('class', 'sw-switch-stack-rect-layer');
        }
        return rectLayer;
    }

    var renderSwitch = function (resourceId, member) {

        calculateXY(resourceId);

        var rectLayer = createRectLayer();

        var vars = variables[resourceId];
        var g = rectLayer.append('g')
                    .attr('id', String.format('sw-switch-stack-{0}-{1}', resourceId, member.MemberID))
                    .attr('x', vars.X)
                    .attr('y', vars.Y)
                    .attr('direction', vars.direction)
                    .attr('transform', function () { return String.format('translate({0},{1})', vars.X, vars.Y); });

        g.append('rect')
            .attr('class', 'sw-switch-stack-rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', sizeConstants.rectWidth)
            .attr('height', sizeConstants.rectHeight);

        g.append('text')
            .attr('x', sizeConstants.rectWidth / 2)
            .attr('y', (sizeConstants.rectHeight + sizeConstants.fontSize) / 2)
            .attr('text-anchor', 'middle')
            .attr('alignment-baseline', 'middle')
            .text(member.Name);

        var href = getRoleHref(member.Role, vars.resourceConfig);
        if (href != '') {
            g.append('rect')
                .attr('class', 'sw-switch-stack-role-rect')
                .attr('x', 0)
                .attr('y', 0)
                .attr('width', sizeConstants.rectHeight)
                .attr('height', sizeConstants.rectHeight);

            g.append('image')
                .attr('x', (sizeConstants.rectHeight - sizeConstants.imgRoleWidth) / 2)
                .attr('y', (sizeConstants.rectHeight - sizeConstants.imgRoleHeight) / 2)
                .attr('width', sizeConstants.imgRoleWidth)
                .attr('height', sizeConstants.imgRoleHeight)
                .attr('xlink:href', href)
                    .append('title')
                    .text(getRoleTitle(member.Role, vars.resourceConfig));
        }
    }

    var renderSwitches = function (resourceId, data) {
        for (var i = 0; i < data.length; i++) {
            renderSwitch(resourceId, data[i]);
        }
    }

    var getRoleTitle = function (role, resourceConfig) {
        switch (role) {
            case 1: // Master
                return resourceConfig.role.master.title;
            case 4: // Standby
                return resourceConfig.role.standby.title;
            default:
                return '';
        }
    }

    var getRoleHref = function (role, resourceConfig) {
        switch (role) {
            case 1: // Master
                return resourceConfig.role.master.href;
            case 4: // Standby
                return resourceConfig.role.standby.href;
            default:
                return '';
        }
    }

    var getStatusTitle = function (status, resourceConfig) {
        switch (status) {
            case 1: // Up
                return resourceConfig.status.up.title;
            case 2: // Down
            case 3:
                return resourceConfig.status.down.title;
            default:
                return '';
        }
    }

    var getStatusHref = function (status, resourceConfig) {
        switch (status) {
            case 1: // Up
                return resourceConfig.status.up.href;
            case 2: // Down
            case 3:
                return resourceConfig.status.down.href;
            default:
                return '';
        }
    }

    var createConnectionLayer = function () {
        var connectionLayer = svg.select('.sw-switch-stack-conn-layer');
        if (connectionLayer.empty()) {
            connectionLayer = svg.append('g')
                                    .attr('class', 'sw-switch-stack-conn-layer');
        }
        return connectionLayer;
    }

    var renderConnectionLine = function (x1, y1, x2, y2) {
        var connectionLayer = createConnectionLayer();
        connectionLayer.append('line')
                        .attr('class', 'sw-switch-stack-line')
                        .attr('x1', x1)
                        .attr('y1', y1)
                        .attr('x2', x2)
                        .attr('y2', y2);
    }

    var renderConnectionPolyline = function (points) {
        var connectionLayer = createConnectionLayer();
        connectionLayer.append('polyline')
                        .attr('class', 'sw-switch-stack-line')
                        .attr('points', points);
    }

    var renderConnectionStatus = function (x1, y1, href, title) {
        var connectionLayer = createConnectionLayer();
        if (href != '') {
            connectionLayer.append('image')
                            .attr('x', x1)
                            .attr('y', y1)
                            .attr('width', sizeConstants.imgStatusWidth)
                            .attr('height', sizeConstants.imgStatusHeight)
                            .attr('xlink:href', href)
                                .append('title')
                                .text(title);
        }
    }

    var createConnectionVariables = function (resourceId, connection) {
        var vars = {};

        var memberId = String.format('#sw-switch-stack-{0}-{1}', resourceId, connection.MemberID);
        var neighborMemberId = String.format('#sw-switch-stack-{0}-{1}', resourceId, connection.NeighborMemberID);

        vars.direction = svg.select(memberId).attr('direction');
        vars.x1 = parseInt(svg.select(memberId).attr('x'));
        vars.y1 = parseInt(svg.select(memberId).attr('y'));
        vars.x2 = parseInt(svg.select(neighborMemberId).attr('x'));
        vars.y2 = parseInt(svg.select(neighborMemberId).attr('y'));

        return vars;
    }

    var renderConnections = function (resourceId, data) {
        if (data.length == 1) {
            // power stack may have only one member - skip render connections
            return;
        }

        for (var i = 0; i < data.length; i++) {
            var conn = data[i];
            var vars = createConnectionVariables(resourceId, conn);

            var href = getStatusHref(conn.Status, variables[resourceId].resourceConfig);
            var title = getStatusTitle(conn.Status, variables[resourceId].resourceConfig);

            switch (vars.direction) {
                case 'right':
                    renderConnectionLine(vars.x1 + sizeConstants.rectWidth, vars.y1 + sizeConstants.rectHeight / 2, vars.x1 + sizeConstants.rectWidth + sizeConstants.lineSize, vars.y1 + sizeConstants.rectHeight / 2);
                    renderConnectionStatus(vars.x1 + sizeConstants.rectWidth + sizeConstants.lineSize / 2 - sizeConstants.imgStatusWidth / 2, vars.y1 + sizeConstants.rectHeight / 2 - sizeConstants.imgStatusHeight / 2, href, title);
                    break;
                case 'down':
                    if (i == (data.length - 1)) {
                        renderConnectionPolyline(function () {
                            return String.format('{0},{1} {2},{3} {4},{5} {6},{7}',
                                vars.x1 + sizeConstants.rectWidth / 2,
                                vars.y1 + sizeConstants.rectHeight,
                                vars.x1 + sizeConstants.rectWidth / 2,
                                vars.y1 + sizeConstants.rectHeight + sizeConstants.lineSize / 2,
                                vars.x2 + sizeConstants.rectWidth / 2,
                                vars.y1 + sizeConstants.rectHeight + sizeConstants.lineSize / 2,
                                vars.x2 + sizeConstants.rectWidth / 2,
                                vars.y2 + sizeConstants.rectHeight
                            );
                        });
                        renderConnectionStatus((vars.x1 + sizeConstants.rectWidth / 2 - vars.x2 + sizeConstants.rectWidth / 2) / 2 - sizeConstants.imgStatusWidth / 2, vars.y1 + sizeConstants.rectHeight + sizeConstants.lineSize / 2 - sizeConstants.imgStatusHeight / 2, href, title);
                    }
                    else {
                        renderConnectionLine(vars.x1 + sizeConstants.rectWidth / 2, vars.y1 + sizeConstants.rectHeight, vars.x1 + sizeConstants.rectWidth / 2, vars.y2);
                        renderConnectionStatus(vars.x1 + sizeConstants.rectWidth / 2 - sizeConstants.imgStatusWidth / 2, vars.y2 - sizeConstants.lineSize / 2 - sizeConstants.imgStatusHeight / 2, href, title);
                    }
                    break;
                case 'left':
                    if (i == (data.length - 1) && variables[resourceId].countOfSwitchesOnLine == 1) {
                        renderConnectionPolyline(function () {
                            return String.format('{0},{1} {2},{3} {4},{5} {6},{7}',
                                vars.x1,
                                vars.y1 + sizeConstants.rectHeight / 2,
                                vars.x1 - sizeConstants.lineSize / 2,
                                vars.y1 + sizeConstants.rectHeight / 2,
                                vars.x1 - sizeConstants.lineSize / 2,
                                vars.y2 + sizeConstants.rectHeight / 2,
                                vars.x1,
                                vars.y2 + sizeConstants.rectHeight / 2
                            );
                        });
                        renderConnectionStatus(vars.x1 - sizeConstants.lineSize / 2 - sizeConstants.imgStatusWidth / 2, (vars.y1 - vars.y2 + sizeConstants.rectHeight) / 2 - sizeConstants.imgStatusHeight / 2, href, title);
                    }
                    else if (i == (data.length - 1)) {
                        renderConnectionPolyline(function () {
                            return String.format('{0},{1} {2},{3} {4},{5}',
                                vars.x1,
                                vars.y1 + sizeConstants.rectHeight / 2,
                                vars.x2 + sizeConstants.rectWidth / 2,
                                vars.y1 + sizeConstants.rectHeight / 2,
                                vars.x2 + sizeConstants.rectWidth / 2,
                                vars.y2 + sizeConstants.rectHeight
                            );
                        });
                        renderConnectionStatus((vars.x1 - vars.x2 + sizeConstants.rectWidth / 2) / 2 - sizeConstants.imgStatusWidth / 2, vars.y1 + sizeConstants.rectHeight / 2 - sizeConstants.imgStatusHeight / 2, href, title);
                    }
                    else {
                        renderConnectionLine(vars.x1, vars.y1 + sizeConstants.rectHeight / 2, vars.x2 + sizeConstants.rectWidth, vars.y1 + sizeConstants.rectHeight / 2);
                        renderConnectionStatus(vars.x1 - sizeConstants.lineSize / 2 - sizeConstants.imgStatusWidth / 2, vars.y1 + sizeConstants.rectHeight / 2 - sizeConstants.imgStatusHeight / 2, href, title);
                    }
                    break;
                case 'up':
                    renderConnectionLine(vars.x1 + sizeConstants.rectWidth / 2, vars.y1, vars.x2 + sizeConstants.rectWidth / 2, vars.y2 + sizeConstants.rectHeight);
                    renderConnectionStatus(vars.x1 + sizeConstants.rectWidth / 2 - sizeConstants.imgStatusWidth / 2, vars.y1 - (vars.y1 - vars.y2 - sizeConstants.rectHeight) / 2 - sizeConstants.imgStatusHeight / 2, href, title);
                    break;
            }
        }
    }

    var showLoading = function (resourceId) {
        var container = getContentContainer(resourceId);
        container.removeClass('sw-switch-stack-content-bg');
        $('#Loading-' + resourceId).show();
    }

    var hideContent = function (resourceId) {
        if ($('#SwitchStackTable-' + resourceId).length) {
            $('#SwitchStackTable-' + resourceId).hide();
        }
        $('#SwitchStackRing-' + resourceId).hide();
    }

    var hideLoading = function (resourceId) {
        var container = getContentContainer(resourceId);
        container.addClass('sw-switch-stack-content-bg');
        $('#Loading-' + resourceId).hide();
    }

    var showContent = function (resourceId) {
        if ($('#SwitchStackTable-' + resourceId).length) {
            $('#SwitchStackTable-' + resourceId).show();
        }
        $('#SwitchStackRing-' + resourceId).show();
    }

    var hideContentContainer = function (resourceId) {
        var container = getContentContainer(resourceId);
        container.hide();
    }

    var hideError = function (resourceId, error) {
        $('#Error-' + resourceId).hide();
    }

    var showError = function (resourceId, error) {
        var errorContainer = $('#Error-' + resourceId);
        errorContainer.show();
        errorContainer.html(String.format('<div class="sw-suggestion sw-suggestion-fail"><div class="sw-suggestion-icon"></div>{0}</div>', error));
    }

    var showWarn = function (error) {
        var errorContainer = $('#Error-' + resourceId);
        errorContainer.show();
        errorContainer.html(String.format('<div class="sw-suggestion sw-suggestion-warn"><div class="sw-suggestion-icon"></div>{0}</div>', error));
    }

    var renderInternal = function (ResourceId, ServiceConfig, ResourceConfig, jsRenderHeaderTableFunc) {

        hideContent(ResourceId);
        hideError(ResourceId);
        showLoading(ResourceId);

        SW.Core.Services.callWebService(ServiceConfig.url, ServiceConfig.methodName, ServiceConfig.methodParams,
            function (data) {
                if (data != null) {
                    createVariables(ResourceId, ResourceConfig, data);
                    prepareSvgContainer(ResourceId);
                    if (jsRenderHeaderTableFunc != undefined) {
                        jsRenderHeaderTableFunc(data, ResourceId);
                    }
                    renderSwitches(ResourceId, data);
                    renderConnections(ResourceId, data);
                    hideLoading(ResourceId);
                    showContent(ResourceId);
                }
                else {
                    hideContentContainer(ResourceId);
                    showWarn(ResourceId, '@{R=NPM.Strings;K=WEBJS_VK0_1;E=js}');
                }
            },
            function (error) {
                hideContentContainer(ResourceId);
                showError(ResourceId, error);
            });
    }

    SW.Core.namespace("SW.NPM").SwitchStackRing = {
        render: function (ResourceId, ServiceConfig, ResourceConfig, jsRenderHeaderTableFunc) {
            
            var resize = function () {
                var vars = variables[ResourceId];
                if (vars != null && vars.data != null) {
                    updateVariables(ResourceId);
                    prepareSvgContainer(ResourceId);
                    renderSwitches(ResourceId, vars.data);
                    renderConnections(ResourceId, vars.data);
                }
            }

            var localRender = function () {
                renderInternal(ResourceId, ServiceConfig, ResourceConfig, jsRenderHeaderTableFunc);
            }

            $('#Content-' + ResourceId).closest('.ResourceWrapper').bind('resized', function () {
                resize();
            });

            SW.Core.View.AddOnRefresh(localRender, "Content-" + ResourceId);
            localRender();
        }
    }
})();