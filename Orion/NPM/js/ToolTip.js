﻿function DoAlert() {
    alert();
}

function GetTooltipHtml() {
    var value = '<span'
    + ' onmousemove="Tooltip_Show(event, $(this).next(\'div\'));"'
    + ' onmouseout="Tooltip_Hide($(this).next(\'div\'));">'
    + '<img src="/orion/npm/images/Tooltip/Ninja_icon_16.png" />';    
    
    value += '@{R=NPM.Strings;K=WEBJS_ZB0_5;E=js}';
    value += '</span>';
    
    value += '<div class="Tooltip" style="display: none;">';
    value += '  <div class="TooltipArrow"></div>';
    value += '  <div class="TooltipBody sw-suggestion">';
    value += '    <div class="TooltipIcon sw-suggestion"></div>';
    value += '    <div class="TooltipTitle">@{R=NPM.Strings;K=WEBJS_ZB0_6;E=js}</div>';
    value += '    <div class="TooltipSubTitle">@{R=NPM.Strings;K=WEBJS_ZB0_5;E=js}</div>';
    value += '    <div class="TooltipText">@{R=NPM.Strings;K=WEBJS_ZB0_7;E=js}</div>';
    value += '    <div class="TooltipFooter">@{R=NPM.Strings;K=WEBJS_ZB0_8;E=js}</div>';
    value += '  </div>';
    value += '</div>';

    return value;
}

function Tooltip_Show(e, tooltipDiv) {
    if (!e)
        e = window.event;

    var tooltipHeight = $(tooltipDiv).height();
    var tooltipWidth = $(tooltipDiv).width();
    var linkPosition = $(tooltipDiv).parents('a').offset();
    var linkWidth = $(tooltipDiv).parents('a').width();

    $(tooltipDiv).css('top', linkPosition.top - tooltipHeight + 62);
    $(tooltipDiv).css('left', linkPosition.left + linkWidth);
    $(tooltipDiv).children('.TooltipArrow').css('top', tooltipHeight - 80);

    $(tooltipDiv).fadeIn("fast");
}
function Tooltip_Hide(tooltipDiv) {
    $(tooltipDiv).fadeOut("fast");
}
