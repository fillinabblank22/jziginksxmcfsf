using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Common;
using System.Collections.Generic;

public partial class Orion_NPM_VSANDetails : OrionView
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected SolarWinds.NPM.Common.Models.VSAN VSAN;
    protected int NodeID;

	protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);
        
        var vsan = (SolarWinds.NPM.Web.VSAN)this.NetObject;
        this.VSAN = GetVSAN(vsan);
        string title = this.ViewInfo.ViewTitle;
        if (this.ViewInfo.IsSubView)
            title = title.Replace(this.ViewInfo.ViewGroupName, string.Format("{0} - {1} (ID {2})", this.ViewInfo.ViewGroupName, this.VSAN.Name, this.VSAN.ID));
        else
            title = string.Format("{0} - {1} (ID {2})", this.ViewInfo.ViewTitle, this.VSAN.Name, this.VSAN.ID);

        this.Title = title;
        this.ifHost.VSAN = vsan;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
	}

    private SolarWinds.NPM.Common.Models.VSAN GetVSAN(SolarWinds.NPM.Web.VSAN _vsan)
    {
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            SolarWinds.NPM.Common.Models.VSAN vsan;
            try
            {
                vsan = proxy.Api.GetVSAN(_vsan.ID);
            }
            catch(Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            if (vsan == null)
                throw new IndexOutOfRangeException(string.Format(NPMWebContent.NPMWEBCODE_LF0_1, _vsan.ID));
            
            return vsan;
        }
    }

	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	protected override ViewInfo GetViewForDeviceType()
	{
		return base.GetViewForDeviceType();
	}

	public override string ViewType
	{
		get { return "VSANDetails"; }
	}

    protected void NPMSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new SolarWinds.NPM.Web.UI.NPMSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("NetObject", Request["NetObject"] ?? string.Empty));
            NPMSiteMapPath.SetUpRenderer(renderer);
        }
    }

}
