using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_InterfaceUtilization : BaseResourceControl
{
	protected void InterfaceList_Init(object sender, EventArgs e)
	{
		InterfaceList.DataSource = ((INodeProvider)Page).Node.GetInterfaces();
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_181; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
