using System;
using System.Web;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using SettingsDAL=SolarWinds.Orion.Web.DAL.SettingsDAL;
using SolarWinds.Orion.NPM.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Helpers;

public partial class CustomPollerChart : CustomPollerGraphResource
{
	private static readonly Log log = new Log();
	public void MyBusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	private bool _isError;
	private bool isSimplePoller = false;
    
	NetObject netObject = null;

	public static string _customPollerValue;
	public string DisplayName;
	public string _customPollerID;
	public string _chartRows;
	public string NetObjectID;
	public string ChartTitle = "";
	public string ChartSubTitle = "";
	public string ChartSubTitle2 = "";
	public string DisplayChartTitle = "";
	public string Period;
	public string SampleSize;
	public int SampleSizeInMinutes;
	public DateTime StartTime;
	public DateTime EndTime;

	public bool ShowDataTable;
	public String SubsetColor;
	public String RYSubsetColor;
	public String PlotStyle;
	public String TimeUnit;
	public String FontSize;
	private CustomPoller _customPoller;
	private ResourceInfo _resInfo;

    //True if we are configuring a multi source chart. In that case, we use different page for chart display
    private bool _isMultiSourceChart = false;

    private ResourceInfo resInfo;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		//dataTableBelowChartDDL.Items.Add(new ListItem("Yes", "0"));
		//dataTableBelowChartDDL.Items.Add(new ListItem("No", "1"));

		if (Profile.AllowCustomize && !string.IsNullOrEmpty(Request.Params["ResourceID"]))
		{
			btnSave.Visible = true;
		}
		else
		{
			btnSave.Visible = false;
		    customPollerControl.HidePoller = true;
		}

		string chartName = Request.Params["ChartName"];
		switch (Request.Params["NetObjectPrefix"])
		{
			case "N":
				NetObject netObj = NetObjectFactory.Create(Request.Params["NetObject"]);
				NetObjectID = "N:" + netObj["NodeID"].ToString();
				break;
			case "S":
				NetObjectID = string.Empty;
				break;
			default:
				NetObjectID = Request.Params["NetObject"];
				break;
		}
		
		switch (NetObjectHelper.GetNetObjectType(NetObjectID))
		{
			case NetObjectHelper.Node:
				if (chartName == "CustomOIDCounterComparison")
				{
					customPollerControl.InitNodeInterfaceCounterCustomPollerList(Convert.ToInt32(NetObjectHelper.GetObjectID(NetObjectID)));
				}
				else
				{
					customPollerControl.InitNodeInterfaceCustomPollerList(Convert.ToInt32(NetObjectHelper.GetObjectID(NetObjectID)));
				}
				break;
			case NetObjectHelper.Interface:
				if (chartName == "CustomOIDCounterComparison")
				{
					customPollerControl.InitInterfaceCounterCustomPollerList(Convert.ToInt32(NetObjectHelper.GetObjectID(NetObjectID)));
				}
				else
				{
					customPollerControl.InitInterfaceCustomPollerList(Convert.ToInt32(NetObjectHelper.GetObjectID(NetObjectID)));
				}
				break;
			default:
				if (chartName == "CustomOIDSummaryCounterComparison")
				{
					customPollerControl.InitSummaryCounterCustomPollerList();
				}
				else
				{
					customPollerControl.InitSummaryCustomPollerList();
				}
				break;
		}

		if (!IsPostBack && String.IsNullOrEmpty(Request.Params["ResourceID"]))
		{
			customPollerControl.CustomPollerID = Request.Params["CustomPollerID"];
		}

        resInfo = new ResourceInfo();
        if (!string.IsNullOrEmpty(Request["ResourceID"]))
        {
            resInfo.ID = Convert.ToInt32(Request["ResourceID"]);
        }
        _resInfo = resInfo;

        _isMultiSourceChart = !String.IsNullOrEmpty(_resInfo.Properties["EntityName"]);


        if (_isMultiSourceChart)
        {
            customPollerControl.Visible = false;
            showTrend.Visible = false;
        }

	}

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);

		CustomPoller customPoller = null;
		
		
		switch (Request.Params["NetObjectPrefix"])
		{
			case "N":
				NetObject netObj = NetObjectFactory.Create(Request.Params["NetObject"]);
				NetObjectID = "N:" + netObj["NodeID"].ToString();
				break;
			case "S":
				NetObjectID = string.Empty;
				break;
			default:
				NetObjectID = Request.Params["NetObject"];
				break;
		}

		if (!string.IsNullOrEmpty(NetObjectID))
		{
			netObject = NetObjectFactory.Create(NetObjectID);
		}
		      
        // Gets Parameters from the Request
		if (!Page.IsPostBack)
		{
			_customPollerID = Request["CustomPollerID"];
			customPollerControl.CustomPollerID = _customPollerID;

            string rowProperty ="Rows";
			_chartRows = (string.IsNullOrEmpty(Request[rowProperty])) ? resInfo.Properties[string.Format("{0}:{1}", NetObjectID ,rowProperty)] : Request[rowProperty].ToString();
			customPollerControl.CustomPollerRows = _chartRows;

			DisplayChartTitle = ChartTitle = chartTitle.Text = (string.IsNullOrEmpty(Request.Params["Title"])) ? resInfo.Properties["Title"] : WebSecurityHelper.SanitizeHtml(Request.Params["Title"].ToString());
			ChartSubTitle = chartSubTitle.Text = (string.IsNullOrEmpty(Request.Params["SubTitle"])) ? resInfo.Properties["SubTitle"] : WebSecurityHelper.SanitizeHtml(Request.Params["SubTitle"].ToString());
			ChartSubTitle2 = chartSubTitle2.Text = (string.IsNullOrEmpty(Request.Params["SubTitle2"])) ? resInfo.Properties["SubTitle2"] : WebSecurityHelper.SanitizeHtml(Request.Params["SubTitle2"].ToString());

			if (!string.IsNullOrEmpty(Request.Params["FontSize"]))
			{
				FontSize = Request.Params["FontSize"];
			}
		}
		else
		{
            //HACK. Do thie properly when time permits.
            //Do not update the _customPollerID from the control, if the control is invisible
            if (customPollerControl.Visible)
            {
                _customPollerID = customPollerControl.CustomPollerID;
            }
            else
            {
                _customPollerID = Request["CustomPollerID"];
            }

			if (_customPollerValue == _customPollerID)
			{
				_chartRows = customPollerControl.CustomPollerRows;
			}
			else
			{
				_chartRows = string.Empty;
			}
			FontSize = fontSizeDDL.SelectedValue;
            DisplayChartTitle = ChartTitle = WebSecurityHelper.SanitizeHtml(chartTitle.Text);
			ChartSubTitle = WebSecurityHelper.SanitizeHtml(chartSubTitle.Text);
			ChartSubTitle2 = WebSecurityHelper.SanitizeHtml(chartSubTitle2.Text);
		}

		_customPollerValue = _customPollerID;


        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            if (!string.IsNullOrEmpty(_customPollerID))
            {
                try
                {
                    customPoller = proxy.Api.GetCustomPoller(new Guid(_customPollerID));
                }
                catch(Exception ex)
                {
                    MyBusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            if (customPoller != null && customPoller.SNMPGetType != "GetSubTree")
            {
                isSimplePoller = true;
            }

            if (String.IsNullOrEmpty(DisplayChartTitle))
            {
                if (netObject != null)
                {
                    if (netObject.ObjectProperties.ContainsKey("FullName"))
                    {
                        DisplayChartTitle = netObject["FullName"].ToString();
                    }
                    else if (netObject.ObjectProperties.ContainsKey("Caption"))
                    {
                        DisplayChartTitle = netObject["Caption"].ToString();
                    }
                }
                else if (string.IsNullOrEmpty(resInfo.Properties["Title"]))
                {
                    if (customPoller != null)
                    {
                        DisplayChartTitle = customPoller.UniqueName;
                    }
                    else
                    {
                        DisplayChartTitle = Request["Title"];
                    }
                }
                else
                {
                    DisplayChartTitle = resInfo.Properties["Title"];
                }
            }
        }

		DisplayName = HttpUtility.HtmlEncode(Request["ChartName"]);
		
		chartWidth.Text = Width.ToString();
		chartHeight.Text = Height.ToString();

		if (!IsPostBack)
		{
			bool boolValue;
			if (!string.IsNullOrEmpty(Request["ShowTrend"]))
			{
				showTrend.Checked = (bool.TryParse(Request["ShowTrend"].ToString(), out boolValue)) ? boolValue : false;
			}
			else
			{
				showTrend.Checked = (resInfo != null) ? (bool.TryParse(resInfo.Properties["ShowTrend"], out boolValue) ? boolValue : true) : true;
			}
		}

		//chartSettings.FontSize = Request["FontSize"];
		if (!string.IsNullOrEmpty(Request["SampleSize"]) && !IsPostBack)
		{
			sampleSizeControl.SampleSizeValue = SampleSize = Request["SampleSize"];
		}
		else
		{
			SampleSize = sampleSizeControl.SampleSizeValue;
		}

		if (timePeriodControl.TimePeriodText == "Custom")
		{
			if (string.IsNullOrEmpty(timePeriodControl.CustomPeriodBegin))
			{
				timePeriodControl.CustomPeriodBegin = "0:00:00";
			}
			if (string.IsNullOrEmpty(timePeriodControl.CustomPeriodEnd))
			{
				timePeriodControl.CustomPeriodEnd = "23:59:59";
			}

            Period = timePeriodControl.CustomPeriodBegin + "~" + timePeriodControl.CustomPeriodEnd;
        }
        else
        {
            Period = timePeriodControl.TimePeriodText;
        }
		timePeriodControl.AttachSampleSizeControl(sampleSizeControl.SampleSizeListControl);
		
		int fontSize = 0;

		if (String.IsNullOrEmpty(FontSize))
		{
			OrionSetting setting = SettingsDAL.GetSetting("Web-ChartFontSize"); // ,1
			fontSize = Convert.ToInt32(setting.SettingValue);
			fontSizeDDL.SelectedValue = fontSize.ToString();
		}
		else
		{
			fontSize = Convert.ToInt32(FontSize);
			fontSizeDDL.SelectedValue = fontSize.ToString();
		}

		_customPoller = customPoller;
		
		this.Page.Title = string.Format(Resources.NPMWebContent.NPMWEBCODE_TM0_35, DisplayChartTitle);

        AddTopRightLinks();
        locButtonExcel.DataBind();
        locButtonChart.DataBind();
	}

	// Returns a URL for Generation of a report with the Chart data
	public String GetDataURL() {
		String url = @"/Orion/NetPerfMon/ChartData.aspx?ChartName=" + DisplayName + "&NetObject=" + NetObjectID;
        if (NetObjectID != null) {
            url += "&CustomPollerID=" + _customPollerID + "&Rows=" + ((isSimplePoller) ? String.Empty : _chartRows);
		}
		DateTime periodBegin = new DateTime();
		DateTime periodEnd = new DateTime();

		if (Period != "Custom")
		{
			url += "&Period=" + Period;

			Periods.Parse(ref Period, ref periodBegin, ref periodEnd);
		}
		else
		{
			periodBegin = Convert.ToDateTime(timePeriodControl.CustomPeriodBegin);
			periodEnd = Convert.ToDateTime(timePeriodControl.CustomPeriodEnd);
		}

		url += "&PeriodBegin=" + periodBegin.ToString();
		url += "&PeriodEnd=" + periodEnd.ToString();
		url += "&SampleSize=" + SampleSize;
		if (!String.IsNullOrEmpty(ChartTitle))
		{
			url += "&Title=" + Server.HtmlEncode(ChartTitle);
		}
		if (!String.IsNullOrEmpty(ChartSubTitle))
		{
			url += "&SubTitle=" + Server.HtmlEncode(ChartSubTitle);
		}
		if (!String.IsNullOrEmpty(ChartSubTitle2))
		{
			url += "&SubTitle2=" + Server.HtmlEncode(ChartSubTitle2);
		}
		//if (chartSettings.Footer.Length > 0) {
		//    url += "&Footer=" + Server.HtmlEncode(chartSettings.Footer);
		//}
		//if (chartSettings.Footer2.Length > 0) {
		//    url += "&Footer2=" + Server.HtmlEncode(chartSettings.Footer2);
		//}

        if (!string.IsNullOrEmpty(Request["ResourceID"]))
		{
			ResourceInfo resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request["ResourceID"]));
			

            
            string chartEntity = resource.Properties["EntityName"];
            string showSum = resource.Properties["ShowSum"];
            string dataColumnName = resource.Properties["DataColumnName"];
            string topXX = resource.Properties["TopXX"];
            bool filterEntities = String.IsNullOrEmpty(resource.Properties["FilterEntities"]) ? false : Boolean.Parse(resource.Properties["FilterEntities"]); 
            string selectedEntities = resource.Properties["SelectedEntities"];
		    string resourceId = WebSecurityHelper.SanitizeHtmlV2(Request["ResourceID"]);

            //If ChartEntity is not empty, we are mutlisource chart
            //If selectedentities is empty then, we are multisource chart with "automatic population" so we need to get data from this method
            if (!string.IsNullOrEmpty(chartEntity))
            {
                if (string.IsNullOrEmpty(selectedEntities))
                {
                    selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(chartEntity, NetObjectID, _customPollerID, selectedEntities, topXX);
                }
                else
                {
                    if (filterEntities)
                    {
                        // filter by netobject and topxx
                        selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(chartEntity, NetObjectID, _customPollerID, selectedEntities, topXX);
                    }
                    else
                    {
                        // Only filter by topXX
                        selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(chartEntity, null, _customPollerID, selectedEntities, topXX);
                    }
                }
            }

            url += string.Format("&SelectedEntities={0}&ChartEntity={1}&ShowSum={2}&DataColumnName={3}&ResourceID={4}",
                                 selectedEntities, chartEntity, showSum, dataColumnName, resourceId);
		}

        
		return url;
	}

	protected void RefreshClick(object sender, EventArgs e)
	{
        //if there will be any code, uncomment this line
        if (!Page.IsValid)
			return;
		Response.Redirect(GetURL());
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
        if (!Page.IsValid)
			return;

		using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
		{
			if (!string.IsNullOrEmpty(customPollerControl.CustomPollerID))
			{
                CustomPoller poller;
                try
                {
                    poller = proxy.Api.GetCustomPoller(new Guid(customPollerControl.CustomPollerID));
                }
                catch(Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
				if (poller.SNMPGetType == "GetSubTree" && !customPollerControl.IsValid)
				{
					IsError = true;
					return;
				}
			}
		}

		ResourceInfo resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request["ResourceID"]));

        //Only save CustomPOllerID if the control is actually visible. Currently, this affects multicharts (who hide it as it's redundant)
		if (!string.IsNullOrEmpty(customPollerControl.CustomPollerID) && customPollerControl.Visible)
		{
			resource.Properties.Remove("CustomPollerID");
			resource.Properties.Add("CustomPollerID", customPollerControl.CustomPollerID);

            string rowProperty = NetObjectID + ":Rows";

            resource.Properties.Remove(rowProperty);
            resource.Properties.Add(rowProperty, customPollerControl.CustomPollerRows);
		}
		resource.Properties.Remove("Period");
		if (timePeriodControl.TimePeriodText != "Custom")
		{
			resource.Properties.Add("Period", timePeriodControl.TimePeriodText);
		}
		else
		{
			resource.Properties.Add("Period", Convert.ToDateTime(timePeriodControl.CustomPeriodBegin, System.Globalization.CultureInfo.CurrentCulture).Ticks.ToString() +
				"~" + Convert.ToDateTime(timePeriodControl.CustomPeriodEnd, System.Globalization.CultureInfo.CurrentCulture).Ticks.ToString());
		}

		resource.Properties.Remove("SampleSize");
		resource.Properties.Add("SampleSize", sampleSizeControl.SampleSizeValue);

		resource.Properties.Remove("ShowTrend");
		resource.Properties.Add("ShowTrend", showTrend.Checked.ToString());

		resource.Properties.Remove("Title");
		resource.Properties.Add("Title", ChartTitle);

		resource.Properties.Remove("SubTitle");
		resource.Properties.Add("SubTitle", ChartSubTitle);

		resource.Properties.Remove("SubTitle2");
		resource.Properties.Add("SubTitle2", ChartSubTitle2);

		string url = string.Format("/Orion/View.aspx?ViewID={0}", resource.View.ViewID);
		if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
		{
			string netObjectViewType = NetObjectFactory.GetViewTypeForNetObject(Request.QueryString["NetObject"]);

			if (netObjectViewType.Equals(resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase))
				url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
		}

		ReferrerRedirectorBase.Return(url);
		//Response.Redirect(url);
	}

	protected int Width
	{
		get
		{
			string value;
			if (!IsPostBack)
			{
				value = Request["Width"];
			}
			else
			{
				value = chartWidth.Text;
			}

			int width = 0;
			if (Int32.TryParse(value, out width) && width > 0)
			{
				return width;
			}
			else
			{
				return 640;
			}
		}
	}

	protected int Height
	{
		get
		{
			string value;
			if (!IsPostBack)
			{
				value = Request["Height"];
			}
			else
			{
				value = chartHeight.Text;
			}

			int height = 0;
			if (Int32.TryParse(value, out height) && height > 0)
			{
				return height;
			}
			else
			{
				return 0;
			}
		}
	}

	protected String GetPrintableVersionURL()
	{
        return GetFormattedURL("/Orion/Netperfmon/PrintableCustomChart.aspx");
	}

	protected String GetChartImageURL()
	{
        /*
        if (_isMultiSourceChart)
        {
            return GetFormattedURL("/Orion/NetPerfMon/MultiChart.aspx");
        }
        else
        {
            
        }
         * */
        return GetFormattedURL("/Orion/Netperfmon/Chart.aspx");

	}

	private String GetFormattedURL(String page)
	{
		string subsetColor = string.Empty;
		string rySubsetColor = string.Empty;

        string selectedEntities = string.Empty;
        string chartEntity = string.Empty;
        string showSum = string.Empty;
        string dataColumnName = string.Empty;

        string multiSourceString = string.Empty;

		if (!string.IsNullOrEmpty(Request["ResourceID"]))
		{
			ResourceInfo resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request["ResourceID"]));
			subsetColor = resource.Properties["SubsetColor"];
			rySubsetColor = resource.Properties["RYSubsetColor"];


            selectedEntities = resource.Properties["SelectedEntities"];
            chartEntity = resource.Properties["EntityName"];
            showSum = resource.Properties["ShowSum"];
            dataColumnName = resource.Properties["DataColumnName"];
            string topXX = resource.Properties["TopXX"];
            bool filterEntities = String.IsNullOrEmpty(resource.Properties["FilterEntities"]) ? false : Boolean.Parse(resource.Properties["FilterEntities"]); 
           
            //If ChartEntity is not empty, we are mutlisource chart
            //If selectedentities is empty then, we are multisource chart with "automatic population" so we need to get data from this method.
            if (!string.IsNullOrEmpty(chartEntity))
            {
                if (string.IsNullOrEmpty(selectedEntities))
                {
                    selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(chartEntity, NetObjectID, _customPollerID, selectedEntities, topXX);
                }
                else
                {
                    if (filterEntities)
                    {
                        // filter by netobject and topxx
                        selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(chartEntity, NetObjectID, _customPollerID, selectedEntities, topXX);
                    }
                    else
                    {
                        // Only filter by topXX
                        selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(chartEntity, null, _customPollerID, selectedEntities, topXX);
                    }
                }
            }
		}

        if (_isMultiSourceChart)
        {
            multiSourceString = string.Format("&SelectedEntities={0}&ChartEntity={1}&ShowSum={2}&DataColumnName={3}",
                                                selectedEntities, chartEntity, showSum, dataColumnName);
        }

        return String.Format("{0}?ChartName={1}&Title={2}&SubTitle={3}&SubTitle2={4}&Width={5}&Height={6}&NetObject={7}&CustomPollerID={8}&Rows={9}&SampleSize={10}&Period={11}&PlotStyle={12}&FontSize={13}&NetObjectPrefix={14}&SubsetColor={15}&RYSubsetColor={16}&Printable=true{17}{18}{19}",
		page,
		DisplayName,
		HttpUtility.UrlEncode(ChartTitle),
		HttpUtility.UrlEncode(ChartSubTitle),
		HttpUtility.UrlEncode(ChartSubTitle2),
		Width,
		Height,
		NetObjectID,
		_customPollerID,
		isSimplePoller ? String.Empty : _chartRows,
		SampleSize,
		Period,
        WebSecurityHelper.SanitizeHtmlV2(Request.Params["PlotStyle"]),
		fontSizeDDL.SelectedValue,
        WebSecurityHelper.SanitizeHtmlV2(Request.Params["NetObjectPrefix"]),
		subsetColor,
		rySubsetColor,
		(!string.IsNullOrEmpty(Request.Params["ResourceID"])) ? "&ResourceID=" + WebSecurityHelper.SanitizeHtmlV2(Request.Params["ResourceID"]) : string.Empty,
		"&ShowTrend=" + showTrend.Checked.ToString(),
        multiSourceString
		);
	}

	private string GetURL()
	{
		string subsetColor = string.Empty;
		string rySubsetColor = string.Empty;
		if (!string.IsNullOrEmpty(Request["ResourceID"]))
		{
			ResourceInfo resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request["ResourceID"]));
			subsetColor = resource.Properties["SubsetColor"];
			rySubsetColor = resource.Properties["RYSubsetColor"];
		}

		return String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName={0}&Title={1}&SubTitle={2}&SubTitle2={3}&Width={4}&Height={5}&NetObject={6}&CustomPollerID={7}&Rows={8}&SampleSize={9}&Period={10}&PlotStyle={11}&FontSize={12}&NetObjectPrefix={13}&SubsetColor={14}&RYSubsetColor={15}{16}{17}{18}",
			DisplayName,
			HttpUtility.UrlEncode(ChartTitle),
			HttpUtility.UrlEncode(ChartSubTitle),
			HttpUtility.UrlEncode(ChartSubTitle2),
			Width,
			Height,
			NetObjectID,
			_customPollerID,
			isSimplePoller ? String.Empty : _chartRows,
			SampleSize,
			Period,
            Request.Params["PlotStyle"],
			fontSizeDDL.SelectedValue,
			Request.Params["NetObjectPrefix"],
			subsetColor,
			rySubsetColor,
			(!string.IsNullOrEmpty(Request["ResourceID"])) ? "&ResourceID=" + Request["ResourceID"] : String.Empty,
			"&ShowTrend=" + showTrend.Checked.ToString(),
			string.IsNullOrEmpty(ReferrerRedirectorBase.GetReferrerUrl()) ? "" : "&ReturnTo=" + ReferrerRedirectorBase.GetReferrerUrl()
			);
	}

	protected override string DefaultTitle
	{
		get { return ""; }
	}

	public bool IsError
	{
		get { return _isError; }
		set { _isError = value; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionPHResourceUniversalDevicePollerChart"; }
	}

    private void AddTopRightLinks()
    {
        if (this.Page.Master == null) return;

        try
        {

            var ctrl = ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinks");
            if (ctrl == null) return;

            ctrl.Controls.Add(new HyperLink
            {
                ID = "Img2",
                NavigateUrl = GetPrintableVersionURL(),
                CssClass = "printablePageLink",
                Text = Resources.NPMWebContent.NPMWEBCODE_TM0_34,
                EnableViewState = false
            });

            ctrl.Controls.Add(new ASP.IconHelpButton
            {
                ID = "helpButton",
                HelpUrlFragment = "OrionPHViewCustomChart",
                EnableViewState = false
            });
        }
        catch (HttpException)
        {
            // -- shouldn't occur --
            // the page may be using this control and incompatible - having control blocks in a content section.
            // if so, this exception will occur. we'd rather default to not having the links show up than see an error page.
            // -- shouldn't occur --
        }
    }
}
