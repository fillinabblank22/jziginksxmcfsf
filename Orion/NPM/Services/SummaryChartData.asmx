﻿<%@ WebService Language="C#" Class="SummaryChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.NPM.Web.ChartingV2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public sealed class SummaryChartData : NPMChartDataWebServiceBase
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private static string GetDateTimeSampled(ChartDataRequest request)
    {
        var interval = request.SampleSizeInMinutes;

        //round to interval in minutes down
        return $"CAST(DATEADD(MINUTE, FLOOR(DATEDIFF(MINUTE, '20000101', [DateTime]) / {interval}) * {interval}, '20000101') AS datetime2)";
    }

    private static void FillViewLimitationInformation(ChartDataRequest request)
    {
        int limitationID = -1;

        if (request.NetObjectIds.Any() && int.TryParse(request.NetObjectIds[0], out limitationID))
        {

            if (System.Web.HttpContext.Current.Items[(object)typeof(ViewInfo).Name] == null)
            {
                System.Web.HttpContext.Current.Items[(object)typeof(ViewInfo).Name] = new ViewInfo("npm", null, null, false,
                                                                                                     false, null)
                {
                    LimitationID = limitationID
                };
            }
        }
    }

    private static string ApplySamplingOnSql(ChartDataRequest request, string query, string sqlFunc, params string[] columns)
    {
        const string queryTemplate = @"
                SELECT {2} as [DateTime],
                {0}
                FROM (
                    {1}
                ) subselect
                GROUP BY {2}
                ORDER BY [DateTime]
                ";

        var rx = new Regex("ORDER BY +[^ \n]+"); // remove order by from inner clause

        return string.Format(queryTemplate,
                                string.Join(",", columns.Select(x => string.Format("{0}({1}) as {1}", sqlFunc, x))),
                                rx.Replace(query, ""),
                                GetDateTimeSampled(request)
            );
    }


    [WebMethod]
    public ChartDataResults NetworkWideAvgRT(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);

        const string sql = @"SELECT rt.DateTime, rt.AvgResponseTime as ResponseTime
                             From Nodes
                            INNER JOIN ResponseTime rt ON Nodes.NodeID=rt.NodeID
                             WHERE 
                                rt.DateTime >= @StartTime AND rt.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);

        var dataSql = ApplySamplingOnSql(request, sql, "AVG", "ResponseTime");
        _log.Debug($"Final query for NetworkWideAvgRT: {dataSql}");

        var data = DoSimpleLoadData(request, dateRange, dataSql,
            "", DateTimeKind.Local, "ResponseTime", null,
                                    SampleMethod.Average, false);

        var series = ComputeTrendsFromSampledData(request, dateRange, data.First(), SampleMethod.Average);

        var result = new ChartDataResults(data.Concat(series));
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(request, dynamicResult);
    }

    [WebMethod]
    public ChartDataResults CustomPollerSummaryChart(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);

        const string sql = @"SELECT ps.DateTime, ps.RawStatus AS Poller
                             From CustomPollerStatistics ps
                             INNER JOIN CustomPollerAssignment pa ON pa.CustomPollerAssignmentID = ps.CustomPollerAssignmentID
                             WHERE 
                                ps.DateTime >= @StartTime AND ps.DateTime <= @EndTime AND
                                pa.CustomPollerID='{0}'
                             ORDER BY [DateTime]";

        Guid g = GetResourceCustomPollerID(request);
        if (g == Guid.Empty)
            return new ChartDataResults(new DataSeries[0]);

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var dataSql = ApplySamplingOnSql(request, string.Format(sql, g), "AVG", "Poller");
        _log.Debug($"Final query for CustomPollerSummaryChart: {dataSql}");

        var data = DoSimpleLoadData(request, dateRange, dataSql,
                   "", DateTimeKind.Utc, "Poller", null,
                                    SampleMethod.Average, false);

        // we need to fill the label
        var undp = data.First();
        using(var swis = InformationServiceProxy.CreateV3())
        {
            var label = swis.Query("SELECT UniqueName FROM Orion.NPM.CustomPollers WHERE CustomPollerID=@cpid",
                new Dictionary<string, object>
                    {
                        {"cpid", g}
                    }
                );

            if (label.Rows.Count > 0) // if undp doesn't exist, don't crash
                undp.Label = label.Rows[0][0].ToString();
        }

        var series = ComputeTrendsFromSampledData(request, dateRange, undp, SampleMethod.Total);
        var result = new ChartDataResults(data.Concat(series));
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(request, dynamicResult);
    }

    [WebMethod]
    public ChartDataResults CustomPollerMMASummaryChart(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);

        Guid g = GetResourceCustomPollerID(request);
        if (g == Guid.Empty)
            return new ChartDataResults(new DataSeries[0]);

        string sql =
            string.Format(
                @"SELECT {1} as DateTime, AVG(AvgRate), MIN(MinRate), MAX(MaxRate), AVG(RawStatus), MIN(RawStatus), MAX(RawStatus)
                             From CustomPollerStatistics ps
                             INNER JOIN CustomPollerAssignment pa ON pa.CustomPollerAssignmentID = ps.CustomPollerAssignmentID
                             WHERE 
                                ps.DateTime >= @StartTime AND ps.DateTime <= @EndTime AND
                                pa.CustomPollerID=@cpid
                             GROUP BY {1}
                             ORDER BY [DateTime]",
                g, GetDateTimeSampled(request));


        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);
        var limitedSql = Limitation.LimitSQL(sql);
        List<DataSeries> allSeries = new List<DataSeries>();

        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);

        var parameters = new[]
                            {
                                new SqlParameter("StartTime", dateRange.StartDate),
                                new SqlParameter("EndTime", dateRange.EndDate),
                                new SqlParameter("cpid", g)
                            };

        _log.Debug($"Final query for CustomPollerMMASummaryChart: {limitedSql}");

        List<DataSeries> rawSeries = GetData(limitedSql, parameters, DateTimeKind.Utc, "", "PollerAvg", "min", "max", "PollerAvg", "min", "max");

        var rawAverage = rawSeries[0];
        var rawMin = rawSeries[1];
        var rawMax = rawSeries[2];

        var rawStatus = rawSeries[3];
        var rawrMin = rawSeries[4];
        var rawrMax = rawSeries[5];


        var sampledAverage = rawAverage.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Average);
        DataSeries sampledMin;
        DataSeries sampledMax;

        if (sampledAverage.Data.Any(x => !x.IsNullPoint)) // behave the same way how old charts behaved
        {
            sampledMin = rawMin.CreateSampledSeries(dateRange, dynamicSampleSize,
                                                    SampleMethod.Min);
            sampledMax = rawMax.CreateSampledSeries(dateRange, dynamicSampleSize,
                                                    SampleMethod.Max);

        }
        else
        {
            sampledAverage = rawStatus.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Average);
            sampledMin = rawrMin.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Min);
            sampledMax = rawrMax.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Max);
        }
        var sampledMinMax = DataSeries.CombineMinMaxSeries(sampledMin, sampledMax, "PollerMinMax");

        var trends = ComputeTrendsFromSampledData(request, dateRange, sampledAverage);
        // we can fetch the label only from proxy

        allSeries.Add(sampledAverage);
        allSeries.AddRange(trends);
        allSeries.Add(sampledMinMax);

        var dynamicResult = dynamicLoader.SetDynamicChartOptions(new ChartDataResults(allSeries));

        return CalculateMinMaxForYAxis(request, dynamicResult);
    }


    [WebMethod]
    public ChartDataResults NetworkWideAvailabilityRT(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);

        const string sql = @"SELECT rt.DateTime, rt.AvgResponseTime As ResponseTime, rt.Availability
                            FROM         Nodes INNER JOIN
                            ResponseTime rt ON Nodes.NodeID = rt.NodeID
                            WHERE 
                                rt.DateTime >= @StartTime AND rt.DateTime <= @EndTime
                            ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);

        var dataSql = ApplySamplingOnSql(request, sql, "AVG", "ResponseTime", "Availability");
        _log.Debug($"Final query for NetworkWideAvailabilityRT: {dataSql}");

        var data = DoSimpleLoadData(request, dateRange, dataSql,
                   "", DateTimeKind.Local, "ResponseTime", "Availability", SampleMethod.Average, false);

        var series = ComputeTrendsFromSampledData(request, dateRange, data.First(), SampleMethod.Average);

        var result = new ChartDataResults(data.Concat(series));
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(request, dynamicResult);
    }

    [WebMethod]
    public ChartDataResults NetworkWideAvailability(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);

        const string sql = @"SELECT rt.DateTime, rt.Availability
                            FROM         Nodes INNER JOIN
                            ResponseTime rt ON Nodes.NodeID = rt.NodeID
                            WHERE 
                                rt.DateTime >= @StartTime AND rt.DateTime <= @EndTime
                            ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);

        var dataSql = ApplySamplingOnSql(request, sql, "AVG", "Availability");
        _log.Debug($"Final query for NetworkWideAvailability: {dataSql}");

        var data = DoSimpleLoadData(request, dateRange, dataSql,
                   "", DateTimeKind.Local, "Availability", null, SampleMethod.Average, false);
        var series = ComputeTrendsFromSampledData(request, dateRange, data.First(), SampleMethod.Average);

        var result = new ChartDataResults(data.Concat(series));
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(request, dynamicResult);
    }


    [WebMethod]
    public ChartDataResults NetworkWideMinMaxAvgRT(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);

        var dateTimeSampled = GetDateTimeSampled(request);

        string sql =$@"
            SELECT {dateTimeSampled} as [DateTime], AVG(rt.AvgResponseTime), MIN(rt.MinResponseTime), MAX(rt.MaxResponseTime)
                FROM Nodes INNER JOIN
                ResponseTime rt  ON Nodes.NodeID=rt.NodeID
                WHERE 
                    rt.DateTime >= @StartTime AND rt.DateTime <= @EndTime
                GROUP BY {dateTimeSampled}
                ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);

        _log.Debug($"Final query for NetworkWideMinMaxAvgRT: {sql}");

        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, "", DateTimeKind.Local, "AvgResponseTime",
                                             "MinMaxResponseTime");

        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(request, dynamicResult);
    }
}
