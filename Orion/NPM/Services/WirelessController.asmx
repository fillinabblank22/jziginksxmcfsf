﻿<%@ WebService Language="C#" Class="WirelessController" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.NPM.Web.UI;
using SolarWinds.NPM.Web.UI.TabularResource;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WirelessController : TabularResourceServiceBase
{

  [WebMethod]
  public string HelloWorld()
  {
    return "Hello World";
  }

  [WebMethod]
  public TabularResourceResult GetClients()
  {
    var tab = new Table();
    tab.CreateStatusColumn();
    tab.CreateNamedColumn("Client name");
    tab.CreateNamedColumn("RSSI");

    TableRow row;
    Random rng = new Random();
    for (int i = 1; i <= 10; i++)
    {
      row = tab.AppendRow();
      row.AddCell(FormatHelper.FormatStatusLedIcon("Up.gif"));
      row.AddCell("Client " + i);
      row.AddCell(-50 + rng.Next(10));
    }

    return new TabularResourceResult
    {
      TotalRows = tab.Rows.Count,
      Data = tab
    };
  }

  [WebMethod]
  public TabularResourceResult GetTopInterference()
  {
    var tab = new Table();
    tab.CreateStatusColumn();
    tab.CreateNamedColumn("Access Point");
    tab.CreateNamedColumn("Channel");
    tab.CreateNamedColumn("Interference");

    TableRow row;
    Random rng = new Random();
    for (int i = 1; i <= 10; i++)
    {
      row = tab.AppendRow();
      row.AddCell(FormatHelper.FormatStatusLedIcon("Up.gif"));
      row.AddCell("Access Point " + i);
      row.AddCell(rng.Next(12));
      row.AddCell(120-10*i);
    }

    return new TabularResourceResult
    {
      TotalRows = tab.Rows.Count,
      Data = tab
    };
  }

}