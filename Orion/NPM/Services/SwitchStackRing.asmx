﻿<%@ WebService Language="C#" Class="SwitchStackRing" %>

using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Services;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using System.Web.Script.Services;
using SolarWinds.NPM.Common.Models.SwitchStack;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SwitchStackRing : WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public object GetSwitchStackDataMembers(int nodeId)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string query = @"
            SELECT 
                m.SwitchNumber,
                m.Role,
                m.SwitchStackMemberPort.MemberID,
                m.SwitchStackMemberPort.NeighborMemberID,
                m.SwitchStackMemberPort.Status
            FROM Orion.NPM.SwitchStackMember m 
            WHERE m.NodeID=@nodeId AND m.SwitchStackMemberPort.MemberID IS NOT NULL
            ORDER BY m.SwitchNumber, m.SwitchStackMemberPort.MemberID, m.SwitchStackMemberPort.NeighborMemberID";

            var table = proxy.Query(query, new Dictionary<string, object> {{"nodeId", nodeId}});

            return GetStackMemberPorts(table);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public object GetSwitchStackPowerMembers(int nodeId, int powerStackId)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var query = string.Format(@"
            SELECT 
	            m.SwitchNumber, 
	            CASE WHEN m.SwitchNumber=p.MasterID THEN {0} ELSE {1} END AS Role, 
	            p.SwitchStackPowerPort.MemberID, 
	            p.SwitchStackPowerPort.NeighborMemberID, 
	            p.SwitchStackPowerPort.LinkStatus AS Status
            FROM Orion.NPM.SwitchStackPower p            
            INNER JOIN Orion.NPM.SwitchStackMember m ON m.NodeID=p.NodeID AND m.MemberID=p.SwitchStackPowerPort.MemberID
            WHERE p.NodeID=@nodeId AND p.PowerStackID=@powerStackId AND p.SwitchStackPowerPort.MemberID IS NOT NULL
            ORDER BY m.SwitchNumber, p.SwitchStackPowerPort.MemberID, p.SwitchStackPowerPort.NeighborMemberID",
            (int)SwitchRoleEnum.Master, (int)SwitchRoleEnum.Member);

            var table = proxy.Query(query, new Dictionary<string, object>
            {
                {"nodeId", nodeId},
                {"powerStackId", powerStackId}
            });
            return GetStackMemberPorts(table);
        }
    }

    private List<SwitchStackMemberPort> GetStackMemberPorts(DataTable table)
    {
        if (table != null && table.Rows.Count > 0)
        {
            var members = RefreshSwitchStackMembersFromDataTable(table);
            if (members.Count == 1) // stack may have only one member
            {
                var member = members[0];
                return new List<SwitchStackMemberPort>
                    {
                        new SwitchStackMemberPort
                        {
                            MemberID = member.MemberID,
                            Name = string.Format(Resources.NPMWebContent.NPMWEBDATA_JP1_17,  member.SwitchNumber),
                            NeighborMemberID = member.NeighborMemberID1,
                            Role = member.Role,
                            Status = member.NeighborMemberID1Status
                        }
                    };
            }

            SubstituteSwitchStackMembers(members);

            var master = GetMasterSwitchStackMember(members);
            var ports = new List<SwitchStackMemberPort>();
            RefreshSwitchStackMemberPorts(members, master, ports);
            if (ports.Count == 0)
                throw new Exception(Resources.NPMWebContent.NPMWEBDATA_VK0_5);

            return ports;
        }

        return null;
    }
    
    private SwitchStackMember GetMasterSwitchStackMember(List<SwitchStackMember> members)
    {
        var master = members.FirstOrDefault(x => x.Role == SwitchRoleEnum.Master); // find master
        return master ?? members[0];
    }

    private void SubstituteSwitchStackMembers(List<SwitchStackMember> members)
    {
        // Case 1: two zero on 1 member - find remaining half zero members and fill connect them with full zero one 
        SwitchStackMember zeroFull = members.FirstOrDefault(x => x.NeighborMemberID1 == 0 && x.NeighborMemberID2 == 0); //can have only one fully with zero 
        if (zeroFull != null)
        {
            List<SwitchStackMember> zeroHalf = members
                .Where(x => x.MemberID != zeroFull.MemberID && (x.NeighborMemberID1 == 0 || x.NeighborMemberID2 == 0))
                .ToList();
            foreach (SwitchStackMember member in zeroHalf)
            {
                SubstituteSwitchStackMemberID(zeroFull, member.MemberID);
                if (member.NeighborMemberID1 != zeroFull.MemberID && member.NeighborMemberID2 != zeroFull.MemberID)
                    SubstituteSwitchStackMemberID(member, zeroFull.MemberID);  //prevent duplication
            }
        }

        // Case 2: substitution - try to find pair for item with one port zero connection in other items and connect them
        List<SwitchStackMember> zeroHalfItems = members.Where(x => x.NeighborMemberID1 == 0 || x.NeighborMemberID2 == 0).ToList();
        foreach (SwitchStackMember member in zeroHalfItems)
        {
            SwitchStackMember x = members
                .Where(k => k.NeighborMemberID1 == member.MemberID || k.NeighborMemberID2 == member.MemberID)
                .FirstOrDefault(z => z.MemberID != member.NeighborMemberID1 && z.MemberID != member.NeighborMemberID2);
            if (x != null)
            {
                SubstituteSwitchStackMemberID(member, x.MemberID);
            }
        }

        // Case 3: fix remaining - if we have only two ports with zero connection left - just connect them together
        List<SwitchStackMember> zeroRemaining = members.Where(x => x.NeighborMemberID1 == 0 || x.NeighborMemberID2 == 0).ToList();
        if (zeroRemaining.Count == 2)
        {
            SubstituteSwitchStackMemberID(zeroRemaining[0], zeroRemaining[1].MemberID);
            SubstituteSwitchStackMemberID(zeroRemaining[1], zeroRemaining[0].MemberID);
        }
    }

    private void SubstituteSwitchStackMemberID(SwitchStackMember member, int memberId)
    {
        if (member.NeighborMemberID1 == 0)
            member.NeighborMemberID1 = memberId;
        else
            member.NeighborMemberID2 = memberId;
    }

    private void RefreshSwitchStackMemberPorts(List<SwitchStackMember> members, SwitchStackMember firstMember, List<SwitchStackMemberPort> ports)
    {
        var port = new SwitchStackMemberPort
        {
            MemberID = firstMember.MemberID,
            Name = string.Format(Resources.NPMWebContent.NPMWEBDATA_JP1_17, firstMember.SwitchNumber),
            Role = firstMember.Role
        };

        int nextMemberId;
        if (ports.Count(x => x.MemberID == firstMember.NeighborMemberID1) == 0)
        {
            nextMemberId = firstMember.NeighborMemberID1;
            port.Status = firstMember.NeighborMemberID1Status;
        }
        else if (ports.Count(x => x.MemberID == firstMember.NeighborMemberID2) == 0)
        {
            nextMemberId = firstMember.NeighborMemberID2;
            port.Status = firstMember.NeighborMemberID2Status;
        }
        else
        {
            // add last member
            nextMemberId = ports[0].MemberID;
            port.Status = firstMember.NeighborMemberID1 == nextMemberId ? firstMember.NeighborMemberID1Status : firstMember.NeighborMemberID2Status;
        }
        port.NeighborMemberID = nextMemberId;

        SwitchStackMember nextMember = members.FirstOrDefault(x => x.MemberID == nextMemberId);
        if (nextMember == null)
        {
            ports.Clear(); // unsuccessful calculation - do not render diagram
            return;
        }

        // calculate port status
        if (nextMember.NeighborMemberID1 == firstMember.MemberID && nextMember.NeighborMemberID1Status != StackPortOperStatusEnum.Up)
            port.Status = nextMember.NeighborMemberID1Status;
        if (nextMember.NeighborMemberID2 == firstMember.MemberID && nextMember.NeighborMemberID2Status != StackPortOperStatusEnum.Up)
            port.Status = nextMember.NeighborMemberID2Status;

        ports.Add(port);

        // hang prevent detection in case we cannot walk by all items
        if (ports.Count() > members.Count())
        {
            ports.Clear(); // unsuccessful calculation - do not render diagram
            return;
        }

        if (ports.Count(x => x.MemberID == nextMember.MemberID) > 0)
        {
            if (members.Count() == 2 && members.Count(x => x.NeighborMemberID1Status == StackPortOperStatusEnum.Up) +
                members.Count(x => x.NeighborMemberID2Status == StackPortOperStatusEnum.Up) >= 2) // special case for two members
            {
                if (ports.Count(x => x.Status == StackPortOperStatusEnum.Up) == 0)
                    ports[0].Status = StackPortOperStatusEnum.Up; // means one of ports is up
            }

            if (ports.Count() != members.Count())
            {
                ports.Clear(); // unsuccessful calculation - do not render diagram
            }
            // successful end
        }
        else
            RefreshSwitchStackMemberPorts(members, nextMember, ports);
    }

    private List<SwitchStackMember> RefreshSwitchStackMembersFromDataTable(DataTable dt)
    {
        var members = new Dictionary<int, SwitchStackMember>();

        foreach (DataRow row in dt.Rows)
        {
            var switchNumber = (byte)row["SwitchNumber"];
            var role = (SwitchRoleEnum)Enum.Parse(typeof(SwitchRoleEnum), row["Role"].ToString());
            var memberId = (int) row["MemberID"];
            var neighborMemberId = (int) row["NeighborMemberID"];
            var neighborMemberStatus = (StackPortOperStatusEnum)Enum.Parse(typeof(StackPortOperStatusEnum), row["Status"].ToString());

            SwitchStackMember member;
            if (members.TryGetValue(switchNumber, out member))
            {
                member.NeighborMemberID2 = neighborMemberId;
                member.NeighborMemberID2Status = neighborMemberStatus;
            }
            else
            {
                member = new SwitchStackMember
                {
                    SwitchNumber = switchNumber,
                    Role = role,
                    MemberID = memberId,
                    NeighborMemberID1 = neighborMemberId,
                    NeighborMemberID1Status = neighborMemberStatus
                };
                members[switchNumber] = member;
            }
        }
        return members.Values.ToList();
    }
    
    private class SwitchStackMember
    {
        public byte SwitchNumber { get; set; }
        public int MemberID { get; set; }
        public SwitchRoleEnum Role { get; set; }
        public int NeighborMemberID1 { get; set; }
        public StackPortOperStatusEnum NeighborMemberID1Status { get; set; }
        public int NeighborMemberID2 { get; set; }
        public StackPortOperStatusEnum NeighborMemberID2Status { get; set; }
    }

    private class SwitchStackMemberPort
    {
        public int MemberID { get; set; }
        public string Name { get; set; }
        public SwitchRoleEnum Role { get; set; }
        public int NeighborMemberID { get; set; }
        public StackPortOperStatusEnum Status { get; set; }
    }
}