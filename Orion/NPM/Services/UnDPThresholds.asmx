﻿<%@ WebService Language="C#" Class="UnDPThresholds" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Xml.Linq;
using System.Web.SessionState;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Common;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.NPM.Common.Models;

/// <summary>
/// Service for managing the UnDP thresholds settings
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class UnDPThresholds : System.Web.Services.WebService
{

    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private const string UNDPDATA = "CustomPollerThresholds";
    private const string LOGKEY = "UnDP Thresholds Settings: ";

    public class CustomPollerThresholdInfo
    {
        public string CustomPollerId { get; set; }
        public string UniqueName { get; set; }
        public string WarningText { get; set; }
        public string CriticalText { get; set; }
        public string WarningNumber { get; set; }
        public string CriticalNumber { get; set; }
        public string ValueType { get; set; }
        public string IconPath { get; set; }
        public bool IsDirty { get; set; }
    }

    /// <summary>
    /// Loads data from SWIS and saves the results to session.
    /// </summary>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public int LoadData()
    {
        try
        {
            Session.Remove(UNDPDATA);
            if (null == Session[UNDPDATA])
            {
                List<CustomPollerThresholdInfo> items = new List<CustomPollerThresholdInfo>();
                DataTable data;
                string valType, warn, crit, warnText, warnNumber, critText, critNumber;

                using (var swis = InformationServiceProxy.CreateV3())
                {
                    data = swis.Query(@"
SELECT cp.CustomPollerId, cp.UniqueName, ToString(cp.NetObjectPrefix) as NetObjectPrefix, ToString(cp.PollerType) as PollerType, cp.SNMPGetType, t.Warning, t.Critical
FROM Orion.NPM.CustomPollers cp
LEFT JOIN Orion.NPM.CustomPollerThresholds t ON cp.CustomPollerId = t.CustomPollerId
ORDER BY cp.UniqueName");
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        warn = row["Warning"].ToString();
                        crit = row["Critical"].ToString();
                        warnText = "";
                        critText = "";
                        warnNumber = "";
                        critNumber = "";
                        valType = GetValueTypeByExpressions(warn, crit);

                        switch (valType)
                        {
                            case CustomPollerStatistic.STR_FIELD:
                                {
                                    warnText = string.IsNullOrEmpty(warn) ? "" : Newtonsoft.Json.JsonConvert.SerializeObject(SerializationHelper.FromXmlString<Expr>(warn));
                                    critText = string.IsNullOrEmpty(crit) ? "" : Newtonsoft.Json.JsonConvert.SerializeObject(SerializationHelper.FromXmlString<Expr>(crit));
                                }
                                break;
                            case CustomPollerStatistic.NUM_FIELD:
                                {
                                    warnNumber = string.IsNullOrEmpty(warn) ? "" : Newtonsoft.Json.JsonConvert.SerializeObject(SerializationHelper.FromXmlString<Expr>(warn));
                                    critNumber = string.IsNullOrEmpty(crit) ? "" : Newtonsoft.Json.JsonConvert.SerializeObject(SerializationHelper.FromXmlString<Expr>(crit));
                                }
                                break;
                            default:
                                break;
                        }

                        items.Add(new CustomPollerThresholdInfo
                        {
                            CustomPollerId = row["CustomPollerId"].ToString(),
                            UniqueName = row["UniqueName"].ToString(),
                            WarningText = warnText,
                            CriticalText = critText,
                            WarningNumber = warnNumber,
                            CriticalNumber = critNumber,
                            ValueType = valType,
                            IconPath = GetPollerIcon(row["NetObjectPrefix"].ToString(), row["PollerType"].ToString(), row["SNMPGetType"].ToString()),
                            IsDirty = false,

                        });
                    }
                    Session[UNDPDATA] = items;
                    return items.Count;
                }
                else return 0;
            }
            else return 0;
        }
        catch (Exception ex)
        {
            _log.DebugFormat("UnDPThresholds: Failed to load and store data in session. Ex: {0}", ex.Message);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int ResetData()
    {
        Session.Remove(UNDPDATA);
        return LoadData();
    }

    [WebMethod(EnableSession = true)]
    public int SaveData()
    {
        AuthorizationChecker.AllowAdmin();
        try
        {                   
            var sessionData = GetSessionData();
            
            var filtered = from f in sessionData
                           where f.IsDirty == true
                           select f;

            if (filtered.Count() > 0)
            {
                string query, warnEx, critEx;
                query = @"
UPDATE dbo.CustomPollerThresholds
SET Warning = @w, Critical = @c
WHERE CustomPollerId = @id
IF @@ROWCOUNT=0
INSERT INTO dbo.CustomPollerThresholds (CustomPollerID, Warning, Critical)
VALUES (@id, @w, @c)";

                foreach (CustomPollerThresholdInfo item in filtered)
                {
                    warnEx = "";
                    critEx = "";

                    switch (item.ValueType)
                    {
                        case CustomPollerStatistic.STR_FIELD:
                            {
                                warnEx = item.WarningText;
                                critEx = item.CriticalText;
                            }
                            break;
                        case CustomPollerStatistic.NUM_FIELD:
                            {
                                warnEx = item.WarningNumber;
                                critEx = item.CriticalNumber;
                            }
                            break;
                        default:
                            break;
                    }
                    if (!string.IsNullOrEmpty(warnEx))
                        warnEx = SerializationHelper.ToXmlString(Newtonsoft.Json.JsonConvert.DeserializeObject<Expr>(warnEx));
                    if (!string.IsNullOrEmpty(critEx))
                        critEx = SerializationHelper.ToXmlString(Newtonsoft.Json.JsonConvert.DeserializeObject<Expr>(critEx));


                    string originalW = "";
                    string originalC = "";
                    string caption = "";
                    loadOriginalThresholdValues(item.CustomPollerId, out originalW, out originalC, out caption);
                    
                    
                    using (var cmd = SqlHelper.GetTextCommand(query))
                    {
                        cmd.Parameters.AddWithValue("id", item.CustomPollerId);
                        cmd.Parameters.AddWithValue("w", warnEx);
                        cmd.Parameters.AddWithValue("c", critEx);
                        SqlHelper.ExecuteScalar(cmd);
                        auditChanges(caption, warnEx, critEx, originalW, originalC);
                    }
                }
                Session.Remove(UNDPDATA);
                LoadData();
                return 1;
            }
            else return 0;
        }
        catch (Exception ex)
        {
            LogException(ex, "Failed to save changes to db.");
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetPollers(string searchValue)
    {
        try
        {
            var sessionData = GetSessionData();
            IEnumerable<CustomPollerThresholdInfo> temp;

            if (string.IsNullOrEmpty(searchValue.Trim())) temp = sessionData;
            else temp = sessionData.Where(x => x.UniqueName.ToLowerInvariant().Contains(searchValue.ToLowerInvariant()));

            var res = from x in temp
                      select new
                      {
                          Id = x.CustomPollerId,
                          Name = x.UniqueName,
                          IconPath = x.IconPath
                      };

            return Newtonsoft.Json.JsonConvert.SerializeObject(res);
        }
        catch (Exception ex)
        {
            LogException(ex, "Unable to retrieve list of pollers from session.");
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetValueTypeById(string customPollerId)
    {
        try
        {
            var sessionData = GetSessionData();
            var item = sessionData.First(x => x.CustomPollerId.Equals(customPollerId));
            if (null == item) throw new Exception("There is no poller matching the provided id.");
            return item.ValueType;
        }
        catch (Exception ex)
        {
            LogException(ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetThresholdExpression(string customPollerId, string valueType, string expressionType)
    {
        try
        {
            Guid pollerGuid;

            if (!Guid.TryParse(customPollerId, out pollerGuid))
                throw new Exception("Wrong parameter - customPollerId has to be a guid as string");
            if (string.IsNullOrEmpty(valueType))
                throw new ArgumentNullException("valueType");
            if (string.IsNullOrEmpty(expressionType))
                throw new ArgumentNullException("expressionType");

            var sessionData = GetSessionData();
            var item = sessionData.First(x => x.CustomPollerId.Equals(customPollerId));
            if (null == item) return null;

            switch (expressionType)
            {
                case "Warning":
                    {
                        //
                        if ((valueType == CustomPollerStatistic.STR_FIELD && string.IsNullOrEmpty(item.WarningText)) ||
                            (valueType == CustomPollerStatistic.NUM_FIELD && string.IsNullOrEmpty(item.WarningNumber)))
                        {
                            return string.Empty;
                        }
                        else
                        {
                            switch (valueType)
                            {
                                case CustomPollerStatistic.STR_FIELD:
                                    {
                                        return item.WarningText;
                                    }
                                case CustomPollerStatistic.NUM_FIELD:
                                    {
                                        return item.WarningNumber;
                                    }
                                default: return string.Empty;
                            }

                        }
                    }
                case "Critical":
                    {
                        if ((valueType == CustomPollerStatistic.STR_FIELD && string.IsNullOrEmpty(item.CriticalText)) ||
                            (valueType == CustomPollerStatistic.NUM_FIELD && string.IsNullOrEmpty(item.CriticalNumber)))
                        {
                            return string.Empty;
                        }
                        else
                        {
                            switch (valueType)
                            {
                                case CustomPollerStatistic.STR_FIELD:
                                    {
                                        return item.CriticalText;
                                    }
                                case CustomPollerStatistic.NUM_FIELD:
                                    {
                                        return item.CriticalNumber;
                                    }
                                default: return string.Empty;
                            }
                        }
                    }
                default: return string.Empty;
            }

        }
        catch (Exception ex)
        {
            LogException(ex, "Unable to retrieve threshold expressions.");
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int SetThresholdExpression(string customPollerId, string valueType, string expressionType, Expr expression)
    {
        AuthorizationChecker.AllowAdmin();
        try
        {
            Guid pollerGuid;

            if (string.IsNullOrEmpty(customPollerId))
                throw new ArgumentNullException("customPollerId");
            if (string.IsNullOrEmpty(valueType))
                throw new ArgumentNullException("valueType");
            if (string.IsNullOrEmpty(expressionType))
                throw new ArgumentNullException("expressionType");
            if (!Guid.TryParse(customPollerId, out pollerGuid))
                throw new Exception("Wrong parameter - customPollerId has to be a guid in string format");

            var sessionData = GetSessionData();
            var item = sessionData.First(x => x.CustomPollerId.Equals(customPollerId));
            if (null == item) return 0;

            var serialized = (null == expression) ? "" : Newtonsoft.Json.JsonConvert.SerializeObject(expression);

            switch (valueType)
            {
                case CustomPollerStatistic.STR_FIELD:
                    {
                        if (expressionType == "Warning") item.WarningText = serialized;
                        if (expressionType == "Critical") item.CriticalText = serialized;
                        item.ValueType = valueType;
                        item.IsDirty = true;
                        return 1;
                    }
                case CustomPollerStatistic.NUM_FIELD:
                    {
                        if (expressionType == "Warning") item.WarningNumber = serialized;
                        if (expressionType == "Critical") item.CriticalNumber = serialized;
                        item.ValueType = valueType;
                        item.IsDirty = true;
                        return 1;
                    }
                default: break;
            }
            return 0;
        }
        catch (Exception ex)
        {
            LogException(ex, "Failed to store threshold expression in session.");
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int SetValueType(string customPollerId, string valueType)
    {
        AuthorizationChecker.AllowAdmin();
        Guid pollerGuid;

        if (string.IsNullOrEmpty(customPollerId))
            throw new ArgumentNullException("customPollerId");
        if (string.IsNullOrEmpty(valueType))
            throw new ArgumentNullException("valueType");
        if (!Guid.TryParse(customPollerId, out pollerGuid))
            throw new Exception("Wrong parameter - customPollerId has to be a guid in string format");

        var sessionData = GetSessionData();
        var item = sessionData.First(x => x.CustomPollerId.Equals(customPollerId));
        if (null == item) return 0;

        switch (valueType)
        {
            case CustomPollerStatistic.STR_FIELD:
                {
                    if (string.IsNullOrEmpty(item.WarningText) && string.IsNullOrEmpty(item.CriticalText)) return 0;
                    item.ValueType = valueType;
                    item.IsDirty = true;
                    return 1;
                }
            case CustomPollerStatistic.NUM_FIELD:
                {
                    if (string.IsNullOrEmpty(item.WarningNumber) && string.IsNullOrEmpty(item.CriticalNumber)) return 0;
                    item.ValueType = valueType;
                    item.IsDirty = true;
                    return 1;
                }
            default: return 0;
        }
    }

    [WebMethod(EnableSession = true)]
    public void Heartbeat()
    {
        try
        {
            Session["UnDPHeartbeat"] = DateTime.Now.ToString();
        }
        catch (Exception ex)
        {
            LogException(ex);
            throw;
        }
    }


    // =============================== HELPERS ================================

    private List<CustomPollerThresholdInfo> GetSessionData()
    {
        try
        {
            if (null != Session[UNDPDATA]) return (List<CustomPollerThresholdInfo>)Session[UNDPDATA];
            else throw new Exception("Failed to get data from session. Call LoadData() first.");
        }
        catch (Exception ex)
        {
            LogException(ex);
            throw;
        }
    }

    private string GetValueTypeByExpressions(string warning, string critical)
    {
        string expression = "";
        string searchTxt = "<NodeType>Field</NodeType><Value>{0}</Value>";

        // check the existence of expressions
        if (string.IsNullOrEmpty(warning))
        {
            if (string.IsNullOrEmpty(critical)) return string.Empty;
            else expression = critical;
        }
        else expression = warning;
        if (string.IsNullOrEmpty(expression)) return string.Empty;

        // there are some, try to find value type
        else
        {
            if (expression.Contains(string.Format(searchTxt, CustomPollerStatistic.NUM_FIELD))) return CustomPollerStatistic.NUM_FIELD;
            else return CustomPollerStatistic.STR_FIELD;
        }
    }

    private string GetPollerIcon(string prefix, string pollerType, string getType)
    {
        // transformation poller
        if (pollerType == "F")
        {
            return "/Orion/NPM/images/UnDP/poller-transformation.png";
        }
        // interface poller
        else if (prefix == "I")
        {
            return "/Orion/NPM/images/UnDP/poller-interface.png";
        }
        // tabular poller
        else if (getType == "GetSubTree")
        {
            return "/Orion/NPM/images/UnDP/poller-tabular.png";
        }
        // default poller icon
        else
        {
            return "/Orion/NPM/images/UnDP/poller-up.png";
        }
    }

    private void LogException(Exception ex, string CustomMessage = "")
    {
        if (!string.IsNullOrEmpty(CustomMessage)) _log.Debug(LOGKEY + CustomMessage);
        _log.Debug(LOGKEY + ex.Message);
    }

    // =============================== AUDITING ================================

    private void loadOriginalThresholdValues(string customPollerId, out string warn, out string critical, out string caption)
    {
        warn = "";
        critical = "";
        caption = "";

        var swisParameters = new Dictionary<string, object> { { "customPollerId", customPollerId } };

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var data = swis.Query(@"SELECT  Warning, Critical FROM Orion.NPM.CustomPollerThresholds WHERE CustomPollerID = @customPollerId",
                       swisParameters);

            foreach (DataRow row in data.Rows)
            {
                warn = (string)row[0];
                critical = (string)row[1];
            }
        }

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var data = swis.Query(@"SELECT UniqueName FROM Orion.NPM.CustomPollers WHERE CustomPollerID = @customPollerId",
                       swisParameters);

            foreach (DataRow row in data.Rows)
            {
                caption = (string)row[0];
            }
        }
    }

    private void auditChanges(string caption, string warnEx, string critEx, string old_warnEx, string old_critEx)
    {
        bool c = critEx != old_critEx;
        bool w = warnEx != old_warnEx;
        if (c || w)
        {
            var ind = new SolarWinds.NPM.Indications.NPMThresholdIndication(SolarWinds.Orion.Core.Common.Indications.IndicationType.System_InstanceModified, caption, w, c);
            SolarWinds.Orion.Core.Common.IndicationPublisher.CreateV3().ReportIndication(ind);
        }
    }

}
