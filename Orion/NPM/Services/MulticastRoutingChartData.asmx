﻿<%@ WebService Language="C#" Class="MulticastRoutingChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.Logging;
using SolarWinds.NPM.Web.ChartingV2;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using NpmCharts = SolarWinds.NPM.Web.ChartingV2.Model;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class MulticastRoutingChartData : NPMChartDataWebServiceBase
{
    private static readonly Log _log = new Log();
    private const DateTimeKind dateFormatInDatabase = DateTimeKind.Utc;

    [WebMethod]
    public ChartDataResults TrafficByGroups(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var originalDateRange = new DateRange { StartDate = dateRange.StartDate, EndDate = dateRange.EndDate };
        dateRange = SetCustomDateRangeBasedOnRequest(request, dateRange, dateFormatInDatabase);
        var bucketDateRange = dynamicLoader.GetSamplingDateRange(dateRange, originalDateRange);

        const string sql =
            @"SELECT gn.NodeID, gn.MulticastGroupNodeID, h.LastChangeUTC, h.InPps + h.InSourcePps AS Pps 
FROM dbo.NPM_MulticastRoutingDataHistory h
JOIN NPM_Multicast_RoutingTable t ON t.MulticastID = h.MulticastID
JOIN NPM_Multicast_GroupNodes gn ON t.MulticastGroupNodeID = gn.MulticastGroupNodeID
JOIN Nodes ON Nodes.NodeID = gn.NodeID
WHERE gn.NodeID IN ({0})
AND h.LastChangeUTC >= @StartTime AND h.LastChangeUTC <= @EndTime
ORDER BY gn.NodeID, gn.MulticastGroupNodeID, h.LastChangeUTC";

        var nodeList = String.Join(",",
                                   request.NetObjectIds.Select(x => x.ToString(CultureInfo.InvariantCulture)).ToArray());
        var sqlWithNodes = String.Format(sql, nodeList);
        var limitedSql = Limitation.LimitSQL(sqlWithNodes);

        var parameters = new[]
                             {
                                 new SqlParameter("StartTime", dateRange.StartDate),
                                 new SqlParameter("EndTime", dateRange.EndDate)
                             };

        List<DataSeries> rawSeries = GetSeries(limitedSql, parameters, dateFormatInDatabase);

        // Update the labels
        var displayNames = LoadDisplayNames(request.NetObjectIds);

        var seriesToReturn = new List<DataSeries>();

        var sampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);

        foreach (var raw in rawSeries)
        {
            var sampled = raw.CreateSampledSeries(bucketDateRange, sampleSize, SampleMethod.Average);

            int groupNodeId;
            if (int.TryParse(raw.TagName, out groupNodeId) && displayNames.ContainsKey(groupNodeId))
            {
                var displayName = displayNames[groupNodeId];
                sampled.Label = displayName;
            }
            
            sampled.TagName = "Group";

            seriesToReturn.Add(sampled);
        }

        if (request.CalculateSum)
            seriesToReturn.Add(DataSeries.CreateSumSeries(seriesToReturn));
        var result =
            dynamicLoader.SetDynamicChartOptions(
                new ChartDataResults(ApplyLimitOnNumberOfSeries(request, seriesToReturn)));
        result.AppliedLimitation = request.AppliedLimitation;

        return CalculateMinMaxForYAxis(request, result);
    }

    [WebMethod]
    public ChartDataResults UtilizationByMulticast(ChartDataRequest request)
    {
        // node interfaces Bps
        const string sqlTemplate =
@"SELECT CAST(FLOOR(CAST(mrth.[LastChangeUTC] AS FLOAT)*1440/{0})*{0}/1440 AS DATETIME) AS 'Time', AVG(mrth.[InBps]) AS 'MulticastBps'
FROM [dbo].[NPM_MulticastRoutingDataHistory] mrth
INNER JOIN [dbo].[NPM_Multicast_RoutingTable] mrt ON mrth.MulticastID = mrt.MulticastID AND mrt.SourceInterfaceIndex != 0
INNER JOIN [dbo].[NPM_Multicast_GroupNodes] mgn ON mrt.MulticastGroupNodeID = mgn.MulticastGroupNodeID
INNER JOIN [dbo].[Nodes] ON [Nodes].NodeID = mgn.NodeID
WHERE mgn.NodeID = @NetObjectId AND mrth.[LastChangeUTC] >= @StartTime AND mrth.[LastChangeUTC] <= @EndTime
GROUP BY CAST(FLOOR(CAST(mrth.[LastChangeUTC] AS FLOAT)*1440/{0})*{0}/1440 AS DATETIME)
ORDER BY CAST(FLOOR(CAST(mrth.[LastChangeUTC] AS FLOAT)*1440/{0})*{0}/1440 AS DATETIME)";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var finalSql = string.Format(sqlTemplate, request.SampleSizeInMinutes);
        var result = SimpleLoadData(request, dateRange, finalSql, string.Empty, dateFormatInDatabase, "Multicast", null, SampleMethod.Total, false);
        return CalculateMinMaxForYAxis(request, result);
    }

    [WebMethod]
    public NpmCharts.ChartDataResults TrafficShareByGroups(ChartDataRequest request)
    {
        const string sql =
@"SELECT g.MulticastGroupID, gn.NodeID, g.GroupIP, mgt.DisplayName, SUM(mrt.InBps) AS Bps, SUM(mrt.InPps + mrt.InSourcePps) AS Pps
    FROM NPM_Multicast_RoutingTable mrt
JOIN NPM_Multicast_GroupNodes gn ON mrt.MulticastGroupNodeID = gn.MulticastGroupNodeID
JOIN NPM_Multicast_Groups g ON gn.MulticastGroupID = g.MulticastGroupID
LEFT JOIN NPM_Multicast_GroupTranslation mgt ON  g.GroupIPGUID = mgt.GroupIPGUID
    WHERE NodeID = @nodeId
GROUP BY g.MulticastGroupID, gn.NodeID, g.GroupIP, mgt.DisplayName
ORDER BY SUM(mrt.InBps) DESC, g.GroupIP";

        var series = new List<NpmCharts.DataSeries>(1)
        {
            new NpmCharts.DataSeries { TagName = "Default" }
        };
        var result = new NpmCharts.ChartDataResults(series);

        var parameters = new[] { new SqlParameter("nodeId", request.NetObjectIds[0]) };
        var groups = GetSeriesForGroups(sql, parameters);
        
        if (groups.TrueForAll(g => g.Bps == 0))
        {
            // there's nothing to show, return empty result and display 'No Data' picture
            return result;
        }

        // only first x groups with highest triffic will be shown
        // rest will be groupped in single slice called "Other"
        // default value 5 could be overwritten by the resource properties
        var numberOfGroupsToShow = 5;
        var numberOfGroupsToShowKey = "NumberOfGroupsToShow".ToLower();
        if (request.AllSettings.ContainsKey("ResourceProperties"))
        {
            var resourceProperties = request.AllSettings["ResourceProperties"] as Dictionary<string, object>;
            if (resourceProperties != null && resourceProperties.ContainsKey(numberOfGroupsToShowKey))
                numberOfGroupsToShow = Convert.ToInt32(resourceProperties[numberOfGroupsToShowKey]);
        }
        
        var otherGroups = new List<GroupInfo>(groups);
        
        // show top x groups in standard way (one slice per group)
        for (int i = 0; i < Math.Min(numberOfGroupsToShow, groups.Count); i++)
        {
            var group = groups[i];
            series[0].Data.Add(group.ToDataPoint());
            otherGroups.Remove(group);
        }

        // show rest of the groups summarized in single slice
        if (otherGroups.Any())
        {
            var otherName = string.Format(NPMWebContent.NPMWEBDATA_ZB0_37, otherGroups.Count);
            var otherBps = Math.Max(0, otherGroups.Sum(g => g.Bps));
            var otherPps = Math.Max(0, otherGroups.Sum(g => g.Pps));
            var otherGroup = new GroupInfo {DisplayName = otherName, Bps = otherBps, Pps = otherPps};

            series[0].Data.Add(otherGroup.ToDataPoint());
        }

        return result;
    }

    [WebMethod]
    public NpmCharts.ChartDataResults CurrentTrafficOverview(ChartDataRequest request)
    {
        const string sql =
@"SELECT SUM(mit.InBps)/NULLIF(SUM(i.InBandwidth), 0) AS MulticastPercent, SUM(i.InBps)/NULLIF(SUM(i.InBandwidth), 0) AS AnycastPercent
FROM [dbo].[Interfaces] i
LEFT JOIN [dbo].[NPM_Multicast_Interfaces] mit ON i.InterfaceIndex = mit.[InterfaceIndex] AND i.[NodeID]=mit.[NodeID] 
WHERE i.[NodeID]=@nodeId";

        var series = new List<NpmCharts.DataSeries>(1)
        {
            new NpmCharts.DataSeries { TagName = "Default" }
        };

        var parameters = new[] { new SqlParameter("nodeId", request.NetObjectIds[0]) };

        var result = GetSeriesForNode(sql, parameters);

        if (result != null)
        {
            var multicast = Math.Max(0, result.MulticastUtil);
            var anycast = Math.Max(0, result.AnycastUtil - result.MulticastUtil);
            var free = Math.Max(0, 1 - result.AnycastUtil);
            
            var points = new List<NpmCharts.DataPoint>(3);
            points.Add(new NpmCharts.DataPoint(NPMWebContent.NPMWEBCODE_ZB0_6, new Dictionary<string, object> { { "y", multicast }, { "templateId", "CurrentTrafficOverview" } }));
            points.Add(new NpmCharts.DataPoint(NPMWebContent.NPMWEBCODE_ZB0_7, new Dictionary<string, object> { { "y", anycast } }));
            points.Add(new NpmCharts.DataPoint(NPMWebContent.NPMWEBCODE_ZB0_8, new Dictionary<string, object> { { "y", free } }));

            series[0].Data.AddRange(points);
        }

        return new NpmCharts.ChartDataResults(series);
    }

    private TrafficInfo GetSeriesForNode(string sql, SqlParameter[] parameters)
    {
        TrafficInfo result = null;
        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.Parameters.AddRange(parameters.ToArray());

            using (var reader = SqlHelper.ExecuteReader(cmd))
            {
                while (reader.Read())
                {
                    var multicast = DatabaseFunctions.GetDouble(reader, 0);
                    var anycast = DatabaseFunctions.GetDouble(reader, 1);

                    result = new TrafficInfo
                    {
                        MulticastUtil = multicast,
                        AnycastUtil = anycast,
                    };
                }
            }
        }

        return result;
    }

    private List<GroupInfo> GetSeriesForGroups(string sql, SqlParameter[] parameters)
    {
        var result = new List<GroupInfo>();
        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.Parameters.AddRange(parameters.ToArray());

            using (var reader = SqlHelper.ExecuteReader(cmd))
            {
                while (reader.Read())
                {
                    var multicastGroupId = DatabaseFunctions.GetInt32(reader, 0);
                    var nodeId = DatabaseFunctions.GetInt32(reader, 1);
                    var groupIp = DatabaseFunctions.GetString(reader, 2);
                    var displayName = DatabaseFunctions.GetString(reader, 3);
                    var bps = DatabaseFunctions.GetDouble(reader, 4);
                    var pps = DatabaseFunctions.GetDouble(reader, 5);

                    var info = new GroupInfo
                    {
                        MulticastGroupID = multicastGroupId,
                        NodeID = nodeId,
                        DisplayName = !string.IsNullOrEmpty(displayName) ? displayName : groupIp,
                        Bps = bps,
                        Pps = pps
                    };
                    result.Add(info);
                }
            }
        }

        return result;
    }

    private List<DataSeries> GetSeries(string sql, SqlParameter[] parameters, DateTimeKind dateFormatInDatabase)
    {
        var results = new List<DataSeries>();

        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.Parameters.AddRange(parameters.ToArray());

            using (var reader = SqlHelper.ExecuteReader(cmd))
            {
                int lastNodeId = Int32.MaxValue;
                var lastGroupNodeId = Int32.MaxValue;
                DataSeries currentSeries = null;

                while (reader.Read())
                {
                    int currentNodeId = DatabaseFunctions.GetInt32(reader, 0);
                    var currentGroupNodeId = DatabaseFunctions.GetInt32(reader, 1);

                    if ((currentNodeId != lastNodeId) || (currentGroupNodeId != lastGroupNodeId) || (currentSeries == null))
                    {
                        lastNodeId = currentNodeId;
                        lastGroupNodeId = currentGroupNodeId;
                        currentSeries = new DataSeries();
                        currentSeries.TagName = currentGroupNodeId.ToString();
                        currentSeries.NetObjectId = currentNodeId.ToString(CultureInfo.InvariantCulture);
                        results.Add(currentSeries);
                    }

                    var date = DatabaseFunctions.GetDateTime(reader, 2, dateFormatInDatabase);

                    var val = reader[3];
                    if ((val != null) && (!Convert.IsDBNull(val)))
                    {
                        var point = DataPoint.CreatePoint(date, Convert.ToDouble(val));
                        currentSeries.AddPoint(point);
                    }
                }
            }
        }

        return results;
    }

    private Dictionary<int, string> LoadDisplayNames(string[] netObjectIds)
    {
        var result = new Dictionary<int, string>();

        const string sqlFormat =
@"SELECT gn.MulticastGroupNodeID, g.GroupIP, gt.DisplayName 
FROM NPM_Multicast_GroupNodes gn
JOIN NPM_Multicast_Groups g ON gn.MulticastGroupID = g.MulticastGroupID
LEFT JOIN NPM_Multicast_GroupTranslation gt ON gt.GroupIPGUID = g.GroupIPGUID
WHERE NodeID IN ({0})";
        var nodeIdsCommaDelimited = String.Join(",", netObjectIds);

        using (var cmd = SqlHelper.GetTextCommand(String.Format(sqlFormat, nodeIdsCommaDelimited)))
        using (var reader = SqlHelper.ExecuteReader(cmd))
        {
            while (reader.Read())
            {
                var groupNodeId = DatabaseFunctions.GetInt32(reader, 0);
                var groupIp = DatabaseFunctions.GetString(reader, 1);
                var displayName = DatabaseFunctions.GetString(reader, 2);

                if (!string.IsNullOrEmpty(displayName))
                {
                    result[groupNodeId] = displayName;
                }
                else
                {
                    result[groupNodeId] = groupIp;
                }
            }
        }

        return result;
    }
    
    class GroupInfo
    {
        public int MulticastGroupID { get; set; }
        public int NodeID { get; set; }
        public string DisplayName { get; set; }
        public double Pps { get; set; }
        public double Bps { get; set; }
        
        public NpmCharts.DataPoint ToDataPoint()
        {
            return new NpmCharts.DataPoint(DisplayName, new Dictionary<string, object>
                                 {
                                     {"y", Math.Max(0, Bps)},
                                     {"pps", Math.Max(0, Pps)},
                                     {"id", MulticastGroupID},
                                     {"nodeid", NodeID},
                                     {"templateId", "TrafficShareByGroups"}
                                 });
        }

        public override bool Equals(object obj)
        {
            var objGroup = obj as GroupInfo;
            if (objGroup != null)
            {
                return this.MulticastGroupID.Equals(objGroup.MulticastGroupID);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return this.MulticastGroupID.GetHashCode();
        }
    }
    
    class TrafficInfo
    {
        public double MulticastUtil { get; set; }
        public double AnycastUtil { get; set; }
    }
}