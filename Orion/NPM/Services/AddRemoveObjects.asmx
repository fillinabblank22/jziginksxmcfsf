﻿<%@ WebService Language="C#" Class="AddRemoveObjects" %>
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;
using SolarWinds.Orion.NPM.Web.UI;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AddRemoveObjects : WebService
{
    private const string CustomProperties = "CustomProperties";
    private const string StatusProperty = "Status";
    private const string UnknownValue = "[Unknown]";
    private const string NoGroupingValue = "[No Grouping]";
        
   
    public class GroupByItem
    {
        public string Column { get; set; }
        public string DisplayName { get; set; }
        public string Type { get; set; }

        public GroupByItem()
        {
            Column = String.Empty;
            DisplayName = String.Empty;
            Type = String.Empty;
        }

        public GroupByItem(string column, string displayName, string type)
        {
            Column = column;
            DisplayName = displayName;
            Type = type;
        }
    }
    
        
    [WebMethod]
    public IEnumerable<GroupByItem> GetGroupByProperties(string entityType)
    {
        var properties = new List<GroupByItem>();
        
        properties.Add(new GroupByItem(String.Empty, NoGroupingValue, String.Empty));

        string query = String.Format(@"SELECT Name,
                                              DisplayName,
                                              Type,
                                              IsNavigable
                                       FROM Metadata.Property
                                       WHERE EntityName = '{0}' AND GroupBy = 'true'", entityType);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.Query(query);

            foreach (DataRow row in table.Rows)
            {
                string name = row["Name"].ToString();
                string displayName = (!row.IsNull("DisplayName")) ? row["DisplayName"].ToString() : name;
                string type = row["Type"].ToString();
                bool isNavigable = (bool)row["IsNavigable"];

                // select custom properties for this entity type
                if (name.Equals(CustomProperties, StringComparison.OrdinalIgnoreCase))
                {
                    String customQuery = String.Format(@"SELECT Name,
                                                                DisplayName,
                                                                Type
                                                         FROM Metadata.Property
                                                         WHERE EntityName = '{0}'
                                                           AND IsNavigable = 'false'
                                                           AND IsInherited = 'false'
                                                           AND IsInjected = 'false'
                                                           AND (Type<>'System.String' OR IsSortable='true')", type);

                    DataTable customTable = swis.Query(customQuery);

                    foreach (DataRow customRow in customTable.Rows)
                    {
                        name = customRow["Name"].ToString();
                        displayName = (!customRow.IsNull("DisplayName")) ? customRow["DisplayName"].ToString() : name;
                        type = customRow["Type"].ToString();
                        
                        properties.Add(new GroupByItem()
                        {
                           Column = String.Format("{0}.{1}.{2}", entityType, CustomProperties, name),
                           DisplayName =  UIHelper.Escape(displayName),
                           Type = type
                        });
                    }
                }
                else
                {
                    if (isNavigable)
                    {
                        properties.Add(new GroupByItem()
                        {
                           Column = String.Format("{0}.{1}.DisplayName", entityType, name),
                           DisplayName =  UIHelper.Escape(displayName),
                           Type = "System.String"
                        });
                    }
                    else
                    {
                        properties.Add(new GroupByItem()
                        {
                           Column = String.Format("{0}.{1}", entityType, name),
                           DisplayName =  UIHelper.Escape(displayName),
                           Type = type
                        });
                    }
                }
            }
        }

        return properties;
    }
  
      

    public class EntityGroup
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public List<int> ChildStatuses { get; set; }
        
        public int Status
        {
            get
            {
                if (ChildStatuses.Count > 1)
                {
                    return StatusInfo.RollupStatus(ChildStatuses, EvaluationMethod.Worst);
                }
                else
                {
                    return ChildStatuses[0];
                }
            }
        }

        public EntityGroup()
        {
            ChildStatuses = new List<int>();
        }
    }

    [WebMethod(EnableSession = true, MessageName = "GetEntityGroups")]
    public IEnumerable<EntityGroup> GetEntityGroups(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions, string filter)
    {
        return GetEntityGroups(entityType, groupByProperty, searchValue, excludeDefinitions, filter, "");
    }

    [WebMethod(EnableSession = true, MessageName = "GetEntityGroupsWithLimitation")]
    public IEnumerable<EntityGroup> GetEntityGroups(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions, string filter, string viewId)
    {
        int? viewLimitationId = GetLimitationIdFromViewIdString(viewId);
        
        var whereList = new List<String>();
        string whereQuery = String.Empty;
        
        if (!String.IsNullOrEmpty(filter))
        {
            whereList.Add(String.Format(" {0} ", filter));
        }

        if (!String.IsNullOrEmpty(searchValue))
        {
            whereList.Add(String.Format(" e.AncestorDisplayNames LIKE '%{0}%' ", searchValue.Replace("'", "''")));
        }

        if (excludeDefinitions != null && excludeDefinitions.Length > 0)
        {
            whereList.Add(String.Format(" e.{0} NOT IN ({1}) ", EntitiesHelper.TableColumnID(entityType), string.Join(",", excludeDefinitions)));
        }

        if (whereList.Count > 0)
        {
            whereQuery = String.Format(" WHERE {0} ", string.Join(" AND ", whereList.ToArray()));
        }
        
       
        // remove entity type prefix
        groupByProperty = groupByProperty.Replace(entityType + ".", string.Empty);
        
        string query = String.Format(@"SELECT {1} e.Status, Count(e.Status) AS Cnt
                                       FROM {0} e
                                       {2}
                                       GROUP BY {1} e.Status
                                       ORDER BY {3} {4}", 
                                        entityType, 
                                        groupByProperty.Equals(StatusProperty, StringComparison.OrdinalIgnoreCase) ? String.Empty : String.Format("e.{0},", groupByProperty),
                                        whereQuery,
                                        String.Format("e.{0}", groupByProperty),
                                        viewLimitationId != null ? String.Format("WITH LIMITATION {0}", viewLimitationId) : String.Empty
                                        );

        var entityGroups = new Dictionary<String, EntityGroup>();
        
        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.Query(query);

            foreach(DataRow row in table.Rows)
            {
                string property = row[0].ToString();
                string value = row[0].ToString();                                                          
                int count = Convert.ToInt32(row["Cnt"]);
                int status = Convert.ToInt32(row["Status"]);

                // show statuses instead of numbers
                if (groupByProperty.Equals(StatusProperty, StringComparison.OrdinalIgnoreCase))
                {
                    property = StatusInfo.GetStatus(Convert.ToInt32(property)).ShortDescription;
                }

                // show [Unknown] for empty strings
                if (String.IsNullOrEmpty(property))
                {
                    property = UnknownValue;
                }

                if (entityGroups.ContainsKey(property))
                {
                    EntityGroup group = entityGroups[property];
                    group.Count += count;
                    group.ChildStatuses.Add(status);
                }
                else
                {
                    EntityGroup group = new EntityGroup()
                    {
                        Name = property,
                        Value = value,
                        Count = count
                    };    
                    
                    group.ChildStatuses.Add(status);
                    entityGroups[property] = group;
                }
            }
        }
        
        return entityGroups.Values;
    }

    public class Entity
    {
        public string ID { get; set; }
        public string FullName { get; set; }
        public string Status { get; set; }
    }

    private string GenerateFullName(IList<string> names)
    {
        if (names == null)
            throw new ArgumentNullException("names");

        return System.Web.HttpUtility.HtmlEncode(string.Join(Resources.NPMWebContent.NPMWEBCODE_VB1_4, names.ToArray()));

    }


    [WebMethod(EnableSession = true, MessageName = "LoadEntities")]
    public IEnumerable<Entity> LoadEntities(string entityType, string entityIds)
    {
        return LoadEntities(entityType, entityIds, "");
    }
        
    [WebMethod(EnableSession = true, MessageName = "LoadEntitiesWithLimitation")]
    public IEnumerable<Entity> LoadEntities(string entityType, string entityIds, string viewId)
    {
        int? viewLimitationId = GetLimitationIdFromViewIdString(viewId);
        
        var entities = new List<Entity>();

        if (String.IsNullOrEmpty(entityType) || String.IsNullOrEmpty(entityIds))
        {
            return entities;
        }
        
        string query = String.Format(@"
SELECT e.{1}, 
       e.Status, 
       e.AncestorDisplayNames 
FROM {0} e
WHERE e.{1} IN ({2}) 
ORDER BY e.DisplayName {3}", entityType, EntitiesHelper.TableColumnID(entityType), entityIds,
                            viewLimitationId != null ? String.Format("WITH LIMITATION {0}", viewLimitationId) : String.Empty);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.Query(query);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    entities.Add(new Entity()
                    {
                        ID = row[0].ToString(),
                        Status = row[1].ToString(),
                        FullName = GenerateFullName((string[])row[2])
                    });
                }
            }
        }

        return entities;
    }
    
    private int? GetLimitationIdFromViewIdString(string viewId)
    {
        int? viewLimitationId = null;
        if (!string.IsNullOrEmpty(viewId))
        {
            int viewIdInt;
            if (Int32.TryParse(viewId, out viewIdInt))
            {
                try
                {
                    viewLimitationId = ViewManager.GetViewById(viewIdInt).LimitationID;
                }
                catch
                {
                    return null;
                }
            }
        }

        return viewLimitationId;
    }

}

