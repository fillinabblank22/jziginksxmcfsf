﻿<%@ WebService Language="C#" Class="Routing" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.NPM.Web.Routing;
using SolarWinds.NPM.Web.UI;
using SolarWinds.NPM.Web.UI.TabularResource;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI.StatusIcons;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public sealed class Routing : TabularResourceServiceBase
{
    static readonly Log _log = new Log();

    public class RouteData
    {
        private readonly Guid _routeDestinationGuid;
        private readonly string _routeNextHop;
        private readonly Guid _routeNextHopGuid;
        private readonly string _protocol;
        private readonly int _routeMaskLen;

        public Guid RouteDestinationGUID
        {
            get { return _routeDestinationGuid; }
        }

        public string RouteNextHop
        {
            get { return _routeNextHop; }
        }

        public Guid RouteNextHopGUID
        {
            get { return _routeNextHopGuid; }
        }

        public string Protocol
        {
            get { return _protocol; }
        }

        public int RouteMaskLen
        {
            get { return _routeMaskLen; }
        }

        public RouteData(Guid routeDestinationGuid, string routeNextHop, Guid routeNextHopGuid, string protocol, int routeMaskLen)
        {
            _routeDestinationGuid = routeDestinationGuid;
            _routeNextHop = routeNextHop;
            _routeNextHopGuid = routeNextHopGuid;
            _protocol = protocol;
            _routeMaskLen = routeMaskLen;
        }

        public override bool Equals(object value)
        {
            var type = value as RouteData;
            return (type != null) &&
                EqualityComparer<Guid>.Default.Equals(type.RouteDestinationGUID, RouteDestinationGUID)
                && EqualityComparer<int>.Default.Equals(type.RouteMaskLen, RouteMaskLen);
        }

        public override int GetHashCode()
        {
            int num = 0x7a2f0b42;
            num = (-1521134295 * num) + EqualityComparer<Guid>.Default.GetHashCode(RouteDestinationGUID);
            return (-1521134295 * num) + EqualityComparer<int>.Default.GetHashCode(RouteMaskLen);
        }
    }

    class NodeData
    {
        private readonly int _nodeId;
        private readonly string _nodeName;
        private readonly int _nodeStatus;
        private readonly Guid _ipAddress;

        public int NodeID
        {
            get { return _nodeId; }
        }

        public string NodeName
        {
            get { return _nodeName; }
        }

        public int NodeStatus
        {
            get { return _nodeStatus; }
        }

        public Guid IpAddress
        {
            get { return _ipAddress; }
        }

        public NodeData(int nodeId, string nodeName, int nodeStatus, Guid ipAddress)
        {
            _nodeId = nodeId;
            _nodeName = nodeName;
            _nodeStatus = nodeStatus;
            _ipAddress = ipAddress;
        }
    }

    class InterfaceData
    {
        private readonly int _interfaceId;
        private readonly string _statusLed;
        private readonly string _caption;
        private readonly int _interfaceIndex;

        public int InterfaceID
        {
            get { return _interfaceId; }
        }

        public string StatusLed
        {
            get { return _statusLed; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public int InterfaceIndex
        {
            get { return _interfaceIndex; }
        }

        public InterfaceData(int interfaceId, string statusLed, string caption, int interfaceIndex)
        {
            _interfaceId = interfaceId;
            _statusLed = statusLed;
            _caption = caption;
            _interfaceIndex = interfaceIndex;
        }
    }


    private static void InsertNodeDataIntoRow(Dictionary<Guid, NodeData> nodeData, Guid nodeIpGuid, string nodeIp, TableRow newRow, int engineId)
    {
        if (nodeData.ContainsKey(nodeIpGuid))
        {
            var nextHopData = nodeData[nodeIpGuid];

            var icon = string.Format(@"<img src='{0}' alt='{1}' />",
                                     NodeIconFactory.SmallIconURL(nextHopData.NodeID, nextHopData.NodeStatus),
                                     Resources.NPMWebContent.NPMWEBDATA_VB0_35);
            var cap = string.Format(@"<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}'>{1}</a>", nextHopData.NodeID,
                                    nextHopData.NodeName);
            newRow.AddCell(icon, null, "");
            newRow.AddCell(cap, nodeIpGuid, nextHopData.NodeName);

        }
        else
        {
            newRow.AddCell();
            if (engineId > 0 && ((ProfileCommon)HttpContext.Current.Profile).AllowNodeManagement)
                newRow.AddCell(string.Format("<a href=\"#\" onclick=\"SW.NPM.ManageNodeDialog('{1}', '{0}'); return false;\"><i>{0}</i></a>", nodeIp, engineId), nodeIpGuid, nodeIp);
            else // parent node not found (??). 
                newRow.AddCell(string.Format("<i>{0}</i>", nodeIp), nodeIpGuid, nodeIp);
        }
    }

    private static Dictionary<Guid, NodeData> GetNodeData(InformationServiceProxy swis, DataTable filterData, string filterColumn)
    {
        var dataFromOrionNodes = GetDataFromOrionNodes(swis, filterData, filterColumn);

        //get the setting for routing next hop linking. If the setting is set to true, only linking next hop node based on primary IP.
        //Otherwise, linking will look for all interface IPs
        var nexthopSetting = swis.Query("SELECT s.CurrentValue FROM Orion.Settings s WHERE s.SettingID = 'NPM_Settings_Routing_LinkingNextHopOnlyOnPrimaryIP'");

        if (nexthopSetting.Rows.Count > 0 && Convert.ToBoolean(nexthopSetting.Rows[0][0]))
        {
            //linking next hop node on primary IP only
            return dataFromOrionNodes.Rows.Cast<DataRow>().Select(x => new NodeData(Convert.ToInt32(x["NodeID"]), Convert.ToString(x["NodeName"]), Convert.ToInt32(x["NodeStatus"]), new Guid(x["IpAddress"].ToString()))).ToDictionary(x => x.IpAddress);
        }

        var dataFromOrionNodeIpAddresses = GetDataFromOrionNodeIpAddresses(swis, filterData, filterColumn);

        var orionNodesDataConverted = ConvertDataToDictionary(dataFromOrionNodes);
        var orionNodeIpAddressesDataConverted = ConvertDataToDictionary(dataFromOrionNodeIpAddresses);

        foreach (var line in orionNodeIpAddressesDataConverted)
        {
            if (!orionNodesDataConverted.ContainsKey(line.Key))
                orionNodesDataConverted.Add(line.Key, line.Value);
        }

        return orionNodesDataConverted;
    }

    private static Dictionary<Guid, NodeData> ConvertDataToDictionary(DataTable sourceDataTable)
    {
        return sourceDataTable.Rows.Cast<DataRow>()
            .Select(
                x => new NodeData(
                    Convert.ToInt32(x["NodeID"]),
                    Convert.ToString(x["NodeName"]),
                    Convert.ToInt32(x["NodeStatus"]),
                    new Guid(x["IpAddress"].ToString())))
            .ToDictionary(x => x.IpAddress);
    }

    private static DataTable GetDataFromOrionNodes(InformationServiceProxy swis, DataTable filterData, string filterColumn)
    {
        var nodeDataFilter = GetDistinctFilter<Guid>("IpAddressGUID IN ({0})", filterData, filterColumn);
        return swis.Query(@"SELECT DISTINCT NodeName, Status AS NodeStatus, NodeID, IpAddressGUID AS IpAddress FROM Orion.Nodes WHERE " + nodeDataFilter);
    }

    private static DataTable GetDataFromOrionNodeIpAddresses(InformationServiceProxy swis, DataTable filterData, string filterColumn)
    {
        var nodeSecDataFilter = GetDistinctFilter<Guid>("nip.IpAddressN IN ({0})", filterData, filterColumn);
        return swis.Query(@"
                SELECT n.NodeName, n.Status as NodeStatus, nj.NodeID, nj.IpAddressN as IpAddress
                FROM Orion.Nodes n
                INNER JOIN (
                  SELECT MIN(nip.NodeID) AS NodeID, nip.IpAddressN FROM Orion.NodeIpAddresses nip
                  WHERE " + nodeSecDataFilter + @"
                  GROUP BY nip.IpAddressN
                ) nj ON n.NodeID=nj.NodeId");
    }

    private static Dictionary<int, InterfaceData> GetInterfaceData(InformationServiceProxy swis, int nodeId, string filter)
    {
        var interfaceData = swis.Query(
            @"select distinct if.InterfaceID, if.StatusLed, if.Caption, if.InterfaceIndex FROM Orion.NPM.Interfaces if
WHERE if.InterfaceSubType=0 AND if.NodeId=@nodeId AND " + filter, new Dictionary<string, object> { { "nodeId", nodeId } });


        return interfaceData.Rows.Cast<DataRow>().Select(x => new InterfaceData(Convert.ToInt32(x["InterfaceID"]), Convert.ToString(x["StatusLed"]), Convert.ToString(x["Caption"]), Convert.ToInt32(x["InterfaceIndex"]))).ToDictionary(x => x.InterfaceIndex);
    }

    [WebMethod]
    public TabularResourceResult RoutingTable(RoutingPagingStatus pagingStatus, RoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.ElementIds[0] }, { "ipVersion", configParameters.ipVersion } };
            if (pagingStatus.vrfIndex.HasValue)
            {
                swisParameters.Add("vrfIndex", pagingStatus.vrfIndex);
            }

            var rv = new Table();

            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_5, "RouteDestinationGUID");
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_6, "RouteMaskLen");
            rv.CreateStatusColumn();
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_7, "RouteNextHopGUID", true);
            rv.CreateStatusColumn();
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_8, "InterfaceIndex", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_9, "Metric");
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_10, "DisplayName");

            using (var swis = InformationServiceProxy.CreateV3())
            {
                string vrfCondition = (pagingStatus.vrfIndex.HasValue) ? "=@vrfIndex" : " IS NULL";
                string query = string.Format(@"
SELECT DISTINCT RouteID, rt.LastChange, rt.RouteDestination, rt.RouteDestinationGUID, rt.RouteMaskLen, rt.ProtocolID, rt.Metric, ISNULL(rp.DisplayName, rt.ProtocolID) AS DisplayName,
rt.RouteNextHop, rt.InterfaceIndex, rt.RouteNextHopGUID FROM Orion.Routing.RoutingTable rt
LEFT JOIN Orion.Routing.RoutingProtocol rp ON rp.ProtocolID = rt.ProtocolID and rp.IP_Version = rt.Ip_Version
WHERE rt.NodeID=@nodeId AND rt.Ip_Version=@ipVersion AND rt.VrfIndex{0}", vrfCondition);

                var data = swis.Query(query, swisParameters);

                var engineIdData = swis.Query("SELECT EngineID FROM Orion.Nodes WHERE NodeID=@nodeId", swisParameters);
                int engineId = 0;

                if (engineIdData.Rows.Count > 0)
                    engineId = Convert.ToInt32(engineIdData.Rows[0][0]);

                var interfacesData = GetInterfaceData(swis, Convert.ToInt32(configParameters.ElementIds[0]), GetDistinctFilter<int>("InterfaceIndex IN ({0})", data, "InterfaceIndex"));
                var nodesData = GetNodeData(swis, data, "RouteNextHopGUID");

                // filldata
                foreach (DataRow row in data.Rows)
                {
                    var nrow = rv.AppendRow();

                    nrow.AddCell(row["RouteDestination"], row["RouteDestinationGUID"], row["RouteDestination"]);
                    nrow.AddCell(row["RouteMaskLen"]);


                    var nextHopGuid = new Guid(row["RouteNextHopGUID"].ToString());

                    InsertNodeDataIntoRow(nodesData, nextHopGuid, Convert.ToString(row["RouteNextHop"]), nrow, engineId);

                    if (row.IsNull("InterfaceIndex") || !interfacesData.ContainsKey(Convert.ToInt32(row["InterfaceIndex"])))
                    {
                        var ifIndex = row["InterfaceIndex"];
                        var ifCaption = string.Format("InterfaceIndex #{0}", ifIndex);
                        nrow.AddCell();
                        nrow.AddCell(@"<i>" + ifCaption + "</i>", ifIndex.ToString(), ifCaption);
                    }
                    else
                    {
                        var thisInterface = interfacesData[Convert.ToInt32(row["InterfaceIndex"])];
                        nrow.AddCell(FormatHelper.FormatStatusLedIcon(thisInterface.StatusLed), null, "");
                        nrow.AddCell(FormatHelper.FormatInterfaceInfo(thisInterface.InterfaceID, thisInterface.Caption), thisInterface.Caption, thisInterface.Caption);
                    }

                    //On some cisco devices, metric (inetCidrRoutMetric1) is returned as an unsigned integer.
                    //Also, since -1 is a valid value of this metric according to the standard, we don't touch it.
                    //If the value is smaller than -1, then we treat it as an overflown integer, and cast it to uint,
                    //for display purposes.
                    //FB261582,304317, we saw some device return metric as COUNTER64. We had to change metric's type to long/ulong
                    long metricValue = Convert.ToInt64(row["Metric"]);
                    if (metricValue < -1)
                    {
                        ulong metricDisplay = (ulong) metricValue;
                        nrow.AddCell(metricDisplay);
                    }
                    else
                    {
                        nrow.AddCell(metricValue);
                    }

                    nrow.AddCell(row["DisplayName"]);

                }

                dynamic cfg = new JsonObject();
                cfg.IncompleteData = CheckIncompleteData(swis, configParameters, pagingStatus);
                cfg.VrfInitialPoll = CheckVrfInitialPoll(swis, configParameters, pagingStatus);

                ApplyFilter(rv, pagingStatus);
                ApplyOrder(rv, pagingStatus);
                ApplyPaging(rv, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = rv,
                    Configuration = cfg
                };

            }


        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    private bool CheckIncompleteData(InformationServiceProxy swis, RoutingTableConfiguration configParameters, RoutingPagingStatus pagingStatus)
    {
        if (pagingStatus.vrfIndex.HasValue)
        {
            const string query = "SELECT CompleteValues FROM Orion.Routing.VRF WHERE VrfIndex = @vrfIndex";
            var res = swis.Query(query, new Dictionary<string, object> {{"vrfIndex", pagingStatus.vrfIndex.Value}});
            return res.Rows.Count > 0 && !Convert.ToBoolean(res.Rows[0][0]);
        }

        return DoSwisQuery(swis, configParameters.ipVersion == 4 ? "N.Routing.SNMP.Ipv4%" : "N.Routing.SNMP.Ipv6%", configParameters.ElementIds[0]);
    }

    private bool CheckVrfInitialPoll(InformationServiceProxy swis, RoutingTableConfiguration configParameters, RoutingPagingStatus paging)
    {
        if (!paging.vrfIndex.HasValue)
        {
            return false;
        }

        return DoSwisQuery(swis, "N.VRFRouting.%", configParameters.ElementIds[0]);
    }

    private bool DoSwisQuery(InformationServiceProxy swis, String poller, String nodeId)
    {
        var parameters = new Dictionary<string, object>
        {
            {"poller", poller},
            {"nodeid", nodeId}
        };
        var res = swis.Query("SELECT CompleteValues FROM Orion.Routing.RoutingDetails WHERE NodeId=@nodeid AND Poller LIKE @poller", parameters);
        return res.Rows.Count > 0 && !Convert.ToBoolean(res.Rows[0][0]);
    }

    [WebMethod]
    public object GetRoutingDetails(int nodeId, bool inReport)
    {
        try
        {
            using (var swis = InformationServiceProxy.CreateV3())
            {
                int inRep = inReport ? 1 : 0;
                var data = swis.Query(@"SELECT P.PollerType, RD.LastPoll, RD.NextPoll, RD.ErrorMessage, @inRep as InRep
FROM Orion.Pollers P
LEFT JOIN Orion.Routing.RoutingDetails RD ON RD.NodeId=P.NetObjectID AND RD.Poller=P.PollerType
WHERE P.NetObjectType='N' AND P.Enabled=1 AND (P.PollerType LIKE 'N.Routing%' OR P.PollerType LIKE 'N.VRF%') AND P.NetObjectID=@nodeId", new Dictionary<string, object> { { "nodeId", nodeId }, { "inRep", inRep } });

                return new
                {
                    protocols = data.Rows.Cast<DataRow>().Select(GetRoutingDetailsRowConvert)
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    private object GetRoutingDetailsRowConvert(DataRow row)
    {
        int inRep = (int)row["InRep"];
        return new
        {
            name = GetNameForPoller(row["PollerType"]),
            hasBeenPolled = !row.IsNull("LastPoll"),
            lastPoll = !row.IsNull("LastPoll") ?
                    inRep == 1 ?
                     ((DateTime)row["LastPoll"]).ToLocalTime().ToString() : FormatHelper.WrapTimeAgo(FormatHelper.GetTimeAgo(((DateTime)row["LastPoll"]).ToLocalTime()),(DateTime)row["LastPoll"]) : "",
            nextPoll = !row.IsNull("NextPoll")
                ? Utils.FormatCurrentCultureDate(((DateTime)row["NextPoll"]).ToLocalTime()) + " " + Utils.FormatToLocalTime(((DateTime)row["NextPoll"]).ToLocalTime())
                : "",
            error = row["ErrorMessage"]
        };
    }

    private string GetNameForPoller(object poller)
    {
        var p = poller.ToString();
        if (p.StartsWith("N.Routing."))
        {
            if (p.Contains("Ipv6"))
                return Resources.NPMWebContent.NPMWEBCODE_LF0_13;
            else
                return Resources.NPMWebContent.NPMWEBCODE_VT0_17;

        }
        if (p.StartsWith("N.RoutingNeighbor."))
        {
            return string.Format(Resources.NPMWebContent.NPMWEBCODE_VT0_18, p.Split('.').Last());
        }
        if(p.StartsWith("N.VRF"))
        {
            return Resources.NPMWebContent.NPMWEBCODE_GK0_1;
        }
        else
        {
            return Resources.NPMWebContent.NPMWEBCODE_VT0_19;
        }
    }

    private static string ProvideDateTimeWithTimeAgoOutput(DateTime dt)
    {
        var humanFriendly = Utils.FormatCurrentCultureDate(dt) + " " + Utils.FormatToLocalTime(dt);
        var rv = string.Format("<abbr class='timeago' title='{0}'>{1}</abbr>", dt.ToString("o"), humanFriendly);
        return rv;
    }

    [WebMethod]
    public TabularResourceResult DefaultRouteChanges(RoutingPagingStatus pagingStatus, RoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);

            DateTime dt = DateTime.UtcNow - TimeSpan.FromDays(pagingStatus.lastDays == 0 ? 7 : pagingStatus.lastDays);

            var rv = new Table();

            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_7, "rc.IpAddressGUID", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_42, "rc.ChangeType", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_43, "rc.DateTimeUTC", true);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.ElementIds[0] }, { "dateTime", dt }, { "ipVersion", configParameters.ipVersion } };
                if (pagingStatus.vrfIndex.HasValue)
                {
                    swisParameters.Add("vrfIndex", pagingStatus.vrfIndex);
                }

                string vrfCondition = (pagingStatus.vrfIndex.HasValue) ? "=@vrfIndex" : " IS NULL";
                var data = swis.Query(string.Format(@"SELECT IpAddress, IpAddressGUID, DateTimeUTC, ChangeType FROM Orion.Routing.DefaultRouteChange rc WHERE rc.DateTimeUTC > @dateTime AND rc.NodeID=@nodeId 
                    AND rc.Ip_Version=@ipVersion AND rc.VrfIndex{0}", vrfCondition),
                           swisParameters);

                foreach (DataRow row in data.Rows)
                {
                    var nrow = rv.AppendRow();
                    var changeType = Convert.ToInt32(row["ChangeType"]); // 1 == create

                    if (changeType == 0)
                    {
                        nrow.AddCell(string.Format("<span style='color:red'>{0}</span>", row["IpAddress"]),
                                     row["IpAddressGUID"], row["IpAddress"]);
                        nrow.AddCell(string.Format("<span style='color:red'>{0}</span>",
                                                  Resources.NPMWebContent.NPMWEBCODE_VT0_44),
                                                  Resources.NPMWebContent.NPMWEBCODE_VT0_44,
                                                  Resources.NPMWebContent.NPMWEBCODE_VT0_44);
                    }
                    else
                    {
                        nrow.AddCell(row["IpAddress"], row["IpAddressGUID"], row["IpAddress"]);
                        nrow.AddCell(Resources.NPMWebContent.NPMWEBCODE_VT0_45);
                    }
                    var inputDate = (DateTime)row["DateTimeUTC"];
                    var timeAgoString = FormatHelper.GetTimeAgo(inputDate);
                    nrow.AddCell(configParameters.IsInReport ? inputDate.ToLocalTime().ToString() : FormatHelper.WrapTimeAgo(timeAgoString,inputDate),null,timeAgoString);
                }

                ApplyFilter(rv, pagingStatus);
                ApplyOrder(rv, pagingStatus);
                ApplyPaging(rv, pagingStatus);

                dynamic cfg = new JsonObject();
                cfg.IncompleteData = CheckIncompleteData(swis, configParameters, pagingStatus);
                cfg.VrfInitialPoll = CheckVrfInitialPoll(swis, configParameters, pagingStatus);

                return new TabularResourceResult
                {
                    Data = rv,
                    TotalRows = data.Rows.Count,
                    Configuration = cfg
                };

            }

        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    private static string GetFormatForFlaps(int flaps, int flapsWarn, int flapsError)
    {
        const string format = "<div style='background-color: {0}; color: {1}; text-align: center'>{{0}}</div>";
        var color = "white";
        var fcolor = "black";
        if (flaps >= flapsError)
        {
            color = "red";
            fcolor = "white";
        }
        else if (flaps >= flapsWarn)
        {
            color = "yellow";
        }
        return string.Format(format, color, fcolor);
    }

    [WebMethod]
    public TabularResourceResult TopFlappingRoutes(RoutingPagingStatus pagingStatus, RoutingFlappingRoutesConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);

            DateTime dt = DateTime.UtcNow - TimeSpan.FromDays(pagingStatus.lastDays == 0 ? 7 : pagingStatus.lastDays);

            var rv = new Table();

            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_5, "rtf.RouteDestinationGUID");
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_6, "rtf.CIDR");
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_25, "Flaps", true);

            rv.CreateStatusColumn();
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_7, "Node", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_13, "Protocol", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_22, "LastChange", true);

            dynamic cfg = new JsonObject();

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.ElementIds[0] }, { "dateTime", dt }, { "ipVersion", configParameters.ipVersion } };
                if (pagingStatus.vrfIndex.HasValue)
                {
                    swisParameters.Add("vrfIndex", pagingStatus.vrfIndex);
                }

                string vrfCondition = (pagingStatus.vrfIndex.HasValue) ? "=@vrfIndex" : " IS NULL";
                var data =
                    swis.Query(string.Format(@"
SELECT rtf.RouteDestinationGUID, rtf.CIDR, MAX(rtf.RouteDestination) as RouteDestination, COUNT(rtf.FlapID) AS Flaps, MAX(rtf.DateTime) as LastChange
FROM Orion.Routing.RoutingTableFlap rtf
WHERE rtf.NodeId=@nodeId AND rtf.DateTime > @dateTime AND rtf.Ip_Version=@ipVersion AND rtf.VrfIndex{0}
GROUP BY rtf.RouteDestinationGUID, rtf.CIDR, rtf.Ip_Version
ORDER BY Flaps DESCENDING", vrfCondition), swisParameters
);

                var routes = swis.Query(string.Format(@"
SELECT DISTINCT rt.RouteDestinationGUID, rt.RouteMaskLen, rt.RouteNextHop, rt.RouteNextHopGUID, ISNULL(rtp.DisplayName, rt.ProtocolID) as Protocol FROM 
Orion.Routing.RoutingTable rt
LEFT JOIN Orion.Routing.RoutingProtocol rtp ON rtp.ProtocolID = rt.ProtocolID and rtp.IP_Version = rt.IP_Version
WHERE rt.NodeId=@nodeId AND rt.Ip_Version=@ipVersion AND rt.VrfIndex{0}", vrfCondition), swisParameters);

                var engineIdData = swis.Query("SELECT EngineID FROM Orion.Nodes WHERE NodeID=@nodeId", swisParameters);
                int engineId = 0;

                if (engineIdData.Rows.Count > 0)
                    engineId = Convert.ToInt32(engineIdData.Rows[0][0]);

                var nodesData = GetNodeData(swis, routes, "RouteNextHopGUID");

                var routesData =
                    routes.Rows.Cast<DataRow>().Select(
                    x => new RouteData(
                        new Guid(x["RouteDestinationGUID"].ToString()),
                        Convert.ToString(x["RouteNextHop"]),
                        new Guid(x["RouteNextHopGUID"].ToString()),
                        Convert.ToString(x["Protocol"]),
                        Convert.ToInt32(x["RouteMaskLen"]))
                    ).Distinct().ToDictionary(x => Tuple.Create(x.RouteDestinationGUID, x.RouteMaskLen));


                foreach (DataRow row in data.Rows)
                {
                    var nrow = rv.AppendRow();

                    nrow.AddCell(row["RouteDestination"], row["RouteDestinationGUID"], row["RouteDestination"]);
                    nrow.AddCell(row["CIDR"]);

                    var flaps = Convert.ToInt32(row["Flaps"]);
                    var format = GetFormatForFlaps(flaps, configParameters.FlapsWarn, configParameters.FlapsError);

                    nrow.AddCell(string.Format(format, flaps), flaps, flaps);

                    Guid routeNextHop = Guid.Empty;
                    string routeNextHopIp = "";
                    string protocol = "";

                    var routeDescr = Tuple.Create(new Guid(row["RouteDestinationGUID"].ToString()),
                                                  Convert.ToInt32(row["CIDR"]));
                    if (
                        routesData.ContainsKey(routeDescr))
                    {
                        routeNextHop = routesData[routeDescr].RouteNextHopGUID;
                        routeNextHopIp = routesData[routeDescr].RouteNextHop;
                        protocol =  string.Format("<div style='padding-left: 5px;'>{0}</div>", routesData[routeDescr].Protocol);
                    }

                    InsertNodeDataIntoRow(nodesData, routeNextHop, routeNextHopIp, nrow, engineId);

                    nrow.AddCell(protocol);

                    var inputDate = (DateTime)row["LastChange"];
                    var timeAgoString = FormatHelper.GetTimeAgo(inputDate);
                    nrow.AddCell(configParameters.IsInReport ? inputDate.ToLocalTime().ToString() : FormatHelper.WrapTimeAgo(timeAgoString, inputDate), null, timeAgoString);
                }

                cfg.DisableRowCountInHeader = true;
                cfg.IncompleteData = CheckIncompleteData(swis, configParameters, pagingStatus);
                cfg.VrfInitialPoll = CheckVrfInitialPoll(swis, configParameters, pagingStatus);
            }

            ApplyFilter(rv, pagingStatus);
            ApplyOrder(rv, pagingStatus);

            // small hack to take only TopXX first items, so I don't need to introduce new method
            ApplyPaging(rv, new PagingStatus{pageIndex = 0, pageSize = configParameters.TopXX});

            return new TabularResourceResult
            {
                Data = rv,
                Configuration = cfg
            };
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult NeighborTable(RoutingPagingStatus pagingStatus, RoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);

            var rv = new Table();
            rv.CreateStatusColumn();
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_12, "NeighborIPGUID", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_13, "ProtocolName", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_14, "ProtocolState", true);
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_21, "NeighborIPGUID");
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_47, "RemoteAS");
            rv.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_22, "LastChange", true);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.ElementIds[0] }, { "ipVersion", configParameters.ipVersion } };
                var data =
                    swis.Query(@"SELECT DISTINCT nb.NeighborIP, nb.AutonomousSystem, p.DisplayName AS ProtocolName, ps.DisplayName AS ProtocolState, nb.IsDeleted, nb.LastChange, nb.NeighborIPGUID
FROM Orion.Routing.Neighbors nb
LEFT JOIN Orion.Routing.RoutingProtocolStateMapping ps ON ps.ProtocolID=nb.ProtocolID AND ps.ProtocolStatus=nb.ProtocolStatus
INNER JOIN Orion.Routing.RoutingProtocol p ON p.ProtocolID=nb.ProtocolID and p.IP_Version = nb.Ip_Version
WHERE nb.NodeID=@nodeId AND nb.Ip_Version=@ipVersion",
                               swisParameters);

                var nodesData = GetNodeData(swis, data, "NeighborIPGUID");

                var engineIdData = swis.Query("SELECT EngineID FROM Orion.Nodes WHERE NodeID=@nodeId", swisParameters);
                int engineId = 0;

                if (engineIdData.Rows.Count > 0)
                    engineId = Convert.ToInt32(engineIdData.Rows[0][0]);

                foreach (DataRow row in data.Rows)
                {
                    var nrow = rv.AppendRow();

                    InsertNodeDataIntoRow(nodesData, new Guid(row["NeighborIPGUID"].ToString()), row["NeighborIP"].ToString(), nrow, engineId);

                    nrow.AddCell(row["ProtocolName"]);

                    if (Convert.ToBoolean(row["IsDeleted"]))
                    {
                        nrow.AddCell("<span style='color: red;'>" + Resources.NPMWebContent.NPMWEBCODE_VT0_15 + "</span>", Resources.NPMWebContent.NPMWEBCODE_VT0_15, Resources.NPMWebContent.NPMWEBCODE_VT0_15);
                    }
                    else
                    {
                        nrow.AddCell(row["ProtocolState"]);
                    }

                    nrow.AddCell(row["NeighborIP"], row["NeighborIPGUID"]);

                    var inputDate = (DateTime)row["LastChange"];
                    var timeAgoString = FormatHelper.GetTimeAgo(inputDate);
                    nrow.AddCell(row["AutonomousSystem"], row.IsNull("AutonomousSystem") ? 0 : Convert.ToInt32(row["AutonomousSystem"]));
                    nrow.AddCell(configParameters.IsInReport ? inputDate.ToLocalTime().ToString() : FormatHelper.WrapTimeAgo(timeAgoString, inputDate), inputDate.ToBinary(), timeAgoString);

                }

                ApplyFilter(rv, pagingStatus);
                ApplyOrder(rv, pagingStatus);
                ApplyPaging(rv, pagingStatus);


                return new TabularResourceResult
                {
                    Data = rv,
                    TotalRows = data.Rows.Count
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }
}