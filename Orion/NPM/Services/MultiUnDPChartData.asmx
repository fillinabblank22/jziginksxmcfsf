﻿<%@ WebService Language="C#" Class="MultiUnDPChartData" %>

using System;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.NPM.Web.ChartingV2;
using SolarWinds.Orion.Web.Charting.v2;
using System.Collections.Generic;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class MultiUnDPChartData : MultiUnDPChartDataWebService
{
    private static readonly Log _log = new Log();
    
    [WebMethod]
    public ChartDataResults MultiUnDP(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        if (request == null)
        {
            _log.Error("ChartDataRequest is null!");
            return null;
        }
            
        if (request.AllSettings == null)
        {
            _log.ErrorFormat("AllSettings is null!");
            return null;
        }
        
        var resourceProperties = request.AllSettings["ResourceProperties"] as Dictionary<string, object>;
        if (resourceProperties == null)
        {
            _log.ErrorFormat("ResourceProperties is null!");
            return null;
        }

        var helper = new MultiUnDPChartHelper(resourceProperties);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);

        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);
        
        string netObjectID = helper.ResolveNetObjectID(request.NetObjectIds[0]);

        var data = GetData(dateRange, dynamicSampleSize, netObjectID, helper.CustomPollerID_y1, helper.CustomPollerID_y2);
        
        double leftMinValue;
        double leftMaxValue;
        double rightMinValue;
        double rightMaxValue;
		CalculateMinMaxValueForLeftAndRightDataSeries(data, "y1", "y2", out leftMinValue, out leftMaxValue, out rightMinValue, out rightMaxValue);
		
		var dynamicResult = dynamicLoader.SetDynamicChartOptions(new ChartDataResults(data));
		
		return ResolveChartOptions(dynamicResult, resourceProperties, leftMinValue, leftMaxValue, rightMinValue, rightMaxValue);
    }

    private static ChartDataResults ResolveChartOptions(ChartDataResults result, Dictionary<string, object> resourceProperties, double leftMinValue, double leftMaxValue, double rightMinValue, double rightMaxValue)
    {
		if(result.ChartOptionsOverride == null)
			result.ChartOptionsOverride = new JsonObject();

        dynamic options = result.ChartOptionsOverride as JsonObject;
		if(options==null)
			_log.Error("ChartOptionsOverride is not a JsonObject");
        
        options.yAxis = JsonObject.CreateJsonArray(2);
        
        if (resourceProperties.ContainsKey("label_y1"))
            options.yAxis[0].title.text = resourceProperties["label_y1"];
        if (resourceProperties.ContainsKey("label_y2"))
            options.yAxis[1].title.text = resourceProperties["label_y2"];

        if (resourceProperties.ContainsKey("unit_y1"))
            options.yAxis[0].unit = resourceProperties["unit_y1"];
        if (resourceProperties.ContainsKey("unit_y2"))
            options.yAxis[1].unit = resourceProperties["unit_y2"];

        options.yAxis[0].min = Math.Min(leftMinValue,0);
        options.yAxis[0].minRange = 10;
        options.yAxis[1].min = Math.Min(rightMinValue,0);
        options.yAxis[1].minRange = 10;

        result.ChartOptionsOverride = options;
		
        return result;
    }
}
