<%@ WebService Language="C#" Class="NPMNodeTree" %>

using System;
using System.Data;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Runtime.Serialization.Json;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Logging;
using CustomPropertyMgr = SolarWinds.Orion.Core.Common.CustomPropertyMgr;
using Limitation = SolarWinds.Orion.Web.Limitation;
using Node = SolarWinds.Orion.NPM.Web.Node;
using RegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;

/// <summary>
/// Summary description for NodeTree
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NPMNodeTree : System.Web.Services.WebService
{

    private const string StatusLedImage = "<img src=\"/NetPerfMon/images/small-{0}\" border=\"0\" />";
    private const string IconImage = "<img src=\"/NetPerfMon/images/Interfaces/{0}\" border=\"0\" />";
    private const string VSANDetailLink = "<a href=\"/Orion/NPM/VSANDetails.aspx?NetObject=NVS:{0}&view=VSANDetails\">{1}</a>";

    private static readonly Log log = new Log();

    const int MaxNodesPerPage = 100;
    private const string VSAN_NAME_KEY = "VsanName";
    bool GroupNodesWithNullPropertiesAsUnknown;
    private bool rememberExpandedGroups;

    [WebMethod(EnableSession = true)]
    public string GetTreeSection(int resourceId, string rootId, object[] keys, int startIndex)
    {
        log.DebugFormat("GetTreeSection({1}, {2}, {3}, {4}) from user {0}",
            Context.User.Identity.Name, resourceId, rootId, FormatArray(keys), startIndex);

        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        if (!bool.TryParse(resource.Properties["GroupNodesWithNullPropertiesAsUnknown"], out GroupNodesWithNullPropertiesAsUnknown))
            GroupNodesWithNullPropertiesAsUnknown = true;

        if (!bool.TryParse(resource.Properties["RememberCollapseState"], out rememberExpandedGroups))
            rememberExpandedGroups = true;

        string filter = GetFilterClause(resource);

        try
        {
            try
            {
                StringWriter stringWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

                BuildTreeLevel(htmlWriter, resource, rootId, keys.Length + 1, keys, startIndex, filter);

                return stringWriter.ToString();
            }
            catch (SqlException ex)
            {
                StringWriter stringWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

                BuildTreeLevel(htmlWriter, resource, rootId, keys.Length + 1, keys, startIndex, string.Empty);

                // No exception when running without the filter. Inform the user.

                return string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_27, ex.Message, filter);
            }
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Exception in GetTreeSection({1}, {2}, {3}, {4}) from user {0}. {5}",
                Context.User.Identity.Name, resourceId, rootId, FormatArray(keys), startIndex, ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void CollapseTreeSection(int resourceId, object[] keys)
    {
        try
        {
            TreeStateManager manager = new TreeStateManager(Context.Session, resourceId);

            string keysArray = FormatArray(keys);
            manager.Collapse(keysArray);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Exception in CollapseTreeSection({1}, {2}) from user {0}. {3}",
                Context.User.Identity.Name, resourceId, FormatArray(keys), ex);
            throw;
        }
    }

    private string GetFilterClause(ResourceInfo resource)
    {
        string filter = resource.Properties["Filter"] ?? string.Empty;
        filter = CommonHelper.FormatFilter(filter.Trim());
        if (filter.Length > 0)
            filter = "(" + filter + ")";
        return filter;
    }

    // returns true if property contains table name
    private bool ForeignProperty(string property, out string propTable, out string propName)
    {
        // properties from other tables are defined as <table>.<column name>
        if (property.Contains("."))
        {
            string[] props = property.Split('.');
            if (props.Length != 2)
                throw (new FormatException(String.Format("NodeTree resource - Sort property \"{0}\" has invalid format!", property)));

            propTable = props[0];
            propName = props[1];
            return true;
        }
        else
        {
            propTable = "Nodes";
            propName = property;
            return false;
        }
    }

    private class Parameters : Dictionary<string, object>
    {
    }

    private string GetGroupingWhereClause(ResourceInfo resource, object[] keys, string filter, Parameters parameters)
    {
        List<string> clauses = new List<string>();

        if (!string.IsNullOrEmpty(filter))
            clauses.Add(filter);

        for (int level = 0; level < Math.Min(keys.Length, 3); ++level)
        {
            string property = resource.Properties[string.Format("Grouping{0}", level + 1)];

            string table, prop;
            ForeignProperty(property, out table, out prop);

            if (string.IsNullOrEmpty(property) || property.Equals("none", StringComparison.OrdinalIgnoreCase))
                break;
            //clauses.Add(string.Format("ISNULL({0},'')='{1}'", property, keys[level]));

            Type cpType = CustomPropertyMgr.GetTypeForProp(table, prop);
            object value = CustomPropertyMgr.ChangeType(keys[level], cpType);

            if (keys[level] == null)
            {
                clauses.Add(string.Format("({1}.{0} IS NULL OR RTRIM(LTRIM({1}.{0}))='')", prop, table));
                parameters.Add(property, value);
            }
            else
            {
                clauses.Add(string.Format("{1}.{0}=@{0}", prop, table));
                parameters.Add(property, value);
            }
        }

        return string.Join(" AND ", clauses.ToArray());
    }

    private static string FormatArray(object[] values)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(values.GetType());
        using (MemoryStream ms = new MemoryStream())
        {
            serializer.WriteObject(ms, values);
            return Encoding.UTF8.GetString(ms.ToArray());
        }
    }

    // returns the aggregated status of all the children (whose statuses may be based on their children)
    private void BuildTreeLevel(HtmlTextWriter writer, ResourceInfo resource, string rootId, int treeLevel, object[] keys, int startIndex, string filter)
    {
        Parameters parameters = new Parameters();
        string whereClause = GetGroupingWhereClause(resource, keys, filter, parameters);

        Parameters p;
        string join = BuildJoin(parameters, out p);

        string property = resource.Properties[string.Format("Grouping{0}", treeLevel)];

        TreeStateManager manager = new TreeStateManager(Context.Session, resource.ID);

        if (rememberExpandedGroups)
        {
            string keysArray = FormatArray(keys);
            manager.Expand(keysArray);
        }

        if (string.IsNullOrEmpty(property) || property.Equals("none", StringComparison.OrdinalIgnoreCase))
        {
            if (startIndex == 0)
                writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "16px");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            // here we have to define what to render: node or interface
            // it's a tricky way: the nodeId is being contained in keys
            // then keys are being mapped to resource properties (usually it's groupings)
            // the nodeId is the last in keys collection and has no property to be mapped to
            bool isNode = keys.Length <= parameters.Count;

            if (isNode)
            {
                RenderNodeList(writer, whereClause, parameters, rootId, resource, keys, startIndex, rootId, resource.ID, manager);
            }
            else
            {
                Int32 nodeId = (Int32)keys[keys.Length - 1];// get nodeid from keys collection (it's always the last)
                string vsanName = p[VSAN_NAME_KEY] as string;// get vsan name from parameters
                RenderInterfacesList(writer, nodeId, vsanName, keys);
            }

            writer.RenderEndTag();
        }
        else
        {
            string table;
            if (ForeignProperty(property, out table, out property))
                join = String.Format("{1} INNER JOIN {0} ON (Nodes.NodeID = {0}.NodeID) ", table, join);

            List<string> propertyValues = Node.GetJoinPropertyValues(join, table, property, whereClause, p);

            int index = 0;
            Action atEnd = null;
            for (int valueIndex = 0; valueIndex < propertyValues.Count; ++valueIndex)
            {
                if (valueIndex < startIndex)
                    continue;

                //Set color for odd line according to Wilbur (All this 'class' should be re-written!)
                if (valueIndex%2 == 0  && treeLevel == 1)
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "ZebraStripe");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                string childId = string.Format("{0}-{1}", rootId, index++);

                if (valueIndex - startIndex >= MaxNodesPerPage)
                {
                    RenderGetMoreLink(writer, childId, resource, keys, valueIndex, "groups");
                    break;
                }

                string key = propertyValues[valueIndex];

                if (string.IsNullOrEmpty(key == null ? key : key.Trim()) && !GroupNodesWithNullPropertiesAsUnknown)
                {
                    // for null keys, just render the next level in place
                    // defer until the end of the list
                    atEnd = delegate { BuildTreeLevel(writer, resource, childId, treeLevel + 1, ArrayAppend(keys, key), 0, filter); };
                }
                else
                {
                    string displayKey = (key == null ? string.Empty : key.Trim());
                    if ((displayKey.Length == 0) || (property == "MachineType" && displayKey == "Unknown"))
                        displayKey = Resources.CoreWebContent.WEBCODE_AK0_121;

                    RenderGroup(writer, resource, childId, treeLevel, property, displayKey, ArrayAppend(keys, key), filter, manager);
                }
                writer.RenderEndTag();
            }

            if (atEnd != null) atEnd();
        }
    }

    private object[] ArrayAppend(object[] array, object toAppend)
    {
        object[] newArray = new object[array.Length + 1];
        array.CopyTo(newArray, 0);
        newArray[array.Length] = toAppend;
        return newArray;
    }

    private void RenderGroup(HtmlTextWriter writer, ResourceInfo resource, string rootId, int treeLevel, string property, string key, object[] keys, string filter, TreeStateManager manager)
    {
        string toggleId = rootId + "-toggle";
        string contentsId = rootId + "-contents";

        Parameters parameters = new Parameters();
        string where = GetGroupingWhereClause(resource, keys, filter, parameters);

        writer.BeginRender();

        string keysArray = FormatArray(keys);
        bool needsExpanding = manager.IsExpanded(keysArray);

        writer.AddAttribute(HtmlTextWriterAttribute.Onclick,
            string.Format("NPM_VSAN.NPMNodeTree.Click('{0}', {1}, {2}); return false;", rootId, resource.ID, FormatArray(keys)));
        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");

        if (needsExpanding)
            writer.AddAttribute("expand", "1");

        writer.RenderBeginTag(HtmlTextWriterTag.A);

        writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Expand.gif");
        writer.AddAttribute(HtmlTextWriterAttribute.Id, toggleId);
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "Toggle");
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag(); // img

        Parameters p = null;
        string join = BuildJoin(parameters, out p);

        string text;
        VSAN vsan = GetVsan((string)p[VSAN_NAME_KEY]);
        if (property == "Status")
        {
            text = ((Status)Convert.ToInt32(key)).ToLocalizedString();
        }
        else if (property == VSAN_NAME_KEY && ((vsan != null)))
        {
            text = String.Format(VSANDetailLink, vsan.ID, vsan.Name);
        }
        else
        {
            text = key;
        }

        Status status = new Status();
        if (vsan != null)
        {
            switch (vsan.AdminState)
            {
                case VsanAdminState.Active:
                    status.Value = OBJECT_STATUS.Up;
                    break;
                case VsanAdminState.Suspended:
                    status.Value = OBJECT_STATUS.Down;
                    break;
                default:
                    status.Value = OBJECT_STATUS.Unknown;
                    break;
            }
        }
        else
        {
            status.Value = OBJECT_STATUS.Unknown;
        }

        status.Value = OBJECT_STATUS.Up;
        RenderGroupStatus(writer, status);

        writer.Write(text); // users expect that HTML in the group name will be rendered, not quoted. see Orion TT#2745.
        //writer.WriteEncodedText(text);
        writer.RenderEndTag(); // a

        writer.WriteBreak();

        writer.AddAttribute(HtmlTextWriterAttribute.Id, contentsId);
        writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "12px");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
        writer.RenderEndTag(); // div

        writer.EndRender();
    }

    // returns join query for parameters not from Nodes table 
    // and save that parameters without table preffix
    // todo: dont add already existing joins
    string BuildJoin(Parameters parameters, out Parameters p)
    {
        p = new Parameters();
        string join = String.Empty;

        foreach (var param in parameters)
        {
            string table, prop;
            if (ForeignProperty(param.Key, out table, out prop))
            {
                // prarameter is not from Nodes table, we need add join
                join = String.Format("{1} INNER JOIN {0} ON (Nodes.NodeID = {0}.NodeID) ", table, join);
            }

            // we need parameters to be without <table>. preffix
            p[prop] = param.Value;
        }

        return join;
    }

    private void RenderNodeList(HtmlTextWriter writer, string whereClause, Parameters parameters, string childId, ResourceInfo resource, object[] newKeys, int startIndex, string rootId, int resourceId, TreeStateManager manager)
    {

        List<Node> nodes = new List<Node>();
        if (!string.IsNullOrEmpty(whereClause))
        {
            Parameters p = null;
            string join = BuildJoin(parameters, out p);

            foreach (Node n in Node.GetJoinFilteredNodes(join, whereClause, p))
                nodes.Add(n);
        }
        else
        {
            foreach (Node n in Node.GetAllObjects())
                nodes.Add(n);
        }
        nodes.Sort(delegate(Node x, Node y)
        {
            return string.Compare(x.Name, y.Name, true);
        });

        for (int nodeIndex = 0; nodeIndex < nodes.Count; ++nodeIndex)
        {
            string nodeIdInTree = string.Format("{0}-{1}", childId, nodeIndex);
            if (nodeIndex < startIndex)
                continue;

            if (nodeIndex - startIndex >= MaxNodesPerPage)
            {
                RenderGetMoreLink(writer, childId, resource, newKeys, nodeIndex, "nodes");
                break;
            }

            RenderNode(writer, nodes[nodeIndex], nodeIdInTree, resourceId, newKeys, manager);
        }
    }

    private void RenderGetMoreLink(HtmlTextWriter writer, string childId, ResourceInfo resource, object[] keys, int index, string itemsType)
    {
        string getMoreId = string.Format("{0}-startFrom-{1}", childId, index);
        writer.AddAttribute(HtmlTextWriterAttribute.Id, getMoreId);
        writer.RenderBeginTag(HtmlTextWriterTag.Div);

        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, string.Format("NPM_VSAN.NPMNodeTree.GetMore('{0}', {1}, {2}, {3}); return false;",
            getMoreId, resource.ID, FormatArray(keys), index));
        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "getMore");
        writer.RenderBeginTag(HtmlTextWriterTag.A);
        writer.WriteEncodedText(string.Format("Get next {0} {1}", MaxNodesPerPage, itemsType));
        writer.RenderEndTag();

        writer.RenderEndTag();
    }

    private void RenderNode(HtmlTextWriter writer, Node node, string childId, int resourceId, object[] keys, TreeStateManager manager)
    {
        string keysString = FormatArray(ArrayAppend(keys, node.NodeID));
        bool needsExpanding = manager.IsExpanded(keysString);

        string toggleId = string.Format("{0}-toggle", childId);
        string contentsId = string.Format("{0}-contents", childId);

        writer.BeginRender();

        //expand image
        if (needsExpanding)
            writer.AddAttribute("expand", "1");
        writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Expand.gif");
        writer.AddAttribute(HtmlTextWriterAttribute.Id, toggleId);
        writer.AddAttribute(HtmlTextWriterAttribute.Onclick,
                            string.Format("NPM_VSAN.NPMNodeTree.Click('{0}', {1}, {2}); return false;", childId,
                                          resourceId, keysString));
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "Toggle");
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag();//img

        //node link
        writer.AddAttribute(HtmlTextWriterAttribute.Href, BaseResourceControl.GetViewLink(node));

        if (((ProfileCommon)Context.Profile).ToolsetIntegration)
        {
            writer.AddAttribute("IP", node.IPAddress.ToString());
            writer.AddAttribute("NodeHostname", node.Hostname);

            if (RegistrySettings.AllowSecureDataOnWeb())
            {
                writer.AddAttribute("Community", node.CommunityString);
            }
            else
            {
                string attrValue = FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(node.CommunityString));
                writer.AddAttribute("Community", attrValue);
            }
        }

        writer.RenderBeginTag(HtmlTextWriterTag.A);
        RenderNodeStatus(writer, node.Status);
        writer.WriteEncodedText(node.Name);
        writer.RenderEndTag(); // a

        // contents div
        writer.WriteBreak();

        writer.AddAttribute(HtmlTextWriterAttribute.Id, contentsId);
        writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "12px");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
        writer.RenderEndTag(); // div

        writer.EndRender();
    }

    private void RenderInterfacesList(HtmlTextWriter writer, int nodeId, string vsanName, object[] keys)
    {
        var vsan = GetVsan(nodeId, vsanName);
        if (vsan != null)
        {
            using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                DataTable vsanMemebers;
                try
                {
                    vsanMemebers = proxy.Api.GetVSANMembers(vsan.ID, Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    log.Error(ex);
                    throw;
                }

                foreach (DataRow row in vsanMemebers.Rows)
                {
                    if ((Int32)row["NodeID"] == nodeId)
                    {
                        RenderInterface(writer, row);
                    }
                }
            }
        }
    }

    private void RenderInterface(HtmlTextWriter writer, DataRow row)
    {
        writer.BeginRender();
        writer.Write(InterfaceLink(row["InterfaceName"], row["InterfaceLED"], row["InterfaceIcon"], row["InterfaceID"]));
        writer.WriteBreak();
        writer.EndRender();
    }

    //InterfaceLink(row["InterfaceName"], row["InterfaceLED"], row["InterfaceIcon"], row["InterfaceID"]))
    private string InterfaceLink(object name, object statusIcon, object interfaceIcon, object interfaceID)
    {
        return String.Format("<a href=\"/Orion/NPM/InterfaceDetails.aspx?NetObject=I:{1}\">{2}{3}&nbsp;{0}</a>", name, interfaceID, String.Format(StatusLedImage, trimSpaces(statusIcon)), String.Format(IconImage, trimSpaces(interfaceIcon)));
    }

    /// <summary>
    /// some strings are saved as char[n] in the database and then we need trim spaces
    /// </summary>
    /// <param name="text">text with trailing spaces</param>
    /// <returns>text without trailing spaces</returns>
    private string trimSpaces(object text)
    {
        if (text == null)
            return string.Empty;

        string str = text.ToString();

        if (string.IsNullOrEmpty(str))
            return string.Empty;
        else
            return str.Trim();
    }

    private VSAN GetVsan(Int32 nodeId, string vsanName)
    {
        Node n = Node.GetNode(nodeId) as Node;

        if (n != null)
        {
            using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                List<VSAN> vsans;
                try
                {
                    vsans = proxy.Api.GetVSANs(n.NodeID);
                }
                catch(Exception ex)
                {
                    log.Error(ex);
                    throw;
                }

                if (vsans != null && vsans.Count > 0)
                {
                    var vsan = vsans.FirstOrDefault(v => v.Name == vsanName);
                    if (vsan != null)
                    {
                        return vsan;
                    }
                    else
                    {
                        log.ErrorFormat("couldn't find vsan with name [{0}]", vsanName);
                    }
                }
                else
                {
                    log.ErrorFormat("GetVSANs({0}) returned empty result", nodeId);
                }
            }
        }
        else
        {
            log.ErrorFormat("can't find node with NodeId {0}", nodeId);
        }

        return null;
    }

    private VSAN GetVsan(string vsanName)
    {
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            List<VSAN> vsans;
            try
            {
                vsans = proxy.Api.GetAllVSANs();
            }
            catch(Exception ex)
            {
                log.Error(ex);
                throw;
            }
            if (vsans != null && vsans.Count > 0)
            {
                var vsan = vsans.FirstOrDefault(v => v.Name == vsanName);
                if (vsan != null)
                {
                    return vsan;
                }
                else
                {
                    log.ErrorFormat("couldn't find vsan with name [{0}]", vsanName);
                }
            }
            else
            {
                log.ErrorFormat("GetAllVSANs() returned empty result");
            }
        }

        return null;
    }

    private void RenderGroupStatus(HtmlTextWriter writer, Status status)
    {
        RenderStatus(writer, status.ToString("smallimgpath", null), status.ToString());
    }

    private void RenderNodeStatus(HtmlTextWriter writer, CompoundStatus status)
    {
        string alt;
        if (status.ChildStatus.Value == OBJECT_STATUS.Up)
            alt = string.Format("Node status is {0}.", status.ParentStatus);
        else
            alt = string.Format("Node status is {0}, and one or more interfaces have a status of {1}.",
                status.ParentStatus, status.ChildStatus);

        RenderStatus(writer, status.ToString("smallimgpath", null), alt);
    }

    private void RenderStatus(HtmlTextWriter writer, string iconPath, string alt)
    {
        writer.BeginRender();

        writer.AddAttribute(HtmlTextWriterAttribute.Class, "StatusIcon");

        writer.AddAttribute(HtmlTextWriterAttribute.Alt, alt);
        writer.AddAttribute(HtmlTextWriterAttribute.Src, iconPath);
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag();

        writer.EndRender();
    }
}

