﻿<%@ WebService Language="C#" Class="VSANChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.NPM.Web.ChartingV2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class VSANChartData : NPMChartDataWebServiceBase
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private const string _vsanNameLookupSwql = "SELECT ID, Name FROM Orion.NPM.VSANs WHERE ID in @NodeID";

    [WebMethod]
    public ChartDataResults TotalBytesTransferred(ChartDataRequest request)
    {
        var sql = $@"SELECT it.DateTime, 
                            it.In_TotalBytes AS InBytes,
                            it.Out_TotalBytes AS OutBytes
                     FROM NPM_VSANs_Traffic it
                     WHERE it.ID = @NetObjectId
                        AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                     ORDER BY [DateTime]";

        ApplyLimitOnNumberOfNetObjects(request, 2);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, string.Empty, DateTimeKind.Utc, "InBytes", "OutBytes", SampleMethod.Total, false);

        var result = new ChartDataResults(ComputeSubseries(request, dateRange, data));
        result.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
        return CalculateMinMaxForYAxis(request, dynamicResult);
    }

    [WebMethod]
    public ChartDataResults VSANErrorsDiscards(ChartDataRequest request)
    {
        var sql = $@"SELECT ie.DateTime, 
                            ie.In_Errors AS InErrors,
                            ie.In_Discards AS InDiscards,
                            ie.Out_Errors AS OutErrors,
                            ie.Out_Discards AS OutDiscards
                     FROM NPM_VSANs_Errors ie
                     WHERE ie.ID = @NetObjectId
                        AND ie.DateTime >= @StartTime AND ie.DateTime <= @EndTime
                     ORDER BY [DateTime]";

        ApplyLimitOnNumberOfNetObjects(request, 4);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, string.Empty, DateTimeKind.Utc,
            new[] { "InErrors", "InDiscards", "OutErrors", "OutDiscards" }, SampleMethod.Total, false);

        var result = new ChartDataResults(ComputeSubseries(request, dateRange, data));
        result.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
        return CalculateMinMaxForYAxis(request, dynamicResult);
    }

    [WebMethod]
    public ChartDataResults VSANTraffic(ChartDataRequest request)
    {
        /* this can be sometimes on summary page, and if passing from summary to CustomChart, no NetObject is passed. in this case, by default, all VSANs are shown */
        /* we need to mimic this behavior there */

        if (request.NetObjectIds.Length == 0 ||
            request.NetObjectIds.Length == 1 && string.IsNullOrWhiteSpace(request.NetObjectIds[0]))
        {

            using (var swql = InformationServiceProxy.CreateV3())
            {
                var r =
                    swql.Query(
                        @"SELECT DISTINCT vs.ID, vs.Name
FROM Orion.NPM.VSANs vs
INNER JOIN Orion.NPM.FCPorts fp ON fp.VsanID=vs.ID
INNER JOIN Orion.NPM.FCUnits fu ON fu.ID=fp.UnitID
");
                request.NetObjectIds = r.Rows.Cast<DataRow>().Select(x => x[0].ToString()).ToArray();
            }
        }

        var sql = $@"
            SELECT tr.DateTime,
                   (tr.In_Averagebps + tr.Out_Averagebps) AS Traffic
            FROM NPM_VSANs_Traffic tr
            INNER JOIN NPM_VSANs v ON (tr.ID=v.ID)
                WHERE 
                    tr.[DateTime]>=@StartTime AND tr.[DateTime] <= @EndTime AND
                    tr.ID = @NetObjectId
                ORDER BY [DateTime]
";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, _vsanNameLookupSwql, DateTimeKind.Utc, new[] { "Traffic" }, SampleMethod.Average, false);

        var result = new ChartDataResults(ComputeSubseries(request, dateRange, data));
        result.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
        return CalculateMinMaxForYAxis(request, dynamicResult);
    }
}
