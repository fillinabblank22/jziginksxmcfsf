﻿<%@ WebService Language="C#" Class="EnergyWiseChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class EnergyWiseChartData : ChartDataWebService 
{
	private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private const string _interfaceNameLookupSwql = "SELECT InterfaceID, FullName FROM Orion.NPM.Interfaces WHERE InterfaceID in @NodeID";
    private const string _nodeNameLookupSwql = "SELECT NodeID, Caption FROM Orion.Nodes WHERE NodeID in @NodeID";
    

    [WebMethod]
    public ChartDataResults DeviceEnergyUsage(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT ew.DateTime, 
                                    ew.Device_MaximumEnergyUsageWatts AS MaximumPowerConsumption,
                                    ew.Device_CurrentEnergyUsageWatts AS ActualPowerConsumption
                             From NPM_NV_EW_REPORT_DEVICE_V ew
                             INNER JOIN Nodes ON Nodes.NodeID = ew.NodeID
                             WHERE 
                                Nodes.NodeID = @NetObjectId
                                AND ew.DateTime >= @StartTime AND ew.DateTime <= @EndTime
                             ORDER BY [DateTime]";
                       
        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = DoSimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "MaximumPowerConsumption", "ActualPowerConsumption", SampleMethod.Average, false);
        var res = new ChartDataResults(ComputeSubseries(request, dateRange, result));
		res.AppliedLimitation = request.AppliedLimitation;
        return CalculateMinMaxForYAxis(res);
    }

    protected IEnumerable<DataSeries> ComputeSubseries(ChartDataRequest request, DateRange dateRange, IEnumerable<DataSeries> series, bool ignoreEmpty = true)
    {
        foreach (var serie in series)
        {
            if (!ignoreEmpty || serie.Data.Any(x => !x.IsNullPoint))
            {
                var trends = ComputeTrendsFromSampledData(request, dateRange, serie);
                yield return serie;
                foreach (var trend in trends)
                    yield return trend;
            }
        }
    }
    
    [WebMethod]
    public ChartDataResults OverallPowerConsumption(ChartDataRequest request)
    {
        // The final datetime is rounded to seconds, this helps to overcome some issues caused by precision of math operations over datetime when creating dates which fit into sample interval.
        // example: '2013-08-20 08:19:59.997' -> 2013-08-20 08:20:00.000'
        string sql = string.Format(@"
      SELECT 
        DATEADD(ms,500 - DATEPART(ms,[DateTime] + '00:00:00.500'),[DateTime]), 
        (SUM(d.Device_MaximumEnergyUsageWatts) * 0.001) as MaximumPowerConsumption,
        (SUM(d.Device_CurrentEnergyUsageWatts) * 0.001) as ActualPowerConsumption
      FROM
      (
         SELECT 
            NodeID,
            Convert(DateTime,Floor(Cast([DateTime] as Float)*1440/{0})*{0}/1440,0) As [DateTime],
            AVG(Device_CurrentEnergyUsageWatts) as Device_CurrentEnergyUsageWatts,
            MAX(Device_MaximumEnergyUsageWatts) as Device_MaximumEnergyUsageWatts
            FROM NPM_NV_EW_REPORT_DEVICE_V 
            GROUP BY NodeID, Convert(DateTime,Floor(Cast([DateTime] as Float)*1440/{0})*{0}/1440,0)
      ) AS d
      INNER JOIN Nodes ON (Nodes.NodeID=d.NodeID)
      WHERE 
            DateTime >= @StartTime AND DateTime <= @EndTime
      GROUP BY [DateTime]
      ORDER BY [DateTime]
", request.SampleSizeInMinutes);

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = DoSimpleLoadData(request, dateRange, sql, string.Empty, DateTimeKind.Local, "MaximumPowerConsumption", "ActualPowerConsumption", SampleMethod.Average, false);
        var res = new ChartDataResults(ComputeSubseries(request, dateRange, result));
        res.AppliedLimitation = request.AppliedLimitation;
        return CalculateMinMaxForYAxis(res);
    }

    [WebMethod]
    public ChartDataResults OverallSavings(ChartDataRequest request)
    {
        // The final datetime is rounded to seconds, this helps to overcome some issues caused by precision of math operations over datetime when creating dates which fit into sample interval.
        // example: '2013-08-20 08:19:59.997' -> 2013-08-20 08:20:00.000'
        string sql = string.Format(@"
      SELECT
         DATEADD(ms,500 - DATEPART(ms,e.[DateTime] + '00:00:00.500'),e.[DateTime]),
         CASE 
            WHEN e.Network_MaximumEnergyUsageWatts = 0 then 0.0
            ELSE (1.0 - (e.Network_CurrentEnergyUsageWatts / e.Network_MaximumEnergyUsageWatts)) * 100.0
         END AS AverageEnergyWiseSavings 
      FROM
      (		
        SELECT 
           [DateTime], 
           (SUM(d.Device_CurrentEnergyUsageWatts) * 0.001) as Network_CurrentEnergyUsageWatts,
           (SUM(d.Device_MaximumEnergyUsageWatts) * 0.001) as Network_MaximumEnergyUsageWatts
        FROM
        (
           SELECT 
              NodeID,
              Convert(DateTime,Floor(Cast([DateTime] as Float)*1440/{0})*{0}/1440,0) As [DateTime],
              AVG(Device_CurrentEnergyUsageWatts) as Device_CurrentEnergyUsageWatts,
              MAX(Device_MaximumEnergyUsageWatts) as Device_MaximumEnergyUsageWatts
           FROM NPM_NV_EW_REPORT_DEVICE_V 
           GROUP BY NodeID, Convert(DateTime,Floor(Cast([DateTime] as Float)*1440/{0})*{0}/1440,0)
        ) AS d
        INNER JOIN Nodes ON (Nodes.NodeID=d.NodeID)
        WHERE 
            DateTime >= @StartTime AND DateTime <= @EndTime
        GROUP BY [DateTime]
      ) AS e
      ORDER BY [DateTime]

", request.SampleSizeInMinutes);

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, string.Empty, DateTimeKind.Local, "AverageEnergyWiseSavings");
        return CalculateMinMaxForYAxis(result);
    }

    [WebMethod]
    public ChartDataResults EntityEnergyUsage(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        const string sql = @"SELECT ew.LastUpdate as [DateTime], 
                                    ISNULL(ew.EnergyWiseMaximumEnergyUsageWatts,0) AS MaximumPowerConsumption,
                                    ISNULL(ew.EnergyWiseCurrentEnergyUsageWatts,0) AS ActualPowerConsumption
                             From NPM_NV_EW_ENTITY_STATS ew
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = ew.OrionLink
                             INNER JOIN Nodes ON Nodes.NodeID = ew.NodeID
                             WHERE 
                                Interfaces.InterfaceID = @NetObjectId
                                AND ew.LastUpdate >= @StartTime AND ew.LastUpdate <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _interfaceNameLookupSwql, DateTimeKind.Local, "MaximumPowerConsumption", "ActualPowerConsumption");
		
        return CalculateMinMaxForYAxis(result);
    }
	
	//Calculate minimal and maximal value for Y-axis. When is minVal higher then 0, we use 0.
	private ChartDataResults CalculateMinMaxForYAxis(ChartDataResults result)
	{
        double minVal;
        double maxVal;
		bool noPoints = CalculateMinMaxValueForDataSeries(result.DataSeries, out minVal, out maxVal);

		minVal=Math.Min(minVal,0);		//when no points result is double.MaxValue
		if(!noPoints)
			maxVal = 0;

		if(result.ChartOptionsOverride == null)
			result.ChartOptionsOverride = new JsonObject();

        dynamic options = result.ChartOptionsOverride as JsonObject;
		if(options==null)
			_log.Error("ChartOptionsOverride is not a JsonObject");
			
        options.yAxis = JsonObject.CreateJsonArray(1);
        options.yAxis[0].min = minVal;
        //options.yAxis[0].minRange = 10; // not needed for EW charts, as they can display very low numbers (~0.5).

        result.ChartOptionsOverride = options;

        return result;
	}	
}
