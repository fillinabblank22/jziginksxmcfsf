﻿<%@ WebService Language="C#" Class="InterfaceChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Web.ChartingV2;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class InterfaceChartData : NPMChartDataWebServiceBase
{
	private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private const string _interfaceNameLookupSwql = "SELECT InterfaceID, FullName FROM Orion.NPM.Interfaces WHERE InterfaceID in @NodeID";

    [WebMethod]
    public ChartDataResults CustomPollerMMAChart(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        Guid g = GetResourceCustomPollerID(request);
        if (g == Guid.Empty)
            return new ChartDataResults(new DataSeries[0]);

        string sql = string.Format(@"SELECT DateTime, AvgRate, MinRate, MaxRate
                             From CustomPollerStatistics ps
                             INNER JOIN CustomPollerAssignment pa ON pa.CustomPollerAssignmentID = ps.CustomPollerAssignmentID
                             WHERE 
                                ps.DateTime >= @StartTime AND ps.DateTime <= @EndTime AND
                                pa.CustomPollerID='{0}' AND
                                pa.InterfaceID=@NetObjectId
                             ORDER BY [DateTime]",
                             g);


        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, _interfaceNameLookupSwql, DateTimeKind.Utc, "PollerAvg", "PollerMinMax");

        return CalculateMinMaxForYAxis(request, result);		
    }

    [WebMethod]
    public ChartDataResults CustomPollerChart(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT ps.DateTime, ps.RawStatus AS Poller
                             From CustomPollerStatistics ps
                             INNER JOIN CustomPollerAssignment pa ON pa.CustomPollerAssignmentID = ps.CustomPollerAssignmentID
                             INNER JOIN Nodes ON Nodes.NodeID = pa.NodeID
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = pa.InterfaceID
                             WHERE 
                                ps.DateTime >= @StartTime AND ps.DateTime <= @EndTime AND
                                pa.CustomPollerID=@cpid AND
                                pa.InterfaceID=@NetObjectId
                             ORDER BY [DateTime]";

        Guid g = GetResourceCustomPollerID(request);
        if (g == Guid.Empty)
            return new ChartDataResults(new DataSeries[0]);

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);
        var limitedSql = Limitation.LimitSQL(sql);

        List<DataSeries> allSeries = new List<DataSeries>();

        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        
        Dictionary<string, string> labelNames = null;

        if ((request.NetObjectIds.Length >= 1) && !string.IsNullOrEmpty(_interfaceNameLookupSwql))
        {
            labelNames = LoadDisplayNameLookup(request.NetObjectIds, _interfaceNameLookupSwql);
        }

        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);
        
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            var poller = proxy.Api.GetCustomPoller(g);

            foreach (var netObjectId in request.NetObjectIds)
            {
            	string label = "";
                
                var parameters = new[]
                                    {
                                        new SqlParameter("NetObjectId", netObjectId),
                                        new SqlParameter("StartTime", dateRange.StartDate),
                                        new SqlParameter("EndTime", dateRange.EndDate),
                                        new SqlParameter("cpid", g)
                                    };

                List<DataSeries> rawSeries = GetData(limitedSql, parameters, DateTimeKind.Utc, netObjectId, "Poller");

                var rawMainSeries = rawSeries[0];
                
                if (labelNames != null)
            	    labelNames.TryGetValue(netObjectId, out label);
                
                // we can fetch the label only from proxy
                rawMainSeries.Label = label;
                if (string.IsNullOrEmpty(rawMainSeries.Label))
                {
                    rawMainSeries.Label = poller.UniqueName;
                }

                var sampledMainSeries = rawMainSeries.CreateSampledSeries(dateRange, dynamicSampleSize,
                                                                          SampleMethod.Average);

                allSeries.Add(sampledMainSeries);
            }
        }

    	// we need to fill the label, in case of this is directly on iface.
    	
        var resdata = new ChartDataResults(allSeries);
        resdata.AppliedLimitation = request.AppliedLimitation;
        var result = dynamicLoader.SetDynamicChartOptions(resdata); 

        return CalculateMinMaxForYAxis(request, result);		
    }
}
