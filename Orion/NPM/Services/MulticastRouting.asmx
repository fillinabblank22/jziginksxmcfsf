﻿<%@ WebService Language="C#" Class="MulticastRouting" %>

using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Common;
using SolarWinds.NPM.Web;
using SolarWinds.NPM.Web.UI;
using System.Net;
using System.Collections.Generic;
using SolarWinds.NPM.Web.UI.TabularResource;

public class GroupIPNames
{
    public string GroupIPAddress;
    public string GroupName;
}

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public sealed class MulticastRouting : TabularResourceServiceBase
{
    private static readonly Log _logger = new Log();
    private const string ViewLinkTemplate = "/Orion/View.aspx?NetObject={0}";

    protected void ApplyViewLimitation(int viewLimitationId)
    {
        if (viewLimitationId > 0 && !HttpContext.Current.Items.Contains(typeof(ViewInfo).Name))
        {
            var viewInfo = new ViewInfo("", "", "", false, false, typeof(SolarWinds.Orion.NPM.Web.Node));
            viewInfo.LimitationID = viewLimitationId;
            HttpContext.Current.Items[typeof(ViewInfo).Name] = viewInfo;
        }
    }

    /// <summary>
    /// Shows details about specific group.
    /// </summary>
    /// <param name="groupId">group ID</param>
    /// <param name="nodeId">node ID</param>
    /// <returns></returns>
    [WebMethod]
    public object GetMulticastGroupDetails(string groupId, string nodeId, int viewLimitationId)
    {
        try
        {
            // 1_ setup
            bool isGroupNode = !string.IsNullOrEmpty(nodeId);
            bool isTrafficProblem = false;
            int groupTraffic = -1, // filter out at client side
                groupStatusId = -1,
                groupNodeStatusId = -1;
            string groupAddress = "",
                groupName = "",
                groupFlags = "",
                groupUptime = "",
                groupExpiry = "",
                statusExplanation = "",
                nodeStatusIcon = "";
            DataTable trafficData, detailsData, groupNodesStatuses = null;
            DataRow details;
            Dictionary<string, object> swisParameters;
            List<int> groupNodeStatusesList = new List<int>();

            string trafficQuery = @"
SELECT ROUND(MIN(rt.InPps + rt.InSourcePps),0) as TotalIncomingTrafficInGroup, CASE WHEN MIN(rt.InPps + rt.InSourcePps)>0 THEN 0 ELSE 1 END AS IsTrafficProblem
FROM Orion.NPM.MulticastRouting.RoutingTable rt
WHERE rt.MulticastSourceID IS NOT NULL AND rt.GroupNode.MulticastGroupID = @groupId
";

            string detailsQuery;
            if (isGroupNode)
            {
                swisParameters = new Dictionary<string, object> { { "groupId", groupId }, { "nodeId", nodeId } };
                detailsQuery = @"
SELECT TOP 1 g.GroupIP, g.DisplayName, rt.Flags, rt.Uptime, rt.ExpiryTime, gn.Status, gn.StatusReason AS [GroupNodeStatus], n.StatusIcon AS [NodeStatusIcon]
FROM Orion.NPM.MulticastRouting.Groups g
LEFT JOIN Orion.NPM.MulticastRouting.GroupNodes gn ON gn.MulticastGroupID = @groupId
LEFT JOIN Orion.NPM.MulticastRouting.RoutingTable rt ON gn.MulticastGroupNodeID = rt.MulticastGroupNodeID
LEFT JOIN Orion.Nodes n ON n.NodeID = gn.NodeID
WHERE g.MulticastGroupID = @groupId AND gn.NodeId = @nodeId
";
                trafficQuery = @"
SELECT ROUND(MIN(rt.InPps + rt.InSourcePps),0) as TotalIncomingTrafficInGroup, CASE WHEN MIN(rt.InPps + rt.InSourcePps)>0 THEN 0 ELSE 1 END AS IsTrafficProblem
FROM Orion.NPM.MulticastRouting.RoutingTable rt
WHERE rt.MulticastSourceID IS NOT NULL AND rt.GroupNode.MulticastGroupID = @groupId AND rt.GroupNode.NodeID = @nodeId
";
            }
            else
            {
                swisParameters = new Dictionary<string, object> { { "groupId", groupId } };
                detailsQuery = @"
SELECT DISTINCT g.GroupIP, g.DisplayName, g.Status
FROM Orion.NPM.MulticastRouting.Groups g
LEFT JOIN Orion.NPM.MulticastRouting.GroupNodes gn ON gn.MulticastGroupID = @groupId
LEFT JOIN Orion.NPM.MulticastRouting.RoutingTable rt ON gn.MulticastGroupNodeID = rt.MulticastGroupNodeID
WHERE g.MulticastGroupID = @groupId
";
            }

            string groupNodesStatusesQuery = @"
SELECT StatusReason AS [GroupNodeStatus]
FROM Orion.NPM.MulticastRouting.GroupNodes
WHERE MulticastGroupID = @groupId
";

            // 2_ load data via swis
            using (var swis = InformationServiceProxy.CreateV3())
            {
                trafficData = swis.Query(trafficQuery, swisParameters);
                detailsData = swis.Query(detailsQuery, swisParameters);
                if (!isGroupNode) groupNodesStatuses = swis.Query(groupNodesStatusesQuery, swisParameters);
            }

            // 3_ get traffic
            if (trafficData != null && trafficData.Rows != null && trafficData.Rows.Count == 1)
            {
                if (DBNull.Value != trafficData.Rows[0][0])
                    groupTraffic = Convert.ToInt32(trafficData.Rows[0][0]);

                if (DBNull.Value != trafficData.Rows[0][1])
                    isTrafficProblem = (Convert.ToInt32(trafficData.Rows[0][1]) == 1);
            }


            // 4_ get details
            if (detailsData != null && detailsData.Rows != null)
            {
                details = detailsData.Rows[0];

                if (details.Table.Columns.Contains("GroupIP") && DBNull.Value != details["GroupIP"])
                    groupAddress = details["GroupIP"].ToString();

                if (details.Table.Columns.Contains("DisplayName") && DBNull.Value != details["DisplayName"])
                    groupName = details["DisplayName"].ToString();

                if (details.Table.Columns.Contains("Status") && DBNull.Value != details["Status"])
                    groupStatusId = Convert.ToInt32(details["Status"]);

                if (details.Table.Columns.Contains("Flags") && DBNull.Value != details["Flags"])
                    groupFlags = new MulticastFlagsInfo(Convert.ToInt32(details["Flags"])).ToString();

                if (details.Table.Columns.Contains("Uptime") && DBNull.Value != details["Uptime"])
                    groupUptime = TimeSpan.FromSeconds(Convert.ToDouble(details["Uptime"])).ToString();

                if (details.Table.Columns.Contains("ExpiryTime") && DBNull.Value != details["ExpiryTime"])
                    groupExpiry = TimeSpan.FromSeconds(Convert.ToDouble(details["ExpiryTime"])).ToString();

                if (details.Table.Columns.Contains("Uptime") && DBNull.Value != details["Uptime"])
                    groupNodeStatusId = Convert.ToInt32(details["GroupNodeStatus"]);

                if (details.Table.Columns.Contains("NodeStatusIcon") && DBNull.Value != details["NodeStatusIcon"])
                    nodeStatusIcon = details["NodeStatusIcon"].ToString();
            }

            // 5_ process group & node status to create status explanation
            if (!isGroupNode)
            {
                foreach (DataRow row in groupNodesStatuses.Rows)
                {
                    if (DBNull.Value != row[0]) groupNodeStatusesList.Add(Convert.ToInt32(row[0]));
                }
            }
            else
            {
                groupNodeStatusesList.Add(groupNodeStatusId);
            }

            statusExplanation = FormatHelper.ConvertGroupStatusToText(groupStatusId, groupNodeStatusesList.ToArray(), isGroupNode, nodeStatusIcon);

            // 6_ finish
            return new
            {
                IPAddress = groupAddress,
                Name = groupName,
                Flags = groupFlags,
                Uptime = groupUptime,
                ExpiresIn = groupExpiry,
                Traffic = groupTraffic,
                IsTrafficProblem = isTrafficProblem,
                Status = statusExplanation
            };

        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public PageableDataTable GetGroupsNames()
    {
        try
        {
            DataTable data;


            string sortColumn = this.Context.Request.QueryString["sort"];
            string sortDirection = this.Context.Request.QueryString["dir"] ?? "ASC";
            bool isAscendingSorting = sortDirection.ToUpperInvariant() == "ASC";

            if (sortColumn.ToUpperInvariant() == "GroupName".ToUpperInvariant())
            {
                sortColumn = "g.DisplayName";
            }
            else
            {
                // default sorting is by IP Address
                sortColumn = "g.GroupIP";
            }

            var orderByClause = string.Format(" ORDER BY {0} {1}", sortColumn, isAscendingSorting ? "ASC" : "DESC");

            using (var swis = InformationServiceProxy.CreateV3())
            {
                data = swis.Query(@"
SELECT DISTINCT g.GroupIP, g.DisplayName
FROM Orion.NPM.MulticastRouting.Groups g
JOIN Orion.NPM.MulticastRouting.GroupNodes gn ON g.MulticastGroupID = gn.MulticastGroupID
JOIN Orion.Nodes n ON n.NodeID = gn.NodeID
" + orderByClause);
            }

            var rt = new DataTable("GroupIPTranslation");
            rt.Columns.Add("GroupIPAddress");
            rt.Columns.Add("GroupName");

            foreach (DataRow row in data.Rows)
            {
                var nrow = rt.NewRow();

                nrow[0] = row["GroupIP"];
                nrow[1] = HttpUtility.HtmlDecode(Convert.ToString(row["DisplayName"]));

                rt.Rows.Add(nrow);
            }

            return new PageableDataTable(rt, rt.Rows.Count);
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }

    }

    [WebMethod]
    public void SetGroupsNames(List<GroupIPNames> names)
    {
        AuthorizationChecker.AllowAdmin();
        //string sql = @"EXECUTE NPM_Multicast_UpdateGroupName @GroupIP, @IPVersion, @DisplayName";
        string sql = @"NPM_Multicast_UpdateGroupName";

        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (GroupIPNames groupName in names)
            {
                IPAddress groupIP = IPAddress.Parse(groupName.GroupIPAddress);
                byte ipVersion = (byte)((groupIP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) ? 4 : 6);

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@GroupIP", CommonHelper.IPtoSqlGuid(groupIP));
                cmd.Parameters.AddWithValue("@DisplayName", string.IsNullOrEmpty(groupName.GroupName) ? "" : HttpUtility.HtmlEncode(groupName.GroupName));
                cmd.Parameters.AddWithValue("@IPVersion", ipVersion);

                SqlHelper.ExecuteScalar(cmd);
            }
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastNodeInterfaces(PagingStatus pagingStatus, MulticastRoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.NodeId }, { "ipVersion", configParameters.IpVersion } };

            var tab = new Table();
            tab.CreateStatusColumn();
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_8, "InterfaceCaption", true);
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VB0_113, "Traffic", true);
            tab.Columns.Add(new TableColumn
            {
                DisplayName = Resources.NPMWebContent.NPMWEBCODE_PD0_12,
                EnableHtml = true,
                CssStyle = "width:20px"
            });

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT DISTINCT mri.InterfaceIndex, if.Caption as InterfaceCaption, if.StatusIcon as InterfaceStatus, if.InterfaceID, mri.MulticastProtocolID, mri.InBps, if.InBandwidth
FROM Orion.NPM.MulticastRouting.Interfaces mri
LEFT JOIN Orion.NPM.Interfaces if on if.Index = mri.InterfaceIndex AND if.NodeID = @nodeId
WHERE mri.NodeID = @nodeId AND if.InterfaceID IS NOT NULL
ORDER BY if.Caption", swisParameters);

                TableRow nrow;
                int ifId = -1,
                    multicastProtocolId = -1;
                float inBps = -1,
                    inBandwidth = -1;
                string ifCaption, ifStatus;

                foreach (DataRow row in data.Rows)
                {
                    if (DBNull.Value != row["InterfaceID"]) ifId = Convert.ToInt32(row["InterfaceID"]);
                    ifStatus = row["InterfaceStatus"].ToString();
                    ifCaption = row["InterfaceCaption"].ToString();
                    if (DBNull.Value != row["MulticastProtocolID"]) multicastProtocolId = Convert.ToInt32(row["MulticastProtocolID"]);
                    if (DBNull.Value != row["InBps"]) inBps = Convert.ToSingle(row["InBps"]);
                    if (DBNull.Value != row["InBandwidth"]) inBandwidth = Convert.ToSingle(row["InBandwidth"]);

                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(ifStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.Interface, ifId.ToString(), ifCaption, ifCaption), ifCaption, ifCaption);
                    var util = string.Format("{0} %", inBandwidth > 0 ? Math.Round(Math.Max(0, inBps * 100 / inBandwidth), 2) : 0);
                    nrow.AddCell(string.Format("<span class=\"sw-text-align-center\">{0}</span>", util), util, util);
                    nrow.AddCell(FormatHelper.FormatMulticastProtocolIcon(multicastProtocolId), "", "");
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastGroupNodeInterfaces(PagingStatus pagingStatus, MulticastRoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.NodeId }, { "groupId", configParameters.GroupId } };

            var tab = new Table();
            tab.CreateStatusColumn();
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_VT0_8, "InterfaceCaption", true);
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBDATA_JF0_37, "Direction", true);
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBDATA_JF0_36, "Traffic", true);
            tab.Columns.Add(new TableColumn
            {
                DisplayName = Resources.NPMWebContent.NPMWEBCODE_PD0_12,
                EnableHtml = true,
                CssStyle = "width:20px"
            });

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT mi.InterfaceIndex, if.Caption as InterfaceCaption, if.StatusIcon as InterfaceStatus, if.InterfaceID, mi.Direction, mi.Pps, mri.MulticastProtocolID
FROM 
(SELECT mrt.GroupNode.NodeID, mrt.SourceInterfaceIndex AS [InterfaceIndex], 'I' AS [Direction], SUM(mrt.InPps) AS [Pps]
FROM Orion.NPM.MulticastRouting.RoutingTable mrt
WHERE mrt.GroupNode.MulticastGroupID = @groupId AND mrt.GroupNode.NodeID = @nodeId AND MulticastSourceID IS NOT NULL
GROUP BY mrt.GroupNode.NodeID, mrt.SourceInterfaceIndex
UNION ALL
(SELECT mrt.GroupNode.NodeID, mrt.GroupNodeInterfaces.InterfaceIndex, 'O' AS [Direction], SUM(mrt.GroupNodeInterfaces.OutPps) AS [Pps]
FROM Orion.NPM.MulticastRouting.RoutingTable mrt
WHERE mrt.GroupNode.MulticastGroupID = @groupId AND mrt.GroupNode.NodeID = @nodeId AND MulticastSourceID IS NOT NULL AND mrt.GroupNodeInterfaces.InterfaceIndex IS NOT NULL
GROUP BY mrt.GroupNode.NodeID, mrt.GroupNodeInterfaces.InterfaceIndex)) mi
LEFT JOIN Orion.NPM.MulticastRouting.Interfaces mri ON mri.NodeID=mi.NodeID AND mri.InterfaceIndex=mi.InterfaceIndex
LEFT JOIN Orion.NPM.Interfaces if on if.Index = mi.InterfaceIndex AND if.NodeID = mi.NodeID
ORDER BY if.Caption
", swisParameters);

                TableRow nrow;
                int ifId, multicastProtocolId;
                string ifCaption, ifStatus, Pps, multicastDirection;

                foreach (DataRow row in data.Rows)
                {
                    ifId = Convert.ToInt32(row["InterfaceID"]);
                    ifStatus = row["InterfaceStatus"].ToString();
                    ifCaption = row["InterfaceCaption"].ToString();
                    multicastDirection = row["Direction"].ToString();
					
					if(DBNull.Value != row["MulticastProtocolID"])
						multicastProtocolId = Convert.ToInt32(row["MulticastProtocolID"]);
					else
						multicastProtocolId = 0;					
						
                    Pps = Convert.ToInt32(row["Pps"]).ToString() + " " + Resources.NPMWebContent.NPMWEBCODE_VB0_20;
                    int sortPPS = Convert.ToInt32(row["Pps"]);

                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(ifStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.Interface, ifId.ToString(), ifCaption, ifCaption), ifCaption, ifCaption);
                    nrow.AddCell(FormatHelper.TrafficDirection(multicastDirection), multicastDirection);
                    nrow.AddCell(string.Format(FormatHelper.RightAlignment, Pps), sortPPS);
                    nrow.AddCell(FormatHelper.FormatMulticastProtocolIcon(multicastProtocolId), "", "");
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastGroupMembership(PagingStatus pagingStatus, MulticastRoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.NodeId }, { "ipVersion", configParameters.IpVersion } };

            var tab = new Table();
            tab.CreateStatusColumn();
            tab.CreateNamedColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_7, true);
            tab.CreateNamedColumn(Resources.NPMWebContent.NPMWEBDATA_JF0_33);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT gn.MulticastGroupID as GroupID, g.DisplayName as GroupTitle, g.GroupIP, g.Status as MulticastGroupStatusID,
CASE WHEN g.DisplayName IS NOT NULL THEN 1 ELSE 0 END AS HasCustomName
FROM Orion.NPM.MulticastRouting.GroupNodes gn
INNER JOIN Orion.NPM.MulticastRouting.Groups g on g.MulticastGroupID = gn.MulticastGroupID
WHERE NodeID = @nodeId
ORDER BY HasCustomName DESC, GroupTitle 
", swisParameters);

                TableRow nrow;
                string groupId, groupTitle, groupIp;

                foreach (DataRow row in data.Rows)
                {
                    groupId = Convert.ToString(row["GroupID"]);
                    groupTitle = Convert.ToString(row["GroupTitle"]);
                    groupIp = Convert.ToString(row["GroupIP"]);

                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatMulticastGroupStatus(row["MulticastGroupStatusID"]), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.MulticastGroupNode, MulticastGroup.CreateMulticastNetObjectId(groupId, configParameters.NodeId.ToString()), groupTitle, groupTitle), null, groupTitle);
                    nrow.AddCell(groupIp);
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastRendezvousPoints(PagingStatus pagingStatus, MulticastRoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "nodeId", configParameters.NodeId }, { "groupId", configParameters.GroupId } };

            var tab = new Table();
            tab.CreateStatusColumn(); // node status
            tab.CreateStatusColumn(); // multicast status
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_JF0_34, "RendezvousPoint", true); // Rendezvous Point IP
            tab.CreateNamedColumn(Resources.NPMWebContent.NPMWEBCODE_JF0_35); // Status

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT n.NodeID AS RPNodeID, n.StatusIcon as NodeStatus, rp.GroupNodes.Status as MulticastStatusID, rp.RendezvousPointIP, n.Caption, rp.Status
FROM Orion.NPM.MulticastRouting.RendezvousPoints rp
LEFT JOIN Orion.NPM.MulticastRouting.Interfaces i ON rp.RendezvousPointIPGUID=i.IPAddressGUID
LEFT JOIN Orion.Nodes n ON i.NodeID=n.NodeID
WHERE rp.GroupNodes.NodeID=@nodeId AND rp.GroupNodes.MulticastGroupID=@groupId", swisParameters);

                TableRow nrow;
                string nodeStatus, rpName, rpIPAddress, rpStatusName;
                int rpNodeId, rpStatus;

                foreach (DataRow row in data.Rows)
                {
                    nodeStatus = Convert.ToString(row["NodeStatus"]);
                    rpIPAddress = Convert.ToString(row["RendezvousPointIP"]);
                    rpName = Convert.ToString(row["Caption"]);
                    rpStatus = Convert.ToInt32(row["Status"]);

                    if (rpStatus == 1)
                        rpStatusName = Resources.NPMWebContent.NPMWEBCODE_JF0_36;   //Enabled
                    else
                        rpStatusName = Resources.NPMWebContent.NPMWEBCODE_JF0_37;   //Disabled

                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(nodeStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatMulticastNodeStatus(row["MulticastStatusID"]), "", "");

                    if (string.IsNullOrEmpty(rpName))
                        nrow.AddCell(rpIPAddress, rpIPAddress, "");
                    else
                    {
                        rpNodeId = Convert.ToInt32(row["RPNodeID"]);
                        nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.MulticastGroupNode, MulticastGroup.CreateMulticastNetObjectId(configParameters.GroupId.ToString(), rpNodeId.ToString()), rpName, rpName), rpName, rpName);
                    }

                    nrow.AddCell(rpStatusName);
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastGroupMembers(PagingStatus pagingStatus, MulticastRoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "groupId", configParameters.GroupId } };

            var tab = new Table();
            tab.CreateStatusColumn(); // node status
            tab.CreateStatusColumn(); // multicast status
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_8, "NodeTitle", true);
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_15 + " (" + Resources.NPMWebContent.NPMWEBCODE_VB0_20 + ")", "TotalBps", true);
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_15 + " (" + Resources.NPMWebContent.NPMWEBCODE_VB0_19 + ")", "TotalPps", true);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT gn.NodeID, n.Caption as NodeTitle, n.StatusIcon as NodeStatus, gn.Status as MulticastStatusID,
(SELECT ROUND(SUM(rt.InBps), 2) AS Bps FROM Orion.NPM.MulticastRouting.RoutingTable rt
WHERE gn.MulticastGroupNodeID = rt.MulticastGroupNodeID) AS TotalBps,
(SELECT ROUND(SUM(rt.InPps + rt.InSourcePps), 0) AS Tps FROM Orion.NPM.MulticastRouting.RoutingTable rt
WHERE gn.MulticastGroupNodeID = rt.MulticastGroupNodeID) AS TotalPps,
gn.StatusReason AS [GroupNodeStatus]
FROM Orion.NPM.MulticastRouting.GroupNodes gn
JOIN Orion.Nodes n on n.NodeID = gn.NodeID
WHERE MulticastGroupID = @groupId AND n.NodeID IS NOT NULL
ORDER BY n.Caption", swisParameters);

                TableRow nrow;
                int nodeId;
                string nodeTitle, nodeStatus;
                string pps, bps;
                string title = "";

                foreach (DataRow row in data.Rows)
                {
                    nodeStatus = Convert.ToString(row["NodeStatus"]);
                    nodeId = Convert.ToInt32(row["NodeID"]);
                    nodeTitle = Convert.ToString(row["NodeTitle"]);
                    pps = Convert.ToString(row["TotalPps"]).ToString() + " " + Resources.NPMWebContent.NPMWEBCODE_VB0_20;
                    bps = FormatHelper.BPSScale(row["TotalBps"]);
                    int sortPPS = Convert.ToInt32(row["TotalPps"]);
                    float sortBPS = Convert.ToSingle(row["TotalBps"]);

                    //Here we are creating ToolTip for GroupNodeStatus icon
                    var groupNodeStatusId = Convert.ToInt32(row["GroupNodeStatus"]);
                    var groupNodeStatusesList = new[] { groupNodeStatusId };
                    title = FormatHelper.ConvertGroupStatusToText(swisParameters["groupId"], groupNodeStatusesList, true, "", true);

                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(nodeStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatMulticastNodeStatus(row["MulticastStatusID"], title), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.Node, nodeId.ToString(), nodeTitle, nodeTitle), nodeTitle, nodeTitle);
                    nrow.AddCell(string.Format(FormatHelper.RightAlignment, pps), sortPPS);
                    nrow.AddCell(string.Format(FormatHelper.RightAlignment, bps), sortBPS);
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastUpstreamRouters(PagingStatus pagingStatus, MulticastRoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "groupId", configParameters.GroupId }, { "nodeId", configParameters.NodeId } };

            var tab = new Table();
            tab.CreateStatusColumn(); // node status
            tab.CreateStatusColumn(); // multicast status
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_9, "NodeCaption", true); // previous node
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_10, "UpTime"); // uptime
            tab.CreateStatusColumn(); // interface status
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_11, "InterfaceCaption", true); // incoming interface
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_15, "OutTraffic", true); // upstream traffic
            tab.Columns.Add(new TableColumn
            {
                DisplayName = Resources.NPMWebContent.NPMWEBCODE_PD0_12,
                EnableHtml = true,
                CssStyle = "width:20px"
            }); // interface mode

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT n.NodeID, n.StatusIcon as NodeStatus, n.Caption AS NodeCaption, gn.MulticastRoutingTables.UpTime, i.InterfaceID, i.StatusIcon AS InterfaceStatus, i.Caption AS InterfaceCaption, gn.MulticastRoutingTables.MulticastProtocolID, ROUND(gni.OutPps,0) as OutTraffic, gn.Status as MulticastStatusID
FROM Orion.NPM.MulticastRouting.GroupNodes gn
INNER JOIN
(SELECT mri.NodeID,mri.InterfaceIndex
FROM Orion.NPM.MulticastRouting.GroupNodes gn
INNER JOIN Orion.NPM.MulticastRouting.Interfaces mri ON mri.IPAddressGUID = gn.MulticastRoutingTables.UpstreamNeighborIPGUID
INNER JOIN Orion.Nodes n ON mri.NodeID=n.NodeID
WHERE gn.MulticastGroupID = @groupId AND gn.NodeID = @nodeId AND gn.MulticastRoutingTables.UpstreamNeighborIPGUID != '00000000-0000-0000-0000-000000000000') dsr
ON gn.NodeID=dsr.NodeID
INNER JOIN Orion.NPM.MulticastRouting.GroupNodeInterfaces gni ON gn.MulticastRoutingTables.MulticastID=gni.MulticastID AND gni.InterfaceIndex = dsr.InterfaceIndex
LEFT JOIN Orion.Nodes n ON n.NodeID = dsr.NodeID
LEFT JOIN Orion.NPM.Interfaces i ON i.NodeID = dsr.NodeID AND i.InterfaceIndex = dsr.InterfaceIndex
WHERE gn.MulticastGroupID = @groupId AND gn.MulticastRoutingTables.MulticastSourceID IS NOT NULL
ORDER BY n.Caption, i.Caption
", swisParameters);

                TableRow nrow;
                string nodeId, ifId, ifStatus, nodeCaption, ifCaption, upTime, OutTraffic, nodeStatus;
                int multicastMode = -1;

                foreach (DataRow row in data.Rows)
                {
                    nodeId = row["NodeId"].ToString();
                    nodeCaption = row["NodeCaption"].ToString();
                    nodeStatus = row["NodeStatus"].ToString();
                    upTime = row["UpTime"].ToString();
                    ifId = row["InterfaceID"].ToString();
                    ifStatus = row["InterfaceStatus"].ToString();
                    ifCaption = row["InterfaceCaption"].ToString();
                    if (DBNull.Value != row["MulticastProtocolID"]) multicastMode = Convert.ToInt32(row["MulticastProtocolID"]);
                    OutTraffic = row["OutTraffic"].ToString() + " " + Resources.NPMWebContent.NPMWEBCODE_VB0_20;
                    int sortOutTraffic = Convert.ToInt32(row["OutTraffic"]);

                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(nodeStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatMulticastNodeStatus(row["MulticastStatusID"]), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.MulticastGroupNode, MulticastGroup.CreateMulticastNetObjectId(configParameters.GroupId.ToString(), nodeId), nodeCaption, nodeCaption), nodeCaption, nodeCaption);
                    nrow.AddCell(TimeSpan.FromSeconds(Convert.ToDouble(upTime)));
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(ifStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.Interface, ifId, ifCaption, ifCaption), ifCaption, ifCaption);
                    nrow.AddCell(string.Format(FormatHelper.RightAlignment, OutTraffic), sortOutTraffic);
                    nrow.AddCell(FormatHelper.FormatMulticastProtocolIcon(multicastMode), "", "");
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };

            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastDownstreamRouters(PagingStatus pagingStatus, MulticastRoutingTableConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "groupId", configParameters.GroupId }, { "nodeId", configParameters.NodeId } };

            var tab = new Table();
            tab.CreateStatusColumn(); // node status
            tab.CreateStatusColumn(); // multicast status
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_9, "NodeCaption", true); // previous node
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_10, "UpTime"); // uptime
            tab.CreateStatusColumn(); // interface status
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_11, "InterfaceCaption", true); // incoming interface
            tab.CreateSortableColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_15, "InTraffic", true); // downstream traffic          
            tab.Columns.Add(new TableColumn
            {
                DisplayName = Resources.NPMWebContent.NPMWEBCODE_PD0_12,
                EnableHtml = true,
                CssStyle = "width:20px"
            }); // interface mode

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT gn.NodeID, n.StatusIcon as NodeStatus, n.Caption as NodeCaption, gn.MulticastRoutingTables.UpTime, i.InterfaceID, i.StatusIcon AS InterfaceStatus, i.Caption as InterfaceCaption, gn.MulticastRoutingTables.MulticastProtocolID, ROUND(gn.MulticastRoutingTables.InPps + gn.MulticastRoutingTables.InSourcePps, 0) as InTraffic, gn.Status as MulticastStatusID
FROM Orion.NPM.MulticastRouting.GroupNodes gn 
INNER JOIN 
(
SELECT mi.NodeID
FROM Orion.NPM.MulticastRouting.GroupNodes gn
INNER JOIN Orion.NPM.MulticastRouting.PIMNeighbors pn ON pn.NodeID = gn.NodeID AND pn.InterfaceIndex = gn.MulticastRoutingTables.GroupNodeInterfaces.InterfaceIndex AND pn.InterfaceIndex != gn.MulticastRoutingTables.SourceInterfaceIndex
INNER JOIN Orion.NPM.MulticastRouting.Interfaces mi ON mi.IPAddressGUID = pn.IPAddressGUID
INNER JOIN Orion.Nodes n ON mi.NodeID=n.NodeID
WHERE gn.NodeID = @nodeId AND gn.MulticastGroupID = @groupId
) usr
ON gn.NodeID = usr.NodeID AND gn.MulticastGroupID = @groupId
LEFT JOIN Orion.Nodes n ON n.NodeID = usr.NodeID
LEFT JOIN Orion.NPM.Interfaces i ON i.NodeID = gn.NodeID AND i.InterfaceIndex = gn.MulticastRoutingTables.SourceInterfaceIndex
WHERE gn.MulticastRoutingTables.MulticastSourceID IS NOT NULL
ORDER BY n.Caption, i.Caption
", swisParameters);

                TableRow nrow;
                string nodeId, ifId, ifStatus, nodeCaption, ifCaption, upTime, inTraffic, nodeStatus;
                int multicastMode = -1;

                foreach (DataRow row in data.Rows)
                {
                    nodeId = row["NodeId"].ToString();
                    nodeCaption = row["NodeCaption"].ToString();
                    nodeStatus = row["NodeStatus"].ToString();
                    upTime = row["UpTime"].ToString();
                    ifId = row["InterfaceID"].ToString();
                    ifStatus = row["InterfaceStatus"].ToString();
                    ifCaption = row["InterfaceCaption"].ToString();
                    if (DBNull.Value != row["MulticastProtocolID"]) multicastMode = Convert.ToInt32(row["MulticastProtocolID"]);
                    inTraffic = row["InTraffic"].ToString() + " " + Resources.NPMWebContent.NPMWEBCODE_VB0_20;
                    int sortInTraffic = Convert.ToInt32(row["InTraffic"]);

                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(nodeStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatMulticastNodeStatus(row["MulticastStatusID"]), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.MulticastGroupNode, MulticastGroup.CreateMulticastNetObjectId(configParameters.GroupId.ToString(), nodeId), nodeCaption, nodeCaption), nodeCaption, nodeCaption);
                    nrow.AddCell(TimeSpan.FromSeconds(Convert.ToDouble(upTime)));
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(ifStatus), "", "");
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.Interface, ifId, ifCaption, ifCaption), ifCaption, ifCaption);
                    nrow.AddCell(string.Format(FormatHelper.RightAlignment, inTraffic), sortInTraffic);
                    nrow.AddCell(FormatHelper.FormatMulticastProtocolIcon(multicastMode), "", "");
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };

            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    [WebMethod]
    public TabularResourceResult GetMulticastTopXXTraffic(PagingStatus pagingStatus, MulticastRoutingTableTopXXConfiguration configParameters)
    {
        try
        {
            SetupService(configParameters);
            var swisParameters = new Dictionary<string, object> { { "maxRecords", configParameters.TopXX } };

            var tab = new Table();
            tab.CreateStatusColumn();
            tab.CreateStatusColumn();
            tab.CreateNamedColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_16, true);
            tab.CreateNamedColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_15 + " (" + Resources.NPMWebContent.NPMWEBCODE_VB0_19 + ")", true);
            tab.CreateNamedColumn(Resources.NPMWebContent.NPMWEBCODE_PD0_15 + " (" + Resources.NPMWebContent.NPMWEBCODE_VB0_20 + ")", true);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(@"
SELECT TOP @maxRecords n.StatusIcon as NodeStatus, gn.NodeID, n.Caption as NodeCaption, ROUND(SUM(mrt.InBps), 2) as TotalBps, ROUND(SUM(mrt.InPps + mrt.InSourcePps), 0) as TotalPps, 
CASE WHEN MIN(gn.Status) = 1 AND MAX(gn.Status) = 0 THEN 0 ELSE MAX(gn.Status) END AS MulticastStatusID
FROM Orion.NPM.MulticastRouting.RoutingTable mrt
INNER JOIN Orion.NPM.MulticastRouting.GroupNodes gn ON gn.MulticastGroupNodeID = mrt.MulticastGroupNodeID 
INNER JOIN Orion.Nodes n ON n.NodeID = gn.NodeID
WHERE n.NodeID IS NOT NULL
GROUP BY gn.NodeID, n.Caption, n.StatusIcon
ORDER BY SUM(mrt.InBps) DESC", swisParameters);

                TableRow nrow;
                string nodeId, nodeCaption, totalBps, totalPps, nodeStatus;

                foreach (DataRow row in data.Rows)
                {
                    nodeId = row["NodeID"].ToString();
                    nodeCaption = row["NodeCaption"].ToString();
                    totalBps = FormatHelper.BPSScale(row["TotalBps"]);
                    totalPps = row["TotalPps"].ToString();
                    nodeStatus = row["NodeStatus"].ToString();
                    float sortBPS = Convert.ToSingle(row["TotalBps"]);
                    int sortPPS = Convert.ToInt32(row["TotalPps"]);
                    
                    nrow = tab.AppendRow();
                    nrow.AddCell(FormatHelper.FormatStatusLedIcon(nodeStatus));
                    nrow.AddCell(FormatHelper.FormatMulticastNodeStatus(row["MulticastStatusID"]));
                    nrow.AddCell(FormatHelper.FormatNetObjectLink(FormatHelper.NetObjectTypes.Node, nodeId, nodeCaption, nodeCaption));
                    nrow.AddCell(string.Format(FormatHelper.RightAlignment, totalBps), sortBPS);
                    nrow.AddCell(string.Format(FormatHelper.RightAlignment, totalPps), sortPPS);
                }

                ApplyFilter(tab, pagingStatus);
                ApplyOrder(tab, pagingStatus);
                ApplyPaging(tab, pagingStatus);

                return new TabularResourceResult
                {
                    TotalRows = data.Rows.Count,
                    Data = tab
                };
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
            throw;
        }
    }

    /// <summary>
    /// Gets group IP tree section.
    /// </summary>
    /// <param name="groupId">group IP as guid</param>
    /// <returns>HTML tree portion</returns>
    [WebMethod(EnableSession = true)]
    public string GetGroupIPTreeSection(int resourceId, string groupId, string nodeId, int viewLimitationId)
    {
        TreeStateManager manager = new TreeStateManager(Context.Session, resourceId);

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        ApplyViewLimitation(viewLimitationId);

        if (string.IsNullOrEmpty(groupId))
            GenerateTopLevel(htmlWriter, resourceId, manager, nodeId, viewLimitationId);
        else
            GenerateSubLevel(htmlWriter, resourceId, groupId.Substring(1), manager);

        return stringWriter.ToString();
    }

    /// <summary>
    /// Remove group IP from TreeStateManager
    /// </summary>
    /// <param name="groupIp">group IP as guid</param>
    /// <returns>HTML tree portion</returns>
    [WebMethod(EnableSession = true)]
    public void RemoveGroupIP(int resourceId, string groupIp)
    {
        AuthorizationChecker.AllowAdmin();
        TreeStateManager manager = new TreeStateManager(Context.Session, resourceId);
        manager.Collapse(groupIp.Substring(1));
    }

    private void GenerateTopLevel(HtmlTextWriter writer, int resourceId, TreeStateManager manager, string nodeId, int viewLimitationId)
    {
        const string queryTemplate = @"
SELECT mg.Status, mg.MulticastGroupID, mg.GroupIP, mg.DisplayName
FROM Orion.NPM.MulticastRouting.RoutingTable mrt
INNER JOIN Orion.NPM.MulticastRouting.GroupNodes mgn ON mrt.MulticastGroupNodeID = mgn.MulticastGroupNodeID 
INNER JOIN Orion.NPM.MulticastRouting.Groups mg ON mgn.MulticastGroupID = mg.MulticastGroupID 
INNER JOIN Orion.Nodes n ON mgn.NodeID = n.NodeID
WHERE 1=1 {0}
GROUP BY mg.Status, mg.MulticastGroupID, mg.GroupIP, mg.DisplayName
ORDER BY mg.MulticastGroupID, mg.GroupIP, mg.DisplayName";

        var where = string.Empty;
        var parameters = new Dictionary<string, object>();
        if (!string.IsNullOrWhiteSpace(nodeId))
        {
            where = "AND mgn.NodeID = @nodeId";
            parameters.Add("nodeId", nodeId);
        }

        var finalQuery = string.Format(queryTemplate, where);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable data = swis.Query(finalQuery, parameters);

            foreach (DataRow row in data.Rows)
            {
                var status = FormatHelper.FormatMulticastGroupStatus(row[0]);
                var groupId = Convert.ToString(row[1]);
                var groupIp = Convert.ToString(row[2]);
                var groupName = Convert.ToString(row[3]);
                var isExpanded = manager.IsExpanded(groupId);
                WriteTopLevelLine(writer, resourceId, groupId, status, groupIp, groupName, isExpanded, nodeId, viewLimitationId);
                if (isExpanded)
                {
                    GenerateSubLevel(writer, resourceId, groupId, manager);
                }
            }
        }
    }

    private void GenerateSubLevel(HtmlTextWriter writer, int resourceId, string groupId, TreeStateManager manager)
    {
        manager.Expand(groupId);

        var swisParameters = new Dictionary<string, object> { { "groupId", groupId } };
        const string swisCMD = @"
SELECT n.StatusIcon, mgn.Status, n.NodeID, n.Caption, n.IPAddress, mgn.StatusReason AS [GroupNodeStatus]
FROM Orion.NPM.MulticastRouting.RoutingTable mrt
INNER JOIN Orion.NPM.MulticastRouting.GroupNodes mgn ON mrt.MulticastGroupNodeID = mgn.MulticastGroupNodeID
INNER JOIN Orion.NPM.MulticastRouting.Groups mg ON mgn.MulticastGroupID = mg.MulticastGroupID
INNER JOIN Orion.Nodes n ON mgn.NodeID = n.NodeID
WHERE mg.MulticastGroupID = @groupId
GROUP BY n.StatusIcon, mgn.Status, mg.MulticastGroupID, n.NodeID, n.Caption, n.IPAddress, mgn.StatusReason
ORDER BY n.Caption";
        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable data = swis.Query(swisCMD, swisParameters);
            string title = "";

            foreach (DataRow row in data.Rows)
            {
                //Here we are creating ToolTip for GroupNodeStatus icon
                var groupNodeStatusId = Convert.ToInt32(row["GroupNodeStatus"]);
                var groupNodeStatusesList = new[] { groupNodeStatusId };
                title = FormatHelper.ConvertGroupStatusToText(swisParameters["groupId"], groupNodeStatusesList, true, "", true);

                var nodeStatusIcon = Convert.ToString(row[0]);
                var multicastStatus = row[1];
                var nodeId = Convert.ToInt32(row[2]);
                var nodeName = Convert.ToString(row[3]);
                var nodeIp = Convert.ToString(row[4]);
                WriteSubLevelLine(writer, resourceId, groupId, nodeStatusIcon, multicastStatus, nodeId, nodeIp, nodeName, title);
            }
        }
    }

    // TODO: use underscore js instead of the htmlwriter
    private void WriteTopLevelLine(HtmlTextWriter writer, int resourceId, string groupId, string statusIcon, string groupIp, string groupName, bool isExpanded, string nodeId, int viewLimitationId)
    {
        // init variables
        string toggleId = string.Format("G{0}-toggle", groupId);
        var displayName = string.IsNullOrWhiteSpace(groupName) ? groupIp : groupName;

        var netObjectId = MulticastGroup.CreateMulticastNetObjectId(groupId, nodeId);

        // render html
        writer.AddAttribute(HtmlTextWriterAttribute.Id, string.Format("G{0}", groupId));
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
        writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);

        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, string.Format("GroupIPTree.Click('G{0}', {1}, '{2}', {3}); return false;", groupId, resourceId, nodeId, viewLimitationId));
        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
        writer.RenderBeginTag(HtmlTextWriterTag.A);

        if (isExpanded)
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Collapse.gif");
        else
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Expand.gif");

        writer.AddAttribute(HtmlTextWriterAttribute.Id, toggleId);
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag(); // img

        writer.Write("&nbsp;");

        writer.Write(statusIcon);

        writer.Write("&nbsp;");

        writer.RenderEndTag(); // a

        writer.AddAttribute(HtmlTextWriterAttribute.Href, string.Format(ViewLinkTemplate, netObjectId));
        writer.RenderBeginTag(HtmlTextWriterTag.A);

        // group name is already encoded in db
        writer.Write(displayName);

        writer.RenderEndTag(); // a

        writer.RenderEndTag(); // td

        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.WriteEncodedText(groupIp);
        writer.RenderEndTag(); // tr

        writer.RenderEndTag();
    }

    private void WriteSubLevelLine(HtmlTextWriter writer, int resourceId, string groupId, string nodeStatusIcon, object multicastStatus, int nodeId, string nodeIp, string nodeName, string title = null)
    {
        // init variables
        var netObjectId = string.Format("N:{0}", nodeId);
        var link = string.Format(ViewLinkTemplate, netObjectId);

        // render html
        writer.AddAttribute(HtmlTextWriterAttribute.Id, string.Format("G{0}-content", groupId));
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
        writer.AddAttribute(HtmlTextWriterAttribute.Width, "20px;");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write("&nbsp;");
        writer.RenderEndTag();

        writer.RenderBeginTag(HtmlTextWriterTag.Td);

        writer.RenderBeginTag(HtmlTextWriterTag.Span);

        writer.Write(FormatHelper.FormatStatusLedIcon(nodeStatusIcon));
        writer.Write(FormatHelper.FormatMulticastNodeStatus(multicastStatus, title));

        writer.AddAttribute(HtmlTextWriterAttribute.Href, link);
        writer.AddStyleAttribute("vertical-align", "3px"); //Local style for correct display of nodeName
        writer.RenderBeginTag(HtmlTextWriterTag.A);
        writer.WriteEncodedText(nodeName);

        writer.RenderEndTag();
        writer.RenderEndTag();
        writer.RenderEndTag();

        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.WriteEncodedText(nodeIp);
        writer.RenderEndTag();

        writer.RenderEndTag();
    }

    private class MulticastFlagsInfo
    {
        private readonly StringBuilder _flagChars;
        private readonly List<string> _flagNames;

        public MulticastFlagsInfo(int flags)
        {
            _flagChars = new StringBuilder();
            _flagNames = new List<string>();
            GetMulticastFlags(flags);
        }

        public override string ToString()
        {
            if (_flagChars.Length > 0) return string.Format("{0} ({1})", _flagChars, string.Join(", ", _flagNames));
            return string.Empty;
        }

        /// Flags are saved as integer in db. This method extracts the names from it.
        private void GetMulticastFlags(int flags)
        {
            if ((flags & (int)MulticastFlags.Prune) != 0)
            {
                _flagChars.Append('P');
                _flagNames.Add("Pruned");
            }
            if ((flags & (int)MulticastFlags.Sparse) != 0)
            {
                _flagChars.Append('S');
                _flagNames.Add("Sparse");
            }
            if ((flags & (int)MulticastFlags.Connected) != 0)
            {
                _flagChars.Append('C');
                _flagNames.Add("Connected");
            }
            if ((flags & (int)MulticastFlags.Local) != 0)
            {
                _flagChars.Append('L');
                _flagNames.Add("Local");
            }
            if ((flags & (int)MulticastFlags.Register) != 0)
            {
                _flagChars.Append('F');
                _flagNames.Add("Register flag");
            }
            if ((flags & (int)MulticastFlags.RP) != 0)
            {
                _flagChars.Append('R');
                _flagNames.Add("RP-bit set");
            }
            if ((flags & (int)MulticastFlags.SPT) != 0)
            {
                _flagChars.Append('J');
                _flagNames.Add("Join SPT");
            }
            if ((flags & (int)MulticastFlags.Join) != 0)
            {
                _flagChars.Append('j');
                _flagNames.Add("Join");
            }
            if ((flags & (int)MulticastFlags.MSDP) != 0)
            {
                _flagChars.Append('M');
                _flagNames.Add("MSDP");
            }
            if ((flags & (int)MulticastFlags.ProxyJoin) != 0)
            {
                _flagChars.Append('X');
                _flagNames.Add("ProxyJoin");
            }
        }
    }

    private static void ProcessException(Exception ex)
    {
        _logger.Error("Multicast service exception: ", ex);
    }

    [Flags]
    public enum MulticastFlags
    {
        Prune = 0x001, //1
        Sparse = 0x002, //2
        Connected = 0x004, //4
        Local = 0x008, //8
        Register = 0x010, //16
        RP = 0x020, //32
        SPT = 0x040, //64
        Join = 0x080, //128
        MSDP = 0x100, // 256
        ProxyJoin = 0x200 //512
    }
}
