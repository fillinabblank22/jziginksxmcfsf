﻿<%@ WebService Language="C#" Class="Vrf" %>

using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using System.Web.Script.Services;

/// <summary>
/// Service supporting VRF technology.
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Vrf : WebService
{
    private const int TreeSubItemsCount = 10;
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VrfListResult> GetAllVrfs(int startItem, int lastItem)
    {
        var result = new List<VrfListResult>();
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string queryString = @"SELECT DISTINCT RouteDistinguisher, COUNT(*) AS Nodes FROM Orion.Routing.VRF 
WHERE VRF.Node.NodeID > 0
GROUP BY RouteDistinguisher
ORDER BY RouteDistinguisher
WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object> {{"StartRow", startItem}, {"EndRow", lastItem}};
            DataTable proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                result.Add(new VrfListResult
                {
                    RouteDistinguisher = (string)row["RouteDistinguisher"],
                    ItemCount = (int)row["Nodes"],
                });
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VrfNodeResult> GetVrfNodes(string rowId, int startFrom)
    {
        var result = new List<VrfNodeResult>();
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string queryString = @"SELECT v.Node.NodeID, v.Node.StatusLED, v.Node.Caption, v.Name, v.Status 
FROM Orion.Routing.VRF v
WHERE v.RouteDistinguisher = @RD AND v.Node.NodeID IS NOT NULL
ORDER BY v.Node.NodeID
WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object> { { "RD", rowId }, { "StartRow", startFrom }, { "EndRow", startFrom + TreeSubItemsCount } };
            var proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                if (row["NodeID"] != DBNull.Value)
                {
                    var node = new VrfNodeResult
                    {
                        NodeName = (string)row["Caption"],
                        NodeDetailsLink = string.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", (int)row["NodeID"]),
                        NodeStatusIcon = (string)row["StatusLED"],
                        VrfName = (string)row["Name"],
                        VrfStatusIcon = MapVrfStatus((int)row["Status"])
                    };
                    result.Add(node);
                }
            }
        }
        return result;
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int GetAllVrfsCount()
    {
        int count = 0;
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string queryString = "SELECT COUNT(DISTINCT RouteDistinguisher) AS VrfCount FROM Orion.Routing.VRF";
            var parameters = new Dictionary<string, object>();
            DataTable proxyResult = proxy.Query(queryString, parameters);
            if (proxyResult.Rows.Count > 0 && proxyResult.Columns["VrfCount"] != null)
            {
                count = (int)proxyResult.Rows[0]["VrfCount"];
            }
        }
        return count;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VrfResult> GetListOfVrfsOnNode(int nodeId, int startItem, int lastItem)
    {
        var result = new List<VrfResult>();
        
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string queryString = @"
                SELECT VrfIndex, Name, RouteDistinguisher, Description, Status FROM Orion.Routing.Vrf
                WHERE NodeId = @NodeId
                ORDER BY RouteDistinguisher, Name
                WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object>
            {
                {"NodeId", nodeId},
                {"StartRow", startItem},
                {"EndRow", lastItem}
            };
            DataTable proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                var vrf = new VrfResult
                {
                    VrfIndex = (int)row["VrfIndex"],
                    Name = Convert.ToString(row["Name"]),
                    RouteDistinguisher = Convert.ToString(row["RouteDistinguisher"]),
                    Description = Convert.ToString(row["Description"]),
                    Status = Convert.ToInt32(row["Status"]),
                };
                int? interfaceCount = QueryInterfaceCount(nodeId, vrf.VrfIndex, proxy);
                if (interfaceCount.HasValue)
                {
                    vrf.ItemCount = interfaceCount.Value;
                }
                vrf.StatusIcon = MapVrfStatus(vrf.Status);
                
                result.Add(vrf);
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VrfInterfaceResult> GetVrfInterfaces(string rowId, int startFrom)
    {
        var result = new List<VrfInterfaceResult>();
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string queryString = @"SELECT vrf.Interfaces.IfIndex, vrf.Interfaces.VrfInterfaceType, vrf.Interfaces.Interface.InterfaceID, vrf.Interfaces.Interface.Caption, vrf.Interfaces.Interface.StatusLED
FROM Orion.Routing.Vrf vrf
WHERE vrf.VrfIndex = @VrfIndex
ORDER BY vrf.Interfaces.Interface.Caption
WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object> { { "VrfIndex", rowId }, { "StartRow", startFrom }, { "EndRow", startFrom + TreeSubItemsCount } };
            var proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                var iface = new VrfInterfaceResult();
                // there is no interface for this VRF, skip the adding
                if (row["IfIndex"] == DBNull.Value)
                {
                    continue;
                }
                //If there's no matching value in the interfaces table, we show the IfIndex from
                //VrfInterface Table with some prefix.
                if (row["InterfaceId"] == DBNull.Value)
                {
                    iface.InterfaceName = string.Format("IfIndex#{0}", (int)row["IfIndex"]);
                    iface.InterfaceStatusIcon = "Unknown.gif";
                    iface.HasItem = false;
                }
                //If there's value for interface, we show it.
                else
                {
                    iface.InterfaceName = (string)row["Caption"];
                    iface.InterfaceStatusIcon = (string)row["StatusLED"];
                    iface.HasItem = true;
                    iface.InterfaceDetailsLink = string.Format("/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}&view=InterfaceDetails", (int)row["InterfaceID"]);
                }
                result.Add(iface);
            }
        }
        return result;
    }
        
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int GetVrfOnNodeCount(int nodeId)
    {
        int count = 0;
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string queryString = @"SELECT COUNT(VrfIndex) AS VrfIndexCount FROM Orion.Routing.Vrf
WHERE NodeId = @NodeId";
            var parameters = new Dictionary<string, object>();
            parameters.Add("NodeId", nodeId);
            DataTable proxyResult = proxy.Query(queryString, parameters);
            if (proxyResult.Rows.Count > 0 && proxyResult.Columns["VrfIndexCount"] != null)
            {
                count = (int)proxyResult.Rows[0]["VrfIndexCount"];
            }
        }
        return count;
    }
    
    
    public class VrfListResult
    {
        public string RouteDistinguisher { get; set; }
        public int ItemCount { get; set; }
    }

    public class VrfNodeResult
    {
        public string NodeName { get; set; }
        public string NodeDetailsLink { get; set; }
        public string NodeStatusIcon { get; set; }
        public string VrfName { get; set; }
        public string VrfStatusIcon { get; set; }
    }

    public class VrfResult
    {
        public int VrfIndex { get; set; }
        public string Name { get; set; }
        public string RouteDistinguisher { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string StatusIcon { get; set; }
        public int ItemCount { get; set; }
    }

    public class VrfInterfaceResult
    {
        public bool HasItem { get; set; }
        public string InterfaceDetailsLink { get; set; }
        public string InterfaceStatusIcon { get; set; }
        public string InterfaceName { get; set; }
        public string InterfaceType { get; set; }
    }

    private int? QueryInterfaceCount(int nodeId, int vrfIndex, InformationServiceProxy proxy)
    {
        string interfaceCountQuery = "SELECT COUNT(*) AS InterfaceCount FROM Orion.Routing.vrfInterface vrfi JOIN Orion.Routing.Vrf vrf ON vrfi.VrfIndex = vrf.VrfIndex WHERE vrf.NodeId = @NodeId AND vrf.VrfIndex = @VrfIndex";
        var interfaceParameters = new Dictionary<string, object>();
        interfaceParameters.Add("NodeId", nodeId);
        interfaceParameters.Add("VrfIndex", vrfIndex);
        DataTable interfaceResult = proxy.Query(interfaceCountQuery, interfaceParameters);
        if (interfaceResult.Rows.Count > 0 && interfaceResult.Columns.Contains("InterfaceCount"))
        {
            return (int)interfaceResult.Rows[0]["InterfaceCount"];
        }
        else
        {
            return null;
        }
    }

    private string MapVrfStatus(int status)
    {
        string statusText = string.Empty;
        switch (status)
        {
            case 1:
                statusText = "Up";
                break;
            case 2:
                statusText = "Down";
                break;
            default:
                statusText = "Unknown";
                break;
        }
        return string.Format("/NetPerfmon/images/small-{0}.gif", statusText);
    }
}
