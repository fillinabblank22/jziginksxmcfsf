﻿<%@ WebService Language="C#" Class="NodeChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Web.ChartingV2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NodeChartData : NPMChartDataWebServiceBase 
{
	private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    [WebMethod]
    public ChartDataResults CiscoBufferFailures(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 6);
        const string sql = @"SELECT cb.DateTime, 
                                    cb.BufferNoMem,
                                    cb.BufferSmMiss,
                                    cb.BufferMdMiss,
                                    cb.BufferBgMiss,
                                    cb.BufferLgMiss,
                                    cb.BufferHgMiss
                             From CiscoBuffers cb WITH(NOLOCK)
                             INNER JOIN Nodes WITH(NOLOCK) ON Nodes.NodeID = cb.NodeID
                             WHERE 
                                Nodes.NodeID = @NetObjectId
                                AND cb.DateTime >= @StartTime AND cb.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = DoSimpleLoadData(request, dateRange, sql, string.Empty, DateTimeKind.Local, 
            new[] { "BufferNoMem", "BufferSmMiss", "BufferMdMiss", "BufferBgMiss", "BufferLgMiss", "BufferHgMiss" },
            SampleMethod.Total, false
            );

        var outputData = new ChartDataResults(ComputeSubseries(request, dateRange, result));
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(outputData);

        return CalculateMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults CustomPollerMMAChart(ChartDataRequest request)
    {
        var totalSeries = GetLimitOfSeries(request);
        int haveSeries = 0;
        const string sql = @"SELECT DateTime, AvgRate, MinRate, MaxRate, RawStatus
                             From CustomPollerStatistics ps WITH(NOLOCK)
                             INNER JOIN CustomPollerAssignment pa WITH(NOLOCK) ON pa.CustomPollerAssignmentID = ps.CustomPollerAssignmentID
                             INNER JOIN Nodes WITH(NOLOCK) ON Nodes.NodeID = pa.NodeID
                             WHERE 
                                ps.DateTime >= @StartTime AND ps.DateTime <= @EndTime AND
                                pa.CustomPollerID=@cpid AND
                                pa.NodeID=@NetObjectId AND ps.RowID=@RowID
                             ORDER BY [DateTime]";


        var g = GetResourceCustomPollerID(request);
        var rowids = GetRowIds(request);
        if (g == Guid.Empty)
            return new ChartDataResults(new DataSeries[0]);


        /* we need to do the loading on our own, modifying the DoSimpleLoadData function to handle this would be tricky, and there is probably no uther use */
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);
        var limitedSql = Limitation.LimitSQL(sql);

        List<DataSeries> allSeries = new List<DataSeries>();

        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);

        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            var poller = proxy.Api.GetCustomPoller(g);
            foreach (var netObjectId in request.NetObjectIds)
            {
                var assignments = proxy.Api.GetCustomPollerAssignmentsForNode(Convert.ToInt32(netObjectId)).Where(x => x.CustomPollerID == g);
                if (assignments.Count() != 1)  // this doesn't work on old charts as well
                    return new ChartDataResults(new DataSeries[0]); // no assignment for this node, but i am visible == no data

                var assignment = assignments.First();

                foreach (var rowId in rowids)
                {
                    haveSeries+=2;
                    if (request.Calculate95thPercentile)
                        haveSeries++;
                    if (request.CalculateTrendLine)
                        haveSeries++;
                    
                    if (haveSeries > totalSeries)
                    {
                        request.AppliedLimitation = true;
                        break;
                    }
                    
                    var parameters = new[]
                                        {
                                            new SqlParameter("NetObjectId", netObjectId),
                                            new SqlParameter("StartTime", dateRange.StartDate),
                                            new SqlParameter("EndTime", dateRange.EndDate),
                                            new SqlParameter("RowID", rowId),
                                            new SqlParameter("cpid", g)
                                        };

                    List<DataSeries> rawSeries = GetData(limitedSql, parameters, DateTimeKind.Utc, netObjectId, "PollerAvg", "min", "max", "PollerAvg");

                    var label = proxy.Api.GetLabelAndRowIDString(assignment, rowId.ToString());
                    if (string.IsNullOrEmpty(label))
                    {
                        label = poller.UniqueName;
                    }

                    var rawAverage = rawSeries[0];
                    var rawMin = rawSeries[1];
                    var rawMax = rawSeries[2];
                    var rawStatus = rawSeries[3];


                    var sampledAverage = rawAverage.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Average);
                    DataSeries sampledMin;
                    DataSeries sampledMax;

                    if (sampledAverage.Data.Any(x => !x.IsNullPoint)) // behave the same way how old charts behaved
                    {
                        sampledMin = rawMin.CreateSampledSeries(dateRange, dynamicSampleSize,
                                                                    SampleMethod.Min);
                        sampledMax = rawMax.CreateSampledSeries(dateRange, dynamicSampleSize,
                                                                    SampleMethod.Max);

                    }
                    else
                    {
                        sampledAverage = rawStatus.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Average);
                        sampledMin = rawStatus.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Min);
                        sampledMax = rawStatus.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Max);
                    }
                    var sampledMinMax = DataSeries.CombineMinMaxSeries(sampledMin, sampledMax, "PollerMinMax");

                    sampledAverage.TagName = "PollerAvg";
                    sampledAverage.Label = label;
                    sampledMinMax.TagName = "PollerMinMax";
                    sampledMinMax.Label = label;
                    
                    var trends = ComputeTrendsFromSampledData(request, dateRange, sampledAverage);
                    // we can fetch the label only from proxy

                    allSeries.Add(sampledAverage);
                    allSeries.AddRange(trends);
                    allSeries.Add(sampledMinMax);


                }
            }
        }
        var resdata = new ChartDataResults(allSeries);
        resdata.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = dynamicLoader.SetDynamicChartOptions(resdata);

		
        return CalculateMinMaxForYAxis(request, dynamicResult);
    }

    [WebMethod]
    public ChartDataResults CustomPollerChart(ChartDataRequest request)
    {
    	var totalSeries = GetLimitOfSeries(request);
        int haveSeries = 0;
        
        const string sql = @"SELECT ps.DateTime, ps.RawStatus AS Poller
                             From CustomPollerStatistics ps WITH(NOLOCK)
                             INNER JOIN CustomPollerAssignment pa WITH(NOLOCK) ON pa.CustomPollerAssignmentID = ps.CustomPollerAssignmentID
                             INNER JOIN Nodes WITH(NOLOCK) ON Nodes.NodeID = pa.NodeID
                             WHERE 
                                ps.DateTime >= @StartTime AND ps.DateTime <= @EndTime AND
                                pa.CustomPollerID=@cpid AND
                                pa.NodeID=@NetObjectId AND ps.RowID=@RowID
                             ORDER BY [DateTime]";

    	var g = GetResourceCustomPollerID(request);
    	var rowids = GetRowIds(request);
        if (g == Guid.Empty)
            return new ChartDataResults(new DataSeries[0]);

        
         /* we need to do the loading on our own, modifying the DoSimpleLoadData function to handle this would be tricky, and there is probably no uther use */
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);
        
        var limitedSql = Limitation.LimitSQL(sql);

        List<DataSeries> allSeries = new List<DataSeries>();

        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);
        
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
        	var poller = proxy.Api.GetCustomPoller(g);
            foreach (var netObjectId in request.NetObjectIds)
            {
                var assignments = proxy.Api.GetCustomPollerAssignmentsForNode(Convert.ToInt32(netObjectId)).Where(x => x.CustomPollerID == g);
                if (assignments.Count() != 1)  // this doesn't work on old charts as well
                    return new ChartDataResults(new DataSeries[0]); // no assignment for this node, but i am visible == no data

            	var assignment = assignments.First();
                
                foreach (var rowId in rowids)
                {
                    haveSeries ++;
                    if (request.Calculate95thPercentile)
                        haveSeries++;
                    if (request.CalculateTrendLine)
                        haveSeries++;

                    if (haveSeries > totalSeries)
                    {
                        request.AppliedLimitation = true;
                        break;
                    }
                    
                    var parameters = new[]
                                        {
                                            new SqlParameter("NetObjectId", netObjectId),
                                            new SqlParameter("StartTime", dateRange.StartDate),
                                            new SqlParameter("EndTime", dateRange.EndDate),
                                            new SqlParameter("RowID", rowId),
                                            new SqlParameter("cpid", g)
                                        };

                    List<DataSeries> rawSeries = GetData(limitedSql, parameters, DateTimeKind.Utc, netObjectId, "Poller");

                    var rawMainSeries = rawSeries[0];
                    // we can fetch the label only from proxy
                    rawMainSeries.Label = proxy.Api.GetLabelAndRowIDString(assignment, rowId.ToString());
                    if (string.IsNullOrEmpty(rawMainSeries.Label))
                    {
                    	rawMainSeries.Label = poller.UniqueName;
                    }

                    var sampledMainSeries = rawMainSeries.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Average);
                    
                    allSeries.Add(sampledMainSeries);

                    
                }
            }
        }

        var resdata = new ChartDataResults(ComputeSubseries(request, dateRange, allSeries));
        resdata.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = dynamicLoader.SetDynamicChartOptions(resdata);

        return CalculateMinMaxForYAxis(request, dynamicResult);
    }

	private static Array GetRowIds(ChartDataRequest request)
	{
        if (request.AllSettings.ContainsKey("RowIds"))
        {
            return (Array)request.AllSettings["RowIds"];
        }
        else // custom chart page.
        {
        	var rp = new Dictionary<string, object>(
        		(Dictionary<string, object>) request.AllSettings["ResourceProperties"], StringComparer.OrdinalIgnoreCase);
        	return (rp["RowIds"].ToString().Split(';'));
        }
	}

    private ChartDataResults CalculateMaxForYAxis(ChartDataResults result)
    {
        double max = 0;
        for (int i = 0; i < result.DataSeries.Count(); i++)
        {
            double tmpMax = result.DataSeries.ElementAt(i).Data.Where(item => !item.IsNullPoint).Select(item => item.Value).Max<double>();
            max = Math.Max(max, tmpMax);
        }

        dynamic options = new JsonObject();
        options.yAxis = JsonObject.CreateJsonArray(1);
        options.yAxis[0].min = 0;
        options.yAxis[0].max = Math.Max(0.1, max);

        result.ChartOptionsOverride = options;
        return result;
    }
}
