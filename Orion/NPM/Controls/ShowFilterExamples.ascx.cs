﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Controls_ShowFilterExamples : System.Web.UI.UserControl
{
	public string[] EntitiesList { get; set; }


	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		var listOfEntities = (Repeater)CollapsePanel1.CollapseBlock.FindControl("listOfEntities");
		listOfEntities.DataSource = EntitiesList;
		listOfEntities.DataBind();
	}

	protected void listOfEntities_onItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		var panel = (CollapsePanel)e.Item.FindControl("panel");
		var title = (Label)panel.PanelTitle.FindControl("title");

		using (var swql = InformationServiceProxy.CreateV3())
		{
			var entity = new Dictionary<string, object> {{"name", e.Item.DataItem.ToString()}};
			
			var properties = swql.Query("SELECT Name FROM Metadata.Property WHERE EntityName=@name", entity);

			var displayname = swql.Query("SELECT ISNULL(DisplayName, Name) as N FROM Metadata.Entity WHERE FullName=@name", entity);

			if (displayname.Rows.Count > 0)
			{
				title.Text = string.Format(Resources.NPMWebContent.NPMWEBCODE_VT0_40, displayname.Rows[0][0]);
				

				var propertyList = (Repeater)panel.CollapseBlock.FindControl("propertyList");
				propertyList.DataSource = properties;
				propertyList.DataBind();

			}
		}
	}
}