<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceList.ascx.cs" Inherits="Orion_Discovery_Wizard_Controls_InterfaceList" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Discovery" Assembly="OrionWeb" %>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {
    $(".CheckAllSelector input[type=checkbox]").click(function() {
        var checked_status = this.checked;
            $(".Selector input[type=checkbox]").each(function() {
                this.checked = checked_status;
            });
        });
    });
    //]]>
</script>

<div>
    <orion:InterfaceGridView ID="InterfaceTableGrid" runat="server" AutoGenerateColumns="False" GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow"  AllowSorting="true">
        <Columns>
            <asp:TemplateField ItemStyle-Width="20px">
                <HeaderTemplate>
                    <asp:CheckBox ID="CheckAllSelector" runat="server" class="CheckAllSelector" OnCheckedChanged="CheckAllSelector_OnCheckedChanged" OnPreRender="CheckAllSelector_OnPreRender" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="Selector" runat="server" Checked='<%# Eval("IsSelected") %>' CssClass="Selector" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Count" HeaderText="<%$ Resources: CoreWebContent,WEBDATA_AK0_159 %>"  ItemStyle-Width="50px" SortExpression="Count" />
            <asp:ImageField DataImageUrlField="Icon"  ItemStyle-Width="16px"></asp:ImageField>
            <asp:BoundField DataField="Description" HeaderText="<%$ Resources: CoreWebContent,WEBDATA_AK0_160 %>" SortExpression="Description" />
        </Columns>
    </orion:InterfaceGridView>
</div>




