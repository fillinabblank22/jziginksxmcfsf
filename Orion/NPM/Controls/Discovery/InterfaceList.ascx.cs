using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.NPM.Common.Models.Discovery;
using SolarWinds.Logging;
using SolarWinds.NPM.Web.Discovery;


public partial class Orion_Discovery_Wizard_Controls_InterfaceList : UserControl
{

    private static readonly Log log = new Log();

    protected void CheckAllSelector_OnCheckedChanged(object sender, EventArgs e)
    {
        DiscoveryResultsHelper.CheckAllInterfaceIsSelected =
            ((CheckBox)this.InterfaceTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked;
    }

    protected void CheckAllSelector_OnPreRender(object sender, EventArgs e)
    {
        ((CheckBox)this.InterfaceTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked =
            DiscoveryResultsHelper.CheckAllInterfaceIsSelected;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.InterfaceTableGrid.OnGetDataSource += InterfaceTableGrid_GetDataSource;
    }

    List<DiscoveryInterfaceTypeGroup> InterfaceTableGrid_GetDataSource()
    {
        return DiscoveryResultsHelper.DiscoveryInterfaceTypeList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // clear list of InterfaceTypeList in Session, because I can enter this page after action, which changed DiscoveredNodes collection
            if (ResultsWorkflowHelper.DiscoveryOnlyNotImportedResults)
            {
                DiscoveryResultsHelper.DiscoveryInterfaceTypeList = null;
            }

            CreateInterfaceTypeList();
            this.InterfaceTableGrid.DataSource = DiscoveryResultsHelper.DiscoveryInterfaceTypeList;
            this.InterfaceTableGrid.DataBind();
        }

        StoreSelectedInterfaceTypes();
    }

    /// <summary>
    /// Last flag for ignore list change.
    /// </summary>
    private Guid DiscoveryInterfaceTypeListIgnoreListChangeFlag
    {
        get
        {
            var value = Session["DiscoveryInterfaceTypeListIgnoreListChangeFlag"];

            if(value != null && value is Guid)
            {
                return (Guid) value;
            }
            else
            {
                return Guid.Empty;
            }
        }

        set { Session["DiscoveryInterfaceTypeListIgnoreListChangeFlag"] = value; }
    }

    /// <summary> Creates data object with interface types used for displaying table </summary>
    private void CreateInterfaceTypeList()
    {
        if (ResultsWorkflowHelper.DiscoveryResults == null)
        {
            log.Error("Unable to get Core discovery plugin result");
        }

        // we need types of interfaces
        var interfacesGroups = new List<DiscoveryInterfaceTypeGroup>();

        // let's process each result independently
        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {

            CoreDiscoveryPluginResult coreResult =
                discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();

            NPMDiscoveryPluginResult npmResult =
                discoveryResult.GetPluginResultOfType<NPMDiscoveryPluginResult>();

            if (coreResult == null)
            {
                log.Error("Unable to get Core discovery plugin result");
                return;
            }

            if (npmResult == null)
            {
                log.Error("Unable to get NPM discovery plugin result");
                return;
            }

            List<int> selectedNodeIDs =
                new List<int>(coreResult.DiscoveredNodes.Where(n => n.IsSelected).Select(n => n.NodeID));
            
            var groups = new List<DiscoveryInterfaceTypeGroup>(
                npmResult.DiscoveredInterfaces
                    // all interfaces refering to selected nodes
                    .Where(i => selectedNodeIDs.Contains(i.DiscoveredNodeID))
                    // group by volume type
                    .GroupBy(i => i.InterfaceType).Select(g => new DiscoveryInterfaceTypeGroup()
                                                                   {
                                                                       Type = g.Key,
                                                                       Icon =
                                                                           "/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Interfaces/" +
                                                                           g.First().InterfaceType + ".gif",
                                                                       Count = g.Count(),
                                                                       Description =
                                                                           npmResult.DiscoveredInterfaces.First(
                                                                               iface => iface.InterfaceType == g.Key).
                                                                           InterfaceTypeDescription,
                                                                       IsSelected = DiscoveryResultsHelper.IsDiscoveredInterfaceSelected(g.Key)
                                                                       //TODO I3 What interface types should be selected by default?
                                                                   })
                );

            interfacesGroups.AddRange(groups);
        }

        // here we must merge groups together
        var finalGroups = interfacesGroups
            .GroupBy(item => item.Type)
            // each group of groups create one unique group
            .Select(group =>
                    new DiscoveryInterfaceTypeGroup()
                        {
                            Type = group.Key,
                            Icon = group.First().Icon,
                            // icon is identical accross group
                            Count = group.Select(item => item.Count).Sum(),
                            Description = group.First().Description,
                            IsSelected = DiscoveryResultsHelper.IsDiscoveredInterfaceSelected(group.Key)
                        })
            .ToList();

        this.DiscoveryInterfaceTypeListIgnoreListChangeFlag = ResultsWorkflowHelper.IgnoreListChanges.ModifyFlag;
        DiscoveryResultsHelper.DiscoveryInterfaceTypeList = finalGroups;
    }

    /// <summary> Stores selected interface ttypes to Session property </summary>
    private void StoreSelectedInterfaceTypes()
    {
        for (int i = 0; i < InterfaceTableGrid.Rows.Count; i++)
        {
            Control item = InterfaceTableGrid.Rows[i];
            CheckBox checkBox = (CheckBox)item.FindControl("Selector");
            DiscoveryResultsHelper.DiscoveryInterfaceTypeList[i].IsSelected = checkBox.Checked;
        }

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            CoreDiscoveryPluginResult coreResult =
                discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();

            NPMDiscoveryPluginResult npmResult =
                discoveryResult.GetPluginResultOfType<NPMDiscoveryPluginResult>();

            List<int> selectedNodeIDs =
                new List<int>(coreResult.DiscoveredNodes.Where(n => n.IsSelected).Select(n => n.NodeID));

            // filter by selected nodes
            foreach (DiscoveredInterface iface in npmResult.DiscoveredInterfaces)
            {
                DiscoveryResultsHelper.SetInterfaceShouldBeSelected(iface, selectedNodeIDs.Contains(iface.DiscoveredNodeID));
            }

            // then remove interfaces of not selected types
            foreach (DiscoveryInterfaceTypeGroup ifaceTypeGroup in DiscoveryResultsHelper.DiscoveryInterfaceTypeList)
            {
                foreach (
                    DiscoveredInterface iface in
                        npmResult.DiscoveredInterfaces.Where(v => v.InterfaceType == ifaceTypeGroup.Type))
                {
                    //Only unselect those wose group is unselected, but do not touch those whose group is selected.
                    //They could be already unselected by node filter

                    DiscoveryResultsHelper.SetInterfaceFilteredOutByType(iface, !ifaceTypeGroup.IsSelected);
                    if (!ifaceTypeGroup.IsSelected)
                        iface.IsSelected = false;
                }

                DiscoveryResultsHelper.DiscoveredInterfaceSelected[ifaceTypeGroup.Type] = ifaceTypeGroup.IsSelected;
            }

            DiscoveryResultsHelper.UpdateIsSelectedForInterfaces(npmResult.DiscoveredInterfaces);
        }
    }
}
