﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Core.Common;

public partial class Orion_NPM_Controls_CustomPollerAxisControl : System.Web.UI.UserControl
{
    public event EventHandler SelectionChanged;
    public void InvokeSelectionChanged(EventArgs e)
    {
        EventHandler handler = SelectionChanged;
        if (handler != null) handler(this, e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            // clear cache
            Session.Remove(SessionKey);

            // add first by default
            if (AddFirstByDefault && SelectedUnDPs.Count == 0)
            {
                AddUnDP();
            }
        }
    }

    public bool AddFirstByDefault { get; set; }

    public string AddButtonText
    {
        set { AddUnDPButton.Text = value; }
    }

    public bool AddButtonVisible
    {
        set { AddButtonArea.Visible = value; }
    }

    public string AxisLabel
    {
        set { Axis.Text = value; }
    }

    public string AxisHintLabel
    {
        set { AxisHint.Text = value; }
    }

    public string NetObjectPrefix
    {
        get
        {
            return ViewState["NetObjectPrefix"].ToString();
        }
        set
        {
            ViewState["NetObjectPrefix"] = value;
        }
    }

    private string SessionKey
    {
        get { return "AvailableUnDPs:" + NetObjectPrefix; }
    }

    public DataTable AvailableUnDPs
    {
        get
        {
            var value = LoadFromCache(SessionKey);
            if (value == null)
            {
                value = LoadCustomPollers(NetObjectPrefix);
                
                // first default value before the actual selection
                var emptyRow = value.NewRow();
                emptyRow[1] = Resources.NPMWebContent.NPMWEBDATA_ZB0_25;
                value.Rows.InsertAt(emptyRow, 0);
                
                Session[SessionKey] = value;
            }

            return value;
        }
    }

    private static DataTable LoadCustomPollers(string netObjectPrefix)
    {
        string query = string.Format(@"
SELECT c.CustomPollerID, c.UniqueName
FROM Orion.NPM.CustomPollers c
WHERE c.IncludeHistoricStatistics = 'true' AND c.NetObjectPrefix = '{0}'
ORDER BY c.UniqueName ASC", netObjectPrefix);

        DataTable results;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            results = swis.Query(query);
        }

        return results;
    }

    public List<string> SelectedUnDPs
    {
        get
        {
            return LoadFromCache() ?? new List<string>();
        }
        set
        {
            ViewState["SelectedUnDPs"] = value;
            RepeaterDataBind();
        }
    }

    private List<string> LoadFromCache()
    {
        return ViewState["SelectedUnDPs"] as List<string>;
    }

    private DataTable LoadFromCache(string sessionKey)
    {
        return Session[sessionKey] as DataTable;
    }

    private void RepeaterDataBind()
    {
        Repeater.DataSource = SelectedUnDPs;
        Repeater.DataBind();
    }

    protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var d = e.Item.FindControl("UnDP") as DropDownList;
        if (d != null)
        {
            d.DataSource = AvailableUnDPs;
            d.DataBind();
            d.SelectedValue = e.Item.DataItem.ToString();
        }
    }

    protected void UnDP_SelectedIndexChanged(object sender, EventArgs e)
    {
        var d = sender as DropDownList;
        if (d != null)
        {
            // update the property selection
            var id = d.SelectedValue;
            var index = (d.NamingContainer is RepeaterItem) ? ((RepeaterItem) d.NamingContainer).ItemIndex : -1;
            if (index > -1 && index < SelectedUnDPs.Count)
                SelectedUnDPs[index] = id;
        }
    }

    protected void Button_Click(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Add":
                AddUnDP();
                break;
            case "Remove":
                RemoveUnDP(e.CommandArgument);
                break;
        }

        InvokeSelectionChanged(e);
    }

    private void RemoveUnDP(object commandArgument)
    {
        // item index within the repeater
        short index;
        if (Int16.TryParse(commandArgument != null ? commandArgument.ToString() : string.Empty, out index)
            && (index > -1 && index < SelectedUnDPs.Count))
        {
            SelectedUnDPs.RemoveAt(index);
            RepeaterDataBind();
        }
    }

    private void AddUnDP()
    {
        SelectedUnDPs.Add(string.Empty);
        RepeaterDataBind();
    }
}