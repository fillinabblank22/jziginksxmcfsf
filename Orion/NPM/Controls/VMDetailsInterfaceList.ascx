<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMDetailsInterfaceList.ascx.cs" Inherits="Orion_NPM_Controls_VMDetailsInterfaceList" %>

<table cellpadding="0" cellspacing="0"> 
    <thead>
        <tr align="left">
            <th colspan="3" align="left">
                &nbsp;<%=Resources.NPMWebContent.NPMWEBDATA_VB0_108%>
            </th>
            <th>
                <%=Resources.NPMWebContent.NPMWEBDATA_VB0_71%></th>
            <th>
                <%=Resources.NPMWebContent.NPMWEBDATA_VB0_65%></th>
            <th>
                <%=Resources.NPMWebContent.NPMWEBDATA_VB0_69%></th>
        </tr>
    </thead>
    <asp:Repeater runat="server" ID="repVMDetails">
        <ItemTemplate>
            <tr>
                <td><img src="/Orion/images/StatusIcons/Small-<%#Eval("StatusLED")%>"></td>
                <td align=left><%# SolarWinds.Orion.Web.DisplayTypes.Status.FromStringInt(Eval("Status").ToString()).ToLocalizedString() %></td>
                <td><img src="/NetPerfMon/images/Interfaces/<%#Eval("InterfaceIcon")%>"></td>
                <td>
                    <a href='../View.aspx?NetObject=I:<%#Eval("InterfaceID")%>'><%#Eval("Caption")%></a>
                </td>
                <td>
                    <%# Convert.ToDouble(Eval("OutPercentUtil")) >= 0.0 ? Eval("OutPercentUtil").ToString() + " %" : "&nbsp;"%>
                    </td>
                <td>
                    <%# Convert.ToDouble(Eval("InPercentUtil")) >= 0.0 ? Eval("InPercentUtil").ToString() + " %" : "&nbsp;"%>
                    </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</table>