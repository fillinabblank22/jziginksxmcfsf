<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollers.ascx.cs" Inherits="Orion_Nodes_Controls_CustomPollers" %>
<%@ Assembly Name="Infragistics2.WebUI.Shared.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igNav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics2.WebUI.UltraWebNavigator.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"%>

<ignav:ultrawebtree runat="server" id="uwtPollers" checkboxes="true"> 
    <Images>
        <CollapseImage Url="/Orion/images/Button.Collapse.GIF" />
        <ExpandImage Url="/Orion/images/Button.Expand.GIF" />
    </Images>
</ignav:ultrawebtree>