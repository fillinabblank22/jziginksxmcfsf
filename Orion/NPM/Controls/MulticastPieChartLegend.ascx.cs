﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_NPM_Controls_MulticastPieChartLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "MulticastPieChartLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}