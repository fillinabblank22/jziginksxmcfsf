using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.NPM.Web.UI;

public partial class TopXXControl : System.Web.UI.UserControl
{
    public bool Enabled
    {
        get
        {
            return topXXEnabled.Checked;
        }

        set
        {
            topXXEnabled.Checked = value;
        }
    }

    public String Value
    {
        get
        {
            return topXXValue.SelectedValue;
        }

        set
        {
            topXXValue.SelectedValue = value;
        }
    }

    private void SetupControlFromRequest()
    {
        string resourceID = this.Request["ResourceID"];
        ResourceInfo resource = new ResourceInfo();
        resource.ID = Convert.ToInt32(resourceID);

        if (!String.IsNullOrEmpty(resource.Properties["TopXX"]))
        {
            Enabled = true;
            Value = resource.Properties["TopXX"];
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            SetupControlFromRequest();
        }
    }
}

