﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using SolarWinds.NPM.Web.UI;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;

public partial class Orion_NPM_Controls_Routing_RoutingEditNodeProperties : BaseNodeEditPlugin, INodePropertyPlugin
{
    protected override HtmlGenericControl MainSectionElement
    {
        get { return RoutingSection; }
    }

    protected override IList<SimplePropertyControl> GetSimplePropertyControlsList(IList<Node> editedNodes)
    {
        var supportedNodes = GetSupportedNodesWithPollerType(editedNodes);

        return new List<SimplePropertyControl>
        {
            new SimplePropertyControl
            {
                SettingId = "NPM_Settings_Routing_Neighbor_PollInterval",
                NodeSettingId = "NPM.RoutingNeighborPollInterval",
                ContainerControl = Neighbor,
                ApplyToAllNodesControl = ApplyRoutingNeighborPollInterval,
                OverrideValueControl = OverrideRoutingNeighborPollInterval,
                InputControl = RoutingNeighborPollInterval,
                RangeValidator = RangeValidatorRoutingNeighborPollInterval,
                SupportedNodeIds = supportedNodes.Where(n => n.Item2.StartsWith("N.RoutingNeighbor.")).Select(n => n.Item1).Distinct().ToList()
            },
            new SimplePropertyControl
            {
                SettingId = "NPM_Settings_Routing_RouteTable_PollInterval",
                NodeSettingId = "NPM.RoutingTablePollInterval",
                ContainerControl = RouteTable,
                ApplyToAllNodesControl = ApplyRouteTablePollInterval,
                OverrideValueControl = OverrideRouteTablePollInterval,
                InputControl = RouteTablePollInterval,
                RangeValidator = RangeValidatorRouteTablePollInterval,
                SupportedNodeIds = supportedNodes.Where(n => n.Item2.StartsWith("N.Routing.") && n.Item2.EndsWith("RoutingTable")).Select(n => n.Item1).Distinct().ToList()
            },
            new SimplePropertyControl
            {
                SettingId = "NPM_Settings_Routing_VRF_PollInterval",
                NodeSettingId = "NPM.VRFPollInterval",
                ContainerControl = VRF,
                ApplyToAllNodesControl = ApplyVRFPollInterval,
                OverrideValueControl = OverrideVRFPollInterval,
                InputControl = VRFPollInterval,
                RangeValidator = RangeValidatorVRFPollInterval,
                SupportedNodeIds = supportedNodes.Where(n => n.Item2.StartsWith("N.VRFRouting")).Select(n => n.Item1).Distinct().ToList()
            }
        };
    }
}
