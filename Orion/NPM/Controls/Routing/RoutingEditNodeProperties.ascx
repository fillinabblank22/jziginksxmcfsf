﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RoutingEditNodeProperties.ascx.cs" Inherits="Orion_NPM_Controls_Routing_RoutingEditNodeProperties" %>
<div class="contentBlock" runat="server" id="RoutingSection">
    <div class="contentBlockHeader" runat="server">
        <table>
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal runat="server" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_22 %>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr runat="server" ID="Neighbor">
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyRoutingNeighborPollInterval" AutoPostBack="true" OnCheckedChanged="ApplyProperty_CheckedChanged" />

                <asp:Literal runat="server" ID="LabelRoutingNeighborPollInterval" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_21 %>"/>
            </td>
            <td class="rightInputColumn">
                <asp:CheckBox runat="server" ID="OverrideRoutingNeighborPollInterval" AutoPostBack="true" OnCheckedChanged="OverrideProperty_CheckedChanged"/>

                <asp:Literal runat="server" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_20 %>"/>
                
                <div runat="server" ID="SettingsRoutingNeighborPollInterval">
                    <asp:TextBox runat="server" ID="RoutingNeighborPollInterval"/>
                    
                    <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_MZ0_3 %>" />

                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RequiredValidatorRoutingNeighborPollInterval"
                        ControlToValidate="RoutingNeighborPollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_27 %>"/>
                    <asp:RegularExpressionValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RegexValidatorRoutingNeighborPollInterval"
                        ControlToValidate="RoutingNeighborPollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_28 %>"
                        ValidationExpression="^\d+$"/>
                    <asp:RangeValidator runat="server" Display="Dynamic" SetFocusOnError="True" Type="Integer"
                        ID="RangeValidatorRoutingNeighborPollInterval"
                        ControlToValidate="RoutingNeighborPollInterval"/>
                </div>
            </td>
        </tr>
        <tr runat="server" ID="RouteTable">
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyRouteTablePollInterval" AutoPostBack="true" OnCheckedChanged="ApplyProperty_CheckedChanged" />

                <asp:Literal runat="server" ID="LabelRouteTablePollInterval" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_23 %>"/>
            </td>
            <td class="rightInputColumn">
                <asp:CheckBox runat="server" ID="OverrideRouteTablePollInterval" AutoPostBack="true" OnCheckedChanged="OverrideProperty_CheckedChanged"/>

                <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_20 %>"/>
                
                <div runat="server" ID="SettingsRouteTablePollInterval">
                    <asp:TextBox runat="server" ID="RouteTablePollInterval"/>
                    
                    <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_MZ0_3 %>" />

                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RequiredValidatorRouteTablePollInterval"
                        ControlToValidate="RouteTablePollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_27 %>"/>
                    <asp:RegularExpressionValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RegexValidatorRouteTablePollInterval"
                        ControlToValidate="RouteTablePollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_28 %>"
                        ValidationExpression="^\d+$"/>
                    <asp:RangeValidator runat="server" Display="Dynamic" SetFocusOnError="True" Type="Integer"
                        ID="RangeValidatorRouteTablePollInterval"
                        ControlToValidate="RouteTablePollInterval"/>
                </div>
            </td>
        </tr>
        <tr runat="server" ID="VRF">
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyVRFPollInterval" AutoPostBack="true" OnCheckedChanged="ApplyProperty_CheckedChanged" />

                <asp:Literal runat="server" ID="LabelVRFPollInterval" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_25 %>"/>
            </td>
            <td class="rightInputColumn">
                <asp:CheckBox runat="server" ID="OverrideVRFPollInterval" AutoPostBack="true" OnCheckedChanged="OverrideProperty_CheckedChanged"/>

                <asp:Literal runat="server" ID="Literal2" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_20 %>"/>
        
                <div runat="server" ID="SettingsVRFPollInterval">
                    <asp:TextBox runat="server" ID="VRFPollInterval"/>
                    
                    <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_MZ0_3 %>" />

                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RequiredValidatorVRFPollInterval"
                        ControlToValidate="VRFPollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_27 %>"/>
                    <asp:RegularExpressionValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RegexValidatorVRFPollInterval"
                        ControlToValidate="VRFPollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_28 %>"
                        ValidationExpression="^\d+$"/>
                    <asp:RangeValidator runat="server" Display="Dynamic" SetFocusOnError="True" Type="Integer"
                        ID="RangeValidatorVRFPollInterval"
                        ControlToValidate="VRFPollInterval"/>
                </div>
            </td>
        </tr>
    </table>
</div>