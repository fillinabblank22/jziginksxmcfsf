using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.NPM.Web.UI;
using System.IO;
using System.Xml;
using SolarWinds.NPM.Web.Charting;
using SolarWinds.Orion.Web.Charting.MultipleSourceCharts;

public partial class ListCharts : System.Web.UI.UserControl
{
    private string selectedChart = String.Empty;

    public String EntityName
    {
        get
        {
            if (ViewState["EntityName"] == null)
            {
                EntityName = "Orion.Nodes";
            }

            return ViewState["EntityName"].ToString();
        }

        set
        {
            ViewState["EntityName"] = value;
        }
    }

    public String SelectedChart
    {
        get
        {
            return Charts.SelectedValue;
        }

        set
        {
            selectedChart = value;
        }
    }

    public string ParentChartInfoName
    {
        get
        {
            if (ViewState["ParentChartInfoName"] == null)
            {
                ParentChartInfoName = "SolarWinds.NPM.Web.Charting.AggregateChartInfo, SolarWinds.NPM.Web";
            }

            return ViewState["ParentChartInfoName"].ToString();
        }

        set
        {
            ViewState["ParentChartInfoName"] = value;
        }
    }

    protected List<EntityChartInfo> ChartList
    {
        get
        {
            if (ViewState["Charts"] == null)
            {
                ChartList = LoadChartsFromConfig();
            }

            return (List<EntityChartInfo>)ViewState["Charts"];
        }

        set
        {
            ViewState["Charts"] = value;
        }
    }


	private void SetupControlFromRequest()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);

		if (!string.IsNullOrEmpty(resource.Properties["ChartName"]))
		{
            SelectedChart = resource.Properties["ChartName"];
		}
        
        if (!string.IsNullOrEmpty(resource.Properties["EntityName"]))
        {
            EntityName = resource.Properties["EntityName"];
        }
	}

	protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);

        if (!IsPostBack)
		{
            SetupControlFromRequest();
		}
	}

    public void ReloadList()
    {
        Charts.Items.Clear();

        if (ChartList != null && ChartList.Count > 0)
        {
            foreach (EntityChartInfo info in ChartList)
            {
                if (info.EntityType == EntityName)
                {
                    var item = new ListItem(info.DisplayName, info.ChartName);
                    item.Selected = (info.ChartName == selectedChart);
                    Charts.Items.Add(item);
                }
            }
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            ReloadList();
        }
    }

    [Serializable]
    protected class EntityChartInfo
    {
        public string EntityType { get; set; }
        public string ChartName { get; set; }
        public string DisplayName { get; set; }
    }

    protected List<EntityChartInfo> LoadChartsFromConfig()
    {
        string path = HttpContext.Current.Server.MapPath("/Orion/NPM/npm.config");

        var charts = new List<EntityChartInfo>();

        if (File.Exists(path))
        {
            XmlDocument doc = new XmlDocument();
            TokenSubstitution.LoadXml(doc, path);

            XmlNode node = doc.SelectSingleNode("configuration/chartInfoPlugins");

            if (node != null)
            {
                XmlNodeList nodeList = node.SelectNodes("chartInfoType");

                foreach (XmlNode chartInfoNode in nodeList)
                {
                    string typeName = chartInfoNode.Attributes["chartInfoType"].Value;
                    string chartName = chartInfoNode.Attributes["chartName"].Value;

                    Type chartInfoType = Type.GetType(typeName);
                    Type parentInfoType = Type.GetType(ParentChartInfoName);

                    if (chartInfoType != null && parentInfoType != null)
                    {
                        //MultiSourceChartInfo
                        if (parentInfoType.IsAssignableFrom(chartInfoType))
                        {
                            string displayName = chartInfoNode.Attributes["displayName"].Value;
                            string entityType = chartInfoNode.Attributes["entityType"].Value;

                            charts.Add(new EntityChartInfo()
                            {
                                ChartName = chartName,
                                DisplayName = displayName,
                                EntityType = entityType
                            });
                        }
                    }
                }
            }
        }

        return charts;
    }
}
