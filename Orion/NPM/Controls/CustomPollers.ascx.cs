using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.ComponentModel;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Nodes_Controls_CustomPollers : System.Web.UI.UserControl
{
	private readonly string CustomPollersKey = "CustomPollers";
	private readonly string CustomPollersKeyOld = "CustomPollersOld";
	private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
	private string _blerrortext = string.Empty;
	[Browsable(true)]
	[Category("Misc")]
	public string NetworkObjectType
	{
		get
		{
			object o = ViewState["NetworkObjectType"];
			return (o == null ? string.Empty : ViewState["NetworkObjectType"].ToString());
		}
		set
		{
			ViewState["NetworkObjectType"] = value;
		}
	}

	public string LastBLErrorText
	{
		get
		{
			return _blerrortext;
		}
		set 
		{
			 _blerrortext = value;
		}

	}


	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected void grvCustomPollers_Sorting(object sender, GridViewSortEventArgs e)
	{
		UpdateInMemoryNodeInformation();
	}

	protected string Direction
	{
		get
		{
			object o = ViewState["SortDirection"];
			return (o == null ? "asc" : o.ToString());
		}
		set
		{
			ViewState["SortDirection"] = value;
		}
	}

	public void UpdateInMemoryNodeInformation()
	{
		if (!SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo.ContainsKey(CustomPollersKey))
			SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo.Add(CustomPollersKey, new Dictionary<Guid, CustomPoller>());

		foreach (Infragistics.WebUI.UltraWebNavigator.Node mainNode in uwtPollers.Nodes)
		{
			foreach (Infragistics.WebUI.UltraWebNavigator.Node node in mainNode.Nodes)
			{
				if (node.Checked)
				{
					((Dictionary<Guid, CustomPoller>)SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo[CustomPollersKey])[new Guid(node.Tag.ToString())] = null;
				}
				else
				{
					Guid customPollerId = new Guid(node.Tag.ToString());
					if (((Dictionary<Guid, CustomPoller>)SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo[CustomPollersKey]).ContainsKey(customPollerId))
					{
						((Dictionary<Guid, CustomPoller>)SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo[CustomPollersKey]).Remove(customPollerId);
					}
				}
			}
		}
	}

	private static string TruncateString(string inputString, int symbolsToRemain)
	{
		string result = inputString;
		if (inputString.Length > symbolsToRemain)
			result = inputString.Substring(0, symbolsToRemain) + "...";
		return result;
	}

	public void LoadCustomPollers()
	{
		CustomPollers pollers;
		using (var proxy = SolarWinds.NPM.Common.NPMBusinessLayerProxyFactory.Instance.Create())
		{
			_blerrortext = string.Empty;
            try
            {
                pollers = proxy.Api.GetAllCustomPollersSorted(Direction.ToString(), this.NetworkObjectType.Substring(0, 1));
            }
            catch(Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }
		}

		uwtPollers.Nodes.Clear();
		System.Collections.Specialized.StringCollection groups = new System.Collections.Specialized.StringCollection();
		foreach (CustomPoller poller in pollers)
		{
			string groupName = EmptyToDefaultGroup(poller.GroupName);
			if (!groups.Contains(groupName))
				groups.Add(groupName);
		}
		
		foreach (string groupName in groups)
		{
			Infragistics.WebUI.UltraWebNavigator.Node node = new Infragistics.WebUI.UltraWebNavigator.Node(WebSecurityHelper.HtmlEncode(groupName),"", "", "", "");			
			node.CheckBox = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.False;

			uwtPollers.Nodes.Add(node);

			foreach (CustomPoller poller in pollers)
			{
				if (EmptyToDefaultGroup(poller.GroupName) == groupName)
				{
					Infragistics.WebUI.UltraWebNavigator.Node childNode = new Infragistics.WebUI.UltraWebNavigator.Node(
						string.Format("{0} ({1})", WebSecurityHelper.HtmlEncode(poller.UniqueName), TruncateString(WebSecurityHelper.HtmlEncode(poller.Description), 65)),
						poller.ID.ToString(), "", "", "");

					node.Nodes.Add(childNode);
					if (SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo.ContainsKey(CustomPollersKey))
						foreach (Guid pollerId in ((Dictionary<Guid, CustomPoller>)SolarWinds.Orion.Web.NodeWorkflowHelper.NewNodeInfo[CustomPollersKey]).Keys)
						{
							if (pollerId.Equals(poller.ID))
							{
								childNode.Checked = true;
							}
						}
				}
			}
		}
	}

	private string EmptyToDefaultGroup(string groupName)
	{
		if (string.IsNullOrEmpty(groupName))
			return Resources.NPMWebContent.NPMWEBCODE_TM0_37;
		else
			return groupName;
	}
	
	public void AssignCustomPollers(List<int> netObjectsIds)
	{
		UpdateInMemoryNodeInformation();
		Dictionary<Guid, CustomPoller> selectedCustomPollers = (Dictionary<Guid, CustomPoller>)NodeWorkflowHelper.NewNodeInfo[CustomPollersKey];

		List<Guid> customPollersIds = new List<Guid>();

		customPollersIds.AddRange(selectedCustomPollers.Keys);

		using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
		{
			_blerrortext = string.Empty;
            try
            {
                if (NetworkObjectType.Equals("Node", StringComparison.InvariantCultureIgnoreCase))
                    proxy.Api.AssignCustomPollersToNodes(netObjectsIds, customPollersIds);
                if (NetworkObjectType.Equals("Interface", StringComparison.InvariantCultureIgnoreCase))
                    proxy.Api.AssignCustomPollersToInterfaces(netObjectsIds, customPollersIds);
            }
            catch(Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

			if (NodeWorkflowHelper.NewNodeInfo.ContainsKey(CustomPollersKeyOld))
			{
				Dictionary<Guid, CustomPoller> oldSelectedCustomPollers = (Dictionary<Guid, CustomPoller>)NodeWorkflowHelper.NewNodeInfo[CustomPollersKeyOld];
				foreach (Guid id in selectedCustomPollers.Keys)
				{
					oldSelectedCustomPollers.Remove(id);
				}

				foreach (Guid id in oldSelectedCustomPollers.Keys)
				{
                    try
                    {
                        if (NodeWorkflowHelper.MultiSelectedInterfaceIds != null && NodeWorkflowHelper.MultiSelectedInterfaceIds.Count > 0)
                            proxy.Api.RemoveCustomPollerAssignmentsFromInterfaces(NodeWorkflowHelper.MultiSelectedInterfaceIds, id);

                        if (NodeWorkflowHelper.MultiSelectedNodeIds != null && NodeWorkflowHelper.MultiSelectedNodeIds.Count > 0)
                            proxy.Api.RemoveCustomPollerAssignmentsFromNodes(NodeWorkflowHelper.MultiSelectedNodeIds, id);
                    }
                    catch(Exception ex)
                    {
                        BusinessLayerExceptionHandler(ex);
                        throw;
                    }
				}

				NodeWorkflowHelper.NewNodeInfo.Remove(CustomPollersKeyOld);
			}
		}
		NodeWorkflowHelper.NewNodeInfo.Remove(CustomPollersKey);
	}

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
		_blerrortext = ex.Message;
	}
}