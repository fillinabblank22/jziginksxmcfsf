﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SingleMemberSwitch.ascx.cs" Inherits="Orion_NPM_Controls_SwitchStack_SingleMemberSwitch" %>

<div class="sw-suggestion sw-suggestion-warn <%= CssClasses%>">
    <span class="sw-suggestion-icon"></span>
     <%= Resources.NPMWebContent.NPMWEBDATA_JP1_29%>
    <a class="coloredLink" href="<%=HelpLink%>" target="_blank">&nbsp;&raquo;&nbsp;<%=Resources.NPMWebContent.NPMWEBDATA_RB0_19 %></a>
</div>
