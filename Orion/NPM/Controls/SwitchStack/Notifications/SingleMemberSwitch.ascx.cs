﻿using System;
using System.Data;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NPM_Controls_SwitchStack_SingleMemberSwitch: System.Web.UI.UserControl
{
    public string SwqlQuery { get; set; }

    public string CssClasses { get; set; }

    protected string HelpLink
    {
        get { return HelpHelper.GetHelpUrl("orionphresourcestackmembers"); }
    }
    
    public void AssingNodeId(int nodeId)
    {
        SwqlQuery = string.Format("SELECT MemberCount AS cnt FROM Orion.NPM.SwitchStack WHERE NodeId = {0} AND RingFailure = 0", nodeId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Visible = ShouldBeVisible();
    }

    private bool ShouldBeVisible()
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            DataTable results = swis.Query(SwqlQuery);
            return results != null && results.Rows != null && results.Rows.Count > 0 && results.Rows[0]["cnt"].Equals(1);
        }
    }
}