using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using System.Web.UI;
using System.Linq;

public partial class CustomPollerUserControl : System.Web.UI.UserControl
{
    private static readonly Log _log = new Log();

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }
}

public partial class CustomPollersControl : CustomPollerUserControl
{
    private CustomPollers _list;
    private bool _isDetails = true;
    private int _netObjectID = 0;
    private string _netObject = string.Empty;
    private string _customPollerValue;
    private bool _hidePoller;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var func = @" function SelectAllRows() {
							var elem = document.getElementById('" + SelectALL.ClientID + @"');
							eds = document.getElementsByTagName('INPUT');
							for (i = 0; i < eds.length; i++) {
								if (eds[i].parentNode != null && eds[i].parentNode.getAttribute('SelectAllGroup') != null) {
									eds[i].checked = elem.checked;
								}
							}
						}

						function SetUpSelectAllCb() {
							var elem = document.getElementById('" + SelectALL.ClientID + @"');

							var chkBoxList = document.getElementById('" + RowIDList.ClientID + @"');
							var chkBoxCount = chkBoxList.getElementsByTagName('input');

                            var makeChecked = elem.checked == false;

							for (var i = 0; i < chkBoxCount.length; i++) {
								if (!chkBoxCount[i].checked) {
									elem.checked = false;
                                    makeChecked = false;
									break;
								}
							}

                            if(makeChecked)
                            {
                                elem.checked = true;
                            }
						}

						function getPollerRowsCheckedItems(el) {
							var result = '';
							$(el).parent().parent().parent().parent().children().each(function () {
								var inp = $(this).find('input');
								if (inp != null)
									result += (inp[0].checked ? inp.parent().attr('key') + ';' : '');
							});
							return result;	
						}
						
						try{
							PageMethods.SaveData('CustomPollerID', $('#" + CustomPollerList.ClientID + @"').val());
						}catch(ex){}";
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key_getPollerRowsCheckedItems", func, true);
        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            CustomPollerList.Attributes.Add("onchange", "javascript:SaveData('CustomPollerID', this.value);");
        }


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        foreach (ListItem item in RowIDList.Items)
        {
            item.Attributes["SelectAllGroup"] = "true";
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (ListChartsContainer != null)
        {
            ListChartsContainer.Visible = true;


            if (!string.IsNullOrEmpty(CustomPollerList.SelectedValue))
            {
                Guid guid = new Guid(CustomPollerList.SelectedValue);
                using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
                {
                    CustomPoller poller;
                    try
                    {
                        poller = proxy.Api.GetCustomPoller(guid);
                    }
                    catch (Exception ex)
                    {
                        BusinessLayerExceptionHandler(ex);
                        throw;
                    }
                    if (poller != null)
                    {
                        ListChartsContainer.Visible = (poller.PollerType == CustomPollerType.Counter);
                    }
                }
            }
        }
    }

    private void SetupControl(INPMBusinessLayer proxy)
    {
        TabularPollerDetails.Visible = false;

        CustomPollerList.DataSource = _list;
        CustomPollerList.DataTextField = "UniqueName";
        CustomPollerList.DataValueField = "CustomPollerID";
        CustomPollerList.DataBind();

        string resourceID = this.Request["ResourceID"];
        ResourceInfo resource = new ResourceInfo();
        resource.ID = Convert.ToInt32(resourceID);

        hf_netObjectID.Value = _netObject + _netObjectID.ToString();

        if (!string.IsNullOrEmpty(resource.Properties["CustomPollerID"]) && !IsPostBack)
            CustomPollerID = resource.Properties["CustomPollerID"];

        if (!string.IsNullOrEmpty(CustomPollerList.SelectedValue))
        {
            Guid guid = new Guid(CustomPollerList.SelectedValue);
            CustomPoller poller;
            try
            {
                poller = proxy.GetCustomPoller(guid);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            if (poller != null && _isDetails)
            {
                TabularPollerDetails.Visible = true;
                InitRowIDList();

                string rowProperty = string.Empty;

                if (!string.IsNullOrEmpty(Request["NetObject"]))
                    rowProperty = Request["NetObject"] + ":Rows";
                else if (!string.IsNullOrEmpty(_netObject))
                    rowProperty = _netObject + _netObjectID.ToString() + ":Rows";

                if (Session["EmbeddedResourceData"] != null && IsPostBack && Session["EmbeddedResourceData"] is Dictionary<string, object> && (Session["EmbeddedResourceData"] as Dictionary<string, object>).ContainsKey(rowProperty))
                {
                    var rows = (Session["EmbeddedResourceData"] as Dictionary<string, object>)[rowProperty].ToString().Split(';').ToList();

                    foreach (ListItem item in RowIDList.Items)
                    {
                        item.Selected = rows.Contains(item.Value);
                    }
                }
                else if (!string.IsNullOrEmpty(resource.Properties[rowProperty]))
                {
                    IList<string> rows = new List<string>(resource.Properties[rowProperty].Split(';'));
                    foreach (ListItem item in RowIDList.Items)
                    {
                        item.Selected = rows.Contains(item.Value);
                    }
                }
                else
                {
                    foreach (ListItem item in RowIDList.Items)
                        item.Selected = true;
                }
            }
            else
            {
                RowIDList.Items.Clear();
                TabularPollerDetails.Visible = false;
            }
        }

        int count = 0;
        foreach (ListItem item in RowIDList.Items)
        {
            if (item.Selected)
                count++;
        }

        SelectALL.Checked = (count == RowIDList.Items.Count);
    }

    #region Get Enabled Custom Pollers
    #region Get All Enabled Custom Pollers
    public void InitInterfaceCustomPollerList(int interfaceId)
    {
        _netObject = NetObjectHelper.Interface;
        _netObjectID = interfaceId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetInterfaceCustomPollers(interfaceId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }
            SetupControl(proxy.Api);
        }
    }

    public void InitNodeCustomPollerList(int nodeId)
    {
        _netObject = NetObjectHelper.Node;
        _netObjectID = nodeId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetNodeCustomPollers(nodeId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }
            SetupControl(proxy.Api);
        }
    }

    public void InitNodeInterfaceCustomPollerList(int nodeId)
    {
        _netObject = NetObjectHelper.Node;
        _netObjectID = nodeId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetNodeInterfaceCustomPollers(nodeId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            SetupControl(proxy.Api);
        }
    }

    public void InitSummaryCustomPollerList()
    {
        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetSummaryCustomPollers();
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            SetupControl(proxy.Api);
        }
    }
    #endregion

    #region Get Counter Enabled Custom Pollers
    public void InitInterfaceCounterCustomPollerList(int interfaceId)
    {
        _netObject = NetObjectHelper.Interface;
        _netObjectID = interfaceId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetInterfaceCounterCustomPollers(interfaceId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            SetupControl(proxy.Api);
        }
    }

    public void InitNodeCounterCustomPollerList(int nodeId)
    {
        _netObject = NetObjectHelper.Node;
        _netObjectID = nodeId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetNodeCounterCustomPollers(nodeId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            SetupControl(proxy.Api);
        }
    }

    public void InitNodeInterfaceCounterCustomPollerList(int nodeId)
    {
        _netObject = NetObjectHelper.Node;
        _netObjectID = nodeId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetNodeInterfaceCounterCustomPollers(nodeId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            SetupControl(proxy.Api);
        }
    }

    public void InitSummaryCounterCustomPollerList()
    {
        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetSummaryCounterCustomPollers();
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            SetupControl(proxy.Api);
        }
    }
    #endregion

    #region Get Enabled Tabular Custom Pollers
    public void InitInterfaceTabularCustomPollerList(int interfaceId)
    {
        _netObject = NetObjectHelper.Interface;
        _netObjectID = interfaceId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetInterfaceTabularCustomPollers(interfaceId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            _isDetails = false;
            SetupControl(proxy.Api);
        }
    }

    public void InitNodeTabularCustomPollerList(int nodeId)
    {
        _netObject = NetObjectHelper.Node;
        _netObjectID = nodeId;

        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetNodeTabularCustomPollers(nodeId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            _isDetails = false;
            SetupControl(proxy.Api);
        }
    }

    public void InitSummaryTabularCustomPollerList()
    {
        _list = new CustomPollers();
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            try
            {
                _list = proxy.Api.GetSummaryTabularCustomPollers();
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            _isDetails = false;
            SetupControl(proxy.Api);
        }
    }
    #endregion
    #endregion Get Enabled Custom Pollers

    public string CustomPollerName
    {
        get { return CustomPollerList.Items[CustomPollerList.SelectedIndex].Text; }
    }

    public string CustomPollerID
    {
        get
        {
            return CustomPollerList.SelectedValue;
        }
        set
        {
            CustomPollerList.SelectedValue = value;
            _customPollerValue = value;
            InitRowIDList();
        }
    }

    public string CustomPollerRows
    {
        get
        {
            if (TabularPollerDetails.Visible)
            {
                string rows = string.Empty;
                string comma = string.Empty;

                if (this.SelectALL.Checked)
                    return string.Empty;

                foreach (ListItem item in RowIDList.Items)
                {
                    if (item.Selected)
                    {
                        rows += comma + item.Value;
                        comma = ";";
                    }
                }
                return rows;
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            if (TabularPollerDetails.Visible)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    foreach (ListItem item in RowIDList.Items)
                    {
                        item.Selected = false;
                        SelectALL.Checked = false;
                    }

                    string[] rows = value.Split(';');
                    foreach (string row in rows)
                    {
                        if (RowIDList.Items.IndexOf(RowIDList.Items.FindByValue(row)) >= 0)
                        {
                            ListItem item = RowIDList.Items.FindByValue(row);
                            if (item != null)
                            {
                                item.Selected = true;
                            }
                        }
                    }

                    int count = 0;
                    foreach (ListItem item in RowIDList.Items)
                    {
                        if (item.Selected)
                        {
                            count++;
                        }
                    }

                    if (count == RowIDList.Items.Count)
                    {
                        SelectALL.Checked = true;
                    }
                    else
                    {
                        SelectALL.Checked = false;
                    }
                }
                else
                {
                    foreach (ListItem item in RowIDList.Items)
                    {
                        item.Selected = true;
                        SelectALL.Checked = true;
                    }
                }
            }
        }
    }

    public bool IsValid
    {
        get
        {
            if (TabularPollerDetails.Visible)
            {
                if (this.SelectALL.Checked)
                    return true;

                foreach (ListItem item in RowIDList.Items)
                {
                    if (item.Selected)
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }
    }

    public bool HidePoller
    {
        get { return _hidePoller; }
        set
        {
            _hidePoller = value;
            this.CustomPollerListArea.Visible = !_hidePoller;
        }
    }

    protected void SelectCustomPoller(object sender, EventArgs e)
    {
        CustomPollerID = CustomPollerList.SelectedValue;
    }

    private void InitRowIDList()
    {
        CustomPollerAssignments customPollerAssignments;
        if (_isDetails && !string.IsNullOrEmpty(CustomPollerList.SelectedValue))
        {
            Guid guid = new Guid(CustomPollerList.SelectedValue);
            using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                CustomPoller poller;
                try
                {
                    poller = proxy.Api.GetCustomPoller(guid);

                    if (poller.SNMPGetType == "GetSubTree")
                    {
                        RowIDList.Items.Clear();

                        switch (_netObject)
                        {
                            case NetObjectHelper.Interface:
                                customPollerAssignments = proxy.Api.GetCustomPollerAssignmentsForInterface(_netObjectID);
                                break;
                            case NetObjectHelper.Node:
                                customPollerAssignments = proxy.Api.GetCustomPollerAssignmentsForNode(_netObjectID);
                                break;
                            default:
                                customPollerAssignments = proxy.Api.GetAllCustomPollerAssignments();
                                break;
                        }

                        List<ListItem> sortedList = new List<ListItem>();

                        foreach (CustomPollerAssignment customPollerAssignment in customPollerAssignments)
                        {
                            if (customPollerAssignment.CustomPollerID == poller.CustomPollerID)
                            {
                                List<String> rows = proxy.Api.GetRowsByCustomPollerAssignment(customPollerAssignment);

                                foreach (string row in rows)
                                {
                                    ListItem item = new ListItem();
                                    item.Value = row;
                                    item.Text = proxy.Api.GetLabelAndRowIDString(customPollerAssignment, row);
                                    item.Attributes["SelectAllGroup"] = "true";
                                    item.Selected = true;
                                    sortedList.Add(item);
                                }
                            }
                        }

                        sortedList.Sort(delegate (ListItem item1, ListItem item2)
                        {
                            return UnsafeNativeMethods.StrCmpLogicalW(item1.Text, item2.Text);
                        });

                        RowIDList.Items.AddRange(sortedList.ToArray());

                        if (RowIDList.Items.Count == 0)
                            TabularPollerDetails.Visible = false;
                        else
                            TabularPollerDetails.Visible = true;
                    }
                    else
                    {
                        TabularPollerDetails.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }
        }
        SelectALL.Checked = true;

        foreach (ListItem item in RowIDList.Items)
        {
            if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
            {
                item.Attributes.Add("onclick",
                                    "javascript:SaveData($(" + hf_netObjectID.ClientID +
                                    ").val()+':Rows', getPollerRowsCheckedItems(this));SetUpSelectAllCb();");
            }
            else
            {
                item.Attributes.Add("onclick",
                                    "getPollerRowsCheckedItems(this);SetUpSelectAllCb();");

            }
            item.Attributes.Add("key", item.Value);
        }
        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            SelectALL.Attributes.Add("onclick",
                                     "javascript:SaveData($(" + hf_netObjectID.ClientID +
                                     ").val()+':Rows', this.checked?'':'-1');SelectAllRows();");
        }
        else
        {

            SelectALL.Attributes.Add("onclick",
                                     "SelectAllRows();");
        }
    }

    /// <summary>
    /// used for hiding, if non-raw is selected
    /// </summary>
    public Control ListChartsContainer
    {
        get;
        set;
    }
}
