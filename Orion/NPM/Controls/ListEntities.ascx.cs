using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.NPM.Web.UI;

public partial class ListEntities : System.Web.UI.UserControl
{
    public int MaxEntities { get; set; }
    public String EntityName
    {
        get
        {
            if (ViewState["EntityName"] == null)
            {
                EntityName = String.Empty;
            }
 
            return ViewState["EntityName"].ToString();
        }

        set
        {
            ViewState["EntityName"] = value;
        }
    }
    public String SelectedEntities
    {
        get
        {
            if (ViewState["SelectedEntities"] == null)
            {
                SelectedEntities = String.Empty;
            }

            return ViewState["SelectedEntities"].ToString();
        }

        set
        {
            ViewState["SelectedEntities"] = value;
        }
    }
    public bool ShowFilter
    {
        get
        {
            if (ViewState["ShowFilter"] == null)
            {
                ShowFilter = true;
            }

            return (bool)ViewState["ShowFilter"];
        }

        set
        {
            ViewState["ShowFilter"] = value;

            if (!value)
            {
                FilterSelection = false;
            }
        }
    }
    public bool FilterSelection
    {
        get
        {
            return Filter.Checked;
        }

        set
        {
            Filter.Checked = value;
        }
    }


    public class EntityInfo
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string DetailsUrl { get; set; }
    }

	private void SetupControlFromRequest()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);

		if (!string.IsNullOrEmpty(resource.Properties["SelectedEntities"]))
		{
            SelectedEntities = resource.Properties["SelectedEntities"];
		}
        
        if (!string.IsNullOrEmpty(resource.Properties["EntityName"]))
        {
            EntityName = resource.Properties["EntityName"];
        }

        if (!string.IsNullOrEmpty(resource.Properties["FilterEntities"]))
        {
            Filter.Checked = Boolean.Parse(resource.Properties["FilterEntities"]);
        }
    }

	protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);
       
        if (!IsPostBack)
		{
			SetupControlFromRequest();
		}
	}

    public void ReloadList()
    {
        DataTable entitiesTable = LoadEntities(EntityName, SelectedEntities);

        var entities = new List<EntityInfo>();
        var selectedEntities = new List<string>();

        if (entitiesTable != null && entitiesTable.Rows.Count > 0)
        {
            foreach (DataRow row in entitiesTable.Rows)
            {
                var entity = new EntityInfo()
                {
                    ID = row["ID"].ToString(),
                    Name = GenerateFullName((string[])row["AncestorDisplayNames"]),
                    Status = row["Status"].ToString(),
                    DetailsUrl = row["DetailsUrl"].ToString()
                };

                entities.Add(entity);
                selectedEntities.Add(entity.ID);
            }
        }

        // save because of limitations
        SelectedEntities = String.Join(",", selectedEntities.ToArray());

        EntitiesRepeater.DataSource = entities;
        EntitiesRepeater.DataBind();

        FilterOption.Visible = (ShowFilter && (entities.Count > 0));
    }

    private void DeleteItem(string itemID)
    {
        string[] ids = SelectedEntities.Split(',');
        var newList = new List<string>();

        if (ids != null)
        {
            foreach(string id in ids)
            {
                if(id != itemID)
                    newList.Add(id);
            }

            SelectedEntities = String.Join(",", newList.ToArray());
            ReloadList();
        }
    }

    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        string id = String.Empty;
        if (e.CommandArgument != null)
            id = e.CommandArgument.ToString();

        switch (e.CommandName)
        {
            case "Delete":
                DeleteItem(id);
                break;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            ReloadList();
        }
    }

    private string GenerateFullName(string[] names)
    {
        if (names == null)
            throw new ArgumentNullException("names");

        return (string.Join(" on ", names));
    }

    protected DataTable LoadEntities(string entityName, string selectedEntities)
    {
        if (String.IsNullOrEmpty(entityName) || String.IsNullOrEmpty(selectedEntities))
        {
            return null;
        }

        string query = String.Format(@"
SELECT e.{0} AS ID, e.AncestorDisplayNames, e.Status, e.DetailsUrl
FROM {1} (nolock=true) e
WHERE e.{0} IN ({2})", EntitiesHelper.TableColumnID(entityName), entityName, selectedEntities);

        DataTable results;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            results = swis.Query(query);
        }

        return results;
    }
}
