﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollerAxisControl.ascx.cs" Inherits="Orion_NPM_Controls_CustomPollerAxisControl" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include1" runat="server" File="../NPM/styles/MultiUnDP.css" />
<table border="0" width="100%">
    <tr>
        <td class="axis">
            <asp:Label ID="Axis" runat="server" CssClass="axisLabel" />
            <asp:Label ID="AxisHint" runat="server" CssClass="axisLabelOptional" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Repeater runat="server" ID="Repeater" OnItemDataBound="Repeater_ItemDataBound">
                <ItemTemplate>
                    <asp:DropDownList ID="UnDP" runat="server" DataTextField="UniqueName" DataValueField="CustomPollerID"
                        OnSelectedIndexChanged="UnDP_SelectedIndexChanged" AutoPostBack="true" Width="90%" />
                    <asp:ImageButton ID="RemoveUnDPButton" runat="server" OnCommand="Button_Click" CommandName="Remove"
                        CommandArgument="<%# Container.ItemIndex %>"
                        ImageUrl="~/Orion/Discovery/images/icon_delete.gif"
                        ToolTip="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_23%>" />
                </ItemTemplate>
                <SeparatorTemplate>
                    <br />
                </SeparatorTemplate>
            </asp:Repeater>
        </td>
    </tr>
    <tr>
        <td>
            <div id="AddButtonArea" runat="server" class="sw-hatchbox" style="width:89%">
                <orion:LocalizableButton ID="AddUnDPButton" DisplayType="Small" runat="server" OnCommand="Button_Click" CommandName="Add" LocalizedText="CustomText" />
            </div>
        </td>
    </tr>
</table>
