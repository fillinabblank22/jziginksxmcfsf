﻿using System;
using System.Web.UI.WebControls;

public partial class Orion_NPM_Controls_LabelUnitAxisControl : System.Web.UI.UserControl
{
    public string CustomLabel
    {
        get { return CustomLabelTextBox.Text; }
        set { CustomLabelTextBox.Text = value; }
    }

    public string Unit
    {
        get 
        {
            if (UnitDropDown.SelectedValue == "custom")
                return UnitTextBox.Text;
            else
                return UnitDropDown.SelectedValue;
        }
        set 
        {
            // default value ('Empty')
            if (string.IsNullOrEmpty(value))
            {
                UnitDropDown.SelectedValue = "empty";
                return;
            }

            // predefined value (bytes, packets, ...)
            foreach (ListItem item in UnitDropDown.Items)
            {
                if (item.Value == value)
                {
                    item.Selected = true;
                    return;
                }
            }

            // custom value
            UnitDropDown.SelectedValue = "custom";
            UnitTextBox.Text = value;
            UnitTextBox.Visible = true;
        }
    }

    protected void UnitDropDown_SelectionChanged(object sender, EventArgs e)
    {
        UnitTextBox.Visible = UnitDropDown.SelectedValue == "custom";
    }
}