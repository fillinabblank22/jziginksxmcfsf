﻿<%@ Control Language="C#" CodeFile="ShowFilterExamples.ascx.cs" Inherits="Orion_NPM_Controls_ShowFilterExamples" %>
<orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
    <TitleTemplate>
        <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_262%></b></TitleTemplate>
    <BlockTemplate>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_263%></p>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_264%><br />
            <b>Status&lt;&gt;1</b></p>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_265%>
            <table>
                <tr>
                    <td>
                        0 = <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_266%></b>
                    </td>
                    <td>
                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_267%>
                    </td>
                </tr>
                <tr>
                    <td>
                        1 = <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_268%></b>
                    </td>
                    <td>
                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_269%>
                    </td>
                    <tr>
                        <td>
                            2 = <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_270%></b>
                        </td>
                        <td>
                            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_271%>
                        </td>
                        <tr>
                            <td>
                                3 = <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_272%></b>
                            </td>
                            <td>
                                <%=Resources.NPMWebContent.NPMWEBDATA_VB0_273%>
                            </td>
                        </tr>
            </table>
        </p>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_274%></p>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_275%></p>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_276%></p>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_277%></p>
        <p>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_278%></p>
        <p>
        </p>
        <asp:Repeater runat="server" ID="listOfEntities" OnItemDataBound="listOfEntities_onItemDataBound">
            <ItemTemplate>
                <orion:CollapsePanel ID="panel" runat="server" Collapsed="true">
                    <TitleTemplate>
                        <b><asp:Label runat="server" ID="title"></asp:Label></b>
                    </TitleTemplate>
                    <BlockTemplate>
                        <ul>
                        <asp:Repeater runat="server" ID="propertyList">
                            <ItemTemplate>
                                <li><%# Eval("Name") %></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        </ul>
                    </BlockTemplate>
                </orion:CollapsePanel>
            </ItemTemplate>
        </asp:Repeater>
    </BlockTemplate>
</orion:CollapsePanel>