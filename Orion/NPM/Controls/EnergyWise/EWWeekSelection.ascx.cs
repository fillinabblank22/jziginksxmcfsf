﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

public partial class EWWeekSelection : System.Web.UI.UserControl
{
    // ========== PUBLIC ======== //
    //Constructors
    #region public EWWeekSelection()
    /// <summary>
    /// Initializes a new instance of the <b>EWWeekSelection</b> class.
    /// </summary>
    public EWWeekSelection()
    {
    }
    #endregion

    //Methods

    //Properties
    #region public DateTime CurrentSelection
    /// <summary>
    /// Get/Sets the CurrentSelection of the EWWeekSelection
    /// </summary>
    /// <value></value>
    public DateTime CurrentSelection
    {
        get
        {
            return m_CurrentSelection;
        }
        private set
        {
            m_CurrentSelection = value;
        }
    }
    #endregion

    //Events
    public event EventHandler DateTimeChanged = delegate { };


    // ========== PROTECTED ======== //
    #region protected void Page_Load(object sender, EventArgs e)
    /// <summary>
    /// This method is called when the Page's Load event has been fired.
    /// </summary>
    /// <param name="sender">The <see cref="object"/> that fired the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> of the event.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion

    #region protected override void OnInit(EventArgs e)
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        DateTime selectedDay = DateTime.Today;
        if (Session["NPM_EW_Week"] != null)
            selectedDay = DateTime.Parse(Session["NPM_EW_Week"].ToString());
        CurrentSelection = selectedDay;
        InitializeDropDown();
    }
    #endregion

    #region protected void DateSelected(object sender, EventArgs e)
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DateSelected(object sender, EventArgs e)
    {
        Session["NPM_EW_Week"] = this.weekSelector.SelectedItem.Value.ToString();
        Response.Redirect(Request.UrlReferrer.ToString());
    }
    #endregion



    // ========== INTERNAL ======== //

    // ========== PROTECTED INTERNAL ======== //

    // ========== PRIVATE ======== //
    //Members
    private DateTime m_CurrentSelection = DateTime.Today;

    //Methods
    #region private void InitializeDropDown()
    /// <summary>
    /// 
    /// </summary>
    private void InitializeDropDown()
    {
        DateTime day = DateTime.Today.AddDays(-1 * (int)(DateTime.Today.DayOfWeek)).AddDays(-7 * 26);
        for (int i = 0; i < 52; i++)
        {
            DateTime weekend = day.AddDays(6);
            this.weekSelector.Items.Add(new ListItem(day.ToShortDateString() + "-" + weekend.ToShortDateString(), day.ToString()));
            if (CurrentSelection >= day && CurrentSelection <= weekend)
            {
                this.weekSelector.SelectedIndex = i;
            }
            day = day.AddDays(7);
        }
    }
    #endregion
}
