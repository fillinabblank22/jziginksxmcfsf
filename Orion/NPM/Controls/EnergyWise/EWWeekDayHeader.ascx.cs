﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

public partial class EWWeekDayHeader : System.Web.UI.UserControl
{

    // ========== PUBLIC ======== //
    #region public DateTime Day
    /// <summary>
    /// Get/Sets the Day of the EWWeekDayHeader
    /// </summary>
    /// <value></value>
    public DateTime Day
    {
        get { return m_Day; }
        set
        {
            m_Day = value;
            this.daycell.InnerHtml = String.Format("{0}<br />{1}", m_Day.ToString("ddd"), m_Day.ToShortDateString());//m_Day.ToString("ddd, MMM d"); 
        }
    }
    #endregion

    // ========== PROTECTED ======== //
    #region protected void Page_Load(object sender, EventArgs e)
    /// <summary>
    /// This method is called when the Page's Load event has been fired.
    /// </summary>
    /// <param name="sender">The <see cref="object"/> that fired the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> of the event.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    // ========== INTERNAL ======== //


    // ========== PROTECTED INTERNAL ======== //


    // ========== PRIVATE ======== //
    private DateTime m_Day = DateTime.Now;

}
