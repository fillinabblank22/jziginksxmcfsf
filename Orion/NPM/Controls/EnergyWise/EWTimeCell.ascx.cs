using System;
using SolarWinds.NPM.Web.EnergyWise;
using System.Web.UI;

public partial class EWTimeCell : UserControl
{
	public PowerLevel Level
	{
		get { return _level; }
		set
		{
			if (Enum.IsDefined(typeof(PowerLevel), value))
			{
				_level = value;
				this.ewCell.Attributes["class"] = styles[(int)_level];
				this.ewCell.InnerText = ((int)_level).ToString();
			}
		}
	}

	public int Hour
	{
		set
		{
			switch (value)
			{
				case 12: this.ewCell.InnerText = Resources.NPMWebContent.NPMWEBCODE_VB0_64; break;
				case 24: this.ewCell.InnerText = Resources.NPMWebContent.NPMWEBCODE_VB0_65; break;
				default: this.ewCell.InnerText = DateTime.Today.AddHours(value).ToString("h tt").ToLowerInvariant(); break;
			}

		}

	}

	PowerLevel _level = PowerLevel.Shutdown;

	private static string[] styles = 
    { 
        "ewshut", 
        "ewhibernate", 
        "ewhibernate", 
        "ewstandby", 
        "ewstandby",
        "ewlow",
        "ewlow",
        "ewmedium",
        "ewmedium",
        "ewfull",
        "ewfull"
    };
}
