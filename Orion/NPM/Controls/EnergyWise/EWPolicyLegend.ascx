<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EWPolicyLegend.ascx.cs" Inherits="EWPolicyLegend" %>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="ewlegendtitle" colspan="12"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_217%></td>
    </tr>
    <tr>
        <td class="ewlegendiconcell" rowspan="2"><img src="/Orion/NPM/images/EnergyWise/EW_0_shut_legend.gif" alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_192%>" /></td>
        <td class="ewlegendcell" rowspan="2"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_192%></td>
        <td class="ewlegendiconcell" rowspan="2"><img src="/Orion/NPM/images/EnergyWise/EW_1-2_hib_sleep_legend.gif" alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_193%>" /></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_212%></td>
        <td class="ewlegendiconcell" rowspan="2"><img src="/Orion/NPM/images/EnergyWise/EW_3-4_stand_ready_legend.gif" alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_194%>" /></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_213%></td>
    </tr>
    <tr>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_202%></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_201%></td>
    </tr>
    <tr>
        <td class="ewlegendiconcell" rowspan="2"><img src="/Orion/NPM/images/EnergyWise/EW_5-6_frugal_low_legend.gif" alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_195%>" /></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_214%></td>
        <td class="ewlegendiconcell" rowspan="2"><img src="/Orion/NPM/images/EnergyWise/EW_7-8_med_red_legend.gif" alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_196%>" /></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_215%></td>
        <td class="ewlegendiconcell" rowspan="2"><img src="/Orion/NPM/images/EnergyWise/EW_9-10_high_full_legend.gif" alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_197%>" /></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_216%></td>
    </tr>
    <tr>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_200%></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_199%></td>
        <td class="ewlegendcell"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_198%></td>
    </tr>
    <tr>
        <td style="border-bottom: none !important; padding:none;" colspan="12">* <%=Resources.NPMWebContent.NPMWEBDATA_VB0_203%></td>
    </tr>
</table>