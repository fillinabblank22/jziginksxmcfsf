﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MulticastPieChartLegend.ascx.cs" Inherits="Orion_NPM_Controls_MulticastPieChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.MulticastPieChartLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.NPM.Charts.MulticastPieChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
</script>

<orion:Include ID="Include1" runat="server" File="NPM/js/MulticastPieChartLegend.js" />

<table runat="server" id="legend" class="chartLegend" ></table>

<script id="CurrentTrafficOverview" type="text/x-template">
    <thead>
        <tr class="ReportHeader">
            <th colspan="2" style="text-transform: uppercase;">
                <%= Resources.NPMWebContent.NPMWEBCODE_ZB0_10 %>
            </th>
            <th style="text-transform: uppercase;">
                <%= Resources.NPMWebContent.NPMWEBCODE_ZB0_11 %>
            </th>
        </tr>
    </thead>
    <tbody>
    {# _.each(data, function(current, index) { #}
        <tr>
            <td style="width:20px">{{ SW.NPM.Charts.MulticastPieChartLegend.createLegendSymbol(current) }}</td>
            <td>{{ current.name }}</td>
            <td style="text-align:center">{{ Math.round(current.percentage) }}%</td>
        </tr>
    {# }); #}
    </tbody>
</script>

<script id="TrafficShareByGroups" type="text/x-template">
    <thead>
        <tr class="ReportHeader">
            <th colspan="2" style="text-transform: uppercase;">
                <%= Resources.NPMWebContent.NPMWEBDATA_JF0_32 %>
            </th>
            <th style="text-transform: uppercase;">
                <%= Resources.NPMWebContent.NPMWEBDATA_VB0_83 %>
            </th>
            <th style="text-transform: uppercase;">
                <%= Resources.NPMWebContent.NPMWEBCODE_ZB0_9 %>
            </th>
        </tr>
    </thead>
    {# _.each(data, function(current, index) { #}
    <tbody>
        <tr>
            <td style="width:20px">{{ SW.NPM.Charts.MulticastPieChartLegend.createLegendSymbol(current) }}</td>
            <td>{{ SW.NPM.Charts.MulticastPieChartLegend.createLegendLabel(current) }}</td>
            <td style="text-align:center">{{ Math.round(current.pps) }} pps</td>
            <td style="text-align:center">{{ Math.round(current.percentage) }}%</td>
        </tr>
    </tbody>
    {# }); #}
</script>