﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NPM_Controls_SwitchStackRingControl : UserControl
{
    public string ResourceID { protected get; set; }
    public string ServiceConfig { protected get; set; }
    public string ResourceConfig { protected get; set; }
    public string jsRenderHeaderTableFunc { protected get; set; }

    protected string GetJsRenderHeaderTableFunc()
    {
        if (!string.IsNullOrEmpty(jsRenderHeaderTableFunc))
            return jsRenderHeaderTableFunc;
        else
            return "undefined";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}