﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LabelUnitAxisControl.ascx.cs" Inherits="Orion_NPM_Controls_LabelUnitAxisControl" %>
<orion:Include ID="Include1" runat="server" File="../NPM/styles/MultiUnDP.css" />
<table width="100%">
    <tr>
        <td class="label">
            <%=Resources.NPMWebContent.NPMWEBDATA_ZB0_21%>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="CustomLabelTextBox" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="unit">
            <%=Resources.NPMWebContent.NPMWEBDATA_ZB0_22%>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList ID="UnitDropDown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="UnitDropDown_SelectionChanged">
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_32 %>" Value="bytes" />
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_33 %>" Value="bps" />
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_34 %>" Value="packets" />
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_35 %>" Value="pps" />
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_36 %>" Value="empty" />
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_31 %>" Value="custom" />
            </asp:DropDownList>
            <asp:TextBox ID="UnitTextBox" runat="server" Visible="false" MaxLength="35" Width="100" />
        </td>
    </tr>
</table>