﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MulticastEditNodeProperties.ascx.cs" Inherits="Orion_NPM_Controls_Multicast_MulticastEditNodeProperties" %>
<div class="contentBlock" runat="server" id="MulticastSection">
    <div class="contentBlockHeader" runat="server">
        <table>
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal runat="server" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_19 %>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr runat="server" id="RouteTable">
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyRouteTablePollInterval" AutoPostBack="true" OnCheckedChanged="ApplyProperty_CheckedChanged" />

                <asp:Literal runat="server" ID="LabelRouteTablePollInterval" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_21 %>"/>
            </td>
            <td class="rightInputColumn">
                <asp:CheckBox runat="server" ID="OverrideRouteTablePollInterval" AutoPostBack="true" OnCheckedChanged="OverrideProperty_CheckedChanged"/>

                <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_20 %>"/>
                
                <div runat="server" ID="SettingsRouteTablePollInterval">
                    <asp:TextBox runat="server" ID="RouteTablePollInterval"/>
                    
                    <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_MZ0_3 %>" />

                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RequiredValidatorRouteTablePollInterval"
                        ControlToValidate="RouteTablePollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_27 %>"/>
                    <asp:RegularExpressionValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RegexValidatorRouteTablePollInterval"
                        ControlToValidate="RouteTablePollInterval"
                        ErrorMessage="<%$ Resources: NPMWebContent,NPMWEBDATA_JP1_28 %>"
                        ValidationExpression="^\d+$"/>
                    <asp:RangeValidator runat="server" Display="Dynamic" SetFocusOnError="True" Type="Integer"
                        ID="RangeValidatorRouteTablePollInterval"
                        ControlToValidate="RouteTablePollInterval"/>
                </div>
            </td>
        </tr>
    </table>
</div>