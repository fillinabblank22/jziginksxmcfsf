﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using SolarWinds.NPM.Web.UI;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;

public partial class Orion_NPM_Controls_Multicast_MulticastEditNodeProperties : BaseNodeEditPlugin, INodePropertyPlugin
{
    protected override HtmlGenericControl MainSectionElement
    {
        get { return MulticastSection; }
    }

    protected override IList<SimplePropertyControl> GetSimplePropertyControlsList(IList<Node> editedNodes)
    {
        var supportedNodes = GetSupportedNodesWithPollerType(editedNodes);

        return new List<SimplePropertyControl>
        {
            new SimplePropertyControl
            {
                SettingId = "NPM_Settings_MulticastRouting_MulticastRouteTable_PollInterval",
                NodeSettingId = "NPM.MulticastRoutingTablePollInterval",
                ContainerControl = RouteTable,
                ApplyToAllNodesControl = ApplyRouteTablePollInterval,
                OverrideValueControl = OverrideRouteTablePollInterval,
                InputControl = RouteTablePollInterval,
                RangeValidator = RangeValidatorRouteTablePollInterval,
                SupportedNodeIds = supportedNodes.Where(n => n.Item2.StartsWith("N.MulticastRouting.")).Select(n => n.Item1).ToList()
            }
        };
    }
}
