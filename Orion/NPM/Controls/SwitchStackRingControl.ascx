﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SwitchStackRingControl.ascx.cs" Inherits="Orion_NPM_Controls_SwitchStackRingControl" %>

<orion:Include ID="Include1" runat="server" Module="NPM" File="d3.min.js" />
<orion:Include ID="Include2" runat="server" Module="NPM" File="SwitchStackRing.js" />
<orion:Include ID="Include3" runat="server" Module="NPM" File="SwitchStackRing.css" />

<script type="text/javascript">
    $(function () {
        SW.NPM.SwitchStackRing.render('<%= ResourceID %>', <%= ServiceConfig %>, <%= ResourceConfig %>, <%= GetJsRenderHeaderTableFunc() %>);
    });
</script>

<div id="Content-<%= ResourceID %>" class="sw-switch-stack-content">
    <div id="Loading-<%= ResourceID %>" class="sw-switch-stack-loading">
        <img src="/Orion/images/loading_gen_small.gif" />
        <%= Resources.CoreWebContent.WEBDATA_AK0_51 %>
    </div>
    <svg id="SwitchStackRing-<%= ResourceID %>" />
</div>

<div id="Error-<%= ResourceID %>"></div>