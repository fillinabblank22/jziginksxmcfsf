<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListEntities.ascx.cs" Inherits="ListEntities" %>

<orion:Include runat="server" Module="NPM" File="Controls/EditControls.css" />
<table style="border-collapse:collapse;">
    <asp:UpdatePanel ID="EntitiesUpdatePanel" runat="server">
    <ContentTemplate>

    <asp:Repeater ID="EntitiesRepeater" runat="server">
        <ItemTemplate>
            <tr style="background-color:#E4F1F8">
                <td class="Icon"><img src="/Orion/StatusIcon.ashx?entity=<%= EntityName %>&amp;status=<%# Eval("Status") %>&amp;size=small"/></td>
                <td style="min-width:300px; padding-right:10px;"><a href='<%# Eval("DetailsUrl") %>'><%# Eval("Name") %></a></td>
                <td>
                    <asp:ImageButton ID="deleteButton" runat="server" ToolTip="<%$Resources:NPMWebContent,NPMWEBDATA_AK0_61%>" ImageUrl="~/Orion/Discovery/images/icon_delete.gif"
                    OnCommand="ActionItemCommand" CommandName="Delete" CommandArgument='<%# Eval("ID") %>' />
                </td>
            </tr>                    
        </ItemTemplate>
    </asp:Repeater>

    </ContentTemplate>
    </asp:UpdatePanel>

    <tr style="background-color:#D3EBF8" id="FilterOption" runat="server">
        <td class="Icon"><asp:CheckBox ID="Filter" runat="server" /></td>
        <td><%=Resources.NPMWebContent.NPMWEBDATA_AK0_60%></td>
        <td></td>
    </tr>
</table>
