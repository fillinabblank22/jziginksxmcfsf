﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Controls_EditResourceControls_EditCustomPollerStatus : BaseResourceEditControl
{
	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> _properties = new Dictionary<string, object>();
            _properties.Add("AutoHide", this.autoHide.AutoHideValue);
			return _properties;
		}
	}
}
