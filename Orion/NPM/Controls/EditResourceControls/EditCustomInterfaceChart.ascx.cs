﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Controls_EditResourceControls_EditCustomInterfaceChart : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool boolValue;
        showTrend.Checked = (Resource != null) ? (bool.TryParse(Resource.Properties["ShowTrend"], out boolValue) ? boolValue : true) : true;

        Dictionary<string, string> list = ChartXMLConfigManager.GetChartSelection("I");

        ListOfCharts.DataSource = list;
        ListOfCharts.DataBind();

        if (!IsPostBack)
            if (!string.IsNullOrEmpty(Resource.Properties["ChartName"]))
            {
                int? pos;
                if ((pos = GetItemPosition(ListOfCharts, Resource.Properties["ChartName"])).HasValue)
                {
                    ListOfCharts.SelectedIndex = pos.Value;
                }
            }

        showTrend.Visible = (ChartInfoFactory.Create(ListOfCharts.SelectedValue).IsTrendAllowed());

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            ListOfCharts.Attributes.Add("onchange", "javascript:SaveData('ChartName', this.value);");
        }
    }

    private static int? GetItemPosition(DropDownList ddl, string search)
    {
        for (int pos = 0; pos < ddl.Items.Count; pos++)
        {
            if (string.Compare(ddl.Items[pos].Value, search, true) == 0)
            {
                return pos;
            }
        }

        return null;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("ChartName", ListOfCharts.SelectedValue);
            properties.Add("Period", Period.PeriodName);
            properties.Add("SampleSize", SampleSize.SampleSizeValue);
            properties.Add("ShowTrend", showTrend.Checked.ToString());

            return properties;
        }
    }

    public override string DefaultResourceTitle
    {
        get
        {
            return ListOfCharts.SelectedItem.Text;
        }
    }
}
