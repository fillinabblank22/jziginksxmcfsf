﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using System.Collections.Specialized;
using System.Data;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.InformationService;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NPM_Controls_EditResourceControls_EditUndpMultiSourceCharts : BaseResourceEditControl
{
	private static readonly Log _log = new Log();
	private string netObjectPrefix = string.Empty;
    private const string defaultEntityName = "Orion.Nodes";
    private const string SessionGuid = "BA0F0007-0438-47F0-89A8-64BA3E4E3332";
    private const string SessionID = "SessionID";
    private StringDictionary parameters = null;
    private string customPollerID = null;

    private bool LoadSessionData()
    {
        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString[SessionID]) && Session[SessionGuid] != null)
            {
                parameters = (StringDictionary)Session[SessionGuid];

                if (parameters.ContainsKey(SessionID) && parameters[SessionID] == Request.QueryString[SessionID])
                {
                    return true;
                }

                parameters = null;
            }
        }
        catch { }
        return false;
    }

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

    public bool SummaryPage
    {
        get
        {
            if (String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                return true;

            NetObject myObj = NetObjectFactory.Create(Request.QueryString["NetObject"]);
            if (myObj != null && (myObj is Node || myObj is Interface || myObj is Volume))
                return false;

            return true;
        }
    }

    public string Entities
    {
        get
        {
            return WebSecurityHelper.SanitizeHtmlV2(ChartEntity.SelectedItem.Text);
        }
    }

    private void LoadProperties(StringDictionary properties)
    {
        if (properties != null)
        {
            if (properties.ContainsKey("EntityName"))
            {
                ChartEntity.SelectedValue = properties["EntityName"];
                ListEntities.EntityName = properties["EntityName"];
            }

            if (properties.ContainsKey("ManualSelect"))
            {
                ManualSelect.Checked = Boolean.Parse(properties["ManualSelect"]);
                AutoSelect.Checked = !ManualSelect.Checked;
                bt_Submit.Visible = ManualSelect.Checked;
            }

            if (properties.ContainsKey("ShowSum"))
            {
                ShowSum.SelectedValue = properties["ShowSum"];
            }

            if (properties.ContainsKey("SelectedEntities"))
            {
                ListEntities.SelectedEntities = properties["SelectedEntities"];
            }

            if (properties.ContainsKey("FilterEntities"))
            {
                ListEntities.FilterSelection = Boolean.Parse(properties["FilterEntities"]);
            }

            if (properties.ContainsKey("CustomPollerID"))
            {
                customPollerID = properties["CustomPollerID"];
            }

            if (properties.ContainsKey("Period"))
            {
                Period.PeriodName = properties["Period"];
            }

            if (properties.ContainsKey("SampleSize"))
            {
                SampleSize.SampleSizeValue = properties["SampleSize"];
            }

            if (properties.ContainsKey("TopXX") && !String.IsNullOrEmpty(properties["TopXX"]))
            {
                TopXX.Enabled = true;
                TopXX.Value = properties["TopXX"];
            }

            if (properties.ContainsKey("AutoHide"))
            {
                AutoHide.AutoHideValue = properties["AutoHide"];
            }
        }
    }

    private void ReloadUnDPs()
    {
        UnDPsList.Items.Clear();

        DataTable entitiesTable = LoadUnDPs(ChartEntity.SelectedValue, null);

        if (entitiesTable != null && entitiesTable.Rows.Count > 0)
        {
            foreach (DataRow row in entitiesTable.Rows)
            {
                UnDPsList.Items.Add(new ListItem(row["UniqueName"].ToString(), row["CustomPollerID"].ToString()));
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            LoadProperties(Resource.Properties);

            if (LoadSessionData())
            {
                LoadProperties(parameters);
            }

            AutoSelect.Enabled = !SummaryPage;

            ReloadUnDPs();
            UnDPsList.SelectedValue = customPollerID;

            if (ManualSelect.Checked)
                bt_Submit.Visible = !String.IsNullOrEmpty(UnDPsList.SelectedValue);

            bt_Submit.Text = GetButtonText(Entities);
            bt_Submit.DataBind();

            ListEntities.ShowFilter = !SummaryPage;

            ChartList.EntityName = ChartEntity.SelectedValue;
            ChartList.ReloadList();
        }
    }

    /// <summary>
    /// get localized text for "select" button by entities
    /// </summary>
    private string GetButtonText(string entities)
    {
        switch (entities.ToLower())
        {
            case "nodes":
                return Resources.NPMWebContent.NPMWEBCODE_AK0_58;
            case "interfaces":
                return Resources.NPMWebContent.NPMWEBCODE_AK0_59;
            default:
                return entities;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }

	private Dictionary<string, object> GetProperties()
	{
		var properties = new Dictionary<string, object>();

        properties.Add("EntityName", ChartEntity.SelectedValue);
        properties.Add("CustomPollerID", UnDPsList.SelectedValue);
        properties.Add("Period", Period.PeriodName);
		properties.Add("SampleSize", SampleSize.SampleSizeValue);
        properties.Add("ShowSum", ShowSum.SelectedValue);
        properties.Add("SelectedEntities", ListEntities.SelectedEntities);
        properties.Add("ManualSelect", ManualSelect.Checked.ToString());
        properties.Add("FilterEntities", ListEntities.FilterSelection.ToString());
        properties.Add("TopXX", TopXX.Enabled ? TopXX.Value.ToString() : String.Empty);
        properties.Add("AutoHide", AutoHide.AutoHideValue);
         //Store chart name
        properties.Add("ChartName", ChartList.SelectedChart);

		return properties;
	}

	public override Dictionary<string, object> Properties
	{
		get { return GetProperties(); }
	}

    protected void SelectObjects_Click(object source, EventArgs e)
    {
        var data = new StringDictionary();

        data["Entities"] = Entities;
        data["EntityName"] = ChartEntity.SelectedValue;
        data["Filter"] = GetUnDPFilterQuery(UnDPsList.SelectedValue, ChartEntity.SelectedValue, null);
        data["CustomPollerID"] = UnDPsList.SelectedValue;
        data["Period"] = Period.PeriodName;
        data["SampleSize"] = SampleSize.SampleSizeValue;
        data["ShowSum"] = ShowSum.SelectedValue;
        data["SelectedEntities"] = ListEntities.SelectedEntities;
        data["ManualSelect"] = ManualSelect.Checked.ToString();
        data["FilterEntities"] = ListEntities.FilterSelection.ToString();
        data["TopXX"] = TopXX.Enabled ? TopXX.Value.ToString() : String.Empty;
        data["AutoHide"] = AutoHide.AutoHideValue;

        Session[SessionGuid] = data;

        string url = String.Format("/Orion/NPM/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjects.aspx{0}", GetUrlParameters());
        this.Response.Redirect(url);
    }

    private string GetUrlParameters()
    {
        var list = new List<string>();

        AddUrlParameter(list, "ResourceID");
        AddUrlParameter(list, "NetObject");
        AddUrlParameter(list, "ViewID");

        return (list.Count > 0) ? "?" + String.Join("&", list.ToArray()) : String.Empty;
    }

    private void AddUrlParameter(List<string> list, string name)
    {
        if (!String.IsNullOrEmpty(Request.QueryString[name]))
            list.Add(String.Format("{0}={1}", name, Request[name]));
    }

    protected void ChartEntity_Change(object source, EventArgs e)
    {
        ReloadUnDPs();

        if (ManualSelect.Checked)
            bt_Submit.Visible = !String.IsNullOrEmpty(UnDPsList.SelectedValue);

        bt_Submit.Text = GetButtonText(Entities);
        bt_Submit.DataBind();

        ListEntities.EntityName = ChartEntity.SelectedValue;
        ChartList.EntityName = ChartEntity.SelectedValue;
        ChartList.ReloadList();
        ListEntities.SelectedEntities = String.Empty;
        ListEntities.ReloadList();
    }

    protected void UnDPsList_Change(object source, EventArgs e)
    {
        ListEntities.EntityName = ChartEntity.SelectedValue;
        ListEntities.SelectedEntities = String.Empty;
        ListEntities.ReloadList();
    }

    protected void SelectObjects_Change(object source, EventArgs e)
    {
        bt_Submit.Visible = ManualSelect.Checked;

        if (ManualSelect.Checked)
            bt_Submit.Visible = !String.IsNullOrEmpty(UnDPsList.SelectedValue);

        ListEntities.SelectedEntities = String.Empty;
        ListEntities.EntityName = ChartEntity.SelectedValue;
        ListEntities.ReloadList();
    }


    private DataTable LoadUnDPs(string entityName, string netObjectFilter)
    {
        if (String.IsNullOrEmpty(entityName))
        {
            return null;
        }

        string filter = String.Empty;
        if (!String.IsNullOrEmpty(netObjectFilter))
        {
            filter = " WHERE " + netObjectFilter;
        }

        string query = String.Format(@"
SELECT c.CustomPollerID, c.UniqueName
FROM Orion.NPM.CustomPollers c
INNER JOIN 
(SELECT DISTINCT e.CustomPollerID FROM Orion.NPM.CustomPollerAssignment e {1}) 
AS T ON T.CustomPollerID = c.CustomPollerID
WHERE c.IncludeHistoricStatistics = 'true' AND c.NetObjectPrefix = '{0}'
ORDER BY c.UniqueName ASC", EntitiesHelper.NetObjectPrefix(entityName), filter);

        DataTable results;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            results = swis.Query(query);
        }

        return results;
    }

    private string GetUnDPFilterQuery(string customPollerID, string entityName, string netObjectFilter)
    {
        if (String.IsNullOrEmpty(customPollerID) || String.IsNullOrEmpty(entityName))
        {
            return string.Empty;
        }

        string filter = String.Empty;
        if (!String.IsNullOrEmpty(netObjectFilter))
        {
            filter = " AND " + netObjectFilter;
        }

        string query = String.Format(@"
SELECT e.NodeID, e.InterfaceID
FROM Orion.NPM.CustomPollerAssignment e
WHERE (e.CustomPollerID = '{0}') {1}", customPollerID, filter);

        DataTable results;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            results = swis.Query(query);

            if (results != null && results.Rows.Count > 0)
            {
                var ids = new List<string>();
                foreach (DataRow row in results.Rows)
                {
                    ids.Add(row[EntitiesHelper.TableColumnID(entityName)].ToString());
                }

                return String.Format("(e.{0} IN ({1}))", EntitiesHelper.TableColumnID(entityName), String.Join(",",ids.ToArray()));
            }
        }

        return String.Empty;
    }

}
