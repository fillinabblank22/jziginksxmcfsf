﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Core.Common;
using System.Collections;
using SolarWinds.NPM.Common;

public partial class Orion_NPM_Controls_EditResourceControls_EditUndpCharts : BaseResourceEditControl
{
	private static readonly Log _log = new Log();
    private bool _isError;
    private string _entityName = "Orion.NPM.CustomPollers";

    protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	protected override void OnInit(EventArgs e)
	{
        ChartList.EntityName = _entityName;
        CustomPollerList.ListChartsContainer = listChartsContainer;

		base.OnInit(e);
		
		if (!string.IsNullOrEmpty(Resource.Properties["SubTitle2"]))
        {
			ResourceSubTitle2.Text = Resource.Properties["SubTitle2"];
        }
        else
        {
            ResourceSubTitle2.Text = String.Empty;
        }
		OnInitControls();
        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            ResourceSubTitle2.Attributes.Add("onchange", "javascript:SaveData('SubTitle2', this.value);");

            if (Session["EmbeddedResourceData"] == null)
                return;

            if (!IsPostBack)
                return;

            var embeddedResourceData = Session["EmbeddedResourceData"] as Dictionary<string, object>;

            if (embeddedResourceData == null)
                return;

            if (!embeddedResourceData.ContainsKey("CustomPollerID"))
                return;

            if (embeddedResourceData["CustomPollerID"] == null)
            {
                embeddedResourceData.Remove("CustomPollerID");
                return;
            }

            CustomPollerList.CustomPollerID = embeddedResourceData["CustomPollerID"].ToString().Trim();
        }
	}

	private void OnInitControls()
	{
		OnInitCustomPollerList();
		ChartFormat.Visible = true;
		BarPlotColor.PlotColorField = "SubsetColor";
		BarPlotColor.ControlName = Resources.NPMWebContent.NPMWEBCODE_TM0_9;
		BarPlotColor.ControlDescription = Resources.NPMWebContent.NPMWEBCODE_TM0_10;
		BarPlotColor.SetupControl();
	}

	private void OnInitCustomPollerList()
	{
        int netObjectId = 0;
        string netObjectType = String.Empty;
        if (!String.IsNullOrEmpty(NetObjectID))
        {
            string[] netObject = NetObjectHelper.ParseNetObject(NetObjectID);
            netObjectType = netObject[0];
            netObjectId = Convert.ToInt32(netObject[1]);
        }

        switch (netObjectType)
        {
            case "N":
                CustomPollerList.InitNodeCustomPollerList(netObjectId);
                break;
            case "I":
                CustomPollerList.InitInterfaceCustomPollerList(netObjectId);
                break;
            default:
                CustomPollerList.InitSummaryCustomPollerList();
                break;
        }
	}

	private Dictionary<string, object> GetProperties()
	{
		Dictionary<string, object> properties = new Dictionary<string, object>();

		String oldCustomPollerId;
		CustomPoller poller = null;


		using (INpmCustomPollerAdapter adapter = NpmAdapterFactory.CreateCustomPollerAdapter())
		{
			if (!string.IsNullOrEmpty(CustomPollerList.CustomPollerID))
			{
				poller = adapter.GetCustomPoller(new Guid(CustomPollerList.CustomPollerID));
				if (poller.SNMPGetType == "GetSubTree" && !CustomPollerList.IsValid)
				{
					IsError = true;
					return properties;
				}

                if (poller.PollerType != CustomPollerType.Counter)
                {
                    ChartList.SelectedChart = "CustomPoller"; /* there are only two types! */
                }
			}
		}

		if (!String.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
			oldCustomPollerId = Resource.Properties["CustomPollerID"];
		else
			oldCustomPollerId = string.Empty;


		//copy row properties
		foreach (DictionaryEntry property in Resource.Properties)
			if (property.Key.ToString().EndsWith(":rows"))
				properties.Add(property.Key.ToString(), property.Value.ToString());

		if (!String.IsNullOrEmpty(CustomPollerList.CustomPollerID))
			properties.Add("CustomPollerID", CustomPollerList.CustomPollerID);
		else
			properties.Add("CustomPollerID", oldCustomPollerId);

		properties.Add(NetObjectID + ":Rows", CustomPollerList.CustomPollerRows);
		properties.Add("Period", Period.PeriodName);
		properties.Add("SampleSize", SampleSize.SampleSizeValue);
		properties.Add("AutoHide", AutoHide.AutoHideValue);

		if (ResourceSubTitle2.Text != null)
			properties.Add("SubTitle2", ResourceSubTitle2.Text);

		properties.Add("SubsetColor", BarPlotColor.Color);
		properties.Add("PlotStyle", ChartFormat.ChartType);

		// save custom property that enables or disables showing of trend
		properties.Add("ShowTrend", showTrend.Checked.ToString());

        //Store chart name 
        properties.Add("ChartName", ChartList.SelectedChart);
        
		return properties;
	}

	public override Dictionary<string, object> Properties
	{
		get { return GetProperties(); }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			string trendEnabled = Resource.Properties["ShowTrend"];

			if (String.IsNullOrEmpty(trendEnabled))
				showTrend.Checked = true;
			else
				showTrend.Checked = trendEnabled != bool.FalseString;
		}
		using (INpmCustomPollerAdapter adapter = NpmAdapterFactory.CreateCustomPollerAdapter())
		{
			if (!string.IsNullOrEmpty(CustomPollerList.CustomPollerID))
			{
				Guid guid = new Guid(CustomPollerList.CustomPollerID);
				CustomPoller poller = adapter.GetCustomPoller(guid);
				if (poller.SNMPGetType == "GetSubTree")
					BarPlotColor.Visible = false;
				else
					BarPlotColor.Visible = true;
			}
			else
			{
				BarPlotColor.Visible = true;
			}
		}
	}

	public bool IsError
	{
		get { return _isError; }
		set { _isError = value; }
	}
}
