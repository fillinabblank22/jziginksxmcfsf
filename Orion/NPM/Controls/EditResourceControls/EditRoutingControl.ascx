﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditRoutingControl.ascx.cs" Inherits="Orion_NPM_Controls_EditResourceControls_EditRoutingControl" %>
<div style="margin-top: 40px">
    <b><%= Resources.NPMWebContent.NPMWEBCODE_VT0_32 %></b><br />
    

    <asp:RadioButtonList runat="server" ID="ipVersion">
        
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBCODE_VT0_33 %>" Value="4"></asp:ListItem>    
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBCODE_VT0_34 %>" Value="6"></asp:ListItem>    
    </asp:RadioButtonList>
</div>