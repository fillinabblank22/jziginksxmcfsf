using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Web.Helpers;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Controls_EditResourceControls_CustomOIDLinearGaugeEdit : BaseUndpGaugeEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack || System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            //Set scale input
            string scale = Resource.Properties["Scale"];
            if (string.IsNullOrEmpty(scale))
                scale = "100";
            scaleInput.Text = scale;

            //Set custom poller properties
            using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                try
                {
                    CustomPoller.DataSource = proxy.Api.GetCurrentCustomPollersAssigned(CustomPollerNodeID, CustomPollerInterfaceID);
                }
                catch(Exception ex)
                {
                    ControlBusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            CustomPoller.DataTextField = "UniqueName";
            CustomPoller.DataValueField = "CustomPollerID";
            CustomPoller.DataBind();

            if (!string.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
                if (CustomPoller.Items.FindByValue(Resource.Properties["CustomPollerID"]) != null)
                    CustomPoller.SelectedValue = Resource.Properties["CustomPollerID"];

            if (!string.IsNullOrEmpty(Resource.Properties["Min"]))
                GaugeMinVal.Text = Resource.Properties["Min"];
            else
                GaugeMinVal.Text = "0";

            if (!string.IsNullOrEmpty(Resource.Properties["Max"]))
                GaugeMaxVal.Text = Resource.Properties["Max"];
            else
                GaugeMaxVal.Text = "100";

            if (!string.IsNullOrEmpty(Resource.Properties["CustomScales"]))
                AutoScaleGauge.SelectedValue = Resource.Properties["CustomScales"];
            else
                AutoScaleGauge.SelectedValue = "1";

            if (!string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
                AutoHide.SelectedValue = Resource.Properties["AutoHide"];
            else
                AutoScaleGauge.SelectedValue = "0";

            if (!string.IsNullOrEmpty(Resource.Properties["Legends"]))
                if (GaugeTickCount.Items.FindByValue(Resource.Properties["Legends"]) != null)
                    GaugeTickCount.SelectedValue = Resource.Properties["Legends"];

            if (!string.IsNullOrEmpty(Resource.Properties["CustomLegends"]))
                CustomLegend.Text = Resource.Properties["CustomLegends"];
            else
                CustomLegend.Text = string.Empty;

            if (!string.IsNullOrEmpty(Resource.Properties["WarningLevel"]))
                WarningLevel.Text = Resource.Properties["WarningLevel"];
            else
                WarningLevel.Text = string.Empty;

            if (!string.IsNullOrEmpty(Resource.Properties["ErrorLevel"]))
                ErrorLevel.Text = Resource.Properties["ErrorLevel"];
            else
                ErrorLevel.Text = string.Empty;

            if (!string.IsNullOrEmpty(Resource.Properties["ReverseThreshold"]))
                ReverseThreshold.SelectedValue = Resource.Properties["ReverseThreshold"];
            else
                ReverseThreshold.SelectedValue = "0";

        }
        //Set styles combobox
        List<string> styles = GaugeHelper.GetAllGaugeStyles(GaugeType.Linear);
        foreach (string styleName in styles)
        {
            stylesList.Items.Add(new ListItem(GetStyleText(styleName), styleName));
        }
        string style = Resource.Properties["Style"];
        if (string.IsNullOrEmpty(style))
            style = "Modern";
        stylesList.SelectedValue = style;
        //Set sample gauges panel

        foreach (string gaugeStyle in styles)
        {
            LinearGaugeWrapper gauge = new LinearGaugeWrapper();
            gauge.GaugeStyle = gaugeStyle;
            gauge.BottomTitle = GetStyleText(gaugeStyle);
            gauge.IsSample = true;
            gauge.ValueFormatString = "35";
            GaugeStylesPanel.Controls.Add(gauge);

            Literal litBreak = new Literal();
            litBreak.Text = "<br/>";

            GaugeStylesPanel.Controls.Add(litBreak);
        }

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            CustomPoller.Attributes.Add("onchange", "javascript:SaveData('CustomPollerID', this.value);javascript:SaveData('CustomPollerName', this.children[this.selectedIndex].text);");
            GaugeMinVal.Attributes.Add("onchange", "javascript:SaveData('Min', this.value);");
            GaugeMaxVal.Attributes.Add("onchange", "javascript:SaveData('Max', this.value);");
            scaleInput.Attributes.Add("onchange", "javascript:SaveData('Scale', this.value);");
            AutoScaleGauge.Attributes.Add("onclick", "javascript:SaveData('CustomScales', this.rows[0].children[0].children[0].checked ? '1' : '0');");
            GaugeTickCount.Attributes.Add("onchange", "javascript:SaveData('Legends', this.value);");
            CustomLegend.Attributes.Add("onchange", "javascript:SaveData('CustomLegends', this.value);");
            WarningLevel.Attributes.Add("onchange", "javascript:SaveData('WarningLevel', this.value);");
            ErrorLevel.Attributes.Add("onchange", "javascript:SaveData('ErrorLevel', this.value);");
            ReverseThreshold.Attributes.Add("onclick", "javascript:SaveData('ReverseThreshold', this.rows[0].children[0].children[0].checked ? '1' : '0');");
            AutoHide.Attributes.Add("onclick", "javascript:SaveData('AutoHide', this.rows[0].children[0].children[0].checked ? '1' : '0');");
            stylesList.Attributes.Add("onchange", "javascript:SaveData('Style', this.value);");
        }
    }

    /// <summary>
    /// Used to retrieve the style name appropriate to locale.
    /// </summary>
    /// <param name="style">style value</param>
    /// <returns>localized style name</returns>
    private string GetStyleText(string style)
    {
        return Resources.CoreWebContent.ResourceManager.GetString(
            ResourceManagerRegistrar.Instance.CleanResxKey("GaugeStyle", style)) ?? style;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("Scale", scaleInput.Text);
            properties.Add("Style", stylesList.SelectedValue);

            //Custom 
            if (CustomPoller.Items.Count > 0)
            {
                string pollerName = CustomPoller.Items[CustomPoller.SelectedIndex].Text;

                properties.Add("CustomPollerID", CustomPoller.SelectedValue);
                properties.Add("CustomPollerName", pollerName);
            }

            properties.Add("Min", GaugeMinVal.Text);
            properties.Add("Max", GaugeMaxVal.Text);
            properties.Add("CustomScales", AutoScaleGauge.SelectedValue);
            properties.Add("Legends", GaugeTickCount.SelectedValue);
            properties.Add("WarningLevel", WarningLevel.Text);
            properties.Add("ErrorLevel", ErrorLevel.Text);
            properties.Add("ReverseThreshold", ReverseThreshold.SelectedValue);
            properties.Add("CustomLegends", CustomLegend.Text);
            properties.Add("AutoHide", AutoHide.SelectedValue);

            return properties;
        }
    }

    public override string DefaultResourceTitle
    {
        get
        {
            if (CustomPoller.Items.Count > 0)
                return string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_50, CustomPoller.Items[CustomPoller.SelectedIndex].Text);

            return string.Empty;
        }
    }

    protected void ValidateDecimalNumber(object sender, ServerValidateEventArgs args)
    {
        decimal argValue = 0;
        args.IsValid = decimal.TryParse(args.Value, out argValue);
    }

    protected void ValidateThreshold(object sender, ServerValidateEventArgs e)
    {
        string errorMessage;
        bool isValid = ThresholdHelper.ValidateThresholds(WarningLevel, ErrorLevel, ReverseThreshold, out errorMessage);

        e.IsValid = isValid;
        ThresholdCustomValidator.ErrorMessage = errorMessage;
    }
}
