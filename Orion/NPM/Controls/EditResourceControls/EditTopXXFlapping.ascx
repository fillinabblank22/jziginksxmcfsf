﻿<%@ Control Language="C#" CodeFile="EditTopXXFlapping.ascx.cs" Inherits="Orion_NPM_Controls_EditResourceControls_EditTopXXFlapping" %>
<%@ Register tagName="showFilterExamples" tagPrefix="Orion" src="~/Orion/NPM/Controls/ShowFilterExamples.ascx" %>
<div style="margin-top: 40px">
    <b><%= Resources.NPMWebContent.NPMWEBCODE_VT0_32 %></b><br />
    

    <asp:RadioButtonList runat="server" ID="ipVersion">
        
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBCODE_VT0_33 %>" Value="4"></asp:ListItem>    
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBCODE_VT0_34 %>" Value="6"></asp:ListItem>    
    </asp:RadioButtonList>
</div>
<div style="margin-top: 40px">
    <b><%= Resources.NPMWebContent.NPMWEBCODE_VT0_35 %></b><br />   
    <%       
        string textWithHyperlink = Resources.NPMWebContent.NPMWEBCODE_LH0_1;
        textWithHyperlink = textWithHyperlink.Replace("###1###", "<a href=\"/Orion/NPM/Admin/NPMSettings.aspx\" class=\"coloredLink\">").Replace("###2###", "</a>");        
    %>
    <%=textWithHyperlink %>
</div>
<%----%>
<%--<div style="margin-top: 40px; margin-bottom: 35px;">--%>
<%--    <b><%= Resources.NPMWebContent.NPMWEBCODE_VT0_38 %></b><br />--%>
<%--    <asp:textbox runat="server" textmode="MultiLine" id="swqlFilter"></asp:textbox>--%>
<%--    <br />--%>
<%--    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_261 %>--%>
<%--    <br /><br />--%>
<%--    --%>
<%--    <Orion:showFilterExamples runat="server" ID="filterExamples"/>--%>
</div>
