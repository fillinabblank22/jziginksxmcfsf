﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLastXXErrorsAndFailures.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditLastXXErrorsAndFailures" %>

<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label" Text="<%$Resources:NPMWebContent,NPMWEBDATA_AK0_87%>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$Resources:NPMWebContent,NPMWEBDATA_AK0_88%>"
                Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
        </td>
    </tr>
</table>
