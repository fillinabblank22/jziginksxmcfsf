﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUnDPChartsV2.ascx.cs" Inherits="Orion_NPM_Controls_EditResourceControls_EditUnDPChartsV2" %>
<%@ Register TagPrefix="orion" TagName="EditCustomPoller" Src="~/Orion/NPM/Controls/CustomPollerControl.ascx" %>
<table>
<orion:EditCustomPoller runat="server" ID="CustomPollerList" EnableViewState="true" />
<tr id="listChartsContainer" runat="server"></tr>
</table>