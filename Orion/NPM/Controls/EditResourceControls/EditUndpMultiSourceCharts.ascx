﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUndpMultiSourceCharts.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditUndpMultiSourceCharts" EnableViewState="true" %>

<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="npm" TagName="ListEntities" Src="~/Orion/NPM/Controls/ListEntities.ascx" %>
<%@ Register TagPrefix="npm" TagName="TopXX" Src="~/Orion/NPM/Controls/TopXXControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditAutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<%@ Register TagPrefix="npm" TagName="ListCharts" Src="~/Orion/NPM/Controls/ListCharts.ascx" %>

<table border="0" width="100%">

    <tr>
        <td style="font-weight:bold;"><%=Resources.NPMWebContent.NPMWEBDATA_AK0_50%></td>
        <td class="formRightInput">
            <asp:DropDownList ID="ChartEntity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChartEntity_Change">
                <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_7%>" Value="Orion.Nodes"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_8%>" Value="Orion.NPM.Interfaces"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>

    <tr>
        <td style="font-weight:bold;" ><%=Resources.NPMWebContent.NPMWEBDATA_AK0_51%></td>
        <td  class="formRightInput">
           <asp:DropDownList ID="UnDPsList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="UnDPsList_Change">
           </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="font-weight:bold;" ><%=Resources.NPMWebContent.NPMWEBDATA_AK0_52%></td>
        <td class="formRightInput">
            <npm:ListCharts runat="server" ID="ChartList" ParentChartInfoName="SolarWinds.NPM.Web.Charting.MultiUnDPChartInfo, SolarWinds.NPM.Web" />
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; vertical-align:top;"><%=Resources.NPMWebContent.NPMWEBDATA_AK0_53%></td>    
        <td class="formRightInput">
            <table>
            <tr>
                <td valign="top">
                    <asp:RadioButton ID="AutoSelect" runat="server" Checked="false" GroupName="SelectObjects" AutoPostBack="true" OnCheckedChanged="SelectObjects_Change"/>
                </td>
                <td>
                    <%= string.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_57,Entities.ToLower()) %><br />
                    <small style="color:#646464;"><%=Resources.NPMWebContent.NPMWEBDATA_AK0_54%></small>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="ManualSelect" runat="server" Checked="true" GroupName="SelectObjects" AutoPostBack="true" OnCheckedChanged="SelectObjects_Change"/>
                </td>
                <td>
                    <%= string.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_56, Entities.ToLower())%>&nbsp;
                    <orion:LocalizableButton id="bt_Submit" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: NPMWebContent, NPMWEBCODE_AK0_58 %>"  OnClick="SelectObjects_Click"/>
                </td>
            </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td></td>  
        <td class="formRightInput" colspan="2">
            <npm:ListEntities runat="server" ID="ListEntities" />
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold;"><%=Resources.NPMWebContent.NPMWEBDATA_AK0_55%></td>    
        <td class="formRightInput">
            <npm:TopXX runat="server" ID="TopXX" />
        </td>
    </tr>

    <tr>
        <td style="font-weight:bold;" ><%=Resources.NPMWebContent.NPMWEBDATA_AK0_56%></td>
        <td class="formRightInput">
            <asp:DropDownList ID="ShowSum" runat="server">
                <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_57%>" Value="NoSum"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_58%>" Value="OnlySum"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_59%>" Value="AlsoSum"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>

    <orion:EditPeriod runat="server" ID="Period" PeriodLabel="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_63%>"/>
    <orion:EditSampleSize runat="server" ID="SampleSize" HelpfulText="" />
    <orion:EditAutoHide runat="server" ID="AutoHide" Description=" " />

</table>

