using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common;

using Limitation = SolarWinds.Orion.Web.Limitation;
using System.Web.UI;

public partial class Orion_NPM_Controls_EditResourceControls_EditTabularCustomPoller : BaseResourceEditControl
{
	private CustomPollers _customPollers;
	private const char _separator = ' ';
	private const string _rowsResourcePrefix = "Rows:";
	private bool _error;
	private string _errorMessage;

	private readonly Log _log = new Log();

	protected new bool Error
	{
		get { return _error; }
		set { _error = value; }
	}

	public string ErrorMessage
	{
		get { return _errorMessage; }
		set { _errorMessage = value; }
	}

	protected string RowsResourceID
	{
		get { return _rowsResourcePrefix + NetObjectID; }
	}

	public void MyBusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!string.IsNullOrEmpty(NetObjectID))
        {
            using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                try
                {
                    _customPollers = proxy.Api.GetNodeTabularCustomPollers(Convert.ToInt32(NetObjectHelper.GetObjectID(NetObjectID)));
                }
                catch(Exception ex)
                {
                    MyBusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            tabularPollers.DataSource = _customPollers;
            tabularPollers.DataTextField = "UniqueName";
            tabularPollers.DataValueField = "CustomPollerID";
            tabularPollers.DataBind();

            pollersLabels.DataSource = _customPollers;
            pollersLabels.DataTextField = "UniqueName";
            pollersLabels.DataValueField = "CustomPollerID";
            pollersLabels.DataBind();

            if (Session["EmbeddedResourceData"] != null && IsPostBack && Session["EmbeddedResourceData"] is Dictionary<string, object> && (Session["EmbeddedResourceData"] as Dictionary<string, object>).ContainsKey("Pollers"))
            {
                SetSelectedItems(tabularPollers, (Session["EmbeddedResourceData"] as Dictionary<string, object>)["Pollers"].ToString().Trim());
            }
            else if (!string.IsNullOrEmpty(Resource.Properties["Pollers"]))
            {
                SetSelectedItems(tabularPollers, Resource.Properties["Pollers"]);
            }

            if (Session["EmbeddedResourceData"] != null && IsPostBack && Session["EmbeddedResourceData"] is Dictionary<string, object> && (Session["EmbeddedResourceData"] as Dictionary<string, object>).ContainsKey("LabPollID"))
            {
                pollersLabels.SelectedValue = (Session["EmbeddedResourceData"] as Dictionary<string, object>)["LabPollID"].ToString().Trim();
            }
            else if (!string.IsNullOrEmpty(Resource.Properties["LabPollID"]))
            {
                pollersLabels.SelectedValue = Resource.Properties["LabPollID"];
            }

            if (Session["EmbeddedResourceData"] != null && IsPostBack && Session["EmbeddedResourceData"] is Dictionary<string, object> && (Session["EmbeddedResourceData"] as Dictionary<string, object>).ContainsKey(RowsResourceID))
            {
                InitSelectedPollersRows(false);
                var val = (Session["EmbeddedResourceData"] as Dictionary<string, object>)[RowsResourceID].ToString().Trim();
                if (string.IsNullOrEmpty(val))
                {
                    foreach (ListItem item in tabularPollerRows.Items)
                        item.Selected = true;
                    selectAllRows.Checked = true;
                }
                SetSelectedItems(tabularPollerRows, (Session["EmbeddedResourceData"] as Dictionary<string, object>)[RowsResourceID].ToString().Trim(), ';');
                bool allSelected = true;
                foreach (ListItem item in tabularPollerRows.Items)
                    if (!item.Selected)
                    {
                        allSelected = false;
                        break;
                    }
                selectAllRows.Checked = allSelected;
            }
            else if (!string.IsNullOrEmpty(Resource.Properties[RowsResourceID]))
            {
                InitSelectedPollersRows(false);
                if (!IsPostBack)
                {
                    SetSelectedItems(tabularPollerRows, Resource.Properties[RowsResourceID]);

                    bool allSelected = true;
                    foreach (ListItem item in tabularPollerRows.Items)
                        if (!item.Selected)
                        {
                            allSelected = false;
                            break;
                        }
                    selectAllRows.Checked = allSelected;
                }
            }
            else
            {
                if (!IsPostBack)
                    InitSelectedPollersRows(true);
                else
                    InitSelectedPollersRows(false);
            }

        }
        else
        {
            _error = true;
            _errorMessage = Resources.NPMWebContent.NPMWEBCODE_VB0_116;
        }

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
			var func = @" function getPollerCheckedItems(el, separator) {
							var result = '';
							$(el).parent().parent().parent().children().each(function () {
								var inp = $(this).find('input');
								if (inp != null)
									result += (inp[0].checked ? inp.next().html() + separator : '');
							});
							return result;	
						}";
			ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key_getPollerCheckedItems", func, true);

           
			foreach (ListItem item in tabularPollers.Items)
				item.Attributes.Add("onclick", "javascript:SaveData('Pollers', getPollerCheckedItems(this, ' '));");

            pollersLabels.Attributes.Add("onchange", "javascript:SaveData('LabPollID', this.value);");

            foreach (ListItem item in tabularPollerRows.Items)
				item.Attributes.Add("onclick", "javascript:SaveData('" + RowsResourceID + "', getPollerCheckedItems(this, ';'));");

            selectAllRows.Attributes.Add("onclick", "javascript:SaveData('" + RowsResourceID + "', this.checked?'':'-1');");

            if (Session["EmbeddedResourceData"] != null && IsPostBack && Session["EmbeddedResourceData"] is Dictionary<string, object>)
            {
                if (((Dictionary<string, object>) Session["EmbeddedResourceData"]).ContainsKey("AutoHide"))
                    autoHide.AutoHideValue = ((Dictionary<string, object>) Session["EmbeddedResourceData"])["AutoHide"].ToString();
                if (((Dictionary<string, object>) Session["EmbeddedResourceData"]).ContainsKey("Pollers"))
                {
                    ((Dictionary<string, object>) Session["EmbeddedResourceData"])["Pollers"] = GetSelectedItems(tabularPollers, string.Empty);
                }
                if (((Dictionary<string, object>) Session["EmbeddedResourceData"]).ContainsKey(RowsResourceID))
                {
                    ((Dictionary<string, object>) Session["EmbeddedResourceData"])[RowsResourceID] = GetSelectedItems(tabularPollerRows, string.Empty);
                }
            }
        }
    }

	public override Dictionary<string, object> Properties
	{
		get 
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();

			string oldPollers =
			!string.IsNullOrEmpty(Resource.Properties["Pollers"]) ?
				Resource.Properties["Pollers"] :
				string.Empty;

			string oldRows =
				!string.IsNullOrEmpty(Resource.Properties[RowsResourceID]) ?
					this.Resource.Properties[RowsResourceID] :
					string.Empty;

			// save row filters
			Dictionary<string, string> savedRows = new Dictionary<string, string>();
			foreach (string key in Resource.Properties.Keys)
				if (key.StartsWith(_rowsResourcePrefix, StringComparison.InvariantCultureIgnoreCase))
					savedRows.Add(key, Resource.Properties[key]);

			if (tabularPollers.Items.Count > 0 && String.IsNullOrEmpty(GetSelectedItems(tabularPollers, string.Empty)))
			{
				selectPollersError.Visible = true;
				selectRowsError.Visible = false;
				return properties;
			}

			if (this.tabularPollerRows.Items.Count > 0 && String.IsNullOrEmpty(this.GetSelectedItems(this.tabularPollerRows, string.Empty)))
			{
				selectRowsError.Visible = true;
				selectPollersError.Visible = false;
				return properties;
			}

			properties.Add("AutoHide", autoHide.AutoHideValue);
			properties.Add("Pollers", GetSelectedItems(tabularPollers, oldPollers));
			properties.Add("LabPollID", pollersLabels.SelectedValue);

			// restore row filters
			foreach (KeyValuePair<string, string> savedRow in savedRows)
				properties.Add(savedRow.Key, savedRow.Value);

			// update filter for this node
			if (selectAllRows.Checked)
				properties.Add(RowsResourceID, string.Empty);
			else
				properties.Add(RowsResourceID, GetSelectedItems(tabularPollerRows, oldRows));
			
			return properties;
		}
	}
	
	private string GetSelectedItems(ListControl listControl, string oldValues)
	{
		if (oldValues == null)
			oldValues = String.Empty;

		StringBuilder values = new StringBuilder();
		foreach (ListItem item in listControl.Items)
		{
			if (item.Selected)
			{
				values.Append(item.Value);
				values.Append(_separator);
			}
		}

		if (values.Length > 0)
		{
			values.Length--;
			return values.ToString();
		}
		return oldValues;
	}

	private void SetSelectedItems(ListControl listControl, string values)
	{
        SetSelectedItems(listControl, values, _separator);
	}

    private void SetSelectedItems(ListControl listControl, string values, char separator)
    {
        foreach (string value in values.Split(separator))
        {
            ListItem foundItem = listControl.Items.FindByValue(value);
            if (foundItem != null)
            {
                foundItem.Selected = true;
            }
            else
            {
                foundItem = listControl.Items.FindByText(value);
                if (foundItem != null)
                {
                    foundItem.Selected = true;
                }
            }
        }
    }

	protected void PollersChanged(object sender, EventArgs e)
	{
		InitSelectedPollersRows(true);
	}

	private void InitSelectedPollersRows(bool selectRows)
	{
		Dictionary<string, bool> oldRows = new Dictionary<string, bool>();
		foreach (ListItem item in tabularPollerRows.Items)
		{
			oldRows[item.Value] = item.Selected;
		}
		tabularPollerRows.Items.Clear();
		selectAllRows.Checked = false;

		using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
		{
			CustomPollers selectedPollers = new CustomPollers();
			foreach (ListItem item in tabularPollers.Items)
			{
				if (item.Selected)
				{
					try
					{
						CustomPoller customPoller = _customPollers.FindByID(new Guid(item.Value));
						if (customPoller != null)
						{
							selectedPollers.Add(customPoller);
						}
					}
					catch { }
					// exception occurs only if item.Value is not a guid, in that case nothing should happen
				}
			}
			bool rowsExist = selectedPollers.Count > 0;
			if (rowsExist)
			{
				Guid labelPollerId =
					!string.IsNullOrEmpty(pollersLabels.SelectedValue) ?
						new Guid(pollersLabels.SelectedValue) :
						new Guid();
                DataTable table;
                try
                {
                    table = proxy.Api.GetTabularCustomPollerData(selectedPollers, labelPollerId, Convert.ToInt32(NetObjectHelper.GetObjectID(NetObjectID)), Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    MyBusinessLayerExceptionHandler(ex);
                    throw;
                }

                if (table != null)
				{
					bool allChecked = true;
					foreach (DataRow row in table.Rows)
					{
						string rowId = row[0].ToString();
						string rowLabel = row[1].ToString();
						ListItem item = new ListItem(rowLabel, rowId);
						tabularPollerRows.Items.Add(item);
						//selected are only new items and items previously selected
						if (selectRows)
							item.Selected = (!oldRows.ContainsKey(rowId) || oldRows[rowId]);
						if (!item.Selected)
							allChecked = false;
					}

					if (selectRows)
						selectAllRows.Checked = allChecked;
				}
				else
				{
					rowsExist = false;
				}
			}
			pollerRows.Visible = rowsExist;
		}
	}

	protected void Check_Clicked(Object sender, EventArgs e)
	{
		for (int i = 0; i < tabularPollerRows.Items.Count; i++)
		{
			if (!tabularPollerRows.Items[i].Selected)
			{
				this.selectAllRows.Checked = false;
				return;
			}
		}
	}

	protected void AllRowsSelected(object sender, EventArgs e)
	{
		foreach (ListItem item in tabularPollerRows.Items)
		{
			item.Selected = selectAllRows.Checked;
		}
	}
}
