﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditVSANCharts.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditVSANCharts" EnableViewState="true" %>

<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>


<style type="text/css"> 
    .formHelpfulTxt { display:none; }
</style>

<table border="0" width="400" >
<tr>
<td>
    <orion:EditPeriod runat="server" ID="Period" PeriodLabel="<%$ Resources: NPMWebContent, NPMWEBDATA_AK0_63%>"/>
    </td>
    </tr>
<tr>
<td>
    <orion:EditSampleSize runat="server" ID="SampleSize" />
    </td>
    </tr>
</table>

