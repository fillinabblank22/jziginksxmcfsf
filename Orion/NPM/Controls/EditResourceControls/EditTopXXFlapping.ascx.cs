﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Controls_EditResourceControls_EditTopXXFlapping : BaseResourceEditControl
{
	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		if (!IsPostBack)
		{
			LoadProperties(Resource.Properties);
		}

/*		filterExamples.EntitiesList = new string[]
		{
			"Orion.Nodes",
			"Orion.Routing.RoutingTableFlap"
		};*/
	}

	private void LoadProperties(ResourcePropertyCollection properties)
	{
		ipVersion.SelectedValue = properties["IpVersion"] ?? "4";
//		swqlFilter.Text = properties["Filter"];
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			var rv = new Dictionary<string, object>();
			rv["IpVersion"] = ipVersion.SelectedValue;
//			rv["Filter"] = swqlFilter.Text;
			return rv;
		}
	}
}