﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NPM_Controls_EditResourceControls_EditMultiUnDPCharts : UserControl, IChartEditorSettings
{
    // TODO: make configurable
    private const int MaxUnDPLimit = 10;
    private const char Separator = ',';

    public void Initialize(ChartSettings settings, SolarWinds.Orion.Web.ResourceInfo resourceInfo, string netObjectId)
    {
        LoadProperties(resourceInfo.Properties);
        OnSelectionChanged(this, EventArgs.Empty);
    }

    private void LoadProperties(StringDictionary properties)
    {
        if (properties != null)
        {
            LeftLabelUnit.CustomLabel = GetPropertyValue(properties, "Label_y1");
            RightLabelUnit.CustomLabel = GetPropertyValue(properties, "Label_y2");

            LeftLabelUnit.Unit = GetPropertyValue(properties, "Unit_y1");
            RightLabelUnit.Unit = GetPropertyValue(properties, "Unit_y2");

            LeftUnDPs.NetObjectPrefix = RightUnDPs.NetObjectPrefix = ResolveNetObjectPrefix(properties);

            LeftUnDPs.SelectedUnDPs = ParseCustomPollerID(GetPropertyValue(properties, "CustomPollerID_y1"));
            RightUnDPs.SelectedUnDPs = ParseCustomPollerID(GetPropertyValue(properties, "CustomPollerID_y2"));
        }
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties["Label_y1"] = LeftLabelUnit.CustomLabel;
        properties["Label_y2"] = RightLabelUnit.CustomLabel;
        properties["Unit_y1"] = LeftLabelUnit.Unit;
        properties["Unit_y2"] = RightLabelUnit.Unit;
        properties["CustomPollerID_y1"] = ParseCustomPollerID(LeftUnDPs.SelectedUnDPs);
        properties["CustomPollerID_y2"] = ParseCustomPollerID(RightUnDPs.SelectedUnDPs);
    }

    public void OnSelectionChanged(object sender, EventArgs e)
    {
        // only 10 undps total (left + right) can be added to the chart
        var limitReached = (LeftUnDPs.SelectedUnDPs.Count + RightUnDPs.SelectedUnDPs.Count) == MaxUnDPLimit;
        SetVisibility(limitReached);
    }

    private string ResolveNetObjectPrefix(StringDictionary properties)
    {
        var netObjectID = GetPropertyValue(properties, "NetObjectID");
        if (!String.IsNullOrEmpty(netObjectID))
        {
            // custom object resource context
            return netObjectID.Substring(0, 1);
        }

        // view context, nodes by default
        return String.IsNullOrEmpty(Request["NetObject"]) ? "N" : Request["NetObject"].Substring(0, 1);
    }

    private void SetVisibility(bool limitReached)
    {
        LimitReached.Visible = limitReached;

        LeftUnDPs.AddButtonVisible =
            RightUnDPs.AddButtonVisible = !limitReached;
    }

    private static string GetPropertyValue(StringDictionary properties, string propertyName)
    {
        if (properties.ContainsKey(propertyName))
            return properties[propertyName];
        else
            return string.Empty;
    }

    private static List<string> ParseCustomPollerID(string customPollerID)
    {
        if (String.IsNullOrEmpty(customPollerID))
            return new List<string>();

        return customPollerID.Split(new char[1] {Separator}, StringSplitOptions.RemoveEmptyEntries).ToList();
    }

    private static string ParseCustomPollerID(List<string> selectCustomPollerID)
    {
        var sb = new StringBuilder();
        selectCustomPollerID.FindAll(s => !String.IsNullOrEmpty(s)).ForEach(s => sb.AppendFormat("{0}{1}", s, Separator));
        return sb.ToString().TrimEnd(Separator);
    }
}