﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting;

public partial class Orion_NPM_Controls_EditResourceControls_EditLastXXErrorsAndFailures : BaseResourceEditControl
{
	private static int defaultNumberOfEvents = 25;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		this.MaxEventsText.Text = Resource.Properties["MaxEvents"];

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            MaxEventsText.Attributes.Add("onchange", "javascript:SaveData('MaxEvents', this.value);");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			try
			{
				Int32.Parse(this.MaxEventsText.Text);
				properties.Add("MaxEvents", this.MaxEventsText.Text);
			}
			catch 
			{
				properties.Add("MaxEvents", defaultNumberOfEvents.ToString());
			}

			return properties;
		}
	}
}
