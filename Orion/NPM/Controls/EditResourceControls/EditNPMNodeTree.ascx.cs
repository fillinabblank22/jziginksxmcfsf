﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_NPMEditNodeTree : BaseResourceEditControl
{
    private const string _vsanName = "VSAN Name";
    private const string _npmVsansNodesVsanname = "NPM_VSANs_Nodes.VsanName";

    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		SQLFilter.FilterTextBox.Text = Resource.Properties["Filter"];

		bool allowUnsortableProperties;
		bool.TryParse(Request.QueryString["AllowUnsortableProperties"] ?? "false", out allowUnsortableProperties);

		bool hideRememberExpanded;
		bool.TryParse(Request.QueryString["HideRememberExpanded"] ?? "false", out hideRememberExpanded);

		if (!this.IsPostBack)
		{
			foreach (string propName in Node.GetCustomPropertyNames(allowUnsortableProperties))
			{
				lbxGroup2.Items.Add(propName);
				lbxGroup3.Items.Add(propName);
			}

			string grouping = Resource.Properties["Grouping2"];

			if (grouping != null)
				lbxGroup2.SelectedValue = grouping;
			else
				lbxGroup2.SelectedValue = string.Empty;

			grouping = Resource.Properties["Grouping3"];

			if (grouping != null
				&& !grouping.Equals(lbxGroup2.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
				lbxGroup3.SelectedValue = grouping;
			else
				lbxGroup3.SelectedValue = string.Empty;

			GroupNulls.SelectedValue = Resource.Properties["GroupNodesWithNullPropertiesAsUnknown"] ?? "true";

			RememberCollapseState.Visible = !hideRememberExpanded;
			string rememberCollapseState = Resource.Properties["RememberCollapseState"] ?? "true";
			RememberCollapseState.Checked = Boolean.Parse(rememberCollapseState);
		}
	}

	private int ValidGroups
	{
		get
		{
			if (string.IsNullOrEmpty(lbxGroup2.SelectedValue))
				return 1;
			if (string.IsNullOrEmpty(lbxGroup3.SelectedValue))
				return 2;

			return 3;
		}
	}

	private string BuildSubTitle()
	{
		switch (this.ValidGroups)
		{
			case 0:
			case 1:return string.Empty;
            case 2:return string.Format(Resources.NPMWebContent.NPMWEBCODE_TM0_32, lbxGroup2.SelectedItem.Text);
			case 3:return string.Format(Resources.NPMWebContent.NPMWEBCODE_TM0_33, lbxGroup2.SelectedItem.Text, lbxGroup3.SelectedItem.Text);
		}

		return string.Empty;
	}

	private void FixupBlankGroupings()
	{
		List<string> groups = new List<string>();
        groups.Add(_npmVsansNodesVsanname);
        groups.Add(lbxGroup2.SelectedValue);
		groups.Add(lbxGroup3.SelectedValue);

		groups.RemoveAll(string.IsNullOrEmpty);

		for (int i = 0; i < 3; ++i) groups.Add(string.Empty);

		lbxGroup2.SelectedValue = groups[1];
		lbxGroup3.SelectedValue = groups[2];
	}

	public override string DefaultResourceSubTitle
	{
		get { return BuildSubTitle(); }
	}

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return Resources.NPMWebContent.NPMWEBCODE_TM0_31;
        }
    }

	public override Dictionary<string, object> Properties
	{
		get 
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();

			FixupBlankGroupings();

            properties.Add("Grouping1", _npmVsansNodesVsanname);
			properties.Add("Grouping2", lbxGroup2.SelectedValue);
			properties.Add("Grouping3", lbxGroup3.SelectedValue);

			properties.Add("Filter", SqlFilterChecker.CleanFilter(SQLFilter.FilterTextBox.Text));

			properties.Add("GroupNodesWithNullPropertiesAsUnknown", GroupNulls.SelectedValue);
			properties.Add("RememberCollapseState", RememberCollapseState.Checked.ToString());

			// We changed stuff so clear out the expanded tree node information
			TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
			manager.Clear();

			return properties;
		}
	}
}
