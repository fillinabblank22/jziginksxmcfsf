﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditNPMNodeTree.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_NPMEditNodeTree" %>

<%@ Register TagPrefix="orion" TagName="FilterNodesSql" Src="~/Orion/Controls/FilterNodesSql.ascx" %>
<script type="text/javascript">
       function ClientValidate(source, arguments) {
           var lbxGroup2 = $('#<%= this.lbxGroup2.ClientID %>');

           if (arguments.Value != lbxGroup2[0].value)
               arguments.IsValid = true;
           else
               arguments.IsValid = false; 
       }
</script>
    <%=Resources.NPMWebContent.NPMWEBDATA_TM0_41%>
<p>
    <%=Resources.NPMWebContent.NPMWEBDATA_TM0_36%><br />
    <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBCODE_AK0_39%>" Value="" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_108%>" Value="Status" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_42%>" Value="Vendor" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_43%>" Value="MachineType" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_44%>" Value="Contact" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_45%>" Value="Location" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_46%>" Value="IOSImage" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_47%>" Value="IOSVersion" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_48%>" Value="ObjectSubType" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_49%>" Value="SysObjectID" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_50%>" Value="NPM_NV_EW_NODES_V.EnergyWise" />
    </asp:ListBox>
</p>
<p>
    <%=Resources.NPMWebContent.NPMWEBDATA_TM0_37%><br />
    <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBCODE_AK0_39%>" Value="" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_108%>" Value="Status" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_42%>" Value="Vendor" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_51%>" Value="MachineType" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_44%>" Value="Contact" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_45%>" Value="Location" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_46%>" Value="IOSImage" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_47%>" Value="IOSVersion" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_48%>" Value="ObjectSubType" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_49%>" Value="SysObjectID" />
        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_50%>" Value="NPM_NV_EW_NODES_V.EnergyWise" />
    </asp:ListBox>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="lbxGroup3" 
        ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_52%>"></asp:CustomValidator>
</p>
<b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_38%></b>
<asp:RadioButtonList runat="server" ID="GroupNulls">
    <asp:ListItem Value="true" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_39%>" />
    <asp:ListItem Value="false" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_40%>" />
</asp:RadioButtonList>
<p>
    <asp:CheckBox runat="server" ID="RememberCollapseState" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_53%>" />
</p>
<orion:FilterNodesSql runat="server" ID="SQLFilter" />