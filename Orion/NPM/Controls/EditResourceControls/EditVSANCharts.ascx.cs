﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Controls_EditResourceControls_EditVSANCharts : BaseResourceEditControl
{
	private static readonly Log _log = new Log();
    private StringDictionary parameters = null;

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

    public bool SummaryPage
    {
        get
        {
            if (String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                return true;

            NetObject myObj = NetObjectFactory.Create(Request.QueryString["NetObject"]);
            if (myObj != null && (myObj is Node || myObj is Interface || myObj is Volume))
                return false;

            return true;
        }
    }

    
    private void LoadProperties(StringDictionary properties)
    {
        if (properties != null)
        {
            if (properties.ContainsKey("Period"))
            {
                Period.PeriodName = properties["Period"];
            }

            if (properties.ContainsKey("SampleSize"))
            {
                SampleSize.SampleSizeValue = properties["SampleSize"];
            }

         
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            LoadProperties(Resource.Properties);

            LoadProperties(parameters);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }

	private Dictionary<string, object> GetProperties()
	{
		var properties = new Dictionary<string, object>();
        properties.Add("Period", Period.PeriodName);
		properties.Add("SampleSize", SampleSize.SampleSizeValue);        
		return properties;
	}

	public override Dictionary<string, object> Properties
	{
		get { return GetProperties(); }
	}

    protected void SelectObjects_Click(object source, EventArgs e)
    {
        var data = new StringDictionary();
        data["Period"] = Period.PeriodName;
        data["SampleSize"] = SampleSize.SampleSizeValue;
        string url = String.Format("/Orion/NPM/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjects.aspx{0}", GetUrlParameters());
        this.Response.Redirect(url);
    }

    private string GetUrlParameters()
    {
        var list = new List<string>();

        AddUrlParameter(list, "ResourceID");     
        AddUrlParameter(list, "ViewID");     

        return (list.Count > 0) ? "?" + String.Join("&", list.ToArray()) : String.Empty;
    }

    private void AddUrlParameter(List<string> list, string name)
    {
        if (!String.IsNullOrEmpty(Request.QueryString[name]))
            list.Add(String.Format("{0}={1}", name, Request[name]));
    }    
}
