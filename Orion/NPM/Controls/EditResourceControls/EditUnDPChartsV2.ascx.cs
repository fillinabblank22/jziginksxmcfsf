﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web;

public partial class Orion_NPM_Controls_EditResourceControls_EditUnDPChartsV2 : System.Web.UI.UserControl, IChartEditorSettings
{
	protected string SelectedPoller { get { return CustomPollerList.CustomPollerID; } set { CustomPollerList.CustomPollerID = value; } }
	protected string SelectedRows { get { return CustomPollerList.CustomPollerRows; } set { CustomPollerList.CustomPollerRows = value; } }

	private string _netObject
	{
		get { return ViewState["NetObject"] as string; }
		set { ViewState["NetObject"] = value; }
	}

	private void GetNetObjectInfoForCORIfThere(ref string netObject, ref string netObjectPrefix, ResourceInfo rcol, ChartResourceSettings settings = null)
	{
        // we can be on COR
        if (rcol != null && rcol.Properties != null 
            && rcol.Properties.ContainsKey("NetObjectID") 
            && !string.IsNullOrWhiteSpace(rcol.Properties["NetObjectID"])
            && netObject != rcol.Properties["NetObjectID"]) 
		{
			netObject = rcol.Properties["NetObjectID"];
		}

        // netObjectPrefix (view) can be different than the netObject (entity)
        if (settings != null && settings.ChartSettings != null
            && !string.IsNullOrWhiteSpace(settings.ChartSettings.NetObjectPrefix)
            && netObjectPrefix != settings.ChartSettings.NetObjectPrefix)
		{
			netObjectPrefix = settings.ChartSettings.NetObjectPrefix;
		}
	}

	protected override void OnInit(EventArgs e)
	{
		CustomPollerList.ListChartsContainer = listChartsContainer;
		if (IsPostBack) /* Initialize isn't called anymore, do it on our own */
		{
			/* hey, fuck the asp.net. i'll take the info from my request */
			int resId = 0;
			int.TryParse(Request.QueryString["ResourceID"], out resId);

			var resInfo = new ResourceInfo {ID = resId};
            var settings = ChartResourceSettings.FromResource(resInfo, new ChartSettingsDAL());

            var netObject = Request.QueryString["NetObject"];
            var netObjectPrefix = settings.NetObjectPrefix;
            
            GetNetObjectInfoForCORIfThere(ref netObject, ref netObjectPrefix, resInfo, settings);
			
			DoInitialization(netObject, netObjectPrefix);
		}

		base.OnInit(e);
	}

	public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
	{
	    var netObjectPrefix = settings.NetObjectPrefix;
		GetNetObjectInfoForCORIfThere(ref netObjectId, ref netObjectPrefix, resourceInfo);

		_netObject = netObjectId;

		SelectedPoller = resourceInfo.Properties["CustomPollerID"];
		SelectedRows = resourceInfo.Properties[netObjectId + ":Rows"];

        DoInitialization(netObjectId, netObjectPrefix);
	}

	private void DoInitialization(string netObjectId, string netObjectPrefix)
	{
		if (netObjectPrefix == "NPM_SUMMARY")
			CustomPollerList.InitSummaryCustomPollerList();
		else if (netObjectPrefix == "I")
			CustomPollerList.InitInterfaceCustomPollerList(int.Parse(netObjectId.Split(':')[1]));
		else if (netObjectPrefix == "N")
		{
			using (var swis = InformationServiceProxy.CreateV3())
			{ // check if we're on tabular poller (GetSubTree method)
				/* we may be on interface page, but configurating node */
				var nid = netObjectId.Split(':');
				int nodeid;

				if (nid[0] == "I")
					nodeid = Convert.ToInt32(swis.Query("SELECT NodeID FROM Orion.NPM.Interfaces WHERE InterfaceID=@ifid", new Dictionary<string, object>{{"ifid", nid[1]}}).Rows[0][0]);
				else
					nodeid = int.Parse(nid[1]); // may not work for some other situations!

				CustomPollerList.InitNodeCustomPollerList(nodeid);
			}
		}
		else
		{
			throw new NotImplementedException();
		}
	}

	public void SaveProperties(Dictionary<string, object> properties)
	{
		if (string.IsNullOrEmpty(SelectedPoller))
			return;

		properties["CustomPollerID"] = SelectedPoller;

		using (var swis = InformationServiceProxy.CreateV3())
		{
			bool nodeTabularCustomPoller = false;
			var rows = swis.Query("SELECT SNMPGetType FROM Orion.NPM.CustomPollers WHERE CustomPollerID=@cpid",
			                      new Dictionary<string, object> {{"cpid", SelectedPoller}});

			if (rows.Rows.Count == 1)
			{
				nodeTabularCustomPoller = rows.Rows[0][0].ToString() == "GetSubTree";
			}

			if (nodeTabularCustomPoller)
				properties[_netObject + ":Rows"] = SelectedRows;
		}
	}
}