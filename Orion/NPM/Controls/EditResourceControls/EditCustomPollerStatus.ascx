<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomPollerStatus.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditCustomPollerStatus" %>
<%@ Register TagPrefix="orion" TagName="AutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>

<table class="text" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <orion:AutoHide runat="server" ID="autoHide" Description="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_141%>" />
    </tr>
</table>
