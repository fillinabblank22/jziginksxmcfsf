﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomOIDEditGauge.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_CustomOIDEditGauge" %>

<asp:ValidationSummary ID="ValidationSummary" runat="server" />
<br />
<span id="GaugesList">
    <asp:DropDownList ID="stylesList" runat="server" onchange="SelectCurrentGauge()">
    </asp:DropDownList>
</span>
<br />
<br />
<b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_176%></b>
<br />
<asp:TextBox ID="scaleInput" runat="server" MaxLength="3"></asp:TextBox>
<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="scaleInput"
    Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_142%>" MinimumValue="30"
    MaximumValue="250" Type="Integer">*</asp:RangeValidator><asp:RequiredFieldValidator
        ID="RequiredFieldValidator1" runat="server" ControlToValidate="scaleInput" Display="Dynamic"
        ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_143%>">*</asp:RequiredFieldValidator>
<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="scaleInput"
    Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_144%>" Operator="DataTypeCheck"
    Type="Integer">*</asp:CompareValidator>
<br />
<br /> 
<b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_145%></b>
<br />
<asp:DropDownList ID="CustomPoller" runat="server" />
<br />
<br />
<b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_146%></b>
<br />
<asp:TextBox ID="GaugeMinVal" runat="server" />
<asp:CompareValidator ID="CompareValidator8" runat="server" ControlToValidate="GaugeMinVal"
    Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_147%>"
    Operator="DataTypeCheck" Type="Double">*</asp:CompareValidator>
<asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="GaugeMinVal"
    ControlToCompare="GaugeMaxVal" Display="Dynamic" Operator="LessThan" Type="Double">*</asp:CompareValidator>
<asp:CustomValidator ID="CustomValidator" runat="server" ControlToValidate="GaugeMinVal"
    Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_148%>"
    OnServerValidate="ValidateDecimalNumber">*</asp:CustomValidator>
<br />
<br />
<b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_149%></b>
<br />
<asp:TextBox ID="GaugeMaxVal" runat="server" />
<asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="GaugeMaxVal"
    Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_150%>"
    Operator="DataTypeCheck" Type="Double">*</asp:CompareValidator>
<asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="GaugeMaxVal"
    ControlToCompare="GaugeMinVal" Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_151%>"
    Operator="GreaterThan" Type="Double">*</asp:CompareValidator>
<asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="GaugeMaxVal"
    ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_152%>" Display="Dynamic"
    OnServerValidate="ValidateDecimalNumber">*</asp:CustomValidator>
<br />
<br />
<table class="text" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td colspan="5">
            <font size="+1"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_178%></font>
        </td>
        <td id="advanced-collapse-button">
            <orion:LocalizableButtonLink runat ="server" LocalizedText="CustomText" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_154 %>" DisplayType="Resource" onclick = "CollapseAdvanced(true)"/>
        </td>
        <td id="advanced-expand-button" >
           <orion:LocalizableButtonLink LocalizedText="CustomText" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_155 %>" DisplayType="Resource" onclick = "CollapseAdvanced(false)" runat="server"/>
        </td>
    </tr>
    <tr class="advanced-collapse">
        <td>
            <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_156%></b><br />
            <asp:RadioButtonList ID="AutoScaleGauge" runat="server" RepeatDirection="Vertical">
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_157%>" Value="1" />
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_158%>" Value="0" />
            </asp:RadioButtonList>
        </td>
        <td colspan="3">
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_159%>
        </td>
    </tr>
    <tr class="advanced-collapse">
        <td>
            <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_160%></b><br />
            <asp:DropDownList ID="GaugeTickCount" runat="server">
                <asp:ListItem Text="3" Value="3" />
                <asp:ListItem Text="4" Value="4" />
                <asp:ListItem Text="5" Value="5" />
                <asp:ListItem Text="6" Value="6" />
                <asp:ListItem Text="7" Value="7" />
                <asp:ListItem Text="8" Value="8" />
                <asp:ListItem Text="9" Value="9" />
                <asp:ListItem Text="10" Value="10" />
                <asp:ListItem Text="11" Value="11" />
            </asp:DropDownList>
        </td>
        <td>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_161%>
        </td>
        <td>
            <img src="../../images/GaugeImages/TickCount3.png" border="0">
            <br>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_162%> = 3
        </td>
        <td>
            <img src="../../images/GaugeImages/TickCount11.png" border="0">
            <br>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_162%> = 11
        </td>
    </tr>
    <tr class="advanced-collapse">
        <td>
            <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_163%></b><br />
            <asp:TextBox ID="CustomLegend" size="40" runat="server" />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="CustomLegend"
                ValidationExpression="(^.+,(.+,)*([^,])*$)|(^$)" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_164%>"
                Display="Dynamic"> 
            </asp:RegularExpressionValidator>
        </td>
        <td>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_165%>
        </td>
        <td>
            <img src="../../images/GaugeImages/CustomLegeng1.png" border="0">
            <br>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_163%> = 0,2k,4k,6k,8k,10k
        </td>
        <td>
            <img src="../../images/GaugeImages/CustomLegeng2.png" border="0">
            <br>
           <%=Resources.NPMWebContent.NPMWEBDATA_VB0_182%>
        </td>
    </tr>
    <tr class="advanced-collapse">
        <td>
            <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_167%></b><br />
            <asp:TextBox ID="WarningLevel" size="10" runat="server" />
            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="WarningLevel"
                Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_168%>"
                Operator="DataTypeCheck" Type="Double">*</asp:CompareValidator>
        </td>
        <td>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_169%>
        </td>
    </tr>
    <tr class="advanced-collapse">
        <td>
            <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_170%></b><br />
            <asp:TextBox ID="ErrorLevel" size="10" runat="server" />
            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ErrorLevel"
                Display="Dynamic" ErrorMessage="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_171%>"
                Operator="DataTypeCheck" Type="Double">*</asp:CompareValidator>
            <asp:CustomValidator ID="ThresholdCustomValidator" runat="server" ControlToValidate="GaugeMinVal"
                Display="Dynamic" ErrorMessage="" OnServerValidate="ValidateThreshold"></asp:CustomValidator>
        </td>
        <td>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_172%>
        </td>
    </tr>
    <tr class="advanced-collapse">
        <td>
            <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_179%></b><br />
            <asp:RadioButtonList ID="ReverseThreshold" runat="server" RepeatDirection="Vertical">
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_157%>" Value="1" />
                <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_158%>" Value="0" />
            </asp:RadioButtonList>
        </td>
        <td>
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_180%>
        </td>
    </tr>
    <tr>
        <td style="white-space:nowrap;">
            <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_174%></b><br />
            <asp:RadioButtonList ID="AutoHide" runat="server">
                <asp:ListItem Value="1" Text="<%$ Resources: NPMWebContent, NPMWEBCODE_VB0_30%>" />
                <asp:ListItem Value="0" Text="<%$ Resources: NPMWebContent, NPMWEBCODE_VB0_31%>" Selected="True" />
            </asp:RadioButtonList>
        </td>
        <td colspan="3">
            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_175%>
        </td>
    </tr>
</table>
<div id="GaugeStylesPanel" runat="server" style="background-color:White; width:840px"></div>
