﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMultiUnDPCharts.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditMultiUnDPCharts" %>
<%@ Register TagPrefix="npm" TagName="SelectUnDPs" Src="~/Orion/NPM/Controls/CustomPollerAxisControl.ascx" %>
<%@ Register TagPrefix="npm" TagName="LabelUnit" Src="~/Orion/NPM/Controls/LabelUnitAxisControl.ascx" %>
<orion:Include runat="server" File="../NPM/js/ToolTip.js" />
<orion:Include runat="server" File="../NPM/styles/MultiUnDP.css" />
<div class="TooltipLinkPlace">
    <a href="<%=SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("npm", "OrionPHUniversalDevicePollerHome")%>" class="TooltipLink" target="_blank"><span id="toolTip"></span></a>
    <script type="text/javascript">
        $().ready(function() {
            $('#toolTip').html(GetTooltipHtml());
        });
    </script>
</div>
<table class="multiUnDP" cellpadding="0" cellspacing="10">
    <tr>
        <td valign="top">
            <npm:SelectUnDPs ID="LeftUnDPs" runat="server" AddFirstByDefault="true" AxisLabel="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_26 %>"
                AddButtonText="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_24 %>" OnSelectionChanged="OnSelectionChanged" />
        </td>
        <td valign="top" class="right">
            <npm:SelectUnDPs ID="RightUnDPs" runat="server" AddFirstByDefault="false" AxisLabel="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_27 %>"
                AxisHintLabel="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_28 %>" AddButtonText="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_29 %>"
                OnSelectionChanged="OnSelectionChanged" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="limit">
            <asp:Label ID="LimitReached" runat="server" Visible="false" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_ZB0_30 %>" />
        </td>
    </tr>
    <tr>
        <td valign="bottom">
            <npm:LabelUnit ID="LeftLabelUnit" runat="server" />
        </td>
        <td valign="bottom" class="right">
            <npm:LabelUnit ID="RightLabelUnit" runat="server" />
        </td>
    </tr>
</table>
