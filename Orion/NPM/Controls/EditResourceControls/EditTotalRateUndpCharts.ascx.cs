﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.NPM.Common.Models;
using System.Collections;

public partial class Orion_NPM_Controls_EditResourceControls_EditTotalRateUndpCharts : BaseResourceEditControl
{
	private static readonly Log _log = new Log();
//	private string _chartType;
//	private string _netObjectID;
	private string _netObjectPrefix;
	private bool _isError;

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	protected override void OnInit(EventArgs e)
	{
		_netObjectPrefix = NetObjectID.Substring(0, NetObjectID.IndexOf(':'));
        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle2"]))
        {
            ResourceSubTitle2.Text = Resource.Properties["SubTitle2"];
        }
        else
        {
            ResourceSubTitle2.Text = String.Empty;
        }
	    OnInitControls();
		base.OnInit(e);
	}

	private void OnInitControls()
	{
		OnInitCustomPollerList();
		BarPlotColor.PlotColorField = "SubsetColor";
		BarPlotColor.ControlName = "Plot Color for Bars";
		BarPlotColor.ControlDescription = "You may select a color for the bar-charted data plotted on the graph from the drop-down list, or select \"(Custom)\" and enter a hex value for the color into the adjacent text box.";
		BarPlotColor.SetupControl();
		LinePlotColor.PlotColorField = "RYSubsetColor";
		LinePlotColor.ControlName = "Plot Color for Line";
		LinePlotColor.ControlDescription = "You may select a color for the line chart data plotted on the graph from the drop-down list, or select \"(Custom)\" and enter a hex value for the color into the adjacent text box.";
		LinePlotColor.SetupControl();
	}

	private void OnInitCustomPollerList()
	{
		NetObject netObject = null;
		int netObjectID = 0;

		if (!string.IsNullOrEmpty(NetObjectID))
		{
			if (_netObjectPrefix == "N")
			{
				netObject = NetObjectFactory.Create(NetObjectID);
				netObjectID = Convert.ToInt32(netObject["NodeID"].ToString());
//				_netObjectID = "N:" + netObjectID.ToString();
			}
			else
			{
				netObjectID = Convert.ToInt32(NetObjectHelper.GetObjectID(NetObjectID));
//				_netObjectID = "I:" + netObjectID.ToString();
			}
		}

		switch (_netObjectPrefix)
		{
			case "N":
				CustomPollerList.InitNodeCounterCustomPollerList(netObjectID);
				break;
			case "I":
				CustomPollerList.InitInterfaceCounterCustomPollerList(netObjectID);
				break;
			default:
				CustomPollerList.InitSummaryCounterCustomPollerList();
				break;
		}
	}

	private Dictionary<string, object> GetProperties()
	{
		Dictionary<string, object> properties = new Dictionary<string, object>();

		String oldCustomPollerId;
		CustomPoller poller = null;

		using (INpmCustomPollerAdapter adapter = NpmAdapterFactory.CreateCustomPollerAdapter())
		{
			if (!string.IsNullOrEmpty(CustomPollerList.CustomPollerID))
			{
				poller = adapter.GetCustomPoller(new Guid(CustomPollerList.CustomPollerID));
				if (poller.SNMPGetType == "GetSubTree" && !CustomPollerList.IsValid)
				{
					IsError = true;
					return properties;
				}
			}
		}

		if (!String.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
			oldCustomPollerId = Resource.Properties["CustomPollerID"];
		else
			oldCustomPollerId = string.Empty;


		//copy row properties
		foreach (DictionaryEntry property in Resource.Properties)
			if (property.Key.ToString().EndsWith(":rows"))
				properties.Add(property.Key.ToString(), property.Value.ToString());

		if (!String.IsNullOrEmpty(CustomPollerList.CustomPollerID))
			properties.Add("CustomPollerID", CustomPollerList.CustomPollerID);
		else
			properties.Add("CustomPollerID", oldCustomPollerId);

		properties.Add(NetObjectID + ":Rows", CustomPollerList.CustomPollerRows);
		properties.Add("Period", Period.PeriodName);
		properties.Add("SampleSize", SampleSize.SampleSizeValue);
		properties.Add("AutoHide", AutoHide.AutoHideValue);

		if (ResourceSubTitle2.Text != null)
			properties.Add("SubTitle2", ResourceSubTitle2.Text);

		properties.Add("SubsetColor", BarPlotColor.Color);
		properties.Add("RYSubsetColor", LinePlotColor.Color);

		// save custom property that enables or disables showing of trend
		properties.Add("ShowTrend", showTrend.Checked.ToString());
		return properties;
	}

	public override Dictionary<string, object> Properties
	{
		get { return GetProperties(); }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			string trendEnabled = Resource.Properties["ShowTrend"];

			if (String.IsNullOrEmpty(trendEnabled))
				showTrend.Checked = true;
			else
				showTrend.Checked = trendEnabled != bool.FalseString;
		}

		using (INpmCustomPollerAdapter adapter = NpmAdapterFactory.CreateCustomPollerAdapter())
		{
			if (!string.IsNullOrEmpty(CustomPollerList.CustomPollerID))
			{
				Guid guid = new Guid(CustomPollerList.CustomPollerID);
				CustomPoller poller = adapter.GetCustomPoller(guid);
				if (poller.SNMPGetType == "GetSubTree")
				{
					BarPlotColor.Visible = false;
					LinePlotColor.Visible = false;
				}
				else
				{
					BarPlotColor.Visible = true;
					LinePlotColor.Visible = true;
				}
			}
			else
			{
				BarPlotColor.Visible = true;
				LinePlotColor.Visible = true;
			}
		}
	}

	public bool IsError
	{
		get { return _isError; }
		set { _isError = value; }
	}
}
