<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectCustomInterfaceProperty.ascx.cs" Inherits="Orion_NPM_Controls_EditResourceControls_SelectCustomInterfaceProperty" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<a href="<%= HelpHelper.GetHelpUrl("OrionAGCustomProperties") %>" style="color: #0000FF" target="_blank">
		<%=Resources.NPMWebContent.NPMWEBDATA_VB0_220%>
	</a>
    
    <p>
        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_221%>
        <br />
        <asp:ListBox runat="server" ID="lbxCustomProps" Rows="1" SelectionMode="single" />
    </p>