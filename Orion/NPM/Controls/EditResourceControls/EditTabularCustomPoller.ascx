<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTabularCustomPoller.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditTabularCustomPoller" %>

<%@ Register TagPrefix="orion" TagName="AutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>

<asp:PlaceHolder runat="server" ID="selectPollersError" Visible="false">
    <div style="color: Red; font-size: small;">
        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_236%></div>
</asp:PlaceHolder>

<table class="text" border="0" cellpadding="0" cellspacing="0" >
    <tr>
        <td><b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_237%></b></td>
    </tr>
    <tr>
        <td align="justify" style="text-align: left; ">
            <asp:CheckBoxList ID="tabularPollers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PollersChanged">
            </asp:CheckBoxList>
        </td>
    </tr>
</table>
<br />

<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="pollerRowsUpdatePanel">
        <ProgressTemplate><img src="/Orion/images/AJAX-Loader.gif"/></ProgressTemplate>
    </asp:UpdateProgress>
<asp:UpdatePanel ID="pollerRowsUpdatePanel" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="tabularPollers" />
        <asp:AsyncPostBackTrigger ControlID="pollersLabels" />
        <asp:AsyncPostBackTrigger ControlID="selectAllRows" />
    </Triggers>
    <ContentTemplate>
        <asp:Panel runat="server" ID="pollerRows">
            <asp:PlaceHolder runat="server" ID="selectRowsError" Visible="false">
                <div style="color: Red; font-size: small;">
                    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_238%></div>
            </asp:PlaceHolder>
            
            <table class="text" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr><td><b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_239%></b></td></tr>
                <tr>
                    <td align="justify" style="text-align: left;">
                        <asp:CheckBox ID="selectAllRows" runat="server" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_240%>" Font-Bold="true" AutoPostBack="true"
                            OnCheckedChanged="AllRowsSelected" />
                        <br />
                        <div>
                            <asp:CheckBoxList ID="tabularPollerRows" runat="server" OnSelectedIndexChanged="Check_Clicked"
                                AutoPostBack="true" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<br />

<table class="text" border="0" cellpadding="0" cellspacing="0" >
    <tr><td><b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_241%></b></td></tr>
    <tr>
        <td align="justify" style="text-align: left;">
            <br />
            <asp:DropDownList ID="pollersLabels" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PollersChanged">
            </asp:DropDownList>
        </td>
    </tr>
</table>
<br />

<table class="text" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <orion:AutoHide runat="server" ID="autoHide" />
        </td>
    </tr>
</table>
