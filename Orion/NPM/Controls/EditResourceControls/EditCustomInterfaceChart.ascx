﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomInterfaceChart.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditCustomInterfaceChart" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ShowTrend" Src="~/Orion/Controls/ShowTrendControl.ascx" %>
<script type="text/javascript" language="javascript">
    $(function () {
        if (typeof (TitleTextBoxID) != 'undefined') {
            $('#<%=ListOfCharts.ClientID %>').change(function () {
                var displayText = $('#<%=ListOfCharts.ClientID %> option:selected').text();
                $('#' + TitleTextBoxID).val(displayText);
            });
        }
    });  
</script>
<table width="650">
    <tr>
        <td class="formRightInput" style="font-weight: bold;">
            <%=Resources.NPMWebContent.NPMWEBDATA_TM0_19%>
        </td>
        <td class="formRightInput">
            <asp:DropDownList runat="server" ID="ListOfCharts" DataValueField="Key" DataTextField="Value">
            </asp:DropDownList>
        </td>
        <td class="formHelpfulText">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <orion:EditPeriod runat="server" ID="Period" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <orion:EditSampleSize runat="server" ID="SampleSize" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <orion:ShowTrend ID="showTrend" Checked="true" runat="server" />
        </td>
    </tr>
</table>