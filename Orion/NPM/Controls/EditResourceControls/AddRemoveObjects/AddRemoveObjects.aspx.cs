﻿using System;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using ContainerModel = SolarWinds.Orion.Core.Common.Models.Container;
using System.Collections.Generic;
using System.Collections.Specialized;

public partial class Orion_NPM_Controls_AddRemoveObjects : System.Web.UI.Page
{
    private const string SessionGuid = "BA0F0007-0438-47F0-89A8-64BA3E4E3332";
    private const string SessionID = "SessionID";
    private StringDictionary parameters = null;

    private bool LoadSessionData()
    {
        try
        {
            if (Session[SessionGuid] != null)
            {
                parameters = (StringDictionary)Session[SessionGuid];
                return true;
            }
        }
        catch { }
        return false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!LoadSessionData() || parameters == null)
            {
                throw new Exception("There are missing parameters in session.");
            }

            string entities = parameters.ContainsKey("Entities") ? parameters["Entities"] : "Nodes";
            string entityName = parameters.ContainsKey("EntityName") ? parameters["EntityName"] : "Orion.Nodes";
            string filter = parameters.ContainsKey("Filter") ? parameters["Filter"] : String.Empty;
            string selectedEntities = parameters.ContainsKey("SelectedEntities") ? parameters["SelectedEntities"] : String.Empty;

            this.Title = String.Format(Resources.NPMWebContent.NPMWEBCODE_VB1_3, entities);
            this.addRemoveObjectsControl.LeftPanelTitle = String.Format(Resources.NPMWebContent.NPMWEBCODE_VB1_1, entities);
            this.addRemoveObjectsControl.RightPanelTitle = String.Format(Resources.NPMWebContent.NPMWEBCODE_VB1_2, entities);
            this.addRemoveObjectsControl.EntityName = entityName;
            this.addRemoveObjectsControl.Filter = filter;
            this.addRemoveObjectsControl.SelectedItems = selectedEntities;
        }
    }

    private string CopySession()
    {
        if (!LoadSessionData() || parameters == null)
        {
            throw new Exception("There are missing parameters in session.");
        }

        Session[SessionGuid] = parameters;

        string guid = Guid.NewGuid().ToString();

        parameters[SessionID] = guid;

        return guid;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        string guid = CopySession();

        parameters["SelectedEntities"] = this.addRemoveObjectsControl.SelectedItems;

        this.Response.Redirect(String.Format("/Orion/NetPerfMon/Resources/EditResource.aspx{0}", GetUrlParameters(SessionID, guid)));
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        string guid = CopySession();

        this.Response.Redirect(String.Format("/Orion/NetPerfMon/Resources/EditResource.aspx{0}", GetUrlParameters(SessionID, guid)));
    }

    private string GetUrlParameters(string name, string value)
    {
        var list = new List<string>();

        AddUrlParameter(list, "ResourceID");
        AddUrlParameter(list, "NetObject");
        AddUrlParameter(list, "ViewID");

        if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(value))
        {
            list.Add(String.Format("{0}={1}", name, value));
        }

        return (list.Count > 0) ? "?" + String.Join("&", list.ToArray()) : String.Empty;
    }

    private void AddUrlParameter(List<string> list, string name)
    {
        if (!String.IsNullOrEmpty(Request.QueryString[name]))
            list.Add(String.Format("{0}={1}", name, Request[name]));
    }

}
