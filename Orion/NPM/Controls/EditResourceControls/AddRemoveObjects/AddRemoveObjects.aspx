﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionResourceEdit.master"
 CodeFile="AddRemoveObjects.aspx.cs" Inherits="Orion_NPM_Controls_AddRemoveObjects" %>
 
<%@ Register Src="~/Orion/NPM/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsControl.ascx" TagName="AddRemoveObjectsControl" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include2" runat="server" File="Admin/Containers/Containers.css" />
    <orion:Include ID="Include3" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
     <h1 style="font-size:large!important;"><%= Page.Title %></h1>
    
    <br />
    <p>
	    <%=Resources.NPMWebContent.NPMWEBDATA_VB1_1%>
    </p>
    <br />

	<orion:AddRemoveObjectsControl ID="addRemoveObjectsControl" runat="server" />
	
    <div class="sw-btn-bar">
        <orion:LocalizableButton ID="submitButton" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
        <orion:LocalizableButton ID="cancelButton" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelClick"/>
	</div>
</asp:Content>
