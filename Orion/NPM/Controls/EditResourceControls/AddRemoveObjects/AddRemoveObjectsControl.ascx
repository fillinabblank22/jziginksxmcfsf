﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddRemoveObjectsControl.ascx.cs" 
Inherits="Orion_NPM_Controls_AddRemoveObjectsControl" %>

<orion:Include ID="Include1" runat="server" Module="NPM" File="Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjects.js"/>

<div id="GroupItemsSelector">
	<div class="GroupSection">
		<label for="groupBySelect"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_83%></label>
		<select id="groupBySelect">
  		</select>
	</div>
    <div class="GroupSection">
		<label for="searchBox"><%=Resources.NPMWebContent.NPMWEBDATA_VB1_2%></label>
        <table>
            <tr>
            <td><input type="text" class="searchBox" id="searchBox" name="searchBox" /></td>
            <td><a href="javascript:void(0);" id="searchButton"><img src="/Orion/images/Button.SearchIcon.gif" /></a></td>
            </tr>
        </table>
	</div>
</div>

<div id="ContainerMembersTable" style="width:99%;">
</div>

<div id="AddButtonPanel" style="padding:10px;">
    <a id="AddToGroupButton" href="javascript:void(0);" style="top: 44%; position: relative;">
        <img src="/Orion/NPM/Images/AddRemoveObjects/arrows_add_32x32.gif" />
    </a>
    <a id="RemoveFromGroupButton" href="javascript:void(0);" style="top: 46%; position: relative;">
        <img src="/Orion/NPM/Images/AddRemoveObjects/arrows_remove_32x32.gif" />
    </a>
</div>

<input type="hidden" id="groupByType" />
<asp:HiddenField ID="gridItems" runat="server" />

<script type="text/javascript">
    // <![CDATA[
    SW.NPM.SelectObjects.SetTitles('<%= LeftPanelTitle %>', '<%= RightPanelTitle %>');
    SW.NPM.SelectObjects.SetEntity('<%= EntityName %>');
    SW.NPM.SelectObjects.SetFilter('<%= Filter %>');
    SW.NPM.SelectObjects.SetGridItemsFieldClientID('<%= gridItems.ClientID %>');
    // ]]>
</script>