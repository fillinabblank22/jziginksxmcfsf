﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Controls_EditResourceControls_EditRoutingControl : BaseResourceEditControl
{
	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		if (!IsPostBack)
		{
			LoadProperties(Resource.Properties);
		}
	}

	private void LoadProperties(ResourcePropertyCollection properties)
	{
		ipVersion.SelectedValue = properties["IpVersion"] ?? "4";
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			var rv = new Dictionary<string, object>();
			rv["IpVersion"] = ipVersion.SelectedValue;
			return rv;
		}
	}
}