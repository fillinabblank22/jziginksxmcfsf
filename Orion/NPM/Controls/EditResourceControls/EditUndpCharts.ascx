﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUndpCharts.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditUndpCharts" EnableViewState="true" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditCustomPoller" Src="~/Orion/NPM/Controls/CustomPollerControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPlotColor" Src="~/Orion/Controls/PlotColorControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditChartFormat" Src="~/Orion/Controls/ChartFormatControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditAutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ShowTrend" Src="~/Orion/Controls/ShowTrendControl.ascx" %>
<%@ Register TagPrefix="npm" TagName="ListCharts" Src="~/Orion/NPM/Controls/ListCharts.ascx" %>
<table border="0" width="100%">
    <tr>
        <td>
            <b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_23%></b>
        </td>
        <td>
            <asp:TextBox runat="server" Width="150px" ID="ResourceSubTitle2" MaxLength="100"></asp:TextBox>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <orion:EditCustomPoller runat="server" ID="CustomPollerList" EnableViewState="true" />

    <tr id="listChartsContainer" runat="server">
    <td><b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_20%></b>
    </td>
    <td>
    <npm:ListCharts runat="server" ID="ChartList" ParentChartInfoName="SolarWinds.Orion.Web.Charting.NPMBarChartInfo, OrionWeb" />
    </td>
     <td class="formHelpfulTxt">
            <%=Resources.NPMWebContent.NPMWEBDATA_TM0_21%>
        </td>
    </tr>
    <orion:EditChartFormat runat="server" ID="ChartFormat" />
    <orion:EditPeriod runat="server" ID="Period" />
    <orion:EditSampleSize runat="server" ID="SampleSize" />
    <orion:EditPlotColor runat="server" ID="BarPlotColor" />
    <orion:ShowTrend ID="showTrend" Checked="true" runat="server" />
    <orion:EditAutoHide runat="server" ID="AutoHide" />
</table>
<%if (IsError)
  {%>
<div style="color: Red; font-size: medium;">
    <b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_22%></b></div>
<br />
<%} %>
