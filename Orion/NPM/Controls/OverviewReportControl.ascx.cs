using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.Security.AntiXss;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using SolarWinds.NPM.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.UI.StatusIcons;

public partial class Orion_Controls_OverviewReportControl : System.Web.UI.UserControl
{
    private const string NodeUrl = "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}";
    private const string InterfaceUrl = "/Orion/NPM/InterfaceDetails.aspx?NetObject=I:{0}";

	private const string IconBasePath = "/Orion/images/StatusIcons/";
	private const string VendorIconBasePath = "/NetPerfMon/images/Vendors/";
	private const string IfTypeIconBasePath = "/NetPerfMon/images/Interfaces/";

	private const string RelativeIcon = "Small-Relative-{0}.gif";

    private const string ExternalIcon = "Small-External.gif";
    private const string UnknownIcon = "Small-Unknown.gif";
    private const string UnmanagedIcon = "Small-Unmanaged.gif";
    private const string UnreachableIcon = "Small-Unreachable.gif";
    private const string UnpluggedIcon = "Small-Unplugged.gif";
    private const string UpIcon = "Small-Up.gif";
    private const string WarningIcon = "Small-Warning.gif";
    private const string CriticalIcon = "Small-Critical.gif";
    private const string DownIcon = "Small-Down.gif";
	private const string ShutdownIcon = "Small-Shutdown.gif";

    private const int TrafficScaleVisibleSize = 5;
    private static readonly long[] TrafficScale = {
            10000, 32000, 56000, 128000, 256000, 512000, 1500000, 10000000, 45000000,
            100000000, 500000000, 1000000000, 10000000000, 100000000000, 1000000000000,
            10000000000000, 100000000000000 };
    private int trafficScaleIndex;
    private Dictionary<string, bool> vendorCache;
    private WebDAL proxy;
	private NodeOverviewStyle nodeStyle;
	private InterfaceOverviewStyle interfaceStyle;
	private bool printable;

    protected WebDAL Proxy
    {
        get
        {
            if (this.proxy == null)
            {
                this.proxy = new WebDAL();
            }
            return this.proxy;
        }
    }

    public NodeOverviewStyle NodeStyle
    {
        get { return nodeStyle; }
        set { nodeStyle = value; }
    }

    public InterfaceOverviewStyle InterfaceStyle
    {
        get { return interfaceStyle; }
        set { interfaceStyle = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Printable
    {
        set { this.printable = value; }
        get { return this.printable; }
    }

    protected override void OnUnload(EventArgs e)
    {
        if (this.proxy != null)
        {
            this.proxy.Dispose();
        }
        base.OnUnload(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (this.InterfaceStyle == InterfaceOverviewStyle.InterfaceTraffic)
        {
            this.SetInterfaceTrafficScale();
        }
		this.GenerateTable(OverviewDAL.GetOverviewData(this.NodeStyle, this.InterfaceStyle));
        this.GenerateLegend();
        base.OnPreRender(e);
    }

    private void GenerateTable(DataTable data)
    {
        int currentNodeId = 0;
        HtmlTableRow currentRow = null;
        HtmlTableCell currentInterfaceCell = null;
        int numInterfaces = 0;

        bool bgFlag = true; // flag to switch between zebra background is on and off

        if (!NodeChildStatusDAL.IsEnhancedNodeStatusCalculation())
        {
            var nodeIds = data.Rows
                .Cast<DataRow>()
                .Select(r => Convert.ToInt32(r["NodeId"]));

            NodeIconFactory.PreloadChildStatusCache(nodeIds);
        }

        foreach (DataRow row in data.Rows)
        {
            int nodeId = Convert.ToInt32(row["NodeId"]);
            if (nodeId != currentNodeId) // found a node
            {
                numInterfaces = 0;
                currentNodeId = nodeId;
                if (currentRow != null) // already have a row
                {
                    // add composed node into a table
                    if (currentRow != null)
                    {
                        if (bgFlag)
                        {
                            currentRow.Attributes.Add("class", "ZebraStripe"); // zebra is on
                        }
                        bgFlag = !bgFlag; // switch zebra
                    }
                    this.overviewTable.Rows.Add(currentRow);
                }
                currentRow = new HtmlTableRow();
                HtmlTableCell nodeCell = new HtmlTableCell();
                nodeCell.Attributes["class"] = "OverviewItem";
                nodeCell.Controls.Add(this.GetNodeLink(row));
                currentRow.Cells.Add(nodeCell);

                currentInterfaceCell = new HtmlTableCell();
                currentInterfaceCell.Attributes["class"] = "InterfaceOverviewItem";
                currentRow.Cells.Add(currentInterfaceCell);
            }
            if(row["InterfaceID"] != DBNull.Value)  // if row contains interface data
            {
                currentInterfaceCell.Controls.Add(this.GetInterfaceLink(row));
                numInterfaces++;
                if (numInterfaces % 24 == 0)
                {
                    currentInterfaceCell.Controls.Add(new HtmlGenericControl("br"));
                    numInterfaces = 0;
                }
            }
        }
        if (currentRow != null) // add last row (if exists)
        {
            if (currentRow != null)
            {
                if (bgFlag)
                {
                    currentRow.Attributes.Add("class", "ZebraStripe"); // zebra
                }
                bgFlag = !bgFlag;
            }
            this.overviewTable.Rows.Add(currentRow);
        }
        else // hide the table when there are no rows
        {
            this.overviewTable.Visible = false;
        }
    }

    #region Legend Functions

    private void GenerateLegend()
    {
        // create separator row
        HtmlTableRow row = new HtmlTableRow();
        HtmlTableCell cell = new HtmlTableCell();
        cell.ColSpan = 2;
        cell.Attributes["class"] = "Property";
        cell.InnerHtml = "&nbsp;";
        row.Cells.Add(cell);
        this.overviewTable.Rows.Add(row);

        // get nodes and interfaces legend columns
        List<HtmlTableCell> nodeLegend = this.GetNodeLegend();
        List<HtmlTableCell> interfaceLegend = this.GetInterfaceLegend();
        int maxRows = Math.Max(nodeLegend.Count, interfaceLegend.Count);
        for (int i = 0; i < maxRows; i++)
        {
            row = new HtmlTableRow();
            // if there are fewer rows in one column, put empty table cell
            row.Cells.Add((i < nodeLegend.Count) ? nodeLegend[i] : new HtmlTableCell());
            row.Cells.Add((i < interfaceLegend.Count) ? interfaceLegend[i] : new HtmlTableCell());
            this.overviewTable.Rows.Add(row);
        }
    }

    private List<HtmlTableCell> GetNodeLegend()
    {
        List<string> scaleText = new List<string>();
        List<string> scaleIcons = new List<string>();

        switch (this.NodeStyle)
        {
            case NodeOverviewStyle.ResponseTime:
            case NodeOverviewStyle.AvgResponseTime:
            case NodeOverviewStyle.MaxResponseTime:
                double max = this.Proxy.LookupSetting("NetPerfMon-ResponseTime-Warning", 350);
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_5, max * 0.25));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_6, max * 0.25 + 1, max * 0.5));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_6, max * 0.5 + 1, max * 0.75));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_6, max * 0.75 + 1, max));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_7, max));
                break;
            case NodeOverviewStyle.CPULoad:
            case NodeOverviewStyle.PercentMemoryUsed:
            case NodeOverviewStyle.PercentLoss:
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_8);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_12);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_9);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_10);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_11);
                break;
            case NodeOverviewStyle.NodeStatus:
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_13);
				scaleIcons.Add(UpIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
			    scaleIcons.Add(UnmanagedIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_2);
				scaleIcons.Add(UnknownIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_1);
				scaleIcons.Add(ExternalIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_14);
                scaleIcons.Add(WarningIcon);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_MY0_1);
                scaleIcons.Add(CriticalIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_4);
				scaleIcons.Add(UnreachableIcon);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_15);
                scaleIcons.Add(DownIcon);

                break;
            case NodeOverviewStyle.MachineType:
                DataTable results = null;
                scaleText = new List<string>();
                scaleIcons = new List<string>();

                results = this.Proxy.GetNodeVendors();

                if (results != null && results.Rows.Count > 0)
                {
                    foreach (DataRow row in results.Rows)
                    {
						string vendor = Convert.ToString(row["Vendor"]).Trim();
						if (string.IsNullOrEmpty(vendor))
							vendor = Resources.NPMWebContent.NPMWEBCODE_AK0_2;
						string vendorIcon = Convert.ToString(row["VendorIcon"]).Trim();

						if (!String.IsNullOrEmpty(vendor) && !String.IsNullOrEmpty(vendorIcon) && !scaleText.Contains(vendor))
                        {
                            scaleIcons.Add(vendorIcon);
                            scaleText.Add(vendor);
                        }
                    }
                }
                break;

            default:
                break;
        }

        string headerText = String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_16,
			OverviewDAL.GetStyleDescription<NodeOverviewStyle>(this.NodeStyle));

		return GenerateLegendColumn(headerText, scaleText, scaleIcons,
			(this.NodeStyle == NodeOverviewStyle.MachineType ? VendorIconBasePath : IconBasePath));
    }

    private List<HtmlTableCell> GetInterfaceLegend()
    {
        List<string> scaleText = new List<string>();
        List<string> scaleIcons = new List<string>();

    	switch (this.InterfaceStyle)
        {
            case InterfaceOverviewStyle.PercentUtilization:
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_8);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_12);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_9);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_10);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_11);
                break;
            case InterfaceOverviewStyle.ErrorsAndDiscardsToday:
            case InterfaceOverviewStyle.ErrorsAndDiscardsThisHour:
                double error = this.Proxy.LookupSetting("NetPerfMon-ErrorsDiscards-Error", 200);
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_18, error * 0.25));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_19, error * 0.25 + 1, error * 0.5));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_19, error * 0.5 + 1, error * 0.75));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_19, error * 0.75 + 1, error));
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_20, error));
                break;
            case InterfaceOverviewStyle.InterfaceTraffic:
                int i = this.trafficScaleIndex;
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_22, new BitsPerSecond(TrafficScale[i])));
                for (int loop = 0; loop < 4; loop++)
                {
                    scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_21,
                        new BitsPerSecond(TrafficScale[i]),
                        new BitsPerSecond(TrafficScale[++i])));
                }
                scaleText.Add(String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_23, new BitsPerSecond(TrafficScale[i])));
                break;
            case InterfaceOverviewStyle.InterfaceStatus:
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_13);
				scaleIcons.Add(UpIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
			    scaleIcons.Add(UnmanagedIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_17);
				scaleIcons.Add(UnpluggedIcon);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_24);
                scaleIcons.Add(ShutdownIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_2);
				scaleIcons.Add(UnknownIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBDATA_MP0_1);
                scaleIcons.Add(WarningIcon);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_MY0_1);
                scaleIcons.Add(CriticalIcon);
				scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_4);
				scaleIcons.Add(UnreachableIcon);
                scaleText.Add(Resources.NPMWebContent.NPMWEBCODE_AK0_15);
                scaleIcons.Add(DownIcon);
				break;
            case InterfaceOverviewStyle.InterfaceType:

                DataTable results = null;
				scaleText = new List<string>();
				scaleIcons = new List<string>();

                results = this.Proxy.GetInterfaceType();

                if (results != null && results.Rows.Count > 0)
                {
                    foreach (DataRow row in results.Rows)
                    {
                        string typeDesc = Convert.ToString(row["TypeDescription"]).Trim();
                        string typeIcon = ImageHelper.GetInterfaceTypeIcon(string.Format("{0}.gif",row["InterfaceType"]));

                        if (!String.IsNullOrEmpty(typeDesc) && !String.IsNullOrEmpty(typeIcon) && !scaleText.Contains(typeDesc))
                        {
                            scaleIcons.Add(typeIcon);
                            scaleText.Add(typeDesc);
                        }
                    }
                }
                break;

            default:
                break;
        }

        string headerText = String.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_16,
			OverviewDAL.GetStyleDescription<InterfaceOverviewStyle>(this.InterfaceStyle));

        return GenerateLegendColumn(headerText, scaleText, scaleIcons,
			(this.InterfaceStyle == InterfaceOverviewStyle.InterfaceType ? IfTypeIconBasePath : IconBasePath));
    }

    private static List<HtmlTableCell> GenerateLegendColumn(string headerText, IList<string> scaleText, IList<string> scaleIcons, string iconBasePath)
    {
        List<HtmlTableCell> cells = new List<HtmlTableCell>();

        HtmlTableCell cell = new HtmlTableCell("th");
        cell.Align = "left";
        cell.Controls.Add(new LiteralControl(headerText));
        cell.Attributes["class"] = "OverviewItem";
        cells.Add(cell);

        int iconCount = 0;

        if (scaleIcons != null)
        {
            iconCount = scaleIcons.Count;
        }

        for (int i = 0; i < scaleText.Count; i++)
        {
            cell = new HtmlTableCell();
            HtmlImage icon = new HtmlImage();

            if (scaleIcons == null || scaleIcons.Count == 0)
            {
                icon.Src = iconBasePath + String.Format(RelativeIcon, i);
            }
            else if (i < iconCount)
            {
                icon.Src = iconBasePath + scaleIcons[i];
            }
            else
            {
                icon.Src = iconBasePath + String.Format(RelativeIcon, i - iconCount);
            }

            cell.Controls.Add(icon);
            cell.Controls.Add(new LiteralControl(scaleText[i]));
            cell.Attributes["class"] = "OverviewItem";
            cells.Add(cell);
        }
        return cells;
    }

    #endregion

    private HtmlControl GetNodeLink(DataRow row)
    {
        int nodeId = Convert.ToInt32(row["NodeId"]);

        HtmlControl nodeLink;

        if (this.Printable)
        {
            nodeLink = new HtmlGenericControl();
        }
        else
        {
            HtmlAnchor nodeAnchor = new HtmlAnchor();
            nodeAnchor.HRef = String.Format(NodeUrl, nodeId);
            nodeAnchor.Attributes["class"] = "NetObject";
            if (this.Profile.ToolsetIntegration)
            {
                nodeAnchor.Attributes["IP"] = Convert.ToString(row["IPAddress"]);
                string host = Convert.ToString(row["DNS"]).Trim();
                if (host == String.Empty)
                {
                    host = Convert.ToString(row["SysName"]).Trim();
                }
                if (host == String.Empty)
                {
                    nodeAnchor.Attributes["NodeHostname"] = host;
                }
                nodeAnchor.Attributes["Community"] = FormatHelper.GetGuidParamString(Convert.ToString(row["GUID"]));
            }
            nodeLink = nodeAnchor;
        }

        string iconName, nodeDetails;
        this.GetNodeIconAndDetails(row, out iconName, out nodeDetails);

        HtmlImage icon = new HtmlImage();
		icon.Src = iconName.StartsWith("/" ) ?
            iconName :
            (this.NodeStyle == NodeOverviewStyle.MachineType ? VendorIconBasePath : IconBasePath) + iconName;
        nodeLink.Controls.Add(icon);

        nodeLink.Controls.Add(new LiteralControl(AntiXssEncoder.HtmlEncode(Convert.ToString(row["NodeName"]).Trim(), false)));

        return nodeLink;
    }

    private void GetNodeIconAndDetails(DataRow row, out string iconUrl, out string details)
    {
        StringBuilder info = new StringBuilder();
        info.Append(row["NodeName"]).AppendLine();
        double value, threshold;

		if (this.NodeStyle != NodeOverviewStyle.MachineType &&
			row.Table.Columns.Contains("NodeStatus") &&
            row.IsNull("NodeStatus") == false &&
            Convert.ToInt32(row["NodeStatus"]) == 11 ) // External = 11.
		{
			info.Append(row["StatusDescription"]).AppendLine();
            iconUrl = ExternalIcon;
		}
		else
			switch (this.NodeStyle)
			{
				case NodeOverviewStyle.ResponseTime:
				case NodeOverviewStyle.AvgResponseTime:
				case NodeOverviewStyle.MaxResponseTime:
					info.Append(row["StatusDescription"]).AppendLine();
					value = GetValue(row[this.NodeStyle.ToString()]);

                    if (FormatAsBool(row["UnManaged"]))
                    {
                        info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                        iconUrl = UnmanagedIcon;
                    }
                    else if (FormatAsBool(row["External"]))
                    {
                        info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_1);
                        iconUrl = ExternalIcon;
                    }
                    else if (value < 0)
					{
						info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_25);
						iconUrl = String.Format(RelativeIcon, 5);
					}
					else
					{
						info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_26,
							GetString(row["ResponseTime"], "ms"), GetString(row["AvgResponseTime"], "ms")).AppendLine();
						info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_27,
							GetString(row["MinResponseTime"], "ms"), GetString(row["MaxResponseTime"], "ms"));
						threshold = this.Proxy.LookupSetting("NetPerfMon-ResponseTime-Warning", 350);
						iconUrl = GetRelativeIcon(value / threshold * 100);
					}
					break;
				case NodeOverviewStyle.CPULoad:
				case NodeOverviewStyle.PercentMemoryUsed:
				case NodeOverviewStyle.PercentLoss:
                    if (FormatAsBool(row["UnManaged"]))
                    {
                        info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                        iconUrl = UnmanagedIcon;
                    }
                    else if (FormatAsBool(row["External"]))
                    {
                        info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_1);
                        iconUrl = ExternalIcon;
                    }
                    else
                    {
                        info.Append(row["StatusDescription"]).AppendLine();
                        object dbValue = row[this.NodeStyle.ToString()];
                        info.AppendFormat("{0}: {1}",
							OverviewDAL.GetStyleDescription<NodeOverviewStyle>(this.NodeStyle), GetString(dbValue, "%"));
                        iconUrl = GetRelativeIcon(GetValue(dbValue));
                    }
					break;
				case NodeOverviewStyle.MachineType:
					info.Append(row["MachineType"]);
					iconUrl = Convert.ToString(row["VendorIcon"]).Trim();
					if (!this.IconExists(VendorIconBasePath + iconUrl))
					{
						iconUrl = "Unknown.gif";
					}
					break;
				default:
                    if (FormatAsBool(row["UnManaged"]))
                    {
                        info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                        iconUrl = UnmanagedIcon;
                    }
                    else if (FormatAsBool(row["External"]))
                    {
                        info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_1);
                        iconUrl = ExternalIcon;
                    }
                    else
                    {
                        info.Append(row["MachineType"]).AppendLine();
                        info.Append(row["StatusDescription"]).AppendLine();
                        info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_28, GetString(row["ResponseTime"], "ms"));
                        iconUrl = SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL(row["NodeID"], row["NodeStatus"]);
                    }
					break;
			}
        details = info.ToString();
    }

    private HtmlControl GetInterfaceLink(DataRow row)
    {
        HtmlControl interfaceLink;
        if (this.Printable)
        {
            interfaceLink = new HtmlGenericControl();
        }
        else
        {
            interfaceLink = new HtmlAnchor();
            ((HtmlAnchor)interfaceLink).HRef = String.Format(InterfaceUrl, Convert.ToInt32(row["InterfaceId"]));
            ((HtmlAnchor)interfaceLink).Attributes["class"] = "NetObject";
        }

        string iconName, interfaceDetails;
        GetInterfaceIconAndDetails(row, out iconName, out interfaceDetails, this.InterfaceStyle);

        HtmlImage icon = new HtmlImage();
		icon.Src = trimSpaces((this.InterfaceStyle == InterfaceOverviewStyle.InterfaceType ? IfTypeIconBasePath : IconBasePath) + iconName);
        interfaceLink.Controls.Add(icon);

        return interfaceLink;
    }

    /// <summary>
    /// some strings are saved as char[n] in the database and then we need trim spaces
    /// </summary>
    /// <param name="text">text with trailing spaces</param>
    /// <returns>text without trailing spaces</returns>
    private string trimSpaces(object text)
    {
        if (text == null)
            return string.Empty;

        string str = text.ToString();

        if (string.IsNullOrEmpty(str))
            return string.Empty;
        else
            return str.Trim();
    }

    private static bool IsInterfaceUnplugged(object obj)
    {
        if (obj is DBNull || obj == null)
        {
            return false;
        }

        if (String.IsNullOrEmpty((string)obj))
        {
            return false;
        }

        if (((string)obj).Trim() == "10")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void GetInterfaceIconAndDetails(DataRow row, out string iconUrl, out string details, InterfaceOverviewStyle style)
    {
        StringBuilder info = new StringBuilder();
        info.AppendFormat("{0} - {1}", row["NodeName"], row["InterfaceName"]).AppendLine();
        info.Append(row["TypeDescription"]).AppendLine();
        double value, threshold;
		switch (style)
        {
            case InterfaceOverviewStyle.InterfaceType:
				iconUrl = ImageHelper.GetInterfaceTypeIcon(string.Format("{0}.gif", row["InterfaceType"]));
				break;
            case InterfaceOverviewStyle.PercentUtilization:
                if (FormatAsBool(row["UnManaged"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                    iconUrl = UnmanagedIcon;
                }
                else if (IsInterfaceUnplugged(row["Status"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_17);
                    iconUrl = UnpluggedIcon;
                }
                else
                {
                    value = GetValue(row["InPercentUtil"]) + GetValue(row["OutPercentUtil"]);
                    iconUrl = GetRelativeIcon(value);
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_29,
                        GetString(row["InPercentUtil"], "%"), GetString(row["InBps"], "bps"));
                    info.AppendLine();
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_30,
                        GetString(row["OutPercentUtil"], "%"), GetString(row["OutBps"], "bps"));
                }
                break;
            case InterfaceOverviewStyle.ErrorsAndDiscardsToday:
                if (FormatAsBool(row["UnManaged"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                    iconUrl = UnmanagedIcon;
                }
                else if (IsInterfaceUnplugged(row["Status"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_17);
                    iconUrl = UnpluggedIcon;
                }
                else
                {
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_31,
                        GetString(row["InErrorsToday"]), GetString(row["InDiscardsToday"]));
                    info.AppendLine();
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_32,
                        GetString(row["OutErrorsToday"]), GetString(row["OutDiscardsToday"]));
                    threshold = this.Proxy.LookupSetting("NetPerfMon-ErrorsDiscards-Error", 200);
                    value = GetValue(row["ErrorsDiscardsToday"]);
                    iconUrl = GetRelativeIcon(value / threshold * 100);
                }
                break;
            case InterfaceOverviewStyle.ErrorsAndDiscardsThisHour:
                if (FormatAsBool(row["UnManaged"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                    iconUrl = UnmanagedIcon;
                }
				else if (IsInterfaceUnplugged(row["Status"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_17);
                    iconUrl = UnpluggedIcon;
                }
                else
                {
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_33,
                        GetString(row["InErrorsThisHour"]), GetString(row["InDiscardsThisHour"]));
                    info.AppendLine();
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_34,
                        GetString(row["OutErrorsThisHour"]), GetString(row["OutDiscardsThisHour"]));
                    threshold = this.Proxy.LookupSetting("NetPerfMon-ErrorsDiscards-Error", 200);
                    value = GetValue(row["ErrorsDiscardsThisHour"]);
                    iconUrl = GetRelativeIcon(value / threshold * 100);
                }
                break;
            case InterfaceOverviewStyle.InterfaceTraffic:
                if (FormatAsBool(row["UnManaged"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                    iconUrl = UnmanagedIcon;
                }
				else if (IsInterfaceUnplugged(row["Status"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_17);
                    iconUrl = UnpluggedIcon;
                }
                else
                {
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_29,
                        GetString(row["InPercentUtil"], "%"), GetString(row["InBps"], "bps"));
                    info.AppendLine();
                    info.AppendFormat(Resources.NPMWebContent.NPMWEBCODE_AK0_30,
                        GetString(row["OutPercentUtil"], "%"), GetString(row["OutBps"], "bps"));
                    value = Math.Max(GetValue(row["InBps"]), GetValue(row["OutBps"]));
                    iconUrl = GetRelativeTrafficIcon(value, this.trafficScaleIndex);
                }
                break;
            default:
                if (FormatAsBool(row["UnManaged"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_3);
                    iconUrl = UnmanagedIcon;
                }
				else if (IsInterfaceUnplugged(row["Status"]))
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_17);
                    iconUrl = UnpluggedIcon;
                }
                else
                {
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_36);
                    info.AppendLine(FormatHelper.GetStatusText(Convert.ToInt32(row["AdminStatus"])));
                    info.Append(Resources.NPMWebContent.NPMWEBCODE_AK0_35);
                    info.AppendLine(FormatHelper.GetStatusText(Convert.ToInt32(row["OperStatus"])));
                    iconUrl = "small-" + Convert.ToString(row["StatusLED"]);
                }
                break;
        }
        details = info.ToString();
    }

    #region Data formatting
    private static double GetValue(object dbValue)
    {
        try
        {
            return Convert.ToDouble(dbValue);
        }
        catch
        {
            return 0;
        }
    }

	private static bool FormatAsBool(object value)
	{
		if (value == null || value is DBNull)
		{
			return false;
		}

		try
		{
			return Convert.ToBoolean(value);
		}
		catch
		{
			return false;
		}
	}

    private static string GetString(object dbValue)
    {
        return GetString(dbValue, String.Empty);
    }

    private static string GetString(object dbValue, string unit)
    {
        double value = GetValue(dbValue);
        if (value < 0)
        {
            return Resources.NPMWebContent.NPMWEBCODE_AK0_2;
        }
        if (unit == "bps")
        {
            return Macros.Convert_To_MB(value, unit, false);
        }
        return String.Format("{0:0} {1}", value, unit);
    }
    #endregion

    #region Icon formatting
    private static string GetRelativeIcon(double value)
    {
        int i = Convert.ToInt32(Math.Ceiling(value / 25));
        i = Math.Max(i, 0);
        i = Math.Min(i, 5);
        return String.Format(RelativeIcon, i);
    }

	private static string GetRelativeTrafficIcon(double value, int trafficScaleIndex)
    {
        for (int i = 0; i < 5; i++)
        {
            if (value < TrafficScale[i + trafficScaleIndex])
            {
                return String.Format(RelativeIcon, i);
            }
        }
        return String.Format(RelativeIcon, 5);
    }

    private bool IconExists(string iconUrl)
    {
        if (this.vendorCache == null)
        {
            this.vendorCache = new Dictionary<string, bool>();
        }
        if (!this.vendorCache.ContainsKey(iconUrl))
        {
            this.vendorCache[iconUrl] = File.Exists(this.Server.MapPath(iconUrl));
        }
        return this.vendorCache[iconUrl];
    }
    #endregion

    private void SetInterfaceTrafficScale()
    {
		double maxValue = OverviewDAL.GetMaxInterfaceTraffic();

        for (int i = TrafficScaleVisibleSize; i < TrafficScale.Length; i++)
        {
            // find lowest scale value that is higher than max traffic
            if (maxValue < TrafficScale[i])
            {
                // set starting index for scaling
                this.trafficScaleIndex = i - TrafficScaleVisibleSize;
                return;
            }
        }
        // else set highest starting index in scale array
        this.trafficScaleIndex = TrafficScale.Length - TrafficScaleVisibleSize;
    }
}
