<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollerControl.ascx.cs"
    Inherits="CustomPollersControl" %>

<!--table-->
<tr id="CustomPollerListArea" runat="server">
    <td style="height: 40px; width: 180px">
        <b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_25%></b>
    </td>
    <td>
        <asp:DropDownList ID="CustomPollerList" runat="server" Width="150px" AutoPostBack="true"
            OnSelectedIndexChanged="SelectCustomPoller">
        </asp:DropDownList>
        <input type="hidden" runat="server" id="hf_netObjectID" />
    </td>
    <td>
        &nbsp;
    </td>
</tr>
<!--/table-->
<tr>
    <asp:Panel runat="server" ID="TabularPollerDetails" Width="100%">
        <tr>
            <td style="height: 30px; width: 180px; vertical-align: top;">
                <b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_26%></b>
            </td>
            <td>
                <asp:CheckBox ID="SelectALL" runat="server" Text="All" Font-Bold="true" />
                <br />
                <div style="overflow: auto; height: 150px; width: 100%;">
                    <asp:CheckBoxList ID="RowIDList" runat="server" />
                </div>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </asp:Panel>
</tr>