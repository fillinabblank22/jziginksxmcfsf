using System;
using System.Collections.Generic;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using BLModels = SolarWinds.Orion.Core.Common.Models;

public partial class Orion_NPM_Controls_VMDetailsInterfaceList : System.Web.UI.UserControl, IPropertyProvider
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly Dictionary<string, object> _properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    public int NodeId
    {
        get
        {
            object nodeIdObject;
            if (_properties.TryGetValue("NodeId", out nodeIdObject))
            {
                int nodeId;
                if (Int32.TryParse(nodeIdObject.ToString(), out nodeId))
                    return nodeId;
            }

            return -1;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            Interfaces interfaces;
            try
            {
                interfaces = proxy.Api.GetNodeInterfaces(NodeId);
            }
            catch (Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            if (interfaces.Count > 0)
            {
                this.Visible = true;
                repVMDetails.DataSource = interfaces;
                repVMDetails.DataBind();
            }
            else
            {
                this.Visible = false;
            }
        }
    }

    public IDictionary<string, object> Properties
    {
        get { return _properties; }
    }
}
