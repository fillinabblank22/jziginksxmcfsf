<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollerChart.ascx.cs" Inherits="CustomPollerChart" %>

<%@ Register TagPrefix="orion" TagName="SampleSizeControl" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="TimePeriodControl" Src="~/Orion/Controls/TimePeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="orion" TagName="ShowTrend" Src="~/Orion/Controls/ShowTrendControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomPollerControl" Src="~/Orion/NPM/Controls/CustomPollerControl.ascx" %>

<%if (Request["Printable"] == null)
  { %>
<table cellpadding="8" border="0">
    <tr valign="top">
        <td nowrap class="PageHeader">
            <%=Page.Title %>
        </td>
    </tr>
</table>
<%} %>

<img src="<%= GetChartImageURL() %>" />

<% if (Request["Printable"] != null) {
		Response.End();
   }
%>
<div style="padding: 10px;">
<table width="100%" cellpadding="0" border="0">
<tr>
	<td>
		<table width="840" class="formTable" border="0" cellspacing="0" cellpadding="0">
		<input type="hidden" name="Name" value="<%= DisplayName %>">
		<input type="hidden" name="CustomPollerID" value="<%= _customPollerID %>">
		<%--input type="hidden" name="NetObject" id="NetObject" value="<--%= NetObjectID %-->"--%>

		<tr class="PageHeader">
			<td style="width: 30%;"><b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_54%></b></td>
			<td style="width: 40%;">
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="ImageButton1" OnClick="RefreshClick" LocalizedText="Refresh" DisplayType="Primary" />
                </div>
			</td>
			<td style="width: 30%;">
			</td>
		</tr>
		<tr>
			<td colspan="3"><hr class="formDivider"></td>
		</tr>
	    <tr>
		    <td colspan="3" class="formGroupHeader"><%= Resources.CoreWebContent.WEBDATA_TM0_71 %></td>
	    </tr>
	    <tr>
		    <td class="formLeftTitle"><%= Resources.CoreWebContent.WEBCODE_VB0_102 %></td>
		    <td class="formRightInput"><asp:TextBox runat="server" ID="chartTitle" Width="200"></asp:TextBox></td>
		    <td class="formHelpfulText">&nbsp;</td>
	    </tr>
	    <tr>
		    <td class="formLeftTitle"><%= Resources.CoreWebContent.WEBDATA_VB0_163 %></td>
		    <td class="formRightInput"><asp:TextBox runat="server" ID="chartSubTitle" Width="200"></asp:TextBox></td>
		    <td class="formHelpfulText">&nbsp;</td>
	    </tr>
	    <tr>
		    <td class="formLeftTitle"><%= Resources.CoreWebContent.WEBDATA_TM0_75 %></td>
		    <td class="formRightInput"><asp:TextBox runat="server" ID="chartSubTitle2" Width="200"></asp:TextBox></td>
		    <td class="formHelpfulText">&nbsp;</td>
	    </tr>
		<tr>
			<td colspan="3"><hr class="formDivider"></td>
		</tr>
        <tr>
		    <td colspan="3" class="formGroupHeader">
                <orion:CustomPollerControl id="customPollerControl" runat="server" />	
            </td>		
		</tr>
        <tr>
			<td colspan="3"><hr class="formDivider"></td>
		</tr>
		<tr>
		    <td colspan="3">
        		<orion:TimePeriodControl ID="timePeriodControl" runat="server" />
		    </td>
		</tr>
        <tr>
		    <td colspan="3"><hr class="formDivider"></td>
	    </tr>
	    <tr>
		    <td colspan="3" class="formGroupHeader"><orion:SampleSizeControl ID="sampleSizeControl" runat="server" /></td>
	    </tr>
		<tr>
			<td colspan="3"><hr class="formDivider"></td>
		</tr>

        <tr>
		    <td colspan="3" class="formGroupHeader"><%= Resources.NPMWebContent.NPMWEBDATA_TM0_56 %></td>
	    </tr>
        <tr>
		    <td class="formLeftTitle"><%= Resources.NPMWebContent.NPMWEBDATA_TM0_57 %></td>
		    <td class="formRightInput"><asp:TextBox runat="server" ID="chartWidth" Width="80"></asp:TextBox></td>
		    <td rowspan="2"><%= Resources.NPMWebContent.NPMWEBDATA_TM0_59%></td>
	    </tr>
	    <tr>
		    <td class="formLeftTitle"><%= Resources.NPMWebContent.NPMWEBDATA_TM0_58%></td>
		    <td class="formRightInput"><asp:TextBox runat="server" ID="chartHeight" Width="80"></asp:TextBox></td>
	    </tr>

<%--		<tr>
			<td colspan="3"><hr class="Property"></td>
		</tr>
		<tr>
			<td width="175"><b>Data Table Below Chart</b></td>
			<td width="120">
   			    <asp:DropDownList AutoPostBack="false" id="dataTableBelowChartDDL" runat="server" />			
			</td>
			<td>
				<%=Resources.NPMWebContent.NPMWEBDATA_TM0_65%>
			</td>
		</tr>--%>
		<tr>
			<td colspan="3"><hr class="formDivider"></td>
		</tr>
		<tr>
			<td width="175" class="formGroupHeader"><b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_60%></b></td>
			<td width="120">
			    <asp:DropDownList AutoPostBack="false" id="fontSizeDDL" runat="server">
			    	<asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_62%>" Value="2"></asp:ListItem>
			        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_63%>" Value="1"></asp:ListItem>
			        <asp:ListItem Text="<%$ Resources: NPMWebContent, NPMWEBDATA_TM0_64%>" Value="0"></asp:ListItem>
			    </asp:DropDownList>
			</td>
			<td width="10">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3"><hr class="formDivider"></td>
		</tr>
		<tr>
		    <td colspan="3"><orion:ShowTrend ID="showTrend" Checked="true" runat="server" /></td>
	    </tr>
		<!-- UnComment this section to enable Chart Styles
		<tr>
			<td width="175"><b>Chart Style</b></td>
			<td width="120">
				<+--%=Chart.GetStyleHTML%--+>
			</td>
			<td>
				Chart Styles are used to adjust the Fonts and Colors of a chart
			</td>
		</tr>
		<tr>
			<td colspan="3"><hr class="Property"></td>
		</tr>
		-->
	  <%if (IsError)
        {%>
            <tr>
                <td colspan="3">
                    <div style="color:Red; font-size:small; text-align:right;" ><b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_22%></b></div>
                </td>
            </tr>
       <%} %>
		<tr>
			<td></td>
			<td>
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="btnRefresh" OnClick="RefreshClick" LocalizedText="Refresh" DisplayType="Primary" />
                    <orion:LocalizableButton runat="server" ID="btnSave" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
                </div>
			</td>
			<td align="right">
			</td>
		</tr>

		<tr>
			<td colspan="3"><hr class="formDivider"></td>
		</tr>
		<tr>
			<td><b><%=Resources.NPMWebContent.NPMWEBDATA_TM0_61%></b></td>
			<td colspan="2">
				<div class="sw-btn-bar-wizard">
					<orion:LocalizableButtonLink ID="locButtonExcel" NavigateUrl='<%# GetDataURL() + "&DataFormat=Excel" %>' DisplayType="Small" 
                        LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_TM0_79 %>" runat="server" />
                    <orion:LocalizableButtonLink ID="locButtonChart" NavigateUrl='<%# GetDataURL() %>' DisplayType="Small" 
                        LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_TM0_80 %>" runat="server" />
			    </div>
			</td>
		</tr>
		</form>
		</table>
	</td>
</tr>
</table>
</div>
