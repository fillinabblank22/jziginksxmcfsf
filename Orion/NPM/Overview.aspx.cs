using System;
using SolarWinds.NPM.Web.DAL;
using SolarWinds.Orion.Web;
using System.Collections.Generic;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Overview : System.Web.UI.Page, IResourceContainer
{
    private const string printPageUrl = "PrintableOverview.aspx?nodes={0}&interfaces={1}";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.NodeStyle.DataSource = OverviewDAL.GetNodeOverviewStyles();
            this.NodeStyle.DataBind();
			this.InterfaceStyle.DataSource = OverviewDAL.GetInterfaceOverviewStyles();
            this.InterfaceStyle.DataBind();

			NodeOverviewStyle nodeStyle = NodeOverviewStyle.NodeStatus;
			// check node style setting in url
			string nodeStyleName = this.Request.QueryString["NodeStyle"];
			if (!String.IsNullOrEmpty(nodeStyleName))
			{
				try
				{
					nodeStyle = (NodeOverviewStyle)Enum.Parse(typeof(NodeOverviewStyle), nodeStyleName, true);
				}
				catch { }
				// no need to handle this
			}
			this.NodeStyle.SelectedValue = Convert.ToInt32(nodeStyle).ToString();
			this.overviewTable.NodeStyle = nodeStyle;

			InterfaceOverviewStyle interfaceStyle = InterfaceOverviewStyle.InterfaceStatus;
			// check interface style setting in url
			string interfaceStyleName = this.Request.QueryString["InterfaceStyle"];
			if (!String.IsNullOrEmpty(interfaceStyleName))
			{
				try
				{
					interfaceStyle = (InterfaceOverviewStyle)Enum.Parse(typeof(InterfaceOverviewStyle), interfaceStyleName, true);
				}
				catch { }
				// no need to handle this
			}
			this.InterfaceStyle.SelectedValue = Convert.ToInt32(interfaceStyle).ToString();
			this.overviewTable.InterfaceStyle = interfaceStyle;
		}
        else
        {
            this.overviewTable.NodeStyle = (NodeOverviewStyle)Enum.Parse(typeof(NodeOverviewStyle), this.NodeStyle.SelectedValue);
            this.overviewTable.InterfaceStyle = (InterfaceOverviewStyle)Enum.Parse(typeof(InterfaceOverviewStyle), this.InterfaceStyle.SelectedValue);
        }

		// set custom tooltips
		NetObjectFactory.ChangeNetObjectTipMap(new Dictionary<string, string> {
			{ "N", "/Orion/NetPerfMon/NodePopup.aspx?NodeOverviewStyle=" + ((int)this.overviewTable.NodeStyle).ToString()  },
			{ "I", "/Orion/NPM/InterfacePopup.aspx?InterfaceOverviewStyle=" + ((int)this.overviewTable.InterfaceStyle).ToString()  }
		});
    }

    protected void Printable_Click(object sender, EventArgs e)
    {
        this.Response.Redirect(String.Format(printPageUrl, this.NodeStyle.SelectedValue, this.InterfaceStyle.SelectedValue));
    }
}
