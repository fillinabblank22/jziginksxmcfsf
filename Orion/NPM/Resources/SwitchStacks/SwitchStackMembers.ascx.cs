﻿using System;
using System.Collections.Generic;
using System.Data;

using Resources;

using SolarWinds.NPM.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NPM_Resources_SwitchStacks_SwitchStackMembers : ResourceControl
{
    public int NodeID
    {
        get
        {
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

            return nodeProvider.Node != null ? nodeProvider.Node.NodeID : 0;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcestackmembers";
        }
    }

    protected override string DefaultTitle
    {
        get { return NPMWebContent.NPMWEBDATA_JP1_7; }
    }

    protected int ScriptFriendlyResourceID
    {   // to handle reporting which uses negative IDs
        get { return Math.Abs(Resource.ID); }
    }

    protected string NoCpuMemoryHelpLink
    {
        get { return HelpHelper.GetHelpUrl(HelpLinkFragment); }
    }

    public String DefaultOrderBy
    {
        get { return "SwitchNumber"; }
    }

    public String SWQL
    {
        get
        {
            const String Query = @"
              SELECT
                    cputhr.ThresholdOperator as _CpuThresholdOperator,
                    cputhr.Level1Value as _CpuWarning, 
                    cputhr.Level2Value as _CpuCritical,

                    ramthr.ThresholdOperator as _RamThresholdOperator,
                    ramthr.Level1Value as _RamWarning, 
                    ramthr.Level2Value as _RamCritical,
                    
                    Role,
                    SwitchNumber,
                    SwPriority,
                    HwPriority,
                    Model,
                    SerialNumber,
                    MacAddress,
                    IsNull(ram.AvgPercentMemoryUsed,-1) as Ram,
                    IsNull(cpu.AvgLoad,-1) as Cpu
               FROM
                    Orion.NPM.SwitchStackMember ssm
                    LEFT JOIN Orion.CPUMultiLoadCurrent cpu ON ssm.NodeID = cpu.NodeID  AND ssm.MemberID = cpu.CPUIndex 
                    LEFT JOIN Orion.MemoryMultiLoadCurrent ram ON ssm.NodeID = ram.NodeID  AND ssm.MemberID = ram.Index
                    LEFT JOIN Orion.CpuLoadThreshold cputhr ON cputhr.InstanceId = ssm.NodeID
                    LEFT JOIN Orion.PercentMemoryUsedThreshold ramthr ON ramthr.InstanceId = ssm.NodeID
               WHERE
                    ssm.NodeID = {0}";

            return String.Format(Query, NodeID);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Notification.AssingNodeId(NodeID);
        Visible = IsVisible();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            CustomTable.SWQL = SWQL;
            CustomTable.OrderBy = DefaultOrderBy;
            CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        }
    }

    protected Boolean IsVisible()
    {
        return IsPollerAssigned() && AreDataAvailable();
    }

    private Boolean AreDataAvailable()
    {
        const String Query = @"
            SELECT
                TOP 1 ID
            FROM
                Orion.NPM.SwitchStackMember
            WHERE
                NodeID = @NodeID";

        var parameters = new Dictionary<string, object> { { "NodeID", NodeID } };

        return HasQueryRows(Query, parameters);
    }

    private Boolean IsPollerAssigned()
    {
        const String Query = @"
            SELECT
                TOP 1 InstanceID
            FROM
                Orion.TechnologyPollingAssignments
            WHERE
                TechnologyPollingAssignments.TechnologyPollingID = @technologyPollingID
                AND
                TechnologyPollingAssignments.InstanceID = @instanceID
                AND
                TechnologyPollingAssignments.TargetEntity = 'Orion.Nodes'
                AND
                TechnologyPollingAssignments.Enabled = True";

        var parameters = new Dictionary<string, object>();
        parameters["technologyPollingID"] = NPMWellKnownTechnologyPolling.SwitchStack;
        parameters["instanceID"] = NodeID;

        return HasQueryRows(Query, parameters);
    }

    private static Boolean HasQueryRows(String query, Dictionary<string, object> parameters)
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            DataTable results = swis.Query(query, parameters);
            return results != null && results.Rows != null && results.Rows.Count > 0;
        }
    }
}
