﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StackPowerRing.ascx.cs" Inherits="Orion_NPM_Resources_SwitchStacks_StackPowerRing" %>

<%@ Import Namespace="SolarWinds.NPM.Common.Models.SwitchStack" %>
<%@ Register Src="~/Orion/NPM/Controls/SwitchStackRingControl.ascx" TagPrefix="npm" TagName="SwitchStackRingControl" %>

<orion:Include ID="Include1" runat="server" Module="NPM" File="SwitchStackRing.css" />

<script type="text/javascript">
    var StackPowerRing = {
        renderRedundancyRunning: function (data, ResourceId) {
            var upStatusCount = $.grep(data, function (c, i) { return c.Status == <%= (int)StackPowerPortLinkStatusEnum.Up %>; }).length;
            var warn = !(data.length == upStatusCount);
            var ringFailure = (data.length - upStatusCount) > 1;
            var redundancyHtmlFormat = '<span class="sw-switch-stack-warn"><span class="sw-switch-stack-warn-icon"></span>{0}</span>';
            var redundancyRunning = $('#RedundancyRunning-' + ResourceId);
            var powerMode = redundancyRunning.attr('powerMode');
            if(ringFailure) {
                redundancyRunning.html(String.format(redundancyHtmlFormat, '<%= Resources.NPMWebContent.NPMWEBDATA_VK0_4 %>'));
            }
            else {
                if(warn) {
                    redundancyRunning.html(String.format(redundancyHtmlFormat, powerMode));
                }
                else {
                    redundancyRunning.html(powerMode);
                }
            }
        }
    }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound">
            <ItemTemplate>
                <table id="SwitchStackTable-<%# GetResourceID(Eval("PowerStackID")) %>" class="sw-switch-stack-table">
                    <tr>
                        <td><%= Resources.NPMWebContent.PowerRing_RingName.ToUpper() %></td>
                        <td><%# Eval("Name") %></td>
                    </tr>
                    <tr>
                        <td><%= Resources.NPMWebContent.PowerRing_RedundancyRunning.ToUpper() %></td>
                        <td id="RedundancyRunning-<%# GetResourceID(Eval("PowerStackID")) %>" powerMode="<%# GetRedundancyRunning(Eval("PowerMode")) %>"></td>
                    </tr>
                </table>
                <npm:SwitchStackRingControl runat="server" ID="SwitchStackRingControl" />
            </ItemTemplate>
            <SeparatorTemplate>
                <hr class="sw-separator-line" />
            </SeparatorTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>