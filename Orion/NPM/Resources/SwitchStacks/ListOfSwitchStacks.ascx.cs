﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

using Resources;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Technology;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_SwitchStack_ListOfSwitchStacks : ResourceControl
{
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override string DefaultTitle
    {
        get { return NPMWebContent.NPMWEBDATA_JP1_1; }
    }

    protected int ScriptFriendlyResourceID
    {   // to handle reporting which uses negative IDs
        get { return Math.Abs(Resource.ID); }
    }

    protected String NodeCaptionLinkFormat
    {
        get { return @"/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}"; }
    }

    public String SWQL
    {
        get
        {
            return @"
                SELECT
                    SwitchStack.Nodes.Caption,
                    SwitchStack.NodeID AS _NodeID,
                    SwitchStack.Nodes.IPAddress,
                    SwitchStackMember.MacAddress,
                    MemberCount,

                    -- Status
                    SwitchStack.Status,
                    si.IconPostfix AS _Status,
                    StatusDescription AS _StatusDescription,

                    -- DataRingStatus
                    SwitchStack.DataRingStatus,
                    drsi.IconPostfix AS _DataRingStatusIcon,
                    drsi.ShortDescription AS _DataRingStatusDescription,

                    -- PowerRingStatus
                    SwitchStack.PowerRingStatus,
                    prsi.IconPostfix AS _PowerRingStatusIcon,
                    prsi.ShortDescription AS _PowerRingStatusDescription
                FROM
	                Orion.NPM.SwitchStack
                LEFT JOIN
	                Orion.NPM.SwitchStackMember ON SwitchStack.MasterNumber = SwitchStackMember.SwitchNumber AND SwitchStack.NodeID = SwitchStackMember.NodeID
                LEFT JOIN
	                Orion.StatusInfo si ON SwitchStack.Status = si.StatusId
                LEFT JOIN
	                Orion.StatusInfo drsi ON SwitchStack.DataRingStatus = drsi.StatusId
                LEFT JOIN
	                Orion.StatusInfo prsi ON SwitchStack.PowerRingStatus = prsi.StatusId
                JOIN
                    Orion.Pollers p ON SwitchStack.NodeID = p.NetObjectID
                        AND p.NetObjectType = 'N'
                        AND p.PollerType = 'N.SwitchStack.SNMP.Cisco'
                        AND p.Enabled = 'True'
                WHERE
                    SwitchStack.Nodes.Caption IS NOT NULL"; // SwitchStack.Nodes.Caption IS NOT NULL - so that limitation condition discards the whole row
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Visible = IsVisible();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            CustomTable.SWQL = SWQL;
            CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        }
    }

    protected Boolean IsVisible()
    {
        return IsAnySwitchStackPolled() && AreDataAvailable();
    }

    protected Boolean AreDataAvailable()
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            DataTable results = swis.Query(@"SELECT TOP 1 NodeID FROM Orion.NPM.SwitchStack");
            return results != null && results.Rows != null && results.Rows.Count > 0;
        }
    }

    protected Boolean IsAnySwitchStackPolled()
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            const String Query = @"
            SELECT
                TOP 1 InstanceID
            FROM
                Orion.TechnologyPollingAssignments
            WHERE
                TechnologyPollingAssignments.TechnologyPollingID = @technologyPollingID
                AND
                TechnologyPollingAssignments.TargetEntity = 'Orion.Nodes'
                AND
                TechnologyPollingAssignments.Enabled = True";

            DataTable results = swis.Query(Query, new Dictionary<string, object> { { "technologyPollingID", NPMWellKnownTechnologyPolling.SwitchStack } });

            return results != null && results.Rows != null && results.Rows.Count > 0;
        }
    }
}
