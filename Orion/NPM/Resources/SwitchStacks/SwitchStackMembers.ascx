﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SwitchStackMembers.ascx.cs" Inherits="Orion_NPM_Resources_SwitchStacks_SwitchStackMembers" %>
<%@ Import Namespace="SolarWinds.NPM.Common.Models.SwitchStack" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="npm" TagName="singleMemberNotification" Src="~/Orion/NPM/Controls/SwitchStack/Notifications/SingleMemberSwitch.ascx" %>

<orion:Include Module="NPM" File="SSMResources.css" runat="server" />
<orion:Include runat="server" File="Formatters.js" Module="NPM"/>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <npm:singleMemberNotification runat="server" ID="Notification" CssClasses="sw-npm-switchStack-Notification" />

        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <div id="KbContent" runat="server" style="display: none; padding-top: 10px;">
            <%=Resources.NPMWebContent.NPMWEBDATA_VM0_2 %>
            <a class="coloredLink" href="<%=NoCpuMemoryHelpLink%>" target="_blank">&nbsp;&raquo;&nbsp;<%=Resources.NPMWebContent.NPMWEBDATA_RB0_19 %></a>
        </div>
        <script type="text/javascript">
            $(function() {
                var query = SW.Core.Resources.CustomQuery;
                var npmFormatters = SW.NPM.Formatters;

                var resourceId = <%= ScriptFriendlyResourceID %>;
                var roleImageHtmlFormat = '<img src="/Orion/NPM/Images/SwitchStacks/{0}-role-{1}.png" alt="{2}" title="{2}">';

                var rolesImageHtml = {
                    "master": String.format(roleImageHtmlFormat, "dataring", "master", "<%= Resources.NPMWebContent.NPMWEBDATA_JP1_12%>"),
                    "standby": String.format(roleImageHtmlFormat, "dataring", "standby", "<%= Resources.NPMWebContent.NPMWEBDATA_JP1_13%>")
                };
                
                var thresholdOperatorEnum = {
                    Greater : 0,
                    GreaterOrEqual : 1,
                    Equal : 2,
                    LessOrEqual : 3,
                    Less : 4,
                    NotEqual : 5
                };

                var isOperatorTrue = function(operator, limitValue, currentValue) {
                    switch(operator) {
                        case thresholdOperatorEnum.Greater:
                            return currentValue > limitValue;
                        case thresholdOperatorEnum.GreaterOrEqual:
                            return currentValue >= limitValue;
                        case thresholdOperatorEnum.Equal:
                            return currentValue === limitValue;
                        case thresholdOperatorEnum.LessOrEqual:
                            return currentValue <= limitValue;
                        case thresholdOperatorEnum.Less:
                            return currentValue < limitValue;
                        case thresholdOperatorEnum.NotEqual:
                            return currentValue !== limitValue;
                    }
                };

                var detectThresholdLevel = function(operator, warningLvl, criticalLvl, currentValue, baseCss) {
                    
                    if (isOperatorTrue(operator, criticalLvl, currentValue)) //is critical
                    {
                        return baseCss.concat(' sw-npm-switchStack-CriticalLevel');
                    } else if (isOperatorTrue(operator, warningLvl, currentValue)) // is warning
                    {
                        return baseCss.concat(' sw-npm-switchStack-WarningLevel');
                    } //is default
                    else {
                        return baseCss;
                    }
                }

                var tableSettings = {
                    uniqueId: resourceId,
                    initialPage: 0,
                    allowSort: true,
                    allowPaging: false,
                    rowsPerPage: 10,
                    columnSettings: {
                        "Role": {
                            header: "",
                            formatter: function(value) {
                                if (value == '<%= (int)SwitchRoleEnum.Master%>') {
                                    return rolesImageHtml.master;
                                } else if (value == '<%= (int)SwitchRoleEnum.Standby%>') {
                                    return rolesImageHtml.standby;
                                }

                                return "";
                            },
                            isHtml: true,
                            cellCssClassProvider: function() {
                                return 'sw-npm-switchStack-Role';
                            }
                        },
                        "SwitchNumber": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_8%>',
                            formatter: function(value) {
                                return String.format('<%= Resources.NPMWebContent.NPMWEBDATA_JP1_17%>', value);
                            },
                            cellCssClassProvider: function() {
                                return 'sw-npm-switchStack-SwNumber';
                            }
                        },
                        "SerialNumber": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_18%>',
                            cellCssClassProvider: function() {
                                return ' sw-npm-switchStack-SerialNumber';
                            }
                        },
                        "SwPriority": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_10%>',
                            cellCssClassProvider: function() {
                                return 'sw-npm-switchStack-SwPriority';
                            }
                        },
                        "HwPriority": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_16%>',
                            cellCssClassProvider: function() {
                                return 'sw-npm-switchStack-HwPriority';
                            }
                        },
                        "Model": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_11%>',
                            cellCssClassProvider: function() {
                                return ' sw-npm-switchStack-Model';
                            }
                           
                        },
                        "MacAddress": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_VB0_120%>',
                            formatter: function(value) {
                                return npmFormatters.formatMacAddress(value);
                            },
                            cellCssClassProvider: function() {
                                return ' sw-npm-switchStack-MacAddress';
                            }
                        },
                        "Ram": {
                            header:  '<%= Resources.NPMWebContent.NPMWEBDATA_VM0_3%>',
                            cellCssClassProvider: function(value, row, cellInfo) {
                                var baseCss = 'sw-npm-switchStack-Ram';

                                var ramOperator = row[3];
                                var ramWarning = row[4];
                                var ramCritical = row[5];

                                var css = detectThresholdLevel(ramOperator, ramWarning, ramCritical, value, baseCss);
                                return css;
                            },
                            formatter: function(value) {

                                if (value >= 0)
                                    return String.format('{0}%', Math.round(value * 100) / 100);
                                else {
                                    $("#<%=KbContent.ClientID%>").show();
                                    return '<%= Resources.NPMWebContent.NPMWEBCODE_VB0_12%>';
                                }
                            }
                           
                        },
                        "Cpu": {
                            header:  '<%= Resources.NPMWebContent.NPMWEBDATA_VM0_1%>',
                            cellCssClassProvider: function(value, row, cellInfo) {
                                var baseCss = 'sw-npm-switchStack-Cpu';

                                var cpuOperator = row[0];
                                var cpuWarning = row[1];
                                var cpuCritical = row[2];

                                var css = detectThresholdLevel(cpuOperator, cpuWarning, cpuCritical, value, baseCss);
                                return css;
                            },
                            formatter: function(value) {
                                if (value >= 0)
                                    return String.format('{0}%', Math.round(value * 100) / 100);
                                else {
                                    $("#<%=KbContent.ClientID%>").show();
                                    return '<%= Resources.NPMWebContent.NPMWEBCODE_VB0_12%>';
                                }
                            }
                           
                        }
                    }
                };
                
                query.initialize(tableSettings);

                var refresh = function() { query.refresh(resourceId); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID%>');
                refresh();
            });
          </script>
    </Content>
</orion:resourceWrapper>
