﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Castle.Core.Internal;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models.SwitchStack;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NPM_Resources_SwitchStacks_SwitchStackDetails : BaseResourceControl
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public string MACAddressString { get; set; }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBDATA_AP0_1; }
    }

    protected Node Node
    {
        get { return GetInterfaceInstance<INodeProvider>().Node; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourceswitchstackdetails";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Visible = IsPollerAssigned() && AreDataAvailable();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            IPAdress.Text = Node.IPAddressString;
            this.MACAddressString = GetMacAddress(Node.NodeID);
        }
    }

    private string GetMacAddress(int nodeId)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        { 
            var dt = proxy.Query(@"
            SELECT MacAddress as mac
            FROM Orion.NPM.SwitchStackMember
            WHERE NodeID=@nodeId AND Role=@master",
                new Dictionary<string, object> { { "nodeId", nodeId }, { "master", (int)SwitchRoleEnum.Master } });

            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0]["mac"].ToString();
            }
        }
        return String.Empty;
    }

    private bool AreDataAvailable()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            const string Query = @"
            SELECT 
                TOP 1 NodeID
            FROM 
                Orion.NPM.SwitchStack
            WHERE
                NodeID = @NodeID";

            DataTable dt = proxy.Query(Query, new Dictionary<string, object> {{"NodeID", this.Node.NodeID}});
            return dt != null && dt.Rows.Count > 0;
        }
    }

    private bool IsPollerAssigned()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            const string Query = @"
            SELECT
                TOP 1 InstanceID
            FROM
                Orion.TechnologyPollingAssignments
            WHERE
                TechnologyPollingAssignments.TechnologyPollingID=@technologyPollingId
            AND
                TechnologyPollingAssignments.InstanceID=@instanceId
            AND
                TechnologyPollingAssignments.TargetEntity='Orion.Nodes'
            AND
                TechnologyPollingAssignments.Enabled=True";

            DataTable dt = proxy.Query(Query, new Dictionary<string, object> { { "technologyPollingId", NPMWellKnownTechnologyPolling.SwitchStack }, { "instanceId", this.Node.NodeID } });
            return dt != null && dt.Rows.Count > 0;
        }
        return true;
    }

}