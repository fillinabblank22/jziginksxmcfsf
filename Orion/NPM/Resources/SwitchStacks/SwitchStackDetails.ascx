﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SwitchStackDetails.ascx.cs" Inherits="Orion_NPM_Resources_SwitchStacks_SwitchStackDetails" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" File="Formatters.js" Module="NPM"/>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
            $(function() {
                var npmFormatters = SW.NPM.Formatters;

                function formatMacAddress(value) {
                    var formatedValue = npmFormatters.formatMacAddress(value);
                    $("#<%=lblMACAddress.ClientID%>").text(formatedValue);
                }

                formatMacAddress("<%=MACAddressString%>");
            });
        </script>

        <div style="height: 5px;">&nbsp;</div>
        <table id="detailsTable" class="NeedsZebraStripes" border="0" cellPadding="2" cellSpacing="0" width="100%" runat="server">
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle">
                    <%= NPMWebContent.NPMWEBDATA_AP0_2 %>
                </td>
                <td class="Property">
                    <asp:Label ID="IPAdress" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
                <td class="PropertyHeader" valign="middle">
                    <%= NPMWebContent.NPMWEBDATA_AP0_3 %>
                </td>
                <td class="Property">
                    <asp:Label ID="lblMACAddress" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>