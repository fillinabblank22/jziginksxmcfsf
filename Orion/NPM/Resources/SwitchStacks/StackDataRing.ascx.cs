﻿using Newtonsoft.Json;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NPM_Resources_SwitchStacks_StackDataRing : BaseResourceControl
{
    private int NodeID
    {
        get
        {
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
            return nodeProvider.Node != null ? nodeProvider.Node.NodeID : 0;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Visible = IsPollerAssigned() && AreDataAvailable();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            SwitchStackDataRingContent.Visible = !IsRingFailure();
            RingFailureContent.Visible = IsRingFailure();

            SwitchStackRingControl.ResourceID = Resource.ID.ToString(CultureInfo.InvariantCulture);
            SwitchStackRingControl.ServiceConfig = JsonConvert.SerializeObject(
                new
                {
                    url = "/Orion/NPM/Services/SwitchStackRing.asmx",
                    methodName = "GetSwitchStackDataMembers",
                    methodParams = new { nodeId = NodeID }
                });
            SwitchStackRingControl.ResourceConfig = JsonConvert.SerializeObject(
                new
                {
                    role = new
                        {
                            master = new { title = Resources.NPMWebContent.NPMWEBDATA_JP1_12, href = "/Orion/NPM/Images/SwitchStacks/dataring-role-master.png" },
                            standby = new { title = Resources.NPMWebContent.NPMWEBDATA_JP1_13, href = "/Orion/NPM/Images/SwitchStacks/dataring-role-standby.png" }
                        },
                    status = new
                        {
                            up = new { title = Resources.NPMWebContent.NPMWEBDATA_VK0_6, href = "/Orion/NPM/Images/SwitchStacks/data-up.png" },
                            down = new { title = Resources.NPMWebContent.NPMWEBDATA_VK0_7, href = "/Orion/NPM/Images/SwitchStacks/data-down.png" }
                        }
                });
            SwitchStackRingControl.jsRenderHeaderTableFunc = "StackDataRing.renderBandwidth";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBDATA_VK0_1; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcestackdataring";
        }
    }

    private bool AreDataAvailable()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            string query = @"
            SELECT TOP 1
                m.SwitchNumber
            FROM Orion.NPM.SwitchStackMember m 
            WHERE m.NodeID=@nodeId AND m.SwitchStackMemberPort.MemberID IS NOT NULL";

            DataTable dt = proxy.Query(query, new Dictionary<string, object>() { { "nodeId", NodeID } });
            return dt != null && dt.Rows.Count > 0;
        }
    }

    private bool IsRingFailure()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            string query = @"
            SELECT 
                RingFailure
            FROM Orion.NPM.SwitchStack
            WHERE NodeID=@nodeId";

            DataTable dt = proxy.Query(query, new Dictionary<string, object>() { { "nodeId", NodeID } });
            if (dt != null && dt.Rows.Count > 0)
                return dt.Rows[0].Field<bool>("RingFailure");
            else
                return true;
        }
    }

    private bool IsPollerAssigned()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            string query = @"
            SELECT
                TOP 1 InstanceID
            FROM
                Orion.TechnologyPollingAssignments
            WHERE
                TechnologyPollingAssignments.TechnologyPollingID=@technologyPollingId
            AND
                TechnologyPollingAssignments.InstanceID=@instanceId
            AND
                TechnologyPollingAssignments.TargetEntity='Orion.Nodes'
            AND
                TechnologyPollingAssignments.Enabled=True";

            DataTable dt = proxy.Query(query, new Dictionary<string, object> { { "technologyPollingId", NPMWellKnownTechnologyPolling.SwitchStack }, { "instanceId", NodeID } });
            return dt != null && dt.Rows.Count > 0;
        }
    }
}