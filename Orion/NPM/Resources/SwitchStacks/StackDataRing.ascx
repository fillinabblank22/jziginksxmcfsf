﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StackDataRing.ascx.cs" Inherits="Orion_NPM_Resources_SwitchStacks_StackDataRing" %>

<%@ Import Namespace="SolarWinds.NPM.Common.Models.SwitchStack" %>
<%@ Register Src="~/Orion/NPM/Controls/SwitchStackRingControl.ascx" TagPrefix="npm" TagName="SwitchStackRingControl" %>

<orion:Include ID="Include1" runat="server" Module="NPM" File="SwitchStackRing.css" />

<script type="text/javascript">
    var StackDataRing = {
        renderBandwidth : function (data, ResourceId) {
            var upStatusCount = $.grep(data, function (c, i) { return c.Status == <%= (int)StackPortOperStatusEnum.Up %>; }).length;
            var fullRedundancy = data.length == upStatusCount;
            var ringFailure = (data.length - upStatusCount) > 1;
            var redundancyHtmlFormat = '<span class="sw-switch-stack-warn"><span class="sw-switch-stack-warn-icon"></span>{0}</span>';
            var bandwidth = $('#Bandwidth-' + ResourceId);
            if(ringFailure) {
                bandwidth.html(String.format(redundancyHtmlFormat, '<%= Resources.NPMWebContent.NPMWEBDATA_VK0_4 %>'));
            }
            else {
                if(fullRedundancy) {
                    bandwidth.html('<%= Resources.NPMWebContent.NPMWEBDATA_VK0_2 %>');
                }
                else {
                    bandwidth.html(String.format(redundancyHtmlFormat, '<%= Resources.NPMWebContent.NPMWEBDATA_VK0_3 %>'));
                }
            }
        }
    }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SwitchStackDataRingContent" runat="server">
            <table id="SwitchStackTable-<%= Resource.ID %>" class="sw-switch-stack-table">
                <tr>
                    <td><%= Resources.NPMWebContent.NPMWEBDATA_VB0_186 %></td>
                    <td id="Bandwidth-<%= Resource.ID %>"></td>
                </tr>
            </table>
            <npm:SwitchStackRingControl runat="server" ID="SwitchStackRingControl"/>
        </asp:Panel>
        <div id="RingFailureContent" runat="server" class="sw-suggestion sw-suggestion-warn">
            <div class="sw-suggestion-icon"></div>
            <%= Resources.NPMWebContent.NPMWEBDATA_VK0_4 %>
        </div>
    </Content>
</orion:resourceWrapper>