﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfSwitchStacks.ascx.cs" Inherits="Orion_NPM_Resources_SwitchStack_ListOfSwitchStacks" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:Include runat="server" File="js/Formatters.js" />
<orion:Include runat="server" File="Formatters.js" Module="NPM"/>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        
        <script type="text/javascript">
            $(function() {
                var query = SW.Core.Resources.CustomQuery;
                var coreFormatters = SW.Core.Formatters;
                var npmFormatters = SW.NPM.Formatters;

                var nodeCaptionLinkFormat = '<%= NodeCaptionLinkFormat%>';
                var resourceId = <%= ScriptFriendlyResourceID %>;

                var tableSettings = {
                    uniqueId: resourceId,
                    initialPage: 0,
                    allowSort: true,
                    allowPaging: true,
                    columnSettings: {
                        "Caption": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_5%>',
                            formatter: function(value, row) {
                                row[1] = String.format(nodeCaptionLinkFormat, row[1]);
                                return coreFormatters.makeBreakableText(value);
                            },
                            linkColumn: 1,
                            isHtml: true
                        },
                        "IPAddress": {
                            header: '<%= Resources.NPMWebContent.NPMWEBCODE_VB0_84%>'
                        },
                        "MacAddress": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_VB0_120%>',
                            formatter: function(value) {
                                return npmFormatters.formatMacAddress(value);
                            }
                        },
                        "Status": {
                            header: '<%= Resources.NPMWebContent.NPMWEBCODE_JF0_35%>',
                            formatter: function(value, row) {
                                return '<img src="/Orion/images/StatusIcons/small-' + row[6] + '.gif" align="absbottom" alt="' + row[7] + '" title="' + row[7] + '">';
                            },
                            isHtml: true
                        },
                        "MemberCount": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_4%>'
                        },
                        "DataRingStatus": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_2%>',
                            formatter: function(value, row) {
                                return '<img src="/Orion/images/StatusIcons/small-' + row[9] + '.gif" align="absbottom"> ' + row[10];
                            },
                            isHtml: true
                        },
                        "PowerRingStatus": {
                            header: '<%= Resources.NPMWebContent.NPMWEBDATA_JP1_3%>',
                            formatter: function(value, row) {
                                return '<img src="/Orion/images/StatusIcons/small-' + row[12] + '.gif" align="absbottom"> ' + row[13];
                            },
                            isHtml: true
                        }
                    }
                };

                query.initialize(tableSettings);

                var refresh = function() { query.refresh(resourceId); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID%>');
                refresh();
            });
          </script>
    </Content>
</orion:resourceWrapper>
