﻿using Newtonsoft.Json;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models.SwitchStack;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NPM_Resources_SwitchStacks_StackPowerRing : BaseResourceControl
{
    private int NodeID
    {
        get
        {
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
            return nodeProvider.Node != null ? nodeProvider.Node.NodeID : 0;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBDATA_VK0_10; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcestackpowerring";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Visible = IsPollerAssigned() && AreDataAvailable();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            repeater.DataSource = GetPowerStackIDs();
            repeater.DataBind();
        }
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView row = e.Item.DataItem as DataRowView;
            if (row != null)
            {
                var switchStackRingControl = e.Item.FindControl("SwitchStackRingControl") as Orion_NPM_Controls_SwitchStackRingControl;
                switchStackRingControl.ResourceID = GetResourceID(row["PowerStackID"]);
                switchStackRingControl.ServiceConfig = JsonConvert.SerializeObject(
                    new
                    {
                        url = "/Orion/NPM/Services/SwitchStackRing.asmx",
                        methodName = "GetSwitchStackPowerMembers",
                        methodParams = new { nodeId = NodeID, powerStackId = row["PowerStackID"] }
                    });
                switchStackRingControl.ResourceConfig = JsonConvert.SerializeObject(
                    new
                    {
                        role = new
                            {
                                master = new { title = Resources.NPMWebContent.NPMWEBDATA_JP1_12, href = "/Orion/NPM/Images/SwitchStacks/powerstack-master.png" },
                                standby = new { title = string.Empty, href = string.Empty }
                            },
                        status = new
                            {
                                up = new { title = Resources.NPMWebContent.NPMWEBDATA_VK0_8, href = "/Orion/NPM/Images/SwitchStacks/power-up.png" },
                                down = new { title = Resources.NPMWebContent.NPMWEBDATA_VK0_9, href = "/Orion/NPM/Images/SwitchStacks/power-down.png" }
                            }
                    });
                switchStackRingControl.jsRenderHeaderTableFunc = "StackPowerRing.renderRedundancyRunning";
            }
        }
    }

    protected string GetResourceID(object powerStackId)
    {
        return string.Format(CultureInfo.InvariantCulture, "{0}-{1}", Resource.ID, powerStackId);
    }

    protected string GetRedundancyRunning(object powerMode)
    {
        StackPowerModeEnum mode = (StackPowerModeEnum)Enum.Parse(typeof(StackPowerModeEnum), Convert.ToString(powerMode, CultureInfo.InvariantCulture));
        switch (mode)
        {
            case StackPowerModeEnum.PowerSharing:
                return Resources.NPMWebContent.NPMWEBDATA_VK0_11;
            case StackPowerModeEnum.PowerSharingStrict:
                return Resources.NPMWebContent.NPMWEBDATA_VK0_12;
            case StackPowerModeEnum.Redundant:
                return Resources.NPMWebContent.NPMWEBDATA_VK0_13;
            case StackPowerModeEnum.RedundantStrict:
                return Resources.NPMWebContent.NPMWEBDATA_VK0_14;
            default:
                return Resources.NPMWebContent.NPMWEBCODE_AK0_2; // Unknown
        }
    }

    private DataTable GetPowerStackIDs()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            var dt = proxy.Query(@"
            SELECT DISTINCT
                PowerStackID,
                Name,
                PowerMode
            FROM Orion.NPM.SwitchStackPower p
            WHERE NodeID=@nodeId AND PowerType=@powerType AND p.SwitchStackPowerPort.MemberID IS NOT NULL", new Dictionary<string, object>() { { "nodeId", NodeID }, { "powerType", (int)StackPowerTypeEnum.Ring } });
            return dt;
        }
    }

    private bool AreDataAvailable()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            DataTable dt = proxy.Query(@"
            SELECT TOP 1
                PowerStackID
            FROM Orion.NPM.SwitchStackPower p            
            WHERE NodeID=@nodeId AND PowerType=@powerType AND p.SwitchStackPowerPort.MemberID IS NOT NULL", new Dictionary<string, object>() { { "nodeId", NodeID }, { "powerType", (int)StackPowerTypeEnum.Ring } });
            return dt != null && dt.Rows.Count > 0;
        }
    }

    private bool IsPollerAssigned()
    {
        using (InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            string query = @"
            SELECT
                TOP 1 InstanceID
            FROM
                Orion.TechnologyPollingAssignments
            WHERE
                TechnologyPollingAssignments.TechnologyPollingID=@technologyPollingId
            AND
                TechnologyPollingAssignments.InstanceID=@instanceId
            AND
                TechnologyPollingAssignments.TargetEntity='Orion.Nodes'
            AND
                TechnologyPollingAssignments.Enabled=True";

            DataTable dt = proxy.Query(query, new Dictionary<string, object> { { "technologyPollingId", NPMWellKnownTechnologyPolling.SwitchStack }, { "instanceId", NodeID } });
            return dt != null && dt.Rows.Count > 0;
        }
    }
}