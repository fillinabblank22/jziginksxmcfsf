﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_InterfaceChartsV2_InterfaceChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "I"; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null)
        {
            return new[] { interfaceProvider.Interface.InterfaceID.ToString(CultureInfo.InvariantCulture) };
        }

        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            const string swql = "SELECT InterfaceID FROM Orion.NPM.Interfaces WHERE NodeID = @NodeId";
            return GetElementsFromSwql(swql, "NodeId", nodeProvider.Node.NodeID);
        }

        return new string[0];
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_ZB0_1; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

	public override IEnumerable<Type> SupportedInterfaces
	{
		get { return new[] { typeof(IInterfaceProvider) }; }
	}


    public bool IsInternal
    {
        get { return true; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}

