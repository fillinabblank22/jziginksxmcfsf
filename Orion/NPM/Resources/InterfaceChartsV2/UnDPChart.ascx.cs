﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Web.ChartingV2;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using System.Collections.Specialized;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_InterfaceChartsV2_UnDPChart : StandardChartResource, IResourceIsInternal
{
	private Guid _assignment;
	private string _nodeid;

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { yield return typeof(IInterfaceProvider); }
	}

	protected override NameValueCollection GetCustomChartParameters()
	{
		var rv = new NameValueCollection();
		rv["CustomPollerID"] = Resource.Properties["CustomPollerID"];
		return rv;
	}

	protected override Dictionary<string, object> GetContextForMacros(NetObject netObject)
	{
		var context = base.GetContextForMacros(netObject);
		var rv = UnDPChartHelper.LoadContextForUnDPChart(Resource.Properties["CustomPollerID"]);
		foreach (var k in context)
		{
			rv.Add(k.Key, k.Value);
		}
		return rv;
	}

	protected override void AddChartExportControl(Control resourceWrapperContents)
	{
		if (!string.IsNullOrEmpty(Resource.Properties["CustomPollerID"])) // do not show export for unconfigured UnDP
		{
			base.AddChartExportControl(resourceWrapperContents);
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);


		bool visible = true;
		if ((!string.IsNullOrEmpty(Resource.Properties["AutoHide"])) && (Resource.Properties["AutoHide"].ToString() == "1"))
		{
			visible = false; // we set this to true if any record is found. 
		}


		using (var swis = InformationServiceProxy.CreateV3())
		{
			if (!string.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
			{
				var dict = new Dictionary<string, object>();
				dict["guid"] = Resource.Properties["CustomPollerID"];

				foreach (var id in GetElementIdsForChart())
				{
					dict["nodeid"] = id;
					var rv = swis.Query(
						"SELECT CustomPollerAssignmentID AS cnt FROM Orion.NPM.CustomPollerAssignment WHERE CustomPollerID=@guid AND InterfaceID=@nodeid",
						dict);

					if (rv.Rows.Count > 0)
					{
						visible = true;
						_assignment = new Guid(rv.Rows[0][0].ToString());
						_nodeid = id;
						break;
					}
				}

				if (!visible)
				{
					this.Visible = false;
//					return; // HandleInit needs to be called regardless of visiblity
				}
			}
		}

		HandleInit(WrapperContents);
	}

	public bool IsInternal
	{
		get { return true; }
	}


	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}


	protected override IEnumerable<string> GetElementIdsForChart()
	{
		var interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
		if (interfaceProvider != null)
		{
			return new[] { interfaceProvider.Interface.InterfaceID.ToString(CultureInfo.InvariantCulture) };
		}

		return new string[0];
	}

	protected override string NetObjectPrefix
	{
		get { return "I"; }
	}

	protected override string DefaultTitle
	{
        get { return Resources.NPMWebContent.NPMWEBCODE_TM0_6; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}