using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_WhatsNew_WhatsNewNPM : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.ShowEditButton = false;
    }

    public override string HelpLinkFragment
    {
        get
        {
            return string.Empty;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_2; }
    }

    public sealed override string DisplayTitle
    {
        get { return Resources.NPMWebContent.WhatsNewResource_Title; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }

    protected void btnRemoveResource_Click(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(this.Resource.ID);

        // reorder resources after delete
        var targetColumn = Resource.Column;
        var resources = ResourceManager.GetResourcesForView(Resource.View.ViewID);
        var pos = 1;

        foreach (var resource in resources)
        {
            if (resource.Column == targetColumn)
            {
                ResourceManager.SetPositionById(pos++, resource.ID);
            }
        }

        // after delete resource is still in Page.Controls, because it's a postback
        // resource. so we have to reload page
        Response.Redirect(Request.Url.AbsoluteUri);
    }

}