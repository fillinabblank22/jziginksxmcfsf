﻿<%@ control language="C#" autoeventwireup="true" codefile="WhatsNewNPM.ascx.cs" inherits="Orion_NPM_Resources_WhatsNew_WhatsNewNPM" %>
<orion:include runat="server" file="../NPM/styles/WhatsNew.css" />
<%@ register src="~/Orion/Controls/HelpLink.ascx" tagprefix="orion" tagname="HelpLink" %>
<orion:resourcewrapper id="Wrapper" runat="server">
    <Content>
        <div id="whatsnew">
        <table class="whatsnew-borderreset">
            <colGroup>
                <col width="auto" />
                <col width="100%" />
            </colGroup>
            <tr>
                <% string kbLinkTemplate = SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBLinkTemplate();%>
                <% string currentLanguage = Resources.CoreWebContent.CurrentHelpLanguage;%>
                <td valign="top"><img alt="" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("NPM","WhatsNew/icon_new.gif") %>" border="0" /> </td>
                <td>
                    <div class="whats-new-line">
                        <strong><%=Resources.NPMWebContent.WhatsNewResource_Feature_1_Title %></strong>
                        <ul>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_1_Item_1 %></li>
                        </ul>
                    </div>
                    <div class="whats-new-line">
                        <strong><%=Resources.NPMWebContent.WhatsNewResource_Feature_2_Title %></strong>
                        <ul>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_2_Item_1 %></li>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_2_Item_2 %></li>
                        </ul>
                    </div>
                    <div class="whats-new-line">
                        <strong><%=Resources.NPMWebContent.WhatsNewResource_Feature_3_Title %></strong>
                        <ul>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_3_Item_1 %></li>
                        </ul>
                    </div>
                    <div class="whats-new-line">
                        <div class="whats-new-line-learnmore">
                            &raquo;&nbsp;<a target=_blank href="<%=string.Format(kbLinkTemplate, currentLanguage, "NPM_2020-2_release_notes")%>">
                                <%# Resources.NPMWebContent.NPMWEBDATA_RB0_19 %>
                            </a>
                        </div>
                    </div>
                    <div class="whats-new-line">
                        <strong><%=Resources.NPMWebContent.WhatsNewResource_Feature_4_Title %></strong>
                        <ul>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_4_Item_1 %></li>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_4_Item_2 %></li>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_4_Item_3 %></li>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_4_Item_4 %></li>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_4_Item_5 %></li>
                            <li><%=Resources.NPMWebContent.WhatsNewResource_Feature_4_Item_6 %></li>
                        </ul>
                        <div class="whats-new-line-learnmore">
                            &raquo;&nbsp;<a target=_blank href="<%=string.Format(kbLinkTemplate, currentLanguage, "OrionPlatform_2020-2_release_notes")%>">
                                <%# Resources.NPMWebContent.NPMWEBDATA_RB0_19 %>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr><td />&nbsp;<td /> </tr>
            
             <tr>
                <td colspan="2" align="right">
                    <div class="sw-btn-bar">
                    <orion:LocalizableButtonLink NavigateUrl='https://www.solarwinds.com/documentation/kbloader.aspx?kb=NPM_2020-2_release_notes' runat="server" ID="imgNewFeature" LocalizedText="CustomText" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_RB0_19%>" DisplayType="Primary" Target="_blank" />
                    <orion:LocalizableButton runat="server" ID="btnRemoveResource" LocalizedText="CustomText" Text="<%$Resources: NPMWebContent, NPMWEBDATA_VB0_27%>" DisplayType="Secondary" OnClick="btnRemoveResource_Click" />
                    </div>
                </td>
            </tr>
        </table>
        </div>
    </Content>
</orion:resourcewrapper>
