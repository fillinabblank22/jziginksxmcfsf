<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSourceCustomPollerChart.ascx.cs" Inherits="MultiSourceCharts_MultiSourceCustomPollerChart" %>

<orion:Include Module="NPM" File="Resources/MultiSourceCharts/Styles.css" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>

        <table id="Info" runat="server" visible="true" >
            <tr>
                <td style="border:0px; padding:0px;">
                <div class="MultiChartInfo">
                    <%=Resources.NPMWebContent.NPMWEBDATA_DP0_SINGLECHART%>
                    <ul>
                       <li><%=Resources.NPMWebContent.NPMWEBDATA_AK0_47%></li>
                       <li><%=Resources.NPMWebContent.NPMWEBDATA_AK0_48%></li>
                    </ul>
                    <br />

                      <%if (Profile.AllowCustomize)
                          {%>
                        <a href="<%= EditURL %>" >&#0187; <%=Resources.NPMWebContent.NPMWEBDATA_AK0_49%></a>
                        <%}%>

                </div>
                </td>
                <td style="border:0px;">
                    <img src="/Orion/NPM/Images/MultiSourceCharts/Graph.BackgroundArt.gif"/>
                </td>
            </tr>
        </table>

        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper> 