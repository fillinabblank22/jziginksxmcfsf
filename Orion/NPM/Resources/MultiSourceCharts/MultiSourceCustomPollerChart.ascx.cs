using System;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System.Collections.Specialized;
using SolarWinds.Orion.NPM.Web.UI;
using System.Collections.Generic;

public partial class MultiSourceCharts_MultiSourceCustomPollerChart : GraphResource
{
    private bool manualSelection;
    private string chartNameFromProperties;
    private string selectedEntities;
    private bool filterEntities;
    private string entityName;
    private string customPollerID;
    private string topXX = String.Empty;
    private bool autoHide = false;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (CheckProperties(Resource.Properties))
        {
            Info.Visible = false;

            if (manualSelection)
            {
                string netObject = filterEntities ? Request.QueryString["NetObject"] : String.Empty;
                selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(entityName, netObject, customPollerID, selectedEntities, topXX);
            }
            else
            {
                selectedEntities = EntitiesHelper.GetUnDPEntitiesFilteredByNetObject(entityName, Request.QueryString["NetObject"], customPollerID, null, topXX);
            }

            string chartName;

            if (string.IsNullOrEmpty(chartNameFromProperties))
            {
                chartName= (entityName == "Orion.Nodes") ? "MultiNodeCustomPoller" : "MultiInterfaceCustomPoller";
            }
            else
            {
                chartName = chartNameFromProperties;
            }

            if (!autoHide || !String.IsNullOrEmpty(selectedEntities))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string> { 
					{"SelectedEntities", selectedEntities},
					{"ChartEntity", Resource.Properties["EntityName"]},
					{"ShowSum", Resource.Properties["ShowSum"]},
					{"DataColumnName", Resource.Properties["DataColumnName"]},
					{"FilterEntities", Resource.Properties["FilterEntities"]},
					{"TopXX", Resource.Properties["TopXX"]},
                    {"ResourceID", Resource.ID.ToString()},
                    {"ShowTrend", "False"}

				};

                CreateChart(null, Request.QueryString["NetObject"], chartName, parameters, chartPlaceHolder);
                Wrapper.SetUpDrDownMenu(chartName, Resource, string.Empty, parameters);
            }
            else
            {
                Visible = false;
            }
        }       
    }

    private bool LoadProperty<T>(StringDictionary dict, string propertyName, out T property)
        where T : IConvertible
    {
        property = default(T);

        if (dict != null)
        {
            if (String.IsNullOrEmpty(dict[propertyName]))
                return false;

            try
            {
                property = (T)Convert.ChangeType(dict[propertyName], typeof(T));
            }
            catch
            {
                return false;
            }

            return true;
        }

        return false;
    }

    private bool CheckProperties(StringDictionary props)
    {
        if (!LoadProperty<string>(props, "EntityName", out entityName))
            return false;

        if (!LoadProperty<bool>(props, "ManualSelect", out manualSelection))
            return false;

        if (!LoadProperty<string>(props, "CustomPollerID", out customPollerID))
            return false;

        LoadProperty<string>(props, "TopXX", out topXX);

        if (manualSelection)
        {
            if (!LoadProperty<string>(props, "SelectedEntities", out selectedEntities))
                return false;

            if (!LoadProperty<bool>(props, "FilterEntities", out filterEntities))
                return false;
        }
        else
        {
            if (String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                return false;
        }

        if (String.IsNullOrEmpty(props["ShowSum"]))
            return false;

        string hide;
        if (LoadProperty<string>(props, "AutoHide", out hide))
        {
            autoHide = (hide != "0");
        }

        if (!LoadProperty<string>(props, "ChartName", out chartNameFromProperties))
        {
            chartNameFromProperties = null;
        }
        

        return true;
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.NPMWebContent.NPMWEBCODE_AK0_55;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceMultipleUnDPsChart"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NPM/Controls/EditResourceControls/EditUnDPMultiSourceCharts.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
