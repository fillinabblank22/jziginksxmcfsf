using System;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.NPM.Web.Gauge.V1;

public partial class CustomPollerLinearGauge : CustomPollerGauge<INodeProvider>
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!isError())
		{
			SetCustomGaugeData(gaugePlaceHolder1,
				"/Orion/Charts/CustomChart.aspx?chartName=CustomPollerChart_Node&rpCustomPollerID=" + Resource.Properties["CustomPollerID"] + "&NetObject=" + GetCurrentNetObject.Node.NetObjectID, 
				GetCurrentNetObject.Node.NetObjectID, "CustomPoller", "Linear");
		}
	}

	public override GaugeType GaugeType { get{return GaugeType.Linear;}}

	public override string EditPageName { get { return "CustomOIDLinearGaugeEdit"; } }
	
	public override Control GetSourceControl() { return Source; }

	public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

    public override string GetErrorText() { return String.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_45, String.Format("<a href=\"{0} \">", EditURL),"</a>"); }

	protected override string DefaultTitle { get { return Resources.NPMWebContent.NPMWEBCODE_VB0_46; } }

	public override string HelpLinkFragment { get { return "OrionPHResourceUniversalDevicePollerGauge"; } }

	public override int CurrentNodeID { get { return GetCurrentNetObject.Node.NodeID; } }
}
