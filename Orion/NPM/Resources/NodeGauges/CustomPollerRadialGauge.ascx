<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollerRadialGauge.ascx.cs" Inherits="CustomPollerRadialGauge" %>
<%@ Register TagPrefix="npm" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>
<link rel="stylesheet" type="text/css" href="/Orion/styles/GaugeStyle.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
	        <tr>
		        <td>
			        <a href="/Orion/Charts/CustomChart.aspx?chartName=CustomPollerChart_Node&CustomPollerID=<%=Resource.Properties["CustomPollerID"] %>&NetObject=<%=GetCurrentNetObject.Node.NetObjectID%>&Period=Today" target="_blank">
			            <asp:PlaceHolder runat="server" ID="gaugePlaceHolder1" />
			        </a>
	            </td>
	        </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
</orion:resourceWrapper>