using System;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class NodeCharts_CustomPollerMinMaxAvgChart : CustomPollerGraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

        //string netObjectID = string.Empty;

        //if (!string.IsNullOrEmpty(Request.Params["netobject"]))
        //{
        //    NetObject netObject = NetObjectFactory.Create(Request.Params["netobject"]);
        //    if (netObject != null)
        //    {
        //        netObjectID = "N:" + netObject["NodeID"].ToString();
        //    }
        //}
        Node node = this.GetInterfaceInstance<INodeProvider>().Node;
        string netObjectID = node != null ? string.Format("N:{0}", node.NodeID) : string.Empty;


		CreateChart("CustomOIDMinMaxAvg", netObjectID, chartPlaceHolder);
		if (!string.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
		{
			Wrapper.SetDrDownMenuParameters("CustomOIDMinMaxAvg", Resource, "N");
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_TM0_19; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionPHResourceUniversalDevicePollerMinMaxAvgChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override string EditURL
	{
		get
		{
			string url = string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}", this.Resource.ID);

			if (!string.IsNullOrEmpty(Request["NetObject"]))
				url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
			if (Page is OrionView)
				url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);

			return (url);
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NPM/Controls/EditResourceControls/EditMinMaxUndpCharts.ascx";
		}
	}
}
