﻿using System;
using System.Collections.Generic;
using SolarWinds.NPM.Web;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

public partial class Orion_NPM_Resources_Multicast_MulticastGroupDetails : MulticastResourceBase
{
    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_PD0_1; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof (IMulticastGroupProvider); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceMulticastGroupDetails"; }
    }
}