﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.NPM.Web;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

public partial class Orion_NPM_Resources_MulticastRouting_MulticastNextRouters : MulticastRoutingTabularResourceBase
{
    protected override string DefaultTitle
    {
        get { return NPMWebContent.NPMWEBCODE_ZB0_3; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof (IMulticastGroupProvider); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceMulticastDownstreamRouters"; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        Visible = NodeId.HasValue;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        HandleInit(WrapperControl);
    }

    protected override string ResourceScriptServiceMethod
    {
        get { return "GetMulticastDownstreamRouters"; }
    }
}