﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

public partial class Orion_NPM_Resources_MulticastRouting_MulticastRoutingChart : StandardChartResource, IResourceIsInternal
{
    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override string DefaultTitle
    {
        get { return NPMWebContent.NPMWEBCODE_ZB0_2; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(INodeProvider); }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            return new[] { nodeProvider.Node.NodeID.ToString(CultureInfo.InvariantCulture) };
        }

        return new string[0];
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "N"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", ""); }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}