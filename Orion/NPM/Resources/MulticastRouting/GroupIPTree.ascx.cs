﻿using System;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NPM_Resources_MulticastRouting_GroupIPTree : MulticastResourceBase
{
    protected override void OnInit(EventArgs e)
    {
        // resource visible only when there are some multicast groups in database
        // join Orion.Nodes because of limitations
        const string swql = @"SELECT COUNT(g.MulticastGroupID) AS RowCount 
FROM Orion.NPM.MulticastRouting.Groups g
JOIN Orion.NPM.MulticastRouting.GroupNodes gn ON g.MulticastGroupID = gn.MulticastGroupID
JOIN Orion.Nodes n ON n.NodeID = gn.NodeID
JOIN Orion.Pollers p ON n.NodeID = p.NetObjectID
WHERE p.NetObjectType = 'N' AND p.Enabled=1 AND p.PollerType LIKE 'N.Multicast%'";
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var result = swis.Query(swql);
            var rowCount = Convert.ToInt32(result.Rows[0][0]);
            this.Visible = rowCount > 0;
        }

        // Multicast Groups Setup button visibility
        if (new FeatureManager().GetMaxElementCount(WellKnownElementTypes.Nodes) > 0)
        {
            Wrapper.ManageButtonText = Resources.NPMWebContent.NPMWEBCODE_JF0_32;
            Wrapper.ManageButtonTarget = "~/Orion/NPM/Admin/MulticastRouting/MulticastGroupManager.aspx";
            Wrapper.ShowManageButton = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin;
        }
        else
        {
            Wrapper.Visible = false;
        }

        base.OnInit(e);
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_JF0_31; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceMulticastGroupsSummary"; }
    }
}