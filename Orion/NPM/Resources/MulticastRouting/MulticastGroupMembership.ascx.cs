﻿using System;
using System.Collections.Generic;
using SolarWinds.NPM.Web;
using SolarWinds.NPM.Web.MulticastRouting;

public partial class Orion_NPM_Resources_MulticastRouting_MulticastGroupMembership : MulticastRoutingTabularResourceBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        HandleInit(WrapperControl);

        // visible only if a node is available
        this.Visible = NodeId.HasValue;
    }

    // Group Membership = all multicast groups on given node
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(IMulticastGroupProvider); }
    }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", "OrionPHResourceMulticastGroupMembership"); }
    }

    protected override string ResourceScriptServiceMethod
    {
        get { return "GetMulticastGroupMembership"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_PD0_22; }
    }
}