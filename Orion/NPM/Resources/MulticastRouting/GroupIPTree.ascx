﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupIPTree.ascx.cs" Inherits="Orion_NPM_Resources_MulticastRouting_GroupIPTree" %>
<orion:Include ID="Include1" runat="server" File="NPM/styles/groupNames.css"  />
<orion:Include ID="Include5" runat="server" File="OrionCore.js" SpecificOrder="2" />
<orion:Include ID="Include2" runat="server" File="NPM/js/GroupIPTree.js" SpecificOrder="3" />

<asp:ScriptManagerProxy id="GroupIPTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/NPM/Services/MulticastRouting.asmx" />
	</Services>
</asp:ScriptManagerProxy>


<orion:resourceWrapper runat="server" ID="Wrapper" >
    <Content>
        <div class="groupNamesOffPage">
            <div id="groupNamesDialog" runat="server" >

                <div runat="server" id="groupNamesGrid"></div>

                <div class="sw-btn-bar" style="white-space: nowrap; float: right;">
                    <orion:LocalizableButtonLink ID="btnDialogOk" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources : NPMWebContent, NPMWEBDATA_JF0_35 %>"  />
                    <orion:LocalizableButtonLink ID="btnDialogCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" ToolTip="<%$ Resources : NPMWebContent, NPMWEBDATA_JF0_34 %>" />
                </div>

            </div>
        </div>
	    <script type="text/javascript">
	        //<![CDATA[

	        var GroupIpTreeController = (function () {
	            var names = null;
	            var groupNames = function () {
	                if (names == null) {
	                    names = new SW.NPM.Resources.GroupNames({
	                        renderTo: '<%= groupNamesGrid.ClientID %>',
	                        onUpdated : refresh,
	                        dialog: {
	                            renderTo: '<%= groupNamesDialog.ClientID %>',
	                            title: '<%= Resources.NPMWebContent.NPMWEBCODE_JF0_32 %>',
	                            width: 500,
                                height: 300,
	                            btnOk: '#<%= btnDialogOk.ClientID %>',
	                            btnCancel: '#<%= btnDialogCancel.ClientID %>'
	                        }
	                    });
	                }
	                names.ShowInDialog();
	            };

	            var refresh = function () {
	                GroupIPTree.LoadRoot($('#<%=this.groupTree.ClientID%>'), '<%=this.Resource.ID %>', '', '<%= this.ViewLimitationId %>');
                 };

	            $(function () {
	            
	               
                         SW.Core.View.AddOnRefresh(refresh, '<%= groupTree.ClientID %>');
                         refresh();
                 });

	            return {
	                groupNames: groupNames
	            };

	        })();
            
	       
	        //]]>
	    </script>
	
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="groupTree" runat="server">
            <tr class="ReportHeader">
                <td colspan="2"><%=Resources.NPMWebContent.NPMWEBDATA_JF0_32%></td>
                <td width="40%"><%=Resources.NPMWebContent.NPMWEBDATA_JF0_33%></td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
