﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Web;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_NPM_Resources_MulticastRouting_MulticastGroupMembers : MulticastRoutingTabularResourceBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        HandleInit(WrapperControl);
    }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", "OrionPHResourceMulticastGroupMembers"); }
    }

    protected override string ResourceScriptServiceMethod
    {
        get { return "GetMulticastGroupMembers"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_PD0_21; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(IMulticastGroupProvider); }
    }
}