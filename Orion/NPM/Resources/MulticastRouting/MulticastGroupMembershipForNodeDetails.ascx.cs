﻿using System;
using System.Collections.Generic;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.NPM.Web;

/// <summary>
/// This resource is duplicated because we need it to place it on the NodeDetails view.
/// We can't use RequiredInterfaces because all of the types needs to be provided by the view.
/// We can't use SupportedInterfaces because IMulticastGroup provider doesn't inherit INodeProvider
/// (as IInterfaceProvider does) so we don't have information about node.
/// </summary>
public partial class Orion_NPM_Resources_MulticastRouting_MulticastGroupMembershipForNodeDetails : MulticastRoutingTabularResourceBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        HandleInit(WrapperControl);
    }

    // Group Membership = all multicast groups on given node
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(INodeProvider); }
    }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", "OrionPHResourceMulticastGroupMembership"); }
    }

    protected override string ResourceScriptServiceMethod
    {
        get { return "GetMulticastGroupMembership"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_PD0_22; }
    }
}