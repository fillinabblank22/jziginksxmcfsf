﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

public partial class Orion_NPM_Resources_MulticastRouting_MulticastNodeInterfaces : MulticastRoutingTabularResourceBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        HandleInit(WrapperControl);
    }

    protected override string DefaultTitle
    {
        get { return NPMWebContent.NPMWEBCODE_PD0_20; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(INodeProvider); }
    }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", "OrionPHResourceMulticastInterfaces"); }
    }

    protected override string ResourceScriptServiceMethod
    {
        get { return "GetMulticastNodeInterfaces"; }
    }
}