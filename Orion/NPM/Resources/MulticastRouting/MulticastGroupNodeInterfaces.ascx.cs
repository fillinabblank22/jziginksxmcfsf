﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.NPM.Web;
using SolarWinds.NPM.Web.MulticastRouting;

public partial class Orion_NPM_Resources_MulticastRouting_MulticastGroupNodeInterfaces : MulticastRoutingTabularResourceBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        HandleInit(WrapperControl);

        // visible only if a node is available
        this.Visible = NodeId.HasValue;
    }

    protected override string DefaultTitle
    {
        get { return NPMWebContent.NPMWEBCODE_PD0_20; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(IMulticastGroupProvider); }
    }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", "OrionPHResourceMulticastInterfaces"); }
    }

    protected override string ResourceScriptServiceMethod
    {
        get { return "GetMulticastGroupNodeInterfaces"; }
    }
}