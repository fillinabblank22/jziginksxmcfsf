﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="MulticastGroupDetails.ascx.cs" Inherits="Orion_NPM_Resources_Multicast_MulticastGroupDetails" %>
<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>

        <script type="text/javascript">
            $(function () {
                var processResults = function (result) {
                    var source = $('#template_multicastGroupDetail').html();
                    var newHtml = _.template(source, result);
                    $('#<%= resourceContent.ClientID %>').html(newHtml);
                };

                var processFailure = function (error) {
                    // error message is added to DOM, but hidden for now..
                    $('#<%= resourceContent.ClientID %>').addClass("Error").text(error).hide();
                };

                var refresh = function () {
                    SW.Core.Services.callWebService(
                        "/Orion/NPM/Services/MulticastRouting.asmx",
                        "GetMulticastGroupDetails",
                        { groupId: '<%= GroupId %>', nodeId: '<%= NodeId %>', viewLimitationId: <%= ViewLimitationId %> },
                        processResults,
                        processFailure
                    );
                };

                SW.Core.View.AddOnRefresh(refresh, '<%= resourceContent.ClientID %>');
                refresh();
            });
        </script>

        <script id="template_multicastGroupDetail" type="text/x-template">
            <table cellspacing="0" cellpadding="2">
                {# if(Status) { #}
                    <tr>
                        <td><strong><%= Resources.NPMWebContent.NPMWEBCODE_VT0_14 %></strong></td>
                        <td>{{ Status }}</td>
                    </tr>
                {# } #}
                {# if(IPAddress) { #}
                    <tr>
                        <td><strong><%= Resources.NPMWebContent.NPMWEBCODE_PD0_24 %></strong></td>
                        <td>{{ IPAddress }}</td>
                    </tr>
                {# } #}
                {# if(Name) { #}
                    <tr>
                        <td><strong><%= Resources.NPMWebContent.NPMWEBDATA_VB0_116 %></strong></td>
                        <td>{{ Name }}</td>
                    </tr>
                {# } #}
                {# if(Flags) { #}
                    <tr>
                        <td><strong><%= Resources.NPMWebContent.NPMWEBCODE_PD0_2 %></strong></td>
                        <td>{{ Flags }}</td>
                    </tr>
                {# } #}
                {# if(Uptime) { #}
                    <tr>
                        <td><strong><%= Resources.NPMWebContent.NPMWEBCODE_PD0_3 %></strong></td>
                        <td>{{ Uptime }}</td>
                    </tr>
                {# } #}
                {# if(ExpiresIn) { #}
                    <tr>
                        <td><strong><%= Resources.NPMWebContent.NPMWEBCODE_PD0_4 %></strong></td>
                        <td>{{ ExpiresIn }}</td>
                    </tr>
                {# } #}
                {# if(Traffic !== '' && Traffic > -1) { #}
                    <tr>
                        <td><strong><%= Resources.NPMWebContent.NPMWEBCODE_PD0_23 %></strong></td>
                    {# if(Traffic==0) { #}
                        {# if(IsTrafficProblem) { #}
                        <td class="Warning">{{ Traffic }} <%= Resources.NPMWebContent.NPMWEBCODE_VB0_20 %></td>
                        {# } else { #}
                        <td>&lt; 1 <%= Resources.NPMWebContent.NPMWEBCODE_VB0_20 %></td>
                        {# } #}
                    {# } else { #}
                        <td>{{ Traffic }} <%= Resources.NPMWebContent.NPMWEBCODE_VB0_20 %></td>
                    {# } #}
                    </tr>
                {# } #}
            </table>
        </script>

        <span id="resourceContent" runat="server"></span>

    </Content>
</orion:resourceWrapper>