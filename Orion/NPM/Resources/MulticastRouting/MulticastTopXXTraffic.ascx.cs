﻿using System;
using SolarWinds.NPM.Web.MulticastRouting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NPM_Resources_MulticastRouting_MulticastTopXXTraffic : MulticastRoutingTabularResourceBase, IResourceIsInternal
{
    protected string MaxRecords
    {
        get { return GetStringValue("MaxRecords", "10"); }
    }

    protected override string DefaultTitle
    {
        get { return string.Format(Resources.NPMWebContent.NPMWEBCODE_PD0_18, "10"); }
    }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", ""); }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        HandleInit(WrapperControl);
    }

    protected override void EnsureVisibility()
    {
        // resource visible only when there are some data in multicast routing table
        // join Orion.Nodes because of limitations
        const string swql = @"SELECT COUNT(rt.MulticastID) AS RowCount 
FROM Orion.NPM.MulticastRouting.RoutingTable rt
JOIN Orion.NPM.MulticastRouting.GroupNodes gn ON rt.MulticastGroupNodeID = gn.MulticastGroupNodeID
JOIN Orion.Nodes n ON n.NodeID = gn.NodeID
JOIN Orion.Pollers p ON n.NodeID = p.NetObjectID
WHERE p.NetObjectType = 'N' AND p.Enabled=1 AND p.PollerType LIKE 'N.Multicast%'";
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var result = swis.Query(swql);
            var rowCount = Convert.ToInt32(result.Rows[0][0]);
            this.Visible = rowCount > 0;
        }
    }

    protected override string ResourceScriptServiceMethod
    {
        get { return "GetMulticastTopXXTraffic"; }
    }

    protected override dynamic GenerateDisplayDetails()
    {
        var rv = base.GenerateDisplayDetails();
        rv.TopXX = MaxRecords;
        return rv;
    }
}