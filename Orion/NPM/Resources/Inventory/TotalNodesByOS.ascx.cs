﻿using System;
using System.Data;

using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_Inventory_TotalNodesByOS : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	#region properties

	// overriden DefaultTitle
	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_TM0_20; }
	}

	// overriden HelpLink
	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceNumberNodesByIOSOS";
		}
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

	// overriden Edit URL
	public override string EditURL
	{
		get
		{
			string url = string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}&HideInterfaceFilter=True", this.Resource.ID);

			if (!string.IsNullOrEmpty(Request["NetObject"]))
				url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
			if (Page is OrionView)
				url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);

			return (url);
		}
	}

	#endregion

	protected void Page_Load(object sender, EventArgs e)
	{
		string filter = Resource.Properties["Filter"];

		int nodeID = -1;
		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
		if (nodeProvider != null)
			nodeID = nodeProvider.Node.NodeID;
		else
			nodeID = CommonWebHelper.TryToGetNodeIDFromRequest(Request.QueryString["NetObject"]);

		if (nodeID > 0)
			if (String.IsNullOrEmpty(filter))
				filter = String.Format("Nodes.NodeID={0}", nodeID);
			else
				filter = String.Format("{0} AND Nodes.NodeID={1}", filter, nodeID);

		DataTable table = null;

		try
		{
			table = SqlDAL.GetTotalNodesByOS(filter);
		}
		catch
		{
			this.SQLErrorPanel.Visible = true;
			return;
		}

		if (table == null || table.Rows.Count == 0)
		{
			return;
		}

		foreach (DataRow r in table.Rows)
			if (String.IsNullOrEmpty((string)r["IOSVersion"])) r["IOSVersion"] = Resources.NPMWebContent.NPMWEBCODE_AK0_2;

		// bind data to repeater
		this.RowRepeater.DataSource = table;
		this.RowRepeater.DataBind();
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
