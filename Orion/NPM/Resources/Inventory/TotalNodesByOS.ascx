﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TotalNodesByOS.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Inventory_TotalNodesByOS" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="RowRepeater">
            <HeaderTemplate>
                <table style="width: 100%" class="NeedsZebraStripes" cellspacing="0">
                    <tr class="HeaderRow">
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_JP0_2%></td>
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_JP0_3%></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property" style="width: 20px; text-align: center;">
                        <%#Eval("Total")%>
                    </td>
                    <td class="Property" style="text-align: left">
                        <%#Eval("IOSVersion")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>

