<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollerTopXX.ascx.cs" Inherits="CustomPollerTopXX" %>
<%@ Register TagPrefix="orion" TagName="ResourceHelpPage" Src="~/Orion/NetPerfMon/Resources/ResourceHelpPage.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    <% if (ShowHelp) { %>
        <orion:ResourceHelpPage id="resourceHelpPage" runat="server" />
    <% } %>    
    <% else if (ShowDetails) { %>
        <asp:Repeater ID="datailsTable" runat="server">
		    <HeaderTemplate>
                <table border="0" cellpadding="3" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_28%></td>
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_29%></td>
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_30%></td>
                    </tr>
		    </HeaderTemplate>
		    <ItemTemplate>
                <tr>
                    <td class="Property">
                        <a href="<%# DataBinder.Eval(Container.DataItem, "ObjectLinkTarget") %>" target="_blank">
                            <%# DataBinder.Eval(Container.DataItem, "Caption") %>
                        </a>
                    </td>
                    <td class="Property">
                        <a href="<%# DataBinder.Eval(Container.DataItem, "LinkTarget") %>" target="_blank">
                            <%# DataBinder.Eval(Container.DataItem, "DisplayCurrentStatus")%>
                        </a>
                    </td>
                    <td class="Property">
                        <%# DataBinder.Eval(Container.DataItem, "DisplayLastPolled")%>
                    </td>
                </tr>
		    </ItemTemplate>

		    <FooterTemplate>
                </table>    
		    </FooterTemplate>
	    </asp:Repeater>
    <% } %>	    	    
    </Content>
</orion:resourceWrapper>
