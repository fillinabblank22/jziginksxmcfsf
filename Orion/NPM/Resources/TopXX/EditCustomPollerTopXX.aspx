<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditCustomPollerTopXX.aspx.cs" Inherits="EditCustomPollerTopXX" Title="<%$Resources: NPMWebContent, NPMWEBDATA_VB0_52%>" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_18,CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"]))) %></h1>
       
    <div style="padding-left: 16px;">
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="false" />
        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_54%>
        <br />
        <br />
        <table class="text" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_55%></b>
                <br/>
                    <asp:DropDownList ID="customPollersDDL" runat="server" />
                <br/>
                <br/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_56%></b><br />
                <input name="MaxRecords" size="5" value="<%= Resource.Properties["MaxRecords"] %>" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br/>
                <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_57%></b><br/>
                <% if (Resource.Properties["SortOrder"] == "DESC") { %>
                <input name="SortOrder" type="radio" value="DESC" checked />
                    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_58%><br/>
                <input name="SortOrder" type="radio" value="ASC" />
                    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_59%><br/>
                <% } else { %>
                <input name="SortOrder" type="radio" value="DESC" />
                    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_58%><br/>
                <input name="SortOrder" type="radio" value="ASC" checked />
                    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_59%><br/>
                <% } %>
            </td>
        </tr>
        <tr>			
            <td colspan="2">
                <br />
                <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />                    
            </td>
        </tr>
        </table>
    </div>
</asp:Content>
