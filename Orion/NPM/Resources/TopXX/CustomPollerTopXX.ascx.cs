using System;
using System.Data;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Common;

public partial class CustomPollerTopXX : BaseResourceControl
{
    public void MyBusinessLayerExceptionHandler(Exception ex)
    {
		SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
		log.Error(ex);
    }
    
    private bool _showDetails = false;
    private bool _showHelp = false;

    private int _maxRecords = 10;

    protected bool ShowDetails
    {
        get { return _showDetails; }
    }

    protected bool ShowHelp
    {
        get { return _showHelp; }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Convert.ToInt32(this.Resource.Properties["MaxRecords"]) == 0)
		{
			Resource.Properties["MaxRecords"] = _maxRecords.ToString();
			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
		}
		else
		{
			_maxRecords = Convert.ToInt32(Resource.Properties["MaxRecords"]);
		}

		if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
		{
			Resource.Title = Resource.Properties["Title"];
		}
		else
		{
			Resource.Title = string.Empty;
		}

		if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
		{
			Resource.SubTitle = Resource.Properties["SubTitle"];
		}
		else
		{
			Resource.SubTitle = string.Empty;
		}


		resourceHelpPage.EditURL = EditURL;
		if (String.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
		{
			if (string.IsNullOrEmpty(Resource.Title))
			{
				Resource.Title = DefaultTitle;
				Resource.Title = Resource.Title.Replace("XX", _maxRecords.ToString());
			}
			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
			_showHelp = true;
			return;
		}

		Guid customPollerID = new Guid(Resource.Properties["CustomPollerID"]);
		using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
		{
            CustomPoller customPoller;
            try
            {
                customPoller = proxy.Api.GetCustomPoller(customPollerID);
            }
            catch(Exception ex)
            {
                MyBusinessLayerExceptionHandler(ex);
                throw;
            }

			if (String.IsNullOrEmpty(Resource.Properties["SortOrder"]))
			{
				Resource.Properties["SortOrder"] = "ASC";
			}

			if (customPoller != null)
			{
				if (string.IsNullOrEmpty(Resource.Title))
				{
					Resource.Title = string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_3, customPoller.UniqueName, _maxRecords.ToString());
				}
			}

			if (string.IsNullOrEmpty(Resource.Title))
			{
				Resource.Title = DefaultTitle;
				Resource.Title = Resource.Title.Replace("XX", _maxRecords.ToString());
			}

			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
			_showDetails = true;

			if (customPoller != null)
			{
                DataTable data;
                try
                {
                    data = proxy.Api.GetCustomPollerTopXXResourceData(_maxRecords, customPoller, Resource.Properties["SortOrder"], Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    MyBusinessLayerExceptionHandler(ex);
                    throw;
                }
				datailsTable.DataSource = FormatData(customPoller, data);
				datailsTable.DataBind();
			}
			else
			{
				_showHelp = true;
				return;
			}
		}
	}

	public DataTable FormatData(CustomPoller customPoller, DataTable statusSummaryResourceDataTable)
	{
		statusSummaryResourceDataTable.Columns.AddRange(new DataColumn[] {
            new DataColumn("LinkTarget", typeof(String)),
            new DataColumn("Caption", typeof(String)),
            new DataColumn("ObjectLinkTarget", typeof(String)),
            new DataColumn("DisplayCurrentStatus", typeof(String)),
            new DataColumn("DisplayLastPolled", typeof(String))
            });

		foreach (DataRow row in statusSummaryResourceDataTable.Rows)
		{
			String unitSuffix = customPoller.UnitAbbv(true);
			String objectLinkTarget = null;

			String netObjectPrefix = Convert.ToString(row["NetObjectPrefix"]);
			String linkTarget = @"/Orion/Charts/CustomChart.aspx?ChartName=CustomPollerChart_Node";
			String caption = Convert.ToString(row["NodeCaption"]);

			if (netObjectPrefix == "N")
			{
				linkTarget += "&rpCustomPollerID=" + customPoller.ID.ToString() + "&SubsetColor=FF0000" + "&NetObject=" + netObjectPrefix + ":" + Convert.ToString(row["NodeID"]);
				objectLinkTarget = "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + Convert.ToString(row["NodeID"]);
			}
			else if (netObjectPrefix == "I")
			{
				linkTarget += "&rpCustomPollerID=" + customPoller.ID.ToString() + "&SubsetColor=FF0000" + "&NetObject=" + netObjectPrefix + ":" + Convert.ToString(row["InterfaceID"]);
				objectLinkTarget = "/Orion/NPM/InterfaceDetails.aspx?NetObject=I:" + Convert.ToString(row["InterfaceID"]);
				caption += " - " + Convert.ToString(row["InterfaceCaption"]);
			}

			if (!String.IsNullOrEmpty(Convert.ToString(row["CurrentStatus"])))
			{
				if (Convert.ToString(row["PollerType"]) == "S")
				{
					row["DisplayCurrentStatus"] = Convert.ToString(row["CurrentStatus"]) + unitSuffix;
				}
				else
				{
					double currentStatus;
					if (Double.TryParse(row["CurrentStatus"].ToString(),System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out currentStatus))
					{
						row["DisplayCurrentStatus"] = Utils.ConvertToMB(currentStatus, "", false) + unitSuffix;
					}
					else
					{
						row["DisplayCurrentStatus"] = Convert.ToString(row["CurrentStatus"]) + unitSuffix;
					}
				}
			}
			else
			{
				row["DisplayCurrentStatus"] = "&nbsp;";
			}

			if (!String.IsNullOrEmpty(Convert.ToString(row["LastPolled"])))
			{
				// SQLResource.GetProperty("LastPolled as NextPoll")
				DateTime dateTime = Convert.ToDateTime(row["LastPolled"]).ToLocalTime();
				if (dateTime.ToShortDateString() != DateTime.Now.ToShortDateString())
				{
					row["DisplayLastPolled"] = dateTime.ToShortDateString() + " " + dateTime.ToShortTimeString();
				}
				else
				{
					row["DisplayLastPolled"] = dateTime.ToShortTimeString();
				}
			}
			else
			{
				row["DisplayLastPolled"] = "&nbsp;";
			}

			row["LinkTarget"] = linkTarget;
			row["Caption"] = caption;
			row["ObjectLinkTarget"] = objectLinkTarget;
		}
		return statusSummaryResourceDataTable;
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VB0_4; }
	}

	public override string EditURL
	{
		get { return string.Format("/Orion/NPM/Resources/TopXX/EditCustomPollerTopXX.aspx?ResourceID={0}&ViewID={1}&NetObject={2}", this.Resource.ID, this.Resource.View.ViewID, Page.Request.QueryString["NetObject"]); }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceUniversalDevicePollerTopXX"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
