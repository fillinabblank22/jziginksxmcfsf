using System;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.NPM.Common;

public partial class EditCustomPollerTopXX : System.Web.UI.Page
{
    public void MyBusinessLayerExceptionHandler(Exception ex)
    {
		SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
		log.Error(ex);
    }

    private ResourceInfo _resource;
    private string _netObjectID;
    private int _resourceID = 0;

    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            _resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            _resource = ResourceManager.GetResourceByID(_resourceID);
            Page.Title = string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_18, Resource.Title);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
        {
            _netObjectID = Request.QueryString["NetObject"];
        }

		if (Convert.ToInt32(Resource.Properties["MaxRecords"]) == 0)
		{
			Resource.Properties["MaxRecords"] = "10";
		}

		if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
		{
			resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
		}
		else
		{
			resourceTitleEditor.ResourceTitle = string.Empty;
		}

		if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
		{
			resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
		}
		else
		{
			resourceTitleEditor.ResourceSubTitle = string.Empty;
		}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            CustomPollers pollers;
            try
            {
                pollers = proxy.Api.GetAllCustomPollers();
            }
            catch(Exception ex)
            {
                MyBusinessLayerExceptionHandler(ex);
                throw;
            }
            foreach (CustomPoller customPoller in pollers)
            {
                ListItem listItem = new ListItem(customPoller.UniqueName);
                listItem.Value = customPoller.ID.ToString();
                if (Resource.Properties["CustomPollerID"] == customPoller.ID.ToString())
                {
                    listItem.Selected = true;
                }
                customPollersDDL.Items.Add(listItem);
            }
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        Resource.Properties["Title"] = resourceTitleEditor.ResourceTitle;
        Resource.Properties["SubTitle"] = resourceTitleEditor.ResourceSubTitle;

        Resource.Properties["MaxRecords"] = Request["MaxRecords"];
        Resource.Properties["CustomPollerID"] = customPollersDDL.SelectedValue;
        Resource.Properties["SortOrder"] = Request["SortOrder"];

        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        if (!string.IsNullOrEmpty(_netObjectID))
        {
            url = string.Format("{0}&NetObject={1}", url, _netObjectID);
        }
        Response.Redirect(url);
    }
}