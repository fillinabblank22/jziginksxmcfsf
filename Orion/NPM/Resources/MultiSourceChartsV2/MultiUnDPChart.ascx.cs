﻿using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_MultiSourceChartsV2_MultiUnDPChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    public override IEnumerable<Type>  SupportedInterfaces
    {
        get { return new[] { typeof(IInterfaceProvider) }; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        yield return Request["NetObject"] ?? string.Empty;
    }

    protected override string NetObjectPrefix
    {
        get { return "N"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBDATA_ZB0_20; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}