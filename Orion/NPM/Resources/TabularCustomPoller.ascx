<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TabularCustomPoller.ascx.cs" Inherits="TabularCustomPoller" %>
<%@ Register TagPrefix="orion" TagName="ResourceHelpPage" Src="~/Orion/NetPerfMon/Resources/ResourceHelpPage.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    <% if (ShowHelp)
       { %>
        <orion:ResourceHelpPage id="resourceHelpPage" runat="server" />
    <% } %>
    <% else { %>   
     <div>
        <asp:GridView ID="gridView" runat="server" BorderWidth="0" CellPadding="0" CellSpacing="0" GridLines="None" CssClass="NeedsZebraStripes" UseAccessibleHeader="false" OnDataBound="gridView_DataBound">
            <HeaderStyle CssClass="ReportHeader"/>
        </asp:GridView>
     </div>
	<%} %>
    </Content>
</orion:resourceWrapper>