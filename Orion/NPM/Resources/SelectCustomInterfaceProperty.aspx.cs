using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_SelectCustomInterfaceProperty : System.Web.UI.Page
{
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_18, this.Resource.Title);
            
            // set up the title editor control
            this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
            this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;
        }

		lbxCustomProps.DataSource = Interface.GetCustomPropertyNames(true);
        lbxCustomProps.DataBind();

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
		if (!(resourceTitleEditor.ResourceTitle.Equals(Resource.Title)))
		{
            if (!String.IsNullOrEmpty(resourceTitleEditor.ResourceTitle))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
            }else
            {
                var resourceControl = (BaseResourceControl) LoadControl(Resource.File);
                Resource.Title = resourceControl.Title;
            }
		    SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
		}

		this.Resource.Properties["CustomProperty"] = this.lbxCustomProps.SelectedValue;
        Response.Redirect(string.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", Resource.View.ViewID, Request.QueryString["NetObject"]));
    }
}
