﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_VSANChartsV2_VSANSummaryChart : StandardChartResource, IResourceIsInternal
{
	private static readonly Log _log = new Log();

	protected void Page_Init(object sender, EventArgs e)
	{
		HandleInit(WrapperContents);
	}

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	protected override string NetObjectPrefix
	{
		get { return "VSAN_SUMMARY"; }
	}

	protected override string DefaultTitle
	{
	  get { return notAvailable; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { yield break; }
	}


	protected override IEnumerable<string> GetElementIdsForChart()
	{
		using(var swql = InformationServiceProxy.CreateV3())
		{
			var r = swql.Query(@"SELECT DISTINCT vs.ID, vs.Name
FROM Orion.NPM.VSANs vs
INNER JOIN Orion.NPM.FCPorts fp ON fp.VsanID=vs.ID
INNER JOIN Orion.NPM.FCUnits fu ON fu.ID=fp.UnitID
");
			return r.Rows.Cast<DataRow>().Select(x => x[0].ToString());
		}
	}

	public bool IsInternal
	{
		get { return true; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}

