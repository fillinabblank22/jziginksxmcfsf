﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_VSANChartsV2_VSANChart : StandardChartResource, IResourceIsInternal
{
	private static readonly Log _log = new Log();

	protected void Page_Init(object sender, EventArgs e)
	{
		HandleInit(WrapperContents);
	}

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	protected override string NetObjectPrefix
	{
		get { return "NVS"; }
	}

	protected override IEnumerable<string> GetElementIdsForChart()
	{
		var vsanProvider = GetInterfaceInstance<IVSANProvider>();
		if (vsanProvider != null)
		{
			return new[] { vsanProvider.VSAN.ID.ToString(CultureInfo.InvariantCulture) };
		}

		return new string[0];
	}

	protected override string DefaultTitle
	{
		get { return notAvailable; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(IVSANProvider) }; }
	}


	public bool IsInternal
	{
		get { return true; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}

