using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NPM.Common.Models;
using System.Text;


public partial class Orion_NPM_Resources_FiberChannel_SensorDetails : ResourceControl
{
    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var node = GetInterfaceInstance<INodeProvider>().Node;
        
        const string selectPollers = @"SELECT Enabled FROM Orion.Pollers WHERE NetObject = @NetObjectID AND PollerType LIKE '%FibreChannel%'";
        
        const string selectUnits = @"SELECT ID, NodeID, ConnUnitID, Type, Product, Name, SerialNumber, PortCount, State, Status 
FROM Orion.NPM.FCUnits 
WHERE NodeID = @NodeID";

        const string selectSensors = @"SELECT UnitID, Index, Name, Type, Characteristic, Message, Status
FROM Orion.NPM.FCSensors 
WHERE NodeID = @NodeID";

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var row =
                swis.Query(selectPollers,
                           new Dictionary<string, object> {{"NetObjectID", node.NetObjectID}}
                    );

            // hide if poller doesn't exist or is disabled
            if (row == null || row.Rows.Count == 0 || !(bool) row.Rows[0]["Enabled"])
            {
                this.Visible = false;
                return;
            }

            var units = GetTableByNodeID(swis, selectUnits, node.NodeID);
            if (units == null || units.Rows.Count == 0)
            {
                // show nothing when there are no connectivity units
                this.Visible = false;
                return;
            }

            var sensors = GetTableByNodeID(swis, selectSensors, node.NodeID);
            if (sensors == null || sensors.Rows.Count == 0)
            {
                // show nothing when there are no sensors
                this.Visible = false;
                return;
            }

            // bind data
            connUnitStatusTable.DataSource = JoinTables(units.AsEnumerable(), sensors.AsEnumerable());
            connUnitStatusTable.DataBind();
        }
    }

    private DataTable GetTableByNodeID(InformationServiceProxy proxy, string select, int nodeID)
    {
        return proxy.Query(select,
                        new Dictionary<string, object> { { "NodeID", nodeID } }
                        );
    }

    private object JoinTables(EnumerableRowCollection<DataRow> units, EnumerableRowCollection<DataRow> sensors)
    {
        return from u in units
               join s in sensors on u["ID"] equals s["UnitID"]
               select new
               {
                   Icon = SensorIcon((FCSensorCharacteristics)s["Characteristic"], (FCSensorType)s["Type"], (FCSensorStatus)s["Status"]),
                   Name = s["Name"],
                   Type = GetLocalizedSensorProperty("FCSensorType", ((FCSensorType)s["Type"]).ToString()),
                   Characteristics = GetLocalizedSensorProperty("FCSensorCharacteristics", ((FCSensorCharacteristics)s["Characteristic"]).ToString()),
                   ConnUnitID = u["ConnUnitID"],
                   Status = GetLocalizedSensorProperty("FCSensorStatus", ((FCSensorStatus)s["Status"]).ToString()),
                   Message = s["Message"]
               };
    }

    private string GetLocalizedSensorProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    private string SensorIcon(FCSensorCharacteristics characteristics, FCSensorType type, FCSensorStatus status)
    {
        var icon = new StringBuilder("/Orion/NPM/Images/sensor_icons/");

        switch (characteristics)
        {
            case FCSensorCharacteristics.Temperature:
                icon.Append("temp");
                break;
            
            case FCSensorCharacteristics.Airflow:
                icon.Append("fan");
                break;

            case FCSensorCharacteristics.Power:
                icon.Append("power");
                break;

            case FCSensorCharacteristics.Other:
                switch (type)
                {
                    case FCSensorType.PowerSupply:
                        icon.Append("power");
                        break;

                    default:
                        return "&nbsp;";
                }
                break;

            default:
                return "&nbsp;";
        }

        switch (status)
        {
            case FCSensorStatus.Ok:
                icon.Append("-up");
                break;
            
            case FCSensorStatus.Warning:
                icon.Append("-warning");
                break;

            case FCSensorStatus.Failed:
                icon.Append("-down");
                break;

            default:
                return "&nbsp;";
        }

        return String.Format("<img src=\"{0}.gif\" />", icon.ToString());
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_175; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceSensorDetails"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
