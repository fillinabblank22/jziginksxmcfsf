<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SensorDetails.ascx.cs" Inherits="Orion_NPM_Resources_FiberChannel_SensorDetails" %>
<orion:Include ID="Include1" runat="server" File="../NPM/styles/FiberChannel.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>

    <table border="0" cellpadding="3" cellspacing="0" width="100%" class="BorderlessTable NeedsZebraStripes">
        <thead style="text-align:left;">
            <tr>
	            <td class="ReportHeader" colspan="2"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_308%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_309%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_310%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_300%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_231%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_311%></td>
            </tr>
        </thead>
        <asp:Repeater runat="server" ID="connUnitStatusTable">
        <ItemTemplate>
	        <tr>
		        <td class="PropertyIcon"><%# Eval("Icon") %></td>
		        <td class="Property"><%# Eval("Name") %></td>
		        <td class="Property"><%# Eval("Type")%></td>
		        <td class="Property"><%# Eval("Characteristics")%></td>
		        <td class="Property"><%# Eval("ConnUnitID") %></td>
		        <td class="Property"><%# Eval("Status")%></td>
		        <td class="Property"><%# Eval("Message")%></td>
	        </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
	        <tr class="AlternatingRow">
		        <td class="PropertyIcon"><%# Eval("Icon") %></td>
		        <td class="Property"><%# Eval("Name") %></td>
		        <td class="Property"><%# Eval("Type")%></td>
		        <td class="Property"><%# Eval("Characteristics")%></td>
		        <td class="Property"><%# Eval("ConnUnitID") %></td>
		        <td class="Property"><%# Eval("Status")%></td>
		        <td class="Property"><%# Eval("Message")%></td>
	        </tr>
        </AlternatingItemTemplate>
        </asp:Repeater>
    </table>    
    
</Content>
</orion:resourceWrapper>