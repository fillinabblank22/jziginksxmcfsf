<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FiberChannelDetails.ascx.cs" Inherits="Orion_NPM_Resources_FiberChannel_FiberChannelDetails" %>

<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>

    <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailsTable" class="NeedsZebraStripes">
        <tr>
          <td class="Property" width="10">&nbsp;</td>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_318%></td>
          <td class="Property">&nbsp;</td>
          <td class="Property" colspan="2"><asp:Label runat="server" ID="physicalPort" />&nbsp;</td>
        </tr>
        <tr>
          <td class="Property" width="10">&nbsp;</td>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_317%></td>
          <td class="Property">&nbsp;</td>
          <td class="Property" colspan="2"><asp:Label runat="server" ID="portWWN" />&nbsp;</td>
        </tr>
        <tr>
          <td class="Property" width="10">&nbsp;</td>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_319%></td>
          <td class="Property">&nbsp;</td>
          <td class="Property" colspan="2"><asp:Label runat="server" ID="hardwareState" />&nbsp;</td>
        </tr>
    </table>
    
</Content>
</orion:resourceWrapper>