<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfVSANs.ascx.cs" Inherits="Orion_NPM_Resources_FiberChannel_ListOfVSANs" %>
<orion:Include runat="server" File="../NPM/styles/FiberChannel.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>

    <table border="0" cellpadding="3" cellspacing="0" width="100%" class="BorderlessTable NeedsZebraStripes">
        <thead style="text-align:left;">
            <tr>
	            <td class="ReportHeader" colspan="2"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_299%></th>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_313%></th>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_320%></th>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_321%></th>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_322%></th>
            </tr>
        </thead>
        <asp:Repeater runat="server" ID="connUnitStatusTable">
        <ItemTemplate>
	        <tr>
		        <td class="PropertyIcon"><%# Eval("Icon") %></td>
		        <td class="Property"><%# Eval("Name") %></td>
		        <td class="Property"><%# Eval("ID")%></td>
		        <td class="Property"><%# Eval("MediaType")%></td>
		        <td class="Property"><%# Eval("LoadBalancingType")%></td>
		        <td class="Property"><%# Eval("AdminState")%></td>
	        </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
	        <tr class="ZebraStripe">
		        <td class="PropertyIcon"><%# Eval("Icon") %></td>
		        <td class="Property"><%# Eval("Name") %></td>
		        <td class="Property"><%# Eval("ID")%></td>
		        <td class="Property"><%# Eval("MediaType")%></td>
		        <td class="Property"><%# Eval("LoadBalancingType")%></td>
		        <td class="Property"><%# Eval("AdminState")%></td>
	        </tr>
        </AlternatingItemTemplate>
        </asp:Repeater>
    </table>    
    
</Content>
</orion:resourceWrapper>