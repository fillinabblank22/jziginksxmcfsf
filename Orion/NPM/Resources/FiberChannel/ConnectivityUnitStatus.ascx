<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConnectivityUnitStatus.ascx.cs" Inherits="Orion_NPM_Resources_FiberChannel_ConnectivityUnitStatus" %>
<orion:Include ID="Include1" runat="server" File="../NPM/styles/FiberChannel.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>

    <table border="0" cellpadding="3" cellspacing="0" width="100%" class="BorderlessTable NeedsZebraStripes">
        <thead style="text-align:left;">
            <tr>
	            <td class="ReportHeader" colspan="2"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_299%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_300%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_301%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_302%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_303%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_304%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_305%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_306%></td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_307%></td>
            </tr>
        </thead>
        <asp:Repeater runat="server" ID="connUnitStatusTable">
        <ItemTemplate>
	        <tr>
		        <td class="PropertyIcon"><%# Eval("Icon") %></td>
		        <td class="Property"><%# Eval("Name") %></td>
		        <td class="Property"><%# Eval("ConnUnitID") %></td>
		        <td class="Property"><%# Eval("Product") %></td>
		        <td class="Property"><%# Eval("SerialNumber") %></td>
		        <td class="Property"><%# Eval("Type") %></td>
		        <td class="Property"><%# Eval("RevisionID") %></td>
		        <td class="Property"><%# Eval("RevisionDescription") %></td>
		        <td class="Property"><%# Eval("State") %></td>
		        <td class="Property"><%# Eval("Status") %></td>
	        </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
	        <tr class="AlternatingRow">
		        <td class="PropertyIcon"><%# Eval("Icon") %></td>
		        <td class="Property"><%# Eval("Name") %></td>
		        <td class="Property"><%# Eval("ConnUnitID") %></td>
		        <td class="Property"><%# Eval("Product") %></td>
		        <td class="Property"><%# Eval("SerialNumber") %></td>
		        <td class="Property"><%# Eval("Type") %></td>
		        <td class="Property"><%# Eval("RevisionID") %></td>
		        <td class="Property"><%# Eval("RevisionDescription") %></td>
		        <td class="Property"><%# Eval("State") %></td>
		        <td class="Property"><%# Eval("Status") %></td>
	        </tr>
        </AlternatingItemTemplate>
        </asp:Repeater>
    </table>    
    
</Content>
</orion:resourceWrapper>