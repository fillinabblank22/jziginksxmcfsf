using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NPM.Common.Models;
using System.Text;


public partial class Orion_NPM_Resources_FiberChannel_ListOfVSANs : ResourceControl
{
    private const string VSANDetailLink = "<a href=\"/Orion/NPM/VSANDetails.aspx?NetObject=NVS:{0}&view=VSANDetails\">{1}</a>";

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var node = GetInterfaceInstance<INodeProvider>().Node;

        const string selectPollers = @"SELECT Enabled FROM Orion.Pollers WHERE NetObject = @NetObjectID AND PollerType LIKE '%FibreChannel%CiscoMDS'";

        const string selectVsans = @"SELECT DISTINCT v.ID, v.Name, MediaType, AdminState, LoadBalancingType, OperationalState, InteroperabilityValue FROM Orion.NPM.FCPorts p 
INNER JOIN Orion.NPM.FCUnits u ON u.ID = p.UnitID 
INNER JOIN Orion.NPM.VSANs v ON p.VsanID = v.ID 
WHERE u.NodeID = @NodeID";

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var row =
                swis.Query(selectPollers,
                           new Dictionary<string, object> {{"NetObjectID", node.NetObjectID}}
                    );

            // hide if poller doesn't exist or is disabled
            if (row == null || row.Rows.Count == 0 || !(bool) row.Rows[0]["Enabled"])
            {
                this.Visible = false;
                return;
            }

            var vsans =
                swis.Query(selectVsans,
                           new Dictionary<string, object>
                               {
                                   {"NodeID", node.NodeID},
                               }
                    );

            if (vsans == null || vsans.Rows.Count == 1)
            {
                // show nothing when there is no port
                this.Visible = false;
                return;
            }

            // bind data
            connUnitStatusTable.DataSource = ProcessTable(vsans.AsEnumerable());
            connUnitStatusTable.DataBind();
        }
    }

    private object ProcessTable(EnumerableRowCollection<DataRow> vsans)
    {
        return vsans.Select(i => new
        {
            Icon = VsanIcon((VsanAdminState)i["AdminState"]),
            Name = String.Format(VSANDetailLink, i["ID"], i["Name"]),
            ID = i["ID"],
            MediaType = GetLocalizedVSANProperty("VsanMediaType", ((VsanMediaType)i["MediaType"]).ToString()),
            LoadBalancingType = (VsanLoadBalancingType)i["LoadBalancingType"],
            AdminState = GetLocalizedVSANProperty("VsanAdminState", ((VsanAdminState)i["AdminState"]).ToString())
        });
    }

    private string GetLocalizedVSANProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    private string VsanIcon(VsanAdminState state)
    {
        var icon = new StringBuilder("/Orion/Images/StatusIcons/small-");

        switch (state)
        {
            case VsanAdminState.Active:
                icon.Append("up");
                break;

            case VsanAdminState.Suspended:
                icon.Append("unknown");
                break;

            default:
                return "&nbsp;";
        }

        return String.Format("<img src=\"{0}.gif\" />", icon.ToString());
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_179; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceListVSANs"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
