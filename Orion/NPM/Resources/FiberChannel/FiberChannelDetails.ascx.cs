﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using Interface = SolarWinds.Orion.NPM.Web.Interface;
using SolarWinds.NPM.Common.Models;


public partial class Orion_NPM_Resources_FiberChannel_FiberChannelDetails : ResourceControl
{
    protected Interface Interface { get; set; }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IInterfaceProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Interface = GetInterfaceInstance<IInterfaceProvider>().Interface;

        const string selectPollers = @"SELECT Enabled FROM Orion.Pollers WHERE NetObject = @NetObjectID AND PollerType LIKE '%FibreChannel%'";

        const string selectPort = @"SELECT PhysicalNumber, WWN, HWState FROM Orion.NPM.FCPorts p 
INNER JOIN Orion.NPM.FCUnits u ON u.ID = p.UnitID 
WHERE u.NodeID = @NodeID AND p.[Index] = @Index";


        using (var swis = InformationServiceProxy.CreateV3())
        {
            var row =
                swis.Query(selectPollers,
                           new Dictionary<string, object> {{"NetObjectID", this.Interface.Node.NetObjectID}}
                    );

            // hide if poller doesn't exist or is disabled
            if (row == null || row.Rows.Count == 0 || !(bool) row.Rows[0]["Enabled"])
            {
                this.Visible = false;
                return;
            }

            var port = 
                swis.Query(selectPort,
                           new Dictionary<string, object>
                               {
                                   { "NodeID", this.Interface.NodeID },
                                   { "Index", this.Interface.InterfaceIndex }
                               }
                    );

            if (port == null || port.Rows.Count != 1)
            {
                // show nothing when there is no port
                this.Visible = false;
                return;
            }

            // bind data
            this.physicalPort.Text = port.Rows[0]["PhysicalNumber"].ToString();
            this.portWWN.Text = port.Rows[0]["WWN"].ToString();
            this.hardwareState.Text = GetLocalizedHWState(((FCPortHWState)port.Rows[0]["HWState"]).ToString());
            this.Visible = true;
        }
    }
    private string GetLocalizedHWState(string state)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey("FCPortHWState", state);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_180; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceFiberChannelDetails"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
