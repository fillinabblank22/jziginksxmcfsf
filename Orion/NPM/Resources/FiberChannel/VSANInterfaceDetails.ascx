<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VSANInterfaceDetails.ascx.cs" Inherits="Orion_NPM_Resources_FiberChannel_VSANInterfaceDetails" %>


<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>
    <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailsTable" class="NeedsZebraStripes">
        <tr>          
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_312%></td>
          <td class="Property">				
				<asp:Image runat="server" ID="vsanStateIcon" style="vertical-align:middle; margin-right: 3px" />
				<asp:Label runat="server" ID="vsanState" style="vertical-align:middle;"/>						
		  </td>          		  
        </tr>
        <tr>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_116%></td>
          <td class="Property"><asp:Label runat="server" ID="vsanName" /></td>
        </tr>
        <tr>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_313%></td>
          <td class="Property"><asp:Label runat="server" ID="vsanID" /></td>
        </tr>
        <tr>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_314%></td>
          <td class="Property"><asp:Label runat="server" ID="vsanMediaType" /></td>
        </tr>
        <tr>          
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_315%></td>
          <td class="Property"><asp:Label runat="server" ID="vsanLoadBalancingType" /></td>
        </tr>
        <tr>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_316%></td>
          <td class="Property"><asp:Label runat="server" ID="portType" /></td>
        </tr>
        <tr>
           <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_317%></td>
          <td class="Property"><asp:Label runat="server" ID="portWWN" /></td>
        </tr>
    </table>
    
</Content>
</orion:resourceWrapper>