using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using Interface = SolarWinds.Orion.NPM.Web.Interface;
using SolarWinds.NPM.Common.Models;
using System.Text;


public partial class Orion_NPM_Resources_FiberChannel_VSANInterfaceDetails : ResourceControl
{
    private const string VSANDetailLink = "<a href=\"/Orion/NPM/VSANDetails.aspx?NetObject=NVS:{0}&view=VSANDetails\">{1}</a>";

    protected Interface Interface { get; set; }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IInterfaceProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Interface = GetInterfaceInstance<IInterfaceProvider>().Interface;

        var node = GetInterfaceInstance<INodeProvider>().Node;

        const string selectPollers = @"SELECT Enabled FROM Orion.Pollers WHERE NetObject = @NetObjectID AND PollerType LIKE '%FibreChannel%CiscoMDS'";

        const string selectVsan = @"SELECT DISTINCT v.ID, v.Name, MediaType, AdminState, LoadBalancingType, p.Type, WWN FROM Orion.NPM.FCPorts p 
INNER JOIN Orion.NPM.FCUnits u ON u.ID = p.UnitID 
INNER JOIN Orion.NPM.VSANs v ON p.VsanID = v.ID 
WHERE u.NodeID = @NodeID AND p.[Index] = @Index";

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var row =
                swis.Query(selectPollers,
                           new Dictionary<string, object> { { "NetObjectID", node.NetObjectID } }
                    );

            // hide if poller doesn't exist or is disabled
            if (row == null || row.Rows.Count == 0 || !(bool)row.Rows[0]["Enabled"])
            {
                this.Visible = false;
                return;
            }

            var vsans =
                swis.Query(selectVsan,
                           new Dictionary<string, object>
                               {
                                   { "NodeID", this.Interface.NodeID },
                                   { "Index", this.Interface.InterfaceIndex }
                               }
                    );

            if (vsans == null || vsans.Rows.Count != 1)
            {
                // show nothing when there is no vsan
                this.Visible = false;
                return;
            }

            // bind data
            var vsan = vsans.Rows[0];
            this.vsanState.Text = String.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_177, GetLocalizedProperty("VsanAdminState", ((VsanAdminState)vsan["AdminState"]).ToString()));
            this.vsanStateIcon.ImageUrl = VsanIcon((VsanAdminState)vsan["AdminState"]);
            this.vsanName.Text = String.Format(VSANDetailLink, vsan["ID"], vsan["Name"]);
            this.vsanID.Text = vsan["ID"].ToString();
            this.vsanMediaType.Text = GetLocalizedProperty("VsanMediaType", ((VsanMediaType)vsan["MediaType"]).ToString());
            this.vsanLoadBalancingType.Text = ((VsanLoadBalancingType)vsan["LoadBalancingType"]).ToString();
            this.portType.Text = GetLocalizedProperty("FCPortType", ((FCPortType)vsan["Type"]).ToString());
            this.portWWN.Text = vsan["WWN"].ToString();
        }
    }

    private string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    private string VsanIcon(VsanAdminState state)
    {
        var icon = new StringBuilder("/Orion/Images/StatusIcons/small-");

        switch (state)
        {
            case VsanAdminState.Active:
                icon.Append("up");
                break;

            case VsanAdminState.Suspended:
                icon.Append("unknown");
                break;

            default:
                return "&nbsp;";
        }

        return String.Format("{0}.gif", icon.ToString());
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_178; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVSANInterfaceDetails"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
