using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NPM.Common.Models;
using System.Text;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.InformationService;


public partial class Orion_NPM_Resources_FiberChannel_ConnectivityUnitStatus : ResourceControl
{
    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var node = GetInterfaceInstance<INodeProvider>().Node;
        
        const string selectPollers = @"SELECT Enabled FROM Orion.Pollers WHERE NetObject = @NetObjectID AND PollerType LIKE '%FibreChannel%'";
        
        const string selectUnits = @"SELECT ID, NodeID, ConnUnitID, Type, Product, Name, SerialNumber, PortCount, State, Status 
FROM Orion.NPM.FCUnits 
WHERE NodeID = @NodeID";
        
        const string selectRevisions = @"SELECT UnitID, Index, RevisionID, RevisionDescription
FROM Orion.NPM.FCRevisions
WHERE NodeID = @NodeID";

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var row =
                swis.Query(selectPollers,
                    new Dictionary<string, object> { { "NetObjectID", node.NetObjectID } }
                    );

            // hide if poller doesn't exist or is disabled
            if (row == null || row.Rows.Count == 0 || !(bool)row.Rows[0]["Enabled"])
            {
                this.Visible = false;
                return;
            }

            var units = GetTableByNodeID(swis, selectUnits, node.NodeID);
            if (units == null || units.Rows.Count == 0)
            {
                // show nothing when there are no connectivity units
                this.Visible = false;
                return;
            }

            var revisions = GetTableByNodeID(swis, selectRevisions, node.NodeID);

            // bind data
            connUnitStatusTable.DataSource = JoinTables(units.AsEnumerable(), revisions.AsEnumerable());
            connUnitStatusTable.DataBind();
        }
    }

    private DataTable GetTableByNodeID(InformationServiceProxy proxy, string select, int nodeID)
    {
        return proxy.Query(select,
                        new Dictionary<string, object> { { "NodeID", nodeID } }
                        );
    }

    private object JoinTables(EnumerableRowCollection<DataRow> units, EnumerableRowCollection<DataRow> revisions)
    {
        return from u in units
               join r in revisions on u["ID"] equals r["UnitID"] into buff
                      from t in buff.DefaultIfEmpty()   // we need left outer join
                      select new
                      {
                          Icon = UnitIcon((FCUnitStatus)u["Status"]),
                          Name = u["Name"],
                          ConnUnitID = u["ConnUnitID"],
                          Product = u["Product"],
                          SerialNumber = u["SerialNumber"],
                          Type = GetLocalizedConnectivityUnitProperty("FCUnitType", ((FCUnitType)u["Type"]).ToString()),
                          RevisionID = (t == null) ? "&nbsp;" : t["RevisionID"],
                          RevisionDescription = (t == null) ? "&nbsp;" : t["RevisionDescription"],
                          State = GetLocalizedConnectivityUnitProperty("FCUnitState", ((FCUnitState)u["State"]).ToString()),
                          Status = GetLocalizedConnectivityUnitProperty("FCUnitStatus", ((FCUnitStatus)u["Status"]).ToString())
                      };
    }

    private string GetLocalizedConnectivityUnitProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    private string UnitIcon(FCUnitStatus status)
    {
        var icon = new StringBuilder("/Orion/Images/StatusIcons/small-");

        switch (status)
        {
            case FCUnitStatus.Ok:
                icon.Append("up");
                break;

            case FCUnitStatus.Warning:
                icon.Append("warning");
                break;

            case FCUnitStatus.Failed:
                icon.Append("down");
                break;

            default:
                return "&nbsp;";
        }

        return String.Format("<img src=\"{0}.gif\" />", icon.ToString());
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_176; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceConnectivityUnitStatus"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
