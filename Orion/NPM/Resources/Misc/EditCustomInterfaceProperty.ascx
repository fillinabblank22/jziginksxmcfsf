<%@ Control Language="C#" ClassName="EditCustomInterfaceProperty" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>
<%@ Import Namespace="SolarWinds.Logging" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.CPE" %>

<script runat="server">
    private Log _log = new Log();
    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBDATA_VB0_209; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IInterfaceProvider) };
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceCustomPropertiesNodes";
        }
    }

    public string CustomProperty
    {
        get
        {
            if (this.Resource.Properties.ContainsKey("CustomProperty"))
                return this.Resource.Properties["CustomProperty"];

            return string.Empty;
        }
    }

    public override string EditURL
    {
        get
        {
            return string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}&NetObject={1}",
                this.Resource.ID, this.ThisInterface != null ? this.ThisInterface.NetObjectID : !string.IsNullOrEmpty(Request["NetObject"]) ? Request["NetObject"] : string.Empty);
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NPM/Controls/EditResourceControls/SelectCustomInterfaceProperty.ascx"; }
    }

    protected override void OnInit(EventArgs e)
    {
        if (string.IsNullOrEmpty(this.CustomProperty))
        {
            this.phNoPropMessage.Visible = true;
            this.phPropEdit.Visible = false;
        }
        else
        {            
            var customProperty = CustomPropertyMgr.GetCustomProperty("Interfaces", this.CustomProperty);
            PropertyValue.Configure(true, customProperty.PropertyType, customProperty.Values);
            PropertyValue.Text = ThisInterface.CustomProperties[this.CustomProperty];
        }

        base.OnInit(e);
    }

    protected void UpdateClick(object sender, EventArgs e)
    {
        try
        {         
            var customProperty = CustomPropertyMgr.GetCustomProperty("Interfaces", this.CustomProperty);

            var value = PropertyValue.Text.Trim();

            if (customProperty.Values.Length > 0 && !string.IsNullOrEmpty(value))
            {
                var list = new List<string>(customProperty.Values);

                if (!list.Contains(value))
                {
                    list.Add(value);
                    CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty, list.ToArray());
                }
            }


            CustomPropertyMgr.SetCustomProp("Interfaces", this.CustomProperty, this.ThisInterface.InterfaceID, value);


        }
        catch (Exception ex)
        {
            _log.Error(String.Format("Edit Custom Interface Property: Exception while updating custom property '{0}'", CustomProperty), ex);
            this.PropertyValue.Text = this.ThisInterface.CustomProperties[this.CustomProperty];
        }
        Response.Redirect(this.Request.RawUrl); //full page refresh
    }

    protected Interface ThisInterface
    {
        get
        {
            var provider = ((IInterfaceProvider)this.GetInterfaceInstance(typeof(IInterfaceProvider)));
            return provider != null ? provider.Interface : null;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Synchronous; } }

</script>

<orion:ResourceWrapper runat="server">
    <Content>
            <div class="DefaultShading sw-no-row-lines" style="padding-left: 9px;">
            <asp:PlaceHolder runat="server" Visible="false" ID="phNoPropMessage">
                <%= String.Format(Resources.NPMWebContent.NPMWEBDATA_VB0_211, String.Format("<a href=\"{0}\">", this.EditURL),"</a>")%>
            </asp:PlaceHolder>
                        
            <asp:PlaceHolder runat="server" ID="phPropEdit">
                <%= String.Format(Resources.NPMWebContent.NPMWEBDATA_VB0_222,this.Resource.Properties["CustomProperty"])%>
                <br />
                
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                    <ContentTemplate>
                        <orion:EditCpValueControl ID="PropertyValue" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                            
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" LocalizedText="CustomText" Text="<%$Resources: NPMWebContent, NPMWEBDATA_VB0_219%>" DisplayType="Secondary" OnClick="UpdateClick" />
                </div>
            </asp:PlaceHolder>
        </div>
    </Content>
</orion:ResourceWrapper>
