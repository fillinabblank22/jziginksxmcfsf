﻿using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;

public partial class Orion_NPM_Resources_VRF_ListOfVrfsNode : BaseResourceControl
{
    private const string VRFPOLLERQUERY = @"SELECT PollerType, Enabled FROM Orion.Pollers WHERE NetObject = @NetObjectID AND (PollerType LIKE 'N.VRFRouting.%')";

    private bool IsPollerEnabled(string netObjectId)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var pollers = swis.Query(VRFPOLLERQUERY, new Dictionary<string, object> { { "NetObjectID", netObjectId } });

            bool isPollerTableEmpty = pollers == null || pollers.Rows.Count == 0;
            bool isPollerEnabled = false;
            

            if (!isPollerTableEmpty)
            {
                foreach (DataRow poller in pollers.Rows)
                {
                    isPollerEnabled |= Convert.ToBoolean(poller["Enabled"]);

                    if (isPollerEnabled)
                    {
                        break;
                    }
                }
            }

            return isPollerEnabled;
        }
    }

    public int NodeId;

    protected void Page_Load(object sender, EventArgs e)
    {
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        NodeId = -1;
        if (nodeProvider != null)
        {
            NodeId = nodeProvider.Node.NodeID;

            // hide if poller doesn't exist or is disabled
            if (!IsPollerEnabled(nodeProvider.Node.NetObjectID))
            {
                this.Visible = false;
                return;
            }
        }

        if (NodeId <= 0)
        {
            this.Visible = false;
            return;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBDATA_LH0_7; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(INodeProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected override void Render(HtmlTextWriter writer)
    {
        tabTemplate.Text = TokenSubstitution.Parse(tabTemplate.Text);
        tabTemplate2.Text = TokenSubstitution.Parse(tabTemplate2.Text);
        base.Render(writer);
    }
}