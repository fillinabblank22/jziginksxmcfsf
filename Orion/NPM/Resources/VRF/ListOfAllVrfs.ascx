﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfAllVrfs.ascx.cs" Inherits="Orion_NPM_Resources_VRF_ListOfAllVrfs" %>
<orion:ResourceWrapper runat="server" ID="Wrapper1">
    <content>
        <orion:Include ID="Include1" runat="server" File="../../Orion/NPM/js/Vrf.js" />
		<orion:Include ID="Include2" runat="server" File="../../Orion/NPM/styles/VRFResources.css" />
        <script type="text/javascript">
            $(function () {
                SW.NPM.ListOfVrfs.SetupListOfVrfs('<%= Resource.ID %>');
            });
        </script>
        <script id="listOfVrfsTemplate-<%= Resource.ID %>" type="text/x-template">
        <table class="vrfTable">
            <thead>
                <tr class="HeaderRow">
                    <td class="ReportHeader vrfRouteDistinguisher"><%= Resources.NPMWebContent.NPMWEBDATA_GKO_10 %></td>
                    <td class="ReportHeader vrfName"><%= Resources.NPMWebContent.NPMWEBDATA_GKO_11 %></td>
                    <td class="ReportHeader vrfDescription"><%= Resources.NPMWebContent.NPMWEBDATA_GKO_12 %></td>
                </tr>
            </thead>
            {# _.each(vrfs, function(current, index) { #}
            <tbody>
                <tr class="vrfAccordion">
                    <td class="vrfRouteDistinguisher"><img src="/Orion/Images/Button.Expand.gif" class="vrfExpander"/>&nbsp;{# if (current.RouteDistinguisher) { #}{{ current.RouteDistinguisher }}{# } else { #}<span class="emptyRouteDistinguisher"><%= Resources.NPMWebContent.NPMWEBDATA_LH0_8 %></span>{# } #}</td>
                    <td>&nbsp;</td>
                    <td>{{ current.ItemCount }}</td>
                </tr>
            </tbody>
            <tbody class="itemList hidden" data-row-id="{{ current.RouteDistinguisher }}">
                <tr class="showMoreRow hidden">
                    <td>&nbsp;</td>
                    <td class="showMoreLink"><a href="#"><%= Resources.NPMWebContent.NPMWEBDATA_PS0_1 %></a></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="loadingRow hidden">
                    <td>&nbsp;</td>
                    <td colspan="2"><%= Resources.CoreWebContent.WEBDATA_AK0_213 %></td>
                </tr>
            </tbody>
            {# }); #}
        </table>
        </script> 
        <script id="listOfVrfsRowTemplate-<%= Resource.ID %>" type="text/x-template">
                <tr>
                    <td>&nbsp;</td>
                    <td><img class="StatusIcon" alt="VRF Status" src="{{item.VrfStatusIcon }}"/>{{ item.VrfName }}</td>
                    <td><a href="{{ item.NodeDetailsLink }}"><img class="StatusIcon" alt="Status" src="/NetPerfmon/images/small-{{ item.NodeStatusIcon }}"/>{{ item.NodeName }}</a></td>
                </tr>    
        </script>
        <span id="Content-<%= Resource.ID %>"></span>
        <div id="Pager-<%= Resource.ID %>" class="ReportFooter ResourcePagerControl"></div>
    </content>
</orion:ResourceWrapper>

