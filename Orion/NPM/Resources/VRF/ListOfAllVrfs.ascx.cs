﻿using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using System;

public partial class Orion_NPM_Resources_VRF_ListOfAllVrfs : BaseResourceControl
{
    private const string query = @"SELECT n.NodeID
FROM Orion.Nodes AS n 
JOIN Orion.Routing.VRF AS vrf on n.NodeID = vrf.NodeID
WHERE (1 IN (
	SELECT Count(p.PollerType) AS Cnt
	FROM Orion.Pollers AS p
	WHERE p.NetObjectID = n.NodeID AND p.NetObjectType = 'N' AND Enabled = true AND p.PollerType LIKE '%.VRFRouting.%'
	))
GROUP By n.NodeID";

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBDATA_GKO_13; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var pollers = swis.Query(query);

            this.Visible = pollers != null && pollers.Rows.Count > 0;
        }
    }
}