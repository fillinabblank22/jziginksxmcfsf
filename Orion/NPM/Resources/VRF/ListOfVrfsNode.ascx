﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfVrfsNode.ascx.cs" Inherits="Orion_NPM_Resources_VRF_ListOfVrfsNode" %>

<orion:ResourceWrapper runat="server" ID="Wrapper1">
    <content>
        <orion:Include ID="Include1" runat="server" File="../../Orion/NPM/js/Vrf.js" />
		<orion:Include ID="Include2" runat="server" File="../../Orion/NPM/styles/VRFResources.css" />
		
        <script type="text/javascript">
            $(function () {
                SW.NPM.ListOfVrfs.SetupListOfVrfsOnNode('<%= Resource.ID %>', '<%= NodeId %>');
             });
        </script>
        <script id="listOfVrfsTemplate-<%= Resource.ID %>" type="text/x-template">
        {# if (vrfs && vrfs.length > 0) { #}
        <table class="vrfTable">
            <thead>
                <tr class="HeaderRow">
                    <td class="ReportHeader vrfRouteDistinguisher"><%=Resources.NPMWebContent.NPMWEBDATA_GKO_10 %></td>        
                    <td class="ReportHeader vrfName"><%= Resources.NPMWebContent.NPMWEBDATA_GKO_11 %></td>
                    <td class="ReportHeader vrfDescription"><%= Resources.NPMWebContent.NPMWEBDATA_LH0_6 %></td>
                </tr>
            </thead>
            {# _.each(vrfs, function(current, index) { #}
            <tbody>
                <tr class="vrfAccordion">
                    <td>{# if (current.ItemCount > 0) { #}<img src="/Orion/Images/Button.Expand.gif" class="vrfExpander" />{# } else { #}<div class="emptyExpander">&nbsp;</div>{# } #}&nbsp;
                        <a class="vrfNameHolder"><img src="{{ current.StatusIcon }}"/>{# if (current.RouteDistinguisher) { #}{{ current.RouteDistinguisher }}</a>{# } else { #}<span class="emptyRouteDistinguisher"><%= Resources.NPMWebContent.NPMWEBDATA_LH0_8 %></span>{# } #}</td>
                    <td>{{ current.Name }}</td>
                    <td>{{ current.Description }}</td>
                </tr>
            </tbody>
            <tbody class="itemList hidden" data-row-id="{{ current.VrfIndex }}">
                {# if (current.ItemCount > 0) { #}
                <tr class="showMoreRow hidden">
                    <td>&nbsp;</td>
                    <td class="showMoreLink"><a href="#"><%= Resources.NPMWebContent.NPMWEBDATA_PS0_1 %></a></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="loadingRow hidden">
                    <td>&nbsp;</td>
                    <td colspan="2"><%= Resources.CoreWebContent.WEBDATA_AK0_213 %></td>
                </tr>
                {# } else {#}  
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <asp:Literal runat="server" ID="tabTemplate">
                        @{R=NPM.Strings;K=NPMWEBJS_LH0_5; E=js}
                        </asp:Literal>
                    </td>
                </tr>
                {# } #}
            </tbody>
            {# }); #}     
        </table>
        {# } else { #}
        <div class="sw-suggestion sw-suggestion-warn">
            <span class="sw-suggestion-icon" />
            <asp:Literal runat="server" ID="tabTemplate2">
            @{R=NPM.Strings;K=NPMWEBJS_PS1_1; E=js}
            </asp:Literal>
        </div>
        {# } #}
        </script>
        <script id="listOfVrfsRowTemplate-<%= Resource.ID %>" type="text/x-template">
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                    {# if (item.HasItem) { #}
                    <a href="{{ item.InterfaceDetailsLink }}">
                        <img class="StatusIcon" alt="Status" src="/NetPerfmon/images/small-{{ item.InterfaceStatusIcon }}"/>
                        {{ item.InterfaceName }}
                    </a>
                    {# } else { #}
                    {{ item.InterfaceName }}
                    {# } #}
                    </td>
                </tr>    
        </script>
        <span id="Content-<%= Resource.ID %>"></span>
        <div id="Pager-<%= Resource.ID %>" class="ReportFooter ResourcePagerControl"></div>
    </content>
</orion:ResourceWrapper>