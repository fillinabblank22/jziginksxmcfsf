﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OverallEnergyWiseSavings.ascx.cs" Inherits="Orion_Resources_EnergyWise_OverallEnergyWiseSavings" %>


<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="false">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper> 