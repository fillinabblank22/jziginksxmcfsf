﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OverallHistoricalPowerConsumption.ascx.cs" Inherits="Orion_Resources_EnergyWise_OverallHistoricalPowerConsumption" %>


<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="false">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper> 