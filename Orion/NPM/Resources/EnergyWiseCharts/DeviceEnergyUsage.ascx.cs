using System;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;


public partial class DeviceEnergyUsage : GraphResource
{

	protected void Page_Load(object sender, EventArgs e)
	{
		Node Nd = ((INodeProvider)GetInterfaceInstance(typeof(INodeProvider))).Node;
		if (Nd != null)
		{
			using (var swql = InformationServiceProxy.CreateV3())
			{
				var count = Convert.ToInt32(swql.Query("SELECT COUNT(ID) AS cnt FROM Orion.NPM.EW.Device WHERE NodeID=@nodeid AND ISNULL(isEwEnabled, 0) = 1",
						   new Dictionary<string, object> { { "nodeid", Nd.NodeID } }).Rows[0][0]);

				if (count == 0)
				{
					this.Visible = false;
				}
			}
		}
		else
		{
			this.Visible = false;
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, "EW_DeviceEnergyUsage", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("EW_DeviceEnergyUsage", Resource, string.Format("N{0}", GetTitlePrefix()));
	}

	protected override string DefaultTitle
	{
		get
		{
			return notAvailable;
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomChart.ascx";
		}
	}


	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodeEnergyUsageAreaChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
