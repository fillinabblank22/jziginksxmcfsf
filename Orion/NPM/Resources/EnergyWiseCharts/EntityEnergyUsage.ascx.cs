using System;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class EntityEnergyUsage : GraphResource
{
	protected void Page_Load(object sender, EventArgs e)
	{
		Interface interf = GetInterfaceInstance<IInterfaceProvider>().Interface;
		if (interf != null)
		{

			using (var swql = InformationServiceProxy.CreateV3())
			{
				var count =
					Convert.ToInt32(
						swql.Query("SELECT COUNT(ID) AS cnt FROM Orion.NPM.EW.Entity WHERE NodeID=@nodeid AND InterfaceIndex=@ifindex",
								   new Dictionary<string, object> { { "nodeid", interf.NodeID }, { "ifindex", interf.InterfaceIndex } })
								   .Rows[0][0]);

				if (count == 0)
				{
					this.Visible = false;
				}

			}
		}
		else
		{
			this.Visible = false;
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		//FB 18018. Changed the NetObjectID from Node to Interface, as the resource is used on interfaces
		//and has "InterfaceID" property in its Where clause.
		CreateChart(null, GetInterfaceInstance<IInterfaceProvider>().Interface.NetObjectID, "EW_EntityEnergyUsage", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("EW_EntityEnergyUsage", Resource, string.Format("I{0}", GetTitlePrefix()));
	}

	protected override string DefaultTitle
	{
		get
		{
			return notAvailable;
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomChart.ascx";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodeEnergyUsageAreaChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
