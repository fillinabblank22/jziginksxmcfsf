﻿using System;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web.UI;


public partial class Orion_Resources_EnergyWise_OverallEnergyWiseSavings : SummaryGraphResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        CreateChart("EW_OverallEnergyWiseSavings", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("EW_OverallEnergyWiseSavings", Resource, "S");
    }

    protected override string DefaultTitle
    {
        get
        {
            return notAvailable;//"Network Latency";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceOverallEnergyWiseSavingsChart"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
