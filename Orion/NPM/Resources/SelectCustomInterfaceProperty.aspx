<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" 
         CodeFile="SelectCustomInterfaceProperty.aspx.cs" Inherits="Orion_NetPerfMon_Resources_SelectCustomInterfaceProperty" 
         Title="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_72%>" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(this.Title, Request["netobject"]))%></h1>
    
    <a href="<%= HelpHelper.GetHelpUrl("OrionAGCustomProperties") %>" style="color: #0000FF" target="_blank">
		<%=Resources.NPMWebContent.NPMWEBDATA_VB0_220%>
	</a>
    
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" />
    
    <p>
        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_221%>
        <br />
        <asp:ListBox runat="server" ID="lbxCustomProps" Rows="1" SelectionMode="single" />
    </p>
    
    <p>
        <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
    </p>
</asp:Content>

