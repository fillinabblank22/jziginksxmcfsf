﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VSANLastXXEvents.ascx.cs" Inherits="Orion_NPM_Resources_VSANSummary_VSANLastXXEvents" %>
<%@ Register TagPrefix="orion" TagName="EventsList" Src="~/Orion/Controls/EventsReportControl.ascx" %>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <orion:EventsList runat="server" ID="EList" />
    </Content>
</orion:resourceWrapper>
