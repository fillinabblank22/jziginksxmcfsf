﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Web;
using System.Text;
using SolarWinds.Orion.Web.Charting;
using Limitation = SolarWinds.Orion.Web.Limitation;
using SolarWinds.NPM.Common;
using System.Data;

public partial class Orion_NPM_Resources_VSANDetails_VSANEventSummary : ResourceControl
{
    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_TM0_30; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditEventSummary.ascx"; }
    }

    protected void GetCurrentNetObjectID(out string netObjectType, out int netObjectID)
    {
        netObjectType = String.Empty;
        netObjectID = -1;

        // if is netObject a VSAN set filter for VSAN's interfaces
        IVSANProvider vsanProvider = GetInterfaceInstance<IVSANProvider>();
        if (vsanProvider != null)
        {
            netObjectType = "NVS";
            netObjectID = vsanProvider.VSAN.ID;
        }

        if (netObjectID > -1 && !string.IsNullOrEmpty(netObjectType))
            return;

        // no provider -> take the query string
        string netObjectString = Request.QueryString["NetObject"];

        if (!String.IsNullOrEmpty(netObjectString))
        {
            string[] temp = netObjectString.Split(':');
            if (temp.Length == 2)
            {
                if (Int32.TryParse(temp[1].Trim(), out netObjectID))
                {
                    netObjectType = temp[0].Trim();
                    return;
                }
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable eventsTable = new DataTable();

            string netObjectType;
            int netObjectID = -1;

            GetCurrentNetObjectID(out netObjectType, out netObjectID);

            // get period from resources
            string periodName = Resource.Properties["Period"];

            // if there is no valid period set "Today" as default
            if (String.IsNullOrEmpty(periodName))
                periodName = "Today";

            // get period interval
            DateTime periodBegin = new DateTime();
            DateTime periodEnd = new DateTime();

            string periodTemp = periodName;
            Periods.Parse(ref periodTemp, ref periodBegin, ref periodEnd);

            using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                try
                {
                    eventsTable = proxy.Api.GetVSANEventsSummaryTable(netObjectID, periodBegin, periodEnd, Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            // sort by EventTime
            eventsTable.DefaultView.Sort = eventsTable.Columns["Total"] + " DESC";

            // adds new column to the table for URL resolving
            eventsTable.Columns.Add("URL");

            foreach (DataRow r in eventsTable.Rows)
            {
                StringBuilder url = new StringBuilder("/Orion/NetPerfMon/Events.aspx?");

                url.AppendFormat("NetObject=N:{0}", r["NetworkNode"]);  // link to Node netobject
                url.AppendFormat("&Period={0}", periodName);
                url.AppendFormat("&EventType={0}", r["EventType"]);

                r["URL"] = url.ToString();
                int color = (int)r["BackColor"];
                r["BackColor"] = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
            }

            this.EList.LoadData(eventsTable);
        }

 

    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceEventSummary"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}