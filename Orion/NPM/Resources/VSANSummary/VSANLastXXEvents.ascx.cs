﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Common;
using System.Data;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web.Charting;

public partial class Orion_NPM_Resources_VSANSummary_VSANLastXXEvents : ResourceControl
{
    // default number is used if there is no record in resource properties (this should never happened)
    static int defaultNumberOfEvents = 25;

    #region properties

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_TM0_28; }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceLastXXEvents";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditLastXXEvents.ascx"; }
    }

    #endregion

    // this function resolves netObjectType & netObjectID
    protected void GetCurrentNetObjectID(out string netObjectType, out int netObjectID)
    {
        netObjectType = String.Empty;
        netObjectID = -1;

        // if is netObject a VSAN set filter for a VSAN's interfaces
        IVSANProvider vsanProvider = GetInterfaceInstance<IVSANProvider>();
        if (vsanProvider != null)
        {
            netObjectType = "NVS";
            netObjectID = vsanProvider.VSAN.ID;
        }

        // no provider -> take the query string
        string netObjectString = Request.QueryString["NetObject"];

        if (!String.IsNullOrEmpty(netObjectString))
        {
            string[] temp = netObjectString.Split(':');
            if (temp.Length == 2)
            {
                if (Int32.TryParse(temp[1].Trim(), out netObjectID))
                {
                    netObjectType = temp[0].Trim();
                    return;
                }
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (String.IsNullOrEmpty(Resource.Properties["Period"]))
                Resource.Properties["Period"] = "Last 12 Months";

            // try to load max event number or use default value
            int maxEvents = defaultNumberOfEvents;
            try
            {
                maxEvents = Int32.Parse(Resource.Properties["MaxEvents"]);
            }
            catch
            {
                maxEvents = defaultNumberOfEvents;
            }

            // set filter args for netobject if necessary
            string netObjectType = String.Empty;
            int netObjectID = -1;
            
            GetCurrentNetObjectID(out netObjectType, out netObjectID);

            DateTime periodBegin = new DateTime();
            DateTime periodEnd = new DateTime();

            // get period from resources
            string periodName = Resource.Properties["Period"];

            // if there is no valid period set "Today" as default
            if (String.IsNullOrEmpty(periodName))
                periodName = "Last 12 Months";

            // parse period to begin and end
            Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

            DataTable eventsTable = new DataTable();
            using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
            {
                try
                {
                    eventsTable = proxy.Api.GetVSANEvents(netObjectID, maxEvents, periodBegin, periodEnd, Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            // adds new column to the table for URL resolving
            eventsTable.Columns.Add("URL");

            foreach (DataRow r in eventsTable.Rows)
            {
                if ((r["NetObjectType"] is DBNull) || (r["NetObjectID"] is DBNull) || (r["NetObjectType"].ToString().Trim() == String.Empty) || ((int)r["NetObjectID"] == 0))
                {
                    r["URL"] = String.Empty;
                }
                else
                {
                    r["URL"] = String.Format("/Orion/View.aspx?NetObject={0}:{1}", r["NetObjectType"].ToString().Trim().ToUpper(), r["NetObjectID"]);
                }

                int color = (int)r["BackColor"];
                r["BackColor"] = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
            }

            this.EList.LoadData(eventsTable);
        }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}