﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_VSANCharts_VSANTraffic : GraphResource, IResourceIsInternal
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        CreateChart(null, String.Empty, "VSANTrafficArea", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("VSANTrafficArea", Resource);
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.NPMWebContent.NPMWEBDATA_TM0_10;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVSANTrafficChart"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NPM/Controls/EditResourceControls/EditVSANCharts.ascx";
        }
    }

    public bool IsInternal
    {
        get { return true; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}