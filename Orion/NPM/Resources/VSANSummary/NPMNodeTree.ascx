<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NPMNodeTree.ascx.cs" Inherits="Orion_NetPerfMon_NPMNodeTree" %>

<asp:ScriptManagerProxy id="NodeTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/NPM/Services/NPMNodeTree.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="../NPM/Resources/VSANSummary/AjaxNodeTree.js" />

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
	    <script type="text/javascript">
	        //<![CDATA[
	        $(function () {
	            var refresh = function () {
	                NPM_VSAN.NPMNodeTree.LoadRoot($('#<%=this.nodetree.ClientID%>'), '<%=this.Resource.ID %>');
	            };

	            SW.Core.View.AddOnRefresh(refresh, '<%= this.nodetree.ClientID %>');
	            refresh();
	        });
	        //]]>
	    </script>
	
		<div class="Tree" ID="nodetree" runat="server">
			<asp:Literal runat="server" ID="TreeLiteral" />
		</div>
	</Content>
</orion:resourceWrapper>
