﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VSANEventSummary.ascx.cs" Inherits="Orion_NPM_Resources_VSANDetails_VSANEventSummary" %>
<%@ Register TagPrefix="orion" TagName="EventsSummaryList" Src="~/Orion/Controls/EventSummaryReportControl.ascx" %>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <orion:EventsSummaryList runat="server" ID="EList" />
    </Content>
</orion:resourceWrapper>
