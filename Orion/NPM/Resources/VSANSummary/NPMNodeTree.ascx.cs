using System;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_NPMNodeTree : BaseResourceControl
{
	public override string EditControlLocation
	{
        get { return "/Orion/NPM/Controls/EditResourceControls/EditNPMNodeTree.ascx"; }
	}

	public override string EditURL
	{
		get
		{
			string url = string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}&HideInterfaceFilter=True", this.Resource.ID);

			if (!string.IsNullOrEmpty(Request["NetObject"]))
				url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
			if (Page is OrionView)
				url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);

			return (url);
		}
	}

	protected override void OnInit(EventArgs e)
	{
	    // handle default groupings
        const string grouping1 = "Grouping1";
	    string groupingValue = Resource.Properties[grouping1];
        if(string.IsNullOrEmpty(groupingValue))
        {
            groupingValue = "NPM_VSANs_Nodes.VsanName";
            ResourcePropertiesDAL.AddUpdateProperty(Resource.ID, grouping1, groupingValue);
        }

        const string grouping2 = "Grouping2";
        string grouping2Value = Resource.Properties[grouping2];
        if (string.IsNullOrEmpty(grouping2Value))
        {
            grouping2Value = "none";
            ResourcePropertiesDAL.AddUpdateProperty(Resource.ID, grouping2, grouping2Value);
        }

        const string grouping3 = "Grouping3";
        string grouping3Value = Resource.Properties[grouping3];
        if (string.IsNullOrEmpty(groupingValue))
        {
            grouping3Value = "none";
            ResourcePropertiesDAL.AddUpdateProperty(Resource.ID, grouping3, grouping3Value);
        }
        base.OnInit(e);
	}

	protected override string DefaultTitle
	{
        get { return Resources.NPMWebContent.NPMWEBCODE_TM0_29; }
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceAllVSANNodes";
		}
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}
