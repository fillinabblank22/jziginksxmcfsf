﻿NPM_VSAN = {};

NPM_VSAN.NPMNodeTree = {

    Succeeded: function (result, context) {
        var contentsDiv = $(context);
        $(contentsDiv).get(0).innerHTML = result;
        contentsDiv.slideDown('fast');
        NPM_VSAN.NPMNodeTree.AutoClickLinks(contentsDiv);
    },

    Failed: function (error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = String.format("@{R=NPM.Strings;K=NPMWEBJS_TM0_2;E=js}", error.get_message());
    },

    AutoClickLinks: function (contentsDiv) {
        // auto-click the "get 100 more Nodes" links
        $("[id*='-startFrom-'] a", contentsDiv).click();

        // expand any nodes that are marked to be expanded	    
        $("[expand]", contentsDiv).removeAttr("expand").click();
    },


    Click: function (rootId, resourceId, keys) {
        return NPM_VSAN.NPMNodeTree.HandleClick(rootId, function (contentDiv) {
            NPMNodeTree.GetTreeSection(resourceId, rootId, keys, 0, NPM_VSAN.NPMNodeTree.Succeeded, NPM_VSAN.NPMNodeTree.Failed, contentDiv);
        }, function () {
            NPMNodeTree.CollapseTreeSection(resourceId, keys);
        });
    },

    LoadRoot: function (treeDiv, resourceId) {
        treeDiv.html(String.format("<div>{0}</div>", "@{R=NPM.Strings;K=NPMWEBJS_TM0_1;E=js}"));
        NPMNodeTree.GetTreeSection(resourceId, "NT_r" + resourceId, [], 0, NPM_VSAN.NPMNodeTree.Succeeded, NPM_VSAN.NPMNodeTree.Failed, treeDiv.get(0));
    },

    HandleClick: function (rootId, expandCallback, collapseCallback) {
        var toggleImg = $('#' + rootId + '-toggle');
        var contentsDiv = $('#' + rootId + '-contents');

        if (!contentsDiv.is(':hidden')) {
            contentsDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
        } else {
            contentsDiv.html("@{R=NPM.Strings;K=NPMWEBJS_TM0_1;E=js}");
            contentsDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            expandCallback(contentsDiv.get(0));
        }

        return false;
    },

    GetMore: function (linkId, resourceId, keys, startFrom) {
        var contentsDiv = $get(linkId);
        contentsDiv.innerHTML = "@{R=NPM.Strings;K=NPMWEBJS_TM0_1;E=js}";
        NPMNodeTree.GetTreeSection(resourceId, linkId, keys, startFrom, NPM_VSAN.NPMNodeTree.Succeeded, NPM_VSAN.NPMNodeTree.Failed, contentsDiv);
        return false;
    }


};

function NPMNodeTreeSectionReceived(result, context) {
    var contentsDiv = $(context);
    contentsDiv.html(result);
    contentsDiv.slideDown('fast');
    AutoClickGetMoreLinks(contentsDiv);
}

function AutoClickGetMoreLinks(contentsDiv) {
    // auto-click the "get 100 more nodes" links
    $("[id*='-startFrom-'] a", contentsDiv).click();
}

$(function () {
    AutoClickGetMoreLinks();
});

function NPMNodeTreeSectionFailed(error, context) {
    var contentsDiv = context;
    contentsDiv.innerHTML = String.format("@{R=NPM.Strings;K=NPMWEBJS_TM0_2;E=js}", error.get_message());
}

function FastNPMNodeTree_Click(rootId, resourceId, keys) {
    var toggleImg = $get(rootId + '-toggle');
    var contentsDiv = $get(rootId + '-contents');

    if (contentsDiv.style.display == "") {
        contentsDiv.style.display = "none";
        toggleImg.src = "/Orion/images/Button.Expand.gif";
    } else {
        contentsDiv.innerHTML = "@{R=NPM.Strings;K=NPMWEBJS_TM0_1;E=js}";
        contentsDiv.style.display = "";
        toggleImg.src = "/Orion/images/Button.Collapse.gif";
        NPMNodeTree.GetTreeSection(resourceId, rootId, keys, 0, NPMNodeTreeSectionReceived, NPMNodeTreeSectionFailed, contentsDiv);
    }

    return false;
}

function FastNPMNodeTree_GetMore_Click(linkId, resourceId, keys, startFrom) {
    var contentsDiv = $get(linkId);
    contentsDiv.innerHTML = "@{R=NPM.Strings;K=NPMWEBJS_TM0_1;E=js}";
    NPMNodeTree.GetTreeSection(resourceId, linkId, keys, startFrom, NPMNodeTreeSectionReceived, NPMNodeTreeSectionFailed, contentsDiv);

    return false;
}
