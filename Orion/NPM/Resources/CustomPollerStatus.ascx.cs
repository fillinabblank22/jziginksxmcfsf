using System;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

using SolarWinds.NPM.Common;
using Limitation = SolarWinds.Orion.Web.Limitation;
using NPMWeb = SolarWinds.Orion.NPM.Web;
using System.Globalization;
using SolarWinds.NPM.Common.Formatters;
using SolarWinds.NPM.Common.Enums;
using SolarWinds.NPM.Web.UI;
using SolarWinds.ApiProxyFactory;

public partial class CustomPollerStatus : BaseResourceControl
{
    private static readonly Log log = new Log();
    private EResourceType _resourceType;
    protected String _previousGroupName;

    public enum EResourceType
    {
        Node,
        Interface
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_25; }
    }

    public String GetDefaultTitle()
    {
        return DefaultTitle;
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceUniversalDevicePollerStatus"; }
    }

    protected String SetPreviousGroupName(object groupName)
    {
        _previousGroupName = (String)groupName;
        return "";
    }

    public EResourceType ResourceType
    {
        get { return _resourceType; }
        set { _resourceType = value; }
    }

    public void MyBusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string autoHide = Convert.ToString(Resource.Properties["AutoHide"]);


        //Possibly override with Resource.Properties, but do not clear the current value
        //Why is this happening anyway?
        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            Resource.Title = Resource.Properties["Title"];
        }


        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            Resource.SubTitle = Resource.Properties["SubTitle"];
        }

        if (Resource.Title.Length == 0)
        {
            Resource.Title = DefaultTitle;
        }


        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        DataTable data = null;
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            int objectID;
            if (ResourceType == EResourceType.Node)
            {
                NPMWeb.Node node = this.GetInterfaceInstance<NPMWeb.INodeProvider>().Node;
                objectID = node != null ? node.NodeID : 0;
                try
                {
                    data = proxy.Api.GetNodeCustomPollerStatusResourceData(objectID, Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    MyBusinessLayerExceptionHandler(ex);
                    throw;
                }
            }
            else
            {
                NPMWeb.Interface itf = this.GetInterfaceInstance<NPMWeb.IInterfaceProvider>().Interface;
                objectID = itf != null ? itf.InterfaceID : 0;
                try
                {
                    data = proxy.Api.GetInterfaceCustomPollerStatusResourceData(objectID, Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    MyBusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            repeater.DataSource = FormatData(objectID, data, proxy);
        }
        if (autoHide == "1" && (repeater.DataSource == null || (repeater.DataSource as DataTable).Rows.Count == 0))
        {
            // Hide resource
            Visible = false;
            return;
        }

        repeater.DataBind();
    }

    public DataTable FormatData(int objectID, DataTable statusResourceDataTable, IApiProxy<INPMBusinessLayer> proxy)
    {
        statusResourceDataTable.Columns.AddRange(new DataColumn[] {
            new DataColumn("LinkTarget", typeof(String)),
            new DataColumn("DisplayPollerGroupName", typeof(String)),
            new DataColumn("DisplayCurrentValue", typeof(String)),
            new DataColumn("DisplayLastPolled", typeof(String))
            });

        foreach (DataRow row in statusResourceDataTable.Rows)
        {
            CustomPoller customPoller;
            try
            {
                customPoller = proxy.Api.GetCustomPoller(new Guid(Convert.ToString(row["CustomPollerID"])));
            }
            catch(Exception ex)
            {
                MyBusinessLayerExceptionHandler(ex);
                throw;
            }
            String unitSuffix = customPoller.UnitAbbv(true);

            if (!String.IsNullOrEmpty(Convert.ToString(row["PollerGroupName"])))
            {
                row["DisplayPollerGroupName"] = row["PollerGroupName"];
            }
            else
            {
                row["DisplayPollerGroupName"] = "&nbsp;";
            }

            if (!String.IsNullOrEmpty(Convert.ToString(row["CurrentValue"])))
            {
                // S = Raw value
                if (Convert.ToString(row["PollerType"]) == "S")
                {
                    string rawValue = Convert.ToString(row["CurrentValue"]);
                    row["DisplayCurrentValue"] = UndpHelper.FormatRawValue(rawValue, customPoller, CultureInfo.CurrentCulture);
                }
                else
                {
                    double currentValue;
                    if (Double.TryParse(row["CurrentValue"].ToString(), System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out currentValue))
                    {
                        // SQLResource.GetProperty("CurrentValue as CustomPollerValue")
                        row["DisplayCurrentValue"] = Utils.ConvertToMB(currentValue, "", false) + unitSuffix;
                    }
                    else
                    {
                        row["DisplayCurrentValue"] = Convert.ToString(row["CurrentValue"]) + unitSuffix;
                    }
                }
            }
            else
            {
                row["DisplayCurrentValue"] = String.Format("<font color=\"Red\">{0}</font>", Resources.NPMWebContent.NPMWEBCODE_VB0_70);
            }

            if (!String.IsNullOrEmpty(Convert.ToString(row["LastPolled"])))
            {
                // SQLResource.GetProperty("LastPolled as NextPoll")
                DateTime dateTime = Convert.ToDateTime(row["LastPolled"]).ToLocalTime();
                if (dateTime.Date != DateTime.Now.Date)
                {
                    row["DisplayLastPolled"] = dateTime.ToShortDateString() + " " + dateTime.ToShortTimeString();
                }
                else
                {
                    row["DisplayLastPolled"] = dateTime.ToShortTimeString();
                }
            }
            else
            {
                row["DisplayLastPolled"] = "&nbsp;";
            }

            string chartName;
            if (Convert.ToString(row["NetObjectPrefix"]) == "I")
            {
                chartName = "CustomPollerChart_Interface";
            }
            else
            {
                chartName = "CustomPollerChart_Node";
            }

            String linkTarget = String.Format("/Orion/Charts/CustomChart.aspx?ChartName={0}", chartName);


            linkTarget += "&rpCustomPollerID=" + row["CustomPollerID"] + "&Rows=" + "&SubsetColor=FF0000" + "&NetObject=" + Convert.ToString(row["NetObjectPrefix"]) + ":" + objectID;
            linkTarget += "&ChartTitle=${CustomPollerName}&ChartSubTitle=${ZoomRange}";
            row["LinkTarget"] = linkTarget;
        }
        return statusResourceDataTable;
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
