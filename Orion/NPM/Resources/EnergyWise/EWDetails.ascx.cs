using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.NPM.Web.EnergyWise;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Resources_EnergyWise_EWDetails : EnergyWiseResourceBase
{
	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceEnergyWiseNodeDetails"; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	// ========== PROTECTED ========== //

	protected void Page_Load(object sender, EventArgs e)
	{
		LoadData();
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VB0_72; }
	}

	protected override void LoadData()
	{
		var swqlParams = new Dictionary<string, object> { { "nodeid", NodeID } };
		DataTable ewDeviceTable;
		using (var swis = InformationServiceProxy.CreateV3())
		{
			ewDeviceTable =
			swis.Query(
				@"
SELECT e.DomainName, e.MaximumImportance, e.NumberOfNeighbors, isEwEnabled
FROM Orion.NPM.EW.DEVICE  as e
where (e.nodeid = @nodeid) and (e.isEwEnabled = 1)
", swqlParams);
			if (ewDeviceTable.Rows.Count > 0)
			{
				this.IsNCMInstalled = SolarWinds.Orion.Web.OrionModuleManager.IsModuleVersionAtLeast("NCM", "5.5");
				if (this.IsNCMInstalled)
				{
					AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBDATA_VB0_109, String.Format("<a href=\"/Orion/NCM/Resources/Energywise/EWNodes/ManageEWNodes.aspx?Nodes=" + NodeID + "&returnto=" + Request.Path + "\"><img src=\"/Orion/Nodes/images/icons/icon_edit.gif\" alt=\"\" /> {0}</a>", Resources.NPMWebContent.NPMWEBCODE_VB0_67)));
				}
				AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBCODE_VB0_79, WebSecurityHelper.HtmlEncode(ewDeviceTable.Rows[0]["DomainName"].ToString())));
				// It is not currently implemented by Cisco. Feature for the future development.
				//AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBCODE_VB0_80, WebSecurityHelper.HtmlEncode(ewDevice.PS.MaximumImportanceEntityName)));
				AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBCODE_VB0_81, WebSecurityHelper.HtmlEncode(ewDeviceTable.Rows[0]["MaximumImportance"].ToString())));
				AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBCODE_VB0_82, WebSecurityHelper.HtmlEncode(ewDeviceTable.Rows[0]["NumberOfNeighbors"].ToString())));
				AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBDATA_VB0_108, Resources.NPMWebContent.NPMWEBCODE_VB0_83));
			}
			else if ((IsEWDevice(NodeID, swis)) &&
					 (!IsEWEnabled(NodeID, swis)))
			{
				this.IsNCMInstalled = SolarWinds.Orion.Web.OrionModuleManager.IsModuleVersionAtLeast("NCM", "5.5");
				if (this.IsNCMInstalled)
				{
					AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBDATA_VB0_109, String.Format("<a href=\"/Orion/NCM/Resources/Energywise/EWNodes/ManageEWNodes.aspx?Nodes=" + NodeID + "&returnto=" + Request.Path + "\"><img src=\"/Orion/Nodes/images/icons/icon_edit.gif\" alt=\"\" /> {0}</a>", Resources.NPMWebContent.NPMWEBCODE_VB0_67)));
				}
				AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBDATA_VB0_108, Resources.NPMWebContent.NPMWEBCODE_VB0_111));
			}
			else
			{
				this.Visible = false;
			}
		}
	}

	/// <summary>
	/// Gets/Sets if the NCM is installed.
	/// </summary>
	public bool IsNCMInstalled
	{
		get
		{
			return (this.m_isNCMInstalled);
		}
		set
		{
			this.m_isNCMInstalled = value;
		}
	}
	/// <summary>
	/// Data member of IsNCMInstalled property.
	/// </summary>
	protected bool m_isNCMInstalled = false;


	private void AddTableRow(HtmlTable table, HtmlTableRow row)
	{
		if (row != null)
		{
			table.Rows.Add(row);
		}
	}

	private HtmlTableRow NewTableRow(String PropertyName, String PropertyValue)
	{
		// Empty properties are not showed in table.
		if (String.IsNullOrEmpty(PropertyValue.Trim()))
		{
			return (null);
		}
		var row = new HtmlTableRow();
		HtmlTableCell cell = new HtmlTableCell();
		cell.Attributes["class"] = "PropertyHeader NPM_PropertyHeader";
		cell.InnerHtml = PropertyName;
		row.Cells.Add(cell);
		cell = new HtmlTableCell();
		cell.Attributes["class"] = "Property";
		cell.InnerHtml = PropertyValue;
		row.Cells.Add(cell);
		return (row);
	}
}
