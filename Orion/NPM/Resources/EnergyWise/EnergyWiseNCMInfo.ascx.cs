﻿#region USING
using System;
using SolarWinds.Orion.Web.UI;

#endregion

public partial class Orion_NetPerfMon_Controls_EnergyWise_EnergyWiseNCMInfo : BaseResourceControl
{
    // ========== PUBLIC ======== //

    #region public void SubmitClick(object sender, EventArgs e)
    public void SubmitClick(object sender, EventArgs e)
    {
        this.RemoveThis();

        // redirect backward
        String url = string.Format("/Orion/SummaryView.aspx?ViewKey={0}", Request.QueryString["ViewKey"]);

        this.Response.Redirect(url);
    }
    #endregion


    #region PROPERTIES

    #region public override String HelpLinkFragment
    /// <summary>
    /// Gets the HelpLinkFragment of the EnergyWiseNCMInfo
    /// </summary>
    /// <value></value>
    public override String HelpLinkFragment
    {
        get
        {
            return ("OrionPHResourceEnergyWiseNCMInfo");
        }
    }
    #endregion

    #endregion


    // ========== PROTECTED ======== //

    #region protected void Page_Load(object sender, EventArgs e)
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Web.OrionModuleManager.IsModuleVersionAtLeast("NCM", "5.5"))
        {
            this.Visible = false;
        }
    }
    #endregion


    #region PROPERTIES

    #region protected override String DefaultTitle
    /// <summary>
    /// Gets the DefaultTitle of the EnergyWiseNCMInfo
    /// </summary>
    /// <value></value>
    protected override String DefaultTitle
    {
        get
        {
            return (Resources.NPMWebContent.NPMWEBCODE_VB0_71);
        }
    }
    #endregion

    #endregion


    // ========== PRIVATE ======== //

    #region private void RemoveThis()
    private void RemoveThis()
    {
        SolarWinds.Orion.Web.ResourceManager.DeleteById(this.Resource.ID);
    }
    #endregion

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Static; } }

}
