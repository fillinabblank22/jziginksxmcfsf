<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EWWeekGrid.ascx.cs" Inherits="EWWeekGrid" %>
<%@ Register Src="~/Orion/NPM/Controls/EnergyWise/EWTimeCell.ascx" TagPrefix="orion" TagName="EWTimeCell" %>
<%@ Register Src="~/Orion/NPM/Controls/EnergyWise/EWWeekDayHeader.ascx" TagPrefix="orion" TagName="EWDayHead" %>
<%@ Register Src="~/Orion/NPM/Controls/EnergyWise/EWPolicyLegend.ascx" TagPrefix="orion" TagName="EWPolicyLegend" %>
<%@ Register Src="~/Orion/NPM/Controls/EnergyWise/EWWeekSelection.ascx" TagPrefix="orion" TagName="EWWeekSelection" %>
<orion:Include runat="server" File="../NPM/styles/EWStyle.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table cellpadding="0" cellspacing="0" class="EW">
            <tr>
                <td align="center" style="border-bottom: none !important; padding: 3px;">
                   <%= Resources.NPMWebContent.NPMWEBCODE_VB0_63 %> <orion:EWWeekSelection ID="weekSelection" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="border-bottom: none !important; padding: 3px;">
                    <table cellpadding="0" cellspacing="0" class="ewcalendar">
                        <tr>
                            <td class="ewtimecell">&nbsp;</td>
                            <td class="ewhead">
                                <orion:EWDayHead ID="ewday0" runat="server" />
                            </td>
                            <td class="ewhead">
                                <orion:EWDayHead ID="ewday1" runat="server" />
                            </td>
                            <td class="ewhead">
                                <orion:EWDayHead ID="ewday2" runat="server" />
                            </td>
                            <td class="ewhead">
                                <orion:EWDayHead ID="ewday3" runat="server" />
                            </td>
                            <td class="ewhead">
                                <orion:EWDayHead ID="ewday4" runat="server" />
                            </td>
                            <td class="ewhead">
                                <orion:EWDayHead ID="ewday5" runat="server" />
                            </td>
                            <td class="ewhead">
                                <orion:EWDayHead ID="ewday6" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell1" Hour="1" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_0" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_0" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_0" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_0" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_0" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_0" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_0" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell2" Hour="2" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_1" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_1" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_1" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_1" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_1" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_1" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell3" Hour="3" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_2" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_2" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_2" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_2" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_2" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_2" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_2" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell4" Hour="4" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_3" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_3" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_3" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_3" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_3" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_3" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_3" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell5" Hour="5" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_4" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_4" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_4" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_4" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_4" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_4" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_4" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell6" Hour="6" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_5" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_5" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_5" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_5" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_5" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_5" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_5" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell7" Hour="7" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_6" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_6" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_6" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_6" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_6" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_6" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_6" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell8" Hour="8" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_7" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_7" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_7" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_7" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_7" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_7" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_7" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell9" Hour="9" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_8" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_8" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_8" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_8" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_8" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_8" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_8" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell10" Hour="10" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_9" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_9" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_9" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_9" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_9" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_9" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_9" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell11" Hour="11" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_10" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_10" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_10" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_10" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_10" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_10" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_10" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell12" Hour="12" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_11" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_11" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_11" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_11" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_11" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_11" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_11" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell13" Hour="13" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_12" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_12" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_12" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_12" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_12" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_12" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_12" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell14" Hour="14" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_13" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_13" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_13" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_13" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_13" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_13" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_13" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell15" Hour="15" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_14" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_14" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_14" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_14" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_14" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_14" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_14" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell16" Hour="16" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_15" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_15" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_15" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_15" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_15" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_15" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_15" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell17" Hour="17" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_16" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_16" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_16" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_16" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_16" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_16" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_16" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell18" Hour="18" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_17" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_17" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_17" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_17" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_17" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_17" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_17" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell19" Hour="19" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_18" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_18" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_18" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_18" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_18" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_18" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_18" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell20" Hour="20" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_19" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_19" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_19" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_19" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_19" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_19" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_19" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell21" Hour="21" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_20" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_20" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_20" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_20" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_20" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_20" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_20" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell22" Hour="22" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_21" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_21" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_21" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_21" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_21" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_21" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_21" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell23" Hour="23" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_22" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_22" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_22" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_22" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_22" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_22" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_22" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ewtimecell">
                                <orion:EWTimeCell ID="EWTimeCell24" Hour="24" runat="server" />
                            </td>

                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew0_23" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew1_23" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew2_23" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew3_23" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew4_23" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew5_23" runat="server" />
                            </td>
                            <td class="ewcell">
                                <orion:EWTimeCell ID="ew6_23" runat="server" />
                            </td>
                        </tr>
						<tr>
                            <td class="ewtimecell">
                                &nbsp;
                            </td>
                            <td colspan="7" style="border-bottom: none !important;">
                                <orion:EWPolicyLegend ID="policyLegend" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
