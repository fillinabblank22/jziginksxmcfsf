﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EnergyWiseNCMInfo.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EnergyWise_EnergyWiseNCMInfo" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div style="background-color: white; padding: 0px; text-align: center;">
	        <table cellpadding="0" cellspacing="0" border="0" style="padding: 0px;margin:10px; text-align: left; width: 100%;">
		        <tr>
			        <td style="padding: 0px; background-image: url('/Orion/NPM/images/EnergyWise/image_left.png'); background-repeat: no-repeat; width: 183px; height: 142px; border-bottom: none;">&nbsp;
			        </td>
			        <td style="background-color: white;border-bottom: none;">
			       <%=Resources.NPMWebContent.NPMWEBCODE_VB0_75%>
        			
				        <ul style="list-style-type: disc;">
					        <li><%=Resources.NPMWebContent.NPMWEBCODE_VB0_76%></li>
					        <li><%=Resources.NPMWebContent.NPMWEBCODE_VB0_77%></li>
					        <li><%= Resources.NPMWebContent.NPMWEBCODE_VB0_78%></li>
				        </ul>
                        <div class="sw-btn-bar" style="float: right;">
                            <orion:LocalizableButtonLink NavigateUrl='<%# Resources.CoreWebContent.SolarWindsComLocUrl + "products/orion/configuration_manager/" %>' LocalizedText="CustomText" runat="server" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_204%>" DisplayType="Primary" />
                            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="CustomText" Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_27%>" OnClick="SubmitClick" DisplayType="Secondary" />
                        </div>
			        </td>
			        <td style="padding: 0px; background-image: url('/Orion/NPM/images/EnergyWise/image_right.gif'); background-repeat: no-repeat; width: 11px; height: 142px;border-bottom: none;">&nbsp;
			        </td>
		        </tr>
	        </table>
        </div>
    </Content>
</orion:resourceWrapper>