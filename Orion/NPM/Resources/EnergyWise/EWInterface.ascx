﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EWInterface.ascx.cs" Inherits="Orion_Resources_EnergyWise_EWInterface" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="orion" TagName="OverridePowerLevel" Src="~/Orion/Controls/OverridePowerLevel.ascx" %>

<orion:Include runat="server" File="Dialog.css" />

<style type="text/css"> 
    .NPM_PropertyHeader
    {   
    	padding-left: 17px;
    	vertical-align: top;
    	width: 55%;
    }
</style>

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
	    <orion:OverridePowerLevel runat="server" />
	    
        <table id="OutputTable" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" class="NeedsZebraStripes">
        </table>
        
        <div style="padding: 5px 5px;">
	    <asp:Label ID="ErrorString" Visible="false" runat="server"></asp:Label>
	    </div>

    </Content>
</orion:resourceWrapper>
