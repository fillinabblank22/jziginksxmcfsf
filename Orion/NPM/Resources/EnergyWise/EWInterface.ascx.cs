using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using SolarWinds.NPM.Web.DAL;
using SolarWinds.NPM.Web.EnergyWise;
using SolarWinds.NPM.Web.EnergyWise.Models;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;



public partial class Orion_Resources_EnergyWise_EWInterface : EnergyWiseResourceBase
{
	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceEnergyWiseInterfaceDetails"; }
	}



	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new [] { typeof(IInterfaceProvider) }; }
	}



	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VB0_66; }
	}

	protected override void LoadData()
	{
		var iface = Interface;

		EWInterface wRow;
		PowerLevel policyState;

		using (var swql = InformationServiceProxy.CreateV3())
		{
			try
			{
				var row =
					swql.Query(
						"SELECT ID, InterfaceIndex,EnergyWiseCurrentLevel,EnergyWiseEntityImportance,EnergyWiseKeywords,EnergyWiseFullName FROM Orion.NPM.EW.Entity WHERE NodeID=@nodeid AND InterfaceIndex=@ifindex",
						new Dictionary<string, object>
							{
								{"nodeid", iface.NodeID},
								{"ifindex", iface.InterfaceIndex}
							}
						);

				if (row.Rows.Count == 0)
				{
					this.Visible = false;
					return;
				}

				wRow = new EWInterface(row.Rows[0]);
			}
			catch
			{
				throw;
			}
			var dal = new EnergyWiseDAL();
			policyState = dal.GetLastTransitionFromBeforeDate(swql, DateTime.Now, wRow.ID);
		}

		var isNCMInstalled = SolarWinds.Orion.Web.OrionModuleManager.IsModuleVersionAtLeast("NCM", "5.5");

		var powerLevel = (int)wRow.CurrentLevel >= 0 ? "<span style=\"background-image:url(" + GetPowerLevelIconImage(wRow.CurrentLevel) + ");background-repeat:no-repeat;padding:2px 0px 2px 30px;\">" + (int)wRow.CurrentLevel + " " + GetLocalizedEnergyWiseLevel(wRow.CurrentLevel) + "</span>" : string.Empty;
		var policyLevel = "<span style=\"background-image:url(" + GetPowerLevelIconImage(policyState) + ");background-repeat:no-repeat;padding:2px 0px 2px 30px;\">" + Convert.ToInt32(policyState) + " " + GetLocalizedEnergyWiseLevel(policyState) + "</span>";

		if (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{
			string manageNCMHtml = isNCMInstalled ? "<a href=\"/Orion/NCM/Resources/EnergyWise/EWInterfaces/ManageEWInterfaces.aspx?Interfaces=" + this.Interface.InterfaceID + "&returnto=" + Request.Path + String.Format("\"><img src=\"/Orion/Nodes/images/icons/icon_edit.gif\" alt=\"\" /> {0}</a><br />", Resources.NPMWebContent.NPMWEBCODE_VB0_67) : String.Empty;
			string setPowerLevelHtml = String.Format("<a href='#' onclick=\"return showPowerLevelDialog(['{0}'],'{1}',true);\"><img src=\"/Orion/NPM/images/EnergyWise/EW_icon_16x16.png\" alt=\"{2}\" /> {3}</a>", this.Interface.InterfaceID, Resources.NPMWebContent.NPMWEBCODE_VB0_98, Resources.NPMWebContent.NPMWEBCODE_VB0_99, Resources.NPMWebContent.NPMWEBCODE_VB0_100);
			AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBDATA_VB0_109, manageNCMHtml + setPowerLevelHtml));
		}

		AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBDATA_VB0_116, wRow.EnergyWiseFullName));
		AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBCODE_VB0_95, powerLevel));
		AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBCODE_VB0_96, policyLevel));
		string keywords = string.Empty;
		if (!string.IsNullOrEmpty(wRow.EnergyWiseKeywords))
		{
			var sb = new System.Text.StringBuilder();
			foreach (string word in wRow.EnergyWiseKeywords.Split(",".ToCharArray()))
			{
				sb.Append(System.Web.HttpUtility.HtmlEncode(word));
				sb.Append("<br />");
			}
			keywords = sb.ToString();
		}

		AddTableRow(OutputTable, NewTableRow(Resources.NPMWebContent.NPMWEBCODE_VB0_97, keywords));
	}


	private static void AddTableRow(HtmlTable table, HtmlTableRow row)
	{
		if (row != null)
		{
			table.Rows.Add(row);
		}
	}

	private static HtmlTableRow NewTableRow(string property, string value)
	{
		// Empty properties are not showed in table.

		var row = new HtmlTableRow();

		HtmlTableCell cell = new HtmlTableCell();
		cell.Attributes["class"] = "PropertyHeader NPM_PropertyHeader";
		cell.InnerHtml = property;
		row.Cells.Add(cell);

		cell = new HtmlTableCell();
		cell.Attributes["class"] = "Property";
		cell.InnerHtml = String.IsNullOrEmpty(value) ? "&nbsp;" : value;
		row.Cells.Add(cell);

		return (row);
	}
}
