using System;
using System.Collections.Generic;
using SolarWinds.NPM.Web.DAL;
using SolarWinds.NPM.Web.EnergyWise;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;


public partial class EWWeekGrid : EnergyWiseResourceBase
{

	private const int NumberOfHoursInDay = 24;
	private EnergyWiseDAL _dal = new EnergyWiseDAL();

	/// <summary>
	/// Sets Week
	/// </summary>
	/// <param name="day"></param>
	public void SetWeek(DateTime day)
	{
		this.ewday0.Day = GetFirstDayWeek(day);
		this.ewday1.Day = this.ewday0.Day.AddDays(1);
		this.ewday2.Day = this.ewday1.Day.AddDays(1);
		this.ewday3.Day = this.ewday2.Day.AddDays(1);
		this.ewday4.Day = this.ewday3.Day.AddDays(1);
		this.ewday5.Day = this.ewday4.Day.AddDays(1);
		this.ewday6.Day = this.ewday5.Day.AddDays(1);
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceEnergyWisePolicyOverviewCalendar"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public DateTime SelectedDay
	{
		get { return this.weekSelection.CurrentSelection; }
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VB0_62; }
	}

	protected override void LoadData()
	{
		Initialize();
		DoLoadData();
		if ((!this.IsPostBack) && Visible)
		{
			SetWeek(SelectedDay);
			SetLevels();
		}
	}

	private void LoadEnergyWiseLevelMatrixForWeek(DateTime weekDay, int entityId, InformationServiceProxy swql)
	{
		
		_levels = new PowerLevel[7,24];

		DateTime start = FirstDayOfWeek(weekDay);
		DateTime end = LastDayOfWeek(weekDay);

		
		// Since the week may not have our initial transition, we need to look at the last transition and initialize our 
		// arrays to that value.

		var data = _dal.LoadDataForTransitions(swql, entityId);

		var initialLevel = _dal.GetLastTransitionFromBeforeDate(swql, start, entityId, data);

		for (int d = 0; d < 7; d++)
		{

			for (int h = 0; h < 24; h++)
			{
				_levels[d,h] = initialLevel;
			}
		}


		var transitions = _dal.GetScheduleListForTimeRange(data, start, end);
		var transition = transitions.First;

		while (transition != null)
		{
			int day = (int)transition.Value.DateTime.DayOfWeek;
			int hour = transition.Value.DateTime.Hour;
			int nextDay = (int)DayOfWeek.Saturday;
			int nextHour = 24;
			if (transition.Next != null)
			{
				nextDay = (int)transition.Next.Value.DateTime.DayOfWeek;
				nextHour = transition.Next.Value.DateTime.Hour;
			}
			for (int d = day; d <= nextDay; d++)
			{
				int startingHour = d == day ? hour : 0;
				int endingHour = d == nextDay ? nextHour : 24;
				for (int h = startingHour; h < endingHour; h++)
				{
					_levels[d,h] = transition.Value.Level;
				}
			}
			transition = transition.Next;
		}
	}

	private void DoLoadData()
	{
		using (var swql = InformationServiceProxy.CreateV3())
		{
			var data = swql.Query("SELECT ID FROM Orion.NPM.EW.Entity WHERE InterfaceIndex=@ifindex AND NodeID=@nodeid",
			           new Dictionary<string, object> {{"ifindex", Interface.InterfaceIndex}, {"nodeid", Interface.NodeID}}
				);


			// test if there is just one entity
			if (data.Rows.Count == 1)
			{
				LoadEnergyWiseLevelMatrixForWeek(SelectedDay, Convert.ToInt32(data.Rows[0][0]), swql);
				SetLevels();
			}
			else
			{
				this.Visible = false;
			}
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
		Wrapper.ManageButtonTarget = "/Orion/NCM/Resources/EnergyWise/EWInterfaces/ManageEWInterfaces.aspx?Interfaces=" + this.Interface.InterfaceID + "&returnto=" + Request.Path;
		Wrapper.ShowManageButton = Profile.AllowNodeManagement && SolarWinds.Orion.Web.OrionModuleManager.IsModuleVersionAtLeast("NCM", "5.5");
	}

	// ========== PRIVATE ======== //    
	//Members
	private EWTimeCell[,] _levelMap;
	private PowerLevel[,] _levels;


	//Methods

	/// <summary>
	/// Initialize this page
	/// </summary>
	private void Initialize()
	{
		_levelMap = new EWTimeCell[7,24];

		_levelMap[0,0] = ew0_0;
		_levelMap[1,0] = ew1_0;
		_levelMap[2,0] = ew2_0;
		_levelMap[3,0] = ew3_0;
		_levelMap[4,0] = ew4_0;
		_levelMap[5,0] = ew5_0;
		_levelMap[6,0] = ew6_0;
		_levelMap[0,1] = ew0_1;
		_levelMap[1,1] = ew1_1;
		_levelMap[2,1] = ew2_1;
		_levelMap[3,1] = ew3_1;
		_levelMap[4,1] = ew4_1;
		_levelMap[5,1] = ew5_1;
		_levelMap[6,1] = ew6_1;
		_levelMap[0,2] = ew0_2;
		_levelMap[1,2] = ew1_2;
		_levelMap[2,2] = ew2_2;
		_levelMap[3,2] = ew3_2;
		_levelMap[4,2] = ew4_2;
		_levelMap[5,2] = ew5_2;
		_levelMap[6,2] = ew6_2;
		_levelMap[0,3] = ew0_3;
		_levelMap[1,3] = ew1_3;
		_levelMap[2,3] = ew2_3;
		_levelMap[3,3] = ew3_3;
		_levelMap[4,3] = ew4_3;
		_levelMap[5,3] = ew5_3;
		_levelMap[6,3] = ew6_3;
		_levelMap[0,4] = ew0_4;
		_levelMap[1,4] = ew1_4;
		_levelMap[2,4] = ew2_4;
		_levelMap[3,4] = ew3_4;
		_levelMap[4,4] = ew4_4;
		_levelMap[5,4] = ew5_4;
		_levelMap[6,4] = ew6_4;
		_levelMap[0,5] = ew0_5;
		_levelMap[1,5] = ew1_5;
		_levelMap[2,5] = ew2_5;
		_levelMap[3,5] = ew3_5;
		_levelMap[4,5] = ew4_5;
		_levelMap[5,5] = ew5_5;
		_levelMap[6,5] = ew6_5;
		_levelMap[0,6] = ew0_6;
		_levelMap[1,6] = ew1_6;
		_levelMap[2,6] = ew2_6;
		_levelMap[3,6] = ew3_6;
		_levelMap[4,6] = ew4_6;
		_levelMap[5,6] = ew5_6;
		_levelMap[6,6] = ew6_6;
		_levelMap[0,7] = ew0_7;
		_levelMap[1,7] = ew1_7;
		_levelMap[2,7] = ew2_7;
		_levelMap[3,7] = ew3_7;
		_levelMap[4,7] = ew4_7;
		_levelMap[5,7] = ew5_7;
		_levelMap[6,7] = ew6_7;
		_levelMap[0,8] = ew0_8;
		_levelMap[1,8] = ew1_8;
		_levelMap[2,8] = ew2_8;
		_levelMap[3,8] = ew3_8;
		_levelMap[4,8] = ew4_8;
		_levelMap[5,8] = ew5_8;
		_levelMap[6,8] = ew6_8;
		_levelMap[0,9] = ew0_9;
		_levelMap[1,9] = ew1_9;
		_levelMap[2,9] = ew2_9;
		_levelMap[3,9] = ew3_9;
		_levelMap[4,9] = ew4_9;
		_levelMap[5,9] = ew5_9;
		_levelMap[6,9] = ew6_9;
		_levelMap[0,10] = ew0_10;
		_levelMap[1,10] = ew1_10;
		_levelMap[2,10] = ew2_10;
		_levelMap[3,10] = ew3_10;
		_levelMap[4,10] = ew4_10;
		_levelMap[5,10] = ew5_10;
		_levelMap[6,10] = ew6_10;
		_levelMap[0,11] = ew0_11;
		_levelMap[1,11] = ew1_11;
		_levelMap[2,11] = ew2_11;
		_levelMap[3,11] = ew3_11;
		_levelMap[4,11] = ew4_11;
		_levelMap[5,11] = ew5_11;
		_levelMap[6,11] = ew6_11;
		_levelMap[0,12] = ew0_12;
		_levelMap[1,12] = ew1_12;
		_levelMap[2,12] = ew2_12;
		_levelMap[3,12] = ew3_12;
		_levelMap[4,12] = ew4_12;
		_levelMap[5,12] = ew5_12;
		_levelMap[6,12] = ew6_12;
		_levelMap[0,13] = ew0_13;
		_levelMap[1,13] = ew1_13;
		_levelMap[2,13] = ew2_13;
		_levelMap[3,13] = ew3_13;
		_levelMap[4,13] = ew4_13;
		_levelMap[5,13] = ew5_13;
		_levelMap[6,13] = ew6_13;
		_levelMap[0,14] = ew0_14;
		_levelMap[1,14] = ew1_14;
		_levelMap[2,14] = ew2_14;
		_levelMap[3,14] = ew3_14;
		_levelMap[4,14] = ew4_14;
		_levelMap[5,14] = ew5_14;
		_levelMap[6,14] = ew6_14;
		_levelMap[0,15] = ew0_15;
		_levelMap[1,15] = ew1_15;
		_levelMap[2,15] = ew2_15;
		_levelMap[3,15] = ew3_15;
		_levelMap[4,15] = ew4_15;
		_levelMap[5,15] = ew5_15;
		_levelMap[6,15] = ew6_15;
		_levelMap[0,16] = ew0_16;
		_levelMap[1,16] = ew1_16;
		_levelMap[2,16] = ew2_16;
		_levelMap[3,16] = ew3_16;
		_levelMap[4,16] = ew4_16;
		_levelMap[5,16] = ew5_16;
		_levelMap[6,16] = ew6_16;
		_levelMap[0,17] = ew0_17;
		_levelMap[1,17] = ew1_17;
		_levelMap[2,17] = ew2_17;
		_levelMap[3,17] = ew3_17;
		_levelMap[4,17] = ew4_17;
		_levelMap[5,17] = ew5_17;
		_levelMap[6,17] = ew6_17;
		_levelMap[0,18] = ew0_18;
		_levelMap[1,18] = ew1_18;
		_levelMap[2,18] = ew2_18;
		_levelMap[3,18] = ew3_18;
		_levelMap[4,18] = ew4_18;
		_levelMap[5,18] = ew5_18;
		_levelMap[6,18] = ew6_18;
		_levelMap[0,19] = ew0_19;
		_levelMap[1,19] = ew1_19;
		_levelMap[2,19] = ew2_19;
		_levelMap[3,19] = ew3_19;
		_levelMap[4,19] = ew4_19;
		_levelMap[5,19] = ew5_19;
		_levelMap[6,19] = ew6_19;
		_levelMap[0,20] = ew0_20;
		_levelMap[1,20] = ew1_20;
		_levelMap[2,20] = ew2_20;
		_levelMap[3,20] = ew3_20;
		_levelMap[4,20] = ew4_20;
		_levelMap[5,20] = ew5_20;
		_levelMap[6,20] = ew6_20;
		_levelMap[0,21] = ew0_21;
		_levelMap[1,21] = ew1_21;
		_levelMap[2,21] = ew2_21;
		_levelMap[3,21] = ew3_21;
		_levelMap[4,21] = ew4_21;
		_levelMap[5,21] = ew5_21;
		_levelMap[6,21] = ew6_21;
		_levelMap[0,22] = ew0_22;
		_levelMap[1,22] = ew1_22;
		_levelMap[2,22] = ew2_22;
		_levelMap[3,22] = ew3_22;
		_levelMap[4,22] = ew4_22;
		_levelMap[5,22] = ew5_22;
		_levelMap[6,22] = ew6_22;
		_levelMap[0,23] = ew0_23;
		_levelMap[1,23] = ew1_23;
		_levelMap[2,23] = ew2_23;
		_levelMap[3,23] = ew3_23;
		_levelMap[4,23] = ew4_23;
		_levelMap[5,23] = ew5_23;
		_levelMap[6,23] = ew6_23;

	}



	/// <summary>
	/// Set levels by Level Map
	/// </summary>
	private void SetLevels()
	{
		for (int i = 0; i <= (int)DayOfWeek.Saturday; i++)
		{
			for (int j = 0; j < NumberOfHoursInDay; j++)
			{
				_levelMap[i,j].Level = _levels[i,j];
			}
		}
	}


	
	private static DateTime GetFirstDayWeek(DateTime day)
	{
		return day.AddDays(-1 * (int)day.DayOfWeek);
	}

	private static DateTime FirstDayOfWeek(DateTime day)
	{
		if (day != DateTime.MinValue)
		{
			return day.Date.AddDays(-1 * (int)day.DayOfWeek).AddSeconds(-1);
		}
		return day;
	}

	private static DateTime LastDayOfWeek(DateTime day)
	{
		return FirstDayOfWeek(day).AddDays(7);
	}
}
