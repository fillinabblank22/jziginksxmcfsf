using System;
using System.Data;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Common;
using System.Globalization;
using SolarWinds.NPM.Common.Enums;
using SolarWinds.NPM.Common.Formatters;
using SolarWinds.NPM.Web.UI;

public partial class CustomPollerSummaryStatus : BaseResourceControl
{
    public void MyBusinessLayerExceptionHandler(Exception ex)
    {
        SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
        log.Error(ex);
    }

    private bool _showDetails = false;
    private bool _showHelp = false;

    protected CustomPoller customPoller = null;

    protected bool ShowDetails
    {
        get { return _showDetails; }
    }

    protected bool ShowHelp
    {
        get { return _showHelp; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceHelpPage.EditURL = EditURL;
        if (String.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
        {
            _showHelp = true;
            return;
        }
        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            Guid customPollerID = new Guid(Resource.Properties["CustomPollerID"]);
            try
            {
                customPoller = proxy.Api.GetCustomPoller(customPollerID);
            }
            catch(Exception ex)
            {
                MyBusinessLayerExceptionHandler(ex);
                throw;
            }

            if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
            {
                Resource.Title = Resource.Properties["Title"];
            }
            else
            {
                Resource.Title = string.Empty;
            }
            if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
            {
                Resource.SubTitle = Resource.Properties["SubTitle"];
            }
            else
            {
                Resource.SubTitle = string.Empty;
            }

            if (customPoller != null)
            {
                if (string.IsNullOrEmpty(Resource.Title))
                {
                    Resource.Title = String.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_124, customPoller.UniqueName);
                }
            }

            if (Resource.Title.Length == 0)
            {
                Resource.Title = DefaultTitle;
            }

            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

            _showDetails = true;
            if (customPoller != null)
            {
                DataTable data;
                try
                {
                    data = proxy.Api.GetCustomPollerSummaryStatusResourceData(customPoller, Limitation.GetCurrentListOfLimitationIDs());
                }
                catch(Exception ex)
                {
                    MyBusinessLayerExceptionHandler(ex);
                    throw;
                }
                datailsTable.DataSource = FormatData(customPoller, data);
                datailsTable.DataBind();
            }
            else
            {
                _showHelp = true;
                return;
            }
        }
    }

    public DataTable FormatData(CustomPoller customPoller, DataTable statusSummaryResourceDataTable)
    {
        statusSummaryResourceDataTable.Columns.AddRange(new DataColumn[] {
            new DataColumn("LinkTarget", typeof(String)),
            new DataColumn("Caption", typeof(String)),
            new DataColumn("ObjectLinkTarget", typeof(String)),
            new DataColumn("DisplayCurrentStatus", typeof(String)),
            new DataColumn("DisplayLastPolled", typeof(String))
            });

        foreach (DataRow row in statusSummaryResourceDataTable.Rows)
        {
            String unitSuffix = customPoller.UnitAbbv(true);
            String objectLinkTarget = null;

            String netObjectPrefix = Convert.ToString(row["NetObjectPrefix"]);

            String linkTarget = "/Orion/Charts/CustomChart.aspx?ChartName=CustomPollerSummaryChart";
            String caption = Convert.ToString(row["NodeCaption"]);
            if (netObjectPrefix == "N")
            {
                String netObject = "&NetObject=" + netObjectPrefix + ":" + Convert.ToString(row["NodeID"]);
                linkTarget += "&rpCustomPollerID=" + customPoller.ID.ToString() + "&SubsetColor=FF0000" + netObject;
                objectLinkTarget = "/Orion/NetPerfMon/NodeDetails.aspx?" + netObject;
            }
            else if (netObjectPrefix == "I")
            {
                String netObject = "&NetObject=" + netObjectPrefix + ":" + Convert.ToString(row["InterfaceID"]);
                linkTarget += "&rpCustomPollerID=" + customPoller.ID.ToString() + "&SubsetColor=FF0000" + netObject;
                objectLinkTarget = "/Orion/NPM/InterfaceDetails.aspx?" + netObject;
                caption += " - " + Convert.ToString(row["InterfaceCaption"]);
            }

            if (!String.IsNullOrEmpty(Convert.ToString(row["CurrentStatus"])))
            {
                if (Convert.ToString(row["PollerType"]) == "S")
                {
                    string rawValue = Convert.ToString(row["CurrentStatus"]);
                    row["DisplayCurrentStatus"] = UndpHelper.FormatRawValue(rawValue, customPoller, CultureInfo.CurrentCulture);
                }
                else
                {
                    double currentStatus;
                    if (Double.TryParse(row["CurrentStatus"].ToString(), System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out currentStatus))
                    {
                        row["DisplayCurrentStatus"] = Utils.ConvertToMB(currentStatus, "", false) + unitSuffix;
                    }
                    else
                    {
                        row["DisplayCurrentStatus"] = Convert.ToString(row["CurrentStatus"]) + unitSuffix;
                    }
                }
            }
            else
            {
                row["DisplayCurrentStatus"] = "&nbsp;";
            }

            if (!String.IsNullOrEmpty(Convert.ToString(row["LastPolled"])))
            {
                // SQLResource.GetProperty("LastPolled as NextPoll")
                DateTime dateTime = Convert.ToDateTime(row["LastPolled"]).ToLocalTime();
                if (dateTime.ToShortDateString() != DateTime.Now.ToShortDateString())
                {
                    row["DisplayLastPolled"] = dateTime.ToShortDateString() + " " + dateTime.ToShortTimeString();
                }
                else
                {
                    row["DisplayLastPolled"] = dateTime.ToShortTimeString();
                }
            }
            else
            {
                row["DisplayLastPolled"] = "&nbsp;";
            }

            row["LinkTarget"] = linkTarget;
            row["Caption"] = caption;
            row["ObjectLinkTarget"] = objectLinkTarget;
        }
        return statusSummaryResourceDataTable;
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_125; }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/NPM/Resources/Summary/EditCustomPollerSummaryStatus.aspx?ResourceID={0}&ViewID={1}&NetObject={2}", this.Resource.ID, this.Resource.View.ViewID, Page.Request.QueryString["NetObject"]); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceUniversalDevicePollerSummaryStatus"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
