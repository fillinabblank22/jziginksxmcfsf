<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditCustomPollerSummaryStatus.aspx.cs" Inherits="EditCustomPollerSummaryStatus" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<%-- 
// TODO: Investigate 	SelectWeb "NetPerfMon" and VerifyLogin "CUSTOMIZE" functions

'
'   Opens the specified Web Database.
'
Public Function SelectWeb(ByVal Web As String) As Boolean
    'gTracing = True
    On Error Resume Next
    If Not MainDatabase.SelectWeb(Web) Then
        Response.Redirect "/Admin/CriticalError.asp?ErrorMessage=" & LastDatabaseError
        'Response.Write "<font color=red size=3><b>" & LastDatabaseError & "</b></font>"
        Response.End
    Else
        ' Login to correct Account based on info in Session object
        ReLoginSession
        SelectWeb = True
    End If
End Function

public bool SelectWeb(String Web) {
    if (!MainDatabase.SelectWeb(Web)) {
        Response.Redirect("/Admin/CriticalError.asp?ErrorMessage=" + LastDatabaseError);
        Response.End();
    {
    else {
        ' Login to correct Account based on info in Session object
        ReLoginSession();
        return true;
    }
}

Public Property Get LastDatabaseError() As String
    LastDatabaseError = MainDatabase.LastDatabaseError
End Property

Private Sub ReLoginSession()
    Dim AccountID As String
    Dim Password As String
    
    On Error Resume Next
    AccountID = Session("AccountID")
    Password = Session("Password")
    
    If Len(AccountID) > 0 Then
        If mCurrentAccount Is Nothing Then Set mCurrentAccount = New Account
        mCurrentAccount.AccountID = CStr(AccountID)
        mCurrentAccount.Refresh
        mCurrentAccount.Login (CStr(Password))
    End If
End Sub

	SelectWeb "NetPerfMon"
	
	VerifyLogin "CUSTOMIZE"
--%>

    <h1><%= string.Format(Resources.NPMWebContent.NPMWEBDATA_VB0_242, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"]))) %></h1>
    
    <div style="padding-left: 16px;">
        <table class="text" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="false" />    
                 <%=Resources.NPMWebContent.NPMWEBDATA_VB0_54%>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <b><%=Resources.NPMWebContent.NPMWEBDATA_VB0_55%></b>
                <br/>
                    <asp:DropDownList ID="customPollersDDL" runat="server" />
                <br/>
            </td>
        </tr>
        <tr>			
            <td colspan="2">
                <br />
                <div class="sw-btn-bar">
                	<orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />                    
                <div class="sw-btn-bar">

            </td>
        </tr>
        </table>
    </div>
</asp:Content>