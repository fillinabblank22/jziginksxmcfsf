﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using System.Text;
using SolarWinds.NPM.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;


public partial class Orion_NPM_Resources_VSANDetails_VSANMembers : ResourceControl
{
    private const string StatusLedImage = "<img src=\"/NetPerfMon/images/small-{0}\" border=\"0\" />";
    private const string IconImage = "<img src=\"/NetPerfMon/images/Interfaces/{0}\" border=\"0\" />";

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVSANProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int vsanID = GetInterfaceInstance<IVSANProvider>().VSAN.ID;

        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            DataTable members;
            try
            {
                members = proxy.Api.GetVSANMembers(vsanID, Limitation.GetCurrentListOfLimitationIDs());
            }
            catch(Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            if (members != null && members.Rows != null && members.Rows.Count > 0)
            {
                int nodeID = 0;
				int vsanIndex = 0;
                foreach (DataRow row in members.Rows)
                {
                    HtmlTableRow tableRow;
                    if(nodeID != (int)row["NodeID"])
                    {
                        nodeID = (int)row["NodeID"];

                        // create node row
                        tableRow = new HtmlTableRow();
                        var biggerCell = NewTableCell(NodeLink(row["NodeName"], row["NodeLED"], nodeID));
                        biggerCell.Attributes["colspan"] = "3";
                        tableRow.Cells.Add(biggerCell);
                        tableRow.Cells.Add(NewTableCell(FormatHelper.GetStatusText(Convert.ToInt32(row["NodeStatus"]))));
						//Wilburized Zebra stripe
						vsanIndex++;
						if(vsanIndex%2 == 0)
							tableRow.Attributes.Add("class", "ZebraStripe");							
                        this.OutputTable.Rows.Add(tableRow);
                    }
                    
                    // create interface row
                    tableRow = new HtmlTableRow();
                    tableRow.Attributes["class"] = (vsanIndex%2 == 0) ? String.Format("ZebraStripe Hidden Node_{0}", nodeID) :
																		String.Format("Hidden Node_{0}", nodeID);
                    tableRow.Cells.Add(NewTableCell("&nbsp;"));
                    tableRow.Cells.Add(NewTableCell(InterfaceLink(row["InterfaceName"], row["InterfaceLED"], row["InterfaceIcon"], row["InterfaceID"])));
                    tableRow.Cells.Add(NewTableCell(row["PhysicalNumber"]));
                    tableRow.Cells.Add(NewTableCell(FormatHelper.GetStatusText(Convert.ToInt32(row["InterfaceStatus"]))));
					
					//Set bgColor for all sub-interfaces equal to VSAN color					
                    this.OutputTable.Rows.Add(tableRow);
                }
            }
        }
    }

    private HtmlTableCell NewTableCell(object obj)
    {
        var cell = new HtmlTableCell();

        cell.Attributes["class"] = "Property";
        cell.InnerHtml = obj.ToString();

        return cell;
    }

    private string NodeLink(object name, object statusIcon, int nodeID)
    {
        return String.Format("<img id=\"Node_{1}\" src=\"/Orion/images/Button.Expand.gif\" onClick=\"toggle({1});\" /><a href=\"/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{1}\">{2}{0}</a>", name, nodeID, String.Format(StatusLedImage, trimSpaces(statusIcon)));
    }

    private string InterfaceLink(object name, object statusIcon, object interfaceIcon, object interfaceID)
    {
        return String.Format("<a href=\"/Orion/NPM/InterfaceDetails.aspx?NetObject=I:{1}\">{2}{3}&nbsp;{0}</a>", name, interfaceID, String.Format(StatusLedImage, trimSpaces(statusIcon)), String.Format(IconImage, trimSpaces(interfaceIcon)));
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_TM0_23; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceAllVSANMembers"; }
    }

    /// <summary>
    /// some strings are saved as char[n] in the database and then we need trim spaces
    /// </summary>
    /// <param name="text">text with trailing spaces</param>
    /// <returns>text without trailing spaces</returns>
    private string trimSpaces(object text)
    {
        if (text == null)
            return string.Empty;

        string str = text.ToString();

        if (string.IsNullOrEmpty(str))
                return string.Empty;
            else
                return str.Trim();        
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}