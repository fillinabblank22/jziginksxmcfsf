﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VSANDetails.ascx.cs" Inherits="Orion_NPM_Resources_VSANDetails_VSANDetails" %>


<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>

    <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailsTable" class="NeedsZebraStripes">
        <tr>          
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_32%></td>          
          <td class="Property"><asp:Image runat="server" ID="vsanStateIcon" style="margin-right: 3px;"/><asp:Label runat="server" ID="vsanState" /></td>
        </tr>
        <tr>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_116%></td>
          <td class="Property"><asp:Label runat="server" ID="vsanName" />&nbsp;</td>
        </tr>
        <tr>          
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_35%></td>          
          <td class="Property" colspan="2"><asp:Label runat="server" ID="vsanID" />&nbsp;</td>
        </tr>
        <tr>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_34%></td>
          <td class="Property"><asp:Label runat="server" ID="vsanMediaType" />&nbsp;</td>
        </tr>
        <tr>
          <td class="PropertyHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_33%></td>
          <td class="Property"><asp:Label runat="server" ID="vsanLoadBalancingType" />&nbsp;</td>
        </tr>
    </table>
    
</Content>
</orion:resourceWrapper>
