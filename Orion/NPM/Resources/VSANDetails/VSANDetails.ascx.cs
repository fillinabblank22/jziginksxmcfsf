﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using VSAN = SolarWinds.NPM.Common.Models.VSAN;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using System.Text;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Core.Common.i18n.Registrar;


public partial class Orion_NPM_Resources_VSANDetails_VSANDetails : ResourceControl
{
    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVSANProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int vsanID = GetInterfaceInstance<IVSANProvider>().VSAN.ID;

        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            VSAN vsan;
            try
            {
                vsan = proxy.Api.GetVSAN(vsanID);
            }
            catch(Exception ex)
            {
                BusinessLayerExceptionHandler(ex);
                throw;
            }

            if (vsan != null)
            {
                this.vsanState.Text = String.Format(Resources.NPMWebContent.NPMWEBCODE_TM0_27, GetLocalizedProperty("AdminState", vsan.AdminState.ToString()));
                this.vsanStateIcon.ImageUrl = VsanIcon(vsan.AdminState);
                this.vsanName.Text = vsan.Name;
                this.vsanID.Text = vsan.ID.ToString();
                this.vsanMediaType.Text = GetLocalizedProperty("VsanMediaType", vsan.MediaType.ToString());
                this.vsanLoadBalancingType.Text = GetLocalizedProperty("VsanLoadBalancingType", vsan.LoadBalancingType.ToString());
            }
        }
    }

    private string VsanIcon(VsanAdminState state)
    {
        var icon = new StringBuilder("/Orion/Images/StatusIcons/small-");

        switch (state)
        {
            case VsanAdminState.Active:
                icon.Append("up");
                break;

            case VsanAdminState.Suspended:
                icon.Append("unknown");
                break;

            default:
                return "&nbsp;";
        }

        return String.Format("{0}.gif", icon.ToString());
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_TM0_22; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVSANDetails"; }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}