﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveVSANAlerts.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_ActiveVSANAlerts" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<orion:resourceWrapper runat="server" ID="wrapper" ShowEditButton="True">
    <Content>
        <asp:Repeater runat="server" ID="alertsTable" OnItemDataBound="AlertItemDataBound">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
	                <tr>
		                <td class="ReportHeader" width="20">&nbsp;</td>
		                <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_30%></td>
		                <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_29%></td>
		                <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_27%></td>
		                <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_28%></td>
	                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr runat="server" id="groupHeader" visible="false">
					    <td class="PropertyHeader" colspan="10" valign="middle"><%# Eval("MonitoredProperty") %></td>
    				</tr>
                    <tr>
				        <td><img src="/NetPerfMon/images/Event-19.gif" border="0"></td>
				        <td valign="middle" width="80"><%# this.FormatTime(Eval("AlertTime")) %>&nbsp;</td>
				        <td valign="middle"><%# Eval("ObjectName") %></td>
				        <td valign="middle" style="color:Red;font-weight:bold;">&nbsp;</td>
				        <td valign="middle"><%# Eval("EventMessage")%>&nbsp;</td>
			        </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
