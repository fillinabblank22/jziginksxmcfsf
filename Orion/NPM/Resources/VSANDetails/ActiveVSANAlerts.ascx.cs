﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NodeDetails_ActiveVSANAlerts : ResourceControl
{
    private string monitoredProperty;
    private bool showAcknowledgedAlerts;

    protected override string DefaultTitle
    {
        get { return NPMWebContent.NPMWEBCODE_TM0_26; }
    }

    public override string SubTitle
    {
        get
        {
            if (showAcknowledgedAlerts)
            {
                return NPMWebContent.NPMWEBCODE_TM0_24;
            }
            return NPMWebContent.NPMWEBCODE_TM0_25;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (IVSANProvider)}; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceActiveVSANAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    private static String GetContentFetchingQuery(int? vsanId, bool showAcknowledged)
    {
        return String.Format(@"
            SELECT
                AlertActive.TriggeredDateTime AS AlertTime,
                AlertConfigurations.ObjectType AS ObjectType,
                AlertObjects.EntityCaption AS ObjectName,
                AlertConfigurations.Name AS EventMessage,
                '' AS ObjectID,
                '' AS CurrentValue,
                '{0}' AS MonitoredProperty
            FROM Orion.NPM.VSANs
            INNER JOIN Orion.AlertObjects ON AlertObjects.EntityUri = VSANs.Uri
            INNER JOIN Orion.AlertActive ON AlertActive.AlertObjectID = AlertObjects.AlertObjectID
            INNER JOIN Orion.AlertConfigurations ON AlertConfigurations.AlertID = AlertObjects.AlertID
            WHERE Enabled = 1 {1} {2}",
            NPMWebContent.NPMWEBDATA_JP0_1,
            vsanId.HasValue ? String.Format("AND VSANs.ID = {0}", vsanId.Value) : String.Empty,
            showAcknowledged ? String.Empty : "AND Acknowledged IS NULL"
            );
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Visible = NodeHelper.IsResourceUnderExternalNode(this) == false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        VSAN vsan = GetInterfaceInstance<IVSANProvider>().VSAN;
        if (vsan == null)
        {
            return;
        }

        string showAck = Resource.Properties["ShowAcknowledgedAlerts"];

        if (!String.IsNullOrEmpty(showAck) && showAck == "False")
        {
            showAcknowledgedAlerts = false;
        }
        else
        {
            showAcknowledgedAlerts = true;
        }

        BindDataSource(vsan.ID, showAcknowledgedAlerts);
    }

    private void BindDataSource(int? vsanId, bool showAcknowledged)
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            String query = GetContentFetchingQuery(vsanId, showAcknowledged);
            DataTable dataTable = swis.Query(query);
            if (dataTable == null || dataTable.Rows == null) return;

            alertsTable.DataSource = dataTable;
            alertsTable.DataBind();
        }
    }

    protected void AlertItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        {
            return;
        }

        var row = e.Item.DataItem as DataRowView;
        if (row == null) return;

        string currentMonitoredProperty = row["MonitoredProperty"].ToString();
        if (monitoredProperty != currentMonitoredProperty)
        {
            monitoredProperty = currentMonitoredProperty;
            var header = e.Item.FindControl("groupHeader");
            if (header != null)
            {
                header.Visible = true;
            }
        }
    }

    protected string FormatTime(object time)
    {
        return Utils.FormatDateTime((DateTime) time, true);
    }
}
