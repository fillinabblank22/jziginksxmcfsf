﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VSANMembers.ascx.cs" Inherits="Orion_NPM_Resources_VSANDetails_VSANMembers" %>

<orion:Include runat="server" File="../NPM/styles/VSANDetails.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>
    <script type="text/javascript" language="javascript">
        function toggle(nodeID) {
            var img = $("#Node_" + nodeID);
            if ($(img).hasClass("Expanded")) {
                $(img).attr("src", "/Orion/images/Button.Expand.gif");
            }
            else {
                $(img).attr("src", "/Orion/images/Button.Collapse.gif");
            }
            $(img).toggleClass("Expanded");
            $(".Node_" + nodeID).toggleClass("Hidden");
        }
    </script>
    
    <table id="OutputTable" runat="server" border="0" cellpadding="3" cellspacing="0" width="100%">
        <tr style="text-align:left;">
	        <th class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_37%></th>
	        <th class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBCODE_VB0_6%></th>
	        <th class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_31%></th>
	        <th class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_231%></th>
        </tr>
    </table>    
    
</Content>
</orion:resourceWrapper>