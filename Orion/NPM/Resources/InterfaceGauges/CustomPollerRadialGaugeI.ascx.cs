using System;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;

public partial class CustomPollerRadialGaugeI : CustomPollerGauge<IInterfaceProvider>
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!isError())
		{
			SetCustomGaugeData(gaugePlaceHolder1,
				"/Orion/Charts/CustomChart.aspx?chartName=CustomPollerChart_Interface&rpCustomPollerID=" + Resource.Properties["CustomPollerID"] + "&NetObject=" + GetCurrentNetObject.Interface.NetObjectID, 
				GetCurrentNetObject.Interface.NetObjectID, "CustomPoller", "Radial");
		}
	}

	public override Control GetSourceControl() { return Source; }

	public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

    public override string GetErrorText() { return String.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_45, String.Format("<a href=\"{0}\">", EditURL), "</a>"); }

	protected override string DefaultTitle { get { return Resources.NPMWebContent.NPMWEBCODE_VB0_48; } }

	public override string HelpLinkFragment { get { return "OrionPHResourceUniversalDevicePollerGauge"; } }

	public override int CurrentNodeID { get { return GetCurrentNetObject.Interface.NodeID; } }

	public override int CurrentInterfaceID { get { return GetCurrentNetObject.Interface.InterfaceID; } }

}
