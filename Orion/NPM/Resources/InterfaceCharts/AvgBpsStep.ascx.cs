using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class AvgBpsStep : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart(null, GetInterfaceInstance<IInterfaceProvider>().Interface.NetObjectID, "AvgBps-Step", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("AvgBps-Step", Resource);
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable; //string.Empty;//"Network Latency";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAverageBpsChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
