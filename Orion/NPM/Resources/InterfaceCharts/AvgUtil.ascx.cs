using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class AvgUtil : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart(null, GetInterfaceInstance<IInterfaceProvider>().Interface.NetObjectID, "AvgUtil-Step", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("AvgUtil-Step", Resource);
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable; // string.Empty;
		}
	}

	public override string HelpLinkFragment
	{
        get { return "OrionPHResourcePercentUtilizationChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}