using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Errors : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart(null, GetInterfaceInstance<IInterfaceProvider>().Interface.NetObjectID, "Errors", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("Errors", Resource);
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceInterfaceErrorsChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
