using System;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class InterfaceCharts_CustomPollerMinMaxAvgChart : CustomPollerGraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart("CustomOIDMinMaxAvg", GetInterfaceInstance<IInterfaceProvider>().Interface.NetObjectID, chartPlaceHolder);
		if (!string.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
		{
			Wrapper.SetDrDownMenuParameters("CustomOIDMinMaxAvg", Resource);
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_TM0_7; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionPHResourceUniversalDevicePollerMinMaxAvgChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override string EditURL
	{
		get
		{
			string url = string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}", this.Resource.ID);

			if (!string.IsNullOrEmpty(Request["NetObject"]))
				url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
			if (Page is OrionView)
				url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);

			return (url);
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NPM/Controls/EditResourceControls/EditMinMaxUndpCharts.ascx";
		}
	}
}
