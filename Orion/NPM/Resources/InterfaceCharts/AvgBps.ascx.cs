using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class AvgBps : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart(null, GetInterfaceInstance<IInterfaceProvider>().Interface.NetObjectID, "AvgBps-Line", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("AvgBps-Line", Resource);
	}

	protected override string DefaultTitle
	{
		get
		{
			return notAvailable;//.Empty;//"Network Latency";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAverageBpsChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}