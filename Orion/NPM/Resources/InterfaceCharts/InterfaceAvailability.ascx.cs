using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class InterfaceAvailability : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        CreateChart(null, GetInterfaceInstance<IInterfaceProvider>().Interface.NetObjectID, "InterfaceAvailability", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("InterfaceAvailability", Resource);
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;
		}
	}

	public override string HelpLinkFragment
	{
        get { return "OrionPHResourceTotalBytesTransferredChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
