﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceCharts.ascx.cs" Inherits="Orion_NetPerfMon_Resources_InterfaceCharts_InterfaceCharts" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Charting" %>

<orion:resourceWrapper runat="server">
    <Content>
        <div runat="server" id="interfaceError" visible="false" style="color:Red; font-weight:bold;"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_9%></div>
        <asp:Repeater runat="server" ID="chartsTable">
            <HeaderTemplate>
                <table class="Text" width="100%" cellpadding="3" cellspacing="0">
            </HeaderTemplate>
            <ItemTemplate>
	            <tr>
		            <td class="PropertyHeader" colspan="2"><%# Eval("Header") %></td>
	            </tr>
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgBps-Line&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_10%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_10%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgBps&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_11%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_11%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_12%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_12%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgUtil&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_13%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_13%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=TotalBytesBar&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_14%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_14%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgPps&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_15%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_15%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=TotalPackets&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_16%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_16%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_17%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_17%></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&NetObject=<%=NetObject%>&ChartDateSpan=<%# Eval("Period")%>&ChartInitialZoom=<%# Eval("PeriodZoom")%>&SampleSize=<%# Eval("SampleSize") %>&ResourceTitle=<%=Resources.NPMWebContent.NPMWEBDATA_TM0_18%>" target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_TM0_18%></a></td>
	            </tr>	            
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>        
        </asp:Repeater>
    
    </Content>
</orion:resourceWrapper>