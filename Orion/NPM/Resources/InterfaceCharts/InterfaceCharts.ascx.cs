﻿using System;
using System.Data;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
public partial class Orion_NetPerfMon_Resources_InterfaceCharts_InterfaceCharts : BaseResourceControl
{
    protected string NetObject
	{
		get
		{
			Interface _interface = this.GetInterfaceInstance<IInterfaceProvider>().Interface;
			if (_interface != null)
				return _interface.NetObjectID;
			return string.Empty;
		}
	}
	protected void Page_Load(object sender, EventArgs e)
	{
		DataTable table = new DataTable();
        table.Columns.Add(new DataColumn("Header", typeof(string)));
        table.Columns.Add(new DataColumn("Period", typeof(string)));
		table.Columns.Add(new DataColumn("PeriodZoom", typeof(string)));
		table.Columns.Add(new DataColumn("SampleSize", typeof(string)));
        table.Rows.Add(Resources.NPMWebContent.NPMWEBCODE_TM0_1, "1", "today", "30");
		table.Rows.Add(Resources.NPMWebContent.NPMWEBCODE_TM0_2, "7", "today", "1440");
		table.Rows.Add(Resources.NPMWebContent.NPMWEBCODE_TM0_3, "30", "today", "1440");
		this.chartsTable.DataSource = table;
		this.chartsTable.DataBind();
	}
	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_TM0_4; }
	}
	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceListInterfaceCharts"; }
	}
	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}
	public override ResourceLoadingMode ResourceLoadingMode
	{
		get
		{
			return ResourceLoadingMode.Static;
		}
	}
}
