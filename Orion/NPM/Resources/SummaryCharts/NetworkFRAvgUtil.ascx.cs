using System;
using SolarWinds.NPM.Web;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class NetworkFRAvgUtil : SummaryGraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        Dictionary<string, string> parameters = new Dictionary<string, string> { { "ResourceID", Resource.ID.ToString() }};

        CreateChart(null, Request.QueryString["NetObject"], "NetworkFRAvgUtil", parameters, chartPlaceHolder);

	    string netObjectId = Request.QueryString["NetObject"];

        if (string.IsNullOrEmpty(netObjectId))
        {
            Wrapper.SetUpDrDownMenu("NetworkFRAvgUtil", Resource, "S", parameters);
        }
        else
        {
            string prefix = netObjectId.Split(':')[0];
            Wrapper.SetUpDrDownMenu("NetworkFRAvgUtil", Resource, prefix, parameters);
        }

	}

	protected override string DefaultTitle
	{
		get 
        {
            return notAvailable;
        }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNetworkWideFrameRelayUtilizationChart"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}