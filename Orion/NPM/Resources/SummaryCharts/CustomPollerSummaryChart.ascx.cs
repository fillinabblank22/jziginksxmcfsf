using System;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class SummaryCharts_CustomPollerSummaryChart : CustomPollerGraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart("CustomOIDSummary", string.Empty, chartPlaceHolder);
		if (!string.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
		{
			Wrapper.SetDrDownMenuParameters("CustomOIDSummary", Resource, "S");
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_TM0_17; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceUniversalDevicePollerSummaryChart"; }
	}

	public override string EditURL
	{
		get
		{
			string url = string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}", this.Resource.ID);

			if (!string.IsNullOrEmpty(Request["NetObject"]))
				url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
			if (Page is OrionView)
				url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);

			return (url);
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NPM/Controls/EditResourceControls/EditUndpCharts.ascx";
		}
	}
}
