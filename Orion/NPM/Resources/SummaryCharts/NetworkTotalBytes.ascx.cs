using System;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;


public partial class NetworkTotalBytes : SummaryGraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        Dictionary<string, string> parameters = new Dictionary<string, string> { {"ResourceID", Resource.ID.ToString()}};

	    CreateChart(null, Request.QueryString["NetObject"], "NetworkTotalBytes", parameters, chartPlaceHolder);


        string netObjectId = Request.QueryString["NetObject"];

        if (string.IsNullOrEmpty(netObjectId))
        {
            Wrapper.SetUpDrDownMenu("NetworkTotalBytes", Resource, "S", parameters);
        }
        else
        {
            string prefix = netObjectId.Split(':')[0];
            Wrapper.SetUpDrDownMenu("NetworkTotalBytes", Resource, prefix, parameters);
        }
		
	}

	protected override string DefaultTitle
	{
		get 
        {
            return notAvailable;
        }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNetworkWideTotalBytesTransferredChart"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}

