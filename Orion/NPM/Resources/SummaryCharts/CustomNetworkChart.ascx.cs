using System;
using System.Text;
using System.Web.UI;
using SolarWinds.NPM.Web;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class CustomNetworkChart : SummaryGraphResource
{
	private string _chartName;

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_TM0_15; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceCustomNetworkWideChart"; }
	}

	public override string EditURL
	{
		get
		{
			return string.Format("/Orion/NPM/Resources/SummaryCharts/EditCustomNetworkChart.aspx?ResourceID={0}&NetObject={1}",
				Resource.ID.ToString(),
				Request["NetObject"]);
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		if (!string.IsNullOrEmpty(Resource.Properties["ChartName"]))
		{
			_chartName = Resource.Properties["ChartName"];

            Dictionary<string, string> parameters = new Dictionary<string, string> { 
					
                    {"ResourceID", Resource.ID.ToString()}
				};

            CreateChart(null, Request.QueryString["NetObject"],_chartName, parameters, chartPlaceHolder);
            Wrapper.SetUpDrDownMenu(_chartName, Resource, "S", parameters);

		}
		else
		{
			_chartName = string.Empty;
            var emptyResource = string.Format(
                   Resources.NPMWebContent.NPMWEBCODE_VB0_183,
                   String.Format("<ul><li><a href=\"{0}\"><font color=\"blue\"><b>", EditURL), "</b></font></a>"
                   , "</li><li>", "</li></ul>", "<span style=\"font-weight:bold;\">", "</span>");
            chartPlaceHolder.Controls.Add(new SolarWinds.Orion.Web.UI.WebResourceWantsEdit
            {
                HelpfulHtml = emptyResource,
                EditUrl = Profile.AllowCustomize ? EditURL : null,
                WatermarkImage = "/Orion/images/ResourceWatermarks/Watermark.CustomNodeChart.gif",
                BackgroundPosition = "right bottom",
                WatermarkBackgroundColor = "#f2f2f2"
                // image has default watermark dimentions
            });
		}
	}
    
	private string RemoveQueryStringParameter(string paramName)
	{
		string returnURL = Page.Request.Url.PathAndQuery;
		if (!returnURL.Contains(paramName))
			return returnURL;

		int paramStart = returnURL.IndexOf(paramName) - 1;
		int paramEnd = returnURL.IndexOf("&", paramStart);

		// if this parameter is last in query string
		if (paramEnd < 0)
		{
			return returnURL.Remove(paramStart);
		}
		else
		{
			return returnURL.Remove(paramStart, paramEnd - paramStart);
		}
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}

