using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;

public partial class EditCustomNetworkChart : System.Web.UI.Page
{
	private ResourceInfo _resource;

	protected void Page_Load(object sender, EventArgs e)
	{
		Dictionary<string, string> list = ChartXMLConfigManager.GetChartSelection("S");

		foreach (string key in list.Keys)
		{
			ListOfCharts.Items.Add(new ListItem(list[key],key));
		}
		if (!IsPostBack)
		{
			if (!string.IsNullOrEmpty(_resource.Properties["ChartName"]))
				ListOfCharts.SelectedValue = _resource.Properties["ChartName"];
		}
	}

	protected override void OnInit(EventArgs e)
	{
		if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			_resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = Resources.NPMWebContent.NPMWEBCODE_TM0_18 + _resource.Title;
		}

		base.OnInit(e);
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		_resource.Properties.Clear();
		_resource.Properties.Add("ChartName", ListOfCharts.SelectedValue);
		_resource.Properties.Add("Period", Period.PeriodName);
		_resource.Properties.Add("SampleSize", SampleSize.SampleSizeValue);
		
		string url = string.Format("/Orion/View.aspx?ViewID={0}", _resource.View.ViewID);
		if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
		{
			url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
		}
		Response.Redirect(url);
	}
}
