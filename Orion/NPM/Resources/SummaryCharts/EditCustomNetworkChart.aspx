<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditCustomNetworkChart.aspx.cs" Inherits="EditCustomNetworkChart" Title="Untitled Page"%>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Page.Title, Request["netobject"])) %></h1>
    <br />
    <table width="750">
        <tr>
            <td  align="justify" style="font-weight:bold;">
                <%=Resources.NPMWebContent.NPMWEBDATA_TM0_19%>
            </td>
            <td class="formRightInput">
                <asp:DropDownList runat="server" ID="ListOfCharts"></asp:DropDownList>
            </td>
            <td class="formHelpfulText">&nbsp;
            </td>
        </tr>
		<tr>
            <td colspan="3">
                <orion:EditPeriod runat="server" ID = "Period" />
            </td>
        </tr>
		<tr>
            <td colspan="3">
                <orion:EditSampleSize runat="server" ID = "SampleSize" />
            </td>
        </tr>
		 <tr>
            <td class="formLeftTitle">
                &nbsp;
            </td>
            <td class="formRightInput">
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
                </div>                
            </td>
            <td class="formHelpfulText">&nbsp;
            </td>
        </tr>
    </table>
    
</asp:Content>