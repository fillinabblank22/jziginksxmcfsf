﻿SW.NPM = SW.NPM || {};
SW.NPM.Resources = SW.NPM.Resources || {};
SW.NPM.Resources.TableResource = SW.NPM.Resources.TableResource || {};

(function (cq) {

    var PageManager = function (pageIndex, pageSize) {
        this.currentPageIndex = pageIndex;
        this.rowsPerPage = pageSize;

        this.startItem = function () {
            return (this.rowsPerPage * this.currentPageIndex) + 1;
        };

        this.lastItem = function () {
            // Ask for rowsPerPage + 1 rows so we can detect if this is the last page.
            return (this.rowsPerPage * (this.currentPageIndex + 1)) + 1;
        };

        this.withRowsClause = function () {
            return " WITH ROWS " + this.startItem() + " TO " + this.lastItem();
        };

        this.isLastPage = function (rowCount) {
            // We asked for rowsPerPage + 1 rows.  If more than rowsPerPage rows 
            // got returned, we know this isn't the last page.
            return rowCount <= this.rowsPerPage;
        };
    };

    var renderCell = function (cellValue, rowArray, columnInfo) {
        var cell = $('<td/>');
        if (cellValue === null) {
            cellValue = "";
        }
        else if (Date.isInstanceOfType(cellValue)) {
            cellValue = cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + " " + cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        if (columnInfo.EnableHtml) {
            $('<span/>').html(cellValue).appendTo(cell);
        } else {
            $('<span/>').text(cellValue).appendTo(cell);
        }

        return cell;
    };

    var updatePagerControls = function (controls, pageManager, rowCount, pagingStatus, callback) {

        var pager = controls.pager;

        var pageIndex = pageManager.currentPageIndex;
        var html = [];

        var previousText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_PREVIOUS;E=js}'; //Previous
        var nextText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_NEXT;E=js}'; //Next
        var showAllText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_SHOWALL;E=js}'; //Show all

        var style = 'style="vertical-align:middle"';
        var previousImgRoot = '/Orion/images/Arrows/button_paging_previous';
        var nextImgRoot = '/Orion/images/Arrows/button_paging_next';

        var showAll = showAllText + ' <img src="/Orion/images/Arrows/show_all.gif" style="vertical-align:middle"/>';
        var haveLinks = false;

        var startHtml;
        var endHtml;
        var contents;

        if (pageIndex > 0) {
            startHtml = '<a href="#" class="previousPage NoTip">';
            contents = String.format('<img src="{0}.gif" {1}/> {2}', previousImgRoot, style, previousText);
            endHtml = '</a>';
            haveLinks = true;
        } else {
            startHtml = '<span style="color:#646464;">';
            contents = String.format('<img src="{0}_disabled.gif" {1}/> {2}', previousImgRoot, style, previousText);
            endHtml = '</span>';
        }

        html.push(startHtml + contents + endHtml);
        html.push(' | ');

        if (!pageManager.isLastPage(rowCount)) {
            startHtml = '<a href="#" class="nextPage NoTip">';
            contents = String.format('{2} <img src="{0}.gif" {1}/>', nextImgRoot, style, nextText);
            endHtml = '</a>';
            haveLinks = true;
        } else {
            startHtml = '<span style="color:#646464;">';
            contents = String.format('{2} <img src="{0}_disabled.gif" {1}/>', nextImgRoot, style, nextText);
            endHtml = '</span>';
        }

        html.push(startHtml + contents + endHtml);
        html.push('<a href="#" class="showAll NoTip">' + showAll + '</a>');

        pager.empty().append(html.join(' '));
        var method = haveLinks ? 'show' : 'hide';
        pager[method]();

        pager.find('.previousPage').click(function () {
            cq.createTableFromCallback(controls, pagingStatus.PreviousPage(), callback);
            return false;
        });

        pager.find('.nextPage').click(function () {
            cq.createTableFromCallback(controls, pagingStatus.NextPage(), callback);
            return false;
        });

        pager.find('.showAll').click(function () {
            // We don't have a good way to show all.  We'll show 1 million and 
            // accept that there's an issue if there are more than that :)
            cq.createTableFromCallback(controls, pagingStatus.Everything(), callback);
            return false;
        });
    };

    var PagingStatusClass = function (parameters) {

        if (parameters) {
            this.pageSize = parameters.PageSize;
            this.pageIndex = 0;
            this.orderColumn = parameters.DefaultOrderColumn;
            this.search = '';
        }
        
        this.Clone = function () {
            var copy = this.constructor() || {};
            for (var attr in this) {
                if (this.hasOwnProperty(attr)) copy[attr] = this[attr];
            }
            return copy;
        };


        this.SetOrderColumn = function (orderColumn) {
            var rv = this.Clone();
            
            rv.orderColumn = orderColumn;
            
            return rv;
        };

        this.PreviousPage = function () {
            var rv = this.Clone();

            rv.pageIndex = rv.pageIndex - 1;
            
            return rv;
        };

        this.NextPage = function () {
            var rv = this.Clone();

            rv.pageIndex = rv.pageIndex + 1;
            
            return rv;
        };

        this.Everything = function () {
            var rv = this.Clone();

            rv.pageSize = 10000000;
            rv.pageIndex = 0;
            
            return rv;
        };

        this.Search = function(search) {
            var rv = this.Clone();

            rv.search = search;

            return rv;
        };
    };

    var setRowCountInHeader = function (header, count) {
        var existingRecordNumber = header.find("span");
        var recordNumber = existingRecordNumber;
        if (existingRecordNumber.length === 0) {
            recordNumber = $("<span style='font-weight: normal' />");
            header.append(recordNumber);
        }

        recordNumber.text("@{R=NPM.Strings;K=NPMWEBJS_VT0_1;E=js}".replace('{0}', count));
    };

    //cq.createTableFromCallback = function (controls, pageIndex, pageSize, orderColumn, callback) {
    cq.createTableFromCallback = function (controls, pagingStatus, callback) {

        var pageManager = new PageManager(pagingStatus.pageIndex, pagingStatus.pageSize);

		if (controls.pagingInitializer) {
			controls.pagingInitializer(controls, pagingStatus);
		}
		
        callback(pagingStatus, function (result) {
            
            if (controls.controlInitializer) {
                controls.controlInitializer(controls, pagingStatus, 
                    function(newPagingStatus) {
                        cq.createTableFromCallback(controls, newPagingStatus, callback);
                    }
                );
            }

            var headers = $('tr.HeaderRow', controls.grid);

            headers.empty();
            controls.grid.find('tr:not(.HeaderRow)').remove();

            var data = result.Data;

            if (!result.Configuration || !result.Configuration.DisableRowCountInHeader) {
                setRowCountInHeader(controls.grid.closest(".ResourceWrapper").find(".HeaderBar h1"), result.TotalRows);
            }
            
            if (controls.searchDiv) {
                createSearch(controls, pagingStatus, callback);
            }

            var columnInfo = data.Columns; // already generated
            $.each(columnInfo, function (colIndex, column) {
                var headerTd = $('<td/>');

                headerTd.addClass('ReportHeader')
                    .text(column.DisplayName)
                    .appendTo(headers);

                if (column.CssStyle) {
                    headerTd.attr("style", column.CssStyle);
                }
                
                if (column.CssClass) {
                    headerTd.attr("class", column.CssClass);
                }

                if (column.OrderColumnName) {
                    var descending = / DESC$/i.test(pagingStatus.orderColumn);
                    var active = (pagingStatus.orderColumn.split(' ')[0] == column.OrderColumnName);

                    $('<div />')
                        .addClass('sortIcon')
                        .addClass(
                            active ?
                                (descending ? 'sort-descending-active' : 'sort-ascending-active') :
                                'sort-descending'
                        )
                        .appendTo(headerTd);

                    headerTd
                        .addClass('sortable')
                        .on('click', function () {
                            cq.createTableFromCallback(controls, pagingStatus.SetOrderColumn(column.OrderColumnName + ((descending && active) ? " ASC" : " DESC")), callback);
                        });

                }
            });

            var rowsToOutput = data.Rows.slice(0, pageManager.rowsPerPage);

            $.each(rowsToOutput, function (rowIndex, row) {
                var tr = $('<tr/>');

                $.each(row.Columns, function (cellIndex, cell) {
                    var info = columnInfo[cellIndex];
                    renderCell(cell, row, info).appendTo(tr);
                });
                controls.grid.append(tr);
            });

            updatePagerControls(controls, pageManager, data.Rows.length + 1, pagingStatus, callback);

            if (controls.searchDiv) { // enable highlighting only when search is turned on
                SW.NPM.Highlighting.HighlightQuery("#"+ controls.grid.attr('id') +" tr:gt(0)", pagingStatus.search);
            }
            
            if (result.Configuration && controls.postProcessMethod) {
                controls.postProcessMethod(result);
            }
            
            if ($.timeago) {
                jQuery("abbr.timeago").timeago();
            }
        }, function (error) {
            controls.errorMsg.text(error).show();
        });
    };

    var createSearch = function (controls, pagingStatus, callback) {
        controls.searchDiv.empty();

        var searchField = $('<input type="text" />')
            .val(pagingStatus.search)
            .appendTo(controls.searchDiv);
        
        $('<input type="button" />')
            .attr('value', '@{R=NPM.Strings;K=NPMWEBJS_VT0_2;E=js}')
            .on('click', function() {
                cq.createTableFromCallback(controls, pagingStatus.Search(searchField.val()), callback);
                return false;
            })
            .appendTo(controls.searchDiv);
    };

    cq.initialize = function (controls, parameters) {
        var defaultPagingStatus = new PagingStatusClass(parameters);

        cq.createTableFromCallback(controls, defaultPagingStatus, function (pagingStatus, onSuccess, onFail) {
            SW.Core.Services.callWebService(
                parameters.ScriptServiceFile, parameters.ScriptServiceMethod,
                {
                    pagingStatus: pagingStatus,
                    configParameters: parameters
                }, onSuccess, onFail);
        });
    };

})(SW.NPM.Resources.TableResource);