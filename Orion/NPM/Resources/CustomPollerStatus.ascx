<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPollerStatus.ascx.cs" Inherits="CustomPollerStatus" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater ID="repeater" runat="server">
		    <HeaderTemplate>
                <table border="0" cellpadding="3" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_104%></td>
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_29%></td>
                        <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_30%></td>                       
                    </tr>
		    </HeaderTemplate>
		    <ItemTemplate>
                <tr>
		            <%# !(DataBinder.Eval(Container.DataItem, "DisplayPollerGroupName").Equals(_previousGroupName)) ? 
			            @"<td class=""Property"" colspan=""10"">
				            <font color=""Blue"">"
				                + DataBinder.Eval(Container.DataItem, "DisplayPollerGroupName") + @"
				            </font>
			            </td>
		            </tr>
		            <tr>"
		            : "" %>
			        <td class="Property">
				        <a href="<%# DataBinder.Eval(Container.DataItem, "LinkTarget") %>" target="_blank">
				            <%# DataBinder.Eval(Container.DataItem, "PollerName")%>
				        </a>
			        </td>                    
                    <td class="Property">
                        <a href="<%# DataBinder.Eval(Container.DataItem, "LinkTarget") %>" target="_blank">
                            <%# DataBinder.Eval(Container.DataItem, "DisplayCurrentValue")%>
                        </a>
                    </td>                    
                    <td class="Property">
                        <%# DataBinder.Eval(Container.DataItem, "DisplayLastPolled")%>
                    </td>
                </tr>
                
		        <%# SetPreviousGroupName(DataBinder.Eval(Container.DataItem, "DisplayPollerGroupName")) %>                
		    </ItemTemplate>

		    <FooterTemplate>
                </table>    
		    </FooterTemplate>
	    </asp:Repeater>
    </Content>
</orion:resourceWrapper>