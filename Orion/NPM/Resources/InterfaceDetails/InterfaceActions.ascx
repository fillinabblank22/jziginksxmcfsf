<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceActions.ascx.cs" Inherits="Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceActions" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="interfaceError" visible="false" style="color:Red;font-weight:bold;" class="Text">
        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_72%>
        </div>
        <div runat="server" id="accessDenied" visible="false" style="color:Red;font-weight:bold;" class="Text">
        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_73%>
        </div>
        <table border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="detailsTable">
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:CPUGAUGE', '');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_74%></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:BANDWIDTHGAUGE', '');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_75%></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'RFC1213-MIB:mib-2');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_76%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'IF-MIB:ifTable');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_77%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'RFC1213-MIB:system');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_78%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'CISCO-SMI:cisco');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_79%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'MSFT-MIB:microsoft');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_80%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'DOCS-IF-MIB:docsIfMib');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_81%></a></td></tr>

	        <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Traffic');"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_82%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Traffic');">                                  <%=Resources.NPMWebContent.NPMWEBDATA_VB0_83%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Interface/Port Status');">                      <%=Resources.NPMWebContent.NPMWEBDATA_VB0_84%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Broadcast/Multicast');">                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_85%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Errors/Discards');">                            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_86%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Interface/Port Configuration');">               <%=Resources.NPMWebContent.NPMWEBDATA_VB0_87%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Time data was last Received/Transmitted');">    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_88%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Ethernet Statistics');">                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_89%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Ethernet Collisions');">                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_90%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Token Ring Statistics');">                      <%=Resources.NPMWebContent.NPMWEBDATA_VB0_91%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Frame Relay Configuration');">                  <%=Resources.NPMWebContent.NPMWEBDATA_VB0_92%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Frame Relay Traffic');">                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_93%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Wireless Clients (Cisco APs)');">               <%=Resources.NPMWebContent.NPMWEBDATA_VB0_94%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Wireless Access Point SSID (Cisco APs)');">     <%=Resources.NPMWebContent.NPMWEBDATA_VB0_95%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Wireless Bridges and Repeaters (Cisco APs)');"> <%=Resources.NPMWebContent.NPMWEBDATA_VB0_96%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'ISDN Line Configuration');">                    <%=Resources.NPMWebContent.NPMWEBDATA_VB0_97%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Traffic Rates (Cisco only)');">                 <%=Resources.NPMWebContent.NPMWEBDATA_VB0_98%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Queue Errors (Cisco only)');">                  <%=Resources.NPMWebContent.NPMWEBDATA_VB0_99%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Slow vs Fast Switching');">                     <%=Resources.NPMWebContent.NPMWEBDATA_VB0_100%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Protocols (Cisco only)');">                     <%=Resources.NPMWebContent.NPMWEBDATA_VB0_101%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'IGRP Settings');">                              <%=Resources.NPMWebContent.NPMWEBDATA_VB0_102%></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Cisco Discovery Protocol');">                   <%=Resources.NPMWebContent.NPMWEBDATA_VB0_103%></a></td></tr>
    	</table>
    </Content>
</orion:resourceWrapper>


