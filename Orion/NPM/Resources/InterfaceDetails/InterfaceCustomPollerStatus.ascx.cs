using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class InterfaceCustomPollerStatus : BaseResourceControl
{
	protected void Page_Init(object sender, EventArgs e)
	{
		customPollerStatus.Resource = Resource;
	}

	protected override string DefaultTitle
	{
		get { return customPollerStatus.GetDefaultTitle(); }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NPM/Controls/EditResourceControls/EditCustomPollerStatus.ascx";
		}
	}

	public override string HelpLinkFragment
	{
		get { return customPollerStatus.HelpLinkFragment; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
