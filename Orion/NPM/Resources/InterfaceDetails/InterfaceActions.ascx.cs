using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceActions : BaseResourceControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (GetInterfaceInstance<IInterfaceProvider>().Interface == null)
		{
			this.interfaceError.Visible = true;
			this.detailsTable.Visible = false;
		}
		if (!this.Profile.ToolsetIntegration)
		{
			this.accessDenied.Visible = true;
			this.detailsTable.Visible = false;
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VB0_24; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceInterfaceActions"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Static; } }


}
