﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NPM.Web.ChartingV2;

public partial class Orion_NPM_Resources_SummaryChartsV2_NetworkWideChart : StandardChartResource, IResourceIsInternal
{
	private static readonly Log _log = new Log();

	protected void Page_Init(object sender, EventArgs e)
	{
		HandleInit(WrapperContents);
	}

	protected override NameValueCollection GetCustomChartParameters()
	{
		var rv = new NameValueCollection();
		if (!string.IsNullOrEmpty(Resource.Properties["CustomPollerID"]))
		{
			rv["CustomPollerID"] = Resource.Properties["CustomPollerID"];
		}
		return rv;
	}

	protected override Dictionary<string, object> GetContextForMacros(NetObject netObject)
	{
		var context = base.GetContextForMacros(netObject);
		var rv = UnDPChartHelper.LoadContextForUnDPChart(Resource.Properties["CustomPollerID"]);
		foreach(var k in context)
		{
			rv.Add(k.Key, k.Value);
		}
		return rv;
	}

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	protected override string NetObjectPrefix
	{
		get { return "NPM_SUMMARY"; }
	}

	protected override string DefaultTitle
	{
		get { return notAvailable; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { yield break; }
	}


	protected override IEnumerable<string> GetElementIdsForChart()
	{
		yield return string.Empty;
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	public bool IsInternal
	{
		get { return true; }
	}
}

