﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NPM.Web.Routing;
using SolarWinds.Orion.Web;
using System.Globalization;

public partial class Orion_NPM_Resources_Routing_TopXXFlapping : RoutingTabularResourceBase
{
	public override string HelpLinkFragment
	{
        get { return "OrionPHResourceTopXFlappingRoutes"; }
	}
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		SearchEnabled = false;
        
		HandleInit(WrapperControl);
	}

	protected override string DefaultTitle
	{
		get
		{
			return string.Format(Resources.NPMWebContent.NPMWEBCODE_VT0_24, GetResourceProperty<int>("FlappingRoutes", 10));
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NPM/Controls/EditResourceControls/EditTopXXFlapping.ascx";
		}
	}

	protected override string ResourceScriptServiceMethod
	{
		get { return "TopFlappingRoutes"; }
	}

	protected override dynamic GenerateDisplayDetails()
	{
		var rv = base.GenerateDisplayDetails();

		rv.TopXX = GetResourceProperty<int>("FlappingRoutes", 10);
		rv.FlapsWarn = SettingsDAL.GetSetting("NPM-RoutingFlaps-Warning").SettingValue;
		rv.FlapsError = SettingsDAL.GetSetting("NPM-RoutingFlaps-Error").SettingValue;
        rv.IsInReport = ResourceManager.GetResourceByID(this.Resource.ID).IsInReport;
		return rv;
	}

	protected override Dictionary<string, string> CreateCustomControls(Control wrapperControl)
	{
		var rv = base.CreateCustomControls(wrapperControl);

		var newselect = new HtmlSelect();
		newselect.Attributes.Add("class", "resourceDateSelector");
		newselect.Items.Add(new ListItem(Resources.NPMWebContent.NPMWEBCODE_VT0_27, "1"));
		newselect.Items.Add(new ListItem(Resources.NPMWebContent.NPMWEBCODE_VT0_28, "3"));
		newselect.Items.Add(new ListItem(Resources.NPMWebContent.NPMWEBCODE_VT0_29, "7"));
		newselect.Items.Add(new ListItem(Resources.NPMWebContent.NPMWEBCODE_VT0_30, "14"));
		newselect.Items.Add(new ListItem(Resources.NPMWebContent.NPMWEBCODE_VT0_31, "31"));
		newselect.SelectedIndex = 2;
        wrapperControl.Controls.AddAt(0, newselect);
        rv["timeFrameSelector"] = newselect.ClientID;

        var vrfPicker = new HtmlSelect();
        vrfPicker.Attributes.Add("class", "resourceVrfSelector");
        if (RoutingPollerEnabled)
        {
            vrfPicker.Items.Add(new ListItem(Resources.NPMWebContent.NPMWEBCODE_LH0_002, ""));
        }

        vrfPicker.SelectedIndex = 0;
        wrapperControl.Controls.AddAt(0, vrfPicker);

        // get list of VRFs
        string nodeIdStr = GetElementIdsForTable().FirstOrDefault();
        int nodeId;
        if (nodeIdStr != null && int.TryParse(nodeIdStr, out nodeId))
        {
            foreach (var vrfRecord in GetVrfList(nodeId))
            {
                vrfPicker.Items.Add(new ListItem(vrfRecord.Name, vrfRecord.Index.ToString(CultureInfo.InvariantCulture)));
            }
        }

        rv["vrfPicker"] = vrfPicker.ClientID;

        // fixes the float issue caused by resourceDateSelector
        TableGrid.Style.Add("clear", "left");

		

		return rv;
	}

	protected override string CreateCustomControlsInitializerJs()
	{
		return @"
	controls.timeFrameSelector.off('change');
	controls.timeFrameSelector.on('change', function() {
		var npagingStatus = pagingStatus.Clone();
		npagingStatus.lastDays = controls.timeFrameSelector.val();
		callback(npagingStatus);
	});

    controls.vrfPicker.off('change');
	controls.vrfPicker.on('change', function() {
        var npagingStatus = pagingStatus.Clone();
		callback(npagingStatus);
	});
";
	}
	
	protected override string CreateCustomPagingStatusInitializerJs() 
	{
		return @"pagingStatus.vrfIndex = controls.vrfPicker.val();
		pagingStatus.lastDays = controls.timeFrameSelector.val();";
	}

    protected override string CreatePostProcessMethodJs()
    {
        var errorDivId = "incomplete-" + Resource.ID;
        var warningDivId = "notpolled-" + Resource.ID;
        return String.Format(@"if (result.Configuration && result.Configuration.IncompleteData) {{
    $('#{0}').show();
}} else {{
    $('#{0}').hide();
}}
if (result.Configuration && result.Configuration.VrfInitialPoll) {{
    $('#{1}').show();
}} else {{
    $('#{1}').hide();
}}", errorDivId, warningDivId);
    }
}