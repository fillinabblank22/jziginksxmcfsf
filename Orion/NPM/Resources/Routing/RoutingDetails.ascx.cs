﻿using System;
using SolarWinds.NPM.Web.Routing;
using SolarWinds.Orion.Web;

public partial class Orion_NPM_Resources_Routing_RoutingDetails : RoutingResourceBase
{
	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VT0_16; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceRoutingDetails"; }
	}

	protected override string PollerAssignedToBeVisible
	{
        get { return "N.Routing%|N.VRFRouting.%"; }
	}
    public string IsInReporting 
    {
        get { return _inReporting ? "true" : "false"; }
    }
    bool _inReporting = true;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);
        _inReporting = resInfo.IsInReport;
    }    
}