﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="RoutingDetails.ascx.cs" Inherits="Orion_NPM_Resources_Routing_RoutingDetails" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
            $(function () {
                var processResults = function(result) {
                    var source = $('#routingDetailsTemplate').html();
                    var newHtml = _.template(source, result);
                    $('#<%= resourceContent.ClientID %>').html(newHtml);
                };

                var refresh = function () {
                    SW.Core.Services.callWebService(
                        "/Orion/NPM/Services/Routing.asmx",
                        "GetRoutingDetails",
                        { 
                            nodeId: <%= NodeId %>,
                            inReport: <%= IsInReporting %>
                        },
                        processResults
                    );
                };

                SW.Core.View.AddOnRefresh(refresh, '<%= resourceContent.ClientID %>');
                refresh();
            });
        </script>

        <script id="routingDetailsTemplate" type="text/x-template">
            <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase"><%= Resources.NPMWebContent.NPMWEBDATA_VT0_20 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase"><%= Resources.NPMWebContent.NPMWEBDATA_VT0_21 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase"><%= Resources.NPMWebContent.NPMWEBDATA_VT0_22 %></td>
                </tr>
                {# _.each(protocols, function(protocol) { #}
                    <tr>
                        <td>{{ protocol.name }}</td>
                        {# if (protocol.error) { #}
                            <td colspan="2" style="color: red">{{ protocol.error }}</td>
                        {# } else if (protocol.hasBeenPolled) { #}
                            <td>{{ protocol.lastPoll }}</td><td>{{ protocol.nextPoll }}</td>
                        {# } else { #}
                            <td colspan="2"><%=Resources.NPMWebContent.NPMWEBDATA_VT0_23 %></td>
                        {# } #}
                    </tr>
                {# }); #}
            </table>
        </script>
        <span id="resourceContent" runat="server"></span>
    </Content>
    
</orion:resourceWrapper>