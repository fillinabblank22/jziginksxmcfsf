﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.NPM.Web.Routing;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NPM.Web.UI.TabularResource;
using System.Globalization;
using SolarWinds.Orion.Web.InformationService;
using System.Data;

public partial class Orion_NPM_Resources_Routing_RoutingTable : RoutingTabularResourceBase
{
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceRoutingTable"; }
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        HandleInit(WrapperControl);
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VT0_4; }
    }

    protected override Dictionary<string, string> CreateCustomControls(Control wrapperControl)
    {
        var rv = base.CreateCustomControls(wrapperControl);

        var vrfPicker = new HtmlSelect();
        vrfPicker.Attributes.Add("class", "resourceVrfSelector");
        if (RoutingPollerEnabled)
        {
            vrfPicker.Items.Add(new ListItem(Resources.NPMWebContent.NPMWEBCODE_LH0_002, ""));
        }
        
        vrfPicker.SelectedIndex = 0;
        wrapperControl.Controls.AddAt(1, vrfPicker);

        // get list of VRFs
        string nodeIdStr = GetElementIdsForTable().FirstOrDefault();
        int nodeId;
        if (nodeIdStr != null && int.TryParse(nodeIdStr, out nodeId))
        {
            foreach (var vrfRecord in GetVrfList(nodeId))
            {
                vrfPicker.Items.Add(new ListItem(vrfRecord.Name, vrfRecord.Index.ToString(CultureInfo.InvariantCulture)));
            }
        }

        rv["vrfPicker"] = vrfPicker.ClientID;
                
        return rv;
    }



    protected override string CreateCustomControlsInitializerJs()
    {
        return @"
	    controls.vrfPicker.off('change');
	    controls.vrfPicker.on('change', function() {
            var npagingStatus = pagingStatus.Clone();
		    callback(npagingStatus);
	    });
        ";
    }

	protected override string CreateCustomPagingStatusInitializerJs() 
	{
		return @"pagingStatus.vrfIndex = controls.vrfPicker.val();";
	}

    protected override string CreatePostProcessMethodJs()
    {
        var errorDivId = "incomplete-" + Resource.ID;
        var warningDivId = "notpolled-" + Resource.ID;
        return String.Format(@"if (result.Configuration && result.Configuration.IncompleteData) {{
    $('#{0}').show();
}} else {{
    $('#{0}').hide();
}}
if (result.Configuration && result.Configuration.VrfInitialPoll) {{
    $('#{1}').show();
}} else {{
    $('#{1}').hide();
}}", errorDivId, warningDivId);
    }

    protected override string ResourceScriptServiceMethod { get { return "RoutingTable"; } }
    protected override string IncompleteDataWarning { get { return Resources.NPMWebContent.NPMWEBCODE_VT0_46; } }
}