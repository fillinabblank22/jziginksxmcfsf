﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.NPM.Web.Routing;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NPM_Resources_Routing_NeighborTable : RoutingTabularResourceBase
{
	protected override bool ReliesOnRoutingTable
	{
		get { return false; }
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceRoutingNeighbors";
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		HandleInit(WrapperControl);
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VT0_11; }
	}

    protected override void EnsureVisibility()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var rd = swis.Query(string.Format(
                @"SELECT COUNT(PollerType) AS i 
						FROM Orion.Pollers 
						WHERE NetObjectType=@netObject AND 
						NetObjectID IN ({0}) AND Enabled=1 AND PollerType LIKE 'N.RoutingNeighbor.%'", 
                        string.Join(",", GetElementIdsForTable().Select(x => "'" + x.Replace("'", "''") + "'"))),
                        new Dictionary<string, object> { { "netObject", NetObjectPrefix } });
            this.Visible = Convert.ToInt32(rd.Rows[0][0]) > 0;
        }
    }

    public override string EditControlLocation
	{
		get { return "/Orion/NPM/Controls/EditResourceControls/EditRoutingControl.ascx"; }
	}

	protected override string ResourceScriptServiceMethod
	{
		get { return "NeighborTable"; }
	}

    protected override dynamic GenerateDisplayDetails()
    {
        var rv = base.GenerateDisplayDetails();
        rv.IsInReport = ResourceManager.GetResourceByID(this.Resource.ID).IsInReport;
        return rv;
    }
}