using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class NodeCustomPollerStatus : BaseResourceControl
{
	protected void Page_Init(object sender, EventArgs e)
	{
		customPollerStatus.Resource = Resource;

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
	}

	protected override string DefaultTitle
	{
		get { return customPollerStatus.GetDefaultTitle(); }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NPM/Controls/EditResourceControls/EditCustomPollerStatus.ascx";
		}
	}

	public override string HelpLinkFragment
	{
		get { return customPollerStatus.HelpLinkFragment; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
