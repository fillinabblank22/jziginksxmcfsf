using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class NodeTabularCustomPoller : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Resource.Title.Length == 0)
		{
			Resource.Title = DefaultTitle;
			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
		}
		tabularCustomPoller.Resource = Resource;
	}
		
	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_VB0_110; }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NPM/Controls/EditResourceControls/EditTabularCustomPoller.ascx";
		}
	}

	public override string HelpLinkFragment
	{
		get { return tabularCustomPoller.HelpLinkFragment; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}

