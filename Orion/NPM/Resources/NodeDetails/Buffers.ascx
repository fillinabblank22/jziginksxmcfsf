<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Buffers.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_Buffers" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_223%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
            <thead>
                <tr>
                    <td class="ReportHeader">&nbsp</td>
                    <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_224%></td>
                    <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBCODE_TM0_1%></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><asp:HyperLink ID="outOfMemoryCaptionLink" runat="server" Target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_225%></asp:HyperLink></td>
                    <td><asp:HyperLink ID="outOfMemoryThisHourLink" runat="server" Target="_blank"><asp:Label ID="NoMemThisHour" runat="server" /></asp:HyperLink></td>
                    <td><asp:HyperLink ID="outOfMemoryTodayLink" runat="server" Target="_blank"><asp:Label ID="NoMemToday" runat="server" /></asp:HyperLink></td>
                </tr>
                <tr>
                    <td><asp:HyperLink ID="smallCaptionLink" runat="server" Target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_226%></asp:HyperLink></td>
                    <td><asp:HyperLink ID="smallThisHourLink" runat="server" Target="_blank"><asp:Label ID="SbThisHour" runat="server" /></asp:HyperLink></td>
                    <td><asp:HyperLink ID="smallTodayLink" runat="server" Target="_blank"><asp:Label ID="SbToday" runat="server" /></asp:HyperLink></td>
                </tr>
                <tr>
                    <td><asp:HyperLink ID="mediumCaptionLink" runat="server" Target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_227%></asp:HyperLink></td>
                    <td><asp:HyperLink ID="mediumThisHourLink" runat="server" Target="_blank"><asp:Label ID="MbThisHour" runat="server" /></asp:HyperLink></td>
                    <td><asp:HyperLink ID="mediumTodayLink" runat="server" Target="_blank"><asp:Label ID="MbToday" runat="server" /></asp:HyperLink></td>
                </tr>
                <tr>
                    <td><asp:HyperLink ID="bigCaptionLink" runat="server" Target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_228%></asp:HyperLink></td>
                    <td><asp:HyperLink ID="bigThisHourLink" runat="server" Target="_blank"><asp:Label ID="BbThisHour" runat="server" /></asp:HyperLink></td>
                    <td><asp:HyperLink ID="bigTodayLink" runat="server" Target="_blank"><asp:Label ID="BbToday" runat="server" /></asp:HyperLink></td>
                </tr>
                <tr>
                    <td><asp:HyperLink ID="largeCaptionLink" runat="server" Target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_229%></asp:HyperLink></td>
                    <td><asp:HyperLink ID="largeThisHourLink" runat="server" Target="_blank"><asp:Label ID="LbThisHour" runat="server" /></asp:HyperLink></td>
                    <td><asp:HyperLink ID="largeTodayLink" runat="server" Target="_blank"><asp:Label ID="LbToday" runat="server" /></asp:HyperLink></td>
                </tr>
                <tr>
                    <td><asp:HyperLink ID="hugeCaptionLink" runat="server" Target="_blank"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_230%></asp:HyperLink></td>
                    <td><asp:HyperLink ID="hugeThisHourLink" runat="server" Target="_blank"><asp:Label ID="HbThisHour" runat="server" /></asp:HyperLink></td>
                    <td><asp:HyperLink ID="hugeTodayLink" runat="server" Target="_blank"><asp:Label ID="HbToday" runat="server" /></asp:HyperLink></td>
                </tr>
            </tbody>
        </table>
    </Content>
</orion:ResourceWrapper>
