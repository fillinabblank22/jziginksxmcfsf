﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceList.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_InterfaceList" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.NPMWebContent.NPMWEBDATA_VB0_223%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr id="Tr1" runat="server" class="ReportHeader">
                        <td class="ReportHeader">
                        &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_231%>
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.NPMWebContent.NPMWEBCODE_VB0_6%>
                        </td>
                        <td id="ewLevelHead" runat="server" class="ReportHeader">
                            <asp:Literal Text="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_232%>" runat="server" />
                        </td>
                        <td class="ReportHeader">
                            &nbsp;                           
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.NPMWebContent.NPMWEBDATA_VB0_233%>
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                    </tr>
        <asp:Repeater runat="server" ID="NodesRepeater">
            <ItemTemplate>
                <tr>
                    <td class="Property">
                        <asp:Panel ID="Panel1" runat="server" >
                            <img alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_108%>" src="/Orion/images/StatusIcons/Small-<%# Eval("StatusLED") %>" />
                        </asp:Panel>
                    </td>
                    <td><%#Eval("StatusText")%></td>
                    <td style="padding-left:10px">
                    <img alt="<%=Resources.NPMWebContent.NPMWEBDATA_VB0_108%>" src="/NetPerfMon/images/Interfaces/<%# Eval("Icon") %>" />
                    </td>
                    <td>
                        <orion:ToolsetLink  runat="server" ID="ToolsetLink1" 
                                            IPAddress='<%#Eval("IPAddress")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("InterfaceURL")%>'
                                            InterfaceIndex='<%#Eval("Index")%>'
                                            InterfaceName='<%#Eval("Name")%>'>
                        <%# Eval("Caption") %>
                        </orion:ToolsetLink>
                    </td>
                    <td><%# Eval("EWPort") %></td>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <%#Eval("TypeDescription")%>
                    </td>
                    <td align="bottom">
                    <orion:ToolsetImage  runat="server" ID="ToolsetLink2" 
                                            IPAddress='<%#Eval("IPAddress")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("InterfaceURL")%>'
                                            InterfaceIndex='<%#Eval("Index")%>'
                                            InterfaceName='<%#Eval("Name")%>'
                                            ImageUrl="/NetPerfMon/Images/Small.Gauge.png"
                                            ImageAlt="<%$ Resources: NPMWebContent, NPMWEBDATA_VB0_234%>"
                                            OnClientClick="javascript:DoAction('SWTOOL:BandwidthGauge', '');"
                                            OnClientMouseOver="javascript:status='Launch SolarWinds Real-Time Bandwidth Gauges'; return true;"
                                            OnClientMouseOut="javascript:status=''; return false;" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        </table>
    </Content>
</orion:ResourceWrapper>
