<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceUtilization.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_InterfaceUtilization" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
    <table border="0" cellpadding="3" cellspacing="0" width="100%" class="NeedsZebraStripes">
        <thead style="text-align:left;">
            <tr>
	            <td class="ReportHeader">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_231%></td>
	            <td class="ReportHeader">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.NPMWebContent.NPMWEBCODE_VB0_6%></td>
	            <td id="ewImportHead" runat="server" class="ReportHeader">&nbsp;</td>
	            <td id="ewLevelHead" runat="server" class="ReportHeader">&nbsp;</td>

	            <% if (this.VSANSupported) {%>
	            <td class="ReportHeader" style="white-space:nowrap;"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_235%></td>
	            <%} %>

	            <td class="ReportHeader" width="100px"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_40%></td>
	            <td class="ReportHeader" width="100px"><%=Resources.NPMWebContent.NPMWEBDATA_VB0_39%></td>
	            <% if (this.Profile.ToolsetIntegration)
                    {%>
	            <td class="ReportHeader">&nbsp;</td>
	                <%} %>
            </tr>
        </thead>
    <asp:Repeater runat="server" ID="interfaceUtilizationTable">
        <ItemTemplate>
		        <tr>
			        <td class="Property"><%# Eval("StatusLed") %></td>
			        <td class="Property"><%# Eval("Status") %></td>
			        <td class="Property"><%# Eval("Icon") %></td>
			        <td class="Property"><%# Eval("InterfaceLink")%>&nbsp;</td>
			        <td class="Property"><%# Eval("EWImportance")%> </td>
			        <td class="Property"><%# Eval("EWLevel")%> </td>

		            <% if (this.VSANSupported) {%>
			        <td class="Property"><%# Eval("VSANName") %>&nbsp;</td>
			        <%} %>

			        <td class="Property" align="right"><%# Eval("OutUtilBar")%>&nbsp;
				<%# Eval("OutUtilChartLink")%>&nbsp;</td>
			        <td class="Property" align="right"><%# Eval("InUtilBar")%>&nbsp;
				<%# Eval("InUtilChartLink")%>&nbsp;</td>
		            <% if (this.Profile.ToolsetIntegration && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                        {%>
			        <td class="Property"><%# Eval("BandwidthGauge") %>&nbsp;</td>
			            <%} %>
		        </tr>
        </ItemTemplate>
    </asp:Repeater>
    </table>
</Content>
</orion:resourceWrapper>