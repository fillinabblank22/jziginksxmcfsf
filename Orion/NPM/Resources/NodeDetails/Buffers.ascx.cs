﻿using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NodeDetails_Buffers : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	private const string NexusSysObjectId = "1.3.6.1.4.1.9.12.3.1.3.";

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_101; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceNodeBufferMisses";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    #endregion

    private string ConvertValue(float number)
    {
        if (number == 1) return String.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_106, Convert.ToInt32(number));
        return String.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_102, Convert.ToInt32(number));
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider == null) return;

        ResourceWrapper.Visible = nodeProvider.Node.Cisco && !nodeProvider.Node.SysObjectID.StartsWith(NexusSysObjectId);

        var todayUrl = String.Format("/Orion/Charts/CustomChart.aspx?ChartName=CiscoBufferFailures&NetObject={0}&Period=Today&SampleSize=30&ChartTitle=${{Caption}}&ChartSubTitle={1}", nodeProvider.Node.NetObjectID, Resources.NPMWebContent.NPMWEBDATA_TM0_5);
        var thisHourUrl = String.Format("/Orion/Charts/CustomChart.aspx?ChartName=CiscoBufferFailures&NetObject={0}&Period=This+Hour&SampleSize=30&ChartTitle=${{Caption}}&ChartSubTitle={1}", nodeProvider.Node.NetObjectID, Resources.NPMWebContent.NPMWEBDATA_TM0_5);

        this.outOfMemoryCaptionLink.NavigateUrl = todayUrl;
        this.outOfMemoryThisHourLink.NavigateUrl = thisHourUrl;
        this.outOfMemoryTodayLink.NavigateUrl = todayUrl;
        this.smallCaptionLink.NavigateUrl = todayUrl;
        this.smallThisHourLink.NavigateUrl = thisHourUrl;
        this.smallTodayLink.NavigateUrl = todayUrl;
        this.mediumCaptionLink.NavigateUrl = todayUrl;
        this.mediumThisHourLink.NavigateUrl = thisHourUrl;
        this.mediumTodayLink.NavigateUrl = todayUrl;
        this.bigCaptionLink.NavigateUrl = todayUrl;
        this.bigThisHourLink.NavigateUrl = thisHourUrl;
        this.bigTodayLink.NavigateUrl = todayUrl;
        this.largeCaptionLink.NavigateUrl = todayUrl;
        this.largeThisHourLink.NavigateUrl = thisHourUrl;
        this.largeTodayLink.NavigateUrl = todayUrl;
        this.hugeCaptionLink.NavigateUrl = todayUrl;
        this.hugeThisHourLink.NavigateUrl = thisHourUrl;
        this.hugeTodayLink.NavigateUrl = todayUrl;

        this.NoMemThisHour.Text = ConvertValue(nodeProvider.Node.BufferNoMemThisHour);
        this.NoMemToday.Text = ConvertValue(nodeProvider.Node.BufferNoMemToday);
        this.SbThisHour.Text = ConvertValue(nodeProvider.Node.BufferSmMissThisHour);
        this.SbToday.Text = ConvertValue(nodeProvider.Node.BufferSmMissToday);
        this.MbThisHour.Text = ConvertValue(nodeProvider.Node.BufferMdMissThisHour);
        this.MbToday.Text = ConvertValue(nodeProvider.Node.BufferMdMissToday);
        this.BbThisHour.Text = ConvertValue(nodeProvider.Node.BufferBgMissThisHour);
        this.BbToday.Text = ConvertValue(nodeProvider.Node.BufferBgMissToday);
        this.LbThisHour.Text = ConvertValue(nodeProvider.Node.BufferLgMissThisHour);
        this.LbToday.Text = ConvertValue(nodeProvider.Node.BufferLgMissToday);
        this.HbThisHour.Text = ConvertValue(nodeProvider.Node.BufferHgMissThisHour);
        this.HbToday.Text = ConvertValue(nodeProvider.Node.BufferHgMissToday);
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}
}

