using System;
using System.Data;
using System.Collections.Generic;

using SolarWinds.Logging;
using SolarWinds.NPM.Web.UI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using Limitation = SolarWinds.Orion.Web.Limitation;
using SolarWinds.Orion.NPM.Web;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Common.Formatters;
using SolarWinds.NPM.Common.Enums;

public partial class TabularCustomPoller : BaseResourceControl
{
    private const string rowsResourcePrefix = "Rows:";

    private bool _showHelp = false;
    private string _type;
    private string _netObject;
    private Guid _labelPollerID;
    private readonly Log _log = new Log();

    protected bool ShowHelp
    {
        get { return _showHelp; }
    }

    public string Type
    {
        get { return _type; }
        set { _type = value; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NPMWebContent.NPMWEBCODE_VB0_110; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceTabularUniversalDevicePoller"; }
    }

    public void MyBusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    #region Helpers
    Guid ToGuid(string maybeGuid)
    {
        Guid guid;
        if (Guid.TryParse(maybeGuid, out guid))
        {
            return guid;
        }

        _log.WarnFormat("Unable to parse Custom Poller Id '{0}'", maybeGuid ?? string.Empty);
        return Guid.Empty;
    }

    CustomPoller ToCustomPoller(Guid pollerId, INPMBusinessLayer proxy)
    {
        if (proxy == null)
        {
            throw new ArgumentNullException("proxy");
        }

        CustomPoller poller;
        try
        {
            poller = proxy.GetCustomPoller(pollerId);
        }
        catch(Exception ex)
        {
            MyBusinessLayerExceptionHandler(ex);
            throw;
        }

        if (poller == null)
        {
            _log.ErrorFormat("Custom Poller for specified Id = {0} was not found. Resource's configuration might be no longer valid.");
        }
        return poller;
    }

    CustomPollers GetCustomPollers(IEnumerable<Guid> pollerIds, INPMBusinessLayer proxy)
    {
        return proxy.GetCustomPollers(pollerIds
            .Where(g => g != Guid.Empty)
            .Distinct()
            .ToList());
    }
    /// <summary>
    /// Get dictionary where key is the table column and value is the <typeparamref name="CustomPoller"/> or null in case of label column (typically first one) or column with unknown custom poller.
    /// </summary>    
    private static Dictionary<int, CustomPoller> GetCustomPollersRelatedToColumnIndexes(DataTable datatable, CustomPollers customPollers)
    {
        var customPollerOnColumns = new Dictionary<int, CustomPoller>();

        for (int columnIndex = 0; columnIndex < datatable.Columns.Count; columnIndex++)
        {
            var customPollerName = datatable.Columns[columnIndex].Caption;
            var customPoller = customPollers.FirstOrDefault(poller => string.Equals(poller.UniqueName, customPollerName, StringComparison.InvariantCultureIgnoreCase));

            customPollerOnColumns[columnIndex] = customPoller;
        }

        return customPollerOnColumns;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable datatable;
        this.resourceHelpPage.EditURL = EditURL;

        _netObject = GetInterfaceInstance<INodeProvider>().Node.NetObjectID;

        //FB31670: Fixing unchangeable title...
        //Do not replace with default if not found in Properties,
        //only if the title is actually empty.
        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            Resource.Title = Resource.Properties["Title"];
        }
        else
        {
            if (string.IsNullOrEmpty(Resource.Title))
            {
                Resource.Title = DefaultTitle;
            }
        }

        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            Resource.SubTitle = Resource.Properties["SubTitle"];
        }


        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

        CustomPollers customPollers = new CustomPollers();

        if (string.IsNullOrEmpty(Resource.Properties["Pollers"]))
        {
            _showHelp = true;
            return;
        }

        using (var proxy = NPMBusinessLayerProxyFactory.Instance.Create())
        {
            string[] guids = Resource.Properties["Pollers"].Split(' ');

            var pollerIds = guids.Select(ToGuid);
            customPollers = GetCustomPollers(pollerIds, proxy.Api);

            if (!customPollers.Any())
            {
                _showHelp = true;
                return;
            }

            if (!string.IsNullOrEmpty(Resource.Properties["LabPollID"]))
            {
                _labelPollerID = new Guid(Resource.Properties["LabPollID"]);
            }
            else
            {
                _labelPollerID = new Guid(guids[0]);
            }

            int netObjectID;
            string netObject = _netObject.Substring(0, _netObject.IndexOf(':') - 1);
            if (NetObjectHelper.IsNodeNetObject(_netObject))
            {
                netObjectID = Convert.ToInt32(NetObjectHelper.GetObjectID(_netObject));
            }
            else
            {
                NetObject netObj = NetObjectFactory.Create(_netObject);
                netObjectID = Convert.ToInt32(netObj["NodeID"]);
            }

            try
            {
                datatable = proxy.Api.GetTabularCustomPollerData(customPollers, _labelPollerID, netObjectID, Limitation.GetCurrentListOfLimitationIDs());
            }
            catch(Exception ex)
            {
                MyBusinessLayerExceptionHandler(ex);
                throw;
            }

            if (Resource.Properties["AutoHide"] == "1" && !string.IsNullOrEmpty(Resource.Properties["Pollers"]) && datatable == null)
            {
                Visible = false;
            }
            else
            {
                Visible = true;
            }
        }


        if (datatable != null)
        {
            string rowResourceID = rowsResourcePrefix + _netObject;
            if (!string.IsNullOrEmpty(Resource.Properties[rowResourceID]))
            {
                HashSet<string> allowedRows = new HashSet<string>(Resource.Properties[rowResourceID].Split(' '));
                int index = 0;
                while (index < datatable.Rows.Count)
                {
                    if (allowedRows.Contains((string)datatable.Rows[index]["RowID"]))
                    {
                        index++;
                    }
                    else
                    {
                        datatable.Rows.RemoveAt(index);
                    }
                }
            }
            datatable.Columns.Remove("RowID");

            string[] columns = (Resource.Properties["NotShow"] ?? string.Empty).Split(';');
            foreach (string column in columns)
            {
                if (datatable.Columns.Contains(column))
                    datatable.Columns.Remove(column);
            }

            if (datatable.Rows.Count > 0)
            {
                // Modify the datatable with current locale.
                // The datatable comes from business layer and it can't be modified there because of other possible uses.
                // => Have to iterate through the datatable here and manually modify the values by current locale.
                CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                Dictionary<int, CustomPoller> customPollerOnColumns = GetCustomPollersRelatedToColumnIndexes(datatable, customPollers);

                for (var i = 0; i < datatable.Rows.Count; i++)
                {
                    for (var j = 0; j < datatable.Rows[i].ItemArray.Length; j++)
                    {
                        var customPoller = customPollerOnColumns[j];
                        string rawValue = Convert.ToString(datatable.Rows[i][j].ToString());
                        datatable.Rows[i][j] = UndpHelper.FormatRawValue(rawValue, customPoller, CultureInfo.CurrentCulture);
                    }
                }

                this.gridView.DataSource = datatable;
                this.gridView.DataBind();
            }
            else
            {
                DataRow row = datatable.NewRow();
                datatable.Rows.Add(row);

                this.gridView.DataSource = datatable;
                this.gridView.DataBind();
                this.gridView.Rows[0].Visible = false;
            }

            if (this.gridView.HeaderRow != null && this.gridView.HeaderRow.Cells != null)
            {
                foreach (var headerCell in this.gridView.HeaderRow.Cells.OfType<WebControl>())
                {
                    headerCell.CssClass = "ReportHeader";
                }
            }
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    protected void gridView_DataBound(object sender, EventArgs e)
    {
        var grid = sender as GridView;

        if (grid != null)
        {
            foreach (GridViewRow row in grid.Rows)
            {
                row.Cells.Cast<TableCell>().ToList().ForEach(cell => cell.CssClass = "Property");
            }
        }
    }
}