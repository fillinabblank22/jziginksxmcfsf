<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" 
    CodeFile="EditRecentlyPostedUniversalPollers.aspx.cs" Inherits="EditRecentlyPostedUniversalPollers" Title="<%$ Resources:NPMWebContent,NPMWEBDATA_VB0_52%>" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
<orion:Include File="Thwack.css" runat="server" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Page.Title, Request["netobject"])) %></h1>

    <p>
    <b><%=Resources.NPMWebContent.NPMWEBDATA_AK0_35%></b><br />
    <asp:CheckBox ID="LimitCount" runat="server" Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_44%>" />&nbsp;
    <asp:TextBox ID="NumberOfRecords" runat="server"  Columns="5" /><asp:RangeValidator ID="NumberOfRecordsValidator" runat="server" ControlToValidate="NumberOfRecords" MinimumValue="0" Type="Integer" Display="Dynamic"></asp:RangeValidator> <br /><br />
    <asp:RadioButton ID="ShowAll" runat="server" GroupName="Show" Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_37%>" /><br />
    <asp:RadioButton ID="ShowLast" runat="server" GroupName="Show" Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_45%>" />&nbsp;
    <asp:TextBox ID="NumberOfDays" runat="server"  Columns="5"/> <asp:RangeValidator ID="NumberOfDaysRangeValidator" runat="server" ControlToValidate="NumberOfDays" MinimumValue="0" Type="Integer" Display="Dynamic"></asp:RangeValidator>    
  
    </p>

    <p>
    <b><%=Resources.NPMWebContent.NPMWEBDATA_AK0_36%></b><br />
    <asp:DropDownList ID="SortByList" runat="server" >
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_41%>" Value="Date"/>
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_40%>" Value="Title"/>
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_39%>" Value="Views" />
        <asp:ListItem Text="<%$ Resources:NPMWebContent,NPMWEBDATA_AK0_38%>" Value="Downloads" />
    </asp:DropDownList>   
    </p>

    <div class="sw-btn-bar">
        <orion:LocalizableButton id="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
    </div>
</asp:Content>
