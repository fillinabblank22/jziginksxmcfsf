<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditRecentPosts.aspx.cs" Inherits="EditRecentPosts" Title="<%$ Resources:NPMWebContent,NPMWEBDATA_VB0_52%>"%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_18, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))%></h1>
    <br />
    <b><%=Resources.NPMWebContent.NPMWEBDATA_AK0_42%></b>
    <br/>
        <asp:TextBox ID="PostsCount" runat="server" /> <asp:RangeValidator ID="PostsCountRangeValidator" runat="server" ControlToValidate="PostsCount" MinimumValue="0" Type="Integer" Display="Dynamic"></asp:RangeValidator>      
    <br/>

    <%if (IsError)
    {%>
    <br />
    <div style="color:Red; font-size:medium;" ><b><%=Resources.NPMWebContent.NPMWEBDATA_AK0_43%></b></div>

    <%} %>
    <br />
    <div class="sw-btn-bar">             
        <orion:LocalizableButton id="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
    </div>
</asp:Content>