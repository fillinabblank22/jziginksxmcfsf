using System;
using SolarWinds.Orion.Web;
using Resources;

public partial class EditRecentlyPostedUniversalPollers : System.Web.UI.Page
{
	private ResourceInfo _resource;
	protected ResourceInfo Resource
	{
		get { return _resource; }
	}

	private string _netObjectID;

    public string ErrorRangeMessage
    {
        get
        {
            string title = NPMWebContent.NPMWEBCODE_LF0_7;
            return string.Format("<img src='/Orion/images/Small-Down.gif' title='{0}'/>", title);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        NumberOfDaysRangeValidator.ErrorMessage = this.ErrorRangeMessage;
        NumberOfDaysRangeValidator.MaximumValue = Int32.MaxValue.ToString();
        NumberOfRecordsValidator.ErrorMessage = this.ErrorRangeMessage;
        NumberOfRecordsValidator.MaximumValue = Int32.MaxValue.ToString();
    }

	protected override void OnInit(EventArgs e)
	{
		if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_18, this.Resource.Title);
		}

		if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
			_netObjectID = Request.QueryString["NetObject"];

		SortByList.SelectedValue = this.Resource.Properties["SortBy"];

		string showType = this.Resource.Properties["ShowType"];
		if (String.Equals(showType, "all", StringComparison.OrdinalIgnoreCase) || String.IsNullOrEmpty(showType))
		{
			ShowAll.Checked = true;
		}
		ShowLast.Checked = !ShowAll.Checked;

		int days;
		NumberOfDays.Text = Int32.TryParse(this.Resource.Properties["ShowLast"], out days) ? days.ToString() : "30";
		int records;
		NumberOfRecords.Text = Int32.TryParse(this.Resource.Properties["NumberOfRecords"], out records) ? records.ToString() : "15";
		bool limit;
		LimitCount.Checked = bool.TryParse(this.Resource.Properties["LimitCount"], out limit) ? limit : true; 
	}


	protected void SubmitClick(object sender, EventArgs e)
	{
		SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

		this.Resource.Properties["SortBy"] = SortByList.SelectedValue;
		this.Resource.Properties["ShowType"] = ShowAll.Checked ? "all" : "lastX";
		this.Resource.Properties["ShowLast"] = NumberOfDays.Text;
		this.Resource.Properties["NumberOfRecords"] = NumberOfRecords.Text;
		this.Resource.Properties["LimitCount"] = LimitCount.Checked ? true.ToString() : false.ToString();

		string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
		if (!string.IsNullOrEmpty(_netObjectID))
		{
			url = string.Format("{0}&NetObject={1}", url, _netObjectID);
		}
		Response.Redirect(url);
	}
}
