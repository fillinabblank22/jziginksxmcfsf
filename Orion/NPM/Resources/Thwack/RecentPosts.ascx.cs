using System;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class RecentPosts : BaseResourceControl
{
    private const int DefaultNumberOfPosts = 15;

	protected void Page_Load(object sender, EventArgs e)
	{
        btnHelp.Text = Resources.CoreWebContent.ResourcesAll_Help;
        btnHelp.NavigateUrl = HelpURL;
        btnEdit.NavigateUrl = EditURL;
    }
    
    public string ResourceID
	{
		get { return Resource.ID.ToString(); }
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public int Count
	{
		get 
		{
			int count;
			if (int.TryParse(Resource.Properties["NumberOfPosts"], out count))
			{
				return count;
			}
			return DefaultNumberOfPosts;
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_AK0_48; }
	}

	public override string SubTitle
	{
		get
		{
            return string.Format(Resources.NPMWebContent.NPMWEBCODE_AK0_49, this.Count.ToString());
		}
	}

	public override string EditURL
	{
		get
		{
			return string.Format("/Orion/NPM/Resources/Thwack/EditRecentPosts.aspx?ResourceID={0}&ViewID={1}&NetObject={2}", this.Resource.ID, this.Resource.View.ViewID, Page.Request.QueryString["NetObject"]);
		}
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceThwackRecentOrionPosts";
		}
	}

	public string HelpURL
	{
		get
		{
			return HelpHelper.GetHelpUrl(HelpLinkFragment);
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}
