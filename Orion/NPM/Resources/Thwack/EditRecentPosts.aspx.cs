using System;
using SolarWinds.Orion.Web;
using Resources;

public partial class EditRecentPosts : System.Web.UI.Page
{
	private int _resourceID;
	private ResourceInfo _resource;
	private string _netObjectID;
	private bool _isError;

	protected ResourceInfo Resource
	{
		get { return _resource; }
	}

	protected bool IsError
	{
		get { return _isError; }
		set { _isError = value; }
	}

    public string ErrorRangeMessage
    {
        get
        {
            string title = NPMWebContent.NPMWEBCODE_LF0_7;
            return string.Format("<img src='/Orion/images/Small-Down.gif' title='{0}'/>", title);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        PostsCountRangeValidator.MaximumValue = Int32.MaxValue.ToString();
        PostsCountRangeValidator.ErrorMessage = this.ErrorRangeMessage;
    }

	protected override void OnInit(EventArgs e)
	{
		if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
		{
			_resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			_resource = ResourceManager.GetResourceByID(_resourceID);
            Page.Title = string.Format(Resources.NPMWebContent.NPMWEBCODE_VB0_18, _resource.Title);
		}

		if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
		{
			_netObjectID = Request.QueryString["NetObject"];
		}
		else
		{
			_netObjectID = string.Empty;
		}

		if (!string.IsNullOrEmpty(Resource.Properties["NumberOfPosts"]) && !IsPostBack)
		{
			PostsCount.Text = Resource.Properties["NumberOfPosts"];
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		int count;
		if (Int32.TryParse(PostsCount.Text, out count) || string.IsNullOrEmpty(PostsCount.Text))
		{
			Resource.Properties.Clear();
			if (!string.IsNullOrEmpty(PostsCount.Text))
			{
				Resource.Properties.Add("NumberOfPosts", count.ToString());
			}

			string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
			if (!string.IsNullOrEmpty(_netObjectID))
			{
				url = string.Format("{0}&NetObject={1}", url, _netObjectID);
			}
			Response.Redirect(url);
		}
		else
		{
			IsError = true;
		}
	}
}
