using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI.Localizer;

public partial class RecentlyPostedUniversalPollers : BaseResourceControl
{
    private const int DefaultNumberOfPosts = 15;

	protected override string DefaultTitle
	{
		get { return Resources.NPMWebContent.NPMWEBCODE_AK0_47; }
	}

	public override string EditURL
	{
		get
		{
			return string.Format("/Orion/NPM/Resources/Thwack/EditRecentlyPostedUniversalPollers.aspx?ResourceID={0}&ViewID={1}&NetObject={2}", this.Resource.ID, this.Resource.View.ViewID, Page.Request.QueryString["NetObject"]);
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceThwackLatestUniversalDevicePollers"; }
	}

    public string HelpURL
    {
        get
        {
            return HelpHelper.GetHelpUrl(HelpLinkFragment);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        btnHelp.Text = Resources.CoreWebContent.ResourcesAll_Help;
        btnHelp.NavigateUrl = HelpURL;
        btnEdit.NavigateUrl = EditURL;
    }
    
    protected int Count
    {
        get
        {
            bool limit;
            int records;
            if (bool.TryParse(this.Resource.Properties["LimitCount"], out limit) && limit &&
                int.TryParse(this.Resource.Properties["NumberOfRecords"], out records))
            {
                return records;
            }
            return DefaultNumberOfPosts;
        }
    }

    protected DateTime OldestAllowedRssItem
    {
        get
        {
            string showType = this.Resource.Properties["ShowType"];
            if (String.IsNullOrEmpty(showType) || String.Equals(showType, "all", StringComparison.OrdinalIgnoreCase))
            {
                return DateTime.MinValue;
            }

            int days;
            days = Int32.TryParse(this.Resource.Properties["ShowLast"], out days) ? days : 30;
            return DateTime.Today.AddDays(-1 * days);
        }
    }

    protected string SortBy
    {
        get
        {
            return this.Resource.Properties["SortBy"];
        }
    }
}
