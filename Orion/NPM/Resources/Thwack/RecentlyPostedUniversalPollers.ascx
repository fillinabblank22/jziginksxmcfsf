<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecentlyPostedUniversalPollers.ascx.cs" Inherits="RecentlyPostedUniversalPollers" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Services>
			<asp:ServiceReference path="../../../Services/Thwack.asmx" />
    </Services>
</asp:ScriptManagerProxy>
<style type="text/css">
    #manageButtons .HelpButton {float: none !Important;}
    #manageButtons .EditResourceButton {float: none !Important;}
</style>
<orion:Include File="Thwack.css" runat="server" />
<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowHeaderBar=false>
    <Content>
	<div>
        <table cellpadding="0" cellspacing="0" border="0" class="ThwackHeaderBackground">
			<tr>
				<td style="border: none;height:36px;width:10px;">&nbsp;</td>
				<td style="border: 0;border-bottom: 0;">
					<table cellpadding="0px" cellspacing="0px" border="0" class="ThwackHeader" >
						<tr>
							<td class="ThwackHeaderLogo" style="border: none;">
								<img class="ThwackHeaderImage" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion", "ThwackImages/ThwackLogo_b.png") %>" alt="<%=Resources.NPMWebContent.NPMWEBDATA_AK0_46%>" />
							</td>
							<td class="ThwackHeaderText" style="border: none;">
								<span><%=Resources.NPMWebContent.NPMWEBDATA_JF0_2%></span>
							</td>
							<td id="manageButtons" style="border: none;" align="right">
								<asp:PlaceHolder ID="phEditButton" runat="server">
                                    <orion:LocalizableButtonLink ID="btnEdit" runat="server" href="<%# this.EditURL %>" DisplayType="Secondary" CssClass="EditResourceButton" LocalizedText="Edit" />
								</asp:PlaceHolder>&nbsp;
								<asp:PlaceHolder ID="phHelpButton" runat="server">
                                    <orion:LocalizableButtonLink ID="btnHelp" runat="server" href="<%# this.HelpURL %>" DisplayType="Secondary" CssClass="HelpButton" LocalizedText="CustomText" />
								</asp:PlaceHolder>
							</td>
						</tr>
					</table>
				</td>
				<td style="border: none;height:34px;width:15px;">&nbsp;</td>
			</tr>
		</table>
        <div runat="server" id="ThwackRecentlyPostedUniversalPollersList" class="ThwackContent" />
		<table cellpadding="0" cellspacing="0" class="ThwackSubmenu <%= this.Count % 2 == 0 ? "ZebraStripe" : ""%>">
            <tr>              
                <td style="border: none; text-align: right; padding-right: 10px;">
                    <a href="https://thwack.com/files/folders/pollers/default.aspx" class="CTA_bottom_resource"><%=Resources.NPMWebContent.NPMWEBDATA_AK0_32%> &#0187;</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="https://thwack.com/user/CreateUser.aspx" class="CTA_bottom_resource"><%=Resources.NPMWebContent.NPMWEBDATA_AK0_33%> &#0187;</a>
                </td>
            </tr>
        </table>
	</div>
        <script type="text/javascript">
//<![CDATA[
            Sys.Application.add_init(function () {
                var refresh = function (){
                    Thwack.GetRecentlyPostedUniversalPollers(<%= this.Count %>, '<%= this.OldestAllowedRssItem.ToString("s") %>','<%= this.SortBy %>', 
                        function(result) {
                            var thwackRecentPollersList = $get('<%=this.ThwackRecentlyPostedUniversalPollersList.ClientID %>');
                            thwackRecentPollersList.innerHTML = result;
                        },
                        function() {
                            var thwackRecentPollersList = $get('<%=this.ThwackRecentlyPostedUniversalPollersList.ClientID %>');
                            thwackRecentPollersList.innerHTML = '<%= ControlHelper.EncodeJsString(Resources.NPMWebContent.NPMWEBCODE_AK0_46)%>';
                        }); };
                SW.Core.View.AddOnRefresh(refresh, '<%= this.ThwackRecentlyPostedUniversalPollersList.ClientID %>');
                refresh();
            });
//]]>
        </script>
    </Content>
</orion:resourceWrapper>
