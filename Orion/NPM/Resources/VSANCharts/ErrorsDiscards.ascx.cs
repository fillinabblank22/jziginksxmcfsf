﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_VSANCharts_ErrorsDiscards : GraphResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        CreateChart(null, GetInterfaceInstance<IVSANProvider>().VSAN.NetObjectID, "VSANErrorsDiscards", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("VSANErrorsDiscards", Resource);
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.NPMWebContent.NPMWEBDATA_VB0_86;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceInOutErrorsDiscardsChart"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVSANProvider) }; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NPM/Controls/EditResourceControls/EditVSANCharts.ascx";
        }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}