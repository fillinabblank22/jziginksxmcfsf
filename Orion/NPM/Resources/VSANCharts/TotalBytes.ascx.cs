﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_VSANCharts_TotalBytes : GraphResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        CreateChart(null, GetInterfaceInstance<IVSANProvider>().VSAN.NetObjectID, "VSANTotalBytes", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("VSANTotalBytes", Resource);
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.NPMWebContent.NPMWEBDATA_TM0_14;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceTotalBytesTransferredChart"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVSANProvider) }; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NPM/Controls/EditResourceControls/EditVSANCharts.ascx";
        }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}