﻿using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.AccountManagement.Models;
using System;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.AccountManagement.Saml;

namespace SolarWinds.Orion.AccountManagement.LegacyWebSite
{
    public partial class Orion_SamlTestResult : Page
	{
		protected static readonly Log log = new Log();

		protected SamlTestResult samlTestResult;
		protected string samlRequest;
		protected string samlResponse;

		protected void Page_Load(object sender, EventArgs e)
        {
            var relayState = SamlWebHelper.GetRelayState(Request);
			samlTestResult = Application[relayState.TestKey] as SamlTestResult;

			try
			{
				log.WarnFormat("SAML Test Result:\r\n{0}",
					JsonConvert.SerializeObject(samlTestResult, Formatting.Indented).Replace(@"\r\n", "\r\n"));
			}
			catch (Exception ex)
			{
				log.Error("Cannot log SAML test result.", ex);
			}

			samlRequest = HttpUtility.HtmlEncode(samlTestResult?.SamlRequest?.Trim());
			samlResponse = HttpUtility.HtmlEncode(samlTestResult?.SamlResponse?.Trim());

			// clear stored result
			Application.Remove(relayState.TestKey);
		}
	}
}