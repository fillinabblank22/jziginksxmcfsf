﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApplicationPopup.aspx.cs" Inherits="Orion_APM_ApplicationPopup" MasterPageFile="~/Orion/APM/Popup.master" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="apm" TagName="PopupComponentList" Src="~/Orion/APM/Controls/PopupComponentList.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="npm" TagName="CPULoad" Src="~/Orion/NetPerfMon/Controls/CPULoad.ascx" %>
<%@ Register TagPrefix="npm" TagName="MemoryUsed" Src="~/Orion/NetPerfMon/Controls/MemoryUsed.ascx" %>
<asp:Content runat="server" ContentPlaceHolderID="NetObjectTipBody">
    <p class="StatusDescription"><%= string.Format(APMWebContent.APMWEBDATA_TM0_58, App.Name, this.App.NodeName) %></p>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= APMWebContent.APMWEBDATA_TM0_50 %></th>
            <td align="center"><apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server" /></td>
            <td><%= string.Format(APMWebContent.APMWEBDATA_TM0_56, this.App.Status.ToLocalizedString()) %></td>
        </tr>
        <tr>
            <th><%= APMWebContent.APMWEBDATA_TM0_51 %></th>
            <td align="center"><orion:SmallNodeStatus ID="ServerStatusIcon" runat="server" /></td>
            <td><%= string.Format(APMWebContent.APMWEBDATA_TM0_57, this.App.NPMNode.Status.ParentStatus.ToLocalizedString()) %></td>
        </tr>
        <asp:PlaceHolder ID="phCpuLoad" runat="server">
            <tr>
                <th><%= APMWebContent.APMWEBDATA_TM0_52 %></th>
                <td><span style="white-space: nowrap;"><npm:CPULoad runat="server" ID="CPULoad" /></span></td>
                <orion:InlineBar runat="server" ID="CPULoadBar" />
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phMemUsed" runat="server">
            <tr>
                <th><%= APMWebContent.APMWEBDATA_TM0_53 %></th>
                <td><span style="white-space: nowrap;"><npm:MemoryUsed runat="server" ID="MemoryUsed" /></span></td>
                <orion:InlineBar runat="server" ID="MemoryUsedBar" />
            </tr>
        </asp:PlaceHolder>
    </table>

    <apm:PopupComponentList ID="componentList" runat="server"/>

</asp:Content>
