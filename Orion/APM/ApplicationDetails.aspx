<%@ Page Language="C#" MasterPageFile="ApmView.master" AutoEventWireup="true" CodeFile="ApplicationDetails.aspx.cs" Inherits="Orion_APM_ApplicationDetails" %>
<%@ Import  Namespace="SolarWinds.APM.Web" %>
<%@ Import  Namespace="SolarWinds.APM.Web.UI" %>
<%@ Register TagPrefix="apm" Assembly="SolarWinds.APM.Web" Namespace="SolarWinds.APM.Web.UI" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="apm" TagName="ApmAppStatusIcon" Src="~/Orion/APM/Controls/ApmAppStatusIcon.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <style>
        .custPageLink {
            background-image: url('/Orion/APM/Images/Page.Icon.CustomPg.gif') !important;
        }
    </style>
    
        <%if (Profile.AllowCustomize) {%>
		    <a href="<%=CustomizeViewHref%>" onclick='<%=CustomizeViewClientHandler%>' class="custPageLink"><%= Resources.APMWebContent.APMWEBDATA_TM0_1 %></a>
	    <%}%>

	    <%if (ApmRoleAccessor.AllowAdmin)
       { %>
			<a href="<%=ApmMasterPage.GetEditApplicationPageUrl(ApmApplication.Id)%>"  
               class="custPageLink" 
               style="background-image: url('/Orion/APM/Images/Button.EditApplication.gif');">  
                  <%= Resources.APMWebContent.APMWEBDATA_TM0_2 %>
			</a>
	    <%}%>

    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHAppDetails" ID="btnHelp" runat="server" />

	<br />
    
    <div style="padding-top: 5px; font-size: 8pt; text-align: right;">
        <%=DateTime.Now.ToString("F")%>
    </div>
</asp:Content>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="ApmPageTitle" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <style type="text/css">.ext-gecko .x-window-body .x-form-item { overflow: hidden; }</style>
    
    <h1><%=ViewInfo.ViewTitle %></h1>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding-left:15px; padding-right:5px;">
			    <apm:ApmAppStatusIcon ID="ApmAppStatusIcon" runat="server" />
			</td>
			<td style="font-weight:bold;">
                <%= String.Format(Resources.APMWebContent.APMWEBDATA_TM0_5, GetApplicationInnerHtml(), GetNodeInnerHtml()) %>
			</td>
		</tr>
	</table>
<br />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<apm:ApmApplicationResourceHost ID="resHost" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</apm:ApmApplicationResourceHost>
</asp:Content>