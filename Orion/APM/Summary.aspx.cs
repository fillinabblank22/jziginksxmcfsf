using System;

using SolarWinds.APM.Web;

public partial class Orion_APM_Summary : ApmViewPage
{
	public override string ViewType
	{
		get { return "APM Summary"; }
	}
	
    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
}
