﻿using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using SolarWinds.APM.Web.Extensions;
using Strings = Resources.APMWebContent;

public partial class Orion_APM_Admin_EventLogViewer_Default : System.Web.UI.Page
{
	#region Fields

	private string accountId;

	private bool nodeInitialize = false;
	private Node node;

	private bool credInitialize = false;
	private CredentialSet cred;

	private bool logsInitialize = false;
	private List<WindowsEventLogInfo> logs;

	#endregion

	#region Properties

	private string AccountId
	{
		get { return accountId ?? (accountId = Context.User.Identity.Name); }
	}

	private int NodeId
	{
		get
		{
			var id = 0;
			int.TryParse(Request.QueryString["NodeId"], out id);

			return id;
		}
	}

	private Node Node
	{
		get
		{
			if (!nodeInitialize)
			{
				nodeInitialize = true;

				var id = NodeId;
				if (id > 0)
				{
					using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
					{
						try
						{
							node = bl.GetNode(id);
						}
						catch { }
					}
				}
			}
			if (node == null)
			{
				throw new ArgumentMissingException("Node doesn't exist.");
			}
			return node;
		}
	}

	private CredentialSet Cred
	{
		get
		{
			if (OrionConfiguration.IsDemoServer)
			{
				if (cred == null) 
				{
					cred = new CredentialSet
					{
						Id = ComponentBase.NoCredentialSetId,
						Name = "[None]"
					};
				}
				return cred;
			}

			if (!credInitialize)
			{
				credInitialize = true;

				var id = 0;
				if (int.TryParse(Request.QueryString["CredId"], out id) && id > 0)
				{
					using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
					{
						try
						{
							cred = bl.GetCredentialsById(id);
						}
						catch { }
					}
				}
				if (cred == null && (Node.NodeSubType == NodeSubType.WMI || node.NodeSubType == NodeSubType.Agent))
				{
					cred = new CredentialSet
					{
						Id = ComponentBase.NodeWmiCredentialsId,
						Name = Strings.APMWEBCODE_AK1_96.Replace("<", "[").Replace(">", "]")
					};
				}
			}
            IHostHelper hosthelper = new HostHelper();
            if (cred == null && hosthelper.IsLocal(node.IpAddress))
			{
				cred = new CredentialSet { Id = 0, Name = "[None]" };
			}
			if (cred == null)
			{
				var message = @"Unable to load events with the supplied WMI credentials.
Please try to use different credentials.";
				throw new ApplicationException(message);
			}
			return cred;
		}
	}

	private List<WindowsEventLogInfo> Logs
	{
		get
		{
			if (!logsInitialize)
			{
				logsInitialize = true;

				using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
				{
					var sysInfo = bl.RealTimeGetSystemEventsInfo(AccountId, Node.Id, Cred.Id);
					if (sysInfo.ErrorMessage.IsValid())
					{
						throw new Exception(sysInfo.ErrorMessage);
					}

					logs = sysInfo.Logs;
					if (logs.Count > 0)
					{
						foreach (var log in logs)
						{
							if (log.SourceNames.Length > 0)
							{
								log.SourceNames = log.SourceNames
									.OrderBy(item => item, StringComparer.InvariantCultureIgnoreCase)
									.ToArray();
							}
						}
					}
				}
			}
			return logs;
		}
	}

	private string FilterLog
	{
		get
		{
			if (Request.QueryStringOrForm("Log").IsValid())
			{
				return Request.QueryStringOrForm("Log");
			}
			return "Application";
		}
	}

	private int FilterLevels
	{
		get
		{
			var level = 0;
			if (int.TryParse(Request.QueryStringOrForm("Levels"), NumberStyles.Integer, CultureInfo.InvariantCulture, out level) && level > 0)
			{
				return level;
			}
			return 31;
		}
	}

	private Dictionary<string, bool> FilterSources
	{
		get
		{
			if (Request.QueryStringOrForm("Sources").IsValid())
			{
				var sources = JsHelper.Deserialize<string[]>(Request.QueryStringOrForm("Sources"));
				if (sources.Length > 0) 
				{
					var result = new Dictionary<string, bool>();
					foreach (var source in sources) 
					{
						var range = source;
						if (!range.Contains("-"))
						{
							range = string.Format("{0}-{0}", range); 
						}
						var parts = range.Split('-');
						for (int from = int.Parse(parts.First()), to = int.Parse(parts.Last()); from <= to; from++) 
						{
							result[from.ToInvariantString()] = true;
						}
					}
					return result;
				}
			}
			return null;
		}
	}

	private bool FilterSourcesVisible 
	{
		get 
		{
			var visible = false;
			bool.TryParse(Request.QueryStringOrForm("SourcesVisible"), out visible);
			
			return visible;
		}
	}

	#endregion

	#region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
	{
		ValidateRight();

		Page.Title = InvariantString.Format(Strings.APMWEBDATA_AK1_208, Strings.APMWEBDATA_VB1_343, Node.Name);

        ApmBaseResource.EnsureApmScripts();
    }

	#endregion

	#region Helper Members

    protected string GetPollingTechnology()
    {
        return Node.NodeSubType == NodeSubType.Agent ? "Agent" : "WMI";
    }

	protected string GetJsonParameters()
	{
		var nodeObj = default(object);
		var credObj = default(object);
		var logsObj = default(object);
		try
		{
			var uri = new Uri(Request.Url.AbsoluteUri, UriKind.Absolute);
			var isRemoteForCredentialDialog = (uri.Scheme.Equals("http") && !Request.IsLocal);

            IHostHelper hostHelper = new HostHelper();
			nodeObj = new
			{
				id = Node.Id,
				name = Node.Name,
				ip = Node.IpAddress,
				status = Node.Status,
				nodeSubType = Node.NodeSubType.ToString(),
                isLocalHost = hostHelper.IsLocal(Node.IpAddress),
				isRemoteForCredentialDialog = isRemoteForCredentialDialog
			};
			credObj = new
			{
				id = Cred.Id,
				name = Cred.Name
			};

			logsObj = Logs
				.Select(item => new object[] { item.LogName, item.SourceNames })
				.ToArray();

			var resp = new
			{
				node = nodeObj,
				cred = credObj,
				logs = logsObj,
				filterLog = FilterLog,
				filterLevels = FilterLevels,
				filterSources = FilterSources,
				filterSourcesVisible = FilterSourcesVisible,
				isDemoServer = OrionConfiguration.IsDemoServer
			};
			return JsHelper.Serialize(resp);
		}
		catch (Exception ex)
		{
			if (nodeObj == null)
			{
				nodeObj = new
				{
					id = 0,
					name = Strings.APMWEBCODE_AK1_23,
					ip = Strings.APMWEBCODE_AK1_23,
					status = 0,
					nodeSubType = Strings.APMWEBCODE_AK1_23,
					isLocalHost = false,
					isRemoteForCredentialDialog = false
				};
			}
			if (credObj == null)
			{
				credObj = new { id = 0, name = Strings.APMWEBCODE_AK1_23 };
			}
			if (logsObj == null)
			{
				logsObj = new[] { new object[] { "Application", new object[] { } } };
			}

			var status = 500;
			if (ex is AccessViolationException)
			{
				status = 403;
			}

			var resp = new
			{
				node = nodeObj,
				cred = credObj,
				logs = logsObj,
				filterLog = FilterLog,
				filterLevels = FilterLevels,
				isDemoServer = OrionConfiguration.IsDemoServer,
				error = new
				{
					status = status,
					message = ex.Message
				}
			};
			return JsHelper.Serialize(resp);
		}
	}

	protected void ValidateRight()
	{
		if (!ApmRoleAccessor.AllowRealTimeEventExplorer)
		{
			Server.Transfer(InvariantString.Format("/Orion/Error.aspx?Message={0}", HttpUtility.UrlEncode("You do not have sufficient rights to view the Log and Event Viewer.")));
		}

		if (Node == null)
		{
			return;
		}

		var result = SwisProxy.ExecuteQuery(InvariantString.Format(@"SELECT n.NodeName FROM Orion.Nodes AS n WHERE n.NodeID = {0}", NodeId));
		if (result.Rows.Count == 0)
		{
			Server.Transfer(InvariantString.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", NodeId));
		}
	}

	#endregion
}