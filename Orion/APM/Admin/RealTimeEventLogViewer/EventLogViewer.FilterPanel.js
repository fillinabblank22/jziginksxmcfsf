﻿Ext.ns("SW.APM.RealTimeEvents");

SW.APM.RealTimeEvents.FilterPanel = function (manager) {
	var EM = SW.APM.EventManager;

	var mMan = manager;

	var mLogs = null, mDefLog = null, mDefLevels = null, mDefSources = null, mDefSourcesVisible = false;
	var mCriteria = null;

	var mPanel = null;

	/*helper members*/
	var getLogSources = function () {
		var logName = getLogName();
		for (var i = 0; i < mLogs.length; i++) {
			if (mLogs[i][0] == logName) { return mLogs[i][1]; }
		}
		return [];
	};
	var renderSources = function (selIndexes, sourcesVisible, callback) {
		var panel = Ext.getCmp("sourcesPanel");
		if (sourcesVisible === null) {
			sourcesVisible = panel.isVisible();
		}
		for (var i = 0; i < panel.items.length; i++) {
			panel.items.items[i].destroy();
		}
		panel.items.clear();
		panel.doLayout();

		var allSources = getLogSources();
		if (allSources.length > 0) {
			var doIt = function () {

				for (var i = 0; i < allSources.length; i++) {
					var item = new Ext.form.Checkbox({
						boxLabel: allSources[i], name: allSources[i], cls: "source-cb", checked: (selIndexes == null || selIndexes[i])
					});
					panel.items.add(item);
				}
				if (sourcesVisible) {
					panel.show();
					panel.doLayout();
					Ext.getCmp("sourceTypes").setValue("1");
				} else {
					panel.hide();
					Ext.getCmp("sourceTypes").setValue("0");
				}

				if (Ext.isFunction(callback)) {
					callback();
				}
			}
			setTimeout(doIt, 100);
		} else {
			if (Ext.isFunction(callback)) {
				callback();
			}
		}
	}
	var populateUi = function (log, levels, sources, sourcesVisible, callback) {
		Ext.getCmp("logNames").setValue(log);

		Ext.getCmp("errorCb").setValue((levels & 1) > 0);
		Ext.getCmp("warningCb").setValue((levels & 2) > 0);
		Ext.getCmp("informationCb").setValue((levels & 4) > 0);
		Ext.getCmp("auditSuccessCb").setValue((levels & 8) > 0);
		Ext.getCmp("auditFailureCb").setValue((levels & 16) > 0);

		renderSources(sources, sourcesVisible, callback);

		Ext.getCmp("subFilter").setTitle(SF(RTWEM_FilterSubTitleTpl, getLogName()))
	}

	/*criteria members*/
	var getLogName = function () {
		return Ext.getCmp("logNames").getValue();
	};
	var getSources = function () {
		var sources = [];
		Ext.getCmp("sourcesPanel").items.each(function (item) {
			if (item.getValue()) { sources.push(item.name); }
		});
		return sources;
	};
	var getGrippedSources = function () {
		var selSources = getSources(), allSources = getLogSources();
		var indexes = [], obj = { firstIndex: -1, lastIndex: -1 };

		var push = function () {
			if (obj.firstIndex === obj.lastIndex) {
				indexes.push(SF("{0}", obj.firstIndex));
			} else {
				indexes.push(SF("{0}-{1}", obj.firstIndex, obj.lastIndex));
			}
		}
		for (var index = -1, i = 0; i < selSources.length; i++) {
			index = Ext.Array.indexOf(allSources, selSources[i]);
			if (obj.firstIndex === -1) {
				obj.firstIndex = obj.lastIndex = index;
			} else {
				if (obj.lastIndex + 1 !== index) {
					push()
					obj.firstIndex = index;
				}
				obj.lastIndex = index;
			}
			if (i + 1 === selSources.length || index + 1 === allSources.length) {
				push()
			}
		}
		return indexes;
	};
	var getLevels = function () {
		var level = 0;
		if (Ext.getCmp("errorCb").getValue()) { level |= 1; }
		if (Ext.getCmp("warningCb").getValue()) { level |= 2; }
		if (Ext.getCmp("informationCb").getValue()) { level |= 4; }
		if (Ext.getCmp("auditSuccessCb").getValue()) { level |= 8; }
		if (Ext.getCmp("auditFailureCb").getValue()) { level |= 16; }
		return level;
	};

	var applyCriteria = function () {
		var newCriteria = {
			LogName: getLogName(),
			SourceNames: getSources(),
			GrippedSources: getGrippedSources(),
			Levels: getLevels(),
			Limit: RTWEM_PAGE_SIZE,
			TimeZoneOffset: new Date().getTimezoneOffset()
		};

		var isDirty = (
			mCriteria == null ||
			newCriteria.LogName != mCriteria.LogName ||
			newCriteria.Levels != mCriteria.Levels ||
			newCriteria.GrippedSources.join("|") != mCriteria.GrippedSources.join("|")
		);

		mCriteria = newCriteria;
		return isDirty;
	};
	var validateSources = function () {
		if (getSources().length == 0) {
			Ext.Msg.show({ title: RTWEM_ErrorTitle, msg: "Please select at least one existing source name.", buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR });
			return false;
		}
		return true;
	};
	var validateLevels = function () {
		if (getLevels() == 0) {
			Ext.Msg.show({ title: RTWEM_ErrorTitle, msg: "Please select at least one existing event level.", buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR });
			return false;
		}
		return true;
	};

	/*event handlers*/
	function onLogNameBeforeSelect() { return validateLevels(); }
	function onLogNameSelect() {
		populateUi(getLogName(), getLevels(), null, null, function () {
			applyCriteria();
			EM.fire("rtwem-filter-criteria-changed");
		});
	}

	function onSourceTypeSelect() {
		var val = Ext.getCmp("sourceTypes").getValue();
		if (val == "0") {
			onSelectAllSrcClick();
			if (applyCriteria()) {
				EM.fire("rtwem-filter-criteria-changed");
			}
			Ext.getCmp("sourcesPanel").hide();
		}
		else if (val == "1") {
			Ext.getCmp("sourcesPanel").show();
		}
	}

	function onSelectAllSrcClick() {
		Ext.getCmp("sourcesPanel").items.each(function (item) {
			item.setValue(true);
		});
	}
	function onSelectNoneSrcClick() {
		Ext.getCmp("sourcesPanel").items.each(function (item) {
			item.setValue(false);
		});
	}

	function onApplyButtonClick() {
		if ($("#rtwemFilterPanel a.sw-btn-disabled").size() > 0) { return; }

		if (validateSources() && validateLevels()) {
			applyCriteria();
			EM.fire("rtwem-filter-criteria-changed");
		}
	}
	function onResetButtonClick() {
		if ($("#rtwemFilterPanel a.sw-btn-disabled").size() > 0) { return; }

		populateUi(getLogName(), mDefLevels, mDefSources, mDefSourcesVisible, function () {
			if (applyCriteria()) {
				EM.fire("rtwem-filter-criteria-changed");
			}
		});
	}

	/*public members*/
	this.getPanel = function () { return mPanel; };

	this.populateUi = function (callback) {
		populateUi(mDefLog, mDefLevels, mDefSources, mDefSourcesVisible, function () {
			applyCriteria();
			if (Ext.isFunction(callback)) { callback(); }
		});
	};

	this.getCriteria = function () {
		if (mCriteria == null) { applyCriteria(); }
		return mCriteria;
	};
	this.validateCriteria = function () {
		return validateSources() && validateLevels();
	};

	this.getState = function () {
		var criteria = this.getCriteria();
		return {
			log: criteria.LogName,
			levels: criteria.Levels,
			sources: criteria.GrippedSources,
			sourcesVisible: Ext.getCmp("sourcesPanel").isVisible()
		};
	};

	this.create = function (defLog, defLevels, defSources, defSourcesVisible) {
		var LLT = "<span><img src='/Orion/APM/Images/EventLog/{0}.png' height='16' width='16'/> {1}</span>";

		mLogs = mMan.getLogs();
		mDefLog = defLog; mDefLevels = defLevels; mDefSources = defSources; mDefSourcesVisible = defSourcesVisible;

		mPanel = Ext.create("Ext.panel.Panel", {
			id: "rtwemFilterPanel", region: "west", title: RTWEM_FilterTitle, minWidth: 390, width: 390, maxWidth: 390,
			frame: true, border: false, autoScroll: true, collapsible: true, split: true, collapseMode: "header", 
			cls: "x-border-box", margins: "0px", padding: "0px",
			defaults: { style: { marginLeft: "10px", marginTop: "5px" } },
			items: [
				{ xtype: "label", text: RTWEM_FilterLogLabel, cls: "font-bold-13", style: { marginLeft: "5px" } },
				{
					xtype: "combo", id: "logNames", cls: "x4-reset", editable: false, width: 300,
					style: { marginLeft: "15px" },
					displayField: "LogName", valueField: "LogName", value: mDefLog,
					triggerAction: "all", mode: "local", store: new Ext.data.ArrayStore({ fields: ["LogName", "SourceNames"], data: mLogs }),
					listeners: { select: onLogNameSelect, beforedeselect: onLogNameBeforeSelect }
				},
				{
					id: "subFilter", xtype: "fieldset", layout: "anchor", title: "Filter", width: 340, style: { margin: "5px" },
					defaults: { style: { marginLeft: "10px" } },
					items: [
						{ xtype: "label", html: RTWEM_FilterSourceLabel, cls: "font-bold-13", style: { marginLeft: "0px" } },
						{
							xtype: "combo", id: "sourceTypes", cls: "x4-reset", width: 290, allowBlank: false, editable: false, triggerAction: "all", typeAhead: false,
							value: (mDefSourcesVisible ? "1" : "0"), mode: "local", store: [["0", "All Sources"], ["1", "Custom Sources"]],
							listeners: { select: onSourceTypeSelect }
						},
						{
							xtype: "panel", id: "sourcesPanel", frame: true, border: false, height: 250, width: 280, autoScroll: true, hidden: !mDefSourcesVisible,
							tbar: [
								{ xtype: "button", text: RTWEM_SelectAll, icon: "/Orion/images/Icon.SelectAll.gif", handler: onSelectAllSrcClick },
								{ xtype: "button", text: RTWEM_SelectNone, icon: "/Orion/images/Icon.SelectNone.gif", handler: onSelectNoneSrcClick }
							]
						},
						{ xtype: "label", html: RTWEM_FilterLevelLabel, cls: "font-bold-13", style: { margin: "0px" } },
						{ xtype: "checkboxfield", id: "errorCb", boxLabel: SF(LLT, "Error", "Error"), checked: (mDefLevels & 1) > 0, width: 300 },
						{ xtype: "checkboxfield", id: "warningCb", boxLabel: SF(LLT, "Warning", "Warning"), checked: (mDefLevels & 2) > 0, width: 300 },
						{ xtype: "checkboxfield", id: "informationCb", boxLabel: SF(LLT, "Information", "Information"), checked: (mDefLevels & 4) > 0, width: 300 },
						{ xtype: "checkboxfield", id: "auditSuccessCb", boxLabel: SF(LLT, "SuccessAudit", "Security Audit Success"), checked: (mDefLevels & 8) > 0, width: 300 },
						{ xtype: "checkboxfield", id: "auditFailureCb", boxLabel: SF(LLT, "FailureAudit", "Security Audit Failure"), checked: (mDefLevels & 16) > 0, width: 300 },
						{
							xtype: "fieldset", layout: "anchor", frame: true,
							style: { marginLeft: "0px", marginTop: "15px", border: "none" },
							defaults: { xtype: "label" },
							items: [
								{ id: "rtwemBtnApply", html: $("#rtwem-btn-apply-filter").html() },
								{ id: "rtwemBtnReset", html: $("#rtwem-btn-reset-filter").html() }
							]
						}
					]
				}
			],
			listeners: {
				afterrender: function () {
					$("#rtwemFilterPanel #rtwemBtnApply")
						.unbind("click")
						.bind("click", onApplyButtonClick);
					$("#rtwemFilterPanel #rtwemBtnReset")
						.unbind("click")
						.bind("click", onResetButtonClick);
				}
			}
		});

		SW.APM.EventManager.on("rtwem-error", function () {
			$("#rtwemFilterPanel a.sw-btn").addClass("sw-btn-disabled");

			Ext.getCmp("subFilter").setDisabled(true);
		});
	}
};