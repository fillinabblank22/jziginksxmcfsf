﻿Ext.override(Ext.selection.Model, {
	storeHasSelected: function (record) {
		var store = this.store,
            records,
            len, id, i;

		if (record.hasId() && store.getById(record)) {
			return true;
		}

		if (store.data instanceof Ext.util.LruCache) {
			var result = false;
			store.data.forEach(function (rec) {
				return !(result = (record.internalId === rec.internalId));
			});
			return result;
		}
		records = store.data.items;
		len = records.length;
		id = record.internalId;
		for (i = 0; i < len; ++i) {
			if (id === records[i].internalId) {
				return true;
			}
		}
		return false;
	}
});

Ext.override(Ext.data.Store, {
	removeAll: function (silent) {
		var me = this;

		if (me.snapshot && me.data) {
			me.snapshot.removeAll(me.data.getRange());
		}

		if (me.buffered) {
			if (me.data) {
				if (silent) { me.suspendEvent('clear'); }
				me.data.clear();
				if (silent) { me.resumeEvent('clear'); }
			}
		} else {
			me.remove({ start: 0, end: me.getCount() - 1 }, false, silent);
			if (silent !== true) {
				me.fireEvent('clear', me);
			}
		}
	},
	remove: function (records, isMove, silent) {
		isMove = isMove === true;
		var me = this, sync = false, snapshot = me.snapshot, data = me.data, i = 0, length, info = [], allRecords = [], indexes = [], item, isNotPhantom, index,
            record, removeRange, removeCount, fireRemoveEvent = !silent && me.hasListeners.remove;
		if (records.isModel) {
			records = [records];
			length = 1;
		}
		else if (Ext.isIterable(records)) {
			length = records.length;
		}
		else if (typeof records === "object") {
			removeRange = true;
			i = records.start;
			length = records.end + 1;
			removeCount = length - i;
		}
		if (!removeRange) {
			for (i = 0; i < length; ++i) {
				record = records[i];
				if (typeof record == "number") {
					index = record;
					record = data.getAt(index);
				}
				else {
					index = me.indexOf(record);
				}
				if (record && index > -1) { info.push({ record: record, index: index }); }
				if (snapshot) { snapshot.remove(record); }
			}
			info = Ext.Array.sort(info, function (o1, o2) {
				var index1 = o1.index, index2 = o2.index;
				return index1 === index2 ? 0 : (index1 < index2 ? -1 : 1);
			});
			i = 0;
			length = info.length;
		}

		for (; i < length; i++) {
			if (removeRange) {
				record = data.getAt(i);
				index = i;
			} else {
				item = info[i];
				record = item.record;
				index = item.index;
			}

			allRecords.push(record);
			indexes.push(index);

			isNotPhantom = record.phantom !== true;

			if (!isMove && isNotPhantom) {
				record.removedFrom = index;
				me.removed.push(record);
			}
			record.unjoin(me);

			index -= i;
			sync = sync || isNotPhantom;

			if (!removeRange) {
				data.removeAt(index);
				if (fireRemoveEvent) {
					me.fireEvent("remove", me, record, index, !!isMove);
				}
			}
		}
		if (removeRange) {
			if (data instanceof Ext.util.LruCache || data instanceof Ext.util.HashMap) {
				data.clear();
			} else if (data.removeRange) {
				data.removeRange(records.start, removeCount);
			}
		}

		if (!silent) {
			me.fireEvent("bulkremove", me, allRecords, indexes, !!isMove);
			me.fireEvent("datachanged", me);
		}
		if (!isMove && me.autoSync && sync && !me.autoSyncSuspended) {
			me.sync();
		}
	}
});

Ext.override(Ext.grid.plugin.BufferedRenderer, {

	scrollToLoadBuffer: 300,
	wemReqStart: 0, wemReqEnd: 70,

	handleViewScroll: function (direction) {
		var me = this,
            rows = me.view.all,
            store = me.store,
            viewSize = me.viewSize,
            totalCount = (store.buffered ? store.getTotalCount() : store.getCount()),
            requestStart,
            requestEnd;

		if (direction == -1) { //UP
			if (rows.startIndex) {
				if ((me.getFirstVisibleRowIndex() - rows.startIndex) < me.numFromEdge) {
					requestStart = Math.max(0, me.getLastVisibleRowIndex() + me.trailingBufferZone - viewSize);
				}
			}
		} else {
			if (rows.endIndex < totalCount - 1) {
				if ((rows.endIndex - me.getLastVisibleRowIndex()) < me.numFromEdge) {
					requestStart = Math.max(0, me.getFirstVisibleRowIndex() - me.trailingBufferZone);
				}
			}
		}
		if (requestStart != null) {
			requestEnd = Math.min(requestStart + viewSize - 1, totalCount - 1);

			me.wemReqStart = requestStart;
			me.wemReqEnd = requestEnd;

			if (requestStart !== rows.startIndex || requestEnd !== rows.endIndex) {
				me.renderRange(requestStart, requestEnd);
				return;
			}
		}
		if (me.lockingPartner && me.lockingPartner.view.el && me.lockingPartner.scrollTop !== me.scrollTop) {
			me.lockingPartner.view.el.dom.scrollTop = me.scrollTop;
		}
	},
	doAttemptLoad: function (start, end) {
		var that = this;
		that.store.getRange(start, end, {
			callback: function (pRecords, start, end, opt) {
			}, scope: that, fireEvent: false
		});
	}
});

Ext.override(Ext.view.AbstractView, {
	refresh: function () {
		var me = this, targetEl, targetParent, oldDisplay, nextSibling, dom, records;

		if (!me.rendered || me.isDestroyed) { return; }

		if (!me.hasListeners.beforerefresh || me.fireEvent('beforerefresh', me) !== false) {
			targetEl = me.getTargetEl();
			records = me.getViewRange();
			dom = targetEl.dom;
			if (!me.preserveScrollOnRefresh) {
				targetParent = dom.parentNode;
				oldDisplay = dom.style.display;
				dom.style.display = 'none';
				nextSibling = dom.nextSibling;
				targetParent.removeChild(dom);
			}

			if (me.refreshCounter) {
				me.clearViewEl();
			} else {
				me.fixedNodes = targetEl.dom.childNodes.length;
				me.refreshCounter = 1;
			}
			me.tpl.append(targetEl, me.collectData(records, me.all.startIndex));

			if (!records || records.length == 0) {
				if (!me.store.loading && (!me.deferEmptyText || me.hasFirstRefresh)) {
					Ext.core.DomHelper.insertHtml('beforeEnd', targetEl.dom, me.emptyText);
				}
				me.all.clear();
			} else {
				me.collectNodes(targetEl.dom);
				me.updateIndexes(0);
			}
			if (me.hasFirstRefresh) {
				if (me.refreshSelmodelOnRefresh !== false) {
					me.selModel.refresh();
				} else {
					me.selModel.pruneIf();
				}
			}
			me.hasFirstRefresh = true;
			if (!me.preserveScrollOnRefresh) {
				targetParent.insertBefore(dom, nextSibling);
				dom.style.display = oldDisplay;
			}
			this.refreshSize();
			me.fireEvent('refresh', me);
			if (!me.viewReady) {
				me.viewReady = true;
				me.fireEvent('viewready', me);
			}
		}
	}
});