﻿using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Models.ProcessMonitor;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

public partial class Orion_APM_Admin_RealTimeEventLogViewer_DataProvider : System.Web.UI.Page
{
	private bool nodeInitialize = false;
	private Node node;

	#region Properties

	private string AccountId
	{
		get { return Context.User.Identity.Name; }
	}

	private int NodeId
	{
		get
		{
			var value = 0;
			if (ToInt(Request.QueryString["cNodeId"], out value) && value > 0)
			{
				return value;
			}
			throw new Exception(string.Format(CultureInfo.InvariantCulture, @"Invalid node id - ""{0}""", Request.QueryString["nodeId"]));
		}
	}

	private Node Node
	{
		get
		{
			if (!nodeInitialize)
			{
				nodeInitialize = true;

				var id = NodeId;
				if (id > 0)
				{
					using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
					{
						try
						{
							node = bl.GetNode(id);
						}
						catch { }
					}
				}
			}
			if (node == null)
			{
				throw new ArgumentMissingException("Node doesn't exist.");
			}
			return node;
		}
	}

	private int CredId
	{
		get
		{
			var value = 0;
			if (ToInt(Request.QueryString["cCredId"], out value))
			{
				return value;
			}
			throw new Exception(string.Format(CultureInfo.InvariantCulture, @"Invalid credential id - ""{0}""", Request.QueryString["credId"]));
		}
	}

	private RealTimeSystemEventsFilter Filter
	{
		get
		{
			var filter = new RealTimeSystemEventsFilter
			{
				Start = 0,
				Limit = 70,
				Buffer = 4,

				LogName = Request.QueryString["cLog"],
				SourceNames = new string[0],
				Levels = RealTimeEventLevel.Information | RealTimeEventLevel.Warning | RealTimeEventLevel.Error | RealTimeEventLevel.FailureAudit | RealTimeEventLevel.SuccessAudit,

				TimeZoneOffset = 0
			};

			var intValue = 0;
			if (ToInt(Request.QueryString["cStart"], out intValue) && intValue > 0)
			{
				filter.Start = intValue;
			}
			if (ToInt(Request.QueryString["cLimit"], out intValue) && intValue > 0)
			{
				filter.Limit = intValue;
			}
			if (ToInt(Request.QueryString["cLastTotalCount"], out intValue) && intValue > 0)
			{
				filter.LastTotalCount = intValue;
			}

			var uintValue = uint.MinValue;
			if (ToUint(Request.QueryString["cLastFirstRecord"], out uintValue))
			{
				filter.LastFirstRecord = uintValue;
			}

			if (ToInt(Request.QueryString["cLevels"], out intValue) && intValue > 0)
			{
				filter.Levels = (RealTimeEventLevel)intValue;
			}

			if (ToInt(Request.QueryString["cTimeZoneOffset"], out intValue))
			{
				filter.TimeZoneOffset = intValue;
			}

			if (Request.QueryString["cGrippedSources"].IsNotValid())
			{
				return filter;
			}

			var info = default(RealTimeSystemEventsInfo);
			using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				info = bl.RealTimeGetSystemEventsInfo(AccountId, NodeId, CredId);
			}

			//formats
			//["0-226"]
			//["0","2","4-8","40-226"]
			var grippedIndexes = JsHelper.Deserialize<string[]>(Request.QueryString["cGrippedSources"]);
			if (grippedIndexes.Length == 0)
			{
				return filter;
			}

			var log = info.Logs
				.First(item => string.Equals(item.LogName, filter.LogName, StringComparison.InvariantCultureIgnoreCase));
			log.SourceNames = log.SourceNames
				.OrderBy(item => item, StringComparer.InvariantCultureIgnoreCase)
				.ToArray();

			var sources = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
			foreach (var grippedIndex in grippedIndexes)
			{
				if (grippedIndex.Contains('-'))
				{
					var pairs = grippedIndex
						.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
					if (pairs.Length == 2)
					{
						var begin = 0;
						if (ToInt(pairs.First(), out begin))
						{
							var end = 0;
							if (ToInt(pairs.Last(), out end))
							{
								while (begin <= end)
								{
									var source = log.SourceNames
										.ElementAtOrDefault(begin++);
									if (source.IsValid())
									{
										sources.Add(source);
									}
								}
							}
						}
					}
				}
				else
				{
					if (ToInt(grippedIndex, out intValue))
					{
						var source = log.SourceNames
							.ElementAtOrDefault(intValue);
						if (source.IsValid())
						{
							sources.Add(source);
						}
					}
				}
			}

			if (log.SourceNames.Length == sources.Count)
			{
				return filter;
			}

			filter.SourceNames = sources.ToArray();
			return filter;
		}
	}

	#endregion

	#region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			ValidateRights();

			var info = default(RealTimeSystemEventsInfo);

			try
			{
				var nodeId = NodeId;
				var credId = CredId;
				var filter = Filter;
				using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
				{
					info = bl.RealTimeGetEventsInfo(AccountId, nodeId, credId, filter);
				}
				if (info.Events.Count > 0)
				{
					info.Events.ForEach(item =>
					{
						item.TimeGenerated = item.TimeGeneratedUtc
							.AddMinutes(-filter.TimeZoneOffset)
							.ToString(CultureInfo.CurrentCulture);
					});
				}
			}
			catch (Exception ex)
			{
				info = new RealTimeSystemEventsInfo
				{
					JobPollingStatus = RealTimePollingStatus.Error,
					ErrorMessage = ex.Message
				};
			}
			Response.ContentType = "application/javascript";
			Response.Write(string.Format("{0}({1})", Request["callback"], JsHelper.Serialize(info)));
			Response.End();
		}
	}

	#endregion

	#region Helper Members

	private bool ToInt(string value, out int result)
	{
		result = 0;
		if (value.IsNotValid())
		{
			return false;
		}
		return int.TryParse(value, NumberStyles.Integer, CultureInfo.InvariantCulture, out result);
	}

	private bool ToUint(string value, out uint result)
	{
		result = 0;
		if (value.IsNotValid())
		{
			return false;
		}
		return uint.TryParse(value, NumberStyles.Integer, CultureInfo.InvariantCulture, out result);
	}

	protected void ValidateRights()
	{
		if (!ApmRoleAccessor.AllowRealTimeEventExplorer)
		{
			Server.Transfer(InvariantString.Format("/Orion/Error.aspx?Message={0}", HttpUtility.UrlEncode("You do not have sufficient rights to view the Log and Event Viewer.")));
		}

		if (Node == null)
		{
			return;
		}

		var result = SwisProxy.ExecuteQuery(InvariantString.Format(@"SELECT n.NodeName FROM Orion.Nodes AS n WHERE n.NodeID = {0}", NodeId));
		if (result.Rows.Count == 0)
		{
			Server.Transfer(InvariantString.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", NodeId));
		}
	}
	#endregion
}
