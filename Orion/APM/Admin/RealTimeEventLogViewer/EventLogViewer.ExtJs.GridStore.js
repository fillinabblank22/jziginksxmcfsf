﻿Ext.define("RtWem.Events", {
	extend: "Ext.data.Model",
	fields: [
		{ name: "RecordNumber" },
		{ name: "Logfile" }, { name: "SourceName" }, { name: "EventType" }, { name: "EventCode" }, { name: "TimeGenerated" },
		{ name: "ComputerName" }, { name: "User" }, { name: "Message" }
	],
	idProperty: "RecordNumber"
});

Ext.define("RtWem.GridStore", {
    extend: "Ext.data.Store",

    model: "RtWem.Events",

	autoLoad: false,
    buffered: true, 
    purgePageCount: 0,
    pageSize: RTWEM_PAGE_SIZE, leadingBufferZone: RTWEM_PAGE_SIZE, trailingBufferZone: RTWEM_PAGE_SIZE,

    wemIsFirstLoad: true,

    wemNewEventsAvailable: false,
    wemLastFirstRecord: 0,
    wemTotalLast: null, wemTotalCurrent: null,

    wemCacheMaxSize: 1000,
    wemTimeoutToUpdateCache: 200,
    wemRecsToCache: null, wemRecsToCacheTask: null,

	onProxyPrefetch: function (operation) {
		var that = this;
		var success = operation.wasSuccessful();
		var resultSet = operation.getResultSet();
		var records = operation.getRecords();

		if (success) {
			that.wemRecsToCache.push.apply(that.wemRecsToCache, records);
			that.wemRecsToCacheTask.delay(that.wemTimeoutToUpdateCache);
		}

		if (operation.pageMapGeneration === that.data.pageMapGeneration) {
			if (resultSet) {
				that.totalCount = resultSet.total;
				that.fireEvent("totalcountchange", that.totalCount);
			}

			that.wemTotalLast = that.wemTotalCurrent;
			that.wemTotalCurrent = that.totalCount;
			if (that.wemTotalLast == null) {
				that.wemTotalLast = that.totalCount;
			}

			if (operation.page !== undefined) {
				delete that.pageRequests[operation.page];
			}

			that.loading = false;
			that.fireEvent("prefetch", that, records, success, operation);

			Ext.callback(operation.callback, operation.scope || that, [records, operation, success]);
		}
	},

	/*helper members*/
	wemGetScroller: function () {
		return this.wemMan.getGrid().getScroller();
	},

	wemHasNewEvents: function (resetValue) {
		return this.wemNewEventsAvailable;
	},

	wemGetResponse: function () {
		var data = this.getProxy().getReader().rawData;
		if (Ext.isObject(data)) {
			return data;
		}
		return { JobPollingStatus: RTWEM_JOB_STATUS_INIT };
	},

	/*reload grid data*/
    wemReload: function () {
    	var that = this;
    	if (that.wemIsFirstLoad) {
    		that.wemIsFirstLoad = false;
    		that.load({ page: 1, start: 0 });
    	} else {
    		that.prefetch({ start: 0 });
    	}
    },
	/*clear grid state*/
    wemResetCache: function () {
    	var that = this;

		that.removeAll(false);

		delete that.totalCount;
		delete that.lastRequestStart;

		var data = that.wemGetResponse();
		data.JobPollingStatus = RTWEM_JOB_STATUS_INIT;

		var scroller = that.wemGetScroller();
		scroller.wemReqStart = 0;
		scroller.wemReqEnd = RTWEM_PAGE_SIZE;

		that.wemNewEventsAvailable = false;
		that.wemLastFirstRecord = 0;
		that.wemTotalLast = that.wemTotalCurrent = null;

		that.load({ page: 1, start: 0 });
    },

	/*merge cached and received pages/records*/
    wemGetCachedRecords: function () {
    	var that = this;
    	if (that.data.map) {
    		var records = [], pages = that.data.map;
    		for (page in pages) {
    			if (pages.hasOwnProperty(page) && Ext.isArray(pages[page].value)) {
    				records.push.apply(records, pages[page].value);
    			}
    		}
    		return records;
    	}
    	return [];
    },
    wemGetFormatedPages: function (records) {
    	var that = this;

    	var data = that.wemGetResponse(), pages = [];
    	if (data.PagesState) {
    		for (var i = 0; i < data.PagesState.length; i++) {
    			pages.push({ index: data.PagesState[i].first(), topRecNumber: data.PagesState[i].last(), records: [] });
    		}

    		for (var i = pages.length - 1; i >= 0; i--) {
    			for (var j = 0; j < records.length && pages[i].records.length < RTWEM_PAGE_SIZE; j++) {
    				var page = pages[i], record = records[j];
    				if (page.topRecNumber >= record.raw.RecordNumber) {

    					record.join(that);
    					record.index = ((page.index - 1) * RTWEM_PAGE_SIZE + page.records.length);

    					page.records.push(record);

    					records.splice(j--, 1);
    				}
    			}
    		}
    	}
    	return pages;
    },
    wemCachePages: function () {
    	var that = this;

    	that.wemRecsToCacheTask.cancel();
    	if (that.wemRecsToCache.length > 0) {

    		var log = "";
    		var allRecords = null, newRecords = that.wemRecsToCache;
    		if (that.data.getCount() >= that.wemCacheMaxSize) {
    			var scroller = that.wemGetScroller();
    			if (that.rangeCached(scroller.wemReqStart, scroller.wemReqEnd)) {
    				allRecords = that.getRange(scroller.wemReqStart, scroller.wemReqEnd);
    			}
    			log = "RTWEM - Cache buffer overflowed and will be cleared.\n";
    			that.data.clear(true);
    		}
    		if (!allRecords) {
    			allRecords = that.wemGetCachedRecords();
    		}
    		for (var rec = newRecords.pop(); rec; rec = newRecords.pop()) {
    			var newRecNum = rec.raw.RecordNumber, isNew = true;
    			for (var i = 0; isNew && i < allRecords.length; i++) {
    				isNew = (newRecNum != allRecords[i].raw.RecordNumber);
    			}
    			if (isNew) {
    				allRecords.push(rec);
    			}
    		}
    		allRecords.sort(function (rec1, rec2) {
    			return rec2.raw.RecordNumber - rec1.raw.RecordNumber;
    		});
    		that.wemLastFirstRecord = Math.max(that.wemLastFirstRecord, allRecords.first().raw.RecordNumber);

    		if (that.data.getCount() > 0) {
    			that.data.clear(true);
    		}

    		if (RTWEM_IsLog) {
    			log += "RTWEM - Clear all cached pages/records.\n";
    		}
    		var pages = that.wemGetFormatedPages(allRecords);
    		for (var page = pages.shift(); page; page = pages.shift()) {
    			if (page.records.length > 0 && (page.records.length == RTWEM_PAGE_SIZE || pages.length == 0)) {
    				that.data.addPage(page.index, page.records);

    				if (RTWEM_IsLog) {
    					var first = page.records.first(), last = page.records.last();
    					log += SF("    Page #{0} added. From {1}({2}); To {3}({4}); Count: {5}\n", page.index, last.raw.RecordNumber, first.index, first.raw.RecordNumber, last.index, page.records.length);
    				}
    			}
    		}
    		if (log.length > 0) {
    			RTWEM_Log(log);
    		}
    	}
    	SW.APM.EventManager.fire("rtwem-store-data-cached", { store: that, response: that.wemGetResponse() });
    },

	/*event handlers*/
    wemOnBeforeLoad: function (manager, operation) {
		var that = this;

		var scroller = that.wemGetScroller();
		if (operation.start == 0) {
			operation.start = scroller.wemReqStart;
		} else {
			if (that.rangeCached(operation.start, operation.start + RTWEM_PAGE_SIZE)) {
				return false;
			}
		}

		var node = manager.getNode();
		var cred = manager.getCred();
		var criteria = manager.getFilter().getCriteria();

		var params = that.getProxy().extraParams;

		params.cNodeId = node.id;
		params.cCredId = cred.id;

		params.cLog = criteria.LogName;
		params.cLevels = criteria.Levels;
		params.cGrippedSources = Ext.JSON.encode(criteria.GrippedSources);

		params.cStart = operation.start;
		params.cLimit = criteria.Limit;
		params.cLastFirstRecord = that.wemLastFirstRecord;
		params.cLastTotalCount = that.getTotalCount();

		params.cTimeZoneOffset = criteria.TimeZoneOffset;

		SW.APM.EventManager.fire("rtwem-store-beforeload", { response: that.wemGetResponse() });
	},
    wemOnLoad: function (manager) {
		var that = this;

		that.wemNewEventsAvailable = false;

		var data = that.wemGetResponse();
		if (data.JobPollingStatus == RTWEM_JOB_STATUS_ERROR) {
			SW.APM.EventManager.fire("rtwem-error", { message: data.ErrorMessage });
		} else {
			if (data.NewEventsCount > 0) {
				that.wemNewEventsAvailable = true;
			}
			SW.APM.EventManager.fire("rtwem-store-load", { response: data });
		}
	},

	constructor: function (config) {
		var that = this;

		var manager = config.wemMan
		delete config.wemMan;

		var defConfig = {
			proxy: Ext.create("Ext.data.proxy.JsonP", {
				url: "/Orion/APM/Admin/RealTimeEventLogViewer/DataProvider.aspx",
				timeout: 20 * 1000,
				extraParams: {
					nodeId: manager.getNode().id, cred: manager.getCred().id
				},
				reader: {
					type: "json", root: "Events", totalProperty: "TotalEventsCount", idProperty: "RecordNumber"
				},
				listeners: {
					exception: function (proxy, request, operation, opts) {
						var error = operation.getError();
						if (error == "timeout") {
							SW.APM.EventManager.fire("rtwem-error", { status: 408 });
						}
						RTWEM_Log(SF("RTWEM - Error occurred. Error: {0};", error));
					}
				}
			}),
			listeners: {
				beforeprefetch: function (store, operation) { that.wemOnBeforeLoad(manager, operation); },
				prefetch: function () { that.wemOnLoad(manager); }
			}
		};
		config = Ext.applyIf(config, defConfig);

		that.wemMan = manager;
		that.callParent([config]);

		that.wemRecsToCache = [];
		that.wemRecsToCacheTask = new Ext.util.DelayedTask(function () {
			that.wemCachePages();
		});

		SW.APM.EventManager.on("rtwem-filter-criteria-changed", function () {
			that.wemResetCache();
		});
	}
});