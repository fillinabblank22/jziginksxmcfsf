﻿Ext.define("RtWem.NotificationBar", {
	extend: "Ext.toolbar.Toolbar",
	alias: "widget.notificationBar",

	cls: "notificationBar",

	wemEnableNotificationsChecking: true,

	showSuccess: function () {
		this.wemEnableNotificationsChecking = false;

		this.getEl().setOpacity(0.25, false);
		this.addClass("notificationBar-success");
		this.showBar($("#rtwem-notification-bar-link").html());
	},

	showBar: function (msg) {
		this.msgItem.update(msg);

		if (this.isHidden()) {
			this.show();
		}

		this.getEl().fadeIn({ opacity: 1, duration: 1000 });
		this.ownerCt.forceComponentLayout();
	},
	hideBar: function () {
		this.wemEnableNotificationsChecking = false;
		if (!this.isHidden()) {
			this.hide();
			this.ownerCt.forceComponentLayout();
			this.removeCls("notificationBar-success");
			this.removeCls("notificationBar-error");
			this.msgItem.update("");
		}
	},

	afterRender: function () {
		this.callParent(arguments);
		this.hide();
	},

	initComponent: function () {
		var that = this;
		SW.APM.EventManager
			.on("rtwem-notificationBar-move-on-top", function () { that.hideBar(); })
			.on("rtwem-notificationBar-enable-checking", function () { that.wemEnableNotificationsChecking = true; })

			.on("rtwem-filter-criteria-changed", function () { that.hideBar(); })
			.on("rtwem-store-data-cached", function (evt) {
				if (that.wemEnableNotificationsChecking && evt.store.wemHasNewEvents()) {
					that.showSuccess();
				}
			});
		this.items = [
			{ itemId: "msg", xtype: "tbtext", text: "" }, "->",
			{ text: "X", scope: this, handler: this.hideBar }
		];
		this.callParent(arguments);
		this.msgItem = this.child("#msg");
	}
});