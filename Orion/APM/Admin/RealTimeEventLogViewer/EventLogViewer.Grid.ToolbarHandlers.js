﻿Ext.ns("SW.APM.RealTimeEvents");

SW.APM.RealTimeEvents.ToolbarHandlers = function () {
	var EM = SW.APM.EventManager;

	var mMan;
	var mBtnMonitoring, mBtnPolling, mBtnSelectNone;
	var mLabelNumOfEvents, mLabelOver, mLabelLoading

	var startEventsMonitoring = function (items) {
		if (mMan.isDemoServer()) { DemoModeAction("RTWEM_StartEventMonitoring"); return; }

		var node = mMan.getNode(), cred = mMan.getCred();
		var success = function () {
    		window.open("/Orion/APM/Admin/MonitorLibrary/AppFinder/EditMonitorProperties.aspx", "EventLogMonitorWizard");
    	};
		var fail = function (ex) {
			EM.fire("rtwem-error", { message: ex._message });
    	};
    	EventLogViewer.Events.InitWizardSession(node.id, node.ip, node.nodeSubType, cred.id, items, success, fail);
	}

	var changeCredentials = function () {
		if (mMan.isDemoServer()) { DemoModeAction("RTWEM_ChangeCredentials"); return; }

		var node = mMan.getNode(), cred = mMan.getCred();
		var params = {
			addNoneCred: node.isLocalHost, isRemote: node.isRemoteForCredentialDialog, addInheritedWindows: true,
			onSuccess: function (newCred) {
				if (cred.id != newCred.id) {
					mMan.startNewPolling(newCred)
				}
			}
		};
		SW.APM.CredentialsDialog.show(params);
	}
	
	var setStartPollingUi = function () {
		mBtnPolling.setText(RTWEM_PausePolling);
		mBtnPolling.setIcon("/Orion/APM/Images/StatusIcons/ServiceStatus_Paused_16x16.gif");

	    mLabelOver.setText(SF(RTWEM_MonitoringInfo, mMan.getOver(), mMan.getCred().name));
		mLabelLoading.setVisible(true);
	}
	var setStopPollingUi = function () {
		mBtnPolling.setText(RTWEM_StartPolling);
		mBtnPolling.setIcon("/Orion/APM/Images/ProcessMonitor/icon_play.png");

		mLabelOver.setText("paused.");
		mLabelLoading.setVisible(false);
	}

	var setNumberOfEvents = function (value) {
		if (Ext.isNumber(value)) {
			value = Ext.util.Format.number(value, "0,000");
		}
		mLabelNumOfEvents.setText(SF(RTWEM_NumberOfEventsTpl, value));
	}

	/*event handlers*/
	var onError = function () {
		if (mMan.isDemoServer()) { return; }

		mBtnMonitoring.setDisabled(true);
		mBtnPolling.setDisabled(true);
		mBtnSelectNone.setDisabled(true);
	}

	/*public members*/
	this.disableMonitoring = function (disable) {
		mBtnMonitoring.setDisabled(disable);
	};

	/*events*/
	this.onStartEventsMonitoring = function () {
		var selItems = mMan.getGrid().getSelectedItems();
		if (selItems.length == 0) {
			Ext.Msg.show({ title: RTWEM_ErrorTitle, msg: "Please select at least one existing event.", buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR });
		} else {
			var items = [];
			for (var i = 0; i < selItems.length; i++) {
				if (selItems[i]) {
					items.push(selItems[i].data);
				}
			}
			startEventsMonitoring(items);
		}
	};
	this.onStartEventMonitoring = function (record) {
		startEventsMonitoring([record.data]);
	};

	this.onClearSelectionClick = function () {
		mMan.getGrid().clearSelectedItems();
	};

	this.onStopStartPollingClick = function (btn, evt) {
		if (mMan.isDemoServer()) { DemoModeAction("RTWEM_StartStopPolling"); return; }

		var text = btn.getText();
		if (text == RTWEM_StartPolling) {
			if (mMan.getFilter().validateCriteria()) {
				setStartPollingUi();
				EM.fire("rtwem-polling-start-click");
			}
		} else if (text == RTWEM_PausePolling) {
			setStopPollingUi();
			EM.fire("rtwem-polling-stop-click");
		}
	};
	this.onChangeCredentialsClick = function () {
		changeCredentials();
	};

	this.init = function (manager, tBar, bBar) {
		mMan = manager;

		mBtnMonitoring = tBar.items
			.getByKey("rtwemBtnMonitoring");
		mBtnPolling = tBar.items
			.getByKey("rtwemBtnPolling");
		mBtnSelectNone = tBar.items
			.getByKey("rtwemBtnSelectNone");
		
		mLabelLoading = bBar.items
			.getByKey("rtwemLabelLoading");
		mLabelOver = bBar.items
			.getByKey("rtwemLabellOver");
		mLabelNumOfEvents = bBar.items
			.getByKey("rtwemLabelNumOfEvents");
		
		SW.APM.EventManager
			.on("rtwem-store-load", function (evt) {
				var data = evt.response, value = RTWEM_NA;
				if (data.JobPollingStatus != RTWEM_JOB_STATUS_INIT) {
					value = data.TotalEventsCount;
				}
				setNumberOfEvents(value);
			})
			.on("rtwem-start-monitoring", function (evt) { startEventsMonitoring([evt.record.data]); })
			.on("rtwem-try-diff-cred", changeCredentials)
			.on("rtwem-set-start-polling-ui", setStartPollingUi)
			.on("rtwem-set-stop-polling-ui", setStopPollingUi)
			.on("rtwem-filter-criteria-changed", function () { setNumberOfEvents(RTWEM_NA); })
			.on("rtwem-error", onError);
	};
};