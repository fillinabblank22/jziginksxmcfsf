﻿Ext.ns("SW.APM.RealTimeEvents");

window.RTWEM = SW.APM.RealTimeEvents;

window.RTWEM_IsLog = Ext.isDefined(Ext.global.console);
window.RTWEM_Log = function (message) {
	if (RTWEM_IsLog) {
		Ext.global.console.warn(message);
	}
}

window.RTWEM_JOB_STATUS_INIT = 1;
window.RTWEM_JOB_STATUS_ERROR = 3;

window.RTWEM_PAGE_SIZE = 70;

window.RTWEM_NA = "@{R=APM.Strings;K=APMLIBCODE_AK1_2;E=js}";

window.RTWEM_ErrorTitle = "@{R=APM.Strings;K=APMWEBJS_ErrorViewerError;E=js}";
window.RTWEM_NumberOfEventsTpl = "@{R=APM.Strings;K=APMWEBJS_NumberOfEvents;E=js}";

window.RTWEM_LoadMsgInit = "";
window.RTWEM_LoadMsgDefault = "@{R=APM.Strings;K=APMWEBJS_AK1_16;E=js}";
window.RTWEM_LoadMsgRestart = "@{R=APM.Strings;K=APMWEBJS_PollingRestarting;E=js}";
window.RTWEM_LoadMsgStartNew = "@{R=APM.Strings;K=APMWEBJS_NewPollingStarting;E=js}";

window.RTWEM_Cancel = "@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}";
window.RTWEM_TryDifCred = "@{R=APM.Strings;K=APMWEBJS_AK1_42;E=js}";
window.RTWEM_MonitoringInfo = "@{R=APM.Strings;K=APMWEBJS_MonitoringInfo;E=js}";

window.RTWEM_FilterTitle = "@{R=APM.Strings;K=APMWEBJS_EventsFilter;E=js}";
window.RTWEM_FilterSubTitleTpl = "@{R=APM.Strings;K=APMWEBJS_FilterFor;E=js}";
window.RTWEM_FilterLogLabel = "@{R=APM.Strings;K=APMWEBJS_SelectLogType;E=js}";
window.RTWEM_FilterSourceLabel = "@{R=APM.Strings;K=APMWEBJS_EventSources;E=js}"
window.RTWEM_FilterLevelLabel = "@{R=APM.Strings;K=APMWEBJS_EventLevels;E=js}"

window.RTWEM_StartMonitoring = "@{R=APM.Strings;K=APMWEBJS_AK1_34;E=js}";
window.RTWEM_SelectAll = "@{R=APM.Strings;K=APMWEBJS_SelectAll;E=js}";
window.RTWEM_SelectNone = "@{R=APM.Strings;K=APMWEBJS_SelectNone;E=js}";
window.RTWEM_StartPolling = "@{R=APM.Strings;K=APMWEBJS_AK1_56;E=js}";
window.RTWEM_PausePolling = "@{R=APM.Strings;K=APMWEBJS_AK1_25;E=js}";
window.RTWEM_UseDifCred = "@{R=APM.Strings;K=APMWEBJS_AK1_26;E=js}";
window.RTWEM_RestartPolling = "@{R=APM.Strings;K=APMWEBJS_RestartPolling;E=js}";
window.RTWEM_RestartPollingTip = "@{R=APM.Strings;K=APMWEBJS_RestartPollingTip;E=js}";
window.RTWEM_Relogin = "@{R=APM.Strings;K=APMWEBJS_Relogin;E=js}";

window.RTWEM_ErrorTimeout = "@{R=APM.Strings;K=APMWEBJS_ErrorTimeout;E=js}";

window.RTWEM_EvtDetailsDlgTitle = "@{R=APM.Strings;K=APMWEBJS_PV0_11;E=js}";
window.RTWEM_EvtLevel = "@{R=APM.Strings;K=APMWEBJS_PV0_1;E=js}";
window.RTWEM_EvtEventId = "@{R=APM.Strings;K=APMWEBJS_PV0_2;E=js}";
window.RTWEM_EvtDate = "@{R=APM.Strings;K=APMWEBJS_PV0_3;E=js}";
window.RTWEM_EvtLog = "@{R=APM.Strings;K=APMWEBJS_PV0_5;E=js}";
window.RTWEM_EvtSource = "@{R=APM.Strings;K=APMWEBJS_PV0_6;E=js}";
window.RTWEM_EvtComputer = "@{R=APM.Strings;K=APMWEBJS_PV0_7;E=js}";
window.RTWEM_EvtUser = "@{R=APM.Strings;K=APMWEBJS_PV0_8;E=js}";