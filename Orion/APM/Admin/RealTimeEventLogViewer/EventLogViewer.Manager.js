﻿Ext.ns("SW.APM.RealTimeEvents");

SW.APM.RealTimeEvents.Manager = function () {
	var mThis = this;
	var mIsDemoServer, mNode, mCred, mLogs;

	var mView, mFilter, mGrid, mPolling, mErrorDlg;
	var mLoadCtr, mRefreshTask;

	/*helper members*/
	var getPageUrl = function () {
		var state = mFilter.getState();
		return SF("/Orion/APM/Admin/RealTimeEventLogViewer/Default.aspx?NodeId={0}&CredId={1}&Log={2}&Levels={3}&Sources={4}&SourcesVisible={5}",
			mNode.id, mCred.id, state.log, state.levels, Ext.encode(state.sources), state.sourcesVisible
		);
	}

	/*store event handlers*/
	function onStoreBeforeLoad(evt) {
		var data = evt.response, message = RTWEM_LoadMsgDefault;
		if (data.JobPollingStatus == RTWEM_JOB_STATUS_INIT) {
			message = RTWEM_LoadMsgInit;
		}
		mThis.showLoadMask(message);
	}
	function onStoreDataCached(evt) {
		var data = evt.response;
		if (data.JobPollingStatus != RTWEM_JOB_STATUS_INIT) {
			setTimeout(function () { mThis.hideLoadMask(); }, 500);
		}
	}

	/*error handler*/
	function onError(error) {
		if (!mThis.isDemoServer()) { mThis.stopPolling(); }

		error = error || {};

		var type = (error.dlgType || "default"), status = (error.status || 500), message = (error.message || "Internal Server Error.");
		if (status == 401 || status == 408) {
			type = "timeout";
			message = RTWEM_ErrorTimeout;
		} else {
			message = message.replace(/[\n]/g, "<br/>");
		}
		if (mThis.isDemoServer()) {
			Ext.Msg.show({ title: RTWEM_ErrorTitle, msg: message, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR });
			return;
		}
		mErrorDlg.wemShow({ dlgType: type, message: message });
	}

	/*public members*/
	this.isDemoServer = function () { return mIsDemoServer; };

	this.getNode = function () { return mNode; };
	this.getCred = function () { return mCred; };
    this.getOver = function() { return mNode.nodeSubType == 'Agent' ? 'Agent' : 'WMI'; };
	this.getLogs = function () { return mLogs; };

	this.getGrid = function () { return mGrid; };
	this.getFilter = function () { return mFilter; };

	this.getPageUrl = function () { return getPageUrl(); };

	this.startPolling = function () {
		if (mThis.isDemoServer()) { DemoModeAction("RTWEM_StartStopPolling"); return; }

		mPolling.start();
		SW.APM.EventManager.fire("rtwem-set-start-polling-ui");
	};
	this.stopPolling = function () {
		if (mThis.isDemoServer()) { DemoModeAction("RTWEM_StartStopPolling"); return; }

		mPolling.stop(); mThis.hideLoadMask();
		SW.APM.EventManager.fire("rtwem-set-stop-polling-ui");
	};
	this.restartPolling = function () {
		if (mThis.isDemoServer()) { DemoModeAction("RTWEM_RestartPolling"); return; }

		mThis.stopPolling();

		mThis.showLoadMask(RTWEM_LoadMsgRestart)

		EventLogViewer.Events.RestartPolling(mNode.id, mCred.id,
			function (args) { window.location = getPageUrl(); },
			function (args) { onError({ message: args._message }); }
		);
	};
	this.startNewPolling = function (cred) {
		if (mThis.isDemoServer()) { DemoModeAction("RTWEM_StartNewPolling"); return; }

		mThis.stopPolling();

		mCred.id = cred.id;
		mThis.showLoadMask(RTWEM_LoadMsgStartNew)

		window.location = getPageUrl();
	};

	this.showLoadMask = function (msg) {
		if (Ext.isString(msg)) {
			mLoadCtr.msg = msg;
		}
		mLoadCtr.show();
	};
	this.hideLoadMask = function () { mLoadCtr.hide(); };

	this.buildUi = function (params) {
		mIsDemoServer = params.isDemoServer;

		mNode = params.node;
		mCred = params.cred; 
		mLogs = params.logs;

		RTWEM_LoadMsgInit = SF($("#rtwem-start-text").html(), mNode.id, mNode.status, mNode.name);

		mErrorDlg = Ext.create("RtWem.ErrorDlg", { wemMan: mThis });

		mFilter = new RTWEM.FilterPanel(mThis);
		mFilter.create(params.filterLog, params.filterLevels, params.filterSources, params.filterSourcesVisible);

		mGrid = new RTWEM.Grid(mThis);
		mGrid.create();

		mPolling = new RTWEM.EventPolling(mThis);

		/*window layout*/
		mView = Ext.create("Ext.Viewport", {
			layout: { type: "border", padding: 5 },
			items: [mFilter.getPanel(), mGrid.getPanel()]
		});
		Ext.EventManager.onWindowResize(function () {
			mView.doLayout();
			mLoadCtr.doComponentLayout();
		});

		mLoadCtr = new Ext.LoadMask(mGrid.getPanel().getEl(), {
			msg: RTWEM_LoadMsgInit
		});

		SW.APM.EventManager
			.on("rtwem-store-beforeload", onStoreBeforeLoad)
			.on("rtwem-store-data-cached", onStoreDataCached)
			.on("rtwem-error", onError);

		if (params.error) {
			SW.APM.EventManager.fire("rtwem-error", params.error);
		} else {
			mFilter.populateUi(function () { mPolling.start(); });
		}

		mRefreshTask = new Ext.util.DelayedTask(function () {
			window.location = getPageUrl();
		});
		mRefreshTask.delay(60 * 60 * 1000);
	};
}