﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_APM_Admin_EventLogViewer_Default"   MasterPageFile="~/Orion/OrionMinReqs.master"%>

<asp:Content ID="c1" runat="server" ContentPlaceHolderID="HeadContent">
	<orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/4.2.1/resources/css/ext-all-gray-debug.css" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/4.2.1/ext-all-debug.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/4.2.1/ext-fix.js" />
    
	<orion:Include runat="server" Module="APM" File="CredentialsDialog.js" />
    
	<orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.Constants.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.ExtJs.Dialogs.js" />
	<orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.ExtJs.GridStore.js" />
	<orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.ExtJs.NotificationBar.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.Grid.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.Grid.Renderers.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.Grid.ToolbarHandlers.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.Grid.EventPolling.js" />
	<orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.FilterPanel.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealTimeEventLogViewer/EventLogViewer.Manager.js" />

	<style type="text/css">
		.x-mask { background: none !important; }
		.x-splitter-vertical { cursor: default; }
		
		.x-ie9m .x-panel > div.x-panel-body > div#rtwemFilterPanel-innerCt { display: none !important; }
		.x-ie9m #rtwemFilterPanel .x-panel-header-default-framed-vertical .x-panel-header-text-container { transform: none !important; }
		
		#rtwemFilterPanel .source-cb div {
			width: 260px; word-wrap: normal; white-space: normal;  
		}
		#rtwemFilterPanel .font-bold-13 {
			font-weight: bold; font-size: 13px;
		}
		#rtwemGridPanel .notificationBar {
            margin: 3px !important;
        }
        #rtwemGridPanel .notificationBar-success, .x-nlg .notificationBar-success {
            border: 1px solid #aee0ae !important; background-color: #d1eed1 !important; background-image: none !important;
        }
        #rtwemGridPanel .notificationBar-error, #rtwemGridPanel .x-nlg .notificationBar-error {
            border:  1px solid #F79897 !important; background-color: #FDDFDE !important; background-image: none !important;
        }
	</style>
    
	<script id="rtwem-start-text" type="text/x-sw-template">
		<div style="width: 400px;">
			<%= Resources.APMWebContent.RealTimeEventLogViewer_StartingEventLogViewerOn%> <img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={0}&status={1}&size=Small"/> {2} <%= Resources.APMWebContent.RealTimeEventLogViewer_Over%> <%=this.GetPollingTechnology()%> 
			<br/><br/>
			<a href="#" id="CancelPolling" onclick="rtWem.stopPolling();" class="sw-btn" style="float:right">
				<span class="sw-btn-c">
					<span class="sw-btn-t"><%= Resources.APMWebContent.RealTimeEventLogViewer_CANCEL%></span>
				</span>
			</a>
			<br/><br/>
		</div>
	</script>
	<script id="rtwem-load-text" type="text/x-sw-template">
		<%= Resources.APMWebContent.RealTimeEventLogViewer_LoadingEventsPleaseWait%>
	</script>
	<script id="rtwem-event-details" type="text/x-sw-template">
		<table width="100%">
			<tr style="background-color:#fafafa;">
				<td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td>
			</tr>
			<tr>
				<td>{7}</td> <td>{8}</td> <td>{9}</td> <td>{10}</td> <td>{11}</td> <td>{12}</td> <td>{13}</td>
			</tr>
		</table>
	</script>
	<script id="rtwem-btn-apply-filter" type="text/x-sw-template">
		<a class="sw-btn-primary sw-btn" href="#" apm_enterhandler="1">
			<span class="sw-btn-c">
				<span class="sw-btn-t"><%= Resources.APMWebContent.RealTimeEventLogViewer_APPLYFILTER%></span>
			</span>
		</a>
	</script>
	<script id="rtwem-btn-reset-filter" type="text/x-sw-template">
		<a class="sw-btn-secondary sw-btn" href="#">
			<span class="sw-btn-c">
				<span class="sw-btn-t"><%= Resources.APMWebContent.RealTimeEventLogViewer_RESETtoDEFAULT%></span>
			</span>
		</a>
	</script>
	<script id="rtwem-btn-start-monitor" type="text/x-sw-template">
		<a href="#" apm_enterhandler="0" class="sw-btn-primary sw-btn">
			<span class="sw-btn-c">
				<span class="sw-btn-t"><%= Resources.APMWebContent.RealTimeEventLogViewer_STARTMONITORING%></span>
			</span>
		</a>
	</script>
	<script id="rtwem-btn-close" type="text/x-sw-template">
		<a href="#" class="sw-btn-secondary sw-btn">
			<span class="sw-btn-c">
				<span class="sw-btn-t"><%= Resources.APMWebContent.RealTimeEventLogViewer_CLOSE%></span>
			</span>
		</a>
	</script>
	<script id="rtwem-notification-bar-link" type="text/x-sw-template">
		<a href="#" onclick="SW.APM.EventManager.fire('rtwem-notificationBar-move-on-top'); return false;">
			<img style="float:left; margin: 0px 5px;" src="/Orion/APM/Images/Icon.Info.gif"/> <%= Resources.APMWebContent.RealTimeEventLogViewer_NewEventsAvailable%> 
		</a>
	</script>
	
	<script type="text/javascript">
		$().ready(function () {
			Ext.onReady(function () {
				Ext.tip.QuickTipManager.init();

				Array.prototype.first = function () { if (this.length == 0) { return null; } return this[0]; };
				Array.prototype.last = function () { if (this.length == 0) { return null; } return this[this.length - 1]; };

				window.rtWem = new SW.APM.RealTimeEvents.Manager();
				rtWem.buildUi(<%=GetJsonParameters()%>);
			});
		});
	</script>
</asp:Content>

<asp:Content ID="c2" runat="server" ContentPlaceHolderID="BodyContent">
	<form id="mainform" runat="server">
		<asp:ScriptManager ID="welman" runat="server">
			<Services>
				<asp:ServiceReference Path="~/Orion/APM/Services/EventLogViewer.asmx" />
			</Services>
		</asp:ScriptManager>
    </form>
</asp:Content>