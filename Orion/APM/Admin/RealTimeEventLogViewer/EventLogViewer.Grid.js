﻿Ext.ns("SW.APM.RealTimeEvents");

SW.APM.RealTimeEvents.Grid = function (manager) {
	var mThis = this;

	var mMan = manager;
	var mPanel, mStore, mToolbar, mDetailsDlg;
	var mLoadCtr;

	/*grid helper members*/
	var moveScrollOnTop = function () {
		var view = mPanel.getView();

		view.getEl().setScrollTop(0);
		view.refresh();
	};

	/*grid reload members*/
	var reloadGridData = function () {
		mStore.wemReload();
	};

	/*store event handlers*/
	function onDataCached(evt) {
		onSelectionChange();
		
		var scroller = mThis.getScroller();
		if (RTWEM_IsLog) {
			RTWEM_Log(SF("RTWEM - onDataCached -> scroller.onRangeFetched: from {0} to {1};", scroller.wemReqStart, scroller.wemReqEnd));
		}
		if (mStore.rangeCached(scroller.wemReqStart, scroller.wemReqEnd)) {
			scroller.onRangeFetched(null, scroller.wemReqStart, scroller.wemReqEnd);
		}
		if (scroller.wemReqStart < RTWEM_PAGE_SIZE || mStore.wemHasNewEvents()) {
			mPanel.getView().refresh();
		}
	}

	/*grid event handlers*/
	function onCellClick(view, cellEl, collIndex, rec, rowEl, rowIndex, evt) {
		if (collIndex == 0) { return; }
		if (collIndex == 6) {
			SW.APM.EventManager.fire("rtwem-start-monitoring", { record: rec });
		} else {
			mDetailsDlg.wemShow(rec);
		}
	}
	function onSelectionChange() {
		mToolbar.disableMonitoring(mThis.getSelectedItems().length == 0);
	}

	/*public members*/
	this.getPanel = function () { return mPanel; };
	this.getScroller = function () { return mPanel.verticalScroller; };

	this.getSelectedItems = function () { return mPanel.getSelectionModel().getSelection(); };
	this.clearSelectedItems = function () { mPanel.getSelectionModel().deselectAll(false); }

	this.create = function () {
		mDetailsDlg = Ext.create("RtWem.EvtDetailsDlg");

		mToolbar = new RTWEM.ToolbarHandlers();
		
		mStore = Ext.create("RtWem.GridStore", {
			wemMan: mMan
		});

		mPanel = Ext.create("Ext.grid.Panel", {
			id: "rtwemGridPanel",
			region: "center", border: false, frame: true, margins: "0px", padding: "0px", 
			store: mStore, selType: "checkboxmodel",
			verticalScrollerType: "paginggridscroller", invalidateScrollerOnRefresh: false,
			selModel: { mode: "SIMPLE", checkOnly: true, pruneRemoved: false },
			viewConfig: {
				trackOver: false, loadMask: false, style: { overflowX: "hidden" }
			},
			columns: {
				defaults: { sortable: false, menuDisabled: true },
				items: [
					{ header: "REC-ID", dataIndex: "RecordNumber", width: 100, align: "center", fixed: true, hidden: true, renderer: RTWEM.Renderers.renderRecId },
		            { header: "@{R=APM.Strings;K=APMWEBJS_Level;E=js}", dataIndex: "EventType", width: 50, align: "center", fixed: true, renderer: RTWEM.Renderers.renderEventLevel },
		            { header: "@{R=APM.Strings;K=APMWEBJS_EventId;E=js}", dataIndex: "EventCode", width: 70, align: "center", fixed: true, renderer: RTWEM.Renderers.renderEventID },
		            { header: "@{R=APM.Strings;K=APMWEBJS_PV0_6;E=js}", dataIndex: "SourceName", width: 200, renderer: RTWEM.Renderers.renderSourceName },
		            { header: "@{R=APM.Strings;K=APMWEBJS_DateAndTime;E=js}", dataIndex: "TimeGenerated", width: 150, renderer: RTWEM.Renderers.renderDateAndTime },
		            { header: "@{R=APM.Strings;K=APMWEBJS_DO1_04;E=js}", dataIndex: "Message", minWidth: 300, flex: 1, renderer: RTWEM.Renderers.renderMessage },
				    { header: "@{R=APM.Strings;K=APMWEBJS_AK1_34;E=js}", minWidth: 210, width: 210, renderer: RTWEM.Renderers.renderAssignedApplication }
				]
			},
			dockedItems: [
		        {
		        	id: "topToolbar", xtype: "toolbar", dock: "top",
		        	items: [
                        { id: "rtwemBtnMonitoring", text: RTWEM_StartMonitoring, disabled: true, icon: "/Orion/APM/Images/icon_add.gif", handler: mToolbar.onStartEventsMonitoring }, "-",
		                { id: "rtwemBtnPolling", text: RTWEM_PausePolling, icon: "/Orion/APM/Images/StatusIcons/ServiceStatus_Paused_16x16.gif", handler: mToolbar.onStopStartPollingClick },
		        	    { id: "useDifferentCredentialsSeparator", xtype: "tbseparator", hidden: mMan.getNode().nodeSubType == 'Agent' },
		                { text: RTWEM_UseDifCred, icon: "/Orion/images/icon_assign_cred_16x16.gif", handler: mToolbar.onChangeCredentialsClick, hidden: mMan.getNode().nodeSubType == 'Agent' }, "-",
		                { text: RTWEM_RestartPolling, icon: "/Orion/APM/Images/icon_refresh.gif", tooltip: { text: RTWEM_RestartPollingTip, width: 200 }, handler: function () { mMan.restartPolling(); } }, "-",
                        { id: "rtwemBtnSelectNone", text: RTWEM_SelectNone, icon: "/Orion/images/Icon.SelectNone.gif", handler: mToolbar.onClearSelectionClick }, "->",
		                { xtype: "label", html: "<img src='/Orion/APM/Images/logo_SW_small.png'/>" }
		        	]
		        },
		        {
		        	id: "notificationBar", xtype: "notificationBar", dock: "top"
		        },
                {
                	id: "bottomToolbar", xtype: "toolbar", dock: "bottom",
                	items: [
		    		    { xtype: "tbtext", id: "rtwemLabelNumOfEvents", text: "" }, "->",
		    			{ xtype: "image", id: "rtwemLabelLoading", src: "/Orion/APM/Images/loading_gen_16x16.gif", width: 16, height: 16 },
		    			{ xtype: "tbtext", text: "Polling on" },
		    			{ xtype: "image", src: SF("/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={0}&status={1}&size=Small", mMan.getNode().id, mMan.getNode().status), width: 16, height: 16 },
		    			{ xtype: "tbtext", text: mMan.getNode().name },
		    			{ xtype: "tbtext", id: "rtwemLabellOver", text: SF(RTWEM_MonitoringInfo, mMan.getOver(), mMan.getCred().name) }
                	]
                }

			],
			listeners: { cellclick: onCellClick, selectionchange: onSelectionChange }
		});

		mToolbar.init(mMan, mPanel.getDockedComponent("topToolbar"), mPanel.getDockedComponent("bottomToolbar"));

		SW.APM.EventManager
			.on("rtwem-store-data-cached", onDataCached)
			.on("rtwem-polling-reload", reloadGridData)
			.on("rtwem-notificationBar-move-on-top", moveScrollOnTop);
	};
};