﻿Ext.namespace("SW.APM.RealTimeEvents");

SW.APM.RealTimeEvents.Renderers = {
	applyToolTip: function (pText, pOpts) {
		if (pText && pOpts) {
			if (Ext.isChrome) {
				pOpts.tdAttr = SF("data-qtip=\"{0}\" data-qwidth=\"{1}\"", pText, Math.max(pText.length, 20) * 7);
			} else {
				pOpts.tdAttr = SF("data-qtip=\"{0}\"", pText);
			}
		}
	},
	getEventLevelImg: function (pVal, pOpts) {
		function format(title, src) {
			RTWEM.Renderers.applyToolTip(title, pOpts);
			return SF("<img src='/Orion/APM/Images/EventLog/{0}.png' height='16' width='16'/>", src || title);
		}
		if (pVal == 1) { return format("Error"); }
		if (pVal == 2) { return format("Warning"); }
		if (pVal == 3) { return format("Information"); }
		if (pVal == 4) { return format("Audit Success", "SuccessAudit"); }
		if (pVal == 5) { return format("Audit Failure", "FailureAudit"); }
		return format("Unknown");
	},

	renderRecId: function (pVal, pOpts) {
		if ((pVal % 100) == 0) {
			return SF("<b style='color:#f00;'>{0}</b>", pVal);
		}
		if ((pVal % 10) == 0) {
			return SF("<b style='color:#00f;'>{0}</b>", pVal);
		}
		return pVal;
	},
	renderEventLevel: function (pVal, pOpts) {
		return RTWEM.Renderers.getEventLevelImg(pVal, pOpts);
	},
	renderEventID: function (pVal) {
		return SF("<a href='#'> {0}</a>", pVal);
	},
	renderSourceName: function (pVal, pOpts) {
		var encoded = Ext.util.Format.htmlEncode(pVal);
		RTWEM.Renderers.applyToolTip(encoded, pOpts);
		return SF("<a href='#'> {0}</a>", encoded);
	},
	renderDateAndTime: function (pVal) {
		return SF("<a href='#'> {0}</a>", pVal);
	},
	renderMessage: function (pVal) {
		return SF("<a href='#'> {0}</a>", Ext.util.Format.htmlEncode(pVal));
	},
	renderAssignedApplication: function (pVal) {
		return "<a href='#'>" + "<img style='margin-bottom:-3px;' height='16' width='16' src='/Orion/APM/Images/icon_add.gif'/>" + " " + "@{R=APM.Strings;K=APMWEBJS_StartMonitoringThisEvent;E=js}" + "</a>";
	}
};