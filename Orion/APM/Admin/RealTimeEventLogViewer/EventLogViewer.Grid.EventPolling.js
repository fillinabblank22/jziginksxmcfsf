﻿Ext.ns("SW.APM.RealTimeEvents");

SW.APM.RealTimeEvents.EventPolling = function (manager) {
	var mMan = manager;

	var mIsStarted = false;

	var POLL_INTERVAL = 25 * 1000, NOTIFICATION_INTERVAL = POLL_INTERVAL * 3;
	var mPollTask, mNotificationBarTask;

	var startPolling = function () {
		if (mIsStarted === true) { return; }

		Ext.TaskManager.start(mPollTask);
		Ext.TaskManager.start(mNotificationBarTask);

	    mIsStarted = true;
	};
	var stopPolling = function () {
		if (mIsStarted === false) { return; }

		Ext.TaskManager.stop(mPollTask);
		Ext.TaskManager.stop(mNotificationBarTask);

		mIsStarted = false;
	};

	/*constructor*/
	function ctor() {
		SW.APM.EventManager
			.on("rtwem-polling-stop-click", stopPolling)
			.on("rtwem-polling-start-click", startPolling);

		mPollTask = {
			run: function () {
				SW.APM.EventManager.fire("rtwem-polling-reload");
			},
			interval: POLL_INTERVAL
		};
		mNotificationBarTask = {
			run: function () {
				SW.APM.EventManager.fire("rtwem-notificationBar-enable-checking");
			},
			interval: NOTIFICATION_INTERVAL
		};
	} ctor();

	/*public members*/
    this.start = function () { startPolling(); };
    this.stop = function () { stopPolling(); };
};