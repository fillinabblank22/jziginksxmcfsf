﻿Ext.define("RtWem.EvtDetailsDlg", {
	extend: "Ext.window.Window",

	id: "rtwemEvtDetailsDlg", title: RTWEM_EvtDetailsDlgTitle,

	modal: true, closeAction: "hide",

	layout: { type: "vbox", align: "stretch" }, buttonAlign: "right", bodyStyle: "background:none;",
	height: 360, width: 800, bodyPadding: 10, border: 0,

	wemRecord: null,

	wemShow: function (rec) {
		var that = this;
		that.wemRecord = rec;

		var encode = function (value) {
			if ((value || "").length == 0) { return "-"; } return Ext.util.Format.htmlEncode(value);
		};

		var headHtml = SF($("#rtwem-event-details").html(),
			RTWEM_EvtLevel, RTWEM_EvtEventId, RTWEM_EvtDate, RTWEM_EvtLog, RTWEM_EvtSource, RTWEM_EvtComputer, RTWEM_EvtUser,
			RTWEM.Renderers.getEventLevelImg(rec.get("EventType")), rec.get("EventCode"), rec.get("TimeGenerated"),
			encode(rec.get("Logfile")), encode(rec.get("SourceName")), encode(rec.get("ComputerName")), encode(rec.get("User"))
		);
		var msgHtml = rec.get("Message");

		Ext.getCmp("rtwemEvtDetHeader").update(headHtml);
		Ext.getCmp("rtwemEvtDetMessage").setValue(msgHtml);

		that.show();
	},

	wemOnStartMonitoring: function () {
		var that = this;
		SW.APM.EventManager.fire("rtwem-start-monitoring", { record: that.wemRecord });
	},

	constructor: function (config) {
		var that = this;

		var defConfig = {
			id: "rtwemEvtDetailsDlg",
			items: [
				{ id: "rtwemEvtDetHeader", xtype: "label", html: "" },
				{ xtype: "tbspacer", height: 10 },
				{ id: "rtwemEvtDetMessage", xtype: "textarea", readOnly: true, value: "", width: "100%", height: "100%", flex: 1 }
			],
			dockedItems: [{
				xtype: "toolbar", dock: "bottom", ui: "footer",
				defaults: { xtype: "label" },
				items: [
					"->",
					{ id: "rtwemBtnStart", html: $("#rtwem-btn-start-monitor").html() },
					{ id: "rtwemBtnClose", html: $("#rtwem-btn-close").html() }
				]
			}],
			listeners: {
				afterrender: function () {
					$("#rtwemEvtDetailsDlg #rtwemBtnStart")
						.unbind("click")
						.bind("click", function () { that.wemOnStartMonitoring() });
					$("#rtwemEvtDetailsDlg #rtwemBtnClose")
						.unbind("click")
						.bind("click", function () { that.hide(); });
				}
			}
		};
		config = Ext.applyIf(config || {}, defConfig);

		that.callParent([config]);
	}
});

Ext.define("RtWem.ErrorDlg", {
	extend: "Ext.window.MessageBox",

	modal: true, closeAction: "hide",
	width: 600, border: 0,

	wemShow: function (error) {
		var that = this;
		if (error.dlgType == "default") {
			Ext.getCmp("rtwemBtnRelogin").hide();
			Ext.getCmp("rtwemBtnRestart").show();
		    if (window.rtWem.getNode().nodeSubType == 'Agent') {
		        Ext.getCmp("rtwemBtnTryDifCred").hide();
		    } else {
		        Ext.getCmp("rtwemBtnTryDifCred").show();
		    }
		    Ext.getCmp("rtwemBtnCancel").show();

		} else if (error.dlgType == "timeout") {
			Ext.getCmp("rtwemBtnRelogin").show();
			Ext.getCmp("rtwemBtnRestart").show();
			Ext.getCmp("rtwemBtnTryDifCred").hide();
			Ext.getCmp("rtwemBtnCancel").hide();

		} else {
			throw SF("Unknown error dialog type - {0}.", error.dlgType);
		}
		that.show({ title: RTWEM_ErrorTitle, msg: SF("<div>{0}<br\></div>", error.message), icon: Ext.Msg.ERROR });
		that.doLayout();
	},

	wemOnReloginClick: function (manager) {
		window.location = manager.getPageUrl();
	},
	wemOnRestartPollClick: function (manager) {
		var that = this;
		that.hide();
		manager.restartPolling();
	},
	wemOnTryDifCredClick: function () {
		var that = this;
		that.hide();

		SW.APM.EventManager.fire("rtwem-try-diff-cred");
	},

	constructor: function (config) {
		var that = this;

		var manager = config.wemMan
		delete config.wemMan;

		var defConfig = {
			title: RTWEM_ErrorTitle,
			buttons: [
				{ id: "rtwemBtnRelogin", text: RTWEM_Relogin, handler: function () { that.wemOnReloginClick(manager); } },
				{ id: "rtwemBtnRestart", text: RTWEM_RestartPolling, handler: function () { that.wemOnRestartPollClick(manager); } },
				{ id: "rtwemBtnTryDifCred", text: RTWEM_TryDifCred, width: 150, handler: function () { that.wemOnTryDifCredClick(manager); } },
				{ id: "rtwemBtnCancel", text: RTWEM_Cancel, handler: function () { that.hide(); } }
			]
		};
		config = Ext.applyIf(config || {}, defConfig);

		that.callParent([config]);
	}
});