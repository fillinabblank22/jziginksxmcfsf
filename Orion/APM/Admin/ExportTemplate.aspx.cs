using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_ExportTemplate : Page
{
    private List<int> selectedTemplateIds = new List<int>();

    protected void Page_Load(object sender, EventArgs e)
    {
        ApmBaseResource.EnsureApmScripts(true);

        if (!IsPostBack)
        {
            string postData = Server.UrlDecode(Request.Form["postData"]);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            selectedTemplateIds = serializer.Deserialize<List<int>>(postData);

            DownloadIFrame.Attributes["src"] = GenerateExportUrl();

            btnShareNow.NavigateUrl = ThwackUrl;

            HttpContext.Current.Session["templatesReadyToDownload"] = null;

            var script = new StringBuilder();
            script.Append("<script type=\"text/javascript\">");
            script.Append("     var timer = setInterval(APMjs.checkTemplateExporting, 1000);");
            script.Append("</script>");

            ClientScript.RegisterClientScriptBlock(this.GetType(), "TemplateExporting", script.ToString());
        }
    }

    protected static string ThwackUrl
    {
        get
        {
            return "http://thwack.solarwinds.com/community/application-and-server_tht/server-and-application-monitor/content?filterID=contentstatus%5Bpublished%5D~category%5Bapplication-monitor-templates%5D&filterID=contentstatus%5Bpublished%5D~objecttype~objecttype%5Bdocument%5D";
        }
    }

    private string GenerateExportUrl()
    {
        StringBuilder sb = new StringBuilder("/Orion/APM/Admin/DownloadTemplate.ashx?id=");

        foreach (int id in selectedTemplateIds)
        {
            sb.AppendFormat("{0},", id);
        }
        return sb.ToString();
    }
}
