<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true" 
    CodeFile="ExportTemplate.aspx.cs" Inherits="Orion_APM_Admin_ExportTemplate" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_159%>" %>
    
    
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="APM/APM-ORION-FIX.css"/>    
    <orion:Include runat="server" File="OrionMinReqs.js" Section="Top" SpecificOrder="-1" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <div>
        <h1><%= Resources.APMWebContent.APMWEBDATA_VB1_159%></h1>
        <div class="apm_ThwackExportMessage"><%= Resources.APMWebContent.APMWEBDATA_PS0_9 %></div>
        <div class="apm_ThwackExportMessage" style="display:none"><%= Resources.APMWebContent.APMWEBDATA_PS0_10 %></div>
        <div class="apm_ThwackExport">
            <div>
                <div id="apm_ShareTitle"><%= Resources.APMWebContent.APMWEBDATA_PS0_11 %></div>
                <p>
                    <%= Resources.APMWebContent.APMWEBDATA_PS0_12 %>
                </p>
                <p>
                    <%= string.Format(Resources.APMWebContent.APMWEBDATA_PS0_13, string.Format("<a href=\"{0}\" target=\"_blank\">", ThwackUrl), "</a>")%>                
                </p>
                <p>
                    <a href="" onclick="$('#DetailedInstructions').css('display','inline'); $(this).css('display','none'); return false;"><%= Resources.APMWebContent.APMWEBDATA_VB1_165 %></a>
                </p>
                <div id="DetailedInstructions">
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_166 %>
                    <ol>
                        <li><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_167, string.Format("<a href=\"{0}\" target=\"_blank\">", ThwackUrl), "</a>")%></li>
                        <li><%= Resources.APMWebContent.APMWEBDATA_PS0_3 %></li>
                        <li><%= Resources.APMWebContent.APMWEBDATA_VB1_170 %></li>                        
                        <li><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_172, SolarWinds.APM.Common.ApmConstants.ModuleShortName)%></li>                        
                        <li><%= Resources.APMWebContent.APMWEBDATA_VB1_173 %></li>
                        <li><%= Resources.APMWebContent.APMWEBDATA_PS0_4 %></li>
                        <li><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_174, SolarWinds.APM.Common.ApmConstants.ModuleShortName)%></li>
                        <li><%= Resources.APMWebContent.APMWEBDATA_PS0_5 %></li>                                                
                        <li><%= Resources.APMWebContent.APMWEBDATA_PS0_6 %></li>                                                
                    </ol>                
                </div>                    
                <div class="sw-btn-bar">
                    <orion:LocalizableButtonLink runat="server" ID="btnShareNow" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_175 %>" DisplayType="Primary"/>
                    <orion:LocalizableButtonLink runat="server" NavigateUrl="/Orion/APM/Admin/ApplicationTemplates.aspx" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_176 %>" DisplayType="Secondary"/>
                </div>
            </div>
        </div>
        
        <iframe runat="server" id="DownloadIFrame" width="0" height="0" scrolling="no" frameborder="0" src=""></iframe>
    </div>

</asp:Content>

