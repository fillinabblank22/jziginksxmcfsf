<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" AutoEventWireup="true" 
    CodeFile="Default.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_Default" Title="<%$ Resources: APMWebContent, APMWEBDATA_TM0_59 %>" %>

<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<%@ Assembly Name="Infragistics2.WebUI.Shared.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Import Namespace="Resources" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>


<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHComponentMonitorLibrary" ID="HelpButton" runat="server" />
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="cPh" Runat="Server">
	<table width="100%" id="breadcrumb">
		<tr>
			<td>
				<h1><%=Page.Title%></h1>
				<div id="apm_adminDescription"><%= APMWebContent.APMWEBDATA_TM0_60 %> </div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="10">
	    <tr>
	        <td style="width:15px;background-color: <%=TemplateColor %>;">&nbsp;</td>
	        <td><%= APMWebContent.APMWEBDATA_TM0_61 %></td>
	        <td>&nbsp;</td>
	        <td style="width:15px;background-color: <%=ApplicationColor %>;">&nbsp;</td>
	        <td><%= APMWebContent.APMWEBDATA_TM0_62 %></td>
	    </tr>
	</table>
	<div style="padding-top:0px;">
        <ignav:UltraWebTree ID="tree" runat="server" Indentation="20" 
                            CssClass="apm_NoFocusBorder apm_MonitorLibraryTree" 
                            LoadOnDemand="ManualSmartCallbacks"
                            WebTreeTarget="HierarchicalTree" RenderAnchors="true" >
            <Images>
                <CollapseImage Url="/Orion/images/Button.Collapse.GIF" />
                <ExpandImage Url="/Orion/images/Button.Expand.GIF" />
            </Images>
            <NodePaddings Top="2px" Bottom="2px"/>
        </ignav:UltraWebTree>	
	</div>
	    
	<table cellpadding="0" cellspacing="10">
	    <tr>
	        <td style="width:15px;background-color: <%=TemplateColor %>;">&nbsp;</td>
	        <td><%= APMWebContent.APMWEBDATA_TM0_61 %></td>
	        <td>&nbsp;</td>
	        <td style="width:15px;background-color: <%=ApplicationColor %>;">&nbsp;</td>
	        <td><%= APMWebContent.APMWEBDATA_TM0_62 %></td>
	    </tr>
	</table>
</asp:Content>



