<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectCredentials.ascx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_Controls_SelectCredentials" %>
    <orion:Include ID="i1" File="APM/Admin/MonitorLibrary/Controls/Js/SelectCredentials.js" runat="server"/>
    <%--TODO: using validator methods is not the right way how to do validation, 
        we should participate on validation by adding rule to current form validator,
        something like $(this).closest('form').validator.addClassRules(),
        see http://docs.jquery.com/Plugins/Validation/Validator/addClassRules#namerules
        I cannot implement it right now, because I don't have enough time to do it --%>
    <script type="text/javascript">
        // support for JQuery Validation plugin, http://docs.jquery.com/Plugins/Validation
        if ($.validator) {
            $.validator.addMethod("credentialSetExists", function(value, element) {
                var isValid = true;
                $('#<%= CredentialSetList.ClientID %>')
                    .find('option')
                    .not('*[value=-1],*[value=-3],*[value=0]')
                    .each(function(index, option) {
                        isValid = isValid & $(option).text() != value;
                    });
                return this.optional(element) || isValid;
            }, 'Credential with the same name already exists');
            $.validator.addMethod("<%=Password.ClientID %>", function (value, element) {
                if ($('#<%= ConfirmPassword.ClientID %>').is(':visible')) {
                    return this.optional(element) || $('#<%= ConfirmPassword.ClientID %>').val() == value; 
                }
                return true;
            }, 'The Password and Confirmation Password must match');
        }
    </script>
    <%-- "label" class defined on table cell is required by SW.APM.EditApp class to show error messages properly --%>
    <tr>
        <td class="label"><%=Resources.APMWebContent.APMWEBDATA_AK1_85 %></td>
        <td>
            <asp:DropDownList ID="CredentialSetList" runat="server" OnPreRender="CredentialSetList_OnPreRender" OnSelectedIndexChanged="CredentialSetList_SelectedIndexChanged"
                              AutoPostBack="false">
            </asp:DropDownList>
        </td>  
        <td> </td>
    </tr>
    <tr>
        <td class="label"><%=Resources.APMWebContent.APMWEBDATA_AK1_86 %></td>
        <td>
            <asp:TextBox ID="CredentialName" runat="server" Width="98%"></asp:TextBox>

        </td>
        <td>
            <asp:RequiredFieldValidator ID="CredentialNameFieldValidator" runat="server" 
                ControlToValidate="CredentialName" ValidationGroup="SelectCredentialsValidationGroup"
                ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_87 %>">*</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="nameExistsValidator" OnServerValidate="OnNameExists_ServerValidate" ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_88 %>" 
                Display="Dynamic" runat="server" ValidationGroup="SelectCredentialsValidationGroup">*</asp:CustomValidator>
            
        </td>
    </tr>
    <tr>
        <td class="label"><%=Resources.APMWebContent.APMWEBDATA_AK1_89 %></td>
        <td>
            <asp:TextBox ID="UserName" runat="server" Width="98%" autocomplete="off"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="UserNameFieldValidator" runat="server" 
                         ControlToValidate="UserName" ValidationGroup="SelectCredentialsValidationGroup"
                         ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_90 %>">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="label"><%=Resources.APMWebContent.APMWEBDATA_AK1_91 %></td>
        <td>
            <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="98%" autocomplete="off"></asp:TextBox>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="label"><%=Resources.APMWebContent.APMWEBDATA_AK1_92 %></td>
        <td>
            <asp:TextBox ID="ConfirmPassword" runat="server" 
    TextMode="Password" Width="98%"></asp:TextBox>
        </td>
        <td>
           <asp:CustomValidator ID="ComparePasswordValidator" OnServerValidate="OnComparePassword_ServerValidate" ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_93 %>" 
                Display="Dynamic" runat="server" ValidationGroup="SelectCredentialsValidationGroup">*</asp:CustomValidator>
		</td>
    </tr>    
	<tr>
		<td colspan="2" style="width:350px;">
			<asp:Label ID="ctrWarningMsg" ForeColor="Red" runat="server">
				<%=Resources.APMWebContent.APMWEBDATA_AK1_94 %>
			</asp:Label> 
		</td>
        <td></td>
	</tr>	