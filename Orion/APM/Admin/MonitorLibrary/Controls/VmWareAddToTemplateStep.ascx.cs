using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_VmWareAddToTemplateStep : UserControl
{
    private static readonly string DefaultMonitorName = Resources.APMWebContent.APMWEBCODE_VB1_101;
    private static readonly string DefaultMonitorNameRegExpPattern = Resources.APMWebContent.APMWEBCODE_VB1_102;

    public event EventHandler SelectionChanged;

    #region Event handlers
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StepTitle.Text = StepTitleText;
            Description.Text = DescriptionText;

            ExistingTemplateList.DataSource = SwisDAL.GetExistingNonCustomTemplates();
            ExistingTemplateList.DataBind();

			ExistingAppList.DataSource = SwisDAL.GetExistingNonCustomApplications();
            ExistingAppList.DataBind();

			if (AddToTemplateSettings.NewName.IsNotValid())
			{
				AddToTemplateSettings.NewName = NewAppName.Text = NewTemplateName.Text = string.Format(DefaultMonitorName, GetCount());
			}
			else 
			{
				NewAppName.Text = NewTemplateName.Text = AddToTemplateSettings.NewName;
			}
            LoadFromSettings(AddToTemplateSettings);
        }

        // We should select default (the third) radio button control if no other is selected
        rbNewApp.Checked = !(rbExistingApp.Checked || rbNewTemplate.Checked || rbExistingTemplate.Checked);
        
        PanelNewTemplate.Visible = rbNewTemplate.Checked;
        PanelExistingTemplate.Visible = rbExistingTemplate.Checked;
        PanelNewApp.Visible = rbNewApp.Checked;
        PanelExistingApp.Visible = rbExistingApp.Checked;
    }

    protected void OnRadioSelectionChanged(object sender, EventArgs e)
    {
        PutDefaultNames();

        RaiseSelectionChanged();
    }
   
    protected void ValidateCheckBoxSelection(object source, ServerValidateEventArgs args)
    {
        if ((!rbExistingApp.Checked || ExistingAppList.SelectedIndex >= 0) &&
            (!rbExistingTemplate.Checked || ExistingTemplateList.SelectedIndex >= 0)) return;
        args.IsValid = false;
        ExistingTemplateListValidator.ErrorMessage = rbExistingApp.Checked ?
            Resources.APMWebContent.APMWEBCODE_VB1_8 :
            Resources.APMWebContent.APMWEBCODE_VB1_9;
    }
    #endregion

    protected void RaiseSelectionChanged()
    {
        if (SelectionChanged != null)
            SelectionChanged(this, EventArgs.Empty);
    }

    #region Properties

    public int MonitorSelectionListCount { get; set; }

    protected string StepTitleText
    {
        get 
        {
            return MonitorSelectionListCount == 1 ?
                Resources.APMWebContent.APMWEBCODE_VB1_4 :
                Resources.APMWebContent.APMWEBCODE_VB1_5;
        }
    }

    protected string DescriptionText
    {
        get
        {
            string prefix = MonitorSelectionListCount == 1 ?
                Resources.APMWebContent.APMWEBCODE_VB1_103 :
                Resources.APMWebContent.APMWEBCODE_VB1_104;

            return prefix;
        }
    }

    public AddToTemplateSettings AddToTemplateSettings { get; set; }

    public int Counter
    {
        set { ViewState["counter"] = value; }
    }
    #endregion

    public void SaveToSettings(AddToTemplateSettings settings)
    {
        if (rbNewTemplate.Checked)
        {
            settings.Mode = AddToTemplateMode.NewTemplate;
            settings.NewName = NewTemplateName.Text;
        }
        else if (rbExistingTemplate.Checked)
        {
            settings.Mode = AddToTemplateMode.ExistingTemplate;
            GetSelectedValueIds(ExistingTemplateList, settings.ExistingIds, settings.ExistingNames);
            settings.NewName = string.Empty;
        }
        else if (rbNewApp.Checked)
        {
            settings.Mode = AddToTemplateMode.NewApplication;
            settings.NewName = NewAppName.Text;
        }
        else if (rbExistingApp.Checked)
        {
            settings.Mode = AddToTemplateMode.ExistingApplication;
            GetSelectedValueIds(ExistingAppList, settings.ExistingIds, settings.ExistingNames);
            settings.NewName = string.Empty;
        }
    }
    
    #region Helpers

    private int GetCount()
    {
        return (int)(ViewState["counter"] ?? -1);
    }

    private void LoadFromSettings(AddToTemplateSettings settings)
    {
        switch (settings.Mode)
        {
            case AddToTemplateMode.NewTemplate:
                rbNewTemplate.Checked = true;
                if (!Regex.IsMatch(settings.NewName, DefaultMonitorNameRegExpPattern, RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant))
                {
                    NewTemplateName.Text = settings.NewName;
                }
                break;

            case AddToTemplateMode.ExistingTemplate:
                rbExistingTemplate.Checked = true;
                SetSelectedValueIds(ExistingTemplateList, settings.ExistingIds);
                break;

            case AddToTemplateMode.NewApplication:
                rbNewApp.Checked = true;
                if (!Regex.IsMatch(settings.NewName, DefaultMonitorNameRegExpPattern, RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant))
                {
                    NewAppName.Text = settings.NewName;
                }
                break;

            case AddToTemplateMode.ExistingApplication:
                rbExistingApp.Checked = true;
                SetSelectedValueIds(ExistingAppList, settings.ExistingIds);
                break;
            default:
                NewTemplateName.Text = NewAppName.Text = string.Format(DefaultMonitorName, GetCount());
                break;
        }
    }

    private static void GetSelectedValueIds(CheckBoxList list, List<string> selectedIds, List<string> selectedNames)
    {
        selectedIds.Clear();
        selectedNames.Clear();

        foreach (ListItem listItem in list.Items)
        {
            if (listItem.Selected)
            {
                selectedIds.Add(listItem.Value);
                selectedNames.Add(listItem.Text);
            }
        }
    }

    private static void SetSelectedValueIds(CheckBoxList list, List<string> selectedIds)
    {
        foreach (string selectedId in selectedIds)
        {
            ListItem item = list.Items.FindByValue(selectedId);
            if (item != null)
                item.Selected = true;
        }
    }

    private void PutDefaultNames()
    {
        if (string.IsNullOrEmpty(NewAppName.Text))
            NewAppName.Text = string.Format(DefaultMonitorName, GetCount());
        if (string.IsNullOrEmpty(NewTemplateName.Text))
            NewTemplateName.Text = string.Format(DefaultMonitorName, GetCount());
    }
    #endregion
}
