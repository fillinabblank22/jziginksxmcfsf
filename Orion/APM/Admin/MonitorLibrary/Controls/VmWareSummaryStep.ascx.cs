using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_VmWareSummaryStep : System.Web.UI.UserControl
{
    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Initialization();
        }
    }

    protected void NewAppNameTextChanged(object sender, EventArgs e)
    {
        if (TemplateSettings == null)
            TemplateSettings = AppFinderWorkflow.SessionInstance.AddToTemplateSettings;

        TemplateSettings.NewName = NewAppName.Text;
    }
    
    #endregion

    #region Properties

    public List<ComponentTemplate> MonitorsToAdd { get; set; }
    
    public List<string> Targets { get; set; }

    public AddToTemplateSettings TemplateSettings { get; set; }

    #endregion

    private void Initialization()
    {
        if (TemplateSettings == null)
            TemplateSettings = AppFinderWorkflow.SessionInstance.AddToTemplateSettings;
        existingMonitors.Visible = false;
        divAppName.Visible = true;
        if (TemplateSettings.UseExisting)
        {
            divAppName.Visible = false;
            existingMonitors.DataSource = GetSource(TemplateSettings.ExistingNames, "existing");
            existingMonitors.DataBind();
            existingMonitors.Visible = true;
        }
        if (AppFinderWorkflow.SessionInstance.SelectedNodeIds == null)
            SetSelectedNodes();

        NewAppName.Text = TemplateSettings.NewName;
        dlServers.DataSource = GetSource(Targets, "servers");
        dlServers.DataBind();
    }

    #region Helpers
    private void SetSelectedNodes()
    {
        if (AppFinderWorkflow.SessionInstance.CurrentSettings.TargetServer != null)
        {
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                // TODO: we shoould identify nodes by id rather than IP
                List<int> nodeIds = new List<int>(Targets.Count);
                nodeIds.AddRange(Targets.Select(target => bl.GetNodeByIPAddress(target)).Select(node => node.Id));

                AppFinderWorkflow.SessionInstance.SelectedNodeIds = nodeIds;
            }
        }
    }

    private DataSet GetSource(List<string> list, string tblName)
    {

        var ds = new DataSet();
        ds.Tables.Add(tblName);
        ds.Tables[0].Columns.Add("Name");

        foreach (var str in list)
        {
            if (!string.IsNullOrEmpty(str))
            {
                DataRow dr = ds.Tables[0].NewRow();
                dr["Name"] = str;
                ds.Tables[0].Rows.Add(dr);
            }
        }
        return ds;
    }

    protected internal string GetAppTemplateMessage()
    {
        switch(TemplateSettings.Mode)
        {
            case AddToTemplateMode.NewApplication:
                return Resources.APMWebContent.APMWEBCODE_VB1_113;
            case AddToTemplateMode.NewTemplate:
                return Resources.APMWebContent.APMWEBCODE_VB1_114;
            case AddToTemplateMode.ExistingApplication:
                return Resources.APMWebContent.APMWEBCODE_VB1_115;
            case AddToTemplateMode.ExistingTemplate:
                return Resources.APMWebContent.APMWEBCODE_VB1_116;
            default:
                return string.Empty;
        }
    }

    #endregion
}
