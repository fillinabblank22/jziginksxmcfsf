﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectPlatform.ascx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_Controls_SelectPlatform" %>

<asp:DropDownList ID="ddlPlatform" runat="server">
    <asp:ListItem Text="<%$ Resources : APMWebContent, APMWEBDATA_VB1_108 %>" Value="False" Selected="True" />
    <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_109 %>" Value="True" />
</asp:DropDownList>