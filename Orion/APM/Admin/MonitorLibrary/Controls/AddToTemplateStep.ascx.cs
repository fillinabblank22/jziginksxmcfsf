using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_AddToTemplateStep : UserControl
{
    private List<string> monitorSelections;
    private AddToTemplateSettings addToTemplateSettings;

    public event EventHandler SelectionChanged;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StepTitle.Text = StepTitleText;
            Description.Text = DescriptionText;

			ExistingTemplateList.DataSource = SwisDAL.GetExistingNonCustomTemplates();
            ExistingTemplateList.DataBind();

			ExistingAppList.DataSource = SwisDAL.GetExistingNonCustomApplications();
			ExistingAppList.DataBind();

            if (addToTemplateSettings != null)
                LoadFromSettings(addToTemplateSettings);
        }

        // We should select default (the first) radio button control if no other is selected
        rbNewTemplate.Checked = !(rbExistingApp.Checked || rbNewApp.Checked || rbExistingTemplate.Checked);

        PanelNewTemplate.Visible = rbNewTemplate.Checked;
        PanelExistingTemplate.Visible = rbExistingTemplate.Checked;
        PanelNewApp.Visible = rbNewApp.Checked;
        PanelExistingApp.Visible = rbExistingApp.Checked;
    }

    public List<string> MonitorSelectionList
    {
        get { return monitorSelections; }
        set { monitorSelections = value; }
    }

    protected string StepTitleText
    {
        get
        {
            if (MonitorSelectionList.Count == 1)
                return APMWebContent.APMWEBCODE_VB1_4;
            else
                return APMWebContent.APMWEBCODE_VB1_5;
        }
    }

    protected string DescriptionText
    {
        get
        {
            string prefix;
            if (MonitorSelectionList.Count == 1)
                prefix = APMWebContent.APMWEBCODE_VB1_6 + " ";
            else
                prefix = APMWebContent.APMWEBCODE_VB1_7 + " ";

            return prefix + String.Join(", ", MonitorSelectionList.ToArray());
        }
    }

    public AddToTemplateSettings AddToTemplateSettings
    {
        get { return addToTemplateSettings; }
        set { addToTemplateSettings = value; }
    }

    private void LoadFromSettings(AddToTemplateSettings settings)
    {
        switch (settings.Mode)
        {
            case AddToTemplateMode.NewTemplate:
                rbNewTemplate.Checked = true;
                NewTemplateName.Text = settings.NewName;
                break;

            case AddToTemplateMode.ExistingTemplate:
                rbExistingTemplate.Checked = true;
                SetSelectedValueIds(ExistingTemplateList, settings.ExistingIds);
                break;

            case AddToTemplateMode.NewApplication:
                rbNewApp.Checked = true;
                NewAppName.Text = settings.NewName;
                break;

            case AddToTemplateMode.ExistingApplication:
                rbExistingApp.Checked = true;
                SetSelectedValueIds(ExistingAppList, settings.ExistingIds);
                break;
        }
    }

    public void SaveToSettings(AddToTemplateSettings settings)
    {
        if (rbNewTemplate.Checked)
        {
            settings.Mode = AddToTemplateMode.NewTemplate;
            settings.NewName = NewTemplateName.Text;
        }
        else if (rbExistingTemplate.Checked)
        {
            settings.Mode = AddToTemplateMode.ExistingTemplate;
            GetSelectedValueIds(ExistingTemplateList, settings.ExistingIds);
        }
        else if (rbNewApp.Checked)
        {
            settings.Mode = AddToTemplateMode.NewApplication;
            settings.NewName = NewAppName.Text;
        }
        else if (rbExistingApp.Checked)
        {
            settings.Mode = AddToTemplateMode.ExistingApplication;
            GetSelectedValueIds(ExistingAppList, settings.ExistingIds);
        }
    }

    private static void GetSelectedValueIds(CheckBoxList list, List<string> selectedIds)
    {
        selectedIds.Clear();

        foreach (ListItem listItem in list.Items)
        {
            if (listItem.Selected)
                selectedIds.Add(listItem.Value);
        }
    }

    private static void SetSelectedValueIds(CheckBoxList list, List<string> selectedIds)
    {
        foreach (string selectedId in selectedIds)
        {
            ListItem item = list.Items.FindByValue(selectedId);
            if (item != null)
                item.Selected = true;
        }
    }

    protected void OnRadioSelectionChanged(object sender, EventArgs e)
    {
        RaiseSelectionChanged();
    }

    protected void RaiseSelectionChanged()
    {
        if (SelectionChanged != null)
            SelectionChanged(this, EventArgs.Empty);
    }

    protected void ValidateCheckBoxSelection(object source, ServerValidateEventArgs args)
    {
        if ((rbExistingApp.Checked && ExistingAppList.SelectedIndex < 0) ||
            (rbExistingTemplate.Checked && ExistingTemplateList.SelectedIndex < 0))
        {
            args.IsValid = false;
            ExistingTemplateListValidator.ErrorMessage = rbExistingApp.Checked ?
                APMWebContent.APMWEBCODE_VB1_8 :
                APMWebContent.APMWEBCODE_VB1_9;
        }
    }
}
