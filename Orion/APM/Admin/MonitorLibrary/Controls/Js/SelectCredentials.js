﻿Ext.namespace('SW.APM.SelectCredentials');
SW.APM.SelectCredentials = function () {
    var updateControls = function (combo, enableControls, credentialSet) {
        var setEnabled = function (jqElement, shouldEnable) {
            if (shouldEnable == true) {
                jqElement.removeAttr("disabled");
            } else {
                jqElement.attr("disabled", "disabled");
            }
        };

        var jqTable = (combo instanceof jQuery ? combo : $(combo)).closest('table');
        var jqCredName = jqTable.find('input[id$=CredentialName]');
        var jqUserName = jqTable.find('input[id$=UserName]');
        var jqPassword = jqTable.find('input[id$=Password]');
        var jqConfirmPassword = jqTable.find('input[id$=ConfirmPassword]');
        
        setEnabled(jqCredName, enableControls);
        setEnabled(jqUserName, enableControls);
        setEnabled(jqPassword, enableControls);
        setEnabled(jqConfirmPassword, enableControls);

        // support for JQuery Validation Plugin used by SW.APM.EditApp class, see http://docs.jquery.com/Plugins/Validation
        if (enableControls) {
            jqCredName.addClass("required");
            jqCredName.addClass("credentialSetExists");
            jqUserName.addClass("required");
            jqPassword.addClass("sameConfirmation");
        } else {
            jqCredName.removeClass("required");
            jqCredName.removeClass("credentialSetExists");
            jqUserName.removeClass("required");
            jqPassword.removeClass("sameConfirmation");
        }
        
        if (credentialSet != null) {
            jqCredName.val(credentialSet.Name);
            jqUserName.val(credentialSet.UserName);
            jqPassword.val(credentialSet.Password);
            jqConfirmPassword.val(credentialSet.Password);
        }
    };

    var selectedIndexChanged = function (combo, parentControlId) {
        var jqCombo = $(combo);
        var credentialSetId = parseInt(jqCombo.val());

        var warnMsg = $("td[colspan='2']>span[id$='ctrWarningMsg']");
        if (credentialSetId === SW.APM.SelectCredentials.NewCredentialId) {
            warnMsg.show();

            var creds = { Name: '', UserName: '', Password: '' };
            updateControls(jqCombo, true, creds);
        } else if (credentialSetId === SW.APM.SelectCredentials.NoneCredentialId ||
            credentialSetId === SW.APM.SelectCredentials.NodeWmiCredentialId) {
            warnMsg.hide();

            var creds = { Name: '', UserName: '', Password: '' };
            updateControls(jqCombo, false, creds);
            //TODO: we should hide validators too!
        } else {
            warnMsg.hide();
            //FB72399
            if (typeof(Page_Validators) != "undefined") {
                $.each(Page_Validators, function(i, el) { $(el).hide(); });
            }
            if (typeof(Page_ValidationSummaries) != "undefined") {
                $.each(Page_ValidationSummaries, function(i, el) {
                    if (parentControlId) {
                        if (el.id.indexOf(parentControlId) > -1) {
                            $(el).hide();
                        }
                    } else {
                        $(el).hide();
                    }
                });
            }

            ORION.callWebService(
                "/Orion/APM/Services/Credentials.asmx", "GetCredentialsById", { "credentialSetId": credentialSetId }, function (result) { updateControls(jqCombo, false, result); }
            );
        }
    };

    var loadCredentialSetList = function (combo, result) {
        var jqCombo = $(combo);
        var lastSelectedCredentialSetId = jqCombo.val();
        var lastNewCredentialSetName = jqCombo.closest('table').find('input[id$=CredentialName]').val();
        
        var notSpecialCredentialsSelector = SF('*[value={0}],*[value={1}],*[value={2}]',
            SW.APM.SelectCredentials.NewCredentialId,
            SW.APM.SelectCredentials.NoneCredentialId,
            SW.APM.SelectCredentials.NodeWmiCredentialId);
        jqCombo.find('option').not(notSpecialCredentialsSelector).remove();
        $.each(result, function (index, element) {
            jqCombo.append($("<option />").val(element.ID).text(element.Name));
            if (lastSelectedCredentialSetId == SW.APM.SelectCredentials.NewCredentialId && lastNewCredentialSetName == element.Name) {
                lastSelectedCredentialSetId = element.ID;
            }
        });

        jqCombo.val(lastSelectedCredentialSetId);
        updateControls(jqCombo, lastSelectedCredentialSetId == SW.APM.SelectCredentials.NewCredentialId);
    };

    var reloadCredentialSets = function(combo, callback) {
        ORION.callWebService(
            "/Orion/APM/Services/Credentials.asmx", "GetCredentialNames", {}, function (result) { loadCredentialSetList(combo, result); if (callback) callback(); }
        );
    };
    
    return {
        OnSelectedIndexChanged: selectedIndexChanged,
        ReloadCredentialSets: reloadCredentialSets,
        NewCredentialId: -1,
        NoneCredentialId: 0,
        NodeWmiCredentialId: -3
};
    
}();
