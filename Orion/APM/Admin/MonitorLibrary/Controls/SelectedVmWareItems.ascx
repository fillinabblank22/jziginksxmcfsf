﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectedVmWareItems.ascx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_Controls_SelectedVmWareItems" %>
<script type="text/javascript">
	Ext.ns("SW.APM.AppFinder");
	SW.APM.AppFinder.VmWareSelectedItems = function (pObj) {
		var config = {
			renderTo: "ctrGrid",
			width: 400, height: 350, title: "<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_18)%>", border: false, frame: true, autoScroll: true, trackMouseOver: false, disableSelection: true, stripeRows: false,
			viewConfig: { forceFit: true, enableRowBody: true },
			store: new Ext.data.ArrayStore({ fields: ["Category", "Counter", "Instance"], data: [] }),
			cm: new Ext.grid.ColumnModel([
				{ header: "<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_19)%>", dataIndex: "Counter", width: 210, renderer: function (pVal, pMD, pRec) { return String.format("<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_21)%>", pRec.get("Category"), pVal); }, sortable: false, menuDisabled: true }, 
				{ header: "<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_20)%>", dataIndex: "Instance", sortable: false, menuDisabled: true }
			])
		};
		var grid = new Ext.grid.GridPanel(config)
		grid.getStore().loadData(pObj.data);

		$("div#ctrContainer>span>a>label").text(String.format("<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_22)%>", pObj.count));
		$("div#ctrContainer>span>a").click(function () {
			var img = $(this).find("img"), colSrc = "/Orion/images/Button.Collapse.gif", expSrc = "/Orion/images/Button.Expand.gif";
			if (img.attr("src") == colSrc) {
				img.attr("src", expSrc); img.attr("title", "<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_23)%>"); $("div#ctrGrid").hide();
			} else {
				img.attr("src", colSrc); img.attr("title", "<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_24)%>"); $("div#ctrGrid").show();
			}
		});
		$("div#ctrGrid").hide();
	}
	Ext.onReady(function () {
		var json = <%=this.SelItemsJSON%>;
		if (json) { SW.APM.AppFinder.VmWareSelectedItems(json); } else { $("div#ctrContainer").remove(); }
	});	
</script>

<div id="ctrContainer" style="padding-bottom:5px;">
	<span>
		<a href="#" style="color:#369; cursor:pointer;">
			<img alt="Expand" src="/Orion/images/Button.Expand.gif" /> <label></label> 
		</a>
	</span>
	<div id="ctrGrid"></div>
</div>
