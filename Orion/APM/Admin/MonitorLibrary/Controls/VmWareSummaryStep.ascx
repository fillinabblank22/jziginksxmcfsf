<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VmWareSummaryStep.ascx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_Controls_VmWareSummaryStep" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Src="SelectedVmWareItems.ascx" TagPrefix="apm" TagName="VmWareItems" %>
<style type="text/css">
	table#QuickCreds{width:auto;}
	table#QuickCreds tbody tr td {border:solid 0px red;font-size:9pt;white-space:normal;}
</style>

<table id="QuickCreds" cellpadding="0" cellspacing="0">
	<tr>
	<td valign="top">        
    <br />
        <b><%=GetAppTemplateMessage() %></b>
        <div id="divAppName" runat="server">
           <asp:TextBox ID="NewAppName" runat="server" columns="40" OnTextChanged="NewAppNameTextChanged"/>
           <asp:RequiredFieldValidator ID="NewAppNameFieldValidator" runat="server" ControlToValidate="NewAppName"
            ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_267 %>">*</asp:RequiredFieldValidator>
        </div>
        <div>
        <asp:DataList ID="existingMonitors" runat="server">
            <ItemTemplate>
                <asp:Label runat="server" Text='<%#Eval("Name")%>'/>
            </ItemTemplate>
        </asp:DataList>
        </div>
    </td>
    </tr>    
    <tr>
	<td valign="top" style="color:#000000;">        
		<br />
        <b><%= Resources.APMWebContent.APMWEBDATA_VB1_268%></b>
        <apm:VmWareItems ID="vwSelectedItems" runat="server" Visible="true" />
	</td>
	</tr>
	<tr>
		<td>
        <%if (!TemplateSettings.UseExisting)
          {%>
        <br />
        <b><%= Resources.APMWebContent.APMWEBDATA_VB1_269 %></b>        
        <div>
        <asp:DataList ID="dlServers" runat="server">
            <ItemTemplate>
                <asp:Label ID="lblServerIP" runat="server" Text='<%#Eval("Name")%>'/>
            </ItemTemplate>
        </asp:DataList>
        </div>
        <%
          }%>
		</td>
	</tr>
</table>

