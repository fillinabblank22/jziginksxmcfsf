<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignToNodeStep.ascx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_Controls_AssignToNodeStep" %>

<%@ Register Src="../../../Controls/SelectNodeTree.ascx" TagPrefix="apm" TagName="SelectNodeTree" %>
<%@ Register Src="SelectedVmWareItems.ascx" TagPrefix="apm" TagName="VWItems" %>

<h2><asp:Literal ID="StepTitle" runat="server" /></h2>
<p><asp:Literal ID="Description" runat="server" /></p>
<apm:VWItems ID="vwSelectedItems" runat="server" Visible="true" />
<br />
<apm:SelectNodeTree ID="SelectNodeTree" runat="server" TreeWidth="700px" />

