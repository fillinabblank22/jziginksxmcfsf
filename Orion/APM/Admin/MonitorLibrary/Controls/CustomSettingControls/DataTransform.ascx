﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataTransform.ascx.cs"
    Inherits="Orion_APM_Admin_MonitorLibrary_Controls_CustomSettingControls_DataTransform" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI" %>
<%--<asp:HiddenField ID="hfIsEditViewPersist" runat="server" Value="false" />--%>
<asp:CheckBox ID="chbEnabled" runat="server" Text="Yes, convert returned value." /><asp:Literal ID="litTemplateView" runat="server" Text="No" />
<div id="divSettHelpText" runat="server"><%= Resources.APMWebContent.DataTransform_TheConvertedValue%>&nbsp;&nbsp;<a href="<%=DescriptionHelpLink %>" target="_blank">» Learn more</a></div><div id="divTransformSettings" runat="server" style="display: none; background-color: #E4F1F8; margin-top: 5px; margin-bottom: 5px; padding: 10px;">
    <asp:HiddenField ID="hfGetCurrentValueProcess" runat="server" Value="false" />
    <asp:HiddenField ID="hfTestExpanded" runat="server" Value="true" />
    <div style="margin-top: 5px;">
        <asp:RadioButton ID="rbCommonFormulas" runat="server" GroupName="Transform" Text="Common formulas" Font-Size="8pt" />
    </div>
    <div id="divCommonFormulas" runat="server" style="display: none; margin-top: 3px; padding-top: 5px; padding-bottom: 5px; padding-left: 40px;">
        <asp:DropDownList ID="ddlFunctions" runat="server">
            <asp:ListItem Text="-- Select function --" Value="0" />
            <asp:ListItem Text="Truncate" Value="1" />
            <asp:ListItem Text="Round" Value="2" />
        </asp:DropDownList>
        <div id="divEmpty" runat="server" style="display: none" />
        <div id="divTruncateRound" runat="server" style="display: none">
            to 
            <asp:DropDownList ID="ddlDecimalPlaces" runat="server">
                <asp:ListItem Text="0" Value="0" />
                <asp:ListItem Text="1" Value="1" />
                <asp:ListItem Text="2" Value="2" />
                <asp:ListItem Text="3" Value="3" />
                <asp:ListItem Text="4" Value="4" />
                <asp:ListItem Text="5" Value="5" />
            </asp:DropDownList>
            decimal places
        </div>
    </div>
    <div style="margin-top: 5px;">
        <asp:RadioButton ID="rbCustomConversion" runat="server" GroupName="Transform" Text="Custom conversion" Font-Size="8pt" />
    </div>
    <div id="divCustomConversion" runat="server" style="display: none; margin-top: 3px;">
        <table style="border: none;">
            <colgroup>
                <col width="20px" />
                <col width="70px" />
                <col width="140px" />
                <col width="300px" />                
            </colgroup>
            <tr>
                <td>&nbsp;</td>
                <td>Formula:</td>
                <td colspan="2"><asp:TextBox ID="tbCustomFormula" runat="server" Width="400px" /></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td colspan="2"><span style="font-size: 7pt;"><%= Resources.APMWebContent.DataTransform_CustomConversion_UseStatistic%> <% if (IsMultiValue) { %> <br /> <% } %> <%= Resources.APMWebContent.DataTransform_CustomConversion_ForExample%>&nbsp;&nbsp;<a href="<%=CustomFormulaHelpLink %>" target="_blank"><%= Resources.APMWebContent.ConvertValueEditor_MoreExamples_LinkText%></a></span></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><asp:Image ID="imgExpandCollapseTest" runat="server" style="cursor: pointer;" /></td>
                <td colspan="2"><%= Resources.APMWebContent.DataTransform_TestFormula%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr id="trInput" runat="server" style="display: none">
                <td>&nbsp;</td>
                <td>Input:</td>
                <td><asp:TextBox ID="tbInput" runat="server" /></td>
                <td><% if (!IsEditMonitorPropertiesStepInWizard)
                       { %><img src="<%= LocalizationUrlHelper.GetUrlFromTemplate("/Orion/APM/images/[lang]/Button.RetrieveCurrentValue.gif") %>" alt="Retrieve current value" style="cursor: pointer" onclick="SW.APM.Transformation.RetrieveValue('<%=TestSinglePostProcessingClientID %>', '<%=TestButtonClientID %>', '<%=tbInput.ClientID %>', '<%=hfGetCurrentValueProcess.ClientID %>', <%=IsMultiValue.ToString().ToLowerInvariant() %>, '<%=ColumnName %>');" /><% } %></td>
                <td>&nbsp;</td>
            </tr>
            <tr id="trOutput" runat="server" style="display: none">
                <td>&nbsp;</td>
                <td>Output:</td>
                <td><asp:Label ID="lblOutput" runat="server" /></td>
                <td><img src="<%= LocalizationUrlHelper.GetUrlFromTemplate("/Orion/APM/images/[lang]/Button.CalculateOutput.gif") %>" alt="Calculate output" style="cursor: pointer" onclick="SW.APM.Transformation.DataTransformEvaluateTest('<%=tbCustomFormula.ClientID %>', '<%=tbInput.ClientID %>', '<%=lblOutput.ClientID %>', '<%=divError.ClientID %>');" /></td>
                <td>&nbsp;</td>
            </tr>            
            <tr id="trError" runat="server" style="display: none; min-height: 1px;">
                <td colspan="2"></td>
                <td colspan="2"><div id="divError" runat="server" style="display: none; background-color: #FACECE; height: 26px; width: 110px; padding-top: 10px; padding-left: 5px;"><img src="/Orion/APM/images/Icon.Stop16x16.gif" alt="Error" style="float: left; margin-right: 5px;" /><div style="font-weight: bold; font-size: 8pt; color: #CE0000;">Invalid Formula</div></div></td>
                <td></td>
            </tr>            
        </table>        
    </div>
</div>