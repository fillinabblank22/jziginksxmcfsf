﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common.Models;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_CustomSettingControls_DataTransform : System.Web.UI.UserControl, ICustomSettingEditor
{
    #region Properties

    public bool IsEditView { get; set; }
    public bool IsMultiValue { get; set; }
    public string SettingTextBoxID { get; set; }
    public string TestButtonID { get; set; }
    public string TestSinglePostProcessingID { get; set; }
    public string ColumnName { get; private set; }

    protected string DescriptionHelpLink
    {
        get { return String.Format("{0}/NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm", SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer, "OrionAPMPHConversionValue"); }
    }

    protected string CustomFormulaHelpLink
    {
        get { return String.Format("{0}/NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm", SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer, "OrionAPMPHFormulaExamples"); }
    }

    protected bool IsEditMonitorPropertiesStepInWizard
    {
        get
        {
            return Page.Request.Url.AbsolutePath.Contains("EditMonitorProperties.aspx");
        }
    }

    private Control FindControlInParents(Control parent, string controlId)
    {
        if (parent == null)
            throw new ArgumentNullException("parent");

        Control ctrl = parent.FindControl(controlId);
        if (ctrl == null)
            return FindControlInParents(parent.Parent, controlId);
        else
            return ctrl;
    }

    protected TextBox SettingTextBox
    {
        get
        {
            if (String.IsNullOrEmpty(SettingTextBoxID))
                return null;
            return
                this.Parent.FindControl(SettingTextBoxID) as TextBox;
        }
    }

    private Control testButton;
    protected string TestButtonClientID
    {
        get
        {
            if (testButton == null)
            {
                if (String.IsNullOrEmpty(TestButtonID))
                    throw new ArgumentNullException("TestButtonID");

                testButton = FindControlInParents(this.Parent, TestButtonID);
            }
            if (testButton != null)
            {
                return testButton.ClientID;
            }

            return String.Empty;
            
            //int index = SettingTextBox.ClientID.IndexOf("editMonitorNode_");
            //return SettingTextBox.ClientID.Substring(0, index) + "editMonitorNode_TestSingleMonitorButton";
        }
    }

    private Control testSinglePostProcessingField;
    protected string TestSinglePostProcessingClientID
    {
        get
        {
            if (testSinglePostProcessingField == null)
            {
                if (String.IsNullOrEmpty(TestSinglePostProcessingID))
                    throw new ArgumentNullException("TestSinglePostProcessingID");

                testSinglePostProcessingField = FindControlInParents(this.Parent, TestSinglePostProcessingID);
            }
            if (testSinglePostProcessingField != null)
            {
                return testSinglePostProcessingField.ClientID;
            }

            return String.Empty;
            
            //int index = SettingTextBox.ClientID.IndexOf("editMonitorNode_");
            //return SettingTextBox.ClientID.Substring(0, index) + "editMonitorNode_TestSinglePostProcessing";
        }
    }

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        this.chbEnabled.Attributes.Add("onclick", String.Format("APMjs.setBlockVisibility('{0}', this.checked);", divTransformSettings.ClientID));
        this.rbCommonFormulas.Attributes.Add("onclick", String.Format("APMjs.setBlockVisibility('{0}', true); APMjs.setBlockVisibility('{1}', false);", divCommonFormulas.ClientID, divCustomConversion.ClientID));
        this.rbCustomConversion.Attributes.Add("onclick", String.Format("APMjs.setBlockVisibility('{0}', false); APMjs.setBlockVisibility('{1}', true);", divCommonFormulas.ClientID, divCustomConversion.ClientID));
        this.imgExpandCollapseTest.Attributes.Add("onclick", String.Format("SW.APM.Transformation.ExpandCollapseTest(this, '{0}', [ '{1}', '{2}', '{3}' ], this);", hfTestExpanded.ClientID, trInput.ClientID, trOutput.ClientID, trError.ClientID));
        this.ddlFunctions.Attributes.Add("onchange", String.Format("SW.APM.Transformation.ShowCommonFormulasDiv(this.selectedIndex, [ '{0}', '{1}', '{2}' ]);", divEmpty.ClientID, divTruncateRound.ClientID, divTruncateRound.ClientID));
    }

    #endregion

    #region ICustomSettingEditor methods

    // This method is executed when this ASCX control is set to visible
    public void Initialize(ComponentBase component, ComponentTemplate template)
    {
        if (SettingTextBox != null)
            SettingTextBox.Visible = false;

        if (template == null)
        {
            litTemplateView.Visible = false;
        }
        else
        {
            string transform = (string)GetSetting<string>(template.Settings, "TransformExpression", String.Empty);
            litTemplateView.Visible = !IsEditView && String.IsNullOrEmpty(transform);
        }

        chbEnabled.Visible = IsEditView;
        divTransformSettings.Visible = IsEditView;
    }

    // This method copies the settings of controls to component settings
    public bool UpdateFromViewState(ComponentBase component)
    {
        if (!this.Visible || !this.IsEditView)
            return true;

        component.Settings["TransformExpression"].Value = GetSettingValue();

        component.Settings["__DataTransformEnabled"] = new SettingValue("__DataTransformEnabled", chbEnabled.Checked.ToString(), SettingValueType.Boolean, SettingLevel.Instance, false);
        component.Settings["__DataTransformCheckedRadioButton"] = new SettingValue("__DataTransformCheckedRadioButton", CheckedRadioButtonIndex().ToString(), SettingValueType.Integer, SettingLevel.Instance, false);
        component.Settings["__DataTransformTestExpanded"] = new SettingValue("__DataTransformTestExpanded", Convert.ToBoolean(hfTestExpanded.Value).ToString(), SettingValueType.Boolean, SettingLevel.Temporary, false);
        component.Settings["__DataTransformInput"] = new SettingValue("__DataTransformInput",rbCustomConversion.Checked ? (Convert.ToBoolean(hfGetCurrentValueProcess.Value) ? "Retrieving..." : tbInput.Text) : String.Empty, SettingValueType.String, SettingLevel.Temporary, false);
        component.Settings["__DataTransformOutput"] = new SettingValue("__DataTransformOutput", rbCustomConversion.Checked ? lblOutput.Text : String.Empty, SettingValueType.String, SettingLevel.Temporary, false);
        component.Settings["__DataTransformOriginalFormula"] = new SettingValue("__DataTransformOriginalFormula", rbCustomConversion.Checked ? tbCustomFormula.Text : String.Empty, SettingValueType.String, SettingLevel.Temporary, false);

        component.Settings["__DataTransformCommonFormulaIndex"] = new SettingValue("__DataTransformCommonFormulaIndex", ddlFunctions.SelectedValue, SettingValueType.Integer, SettingLevel.Instance, false);
        component.Settings["__DataTransformCommonFormulaOptions"] = new SettingValue("__DataTransformCommonFormulaOptions", ddlDecimalPlaces.SelectedValue, SettingValueType.String, SettingLevel.Instance, false);

        return true;
    }

    // This method sets the controls from component settings
    public void Update(ComponentBase component, ComponentTemplate template)
    {
        if (!this.Visible || !this.IsEditView)
            return;

        PopulateUI(
            (bool)GetSetting<bool>(component.Settings, "__DataTransformEnabled", false),
            (int)GetSetting<int>(component.Settings, "__DataTransformCheckedRadioButton", 0),
            (bool)GetSetting<bool>(component.Settings, "__DataTransformTestExpanded", true),
            (string)GetSetting<string>(component.Settings, "__DataTransformInput", String.Empty),
            (string)GetSetting<string>(component.Settings, "__DataTransformOutput", String.Empty),
            (string)GetSetting<string>(component.Settings, "TransformExpression", String.Empty),
            (string)GetSetting<string>(component.Settings, "__DataTransformOriginalFormula", String.Empty),
            (int)GetSetting<int>(component.Settings, "__DataTransformCommonFormulaIndex", 0),
            (string)GetSetting<string>(component.Settings, "__DataTransformCommonFormulaOptions", String.Empty));
    }

    #endregion

    #region Methods for DynamicColumn.ascx

    // This method is executed when this ASCX control is set to visible
    public void InitializeMultiValue(ComponentBase component, ComponentTemplate template, DynamicEvidenceColumnSchema column)
    {
        tbCustomFormula.Width = 275;

        chbEnabled.Visible = IsEditView;
        divTransformSettings.Visible = IsEditView;

        string transform = null;
        if (template != null)
        {
            var templateColumn = template.DynamicColumnSettings.FirstOrDefault(c => c.Name.Equals(column.Name, StringComparison.InvariantCultureIgnoreCase) && c.Type == column.Type);
            transform = (templateColumn == null || templateColumn.DataTransform == null) ? String.Empty : templateColumn.DataTransform.TransformExpression;
        }
        litTemplateView.Visible = !IsEditView;
        litTemplateView.Text = String.IsNullOrEmpty(transform) ? "No" : transform;

        ColumnName = column.Name;
    }

    // This method copies the settings of controls to component settings
    public bool UpdateFromViewStateMultiValue(ComponentBase component, ComponentTemplate template, DynamicEvidenceColumnSchema column)
    {
        if (!this.Visible || !this.IsEditView)
            return true;

        column.DataTransform.TransformExpression = GetSettingValue();

        column.DataTransform.Enabled = chbEnabled.Checked;
        column.DataTransform.CheckedRadioButton = CheckedRadioButtonIndex();
        column.DataTransform.TestExpanded = Convert.ToBoolean(hfTestExpanded.Value);
        column.DataTransform.TestInput = rbCustomConversion.Checked ? (Convert.ToBoolean(hfGetCurrentValueProcess.Value) ? "Retrieving..." : tbInput.Text) : String.Empty;
        column.DataTransform.TestOutput = rbCustomConversion.Checked ? lblOutput.Text : String.Empty;
        column.DataTransform.OriginalFormula = rbCustomConversion.Checked ? tbCustomFormula.Text : String.Empty;

        column.DataTransform.CommonFormulaIndex = Convert.ToInt32(ddlFunctions.SelectedValue);
        column.DataTransform.CommonFormulaOptions = ddlDecimalPlaces.SelectedValue;

        return true;
    }

    public void UpdateMultiValue(ComponentBase component, ComponentTemplate template, DynamicEvidenceColumnSchema column)
    {
        if (!this.Visible || !this.IsEditView)
            return;

        if (column.DataTransform == null)
            return;

        PopulateUI(
            column.DataTransform.Enabled,
            column.DataTransform.CheckedRadioButton,
            column.DataTransform.TestExpanded,
            column.DataTransform.TestInput,
            column.DataTransform.TestOutput,
            column.DataTransform.TransformExpression,
            column.DataTransform.OriginalFormula,
            column.DataTransform.CommonFormulaIndex,
            column.DataTransform.CommonFormulaOptions);
    }

    #endregion

    #region Helper methods

    private void SetDislayStyle(IEnumerable<HtmlControl> ctrls, string value)
    {
        foreach (HtmlControl ctrl in ctrls)
            SetDislayStyle(ctrl, value);
    }

    private void SetDislayStyle(HtmlControl ctrl, string value)
    {
        string style = ctrl.Attributes["style"] ?? String.Empty;
        style += ";";

        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("display:([^;])*;");
        var match = regex.Match(style);

        if (match.Success)
        {
            style = style.Replace(match.Value, String.Format("display: {0};", value));
        }
        else
        {
            style += String.Format("; display: {0};", value);
        }

        ctrl.Attributes["style"] = style;
    }

    private void PopulateUI(
        bool dataTransformEnabled,
        int checkedRadioButtonIndex,
        bool expandedTestFormula,
        string testInput,
        string testOutput,
        string transformExpression,
        string originalFormula,
        int commonFormulaIndex,
        string commonFormulaOption)
    {
        chbEnabled.Checked = dataTransformEnabled;

        rbCommonFormulas.Checked = rbCustomConversion.Checked = false;
        GetRadioButton(checkedRadioButtonIndex).Checked = true;

        imgExpandCollapseTest.ImageUrl = expandedTestFormula ? "~/Orion/images/Button.Collapse.gif" : "~/Orion/images/Button.Expand.gif";
        SetDislayStyle(new HtmlControl[] { trInput, trOutput, trError }, expandedTestFormula ? "table-row" : "none");

        tbInput.Text = testInput ?? String.Empty;
        lblOutput.Text = testOutput ?? String.Empty;

        tbCustomFormula.Text = String.IsNullOrEmpty(originalFormula) ? transformExpression : originalFormula;

        if (chbEnabled.Checked)
            SetDislayStyle(divTransformSettings, "block");

        if (rbCustomConversion.Checked)
        {
            SetDislayStyle(divCustomConversion, "block");
        }
        else if (rbCommonFormulas.Checked)
        {
            SetDislayStyle(divCommonFormulas, "block");
            ddlFunctions.SelectedIndex = commonFormulaIndex;
            SetCommonFormulasOptions(ddlFunctions.SelectedIndex, commonFormulaOption);
        }
    }

    private void SetCommonFormulasOptions(int selectedIndex, string options)
    {
        HtmlControl[] ctrlArray = new HtmlControl[] { divEmpty, divTruncateRound };
        foreach (HtmlControl ctrl in ctrlArray)
            SetDislayStyle(ctrl, "none");

        switch (selectedIndex)
        {
            case 0:
                SetDislayStyle(divEmpty, "inline");
                break;
            case 1:
            case 2:
                string decimalPlaces = String.IsNullOrEmpty(options) ? "0" : options;
                ddlDecimalPlaces.SelectedIndex = Convert.ToInt32(decimalPlaces);
                SetDislayStyle(divTruncateRound, "inline");
                break;
        }
    }

    private object GetSetting<T>(IDictionary<string, SettingValue> settings, string key, T defaultValue)
    {
        if (settings == null)
            return defaultValue;

        if (!settings.ContainsKey(key))
            return defaultValue;

        string value = settings[key].Value;

        if (typeof(T) == typeof(bool))
            return Convert.ToBoolean(value);
        else if (typeof(T) == typeof(int))
            return Convert.ToInt32(value);
        else if (typeof(T) == typeof(double))
            return Convert.ToDouble(value);
        else if (typeof(T) == typeof(DateTime))
            return Convert.ToDateTime(value);
        else
            return value;
    }

    // Returns the final transform expression
    private string GetSettingValue()
    {
        if (!chbEnabled.Checked)
            return String.Empty;

        if (Convert.ToBoolean(hfGetCurrentValueProcess.Value))
        {
            return String.Empty;
        }
        else
        {
            if (rbCommonFormulas.Checked)
            {
                switch (ddlFunctions.SelectedIndex)
                {
                    case 0:
                        return String.Empty;
                    case 1:
                        return String.Format("Truncate({0}, {1})", "${Statistic}", ddlDecimalPlaces.SelectedValue);
                    case 2:
                        return String.Format("Round({0}, {1})", "${Statistic}", ddlDecimalPlaces.SelectedValue);
                    default:
                        return String.Empty;
                }
            }
            else if (rbCustomConversion.Checked)
            {
                return tbCustomFormula.Text;
            }
            else
            {
                return String.Empty;
            }
        }
    }

    // Returns the name of selected radio button
    private int CheckedRadioButtonIndex()
    {
        if (rbCommonFormulas.Checked)
            return 0;
        else if (rbCustomConversion.Checked)
            return 1;
        else
            return 0;
    }

    private RadioButton GetRadioButton(int index)
    {
        switch (index)
        {
            case 0:
                return rbCommonFormulas;
            case 1:
                return rbCustomConversion;
            default:
                throw new IndexOutOfRangeException("index");
        }
    }


    #endregion
}
