using System;
using System.Globalization;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_SelectCredentials : System.Web.UI.UserControl
{
    private CredentialSet credentialSet = null;
    private CredentialSet dataSource = null;

    private bool credentialSetNameNotExists = false;
    private bool credentialSetNameExistsValidated = false;

    private bool allowNoneCredential = false;
    private bool allowNodeWmiCredential = false;

    #region Properties

    public bool IsValid 
	{
		get
        {
            // validation is needed only for Newly created credentials
            if (SelectedCredentialSetId != ComponentBase.NewCredentialsId) return true;

            return CredentialName.Text.IsValid() 
                && UserName.Text.IsValid() 
                && Password.Text == ConfirmPassword.Text
                && nameExistsValidator.IsValid;
        }
	}

    /// <summary>
    /// Generally after switching to known credential set, 
    /// javascript hides all validation summaries on the page.
    /// By setting <see cref="ValidationSummaryParentClientID"/> property
    /// the javascript part of the control hides only 
    /// validation summaries which are contained in the control set by this property.
    /// </summary>
    /// <remarks>
    /// <para>Always set this property if you have more than one validation summary on a page!</para>
    /// <para>You can set the property to ClientID of <see cref="System.Web.UI.Control.NamingContainer"/> of the validation summary
    /// or you can set ClientID directly of the validation summary control itself.</para>
    /// </remarks>
    //TODO: refactor to automatically hide proper validation summary without need to set ValidationSummaryParentClientID property
    public string ValidationSummaryParentClientID { get; set; }

	private string validationGroupName;
    public string ValidationGroupName
    {
        get 
		{
			const String DefaultGroupName = "SelectCredentialsValidationGroup";
			if (validationGroupName.IsNotValid()) 
			{
				validationGroupName = DefaultGroupName;
			}
			return validationGroupName; 
		}
		set 
		{ 
			validationGroupName = value;
			SetValidationGroup(ValidationGroupName);
		}
    }

    public CredentialSet DataSource
    {
        set
        {
            if (value == null && allowNoneCredential)
            {
				CredentialSetList.SelectedValue = ComponentBase.NoCredentialSetId.ToString();
            }

            this.dataSource = value;
        }
    }

    public CredentialSet SelectedCredentialSet
    {
        get
        {
            if (credentialSet == null)
            {
                this.credentialSet = loadOrCreateCredentialSet(SelectedCredentialSetId, CredentialName.Text, UserName.Text, Password.Text);
            }

            return credentialSet;
        }
    }

    public int SelectedCredentialSetId
    {
        get
        {
            int result;
            if (int.TryParse(CredentialSetList.SelectedValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
            {
				if (result != ComponentBase.NewCredentialsId) 
				{
					return result;
				}
            }
            return ComponentBase.NewCredentialsId;
        }
    }

    public bool AllowNoneCredential
    {
        get { return allowNoneCredential; }
        set
        {
            allowNoneCredential = value;
            ConfigureSpecialCredentials();
        }
    }

    public bool AllowNodeWmiCredential
    {
        get { return allowNodeWmiCredential; }
        set
        {
            allowNodeWmiCredential = value;
            ConfigureSpecialCredentials();
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        string cmd = String.Format("SW.APM.SelectCredentials.OnSelectedIndexChanged(this, {0});",
            SolarWinds.APM.Web.Utility.JsHelper.Serialize(this.ValidationSummaryParentClientID)
        );
        CredentialSetList.Attributes["onchange"] = cmd;

        var uri = new Uri(Request.Url.AbsoluteUri, UriKind.Absolute);
        ctrWarningMsg.Visible = (
            !Request.IsLocal && uri.Scheme.Equals(Uri.UriSchemeHttp, StringComparison.OrdinalIgnoreCase)
        );

        SetValidationGroup(ValidationGroupName);

        //if we want to see values in credential box after postback we should reset it here
        //For new credentials password shouldn't be reseted
        if (Page.IsPostBack)
        {
            CredentialName.Text = SelectedCredentialSet.Name;
            UserName.Text = SelectedCredentialSet.UserName;
            if (SelectedCredentialSet.Id != ComponentBase.NewCredentialsId && SelectedCredentialSet.Id != ComponentBase.NodeWmiCredentialsId)
            {
                Password.Attributes.Add("value", CredentialSet.FakePassword);
                ConfirmPassword.Attributes.Add("value", CredentialSet.FakePassword);
            }
            else
            {
                Password.Attributes.Add("value", String.Empty);
                ConfirmPassword.Attributes.Add("value", String.Empty);
            }
        }
    }

    /// <summary>
    /// In case that you don't want to loose your password during postback 
    /// (if password is not saved to DB for example),
    /// call this method.
    /// </summary>
    public void PreFillPasswords()
    {
        Password.Attributes["value"] = Password.Text;
        ConfirmPassword.Attributes["value"] = ConfirmPassword.Text;
    }

    #region CredentialSet list

    public void ReloadCredentialSets()
    {
        CredentialSetList.Items.Clear();
        CredentialSetList.Items.Add(new ListItem(Resources.APMWebContent.APMWEBCODE_AK1_95, ComponentBase.NewCredentialsId.ToString()));

        ConfigureSpecialCredentials();

        IDictionary<int, string> credentialNames;
        using (IAPMBusinessLayer businesLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            credentialNames = businesLayer.GetCredentialNames();
        }

        // following is implemented in SW.APM.SelectCredentials.ReloadCredentialSets() too
        CredentialSetList.AppendDataBoundItems = true;
        CredentialSetList.DataTextField = "Value";
        CredentialSetList.DataValueField = "Key";
        CredentialSetList.DataSource = credentialNames;
        CredentialSetList.DataBind();

    }

    protected void CredentialSetList_OnPreRender(object sender, EventArgs e)
    {
        if (CredentialSetList.Items.Count == 0)
            this.ReloadCredentialSets();
        this.EnableNewCredentialSetForm();
    }

    protected void CredentialSetList_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.EnableNewCredentialSetForm();
    }

    private void ConfigureSpecialCredentials()
    {
        if (CredentialSetList == null || CredentialSetList.Items.Count == 0)
        {
            return;
        }
        ConfigureSpecialCredentials(Resources.APMWebContent.APMWEBCODE_AK1_96, ComponentBase.NodeWmiCredentialsId, allowNodeWmiCredential);
        ConfigureSpecialCredentials(Resources.APMWebContent.APMWEBCODE_AK1_97, ComponentBase.NoCredentialSetId, allowNoneCredential);
    }

    private void ConfigureSpecialCredentials(string displayName, int value, bool required)
    {
        if (required)
        {
            if (CredentialSetList != null && CredentialSetList.Items.FindByValue(value.ToString()) == null)
            {
                ListItem itemToAdd = new ListItem(displayName, value.ToString());
                CredentialSetList.Items.Insert(1, itemToAdd);
            }
        }
        else
        {
            if (CredentialSetList == null) return;
            ListItem itemToRemove = CredentialSetList.Items.FindByValue(value.ToString());
            if (itemToRemove != null)
            {
                CredentialSetList.Items.Remove(itemToRemove);
            }
        }
    }

    #endregion

    #region CredentialSet form

    public void DataBindNewCredentialSetForm()
    {
        if (CredentialSetList.Items.Count == 0)
            this.ReloadCredentialSets();

        var creds = this.dataSource ?? this.loadOrCreateCredentialSet(this.SelectedCredentialSetId, this.CredentialName.Text, this.UserName.Text, this.Password.Text);

        var item = CredentialSetList.Items.FindByValue(creds.Id.ToString(CultureInfo.InvariantCulture));
        if (item != null)
            item.Selected = true;

        CredentialName.Text = creds.Name;
        UserName.Text = creds.UserName;

		if (creds.Id == ComponentBase.NewCredentialsId || creds.Id == ComponentBase.NoCredentialSetId)
        {
            Password.Attributes["value"] = string.Empty;
            ConfirmPassword.Attributes["value"] = string.Empty;
        }
        else
        {
			Password.Attributes["value"] = CredentialSet.FakePassword;
			ConfirmPassword.Attributes["value"] = CredentialSet.FakePassword;
        }
    }

    private CredentialSet loadOrCreateCredentialSet(int credentialSetId, string credentialName, string userName, string password)
    {
        CredentialSet result;
        if (credentialSetId == ComponentBase.NewCredentialsId)
        {
            result = new CredentialSet();
            result.Id = ComponentBase.NewCredentialsId;
            result.Name = credentialName;
            result.UserName = userName;
            result.Password = password;
        }
        else if (credentialSetId == ComponentBase.NoCredentialSetId || credentialSetId == ComponentBase.NodeWmiCredentialsId) // special credentials
        {
            result = new CredentialSet();
            result.Id = credentialSetId;
            result.Name = string.Empty;
            result.UserName = string.Empty;
            result.Password = password;
        }
        else // existing credentials
        {
            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                result = businessLayer.GetCredentialsById(credentialSetId);
            }
        }
        return result;
    }

    private void EnableNewCredentialSetForm()
    {
        // the list of controls should be same as in SW.APM.SelectCredentials.SelectedIndexChanged (.js)
        var allowEdit = this.SelectedCredentialSetId == ComponentBase.NewCredentialsId;
        CredentialName.Enabled = allowEdit;
        UserName.Enabled = allowEdit;
        Password.Enabled = allowEdit;
        ConfirmPassword.Enabled = allowEdit;
        EnableValidationForAnonymousCredentials(allowEdit);
    }

    #endregion

    #region Validation

    protected void OnNameExists_ServerValidate(Object source, ServerValidateEventArgs args)
    {
        //to prevent this redundant expensive validation if page validation is called explicitly
        if (!credentialSetNameExistsValidated)
        {
            credentialSetNameExistsValidated = true;

            // the test should be same when using JQuery Validation plugin, see script tag in SelectCredentials.ascx
            credentialSetNameNotExists = true;
            if (SelectedCredentialSet.Id == -1 && CredentialNameFieldValidator.IsValid)
            {
				credentialSetNameNotExists = true;
				
				var credName = this.CredentialName.Text.Trim();
				foreach (ListItem item in CredentialSetList.Items)
				{
					if (item.Text.Trim().Equals(credName, StringComparison.InvariantCultureIgnoreCase))
					{
						credentialSetNameNotExists = false;
						break;
					}
				}
            }
        }
        args.IsValid = credentialSetNameNotExists;
    }

	protected void OnComparePassword_ServerValidate(Object source, ServerValidateEventArgs args) 
	{
		if (SelectedCredentialSetId != ComponentBase.NewCredentialsId) 
		{
			return;
		}
        // the test should be same when using JQuery Validation plugin, see script tag in SelectCredentials.ascx
        args.IsValid = Password.Text == ConfirmPassword.Text;
	}

    private void EnableValidationForAnonymousCredentials(bool enable)
    {
        CredentialNameFieldValidator.Enabled = enable;
        nameExistsValidator.Enabled = enable;
        UserNameFieldValidator.Enabled = enable;
        ComparePasswordValidator.Enabled = enable;
    }

	private void SetValidationGroup(string validationGroupName) 
	{
		CredentialNameFieldValidator.ValidationGroup = validationGroupName;
		nameExistsValidator.ValidationGroup = validationGroupName;
		UserNameFieldValidator.ValidationGroup = validationGroupName;
		ComparePasswordValidator.ValidationGroup = validationGroupName;

        Password.CssClass = Password.ClientID;
    }

    #endregion

}
