<%@ Import namespace="SolarWinds.APM.Web"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddToTemplateStep.ascx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_Controls_AddToTemplateStep" %>

<style type="text/css">
	input[type=text] {
		margin-left: 5px;
	}
	input[type=radio], input[type=checkbox] {
		margin-right: 5px;
	}
</style>
<h2><asp:Literal ID="StepTitle" runat="server" /></h2>
<p><asp:Literal ID="Description" runat="server" /></p>

<br />
<asp:ValidationSummary ID="ValidationSummary1" runat="server"/>
<asp:CustomValidator runat="server" ID="ExistingTemplateListValidator"  OnServerValidate="ValidateCheckBoxSelection" Display="none"></asp:CustomValidator>
<asp:RadioButton ID="rbNewTemplate" runat="server" GroupName="addTo" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_34 %>" AutoPostBack="true" OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelNewTemplate" runat="server" CssClass="apm_IndentedPanel">

    <table cellpadding="5" cellspacing="0">
        <tr>
            <td><%= Resources.APMWebContent.APMWEBDATA_VB1_35 %></td>
            <td>
                <asp:TextBox ID="NewTemplateName" runat="server" columns="40"/>
                <asp:RequiredFieldValidator ID="NewTemplateNameFieldValidator" runat="server" ControlToValidate="NewTemplateName"
                 ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_VB1_36 %>">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:RadioButton ID="rbExistingTemplate" runat="server" GroupName="addTo" Text="<%$ Resources:APMWebContent, APMWEBDATA_VB1_37 %>" AutoPostBack="true" OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelExistingTemplate" runat="server" CssClass="apm_IndentedPanel">
    <asp:CheckBoxList ID="ExistingTemplateList" DataTextField="Name" DataValueField="ID" runat="server"/>
</asp:Panel>

<asp:RadioButton ID="rbNewApp" runat="server" GroupName="addTo" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_38 %>" AutoPostBack="true"  OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelNewApp" runat="server" CssClass="apm_IndentedPanel">
    <table cellpadding="5" cellspacing="0">
        <tr>
            <td><%= Resources.APMWebContent.APMWEBDATA_VB1_39 %></td>
            <td>
                <asp:TextBox ID="NewAppName" runat="server" columns="40" />
                <asp:RequiredFieldValidator ID="NewAppNameFieldValidator" runat="server" ControlToValidate="NewAppName"
                 ErrorMessage="<%$ Resources:APMWebContent, APMWEBDATA_VB1_36 %>">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
    
<asp:RadioButton ID="rbExistingApp" runat="server" GroupName="addTo" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_40 %>" AutoPostBack="true"  OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelExistingApp" runat="server" CssClass="apm_IndentedPanel">
    <asp:CheckBoxList ID="ExistingAppList" DataTextField="Name" DataValueField="ID" runat="server"/>
</asp:Panel>





    
