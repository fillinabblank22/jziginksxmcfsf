﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_SelectPlatform : System.Web.UI.UserControl
{
    public bool Use64Bit
    {
        get { return Convert.ToBoolean(ddlPlatform.SelectedItem.Value); }
        set { ddlPlatform.SelectedIndex = value ? 1 : 0; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}