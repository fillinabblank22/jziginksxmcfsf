<%@ Import namespace="SolarWinds.APM.Web"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VmWareAddToTemplateStep.ascx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_Controls_VmWareAddToTemplateStep" %>
<%@ Register Src="SelectedVmWareItems.ascx" TagPrefix="apm" TagName="VWItems" %>

<h2><asp:Literal ID="StepTitle" runat="server" /></h2>
<p><asp:Literal ID="Description" runat="server" /></p>

<asp:ValidationSummary runat="server"/>
<asp:CustomValidator runat="server" ID="ExistingTemplateListValidator"  OnServerValidate="ValidateCheckBoxSelection" Display="none"></asp:CustomValidator>
<br />
<apm:VWItems ID="vwSelectedItems" runat="server" Visible="true" />
<br />
<asp:RadioButton ID="rbNewTemplate" runat="server" GroupName="addTo" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_34 %>" AutoPostBack="true" OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelNewTemplate" runat="server" CssClass="apm_IndentedPanel">

    <table cellpadding="5" cellspacing="0">
        <tr>
            <td><%= Resources.APMWebContent.APMWEBDATA_VB1_35%></td>
            <td>
                <asp:TextBox ID="NewTemplateName" runat="server" columns="40"/>
                <asp:RequiredFieldValidator ID="NewTemplateNameFieldValidator" runat="server" ControlToValidate="NewTemplateName"
                 ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_36%>">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:RadioButton ID="rbExistingTemplate" runat="server" GroupName="addTo" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_37 %>" AutoPostBack="true" OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelExistingTemplate" runat="server" CssClass="apm_IndentedPanel">
    <asp:CheckBoxList ID="ExistingTemplateList" runat="server" DataTextField="Name" DataValueField="ID" />
</asp:Panel>

<asp:RadioButton ID="rbNewApp" runat="server" GroupName="addTo" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_38 %>" AutoPostBack="true"  OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelNewApp" runat="server" CssClass="apm_IndentedPanel">
    <table cellpadding="5" cellspacing="0">
        <tr>
            <td><%= Resources.APMWebContent.APMWEBDATA_VB1_39%></td>
            <td>
                <asp:TextBox ID="NewAppName" runat="server" columns="40" />
                <asp:RequiredFieldValidator ID="NewAppNameFieldValidator" runat="server" ControlToValidate="NewAppName"
                 ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_267 %>">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
    
<asp:RadioButton ID="rbExistingApp" runat="server" GroupName="addTo" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_40 %>" AutoPostBack="true"  OnCheckedChanged="OnRadioSelectionChanged" />
<br />
<asp:Panel ID="PanelExistingApp" runat="server" CssClass="apm_IndentedPanel">
    <asp:CheckBoxList ID="ExistingAppList" runat="server" DataTextField="Name" DataValueField="ID"/>
</asp:Panel>





    
