﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_SelectedVmWareItems : System.Web.UI.UserControl
{
	#region Properties

	protected String SelItemsJSON
	{
		get 
		{
			var workflow = this.Session[AppFinderWorkflow.SessionKey] as AppFinderWorkflow;
			if (workflow != null && workflow.CurrentSettings.BrowseMode == AppFinderBrowseMode.VmWarePerfCounter) 
			{
				var info = workflow.CurrentSettings.VmWareComponentList;
				if (info != null) 
				{
					var data = new List<String[]>();
					foreach (var category in info.GetCategories()) 
					{
						foreach (var counter in info.GetCounters(category))
						{
							foreach (var instance in counter.CounterInstances)
							{
								data.Add(new[] { category, counter.CounterDisplayName, instance });
							}
						}
					}
					if (data.Count > 0)
					{
						var result = new {count = data.Count, data = data};
						return JsHelper.Serialize(result);
					}
				}
			}
			return "null";
		}
	}

	#endregion
}
