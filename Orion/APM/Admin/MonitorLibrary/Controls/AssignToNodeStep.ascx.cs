using System;
using System.Collections.Generic;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_Controls_AssignToNodeStep : System.Web.UI.UserControl
{
    public List<string> MonitorSelectionList
    {
        get;
        set;
    }

    public string NodeToSelectByDefault
    {
        get;
        set;
    }

    public AppFinderBrowseMode BrowseMode { get; set; }

    protected string StepTitleText
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_113; }
    }

    protected string DescriptionText
    {
        get
        {
            string prefix;
            if (MonitorSelectionList.Count == 1)
                prefix = Resources.APMWebContent.APMWEBCODE_AK1_114;
            else
                prefix = Resources.APMWebContent.APMWEBCODE_AK1_115;
            if (BrowseMode == AppFinderBrowseMode.VmWarePerfCounter)
                return String.Format(prefix, "");
            return String.Format(prefix, String.Join(", ", MonitorSelectionList.ToArray()));
        }
    }

	public string ViewNodeFilter
	{
		set { SelectNodeTree.ViewNodeFilter = value; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			StepTitle.Text = StepTitleText;
			Description.Text = DescriptionText;

			SelectNodeTree.NodeToSelectByDefault = NodeToSelectByDefault;
		}
	}

    public List<int> GetSelectedNodeIds()
    {
        return SelectNodeTree.GetSelectedNodeIds();
    }
}
