<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
    AutoEventWireup="true" CodeFile="AddToTemplate.aspx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_AddToTemplate" Title="<%$ Resources: APMWebContent, APMWEBDATA_VB1_33%>" %>
    
<%@ Register Src="../Controls/AddToTemplateStep.ascx" TagPrefix="apm" TagName="AddToTemplateStep" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">

    <apm:AddToTemplateStep ID="AddToTemplateStep" runat="server"  OnSelectionChanged="OnAddToTemplateSelectionChanged" />

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbBack" runat="server" OnClick="OnBack" CausesValidation="False" DisplayType="Secondary" LocalizedText="Back"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" OnClick="OnNext" DisplayType="Primary" LocalizedText="Next"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" OnClick="OnCancel" CausesValidation="False" DisplayType="Secondary" LocalizedText="Cancel"/>
    </div>
    
</asp:Content>

