﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" AutoEventWireup="true" CodeFile="SelectVmWareCounters.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_SelectVmWareCounters" %>

<%@ Register TagPrefix="Custom" Namespace="SolarWinds.APM.Web.Controls" Assembly="SolarWinds.APM.Web" %>

<asp:Content ID="ctrBody" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <orion:Include runat="server" Module="APM" File="Admin/MonitorLibrary/AppFinder/JS/VmWareCounters.js"/>
	<script type="text/javascript">
		Ext.onReady(function () {
			window.$af = new SW.APM.AppFinder.VmWareCounter();
			$af.reloadCounters(); 
			$af.showError(<%=this.ErrorMessage%>);
		});	
	</script>

    <style type="text/css">
        option.VirtualMachinePowerStateUp { background-image: url(/Orion/APM/Images/StatusIcons/Small-Up.gif); background-repeat: no-repeat; background-position: 0px 0px; padding-left: 20px; }
        option.VirtualMachinePowerStateDown { background-image: url(/Orion/APM/Images/StatusIcons/Small-Down.gif); background-repeat: no-repeat; background-position: 0px 0px; padding-left: 20px; }
    </style>

	<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_255 %></h2>
    <p>
        <%=Resources.APMWebContent.APMWEBDATA_VB1_256 %>
    </p>
	<div id="ctrBodyCounter">
		<br />
		<asp:UpdatePanel ID="ctrUP" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div style="padding-bottom:5px; border-bottom:1px solid #ccc;">
                    <asp:RadioButton ID="ctrInstanceTypeM" GroupName="instanceType" Text="<%$ Resources : APMWebcontent , APMWEBDATA_VB1_257 %>" OnCheckedChanged="OnInstanceType_CheckedChanged" Checked="true" AutoPostBack="true" runat="server" />&nbsp;&nbsp;
                    <asp:RadioButton ID="ctrInstanceTypeS" GroupName="instanceType" Text="<%$ Resources : APMWebcontent , APMWEBDATA_VB1_258 %>" OnCheckedChanged="OnInstanceType_CheckedChanged" AutoPostBack="true" runat="server" />
				</div>
				<br />
				<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_259 %></h2>
				<p>
					<%= Resources.APMWebContent.APMWEBDATA_VB1_260 %>
				</p>
                <br />
				<asp:DropDownList ID="ctrEntityTypes" Width="200" DataTextField="Text" DataValueField="Value" OnTextChanged="OnVmWareEntityTypes_TextChanged" onchange="$af.onPostBack();" AutoPostBack="true" runat="server"/>&nbsp;&nbsp;
				<Custom:CustomDropDownList ID="ctrEntities" Width="200" DataTextField="Text" DataValueField="Value" OnTextChanged="OnVmWareEntities_TextChanged" onchange="$af.onPostBack();" AutoPostBack="true" runat="server"/>

				<br /><br />
				<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_261 %></h2>
                <p>
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_262 %>
                </p>
				<br />
				<%= Resources.APMWebContent.APMWEBDATA_VB1_263 %><br />
				<asp:DropDownList ID="ctrPerfObjects" Width="200" DataTextField="Text" DataValueField="Value" onChange="return $af.reloadCounters();" runat="server"/>
			</ContentTemplate>
		</asp:UpdatePanel>
	
		<br />
		<div id="ctrGridCounter"></div>	
		<input id="selEntities" name="selEntities" type="hidden" />
	</div>	
	<br/>
	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton ID="imgbBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack_Click"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" LocalizedText="Next" DisplayType="Primary" OnClick="OnNext_Click" OnClientClick="return $af.onNextClick();" CausesValidation="false"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click"/>
	</div>
</asp:Content>