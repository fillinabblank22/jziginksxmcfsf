﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
	AutoEventWireup="true" CodeFile="AddToExistingTemplate.aspx.cs" 
	Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_AddToExistingTemplate" Title="<%$ Resources: APMWebContent, APMWEBDATA_VB1_33%>" %>

<asp:Content ID="c1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">

    <%= Resources.APMWebContent.AddToExistingTemplate_TheFollowingComponent%> <br />
	<div style="padding-left: 15px;">
		<asp:Literal ID="ctrCmpsLabel" runat="server"/> <br />
	</div>
    <%= Resources.APMWebContent.AddToExistingTemplate_WillBeAddedTo%> <asp:Literal ID="ctrEntryLabel" runat="server"/> <br />
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbBack" runat="server" OnClick="OnBack" CausesValidation="False" DisplayType="Secondary" LocalizedText="Back"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" OnClick="OnNext" DisplayType="Primary" LocalizedText="Next"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" OnClick="OnCancel" CausesValidation="False" DisplayType="Secondary" LocalizedText="Cancel"/>
    </div>
    
</asp:Content>