using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_Summary : AppFinderPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckIfValid();
        if (!IsPostBack)
        {
            LoadMonitorsToAdd();

			imgbNext.AddEnterHandler(1);
        }
    }

    private void LoadMonitorsToAdd()
    {
        monitorRepeater.DataSource = Workflow.MonitorsToAdd;
        monitorRepeater.DataBind();
    }

    protected void OnBack(object sender, EventArgs e)
    {
        GotoPreviousPage();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        Workflow.BeginCreateApplications();

        GotoNextPage();
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected void monitorRepeater_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int componentId = Int32.Parse(e.CommandArgument.ToString(), CultureInfo.InvariantCulture);

		Workflow.MonitorsToAdd.RemoveAll(ct => ct.Id == componentId);

        LoadMonitorsToAdd();

    }


}
