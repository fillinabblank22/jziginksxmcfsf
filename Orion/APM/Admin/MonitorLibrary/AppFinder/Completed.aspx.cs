using System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_Completed : SolarWinds.APM.Web.UI.AppFinderPageBase
{
	protected void Page_Load(object sender, EventArgs e)
	{
		imgbViewExApps.AddEnterHandler(1);
		imgbEditExApps.Visible = false;

		CheckIfValid();

		var sett = Workflow.AddToTemplateSettings;
		if (sett.NavigatedFrom == NavigatedFrom.EditPage)
		{
			if (sett.Mode == AddToTemplateMode.ExistingTemplate)
			{
				imgbEditExApps.Visible = true;
				imgbEditExApps.Text = string.Format(@"Continue edit ""{0}"" template", sett.ExistingNames[0]);
			}
			else if (sett.Mode == AddToTemplateMode.ExistingApplication)
			{
				imgbEditExApps.Visible = true;
				imgbEditExApps.Text = string.Format(@"Continue edit ""{0}"" application", sett.ExistingNames[0]);
			}
			if (imgbEditExApps.Visible)
			{
				imgbEditExApps.PostBackUrl = ReferrerRedirector.Base64ToString(sett.EditPageUrl);
			}
		}
		AppFinderWorkflow.ResetSession();
	}
}