﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorTypeSection.ascx.cs" Inherits="MonitorTypeSection" %>
<%@ Register Src="~/Orion/APM/Admin/MonitorLibrary/AppFinder/Controls/MonitorTypeItem.ascx" TagPrefix="apm" TagName="MonitorTypeItem" %>

<div class="section">
    <div class="header">
        <img alt="<%= this.ImageFilename %>" src="<%= this.ImageUrl %>" />      
        <div>
            <span class="title"><%= this.Title %></span>
            <br />
            <span class="description"><%= this.Description %></span>     
        </div>                      
    </div>
    <div class="body">
        <asp:PlaceHolder ID="phMonitorTypeItems" runat="server"></asp:PlaceHolder>
    </div>
</div>