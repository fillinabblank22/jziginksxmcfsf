﻿using System;
using SolarWinds.APM.Common.Models;

public partial class MonitorTypeItem : System.Web.UI.UserControl
{
	#region Properties

	public string Title
	{
		get;
		set;
	}

	public string Description
	{
		get;
		set;
	}

	public string ImageFilename
	{
		get;
		set;
	}

	public string ImageUrl
	{
		get
		{
			return string.Format("/Orion/APM/Images/Admin/ComponentMonitors/{0}", this.ImageFilename);
		}
	}

	public ComponentType Type
	{
		get;
		set;
	}

	public bool Selected
	{
		get
		{
			return this.rbtnSelection.Checked;
		}
		set
		{
			this.rbtnSelection.Checked = value;
		}
	}

	#endregion

	#region Protected methods

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		CreateRadioButtonsScript();
	}

	#endregion

	#region Private methods

	private void CreateRadioButtonsScript()
	{
		var script = @"
$().ready(function() { 
	$('div[apm-el-type=radiobox]').click(function(evt) { 
		if(!$(evt.target).is('input')) { 
			$(this).find('input[id$=rbtnSelection]').click(); 
		}
		return true;
	});
	$('input[id$=rbtnSelection]').click(function(evt) { 
		$('input[id$=rbtnSelection]').attr('checked', false);
		$(this).attr('checked', true);
		return true;	
	});
});
";
		if (!this.Page.ClientScript.IsClientScriptBlockRegistered("radioButtonsSelection"))
		{
			this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "radioButtonsSelection", script, true);
		}
	}

	#endregion
}