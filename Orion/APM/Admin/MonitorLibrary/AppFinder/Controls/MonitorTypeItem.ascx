﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorTypeItem.ascx.cs" Inherits="MonitorTypeItem" %>

<div class="item" apm-el-type="radiobox">
	<table>
		<tbody>
			<tr>
				<td class="rbtnCell">
					<asp:RadioButton ID="rbtnSelection" runat="server" GroupName="select" />
				</td>
				<td class="iconCell">
					<img alt="<%= this.ImageFilename %>" src="<%= this.ImageUrl %>" style="float: left;" />
				</td>
				<td class="textCell">
					<span class="title"><%= this.Title %></span>
					<br />
					<span class="description"><%= this.Description %></span>
				</td>
			</tr>
		</tbody>
	</table>
</div>