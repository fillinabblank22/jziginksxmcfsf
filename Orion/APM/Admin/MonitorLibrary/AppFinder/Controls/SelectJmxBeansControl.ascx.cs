﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.UI.AppFinder;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_Controls_SelectJmxBeansControl : ScriptUserControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this.Page).Services.Add(new ServiceReference("~/Orion/APM/Services/JmxWizardService.asmx"));
    }

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor =
            new ScriptControlDescriptor("Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl",
                                        this.ClientID);
        descriptor.AddElementProperty("extArea", extArea.ClientID);
        descriptor.AddProperty("emptyGuid", Guid.Empty);
        descriptor.AddProperty("jmxEntityTypeCompositeValue", JmxEntityType.CompositeAttribute);
        descriptor.AddProperty("jmxEntityTypeSimpleValue", JmxEntityType.SimpleAttribute);
        descriptor.AddProperty("jmxEntityTypeCompositeValueItem", JmxEntityType.CompositeAttributeItem);

        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.4/ext-all-plus-jquery_adapter.js"));
        //yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.2.0/debug/jquery-bridge.js"));
        //yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.2.0/debug/ext.js"));
        //yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.2.0/debug/ext-all-debug.js"));
        yield return new ScriptReference(this.ResolveUrl("~/Orion/APM/js/RowExpander.js"));
        yield return new ScriptReference(this.ResolveUrl("~/Orion/APM/js/ScriptServiceInvoker.js"));
        yield return new ScriptReference(this.ResolveUrl("SelectJmxBeansControl.js"));
    }
}