﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference name="~/Orion/js/jquery/jquery-1.2.6-vsdoc.js"/>

Type.registerNamespace("Orion.APM.Admin.MonitorLibrary.AppFinder.Controls");

Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl = function (element) {
    Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl.initializeBase(this, [element]);
    this.extArea = null;
    this.emptyGuid = null;
    this.jmxEntityTypeCompositeValue = null;
    this.jmxEntityTypeSimpleValue = null;
    this.jmxEntityTypeCompositeValueItem = null;
    this.mainPanel = null;
    this.mbeansTree = null;
    this.mbeansPanel = null;
    this.mbeansGrid = null;
    this.rootTreeNode = null;
    this.treeLoader = null;
    this.scriptServiceInvoker = null;
    this.treeLoadMask = null;
    this.rowExpander = null;

    this.store = null;
}

Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl.prototype = {
    get_extArea: function () {
        return this.extArea;
    },
    set_extArea: function (value) {
        this.extArea = value;
    },

    get_jmxEntityTypeCompositeValue: function () {
        return this.jmxEntityTypeCompositeValue;
    },
    set_jmxEntityTypeCompositeValue: function (value) {
        this.jmxEntityTypeCompositeValue = value;
    },

    get_jmxEntityTypeSimpleValue: function () {
        return this.jmxEntityTypeSimpleValue;
    },
    set_jmxEntityTypeSimpleValue: function (value) {
        this.jmxEntityTypeSimpleValue = value;
    },

    get_jmxEntityTypeCompositeValueItem: function () {
        return this.jmxEntityTypeCompositeValueItem;
    },
    set_jmxEntityTypeCompositeValueItem: function (value) {
        this.jmxEntityTypeCompositeValueItem = value;
    },

    get_emptyGuid: function () {
        return this.emptyGuid;
    },
    set_emptyGuid: function (value) {
        this.emptyGuid = value;
    },

    initialize: function () {
        Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl.callBaseMethod(this, 'initialize');

        this.scriptServiceInvoker = $create(Orion.APM.js.ScriptServiceInvoker);

        $(Function.createDelegate(this, this.initializeExtJs));

        $("div[tooltip!='processed'][class~='x-tree-node']:not([class~='NoTip'])").livequery(function () {
            this.tooltip = 'processed';
            var nodeId = $(this).attr("ext:tree-node-id");
            var treePanel = Ext.ComponentMgr.get("mbeansTree");
            var node = treePanel.root.findChild('id', nodeId, true);
            if (node.attributes.AttributeName) {
                var html = '';
                var htmlPattern = '\
<div class="apm_jmx_tooltip">\
<div class="apm_jmx_tooltip_header">{0}</div>\
<div class="apm_jmx_tooltip_body">\
<div class="apm_jmx_tooltip_body_item"><div class="apm_jmx_tooltip_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_AK1_22;E=js}</div>{1}</div>\
<div class="apm_jmx_tooltip_body_item" style="display:{3};"><div class="apm_jmx_tooltip_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_TM0_70;E=js}</div>{2}</div>\
<div class="apm_jmx_tooltip_body_item"  style="display:{5};"><div class="apm_jmx_tooltip_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_TM0_71;E=js}</div>{4}</div>\
<div class="apm_jmx_tooltip_body_item"><div class="apm_jmx_tooltip_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_TM0_72;E=js}</div>{6}</div>\
</div>\
</div>';
                html = String.format(
                    htmlPattern,
                    node.attributes.DisplayName,
                    node.attributes.ObjectName,
                    node.attributes.Description,
                    node.attributes.Description ? "inline" : "none",
                    node.attributes.Key,
                    node.attributes.Key ? "inline" : "none",
                    node.attributes.Type);
                $.swtooltip(this, html);
            }
        });

        $("div[class~='apm_jmx_tooltip']").livequery(function () {
            var parent = $(this).parent();
            parent.addClass('cluetip-apm-jmx');

            //let's minimize shadow to avoid visible shadow reducing after the div is resized
            parent.siblings("div[class~='tooltip-shadow']").css({
                width: 1,
                height: 1
            });

            //let's wait with shadow resizing until tooltip events are finished
            setTimeout(function () {
                parent.siblings("div[class~='tooltip-shadow']").css({
                    width: parent.width(),
                    height: parent.height()
                });
            }, 0);
        });
    },

    dispose: function () {
        // Add custom dispose actions here
        Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl.callBaseMethod(this, 'dispose');
    },

    initializeExtJs: function () {

        this.rootTreeNode = new Ext.tree.AsyncTreeNode({
            id: 'root',
            Name: 'root',
            EntityType: 0
        });

        this.treeLoader = new Ext.tree.TreeLoader({
            directFn: Function.createDelegate(this, this.GetTreeData),
            nodeParameter: 'node',
            listeners: {
                // init load event handler
                beforeload: Function.createDelegate(this, this.BeforeTreeNodeLoad),
                // after load event handler
                load: Function.createDelegate(this, this.OnTreeNodeLoad)
            }
        });


        this.store = new Ext.data.ArrayStore({
            id: 'store',
            fields: [
                { name: 'DisplayName' },
                { name: 'Domain' },
                { name: 'ObjectName' },
                { name: 'AttributeName' },
                { name: 'Key' },
                { name: 'KeyDisplay' },
                { name: 'Description' },
                { name: 'DescriptionDisplay' },
                { name: 'Type' },
                { name: 'Value' },
                { name: 'RefreshButtonId'}],
            data: []
        });

        this.mbeansTree = new Ext.tree.TreePanel({
            id: 'mbeansTree',
            autoScroll: true,
            loader: this.treeLoader,
            rootVisible: false,
            lines: false,
            rootVisible: false,
            useArrows: true,
            root: this.rootTreeNode
        });

        this.rowExpander = new Ext.ux.grid.RowExpander({
            tpl: new Ext.Template(
                            '<div class="apm_jmx_attribute_detail">',
                            '<div class="apm_jmx_attribute_item"><div class="apm_jmx_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_AK1_22;E=js}</div> {ObjectName}</div>',
                            '<div class="apm_jmx_attribute_item" style="display:{DescriptionDisplay};"><div class="apm_jmx_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_TM0_70;E=js}</div> {Description}</div>',
                            '<div class="apm_jmx_attribute_item" style="display:{KeyDisplay};"><div class="apm_jmx_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_TM0_71;E=js}</div> {Key}</div>',
                            '<div class="apm_jmx_attribute_item"><div class="apm_jmx_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_TM0_72;E=js}</div> {Type}</div>',
                            '<div class="apm_jmx_attribute_item"><div class="apm_jmx_attribute_subtitle">@{R=APM.Strings;K=APMWEBJS_TM0_73;E=js}</div>',
                            '<table style="table-layout: auto;"><tr><td style="vertical-align: middle; width: auto;"><span class="apm_jmx_attribute_value">{Value}</span></td><td>' + SW.Core.Widgets.Button('@{R=APM.Strings;K=APMWEBJS_VB1_93; E=js}', { type: 'small', cls: 'x-btn apm_jmx_attribute_refresh_value', id: '{RefreshButtonId}' }) + '</td></tr></table></div>',
                            '</div>'
                        ),
            listeners: {
                'expand': Function.createDelegate(this, this.OnRowExpanderExpand),
                'collapse': Function.createDelegate(this, this.OnRowExpanderCollapse)
            }
        });

        this.mbeansGrid = new Ext.grid.GridPanel({
            id: 'mbeansGrid',
            store: this.store,
            columns: [
                {
                    renderer: Function.createDelegate(this, this.RenderExpandCollapseColumnn),
                    width: 25
                },
                { id: 'DisplayName', dataIndex: 'DisplayName' },
                {
                    renderer: Function.createDelegate(this, this.RenderDeleteColumn),
                    width: 25
                }
            ],
            autoExpandColumn: 'DisplayName',
            plugins: [this.rowExpander],
            hideHeaders: true,
            stripeRows: true,
            listeners: {
                'cellclick': Function.createDelegate(this, this.GridCellClick)
            }
        });

        this.treePanel = new Ext.Panel({
            id: 'treePanel',
            title: '@{R=APM.Strings;K=APMWEBJS_TM0_74;E=js}',
            region: 'center',
            layout: 'fit',
            frame: true,
            split: true,
            items: [this.mbeansTree]
        });

        this.mbeansPanel = new Ext.Panel({
            id: 'mbeansPanel',
            title: '@{R=APM.Strings;K=APMWEBJS_TM0_75;E=js}',
            region: 'east',
            layout: 'fit',
            width: 550,
            frame: true,
            split: true,
            items: [this.mbeansGrid]
        });


        this.mainPanel = new Ext.Panel({
            id: 'mainPanel',
            id: 'mainPanel',
            layout: 'border',
            autoWidth: true,
            height: 400,
            renderTo: this.extArea,
            items: [this.treePanel, this.mbeansPanel]
        });

        this.LoadGrid();

    },

    OnRowExpanderExpand: function (expander, record, body, rowIndex) {
        this.SetExpandCollapseButton(rowIndex);

        var valueElement = $(body).find(".apm_jmx_attribute_value");
        var eventData = {
            record: record,
            valueElement: valueElement
        }
        $(body).find(".apm_jmx_attribute_refresh_value").bind('click', eventData, Function.createDelegate(this, this.OnRefreshValueClick));
    },

    OnRefreshValueClick: function (eventData) {
        this.scriptServiceInvoker.callService(
            JmxWizard.JmxWizardService.GetAttributeValue,
            [this.emptyGuid, eventData.data.record.data.ObjectName, eventData.data.record.data.AttributeName, eventData.data.record.data.Key || null],
            eventData,
            2000,
            this,
            Function.createDelegate(this, this.OnGetAttributeValueSuccess),
            Function.createDelegate(this, this.OnGetAttributeValueSuccess),
            Function.createDelegate(this, this.OnGetAttributeValueError));


        $(eventData.data.valueElement).addClass('apm_jmx_attribute_value_loading');
        $(eventData.data.valueElement).html("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");
        return false;
    },

    OnGetAttributeValueSuccess: function (resultContainer, context) {
        //if used for various webmethods, taskId as first attribute expected !!!
        resultContainer.webMethodParams[0] = resultContainer.result.TaskId;

        if (resultContainer.result.IsNotFinished) {
            return;
        }

        var eventData = context;
        $(eventData.data.valueElement).removeClass('apm_jmx_attribute_value_loading');

        var value = resultContainer.result.ResponseData
        $(eventData.data.valueElement).html(String.format("{0}", value));

        var node = this.FindTreeNode(eventData.data.record.data.ObjectName, eventData.data.record.data.AttributeName, eventData.data.record.data.Key);
        if (node != null) {
            node.attributes.Value = value;
        }
    },

    OnGetAttributeValueError: function (error, context) {
        var eventData = context;
        $(eventData.data.valueElement).removeClass('apm_jmx_attribute_value_loading');
        $(eventData.data.valueElement).html("@{R=APM.Strings;K=APMLIBCODE_AK1_2;E=js}"); 
        if (this.HandleCommonError(error)) {
            return;
        }
        Ext.Msg.alert('Error', error.get_message());

    },

    OnRowExpanderCollapse: function (expander, record, body, rowIndex) {
        this.SetExpandCollapseButton(rowIndex);
    },

    RenderExpandCollapseColumnn: function (value, metaData, record, rowIndex, colIndex, store) {
        var row = this.mbeansGrid.getView().getRow(rowIndex);
        if (!row || Ext.fly(row).hasClass('x-grid3-row-collapsed')) {
            metaData.css = 'apm_jmx_grid_expand';
        }
        else {
            metaData.css = 'apm_jmx_grid_collapse';
        }
    },

    SetExpandCollapseButton: function (rowIndex) {
        var row = this.mbeansGrid.getView().getRow(rowIndex);
        if (!row) {
            return;
        }
        var cell = this.mbeansGrid.getView().getCell(rowIndex, 0);
        if (!cell) {
            return;
        }
        if (Ext.fly(row).hasClass('x-grid3-row-collapsed')) {
            Ext.fly(cell).removeClass('apm_jmx_grid_collapse');
            Ext.fly(cell).addClass('apm_jmx_grid_expand');
        }
        else {
            Ext.fly(cell).removeClass('apm_jmx_grid_expand');
            Ext.fly(cell).addClass('apm_jmx_grid_collapse');
        }
    },

    RenderDeleteColumn: function (value, metaData, record, rowIndex, colIndex, store) {
        metaData.css = 'apm_jmx_grid_delete'
    },

    GridCellClick: function (grid, rowIndex, columnIndex, e) {
        if (columnIndex == 0) { //expand, collapse
            this.rowExpander.toggleRow(rowIndex);
        }
        else if (columnIndex == 2) {//delete
            var record = this.mbeansGrid.getStore().getAt(rowIndex);
            var node = this.FindTreeNode(record.data.ObjectName, record.data.AttributeName, record.data.Key);
            if (node != null) {
                node.getUI().toggleCheck(false);
            }
            else {
                JmxWizard.JmxWizardService.UnselectAttribute(
                    record.data.Domain, record.data.ObjectName, record.data.AttributeName, record.data.Key,
                    Function.createDelegate(this, this.OnUnselectAttributeSuccess),
                    Function.createDelegate(this, this.OnUnselectAttributeError));
            }
        }
    },

    OnUnselectAttributeSuccess: function (result, context) {
        this.LoadGrid();
    },

    OnUnselectAttributeError: function (error, context) {
        if (this.HandleCommonError(error, context)) {
            return;
        }
        Ext.Msg.alert('Error', error.get_message());
    },

    FindTreeNode: function (objectName, attributeName, key) {
        var node = this.mbeansTree.getRootNode().findChildBy(
            function (node) {
                return (node.attributes.ObjectName == objectName
                && node.attributes.AttributeName == attributeName
                && (node.attributes.Key || null) == (key || null));
            },
            this, true);
        return node;
    },

    LoadGrid: function () {
        this.ShowGridLoadMask();
        this.scriptServiceInvoker.callService(
            JmxWizard.JmxWizardService.GetSelectedAttributes,
            [],
            this,
            2000,
            this,
            Function.createDelegate(this, this.OnGetSelectedAttributesSuccess),
            Function.createDelegate(this, this.OnGetSelectedAttributesSuccess),
            Function.createDelegate(this, this.OnGetSelectedAttributesError));
    },

    OnGetSelectedAttributesSuccess: function (resultContainer, context) {
        //if used for various webmethods, taskId as first attribute expected !!!
        resultContainer.webMethodParams[0] = resultContainer.result.TaskId;

        if (resultContainer.result.IsNotFinished) {
            return;
        }

        this.HideGridLoadMask();
        var data = [];
        var responseData = resultContainer.result.ResponseData;
        for (var i = 0; i < responseData.length; i++) {
            data.push([
                responseData[i].DisplayName,
                responseData[i].Domain,
                responseData[i].ObjectName,
                responseData[i].AttributeName,
                responseData[i].Key,
                responseData[i].Key ? "inline" : "none",
                responseData[i].Description,
                responseData[i].Description ? "inline" : "none",
                responseData[i].Type,
                responseData[i].Value || "@{R=APM.Strings;K=APMLIBCODE_AK1_2;E=js}",
                (this.get_id() + "_apmRefreshValue_" + i)]);
        }

        this.mbeansGrid.getStore().loadData(data);

        for (var i = 0; i < responseData.length; i++) {
            this.SetExpandCollapseButton(i);
        }
        this.mbeansPanel.setTitle(String.format("@{R=APM.Strings;K=APMWEBJS_TM0_76;E=js}", responseData.length));
    },

    OnGetSelectedAttributesError: function (error, context) {
        this.HideGridLoadMask();
        this.mbeansPanel.setTitle("@{R=APM.Strings;K=APMWEBJS_TM0_75;E=js}");
        if (HandleCommonError(error, context)) {
            return;
        }
    },

    HandleCommonError: function (error, context) {
        if (error.get_statusCode() == 401 || error.get_statusCode() == 403) {
            Ext.Msg.alert('@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}', '@{R=APM.Strings;K=APMWEBJS_TM0_66;E=js}');
            window.location.reload();
            return true;
        }
        return false;
    },

    GetTreeData: function (nodeId, callbackMethod) {
        var node = this.mbeansTree.getNodeById(nodeId);
        var taskId = this.emptyGuid;
        var nodeNames = this.GetNodeNamesToRoot(node);
        var context = {
            nodeId: nodeId,
            callbackMethod: callbackMethod
        }
        if (nodeId == 'root') {
            this.ShowTreeLoadMask();
        }

        if (node.attributes.EntityType == this.jmxEntityTypeCompositeValue) {
            var objectName = node.parentNode.attributes.ObjectName;
            var attributeName = node.attributes.AttributeName;
            this.scriptServiceInvoker.callService(
                JmxWizard.JmxWizardService.GetChildrenNodesForCompositeAttribute,
                [taskId, nodeNames, objectName, attributeName],
                context,
                2000,
                this,
                Function.createDelegate(this, this.OnGetChildrenTreeNodesSuccess),
                Function.createDelegate(this, this.OnGetChildrenTreeNodesSuccess),
                Function.createDelegate(this, this.OnGetChildrenTreeNodesError));
        }
        else {
            var entityType = node.attributes.EntityType;
            this.scriptServiceInvoker.callService(
                JmxWizard.JmxWizardService.GetChildrenTreeNodes,
                [taskId, nodeNames, entityType],
                context,
                2000,
                this,
                Function.createDelegate(this, this.OnGetChildrenTreeNodesSuccess),
                Function.createDelegate(this, this.OnGetChildrenTreeNodesSuccess),
                Function.createDelegate(this, this.OnGetChildrenTreeNodesError));
        }
    },

    OnGetChildrenTreeNodesSuccess: function (resultContainer, context) {
        var result = resultContainer.result.ResponseData;

        //if used for various webmethods, taskId as first attribute expected !!!
        resultContainer.webMethodParams[0] = resultContainer.result.TaskId;

        if (resultContainer.result.IsNotFinished) {
            return;
        }
        var nodes = [];
        if (result) {
            for (var i = 0; i < result.length; i++) {
                var node = {
                    id: result[i].Id,
                    text: result[i].Text,
                    iconCls: result[i].IconCls,
                    leaf: result[i].IsLeaf,
                    checked: result[i].IsCheckable ? result[i].IsChecked : 'undefined',
                    Name: result[i].Name,
                    DisplayName: result[i].DisplayName,
                    ObjectName: result[i].ObjectName,
                    AttributeName: result[i].AttributeName,
                    Description: result[i].Description,
                    Key: result[i].Key,
                    Type: result[i].Type,
                    Value: result[i].Value,
                    EntityType: result[i].EntityType,
                    listeners: { 'checkchange': Function.createDelegate(this, this.OnNodeCheckChange) }
                }
                nodes.push(node);
            }
        }
        var response = {
            status: true,
            scope: this,
            argument: {
                callback: context.callbackMethod,
                node: context.nodeId
            }
        };

        if (typeof context.callbackMethod == "function") {
            context.callbackMethod(nodes, response);
        }
    },

    OnGetChildrenTreeNodesError: function (error, context) {
        if (this.HandleCommonError(error, context)) {
            return;
        }
        this.HideTreeLoadMask();
        var node = this.mbeansTree.getNodeById(context.nodeId);
        this.SetNodeIcon(node, node.attributes.iconCls, 'apm_jmx_icon_loading');
        Ext.Msg.alert('Error', error.get_message().replace('\n', '<br/>'));

        var response = {
            status: false,
            scope: this,
            argument: {
                callback: context.callbackMethod,
                node: context.nodeId
            }
        };

        if (typeof context.callbackMethod == "function") {
            context.callbackMethod([], response);
        }
        node.attributes.expandable = true;
        node.collapse();
        node.loaded = false;
    },

    OnNodeCheckChange: function (node, checked) {
        var displayName = null;
        var objectName = null;
        var attributeName = null;
        var key = null;
        var nodeNames = this.GetNodeNamesToRoot(node);

        if (node.attributes.EntityType == this.jmxEntityTypeSimpleValue) {
            displayName = node.attributes.DisplayName;
            objectName = node.attributes.ObjectName;
            attributeName = node.attributes.AttributeName;
        }
        else if (node.attributes.EntityType == this.jmxEntityTypeCompositeValueItem) {
            displayName = node.attributes.DisplayName;
            objectName = node.attributes.ObjectName;
            attributeName = node.attributes.AttributeName;
            key = node.attributes.Key;
        }
        if (objectName != null && attributeName != null) {
            JmxWizard.JmxWizardService.ChangeAttributeSelection(
                nodeNames, displayName, objectName, attributeName, key, checked,
                Function.createDelegate(this, this.OnChangeAttributeSelectionSuccess),
                Function.createDelegate(this, this.OnChangeAttributeSelectionError));
        }
    },

    OnChangeAttributeSelectionSuccess: function (result, context) {
        this.LoadGrid();
    },

    OnChangeAttributeSelectionError: function (error, context) {
        if (this.HandleCommonError(error, context)) {
            return;
        }
        Ext.Msg.alert('Error', error.get_message());
    },

    GetNodeNamesToRoot: function (node) {
        var nodeNames = [];
        while (node) {
            nodeNames.push(node.attributes.Name);
            node = node.parentNode;
        }
        return nodeNames;
    },

    BeforeTreeNodeLoad: function (treeLoader, node) {
        this.SetNodeIcon(node, 'apm_jmx_icon_loading', node.attributes.iconCls);
    },

    OnTreeNodeLoad: function (treeLoader, node, response) {
        node.expanded = true;
        this.SetNodeIcon(node, node.attributes.iconCls, 'apm_jmx_icon_loading');
        if (node.id == 'root') {
            this.HideTreeLoadMask();
        }
    },

    ShowTreeLoadMask: function () {
        if (!this.treeLoadMask) {
            this.treeLoadMask = new Ext.LoadMask(this.treePanel.el, {
                msg: "@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}"
            });
        }
        this.treeLoadMask.show();
    },

    HideTreeLoadMask: function () {
        if (this.treeLoadMask) {
            this.treeLoadMask.hide();
        }
    },

    ShowGridLoadMask: function () {
        if (!this.gridLoadMask) {
            this.gridLoadMask = new Ext.LoadMask(this.mbeansGrid.el, {
                msg: "@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}"
            });
        }
        this.gridLoadMask.show();
    },

    HideGridLoadMask: function () {
        if (this.gridLoadMask) {
            this.gridLoadMask.hide();
        }
    },

    SetNodeIcon: function (node, iconClsToAdd, iconClsToRemove) {
        var iel = node.getUI().getIconEl();
        if (iel) {
            var el = Ext.get(iel);
            if (el) {
                el.removeClass(iconClsToRemove);
                el.addClass(iconClsToAdd);
            }
        }
    }

}
Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl.registerClass('Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxBeansControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
