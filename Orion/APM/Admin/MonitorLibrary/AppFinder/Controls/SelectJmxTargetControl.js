﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference name="~/Orion/js/jquery/jquery-1.2.6-vsdoc.js"/>

Type.registerNamespace("Orion.APM.Admin.MonitorLibrary.AppFinder.Controls");

Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl = function (element) {
    Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl.initializeBase(this, [element]);
    this.selectTargetNode_SelectedIpAddress = null;
    this.selectTargetNode_Id = null;
    this.selectNodePopUpOkButton = null;
    this.targetServerLabel = null;
    this.targetServerStatusImg = null;
    this.targetServerHiddenField = null;
    this.targetServerIdHiddenField = null;
    this.portTextBox = null;
    this.protocolDropDown = null;
    this.urlTextBox = null;
    this.targetEndpointUrlElement = null;

    this.defaultUrlFormat = null;
    this.urlFormatTextBox = null;

    this.btnApplyCustomizedURL = null;
    this.btnReapplyDefaultURL = null;
}

Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl.prototype = {
    initialize: function () {
        Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl.callBaseMethod(this, 'initialize');
        if (!$(this.targetServerStatusImg).attr("src")) {
            $(this.targetServerStatusImg).hide();
        }
        if ($(this.targetServerHiddenField).val() != "")
            $(this.targetServerLabel).html($(this.targetServerHiddenField).val() + "&nbsp;");
        $(this.portTextBox).keydown(Function.createDelegate(this, this.OnPortTextBoxKeyDown));
        $(this.portTextBox).change(Function.createDelegate(this, this.OnUrlPartChange));
        $(this.protocolDropDown).change(Function.createDelegate(this, this.OnUrlPartChange));
        $(this.urlTextBox).change(Function.createDelegate(this, this.OnUrlPartChange));

        $(this.btnApplyCustomizedURL).click(Function.createDelegate(this, this.OnApplyUrl));
        $(this.btnReapplyDefaultURL).click(Function.createDelegate(this, this.OnReaplyDefaultUrl));

        if ($(this.urlFormatTextBox).val() == "")
            this.OnReaplyDefaultUrl();
        else
            this.OnUrlPartChange();

        this.selectNodePopUpOkButton.disabled = true;
    },

    dispose: function () {
        // Add custom dispose actions here
        Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl.callBaseMethod(this, 'dispose');
    },

    get_selectNodePopUpOkButton: function () {
        return this.selectNodePopUpOkButton;
    },
    set_selectNodePopUpOkButton: function (value) {
        this.selectNodePopUpOkButton = value;
    },

    get_targetServerLabel: function () {
        return this.targetServerLabel;
    },
    set_targetServerLabel: function (value) {
        this.targetServerLabel = value;
    },

    get_targetServerStatusImg: function () {
        return this.targetServerStatusImg;
    },
    set_targetServerStatusImg: function (value) {
        this.targetServerStatusImg = value;
    },

    get_targetServerHiddenField: function () {
        return this.targetServerHiddenField;
    },
    set_targetServerHiddenField: function (value) {
        this.targetServerHiddenField = value;
    },
    
    get_targetServerIdHiddenField: function () {
        return this.targetServerIdHiddenField;
    },
    set_targetServerIdHiddenField: function (value) {
        this.targetServerIdHiddenField = value;
    },
    
    get_portTextBox: function () {
        return this.portTextBox;
    },
    set_portTextBox: function (value) {
        this.portTextBox = value;
    },

    get_protocolDropDown: function () {
        return this.protocolDropDown;
    },
    set_protocolDropDown: function (value) {
        this.protocolDropDown = value;
    },

    get_urlTextBox: function () {
        return this.urlTextBox;
    },
    set_urlTextBox: function (value) {
        this.urlTextBox = value;
    },

    get_targetEndpointUrlElement: function () {
        return this.targetEndpointUrlElement;
    },
    set_targetEndpointUrlElement: function (value) {
        this.targetEndpointUrlElement = value;
    },

    get_defaultUrlFormat: function () {
        return this.defaultUrlFormat;
    },
    set_defaultUrlFormat: function (value) {
        this.defaultUrlFormat = value;
    },

    get_urlFormatTextBox: function () {
        return this.urlFormatTextBox;
    },
    set_urlFormatTextBox: function (value) {
        this.urlFormatTextBox = value;
    },

    get_btnApplyCustomizedURL: function () {
        return this.btnApplyCustomizedURL;
    },
    set_btnApplyCustomizedURL: function (value) {
        this.btnApplyCustomizedURL = value;
    },

    get_btnReapplyDefaultURL: function () {
        return this.btnReapplyDefaultURL;
    },
    set_btnReapplyDefaultURL: function (value) {
        this.btnReapplyDefaultURL = value;
    },

    HandleCommonError: function (error, context) {
        if (error.get_statusCode() == 401 || error.get_statusCode() == 403) {
            Ext.Msg.alert('@{R=APM.Strings;K=APMWEBJS_VB1_1;E=xml}', '@{R=APM.Strings;K=APMWEBJS_TM0_66;E=xml}');
            window.location.reload();
            return true;
        }
        return false;
    },

    OnPortTextBoxKeyDown: function (e) {
        var key = e.charCode || e.keyCode || 0;
        if (e.ctrlKey) {
            return (key == 65 //a - select all
                || key == 67 //c - copy
                || key == 88 //x - cut
                || key == 86 //v - paste
                || key == 90 //z - undo
                || key == 36 //home 
                || (key >= 37 && key <= 40) //arrows
                || key == 35 //end
                || key == 45) //insert - copy
        }
        if (e.shiftKey) {
            return (key == 36 //home
                || key == 35 //end
                || (key >= 37 && key <= 40) //arrows
                || key == 45 //insert - paste
                || key == 46) //delete
        }
        return (key == 8 //backspace
            || key == 9 //tab
            || key == 46 //delete
            || key == 36 //home 
            || key == 35 //end
            || (key >= 37 && key <= 40) //arrows
            || (key >= 48 && key <= 57)  //numbers
            || (key >= 96 && key <= 105)); //keypad numbers
    },

    OnOkPopup: function () {
        $(this.targetServerLabel).html(this.selectTargetNode_SelectedIpAddress + "&nbsp;");
        $(this.targetServerHiddenField).val(this.selectTargetNode_SelectedIpAddress);
        $(this.targetServerIdHiddenField).val(this.selectTargetNode_Id);
        if (this.selectTargetNode_SelectedIpAddress && this.selectTargetNode_SelectedIpAddress != "") {
            JmxWizard.JmxWizardService.GetStatusIconUrl(this.selectTargetNode_Id,
                Function.createDelegate(this, this.OnGetStatusIconUrlSuccess),
                Function.createDelegate(this, this.OnGetStatusIconUrlError));
        }
        this.OnUrlPartChange();
    },

    OnGetStatusIconUrlSuccess: function (result, context) {
        if (result && result != "") {
            $(this.targetServerStatusImg).attr("src", result);
            $(this.targetServerStatusImg).show();
        }
        else {
            $(this.targetServerStatusImg).attr("src", null);
            $(this.targetServerStatusImg).hide();
        }
    },

    OnGetStatusIconUrlError: function (error, context) {
        if (this.HandleCommonError(error, context)) {
            return;
        }
        $(this.targetServerStatusImg).attr("src", null);
        $(this.targetServerStatusImg).hide();
    },

    OnAfterNodeSelectionChange: function (nodeId, nodeCaption, nodeIpAddress) {
        this.selectTargetNode_SelectedIpAddress = nodeIpAddress;
        this.selectTargetNode_Id = nodeId;

        if (this.selectTargetNode_SelectedIpAddress.length > 0) {
            if (this.selectNodePopUpOkButton) {
                this.selectNodePopUpOkButton.disabled = false;
                $(this.selectNodePopUpOkButton).attr("class", "sw-btn-primary sw-btn sw-btn-enabled");
            }
        }
    },

    OnUrlPartChange: function () {
        var targetServerAddress = $(this.targetServerHiddenField).val();
        if (targetServerAddress && targetServerAddress != "") {
            var portNumber = $(this.portTextBox).val();
            var protocol = parseInt($(this.protocolDropDown).val());
            var urlPath = $(this.urlTextBox).val();
            var urlFormat = $(this.urlFormatTextBox).val();
            JmxWizard.JmxWizardService.GetTargetEndpointUrl(
            urlFormat,
            targetServerAddress, portNumber, protocol, urlPath,
            Function.createDelegate(this, this.OnGetTargetEndpointUrlSuccess),
            Function.createDelegate(this, this.OnGetTargetEndpointUrlError));
        }
        else {
            $(this.targetEndpointUrlElement).html("@{R=APM.Strings;K=APMWEBJS_TM0_65;E=xml}");
        }
    },

    OnGetTargetEndpointUrlSuccess: function (result, context) {
        if (result && result != "") {
            $(this.targetEndpointUrlElement).html(result);
        }
        else {
            $(this.targetEndpointUrlElement).html("");
        }
    },

    OnGetTargetEndpointUrlError: function (error, context) {
        if (this.HandleCommonError(error, context)) {
            return;
        }
        $(this.targetEndpointUrlElement).html("");
    },

    OnApplyUrl: function () {
        this.OnUrlPartChange();
        return false;
    },

    OnReaplyDefaultUrl: function () {
        $(this.urlFormatTextBox).val(this.defaultUrlFormat);
        this.OnUrlPartChange();
        return false;
    }
}
Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl.registerClass('Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
