﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Common.Models;
using System.Net;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_Controls_SelectJmxTargetControl : ScriptUserControlBase
{
    public CredentialSet CredentialSet
    {
        get { return selectCredentials.SelectedCredentialSet; }
        set {
            selectCredentials.DataSource = value;
            selectCredentials.DataBindNewCredentialSetForm();
        }
    }

    // There is need to public property from SelectCredentials
    public bool AreSelectedNoAuthenticationRequiredCredentials
    {
        get { return this.selectCredentials.SelectedCredentialSetId == ComponentBase.NoCredentialSetId; }
    }

    public void ReloadCredentialSets()
    {
        selectCredentials.ReloadCredentialSets();
    }

    public string IPAddress
    {
        get { return this.targetServerHiddenField.Value; }
        set { this.targetServerHiddenField.Value = value; }
    }

    public int NodeId
    {
        get
        {
            int id;
            int.TryParse(this.targetServerIdHiddenField.Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out id);
            return id;
        }
        set { this.targetServerIdHiddenField.Value = value.ToString(CultureInfo.InvariantCulture); }
    }

    public int PortNumber
    {
        get { return Int32.Parse(this.portTextBox.Text); }
        set { this.portTextBox.Text = value.ToString(); }
    }

    public JmxProtocol Protocol
    {
        get { return (JmxProtocol)Int32.Parse(this.protocolDropDown.SelectedValue); }
        set { this.protocolDropDown.SelectedValue = ((int) value).ToString(); }
    }

    public string UrlPath
    {
        get { return this.urlTextBox.Text; }
        set { this.urlTextBox.Text = value; }
    }

    public string UrlFormat
    {
        get { return this.urlFormatTextBox.Text; }
        set { this.urlFormatTextBox.Text = value; }
    }

    public bool Use64Bit
    {
        get { return selectPlatform.Use64Bit; }
        set { selectPlatform.Use64Bit = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            protocolDropDown.Items.Clear();
            var protocolValues = Enum.GetValues(typeof(JmxProtocol));
            foreach (var protocolValue in protocolValues)
            {
                var protocol = (JmxProtocol)protocolValue;
                protocolDropDown.Items.Add(new ListItem(EnumHelper.GetLocalizedDescription(protocol), ((int)protocol).ToString()));
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.SelectNodeTree.AfterNodeSelectionChangeScript = string.Format("Function.createDelegate($find(\"{0}\"), $find(\"{0}\").OnAfterNodeSelectionChange)", this.ClientID);
        this.SelectNodeModalPopupExtender.OnOkScript = string.Format("$find('{0}').OnOkPopup()", this.ClientID);
        ScriptManager.GetCurrent(this.Page).Services.Add(new ServiceReference("~/Orion/APM/Services/JmxWizardService.asmx"));

        if (this.NodeId != 0)
        {
            this.targetServerStatusImg.Src = StatusIconHelper.GetNodeStatusIconUrl(this.NodeId);
        }
        else
        {
            this.targetServerStatusImg.Src = null;
        }
    }

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor =
            new ScriptControlDescriptor("Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.SelectJmxTargetControl",
                                        this.ClientID);
        descriptor.AddElementProperty("selectNodePopUpOkButton", selectNodePopUpOkButton.ClientID);
        descriptor.AddElementProperty("targetServerLabel", targetServerLabel.ClientID);
        descriptor.AddElementProperty("targetServerStatusImg", targetServerStatusImg.ClientID);
        descriptor.AddElementProperty("targetServerHiddenField", targetServerHiddenField.ClientID);
        descriptor.AddElementProperty("targetServerIdHiddenField", targetServerIdHiddenField.ClientID);
        descriptor.AddElementProperty("portTextBox", portTextBox.ClientID);
        descriptor.AddElementProperty("protocolDropDown", protocolDropDown.ClientID);
        descriptor.AddElementProperty("urlTextBox", urlTextBox.ClientID);
        descriptor.AddElementProperty("targetEndpointUrlElement", targetEndpointUrlElement.ClientID);

        descriptor.AddElementProperty("urlFormatTextBox", urlFormatTextBox.ClientID);
        descriptor.AddElementProperty("btnApplyCustomizedURL", btnApplyCustomizedURL.ClientID);
        descriptor.AddElementProperty("btnReapplyDefaultURL", btnReapplyDefaultURL.ClientID);
        descriptor.AddProperty("defaultUrlFormat", JmxHelper.DefaultUrlFormat);

        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("SelectJmxTargetControl.js"));
    }

    public void Validate()
    {
        Page.Validate(selectCredentials.ValidationGroupName);
    }

    protected void ValidateTargetServer(Object source, ServerValidateEventArgs args)
    {
        args.IsValid = this.NodeId != 0;
    }
}
