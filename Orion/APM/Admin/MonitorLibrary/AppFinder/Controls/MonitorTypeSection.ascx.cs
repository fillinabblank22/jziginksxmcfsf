﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using System.Collections;

public partial class MonitorTypeSection : System.Web.UI.UserControl
{
    private ITemplate monitorTypeItems = null;

    [TemplateContainer(typeof(TemplateControl))]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate MonitorTypeItems
    {
        get
        {
            return monitorTypeItems;
        }
        set
        {
            monitorTypeItems = value;
        }
    }

    #region Properties

    public string Title
    {
        get;
        set;
    }

    public string Description
    {
        get;
        set;
    }

    public string ImageFilename
    {
        get;
        set;
    }

    public string ImageUrl
    {
        get
        {
            return string.Format("/Orion/APM/Images/Admin/ComponentMonitors/{0}", this.ImageFilename);
        }
    }

    #endregion

    #region Protected methods

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.phMonitorTypeItems.Controls.Clear();

        if (this.MonitorTypeItems == null)
        {
            this.phMonitorTypeItems.Controls.Add(new LiteralControl("No template defined"));
        }
        else
        {
            this.MonitorTypeItems.InstantiateIn(this.phMonitorTypeItems);
        }
    }

    #endregion

    #region Public methods

    /// <summary>
    /// Find selected child MonitorTypeItem user control
    /// </summary>
    /// <returns>MonitorTypeItem user control</returns>
    public MonitorTypeItem GetSelectedMonitorItem()
    {
        if (this.phMonitorTypeItems.Controls != null)
        {
            foreach (Control c in this.phMonitorTypeItems.Controls)
            {
                if ((c is MonitorTypeItem) && ((MonitorTypeItem)c).Selected.Equals(true))
                {
                    return c as MonitorTypeItem;
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Set selected child MonitorTypeItem user control
    /// </summary>
    /// <param name="monitorItemId">User control ID</param>
    public void SetSelectedMonitorItem(string monitorItemId)
    {
        if (this.phMonitorTypeItems.Controls != null)
        {
            foreach (Control c in this.phMonitorTypeItems.Controls)
            {
                if (c is MonitorTypeItem)
                {
                    if (((MonitorTypeItem)c).ID.Equals(monitorItemId))
                    {
                        ((MonitorTypeItem)c).Selected = true;
                    }
                    else
                    {
                        ((MonitorTypeItem)c).Selected = false;
                    }
                }
            }
        }
    }

    #endregion
}