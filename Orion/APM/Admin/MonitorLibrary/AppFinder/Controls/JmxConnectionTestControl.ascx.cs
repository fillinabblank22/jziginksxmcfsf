﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_Controls_JmxConnectionTestControl : ScriptUserControlBase, IPostBackEventHandler
{
    public event EventHandler ContinueWizard;

    public Control LoadMaskElement { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void InitializeConnectionTest()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "TestJmxConnection",
            "$(function(){setTimeout(function(){$find('" + this.ClientID + "').TestJmxConnection();});});",
            true);
    }

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor =
    new ScriptControlDescriptor("Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl",
                                this.ClientID);
        descriptor.AddProperty("emptyGuid", Guid.Empty);
        descriptor.AddElementProperty("loadMaskElement", LoadMaskElement.ClientID);
        descriptor.AddScriptProperty("invokeContinuePostback",
            "function(){" + this.Page.ClientScript.GetPostBackEventReference(this, "ContinueWizard") + ";}");

        yield return descriptor;

    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.4/ext-all-plus-jquery_adapter.js"));
        //yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.2.0/debug/jquery-bridge.js"));
        //yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.2.0/debug/ext.js"));
        //yield return new ScriptReference(this.ResolveUrl("~/Orion/js/extjs/3.2.0/debug/ext-all-debug.js"));
        yield return new ScriptReference(this.ResolveUrl("~/Orion/APM/js/ScriptServiceInvoker.js"));
        yield return new ScriptReference(this.ResolveUrl("JmxConnectionTestControl.js"));
    }

    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "ContinueWizard":
                OnContinueWizard();
                break;
        }
    }

    private void OnContinueWizard()
    {
        if (ContinueWizard != null)
        {
            ContinueWizard(this, new EventArgs());
        }
    }

}