﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference name="~/Orion/js/jquery/jquery-1.2.6-vsdoc.js"/>

Type.registerNamespace("Orion.APM.Admin.MonitorLibrary.AppFinder.Controls");

Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl = function (element) {
    Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl.initializeBase(this, [element]);

    this.emptyGuid = null;
    this.loadMaskElement = null;
    this.invokeContinuePostback = null;

    this.loadMask = null;
    this.scriptServiceInvoker = null;
}

Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl.prototype = {
    get_emptyGuid: function () {
        return this.emptyGuid;
    },
    set_emptyGuid: function (value) {
        this.emptyGuid = value;
    },

    get_loadMaskElement: function () {
        return this.loadMaskElement;
    },
    set_loadMaskElement: function (value) {
        this.loadMaskElement = value;
    },

    get_invokeContinuePostback: function () {
        return this.invokeContinuePostback;
    },
    set_invokeContinuePostback: function (value) {
        this.invokeContinuePostback = value;
    },

    initialize: function () {
        Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl.callBaseMethod(this, 'initialize');

        this.scriptServiceInvoker = $create(Orion.APM.js.ScriptServiceInvoker);
    },

    dispose: function () {
        // Add custom dispose actions here
        Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl.callBaseMethod(this, 'dispose');
    },

    HandleCommonError: function (error, context) {
        if (error.get_statusCode() == 401 || error.get_statusCode() == 403) {
            Ext.Msg.alert('@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}', '@{R=APM.Strings;K=APMWEBJS_TM0_66;E=js}');
            window.location.reload();
            return true;
        }
        return false;
    },

    TestJmxConnection: function () {
        this.ShowLoadMask();
        this.scriptServiceInvoker.callService(
            JmxWizard.JmxWizardService.TestJmxConnection,
            [this.emptyGuid],
            this,
            1000,
            this,
            Function.createDelegate(this, this.OnTestJmxConnectionSuccess),
            Function.createDelegate(this, this.OnTestJmxConnectionSuccess),
            Function.createDelegate(this, this.OnTestJmxConnectionError));

    },

    OnTestJmxConnectionSuccess: function (resultContainer, context) {
        //if used for various webmethods, taskId as first attribute expected !!!
        resultContainer.webMethodParams[0] = resultContainer.result.TaskId;

        if (resultContainer.result.IsNotFinished) {
            return;
        }
        this.HideLoadMask();

        var value = resultContainer.result.ResponseData;
        if (value) {
            this.ContinueWizard();
        }
        else {
            Ext.Msg.alert('@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}', '@{R=APM.Strings;K=APMWEBJS_TM0_67;E=js}');
        }
    },

    ContinueWizard: function () {
        setTimeout(Function.createDelegate(this, function () {
            this.invokeContinuePostback.call(this);
        }), 0);
    },

    OnTestJmxConnectionError: function (error, context) {
        this.HideLoadMask();
        if (this.HandleCommonError(error, context)) {
            return;
        }
        Ext.Msg.alert('@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}', error.get_message().replace('\n', '<br/>') + '<br/>' + '@{R=APM.Strings;K=APMWEBJS_TM0_68;E=js}');
    },

    ShowLoadMask: function () {
        if (!this.loadMask) {
            this.loadMask = new Ext.LoadMask(this.loadMaskElement, {
                msg: "@{R=APM.Strings;K=APMWEBJS_TM0_69;E=js}"
            });
        }
        this.loadMask.show();

    },

    HideLoadMask: function () {
        if (this.loadMask) {
            this.loadMask.hide();
        }
    }

}
Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl.registerClass('Orion.APM.Admin.MonitorLibrary.AppFinder.Controls.JmxConnectionTestControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
