﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectJmxTargetControl.ascx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_Controls_SelectJmxTargetControl" %>
<%@ Import Namespace="Resources" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="atk" %>
<%@ Register Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" TagPrefix="apm" TagName="SelectCredentials" %>
<%@ Register Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectPlatform.ascx" TagPrefix="apm" TagName="SelectPlatform" %>
<%@ Register Src="~/Orion/APM/Controls/SelectNodeTree.ascx" TagPrefix="apm" TagName="SelectNodeTree" %>

<table cellpadding="0" cellspacing="10">
    <tr>
        <td align="right"><%= APMWebContent.APMWEBDATA_TM0_73 %></td>
        <td>
            <img id="targetServerStatusImg" runat="server" />
            <asp:Label id="targetServerLabel" runat="server"></asp:Label>
            <orion:LocalizableButton runat="server" ID="browseForServer" LocalizedText="CustomText" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_118 %>" DisplayType="Small" CausesValidation="false" />
            <%--<asp:TextBox runat="stargetServerLiteralerver" ID="targetServerElement"></asp:TextBox>--%>
            <%--<asp:RequiredFieldValidator ID="TargetServerFieldValidator" runat="server" ControlToValidate="targetServerHiddenField"
                ErrorMessage="Please enter an IP Address for the 'Server IP Address' field">*</asp:RequiredFieldValidator>--%>
            <asp:CustomValidator ID="TargetServerNodeExistsValidator" runat="server" 
            ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_TM0_72 %>" OnServerValidate="ValidateTargetServer">*</asp:CustomValidator>
            <asp:HiddenField ID="targetServerHiddenField" runat="server" />
            <asp:HiddenField ID="targetServerIdHiddenField" runat="server" />
        </td>
    </tr>
    <tr>
        <td align="right"><%= APMWebContent.APMWEBDATA_TM0_74 %></td>
        <td><asp:TextBox ID="portTextBox" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="portRequiredFieldValidator" runat="server" ControlToValidate="portTextBox"
                ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_TM0_75 %>">*</asp:RequiredFieldValidator>
        <asp:RangeValidator ID="portCompareValidaor" runat="server"
        ControlToValidate="portTextBox" Type="Integer" MinimumValue="1" MaximumValue="65535"
        ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_TM0_76 %>">*</asp:RangeValidator>

        </td>
    </tr>
    <tr>
        <td align="right" style="vertical-align: text-top; padding-top: 3px;"><%= APMWebContent.APMWEBDATA_TM0_77 %></td>
        <td><asp:DropDownList id="protocolDropDown" runat="server"></asp:DropDownList><br />
        <span class="sw-text-helpful"><%= APMWebContent.APMWEBDATA_TM0_78 %></span>
        </td>
    </tr>
    <tr>
        <td align="right" style="vertical-align: text-top; padding-top: 2px;"><%= APMWebContent.APMWEBDATA_TM0_80 %></td>
        <td><asp:TextBox ID="urlTextBox" runat="server" CssClass="apm_jmx_longField"></asp:TextBox><br />
        <span class="sw-text-helpful"><%= APMWebContent.APMWEBDATA_TM0_79 %></span>
        </td>
    </tr>
    <tr>
        <td align="right"><%= APMWebContent.APMWEBDATA_TM0_81 %></td>
        <td>
        <span id="targetEndpointUrlElement" runat="server" class="apm_yellowBox"></span>
        </td>
    </tr>
    <tr>
        <td />
        <td>
        <asp:ImageButton id="AdvUrlCollapseButton" ImageUrl="~/Orion/images/Button.Expand.gif" runat="server" ImageAlign="left" CausesValidation="false" />
        <span style="padding-left:3px;"><%= APMWebContent.APMWEBDATA_TM0_82 %><br/></span>
        <asp:Panel ID="AdvUrlPanel" runat="server">
            <div style="padding-top: 10px; padding-left: 20px;">
            <asp:TextBox ID="urlFormatTextBox" runat="server" CssClass="apm_jmx_longField"></asp:TextBox><br />
            <span class="apm_helpfulText apm_noLeftPadding"><%= APMWebContent.APMWEBDATA_TM0_83 %></span><br />
                <div style="padding-top: 5px;">
                    <orion:LocalizableButton runat="server" ID="btnApplyCustomizedURL" LocalizedText="CustomText" 
                                             Text="<%$ Resources: APMWebContent, APMWEBDATA_TM0_84 %>" DisplayType="Small" CausesValidation="false" />

                    <orion:LocalizableButton runat="server" ID="btnReapplyDefaultURL" LocalizedText="CustomText" 
                                             Text="<%$ Resources: APMWebContent, APMWEBDATA_TM0_85 %>" DisplayType="Small" CausesValidation="false" />
                </div>
            </div>
        </asp:Panel>
        <atk:CollapsiblePanelExtender ID="cpe" runat="Server" 
            TargetControlID="AdvUrlPanel"
            ExpandControlID="AdvUrlCollapseButton" 
            CollapseControlID="AdvUrlCollapseButton" 
            Collapsed="True"
            ImageControlID="AdvUrlCollapseButton" 
            ExpandedImage="~/Orion/images/Button.Collapse.gif" 
            CollapsedImage="~/Orion/images/Button.Expand.gif"
            SuppressPostBack="true">
        </atk:CollapsiblePanelExtender>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <asp:ValidationSummary ID="PageValidationSummary" runat="server" />
        </td>
    </tr>
    <tr valign="top">               
        <th align="right">
            <asp:Label ID="credentialLabel" runat="server" Text="<%$ Resources: APMWebContent, APMWEBDATA_TM0_86 %>"></asp:Label>
        </th>
        <td>
            <div class="selectCreds" runat="server" id="SelectCredentialsDiv">
                <table>
                    <apm:SelectCredentials ID="selectCredentials" runat="server" ValidationGroupName="SelectCredentialsValidationGroup" AllowNoneCredential="true"/>                
                </table>
                <asp:ValidationSummary ID="CredentialsValidationSummary" runat="server"  ValidationGroup="SelectCredentialsValidationGroup"/>        
            </div>
        </td>
    </tr>
    <tr valign="top">               
        <th align="right">
            <%= APMWebContent.APMWEBDATA_TM0_87 %>
        </th>
        <td>
            <apm:SelectPlatform ID="selectPlatform" runat="server" />
        </td>
    </tr>
</table>

<atk:ModalPopupExtender ID="SelectNodeModalPopupExtender" PopupControlID="SelectNodeDiv"
    TargetControlID="browseForServer" runat="server" BackgroundCssClass="apm_modalBackground"
    BehaviorID="SelectNodeModalPopupDialog" 
    CancelControlID="SelectNodePopUpCancel"
    OkControlID="selectNodePopUpOkButton">
</atk:ModalPopupExtender>

<div runat="server" id="SelectNodeDiv"
         style="background-color: #FFF; border: solid 1px #333;z-index:110000; width:500px; height:400px; padding:20px;display:none;">
        <asp:UpdatePanel ID="_popupUpdatePanel" runat="server" UpdateMode="conditional">
            <ContentTemplate>                 
                <apm:SelectNodeTree ID="SelectNodeTree" runat="server" Visible="true" 
                                    TreeHeight="300px" TreeWidth="100%" 
                                    AllowMultipleSelections="false" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="selectNodePopUpOkButton" LocalizedText="CustomText" 
                                     Text="<%$ Resources: APMWebContent, APMWEBDATA_TM0_88 %>" 
                                     DisplayType="Primary" CausesValidation="false" Enabled="false" />
            <orion:LocalizableButton runat="server" ID="SelectNodePopUpCancel" LocalizedText="Cancel" 
                                     DisplayType="Secondary" CausesValidation="false" />
        </div>
        
</div>
<br />
