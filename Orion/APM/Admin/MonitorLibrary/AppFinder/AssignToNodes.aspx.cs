using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_AssignToNodes : SolarWinds.APM.Web.UI.AppFinderPageBase
{
	#region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
    {
        CheckIfValid();
        if (!IsPostBack)
        {
            LoadMonitorSelections(Workflow);

			imgbNext.AddEnterHandler(1);
        }
		if (Settings.BrowseMode == AppFinderBrowseMode.VmWarePerfCounter)
		{
			var filter = String.Format("n.Vendor = '{0}'", AppFinderCurrentSettings.VMWARE_VENDOR);
			if (Settings.VmWareEntityTypeEnum is VmWareESXEntityType)
			{
				filter = String.Format("{0} AND n.MachineType = '{1}'", filter, AppFinderCurrentSettings.VMWARE_ESX_MACHINE_TYPE);
			}
			else if (Settings.VmWareEntityTypeEnum is VmWareVCenterEntityType)
			{
				filter = String.Format("{0} AND n.MachineType = '{1}'", filter, AppFinderCurrentSettings.VMWARE_VCENTER_MACHINE_TYPE);
			}
			AssignToNodeStep.ViewNodeFilter = filter;
		}
		AssignToNodeStep.BrowseMode = Settings.BrowseMode;
	}

	protected void OnBack(object sender, EventArgs e)
	{
		GotoPreviousPage();
	}

	protected void OnNext(object sender, EventArgs e)
	{
		if (IsValid) 
		{
			GotoNextPage();
		}
	}

	protected void OnCancel(object sender, EventArgs e)
	{
		CancelWizard();
	}

	protected void OnValidatePage(object source, ServerValidateEventArgs args)
	{
        Workflow.SelectedNodeIds = AssignToNodeStep.GetSelectedNodeIds();
		if (Workflow.SelectedNodeIds == null || Workflow.SelectedNodeIds.Count < 1)
		{
			if (Workflow.AddToTemplateSettings.Mode == AddToTemplateMode.NewApplication)
			{
				args.IsValid = false;
			}
		}
	}

	#endregion

	#region Helper Members

	private void LoadMonitorSelections(AppFinderWorkflow workflow)
	{
		List<string> selections = new List<string>(workflow.MonitorsToAdd.Count);

		foreach (ComponentTemplate template in workflow.MonitorsToAdd)
		{
			selections.Add(template.Name);
		}

		AssignToNodeStep.MonitorSelectionList = selections;

		if (Workflow.CurrentSettings.TargetServer != null)
		{
		    Workflow.SelectedNodeIds = new List<int>() { Workflow.CurrentSettings.TargetServer.Id };
            AssignToNodeStep.NodeToSelectByDefault = string.Format(
                "{0}'{1}':'{2}'{3}", "{", Workflow.SelectedNodeIds[0], Workflow.CurrentSettings.TargetServer.IpAddress, "}"
            );
		}
	}
	
	#endregion
}
