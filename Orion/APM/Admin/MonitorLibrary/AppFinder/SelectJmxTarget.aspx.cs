﻿using System;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Common.Models;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_SelectJmxTarget : AppFinderPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckIfValid();
        if (!IsPostBack)
        {
            LoadFromWorkflow();

            imgbNext.AddEnterHandler(1);
        }
        this.jmxConnectionTestControl.LoadMaskElement = wizardElement;
        this.jmxConnectionTestControl.ContinueWizard += ContinueWizard;
    }


    private void LoadFromWorkflow()
    {
        if (Workflow.CurrentSettings.TargetServer != null)
        {
            selectJmxTargetControl.IPAddress = Workflow.CurrentSettings.TargetServer.IpAddress;
            selectJmxTargetControl.NodeId = Workflow.CurrentSettings.TargetServer.Id;
        }

        selectJmxTargetControl.CredentialSet = Workflow.CurrentSettings.CredentialSet;
        selectJmxTargetControl.Use64Bit = Workflow.CurrentSettings.Use64Bit;
        selectJmxTargetControl.PortNumber = Workflow.CurrentSettings.JmxPortNumber;
        selectJmxTargetControl.Protocol = Workflow.CurrentSettings.JmxProtocol;
        selectJmxTargetControl.UrlPath = Workflow.CurrentSettings.JmxUrlPath;
        selectJmxTargetControl.UrlFormat = Workflow.CurrentSettings.JmxUrlFormat;
    }

    private void SyncSelectedNode()
    {
        if (Workflow.CurrentSettings.TargetServer == null
            || Workflow.CurrentSettings.TargetServer.Id != selectJmxTargetControl.NodeId)
        {
            if (selectJmxTargetControl.NodeId == 0)
            {
                Workflow.CurrentSettings.TargetServer = null;
            }
            else
            {
                using (var bl = ServiceLocatorForWeb.GetService<IBusinessLayerFactory>().Create())
                {
                    Workflow.CurrentSettings.TargetServer = bl.GetNode(selectJmxTargetControl.NodeId);
                }
            }
        }
    }

    protected void OnBack(object sender, EventArgs e)
    {
        //FB580 
        try
        {
            Workflow.CurrentSettings.CredentialSet = selectJmxTargetControl.CredentialSet;
            Workflow.CurrentSettings.Use64Bit = selectJmxTargetControl.Use64Bit;
            this.SyncSelectedNode();
        }
        catch
        {
            //just ignore
        }

        GotoPreviousPage();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        selectJmxTargetControl.Validate();
        Page.Validate();

        if (IsValid)
        {

            Workflow.CurrentSettings.CredentialSet = selectJmxTargetControl.CredentialSet;
            Workflow.CurrentSettings.Use64Bit = selectJmxTargetControl.Use64Bit;

            if (!selectJmxTargetControl.AreSelectedNoAuthenticationRequiredCredentials)
            {
                Workflow.SaveCredentials();
                selectJmxTargetControl.ReloadCredentialSets();
                selectJmxTargetControl.CredentialSet = Workflow.CurrentSettings.CredentialSet;
            }

            Workflow.CurrentSettings.ClearVmWareEntitiesAndPerfCountersTables();
            Workflow.CurrentSettings.VmWareMachineType = null;

            this.SyncSelectedNode();

            Workflow.CurrentSettings.JmxPortNumber = selectJmxTargetControl.PortNumber;
            Workflow.CurrentSettings.JmxProtocol = selectJmxTargetControl.Protocol;
            Workflow.CurrentSettings.JmxUrlPath = selectJmxTargetControl.UrlPath;
            Workflow.CurrentSettings.JmxUrlFormat = selectJmxTargetControl.UrlFormat;

            jmxConnectionTestControl.InitializeConnectionTest();
        }
    }

    protected void ContinueWizard(object sender, EventArgs e)
    {
        GotoNextPage();
    }


    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }
}
