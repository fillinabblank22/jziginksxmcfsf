﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.UI.AppFinder;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_SelectJmxMBeans : AppFinderPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lock (Workflow.CurrentSettings)
        {
            if (Workflow.CurrentSettings.JmxSelectedEntities == null)
            {
                Workflow.CurrentSettings.JmxSelectedEntities = new List<JmxMonitorSettings>();
            }
        }
    }

    protected void OnBack(object sender, EventArgs e)
    {
        Workflow.CurrentSettings.JmxSelectedEntities.Clear();
        GotoPreviousPage();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        if (Workflow.CurrentSettings.JmxSelectedEntities.Count > 0)
        {
            Workflow.CurrentSettings.CreateMonitorsFromBrowserSelections();

            GotoNextPage();
        }
        else
        {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "NoJmxAttributesSelected",
            "$(function() { " + String.Format("Ext.Msg.alert('{0}', '{1}')", Resources.APMWebContent.APMWEBDATA_TM0_47, Resources.APMWebContent.APMWEBCODE_TM0_23) + ";});",
            true);
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }
}