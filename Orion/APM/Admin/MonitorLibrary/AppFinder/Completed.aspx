<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
    AutoEventWireup="true" CodeFile="Completed.aspx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_Completed" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_244%>" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <h1><%= Resources.APMWebContent.APMWEBDATA_VB1_245 %></h1>
    <div class="CompletedContent">
		<br />                
			<orion:LocalizableButton runat="server" ID="imgbViewExApps" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_246 %>" DisplayType="Primary" PostBackUrl="~/Orion/APM/Summary.aspx"/>
		<br />                
		<br />       
			<orion:LocalizableButton runat="server" ID="imgbEditExApps" DisplayType="Secondary"/>
		<br />                
		<br />       
		<a href="/Orion/APM/Admin/MonitorLibrary/AppFinder/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_247 %></a>
		<br />
		<a href="/Orion/APM/Admin/Applications/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_248 %></a>
		<br />
		<a href="/Orion/APM/Admin/ApplicationTemplates.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_249 %></a>
		<br />
    </div>   
</asp:Content>

