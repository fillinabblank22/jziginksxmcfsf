﻿using System;
using System.Linq;
using System.Web.UI;

using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_VmWareSummary : SolarWinds.APM.Web.UI.AppFinderPageBase
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (Workflow.CurrentSettings.TargetServer == null)
		{
			base.CancelWizard();
			return;
		}
		if (!IsPostBack)
		{
            InitControl();

			imgbNext.AddEnterHandler(1);
		}
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
		base.GotoPreviousPage();
	}

	protected void OnNext_Click(Object sender, EventArgs e)
	{
		if (base.IsValid)
		{
			Workflow.BeginCreateApplications();

			base.GotoNextPage();
		}
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		base.CancelWizard();
	}

	#endregion

	#region Helper Members

	private void InitControl()
	{
		var targets = new[] { Workflow.CurrentSettings.TargetServer.IpAddress };
		if (Workflow.SelectedNodeIds != null && Workflow.SelectedNodeIds.Count > 0)
		{
			const String QUERY = @"
SELECT Caption 
FROM Orion.Nodes
WHERE {0}
ORDER BY Caption
";
			var clause = Workflow.SelectedNodeIds
				.Select(item => String.Format("NodeID = {0}", item))
				.Aggregate((item1, item2) => String.Format("{0} OR {1}", item1, item2));

			targets = SwisProxy.ExecuteQuery(String.Format(QUERY, clause))
				.Select()
				.Select(item => item["Caption"].ToString())
				.ToArray();
		}

		vwSummaryControl.MonitorsToAdd = Workflow.MonitorsToAdd;
		vwSummaryControl.TemplateSettings = Workflow.AddToTemplateSettings;
		vwSummaryControl.Targets = targets.ToList();
	}

	#endregion
}