<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
    AutoEventWireup="true" CodeFile="Progress.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_Progress" %>

    
<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>        
            <br />
            <img src="/Orion/images/animated_loading_sm3_whbg.gif" alt=""/>
            <asp:Literal ID="Status" runat="server" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_254 %>" />
            <br />
            <br />
            <br />
            
            <asp:Timer ID="UpdateTimer" Enabled="true" Interval="1000" OnTick="OnUpdateTimerTick" runat="server"/>            
        </ContentTemplate>
    </asp:UpdatePanel>    
</asp:Content>

