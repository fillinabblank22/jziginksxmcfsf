﻿using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_SelectVmWareInstances : SolarWinds.APM.Web.UI.AppFinderPageBase
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			imgbNext.AddEnterHandler(1);
		}
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
		base.GotoPreviousPage();
	}

	protected void OnNext_Click(Object sender, EventArgs e)
	{
		var pairs = new System.Web.Script.Serialization.JavaScriptSerializer()
		    .Deserialize<List<string[]>>(Request.QueryStringOrForm("selEntities"));
		var info = new SolarWinds.APM.Web.UI.AppFinderSelCategoryInfo();
		if (pairs.Count > 0)
		{
			foreach (var pair in pairs)
			{
				info.AddInstance(pair[0], pair[1], pair[2], pair[3], pair[4]);
			}
		}

        if (Settings.VmWareComponentList != null)
        {
            Settings.VmWareComponentList.Clear();
        }
        Workflow.MonitorsToAdd.Clear();

	    Settings.VmWareSelectedItems = info;
        Settings.AddToComponentList(info);
        Workflow.CurrentSettings.CreateMonitorsFromBrowserSelections();

		base.GotoNextPage();
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		base.CancelWizard();
	}

	#endregion
}