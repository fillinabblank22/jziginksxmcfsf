﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_SelectVmWareCounters : SolarWinds.APM.Web.UI.AppFinderPageBase
{
	#region Properties

	protected String ErrorMessage 
	{
		get 
		{
			var message = String.Empty;
			if (Settings.LastBrowseError.IsValid())
			{
				message = Settings.LastBrowseError;
			}
			else 
			{
				if (ctrEntities.Items.Count == 0)
				{
					message = String.Format("Can not find any Entities for '{0}'.", Settings.VmWareEntityType);
				}
				else if (ctrPerfObjects.Items.Count == 0)
				{
					message = String.Format("Can't find '{0}' performance objects. Verify that '{0}' is running, and then restart this wizard.", Settings.VmWareEntity);
				}
			}
			return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(message);
		}
	}

	#endregion 

	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
            // If the selected target is vCenter then we want to display only 'A single system' option
            if (Settings.VmWareMachineType.Equals(SolarWinds.APM.Web.UI.AppFinderCurrentSettings.VMWARE_VCENTER_MACHINE_TYPE, StringComparison.InvariantCultureIgnoreCase))
            {
                Settings.VmWareInstanceType = VmWareWizardInstanceType.All;
                ctrInstanceTypeM.Enabled = false;
            }
            else
            {
                ctrInstanceTypeS.Attributes.Add("onChange", "$af.onPostBack();");
                ctrInstanceTypeM.Attributes.Add("onChange", "$af.onPostBack();");
            }

            ctrInstanceTypeS.Checked = Settings.VmWareInstanceType == VmWareWizardInstanceType.All;
            ctrInstanceTypeM.Checked = Settings.VmWareInstanceType == VmWareWizardInstanceType.Aggregate;

			ctrEntities.Visible = ctrInstanceTypeS.Checked;

			this.PopulateEntityTypes();
			if (this.PopulateEntities())
			{
				this.PopulatePerfObjects();
			}
			imgbNext.AddEnterHandler(1);
		}
	}

	protected void OnInstanceType_CheckedChanged(Object sender, EventArgs e)
	{
		ctrEntities.Visible = ctrInstanceTypeS.Checked;
		if (ctrInstanceTypeS.Checked)
		{
			Settings.VmWareInstanceType = VmWareWizardInstanceType.All;
			Settings.VmWareEntityType = VmWareVCenterEntityType.VirtualMachine.ToString();
		}
		else
		{
			Settings.VmWareInstanceType = VmWareWizardInstanceType.Aggregate;
			Settings.VmWareEntityType = VmWareVCenterEntityType.HostSystem.ToString();
		}
		Settings.ClearVmWareEntitiesAndPerfCountersTables();

		this.PopulateEntityTypes();
		if (this.PopulateEntities())
		{
			if (this.PopulatePerfObjects())
			{
				this.RegisterScript();
				return;
			}
		}
		this.RegisterErrorScript();
	}

	protected void OnVmWareEntityTypes_TextChanged(Object sender, EventArgs e)
	{
		if (ctrEntityTypes.SelectedValue.IsNotValid()) 
		{
			return;
		}
		Settings.VmWareEntityType = ctrEntityTypes.SelectedValue;
		Settings.ClearVmWareEntitiesAndPerfCountersTables();

		if (this.PopulateEntities())
		{
			if (this.PopulatePerfObjects())
			{
				this.RegisterScript();
				return;
			}
		}
		this.RegisterErrorScript();
	}

	protected void OnVmWareEntities_TextChanged(Object sender, EventArgs e) 
	{
		if (ctrEntities.SelectedValue.IsNotValid()) 
		{
			return;
		}
		Settings.VmWareEntity = ctrEntities.SelectedValue;
		Settings.ClearVmWarePerfGroupsCountersInstancesForEntity();

		if (this.PopulatePerfObjects())
		{
			this.RegisterScript();
			return;
		}
		this.RegisterErrorScript();
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
        //Cleanup of performance counters table
        Workflow.CurrentSettings.ClearPerformanceCountersTable();
        //Cleanup of processes or services table
        Workflow.CurrentSettings.ResetSelectedItems();
        //Cleanup of selected counter category
        Workflow.CurrentSettings.ClearPerformanceCategory();
        //Cleanup of VmWare entities and performance counters tables
        Workflow.CurrentSettings.ClearVmWareEntitiesAndPerfCountersTables();
        //Cleanup of VmWareMachineType property
        Workflow.CurrentSettings.VmWareMachineType = null;
        //Cleanup old monitors
        Workflow.MonitorsToAdd.Clear();

		base.GotoPreviousPage();
	}

	protected void OnNext_Click(Object sender, EventArgs e)
	{
		var pairs = new System.Web.Script.Serialization.JavaScriptSerializer()
		    .Deserialize<List<string[]>>(Request.QueryStringOrForm("selEntities"));
		var info = new SolarWinds.APM.Web.UI.AppFinderSelCategoryInfo();
		if (pairs.Count > 0)
		{
			foreach (var pair in pairs)
			{
				if (ctrInstanceTypeS.Checked)
				{
					info.AddCounter(pair[0], pair[1], pair[2], pair[3]);
				}
				else 
				{
					info.AddInstance(pair[0], pair[1], pair[2], pair[3], SolarWinds.APM.Web.UI.AppFinderCurrentSettings.EMPTY_VMWARE_INSTANCE);
				}
			}
		}
		Settings.VmWareSelectedItems = info;
        Settings.VmWareInstanceType = ctrInstanceTypeS.Checked ? VmWareWizardInstanceType.All : VmWareWizardInstanceType.Aggregate;
		Settings.VmWareEntityType = ctrEntityTypes.SelectedValue;
		Settings.VmWareEntity = ctrEntities.SelectedValue;
		Settings.VmWarePerfGroup = ctrPerfObjects.SelectedValue;

        if (Settings.VmWareInstanceType == VmWareWizardInstanceType.Aggregate)
        {
            if (Settings.VmWareComponentList != null)
            {
                Settings.VmWareComponentList.Clear();
            }
            Workflow.MonitorsToAdd.Clear();

            Settings.AddToComponentList(info);
            Settings.CreateMonitorsFromBrowserSelections();
        }

	    base.GotoNextPage();
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		base.CancelWizard();
	}

	#endregion

	#region Helper Members

	private void PopulateEntityTypes() 
	{
		ctrEntities.Items.Clear();
		ctrPerfObjects.Items.Clear();

		Func<ListItem, Boolean> filter = null;
		if (ctrInstanceTypeS.Checked)
		{
			filter = item => true;
		}
		else
		{
			filter = item => item.Value != VmWareVCenterEntityType.VirtualMachine.ToString();
		}
		ctrEntityTypes.DataSource = Settings.VmWareEntityTypesDataSource
			.Where(filter)
			.ToArray();
		ctrEntityTypes.DataBind();

		if (Settings.VmWareEntityType.IsNotValid())
		{
			Settings.VmWareEntityType = ctrEntityTypes.SelectedValue;
			Settings.ClearVmWareEntitiesAndPerfCountersTables();
		}
		HtmlHelper.SetListSelectedValue(ctrEntityTypes, Settings.VmWareEntityType, null);
	}

	private Boolean PopulateEntities()
	{
		ctrEntities.Items.Clear();
		ctrPerfObjects.Items.Clear();

		if (Settings.VmWareEntityType.IsNotValid())
		{
			return false;
		}
		var data = Settings.VmWareEntitiesTable;
		if (data == null || data.Rows.Count == 0) 
		{
			return false;
		}
		if (!ctrEntities.Visible) 
		{
			var row = data
				.Select(null, "Name ASC")
				.FirstOrDefault();
			if (row != null) 
			{
				Settings.VmWareEntity = row["Name"].ToString();
			}
		}

        if (Settings.VmWareEntityType.Equals("VirtualMachine", StringComparison.InvariantCultureIgnoreCase))
        {
            var upPowerState = data.Select("VirtualMachinePowerState like 'PoweredOn'", "Name ASC").
                Select(item =>
                {
                    var listItem = new ListItem(String.Format("{0} - VM's power state is 'On'", item["Name"].ToString()), item["Name"].ToString());
                    listItem.Attributes["class"] = "VirtualMachinePowerStateUp";
                    return listItem;
                });

            var downPowerState = data.Select("VirtualMachinePowerState not like 'PoweredOn'", "Name ASC").
                Select(item =>
                {
                    var listItem = new ListItem(String.Format("{0} - VM's power state is 'Off' or 'Suspended'", item["Name"].ToString()), item["Name"].ToString());
                    listItem.Attributes["class"] = "VirtualMachinePowerStateDown";
                    return listItem;
                });

            upPowerState = upPowerState.Concat(downPowerState);

            ctrEntities.Items.AddRange(upPowerState.ToArray());
        }
        else
        {
            ctrEntities.Items.AddRange(data
                .Select(null, "Name ASC")
                .Select(item => new ListItem(item["Name"].ToString(), item["Name"].ToString()))
                .ToArray());
        }

		if (Settings.VmWareEntity.IsNotValid())
		{
			Settings.VmWareEntity = ctrEntities.SelectedValue;
			Settings.ClearVmWarePerfGroupsCountersInstancesForEntity();
		}
		HtmlHelper.SetListSelectedValue(ctrEntities, Settings.VmWareEntity, null);
		
		return true;
	}

	private Boolean PopulatePerfObjects() 
	{
		ctrPerfObjects.Items.Clear();
		
		if (Settings.VmWareEntity.IsNotValid())
		{
			return false;
		}
		var data = Settings.VmWarePerfGroupsForEntityTable;
		if (data == null || data.Rows.Count == 0)
		{
			return false;
		}

		ctrPerfObjects.DataSource = data
			.Select(null, "Name ASC")
			.Select(item => new ListItem(item["Name"].ToString(), item["Name"].ToString()))
			.ToArray();
		ctrPerfObjects.DataBind();

		if (Settings.VmWarePerfGroup.IsNotValid())
		{
			Settings.VmWarePerfGroup = ctrPerfObjects.SelectedValue;
			Settings.ClearVmWarePerfCountersForGroup();
		}
		HtmlHelper.SetListSelectedValue(ctrPerfObjects, Settings.VmWarePerfGroup, null);
		
		return true;
	}

	private void RegisterScript() 
	{
		ScriptManager.RegisterClientScriptBlock(
			ctrUP, typeof(UpdatePanel), "KEY_ReloadPerfObj", "$af.reloadCounters();", true
		);
	}

	private void RegisterErrorScript()
	{
		ScriptManager.RegisterClientScriptBlock(
			ctrUP, typeof(UpdatePanel), "KEY_Error", String.Format("$af.showError({0});", this.ErrorMessage), true
		);
	}

	#endregion
}
