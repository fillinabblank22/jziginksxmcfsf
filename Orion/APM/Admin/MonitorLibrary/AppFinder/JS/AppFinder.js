﻿Ext.ns("SW.APM.AppFinder");
SW.APM.AppFinder.ItemsToMonitor = function () {
	var mURL = "/Orion/APM/Services/AppFinderSelectItemsGrid.aspx", mTIMEOUT = (20 * 60 * 1000);
	var mConfig = null;
	var mGrid = null;
	var mSelUIDs = [];

	/*helper members*/
	function call(pAct, pCallback) {
		$.ajax({
			type: "GET", url: mURL, data: ("action=" + pAct),
			contentType: "application/json; charset=utf-8", dataType: "json", success: function (pResp) { if (pCallback) { pCallback(pResp); } }
		});
	}

	function setTitle() {
		var text = "@{R=APM.Strings;K=APMWEBJS_TM0_49;E=js}";
		if (mConfig.mode == "PerformanceCounter") {
			text = "@{R=APM.Strings;K=APMWEBJS_TM0_50;E=js}";
		} else if (mConfig.mode == "ServiceMonitor") {
			text = "@{R=APM.Strings;K=APMWEBJS_TM0_51;E=js}";
		}
		mGrid.getTopToolbar().items.get(0).setText(String.format(text, mSelUIDs.length));
	}
	function showError(errorMsg) {
		Ext.Msg.show({ title: "@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}", msg: errorMsg, icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });
	}

	/*event handlers*/
	function onPOSelect(sender, rec, index) {
		var store = mGrid.getStore();
		store.removeAll(); mSelUIDs = []; setTitle();
		store.load({ params: { action: "getPCData", start: 0, limit: 25, name: rec.data.RealName} });
	}
	function onPODataLoaded(obj) {
		if (obj.error) {
			showError(obj.error);
			return false;
		}
		Ext.getCmp("cbPO").store.loadData(obj.data);
		return true;
	}

	function onVmWarePOSelect(sender, rec, index) {
		var store = mGrid.getStore();
		store.removeAll();

		mSelUIDs = [];
		setTitle();

		store.load({ params: { action: "getVmWarePCData", start: 0, limit: 25, name: rec.data.RealName} });
	}
	function onVmWarePODataLoaded(sender, response, options) {
		var obj = Ext.decode(response.responseText);
		if (obj.error) {
			var loadMask = mGrid.loadMask;
			if (loadMask) { loadMask.hide(); }
			showError(obj.error);
		} else {
			Ext.getCmp("cbVmWarePO").store.loadData(obj.data);
		}
	}

	function onSelectionChange(sender) {
		$("#selEntities").val(mSelUIDs.join(","));
	}
	function onRowSelect(sender, index, record) {
		var uID = record.data["uID"];
		if (mSelUIDs.indexOf(uID) < 0) {
			mSelUIDs.push(uID); setTitle();
		}
	}
	function onRowDeselect(sender, index, record) {
		mSelUIDs.remove(record.data["uID"]); setTitle();
	}
	function onLoadComplete(sender, records, options) {
		if (onLoadException(null, null, null, sender, null)) {
			return;
		}
		var selRecords = [];
		for (var i = 0; i < records.length; i++) {
			if (mSelUIDs.indexOf(records[i].data["uID"]) > -1) {
				selRecords.push(records[i]);
			}
		}
		mGrid.getSelectionModel().selectRecords(selRecords);
	}
	function onLoadException(misc, txtType, txtMethod, store, response) {
		var errMsg = store.reader.jsonData.error;
		if (errMsg) {
			showError(errMsg); return true;
		}
		return false;
	}

	/*grid configuration members*/
	function getConfig(type, selModel) {
		if (type == "ProcessMonitorWmi" || type == "ProcessMonitorSnmp") {
			return {
				mode: type,
				gridCM: new Ext.grid.ColumnModel([selModel, { header: "@{R=APM.Strings;K=APMWEBJS_TM0_59;E=js}", dataIndex: "Name", sortable: true }, { header: "@{R=APM.Strings;K=APMWEBJS_TM0_62;E=js}", width: 40, dataIndex: "InstanceCount", sortable: true}]),
				storeCM: ["uID", "Name", "InstanceCount"],
				storeBP: { action: "getPMSMData" }
			};
		}
		if (type == "ServiceMonitor") {
			function renderWithIcon(value) {
				if (value == "Stopped" || value == "Paused" || value == "Running") {
					return '<img src="/Orion/APM/Images/StatusIcons/ServiceStatus_' + value + '_16x16.gif"> ' + value;
				}
				return value;
			}
			return {
				mode: type,
				gridCM: new Ext.grid.ColumnModel([selModel, { header: "@{R=APM.Strings;K=APMWEBJS_TM0_60;E=js}", dataIndex: "Name", sortable: true }, { header: "@{R=APM.Strings;K=APMWEBJS_TM0_63;E=js}", width: 150, dataIndex: "DisplayName", sortable: true }, { header: "@{R=APM.Strings;K=APMWEBJS_TM0_26;E=js}", width: 40, renderer: renderWithIcon, dataIndex: "Status", sortable: true}]),
				storeCM: ["uID", "Name", "DisplayName", "Status"],
				storeBP: { action: "getPMSMData" }
			};
		}
		if (type == "PerformanceCounter") {
			return {
				mode: type,
				gridCM: new Ext.grid.ColumnModel([selModel, { header: "@{R=APM.Strings;K=APMWEBJS_TM0_61;E=js}", dataIndex: "AdditionalInfo", sortable: true }, { header: "@{R=APM.Strings;K=APMWEBJS_TM0_64;E=js}", width: 70, dataIndex: "Name", sortable: true}]),
				storeCM: ["uID", "AdditionalInfo", "Name"],
				storeBP: { action: "getPCData" }
			};
		}
		return null;
	}
	function configure(pConfig) {
		var selModel = new Ext.grid.CheckboxSelectionModel({
			listeners: {
				rowselect: onRowSelect,
				rowdeselect: onRowDeselect,
				selectionchange: onSelectionChange
			}
		});
		mConfig = getConfig(pConfig.data.mode, selModel);
		var gStore = new Ext.data.JsonStore({
			root: "data", totalProperty: "total", /*error: "error", */baseParams: mConfig.storeBP, fields: mConfig.storeCM,
			proxy: new Ext.data.HttpProxy({ url: mURL, method: "POST", timeout: mTIMEOUT }),
			remoteSort: true, sortInfo: { field: "Name", direction: "ASC" },
			listeners: { load: onLoadComplete, exception: onLoadException }
		});
		var gConfig = {
			renderTo: "itmGrid", width: 750, height: 400, border: false, frame: true, autoScroll: true, trackMouseOver: false, disableSelection: true, stripeRows: true,
			loadMask: true, maskDisabled: false, viewConfig: { forceFit: true, enableRowBody: true },
			store: gStore, cm: mConfig.gridCM,
			sm: selModel,
			bbar: new Ext.PagingToolbar({
				store: gStore, pageSize: 25, displayInfo: true, displayMsg: "@{R=APM.Strings;K=APMWEBJS_TM0_52;E=js}", emptyMsg: "@{R=APM.Strings;K=APMWEBJS_TM0_53;E=js}",
				beforePageText: "@{R=APM.Strings;K=APMWEBJS_TM0_34;E=js}",
				afterPageText: "@{R=APM.Strings;K=APMWEBJS_TM0_35;E=js}"
			}),
			tbar: function () {
				return [{ xtype: "label", text: "", style: "font-weight:bold;"}];
			} ()
		};

		mGrid = new Ext.grid.GridPanel(gConfig);
		mGrid.loadMask.msg = "@{R=APM.Strings;K=APMWEBJS_TM0_54;E=js}";

		if (mConfig.mode == "PerformanceCounter") {
			var perfCategoryCombo = new Ext.form.ComboBox({
				renderTo: "itmPerfCategoryCombo",
				id: "cbPO", valueField: "RealName", displayField: 'Name', typeAhead: false, mode: 'local', triggerAction: 'all', selectOnFocus: false, width: 750, editable: false, emptyText: "",
				store: new Ext.data.SimpleStore({ fields: ["Name", "RealName"], data: [] }),
				listeners: { select: onPOSelect }
			});

			mGrid.loadMask.msg = "@{R=APM.Strings;K=APMWEBJS_TM0_55;E=js}"; mGrid.loadMask.show();

			call("getPOData", function (response) {
				if (onPODataLoaded(response)) {
					perfCategoryCombo.setValue("Processor");
					var store = mGrid.getStore();
					store.removeAll();
					mSelUIDs = [];
					setTitle();
					store.load({ params: { action: "getPCData", start: 0, limit: 25, name: "Processor"} });
				}
				mGrid.loadMask.hide();
				mGrid.loadMask.msg = "@{R=APM.Strings;K=APMWEBJS_TM0_54;E=js}";
			});
		} else {
			mGrid.getStore().reload();
		}
		setTitle();
	}

	function ctor() {
		call("getConfig", function (response) { configure(response); });
	} ctor();

	/*public members*/
	this.onNextClick = function () {
		if (mSelUIDs.length < 1) {
			showError(mConfig.mode == "PerformanceCounter" ? "@{R=APM.Strings;K=APMWEBJS_TM0_56;E=js}" : (mConfig.mode == "ServiceMonitor" ? "@{R=APM.Strings;K=APMWEBJS_TM0_57;E=js}" : "@{R=APM.Strings;K=APMWEBJS_TM0_58;E=js}"));
		}
		return mSelUIDs.length > 0;
	}
}