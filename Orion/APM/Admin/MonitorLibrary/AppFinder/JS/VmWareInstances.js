﻿Ext.ns("SW.APM.AppFinder");

SW.APM.AppFinder.VmWareInstance = function () {

	var IMG_DEL = "<img style='cursor:pointer;' src='/Orion/Discovery/images/icon_delete.gif'>";
	var mURL = "/Orion/APM/Services/AppFinderSelectItemsGrid.aspx", mTIMEOUT = (20 * 60 * 1000), mLimit = 100000000;
	var mCategoryCombo = null, mCounterGrid = null, mInstanceGrid = null, mSelItemsGrid = null;
	var mSelItems = [];

	/*helper members*/
	function call(pAct, pCallback) {
		$.ajax({
			type: "GET", url: mURL, data: ("action=" + pAct),
			contentType: "application/json; charset=utf-8", dataType: "json", success: function (pResp) { if (pCallback) { pCallback(pResp); } }
		});
	}

	function setTitle() {
		mInstanceGrid.setTitle(String.format("@{R=APM.Strings;K=APMWEBJS_VB1_151;E=js}", mSelItems.length));
	}
	function manageChecker() {
		var rC = mInstanceGrid.getStore().getCount(), sC = mInstanceGrid.getSelectionModel().getCount();

		var el = $("div[class^='x-grid3-hd-inner x-grid3-hd-checker']", mInstanceGrid.body.dom), css = "x-grid3-hd-checker-on";
		if ((el.attr("class").indexOf(css) > 0 && (rC == 0 || rC != sC)) || (el.attr("class").indexOf(css) == -1 && (rC > 0 && rC == sC))) {
			el.toggleClass(css);
		}
	}
	function showError(pError) {
		Ext.Msg.show({ title: "@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}", msg: pError, icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });
	}

	function getSelInfo(pRec) {
		var rec = mCounterGrid.getSelectionModel().getSelected();
		return { Category: rec.get("Category"), DisplayName: rec.get("DisplayName"), RealName: rec.get("RealName"), RollupType: rec.get("RollupType"), Instance: pRec.get("Instance"), Action: true };
	}
	function getSelIndex(pObj) {
		if (pObj.get) { pObj = getSelInfo(pObj); }
		for (var i = 0; i < mSelItems.length; i++) {
			if (mSelItems[i].Category == pObj.Category && mSelItems[i].DisplayName == pObj.DisplayName && mSelItems[i].Instance == pObj.Instance) {
				return i;
			}
		}
		return -1;
	}

	function loadCategories(pResp) {
		if (pResp.error) { showError(pResp.error); return; }

		var data = [["All Counters", '@{R=APM.Strings;K=APMWEBJS_VB1_157;E=js}']];
		for (var i = 0; i < pResp.data.length; i++) { data.push([pResp.data[i], pResp.data[i]]); }

		mCategoryCombo.getStore().loadData(data);
		mCategoryCombo.setValue(data[0][0]);
		mCategoryCombo.enable();

		onCategorySelect(mCategoryCombo, mCategoryCombo.getStore().getAt(0), 0)
	}

	/*event handlers*/
	function onCategorySelect(pSender, pRec, pIndex) {
		mCounterGrid.getStore().reload({ params: { action: "getVmWareSelCnt", start: 0, limit: mLimit, category: mCategoryCombo.getValue()} })
		mInstanceGrid.getStore().removeAll();

		manageChecker();
		return false;
	}
	function onCounterSelect(pSelModelthis, pIndex, pRec) {
		mInstanceGrid.getStore().removeAll();
		mInstanceGrid.getStore().load({ params: { action: "getVmWarePIData", start: 0, limit: mLimit, category: pRec.get("Category"), counter: pRec.get("DisplayName")} });

		manageChecker();
		return false;
	}

	function onInstanceLoaded(pSender, pRecs, pOpt) {
		if (pSender.reader.jsonData.error) { showError(pSender.reader.jsonData.error); return; }
		var selRecs = [], store = mInstanceGrid.getStore();
		for (var i = 0, rec = store.getAt(i); i < store.getCount(); rec = store.getAt(++i)) {
			if (getSelIndex(rec) > -1) {
				selRecs.push(rec);
			}
		}
		mInstanceGrid.getSelectionModel().selectRecords(selRecs);

		manageChecker();
		return true;
	}

	function onInstanceSelectionChange(sender) {
		var res = [];
		if (mSelItems.length > 0) {
			for (var i = 0, item = mSelItems[i]; i < mSelItems.length; item = mSelItems[++i]) {
				res.push([item.Category, item.DisplayName, item.RealName, item.RollupType, item.Instance]);
			}
		}
		$("#selEntities").val(Ext.encode(res));
		return false;
	}
	function onInstanceSelect(pSender, pIndex, pRec) {
		var info = getSelInfo(pRec);
		if (getSelIndex(info) != -1) { return false; }

		mSelItems.push(info);

		var Rec = Ext.data.Record.create([{ name: "Category" }, { name: "DisplayName" }, { name: "RealName" }, { name: "RollupType" }, { name: "Instance" }, { name: "Action"}]);
		mSelItemsGrid.getStore().add(new Rec(info));

		setTitle(); manageChecker();
		return false;
	}
	function onInstanceDeselect(pSender, pIndex, pRec) {
		var info = getSelInfo(pRec), index = getSelIndex(info);
		if (index == -1) { return false; }

		mSelItems.splice(index, 1);

		index = mSelItemsGrid.getStore().findBy(function (iRec) { return iRec.get("Category") == info.Category && iRec.get("DisplayName") == info.DisplayName && iRec.get("Instance") == info.Instance });
		if (index > -1) {
			mSelItemsGrid.getStore().removeAt(index);
		}
		setTitle(); manageChecker();
		return false;
	}
	function onSelItemDelete(pSender, pRow, pColumn) {
		if (pColumn != 2) { return false; }

		var store = mSelItemsGrid.getStore(), rec = store.getAt(pRow);
		store.removeAt(pRow);

		var info = { Category: rec.get("Category"), DisplayName: rec.get("DisplayName"), Instance: rec.get("Instance") }, index = getSelIndex(info);
		if (index == -1) { return false; }
		mSelItems.splice(index, 1);

		var selInfo = getSelInfo(rec);
		if (info.Category == selInfo.Category && info.DisplayName == selInfo.DisplayName) {
			index = mInstanceGrid.getStore().find("Instance", info.Instance);
			if (index > -1) {
				mInstanceGrid.getSelectionModel().deselectRow(index);
				manageChecker();
			}
		}
		setTitle();
		return false;
	}

	/*configure ui*/
	function configure(pConfig) {
		/*init performance category combo box*/
		var catConfig = {
		    store: new Ext.data.ArrayStore({ fields: ["Category", "CategoryDisplay"], data: [] }),
			valueField: "Category", displayField: "CategoryDisplay", mode: "local", triggerAction: "all", width: 250, editable: false, disabled: true,
			listeners: { select: onCategorySelect }
		};
		/*init performance counters grid*/
		var cntConfig = {
			height: 430, border: true, frame: true, autoScroll: true, trackMouseOver: false, stripeRows: true, loadMask: true, maskDisabled: false, hideHeaders: true,
			viewConfig: { forceFit: true, enableRowBody: true },
			store: new Ext.data.JsonStore({
				root: "data", totalProperty: "total", error: "error", baseParams: { action: "getVmWareSelCnt" }, fields: ["Category", "DisplayName", "RealName", "RollupType"],
				proxy: new Ext.data.HttpProxy({ url: mURL, method: "POST", timeout: mTIMEOUT })
			}),
			cm: new Ext.grid.ColumnModel([{ dataIndex: "DisplayName", renderer: function (pVal, pMD, pRec) { if (mCategoryCombo.getValue() == "All Counters") { return String.format("@{R=APM.Strings;K=APMWEBJS_VB1_150;E=js}", pRec.get("Category"), pVal); } return pVal; } }]),
			sm: new Ext.grid.RowSelectionModel({ singleSelect: true, listeners: { rowselect: onCounterSelect} })
		};

		/*init instance grid*/
		var instStore = new Ext.data.JsonStore({
			root: "data", totalProperty: "total", error: "error",
			baseParams: { action: "getVmWarePIData" }, fields: ["Instance"], remoteSort: true, sortInfo: { field: "Name", direction: "ASC" },
			proxy: new Ext.data.HttpProxy({ url: mURL, method: "POST", timeout: mTIMEOUT }),
			listeners: { load: onInstanceLoaded }
		});

		var sm1 = new Ext.grid.CheckboxSelectionModel({ listeners: { rowselect: onInstanceSelect, rowdeselect: onInstanceDeselect, selectionchange: onInstanceSelectionChange } });
		var instConfig = {
			title: "@{R=APM.Strings;K=APMWEBJS_AK1_13;E=js}", height: 488, border: true, frame: true, autoScroll: true, trackMouseOver: false, disableSelection: true, stripeRows: true, loadMask: true, maskDisabled: false,
			viewConfig: { forceFit: true, enableRowBody: true },
			store: instStore,
			cm: new Ext.grid.ColumnModel([sm1, { header: "@{R=APM.Strings;K=APMWEBJS_VB1_152;E=js}", dataIndex: "Instance", sortable: true, menuDisabled: true}]),
			sm: sm1
		};

		/*init selected items grid*/
		var siConfig = {
			height: 350, title: "@{R=APM.Strings;K=APMWEBJS_VB1_146;E=js}", border: false, frame: true, autoScroll: true, trackMouseOver: false, disableSelection: true, stripeRows: false,
			viewConfig: { forceFit: true, enableRowBody: true },
			store: new Ext.data.SimpleStore({ fields: ["Category", "DisplayName", "RealName", "RollupType", "Instance", "Action"], data: [] }),
			listeners: { cellclick: onSelItemDelete },
			cm: new Ext.grid.ColumnModel([
				{ width: 170, header: "@{R=APM.Strings;K=APMWEBJS_TM0_64;E=js}", dataIndex: "DisplayName", menuDisabled: true },
				{ width: 130, header: "@{R=APM.Strings;K=APMWEBJS_TM0_61;E=js}", dataIndex: "Instance", menuDisabled: true },
				{ width: 35, dataIndex: "Action", hideable: false, menuDisabled: true, renderer: function () { return IMG_DEL; } }
			])
		};
		/*populate ui to page*/
		var container = new Ext.Panel({
			renderTo: "ctrContainer", layout: "border", height: 500, width: 1000, hideBorders: true, border: false, bodyBorder: false, bodyStyle: "background-color: transparent;",
			items: [
				{
					region: "west", margins: "5 0 0 0", width: 300, items: [
						{ xtype: "label", text: "@{R=APM.Strings;K=APMWEBJS_VB1_156;E=js}" }, (mCategoryCombo = new Ext.form.ComboBox(catConfig)),
						{ xtype: "label", html: "<hr/>" }, (mCounterGrid = new Ext.grid.GridPanel(cntConfig))
					]
				}, {
					region: "center", margins: "0 5 0 5", collapsible: false, items: [(mInstanceGrid = new Ext.grid.GridPanel(instConfig))]
				}, {
					region: "east", width: 350, height: 240, margins: "0 5 0 0", items: [(mSelItemsGrid = new Ext.grid.GridPanel(siConfig))]
				}
			]
		});

		mCounterGrid.loadMask.msg = "@{R=APM.Strings;K=APMWEBJS_VB1_153;E=js}";
		mInstanceGrid.loadMask.msg = "@{R=APM.Strings;K=APMWEBJS_VB1_154;E=js}";

		setTitle();

		call("getVmWareSelCat", loadCategories);
	} configure({});

	/*public members*/
	this.onNextClick = function () {
		if (mSelItems.length < 1) {
			showError("@{R=APM.Strings;K=APMWEBJS_VB1_155;E=js}");
		}
		return mSelItems.length > 0;
	}
};
