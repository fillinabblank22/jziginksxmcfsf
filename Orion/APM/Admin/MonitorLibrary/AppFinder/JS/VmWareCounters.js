﻿Ext.ns("SW.APM.AppFinder");

SW.APM.AppFinder.VmWareCounter = function () {

	var KEY_LIMIT = "AppFinder_VmWareCounters_Limit", IMG_DEL = "<img style='cursor:pointer;' src='/Orion/Discovery/images/icon_delete.gif'>";
	var mURL = "/Orion/APM/Services/AppFinderSelectItemsGrid.aspx", mTIMEOUT = (20 * 60 * 1000), mLimit = 25;
	var mLoadCtr = null, mMainPanel = null, mCounterGrid = null, mSelItemsGrid = null;
	var mSelItems = [];

	/*helper members*/
	function call(pAct, pCallback) {
		$.ajax({ type: "GET", url: mURL, data: ("action=" + pAct), contentType: "application/json; charset=utf-8", dataType: "json", success: function (pResp) { if (pCallback) { pCallback(pResp); } } });
	}

	function setTitle() {
		mCounterGrid.setTitle(String.format("@{R=APM.Strings;K=APMWEBJS_VB1_145;E=js}", mSelItems.length));
	}
	function manageChecker() {
		var store = mCounterGrid.getStore(), rC = store.getCount(), sC = 0;
		for (var i = 0; i < rC; i++) {
			sC += (getSelIndex(store.getAt(i)) > -1 ? 1 : 0);
		}
		var el = $("div[class^='x-grid3-hd-inner x-grid3-hd-checker']", mCounterGrid.body.dom), css = "x-grid3-hd-checker-on";
		if ((el.attr("class").indexOf(css) > 0 && rC != sC) || (el.attr("class").indexOf(css) == -1 && rC > 0 && rC == sC)) {
			el.toggleClass(css);
		}
	}
	function showError(pMsg) {
		mLoadCtr.hide();
		if (pMsg && pMsg.trim().length > 0) {
			Ext.Msg.show({ title: "@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}", msg: pMsg, icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });
		}
	}

	function getSelInfo(pRec, pCategory) {
		return { Category: pCategory || $("select[id$='ctrPerfObjects']").val(), DisplayName: pRec.get("DisplayName"), RealName: pRec.get("RealName"), RollupType: pRec.get("RollupType") };
	}
	function getSelIndex(pObj) {
		if (pObj.get) { pObj = getSelInfo(pObj); }
		for (var i = 0; i < mSelItems.length; i++) {
			if (mSelItems[i].Category == pObj.Category && mSelItems[i].DisplayName == pObj.DisplayName) {
				return i;
			}
		}
		return -1;
	}

	function clearCounters() {
		mSelItems = [];
		mCounterGrid.getStore().removeAll();
		mSelItemsGrid.getStore().removeAll();

		manageChecker();
	}
	function reloadCounters() {
		mCounterGrid.getStore().removeAll();

		var val = $("select[id$='ctrPerfObjects']").val();
		if (val && val.trim().length > 0) {
			mCounterGrid.getStore().load({ params: { action: "getVmWarePCData", start: 0, limit: mLimit, category: val} });
		}
		mMainPanel.doLayout(); manageChecker();
	}

	/*event handlers*/
	function onCounterLoaded(pSender, pRecs, pOptions) {
		if (pSender.reader.jsonData.error) { showError(pSender.reader.jsonData.error); return; }

		var selRecords = [], store = mCounterGrid.getStore();
		for (var i = 0, count = store.getCount(), rec = store.getAt(i); i < count; rec = store.getAt(++i)) {
			if (getSelIndex(rec) > -1) { selRecords.push(rec); }
		}
		mCounterGrid.getSelectionModel().selectRecords(selRecords);

		setTitle(); manageChecker();
		return true;
	}

	function onCounterSelectionChange(pSender) {
		var res = [];
		if (mSelItems.length > 0) {
			for (var i = 0, item = mSelItems[i]; i < mSelItems.length; item = mSelItems[++i]) {
				res.push([item.Category, item.DisplayName, item.RealName, item.RollupType]);
			}
		}
		$("#selEntities").val(Ext.encode(res));
		return false;
	}
	function onCounterSelect(pSender, pIndex, pRec) {
		var info = getSelInfo(pRec);
		if (getSelIndex(info) != -1) { return false; }

		mSelItems.push(info);

		var Rec = Ext.data.Record.create([{ name: "Category" }, { name: "DisplayName" }, { name: "RealName" }, { name: "RollupType"}]);
		mSelItemsGrid.getStore().add(new Rec(info));

		setTitle(); manageChecker();
		return false;
	}
	function onCounterDeselect(pSender, pIndex, pRec) {
		var info = getSelInfo(pRec);

		mSelItems.splice(getSelIndex(info), 1);

		var index = mSelItemsGrid.getStore().findBy(function (iRec) { return iRec.get("Category") == info.Category && iRec.get("DisplayName") == info.DisplayName });
		if (index > -1) {
			mSelItemsGrid.getStore().removeAt(index);
		}

		setTitle(); manageChecker();
		return false;
	}

	function onSelItemDelete(pSender, pRow, pColumn) {
		if (pColumn != 1) { return false; }

		var rec = mSelItemsGrid.getStore().getAt(pRow), info = getSelInfo(rec, rec.get("Category"));
		mSelItemsGrid.getStore().removeAt(pRow);

		var index = mCounterGrid.getStore().findBy(function (iRec) { return iRec.get("Category") == info.Category && iRec.get("DisplayName") == info.DisplayName; });
		if (index > -1) {
			mCounterGrid.getSelectionModel().deselectRow(index);
			return false;
		}
		index = getSelIndex(info);
		if (index > -1) {
			mSelItems.splice(index, 1); setTitle();
		}
		return false;
	}

	/*configure ui*/
	function configure(pConfig) {
		if (getCookie(KEY_LIMIT)) { mLimit = parseInt(getCookie(KEY_LIMIT)); }
		if (pConfig.selItems) { mSelItems = pConfig.selItems; }

		/*counters grid*/
		var cStore = new Ext.data.JsonStore({
			root: "data", totalProperty: "total", error: "error",
			baseParams: { action: "getVmWarePCData" }, fields: ["Category", "DisplayName", "RealName", "RollupType"],
			proxy: new Ext.data.HttpProxy({ url: mURL, method: "POST", timeout: mTIMEOUT }),
			remoteSort: true, sortInfo: { field: "Name", direction: "ASC" },
			listeners: { load: onCounterLoaded }
		});
		var sm1 = new Ext.grid.CheckboxSelectionModel({ listeners: { rowselect: onCounterSelect, rowdeselect: onCounterDeselect, selectionchange: onCounterSelectionChange } });
		var cConfig = {
			title: "@{R=APM.Strings;K=APMWEBJS_AK1_13;E=js}", width: 860, height: 400, border: false, frame: true, autoScroll: true, trackMouseOver: false, disableSelection: true, stripeRows: true, loadMask: true, maskDisabled: false,
			viewConfig: { forceFit: true, enableRowBody: true },
			store: cStore,
			cm: new Ext.grid.ColumnModel([
				sm1,
				{ header: "@{R=APM.Strings;K=APMWEBJS_TM0_64;E=js}", width: 70, dataIndex: "DisplayName", sortable: true, menuDisabled: true }
			]),
			sm: sm1,
			bbar: new Ext.PagingToolbar({
			    store: cStore, pageSize: mLimit, items: ["-", "@{R=APM.Strings;K=APMWEBJS_TM0_21;E=js}", "<span id='ctrPSCB'></span>"],
				displayInfo: true, displayMsg: "@{R=APM.Strings;K=APMWEBJS_TM0_52;E=js}", emptyMsg: "@{R=APM.Strings;K=APMWEBJS_TM0_53;E=js}"
			}),
			listeners: {
				afterrender: function () {
					new Ext.form.ComboBox({
						renderTo: "ctrPSCB", store: new Ext.data.SimpleStore({ fields: ["Limit"], data: [[10], [25], [50]] }), displayField: "Limit", valueField: "Limit",
						mode: "local", triggerAction: "all", value: mLimit, editable: false, width: 70,
						listeners: {
							select: function (pSender, pRec, pIndex) {
								mLimit = mCounterGrid.getBottomToolbar().pageSize = parseInt(pRec.get("Limit"));
								setCookie(KEY_LIMIT, mLimit, "months", 1);

								mCounterGrid.getStore().removeAll();
								mCounterGrid.getStore().load({ params: { action: "getVmWarePCData", start: 0, limit: mLimit, category: $("select[id$='ctrPerfObjects']").val()} });
							}
						}
					});
				}
			}
		};

		/*selected items grid*/
		var siConfig = {
			width: 330, height: 350,
			title: "@{R=APM.Strings;K=APMWEBJS_VB1_146;E=js}", hideHeaders: true, border: false, frame: true, autoScroll: true, trackMouseOver: false, disableSelection: true, stripeRows: false,
			viewConfig: { forceFit: true, enableRowBody: true },
			store: new Ext.data.SimpleStore({ fields: ["Category", "DisplayName", "RealName", "RollupType", "Alias", "Action"], data: [] }),
			cm: new Ext.grid.ColumnModel([
				{ dataIndex: "Alias", renderer: function (pVal, pMD, pRec) { return String.format("@{R=APM.Strings;K=APMWEBJS_VB1_150;E=js}", pRec.get("Category"), pRec.get("DisplayName")); } },
				{ dataIndex: "Action", width: 10, renderer: function () { return IMG_DEL; } }
			]),
			listeners: { cellclick: onSelItemDelete }
		};

		mLoadCtr = new Ext.LoadMask($("div#ctrBodyCounter").get(0), { msg: "@{R=APM.Strings;K=APMWEBJS_VB1_147;E=js}" });
		/*populate ui to page*/
		mMainPanel = new Ext.Panel({
			renderTo: "ctrGridCounter", layout: "border", width: 1230, height: 400, hideBorders: true, border: false, bodyBorder: false, bodyStyle: "background-color: transparent;",
			items: [
				{
					region: "center", margins: "0 5 0 0", collapsible: false, items: [(mCounterGrid = new Ext.grid.GridPanel(cConfig))]
				}, {
					region: "east", width: 350, height: 240, margins: "0 5 0 0", items: [(mSelItemsGrid = new Ext.grid.GridPanel(siConfig))]
				}
			]
		});
		mCounterGrid.loadMask.msg = "@{R=APM.Strings;K=APMWEBJS_VB1_148;E=js}";
	} configure({});

	/*public members*/
	this.reloadCounters = function () {
		mLoadCtr.hide(); reloadCounters();
		return false;
	}
	this.showError = function (pError) {
		showError(pError);
		return false;
	}
	this.onPostBack = function () {
		clearCounters(); mLoadCtr.show();
		return false;
	}
	this.onNextClick = function () { if (mSelItems.length < 1) { showError("@{R=APM.Strings;K=APMWEBJS_VB1_149;E=js}"); } return mSelItems.length > 0; }
};
