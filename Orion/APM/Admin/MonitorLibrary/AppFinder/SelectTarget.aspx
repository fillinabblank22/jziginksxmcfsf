<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
    AutoEventWireup="true" CodeFile="SelectTarget.aspx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_SelectTarget" Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_114 %>"  %>
<%@ Register Src="../Controls/SelectCredentials.ascx" TagPrefix="apm" TagName="SelectCredentials" %>
<%@ Register Src="../Controls/SelectPlatform.ascx" TagPrefix="apm" TagName="SelectPlatform" %>
<%@ Register Src="../../../Controls/SelectServerIp.ascx" TagPrefix="apm" TagName="SelectServerIp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">

    <style type="text/css">
        input.apm_Target { width:290px; }
    </style>
        
    <h2><%= Resources.APMWebContent.APMWEBDATA_AK1_115 %></h2>
    <p><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_116, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %></p>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
        <ContentTemplate>            
            <table cellpadding="0" cellspacing="10">
                <tr>
                    <th align="right"><%= Resources.APMWebContent.APMWEBDATA_AK1_117 %></th>
                    <td>
                        <apm:SelectServerIp ID="serverIP" runat="server" OnVmwareValidating="ServerIpVmwareValidating" CssClass="apm_Target" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                       <asp:ValidationSummary ID="PageValidationSummary" runat="server" />
                    </td>
                </tr>
                <tr valign="top">               
                    <th align="right">
                        <asp:Label ID="credentialLabel" runat="server" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_119 %>"></asp:Label>
                    </th>
                    <td>
                        <div class="selectCreds" runat="server" id="SelectCredentialsDiv">
                            <table>
                                <apm:SelectCredentials ID="selectCredentials" ValidationGroupName="SelectCredentialsValidationGroup" runat="server" />                
                            </table>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server"  ValidationGroup="SelectCredentialsValidationGroup"/>        
                        </div>
                    </td>
                </tr>
                <tr valign="top">               
                    <td align="right">
                        <%= Resources.APMWebContent.APMWEBDATA_AK1_120 %>
                    </td>
                    <td>
                        <apm:SelectPlatform ID="selectPlatform" runat="server" />
                    </td>
                </tr>
          </table>

        </ContentTemplate>
    </asp:UpdatePanel>
       
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" OnClick="OnBack" DisplayType="Secondary" CausesValidation="false" />
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" OnClick="OnNext" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" OnClick="OnCancel" DisplayType="Secondary" CausesValidation="false" />
    </div>   

</asp:Content>


