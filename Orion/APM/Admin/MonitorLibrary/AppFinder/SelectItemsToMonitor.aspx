<%@ Page Language="C#" Title="<%$ Resources: APMWebContent, APMWEBDATA_TM0_68 %>" AutoEventWireup="true"  MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" CodeFile="SelectItemsToMonitor.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_SelectItemsToMonitor" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="wizardContentPlaceholder" runat="Server">
    
    <orion:Include runat="server" File="Admin/MonitorLibrary/AppFinder/JS/AppFinder.js" Module="APM" />
	<script type="text/javascript">
		Ext.onReady(
			function() { window.$afITM = new SW.APM.AppFinder.ItemsToMonitor(); }
		);	
	</script>

	<h2><%= StepTitle%></h2>
	<p><%= Description %></p>
	
	<input id="selEntities" name="selEntities" type="hidden" />
    
	<div id="itmPerfCategoryComboContainer" visible="false" runat="server">
	    <br />
        <asp:Label ID="perfObjectLabel" runat="server" Text="<%$ Resources: APMWebContent, APMWEBDATA_TM0_89 %>" ></asp:Label>
        <div id="itmPerfCategoryCombo"></div>
	</div>
	
	<div id="itmGrid" style="padding-top:10px;"></div>
	<br/>

	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" OnClick="OnBack" 
                                 DisplayType="Secondary" CausesValidation="false"  />
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" OnClick="OnNext" 
                                 DisplayType="Primary" OnClientClick="return $afITM.onNextClick();" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" OnClick="OnCancel" 
                                 DisplayType="Secondary" CausesValidation="false" />
	</div>
</asp:Content>
