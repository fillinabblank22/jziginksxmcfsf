﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" AutoEventWireup="true" CodeFile="VmWareSummary.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_VmWareSummary" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_250%>" %>
<%@ Register Src="..\Controls\VmWareSummaryStep.ascx" TagPrefix="apm" TagName="VmWareSummaryStep" %>
<asp:Content ID="ctrContent" ContentPlaceHolderID="wizardContentPlaceholder" runat="server">
    <h2><%= Resources.APMWebContent.APMWEBDATA_VB1_251%></h2>
	<p><%= Resources.APMWebContent.APMWEBDATA_VB1_252%></p>
    <apm:VmWareSummaryStep id="vwSummaryControl" runat="server" />	
	<br/>
	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton ID="imgbBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack_Click"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_253 %>" DisplayType="Primary" OnClick="OnNext_Click"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click" CausesValidation="false"/>
    </div>
</asp:Content>