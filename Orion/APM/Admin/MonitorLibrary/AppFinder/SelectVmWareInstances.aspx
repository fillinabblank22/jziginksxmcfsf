﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" AutoEventWireup="true" CodeFile="SelectVmWareInstances.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_SelectVmWareInstances" %>

<asp:Content ID="ctrBody" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <orion:Include runat="server" Module="APM" File="Admin/MonitorLibrary/AppFinder/JS/VmWareInstances.js"/>
	<script type="text/javascript">
		Ext.onReady(function () { window.$af = new SW.APM.AppFinder.VmWareInstance(); });	
	</script>
	<div>
		<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_264 %></h2>
		<p>
			<%= Resources.APMWebContent.APMWEBDATA_VB1_265 %>
		</p>
		<br />
		
		<div id="ctrContainer"></div>	
		<input id="selEntities" name="selEntities" type="hidden" />
	</div>

	<br/>
	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton ID="imgbBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack_Click" CausesValidation="false"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" LocalizedText="Next" DisplayType="Primary"  OnClick="OnNext_Click" OnClientClick="return $af.onNextClick();"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click" CausesValidation="false"/>
	</div>
</asp:Content>