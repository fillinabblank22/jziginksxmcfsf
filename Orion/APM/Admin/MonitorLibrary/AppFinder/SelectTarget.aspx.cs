using System;
using System.Net;
using System.Web.UI;

using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Common.Models;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_SelectTarget : AppFinderPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckIfValid();
        if (!IsPostBack)
        {
            LoadFromWorkflow();

            imgbNext.AddEnterHandler(1);
        }
        if (Settings.BrowseMode == AppFinderBrowseMode.VmWarePerfCounter)
        {
            serverIP.NodeFilter = String.Format("n.Vendor = '{0}'", AppFinderCurrentSettings.VMWARE_VENDOR);
        }
        serverIP.ValidateVmware = Settings.BrowseMode == AppFinderBrowseMode.VmWarePerfCounter;
    }

    private void LoadFromWorkflow()
    {
        serverIP.SelectedNode = Workflow.CurrentSettings.TargetServer;

        switch (Workflow.CurrentSettings.CurrentComponentType)
        {
            case ComponentType.Process:
            case ComponentType.WindowsService:
            case ComponentType.PerformanceCounter:
            case ComponentType.WmiPerformanceCounter:
                selectCredentials.AllowNodeWmiCredential = true;
                selectCredentials.DataSource = Workflow.CurrentSettings.CredentialSet;
                selectCredentials.DataBindNewCredentialSetForm();
                break;
            case ComponentType.ProcessOverSNMP:
                SelectCredentialsDiv.Visible = false;
                credentialLabel.Visible = false;
                break;
            default:
                selectCredentials.DataSource = Workflow.CurrentSettings.CredentialSet;
                selectCredentials.DataBindNewCredentialSetForm();
                break;
        }

        selectPlatform.Use64Bit = Workflow.CurrentSettings.Use64Bit;
    }

    protected void OnBack(object sender, EventArgs e)
    {
        //FB580 
        try
        {
            if (Workflow.CurrentSettings.CurrentComponentType != SolarWinds.APM.Common.Models.ComponentType.ProcessOverSNMP)
                Workflow.CurrentSettings.CredentialSet = selectCredentials.SelectedCredentialSet;

            Workflow.CurrentSettings.TargetServer = serverIP.SelectedNode;
            Workflow.CurrentSettings.Use64Bit = selectPlatform.Use64Bit;
        }
        catch
        {
            //just ignore
        }

        GotoPreviousPage();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        if (SelectCredentialsDiv.Visible)
        {
            Page.Validate(selectCredentials.ValidationGroupName);
        }

        if (IsValid)
        {
            if (Workflow.CurrentSettings.CurrentComponentType != SolarWinds.APM.Common.Models.ComponentType.ProcessOverSNMP)
            {
                Workflow.CurrentSettings.CredentialSet = selectCredentials.SelectedCredentialSet;
                if (selectCredentials.SelectedCredentialSet.Id == -1)
                {
                    Workflow.SaveCredentials();
                }
            }
            
            Workflow.CurrentSettings.ClearVmWareEntitiesAndPerfCountersTables();
            Workflow.CurrentSettings.VmWareMachineType = null;

            Workflow.CurrentSettings.TargetServer = serverIP.SelectedNode;
            Workflow.CurrentSettings.Use64Bit = selectPlatform.Use64Bit;

            if (Workflow.CurrentSettings.CurrentComponentType == SolarWinds.APM.Common.Models.ComponentType.VmWarePerfCounter)
            {
                if (Settings.VmWareMachineType == null)
                    Settings.VmWareMachineType = serverIP.GetMachineType();

                if (Settings.VmWareMachineType == null)
                    return;

                if (Settings.VmWareMachineType.Equals(AppFinderCurrentSettings.VMWARE_VCENTER_MACHINE_TYPE, StringComparison.InvariantCultureIgnoreCase))
                {
                    Settings.VmWareEntityType = VmWareVCenterEntityType.HostSystem.ToString();
                    Settings.VmWareEntityTypeEnum = VmWareVCenterEntityType.HostSystem;
                }
                else if (Settings.VmWareMachineType.Equals(AppFinderCurrentSettings.VMWARE_ESX_MACHINE_TYPE, StringComparison.InvariantCultureIgnoreCase))
                {
                    Settings.VmWareEntityType = VmWareESXEntityType.HostSystem.ToString();
                    Settings.VmWareEntityTypeEnum = VmWareESXEntityType.HostSystem;
                }
                else
                {
                    return;
                }
            }

            if (Settings.CurrentComponentType == ComponentType.VmWarePerfCounter)
            {
                var vmWareEntitiesTableData = Settings.VmWareEntitiesTable;
                if (vmWareEntitiesTableData != null && vmWareEntitiesTableData.Rows.Count != 0)
                {
                    GotoNextPage();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.UpdatePanel1, typeof(UpdatePanel), "KEY_Error",
                        @"Ext.onReady(function () {Ext.Msg.show({ title: '" + Resources.APMWebContent.APMWEBDATA_TM0_47 + "', msg: '" + Settings.LastBrowseError + "', icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });});", true);
                }
            }
            else
            {
                GotoNextPage();
            }
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected void ServerIpVmwareValidating(object sender, VmwareValidatingEventArgs e)
    {
        Settings.VmWareMachineType = e.MachineType;
    }
}
