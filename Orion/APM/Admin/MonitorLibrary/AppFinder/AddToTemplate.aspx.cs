using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.APM.Common.Models;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_AddToTemplate : SolarWinds.APM.Web.UI.AppFinderPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckIfValid();
        if (!IsPostBack)
        {
            LoadMonitorSelections(Workflow);
            AddToTemplateStep.AddToTemplateSettings = Workflow.AddToTemplateSettings;

			imgbNext.AddEnterHandler(1);
        }
    }

    private void LoadMonitorSelections(AppFinderWorkflow workflow)
    {

        List<string> selections = new List<string>(workflow.MonitorsToAdd.Count);

        foreach (ComponentBase c in workflow.MonitorsToAdd)
        {
            selections.Add(c.Name);
        }

        AddToTemplateStep.MonitorSelectionList = selections;
    }

    protected void OnBack(object sender, EventArgs e)
    {
        GotoPreviousPage();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            AddToTemplateStep.SaveToSettings(Workflow.AddToTemplateSettings);

            GotoNextPage();
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected void OnAddToTemplateSelectionChanged(object sender, EventArgs e)
    {
        AddToTemplateStep.SaveToSettings(Workflow.AddToTemplateSettings);
        UpdateProgressIndicator();
    }
}
