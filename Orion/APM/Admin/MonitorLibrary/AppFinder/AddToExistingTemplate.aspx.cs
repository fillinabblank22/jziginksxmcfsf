﻿using System;
using System.Linq;
using System.Web.UI;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_AddToExistingTemplate : SolarWinds.APM.Web.UI.AppFinderPageBase
{
	protected void Page_Load(object sender, EventArgs e)
	{
		CheckIfValid();
		if (!IsPostBack)
		{
			imgbNext.AddEnterHandler(1);

			ctrCmpsLabel.Text = string.Join("<br />", Workflow.MonitorsToAdd.Select(item => item.Name));

			if (Workflow.AddToTemplateSettings.Mode == AddToTemplateMode.ExistingApplication)
			{
				ctrEntryLabel.Text = string.Format("<b>{0}</b> application.", Workflow.AddToTemplateSettings.ExistingNames.First());
			}
			else 
			{
				ctrEntryLabel.Text = string.Format("<b>{0}</b> template.", Workflow.AddToTemplateSettings.ExistingNames.First());
			}
		}
	}

	protected void OnBack(object sender, EventArgs e)
	{
		GotoPreviousPage();
	}

	protected void OnNext(object sender, EventArgs e)
	{
		if (Page.IsValid)
		{
			GotoNextPage();
		}
	}

	protected void OnCancel(object sender, EventArgs e)
	{
		CancelWizard();
	}
}