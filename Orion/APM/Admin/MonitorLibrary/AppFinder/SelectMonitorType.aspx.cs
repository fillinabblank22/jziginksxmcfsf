using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_SelectMonitorType : AppFinderPageBase
{
    private MonitorTypeItem SelectedMonitorItem
    {
        get
        {
			foreach (var control in pnlMonitors.Controls.Cast<Control>().Where(item => item is MonitorTypeSection))
			{
				var monType = control as MonitorTypeSection;
				if (monType.GetSelectedMonitorItem() != null)
				{
					return monType.GetSelectedMonitorItem();
				}
			}
			return null;
        }
    }

	protected void Page_Load(Object sender, EventArgs e)
    {
        if (IsPostBack)
        {
			return;
		}
		imgbNext.AddEnterHandler(1);
		
		if (!string.IsNullOrEmpty(Settings.CurrentMonitorItemId))
		{
			foreach (var control in pnlMonitors.Controls.Cast<Control>().Where(item => item is MonitorTypeSection))
			{
				((MonitorTypeSection)control).SetSelectedMonitorItem(Settings.CurrentMonitorItemId);
			}
		}

		TryUpdateEditPageParams();

		if (Workflow.MonitorsToAdd.Count() == 0)
		{
			var entityToUpdate = Workflow.AddToTemplateSettings.EntityToUpdate;
			var navigatedFrom = Workflow.AddToTemplateSettings.NavigatedFrom;
			var editPageUrl = Workflow.AddToTemplateSettings.EditPageUrl;

			AppFinderWorkflow.ResetSession();

			Workflow.AddToTemplateSettings.EntityToUpdate = entityToUpdate;
			Workflow.AddToTemplateSettings.NavigatedFrom = navigatedFrom;
			Workflow.AddToTemplateSettings.EditPageUrl = editPageUrl;
		}
		else
		{
			if (Workflow.CurrentSettings.BrowseMode == AppFinderBrowseMode.VmWarePerfCounter)
			{
				var info = Workflow.CurrentSettings.VmWareComponentList;
				Workflow.ResetCurrentMonitor();
				Workflow.CurrentSettings.VmWareComponentList = info;
			}
			else
			{
				Workflow.ResetCurrentMonitor();
			}
		}
	}

	protected void OnNext(Object sender, EventArgs e)
	{
        if (SelectedMonitorItem != null)
        {            
            Settings.CurrentComponentType = SelectedMonitorItem.Type;            
            Settings.CurrentMonitorItemId = SelectedMonitorItem.ID;

			Workflow.AddToTemplateSettings.Mode = AddToTemplateMode.NewTemplate;
			if (Settings.CurrentComponentType == ComponentType.Jmx)
			{
				Workflow.AddToTemplateSettings.Mode = AddToTemplateMode.NewApplication;
			}
			
			Workflow.CurrentSettings.TargetServer = null;
			Workflow.AddToTemplateSettings.ExistingIds.Clear();
			Workflow.AddToTemplateSettings.ExistingNames.Clear();

			TryUpdateEditPageParams();
			
			GotoNextPage();
		}
	}

	protected void OnCancel(Object sender, EventArgs e)
	{
		CancelWizard();
	}

	private void TryUpdateEditPageParams() 
	{
		var sett = Workflow.AddToTemplateSettings;

		var entityToUpdate = Request.QueryStringOrForm("EntityToUpdate") ?? sett.EntityToUpdate;
		if (!string.IsNullOrWhiteSpace(entityToUpdate))
		{
			if (string.IsNullOrWhiteSpace(sett.EditPageUrl))
			{
				sett.EditPageUrl = Request.QueryStringOrForm("EditPageUrl");
			}

			var id = NetObjectHelper.GetId(entityToUpdate);
			if (id > 0)
			{
				var query = default(string);
				if (NetObjectHelper.IsApplication(entityToUpdate))
				{
					sett.Mode = AddToTemplateMode.ExistingApplication;

					query = string.Format(@"
SELECT a.ApplicationID AS Id, a.Name, n.NodeID
FROM Orion.APM.Application AS a
JOIN Orion.Nodes AS n ON n.NodeID = a.NodeID
WHERE a.ApplicationID = {0}", id);
				}
				else if (NetObjectHelper.IsTemplate(entityToUpdate))
				{
					sett.Mode = AddToTemplateMode.ExistingTemplate;

					query = string.Format(@"
SELECT at.ApplicationTemplateID AS Id, at.Name
FROM Orion.APM.ApplicationTemplate AS at 
WHERE at.ApplicationTemplateID = {0}", id);
				}

				var data = SwisProxy.ExecuteQuery(query);
				if (data.Rows.Count > 0)
				{
					sett.EntityToUpdate = entityToUpdate;
					sett.NavigatedFrom = NavigatedFrom.EditPage;

					var row = data.Rows[0];
                    if (sett.Mode == AddToTemplateMode.ExistingApplication)
					{
                        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                        {
                            var nodeId = (int)row["NodeID"];
                            Workflow.CurrentSettings.TargetServer = bl.GetNode(nodeId);
                        }
					}

					sett.ExistingIds.Add(row["Id"].ToString());
					sett.ExistingNames.Add(row["Name"].ToString());
				}
			}
		}
	}
}