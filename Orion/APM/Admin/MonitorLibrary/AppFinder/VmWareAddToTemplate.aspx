<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
    AutoEventWireup="true" CodeFile="VmWareAddToTemplate.aspx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_VmWareAddToTemplate" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_266%>" %>
<%@ Register Src="../Controls/VmWareAddToTemplateStep.ascx" TagPrefix="apm" TagName="VmWareAddToTemplateStep" %> 

<asp:Content ID="ctrBody" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">

    <apm:VmWareAddToTemplateStep ID="vmWareAddToTemplateStep" runat="server"  OnSelectionChanged="OnAddToTemplateSelectionChanged" />

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack" CausesValidation="false"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" LocalizedText="Next" DisplayType="Primary" OnClick="OnNext"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false"/>
    </div>
    
</asp:Content>

