using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_VmWareAddToTemplate : AppFinderPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckIfValid();
        if(!IsPostBack)
        {
            LoadMonitorSelections(Workflow);
            vmWareAddToTemplateStep.AddToTemplateSettings = Workflow.AddToTemplateSettings;
            vmWareAddToTemplateStep.Counter = Settings.VmWareAllCountersSelectedItemsCount;

			imgbNext.AddEnterHandler(1);
        }
    }
    
    private void LoadMonitorSelections(AppFinderWorkflow workflow)
    {
        vmWareAddToTemplateStep.MonitorSelectionListCount = workflow.MonitorsToAdd.Count;
    }

    protected void OnBack(object sender, EventArgs e)
    {
        GotoPreviousPage();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            vmWareAddToTemplateStep.SaveToSettings(Workflow.AddToTemplateSettings);

            GotoNextPage();
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }


    protected void OnAddToTemplateSelectionChanged(object sender, EventArgs e)
    {
        vmWareAddToTemplateStep.SaveToSettings(Workflow.AddToTemplateSettings);
        UpdateProgressIndicator();
    }
    
}
