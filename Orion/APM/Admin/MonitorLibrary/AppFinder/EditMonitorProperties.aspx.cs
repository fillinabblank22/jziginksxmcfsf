using System;
using System.Linq;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_EditMonitorProperties : AppFinderPageBase
{
	protected String ComponentDefinitions
	{
		get
		{
			var defs = APMCache.ComponentDefinitions
				.ToDictionary(x => x.Key.ToString(), x => x.Value);
			return JsHelper.Serialize(defs);
		}
	}

    protected String TargetNode
    {
        get
        {
            var node = Workflow.CurrentSettings.TargetServer;
            var res = new
                {
                    node.Id,
                    node.Name,
                    node.IpAddress,
                    node.Status
                };
            return JsHelper.Serialize(res);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
		if (ctrExecutePollingMethod.Options == null || ctrExecutePollingMethod.Options.Count == 0)
		{
			ctrExecutePollingMethod.Options = DropdownOptionsHelper.OptionsFromEnum<PollingMethod>();
		}

        CheckIfValid();

        btnNext.AddEnterHandler(1);
	}

	protected void OnBack(object sender, EventArgs e)
    {
        GotoPreviousPage();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        Workflow.SaveCurrentMonitor();
        GotoNextPage();
    }

	protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }
}