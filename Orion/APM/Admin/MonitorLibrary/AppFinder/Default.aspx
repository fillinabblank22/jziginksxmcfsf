<%@ Page Language="C#" %>
<%@ Import namespace="SolarWinds.APM.Web.UI"%>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        AppFinderWorkflow.ResetSession();
        Response.Redirect(AppFinderWorkflow.SessionInstance.WizardSteps.FirstStep(), true);                
    }
</script>

