﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" AutoEventWireup="true" CodeFile="SelectJmxMBeans.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_SelectJmxMBeans" %>
<%@ Register Src="./Controls/SelectJmxBeansControl.ascx" TagPrefix="apm" TagName="SelectJmxBeansControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <h2><%= Resources.APMWebContent.APMWEBDATA_TM0_90 %></h2>
    <p><%= Resources.APMWebContent.APMWEBDATA_TM0_91 %></p>

    <apm:SelectJmxBeansControl id="SelectJmxBeansControl" runat="server"></apm:SelectJmxBeansControl>


    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" OnClick="OnBack" DisplayType="Secondary" CausesValidation="false"  />
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" OnClick="OnNext" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" OnClick="OnCancel" DisplayType="Secondary" CausesValidation="false" />
    </div>   
</asp:Content>

