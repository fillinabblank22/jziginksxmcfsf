using Resources;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_SelectItemsToMonitor : AppFinderPageBase
{
	#region Properties

	protected string StepTitle
	{
		get
		{
			switch (Workflow.CurrentSettings.BrowseMode)
			{
				case AppFinderBrowseMode.ProcessMonitorSnmp:
				case AppFinderBrowseMode.ProcessMonitorWmi:
					return APMWebContent.APMWEBCODE_TM0_16;
				case AppFinderBrowseMode.ServiceMonitor:
					return APMWebContent.APMWEBCODE_TM0_17;
				case AppFinderBrowseMode.PerformanceCounter:
					return APMWebContent.APMWEBCODE_TM0_18;
				case AppFinderBrowseMode.VmWarePerfCounter:
					return APMWebContent.APMWEBCODE_TM0_19;
			}
			return string.Empty;
		}
	}

	protected string Description
	{
		get
		{
			switch (Workflow.CurrentSettings.BrowseMode)
			{
				case AppFinderBrowseMode.ProcessMonitorSnmp:
				case AppFinderBrowseMode.ProcessMonitorWmi:
				case AppFinderBrowseMode.ServiceMonitor:
					return APMWebContent.APMWEBCODE_TM0_20;
				case AppFinderBrowseMode.PerformanceCounter:
					return APMWebContent.APMWEBCODE_TM0_21;
				case AppFinderBrowseMode.VmWarePerfCounter:
					return APMWebContent.APMWEBCODE_TM0_22;
			}
			return string.Empty;
		}
	}

	#endregion

	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			itmPerfCategoryComboContainer.Visible = (
				Workflow.CurrentSettings.BrowseMode == AppFinderBrowseMode.PerformanceCounter || Workflow.CurrentSettings.BrowseMode == AppFinderBrowseMode.VmWarePerfCounter
			);
			imgbNext.AddEnterHandler(1);
		}
	}

	protected void OnBack(object sender, EventArgs e)
	{
		//Cleanup of performance counters table
		Workflow.CurrentSettings.ClearPerformanceCountersTable();
		//Cleanup of processes or services table
		Workflow.CurrentSettings.ResetSelectedItems();
		//Cleanup of selected counter category
		Workflow.CurrentSettings.ClearPerformanceCategory();
		//Cleanup of VmWare entities and performance counters tables
		Workflow.CurrentSettings.ClearVmWareEntitiesAndPerfCountersTables();
		//Cleanup old monitors
		Workflow.MonitorsToAdd.Clear();

		GotoPreviousPage();
	}

	protected void OnNext(object sender, EventArgs e)
	{
		List<string> uIDs = new List<string>(Request.Form["selEntities"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));

		DataTable table = null;
		switch (Workflow.CurrentSettings.BrowseMode)
		{
			case AppFinderBrowseMode.None:
			case AppFinderBrowseMode.ProcessMonitorWmi:
			case AppFinderBrowseMode.ProcessMonitorSnmp:
			case AppFinderBrowseMode.ServiceMonitor:
				table = Workflow.CurrentSettings.ProcessOrServicesTable;
				break;
			case AppFinderBrowseMode.PerformanceCounter:
				table = Workflow.CurrentSettings.GetPerformanceCountersData();
				break;
		}
		foreach (DataRow row in table.Rows)
		{
			row["IsSelected"] = uIDs.Contains(row["UniqueId"].ToString());
		}
		Workflow.CurrentSettings.CreateMonitorsFromBrowserSelections();
		GotoNextPage();
	}

	protected void OnCancel(object sender, EventArgs e)
	{
		CancelWizard();
	}

	#endregion
}