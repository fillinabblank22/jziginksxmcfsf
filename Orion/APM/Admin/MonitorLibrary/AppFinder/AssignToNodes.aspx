<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" AutoEventWireup="true" CodeFile="AssignToNodes.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_AssignToNodes" Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_125 %>" %>
<%@ Register Src="../Controls/AssignToNodeStep.ascx" TagPrefix="apm" TagName="AssignToNodeStep" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <apm:AssignToNodeStep ID="AssignToNodeStep" runat="server" />
    <asp:CustomValidator ID="PageValidator" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_126 %>" OnServerValidate="OnValidatePage" runat="server"/>
	<br/>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack" />
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" DisplayType="Primary" OnClick="OnNext" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false" />
    </div>
</asp:Content>

