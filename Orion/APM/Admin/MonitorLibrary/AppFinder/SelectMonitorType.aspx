<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
    AutoEventWireup="true" CodeFile="SelectMonitorType.aspx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_SelectMonitorType" Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_111 %>" %>
<%@ Register Src="~/Orion/APM/Admin/MonitorLibrary/AppFinder/Controls/MonitorTypeSection.ascx" TagPrefix="apm" TagName="MonitorTypeSection" %>
<%@ Register Src="~/Orion/APM/Admin/MonitorLibrary/AppFinder/Controls/MonitorTypeItem.ascx" TagPrefix="apm" TagName="MonitorTypeItem" %>

<asp:Content ID="content" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">   
	<h2><%= Resources.APMWebContent.APMWEBDATA_AK1_112 %></h2>
	<p><%= Resources.APMWebContent.APMWEBDATA_PS0_1%></p>
    <br />
    <asp:Panel ID="pnlMonitors" CssClass="monitors" runat="server">
        <div class="column">
            <apm:MonitorTypeSection ID="mtsWindows" runat="server" Title="<%$ Resources: APMWebContent, Web_WindowsSystems_Title %>" Description="<%$ Resources: APMWebContent, Web_WindowsSystems_Description %>" ImageFilename="icon_Windows.png">
                <MonitorTypeItems>                                
                    <apm:MonitorTypeItem ID="mtiWinService" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_WindowsServiceMonitor_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_WindowsServiceMonitor_Description %>"
                        ImageFilename="icon_monitor_WindowsService.png" Type="WindowsService" />
                    <apm:MonitorTypeItem ID="mtiProcWmi" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_ProcessMonitorWindows_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_ProcessMonitorWindows_Description %>"
                        ImageFilename="icon_monitor_processWMI.png" Type="Process" />
                    <apm:MonitorTypeItem ID="mtiWinProcSNMP" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_ProcessMonitor_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_ProcessMonitor_Description %>"
                        ImageFilename="icon_monitor_WindowsSNMP.png" Type="ProcessOverSNMP" />
                    <apm:MonitorTypeItem ID="mtiWinCounter" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_WindowsPerformanceCounterMonitor_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_WindowsPerformanceCounterMonitor_Description %>"
                        ImageFilename="icon_monitor_WindowsPerf.png" Type="PerformanceCounter" Selected="true" />
                    <apm:MonitorTypeItem ID="mtiCustomWmi" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_CustomPerformanceMonitorWMI_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_CustomPerformanceMonitorWMI_Description %>"
                        ImageFilename="icon_monitor_customPerf.png" Type="WmiPerformanceCounter" />
                </MonitorTypeItems>
            </apm:MonitorTypeSection>
        </div>
        <div class="column">
            <apm:MonitorTypeSection ID="mtsUnix" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_LinuxUnixSystems_Title %>" Description="<%$ Resources: APMWebContent, MonitorType_LinuxUnixSystems_Description %>" ImageFilename="icon_Linux.png">
                <MonitorTypeItems>                                
                    <apm:MonitorTypeItem ID="mtiUnixProcSNMP" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_ProcessMonitor_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_ProcessMonitor_Description %>"
                        ImageFilename="icon_monitor_UnixSNMP.png" Type="ProcessOverSNMP" />                            
                </MonitorTypeItems>
            </apm:MonitorTypeSection>                    
            <apm:MonitorTypeSection ID="mtsVMWare" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_VMWareSystems_Title %>" Description="<%$ Resources: APMWebContent, MonitorType_VMWareSystems_Description %>" ImageFilename="icon_VMWare.png">
                <MonitorTypeItems>                                
                    <apm:MonitorTypeItem ID="mtiVMWarePerfCounter" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_VMWarePerformanceCounterMonitor_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_VMWarePerformanceCounterMonitor_Description %>"
                        ImageFilename="icon_monitor_VMWarePerf.png" Type="VmWarePerfCounter" />                            
                </MonitorTypeItems>
            </apm:MonitorTypeSection>
            <apm:MonitorTypeSection ID="mtsApplications" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_Applications_Title %>" Description="<%$ Resources: APMWebContent, MonitorType_Applications_Description %>" ImageFilename="icon_applications.png">
                <MonitorTypeItems>                                
                    <apm:MonitorTypeItem ID="mtiJavaJMX" runat="server" Title="<%$ Resources: APMWebContent, MonitorType_JavaJMXmonitor_Title %>"
                        Description="<%$ Resources: APMWebContent, MonitorType_JavaJMXmonitor_Description %>"
                        ImageFilename="icon_monitor_JavaJMX.png" Type="Jmx" />                            
                </MonitorTypeItems>
            </apm:MonitorTypeSection>
        </div>      
    </asp:Panel>

    <br /><br />
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="OnNext" LocalizedText="Next" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="OnCancel" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
    </div>   
</asp:Content>