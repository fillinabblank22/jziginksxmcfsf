<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master"
	AutoEventWireup="true" CodeFile="EditMonitorProperties.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_EditMonitorProperties"
	Title="Edit Monitor Properties" %>

<%@ Register TagPrefix="apm" TagName="NumericEditor" Src="~/Orion/APM/Admin/Edit/Editors/NumericEditor.ascx" %>
<%@ Register TagPrefix="apm" TagName="BooleanEditor" Src="~/Orion/APM/Admin/Edit/Editors/BooleanEditor.ascx" %>
<%@ Register TagPrefix="apm" TagName="DropdownEditor" Src="~/Orion/APM/Admin/Edit/Editors/DropdownEditor.ascx" %>
<%@ Register TagPrefix="apm" TagName="ValidationErrorPlaceholder" Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" %>
<%@ Register TagPrefix="apm" TagName="IncludeEditResource" Src="~/Orion/APM/Admin/Edit/Controls/IncludeEditResource.ascx" %>
<%@ Register TagPrefix="apm" TagName="HtmlTemplates" Src="~/Orion/APM/Admin/Edit/Controls/HtmlTemplates.ascx" %>

<asp:Content ContentPlaceHolderID="cpHead" runat="server">
	<apm:IncludeEditResource ID="ier1" runat="server" />
	<apm:HtmlTemplates ID="ctr2" runat="server" />
	<orion:Include ID="I22" runat="server" File="APM/Admin/Edit/Js/AppModel.Template.js" SpecificOrder="111" />
	<style type="text/css">
		div.apm_WizardFrame { width: 1200px; }
		div.apm_WizardFrame div.apm_WizardContainer { width: 97%; }
		.mainAppEditContent { width: 97%; }
	</style>
	<script type="text/javascript">
		(function () {
			SW.APM.EditApp.ComponentDefinitions = <%= ComponentDefinitions %>;
		    SW.APM.EditApp.AppModel.targetNode = <%=TargetNode%>; 
		    SW.APM.EditApp.AppModel.wizardNodeCredentials = <%=Workflow.CurrentSettings.CredentialSet == null ? "0" : SolarWinds.APM.Web.Utility.JsHelper.Serialize(Workflow.CurrentSettings.CredentialSet)%>; 
			SW.APM.EditApp.AppModel.editMode = "FindProcessesWizard";
			SW.APM.EditApp.AppModel.wizardType = "<%=Workflow.CurrentSettings.BrowseMode.ToString()%>";
			SW.APM.EditApp.AppModel.Config.webServiceLoadMethod = "LoadApplicationTemplateFromSession";
			SW.APM.EditApp.AppModel.Config.webServiceSaveMethod = "SaveApplicationTemplateToSession";
		})();
		$(function() {
    	    SW.APM.EditApp.init();
    	});
	</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" runat="Server">
	<div class="mainAppEditContent">
		<table id="appTitle">
			<tr>
				<td>
					<div id="pageTitle" />
					<textarea id="pageTitleTemplate" name="pageTitleTemplate" rows="1" style="display: none"></textarea>
				</td>
			</tr>
		</table>
		<div class="boxedContent">
			<table id="appEditContent" class="x-hidden">
				<tr>
					<td>
						<table id="appMainProp" class="appProperties" cellpadding="0" cellspacing="0">
							<tr>
								<td class="label">
                                    <%= Resources.APMWebContent.EditApplication_TemplateName%>
								</td>
								<td colspan="2">
									<input id="appName" name="appName" class="edit name required" size="60" type="text"
										apmsettctr="false" />
								</td>
							</tr>
							<tr>
								<td class="label">
                                    <%= Resources.APMWebContent.EditTemplate_TemplateAppliedTo%>
								</td>
								<td id="tdAppliedApps" style="border: none; white-space: normal;" runat="server">
									&nbsp;
								</td>
							</tr>
							<tr id="trViewAllHrefContent" visible="false" runat="server">
								<td>
									&nbsp;
								</td>
								<td>
									<a id="aViewAll" href="#" runat="server">&nbsp;</a>
								</td>
							</tr>
							<tr>
								<td class="label">
                                    <%= Resources.APMWebContent.EditTemplate_Description%>
								</td>
								<td class="apm_TemplateDescription">
									<textarea id="appDescription" class="apm_Resizable edit name " cols="60" rows="3"
										name="appDescription"></textarea>
									<apm:ValidationErrorPlaceholder runat="server" />
								</td>
							</tr>
							<tr>
								<td class="label">
                                    <%= Resources.APMWebContent.APMWEBDATA_TM0_37%>
									<br />
									<span style="font-size: 7pt;"><%= Resources.APMWebContent.EditTemplate_SeparateTagsWithCommas%></span>
								</td>
								<td>
									<input id="appTags" name="appTags" type="text" class="edit name" size="60" />
									<apm:ValidationErrorPlaceholder runat="server" />
								</td>
							</tr>
							<tr>
								<td class="label">
                                    <%= Resources.APMWebContent.EditApplication_CustomApplicationDetailsView%>
								</td>
								<td>
									<select id="appDetailsView" name="appDetailsView" style="width: 500px;">
									</select>
								</td>
							</tr>
							<apm:NumericEditor ID="ctrFrequency" DisplayText="<%$ Resources: APMWebContent, MonitorLibrary_PollingFrequency %>" BaseValueName="appFrequency"
								UnitsLabel="<%$ Resources: APMWebContent, Edit_UnitsLabel_Seconds %>" runat="server" />
							<apm:NumericEditor ID="ctrTimeout" DisplayText="<%$ Resources: APMWebContent, MonitorLibrary_PollingTimeout %>" BaseValueName="appTimeout"
								UnitsLabel="<%$ Resources: APMWebContent, Edit_UnitsLabel_Seconds %>" runat="server" />
							<tr>
								<td>
									<div style="padding-top: 10px">
										<img src="../../../Images/Button.Expand.gif" class="collapsable" data-collapse-target="advancedSettings" />
										<span class="advancedTitle"><%= Resources.APMWebContent.APMWEBDATA_VB1_100%><br />
										</span>
									</div>
								</td>
							</tr>
							<tbody id="advancedSettings" style="display: none;">
								<apm:DropdownEditor id="ctrExecutePollingMethod" DisplayText="<%$ Resources: APMWebContent, MonitorLibrary_Polling Method %>" BaseValueName="appExecutePollingMethod" AdditionalLabelCss="indended" runat="server" />
								<apm:BooleanEditor runat="server" DisplayText="<%$ Resources: APMWebContent, MonitorLibrary_DebugLogging %>" BaseValueName="appDebugLogging"
									AdditionalLabelCss="indended" TrueDisplayValue="<%$ Resources: APMWebContent, MonitorLibrary_On %>" FalseDisplayValue="<%$ Resources: APMWebContent, MonitorLibrary_Off %>" />
								<apm:NumericEditor runat="server" DisplayText="<%$ Resources: APMWebContent, MonitorLibrary_NumberOfLogFilesToKeep %>" BaseValueName="appLogFiles"
									MinValue="1" AdditionalLabelCss="indended" UnitsLabel="<%$ Resources: APMWebContent, TheNumberOfMostRecentLogFiles %>" />
								<apm:BooleanEditor runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_PlatformToRunPollingJobOn %>" BaseValueName="appUse64Bit"
									AdditionalLabelCss="indended" TrueDisplayValue="<%$ Resources: APMWebContent, Edit_x64 %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_x86 %>" />
							</tbody>
						</table>
					</td>
				</tr>
			</table>
			<div id="componentHeader">
				<h2><%= Resources.APMWebContent.Edit_master_ComponentMonitors%></h2>
                <%= Resources.APMWebContent.EditMonitorProperties_AddEditTest%>
			</div>
			<div id="grid">
			</div>
			<div id="formErrors" class="sw-suggestion sw-suggestion-fail" style="display: none;">
				<span class="sw-suggestion-icon"></span><span id="formErrors-txt"></span>
			</div>
		</div>
		<div id="componentEditorBody" style="display: none;">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="x-grid3-col x-grid3-cell x-grid3-td-checker x-grid3-cell-first " tabindex="0">
					</td>
					<td>
						<div class="componentEditContent">
							<img src="/Orion/APM/Images/loading_gen_16x16.gif" />
                            <%= Resources.APMWebContent.LoadingSettings%></div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<br />
	<br />
	<br />
	<div class="apm_WizardButtons">
		<div class="buttonBar">
		</div>
		<asp:Button ID="btnBack" CssClass="x-hidden" OnClick="OnBack" CausesValidation="false" runat="server"/>
		<asp:Button ID="btnNext" CssClass="x-hidden" OnClick="OnNext" CausesValidation="false" runat="server"/>
		<asp:Button ID="btnCancel" CssClass="x-hidden" OnClick="OnCancel" CausesValidation="false" runat="server"/>
	</div>
</asp:Content>
