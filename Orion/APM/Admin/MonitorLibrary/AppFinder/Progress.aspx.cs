using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_MonitorLibrary_AppFinder_Progress : AppFinderPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckIfValid();
        if (!IsPostBack)
        {
            UpdateProgress();
        }

    }

    protected void OnUpdateTimerTick(object sender, EventArgs e)
    {
        UpdateProgress(); 
    }

    private void UpdateProgress()
    {
        Status.Text = Workflow.CreationStatus;

        if (Workflow.IsCreationComplete)
        {
            Workflow.EndCreateApplications();
            GotoNextPage();
        }
    }
}
