<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" 
    AutoEventWireup="true" CodeFile="Summary.aspx.cs" 
    Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_Summary" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_250%>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <h2><%= Resources.APMWebContent.APMWEBDATA_VB1_251 %></h2>
    <p><%= Resources.APMWebContent.APMWEBDATA_VB1_252 %></p>
    
    <ul>
        <asp:Repeater ID="monitorRepeater" runat="server" OnItemCommand="monitorRepeater_OnItemCommand">
            <ItemTemplate>
                <li>
                    <%# Eval("Name") %>&nbsp; 
                    <orion:LocalizableButton ID="DeleteMonitor" runat="server" LocalizedText="Delete" DisplayType="Small" CommandName="Delete" CommandArgument='<%# Eval("Id") %>' CausesValidation="False"/>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    
    <br />
    <br />
    <br />
    
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_253 %>" DisplayType="Primary" OnClick="OnNext"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false"/>
    </div>

</asp:Content>

