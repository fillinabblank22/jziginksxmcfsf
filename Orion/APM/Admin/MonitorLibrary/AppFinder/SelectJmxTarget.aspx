﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/MonitorLibrary/AppFinder/AppFinderWizard.master" AutoEventWireup="true" CodeFile="SelectJmxTarget.aspx.cs" Inherits="Orion_APM_Admin_MonitorLibrary_AppFinder_SelectJmxTarget" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Register Src="./Controls/SelectJmxTargetControl.ascx" TagPrefix="apm" TagName="SelectJmxTargetControl" %>
<%@ Register Src="./Controls/JmxConnectionTestControl.ascx" TagPrefix="apm" TagName="JmxConnectionTestControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
<div id="wizardElement" runat="server">
    <style type="text/css">
        .selectCreds
        {
            background-color: #E4F1F8;        
            padding: 7px 10px 7px 12px;            
        }
        .selectCreds td
        {
            padding: 0px 5px 4px 0px;            
        }
    </style>
        
    <h2><%= APMWebContent.APMWEBDATA_TM0_69 %></h2>
    <p><%= String.Format(APMWebContent.APMWEBDATA_TM0_70, SolarWinds.APM.Common.ApmConstants.ModuleShortName)%><a class="apm_learnMore" href="<%= HelpLocator.CreateHelpUrl("OrionAPMAGConfigureJMX") %>"><%= APMWebContent.APMWEBDATA_TM0_71 %></a></p>

    <apm:SelectJmxTargetControl id="selectJmxTargetControl" runat="server"></apm:SelectJmxTargetControl>
    <apm:JmxConnectionTestControl id="jmxConnectionTestControl" runat="server"></apm:JmxConnectionTestControl>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" OnClick="OnBack" DisplayType="Secondary" CausesValidation="false"  />
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" OnClick="OnNext" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" OnClick="OnCancel" DisplayType="Secondary" CausesValidation="false" />
    </div>   
</div>
</asp:Content>


