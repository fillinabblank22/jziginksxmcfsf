using Infragistics.WebUI.UltraWebNavigator;
using Resources;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Net;

public partial class Orion_APM_Admin_MonitorLibrary_Default : Page
{
	private const string FolderImagePath = "/Orion/APM/images/admin/icon_folder.gif";

	private Dictionary<int, int> lookupTemplateCounts;
	private Dictionary<int, int> lookupApplicationCounts;

	private enum FolderType
	{
		Template,
		Application
	}

	protected static string TemplateColor
	{
		get { return "#a7a7a7"; }
	}

	protected static string ApplicationColor
	{
		get { return "#86baf8"; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			LoadComponentCounts();
			LoadTreeRoot();
		}

		tree.LoadOnDemandPrompt = String.Format("<b>{0}</b>", APMWebContent.APMWEBDATA_TM0_45);
	}

	override protected void OnInit(EventArgs e)
	{
		this.tree.DemandLoad += new DemandLoadEventHandler(NodeTree_OnDemandLoad);
	    OverrideDefaultStyle();
		base.OnInit(e);
	}

    private void OverrideDefaultStyle()
    {
        tree.SelectedNodeStyle.BackColor = Color.Transparent;
    }

	private void LoadComponentCounts()
	{
		lookupApplicationCounts = SwisDAL.GetApplicationCountsByComponentType();
		lookupTemplateCounts = SwisDAL.GetTemplateCountsByComponentType();
	}

	private void LoadTreeRoot()
	{
		var list = this.GetDisplayableComponentDefinitions();
		var sortedList = new List<ComponentDefinition>(list);
		sortedList.Sort((x, y) => String.Compare(x.Name, y.Name, StringComparison.InvariantCultureIgnoreCase));

		tree.Nodes.Clear();

		foreach (ComponentDefinition definition in sortedList)
		{
            Infragistics.WebUI.UltraWebNavigator.Node definitionNode = tree.Nodes.Add(GetDefinitionLabel(definition));

            Infragistics.WebUI.UltraWebNavigator.Node tempFolderNode = definitionNode.Nodes.Add(GetDefinitionTemplatesLabel(definition));
			tempFolderNode.DataKey = FolderType.Template;
			tempFolderNode.Tag = definition.Type;
			tempFolderNode.ImageUrl = FolderImagePath;
			tempFolderNode.ShowExpand = true;

            Infragistics.WebUI.UltraWebNavigator.Node appFolderNode = definitionNode.Nodes.Add(GetDefinitionApplicationsLabel(definition));
			appFolderNode.DataKey = FolderType.Application;
			appFolderNode.Tag = definition.Type;
			appFolderNode.ImageUrl = FolderImagePath;
			appFolderNode.ShowExpand = true;
		}
	}

    private IEnumerable<ComponentDefinition> GetDisplayableComponentDefinitions()
    {
        IEnumerable<ComponentDefinition> components = APMCache.ComponentDefinitions.Values;
        if (ComponentConstants.IsDevMode) {
            return components;
        }
        return components.Where(aaComponent => ReflectionHelper.GetCustomEnumValueAttribute<IsInternalAttribute>(aaComponent.Type) == null);
    }

	private string GetDefinitionLabel(ComponentDefinition definition)
	{
		return String.Format("{0} ({1}, {2})",
				definition.Name,
				GetTemplateCount((int)definition.Type),
				GetApplicationCount((int)definition.Type));
	}

	private string GetDefinitionTemplatesLabel(ComponentDefinition definition)
	{
		return String.Format(APMWebContent.APMWEBCODE_TM0_12, definition.Name, GetTemplateCount((int)definition.Type));
	}

	private string GetDefinitionApplicationsLabel(ComponentDefinition definition)
	{
		return String.Format(APMWebContent.APMWEBCODE_TM0_13, definition.Name, GetApplicationCount((int)definition.Type));
	}

	private string GetApplicationCount(int componentType)
	{
		int count = 0;
		if (lookupApplicationCounts.ContainsKey(componentType))
			count = lookupApplicationCounts[componentType];

		return String.Format("<span style=\"color:{0};font-weight: bold;\">{1}</span>", ApplicationColor, count);
	}

	private string GetTemplateCount(int componentType)
	{
		int count = 0;
		if (lookupTemplateCounts.ContainsKey(componentType))
			count = lookupTemplateCounts[componentType];

		return String.Format("<span style=\"color:{0};font-weight: bold;\">{1}</span>", TemplateColor, count);
	}

	private static void NodeTree_OnDemandLoad(object sender, WebTreeNodeEventArgs e)
	{
		FolderType folderType = (FolderType)e.Node.DataKey;
		ComponentType componentType = (ComponentType)e.Node.Tag;

		if (folderType == FolderType.Application)
		{
			DataTable table = SwisDAL.GetExistingApplicationsByComponentType(componentType);

			foreach (DataRow dataRow in table.Rows)
			{
				string caption = String.Format(APMWebContent.APMWEBCODE_VB1_3, dataRow["AppName"], dataRow["NodeName"]);
				caption = WebUtility.HtmlEncode(caption);

				ApmStatus status =
					Convert.IsDBNull(dataRow["Status"]) ? new ApmStatus(Status.Undefined) :
					new ApmStatus((Status)dataRow["Status"]);

				var treeNode = e.Node.Nodes.Add(caption);
				treeNode.ImageUrl = status.ToString("smallappimgpath", null);
				treeNode.TargetUrl = ApmMasterPage.GetEditApplicationPageUrl(dataRow["ID"]);
			}

			if (e.Node.Nodes.Count == 0)
				e.Node.Nodes.Add(String.Format("<i>{0}</i>", APMWebContent.APMWEBCODE_TM0_14));

			e.Node.Expanded = true;
		}
		else if (folderType == FolderType.Template)
		{
			DataTable table = SwisDAL.GetExistingTemplatesByComponentType(componentType);

			foreach (DataRow dataRow in table.Rows)
			{
				string caption = dataRow["Name"].ToString();
				caption = WebUtility.HtmlEncode(caption);

				var treeNode = e.Node.Nodes.Add(caption);
				treeNode.ImageUrl = "/Orion/APM/images/admin/icon_application_template.gif";
				treeNode.TargetUrl = ApmMasterPage.GetEditTemplatePageUrl(dataRow["ID"]);
			}

			if (e.Node.Nodes.Count == 0)
			{
				e.Node.Nodes.Add(String.Format("<i>{0}</i>", APMWebContent.APMWEBCODE_TM0_15));
			}

			e.Node.Expanded = true;

		}
	}

	protected void AppBuilderWizard_Click(object sender, EventArgs e)
	{
		Response.Redirect("AppFinder/Default.aspx");
	}
}
