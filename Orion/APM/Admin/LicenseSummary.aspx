<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true" 
    CodeFile="LicenseSummary.aspx.cs" Inherits="Orion_APM_Admin_LicenseSummary" Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_62 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
    
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks">
        <orion:IconHelpButton HelpUrlFragment="OrionAPMPHConfigSettingsLicensingSum" ID="IconHelpButton1" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
	<orion:Include runat="server" Module="APM" File="APM.css" />
    <orion:Include runat="server" Module="APM" File="APM-ORION-FIX.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<table width="100%" id="breadcrumb" style="width:100%;">
		<tr>
			<td style="border-bottom-width: 0px;">
				<h1><%=Page.Title%></h1>
			</td>
		</tr>
	</table>

    <div style="width: auto">
	    <table cellpadding="0" cellspacing="0" width="440px">
			    <tr>
    			    <td class="PropertyHeader" colspan="2"><%=summary.LicenseLimitWording %></td>
	    		    <td class="Property"><%=summary.LicenseLimitDisplay %></td>
                </tr>
			    <tr>
                    <td class="PropertyHeader" colspan="2"><%=summary.ElementsCountWording %></td>
	    		    <td class="Property"><%=summary.ElementsCount %></td>
                </tr>
			    <tr>
			        <td>&nbsp;&nbsp;</td>
    			    <td class="PropertyHeader"><%=summary.LicensedElementsWording %></td>
	    		    <td class="Property"><%=summary.LicensedElements %></td>
                </tr>
			    <tr>
			        <td style="width: 1px;">&nbsp;&nbsp;</td>
    			    <td class="PropertyHeader"><%=summary.UnlicensedElementsWording %></td>
	    		    <td class="Property"><%=summary.UnlicensedElements %></td>
                </tr>
			    <tr>
    			    <td class="PropertyHeader" colspan="2"><%=summary.AvailableElementsWording %></td>
	    		    <td class="Property"><%=summary.AvailableElementsDisplay %></td>
                </tr>
	    </table>
	</div>

</asp:Content>



