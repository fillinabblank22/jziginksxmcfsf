<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true"
    CodeFile="ImportApplicationTemplate.aspx.cs" Inherits="Orion_APM_Admin_ImportApplicationTemplate"
    Title="Import Application Template" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="APM/APM.css"/>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1>
        <%= Resources.APMWebContent.APMWEBDATA_VB1_178 %></h1>
    <asp:FileUpload ID="uploadTemplate" runat="server" Width="50em" size="80" />
    <br />
    <br />
    <asp:CustomValidator ID="valTemplateFile" runat="server"
        ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_179 %>" />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="submitButton" LocalizedText="Submit"
            DisplayType="Primary" OnClick="submitButton_Click" CausesValidation="true" />
        <orion:LocalizableButton ID="cancelButton" runat="server" LocalizedText="Cancel"
            DisplayType="Secondary" CausesValidation="false" PostBackUrl="~/Orion/APM/Admin/ApplicationTemplates.aspx" />
    </div>
</asp:Content>
