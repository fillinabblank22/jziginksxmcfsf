<%@ WebHandler Language="C#" Class="DownloadTemplate" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;

public class DownloadTemplate : IHttpHandler, IRequiresSessionState 
{
    private const int IEMaxFileNameSize = 23;
    public void ProcessRequest (HttpContext context) 
    {
        List<int> exportIds = GetTemplateIdsToExport(context);
        IList<ApplicationTemplate> toExport = LoadTemplatesToExport(exportIds);
        
        if (toExport.Count == 0)
        {
            context.Response.ContentType = "text/html";
            return;
        }

        string fileName = GetExportFileName(context, toExport);

        context.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
        context.Response.ContentType = "application/octet-stream";
        ApmViewManager.ExportApplicationTemplates(context.Response.OutputStream, toExport);
        context.Session["templatesReadyToDownload"] = true;
        context.Response.End();
    }

    public bool IsReusable
    {
        get { return false; }
    }    

    private static string GetExportFileName(HttpContext context, IList<ApplicationTemplate> toExport)
    {
        string filename = "APM_multiple.apm-template";

        if (toExport.Count == 1)
            filename = String.Format("\"{0}.apm-template\"", EncodeFilenameIfNeeded(context, toExport[0].Name));

        return filename;
    }

    private static string EncodeFilenameIfNeeded(HttpContext context, string filename)
    {
        string browserType = context.Request.Browser.Type.ToUpperInvariant();
        
        if (browserType.StartsWith("IE"))
        {
            //Internet Explorer cuts not ANSI file name from the beginning if it is larger than 23 symbols
            //This code cuts file name from the end.
            if (!IsAnsiString(filename) && (filename.Length > IEMaxFileNameSize))
                filename = filename.Substring(0, IEMaxFileNameSize);
            
            return HttpUtility.UrlPathEncode(filename);
        }

        return filename;
    }

    private static bool IsAnsiString(string str)
    {
        const char compareCharacter = '\x7F';
        foreach (char c in str)
        {
            if (c > compareCharacter)
                return false;
        }
        return true;
    }
        
    private static IList<ApplicationTemplate> LoadTemplatesToExport(IList<int> templateIds)
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            return businessLayer.GetApplicationTemplates(templateIds);
        }
    }

    public List<int> GetTemplateIdsToExport(HttpContext context)
    {
        string queryString = context.Request.QueryString["id"] ?? String.Empty;

        string[] exportIdStrings = queryString.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        var exportIds = new List<int>();

        foreach (string id in exportIdStrings)
        {
            int val;
            if (int.TryParse(id, out val))
            {
                exportIds.Add(val);
            }
        }

        return exportIds;
    }

}