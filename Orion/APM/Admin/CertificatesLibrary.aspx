﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" AutoEventWireup="true"
    CodeFile="CertificatesLibrary.aspx.cs" Inherits="Orion_APM_Admin_CertificatesLibrary"
    Title="<%$ Resources: APMWebContent, APMWEBDATA_RB0_1 %>" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="c1" ContentPlaceHolderID="hPh" runat="server">
	<script type="text/javascript">
	    function ConfirmDeleteCredentialSet() {
	        return confirm("<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_1)%>");
	    }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="SAMAGCertificateCredentialsLibrary" ID="IconHelpButton1" runat="server" />
</asp:Content>
<asp:Content ID="c2" ContentPlaceHolderID="cPh" runat="Server">
	<table width="100%" id="breadcrumb" style="width:100%; padding: 0;">
		<tr>
			<td style="border-bottom-width: 0px; padding: 0px;">
				<h1><%=Page.Title%></h1>
			</td>
		</tr>
	</table>
        
    <div style="width:700px;">
    <%= Resources.APMWebContent.APMWEBDATA_RB1_1%>
    </div>           
    <br /> 
    <asp:PlaceHolder id="phBadCredentialsNote" runat="server" >
        <span class="apm_bad_credetnials_note"><%= Resources.APMWebContent.APMWEBDATA_VB1_2 %>&nbsp;&nbsp;<a href="<%= SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(2149) %>" target="_blank">&raquo; <%= Resources.APMWebContent.APMWEBDATA_VB1_3 %></a></span>
        <br />  
        <br />  
    </asp:PlaceHolder>
    <table style="width: 700px; border: 1px solid; border-color: #dfdfde;" cellpadding="0" cellspacing="0" class="NeedsZebraStripes">
        <thead>
            <tr bgcolor="#e2e1d4">
                <th>
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_4 %>
                </th>
                <th style="padding-left: 8px">
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_5 %>
                </th>
                <th id="thBadCredentials" runat="server" >
                    &nbsp;
                </th>
                <th style="text-align: right">
                    <orion:LocalizableButton ID="NewCredentials" runat="server" DisplayType="Primary" PostBackUrl="~/Orion/APM/Admin/EditCertificateSet.aspx?mode=Insert" Text="<%$ Resources:APMWebContent, APMWEBDATA_VB1_7 %>"/>
                </th>
            </tr>
        </thead>
        <asp:Repeater ID="repCredentialsLibrary" runat="server" OnItemCommand="rep_ItemCommand">
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lbName" Text='<%# Eval("Name") %>' /></td>
                    <td style="padding-left: 8px">
                        <asp:Label runat="server" ID="lbUserName" Text='<%# Eval("UserName") %>' />
                        <asp:Label runat="server" ID="lbId" Visible="false" Text='<%# Eval("Id") %>' />
                    </td>
                    <td id="tdBadCredentials" runat="server">
                        <span id="badCredentials" runat="server" class="apm_bad_credetnials" Visible='<%# Eval("Broken") %>'><%= Resources.APMWebContent.APMWEBDATA_VB1_6 %>&nbsp;&nbsp;<a href="<%= SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(2149) %>" target="_blank">&raquo; <%= Resources.APMWebContent.APMWEBDATA_VB1_3 %></a></span>
                    </td>
                    <td>
                        <orion:LocalizableButton ID="btnEdit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("Id") %>' DisplayType="Small" LocalizedText="Edit"/>
                        &nbsp;
                        <orion:LocalizableButton ID="btnDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("Id") %>' DisplayType="Small" LocalizedText="Delete" OnClientClick='return ConfirmDeleteCredentialSet();'/>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
        
</asp:Content>
