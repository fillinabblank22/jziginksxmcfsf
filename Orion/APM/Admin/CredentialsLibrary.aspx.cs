using System;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Credentials;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_CredentialsLibrary : System.Web.UI.Page
{
    private IList<CredentialSet> list;

    public bool AnyBadCredentials 
    {
        get 
        {
            if (list == null)
            {
                return false;
            }
            return list.Any(cr => cr.Broken);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadCredentilas();
            NewCredentials.Focus();
			NewCredentials.AddEnterHandler(1);
        }
    }

    private void LoadCredentilas()
    {
        list = new List<CredentialSet>();
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            list = (List<CredentialSet>)businessLayer.GetCredentials();
            repCredentialsLibrary.DataSource = list;
            repCredentialsLibrary.DataBind();
            //AnyBadCredentials = list.Any(cr => cr.Broken);

            phBadCredentialsNote.Visible = AnyBadCredentials;
            thBadCredentials.Visible = AnyBadCredentials;

            foreach (RepeaterItem item in repCredentialsLibrary.Items)
            {

                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlTableCell badCredentialsCell = (HtmlTableCell)item.FindControl("tdBadCredentials");
                    badCredentialsCell.Visible = AnyBadCredentials;
                }

            }
            //tdBadCredentials.Visible = anyBadCredentials;
        }
    }
    protected void rep_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string path, tempPath;
        if (e.CommandName == "Edit")
        {
            string crdId = e.CommandArgument.ToString();
            tempPath = "&crdId=" + crdId;
            path = "~/Orion/APM/Admin/EditCredentialSet.aspx?mode=Edit";
            path = path + tempPath;
            Response.Redirect(path);
        }
        if (e.CommandName == "Delete")
        {
            int id = int.Parse(e.CommandArgument.ToString());
            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                businessLayer.DeleteCredentialSet(id);
            }
            LoadCredentilas();
        }
    }


}
