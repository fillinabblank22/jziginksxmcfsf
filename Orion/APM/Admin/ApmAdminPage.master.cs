﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common;

public partial class Orion_APM_Admin_ApmAdminPage : SolarWinds.APM.Web.UI.ApmMasterPage
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (!SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin)
		{
			Server.Transfer(InvariantString.Format("~/Orion/Error.aspx?Message=You must be an {0} Administrator to access this page.", ApmConstants.ModuleShortName));
		}
	}	
}