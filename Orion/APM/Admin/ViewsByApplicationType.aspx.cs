using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.EditModels;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using ApplicationTemplateInfo = SolarWinds.APM.Web.Models.ApplicationTemplateInfo;
using AppWebDal = SolarWinds.APM.Web.DAL.ApplicationTemplateDAL;

public partial class Orion_APM_Admin_ViewsByApplicationType : Page
{
    private struct ListBoxProp
    {
        public readonly int TemplateId;
        public readonly string OriginalViewType;
        public readonly ListBox ListBox;

        public ListBoxProp(int templateId, string originalViewType, ListBox listBox)
        {
            TemplateId = templateId;
            OriginalViewType = originalViewType;
            ListBox = listBox;
        }
    }

    private static readonly Log Log = new Log();

    private readonly List<ListBoxProp> appTemplateListBoxes = new List<ListBoxProp>();

    protected override void OnInit(EventArgs e)
    {
        if (!Profile.AllowCustomize)
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", APMWebContent.APMWEBCODE_VB1_83));
        }
        btnSubmit.AddEnterHandler(1);

        SetupApplicationBoxes();

        base.OnInit(e);
    }

    private void SetupApplicationBoxes()
    {
        var dal = new AppWebDal();

        appTemplateListBoxes.Clear();
        rptNodes.DataSource = dal.GetGenericTemplateInfos()
            .Where(at => at.ViewXml != null || (at.ViewId > 0 && at.ViewId != ApmApplicationBase.DefaultViewID && ViewManager.GetViewById(at.ViewId) != null))
            .OrderBy(at => at.Name);
        rptNodes.DataBind();
    }

    protected void AppTemplateViewsDataBinding(object sender, EventArgs e)
    {
        string defaultCaption = APMWebContent.APMWEBCODE_VB1_84;

        var lbxViews = (ListBox)sender;
        var container = (RepeaterItem)lbxViews.NamingContainer;

        var ati = (ApplicationTemplateInfo)container.DataItem;

        lbxViews.Items.Add(new ListItem(defaultCaption, ApmViewManager.DEFAULT));
        KeyValuePair<string, string> viewSpec = ApmViewManager.DefaultViewSpec(ati.ViewId, ati.HasImportedView, ati.Name, ati.ViewXml, false);
        lbxViews.Items.Add(new ListItem(viewSpec.Value, ApmViewManager.CUSTOM));
        lbxViews.SelectedValue = viewSpec.Key;

        appTemplateListBoxes.Add(new ListBoxProp(ati.Id, viewSpec.Key, lbxViews));
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        IList<KeyValuePair<int, ListBox>> changes = GetChangedListboxes().ToList();

        if (changes.Count != 0)
        {
            UpdateTemplates(changes);
            if (Log.IsDebugEnabled) Log.Debug(InvariantString.Format("{0} application templates updated.", changes.Count));
        }
        else if (Log.IsDebugEnabled) { Log.Debug("No changes detected."); }

        var redirectUrl = "~/Orion/Admin/";
        if ((Request.Params["redirectTo"] != null) && (Request.Params["redirectTo"].Equals("apmSettings", StringComparison.OrdinalIgnoreCase)))
        {
            redirectUrl = "~/Orion/APM/Admin/";
        }

        Response.Redirect(redirectUrl);
    }

    private static void UpdateTemplates(IEnumerable<KeyValuePair<int, ListBox>> changes)
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            foreach (KeyValuePair<int, ListBox> myPair in changes)
            {
                ApplicationTemplate appTemplate = businessLayer.GetApplicationTemplateWithoutComponents(myPair.Key);
                ApplicationTemplateEditModel.SetDetailsView(appTemplate, myPair.Value.SelectedValue);
                businessLayer.UpdateApplicationTemplateHeaderInfo(appTemplate);
            }
        }
    }

    private IEnumerable<KeyValuePair<int, ListBox>> GetChangedListboxes()
    {
        return appTemplateListBoxes
            .Where(box => box.OriginalViewType != box.ListBox.SelectedValue)
            .Select(box => new KeyValuePair<int, ListBox>(box.TemplateId, box.ListBox));
    }
}