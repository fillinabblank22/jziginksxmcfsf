﻿Ext4.namespace('SW.APM.RealTimeServiceGrid');

SW.APM.RealTimeServiceGrid.Renderers = (function () {
	var SF = String.format;
	var DETAILS_FORMAT =
		"<div class=\"rtsm-qtip\" style=\"max-height: 400px; overflow:auto; overflow-x:hidden;\" >" +
			"<div class=\"row\"><div class=\"svc-prop-name bold\">Name:</div> <div class=\"svc-prop-value\">{0}</div></div>" +
			"<div class=\"row\"><div class=\"svc-prop-name bold\">Display Name:</div> <div class=\"svc-prop-value\">{1}</div></div>" +
			"<div class=\"row\"><div class=\"svc-prop-name bold\">Description:</div> <div class=\"svc-prop-value\">{2}</div></div>" +
			"<div class=\"row\"><div class=\"svc-prop-name bold\">Process Id:</div> <div class=\"svc-prop-value\">{3}</div></div>" +
			"<div class=\"row\"><div class=\"svc-prop-name bold\">Log On As:</div> <div class=\"svc-prop-value\">{4}</div></div>" +
			"<div class=\"row\"><div class=\"svc-prop-name bold\">Path:</div> <div class=\"svc-prop-value\">{5}</div></div>" +
			"<div class=\"row bold\" style=\"padding-top: 10px;\">Services, which are dependent on this service: <b style=\"font-weight: normal;\">{6}</b></div>" +
			"<div class=\"row\">{7}</div>" +
		"</div>";
	var serviceGrid;

    return {
    	init: function (grid) {
    		serviceGrid = grid;
    	},

    	renderStatus: function (pVal) {
    		var statusResult;
    		switch (pVal) {
    			case 'Running':
    				statusResult = '<img class="row-img" src="/Orion/Images/icon_run.gif"/>&nbsp;&nbsp;' + pVal;
    				break;
    			case 'Stopped':
    				statusResult = '<img class="row-img" src="/Orion/Images/Cmd-Stopped.png"/>&nbsp;&nbsp;' + pVal;
    				break;
    			default:
    				statusResult = pVal;
    				break;
    		}

    		return statusResult;
    	},

    	renderAssignedApplication: function (pVal, pMd, record) {

    	        if (record.data.MonitoredEntities.length > 0) {
    	            var res = '';
    	            for (var i = 0; i < record.data.MonitoredEntities.length; i++) {
    	                var entitie = record.data.MonitoredEntities[i];

    	                var appStatusImg = '<img lass="row-img" src="/Orion/StatusIcon.ashx?entity=Orion.APM.Application&status=' + entitie.ApplicationStatus + '&size=Small" ></img>';

    	                var app = '<span>' + appStatusImg + '<a href="/Orion/APM/ApplicationDetails.aspx?NetObject=AA:' + entitie.ApplicationId + '" target="_blank"> ' + record.data.MonitoredEntities[i].ApplicationName + ' </a></span></br>';

    	                res += app;
    	            }
    	            return res;
    	        }
    	        else {
    	            if (SW.APM.RealTimeServiceGrid.MonitoredNode.isDemo == false) {
    	                return '<div><a href="#" id="StartMonitoring" onclick="SW.APM.RealTimeServiceGrid.ToolbarHandlers.StartServiceMonitoring(\''
                            + record.data.Name + '\',' + '\'' + record.data.DisplayName + '\');"><img src="/Orion/APM/Images/icon_add.gif" height="16" width="16"/> &nbsp;&nbsp;' + '@{R=APM.Strings; K=APMWEBJS_StartMonitoringThisService; E=js}' + '</a></div>';
    	            }
    	        }
    	},

    	getDetailsTitle: function (record) {
    		return SF("<a href='#'>\"{0}\" Service Details</a>", record.get("Name"));
    	},
    	getDetailsBody: function (record) {
    		var deps = record.get("Dependencies"), depsMessage1 = "None", depsMessage2 = "", that = SW.APM.RealTimeServiceGrid.Renderers;
    		if (deps.length > 0) {
    			depsMessage1 = "";
    			$.each(deps, function () {
    				depsMessage2 += SF("<div class=\"dep-svc-name\">{0}</div>&nbsp;&nbsp;&nbsp;&nbsp;{1}</br>", this.DisplayName, that.renderStatus(this.State));
    			});
    		}
    		return SF(DETAILS_FORMAT,
				record.get("Name"), record.get("DisplayName"), record.get("Description"),
				record.get("ProcessId"), record.get("LogOnAs"), record.get("Path"),
				depsMessage1, depsMessage2
			);
    	}
    };
})();