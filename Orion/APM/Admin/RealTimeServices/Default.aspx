﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_APM_Admin_RealTimeServices_Default"  MasterPageFile="~/Orion/OrionMinReqs.master"%>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="4.0" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" Module="APM" File="Ext4AjaxProxy.js" />
    <orion:Include runat="server" Module="APM" File="CredentialsDialog.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealtimeServices/ServicesGrid.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealtimeServices/ServicesGrid.Renderers.js" />
    <orion:Include runat="server" Module="APM" File="Admin/RealtimeServices/ServiceGrid.ToolbarHandlers.js" />
    
    <link rel="stylesheet" type="text/css" href="/WebEngine/Resources/common/globalstyles.css"/>

	<style type="text/css">
		.rtsm-qtip .row {
			float: left; width: 490px; 
		}
		.rtsm-qtip .bold {
			font-weight: bold;
		}
		.rtsm-qtip .svc-prop-name {
			float: left; width: 85px; 
		}
		.rtsm-qtip .svc-prop-value {
			float: right; width: 400px; text-align: left; 
		}
		.rtsm-qtip .dep-svc-name {
			width: 300px; float: left; padding-left: 20px;
		}
		/*override ext js classes (tool tip arrow size)*/
		.x4-border-box .x4-reset .x4-tip-anchor {
			height: 22px; width: 22px;
		}
		.x4-reset .x4-tip-anchor {
			border-width: 11px;
		} 
		body, table, div, td{
		    font-size: small;
		    font-size: 11px;
        }
        .x-combo-list-item {
		   font-size: 12px;
        }
        .x4-reset .x4-window-header-text-default  {
		    font-size: small;
		    font-size: 12px;
        }
     </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="BodyContent">
    <form id="form1" runat="server">
        <div id="RealTimeServiceGrid">
    </div>
    <asp:ScriptManager runat="server" ID="scman">
        <Services>
            <asp:ServiceReference Path="~/Orion/APM/Services/Services.asmx" />
        </Services>
    </asp:ScriptManager>
    <script type="text/javascript">
        $(document).ready(function () { 
            document.title = '<%= Resources.APMWebContent.APMWEBDATA_RM0_7%>';
            SW.APM.RealTimeServiceGrid.MonitoredNode = <%=MonitoredNode%>; 
            
            if(SW.APM.RealTimeServiceGrid.MonitoredNode.AllowRealTimeServiceExplorer == false) {
                Ext.Msg.show({ title: '<%= Resources.APMWebContent.Permissions_information%>', msg: "<%= Resources.APMWebContent.You_do_not_have_sufficient_rights_to_view_the_ServiceControl_Manager%>", buttons: { ok: '<%= Resources.APMWebContent.Button_OK%>' }, width: 400, height: 150, icon: Ext.MessageBox.INFO });
                return;
            };
            
            SW.APM.RealTimeServiceGrid.createGrid();   
        });
    </script>
    </form>
</asp:Content>