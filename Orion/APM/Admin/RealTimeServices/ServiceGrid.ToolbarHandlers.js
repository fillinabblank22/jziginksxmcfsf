﻿Ext4.namespace('SW.APM.RealTimeServiceGrid');

SW.APM.RealTimeServiceGrid.ToolbarHandlers = (function () {
    var serviceGrid;

    Ext4.define('Services.ServiceErrorDialog', {
        show: function (msg) {
            var dialog = Ext4.create('Ext.window.MessageBox', {
                buttons: [
                    { text: '@{R=APM.Strings;K=APMWEBJS_Retry;E=js}', handler: function () {
                        dialog.close();
                        SW.APM.RealTimeServiceGrid.ReloadStore();
                        startPolling();
                    }
                    },
                    { text: '@{R=APM.Strings;K=APMWEBJS_TryDifferentCredentials;E=js}', width: 150, hidden: SW.APM.RealTimeServiceGrid.MonitoredNode.nodeSubType == 'Agent', handler: function () {
                        dialog.close();
                        SW.APM.RealTimeServiceGrid.ToolbarHandlers.onChangeCredentialsClick();
                        SW.APM.RealTimeServiceGrid.IsPolilingPaused = false;
                    }
                    },
                    { text: '@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}', handler: function () { dialog.close(); } }]

            });
            dialog.show({
                title: '@{R=APM.Strings;K=APMWEBJS_ServiceManagerError;E=js}',
                msg:'<div style=\"font-size: 12px; \">'+ msg + '<br\>'+'</div>',
                icon: Ext.MessageBox.ERROR
            });
            dialog.setWidth(400);
        }
    });

    var startPolling = function () {
        var map = serviceGrid.getDockedComponent('topToolbar').items.map,
            node = SW.APM.RealTimeServiceGrid.MonitoredNode;

        map.pausePolling.setText("Stop polling");
        map.pausePolling.setIcon("/Orion/APM/Images/StatusIcons/ServiceStatus_Paused_16x16.gif");
        map.pausePolling.setHandler(SW.APM.RealTimeServiceGrid.ToolbarHandlers.onPausePollingClick);

        map = serviceGrid.getDockedComponent('bottomToolbar').items.map;
        map.pollingLabel.setText("@{R=APM.Strings;K=APMWEBJS_PollingServicesOn;E=js}"); map.credentialsType.setText("WMI");
        map.overLabel.setText("@{R=APM.Strings;K=APMWEBJS_Over;E=js}"); map.usingLabel.setText("@{R=APM.Strings;K=APMWEBJS_Using;E=js}");
        map.credentialsName.setText('"' + node.credentialsName + '"'); map.credentialsLabel.setText("@{R=APM.Strings;K=APMWEBJS_credentials;E=js}");

        serviceGrid.getDockedComponent('bottomToolbar').doLayout();
        map.pollingLoading.setVisible(true);
        SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true;
        SW.APM.RealTimeServiceGrid.IsPolilingPaused = false;
        var selection = serviceGrid.getSelectionModel().selected ? serviceGrid.getSelectionModel().selected.items : null;
        SW.APM.RealTimeServiceGrid.UpdateToolbarButtons(selection);
    };

    var showGridActionErrorDialog = function (msg) {
        SW.APM.RealTimeServiceGrid.ToolbarHandlers.onPausePollingClick();
        SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;
        var dialog = Ext4.create('Services.ServiceErrorDialog');
        dialog.show(msg);
    };

    var showServiceActionErrorDialog = function (error) {
        Ext.Msg.show({ title: '@{R=APM.Strings;K=JS_Error_Message;E=js}', msg: error, buttons: { ok: '@{R=APM.Strings;K=JS_OK_Button_Text;E=js}' }, width: 400, height: 150, icon: Ext.MessageBox.ERROR });
    };

    var showServicePermissionsErrorDialog = function () {
        Ext.Msg.show({
            title: '@{R=APM.Strings;K=APMWEBJS_PermissionsInformation;E=js}', msg: "@{R=APM.Strings;K=APMWEBJS_YouDoNotHaveSufficientRightsToPerformThisOperation;E=js}", buttons: { ok: '@{R=APM.Strings;K=JS_OK_Button_Text;E=js}' },
            width: 400, height: 150, icon: Ext.MessageBox.INFO
        });
    };
    
    Ext4.define('Services.ServiceWarningDialog', {
        show: function (action, msg, successAction) {
            var isDependencyRestartClicked = false;
            var dialog = Ext4.create('Ext.window.MessageBox', {
                buttons: [{ text: action + ' related services', handler: function () { successAction(); isDependencyRestartClicked = true; dialog.close(); } },
                        { text: 'Cancel', handler: function () { dialog.close(); } }],
                listeners: {
                    hide: function () {
                        if (isDependencyRestartClicked == false) {
                            SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true;
                        }
                    }
                }
            });
            dialog.show({
                title: action + ' related services?',
                msg: '<div style=\"font-size: 12px; \">' + msg + '</div>',
                icon: Ext.MessageBox.WARNING
            });
            dialog.setWidth(420);
            dialog.setHeight(dialog.height + 30);
           
        }
    });

    var showLoadingMask = function (action) {
        var selection = serviceGrid.getSelectionModel().getSelection(),
            actionMessage = action + ' ' + selection[0].data.DisplayName + '.',
            loadingMask = new Ext4.LoadMask(serviceGrid, { msg: actionMessage + " Please wait..." });
        loadingMask.show();
        return loadingMask;
    };

    var processToolbarVisibility = function (isDisable) {
        var map = serviceGrid.getDockedComponent('topToolbar').items.map;
        map.startService.setDisabled(isDisable);
        map.restartService.setDisabled(isDisable);
        map.stopService.setDisabled(isDisable);
        map.useDifferentCredentials.setDisabled(isDisable);
        map.pausePolling.setDisabled(isDisable);
        
    };

    var onCredentialSelected = function (newCredentials) {
        SW.APM.RealTimeServiceGrid.MonitoredNode.credentialsId = newCredentials.id;
        SW.APM.RealTimeServiceGrid.MonitoredNode.credentialsName = newCredentials.name;

        serviceGrid.store.getProxy().extraParams.credentialId = newCredentials.id;
        serviceGrid.getDockedComponent('bottomToolbar').items.map.credentialsName.setText('"' + newCredentials.name + '"');
        serviceGrid.getDockedComponent('bottomToolbar').doLayout();
        SW.APM.RealTimeServiceGrid.ReloadStore();
        startPolling();
    };

    var getServiceByName = function (name, services) {
        var service = null;
        Ext.each(services, function () {
            if (this.Name == name) {
                service = this;
                return service;
            }
        });
        return service;
    };

    var getServiceDependenciesHtml = function (dependencies) {
        var depMsg = '';
        Ext.each(dependencies, function () {
            if (this.AcceptStop == true && this.State == "Running") {
            	depMsg += '<li> -  ' + this.DisplayName + ' (' + this.Name + ')' + '</li>';
            }
        });
        if (depMsg != '') {
            return '<div style=\"max-height: 220px; overflow:auto; overflow-x:hidden;font-size: 12px; \" ><ul>' + depMsg + '</ul></div><br/><br/><br/>';
        } else {
            return depMsg;
        }
    };

    var getServiceRowIndexByName = function (name) {
        var store = serviceGrid.store.data.items;
        var index = 0;
        Ext.each(store, function () {
            if (this.data.Name == name) {
                index = this.index;
                return this.index;
            }
        });
        return index;
    };

    var stopService = function () {
        processToolbarVisibility(true);
        SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;

        var row = serviceGrid.getSelectionModel().selected,
            node = SW.APM.RealTimeServiceGrid.MonitoredNode,
            serviceName = row.items[0].data.Name,
            rowIndex = getServiceRowIndexByName(row.items[0].data.Name);

        if (row.items[0].data.Unmanaged == true) {
            showServiceActionErrorDialog('The <b>' + row.items[0].data.DisplayName + '</b>' + ' service cannot be stopped because this is a critical system service. Stopping this service would cause system instability.');
            return;
        }
        var loadingMask = showLoadingMask("Stopping");
        SolarWinds.APM.Web.Services.SystemServicesWebService.StopService(
            node.Id,
            node.credentialsId,
            row.items[0].data.Name,
        //success
            function (res) {

                var service = getServiceByName(serviceName, res.Services);
                if (res.ErrorMessage != null) {
                    showServiceActionErrorDialog(res.ErrorMessage);
                    setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                }
                else {
                    if (service != null && service.ActionExitCode != 0 && service.ActionMessage) {
                        showServiceActionErrorDialog(service.ActionMessage);
                        setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                    }
                    else {
                        setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                        var store = serviceGrid.getStore();
                        store.loadData(res.Services);
                        SW.APM.RealTimeServiceGrid.SortGrid();
                    }
                }
                processToolbarVisibility(false);

                var selMod = serviceGrid.getSelectionModel();
                selMod.select(rowIndex);
               
                SW.APM.RealTimeServiceGrid.UpdateToolbarButtons(serviceGrid.getSelectionModel().selected.items);
                loadingMask.hide();

            },
        //error
            function (error) {
                SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true;
                showServiceActionErrorDialog(error._message);
            });
    };

    var restartService = function () {
        processToolbarVisibility(true);
        SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;

        var row = serviceGrid.getSelectionModel().selected,
            node = SW.APM.RealTimeServiceGrid.MonitoredNode,
            serviceName = row.items[0].data.Name,
            rowIndex = getServiceRowIndexByName(row.items[0].data.Name);

        if (row.items[0].data.Unmanaged == true) {
            showServiceActionErrorDialog(' The <b>' + row.items[0].data.DisplayName + '</b>' + ' service cannot be restarted because this is a critical system service. Restarting this service would cause system instability.');
            return;
        }
        var loadingMask = showLoadingMask("Restarting");
        SolarWinds.APM.Web.Services.SystemServicesWebService.RestartService(
            node.Id,
            node.credentialsId,
            row.items[0].data.Name,
        //success
            function (res) {
                var service = getServiceByName(serviceName, res.Services);

                if (res.ErrorMessage != null) {
                    showServiceActionErrorDialog(res.ErrorMessage);
                    setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                }
                else {
                    if (service != null && service.ActionExitCode != 0 && service.ActionMessage) {
                        showServiceActionErrorDialog(service.ActionMessage);
                        setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                    }
                    else {
                        setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                        var store = serviceGrid.getStore();
                        store.loadData(res.Services);
                        SW.APM.RealTimeServiceGrid.SortGrid();
                    }
                }
                processToolbarVisibility(false);
                var selMod = serviceGrid.getSelectionModel();
                selMod.select(rowIndex);
                SW.APM.RealTimeServiceGrid.UpdateToolbarButtons(serviceGrid.getSelectionModel().selected.items);
                loadingMask.hide();
            },
        //error
            function (error) {
                SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true;
                showServiceActionErrorDialog(error._message);
            });

    };
    return {
        init: function (grid) {
            serviceGrid = grid;
        },
        GetServiceRowIndexByName: getServiceRowIndexByName,
        ShowErrorDialog: showGridActionErrorDialog,
        StartServiceMonitoring: function (serviceName, serviceDisplayName) {
            var node = SW.APM.RealTimeServiceGrid.MonitoredNode;

            SolarWinds.APM.Web.Services.SystemServicesWebService.InitWizardSession(
                node.Id,
                node.credentialsId,
                serviceName,
                serviceDisplayName,
                function () {
                    window.open('/Orion/APM/Admin/MonitorLibrary/AppFinder/EditMonitorProperties.aspx', 'ServicesMonitorWizard');
                },
                function (error) {
                    Ext.Msg.show({
                        title: '@{R=APM.Strings;K=JS_Error_Message;E=js}',
                        msg: error._message,
                        buttons: { ok: '@{R=APM.Strings;K=JS_OK_Button_Text;E=js}' },
                        icon: Ext.MessageBox.ERROR
                    });
                }
              );
        },

        onStartServiceClick: function () {
            if (SW.APM.RealTimeServiceGrid.MonitoredNode.isDemo == true) {
                DemoModeAction("Service_Control_Manager_Start_Service");
                return;
            }
            
            var nodeSettings = SW.APM.RealTimeServiceGrid.MonitoredNode;
            
            if (nodeSettings.AllowRealTimeServiceManage == false) {
                showServicePermissionsErrorDialog();
                return;
            }
            
            SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;
            processToolbarVisibility(true);

            var row = serviceGrid.getSelectionModel().selected,
                node = SW.APM.RealTimeServiceGrid.MonitoredNode,
                serviceName = row.items[0].data.Name,
                rowIndex = getServiceRowIndexByName(row.items[0].data.Name);

            if (row.items[0].data.Unmanaged == true) {
                showServiceActionErrorDialog('<b>' + row.items[0].data.DisplayName + '</b>' + ' service can not be started, because it included in unmanaged list.');
                return;
            }

            var loadingMask = showLoadingMask("Starting");
            SolarWinds.APM.Web.Services.SystemServicesWebService.StartService(
            node.Id,
            node.credentialsId,
            row.items[0].data.Name,
            //success
                 function (res) {
                     var service = getServiceByName(serviceName, res.Services);
                     if (res.ErrorMessage != null) {
                         showServiceActionErrorDialog(res.ErrorMessage);
                         setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                     }
                     else {
                         if (service != null && service.ActionExitCode != 0 && service.ActionMessage) {
                             showServiceActionErrorDialog(service.ActionMessage);
                             setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                         }
                         else {
                             setTimeout(function () { SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true; }, 30000);
                             var store = serviceGrid.getStore();
                             store.loadData(res.Services);
                             SW.APM.RealTimeServiceGrid.SortGrid();
                         }
                     }
                     processToolbarVisibility(false);
                     var selMod = serviceGrid.getSelectionModel();
                     selMod.select(rowIndex);
                     SW.APM.RealTimeServiceGrid.UpdateToolbarButtons(serviceGrid.getSelectionModel().selected.items);
                     loadingMask.hide();
                 },
            //error
            function (error) {
                SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true;
                showServiceActionErrorDialog(error._message);
            });
        },

        onStopServiceClick: function () {
            if (SW.APM.RealTimeServiceGrid.MonitoredNode.isDemo == true) {
                DemoModeAction("Service_Control_Manager_Stop_Service");
                return;
            }
            
            var nodeSettings = SW.APM.RealTimeServiceGrid.MonitoredNode;

            if (nodeSettings.AllowRealTimeServiceManage == false) {
                showServicePermissionsErrorDialog();
                return;
            }
            
            var row = serviceGrid.getSelectionModel().selected,
                rowInfo = row.items[0].data,
                dialog = Ext4.create('Services.ServiceWarningDialog');

            if (rowInfo.Dependencies && rowInfo.Dependencies.length > 0) {
                var msg = '<b>Do you want to stop "' + rowInfo.DisplayName + '" and all dependent services?</b><br/><br/> <div style="padding-left:50px;font-size: 12px;">The following services are dependent and will be stopped:<br/>';
                var depHtml = getServiceDependenciesHtml(rowInfo.Dependencies);

                if (depHtml != '') {
                    var resMsg = msg + depHtml + '</div>';
                    SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;
                    dialog.show('@{R=APM.Strings;K=APMWEBJS_Stop;E=js}', resMsg, stopService);
                } else {
                    stopService();
                }
            }
            else {
                stopService();
            }
        },

        onRestartServiceClick: function () {
            if (SW.APM.RealTimeServiceGrid.MonitoredNode.isDemo == true) {
                DemoModeAction("Service_Control_Manager_Restart_Service");
                return;
            }
            
            var nodeSettings = SW.APM.RealTimeServiceGrid.MonitoredNode;

            if (nodeSettings.AllowRealTimeServiceManage == false) {
                showServicePermissionsErrorDialog();
                return;
            }
            var row = serviceGrid.getSelectionModel().selected,
                rowInfo = row.items[0].data,
                dialog = Ext4.create('Services.ServiceWarningDialog');

            if (rowInfo.Dependencies && rowInfo.Dependencies.length > 0) {
                var msg = '<b>Do you want to restart "' + rowInfo.DisplayName + '" and all dependent services?</b><br/><br/> <div style="padding-left:50px;font-size: 12px;  ">The following services are dependent and will be restarted:<br/>';

                var depHtml = getServiceDependenciesHtml(rowInfo.Dependencies);

                if (depHtml != '') {
                    var resMsg = msg + depHtml + '</div>';
                    SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;
                    dialog.show('@{R=APM.Strings;K=APMWEBJS_Restart;E=js}', resMsg, restartService);
                } else {
                    restartService();
                }
            }
            else {
                restartService();
            }
        },

        onChangeCredentialsClick: function () {
            var nodeSettings = SW.APM.RealTimeServiceGrid.MonitoredNode;
            SW.APM.CredentialsDialog.show({
                addNoneCred: nodeSettings.isLocalHost,
                isRemote: nodeSettings.isRemoteForCredentialDialog,
                onSuccess: onCredentialSelected,
                addInheritedWindows: true
            });
        },

        onStartPollingClick: function () {
            startPolling();
            SW.APM.RealTimeServiceGrid.IsPolilingPaused = false;
            SW.APM.RealTimeServiceGrid.AllowGridStoreReload = true;
        },

        onPausePollingClick: function () {
            var map = serviceGrid.getDockedComponent('topToolbar').items.map;
            map.pausePolling.setText("Start polling");
            map.pausePolling.setIcon("/Orion/APM/Images/ProcessMonitor/icon_play.png");
            map.pausePolling.setHandler(SW.APM.RealTimeServiceGrid.ToolbarHandlers.onStartPollingClick);
            SW.APM.RealTimeServiceGrid.IsPolilingPaused = true;

            map = serviceGrid.getDockedComponent('bottomToolbar').items.map;
            map.pollingLabel.setText("Polling on"); map.credentialsType.setText("");
            map.overLabel.setText("paused"); map.usingLabel.setText("");
            map.credentialsName.setText(""); map.credentialsLabel.setText("");

            map.pollingLoading.setVisible(false);
            serviceGrid.getDockedComponent('bottomToolbar').doLayout();
            SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;
            SW.APM.RealTimeServiceGrid.UpdateToolbarButtons();
        }

    };

})();