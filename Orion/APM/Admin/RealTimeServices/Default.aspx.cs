﻿using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using System;
using System.Globalization;
using System.Web;
using System.Web.UI.HtmlControls;

public partial class Orion_APM_Admin_RealTimeServices_Default : System.Web.UI.Page
{
	private bool nodeInitialize = false;
	private Node node;

	private int NodeId
	{
		get
		{
			var id = 0;
			int.TryParse(Request.QueryString["NodeId"], out id);

			return id;
		}
	}

	private Node Node
	{
		get
		{
			if (!nodeInitialize)
			{
				nodeInitialize = true;

				var id = NodeId;
				if (id > 0)
				{
					using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
					{
						try
						{
							node = bl.GetNode(id);
						}
						catch { }
					}
				}
			}
			if (node == null)
			{
				throw new ArgumentMissingException("Node doesn't exists.");
			}
			return node;
		}
	}

	protected String MonitoredNode
	{
	    get
	    {
	        var nodeId = NodeId;
	        var node = Node;

	        var nodeStatusIconPath = string.Format(
	            CultureInfo.InvariantCulture,
	            "/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={0}&status={1}&size=Small",
	            node.Id,
	            node.Status);

	        var credentialsName = String.Empty;
	        var credentialsId = 0;

	        if (node.NodeSubType == NodeSubType.Agent)
	        {
	            credentialsName = "[Inherit Windows credential from node]";
	        }
	        else
	        {
	            var urlCredentials = Request.QueryString["CredentialsId"];
	            if (urlCredentials != null)
	            {
                    int.TryParse(urlCredentials, out credentialsId);
	            }

	            CredentialSet credentials;
	            using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
	            {
	                credentials = credentialsId <= 0
	                                  ? businessLayer.GetWorkingWindowsCredentialForNode(nodeId)
	                                  : businessLayer.GetCredentialsById(credentialsId);
	            }

	            if (credentials != null)
	            {
	                credentialsName = credentials.Name;
	                credentialsId = credentials.Id;
	            }

	            if ((credentials == null && credentialsId == -3)
	                || (credentialsId == 0 && node.NodeSubType == NodeSubType.WMI))
	            {
	                credentialsId = -3;
	                credentialsName = "[Inherit Windows credential from node]";
	            }

	            if (credentialsId == 0)
	            {
	                credentialsName = "[None]";
	            }
	        }

	        //credentials dialog settings
            IHostHelper hostHelper = new HostHelper();
	        Uri uri = new Uri(Request.Url.AbsoluteUri, UriKind.Absolute);
            var isLocalHost = hostHelper.IsLocal(node.IpAddress);
	        var isRemoteForCredentialDialog = (uri.Scheme.Equals("http") && !Request.IsLocal);
	        var isDemo = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
	        var nodeSubType = node.NodeSubType.ToString();

	        var res = new
	            {
	                node.Id,
	                credentialsId,
	                node.Name,
	                node.Status,
	                nodeSubType,
	                credentialsName,
	                nodeStatusIconPath,
	                isLocalHost,
	                isRemoteForCredentialDialog,
	                isDemo,
	                ApmRoleAccessor.AllowRealTimeServiceExplorer,
	                ApmRoleAccessor.AllowRealTimeServiceManage
	            };

	        return SolarWinds.APM.Web.Utility.JsHelper.Serialize(res);
	    }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		ValidateRight();

		if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion < 10)
		{
			Header.Controls.AddAt(0, new HtmlMeta { HttpEquiv = "X-UA-Compatible", Content = "IE=8" });
		}

        ApmBaseResource.EnsureApmScripts();
    }

	protected void ValidateRight()
	{
		if (!ApmRoleAccessor.AllowRealTimeServiceExplorer)
		{
			Server.Transfer(InvariantString.Format("/Orion/Error.aspx?Message={0}", HttpUtility.UrlEncode("You do not have sufficient rights to view the Service Control Manager.")));
		}

		if (Node == null)
		{
			return;
		}

		var result = SwisProxy.ExecuteQuery(InvariantString.Format(@"SELECT n.NodeName FROM Orion.Nodes AS n WHERE n.NodeID = {0}", NodeId));
		if (result.Rows.Count == 0)
		{
			Server.Transfer(InvariantString.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", NodeId));
		}
	}
}