﻿
Ext4.namespace('SW.APM');

SW.APM.RealTimeServiceGrid = (function () {
	var grid, gridLoadMask, toolTip, task;
	var allowGridStoreReload = true;
    var sorter = null;
    var sortDirection = "ASC";
    var isPageReloaded = true;
    
    function getUrlParameter(sParam) {
        var sPageUrl = window.location.search.substring(1);
        var sUrlVariables = sPageUrl.split('&');
        for (var i = 0; i < sUrlVariables.length; i++) {
            var sParameterName = sUrlVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    };
    
    function showICMPNodeError(errorMessage) {
        SW.APM.RealTimeServiceGrid.ToolbarHandlers.onPausePollingClick();
        SW.APM.RealTimeServiceGrid.AllowGridStoreReload = false;
        Ext.Msg.show({
            title: '@{R=APM.Strings;K=APMWEBJS_ServiceManagerError;E=js}',
            msg: errorMessage,
            buttons: { yes: '@{R=APM.Strings;K=APMWEBJS_AK1_54;E=js}', cancel: '@{R=APM.Strings;K=APMWEBJS_AK1_41;E=js}' },
            icon: Ext.MessageBox.ERROR,
            fn: function(dr) {
                if (dr == 'yes') {
                    SW.APM.RealTimeServiceGrid.ToolbarHandlers.onChangeCredentialsClick();
                    SW.APM.RealTimeServiceGrid.IsPolilingPaused = false;
                }
            }
        });
    };
    
    var reloadStore = function () {
        var store = grid.getStore();
        var rowIndex = 0;
        if (grid.getSelectionModel()) {
            var selectedRow = grid.getSelectionModel().selected;
            if (selectedRow.items.length > 0 && selectedRow.items[0].data) {
                rowIndex = SW.APM.RealTimeServiceGrid.ToolbarHandlers.GetServiceRowIndexByName(selectedRow.items[0].data.Name);
            }
        }
        
        store.load({
            callback: function (records, operation) {
                var obj = Ext4.decode(operation.response.responseText);
                sortGrid();
                if (obj.d.ErrorMessage != null) {
                    gridLoadMask.hide();
                    SW.APM.RealTimeServiceGrid.ToolbarHandlers.ShowErrorDialog(obj.d.ErrorMessage);
                }

                if (grid.getSelectionModel() && rowIndex > 0) {
                    grid.getSelectionModel().select(rowIndex);
                } 
                    // restore grid state in ie
                if ($.browser.msie && parseInt($.browser.version, 10) != 10 && isPageReloaded == true) {
                    if (getUrlParameter('sort') && getUrlParameter('dir')) {
                        grid.store.sort(getUrlParameter('sort'), getUrlParameter('dir'));
                    }

                    var index = getUrlParameter('SelectedRow');
                    if (index && grid.getSelectionModel() && index > 0) {
                        grid.getSelectionModel().select(parseInt(index));
                        if (index > 10) {
                            grid.getView().getNode(parseInt(index - 10)).scrollIntoView();
                        }
                    }
                    isPageReloaded = false;
                }

                //we should wait for service loading
                if (obj.d.Services.length == 0 && obj.d.ErrorMessage == null) gridLoadMask.show();
                else gridLoadMask.hide();
            }
        });
    };

    var sortGrid = function () {
        if (sorter = grid.store.sorters.items[0]) {
            var sorters = grid.store.sorters;
            sorters.events.add = false;
            sorter = grid.store.sorters.items[0];

            if (sorter.property == 'DisplayName' && sorter.direction.toUpperCase() == 'DESC') {
                grid.store.sort('DisplayName', 'DESC');
                sortDirection = "DESC";
                sorters.addListener("add", sortGrid);
                return;
            }
            
            if (sorter.property == 'DisplayName' && sorter.direction.toUpperCase() == 'ASC') {
                sortDirection = "ASC";
            }

            grid.store.sort('DisplayName', sortDirection);
            grid.store.sort(sorter.property, sorter.direction);
            sorters.add(sorter);
            sorters.addListener("add", sortGrid);
        }
    };
    
    var updateToolbarButtons = function (selectedRow) {
        var map = grid.getDockedComponent('topToolbar').items.map;
        if (!selectedRow || !Ext4.isDefined(selectedRow[0]) || !selectedRow[0].data) {
            map.startService.setDisabled(true);
            map.restartService.setDisabled(true);
            map.stopService.setDisabled(true);
        }
        else {
        	if (selectedRow[0].data.StartMode == "Disabled") {
                map.startService.setDisabled(true);
                map.restartService.setDisabled(true);
                map.stopService.setDisabled(true);
            }
            else {
                switch (selectedRow[0].data.State) {
                    case 'Running':
                        map.startService.setDisabled(true);
                        map.restartService.setDisabled(false);
                        map.stopService.setDisabled(false);
                        break;
                    case 'Stopped':
                        map.startService.setDisabled(false);
                        map.restartService.setDisabled(true);
                        map.stopService.setDisabled(true);
                        break;
                }
            }
        }

        if (SW.APM.RealTimeServiceGrid.IsPolilingPaused == true) {
            map.startService.setDisabled(true);
            map.restartService.setDisabled(true);
            map.stopService.setDisabled(true);
            map.useDifferentCredentials.setDisabled(true);
        }
        else {
            map.useDifferentCredentials.setDisabled(false);
        }
        
        if (SW.APM.RealTimeServiceGrid.MonitoredNode.isDemo == true) {
            map.useDifferentCredentials.setDisabled(true);
            map.pausePolling.setDisabled(true);     
        }
    };

    var showToolTip = function (el, evt, record) {
        
        if (grid.getView().mouseOverItem == null) return;
        
    	if (toolTip == null) {
    		var toolTipConfig = {
    			anchor: "left", anchorOffset: 10,
    			maxWidth: 500, width: 500, showDelay: 1000,
    			targetXY: evt.getXY(), trackMouse: true,
    			target: el, title: "title", html: "html",
    			autoHide: false
    		};
    		toolTip = Ext4.create("Ext.tip.ToolTip", toolTipConfig);
    	}
    	toolTip.setTarget(el);
    	toolTip.targetXY = evt.getXY();
    	toolTip.setTitle(SW.APM.RealTimeServiceGrid.Renderers.getDetailsTitle(record));
    	toolTip.update(SW.APM.RealTimeServiceGrid.Renderers.getDetailsBody(record));
    	toolTip.show();
    	toolTip.doLayout();
    };

    return {
    	UpdateToolbarButtons: updateToolbarButtons,
    	ReloadStore: reloadStore,
    	AllowGridStoreReload: allowGridStoreReload,
    	IsPolilingPaused: false,
    	PollingTask: task,
    	SortGrid:sortGrid ,
    	createGrid: function () {
    		var that = SW.APM.RealTimeServiceGrid;
    		var monitoredNodeSettings = that.MonitoredNode;

    		Ext4.Ajax.timeout = 5 * 60 * 1000;
    		Ext4.USE_NATIVE_JSON = true;
     
    		var proxy = Ext4.create('AspWebAjaxProxy', {
    			url: "/Orion/APM/Services/Services.asmx/GetServicesInfo",
    			headers: { 'Content-Type': 'application/json; charset=utf-8' },
    			actionMethods: { read: 'POST' },
    			extraParams: {
    				nodeId: monitoredNodeSettings.Id,
    				credentialId: monitoredNodeSettings.credentialsId
    			},
    			reader: { type: 'json', root: 'd.Services' }
    		});

            function escapeValue(value, record) {
				if(!value)
					return value;
				return _.escape(value);
			}

            function escapeDependencies(items, record) {
                $.each(items, function () {
                    this.Name = escapeValue(this.Name);
                    this.DisplayName = escapeValue(this.DisplayName);
                });
                return items;
            }

    		var dataStore = Ext4.data.Store(
                {
                	id: 'gridStore',
                	fields: [
                        { name: 'DisplayName', convert: escapeValue },
                        { name: 'Description', convert: escapeValue },
                        { name: 'State' },
                        { name: 'StartMode' },
                        { name: 'MonitoredEntities' },
                        { name: 'Name', convert: escapeValue },
                        { name: 'Dependencies', convert: escapeDependencies },
                        { name: 'Unmanaged' },
                        { name: 'Path' },
                        { name: 'ProcessId' },
                        { name: 'LogOnAs' }
                	],
                	proxy: proxy,
                	sorters: [{ property: 'DisplayName', direction: 'asc' }]
                }
			);

    		var width = Ext.getBody().getViewSize().width - 5;
    		var height = Ext.getBody().getViewSize().height - 15;
    		Ext4.QuickTips.init();

    		var topToolbar = Ext4.create('Ext.toolbar.Toolbar', {
    			id: 'topToolbar',
    			items: [
                    { id: 'startService', text: '@{R=APM.Strings;K=APMWEBJS_StartService;E=js}', icon: '/Orion/APM/Images/ProcessMonitor/icon_play.png', handler: that.ToolbarHandlers.onStartServiceClick }, '-',
                    { id: 'restartService', text: '@{R=APM.Strings;K=APMWEBJS_RestartService;E=js}', icon: '/Orion/APM/Images/restart_service.png', handler: that.ToolbarHandlers.onRestartServiceClick }, '-',
                    { id: 'stopService', text: '@{R=APM.Strings;K=APMWEBJS_StopService;E=js}', icon: '/Orion/APM/Images/stop_service.png', handler: that.ToolbarHandlers.onStopServiceClick }, '-',
                    { id: 'pausePolling', text: '@{R=APM.Strings;K=APMWEBJS_PausePolling;E=js}', icon: '/Orion/APM/Images/StatusIcons/ServiceStatus_Paused_16x16.gif', handler: that.ToolbarHandlers.onPausePollingClick },
    			    { id: 'useDifferentCredentialsSeparator', xtype: 'tbseparator', hidden: monitoredNodeSettings.nodeSubType == 'Agent' },
                    { id: 'useDifferentCredentials', text: '@{R=APM.Strings;K=APMWEBJS_UseDifferentCredentials;E=js}', icon: '/Orion/images/icon_assign_cred_16x16.gif', handler: that.ToolbarHandlers.onChangeCredentialsClick, hidden: monitoredNodeSettings.nodeSubType == 'Agent' }, '->',
                    { xtype: 'label', html: '<img src="/Orion/APM/Images/logo_SW_small.png"/>' }
    			]
    		});

    		var buttomToolbar = Ext4.create('Ext.toolbar.Toolbar', {
    			id: 'bottomToolbar',
    			items: ['->',
                    { id: 'pollingLoading', xtype: 'image', src: '/Orion/APM/Images/loading_gen_16x16.gif',width:16, height:16 },
                    { id: 'pollingLabel', xtype: 'tbtext', text: 'Polling Services on' },
    			    { xtype: 'image', src: monitoredNodeSettings.nodeStatusIconPath, width:16, height:16},
                    { xtype: 'tbtext', text: monitoredNodeSettings.Name },
                    { id: 'overLabel', xtype: 'tbtext', text: 'over' },
    			    { id: 'credentialsType', xtype: 'tbtext', text: monitoredNodeSettings.nodeSubType == "Agent" ? "Agent" : "WMI" },
                    { id: 'usingLabel', xtype: 'tbtext', text: 'using' },
                    { id: 'credentialsName', xtype: 'tbtext', text: '"' + monitoredNodeSettings.credentialsName + '"' },
                    { id: 'credentialsLabel', xtype: 'tbtext', text: '@{R=APM.Strings;K=APMWEBJS_credentials;E=js}' }
    			]
    		});
    	    var toolTipTimeout = 0;
    		grid = Ext4.create('Ext.grid.Panel', {
    			id: 'serviceGrid',
    			store: dataStore,
    			width: width,
    			renderTo: "RealTimeServiceGrid",
    			scroll: false,
    			viewConfig: { style: { overflow: 'auto', overflowX: 'hidden' }, loadingText: 'Loading Services. Please wait...' },
    			autoScroll: true,
    			height: height,
    			loadMask: true,
    			selModel: { mode: 'SINGLE' },
    			columns: [
                    { header: '@{R=APM.Strings;K=APMWEBJS_TM0_60;E=js}', dataIndex: 'DisplayName', width: 200 },
                    { header: '@{R=APM.Strings;K=APMWEBJS_TM0_70;E=js}', dataIndex: 'Description', width: 300 },
                    { header: '@{R=APM.Strings;K=APMWEBJS_TM0_26;E=js}', dataIndex: 'State', width: 100, renderer: that.Renderers.renderStatus },
                    { header: '@{R=APM.Strings;K=APMWEBJS_StartupType;E=js}', dataIndex: 'StartMode', width: 100 },
                    { header: '@{R=APM.Strings;K=APMWEBJS_AK1_31;E=js}', sortable: false, flex: 1, renderer: that.Renderers.renderAssignedApplication }
    			],
    			stripeRows: true,
    			listeners: {
    			    itemmouseenter: function (view, record, item, index, evt, eOpts) {
    			        if (toolTip != null) toolTip.hide();

    			        var column = evt.getTarget(this.view.cellSelector).cellIndex;
    			        if (Ext.isDefined(column) && (column == 0 || column == 1)) {
    			            if (toolTipTimeout != 0) {
    			                clearTimeout(toolTipTimeout);
    			            }
    			            toolTipTimeout = setTimeout(function () {
    			                showToolTip(item, evt, record);
    			            }, 1000);
    			        } else {
    			            if (toolTip != null) toolTip.hide(); 
    			        }
    				}
    			}
    		});

    		topToolbar.dock = 'top';
    		buttomToolbar.dock = 'bottom';
    		grid.addDocked(topToolbar);
    		grid.addDocked(buttomToolbar);

    		that.Renderers.init(grid);
    		that.ToolbarHandlers.init(grid);
            gridLoadMask = new Ext4.LoadMask(grid, { msg: "@{R=APM.Strings;K=APMWEBJS_LoadingServicesPleaseWait;E=js}" });
    		updateToolbarButtons(grid);

    		grid.getSelectionModel().on('selectionchange', function (sm, selectedRows) {
    			updateToolbarButtons(selectedRows);
    		});
    	
    		Ext.EventManager.onWindowResize(function () {
    			var windowWidth = Ext.getBody().getViewSize().width > 900 ? Ext.getBody().getViewSize().width : 900;
    			grid.setWidth(windowWidth - 5);
    			grid.setHeight(Ext.getBody().getViewSize().height - 15);
    		});
    	   
    	    grid.store.sorters.addListener("add", sortGrid);
    		
            task = {
    		    run: function () {
    		        if (monitoredNodeSettings && monitoredNodeSettings.nodeSubType == 'ICMP' && (monitoredNodeSettings.credentialsId == -3 || monitoredNodeSettings.credentialsId == 0)) {
    		            showICMPNodeError('No credentials were found for the selected ICMP node. Please select Windows credentials manually. </br>Consider promoting the selected node to WMI to prevent this message from reappearing.');
    		            return;
    		        }
    		        
    			    if (SW.APM.RealTimeServiceGrid.AllowGridStoreReload) {
    					reloadStore(grid);
    				}
    			},
    			interval: 30000 //(1 second = 1000)
    		};
    	
    		SW.APM.RealTimeServiceGrid.PollingTask = task;
    		Ext4.TaskManager.start(task);
    	
    		if ($.browser.msie && parseInt($.browser.version, 10) != 10) {
    		    var time = 30 * 60 * 1000;
               setTimeout(refresh, time);
    		}
    	    
    		function refresh() {
    		    var selectedRow = grid.getSelectionModel().selected, selectedRowUrl = '';

    		    if (selectedRow.items[0] && selectedRow.items[0].data) {
    		        var rowIndex = SW.APM.RealTimeServiceGrid.ToolbarHandlers.GetServiceRowIndexByName(selectedRow.items[0].data.Name);
    		        selectedRowUrl = '&SelectedRow=' + rowIndex;
    		    }

    		    var sortClumn = grid.store.sorters.items[0], sortBy = "", dir = "";
    		    
    		    if (sortClumn.property != 'DisplayName') {
    		        sortBy = '&sort=' + sortClumn.property;
    		        dir = '&dir=' + sortClumn.direction;
    		    }
    		    
    		    var pathname = window.location.pathname + '?NodeId=' + SW.APM.RealTimeServiceGrid.MonitoredNode.Id + '&CredentialsId=' + SW.APM.RealTimeServiceGrid.MonitoredNode.credentialsId + selectedRowUrl + sortBy + dir;
    		    window.location.href = pathname;
    		}
    	}
    };
})();
