﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Configuration;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Logging;

public partial class Orion_APM_Admin_Edit_Edit : SolarWinds.APM.Web.UI.ApmMasterPage, SolarWinds.APM.Web.EditPage.IEditPageConfiguration
{
    private readonly Log log = new Log();

    private List<CustomEditorElement> plugins;

    #region Properties

    protected string PageTypeName
    {
        get { return (this.Page == null) ? "" : this.Page.GetType().BaseType.FullName; }
    }

    protected Boolean IsApplication
    {
        get { return this.PageTypeName.EqualsInvariant("Orion_APM_Admin_Edit_EditApplication") || this.PageTypeName.EqualsInvariant("Orion_APM_Admin_Edit_EditApplicationItem"); }
    }

    protected Boolean IsTemplate
    {
        get { return this.PageTypeName.EqualsInvariant("Orion_APM_Admin_Edit_EditTemplate"); }
    }

	protected String ComponentDefinitionsSerialized
	{
		get
		{
			var defs = APMCache.ComponentDefinitions
				.ToDictionary(x => x.Key.ToString(), x => x.Value);
			return JsHelper.Serialize(defs);
		}
	}

	protected long SelectedComponentId
	{
		get
		{
			long id = long.MinValue;
			long.TryParse(Request.QueryStringOrForm("selected"), NumberStyles.Integer, CultureInfo.InvariantCulture, out id);
			return id;
		}
	}

	protected String ReturnUrlEncoded
	{
		get
		{
            var url = ReferrerRedirector.GetRedirectionUrl(this.Request.Url, ViewState);
			if (string.IsNullOrWhiteSpace(url)) 
			{
				return string.Empty;
			}
			return ReferrerRedirector.StringToBase64(url);
		}
	}

    protected string CustomEditorListSerialized
    {
        get
        {
            var editors = GetPluginsFromConfiguration();

            if (editors.Count == 0) return "[]";
            return JsHelper.Serialize(editors.Select(e => new { ptype = e.JsCustomEditorClassName, apptype = e.CustomApplicationType }));
        }
    }

	#endregion

	#region Evet Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			ReferrerRedirector.Initialize(ViewState);
		}
	}

	protected void OnButtonSubmit_Click(Object sender, EventArgs e)
	{
		if (IsTemplate)
		{
			ReferrerRedirector.Redirect(ViewState, "~/Orion/APM/Admin/ApplicationTemplates.aspx", true);
		}
		else if (IsApplication)
		{
			ReferrerRedirector.Redirect(ViewState, "~/Orion/APM/Admin/Applications/Default.aspx", true);
		}
		else
		{
			ReferrerRedirector.Redirect(ViewState, "~/Orion/APM/Admin/");
		}
	}

	protected void OnButtonCancel_Click(Object sender, EventArgs e)
	{
		var mode = Request.QueryString["Mode"];
		if (!string.IsNullOrWhiteSpace(mode) && mode.Trim() == "1"/*create new template*/)
		{
		    var id = this.TryParseId();
		    if (id != null && id > 0)
			{
				if (IsTemplate)
				{
					using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
					{
						bl.DeleteApplicationTemplates(new[] { id.Value });
					}
				}
				else if (IsApplication)
				{
					using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
					{
						bl.DeleteApplications(new[] { id.Value });
					}
				}
			}
		}
		OnButtonSubmit_Click(null, EventArgs.Empty);
	}

	protected void OnButtonBrowseMonitors_Click(Object sender, EventArgs e) 
	{
		var entityType = "AT";
		if (IsApplication)
		{
			entityType = "AA";
		}
		
		var currUrl = Request.Url.PathAndQuery;
		if (currUrl.IndexOf("ReturnUrl", StringComparison.InvariantCultureIgnoreCase) == -1) 
		{
            var retUrl = ReferrerRedirector.GetRedirectionUrl(this.Request.Url, ViewState);
			if (string.IsNullOrWhiteSpace(retUrl))
			{
				retUrl = "~/Orion/APM/Admin/";
			}
			currUrl = string.Format("{0}&ReturnUrl={1}", currUrl, ReferrerRedirector.StringToBase64(retUrl));
		}

		var url = string.Format("~/Orion/APM/Admin/MonitorLibrary/AppFinder/SelectMonitorType.aspx?EntityToUpdate={0}:{1}&EditPageUrl={2}",
			entityType, Request.QueryString["ID"], ReferrerRedirector.StringToBase64(currUrl)
		);

		Response.Redirect(url);
	}
	
    private int? TryParseId()
    {
        int id;
        return int.TryParse(Request.QueryString["id"], NumberStyles.Integer, CultureInfo.InvariantCulture, out id) ? id : (int?)null;
    }

    #endregion

    #region Plugins support

    public List<CustomEditorElement> GetPluginsFromConfiguration()
    {
        if (plugins == null)
        {
            // we don't need to be thread safe, because only master and page access to the method (sequentially)
            plugins = new List<CustomEditorElement>();
            var configSection = this.GetConfigurationSection();
            if (configSection != null)
            {
                plugins = configSection
                    .CustomEditors
                    .OfType<CustomEditorElement>()
                    .Where(ce => (ce != null) && !string.IsNullOrWhiteSpace(ce.Src))
                    .ToList();
            }
        }
        return plugins;
    }

    protected EditPageConfigurationSection GetConfigurationSection()
    {
        try
        {
            return EditPageConfigurationSection.Config;
        }
        catch (Exception exc)
        {
            log.ErrorFormat("Cannot load custom application editors: {0}", exc);
            return null;
        }
    }

    #endregion
}
