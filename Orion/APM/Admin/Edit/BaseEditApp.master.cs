﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Configuration;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Logging;

public partial class Orion_APM_Admin_Edit_BaseEditApp : SolarWinds.APM.Web.UI.ApmMasterPage, SolarWinds.APM.Web.EditPage.IEditPageConfiguration
{
    private readonly Log log = new Log();

    private List<CustomEditorElement> plugins;

    #region Properties

	protected String ComponentDefinitionsSerialized
	{
		get
		{
			var defs = APMCache.ComponentDefinitions
				.ToDictionary(x => x.Key.ToString(), x => x.Value);
			return JsHelper.Serialize(defs);
		}
	}

	protected Int64 SelectedComponentId
	{
		get
		{
			var id = Int64.MinValue;
			Int64.TryParse(Request.QueryStringOrForm("selected"), NumberStyles.Integer, CultureInfo.InvariantCulture, out id);
			return id;
		}
	}

	protected String ReturnUrlEncoded
	{
		get
		{
            var url = ReferrerRedirector.GetRedirectionUrl(this.Request.Url, ViewState);
			if (string.IsNullOrWhiteSpace(url)) 
			{
				return string.Empty;
			}
			return ReferrerRedirector.StringToBase64(url);
		}
	}

    protected string CustomEditorListSerialized
    {
        get
        {
            var editors = GetPluginsFromConfiguration();
            if (editors.Count == 0)
            {
                return "[]";
            }
            return JsHelper.Serialize(editors.Select(e => new { ptype = e.JsCustomEditorClassName, apptype = e.CustomApplicationType }));
        }
    }

	#endregion

    #region Plugins support

    public List<CustomEditorElement> GetPluginsFromConfiguration()
    {
        if (plugins == null)
        {
            // we don't need to be thread safe, because only master and page access to the method (sequentially)
            plugins = new List<CustomEditorElement>();
            var configSection = this.GetConfigurationSection();
            if (configSection != null)
            {
                plugins = configSection
                    .CustomEditors
                    .OfType<CustomEditorElement>()
                    .Where(ce => (ce != null) && !string.IsNullOrWhiteSpace(ce.Src))
                    .ToList();
            }
        }
        return plugins;
    }

    protected EditPageConfigurationSection GetConfigurationSection()
    {
        try
        {
            return EditPageConfigurationSection.Config;
        }
        catch (Exception exc)
        {
            log.ErrorFormat("Cannot load custom application editors: {0}", exc);
            return null;
        }
    }

    #endregion
}
