﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlChild" %>
<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>

<script runat="server">

    public override string InputId
    {
        get { return "override" + this.RootName; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        var button = new HtmlGenericControl("a");
        button.Attributes["id"] = this.InputId;
        button.Attributes["href"] = "#";
        button.Attributes["class"] = "overrideLink";

        if (this.IsComplex)
        {
            button.Attributes["onOverrideNotify"] = InvariantString.Format("#{0}", this.RootName);
        }

        this.phButton.Controls.Add(button);

        base.OnPreRender(e);
    }

</script>

<asp:PlaceHolder ID="phButton" runat="server" />
