﻿<%@ Control Language="C#" ClassName="IncludeEditResource" %>

<orion:Include File="APM/APM.css" runat="server" />
<orion:Include File="APM/APM-ORION-FIX.css" runat="server" />
<orion:Include File="APM/Admin/Edit/Edit.css" runat="server" />

<orion:Include Framework="jQuery" FrameworkVersion="1.7.1" Section="Top" SpecificOrder="1" runat="server" />
<orion:Include Framework="Ext" FrameworkVersion="3.4" SpecificOrder="2" runat="server" />
<orion:Include File="APM/js/RowExpander.js" SpecificOrder="3" runat="server" />
<orion:Include File="APM/js/jquery.validate.js" SpecificOrder="4" runat="server" />

<orion:Include File="OrionCore.js" SpecificOrder="10" runat="server" />
<orion:Include File="APM/APM.js" SpecificOrder="11" runat="server" Section="Top" />
<orion:Include File="APM/Js/Transformation.js" SpecificOrder="12" runat="server" />
<orion:Include File="APM/Js/NodesDialog.js" SpecificOrder="20" runat="server" />
<orion:Include File="APM/Js/CredentialsDialog.js" SpecificOrder="21" runat="server" />
<orion:Include File="APM/Js/AddComponentDefinition.js" SpecificOrder="22" runat="server" />
<orion:Include File="APM/Admin/Js/FileUpload.js" SpecificOrder="23" runat="server" />

<orion:Include File="APM/Admin/Edit/Js/Edit.Utility.js" SpecificOrder="30" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.ThresholdsBaseline.js" SpecificOrder="31" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.Thresholds.js" SpecificOrder="32" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.js" SpecificOrder="33" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.ScriptDialog.js" SpecificOrder="34" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.MultiEditDialog.js" SpecificOrder="35" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.DynamicEvidence.js" SpecificOrder="36" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.CustomProperties.js" SpecificOrder="37" runat="server" />

<orion:Include File="APM/Admin/Edit/Js/ComponentGrid.js" SpecificOrder="40" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentGrid.Renderers.js" SpecificOrder="41" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentGrid.ToolbarHandlers.js" SpecificOrder="42" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentGrid.TestManager.js" SpecificOrder="43" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/AppModel.js" SpecificOrder="44" runat="server" />

<orion:Include File="APM/Admin/Edit/Js/EditorsHelper.js" SpecificOrder="50" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Edit.PollingMethod.js" SpecificOrder="51" runat="server" />

<orion:Include File="APM/Admin/Edit/Js/Editors/ConvertValueEditor.js" Section="Bottom" SpecificOrder="52" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Editors/FileUpload.js" Section="Bottom" SpecificOrder="53" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/Editors/OidEditor.js" Section="Bottom" SpecificOrder="54" runat="server" />

<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/CustomPowerShell.gen.js" Section="Bottom" SpecificOrder="100" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/CustomWMI.js" Section="Bottom" SpecificOrder="101" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/Http.js" Section="Bottom" SpecificOrder="102" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/Https.js" Section="Bottom" SpecificOrder="103" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/LogFile.js" Section="Bottom" SpecificOrder="104" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/LogParser.js" Section="Bottom" SpecificOrder="105" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/MapiUserExperience.js" Section="Bottom" SpecificOrder="106" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/Soap.js" Section="Bottom" SpecificOrder="107" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/SoapParameters.js" Section="Bottom" SpecificOrder="108" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/PerformanceCounter.js" Section="Bottom" SpecificOrder="109" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/Process.js" Section="Bottom" SpecificOrder="110" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/ProcessOverSnmp.js" Section="Bottom" SpecificOrder="111" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/SqlConnection.js" Section="Bottom" SpecificOrder="112" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/SqlTable.js" Section="Bottom" SpecificOrder="113" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/WindowsPowerShell.js" Section="Bottom" SpecificOrder="114" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/WindowsService.js" Section="Bottom" SpecificOrder="115" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/WindowsTaskScheduler.js" Section="Bottom" SpecificOrder="116" runat="server" />
<orion:Include File="APM/Admin/Edit/Js/ComponentEditors/WmiPerformanceCounter.js" Section="Bottom" SpecificOrder="117" runat="server" />
