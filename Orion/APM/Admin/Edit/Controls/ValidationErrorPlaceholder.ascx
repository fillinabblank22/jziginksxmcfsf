﻿<%@ Control Language="C#" ClassName="ValidationErrorPlaceholder" %>
<script runat="server">
    public string CssClass { get; set; }

    protected string GetCssClass()
    {
        return string.IsNullOrEmpty(this.CssClass) ? "validationMsg" : this.CssClass;
    }
</script>
<div class="<%= this.GetCssClass() %> sw-suggestion sw-suggestion-fail" style="display: none;">
    <span class="sw-suggestion-icon"></span><span class="validationError"></span>
</div>
