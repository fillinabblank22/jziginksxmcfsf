﻿using System;
using System.Collections.Specialized;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_Edit_Controls_MultiCheckbox : SettingEditorControlChild
{
    public NameValueCollection CheckAttributes { get; set; }

    public override string InputId { get { return this.RootName + "MultiChekbox"; } }

    public Orion_APM_Admin_Edit_Controls_MultiCheckbox()
    {
        this.CheckAttributes = new NameValueCollection();
    }

    void InsertCheckbox(bool isComplex)
    {
        var name = this.InputId;

        var control = new HtmlGenericControl("input");
        control.Attributes["type"] = "checkbox";
        control.Attributes["id"] = name;
        control.Attributes["name"] = name;
        control.Attributes["class"] = "apmMultiCheckbox";

        foreach (var key in this.CheckAttributes.AllKeys)
        {
            control.Attributes[key] = this.CheckAttributes[key];
        }

        if (isComplex)
        {
            control.Attributes["onOverrideNotify"] = InvariantString.Format("#{0}", this.RootName);
        }

        this.phContent.Controls.Add(control);
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (this.IsMulti)
        {
            this.InsertCheckbox(this.IsComplex);
        }

        base.OnPreRender(e);
    }
}
