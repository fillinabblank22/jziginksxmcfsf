﻿<%@ Control Language="C#" ClassName="HtmlTemplates" %>

<script id="edit-parameter-simple" type="text/x-sw-template">
<tr>
	<td>
		<b>{1}:</b> <br/>
        <div class="editor">
			<input id="soap-arg-{0}-{1}-{2}" name="soap-arg-{0}-{1}-{2}" size="50" type="text" value="{3}" />
        </div>
        <div class="viewer">{3}</div>

		<div class="helpText">(Type: {4})</div>
	</td>
</tr>
</script>
<script id="edit-parameter-complex" type="text/x-sw-template">
<tr>
	<td>
		<b>{1}:</b> <br/>
        <div class="editor">
			<textarea id="soap-arg-{0}-{1}-{2}" name="soap-arg-{0}-{1}-{2}" class="apm_Resizable" cols="60" rows="2">{3}</textarea>
        </div>
        <div class="viewer">
			<textarea id="soap-arg-{0}-{1}-{2}" name="soap-arg-{0}-{1}-{2}" class="viewer apm_Resizable" cols="60" rows="2" readonly="readonly">{3}</textarea>
		</div>
		<div class="helpText">(Type: {4})</div>
	</td>
</tr>
</script>