﻿<%@ Control Language="C#" AutoEventWireup="true" ClassName="ComponentEditorGroupTitle" %>

<script runat="server">

    public string Title { get; set; }

    public string SubTitle { get; set; }

    public string HelpLink { get; set; }

    public string HelpText { get; set; }

    protected bool HasHelp
    {
        get { return !string.IsNullOrEmpty(this.HelpLink); }
    }

    protected bool HasSubTitle
    {
        get { return !string.IsNullOrEmpty(this.SubTitle); }
    }

    void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();
    }

</script>

<tr class="apmComponentGroupTitle">
    <td colspan="3">
        <div>
            <label><%= this.Title %></label>
            <asp:PlaceHolder runat="server" Visible="<%# this.HasHelp %>"><a href="<%= this.HelpLink %>"><%= this.HelpText %></a></asp:PlaceHolder>
        </div>
        <asp:PlaceHolder runat="server" Visible="<%# this.HasSubTitle %>"><div class="subtitle"><%= this.SubTitle %></div></asp:PlaceHolder>
    </td>
</tr>
