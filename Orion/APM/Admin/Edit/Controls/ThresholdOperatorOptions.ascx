﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<script runat="server">

    protected static string GetOperatorValue(object item)
    {
        return ((int)((KeyValuePair<ThresholdOperator, string>)item).Key).ToString();
    }

    protected static string GetOperatorText(object item)
    {
        return HttpUtility.HtmlEncode(((KeyValuePair<ThresholdOperator, string>)item).Value);
    }

    protected override void OnPreRender(EventArgs e)
    {
        this.thresholdOperator.DataSource = Threshold.GetOperators();
        this.thresholdOperator.DataBind();
        
        base.OnPreRender(e);
    }

</script>
<asp:Repeater ID="thresholdOperator" runat="server">
    <ItemTemplate><option value="<%# GetOperatorValue(Container.DataItem) %>"><%# GetOperatorText(Container.DataItem) %></option></ItemTemplate>
</asp:Repeater>
