﻿<%@ Control Language="C#" ClassName="ComponentEditorHelpText" AutoEventWireup="true" %>

<script runat="server">

    public string HelpHtml { get; set; }

    public bool? DisplayAsRow { get; set; }

    protected bool IsFilled {
        get { return !string.IsNullOrEmpty(this.HelpHtml); }
    }

    void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();
    }

</script>

<asp:PlaceHolder ID="phHelpTextDiv" runat="server" Visible="<%# this.IsFilled %>">
    <asp:PlaceHolder ID="phHelpTextRowOpen" runat="server" Visible="<%# this.DisplayAsRow ?? true %>">
        <tr class="helpContainer"><td class="columnLabel">&nbsp;</td><td class="columnEditor">
    </asp:PlaceHolder>
    <div class="helpText"><%= this.HelpHtml %></div>
    <asp:PlaceHolder ID="phHelpTextRowClose" runat="server" Visible="<%# this.DisplayAsRow ?? true %>">
        </td><td>&nbsp;</td></tr>
    </asp:PlaceHolder>
</asp:PlaceHolder>
