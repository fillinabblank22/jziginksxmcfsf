﻿using System;
using SolarWinds.APM.Web.UI;
using System.Linq;

public partial class Orion_APM_Admin_Edit_Controls_DisplayText : SettingEditorControlChild
{
    public string DisplayText { get; set; }

    protected string EnsureDisplayText()
    {
        var text = this.DisplayText;
        if (string.IsNullOrEmpty(text))
        {
            if (this.BaseParent != null)
            {
                text = this.BaseParent.DisplayText;
            }
        }
        text = (text ?? "").Trim();
        if (!string.IsNullOrEmpty(text))
        {
            if (!text.EndsWith(":"))
            {
                text += ":";
            }
        }
        return text;
    }

    protected string GetTargetControlID()
    {
        var ctlMultiCheck = this.Parent.Controls.OfType<SettingEditorControlChild>().FirstOrDefault(ctl => ctl != this);
        return (ctlMultiCheck != null) ? ctlMultiCheck.InputId : String.Empty;
    }
}
