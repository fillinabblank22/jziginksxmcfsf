﻿<%@ Control Language="C#" ClassName="ThresholdEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>

<%@ Register Src="~/Orion/APM/Controls/MessageBox.ascx" TagPrefix="apm" TagName="MessageBox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" TagPrefix="apm" TagName="DisplayText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ThresholdOperatorOptions.ascx" TagPrefix="apm" TagName="ThresholdOperatorOptions" %>

<script runat="server">

    protected List<KeyValuePair<ThresholdOperator, string>> Options { get; set; }
    public bool IsColumnTemplate { get; set; }

    private string GetTitleTdClasses()
    {
        string ret = "thresholdColumnLabel label";
        if (IsColumnTemplate)
        {
            return ret;
        }
        return ret+ " " + this.AdditionalLabelCss;
    }

    protected string ComputeDisplayText()
    {
        var text = InvariantString.Format("{0} {1}", this.DisplayText, Resources.APMWebContent.APMWEBDATA_PS0_15);
        if (!string.IsNullOrEmpty(this.UnitsLabel))
        {
            text += InvariantString.Format("&nbsp;<span class=\"{2}_Unit\">{3}</span>", this.RootName, this.UnitsLabel);
        }
        return text;
    }
    
    protected override void OnPreRender(EventArgs e)
    {
        this.phHeaderForColumnTemplate.Visible = this.IsColumnTemplate;
        this.phHeaderStandardEditor.Visible = !this.IsColumnTemplate;
        base.OnPreRender(e);
    }

    
</script>

<tr>
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
    <asp:PlaceHolder runat="server" ID="phHeaderForColumnTemplate">
        <span id="<%= this.RootName %>ThresholdLabel" class="thresholdLabel"></span>&nbsp;<span class="<%= this.RootName %>_Unit"><%= this.UnitsLabel %></span>
    </td>
        <td class="columnEditor edit"></td>
        <td class="columnOverride">&nbsp;</td>
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="phHeaderStandardEditor">
        <apm:MultiCheckbox runat="server" />
        <apm:DisplayText ID="lblDisplayText" runat="server" DisplayText="<%# this.ComputeDisplayText() %>" />
        <input type="hidden" id="<%= this.RootName %>DisplayName" name="<%= this.RootName %>DisplayName" value="<%= this.DisplayText %>" />
        <input type="hidden" id="<%= this.RootName %>ElementId" name="<%= this.RootName %>ElementId" value="<%= this.ElementId %>" />
    </td>
    <td colspan="2">
        <div class="helpText"><%= HelpTextTop %> </div>
        <a class="thresholdsHelpLink" target="_blank" href="<%= HelpLocator.CreateHelpUrl("OrionSAMAGBaselineDataCalculation") %>">» <%= Resources.APMWebContent.APMWEBDATA_PS0_97 %><%--Learn more about setting effective thresholds--%></a>
    </td>
    </asp:PlaceHolder>
</tr>
<!-- threshold configuration -->
<tr>
    <!-- titles -->
    <td></td>
    <!-- configuration -->
    <td class="columnEditor edit number" style="display: none; padding-top: 0px;">
        <!-- template and application editor -->
        <div class="editor">
            <table  class="thresholdTable">
                <tr class="thresholdEditor warningThreshold">
                    <td class="<%= this.GetTitleTdClasses() %>">
                        <img src="/Orion/APM/images/StatusIcons/Small-Up-Warn.gif" alt="warning_icon" />
                        <span>&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_16 %></span><%--Warning:--%>
                    </td>

                    <td class="title">
                        <select id="<%= this.RootName %>Operator" name="<%= this.RootName %>Operator" class="thresholdOperator validateOperatorLess validateOperatorGreater" apmsettctr="true" data-validator-container-selector="::parent().validationMsg1">
                            <apm:ThresholdOperatorOptions runat="server"/>
                        </select>
                        <input id="<%= this.RootName %>Warning" name="<%= this.RootName %>Warning" type="text" size="200" apmsettctr="true" class="validateThreshold validateOperatorSub" data-validator-container-selector="::parent().validationMsg1"/>
                        <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override" />
                        <input id="<%= this.RootName %>WarningBaselineValue" name="<%= this.RootName %>WarningBaselineValue" type="hidden" />
                        <input id="<%= this.RootName %>WarningHelper" name="<%= this.RootName %>WarningHelper" type="hidden" />
                        <apm:ValidationErrorPlaceholder CssClass="validationMsg1" runat="server" />
                        <div id="<%= this.RootName %>WarningPollsContainer" class="pollsCountContainer">
                            <%= Resources.APMWebContent.APMWEBDATA_PS0_89 %>
                            <!-- for -->
                            <span class="pollsSingle selection"><%= Resources.APMWebContent.APMWEBDATA_PS0_90 %>&nbsp;</span><img src="/Orion/APM/Images/arrow_down.png" alt="arrow_down" class="selection pollsSingle" />
                            <!-- a single poll -->
                            <span class="pollsX selection"><%= Resources.APMWebContent.APMWEBDATA_PS0_91 %>&nbsp;</span><img src="/Orion/APM/Images/arrow_down.png" alt="arrow_down" class="selection pollsX" />&nbsp;
                            <!-- at least -->
                            <input id="<%= RootName %>WarningPolls" name="<%= RootName %>WarningPolls" type="text" apmsettctr="true" size="2" class="validatePollsNum validatePollsMin validatePollsMax validatePollsSub count pollsX" data-validator-container-selector="::parent(2).validationMsg"/>
                            <span class="pollsXofY">&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_92 %>&nbsp;&nbsp;<input id="<%= RootName %>WarningPollsInterval" name="<%= RootName %>WarningPollsInterval" apmsettctr="true" type="text" size="2" class="validatePollsNum validatePollsMin validatePollsMax validatePolls count pollsXofY" data-validator-container-selector="::parent(2).validationMsg"/></span>
                            <!-- out of -->
                            <span class="pollsX">&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_93 %></span>
                            <!-- polls -->
                            <div class="popup">
                                <div class="type">
                                    <input type="radio" value="pollsSingle" name="radio<%= this.RootName %>WarningPolls"><%= Resources.APMWebContent.APMWEBDATA_PS0_94 %>
                                </div>
                                <!-- A single poll -->
                                <div class="type">
                                    <input type="radio" value="pollsX" name="radio<%= this.RootName %>WarningPolls"><%= Resources.APMWebContent.APMWEBDATA_PS0_95 %>
                                </div>
                                <!-- X consecutive polls -->
                                <div class="type">
                                    <input type="radio" value="pollsXofY" name="radio<%= this.RootName %>WarningPolls"><%= Resources.APMWebContent.APMWEBDATA_PS0_96 %>
                                </div>
                                <!-- X out of Y polls -->
                            </div>
                        </div>
                        <apm:ValidationErrorPlaceholder CssClass="validationMsg" runat="server" />
                    </td>
                    <td rowspan="2" class="baselineIcon">
                        <img id="<%= this.RootName %>BaselineApplied" class="applicationEditor"  style="top: 17px;" alt="baseline_applied" title="<%= Resources.APMWebContent.APMWEBDATA_PS0_18 %><%--Thresholds calculated from baseline data are being used.--%>" src="/Orion/APM/images/baselineApplied_large.png">
                    </td>
                    <td rowspan="3" class="baselineInfo">
                        <div class="templateEditor baseline">
                            <div class="useBaseline">
                                <input type="checkbox" id="<%= this.RootName %>chbUseBaseline" name="<%= this.RootName %>chbUseBaseline" />
                                <label for="<%= this.RootName %>chbUseBaseline">
                                    <%= Resources.APMWebContent.APMWEBDATA_PS0_19 %><%--Use thresholds calculated from baseline data--%>
                                </label>
                            </div>
                        </div>

                        <asp:PlaceHolder ID="phSingleBaseline" runat="server" Visible="<%# !this.IsMultiEdit %>">
                            <div class="applicationEditor baseline">
                                <div class="useLatestBaseline" style="width: 240px;">
                                    <button id="<%= this.RootName %>LatestBaseline" name="<%= this.RootName %>LatestBaseline" type="button">
                                        <%= Resources.APMWebContent.APMWEBDATA_PS0_20 %><%--Use Latest Baseline Thresholds--%>
                                    </button>
                                    <br />
                                    <a id="<%= this.RootName %>BaselineDetails">» <%= Resources.APMWebContent.APMWEBDATA_PS0_21 %><%--Latest Baseline Details--%></a>
                                    <div class="info">
                                        <%= Resources.APMWebContent.APMWEBDATA_PS0_22 %><%--Thresholds calculated using baseline data allow for more accurate alerting.--%>
                                        <a target="_blank" href="<%= HelpLocator.CreateHelpUrl("OrionSAMAGBaselineDataCalculation") %>">» <%= Resources.APMWebContent.APMWEBDATA_LearnMore %><%--Learn more--%></a>
                                    </div>
                                </div>
                                <br />
                                <div id="<%= this.RootName %>EditorBaselineInfo">
                                    <apm:MessageBox runat="server" ID="mbEditorHint" Type="Hint" Width="227px" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </td>
                </tr>
                <tr class="thresholdEditor criticalThreshold">
                    <td class="<%= this.GetTitleTdClasses() %>">
                        <img src="/Orion/APM/images/StatusIcons/Small-Up-Critical.gif" alt="critical_icon" />
                        <span>&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_17 %></span><%--Critical:--%>
                    </td>

                    <td>
                        <span id="<%= this.RootName %>CriticalOp" class="thresholdOpStatic"></span>
                        <input id="<%= this.RootName %>Critical" name="<%= this.RootName %>Critical" type="text" size="200" apmsettctr="true" class="validateThreshold validateOperatorSub" data-validator-container-selector="::parent(3:).validationMsg1"/>
                        <input id="<%= this.RootName %>CriticalBaselineValue" name="<%= this.RootName %>CriticalBaselineValue" type="hidden" />
                        <input id="<%= this.RootName %>CriticalHelper" name="<%= this.RootName %>CriticalHelper" type="hidden" />
                        <br />
                        <div id="<%= this.RootName %>CriticalPollsContainer" class="pollsCountContainer">
                            <%= Resources.APMWebContent.APMWEBDATA_PS0_89 %>
                            <!-- for -->
                            <span class="pollsSingle selection"><%= Resources.APMWebContent.APMWEBDATA_PS0_90 %>&nbsp;</span><img src="/Orion/APM/Images/arrow_down.png" alt="arrow_down" class="selection pollsSingle" />
                            <!-- a single poll -->
                            <span class="pollsX selection"><%= Resources.APMWebContent.APMWEBDATA_PS0_91 %>&nbsp;</span><img src="/Orion/APM/Images/arrow_down.png" alt="arrow_down" class="selection pollsX" />&nbsp;
                            <!-- at least -->
                            <input id="<%= this.RootName %>CriticalPolls" name="<%= this.RootName %>CriticalPolls" type="text" apmsettctr="true" size="2" class="validatePollsNum validatePollsMin validatePollsMax validatePollsSub count pollsX" data-validator-container-selector="::parent(2).validationMsg"/>
                            <span class="pollsXofY">&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_92 %>&nbsp;&nbsp;<input id="<%= this.RootName %>CriticalPollsInterval" name="<%= this.RootName %>CriticalPollsInterval" type="text" apmsettctr="true" size="2" class="validatePollsNum validatePollsMin validatePollsMax validatePolls count pollsXofY" data-validator-container-selector="::parent(2).validationMsg"/></span>
                            <!-- out of -->
                            <span class="pollsX">&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_93 %></span>
                            <!-- polls -->
                            <div class="popup">
                                <div class="type">
                                    <input type="radio" value="pollsSingle" name="radio<%= this.RootName %>CriticalPolls"><%= Resources.APMWebContent.APMWEBDATA_PS0_94 %>
                                </div>
                                <!-- A single poll -->
                                <div class="type">
                                    <input type="radio" value="pollsX" name="radio<%= this.RootName %>CriticalPolls"><%= Resources.APMWebContent.APMWEBDATA_PS0_95 %>
                                </div>
                                <!-- X consecutive polls -->
                                <div class="type">
                                    <input type="radio" value="pollsXofY" name="radio<%= this.RootName %>CriticalPolls"><%= Resources.APMWebContent.APMWEBDATA_PS0_96 %>
                                </div>
                                <!-- X out of Y polls -->
                            </div>
                        </div>
                        <apm:ValidationErrorPlaceholder CssClass="validationMsg" runat="server" />
                    </td>
                </tr>
                <tr class="thresholdEditorEmptyLine">
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
            </table>
            <div class="templateEditor baseline">
                <div class="info">
                    <%= Resources.APMWebContent.APMWEBDATA_PS0_24 %><%--Thresholds calculated using baseline data allow for more accurate alerting. These thresholds are not repeatedly calculated.--%>
                    <a onclick="SW.APM.EditApp.showThresholdMoreInfoDialog();">» <%= Resources.APMWebContent.APMWEBDATA_PS0_25 %><%--More--%>
                    </a>
                </div>
            </div>
        </div>
        <!--application viewer -->
        <div class="viewer"  style="width: 100%;">
            <table class="thresholdTable">
                <tr class="thresholdViewer">
                    <td class="thresholdColumnLabel label">
                        <img src="/Orion/APM/images/StatusIcons/Small-Up-Warn.gif" alt="warning_icon" />
                        <span>&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_16 %></span><%--Warning:--%>
                    </td>

                    <td class="warningMessage">
                        <span id="<%= this.RootName %>WView"></span>&nbsp;
                        <span id="<%= this.RootName %>WPollsCountView"></span>
                    </td>
                    <td rowspan="3">
                        <div class="applicationViewer baseline">
                            <div id="<%= this.RootName %>ViewerBaselineInfo">
                                <apm:MessageBox runat="server" ID="mbViewerHint" Type="Hint" Width="227px" />
                            </div>
                            <div id="<%= this.RootName %>ViewerNoBaselineInfo">
                                <apm:MessageBox runat="server" ID="mbViewerInfo" Type="Info" Width="227px" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="thresholdViewer">
                    <td class="thresholdColumnLabel label">
                        <img src="/Orion/APM/images/StatusIcons/Small-Up-Critical.gif" alt="critical_icon" />
                        <span>&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_17 %></span><%--Critical:--%>
                    </td>

                    <td class="criticalMessage">
                        <span id="<%= this.RootName %>CView"></span>&nbsp;
                        <span id="<%= this.RootName %>CPollsCountView"></span>
                    </td>
                </tr>
                <tr class="thresholdViewerEmptyLine">
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </td>
    <!-- template override/inherit button -->
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
