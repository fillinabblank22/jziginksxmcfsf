﻿<%@ Control Language="C#" ClassName="ColumnTemplate" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>

<%@ Register Src="~/Orion/APM/Controls/MessageBox.ascx" TagPrefix="apm" TagName="MessageBox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ThresholdOperatorOptions.ascx" TagPrefix="apm" TagName="ThresholdOperatorOptions" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>
<%@ Register TagPrefix="apm" TagName="ThresholdEditor_1" Src="~/Orion/APM/Admin/Edit/Editors/ThresholdEditor.ascx" %>
<script runat="server">

    public string CustomFormulaHelpLink { get; set; }
    public string DescriptionHelpLink { get; set; }

    protected string CommonFormulasCheckboxName { get { return this.RootName + "CommonFormulasCheckbox"; } }
    protected string CustomConversionCheckboxName { get { return this.RootName + "CustomConversionCheckbox"; } }

</script>

<tbody id="column-edit-container-template-<%= this.ElementId %>" style="display: none;">
    <tr id="column-container-<%= this.ElementId %>" apm-col-type="column-value-container" class="dynamic-settings">
        <td apm-col-type="row-label" class="top"></td>
        <td colspan="2" class="top">
            <table cellspacing="0" style="width: 100%;">
                <tr apm-col-type="name-container">
                    <td class="columnLabel edit label"><%= Resources.APMWebContent.APMWEBDATA_PS0_27 %><%--Unique ID:--%></td>
                    <td class="columnEditor edit"><span apm-col-type="column-name" class="columnUniqueID"></span></td>
                    <td apm-col-type="column-actions" class="columnOverride">
                        <span apm-col-type="delete-button-container" style="display: none;">
                            <a class="remove" apm-col-type="column-delete-button-link" href="#">
                                <img class="margin-bottom-m3" alt="Delete Column" src="/Orion/Images/delete_16x16.gif" />
                                <%= Resources.APMWebContent.APMWEBDATA_PS0_28 %><%--Delete--%>
                            </a>
                        </span>
                        <span apm-col-type="disable-button-container" style="display: none;">
                            <a class="remove" apm-col-type="column-disable-button-link" href="#">
                                <img class="margin-bottom-m3" alt="Disable Column" src="/Orion/Nodes/images/icons/icon_manage.gif" />
                                <%= Resources.APMWebContent.APMWEBDATA_PS0_29 %><%--Disable--%>
                            </a>
                        </span>
                        <span apm-col-type="enable-button-container" style="display: none;">
                            <a class="remove" apm-col-type="column-enable-button-link" href="#">
                                <img class="margin-bottom-m3" alt="Enable Column" src="/Orion/Nodes/images/icons/icon_remanage.gif" />
                                <%= Resources.APMWebContent.APMWEBDATA_PS0_30 %><%--Enable--%>
                            </a>
                        </span>
                    </td>
                </tr>
                <tr apm-col-type="label-container">
                    <td class="columnLabel edit label"><%= Resources.APMWebContent.APMWEBDATA_PS0_31 %><%--Display Name:--%></td>
                    <td class="columnEditor edit">
                        <span apm-col-type="column-label-edit-container" class="editor" style="display: none;">
                            <input apm-col-type="column-label-editor" class="columnDisplayName" size="40" type="text" name="clmDisplayName" />
                        </span>
                        <span apm-col-type="column-label-viewer" class="viewer" style="width: 400px; max-width: 400px; overflow: hidden;"></span>
                        <span class="validationMsg sw-suggestion sw-suggestion-fail" style="display: none;"><span class="sw-suggestion-icon"></span><span class="validationError"></span></span>
                    </td>
                    <td class="columnOverride">
                        <a id="overrideColumnLink" apm-col-type="column-label-override-link" href="#" class="overrideLink">
                            <%= Resources.APMWebContent.APMWEBDATA_PS0_26 %><%--Override Template--%>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="convert-value-container-<%= this.ElementId %>" apm-col-type="convert-value-container">
        <td>&nbsp;</td>
        <td colspan="2">
            <input type="hidden" id="<%= this.RootName %>TestExpandedHidden" value="true" apm-col-type="column-test-expanded-hidden-input" name="<%= this.RootName %>TestExpandedHiddenClm" />
            <input type="hidden" id="<%= this.RootName %>GetCurrentValueProcess" value="false" name="<%= this.RootName %>GetCurrentValueProcessClm" />
            <table width="100%" style="table-layout: fixed; width: 100%;">
                <tr class="dataTransform">
                    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
                        <%= Resources.APMWebContent.APMWEBDATA_PS0_32 %><%--Convert Value--%>
                    </td>
                    <td class="columnEditor edit number" apm-col-type="convertValueRow">
                        <span class="editor">
                            <input type="checkbox" id="<%= this.RootName %>" name="<%= this.RootName %>" class="dataTransformEnableCheckbox" apm-col-type="column-datatransform-enable-checkbox" value="enabled" />
                            <label for="<%= this.RootName %>"><%= Resources.APMWebContent.APMWEBDATA_PS0_33 %><%--Yes, convert returned value.--%></label>
                        </span>
                        <span class="viewer"></span>
                        <div class="dynamicColumnHelpLinkDiv">
                            <%= Resources.APMWebContent.APMWEBDATA_PS0_34 %><%--The converted value will be used by thresholds & displayed.--%>&nbsp;&nbsp;
                            <a href="<%= this.DescriptionHelpLink ?? SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionAPMPHConversionValue") %>" target="_blank">» <%= Resources.APMWebContent.APMWEBDATA_LearnMore %><%--Learn more--%>
                            </a>
                        </div>
                    </td>
                    <td class="columnOverride">
                        <apm:OverrideLink runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <div class="editor" id="<%= this.RootName %>TransformSettingsEditor" style="display: none;">
                            <div>
                                <input type="radio" value="0" id="<%= this.CommonFormulasCheckboxName %>" apm-col-type='column-common-formulas-checkbox' name="<%= this.RootName %>Radio" />
                                <label for="<%= this.CommonFormulasCheckboxName %>"><%= Resources.APMWebContent.APMWEBDATA_PS0_35 %><%--Common formulas--%></label>
                            </div>
                            <div style="padding-top: 10px" id="<%= this.RootName %>CommonFormulasDiv" apm-col-type="column-common-formulas-div">
                                <select id="<%= this.RootName %>CommonFormulasList" name="<%= this.RootName %>CommonFormulasList" apm-col-type="column-common-formulas-list">
                                    <option value="0" selected="selected"><%= Resources.APMWebContent.APMWEBDATA_PS0_36 %><%---- Select function ----%></option>
                                    <option value="1"><%= Resources.APMWebContent.APMWEBDATA_PS0_37 %><%--Truncate--%></option>
                                    <option value="2"><%= Resources.APMWebContent.APMWEBDATA_PS0_38 %><%--Round--%></option>
                                </select>
                                <span id="<%= this.RootName %>CommonFormulasEmptyDiv" apm-col-type="column-common-formulas-empty-div"></span>
                                <span id="<%= this.RootName %>CommonFormulasTruncateRoundDiv" apm-col-type="column-common-formulas-truncate-round-div">
                                    <%= Resources.APMWebContent.APMWEBDATA_PS0_45 %><%--to--%>
                                    <select id="<%= this.RootName %>DecimalPlacesList" name="<%= this.RootName %>DecimalPlacesList" apm-col-type="column-decimal-places-list">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <%= Resources.APMWebContent.APMWEBDATA_PS0_39 %><%--decimal places--%>
                                </span>
                            </div>
                            <div style="padding-top: 10px">
                                <input type="radio" value="1" id="<%= this.CustomConversionCheckboxName %>" name="<%= this.RootName %>Radio" apm-col-type="column-custom-conversion-checkbox" />
                                <label for="<%= this.CustomConversionCheckboxName %>">Custom conversion</label>
                            </div>
                            <div style="padding-top: 10px" id="<%= this.RootName %>CustomConversionDiv" apm-col-type="column-custom-conversion-div">
                                <table style="border: none;">
                                    <colgroup>
                                        <col width="20px" />
                                        <col width="70px" />
                                        <col width="140px" />
                                        <col width="300px" />
                                    </colgroup>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>Formula:</td>
                                        <td colspan="2">
                                            <input type="text" id="<%= this.RootName %>CustomFormulaTextbox" name="<%= this.RootName %>CustomFormulaTextbox" style="width: 400px" apm-col-type="column-custom-formula-textbox" /></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">
                                            <span style="font-size: 7pt;">
                                                <%= Resources.APMWebContent.APMWEBDATA_PS0_40 %><%--Use ${Statistic} to represent the input value. For example: ${Statistic}*1000--%>&nbsp;&nbsp;
                                                <a href="<%= this.CustomFormulaHelpLink ?? SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionAPMPHFormulaExamples")%>" target="_blank">» <%= Resources.APMWebContent.APMWEBDATA_PS0_41 %><%--More examples--%>
                                                </a>
                                            </span>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img id="<%= this.RootName %>ExpandCollapseTestImage" style="cursor: pointer;" src="/Orion/images/Button.Expand.gif" alt="Expand/Collapse button" apm-col-type="column-expand-collapse-test-button" /></td>
                                        <td colspan="2"><%= Resources.APMWebContent.APMWEBDATA_PS0_42 %><%--Test formula--%></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr id="<%= this.RootName %>InputTableRow" style="display: none" apm-col-type="column-input-table-row">
                                        <td>&nbsp;</td>
                                        <td><%= Resources.APMWebContent.APMWEBDATA_PS0_43 %><%--Input:--%></td>
                                        <td>
                                            <input type="text" id="<%= this.RootName %>InputTextbox" name="<%= this.RootName %>InputTextbox" apm-col-type="column-input-textbox" /></td>
                                        <td>
                                            <a id="<%= this.RootName %>RetrieveCurrentValue" apm-col-type="column-retrieve-current-value-button" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.APMWebContent.ConvertValueEditor_RetrieveCurrentValue %></span></span></a>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr id="<%= this.RootName %>OutputTableRow" style="display: none" apm-col-type="column-output-table-row">
                                        <td>&nbsp;</td>
                                        <td><%= Resources.APMWebContent.APMWEBDATA_PS0_44 %><%--Output:--%></td>
                                        <td><span id="<%= this.RootName %>OutputSpan" apm-col-type="column-output-span"></span></td>
                                        <td>
                                            <a id="<%= this.RootName %>CalculateOutput" apm-col-type="column-calculate-output-button" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.APMWebContent.ConvertValueEditor_CalculateOutput %></span></span></a>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr id="<%= this.RootName %>ErrorTableRow" style="display: none; min-height: 1px;" apm-col-type="column-error-table-row">
                                        <td colspan="2"></td>
                                        <td colspan="2">
                                            <div id="<%= this.RootName %>ErrorDiv" apm-col-type="column-error-div" style="display: none; background-color: #FACECE; height: 26px; width: 110px; padding-top: 10px; padding-left: 5px;">
                                                <img src="/Orion/APM/images/Icon.Stop16x16.gif" alt="Error" style="float: left; margin-right: 5px;" /><div style="font-weight: bold; font-size: 8pt; color: #CE0000;">Invalid Formula</div>
                                            </div>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="threshold-container-<%= this.ElementId %>" apm-col-type="column-threshold-container">
        <td>&nbsp;</td>
        <td colspan="2">
            <table class="columnTemplateThreshold" cellspacing="0">
             <apm:ThresholdEditor_1 IsColumnTemplate="true" ElementId="<%# this.ElementId  %>" EditorMode="<%# this.EditorMode%>"  runat="server"  />
            </table>
        </td>
    </tr>
</tbody>
