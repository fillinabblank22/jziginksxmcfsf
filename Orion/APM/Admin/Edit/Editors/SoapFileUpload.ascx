﻿<%@ Control Language="C#" ClassName="SoapFileUpload" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>

<tr class="fileUpload">
    <td class="columnLabel edit label">
    </td>
    <td class="columnEditor edit">
        <div class="editor">
            <%= this.DisplayText %>:<br />
            <input id="<%= this.RootName %>" name="<%= this.RootName %>" type="text" size="50" apmsettctr="true" /> &nbsp;&nbsp;
            <input id="<%= this.RootName %>File" type="file" size="45" name="<%= RootName %>" style="width: 400px; padding-bottom: 4px; display:none;"/>
            <br />
            <input id="<%= this.RootName %>State" name="<%= RootName %>State" type="hidden" value="override" />
        </div>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
        <apm:ValidationErrorPlaceholder runat="server" />
    </td>
    <td class="columnOverride"></td>
</tr>
