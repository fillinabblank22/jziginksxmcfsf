﻿<%@ Control Language="C#" CodeFile="DropdownAndStringEditor.ascx.cs" Inherits="Orion_APM_Admin_Edit_Editors_DropdownAndStringEditor" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" TagPrefix="apm" TagName="DisplayText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" TagPrefix="apm" TagName="HelpText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>

<tr class="dropdownAndStringEditorContainer">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server" />
        <apm:DisplayText runat="server" />
    </td>
    <td class="columnEditor edit number" style="display: none">
        <span class="editor">
            <select id="<%= this.RootName %>" name="<%= this.RootName %>">
                <% foreach (var option in this.Options) { %>
                <option value="<%= option.Value %>"><%= option.Description %></option>
                <% } %>
            </select>
            <div class="stringEditor">
                <apm:HelpText runat="server" HelpHtml="<%# this.HelpTextTop %>" DisplayAsRow="false" />
                <% if (this.StringIsMultiline) { %>
                <textarea id="<%= this.RootNameInput %>_String" name="<%= this.RootNameInput %>_String" class="apm_Resizable" cols="20" rows="2" apmsettctr="<%= this.Required.ToString().ToLowerInvariant()%>"></textarea>
                <% } else { %>
                <input id="<%= this.RootNameInput %>_String" name="<%= this.RootNameInput%>_String" type="text" size="<%= String.IsNullOrEmpty(this.StringSize) ? "50" : this.StringSize %>" apmsettctr="<%= this.Required.ToString().ToLowerInvariant()%>"/>
                <% } %>
                <apm:HelpText runat="server" HelpHtml="<%# this.HelpTextBottom %>" DisplayAsRow="false" />
                <apm:ValidationErrorPlaceholder runat="server"/>
            </div>
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override"/>
        </span>
        <span class="viewer"></span>
        <%= UnitsLabel %>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
