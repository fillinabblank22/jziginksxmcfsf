﻿<%@ Control Language="C#" ClassName="FileUpload" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" TagPrefix="apm" TagName="DisplayText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>

<script runat="server">
    public string Size { get; set; }
    public bool IsMultiline { get; set; }

    protected override void OnInit(EventArgs e)
    {
        this.edtMulti.CheckAttributes["onclick"] = "SW.APM.Editors.FileUpload.uploadVisibility('" + this.RootName + "', this.checked)";
    }
</script>

<orion:Include ID="Include1" runat="server" Module="APM" File="Admin/Edit/js/Editors/FileUpload.js" />

<tr class="fileUpload">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox ID="edtMulti" runat="server" />
        <apm:DisplayText runat="server" />
    </td>
    <td class="columnEditor edit">
        <div class="editor">
            <input id="<%= this.RootName %>" name="<%= this.RootName %>" type="text" size="<%= String.IsNullOrEmpty(this.Size) ? "50" : this.Size %>"
                apmsettctr="<%= this.Required.ToString().ToLowerInvariant()%>" readonly="readonly" style="background: #F8F8F8; border: solid 1px #B8B8B8; padding: 2px;" />
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override" />
        </div>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
        <apm:ValidationErrorPlaceholder runat="server" />
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
<tr id="<%= this.RootName %>FileUploadContainer">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <label>
            <%= Resources.APMWebContent.FileUpload_SelectAndUploadFileForSnapshotCreation%>
        </label>
    </td>
    <td>
        <div style="background-color: #F0F0F0; padding: 5px; margin-right: 5px;">
            <div class="fileInput">
                <input id="fileToUpload<%= this.RootName %>" type="file" size="45" name="<%= this.RootName %>" style="width:400px;padding-bottom:4px"/><br /><br />
            </div>
            <orion:LocalizableButton ID="updateChecksum" runat="server" LocalizedText="CustomText"	Text="Update Checksum" DisplayType="Secondary" OnClientClick="return SW.APM.Editors.FileUpload.ajaxFileUpload(this)" />
        </div>
    </td>
    <td>
    </td>
</tr>

<script type="text/javascript">
    SW.APM.Editors.FileUpload.init('<%= this.RootName %>', '<%= this.IsMultiEdit %>');
</script>
