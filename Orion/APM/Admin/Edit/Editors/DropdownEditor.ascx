﻿<%@ Control Language="C#" ClassName="DropdownEditor" CodeFile="DropdownEditor.ascx.cs" Inherits="Orion_APM_Admin_Edit_Editors_DropdownEditor" %>
<%@ Register TagPrefix="apm" TagName="HelpText" Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" %>
<%@ Register TagPrefix="apm" TagName="MultiCheckbox" Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" %>
<%@ Register TagPrefix="apm" TagName="DisplayText" Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" %>
<%@ Register TagPrefix="apm" TagName="OverrideLink" Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" %>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextTop %>" />

<tr class="dropdownEditorContainer">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server" />
        <apm:DisplayText runat="server" />
    </td>
    <td class="columnEditor edit number">
        <span class="editor">
            <asp:PlaceHolder runat="server" ID="phOptions"></asp:PlaceHolder>
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override"/>
        </span>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextBottom %>" />

<asp:PlaceHolder runat="server" ID="phFipsRelated">
<tr id="<%= this.RootName %>FipsWarning" class="fips-warning-container">
    <td>&nbsp;</td>
    <td><div class="fips-warning"><%= Resources.APMWebContent.CurrentSettingIsNotCompatibleWithFIPS%></div></td>
    <td>&nbsp;</td>
</tr>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="phNonSecureConn">
    <tr id="<%= this.RootName %>NonSecureConnWarning" class="non-secure-warning-container">
        <td>&nbsp;</td>
        <td><div class="non-secure-warning"><%= Resources.APMWebContent.NonSecureConnectionHasBeenSelected %></div></td>
        <td>&nbsp;</td>
    </tr>
</asp:PlaceHolder>