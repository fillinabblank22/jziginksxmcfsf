﻿using System;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_Edit_Editors_ComponentTypeEditor : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        this.phFipsRelated.Visible = EnumHelper.IsNotSupportedInFIPS(Definition.Type);
    }

    private string _helpLink;
    public string HelpLink
    {
        get
        {
            if (string.IsNullOrEmpty(_helpLink))
            {
                _helpLink = HelpLocator.GetHelpUrlForComponentType(ComponentType);
            }
            return _helpLink;
        }
        set { _helpLink = SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl(value); }
    }

    public string FipsHelpLink => SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl(@"OrionSAMFIPS");

    public ComponentType ComponentType { get; set; }

    protected ComponentDefinition Definition => ComponentType != ComponentType.None ? APMCache.ComponentDefinitions[ComponentType] : new ComponentDefinition();
}