﻿<%@ Control Language="C#" ClassName="RadioButtonEditor" CodeFile="RadioButtonEditor.ascx.cs" Inherits="Orion_APM_Admin_Edit_Editors_RadioButtonEditor" %>
<%@ Register TagPrefix="apm" TagName="HelpText" Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" %>
<%@ Register TagPrefix="apm" TagName="MultiCheckbox" Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" %>
<%@ Register TagPrefix="apm" TagName="DisplayText" Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" %>
<%@ Register TagPrefix="apm" TagName="OverrideLink" Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" %>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextTop %>" />

<tr class="radioButtonEditorContainer">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server" />
        <apm:DisplayText runat="server" />
    </td>
    <td class="columnEditor edit number" style="display: none">
        <div class="editor">
            <asp:PlaceHolder runat="server" ID="phOptions"></asp:PlaceHolder>
        </div>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextBottom %>" />

<asp:PlaceHolder runat="server" ID="phFipsRelated">
<tr id="<%= this.RootName %>FipsWarning" class="fips-warning-container">
    <td>&nbsp;</td>
    <td><div class="fips-warning"><%= Resources.APMWebContent.CurrentSettingIsNotCompatibleWithFIPS%></div></td>
    <td>&nbsp;</td>
</tr>
</asp:PlaceHolder>
