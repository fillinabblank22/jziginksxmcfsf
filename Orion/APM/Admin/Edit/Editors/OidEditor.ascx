﻿<%@ Control Language="C#" ClassName="OidEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" TagPrefix="apm" TagName="DisplayText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>

<script runat="server">
    public string Size { get; set; }
</script>

<orion:Include ID="Include1" runat="server" Module="APM" File="Admin/Edit/js/Editors/OidEditor.js" />

<tr class="oidEditorContainer">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server"/>
        <apm:DisplayText runat="server"/>
    </td>
    <td class="columnEditor edit" style="display: none">
        <span class="editor"><span class="helpText">
            <%= this.HelpTextTop %></span>
            <input id="<%= this.RootName %>" name="<%= this.RootName %>" type="text" size="<%= String.IsNullOrEmpty(this.Size) ? "50" : this.Size %>" class="OIDvalidator" apmsettctr="<%= this.Required.ToString().ToLowerInvariant() %>" />
            <span class="helpText">
                <%= this.HelpTextBottom %></span>
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override" />
        </span><span class="viewer"></span>
        <%= this.UnitsLabel %>
        <apm:ValidationErrorPlaceholder ID="ValidationErrorPlaceholder1" runat="server" />
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
<script type="text/javascript">
    SW.APM.Editors.OidEditor.init('<%= this.RootName %>');
</script>
