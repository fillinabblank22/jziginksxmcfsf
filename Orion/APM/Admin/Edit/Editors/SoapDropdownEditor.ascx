﻿<%@ Control Language="C#" ClassName="SoapDropdownEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorComplexControlBase"%>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" TagPrefix="apm" TagName="HelpText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>

<script runat="server">
    public bool IsDisabled { get; set; }
    public List<DropdownOption> Options { get; set; }

    protected override void OnInit(EventArgs e)
    {
        if (this.Options == null)
        {
            this.Options = new List<DropdownOption>();
        }
        base.OnInit(e);
    }
</script>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextTop %>" />
<tr class="dropdownEditorContainer">
    <td class="columnLabel edit label <%= AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server"/>
        <%= this.DisplayText %>:
    </td>
    <td class="columnEditor edit number">
        <span class="editor">
            <select id="<%= this.RootName %>" name="<%= this.RootName %>" apmsettctr="true"
                fips-related="<%= this.Options.Any(item => item.IsFipsRelated).ToString().ToLowerInvariant() %>"
                <% if (this.IsDisabled) { %>disabled="disabled"<% } %>>
                <% foreach (var option in this.Options) { %>
                <option value="<%= option.Value %>" fips-related="<%=option.IsFipsRelated.ToString().ToLowerInvariant()%>"><%= option.Description %></option>
                <% } %>
            </select>
            <input id="<%= this.RootName %>State" name="<%= RootName %>State" type="hidden" value="override"/>
        </span>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
    </td>
    <td class="columnOverride"></td>
</tr>
<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextBottom %>" />
<% if (this.Options.Any(item => item.IsFipsRelated)) { %>
<tr id="<%= this.RootName %>FipsWarning" class="fips-warning-container">
    <td>&nbsp;</td>
    <td><div class="fips-warning"><%= Resources.APMWebContent.CurrentSettingIsNotCompatibleWithFIPS%></div></td>
    <td>&nbsp;</td>
</tr>
<% } %>
