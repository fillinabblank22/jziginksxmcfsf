﻿<%@ Control Language="C#" ClassName="SoapArgumentsEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase" %>

<tr class="soap-args-container">
    <td class="columnLabel edit label">
    <%= this.DisplayText %>:
    </td>
    <td colspan="2">
        <table id="<%= this.RootName %>SoapArgsContainer">
            <tbody>
            </tbody>
        </table>
    </td>
</tr>
