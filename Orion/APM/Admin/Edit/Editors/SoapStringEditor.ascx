﻿<%@ Control Language="C#" ClassName="SoapStringEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase"%>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>

<script runat="server">
    public string Size { get; set; }
    public bool IsMultiline { get; set; }
    public bool IsReadOnly { get; set; }
</script>

<tr class="stringEditorContainer">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <%= this.DisplayText %>:
    </td>
    <td class="columnEditor edit">
        <div class="editor">
            <% if (this.IsMultiline) { %>
            <textarea id="<%= this.RootName %>" name="<%= this.RootName %>" class="apm_Resizable" cols="20" rows="2"
                apmsettctr="<%= this.Required.ToString().ToLowerInvariant()%>"
                style="<% if (this.IsReadOnly) { %>color: gray;<% } %>"
                <% if (this.EditorMode == EditMode.Multi) { %> style="background-color: #fff" <% } %>
                <% if (this.IsReadOnly) { %>readonly="readonly"<% } %>>
            </textarea>
            <% } else { %>
            <input id="<%= this.RootName %>" name="<%= this.RootName %>" type="text" size="50"
                apmsettctr="<%= this.Required.ToString().ToLowerInvariant()%>"
                style="<% if (this.IsReadOnly) { %>color: gray;<% } %>"
                <% if (this.IsReadOnly) { %> readonly="readonly"<% } %> />
            <% } %>
        </div>
        <% if (this.IsMultiline) { %>
        <div id="<%= this.RootName %>Viewer" class="viewer-cnt">
            <textarea name="<%= this.RootName %>Viewer" cols="20" rows="2" class="viewer apm_Resizable" readonly="readonly" style="color: gray;"></textarea>
        </div>
        <% } else { %>
        <span class="viewer"></span>
        <% } %>
        <%= this.UnitsLabel %>
        <apm:ValidationErrorPlaceholder runat="server"/>
    </td>
    <td class="columnOverride"></td>
</tr>
