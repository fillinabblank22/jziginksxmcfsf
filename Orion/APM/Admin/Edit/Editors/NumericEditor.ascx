﻿<%@ Control Language="C#" ClassName="NumericEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorComplexControlBase" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" TagPrefix="apm" TagName="DisplayText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" TagPrefix="apm" TagName="HelpText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>

<script runat="server">
    public int? MinValue { get; set; }
    public int? MaxValue { get; set; }
    public string AdditionalEditorCss { get; set; }

    protected Control GetInputControl()
    {
        var ctl = new HtmlGenericControl("input");
        ctl.Attributes["id"] = this.RootName;
        ctl.Attributes["name"] = this.RootName;
        return ctl;
    }

    protected static string FormatAttr<T>(string name, T? value) where T : struct
    {
        return value.HasValue
            ? string.Format(CultureInfo.InvariantCulture, " {0}=\"{1}\"", name, value.Value)
            : string.Empty;
    }

</script>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextTop %>" />
<tr class="numericEditorContainer <%= this.AdditionalEditorCss %>">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server" />
        <apm:DisplayText runat="server" />
    </td>
    <td class="columnEditor edit number">
        <span class="editor">
            <input id="<%= this.RootName %>" name="<%= this.RootName %>" type="text" size="10" class="number" apmsettctr="true"<%= FormatAttr("min", this.MinValue) %><%= FormatAttr("max", this.MaxValue) %> />
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override" />
        </span>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
        <apm:ValidationErrorPlaceholder runat="server" />
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextBottom %>" />
