﻿<%@ Control Language="C#" ClassName="BooleanEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorComplexControlBase" %>
<%@ Register TagPrefix="apm" TagName="MultiCheckbox" Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" %>
<%@ Register TagPrefix="apm" TagName="DisplayText" Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" %>
<%@ Register TagPrefix="apm" TagName="OverrideLink" Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" %>

<script runat="server">
    public string TrueDisplayValue { get; set; }
    public string FalseDisplayValue { get; set; }
    public string AdditionalEditorCss { get; set; }
</script>

<tr class="booleanEditorContainer <%= this.AdditionalEditorCss %>">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server"/>
        <apm:DisplayText runat="server"/>
    </td>
    <td class="columnEditor edit number">
        <span class="editor">
            <select id="<%= this.RootName %>" name="<%= this.RootName %>" apm-data-type="bool">
                <option value="true"><%= this.TrueDisplayValue %></option>
                <option value="false"><%= this.FalseDisplayValue %></option>
            </select>
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override"/>
        </span>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
