﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ComponentTypeEditor.ascx.cs" Inherits="Orion_APM_Admin_Edit_Editors_ComponentTypeEditor" %>

<tr class="componentTypeEditorContainer">
    <td class="columnLabel edit label"><%= Resources.APMWebContent.APMWEBDATA_VB1_314%></td>
    <td class="columnEditor edit">
        <div class="name">
            <%= Definition.Name %>
        </div>
        <div>
            <%= Definition.Description %>
        </div>
        <div>
            <a target="_blank" href="<%= HelpLink %>">
                <img alt="icon_info" src="../../../Images/Icon.Info.gif" />&nbsp;<%= Resources.APMWebContent.HowDoIConfigureThisComponent%>
            </a>
        </div>
    </td>
    <td></td>
</tr>

<asp:PlaceHolder runat="server" ID="phFipsRelated">
    <tr id="ComponentFipsWarning" class="fips-warning-component-container">
        <td>&nbsp;</td>
        <td><div class="fips-warning"><%= Resources.APMWebContent.CurrentComponentIsNotCompatibleWithFIPS%> <a target="_blank" href="<%= FipsHelpLink %>"><%= Resources.CoreWebContent.WEBDATA_TK0_51%></a></div></td>
        <td>&nbsp;</td>
    </tr>
</asp:PlaceHolder>
