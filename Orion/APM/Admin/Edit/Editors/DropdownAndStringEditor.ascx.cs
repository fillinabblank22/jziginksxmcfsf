﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.Web.Utility;

/// <summary>
/// Summary description for DropdownAndStringEditor
/// </summary>
public partial class Orion_APM_Admin_Edit_Editors_DropdownAndStringEditor : SolarWinds.APM.Web.UI.SettingEditorComplexControlBase
{
    public string BaseValueNameInput { get; set; }
    public string StringSize { get; set; }
    public bool StringIsMultiline { get; set; }
    public List<DropdownOption> Options { get; set; }

    protected string RootNameInput
    {
        get
        {
            return string.IsNullOrEmpty(BaseValueNameInput) ? this.RootName : this.BaseValueNameInput + this.ElementId;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (this.Options == null)
        {
            this.Options = new List<DropdownOption>();
        }

        base.OnInit(e);
    }
}
