﻿<%@ Control Language="C#" ClassName="ConvertValueEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase"  %>
<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>
<%@ Register TagPrefix="apm" TagName="HelpText" Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" %>
<%@ Register TagPrefix="apm" TagName="MultiCheckbox" Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" %>
<%@ Register TagPrefix="apm" TagName="DisplayText" Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" %>
<%@ Register TagPrefix="apm" TagName="OverrideLink" Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" %>

<orion:Include runat="server" Module="APM" File="Admin/Edit/Js/Editors/ConvertValueEditor.js" />

<script runat="server">
    public string CustomFormulaHelpLink { get; set; }
    public string DescriptionHelpLink { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var url = this.DescriptionHelpLink ?? SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionAPMPHConversionValue");
        this.edtHelp.HelpHtml = InvariantString.Format(Resources.APMWebContent.ConvertValueEditor_HelpHtml_Text +"&nbsp;&nbsp;<a href=\"{0}\" target=\"_blank\">" + Resources.APMWebContent.ConvertValueEditor_HelpHtml_LearnMore_LinkText + "</a>", url);
    }
</script>

<input type="hidden" id="<%= this.RootName %>TestExpandedHidden" value="true" name="<%= this.RootName %>TestExpandedHidd" />
<input type="hidden" id="<%= this.RootName %>GetCurrentValueProcess" value="false" name="<%= this.RootName %>GetCurrentValueProcessHidd"/>
<tr class="dataTransform">
    <td class="columnLabel edit label <%= AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server"/>
        <apm:DisplayText runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_ConvertValue %>"/>
    </td>
    <td class="columnEditor edit dataTransformEnable">
        <span class="editor">
            <input type="checkbox" id="<%= this.RootName %>" name="<%= this.RootName %>" class="dataTransformEnableCheckbox" value="enabled"  />
            <label for="<%= this.RootName %>"><%= Resources.APMWebContent.ConvertValueEditor_YesConvertReturnedValue%></label>
        </span>
        <span class="viewer"></span>
        <apm:HelpText ID="edtHelp" runat="server" DisplayAsRow="False"/>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server"/>
    </td>
</tr>
<tr class="dataTransformSettingsContainer">
    <td>&nbsp;</td>
    <td colspan="2" class="columnEditor edit convertType">
        <div class="editor" id="<%= this.RootName %>TransformSettingsEditor">
            <div>
                <input type="radio" value="0" id="<%= this.RootName %>CommonFormulasCheckbox" name="<%= this.RootName %>Radio" />
                <label for="<%= this.RootName %>CommonFormulasCheckbox"><%= Resources.APMWebContent.ConvertValueEditor_CommonFormulas%></label>
            </div>
            <div style="padding-top: 10px;" id="<%= this.RootName %>CommonFormulasDiv">
                <select id="<%= this.RootName %>CommonFormulasList" name="<%= this.RootName %>CommonFormulasList">
                    <option value="0" selected="selected"><%= Resources.APMWebContent.ConvertValueEditor_SelectFunction%></option>
                    <option value="1"><%= Resources.APMWebContent.ConvertValueEditor_Truncate%></option>
                    <option value="2"><%= Resources.APMWebContent.ConvertValueEditor_Round%></option>
                </select>
                <div id="<%= this.RootName %>CommonFormulasEmptyDiv" style="display: none" />
                <div id="<%= this.RootName %>CommonFormulasTruncateRoundDiv" style="display: none">
                    to
                    <select id="<%= this.RootName %>DecimalPlacesList" name="<%= this.RootName %>DecimalPlacesList">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    decimal places
                </div>
            </div>
            <div style="padding-top: 10px;">
                <input type="radio" value="1" id="<%= this.RootName %>CustomConversionCheckbox" name="<%= this.RootName %>Radio" />
                <label for="<%= this.RootName %>CustomConversionCheckbox"><%= Resources.APMWebContent.ConvertValueEditor_CustomConversion%></label>
            </div>
            <div style="padding-top: 10px;" id="<%= this.RootName %>CustomConversionDiv">
                <table style="border: none;">
                    <colgroup>
                        <col width="20px" />
                        <col width="70px" />
                        <col width="140px" />
                        <col width="300px" />
                    </colgroup>
                    <tr id='<%= this.RootName %>CustomFormula'>
                        <td>&nbsp;</td>
                        <td>Formula:</td>
                        <td colspan="2"><input type="text" id="<%= this.RootName %>CustomFormulaTextbox" name="<%= this.RootName %>CustomFormulaTextbox" style="width: 400px" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr id='<%= this.RootName %>FormulaExamples'>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2"><span style="font-size: 7pt;"><%= Resources.APMWebContent.ConvertValueEditor_FormulasExample%>&nbsp;&nbsp;<a href="<%= this.CustomFormulaHelpLink ?? SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionAPMPHFormulaExamples")%>" target="_blank"><%= Resources.APMWebContent.ConvertValueEditor_MoreExamples_LinkText%></a></span></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr id='<%= this.RootName %>ExpandCollapse'>
                        <td><img id="<%= this.RootName %>ExpandCollapseTestImage" style="cursor: pointer;" src="/Orion/images/Button.Expand.gif" alt="Expand/Collapse button" /></td>
                        <td colspan="2">Test formula</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr id="<%= this.RootName %>InputTableRow" style="display: none">
                        <td>&nbsp;</td>
                        <td>Input:</td>
                        <td><input type="text" id="<%= this.RootName %>InputTextbox" name="<%= this.RootName %>InputTextbox" /></td>
                        <td><a id="<%= this.RootName %>RetrieveCurrentValue" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.APMWebContent.ConvertValueEditor_RetrieveCurrentValue %></span></span></a></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr id="<%= this.RootName %>OutputTableRow" style="display: none">
                        <td>&nbsp;</td>
                        <td>Output:</td>
                        <td><span id="<%= this.RootName %>OutputSpan"></span></td>
                        <td><a id="<%= this.RootName %>CalculateOutput" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.APMWebContent.ConvertValueEditor_CalculateOutput %></span></span></a></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr id="<%= this.RootName %>ErrorTableRow" style="display: none; min-height: 1px;">
                        <td colspan="2"></td>
                        <td colspan="2"><div id="<%= this.RootName %>ErrorDiv" style="display: none; background-color: #FACECE; height: 26px; width: 110px; padding-top: 10px; padding-left: 5px;"><img src="/Orion/APM/images/Icon.Stop16x16.gif" alt="Error" style="float: left; margin-right: 5px;" /><div style="font-weight: bold; font-size: 8pt; color: #CE0000;">Invalid Formula</div></div></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </td>
</tr>
<script type="text/javascript">
    SW.APM.Editors.ConvertValueEditor.init('<%= this.RootName %>', '<%= this.IsMultiEdit %>', '<%= this.ElementId %>');
</script>
