﻿<%@ Control Language="C#" ClassName="ScriptBody" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase"%>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" TagPrefix="apm" TagName="DisplayText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>

<script runat="server">
    public string Size { get; set; }
    public bool IsMultiline { get; set; }

    protected override void OnPreRender(EventArgs e)
    {
        var edtTextArea = new HtmlGenericControl("textarea");

        if (this.EditorMode == EditMode.Multi)
        {
            edtTextArea.Attributes["style"] = "background-color: #fff";
        }
        else
        {
            edtTextArea.Attributes["style"] = "background-color: #e4f1f8";
            edtTextArea.Attributes["readonly"] = "readonly";
        }

        edtTextArea.Attributes["id"] = this.RootName;
        edtTextArea.Attributes["name"] = this.RootName;
        edtTextArea.Attributes["class"] = "apm_Resizable";
        edtTextArea.Attributes["cols"] = "20";
        edtTextArea.Attributes["rows"] = "2";
        edtTextArea.Attributes["apmsettctr"] = this.Required.ToString().ToLowerInvariant();

        this.phTextArea.Controls.Add(edtTextArea);

        this.phEdit.Visible = (this.EditorMode != EditMode.Multi);

        base.OnPreRender(e);
    }
</script>

<tr>
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server"/>
        <apm:DisplayText runat="server"/>
    </td>
    <td class="columnEditor edit" style="display: none">
        <div class="editor">
            <asp:PlaceHolder ID="phTextArea" runat="server"/>
            <input id="<%= RootName %>State" name="<%= RootName %>State" type="hidden" value="override"/>
            <asp:PlaceHolder ID="phEdit" runat="server">
                <div style="padding-top:10px;">
                    <button id="btnEdit<%# this.ElementId %>" type="button" class="sw-btn sw-btn-secondary"><%= Resources.APMWebContent.ScriptBody_EditScript%></button>
                </div>
            </asp:PlaceHolder>
        </div>
        <div id="Viewer">
            <div id="<%= this.RootName %>Viewer">
                <textarea cols="20" rows="2" class="viewer apm_Resizable" readonly="readonly"  style="color:gray" name="<%= this.RootName %>Viewer"></textarea>
            </div>
        </div>
        <%= this.UnitsLabel %>
        <apm:ValidationErrorPlaceholder ID="veph" runat="server"/>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
