﻿<%@ Control Language="C#" ClassName="BooleanCheckBoxEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase" %>
<%@ Register TagPrefix="apm" TagName="HelpText" Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" %>
<%@ Register TagPrefix="apm" TagName="MultiCheckbox" Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" %>
<%@ Register TagPrefix="apm" TagName="DisplayText" Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" %>
<%@ Register TagPrefix="apm" TagName="OverrideLink" Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" %>

<script runat="server">
    
    public string CheckBoxLabel { get; set; }

    protected bool IsLabeled { get { return !string.IsNullOrEmpty(this.CheckBoxLabel); } }
    
</script>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextTop %>" />
<tr class="booleanCheckBoxEditorContainer">
    <td class="columnLabel edit label <%= this.AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server"/>
        <apm:DisplayText runat="server"/>
    </td>
    <td class="columnEditor edit number">
        <span class="editor">
            <input type="checkbox" id="<%= this.RootName %>" name="<%= this.RootName %>"/>
            <asp:PlaceHolder runat="server" Visible="<%# this.IsLabeled %>">
                <label for="<%= this.RootName %>"><%= this.CheckBoxLabel %></label>
            </asp:PlaceHolder>
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override"/>
        </span>
        <span class="viewer"></span>
        <%= this.UnitsLabel %>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextBottom %>" />
