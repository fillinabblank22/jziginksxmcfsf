﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;

public partial class Orion_APM_Admin_Edit_Editors_RadioButtonEditor : SettingEditorComplexControlBase
{
    public List<DropdownOption> Options { get; set; }

    protected bool IsFipsRelated { get { return this.Options.Any(o => o.IsFipsRelated); } }

    protected HtmlGenericControl FormatOptions()
    {
        var ctlSelect = new HtmlGenericControl("div");
        ctlSelect.Attributes["fips-related"] = this.IsFipsRelated.ToString().ToLowerInvariant();
        ctlSelect.Attributes["id"] = this.RootName;
        ctlSelect.Attributes["apmsetctr"] = "true";
        ctlSelect.Attributes["class"] = "radioGroup";

        foreach (var o in this.Options)
        {
            var ctlDiv = new HtmlGenericControl("div");

            var radioId = InvariantString.Format("{0}_{1}", this.RootName, o.Value);

            var ctlRadio = new HtmlGenericControl("input");
            ctlRadio.Attributes["name"] = this.RootName;
            ctlRadio.Attributes["type"] = "radio";
            ctlRadio.Attributes["value"] = o.Value;
            ctlRadio.Attributes["fips-related"] = o.IsFipsRelated.ToString().ToLowerInvariant();
            ctlRadio.Attributes["id"] = radioId;
            ctlDiv.Controls.Add(ctlRadio);

            var ctlLabel = new HtmlGenericControl("label");
            ctlLabel.Attributes["for"] = radioId;
            ctlLabel.InnerText = o.Description;
            ctlDiv.Controls.Add(ctlLabel);

            ctlSelect.Controls.Add(ctlDiv);
        }

        return ctlSelect;
    }

    protected override void OnInit(EventArgs e)
    {
        Options = Options??new List<DropdownOption>();

        base.OnInit(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        phFipsRelated.Visible = IsFipsRelated;

        phOptions.Controls.Add(FormatOptions());

        base.OnPreRender(e);
    }
}
