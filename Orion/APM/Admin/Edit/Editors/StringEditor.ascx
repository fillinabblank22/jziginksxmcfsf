﻿<%@ Control Language="C#" ClassName="StringEditor" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlBase"%>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/DisplayText.ascx" TagPrefix="apm" TagName="DisplayText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/HelpText.ascx" TagPrefix="apm" TagName="HelpText" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/MultiCheckbox.ascx" TagPrefix="apm" TagName="MultiCheckbox" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/OverrideLink.ascx" TagPrefix="apm" TagName="OverrideLink" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" TagPrefix="apm" TagName="ValidationErrorPlaceholder" %>

<script runat="server">
    public string Size { get; set; }
    public bool IsMultiline { get; set; }
    public bool IsReadOnly { get; set; }
    public int MaxLength { get; set; }

    public string SizeOrDefault
    {
        get { return string.IsNullOrEmpty(this.Size) ? "50" : this.Size; }
    }
</script>

<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextTop %>" />
<tr class="stringEditorContainer">
    <td class="columnLabel edit label <%=AdditionalLabelCss %>">
        <apm:MultiCheckbox runat="server"/>
        <apm:DisplayText runat="server"/>
    </td>
    <td class="columnEditor edit">
        <div class="editor">
            <% if (this.IsMultiline) { %>
            <div id="<%= this.RootName %>Editor">
                <textarea id="<%= this.RootName %>" name="<%= this.RootName %>" class="apm_Resizable" cols="20" rows="2"
                    apmsettctr="<%= this.Required.ToString().ToLowerInvariant()%>"
                    <% if (this.EditorMode == EditMode.Multi) { %> style="background-color: #fff" <% } %>
                    <% if (this.IsReadOnly) { %> readonly="readonly"<% } %>
                    <% if (this.MaxLength > 0) { %> maxlength="<%= this.MaxLength %>"<% } %>>
                </textarea>
            </div>
            <% } else { %>
            <input id="<%= this.RootName %>" name="<%= this.RootName %>" type="text" size="<%= this.SizeOrDefault %>"
                apmsettctr="<%= this.Required.ToString().ToLowerInvariant() %>"
                <% if (this.IsReadOnly) { %> readonly="readonly"<% } %> />
            <% } %>
            <input id="<%= this.RootName %>State" name="<%= this.RootName %>State" type="hidden" value="override"/>
        </div>
        <% if (this.IsMultiline) { %>
        <div id="<%= this.RootName %>Viewer">
            <textarea name="<%= this.RootName %>Viewer" cols="20" rows="2" class="viewer apm_Resizable" readonly="readonly"
                <% if (!this.IsReadOnly) { %> style="color:gray"<% } %>>
            </textarea>
        </div>
        <% } else { %>
        <span class="viewer"></span>
        <% } %>
        <%= this.UnitsLabel %>
        <apm:ValidationErrorPlaceholder runat="server"/>
    </td>
    <td class="columnOverride">
        <apm:OverrideLink runat="server" />
    </td>
</tr>
<apm:HelpText runat="server" HelpHtml="<%# this.HelpTextBottom %>" />
