﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Web.Utility;

/// <summary>
/// Summary description for DropdownEditor
/// </summary>
public partial class Orion_APM_Admin_Edit_Editors_DropdownEditor : SolarWinds.APM.Web.UI.SettingEditorComplexControlBase
{
    public List<DropdownOption> Options { get; set; }

    protected bool IsFipsRelated { get { return this.Options.Any(o => o.IsFipsRelated); } }
    protected bool IsNonSecureConnection { get { return this.Options.Any(o => o.IsNonSecureConnection); } }

    protected HtmlGenericControl FormatOptions()
    {
        var ctlSelect = new HtmlGenericControl("select");
        ctlSelect.Attributes["fips-related"] = this.IsFipsRelated.ToString().ToLowerInvariant();
        ctlSelect.Attributes["non-secure-connection"] = this.IsNonSecureConnection.ToString().ToLowerInvariant();
        ctlSelect.Attributes["id"] = this.RootName;
        ctlSelect.Attributes["name"] = this.RootName;
        ctlSelect.Attributes["apmsetctr"] = "true";

        foreach (var o in this.Options)
        {
            var ctl = new HtmlGenericControl("option");
            ctl.Attributes["value"] = o.Value;
            ctl.Attributes["fips-related"] = o.IsFipsRelated.ToString().ToLowerInvariant();
            ctl.Attributes["non-secure-connection"] = o.IsNonSecureConnection.ToString().ToLowerInvariant();
            ctl.InnerText = o.Description;
            ctlSelect.Controls.Add(ctl);
        }

        return ctlSelect;
    }

    protected override void OnInit(EventArgs e)
    {
        if (this.Options == null)
        {
            this.Options = new List<DropdownOption>();
        }

        base.OnInit(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        this.phFipsRelated.Visible = this.IsFipsRelated;

        this.phNonSecureConn.Visible = this.IsNonSecureConnection;

        this.phOptions.Controls.Add(this.FormatOptions());

        base.OnPreRender(e);
    }
}
