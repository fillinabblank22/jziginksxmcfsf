﻿using System;
using System.Linq;
using System.Web.UI;

using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Admin_Edit_EditApplicationItem : SolarWinds.APM.Web.EditPage.ApmEditPageBase
{
    private readonly Log log = new Log();

    protected string InheritFromInstanceLabel
    {
        get
        {
            return WebPluginManager.Instance.GetInheritFromInstanceLabel(CustomApplicationType);
        }
    }

    protected string OverrideInstanceLabel
    {
        get
        {
            return WebPluginManager.Instance.GetOverrideInstanceLabel(CustomApplicationType);
        }
    }

    protected void Page_Init(Object sender, EventArgs e)
    {
    }

    protected void Page_Load(Object sender, EventArgs e)
    {
        this.ApplyHeadTemplateApplicationItem(this.ApplicationItemType);
        this.ApplyCustomControlsApplicationItem(this.ApplicationItemType, this.phContent);
    }

}
