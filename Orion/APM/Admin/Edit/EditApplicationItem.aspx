﻿<%@ Page Title="New Edit Application" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/BaseEditApp.master" AutoEventWireup="true" CodeFile="EditApplicationItem.aspx.cs" Inherits="Orion_APM_Admin_Edit_EditApplicationItem" %>

<asp:Content runat="server" ContentPlaceHolderID="apmCphHead">
    <script type="text/javascript">
        SW.APM.EditApp.InheritFromTemplateLabel = '<%= this.InheritFromInstanceLabel %>';
        SW.APM.EditApp.OverrideTemplateLabel = '<%= this.OverrideInstanceLabel %>';
    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="apmCphMainPropertiesTop">
    <asp:PlaceHolder runat="server" ID="phContent"/>
</asp:Content>
