/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.withGlobal('SW.APM.EditApp.AppModel', function (AppModel, APMjs) {
    'use strict';

    var CustomProp = null,
        $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        SF = APMjs.format,
        SW = APMjs.assertGlobal('SW'),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp');


    function loadAppSettingsToForm() {
        var app = this,
            templateName = 'pageTitleTemplate',
            template,
            selectedChoice,
            pageTitleTmpl,
            link = null,
            returnUrl = '';

        if (AppModel.pageTitleTemplate) {
            pageTitleTmpl = new Ext.Template(AppModel.pageTitleTemplate);
        } else {
            pageTitleTmpl = Ext.Template.from(templateName, { disableFormats: false });
        }

        pageTitleTmpl.append('pageTitle', app);

        if (app.editMode !== 'FindProcessesWizard') {
            // set page title
            if (app.Name.search('New ') < 0) {
                document.title = 'Edit Application - ' + String(app.Name);
            } else {
                document.title = 'New Application';
            }
        }

        if (app.ApplicationItemId > 0) {
            return;
        }

        $('#appName').val(app.NonEscapedName);

        if (app.Template) {
            if (EditApp.ReturnUrl.length > 10) {
                returnUrl = SF('&ReturnUrl={0}', EditApp.ReturnUrl);
            }
            link = SF('/Orion/APM/Admin/Edit/EditTemplate.aspx?appID={0}&id={1}{2}', app.Id, app.Template.Id, returnUrl);
            $('.templateName').text(app.Template.Name);
            $('.editTemplateLink').wrapInner('<a />').children('a').attr('href', link);

            selectedChoice = $.grep(app.Template.DetailsViewChoices, function (x) {
                return x.Value === app.Template.DetailsViewSelection;
            });
            $('#detailsView').text((selectedChoice.length && selectedChoice[0].Name) || '');
        } else {
            $('*:contains("Modify Template Settings")').each(function () {
                var jSetting = $(this);
                if (!jSetting.children().length) {
                    jSetting.html(jSetting.text().replace('Modify Template Settings', ''));
                }
            });

            $('#detailsView').text('No, use Default Application Details View ');
            $('.templateName').text(app.Name);
            $('.editTemplate').hide();
        }

        template = app.Template;
        EditApp.initUiState($('#appExecutePollingMethod'), 'ExecutePollingMethod', app, template);
        EditApp.initUiState($('#appFrequency'), 'Frequency', app, template);
        EditApp.initUiState($('#appTimeout'), 'Timeout', app, template);
        EditApp.initUiState($('#appDebugLogging'), 'DebugLoggingEnabled', app, template);
        EditApp.initUiState($('#appLogFiles'), 'NumberOfLogFilesToKeep', app, template);
        EditApp.initUiState($('#appUse64Bit'), 'Use64Bit', app, template);

        app.callCustomEditor(function (plugin) {
            if (APMjs.isFn(plugin.initUiState)) {
                plugin.initUiState(app, template);
            }
        }, app.CustomApplicationType);

        CustomProp = new SW.APM.Admin.CustomProperties(app, '#customProperties');
        CustomProp.render();

        // init polling method message handler
        APMjs.launch(function () {
            SW.APM.EditApp.PollingMethod.init({
                idEditorRoot: 'appExecutePollingMethod',
                app: app
            });
        });
    }

    function saveAppSettingsFromForm() {
        var app = this;

        if (app.ApplicationItemId > 0) {
            return;
        }

        app.Name = $('#appName').val();

        EditApp.updateModelFromUi($('#appExecutePollingMethod'), app.ExecutePollingMethod, false);
        EditApp.updateModelFromUi($('#appFrequency'), app.Frequency, false);
        EditApp.updateModelFromUi($('#appTimeout'), app.Timeout, false);
        EditApp.updateModelFromUi($('#appDebugLogging'), app.DebugLoggingEnabled, false);
        EditApp.updateModelFromUi($('#appLogFiles'), app.NumberOfLogFilesToKeep, false);
        EditApp.updateModelFromUi($('#appUse64Bit'), app.Use64Bit, false);

        app.callCustomEditor(function (plugin) {
            if (APMjs.isFn(plugin.updateModelFromUi)) {
                plugin.updateModelFromUi(app);
            }
        }, app.CustomApplicationType);

        if (CustomProp) {
            CustomProp.save();
        }
    }


    // export module functions
    AppModel.loadAppSettingsToForm = loadAppSettingsToForm;
    AppModel.saveAppSettingsFromForm = saveAppSettingsFromForm;

    // initialize defaults
    AppModel.setConfig({
        load: 'LoadApplication',
        save: 'SaveApplication'
    });

});
