/*jslint browser: true*/
/*global APMjs: false*/
APMjs.withGlobal('SW.APM.EditApp.AppModel', function (AppModel, APMjs) {
    'use strict';

    var Ext = APMjs.assertExt(),
        $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp');

    function loadAppSettingsToForm() {
        var app = this, xPageTitleTmpl, jCombo;

        xPageTitleTmpl = Ext.Template.from('pageTitleTemplate', { disableFormats: false });
        xPageTitleTmpl.append('pageTitle', app);

        $('#appName').val(app.NonEscapedName);
        $('#appDescription').val(app.Description);
        $('#appTags').val(app.Tags.join(', '));

        if (AppModel.editMode !== 'FindProcessesWizard') {
            //set page title
            if (app.Name.search('@{R=APM.Strings;K=ApmCode_NewTemplateName;E=js}') < 0) {
                document.title = '@{R=APM.Strings;K=APMWEBJS_EditApplicationTemplate;E=js}' + ' - ' + app.Name;
            } else {
                document.title = '@{R=APM.Strings;K=APMWEBJS_NewApplicationTemplate;E=js}';
            }

            jCombo = $('#appDetailsView');
            APMjs.linqEach(app.DetailsViewChoices, function (e) {
                jCombo.append($('<option>').attr('value', e.Value).text(e.Name));
            });
            jCombo.val(app.DetailsViewSelection);
        }

        // This is a template so app.Template will be undefined.  That's ok, we handle it and this lets
        // us use the same code for both apps and templates.  Can we move this code to a common place?
        EditApp.initUiState($('#appExecutePollingMethod'), 'ExecutePollingMethod', app, app.template);
        EditApp.initUiState($('#appFrequency'), 'Frequency', app, app.Template);
        EditApp.initUiState($('#appTimeout'), 'Timeout', app, app.Template);
        EditApp.initUiState($('#appDebugLogging'), 'DebugLoggingEnabled', app, app.Template);
        EditApp.initUiState($('#appLogFiles'), 'NumberOfLogFilesToKeep', app, app.Template);
        EditApp.initUiState($('#appUse64Bit'), 'Use64Bit', app, app.Template);

        // init polling method message handler
        APMjs.launch(function () {
            SW.APM.EditApp.PollingMethod.init({
                idEditorRoot: 'appExecutePollingMethod',
                app: app
            });
        });
    }

    function saveAppSettingsFromForm() {
        var model = this, tags;

        tags = $('#appTags').val().split(',');
        tags = APMjs.linqSelect(tags, function (tag) {
            return APMjs.isStr(tag) && tag.trim();
        });

        model.Name = $('#appName').val();
        model.Description = $('#appDescription').val();
        model.Tags = tags;
        model.DetailsViewSelection = $('#appDetailsView').val();

        EditApp.updateModelFromUi($('#appExecutePollingMethod'), model.ExecutePollingMethod, true);
        EditApp.updateModelFromUi($('#appFrequency'), model.Frequency, true);
        EditApp.updateModelFromUi($('#appTimeout'), model.Timeout, true);
        EditApp.updateModelFromUi($('#appDebugLogging'), model.DebugLoggingEnabled, true);
        EditApp.updateModelFromUi($('#appLogFiles'), model.NumberOfLogFilesToKeep, true);
        EditApp.updateModelFromUi($('#appUse64Bit'), model.Use64Bit, true);
    }


    // export module functions
    AppModel.loadAppSettingsToForm = loadAppSettingsToForm;
    AppModel.saveAppSettingsFromForm = saveAppSettingsFromForm;

    // initialize defaults
    AppModel.setConfig({
        load: 'LoadApplicationTemplate',
        save: 'SaveApplicationTemplate'
    });
});
