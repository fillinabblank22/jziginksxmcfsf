/*jslint browser: true, indent: 4*/
/*globals APMjs: false, SW: false, Ext: false*/
/// <reference path="../../../APM.js"/>
APMjs.initGlobalClass('SW.APM.EditApp.DynamicEvidence', function constructor_DynamicEvidence(model) {
    'use strict';
    this.model = model;
}, function initializer_DynamicEvidence(DynamicEvidence, APMjs) {
    'use strict';

    var setBlockVisibility = APMjs.setBlockVisibility,
        $ = APMjs.assertQuery(),
        SF = APMjs.format,
        Ext = APMjs.assertExt(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        prefixAttributes = ['id', 'name', 'for'];

    function getParentColumnFromTemplate(column, templateDynamicColumnSettings) {
        var templateColumn = null;
        APMjs.linqAny(templateDynamicColumnSettings, function (tmplCol) {
            if ((tmplCol.Type === column.Type) && (tmplCol.Name === column.Name)) {
                templateColumn = tmplCol;
                return true;
            }
        });
        return templateColumn;
    }

    function setupDynamicColumnLabelContainer(setting, tmplColumnSettings, jColumn) {
        var jLabelOverrideLink, jLabelViewer,
            isTemplateColumn, isComponentColumn, isLinkedColumn, allowEditColumnLabel, isColumnLabelOverriden,
            showEditor, tmplCol;

        isTemplateColumn = (setting.ComponentID < 0);
        isComponentColumn = (setting.ComponentID > 0) && (setting.ParentID < 0);
        isLinkedColumn = (setting.ComponentID > 0) && (setting.ParentID > 0);

        isColumnLabelOverriden = isLinkedColumn && setting.LabelOverridden;
        allowEditColumnLabel = isTemplateColumn || isComponentColumn || isColumnLabelOverriden;

        showEditor = isLinkedColumn ? isColumnLabelOverriden : allowEditColumnLabel;

        jLabelOverrideLink = $('[apm-col-type="column-label-override-link"]', jColumn);
        jLabelViewer = $('[apm-col-type="column-label-viewer"]', jColumn);

        jLabelOverrideLink = EditApp.Utility.convertLinkToButton(jLabelOverrideLink);
        jLabelOverrideLink.css('display', 'inline-block');

        if (showEditor) {
            $('[apm-col-type="column-label-editor"]', jColumn).val(setting.Label);
        }

        EditApp.Utility.toggleOverrideUi(jColumn, showEditor);

        if (isLinkedColumn) {
            tmplCol = getParentColumnFromTemplate(setting, tmplColumnSettings);
            jLabelViewer.text(tmplCol ? tmplCol.Label : '');
            jLabelOverrideLink.show();
        } else {
            jLabelOverrideLink.hide();
        }
    }

    function onDeleteDynamicColumnClick(cmp, setting, jItem) {
        var model = this.model;
        Ext.Msg.show({
            title: SF('Delete {0}', setting.Name),
            msg: 'Deleting this script output will permanently delete all its historical data.<br/> Do you want to continue?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.WARNING,
            fn: function (pBtn) {
                var tempDynamicSettings = [];
                if (pBtn === 'yes') {
                    APMjs.linqEach(cmp.DynamicColumnSettings, function (tmpSetting) {
                        if (tmpSetting.Name !== setting.Name) {
                            tempDynamicSettings.push(tmpSetting);
                        }
                    });
                    cmp.DynamicColumnSettings = tempDynamicSettings;
                    jItem.remove();
                    if (cmp.loadToForm) {
                        cmp.loadToForm(model);
                    }
                }
            }
        });
        return false;
    }

    function onToggleDisableDynamicColumnClick(cmp, setting, tmplColumnSettings, jColumn, jConvert, jThreshold) {
        var that = this, titleConfirmation, messageConfirmation, enabledColumnsCount;

        if (setting.Disabled) {
            enabledColumnsCount = 0;

            APMjs.linqEach(cmp.DynamicColumnSettings, function (tmpSetting) {
                if ((tmpSetting.Type === 1) && (!tmpSetting.Disabled)) {
                    enabledColumnsCount++;
                }
            });

            if (enabledColumnsCount >= 10) {
                Ext.Msg.show({
                    title: 'Script Output Limitation',
                    msg: 'A maximum of 10 output pairs can be monitored per script &raquo; <a style="color:#369;" href="#" target="_blank">Learn how to format unique identifiers in your script</a>',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
                return false;
            }

            titleConfirmation = SF('Enable {0}', setting.Name);
            messageConfirmation = 'Enabling this script output will start again collecting its historical data.<br/> Do you want to continue?';
        } else {
            titleConfirmation = SF('Disable {0}', setting.Name);
            messageConfirmation = 'Disabling this script output will stop collecting its historical data.<br/> Do you want to continue?';
        }

        Ext.Msg.show({
            title: titleConfirmation,
            msg: messageConfirmation,
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.WARNING,
            fn: function (pBtn) {
                if (pBtn === 'yes') {
                    if (setting.Disabled) {
                        setting.Disabled = false;

                        APMjs.linqAny(cmp.DynamicColumnSettings, function (tmpSetting) {
                            if ((tmpSetting.Name === setting.Name) && (tmpSetting.Type === 0)) {
                                tmpSetting.Disabled = setting.Disabled;
                                return true;
                            }
                        });

                        $('[apm-col-type="enable-button-container"]', jColumn).hide();
                        $('[apm-col-type="disable-button-container"],[apm-col-type="label-container"]', jColumn).show();
                        jConvert.show();
                        jThreshold.show();

                        that.setupDynamicColumnLabelContainer(setting, tmplColumnSettings, jColumn);
                    } else {
                        setting.Disabled = true;

                        APMjs.linqAny(cmp.DynamicColumnSettings, function (tmpSetting) {
                            if ((tmpSetting.Name === setting.Name) && (tmpSetting.Type === 0)) {
                                tmpSetting.Disabled = setting.Disabled;
                                return true;
                            }
                        });

                        $('[apm-col-type="enable-button-container"]', jColumn).show();
                        $('[apm-col-type="disable-button-container"],[apm-col-type="label-container"]', jColumn).hide();
                        jConvert.hide();
                        jThreshold.hide();
                    }
                }
            }
        });

        return false;
    }

    function addAttrPrefix(jEl, name, prefix) {
        jEl.attr(name, prefix + '-' + jEl.attr(name));
    }

    function addAttrPrefixes(jParent, names, prefix) {
        APMjs.linqEach(names, function (name) {
            jParent.find('*[' + name + ']').andSelf().each(function () {
                addAttrPrefix($(this), name, prefix);
            });
        });
    }

    function updateDynamicColumnItem(jItem, cmp, setting, tmplColumnSettings, colNum) {
        var that, compId, jColumn, jConvert, jThreshold, isTemplateColumn, isComponentColumn;

        that = this;
        compId = cmp.Id;

        jColumn = jItem.find('#column-container-' + compId);
        jColumn.attr('id', jColumn.attr('id') + '-' + setting.ID);

        jConvert = jItem.find('[apm-col-type="convert-value-container"]');
        jConvert.attr('id', jConvert.attr('id') + '-' + setting.ID);
        addAttrPrefixes(jConvert, prefixAttributes, setting.ID);

        jThreshold = jItem.find('#threshold-container-' + compId);
        jThreshold.attr('id', jThreshold.attr('id') + '-' + setting.ID);
        addAttrPrefixes(jThreshold, prefixAttributes, setting.ID);

        isTemplateColumn = (setting.ComponentID < 0);
        isComponentColumn = (setting.ComponentID > 0) && (setting.ParentID < 0);

        $('[apm-col-type="row-label"]', jColumn).text('Script Output #' + colNum);
        $('[apm-col-type="column-name"]', jColumn).text(setting.Name);

        if (isTemplateColumn || isComponentColumn) {
            $('[apm-col-type="delete-button-container"]', jColumn).show();
            $('[apm-col-type="disable-button-container"],[apm-col-type="enable-button-container"]', jColumn).hide();

            $('[apm-col-type="column-delete-button-link"]', jColumn).click(function () {
                that.onDeleteDynamicColumnClick(cmp, setting, jItem);
                return false;
            });
        } else {
            $('[apm-col-type="column-disable-button-link"]', jColumn).click(function () {
                that.onToggleDisableDynamicColumnClick(cmp, setting, tmplColumnSettings, jColumn, jConvert, jThreshold);
                return false;
            });

            $('[apm-col-type="column-enable-button-link"]', jColumn).click(function () {
                that.onToggleDisableDynamicColumnClick(cmp, setting, tmplColumnSettings, jColumn, jConvert, jThreshold);
                return false;
            });

            if (setting.Disabled) {
                $('[apm-col-type="enable-button-container"]', jColumn).show();
                $('[apm-col-type="label-container"]', jColumn).hide();
                jConvert.hide();
                jThreshold.hide();
            } else {
                $('[apm-col-type="disable-button-container"]', jColumn).show();
            }

            $('[apm-col-type="delete-button-container"]', jColumn).hide();
        }

        that.setupDynamicColumnLabelContainer(setting, tmplColumnSettings, jColumn);
    }

    function rebindColType(jItem, colType, callback, eventName, trigger) {
        jItem
            .find('[apm-col-type="' + colType + '"]')
            .unbind(eventName)
            .bind(eventName, callback);

        if (trigger) {
            jItem.trigger(eventName);
        }
    }

    function initDynamicColumnsConvertValueEvents(jItem, setting, cmp) {

        function transformCheckboxHandler() {
            setBlockVisibility(setting.ID + '-' + cmp.Id + 'TransformSettingsEditor', this.checked);
        }

        function commonFormulasCheckboxHandler() {
            setBlockVisibility(jItem.find('[apm-col-type="column-common-formulas-div"]'), true);
            setBlockVisibility(jItem.find('[apm-col-type="column-custom-conversion-div"]'), false);
        }

        function customConversionCheckboxHandler() {
            setBlockVisibility(jItem.find('[apm-col-type="column-custom-conversion-div"]'), true);
            setBlockVisibility(jItem.find('[apm-col-type="column-common-formulas-div"]'));
        }

        function commonFormulasListClick() {
            SW.APM.Transformation.ShowCommonFormulasDiv(
                this.selectedIndex,
                [
                    jItem.find('[apm-col-type="column-common-formulas-empty-div"]').attr('id'),
                    jItem.find('[apm-col-type="column-common-formulas-truncate-round-div"]').attr('id'),
                    jItem.find('[apm-col-type="column-common-formulas-truncate-round-div"]').attr('id')
                ]
            );
        }

        function expandCollapseTestImageClick() {
            SW.APM.Transformation.ExpandCollapseTest(
                this,
                jItem.find('[apm-col-type="column-test-expanded-hidden-input"]').attr('id'),
                [
                    jItem.find('[apm-col-type="column-input-table-row"]').attr('id'),
                    jItem.find('[apm-col-type="column-output-table-row"]').attr('id'),
                    jItem.find('[apm-col-type="column-error-table-row"]').attr('id')
                ],
                this
            );
        }

        function retrieveCurrentValueClick() {
            jItem.find('[apm-col-type="column-input-textbox"]').val('Retrieving...');
            EditApp.Grid.Handlers.singleComponentTest(cmp.Id, setting.ID, setting.Name);
        }

        function calculateOutputClick() {
            SW.APM.Transformation.DataTransformEvaluateTest(
                jItem.find('[apm-col-type="column-custom-formula-textbox"]').attr('id'),
                jItem.find('[apm-col-type="column-input-textbox"]').attr('id'),
                jItem.find('[apm-col-type="column-output-span"]').attr('id'),
                jItem.find('[apm-col-type="column-error-div"]').attr('id')
            );
        }

        rebindColType(jItem, 'column-datatransform-enable-checkbox', transformCheckboxHandler, 'change', true);
        rebindColType(jItem, 'column-common-formulas-checkbox', commonFormulasCheckboxHandler, 'change');
        rebindColType(jItem, 'column-custom-conversion-checkbox', customConversionCheckboxHandler, 'change');
        rebindColType(jItem, 'column-common-formulas-list', commonFormulasListClick, 'click');
        rebindColType(jItem, 'column-expand-collapse-test-button', expandCollapseTestImageClick, 'click');
        rebindColType(jItem, 'column-retrieve-current-value-button', retrieveCurrentValueClick, 'click');
        rebindColType(jItem, 'column-calculate-output-button', calculateOutputClick, 'click');
    }

    function showEditScriptDialog(cmp) {
        var model = this.model;

        function callback(testModel) {
            var testComponent = null, credId;

            APMjs.linqAny(testModel.Components, function (tmpComponent) {
                if (tmpComponent.Id === cmp.Id) {
                    testComponent = tmpComponent;
                    return true;
                }
            });

            if (cmp.Settings.ScriptArguments) {
                cmp.Settings.ScriptArguments.Value = testComponent.Settings.ScriptArguments.Value;
                cmp.Settings.ScriptArguments.SettingLevel = testComponent.Settings.ScriptArguments.SettingLevel;
            } else {
                cmp.Settings.CommandLineToPass.Value = testComponent.Settings.CommandLineToPass.Value;
                cmp.Settings.CommandLineToPass.SettingLevel = testComponent.Settings.CommandLineToPass.SettingLevel;
            }

            cmp.Settings.ScriptBody.Value = testComponent.Settings.ScriptBody.Value;
            cmp.Settings.ScriptBody.SettingLevel = testComponent.Settings.ScriptBody.SettingLevel;

            credId = parseInt(testComponent.CredentialSetId.Value, 10);
            if (credId && credId > 0) {
                cmp.CredentialSetId.Value = testComponent.CredentialSetId.Value;
                cmp.CredentialSetId.SettingLevel = testComponent.CredentialSetId.SettingLevel;
            }

            APMjs.linqEach(testComponent.DynamicColumnSettings, function (definition) {
                var exists = APMjs.linqAny(cmp.DynamicColumnSettings, function (cmpColumn) {
                    return (cmpColumn.Name.toLowerCase() === definition.Name.toLowerCase() && cmpColumn.Type === definition.Type);
                });
                if (!exists) {
                    cmp.DynamicColumnSettings.push(definition);
                }
            });

            if (APMjs.isFn(cmp.loadToForm)) {
                cmp.loadToForm(model);
            }
        }

        if (APMjs.isFn(cmp.saveFromForm)) {
            cmp.saveFromForm(model);
        }

        model.saveAppSettingsFromForm();

        SW.APM.Admin.EditScriptDialog.show(cmp.Id, model, callback);
    }

    function saveDynamicColumnItem(jCol, cmp, setting, isTemplate, templateColumn) {
        var jLabel, jEditorContainer, jEditor, isOveridden;

        jLabel = jCol.find('[apm-col-type="label-container"]');
        jEditorContainer = jLabel.find('[apm-col-type="column-label-edit-container"]');
        jEditor = jLabel.find('[apm-col-type="column-label-editor"]');
        isOveridden = !isTemplate && jEditorContainer.is(':visible');

        setting.LabelOverridden = isOveridden;

        if (isTemplate || isOveridden) {
            setting.Label = jEditor.val();
        } else if (templateColumn) {
            setting.Label = templateColumn.Label;
        } else { //fallback
            setting.Label = setting.Name;
        }

        APMjs.linqAny(cmp.DynamicColumnSettings, function (tmpSetting) {
            if ((tmpSetting.Type === 0) && (tmpSetting.Name === setting.Name)) {
                tmpSetting.LabelOverridden = setting.LabelOverridden;
                tmpSetting.Label = setting.Label;
                return true;
            }
        });
    }

    DynamicEvidence.prototype.onDeleteDynamicColumnClick = onDeleteDynamicColumnClick;
    DynamicEvidence.prototype.onToggleDisableDynamicColumnClick = onToggleDisableDynamicColumnClick;
    DynamicEvidence.prototype.setupDynamicColumnLabelContainer = setupDynamicColumnLabelContainer;
    DynamicEvidence.prototype.updateDynamicColumnItem = updateDynamicColumnItem;
    DynamicEvidence.prototype.initDynamicColumnsConvertValueEvents = initDynamicColumnsConvertValueEvents;
    DynamicEvidence.prototype.showEditScriptDialog = showEditScriptDialog;

    DynamicEvidence.getParentColumnFromTemplate = getParentColumnFromTemplate;
    DynamicEvidence.saveDynamicColumnItem = saveDynamicColumnItem;
});
