/*jslint indent: 4, */
/*globals APMjs: false*/

APMjs.initGlobal('SW.APM.Editors.ConvertValueEditor', function (ConvertValueEditor, APMjs) {
    'use strict';


    var $ = APMjs.assertQuery(),
        SW = APMjs.assertGlobal('SW'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors');


    function init(rootName, isMultiEdit, elementId) {

        function setBlockVisibility(id, value) {
            APMjs.setBlockVisibility(rootName + id, value);
        }

        function expandCollapseTestImageClick() {
            SW.APM.Transformation.ExpandCollapseTest(this, rootName + 'TestExpandedHidden', [rootName + 'InputTableRow', rootName + 'OutputTableRow', rootName + 'ErrorTableRow'], this);
        }

        function onRootNameClick() {
            setBlockVisibility('TransformSettingsEditor', this.checked);
        }

        function onCommonFormulasCheckboxClick() {
            setBlockVisibility('CommonFormulasDiv', true);
            setBlockVisibility('CustomConversionDiv', false);
        }

        function initConvertValueEvents(args) {

            var component, currentCmpId, cmpId;

            function onCommonFormulasListClick() {
                SW.APM.Transformation.ShowCommonFormulasDiv(this.selectedIndex, [rootName + 'CommonFormulasEmptyDiv', rootName + 'CommonFormulasTruncateRoundDiv', rootName + 'CommonFormulasTruncateRoundDiv']);
            }

            function onCustomConversionCheckboxClick() {
                setBlockVisibility('CustomConversionDiv', true);
                setBlockVisibility('CommonFormulasDiv', false);
                $('#' + rootName + 'CommonFormulasList').click();
            }

            function onRetrieveCurrentValueClick() {
                if (!SW.APM.EditApp.isComponentTestingNow(cmpId)) {
                    SW.APM.EditApp.Grid.Handlers.singleComponentTest(cmpId);
                }
            }

            function onCalculateOutputClick() {
                SW.APM.Transformation.DataTransformEvaluateTest(rootName + 'CustomFormulaTextbox', rootName + 'InputTextbox', rootName + 'OutputSpan', rootName + 'ErrorDiv');
            }

            isMultiEdit = Editors.parseBool(isMultiEdit);

            if (args && args.record) {
                component = args.record.json;
                currentCmpId = component.Id;
                cmpId = Number(elementId);
                if (currentCmpId !== cmpId) {
                    return;
                }
            }

            setBlockVisibility('TransformSettingsEditor', false);
            setBlockVisibility('CommonFormulasDiv', false);
            setBlockVisibility('CustomConversionDiv', false);

            $('#' + rootName)
                .unbind('click')
                .bind('click', onRootNameClick);

            $('#' + rootName + 'CommonFormulasCheckbox')
                .unbind('click')
                .bind('click', onCommonFormulasCheckboxClick);

            $('#' + rootName + 'CommonFormulasList')
                .unbind('click')
                .bind('click', onCommonFormulasListClick);

            $('#' + rootName + 'CustomConversionCheckbox')
                .unbind('click')
                .bind('click', onCustomConversionCheckboxClick);

            $('#' + rootName + 'ExpandCollapseTestImage').unbind('click').bind('click', expandCollapseTestImageClick);
            $('#' + rootName + 'RetrieveCurrentValue').unbind('click').bind('click', onRetrieveCurrentValueClick);
            $('#' + rootName + 'CalculateOutput').unbind('click').bind('click', onCalculateOutputClick);


            if ($('#' + rootName + 'CommonFormulasCheckbox').is(':checked')) {
                $('#' + rootName + 'CommonFormulasCheckbox').click();
                $('#' + rootName + 'CommonFormulasList').click();
            }

            if ($('#' + rootName + 'CustomConversionCheckbox').is(':checked')) {
                $('#' + rootName + 'CustomConversionCheckbox').click();
            }
        }

        SW.APM.EventManager.on('onGridRowUpdated', initConvertValueEvents);

        if (isMultiEdit) {
            initConvertValueEvents();
        }
    }


    // publish methods
    ConvertValueEditor.init = init;
});
