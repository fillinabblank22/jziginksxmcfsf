﻿/*jslint indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.Editors.OidEditor', function (OidEditor, APMjs) {

    var $ = APMjs.assertQuery();

    function init(rootId) {
        $(function () {
            $('#' + rootId).keypress(function (event) {
                event = event || window.event;
                var ctrlDown = event.ctrlKey || event.metaKey;
                var controlKeys = '';
                var res = $(this).val().split('');
                if (res[res.length - 1] == '.') {
                    controlKeys = '8 9 13 0';
                } else {
                    controlKeys = '8 9 13 46 0';
                }
                var isControlKey = controlKeys.match(new RegExp(event.which));
                if (isControlKey || (event.which > 47 && event.which < 58) || (ctrlDown && (event.which === 99 || event.which === 118 || event.which === 120))) {
                    return;
                } else {
                    event.preventDefault();
                }
            });
            $.validator.addMethod('OIDvalidator',
                function (value, element) {
                    var el = $(element);
                    if (el.is('.OIDvalidator')) {
                        var val = el.val(), rege = /^\d+$/;
                        return rege.test(val.split('.').join(''));
                    }
                }, 'Entered OID is not valid.'
                // TODO: move text to resources
            );
        });
    }

    OidEditor.init = init;
});
