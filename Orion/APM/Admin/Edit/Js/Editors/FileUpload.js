/*jslint indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.Editors.FileUpload', function (module, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        FileUpload = APMjs.assertGlobal('SW.APM.FileUpload');

    function uploadVisibility(rootId, state) {
        APMjs.$show(rootId + 'FileUploadContainer', state);
    }

    function ajaxFileUpload(element) {
        var id;

        function onSuccess(data) {
            var response = JSON.parse($(data).text());
            if (response.Error) {
                Ext.Msg.show({
                    title: '@{R=APM.Strings;K=JS_Error_Message;E=js}',
                    msg: response.Error,
                    minWidth: 250,
                    modal: true,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            } else {
                $('#' + id).val(response.Data).focus();
                $('#button' + id).focus();
                Ext.Msg.show({
                    title: '@{R=APM.Strings;K=APMWEBJS_Success;E=js}',
                    msg: 'Checksum successfully updated.',
                    modal: true,
                    icon: Ext.Msg.INFO,
                    buttons: Ext.Msg.OK
                });
            }
        }

        id = $(element).parents('tr').attr('id').replace('FileUploadContainer', '');
        FileUpload.uploadFile({
            url: '/Orion/APM/Admin/Edit/FileUpload.ashx',
            secureuri: false,
            fileElementId: 'fileToUpload' + id,
            dataType: 'text',
            data: {
                name: 'logan',
                id: 'id',
                method: 'GetHash'
            },
            success: onSuccess
        });
        return false;
    }

    function init(rootId, isMulti) {
        isMulti = Editors.parseBool(isMulti);
        $(function () {
            if (isMulti) {
                uploadVisibility(rootId, false);
            }
        });
    }


    // publish methods
    module.init = init;
    module.ajaxFileUpload = ajaxFileUpload;
    module.uploadVisibility = uploadVisibility;
});
