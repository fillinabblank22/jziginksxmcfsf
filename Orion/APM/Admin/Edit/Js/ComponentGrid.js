/*jslint browser: true, indent: 4, nomen: true*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Grid', function (Grid, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        SF = APMjs.format,
        APM = APMjs.assertGlobal('SW.APM'),
        EM = APMjs.assertGlobal('SW.APM.EventManager'),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        CLS = {
            DisabledText: 'disabledText'
        },
        TXT = {
            PleaseWait: '@{R=APM.Strings;K=APMWEBJS_PleaseWait;E=js}'
        },
        State = {};


    function isFindProcessWizard() {
        return EditApp.AppModel.editMode === 'FindProcessesWizard';
    }

    /*jslint unparam: true*/
    function renderRowClass(record, index, p) {
        // we want to revert the change to p.cols made in RowExpander.js
        p.cols = p.cols + 1;
        return (String(record.json.IsDisabled.Value) === 'true') ? CLS.DisabledText : '';
    }
    /*jslint unparam: false*/

    function validationFailed(evt) {
        var ids = evt.failedRecordIds;
        APMjs.linqEach(ids, function (id) {
            id = parseInt(id, 10);
            var index = State.Store.indexOfId(id);
            if (index >= 0) {
                State.RowExpander.expandRow(index);
            }
        });
    }

    function isAnyToolVisible() {
        var map = State.Grid.getTopToolbar().items.map;
        return APMjs.linqAny(map, function (obj) {
            return obj.hidden === false;
        });
    }

    function checkModel() {
        if (!State.Model) {
            APMjs.console.warn('Model not initialized!');
            return false;
        }
        return true;
    }

    function setItemVisibility(map, key, visible) {
        var item = map[key];
        if (item) {
            item.setVisible(visible);
        }
        item = map['Sep' + key];
        if (item) {
            item.setVisible(visible);
        }
    }

    function updateButtonsVisibility() {
        var editable, toolbar;

        if (!checkModel()) {
            return;
        }

        editable = !State.Model.IsReadOnly || APMjs.isApmDevMode();
        toolbar = State.Grid.getTopToolbar();

        APMjs.linqEach(toolbar.items.map, function (item) {
            item.setVisible(editable);
        });

        toolbar.setVisible(isAnyToolVisible());
    }

    function updateOrderingButtons() {
        if (!checkModel()) {
            return;
        }

        if (State.Model.IsReadOnly && !APMjs.isApmDevMode()) {
            return;
        }

        var map = State.Grid.getTopToolbar().items.map,
            hide = false;

        if (APMjs.isDef(State.Model.ComponentOrderSettingLevel)) {
            if (State.Model.ComponentOrderSettingLevel === 1) {
                map.OverrideOrder.toggle(true);
                map.OverrideOrder.setHandler(State.Main.Handlers.onOverrideOrderClick);
                if (!State.Model.Template) {
                    State.Model.ComponentOrderSettingLevel = 0;
                    hide = true;
                }
            }
            if (State.Model.ComponentOrderSettingLevel === 0) {
                map.OverrideOrder.toggle(false);
                map.OverrideOrder.setHandler(State.Main.Handlers.onInheritOrderClick);
                if (!State.Model.Template) {
                    hide = true;
                }
            }
        } else {
            hide = true;
        }

        if (hide) {
            setItemVisibility(map, 'OverrideOrder', false);
        }
    }

    function onDataLoaded(store, records) {
        var id, record, index;
        id = EditApp.SelectedComponentId;
        if (id > 0) {
            record = APMjs.linqFirst(records, function (r) {
                return r.json.Id === id;
            });
            if (record) {
                State.Selection.selectRecords([record]);
                index = store.indexOf(record);
                if (index > -1) {
                    State.RowExpander.expandRow(index);
                    State.Grid.getView().getRow(index).scrollIntoView();
                }
            }
        }
    }


    function isTemplateComponent(selectedComponentId) {
        if (!checkModel()) {
            return;
        }
        return APMjs.linqAny(State.Model.Components, function (cmp) {
            return (cmp.Id === selectedComponentId && cmp.TemplateId > 0);
        });
    }

    function anyComponentsTestingNow() {
        return APMjs.linqAny(State.Selection.items, function (rec) {
            return rec && rec.json && (EditApp.ComponentsThatTestingNow.indexOf(rec.json.Id) !== -1);
        });
    }

    function enableButton(jButton, enable, handler, additionalClass) {
        var clsDisabled = 'sw-btn-disabled';
        jButton
            .prop('enabled', enable)
            .unbind('click');
        if (enable) {
            if (APMjs.isFn(handler)) {
                jButton.click(handler);
            }
            jButton.removeClass(clsDisabled);
            jButton.addClass('sw-btn');
            if (APMjs.isStr(additionalClass)) {
                jButton.addClass(additionalClass);
            }
        } else {
            jButton.addClass(clsDisabled);
        }
    }

    function enableButtonBar(enable) {
        enable = APMjs.isBool(enable) ? enable : true;
        if (isFindProcessWizard()) {
            return;
        }
        enableButton($('#submitBtn'), enable, EditApp.save, 'sw-btn-primary');
        enableButton($('#applyBtn'), enable, EditApp.applyChanges);
        enableButton($('#cancelBtn'), enable, EditApp.cancel, 'sw-btn-cancel');
    }

    function updateToolbarButtons() {
        var count = State.Selection.getCount(),
            map = State.Grid.getTopToolbar().items.map,
            enabledCount = 0,
            disabledCount = 0,
            selTypes = [],
            numOfSelTypes = 0,
            reEditApp = /EditApplication/,
            selectedComponent;

        State.Selection.each(function (rec) {
            if (!selTypes[rec.json.ComponentType] && rec.json.ComponentType !== 'SqlTable') {
                selTypes[rec.json.ComponentType] = true;
                numOfSelTypes += 1;
            }
            if (String(rec.json.IsDisabled.Value) === 'true') {
                disabledCount += 1;
            }
        });

        enabledCount = count - disabledCount;

        // something has to be selected for these to work.
        selTypes = null;

        map.MultiEdit.setDisabled(count < 2 || numOfSelTypes !== 1);

        map.Assign.setDisabled(count === 0);
        map.Test.setDisabled(count === 0);
        map.Delete.setDisabled(count === 0);

        map.OverrideOrder.setDisabled(false);
        map.Add.setDisabled(false);

        map.SetNode.setDisabled(count === 0 || reEditApp.test(document.location.href));

        // You can only rename one at a time
        map.Rename.setDisabled(count !== 1);

        if (State.Model && count === 1) {

            selectedComponent = 0;
            State.Selection.each(function (rec) {
                selectedComponent = rec.id;
            });

            if (!State.Model.IsTemplate) {
                map.Rename.setDisabled(isTemplateComponent(selectedComponent));
            }
        }

        // these depend on the enabled/disabled state
        map.Enable.setDisabled(disabledCount === 0);
        map.Disable.setDisabled(enabledCount === 0);

        if (State.Model) {
            updateOrderingButtons();
        }

        APMjs.Utility.manageChecker(State.Grid);

        if (anyComponentsTestingNow()) {
            map.Test.setDisabled(true);
            map.SetNode.setDisabled(true);
        }
        if (EditApp.LoadingInProgressNow()) {
            enableButtonBar(false);
            State.Grid.getTopToolbar().disable();
        } else {
            enableButtonBar();
        }
    }

    function loadEditData(component, parentContainer) {
        EditApp.setupUiEvents(component, parentContainer);
        if (component.loadToForm) {
            if (checkModel()) {
                component.loadToForm(State.Model);
            }
        }
    }

    // begin => component disabled/enabled handlers

    function onCmpDisableChanged(sender, record) {
        var recVal = record.json.IsDisabled.Value,
            recLevel = record.json.IsDisabled.SettingLevel,
            jSender = $(sender),
            jEditor = jSender.parent(),
            uiVal;

        if (jEditor.is(':visible')) {
            uiVal = jSender.val();
            if (uiVal === 'true' && String(recVal) === 'false') {
                recVal = true;
                recLevel = 2;
            } else if (uiVal === 'false' && String(recVal) === 'true') {
                recVal = false;
                recLevel = 2;
            }
        } else {
            uiVal = jEditor.next('.viewer').text();
            if (uiVal === 'Disabled' && String(recVal) === 'false') {
                recVal = true;
                recLevel = 1;
            } else if (uiVal === 'Enabled' && String(recVal) === 'true') {
                recVal = false;
                recLevel = 1;
            }
        }
        if (record.json.IsDisabled.Value !== recVal || record.json.IsDisabled.SettingLevel !== recLevel) {
            record.json.IsDisabled.Value = recVal;
            record.json.IsDisabled.SettingLevel = recLevel;
            record.set('IsDisabled', record.json.IsDisabled);
        }
    }

    function onToolbarDisableEnableClick(record) {
        var jBtn, jSel, val;
        jBtn = $(SF('#overridecmpDisabled{0}', record.id));
        if (jBtn.length && jBtn.text() === 'Override Template') {
            jBtn.attr('ignoreOverrideClick', 'true');
            jBtn.click();
            jBtn.removeAttr('ignoreOverrideClick');
        }
        jSel = $(SF('#cmpDisabled{0}', record.id));
        val = String(record.get('IsDisabled').Value);
        if (jSel.length && jSel.val() !== val) {
            jSel.val(val);
            jSel.change();
        }
    }

    function onToolbarCredentialsClick(record) {
        var jBtn, jSel, val, jSub, newCred;

        if (!checkModel()) {
            return;
        }

        jBtn = $(SF('#overridecmpCredential{0}', record.id));

        if (jBtn.length && jBtn.text() === 'Override Template') {
            jBtn.attr('ignoreOverrideClick', 'true');
            jBtn.click();
            jBtn.removeAttr('ignoreOverrideClick');
        }

        val = String(record.get('CredentialSetId').Value);
        jSel = $(APMjs.format('#cmpCredential{0}', record.id));

        if (jSel.length === 0) {
            jSel = $(SF('#cmpCredentialUsernamePassword{0}', record.id));
        }

        if (jSel.length && jSel.val() !== val) {
            jSub = jSel.find(SF('[value={0}]', val));
            if (jSub.length === 0) {
                newCred = State.Model.Credentials.slice(-1)[0];
                jSel.append(SF('<option value="{0}">{1}</option>', val, newCred.Name));
            }
            jSel.val(val);
        }
    }

    // end => component disabled/enabled handlers

    function updateRow(view, rowIndex, record) {
        var rowBody, component, jParent, editorHtml, content, jContext;

        if (!checkModel()) {
            return;
        }

        rowBody = view.getRow(rowIndex);
        component = State.Model.getComponent(record.id);

        if (component) {
            jParent = $('#aspnetForm')
                .find('div.mainAppEditContent')
                .find('table[class="settingsTable"][cmpid="' + component.Id + '"]');

            editorHtml = jParent.eq(0).parent().parent().html();
            content = SF('<form id="componentEditorForm{0}">{1}</form>', component.Id, editorHtml);

            jContext = $(rowBody).find('.componentEditContent');
            jContext.empty().append(content);

            loadEditData(component, jContext);

            EM.fire('onGridRowUpdated', {
                model: State.Model,
                grid: State.Grid,
                rowBody: rowBody,
                record: record
            });

            $(rowBody)
                .find('select[id^=cmpDisabled]')
                .unbind('change')
                .bind('change', function () {
                    onCmpDisableChanged(this, record);
                });

            updateToolbarButtons();
        }
    }

    function onRowUpdated(view, rowIndex, record) {
        APMjs.launch(function () {
            updateRow(view, rowIndex, record);
        });
    }

    function onFipsRelatedOptionChanged() {
        var jOption = $(this).find(':selected'),
            jFips = $(SF('#{0}FipsWarning', this.id));
        if (jOption.attr('fips-related') === 'true') {
            jFips.show();
        } else {
            jFips.hide();
        }
    }

    function bindFipsRelatedHandlers() {
        $('select[fips-related=true]')
            .unbind('change', onFipsRelatedOptionChanged)
            .bind('change', onFipsRelatedOptionChanged)
            .change();
    }

    function onNonSecureConnectionOptionChanged() {
        var jOption = $(this).find(':selected'),
            jNonSecureConnWarning = $(SF('#{0}NonSecureConnWarning', this.id));
        if (jOption.attr('non-secure-connection') === 'true') {
            jNonSecureConnWarning.show();
        } else {
            jNonSecureConnWarning.hide();
        }
    }

    function bindNonSecureConnectionHandlers() {
        $('select[non-secure-connection=true]')
            .unbind('change', onNonSecureConnectionOptionChanged)
            .bind('change', onNonSecureConnectionOptionChanged)
            .change();
    }

    function loadComponentEditorError(component, jqXHR, rowBody) {
        EditApp.Utility.handleException('single', jqXHR.responseText, rowBody, component.Id, State.RowExpander);
    }

    function loadComponentEditor(component, allHtml, rowBody, rowIndex, record) {
        var jContent, jEditor, jRow;

        jContent = $(allHtml);
        if (!EditApp.Utility.isValidResponse(jContent)) {
            loadComponentEditorError(component, { responseText: allHtml }, rowBody);
            return;
        }

        jEditor = jContent.find('div.ComponentEditorParent');

        jEditor
            .find('table, input, select, textarea')
            .attr('cmpid', component.Id);

        jRow = $(rowBody);
        jRow
            .find('.componentEditContent')
            .empty()
            .append(jEditor)
            .addClass('loaded');

        APM.TextAreaResizer.makeTextAreasResizable(jEditor);

        State.RowExpander.bodyContent[component.Id] = jRow.html();

        onRowUpdated(State.Grid.getView(), rowIndex, record);
    }

    function onBeforeRowExpand(record, rowBody, rowIndex) {
        var jRow, component, appItemId, url;

        if (!checkModel()) {
            return;
        }

        jRow = $(rowBody);
        component = record.json;

        if (jRow.find('.componentEditContent.loaded').length) {
            return;
        }

        jRow
            .find('.x-grid3-cell-first')
            .width(State.CheckboxColumnWidth);

        appItemId = State.Model.ApplicationItemId || 0;

        url = SF(
            '/Orion/APM/Admin/Edit/ComponentEditors/{0}.aspx?Id={1}&IsBlackBox={2}&IsTemplate={3}&ApplicationItemId={4}',
            component.ComponentType,
            component.Id,
            State.Model.IsReadOnly,
            State.Model.IsTemplate,
            appItemId
        );

        jRow
            .find('.componentEditContent')
            .load(url, function (response, status, jqXHR) {
                if (status === 'success') {
                    loadComponentEditor(component, response, rowBody, rowIndex, record);
                } else {
                    loadComponentEditorError(component, jqXHR, rowBody);
                }
            });
    }


    function onGridCellClick(grid, rowIndex, columnIndex, evt) {
        columnIndex = parseInt(columnIndex, 10);
        if (columnIndex === 4) {
            var el = $(evt.getTarget());
            if (el.is("[trinfo]")) {
                evt.stopEvent();
                State.Main.TestManager.showErrorMessage(el, grid.getStore().getAt(rowIndex));
            }
        }
        return true;
    }

    function chainRenderFnAndJoinResults(funcs, separator) {
        return APMjs.chainFn(funcs, function (results) {
            return results.join(separator || ' ');
        });
    }

    function setHiddenColumn(column, hidden, keepHideable) {
        if (!APMjs.isStr(column)) {
            throw new TypeError("'column' parameter is not a string value");
        }
        if (!APMjs.isBool(hidden)) {
            throw new TypeError("'hidden' parameter is not a bool value");
        }
        if (!APMjs.isBool(keepHideable)) {
            throw new TypeError("'keepHideable' parameter is either undefined or not bool value");
        }
        var columnModel, index;

        columnModel = State.Grid.getColumnModel();
        index = columnModel.findColumnIndex(column);
        columnModel.config[index].hideable = true;
        columnModel.setHidden(index, hidden);
        if (!keepHideable) {
            columnModel.config[index].hideable = false;
        }
    }

    function enableOrderingArrows(enable) {
        if (!checkModel()) {
            return;
        }

        enable = APMjs.isBool(enable) ? enable : true;
        if (enable) {
            if (!State.Model.ComponentOrderSettingLevel || State.Model.ComponentOrderSettingLevel === 0) {
                APMjs.linqEach(State.Model.Components, function (cmp) {
                    $('#orderArrowUp' + cmp.Id)
                        .attr('src', '/Orion/APM/Images/up_arrow.png')
                        .attr('apm_cmp_loading', 'false');
                    $('#orderArrowDown' + cmp.Id)
                        .attr('src', '/Orion/APM/Images/down_arrow.png')
                        .attr('apm_cmp_loading', 'false');
                });
            }
        } else {
            APMjs.linqEach(EditApp.ComponentsThatLoadingNow, function (cmpId) {
                $('#orderArrowUp' + cmpId)
                    .attr('src', '/Orion/APM/Images/up_arrow_disabled.png')
                    .attr('apm_cmp_loading', 'true');
                $('#orderArrowDown' + cmpId)
                    .attr('src', '/Orion/APM/Images/down_arrow_disabled.png')
                    .attr('apm_cmp_loading', 'true');
            });
        }
    }

    function getToolbarConfig(grid) {

        function createMenu() {
            var items = [
                {
                    text: '@{R=APM.Strings;K=APMWEBJS_ManuallyAddComponent;E=js}',
                    icon: '/Orion/APM/Images/Admin/Icon.ManuallyAddComponentMonitor.png',
                    handler: grid.Handlers.onManuallyAddClick
                },
                {
                    text: '@{R=APM.Strings;K=APMWEBJS_BrowseForComponent;E=js}',
                    icon: '/Orion/APM/Images/Admin/Icon.BrowseForComponentMonitor.png',
                    handler: grid.Handlers.onBrowseAddClick
                }
            ];
            return new Ext.menu.Menu({
                items: items.slice(0, isFindProcessWizard() ? 1 : 2)
            });
        }

        return [
            {
                id: 'MultiEdit',
                text: '@{R=APM.Strings;K=APMWEBJS_MultiEdit;E=js}',
                tooltip: '@{R=APM.Strings;K=APMWEBJS_EditMultipleComponents;E=js}',
                icon: '/Orion/APM/images/Icon.Edit16x16.gif',
                handler: grid.Handlers.onMultiEditClick,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepMultiEdit', hidden: true }),
            {
                id: 'Add',
                xtype: 'splitbutton',
                text: '@{R=APM.Strings;K=APMWEBJS_AddComponentMonitors;E=js}',
                icon: '/Orion/APM/Images/icon_add.gif',
                hidden: true,
                disabled: (EditApp.AppModel.wizardType === 'VmWarePerfCounter'),
                handler: grid.Handlers.onManuallyAddClick,
                menu: createMenu()
            },
            new Ext.Toolbar.Separator({id: 'SepAdd', hidden: true}),
            {
                id: 'Assign',
                text: '@{R=APM.Strings;K=APMWEBJS_AssignCredentials;E=js}',
                icon: '/Orion/APM/Images/assign_credential.png',
                handler: grid.Handlers.onCredentialsClick,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepAssign', hidden: true }),
            {
                id: 'Test',
                text: '@{R=APM.Strings;K=APMWEBJS_AK1_20;E=js}',
                icon: '/Orion/APM/Images/test_credentials_16x16.png',
                handler: grid.Handlers.onTestClick,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepTest', hidden: true }),
            {
                id: 'SetNode',
                text: '@{R=APM.Strings;K=APMWEBJS_SetTestNode;E=js}',
                icon: '/Orion/APM/Images/set_test_node_16x16.png',
                handler: grid.Handlers.onSetNodeClick,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepSetNode', hidden: true }),
            {
                id: 'Rename',
                text: '@{R=APM.Strings;K=APMWEBJS_Rename;E=js}',
                icon: '/Orion/APM/Images/rename_16x16.png',
                handler: grid.Handlers.onRenameClick,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepRename', hidden: true }),
            {
                id: 'Enable',
                text: '@{R=APM.Strings;K=APMWEBJS_Enable;E=js}',
                icon: '/Orion/APM/Images/enable_monitoring.png',
                handler: grid.Handlers.onEnableClick,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepEnable', hidden: true }),
            {
                id: 'Disable',
                text: '@{R=APM.Strings;K=APMWEBJS_Disable;E=js}',
                icon: '/Orion/APM/Images/disable_monitoring.png',
                handler: grid.Handlers.onDisableClick,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepDisable', hidden: true }),
            {
                id: 'OverrideOrder',
                text: '@{R=APM.Strings;K=APMWEBJS_InheritOrderFromTemplate;E=js}',
                icon: '/Orion/APM/Images/InheritOrderFromTemplate.png',
                enableToggle: true,
                hidden: true
            },
            new Ext.Toolbar.Separator({ id: 'SepOverrideOrder', hidden: true }),
            {
                id: 'Delete',
                text: '@{R=APM.Strings;K=APMWEBJS_TM0_15;E=js}',
                icon: '/Orion/APM/Images/delete_16x16.gif',
                handler: grid.Handlers.onDeleteClick,
                hidden: true
            }
        ];
    }

    function init() {
        var DefaultRecord, CustomRecord, gridWidth, config, view;

        State.Main = this;

        Ext.QuickTips.init();

        DefaultRecord = new Ext.data.Record.create([
            'Name', 'ComponentType', 'TestResults', 'IsDisabled', 'CategoryDisplayName'
        ]);

        CustomRecord = Ext.extend(DefaultRecord, {
            set: function (name, value) {
                var rowIndex;

                function updateRowView(rowIndex, colId, colIndex, record, renderer) {
                    var view2 = State.Grid.getView();
                    $(view2.getCell(rowIndex, colIndex))
                        .find(SF('.x-grid3-col-{0}', colId))
                        .html(renderer(value, null, record, rowIndex, colId));

                    if (String(record.json.IsDisabled.Value) === 'true') {
                        $(view2.getRow(rowIndex)).addClass(CLS.DisabledText);
                    } else {
                        $(view2.getRow(rowIndex)).removeClass(CLS.DisabledText);
                    }
                }

                this.data[name] = value;
                rowIndex = State.Store.indexOf(this);
                if (rowIndex > -1) {
                    if (name === 'Name') {
                        updateRowView(rowIndex, 'Name', 2, this, State.Main.Renderers.renderName);
                    } else if (name === 'TestResults') {
                        updateRowView(rowIndex, 'TestResults', 4, this, State.Main.Renderers.renderTestResults);
                    } else if (name === 'IsDisabled') {
                        updateRowView(rowIndex, 'IsDisabled', 5, this, State.Main.Renderers.renderIsDisabled);
                        EM.fire('onDisableChanged', { record: this });
                    }
                }
            }
        });

        State.Store = new Ext.data.JsonStore({
            autoDestroy: true,
            idProperty: 'Id',
            root: 'Components',
            fields: CustomRecord,
            listeners: { load: onDataLoaded }
        });

        /*jslint unparam: true*/
        State.RowExpander = new Ext.ux.grid.RowExpander({
            expandOnEnter: false,
            expandOnDblClick: false,
            lazyRender: false,
            enableCaching: true,
            hideable: false,
            width: 25,
            tpl: $('#componentEditorBody').html(),
            listeners: {
                beforeexpand: function (rowExpander, record, rowBody, rowIndex) {
                    onBeforeRowExpand(record, rowBody, rowIndex);
                },
                expand: function (rowExpander, record, rowBody, rowIndex) {
                    return;
                }
            }
        });
        /*jslint unparam: false*/

        State.Selection = new Grid.CheckboxSelectionModel();
        State.Selection.on('selectionchange', updateToolbarButtons);

        //begin -> update component ui settings
        EM.on('onToolbarDisableClick', onToolbarDisableEnableClick);
        EM.on('onToolbarEnableClick', onToolbarDisableEnableClick);
        EM.on('onToolbarCredentialsClick', onToolbarCredentialsClick);

        //end -> update component ui settings

        EM.on('onDisableChanged', updateToolbarButtons);
        EM.on('onComponentTesting', function () {
            var map = State.Grid.getTopToolbar().items.map;
            if (anyComponentsTestingNow()) {
                map.Test.setDisabled(true);
                map.SetNode.setDisabled(true);
            } else {
                updateToolbarButtons();
            }
        });

        EM.on('onComponentLoading', function () {
            if (EditApp.LoadingInProgressNow()) {
                enableButtonBar(false);
                State.Grid.getTopToolbar().disable();
                enableOrderingArrows(false);
            } else {
                enableButtonBar();
                updateToolbarButtons();
                enableOrderingArrows();
                bindFipsRelatedHandlers();
                bindNonSecureConnectionHandlers();
            }
        });

        gridWidth = $('div.boxedContent').width() - 20;

        config = {
            store: State.Store,
            autoHeight: true,
            width: gridWidth,
            maxHeight: 1000, ///TODO:  What should the max height be????
            plugins: State.RowExpander,
            renderTo: 'grid',
            selModel: State.Selection,
            enableColumnMove: false,
            stripeRows: true,
            loadMask: { msg: "@{R=APM.Strings;K=APMWEBJS_LoadingComponents;E=js}" },
            viewConfig: {
                autoFill: true,
                forceFit: true,
                getRowClass: renderRowClass,
                listeners: { rowupdated: onRowUpdated }
            },
            columns: [
                State.Selection,
                State.RowExpander,
                {
                    id: 'Name',
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_97;E=js}',
                    dataIndex: 'Name',
                    width: 75,
                    hideable: false,
                    renderer: State.Main.Renderers.renderName
                },
                {
                    id: 'ComponentType',
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_98;E=js}',
                    dataIndex: 'ComponentType',
                    width: 60,
                    renderer: State.Main.Renderers.renderComponentType
                },
                {
                    id: 'TestResults',
                    header: '@{R=APM.Strings;K=APMWEBJS_TestNode;E=js}',
                    dataIndex: 'TestResults',
                    renderer: State.Main.Renderers.renderTestResults
                },
                {
                    id: 'IsDisabled',
                    header: '@{R=APM.Strings;K=APMWEBJS_MonitoringStatus;E=js}',
                    dataIndex: 'IsDisabled',
                    width: 35,
                    resizable: false,
                    renderer: State.Main.Renderers.renderIsDisabled
                },
                {
                    id: 'ComponentOrder',
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_89;E=js}',
                    dataIndex: 'ComponentOrder',
                    width: 30,
                    resizable: false,
                    renderer: State.Main.Renderers.renderComponentOrder,
                    align: 'center'
                }
            ],
            tbar: getToolbarConfig(State.Main),
            listeners: { cellclick: onGridCellClick }
        };

        State.Grid = new Ext.grid.GridPanel(config);
        updateToolbarButtons();

        State.LoadCtr = new Ext.LoadMask(State.Grid.getEl(), { msg: "Loading Components. Please wait..." });

        // The RowExpander plugin overwrites our getRowClass implementation.  We need to chain our implementation
        // so that it runs after RowExpander's implementation.
        view = State.Grid.getView();
        view.getRowClass = chainRenderFnAndJoinResults([view.getRowClass, renderRowClass]);

        // The width is different on different browsers.  We'll just ask the grid how big it made the
        // header and we'll do the same for the expanded row body's first column (the one under the checkbox)
        State.CheckboxColumnWidth = view.getHeaderCell(0).style.width;

        State.Main.Renderers.init(State.Grid);
        State.Main.Handlers.init(State.Main, State.Grid, State.RowExpander);
        State.Main.TestManager.init(State.Grid);

        $(window).bind('resize', function () {
            var width = $('div.boxedContent').width() - 20;
            State.Grid.setWidth(Math.max(1100, width));
            State.Grid.syncSize();
        });

        EM.on('onValidationFailed', validationFailed);
    }

    function loadData(model) {
        var readonly, config, targetServer, wizardNodeCredentials;

        State.Model = model;

        updateButtonsVisibility();
        updateOrderingButtons();

        readonly = APMjs.isDef(model.IsReadOnly) ? model.IsReadOnly : false;
        if (readonly) {
            config = [
                State.RowExpander,
                {
                    id: 'Name',
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_97;E=js}',
                    dataIndex: 'Name',
                    width: 75,
                    hideable: false,
                    renderer: State.Main.Renderers.renderName
                },
                {
                    id: 'CategoryDisplayName',
                    header: '@{R=APM.Strings;K=APMWEBJS_Group;E=js}',
                    dataIndex: 'CategoryDisplayName',
                    width: 75,
                    hideable: false
                },
                {
                    id: 'ComponentType',
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_98;E=js}',
                    dataIndex: 'ComponentType',
                    width: 60,
                    renderer: State.Main.Renderers.renderComponentType
                }
            ];

            if (isAnyToolVisible()) {
                config.unshift(State.Selection);
            }

            State.Grid.getColumnModel().setConfig(config);
        }

        if (isFindProcessWizard()) {
            targetServer = EditApp.AppModel.targetNode;
            wizardNodeCredentials = EditApp.AppModel.wizardNodeCredentials;
            if (targetServer) {
                $.each(State.Model.Components, function () {
                    this.TestResults = {
                        nodeId: targetServer.Id,
                        nodeName: targetServer.Name,
                        nodeStatus: targetServer.Status,
                        testStatus: null,
                        messages: ''
                    };
                    if (wizardNodeCredentials.Id && this.CredentialSetId) {
                        this.CredentialSetId.Value = wizardNodeCredentials.Id;
                    }
                });
            }
        }

        State.Store.loadData(State.Model, { add: true });
    }

    function registerEditor(cmpId, onLoad, onSave) {
        var component;

        if (!checkModel()) {
            return;
        }

        component = State.Model.getComponent(cmpId);
        if (!component) {
            APMjs.console.warn(SF('Could not find component [{0}]', cmpId));
        }

        component.loadToForm = onLoad;
        component.saveFromForm = onSave;
    }

    function registerMultiEditor(cmpIds, onLoad, onSave) {
        var component;

        if (!checkModel()) {
            return;
        }

        State.Model.setMultiEditComponent(cmpIds);

        component = State.Model.getMultiEditComponent();
        if (!component) {
            APMjs.console.warn(SF('Could not find multi edit component in [{0}]', cmpIds.join(', ')));
        }

        component.loadToForm = onLoad;
        component.saveFromForm = onSave;
    }

    function showLoadMask(message) {
        State.LoadCtr.msg = message || TXT.PleaseWait;
        State.LoadCtr.show();
    }

    function hideLoadMask() {
        State.LoadCtr.hide();
    }


    // publish methods

    Grid.setHiddenColumn = setHiddenColumn;
    Grid.init = init;
    Grid.loadData = loadData;
    Grid.registerEditor = registerEditor;
    Grid.registerMultiEditor = registerMultiEditor;
    Grid.showLoadMask = showLoadMask;
    Grid.hideLoadMask = hideLoadMask;

    Grid.CheckboxSelectionModel = Ext.extend(Ext.grid.CheckboxSelectionModel, {
        initEvents: function () {
            var that = this;

            if (!that.grid.enableDragDrop && !that.grid.enableDrag) {
                that.grid.on('rowmousedown', that.handleMouseDown, that);
            }

            that.rowNav = new Ext.KeyNav(that.grid.getGridEl(), {
                up: function (e) {
                    if ($(e.target).is("textarea")) {
                        return true;
                    }
                    if (!e.shiftKey || this.singleSelect) {
                        this.selectPrevious(false);
                    } else if (this.last !== false && this.lastActive !== false) {
                        var last = this.last;
                        this.selectRange(this.last, this.lastActive - 1);
                        this.grid.getView().focusRow(this.lastActive);
                        if (last !== false) {
                            this.last = last;
                        }
                    } else {
                        this.selectFirstRow();
                    }
                },
                down: function (e) {
                    if ($(e.target).is("textarea")) {
                        return true;
                    }
                    if (!e.shiftKey || this.singleSelect) {
                        this.selectNext(false);
                    } else if (this.last !== false && this.lastActive !== false) {
                        var last = this.last;
                        this.selectRange(this.last, this.lastActive + 1);
                        this.grid.getView().focusRow(this.lastActive);
                        if (last !== false) {
                            this.last = last;
                        }
                    } else {
                        this.selectFirstRow();
                    }
                },
                scope: that
            });

            that.grid.getView().on({
                scope: that,
                refresh: that.onRefresh,
                rowupdated: that.onRowUpdated,
                rowremoved: that.onRemove
            });

            that.grid.on('render', function () {
                var view = this.grid.getView();
                view.mainBody.on('mousedown', this.onMouseDown, this);
                Ext.fly(view.innerHd).on('mousedown', this.onHdMouseDown, this);
            }, that);
        }
    });
});
