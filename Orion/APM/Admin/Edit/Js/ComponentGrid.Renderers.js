/*jslint browser: true, indent: 4, nomen: true*/
/*global APMjs: false*/
/// <reference path="ComponentGrid.js" />

APMjs.initGlobal('SW.APM.EditApp.Grid.Renderers', function (Renderers, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        SF = APMjs.format,
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        State = {},
        maxComponentOrder = 0;


    function setMaxComponentOrder(value) {
        maxComponentOrder = value;
    }

    function isOrderInherited(model) {
        model = model || EditApp.getModel();
        return model.ComponentOrderSettingLevel === 1;
    }

    function convertToAscii(text) {
        var result = '', code, i;
        for (i = 0; text && i < text.length; i += 1) {
            code = text.charCodeAt(i);
            result += SF('&#{0};', code);
        }
        return result;
    }

    // callback format: render(pVal, pMD, record, pRowIndex)
    /*jslint unparam: true*/
    function renderName(value, unusedMD, record) {
        value = convertToAscii(value);
        return SF(
            '<img class="row-img" src="{0}"/>&nbsp;{1}',
            record.json.StatusIconPath,
            value
        );
    }
    /*jslint unparam: false*/


    // callback format: render(pVal, pMD, record, pRowIndex)
    function renderComponentType(value) {
        return SF(
            '<img src="{0}" height="18" width="0"/>{1}',
            Ext.BLANK_IMAGE_URL,
            EditApp.ComponentDefinitions[value].Name
        );
    }



    function getImgMsg(testStatus, statusDescription) {
        var data = {
            success: {
                img: '/Orion/APM/Images/Admin/icon_check2.gif',
                msg: SF('Test successful with "{0}" status on', statusDescription)
            },
            fail: {
                img: '/Orion/APM/Images/Admin/failed_16x16.gif',
                msg: SF('Test failed with "{0}" status on', statusDescription)
            },
            inprogress: {
                img: '/Orion/APM/Images/Admin/animated_loading_16x16.gif',
                msg: 'Testing against'
            },
            skipped: {
                img: '/Orion/APM/Images/delete_16x16.gif',
                msg: 'Test skipped for disabled component against'
            },
            skippedvalidationerror: {
                img: '/Orion/APM/Images/delete_16x16.gif',
                msg: 'Test skipped due to validation errors'
            },
            skippedtestnodenotspecified: {
                img: '/Orion/APM/Images/Icon.Stop16x16.gif',
                msg: 'Test skipped for due to not specified test node'
            }
        };
        return data[testStatus] || { img: '', msg: 'Will test against' };
    }

    function getNodeInfo(results, imsg) {
        if (results.nodeId) {
            imsg.nodeInfo = SF(
                '<img class="row-img" src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={0}&status={1}&size=Small" /> {2}',
                results.nodeId,
                results.nodeStatus,
                results.nodeName
            );
        } else {
            imsg.nodeInfo = SF('<img src="{0}" height="16" width="0"/> {1}', Ext.BLANK_IMAGE_URL, '@{R=APM.Strings;K=APMWEBJS_TestNodeNotSpecified;E=js}');
            imsg.msg = '';
        }
    }

    function getAddInfo(messages, resultId, imsg) {
        var message, link;
        imsg.addInfo = '';
        if (messages && messages.length) {
            link = SF('<a href="#" trinfo="{0}"> <span class="LinkArrow">&#0187;</span> Details</a>', resultId);
            message = messages[0].split('<br/>').slice(0, 5).join('<br/>').replace(/(<br(\/|\s)*>)+/g, '<br/>');
            imsg.addInfo = '<br/>' + message + '<br/>' + link;
        }
    }

    function getInfo(results) {
        var imsg = getImgMsg(results.testStatus, results.StatusDescription);
        getNodeInfo(results, imsg);
        getAddInfo(results.messages, results.Id, imsg);
        return imsg;
    }

    function getImage(img) {
        return (img && img.length) ? SF('<img class="row-img" src="{0}"/>', img) : '';
    }


    // callback format: render(pVal, pMD, record, pRowIndex)
    /*jslint unparam: true*/
    function renderTestResults(unusedValue, unusedMD, record) {
        var model, info;

        model = EditApp.getModel();

        record.json.TestResults = record.json.TestResults || {
            nodeId: model.NodeId,
            nodeName: model.NodeName,
            nodeStatus: model.NodeStatus,
            testStatus: null,
            status: null,
            statusDescription: null,
            messages: null
        };

        info = getInfo(record.json.TestResults);

        return SF('{0} {1} {2} {3}', getImage(info.img), info.msg, info.nodeInfo, info.addInfo);
    }
    /*jslint unparam: false*/


    // callback format: render(pVal, pMD, record, pRowIndex)
    function renderIsDisabled(value) {
        var isDisabled = String(value.Value) === 'true';
        return SF(
            '<img class="row-img" src="/Orion/APM/Images/Admin/{0}"/> {1}',
            isDisabled ? 'failed_16x16.gif' : 'icon_check2.gif',
            isDisabled ? 'Disabled' : 'Enabled'
        );
    }


    function getIndexForSimpleReordering(direction, record) {
        var index = State.Grid.getStore().indexOf(record);
        if (direction < 0) {
            index -= 1;
            return (index < 0) ? 0 : index;
        }
        index += 1;
        return (index >= State.Grid.getStore().getCount()) ? 0 : index;
    }


    function moveRow(direction) {
        var model = EditApp.getModel(),
            record = State.Grid.getSelectionModel().getSelected(),
            intRegex = /^\d+$/,
            index;

        if (isOrderInherited(model)) {
            return;
        }

        // component is loading now
        if ($('#orderArrowUp' + record.id).attr('apm_cmp_loading') === 'true') {
            return;
        }

        model.saveFromForm();

        if (!record) {
            return;
        }

        // if order input not set
        if (!intRegex.test($('#orderInput' + record.id).val())) {
            Renderers.updateRowsAfterOrdering();
            return;
        }

        index = Renderers.getIndexForSimpleReordering(direction, record);

        State.Grid.getStore().remove(record);
        State.Grid.getStore().insert(index, record);
        State.Grid.getSelectionModel().selectRow(index, true);

        record.commit();
        Renderers.updateRowsAfterOrdering();
    }


    function updateRowsAfterOrdering() {
        var store = State.Grid.getStore(),
            cmpIds = store.data.keys,
            last = cmpIds.length - 1,
            css = {
                hide: { visibility: 'hidden' },
                show: { visibility: '' }
            };

        APMjs.linqEach(cmpIds, function (cmpId, index) {
            var jOrder = $(SF('#orderInput{0}', cmpId));
            jOrder.val(String(index + 1));

            $('#orderArrowUp' + cmpId)
                .css((index === 0) ? css.hide : css.show);

            $('#orderArrowDown' + cmpId)
                .css((index === last) ? css.hide : css.show);
        });
    }


    function getImageName(direction, disabled) {
        return ((direction === 'Up') ? 'up_arrow' : 'down_arrow') + (disabled ? '_disabled' : '');
    }

    function getImageId(direction, recordId) {
        return SF(
            'orderArrow{0}{1}',
            (direction === 'Up') ? 'Up' : 'Down',
            recordId
        );
    }

    function getArrowInfo(model, component) {
        var index = model.Components.indexOf(component);
        return {
            disabled: isOrderInherited(model),
            isFirst: (index === 0),
            isLast: (model.Components.length === (index + 1))
        };
    }

    function getArrow(recordId, direction, info) {
        return SF(
            '<span>&nbsp;<img id="{0}" {1} class="row-img" {2} src="/Orion/APM/Images/{3}.png" {4}/></span>',
            getImageId(direction, recordId),
            info.disabled ? 'disabled="disabled"' : '',
            ((direction === 'Up' && info.isFirst) || (direction === 'Down' && info.isLast)) ? 'style="visibility:hidden"' : '',
            getImageName(direction, info.disabled),
            info.disabled ? '' : SF('onclick="SW.APM.EditApp.Grid.Renderers.moveRow({0});"', (direction === 'Up') ? -1 : 1)
        );
    }

    function getOrderInput(recordId, componentOrder, info) {
        return SF(
            '<input id="orderInput{0}" name="nick" type="text" size="1" value="{1}" {2} style="display:none">',
            recordId,
            componentOrder,
            info.disabled ? 'disabled="disabled"' : ''
        );
    }

    // callback format: render(pVal, pMD, record, pRowIndex)
    /*jslint unparam: true*/
    function renderComponentOrder(unusedValue, unusedMD, record) {
        var model = EditApp.getModel(),
            component = model.getComponent(record.id),
            info = getArrowInfo(model, component);

        if (component.ComponentOrder === 0) {
            maxComponentOrder += 1;
            component.ComponentOrder = maxComponentOrder;
        } else if (component.ComponentOrder > maxComponentOrder) {
            maxComponentOrder = component.ComponentOrder;
        }

        return SF(
            '<span>{0}</span>{1}{2}',
            getOrderInput(record.id, component.ComponentOrder, info),
            getArrow(record.id, 'Up', info),
            getArrow(record.id, 'Down', info)
        );
    }
    /*jslint unparam: false*/


    function init(grid) {
        State.Grid = grid;
    }


    // publish methods

    Renderers.init = init;
    Renderers.setMaxComponentOrder = setMaxComponentOrder;
    Renderers.moveRow = moveRow;
    Renderers.getIndexForSimpleReordering = getIndexForSimpleReordering;
    Renderers.renderName = renderName;
    Renderers.renderComponentType = renderComponentType;
    Renderers.renderTestResults = renderTestResults;
    Renderers.renderIsDisabled = renderIsDisabled;
    Renderers.updateRowsAfterOrdering = updateRowsAfterOrdering;
    Renderers.renderComponentOrder = renderComponentOrder;

});
