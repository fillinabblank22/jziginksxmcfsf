/*jslint browser: true, indent: 4, plusplus: true*/
/*global APMjs: false*/
/// <reference path="../../../APM.js"/>
APMjs.initGlobal('SW.APM.EditApp.Thresholds', function (module, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        SF = APMjs.format,
        SW = APMjs.assertGlobal('SW'),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        EnumOperator = {
            Greater: 0,
            GreaterOrEqual: 1,
            Equal: 2,
            LessOrEqual: 3,
            Less: 4,
            NotEqual: 5
        },
        TEXT = {
            NotSet: '@{R=APM.Strings;K=APMWEBJS_PS0_32;E=js}',
            BaselineDataNotAvail: '@{R=APM.Strings;K=APMWEBJS_PS0_18;E=js}',
            BaselineAvailIn: '@{R=APM.Strings;K=APMWEBJS_PS0_20;E=js}',
            CalculateDuringMaintenance: '@{R=APM.Strings;K=APMWEBJS_PS0_19;E=js}',
            CouldNotBeCalculated: '@{R=APM.Strings;K=APMWEBJS_PS0_28;E=js}',
            NotEnoughDeviation: '@{R=APM.Strings;K=APMWEBJS_PS0_27;E=js}',
            ToBeCalculated: '@{R=APM.Strings;K=APMWEBJS_PS0_31;E=js}',
            InvalidThreshold: '@{R=APM.Strings;K=APMWEBJS_LB0_1;E=js}',
            InvalidThresholdLess: '@{R=APM.Strings;K=APMWEBJS_PS0_42;E=js}',
            InvalidThresholdGreater: '@{R=APM.Strings;K=APMWEBJS_PS0_41;E=js}',
            InvalidPollsMin: '@{R=APM.Strings;K=APMWEBJS_PS0_34;E=js}',
            InvalidPollsMax: '@{R=APM.Strings;K=APMWEBJS_PS0_35;E=js}',
            InvalidPollsDigit: '@{R=APM.Strings;K=APMWEBJS_PS0_39;E=js}',
            InvalidPollsXY: '@{R=APM.Strings;K=APMWEBJS_PS0_40;E=js}'
        },
        RE = {
            OperatorSuffix: /Warning$|Critical$|Operator$/
        };

    function getOperator(jElement) {
        var id = jElement.attr('id').replace(RE.OperatorSuffix, 'Operator');
        return $('select[id^=' + id + '].thresholdOperator');
    }

    function getWarning(jElement) {
        var id = jElement.attr('id').replace(RE.OperatorSuffix, 'Warning');
        return $('#' + id);
    }

    function getCritical(jElement) {
        var id = jElement.attr('id').replace(RE.OperatorSuffix, 'Critical');
        return $('#' + id);
    }

    function validateThresholdOperator(element, checkOperator) {
        var jEl, jWarning, jCritical, jOper, valWarning, valCritical, valOper;

        jEl = $(element);

        jOper = getOperator(jEl);
        jWarning = getWarning(jEl);
        jCritical = getCritical(jEl);

        valWarning = parseFloat(jWarning.val());
        valCritical = parseFloat(jCritical.val());
        valOper = parseInt(jOper.val(), 10);

        if (isNaN(valWarning) || isNaN(valCritical) || valOper === EnumOperator.Equal || valOper === EnumOperator.NotEqual) {
            return true;
        }

        switch (checkOperator) {
        case EnumOperator.Less:
            if ((valOper === EnumOperator.LessOrEqual || valOper === EnumOperator.Less) && (valWarning < valCritical)) {
                return false;
            }
            break;
        case EnumOperator.Greater:
            if ((valOper === EnumOperator.GreaterOrEqual || valOper === EnumOperator.Greater) && (valWarning > valCritical)) {
                return false;
            }
            break;
        }

        return true;
    }

    function reblur(jEl) {
        var hadFocus = jEl.is(':focus');
        jEl.blur();
        if (hadFocus) {
            jEl.focus();
        }
    }

    function validateThresholdOperatorSub(element) {
        var jOper = getOperator($(element));
        APMjs.launch(function () {
            reblur(jOper);
        });
        return true;
    }

    function validateThresholdValidator(element) {
        var result, jEl, val, operator;
        result = true;
        jEl = $(element);

        if (!jEl.is('.validateThreshold')) {
            return true;
        }

        val = jEl.val();
        operator = getOperator(jEl).val();
        if (operator) {
            SW.Core.Services.callWebServiceSync(
                '/Orion/APM/Admin/Edit/Edit.asmx',
                'TestThresholdValue',
                {
                    inputValue: val,
                    thresholdOperator: operator
                },
                function (value) {
                    result = value;
                },
                function () {
                    result = false;
                }
            );
        }
        return result;
    }

    function registerBaselineFormulaValidation() {
        if (!$.validator.methods.validateThreshold) {
            /*jslint unparam: true*/
            $.validator.addMethod('validateThreshold',
                function (value, element) {
                    return validateThresholdValidator(element);
                },
                TEXT.InvalidThreshold);
            /*jslint unparam: false*/
        }
        if (!$.validator.methods.validateOperatorLess) {
            /*jslint unparam: true*/
            $.validator.addMethod('validateOperatorLess',
                function (value, element) {
                    return validateThresholdOperator(element, EnumOperator.Less);
                }, TEXT.InvalidThresholdLess);
            /*jslint unparam: false*/
        }
        if (!$.validator.methods.validateOperatorGreater) {
            /*jslint unparam: true*/
            $.validator.addMethod('validateOperatorGreater',
                function (value, element) {
                    return validateThresholdOperator(element, EnumOperator.Greater);
                }, TEXT.InvalidThresholdGreater);
            /*jslint unparam: false*/
        }
        if (!$.validator.methods.validateOperatorSub) {
            /*jslint unparam: true*/
            $.validator.addMethod('validateOperatorSub',
                function (value, element) {
                    return validateThresholdOperatorSub(element);
                }, '');
            /*jslint unparam: false*/
        }
    }

    function isValidInteger(jElement) {
        var val;
        if (jElement && jElement.is(':visible')) {
            val = jElement.val();
            if (isNaN(val) || parseFloat(val) !== parseInt(val, 10)) {
                return false;
            }
        }
        return true;
    }

    function isHidden(jElement) {
        return !jElement.is(':visible');
    }

    function isIntInRange(jElement, valueMin, valueMax) {
        var val, vali;
        if (jElement.is(':visible')) {
            val = jElement.val();
            vali = parseInt(val, 10);
            if (isNaN(vali) || parseFloat(val) !== vali) {
                return false;
            }
            if (!isNaN(valueMin) && vali < valueMin) {
                return false;
            }
            if (!isNaN(valueMax) && vali > valueMax) {
                return false;
            }
        }
        return true;
    }

    function isCountInInterval(jCount, jInterval) {
        var count, interval;
        if (jInterval.is(':visible')) {
            count = parseInt(jCount.val(), 10);
            interval = parseInt(jInterval.val(), 10);
            if (count >= interval) {
                return false;
            }
        }
        return true;
    }

    function validatePollsMin(element) {
        var jEl = $(element);
        return isHidden(jEl) || isIntInRange(jEl, 1, NaN);
    }

    function validatePollsMax(element) {
        var jEl = $(element);
        return isHidden(jEl) || isIntInRange(jEl, NaN, 60);
    }

    function validatePollsNum(element) {
        var jEl = $(element);
        return isHidden(jEl) || isValidInteger(jEl);
    }


    function getPollsElements(element) {
        var jCount, jInterval;
        jCount = $(element);
        if (jCount.is('.pollsX')) {
            jInterval = $('#' + jCount.attr('id') + 'Interval');
        } else if (jCount.is('.pollsXofY')) {
            jInterval = jCount;
            jCount = $('#' + jInterval.attr('id').replace('Interval', ''));
        }
        return {
            jCount: jCount,
            jInterval: jInterval
        };
    }

    function validatePolls(element) {
        var elements = getPollsElements(element);
        return isCountInInterval(elements.jCount, elements.jInterval);
    }

    function validatePollsSub(element) {
        var elements, jInterval;
        elements = getPollsElements(element);
        jInterval = elements.jInterval;
        APMjs.launch(function () {
            reblur(jInterval);
        });
        return true;
    }

    function registerPollsCountValidation() {
        if (!$.validator.methods.validatePollsNum) {
            /*jslint unparam: true*/
            $.validator.addMethod('validatePollsNum',
                function (value, element) {
                    return validatePollsNum(element);
                },
                TEXT.InvalidPollsDigit);
            /*jslint unparam: false*/
        }
        if (!$.validator.methods.validatePollsMin) {
            /*jslint unparam: true*/
            $.validator.addMethod('validatePollsMin',
                function (value, element) {
                    return validatePollsMin(element);
                },
                TEXT.InvalidPollsMin);
            /*jslint unparam: false*/
        }
        if (!$.validator.methods.validatePollsMax) {
            /*jslint unparam: true*/
            $.validator.addMethod('validatePollsMax',
                function (value, element) {
                    return validatePollsMax(element);
                },
                TEXT.InvalidPollsMax);
            /*jslint unparam: false*/
        }
        if (!$.validator.methods.validatePolls) {
            /*jslint unparam: true*/
            $.validator.addMethod('validatePolls',
                function (value, element) {
                    return validatePolls(element);
                },
                TEXT.InvalidPollsXY);
            /*jslint unparam: false*/
        }
        if (!$.validator.methods.validatePollsSub) {
            /*jslint unparam: true*/
            $.validator.addMethod('validatePollsSub',
                function (value, element) {
                    return validatePollsSub(element);
                },
                '');
            /*jslint unparam: false*/
        }
    }

    function setVisibility(jEl, visible) {
        if (visible) {
            jEl.show();
        } else {
            jEl.hide();
        }
    }

    function getPollInfo(pfx, baseName, itemThreshold) {
        return {
            pollIndex: itemThreshold ? (itemThreshold['Is' + pfx + 'FiredAfterSinglePoll'] ? 0 : itemThreshold['Is' + pfx + 'FiredAfterXPolls'] ? 1 : itemThreshold['Is' + pfx + 'FiredAfterXofYPolls'] ? 2 : -1) : 0,
            polls: itemThreshold ? itemThreshold[pfx + 'Polls'] : '',
            pollsInterval: itemThreshold ? itemThreshold[pfx + 'PollsInterval'] : '',
            selPolls: '#' + baseName + pfx + 'Polls',
            selPollsInterval: '#' + baseName + pfx + 'PollsInterval'
        };
    }

    function updateCountUi(jContainer, pollsInfo) {
        var jPopup, jSelItems, fSingle, fX, fXofY;

        jPopup = jContainer.find('div.popup');
        jSelItems = jPopup.find('input:radio');

        if (pollsInfo) {
            jSelItems.each(function (i) {
                $(this).prop('checked', (i === pollsInfo.pollIndex));
            });

            $(pollsInfo.selPolls).val(pollsInfo.pollIndex !== 0 ? String(pollsInfo.polls) : '');
            $(pollsInfo.selPollsInterval).val(pollsInfo.pollIndex !== 0 ? String(pollsInfo.pollsInterval) : '');
        }

        fSingle = jSelItems.eq(0).is(':checked');
        fX = jSelItems.eq(1).is(':checked');
        fXofY = jSelItems.eq(2).is(':checked');

        setVisibility(jContainer.find('.pollsSingle'), fSingle);
        setVisibility(jContainer.find('.pollsX'), fX || fXofY);
        setVisibility(jContainer.find('.pollsXofY'), fXofY);

        if (fX || fXofY) {
            jContainer.find('input.pollsX').focus();
        }

        jContainer.find('input.count').valid();

        jPopup.siblings('img.selection').attr('src', '/Orion/APM/Images/arrow_down.png');
        jPopup.siblings('span.selection').css({ 'background': '' });
        jPopup.hide();
    }

    function createPollsCountHandlers() {
        // Set validation after loosing focus
        $('input.count').unbind('blur').bind('blur', function () {
            $(this).valid();
        });

        // Set handlers for changing type of status counting
        $('div.pollsCountContainer .selection').unbind('click').bind('click', function (event) {
            var jEl = $(this),
                popup = jEl.parent().children('div.popup');
            popup
                .toggle()
                .position({
                    at: 'right bottom',
                    of: jEl,
                    my: 'right top',
                    collision: 'none'
                }).css('top', '');
            event.stopPropagation();
        });

        $('div.pollsCountContainer span.selection').unbind('mouseover').bind('mouseover', function () {
            var jEl = $(this);
            jEl.next('img').attr('src', '/Orion/APM/Images/arrow_down_bg.png');
            jEl.css({ 'background': 'white url("/Orion/APM/Images/dropdown_arrow_bg.gif") 50% top repeat-x' });
        });

        $('div.pollsCountContainer span.selection').unbind('mouseleave').bind('mouseleave', function () {
            var jEl = $(this);
            if (jEl.siblings('div.popup:hidden').length) {
                jEl.next('img').attr('src', '/Orion/APM/Images/arrow_down.png');
                jEl.css({ 'background': '' });
            }
        });

        $('div.pollsCountContainer img.selection').unbind('mouseover').bind('mouseover', function () {
            $(this).attr('src', '/Orion/APM/Images/arrow_down_bg.png');
            $(this).prev('span').css({ 'background': 'white url("/Orion/APM/Images/dropdown_arrow_bg.gif") 50% top repeat-x' });
        });

        $('div.pollsCountContainer img.selection').unbind('mouseleave').bind('mouseleave', function () {
            var jEl = $(this);
            if (jEl.siblings('div.popup:hidden').length) {
                jEl.attr('src', '/Orion/APM/Images/arrow_down.png');
                jEl.prev('span').css({ 'background': '' });
            }
        });

        $('div.pollsCountContainer div.popup div.type').unbind('click').bind('click', function (event) {
            $(this).find('input:radio').prop('checked', true);
            updateCountUi($(this).closest('div.pollsCountContainer'));
            event.stopPropagation();
        });

        $('.ComponentEditorParent').unbind('click').bind('click', function () {
            var jEl = $(this);
            jEl.find('div.popup:visible').hide();
            jEl.find('div.pollsCountContainer img.selection:visible').attr('src', '/Orion/APM/Images/arrow_down.png');
            jEl.find('div.pollsCountContainer span.selection:visible').css({ 'background': '' });
        });
    }

    function getInt(value, defValue) {
        return parseInt(isNaN(value) ? defValue : value, 10);
    }

    function applyThreshold(jItem, threshold) {
        var baseName = EditApp.getBasename(jItem.attr('id')),
            operator = parseInt(jItem.val(), 10),
            jWarningPolls = $('#' + baseName + 'WarningPolls'),
            jWarningPollsInterval = $('#' + baseName + 'WarningPollsInterval'),
            jCriticalPolls = $('#' + baseName + 'CriticalPolls'),
            jCriticalPollsInterval = $('#' + baseName + 'CriticalPollsInterval'),
            warningValue = $('#' + baseName + 'Warning').val().trim(),
            criticalValue = $('#' + baseName + 'Critical').val().trim(),
            useBaseline = $('#' + baseName + 'chbUseBaseline').is(':checked'),
            warningBaselineValue = ($('#' + baseName + 'WarningBaselineValue').val() || '').trim(),
            criticalBaselineValue = ($('#' + baseName + 'CriticalBaselineValue').val() || '').trim(),
            warningPollsSelector = $('#' + baseName + 'WarningPollsContainer div.popup div.type input:radio:checked').val(),
            criticalPollsSelector = $('#' + baseName + 'CriticalPollsContainer div.popup div.type input:radio:checked').val(),
            valWarningPolls,
            valCriticalPolls;

        // set operator
        threshold.ThresholdOperator = operator;

        // set polls counts and intervals
        if (warningPollsSelector === 'pollsSingle') {
            threshold.WarningPolls = 1;
            threshold.WarningPollsInterval = 1;
            threshold.IsWarningFiredAfterSinglePoll = true;
            threshold.IsWarningFiredAfterXPolls = false;
            threshold.IsWarningFiredAfterXofYPolls = false;
        } else {
            valWarningPolls = getInt(jWarningPolls.val(), 1);
            threshold.WarningPolls = valWarningPolls;
            threshold.IsWarningFiredAfterSinglePoll = false;

            if (warningPollsSelector === 'pollsX') {
                threshold.WarningPollsInterval = threshold.WarningPolls;
                threshold.IsWarningFiredAfterXPolls = true;
                threshold.IsWarningFiredAfterXofYPolls = false;
            } else {
                threshold.WarningPollsInterval = getInt(jWarningPollsInterval.val(), valWarningPolls);
                threshold.IsWarningFiredAfterXPolls = false;
                threshold.IsWarningFiredAfterXofYPolls = true;
            }
        }

        if (criticalPollsSelector === 'pollsSingle') {
            threshold.CriticalPolls = 1;
            threshold.CriticalPollsInterval = 1;
            threshold.IsCriticalFiredAfterSinglePoll = true;
            threshold.IsCriticalFiredAfterXPolls = false;
            threshold.IsCriticalFiredAfterXofYPolls = false;
        } else {
            valCriticalPolls = getInt(jCriticalPolls.val(), 1);
            threshold.CriticalPolls = valCriticalPolls;
            threshold.IsCriticalFiredAfterSinglePoll = false;

            if (criticalPollsSelector === 'pollsX') {
                threshold.CriticalPollsInterval = threshold.CriticalPolls;
                threshold.IsCriticalFiredAfterXPolls = true;
                threshold.IsCriticalFiredAfterXofYPolls = false;
            } else {
                threshold.CriticalPollsInterval = getInt(jCriticalPollsInterval.val(), valCriticalPolls);
                threshold.IsCriticalFiredAfterXPolls = false;
                threshold.IsCriticalFiredAfterXofYPolls = true;
            }
        }

        // set baseline dates
        if (threshold.ComputeBaseline !== false) {
            if (threshold.IsForTemplate === false && warningValue === warningBaselineValue && criticalValue === criticalBaselineValue && (warningValue !== '' || criticalValue !== '')) {
                // baseline thresholds have been applied
                if (String(threshold.WarnLevel) !== warningBaselineValue || String(threshold.CriticalLevel) !== criticalBaselineValue || threshold.BaselineAppliedTicks === null) {
                    // new baseline thresholds have been set -> needs to change dates
                    threshold.BaselineFromTicks = threshold.StatsFromTicks;
                    threshold.BaselineToTicks = threshold.StatsToTicks;
                    threshold.BaselineAppliedTicks = 0; // DateTime.Now will be applied
                }
            } else {
                // baseline thresholds have not been applied
                threshold.BaselineFromTicks = null;
                threshold.BaselineToTicks = null;
                threshold.BaselineAppliedTicks = null;
            }
        }

        if (EditApp.ThresholdsBaseline.useBaselineFormulas(baseName)) {
            // Set threshold formulas
            useBaseline = true;
            threshold.WarningFormula = warningValue;
            threshold.CriticalFormula = criticalValue;

            if (operator === EnumOperator.Greater || operator === EnumOperator.GreaterOrEqual) {
                threshold.WarnLevel = Number.MAX_VALUE;
                threshold.CriticalLevel = Number.MAX_VALUE;
            } else if (operator === EnumOperator.Equal) {
                threshold.WarnLevel = 0.0;
                threshold.CriticalLevel = 0.0;
            } else if (operator === EnumOperator.LessOrEqual || operator === EnumOperator.Less || operator === EnumOperator.NotEqual) {
                threshold.WarnLevel = -Number.MAX_VALUE;
                threshold.CriticalLevel = -Number.MAX_VALUE;
            }
        } else {
            // Set threshold levels
            useBaseline = false;
            threshold.WarningFormula = '';
            threshold.CriticalFormula = '';
            threshold.BaselineApplyError = '';
            threshold.IsWarnLevelSet = warningValue.length && warningValue !== Number.MAX_VALUE && warningValue !== -Number.MAX_VALUE;
            threshold.IsCriticalLevelSet = criticalValue.length && criticalValue !== Number.MAX_VALUE && criticalValue !== -Number.MAX_VALUE;

            if (threshold.IsWarnLevelSet) {
                threshold.WarnLevel = parseFloat(warningValue);
            } else {
                if (operator === EnumOperator.Greater || operator === EnumOperator.GreaterOrEqual) {
                    threshold.WarnLevel = Number.MAX_VALUE;
                } else if (operator === EnumOperator.Equal) {
                    threshold.WarnLevel = 0.0;
                } else if (operator === EnumOperator.LessOrEqual || operator === EnumOperator.Less || operator === EnumOperator.NotEqual) {
                    threshold.WarnLevel = -Number.MAX_VALUE;
                }
            }

            if (threshold.IsCriticalLevelSet) {
                threshold.CriticalLevel = parseFloat(criticalValue);
            } else {
                if (operator === EnumOperator.Greater || operator === EnumOperator.GreaterOrEqual) {
                    threshold.CriticalLevel = Number.MAX_VALUE;
                } else if (operator === EnumOperator.Equal) {
                    threshold.CriticalLevel = 0.0;
                } else if (operator === EnumOperator.LessOrEqual || operator === EnumOperator.Less || operator === EnumOperator.NotEqual) {
                    threshold.CriticalLevel = -Number.MAX_VALUE;
                }
            }
        }

        // Set usage of baseline
        threshold.UseBaseline = useBaseline;
    }


    // *** public methods

    function isNumberNotMax(value) {
        return APMjs.isNum(value) && value !== Number.MAX_VALUE && value !== -Number.MAX_VALUE;
    }

    // Set Get Latest Baseline button
    function setGetLatestButton(baseName, item, itemThreshold, isDynamicEvidence) {
        $('#' + baseName + 'LatestBaseline').unbind('click').bind('click', function () {
            var compId = '',
                columnSchemaId = 0;

            if (isDynamicEvidence) {
                compId = item.ComponentID;
                columnSchemaId = item.ID;
            } else {
                compId = $('#' + baseName + 'ElementId').val();
            }

            EditApp.ThresholdsBaseline.getLatestBaselineThresholds(baseName, compId, columnSchemaId, itemThreshold.Name, itemThreshold.BaselineAvailableIn, itemThreshold.StatsFromTicks, false);
        });
    }

    // Set Latest Baseline Details link
    function setBaseline(baseName, item, itemThreshold, isDynamicEvidence) {
        $('#' + baseName + 'BaselineDetails').unbind('click').bind('click', function () {
            var compId = '',
                statisticColumnName = '',
                displayName = '',
                columnSchemaId = 0,
                warnLevel = '',
                criticalLevel = '';

            if (isNumberNotMax(itemThreshold.WarnLevel)) {
                warnLevel = itemThreshold.WarnLevel;
            }

            if (isNumberNotMax(itemThreshold.CriticalLevel)) {
                criticalLevel = itemThreshold.CriticalLevel;
            }

            if (isDynamicEvidence) {
                compId = item.ComponentID;
                displayName = 'Statistic Data - ' + item.Label;
                statisticColumnName = item.Name;
                columnSchemaId = item.ID;
            } else {
                compId = $('#' + baseName + 'ElementId').val();
                displayName = $('#' + baseName + 'DisplayName').val();
            }

            EditApp.ThresholdsBaseline.showLatestBaselineDetailsDialog(baseName, compId, columnSchemaId, statisticColumnName, itemThreshold.Name, displayName, itemThreshold.ThresholdOperator, warnLevel, criticalLevel, itemThreshold.BaselineAppliedTicks, itemThreshold.BaselineFromTicks, itemThreshold.BaselineToTicks, itemThreshold.BaselineAvailableIn, itemThreshold.StatsFromTicks);
        });
    }

    // Set the editor values and formulas
    function setFormulas(baseName, itemThreshold) {
        var displayWarnFormula, displayCriticalFormula, displayWarn, displayCrit;

        displayWarnFormula = itemThreshold.WarningFormula || EditApp.ThresholdsBaseline.DEFAULT_BASELINE_FORMULA;
        displayWarn = itemThreshold.IsWarnLevelSet ? itemThreshold.WarnLevel : '';

        displayCriticalFormula = itemThreshold.CriticalFormula || EditApp.ThresholdsBaseline.DEFAULT_BASELINE_FORMULA;
        displayCrit = itemThreshold.IsCriticalLevelSet ? itemThreshold.CriticalLevel : '';

        if (itemThreshold.UseBaseline) {
            $('#' + baseName + 'Warning').val(String(displayWarnFormula));
            $('#' + baseName + 'Critical').val(String(displayCriticalFormula));
            $('#' + baseName + 'WarningHelper').val(String(displayWarn));
            $('#' + baseName + 'CriticalHelper').val(String(displayCrit));
        } else {
            $('#' + baseName + 'Warning').val(String(displayWarn));
            $('#' + baseName + 'Critical').val(String(displayCrit));
            $('#' + baseName + 'WarningHelper').val(String(displayWarnFormula));
            $('#' + baseName + 'CriticalHelper').val(String(displayCriticalFormula));
        }
    }

    function joinText(space, args) {
        var text = '';
        APMjs.linqEach(args, function (arg) {
            if (APMjs.isStr(arg) && arg.length) {
                if (text.length) {
                    text += space;
                }
                text += arg;
            }
        });
        return text;
    }

    function firstNonEmpty(args) {
        var choice;
        APMjs.linqAny(args, function (arg) {
            if (APMjs.isStr(arg) && arg.length) {
                choice = arg;
                return true;
            }
        });
        return choice;
    }

    function processOperator(baseName, itemThreshold, templateThreshold, jChild, display) {
        var selector, operatorText, textWarning, textCritical;

        selector = SF('option[value="{0}"]', templateThreshold.ThresholdOperator);
        operatorText = jChild.children(selector).text();

        if (templateThreshold.UseBaseline) {
            $('#' + baseName + 'WarningHelper').val(String(templateThreshold.WarningFormula));
            $('#' + baseName + 'CriticalHelper').val(String(templateThreshold.WarningFormula));

            if (itemThreshold.BaselineApplyError) {
                $('#' + baseName + 'ViewerBaselineInfo span.text').html(TEXT.NotEnoughDeviation); // The data collected did not have enough statistical deviation to calculate viable thresholds. SAM will continually collect data and attempt to provide thresholds during nightly maintenance.<br /><br />If this message persists, consider adding thresholds manually.
                $('#' + baseName + 'WView').text(TEXT.CouldNotBeCalculated).css('font-style', 'italic'); // Thresholds could not be calculated.
                $('#' + baseName + 'CView').text(TEXT.CouldNotBeCalculated).css('font-style', 'italic'); // Thresholds could not be calculated.
                $('#' + baseName + 'ViewerBaselineInfo').addClass('sw-suggestion-warn');
            } else {
                $('#' + baseName + 'WView').text(TEXT.ToBeCalculated).css('font-style', 'italic'); // to be calculated...
                $('#' + baseName + 'CView').text(TEXT.ToBeCalculated).css('font-style', 'italic'); // to be calculated...

                if (itemThreshold.BaselineAvailableIn === null) {
                    $('#' + baseName + 'ViewerBaselineInfo span.text').text(TEXT.BaselineDataNotAvail); // Baseline data information is not available.
                } else if (itemThreshold.BaselineAvailableIn === 0) {
                    $('#' + baseName + 'ViewerBaselineInfo span.text').text(TEXT.CalculateDuringMaintenance); // Thresholds will be calculated during nightly maintenance.
                } else {
                    $('#' + baseName + 'ViewerBaselineInfo span.text').text(String.format(TEXT.BaselineAvailIn, itemThreshold.BaselineAvailableIn)); // Thresholds calculated from baseline data will be available in {0} day(s).
                }
            }

            $('#' + baseName + 'ViewerNoBaselineInfo').hide();
            if (itemThreshold.IsForParentComponent) { // SQLBB: don't show any message for 'template' component
                $('#' + baseName + 'ViewerBaselineInfo').hide();
            }
        } else {
            display.warning = templateThreshold.IsWarnLevelSet ? templateThreshold.WarnLevel : '';
            display.critical = templateThreshold.IsCriticalLevelSet ? templateThreshold.CriticalLevel : '';

            $('#' + baseName + 'WarningHelper').val('');
            $('#' + baseName + 'CriticalHelper').val('');

            textWarning = joinText(' ', [operatorText, firstNonEmpty([String(display.warning), TEXT.NotSet])]);
            textCritical = joinText(' ', [operatorText, firstNonEmpty([String(display.critical), TEXT.NotSet])]);

            $('#' + baseName + 'WView').text(textWarning).css('font-style', 'normal');
            $('#' + baseName + 'CView').text(textCritical).css('font-style', 'normal');

            if (itemThreshold.BaselineAvailableIn === null) {
                $('#' + baseName + 'ViewerNoBaselineInfo span.text').html(TEXT.BaselineDataNotAvail); // Baseline data information is not available.
            } else if (itemThreshold.BaselineAvailableIn === 0) {
                $('#' + baseName + 'ViewerNoBaselineInfo span.text').html('@{R=APM.Strings;K=APMWEBJS_PS0_21;E=js} <a onclick="SW.APM.EditApp.showThresholdMoreInfoDialog();">» @{R=APM.Strings;K=APMWEBJS_PS0_23;E=js}</a>'); // Thresholds calculated from baseline data are available and are not being used. » More
            } else {
                $('#' + baseName + 'ViewerNoBaselineInfo span.text').html(String.format('@{R=APM.Strings;K=APMWEBJS_PS0_22;E=js} <a onclick="SW.APM.EditApp.showThresholdMoreInfoDialog();">» @{R=APM.Strings;K=APMWEBJS_PS0_23;E=js}</a>', itemThreshold.BaselineAvailableIn)); // Baseline data for thresholds calculations will be available in {0} day(s). » More
                $('#' + baseName + 'ViewerNoBaselineInfo').hide(); // finally it should not be visible
            }

            $('#' + baseName + 'ViewerBaselineInfo').hide();
            if (itemThreshold.IsForParentComponent) { // SQLBB: don't show any message for 'template' parent component
                $('#' + baseName + 'ViewerNoBaselineInfo').hide();
            }
        }
    }

    function processItemThreshold(baseName, item, flags, settingPropertyName, jChild, jRow) {
        var itemThreshold, jOperator, jStaticOp,
            defaultThreshold = {
                CriticalLevel: '',
                IsCriticalLevelSet: false,
                IsForTemplate: true,
                IsWarnLevelSet: false,
                Name: item.Name,
                ThresholdOperator: 0,
                WarnLevel: ''
            };

        jOperator = $('#' + baseName + 'Operator');
        jStaticOp = $('#' + baseName + 'CriticalOp');

        if (flags.isDynamicEvidence) { // Dynamic evidence component related
            itemThreshold = item.Threshold || defaultThreshold;

            // Check if colum is from template
            flags.haveTemplate = (item.ComponentID > 0 && item.ParentID > 0);

            // Set label
            jRow.closest('table')
                .find('.thresholdLabel')
                .val(item.Name)
                .text(item.Name + ' Threshold');

            flags.isOverriden = item.ThresholdOverridden;
        } else { // Port/process evidence component related
            itemThreshold = item[settingPropertyName] || defaultThreshold;
            flags.isOverriden = !itemThreshold.IsForTemplate;
        }

        jOperator.val(String(itemThreshold.ThresholdOperator));
        jStaticOp.text(jChild.find('option:selected').text());

        jOperator.unbind('change').bind('change', function () {
            var jSelected = jOperator.find('option:selected');
            jStaticOp.text(jSelected.text());
            $('#' + baseName + 'BaselineApplied').css('visibility', 'hidden');
            $('#' + baseName + 'WarningBaselineValue').val('');
            $('#' + baseName + 'CriticalBaselineValue').val('');
        });

        return itemThreshold;
    }

    function getTemplateThreshold(item, itemTemplate, flags, settingPropertyName) {
        if (flags.isDynamicEvidence) {
            return (itemTemplate && itemTemplate.Threshold) || {
                Name: item.Name,
                ThresholdOperator: 0,
                WarnLevel: '',
                CriticalLevel: '',
                IsWarnLevelSet: false,
                IsCriticalLevelSet: false,
                IsForTemplate: false,
                ComputeBaseline: null,
                UseBaseline: false,
                WarningPolls: 1,
                WarningPollsInterval: 1,
                CriticalPolls: 1,
                CriticalPollsInterval: 1
            };
        }
        return itemTemplate && itemTemplate[settingPropertyName];
    }

    // Set the static template settings
    /*jslint unparam: true*/
    function processTemplateThreshold(baseName, item, itemThreshold, itemTemplate, templateThreshold, flags, display, settingPropertyName, jChild) {
        processOperator(baseName, itemThreshold, templateThreshold, jChild, display);

        if (templateThreshold.IsWarnLevelSet) {
            if (templateThreshold.IsWarningFiredAfterSinglePoll) {
                $('#' + baseName + 'WPollsCountView').text('@{R=APM.Strings;K=APMWEBJS_PS0_36;E=js}'); // for a single poll
            } else if (templateThreshold.IsWarningFiredAfterXPolls) {
                $('#' + baseName + 'WPollsCountView').text(String.format('@{R=APM.Strings;K=APMWEBJS_PS0_37;E=js}', templateThreshold.WarningPolls)); // for at least {0} polls
            } else {
                $('#' + baseName + 'WPollsCountView').text(String.format('@{R=APM.Strings;K=APMWEBJS_PS0_38;E=js}', templateThreshold.WarningPolls, templateThreshold.WarningPollsInterval)); // for at least {0} out of {1} polls
            }
        }

        if (templateThreshold.IsCriticalLevelSet) {
            if (templateThreshold.IsCriticalFiredAfterSinglePoll) {
                $('#' + baseName + 'CPollsCountView').text('@{R=APM.Strings;K=APMWEBJS_PS0_36;E=js}'); // for a single poll
            } else if (templateThreshold.IsCriticalFiredAfterXPolls) {
                $('#' + baseName + 'CPollsCountView').text(String.format('@{R=APM.Strings;K=APMWEBJS_PS0_37;E=js}', templateThreshold.CriticalPolls)); // for at least {0} polls
            } else {
                $('#' + baseName + 'CPollsCountView').text(String.format('@{R=APM.Strings;K=APMWEBJS_PS0_38;E=js}', templateThreshold.CriticalPolls, templateThreshold.CriticalPollsInterval)); // for at least {0} out of {1} polls
            }
        }

        return templateThreshold;
    }
    /*jslint unparam: false*/

    // Show/hide application/template related things
    function toggleTplApp(baseName, itemThreshold, flags, jRow) {
        var jBaseEditorTpl, jBaseEditorApp, componentId;

        jBaseEditorTpl = jRow.find('.templateEditor');
        jBaseEditorApp = jRow.find('.applicationEditor');

        if ((flags.haveTemplate || flags.isOverriden) && !itemThreshold.IsForParentComponent) { // SQLBB: show template controls for 'template' parent component
            componentId = $('#' + baseName + 'ElementId').val();
            if ((componentId && componentId > 0) || (flags.isDynamicEvidence && !baseName.startsWith('-'))) {
                jBaseEditorTpl.hide();
                jBaseEditorApp.show();
            } else {
                jBaseEditorTpl.show();
                jBaseEditorApp.hide();
            }
        } else {
            jBaseEditorTpl.show();
            jBaseEditorApp.hide();
        }
    }

    function onUseBaselineClick(check, baseName, display) {
        if ($(check).is(':checked')) {
            $('#' + baseName + 'Warning').val(EditApp.ThresholdsBaseline.DEFAULT_BASELINE_FORMULA);
            $('#' + baseName + 'Critical').val(EditApp.ThresholdsBaseline.DEFAULT_BASELINE_FORMULA);
        } else {
            $('#' + baseName + 'Warning').val((display && display.warning) || '');
            $('#' + baseName + 'Critical').val((display && display.critical) || '');
        }
    }

    // TODO remove hack after refactor of all edit pages to new framework
    function ensureOperatorElement(jChild) {
        if (jChild) {
            if (!jChild.length) {
                jChild = $(jChild.selector + 'Operator');
            }
            if (jChild.length) {
                return jChild;
            }
        }
        APMjs.console.warn('threshold operator element not found');
    }

    function initThreshold(jChild, settingPropertyName, item, itemTemplate, hideSustainedStatusControls) {
        var jRow, jLink, jViewer, jBaseline, flags, display, itemThreshold, templateThreshold, baseName,
            jBaseWarn, jBaseCrit, jWarn, jCrit, jUseBase, jBaseEdit, jBaseApplied;

        function onThresholdChange() {
            // Baseline icon indication displaying
            var visible = (jBaseWarn.val() !== '' && jBaseCrit.val() !== '' && jBaseWarn === jWarn.val() && jBaseCrit === jCrit.val());

            jBaseApplied.css('visibility', visible ? 'visible' : 'hidden');

            // Editor baselineInfo message displaying and template 'use baseline' checkbox setting
            if (EditApp.ThresholdsBaseline.useBaselineFormulas(baseName)) {
                jBaseEdit.show();
                jUseBase.prop('checked', true);
            } else {
                jBaseEdit.hide();
                jUseBase.prop('checked', false);
            }
        }

        jChild = ensureOperatorElement(jChild);
        if (!jChild) {
            return;
        }

        registerBaselineFormulaValidation();
        registerPollsCountValidation();

        flags = {
            isDynamicEvidence: APMjs.isDef(item.Threshold),
            haveTemplate: APMjs.isSet(itemTemplate),
            isOverriden: false
        };

        display = {
            warning: '',
            critical: ''
        };

        jRow = EditApp.getEditRow(jChild);
        jLink = EditApp.getOverrideLink(jChild, jRow);
        jViewer = EditApp.getViewer(jChild, jRow);
        jBaseline = jRow.find('.baseline');

        jLink = EditApp.Utility.convertLinkToButton(jLink);

        baseName = EditApp.getBasename(jChild.attr('id'));

        jBaseWarn = $('#' + baseName + 'WarningBaselineValue');
        jBaseCrit = $('#' + baseName + 'CriticalBaselineValue');
        jWarn = $('#' + baseName + 'Warning');
        jCrit = $('#' + baseName + 'Critical');
        jUseBase = $('#' + baseName + 'chbUseBaseline');
        jBaseEdit = $('#' + baseName + 'EditorBaselineInfo');
        jBaseApplied = $('#' + baseName + 'BaselineApplied');

        itemThreshold = processItemThreshold(baseName, item, flags, settingPropertyName, jChild, jRow);
        templateThreshold = getTemplateThreshold(item, itemTemplate, flags, settingPropertyName);

        // Show the editor if we are overriding the template.  If there is no template then always show the editor
        EditApp.Utility.toggleOverrideUi(jViewer, flags.haveTemplate ? flags.isOverriden : true);

        updateCountUi($('#' + baseName + 'WarningPollsContainer'), getPollInfo('Warning', baseName, itemThreshold));
        updateCountUi($('#' + baseName + 'CriticalPollsContainer'), getPollInfo('Critical', baseName, itemThreshold));


        // Create event handlers related to number of polls needed for triggering thresholds
        createPollsCountHandlers();

        // Show first help threshold link
        $('div.ComponentEditorParent').find('.thresholdsHelpLink:first').show();

        setGetLatestButton(baseName, item, itemThreshold, flags.isDynamicEvidence);
        setBaseline(baseName, item, itemThreshold, flags.isDynamicEvidence);
        setFormulas(baseName, itemThreshold);

        // Set baseline calculation checkbox
        jUseBase.prop('checked', itemThreshold.UseBaseline);

        // Baseline calculation checkbox  handler
        jUseBase.unbind('click').bind('click', function () {
            onUseBaselineClick(this, baseName, display);
        });

        // Baseline inputs change handler
        $('#' + baseName + 'Warning, #' + baseName + 'Critical').unbind('change').bind('change', onThresholdChange);

        // Set the editor baseline values to track usage of baseline or hide baseline icons
        if (itemThreshold.BaselineAppliedTicks !== null) {
            $('#' + baseName + 'WarningBaselineValue').val(String(display.warning));
            $('#' + baseName + 'CriticalBaselineValue').val(String(display.critical));
        } else {
            $('#' + baseName + 'BaselineApplied').css('visibility', 'hidden');
        }

        // Editor baselineInfo message
        if (itemThreshold.UseBaseline) {
            $('#' + baseName + 'EditorBaselineInfo').show();
        } else {
            $('#' + baseName + 'EditorBaselineInfo').hide();
        }

        if (itemThreshold.BaselineApplyError) {
            $('#' + baseName + 'EditorBaselineInfo span.text').html(TEXT.NotEnoughDeviation); // The data collected did not have enough statistical deviation to calculate viable thresholds. SAM will continually collect data and attempt to provide thresholds during nightly maintenance.<br /><br />If this message persists, consider adding thresholds manually.
            $('#' + baseName + 'EditorBaselineInfo').addClass('sw-suggestion-warn');
        } else {
            if (itemThreshold.BaselineAvailableIn === null) {
                $('#' + baseName + 'EditorBaselineInfo span.text').text(TEXT.BaselineDataNotAvail); // Baseline data information is not available.
            } else if (itemThreshold.BaselineAvailableIn === 0) {
                $('#' + baseName + 'EditorBaselineInfo span.text').text(TEXT.CalculateDuringMaintenance); // Thresholds will be calculated during nightly maintenance.
            } else {
                $('#' + baseName + 'EditorBaselineInfo span.text').text(String.format(TEXT.BaselineAvailIn, itemThreshold.BaselineAvailableIn)); // Thresholds calculated from baseline data will be available in {0} day(s).
            }
        }

        // Viewer (application)
        if (flags.haveTemplate) {
            processTemplateThreshold(baseName, item, itemThreshold, itemTemplate, templateThreshold, flags, display, settingPropertyName, jChild);
            jLink.show();
        } else {
            // Editor
            jLink.hide();
        }

        toggleTplApp(baseName, itemThreshold, flags, jRow);

        // Show/hide baseline controls
        if (itemThreshold.ComputeBaseline === false) {
            jBaseline.hide();
        }

        // Show/hide sustained status condition controls
        if (hideSustainedStatusControls === true) {
            jRow.find('div.pollsCountContainer').css('visibility', 'hidden');
        }

        // the editor/viewer cell is initially hidden to keep from flashing.
        jRow.find('.columnEditor').show();
        jRow.next('tr').find('.columnEditor').show();
    }

    function enableElement(jElement, enabled) {
        var name = 'disabled';
        if (enabled === undefined || enabled) {
            jElement.removeAttr(name);
        } else {
            jElement.attr(name, name);
        }
    }


    function initMultiUiThresholdInternal(jEl, hideBaselineControls) {
        var jRow, jThresholdContainer, jEditor, jRes, browserVer, jOperatorDropdowns, jOperatorLabel, baseName;

        function disableEditor() {
            var jNodes, checked;
            jNodes = jEditor.find('input, select');
            checked = this.checked || false;
            if (jNodes.length) {
                jEl.attr('apm-me-was-changed', checked);
                enableElement(jNodes, checked);
                jRow.find('.thresholdOpStatic').attr('style', checked ? '' : 'color:gray');
            }
            return true;
        }

		jEl = ensureOperatorElement(jEl);
        if (!jEl) {
            return;
        }
		
        registerBaselineFormulaValidation();
        registerPollsCountValidation();

        jRow = EditApp.getEditRow(jEl);
        EditApp.getOverrideLink(jEl, jRow).hide();
        EditApp.getViewer(jEl, jRow).hide();

        jThresholdContainer = jRow.find('.columnEditor.edit.number');
        jThresholdContainer.attr('style', 'padding-top: 0px;');

        jEditor = EditApp.getEditor(jEl, jRow);

        jRes = jRow.prev();

        if ($.browser.msie) {
            browserVer = parseInt($.browser.version, 10);
            if (browserVer !== 9 && browserVer !== 10) {
                jRes = jRow.prev();
                if (!jRes.find('input[id$=MultiChekbox]').length) {
                    jRes = jRes.prev();
                }
            }
        }

        jRes.find('input[id$=MultiChekbox]')
            .click(disableEditor)
            .each(disableEditor);

        jOperatorDropdowns = jRow.find('select.thresholdOperator');
        jOperatorLabel = jRow.find('.thresholdOpStatic');

        jOperatorLabel.text(jOperatorDropdowns.find('option:selected').text());

        jOperatorDropdowns.change(function () {
            var selectedText = $(this).find('option:selected').text();
            jOperatorLabel.text(selectedText);
        });

        baseName = EditApp.getBasename(jEl.attr('id'));

        // Set values and visibility of elements related to number of polls needed for triggering thresholds
        updateCountUi($('#' + baseName + 'WarningPollsContainer'), getPollInfo('Warning', baseName));
        updateCountUi($('#' + baseName + 'CriticalPollsContainer'), getPollInfo('Critical', baseName));

        // Create event handlers related to number of polls needed for triggering thresholds
        createPollsCountHandlers();

        // Show first help threshold link
        $('div.ComponentEditorParent').find('.thresholdsHelpLink:first').show();

        // Baseline calculation checkbox handler
        $('#' + baseName + 'chbUseBaseline').unbind('click').bind('click', function () {
            onUseBaselineClick(this, baseName);
        });

        // Baseline inputs change handler
        $('#' + baseName + 'Warning, #' + baseName + 'Critical').unbind('change').bind('change', function () {
            // 'use baseline' checkbox setting
            $('#' + baseName + 'chbUseBaseline').prop('checked', EditApp.ThresholdsBaseline.useBaselineFormulas(baseName));
        });

        // Hide baseline controls
        if (hideBaselineControls) {
            jRow.find('.baseline').hide();
        }

        // Hide baseline applied icon (always)
        $('#' + baseName + 'BaselineApplied').css('visibility', 'hidden');
    }

    /*jslint unparam: true*/
    function initMultiUiThreshold(jEl, setting, key, hideBaselineControls) {
        initMultiUiThresholdInternal(jEl, hideBaselineControls);
    }
    /*jslint unparam: false*/


    function updateThresholdModelFromUi(jItem, threshold, isTemplate, isDynamicColumn, column, templateColumn) {
        var model, jEditor, isOveridden, templateThreshold, jThresholdOperator, compId, component, template, thresholds;

        jItem = ensureOperatorElement(jItem);
        if (!jItem) {
            return;
        }

        model = EditApp.getModel();
        jEditor = jItem.parents('.editor').eq(0);

        // If the editor is visible, then we are overriding the template (or there isn't one);
        isOveridden = EditApp.isVisible(jEditor);

        threshold.IsForTemplate = (isTemplate || !isOveridden);

        if (isDynamicColumn) {
            column.ThresholdOverridden = (!isTemplate && isOveridden);
        }

        if (isOveridden) {
            // application editor
            applyThreshold(jItem, threshold);
        } else {
            // template editor or application viewer
            templateThreshold = null;

            if (isDynamicColumn) {
                templateThreshold = templateColumn ? templateColumn.Threshold : null;
            } else {
                if (!isTemplate) {
                    jThresholdOperator = jEditor.find('select.thresholdOperator');
                    if (jThresholdOperator.length) {
                        compId = jThresholdOperator.attr('cmpid');
                        if (compId) {
                            component = model.getComponent(compId);
                            template = model.getComponentTemplate(component.TemplateId);
                            thresholds = template ? template.Thresholds : null;
                            if (thresholds) {
                                templateThreshold = thresholds[threshold.Name];
                            }
                        }
                    }
                }
            }

            if (templateThreshold) {
                threshold.WarnLevel = templateThreshold.WarnLevel;
                threshold.IsWarnLevelSet = templateThreshold.IsWarnLevelSet;
                threshold.CriticalLevel = templateThreshold.CriticalLevel;
                threshold.IsCriticalLevelSet = templateThreshold.IsCriticalLevelSet;
                threshold.ThresholdOperator = templateThreshold.ThresholdOperator;
                threshold.ComputeBaseline = templateThreshold.ComputeBaseline;
                threshold.UseBaseline = templateThreshold.UseBaseline;
                threshold.WarningFormula = templateThreshold.WarningFormula;
                threshold.CriticalFormula = templateThreshold.CriticalFormula;
                threshold.BaselineFromTicks = templateThreshold.BaselineFromTicks;
                threshold.BaselineToTicks = templateThreshold.BaselineToTicks;
                threshold.BaselineAppliedTicks = templateThreshold.BaselineAppliedTicks;
                threshold.BaselineApplyError = templateThreshold.BaselineApplyError;
                threshold.WarningPolls = templateThreshold.WarningPolls;
                threshold.WarningPollsInterval = templateThreshold.WarningPollsInterval;
                threshold.CriticalPolls = templateThreshold.CriticalPolls;
                threshold.CriticalPollsInterval = templateThreshold.CriticalPollsInterval;
            }
        }
    }


    function updateMultiThresholdFromUiInternal(jEl, key, cmpIds) {
        var model = EditApp.getModel();
		jEl = ensureOperatorElement(jEl);
        if (EditApp.Utility.parseBoolean(jEl.attr('apm-me-was-changed'))) {
            APMjs.linqEach(cmpIds, function (cmpId) {
                var cmp = model.getComponent(cmpId), threshold;
                if (cmp) {
                    threshold = cmp.Thresholds[key];
                    threshold.IsForTemplate = !model.haveTemplate();
                    applyThreshold(jEl, threshold);
                    if (cmp.loadToForm) {
                        cmp.loadToForm(model);
                    }
                }
            });
        }
    }

    /*jslint unparam: true*/
    function updateMultiThresholdFromUi(jEl, settings, key, cmpIds) {
        updateMultiThresholdFromUiInternal(jEl, key, cmpIds);
    }
    /*jslint unparam: false*/


    module.initThreshold = initThreshold;
    module.initMultiUiThreshold = initMultiUiThreshold;
    module.updateThresholdModelFromUi = updateThresholdModelFromUi;
    module.updateMultiThresholdFromUi = updateMultiThresholdFromUi;
});
