/*jslint indent: 4, plusplus: true, vars: true*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.EditApp.ThresholdsBaseline', function (module, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        SW = APMjs.assertGlobal('SW'),
        CONST;

    CONST = {
        weekday: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        cssLoading: 'url(/Orion/APM/Images/loading_gen_16x16.gif) no-repeat scroll 3px 0px',
        textLoading: '        @{R=APM.Strings;K=APMWEBJS_PS0_30;E=js}'
    };

    function isBaselineFormula(text) {
        if (!APMjs.isStr(text)) {
            return false;
        }
        var t = text.toUpperCase();
        return t === '${USE_BASELINE}' || t.indexOf("${MEAN}") !== -1 || t.indexOf("${STD_DEV}") !== -1;
    }

    function useLatestBaselineThresholds(basename, warning, critical, baselineApplied, isForDetailsDialog) {
        warning = String(warning);
        critical = String(critical);
        if (isForDetailsDialog) {
            if ((warning && isBaselineFormula(warning)) || (critical && isBaselineFormula(critical))) {
                $('#LatestBaselineDetails #WarningLevel').val('').css('background', '');
                $('#LatestBaselineDetails #CriticalLevel').val('').css('background', '');
            } else {
                $('#LatestBaselineDetails #WarningLevel').val(warning).css('background', '');
                $('#LatestBaselineDetails #CriticalLevel').val(critical).css('background', '');
            }

            if (baselineApplied) {
                $('#LatestBaselineDetails img.baseline').css('visibility', 'visible');
                $('#LatestBaselineDetails #WarningBaselineValue').val(warning);
                $('#LatestBaselineDetails #CriticalBaselineValue').val(critical);
            } else {
                $('#LatestBaselineDetails img.baseline').css('visibility', 'hidden');
                $('#LatestBaselineDetails #WarningBaselineValue').val('');
                $('#LatestBaselineDetails #CriticalBaselineValue').val('');
            }
        } else {
            $('#' + basename + 'Warning').val(warning).css('background', '');
            $('#' + basename + 'Critical').val(critical).css('background', '');

            if (baselineApplied) {
                $('#' + basename + 'BaselineApplied').css('visibility', 'visible');
                $('#' + basename + 'EditorBaselineInfo').hide();
                $('#' + basename + 'WarningBaselineValue').val(warning);
                $('#' + basename + 'CriticalBaselineValue').val(critical);
            }
        }
    }

    function preciseRound(num, decimals) {
        return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
    }

    function isInteger(value) {
        var f = parseFloat(value);
        return !isNaN(value) && (f === Math.floor(f));
    }

    function fillStatisticValues(html, className, label, min, max, mean, stdDev) {
        var decimals, result;
        decimals = isInteger(min) && isInteger(max) ? 0 : 2;
        result = html.replace('{CLASS}', className);
        result = result.replace('{PERIOD}', label);
        result = result.replace('{MIN}', preciseRound(min, decimals));
        result = result.replace('{MAX}', preciseRound(max, decimals));
        result = result.replace('{σ}', preciseRound(stdDev, decimals));
        result = result.replace('{-3σ}', preciseRound(mean - 3 * stdDev, decimals));
        result = result.replace('{-2σ}', preciseRound(mean - 2 * stdDev, decimals));
        result = result.replace('{-1σ}', preciseRound(mean - stdDev, decimals));
        result = result.replace('{MEAN}', preciseRound(mean, decimals));
        result = result.replace('{1σ}', preciseRound(mean + stdDev, decimals));
        result = result.replace('{2σ}', preciseRound(mean + 2 * stdDev, decimals));
        result = result.replace('{3σ}', preciseRound(mean + 3 * stdDev, decimals));
        return result;
    }

    // return date from ticks
    function convertTicksToDate(pTicks) {
        try {
            var dateTimeObject = new Date((pTicks - 621355968000000000) / 10000);
            return dateTimeObject;
        } catch (error) {
            return pTicks + ' could not be converted to date.';
        }
    }

    // return date from json string parameter
    function convertStringToDate(dateString) {
        try {
            var ticks = dateString.replace(/\/Date\((-?\d+)\)\//, '$1'); // remove json string prefix and suffix e.g.: /Date(1224043200000)/
            return new Date(parseInt(ticks, 10));
        } catch (error) {
            return dateString + ' could not be converted to date.';
        }
    }

    function rangeEmpty(range) {
        return !range || range.Min === range.max;
    }

    function getDateTimeSince(date) { // this ignores months
        var obj = {}, msec, sec, min, hours, days;
        msec = (new Date()).valueOf() - date.valueOf();
        obj.milliseconds = msec % 1000;
        sec = (msec - obj.milliseconds) / 1000;
        obj.seconds = sec % 60;
        min = (sec - obj.seconds) / 60;
        obj.minutes = min % 60;
        hours = (min - obj.minutes) / 60;
        obj.hours = hours % 24;
        days = (hours - obj.hours) / 24;
        obj.days = days % 365;
        // finally
        obj.years = (days - obj.days) / 365;
        return obj;
    }

    function getDayName(date) {
        return CONST.weekday[date.getUTCDay()];
    }

    function getMonthName(date) {
        return CONST.month[date.getUTCMonth()];
    }

    function getTime(hours, minutes) {
        var ampm = (hours >= 12) ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours || 12; // the hour '0' should be '12'
        minutes = (minutes < 10) ? '0' + minutes : minutes;
        return hours + ':' + minutes + ' ' + ampm;
    }

    function getCalculatedOnDate(date) {
        var daysSinceText = ' ';
        var timeSince = getDateTimeSince(date);
        if (timeSince.days === 0) {
            daysSinceText = ' (Today)';
        } else if (timeSince.days === 1) {
            daysSinceText = ' (Yesterday)';
        } else if (timeSince.days > 1) {
            if (timeSince.years > 0) {
                timeSince.days = timeSince.years * 365 + timeSince.days;
            }
            daysSinceText = ' (' + timeSince.days + ' days ago)';
        }
        return getDayName(date) + ', ' + getMonthName(date) + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear() + ' at ' + getTime(date.getUTCHours(), date.getUTCMinutes()) + daysSinceText;
    }

    function getBaselineUsedDate(date) {
        return getMonthName(date) + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear() + ' at ' + getTime(date.getUTCHours(), date.getUTCMinutes());
    }

    // parse a threshold's statistics
    function parseStatistics(statistics, operator) {
        var i, all, day, night, $tblStatistics, $rowPattern;

        for (i = 0; i < statistics.length; i++) {
            switch (statistics[i].TimeFrameName) {
            case 'APM_All':
                all = statistics[i];
                break;
            case 'APM_WorkHours':
                day = statistics[i];
                break;
            case 'APM_NightAndWeekend':
                night = statistics[i];
                break;
            }
        }

        $tblStatistics = $("#LatestBaselineDetails #tblStatistics tbody");

        // clear statistics details
        $tblStatistics.html('');
        $('#LatestBaselineDetails #BaselineDetails span.calculatedOn').html('');
        $('#LatestBaselineDetails #BaselineDetails span.dataUsed').html('');

        $rowPattern = '<tr class="{CLASS}">' +
            '"<td>{PERIOD}</td>' +
            '<td><div class="statisticValue">{MIN}</div></td>' +
            '<td><div class="statisticValue">{MAX}</div></td>' +
            '<td>{σ}</td>' +
            '<td><div class="statisticValue">{-3σ}</div></td>' +
            '<td><div class="statisticValue">{-2σ}</div></td>' +
            '<td><div class="statisticValue">{-1σ}</div></td>' +
            '<td><div class="statisticValue">{MEAN}</div></td>' +
            '<td><div class="statisticValue">{1σ}</div></td>' +
            '<td><div class="statisticValue">{2σ}</div></td>' +
            '<td><div class="statisticValue">{3σ}</div></td>' +
            '</tr>';

        // empty statistics
        if (!statistics.length) {
            // Statistics are not available.
            $tblStatistics.append('<tr style="height:66px"><td colspan="11">@{R=APM.Strings;K=APMWEBJS_PS0_33;E=js}</td></tr>');
        } else if (rangeEmpty(day) && rangeEmpty(night) && rangeEmpty(all)) {
            // standard deviation is zero
            // Statistics are not available. Standard deviation is zero.
            $tblStatistics.append('<tr style="height:66px"><td colspan="11">@{R=APM.Strings;K=APMWEBJS_PS0_33;E=js} @{R=APM.Strings;K=APMWEBJS_PS0_26;E=js}</td></tr>');
        } else {
            // fill day row
            if (day) {
                var $rowDay = fillStatisticValues($rowPattern, 'day', '<img class="workdays" alt="sun_icon" src="/Orion/APM/images/baselineDay.png" />', day.Min, day.Max, day.Mean, day.StdDev);
                $tblStatistics.append($rowDay);
            }

            // fill night row
            if (night) {
                var $rowNight = fillStatisticValues($rowPattern, 'night', '<img class="offdays" alt="moon_icon" src="/Orion/APM/images/baselineNight.png" />', night.Min, night.Max, night.Mean, night.StdDev);
                $tblStatistics.append($rowNight);
            }

            // fill all hours row and latest baseline details div
            if (all) {
                var $rowAll = fillStatisticValues($rowPattern, 'all', 'All hours', all.Min, all.Max, all.Mean, all.StdDev);
                $tblStatistics.append($rowAll);

                $tblStatistics
                    .find('tr.all td:first')
                    .css('font-size', '9px');

                module.highlightRecommendedThresholds(operator);

                var calculatedOnText = getCalculatedOnDate(SW.Core.Date.ToServerTime(convertStringToDate(all.Timestamp)));
                $('#LatestBaselineDetails #BaselineDetails span.calculatedOn').text(calculatedOnText);

                var dataUsedText = getBaselineUsedDate(SW.Core.Date.ToServerTime(convertStringToDate(all.MinDateTime))) + ' - ' + getBaselineUsedDate(SW.Core.Date.ToServerTime(convertStringToDate(all.MaxDateTime)));
                $('#LatestBaselineDetails #BaselineDetails span.dataUsed').text(dataUsedText);
            }
        }
    }

    function loadAreaChart(id, statisticColumnName, thresholdName, thresholdDisplayName, chartWidth, columnSchemaId) {
        // function created inside chart control
        SW.APM.BaselineDetails.callLoadFn('ccArea', [id, statisticColumnName, thresholdName, thresholdDisplayName, chartWidth, columnSchemaId]);
    }

    function loadMinMaxChart(id, statisticColumnName, thresholdName, thresholdDisplayName, chartWidth) {
        // function created inside chart control
        SW.APM.BaselineDetails.callLoadFn('ccMinMax', [id, statisticColumnName, thresholdName, thresholdDisplayName, chartWidth]);
    }

    function setBaselineStatisticsAndDetails(id, statisticColumnName, thresholdName, thresholdDisplayName, columnSchemaId, operator) {
        // show loading masks
        $("#LatestBaselineDetails #tblStatistics tbody")
            .empty()
            .append("<tr style='height:66px'><td colspan='11'><img src='/Orion/APM/Images/loading_gen_16x16.gif' />&nbsp;@{R=APM.Strings;K=APMWEBJS_PS0_29;E=js}</td></tr>");

        $('#LatestBaselineDetails #BaselineDetails span.calculatedOn')
            .empty()
            .append("<img src='/Orion/APM/Images/loading_gen_16x16.gif' />&nbsp;@{R=APM.Strings;K=APMWEBJS_PS0_29;E=js}");

        $('#LatestBaselineDetails #BaselineDetails span.dataUsed')
            .empty()
            .append("<img src='/Orion/APM/Images/loading_gen_16x16.gif' />&nbsp;@{R=APM.Strings;K=APMWEBJS_PS0_29;E=js}");

        var ORION = APMjs.assertGlobal('ORION');

        ORION.callWebService('/Orion/APM/Admin/Edit/Edit.asmx', 'GetBaselineDetails',
            { componentId: id, columnSchemaId: columnSchemaId, thresholdName: thresholdName },
            function (statistics) {
                // parse statistics
                parseStatistics(statistics, operator);

                // load a chart on the first tab (function created inside a chart control)
                loadAreaChart(id, statisticColumnName, thresholdName, thresholdDisplayName, null, columnSchemaId);

                // table's tooltip events
                $('div.statisticValue').unbind('mouseover').bind('mouseover', function () {
                    $('#StastisticsTooltipValue').text($(this).text());
                    $('#StastisticsTooltip .recommended').hide();
                    $('#StastisticsTooltip a').removeClass('disabled');

                    $('#tblStatistics div.inTooltip').removeClass('inTooltip');
                    $(this).addClass('inTooltip');

                    var tooltip = $('#StastisticsTooltip');
                    if ($(this).hasClass('warning')) {
                        $('#StastisticsTooltip #lnkSetWarning').addClass('disabled');
                    }
                    if ($(this).hasClass('critical')) {
                        $('#StastisticsTooltip #lnkSetCritical').addClass('disabled');
                    }
                    if ($(this).hasClass('recommendedWarning')) {
                        $('#StastisticsTooltip #lblRecomendedWarning').show();
                    }
                    if ($(this).hasClass('recommendedCritical')) {
                        $('#StastisticsTooltip #lblRecomendedCritical').show();
                    }

                    tooltip.show().position({ at: 'right bottom', of: $(this), my: 'left bottom' });

                    module.redrawPlotLineInCharts($(this).text(), false);
                });

                $('#StastisticsTooltip').unbind('mouseleave').bind('mouseleave', function () {
                    $(this).hide();
                    module.redrawPlotLineInCharts(null, true);
                });

                $('#tblStatistics').unbind('mouseleave').bind('mouseleave', function () {
                    $('#StastisticsTooltip').hide();
                    module.redrawPlotLineInCharts(null, true);
                });

                $('#StastisticsTooltip #lnkSetWarning').unbind('click').bind('click', function () {
                    if ($(this).hasClass('disabled')) {
                        return false;
                    }
                    $('#StastisticsTooltip').hide();
                    $('#tblStatistics div.warning').removeClass('warning');
                    $('#tblStatistics div.inTooltip').addClass('warning');
                    module.redrawPlotBandsInCharts();
                });

                $('#StastisticsTooltip #lnkSetCritical').unbind('click').bind('click', function () {
                    if ($(this).hasClass('disabled')) {
                        return false;
                    }
                    $('#StastisticsTooltip').hide();
                    $('#tblStatistics div.critical').removeClass('critical');
                    $('#tblStatistics div.inTooltip').addClass('critical');
                    module.redrawPlotBandsInCharts();
                });

                // time frames tooltip events
                $('img.workdays').unbind('mouseover').bind('mouseover', function () {
                    $('div.APM_WorkHours').show().position({ at: 'right bottom', of: $(this), my: 'left bottom' });
                    $('#StastisticsTooltip').hide();
                });

                $('img.offdays').unbind('mouseover').bind('mouseover', function () {
                    $('div.APM_NightAndWeekend').show().position({ at: 'right bottom', of: $(this), my: 'left bottom' });
                    $('#StastisticsTooltip').hide();
                });

                $('img.workdays').unbind('mouseout').bind('mouseout', function () {
                    $('div.APM_WorkHours').hide();
                });

                $('img.offdays').unbind('mouseout').bind('mouseout', function () {
                    $('div.APM_NightAndWeekend').hide();
                });
            },
            function (error) {
                //window.console && console.log(error);

                $('#LatestBaselineThresholdsErrorMajor span.message').html('@{R=APM.Strings;K=APMWEBJS_PS0_24;E=js}<br/><br/>' + error); //Statistics could not be loaded.
                $('#LatestBaselineThresholdsErrorMajor').dialog({
                    width: 350,
                    modal: true,
                    close: function () {
                        $("#LatestBaselineDetails #tblStatistics tbody").empty().append("<tr style='height:66px'><td colspan='11'>@{R=APM.Strings;K=APMWEBJS_PS0_24;E=js}</td></tr>");
                        // load a chart on the first tab (function created inside a chart control)
                        loadAreaChart(id, statisticColumnName, thresholdName, thresholdDisplayName, null, columnSchemaId);
                    }
                });
            });
    }

    function destroyChart(parentDiv) {
        var chart = parentDiv.data();
        if (chart) {
            try {
                chart.destroy();
            } catch (ignore) {
            }
        }
    }

    function resizeChart(parentDiv) {
        var chart = parentDiv.data();
        if (chart) {
            try {
                chart.setSize(parentDiv.width(), chart.chartHeight, false);
            } catch (ignore) {
            }
        }
    }

    function resizeCharts(activeTab, basename, id, statisticColumnName, thresholdName, thresholdDisplayName, columnSchemaId) {
        var newWidth = 0;

        // Area chart - setSize() method does not work for highcharts in core chart framework -> reloading is needed
        if (activeTab === 0) {
            newWidth = $('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first().width();
            loadAreaChart(id, statisticColumnName, thresholdName, thresholdDisplayName, newWidth, columnSchemaId);
        }
        // MinMax chart
        if (activeTab === 1) {
            var $parentDiv = $('#LatestBaselineDetails #Tabs #tabMetricOverTime').find('.hasChart').first();
            newWidth = $parentDiv.width();
            resizeChart($parentDiv);
        }

        $('#LatestBaselineDetails #Tabs #ChartsWidth').val(newWidth);
    }

    function openThresholdsDetailsDialog(basename, id, statisticColumnName, thresholdName, thresholdDisplayName, columnSchemaId) {
        $('#LatestBaselineDetails').dialog({
            title: 'Latest Baseline Details (' + thresholdDisplayName + ')',
            modal: true,
            minHeight: 660,
            maxHeight: 660,
            resizable: true,
            minWidth: 850,
            width: 850,
            open: function () {
                $("#LatestBaselineDetails #Tabs").tabs({
                    selected: 0
                });

                destroyChart($('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first());
                $('#LatestBaselineDetails #Tabs #tabOccurrences table.chartLegend').empty();

                $("#LatestBaselineDetails #Tabs").unbind('tabsselect').bind('tabsselect', function (event2, ui2) {
                    var chartWidth = $('#LatestBaselineDetails #Tabs #ChartsWidth').val();

                    if (ui2.index === 0) {
                        //if (!$('#LatestBaselineDetails #Tabs #AreaChartLoaded').attr('checked')) {
                        loadAreaChart(id, statisticColumnName, thresholdName, thresholdDisplayName, chartWidth, columnSchemaId);
                        //}
                    }

                    if (ui2.index === 1) {
                        //if (!$('#LatestBaselineDetails #Tabs #MinMaxChartLoaded').attr('checked')) {
                        loadMinMaxChart(id, statisticColumnName, thresholdName, thresholdDisplayName, chartWidth);
                        //}
                    }
                });

                // Adding help link
                $('div.ui-dialog-titlebar:has(#ui-dialog-title-LatestBaselineDetails)')
                    .append('<a target="_blank" href="' + $('#HelpLinkUrl').text() + '"><img src="/Orion/Images/Help_Icon_10x10px.png" alt="Help link"></img><span class="helpText">Help</span></a>');
            },
            resizeStop: function () {
                resizeCharts($("#LatestBaselineDetails #Tabs").tabs('option', 'selected'), basename, id, statisticColumnName, thresholdName, thresholdDisplayName, columnSchemaId);
            },
            close: function () {
                if ($('#LatestBaselineDetails #ApplyThresholds').attr('checked')) {
                    var dialogWarningLevel = $('#LatestBaselineDetails #WarningLevel').val();
                    var dialogCriticalLevel = $('#LatestBaselineDetails #CriticalLevel').val();
                    var dialogBaselineWarningLevel = $('#LatestBaselineDetails #WarningBaselineValue').val();
                    var dialogBaselineCriticalLevel = $('#LatestBaselineDetails #CriticalBaselineValue').val();

                    $('select[id^=' + basename + '].thresholdOperator').val($('#LatestBaselineDetails select.thresholdDialogOperator').val());
                    $('select[id^=' + basename + '].thresholdOperator').change();
                    $('#' + basename + 'Warning').val(dialogWarningLevel);
                    $('#' + basename + 'Critical').val(dialogCriticalLevel);

                    if ((dialogWarningLevel || dialogCriticalLevel) && dialogWarningLevel === dialogBaselineWarningLevel && dialogCriticalLevel === dialogBaselineCriticalLevel) {
                        $('#' + basename + 'WarningBaselineValue').val(dialogWarningLevel);
                        $('#' + basename + 'CriticalBaselineValue').val(dialogCriticalLevel);
                        $('#' + basename + 'BaselineApplied').css('visibility', 'visible');
                        $('#' + basename + 'EditorBaselineInfo').hide();
                    } else {
                        $('#' + basename + 'WarningBaselineValue').val('');
                        $('#' + basename + 'CriticalBaselineValue').val('');
                        $('#' + basename + 'BaselineApplied').css('visibility', 'hidden');
                    }
                }

                $('#LatestBaselineDetails #Tabs #ChartsWidth').val('');
            }
        });
    }

    function setBaselineDataForCurrentThresholdsDiv(baselineAppliedTicks, baselineFromTicks, baselineToTicks, warnLevel, criticalLevel) {
        if (baselineAppliedTicks !== null && baselineFromTicks !== null && baselineToTicks !== null) {
            $('#LatestBaselineDetails #DataForCurrentThresholds').show();
            $('#LatestBaselineDetails img.baseline').css('visibility', 'visible');
            $('#LatestBaselineDetails #WarningBaselineValue').val(warnLevel);
            $('#LatestBaselineDetails #CriticalBaselineValue').val(criticalLevel);

            if (baselineAppliedTicks === 0) {
                $('#LatestBaselineDetails #DataForCurrentThresholds span.calculatedOn').text(getCalculatedOnDate(SW.Core.Date.ToServerTime(new Date())));
            } else {
                $('#LatestBaselineDetails #DataForCurrentThresholds span.calculatedOn').text(getCalculatedOnDate(SW.Core.Date.ToServerTime(convertTicksToDate(baselineAppliedTicks))));
            }

            var dataUsedText = getBaselineUsedDate(SW.Core.Date.ToServerTime(convertTicksToDate(baselineFromTicks))) + ' through ' + getBaselineUsedDate(SW.Core.Date.ToServerTime(convertTicksToDate(baselineToTicks)));
            $('#LatestBaselineDetails #DataForCurrentThresholds span.dataUsed').text(dataUsedText);
        } else {
            $('#LatestBaselineDetails #DataForCurrentThresholds').hide();
            $('#LatestBaselineDetails img.baseline').css('visibility', 'hidden');
        }
    }

    function setLoading(elOrId) {
        $(elOrId).css('background', CONST.cssLoading).val(CONST.textLoading);
    }

    // PUBLIC functions

    function isExtreme(value) {
        var f = parseFloat(value);
        return ((f === Number.MAX_VALUE) || (f === -Number.MAX_VALUE));
    }

    function getLatestBaselineThresholds(basename, componentId, columnSchemaId, thresholdName, availableInDays, statsFromTick, isForDetailsDialog) {
        var warningLevel = $('#' + basename + 'Warning').val();
        var criticalLevel = $('#' + basename + 'Critical').val();
        var warningFormula = $('#' + basename + 'Warning').val();
        var criticalFormula = $('#' + basename + 'Critical').val();
        var operator = !isForDetailsDialog ? $('select[id^=' + basename + '].thresholdOperator').val() : $('#LatestBaselineDetails select.thresholdDialogOperator').val();

        if (!isNaN(warningLevel) || !isNaN(criticalLevel)) {
            // Using default formulas
            warningFormula = module.DEFAULT_BASELINE_FORMULA;
            criticalFormula = module.DEFAULT_BASELINE_FORMULA;
        }

        // display loading mask
        if (isForDetailsDialog) {
            setLoading('#LatestBaselineDetails #WarningLevel');
            setLoading('#LatestBaselineDetails #CriticalLevel');
        } else {
            setLoading('#' + basename + 'Warning');
            setLoading('#' + basename + 'Critical');
        }

        var ORION = APMjs.assertGlobal('ORION');

        ORION.callWebService('/Orion/APM/Admin/Edit/Edit.asmx', 'CountBaselineThresholdsOnDemand',
            { componentId: componentId, columnSchemaId: columnSchemaId, thresholdName: thresholdName, warningFormula: warningFormula, criticalFormula: criticalFormula, oper: operator, availableInDays: availableInDays, statsFromTick: statsFromTick },
            function (thresholds) {
                if (isExtreme(thresholds.Warning)) {
                    thresholds.Warning = '';
                }
                if (isExtreme(thresholds.Critical)) {
                    thresholds.Critical = '';
                }
                if (thresholds.Severity === 0) {
                    useLatestBaselineThresholds(basename, thresholds.Warning, thresholds.Critical, true, isForDetailsDialog);
                } else if (thresholds.Severity === 1) {
                    $('#LatestBaselineThresholdsErrorMinor span.message').text(thresholds.Message);
                    $('#LatestBaselineThresholdsErrorMinor span.warningValue').text(thresholds.Warning);
                    $('#LatestBaselineThresholdsErrorMinor span.criticalValue').text(thresholds.Critical);
                    $('#LatestBaselineThresholdsErrorMinor').dialog({
                        width: 360,
                        modal: true,
                        close: function () {
                            if ($('#LatestBaselineThresholdsErrorMinor #setThresholds').attr('checked')) {
                                useLatestBaselineThresholds(basename, thresholds.Warning, thresholds.Critical, true, isForDetailsDialog);
                            } else {
                                useLatestBaselineThresholds(basename, warningLevel, criticalLevel, false, isForDetailsDialog);
                            }
                        }
                    });
                } else {
                    $('#LatestBaselineThresholdsErrorMajor span.message').html('@{R=APM.Strings;K=APMWEBJS_PS0_28;E=js}<br/><br/>' + thresholds.Message);
                    $('#LatestBaselineThresholdsErrorMajor').dialog({
                        width: 350,
                        modal: true,
                        close: function () {
                            useLatestBaselineThresholds(basename, warningLevel, criticalLevel, false, isForDetailsDialog);
                        }
                    });
                }
            },
            function () {
                $('#LatestBaselineThresholdsErrorMajor span.message').html('@{R=APM.Strings;K=APMWEBJS_PS0_28;E=js}');
                $('#LatestBaselineThresholdsErrorMajor').dialog({
                    width: 350,
                    modal: true,
                    close: function () {
                        useLatestBaselineThresholds(basename, warningLevel, criticalLevel, false, isForDetailsDialog);
                    }
                });
            });
    }

    function useBaselineFormulas(basename) {
        var isWarningFormula = isBaselineFormula($('#' + basename + 'Warning').val());
        var isCriticalFormula = isBaselineFormula($('#' + basename + 'Critical').val());
        return (isWarningFormula || isCriticalFormula);
    }

    function showThresholdMoreInfoDialog() {
        $('#BaselineThresholdsMoreInfo').dialog({
            width: 350,
            modal: true,
            zIndex: 9004
        });
    }

    function showLatestBaselineDetailsDialog(basename, id, columnSchemaId, statisticColumnName, thresholdName, thresholdDisplayName, operator, warnLevel, criticalLevel, baselineAppliedTicks, baselineFromTicks, baselineToTicks, baselineAvailableIn, baselineStatsFromTicks) {
        // adding threshold name
        $('#LatestBaselineDetails span.thresholdName').text('(' + thresholdDisplayName + ')');

        // set current threshold settings
        $('#LatestBaselineDetails #WarningLevel').val(warnLevel);
        $('#LatestBaselineDetails #CriticalLevel').val(criticalLevel);
        $('#LatestBaselineDetails select.thresholdDialogOperator').val(operator);
        $('#LatestBaselineDetails #CriticalOp').text($('#LatestBaselineDetails select.thresholdDialogOperator').find('option:selected').text());

        // set baseline data for current thresholds div
        setBaselineDataForCurrentThresholdsDiv(baselineAppliedTicks, baselineFromTicks, baselineToTicks, warnLevel, criticalLevel);

        // set latest baseline statistics and details -> load area chart
        setBaselineStatisticsAndDetails(id, statisticColumnName, thresholdName, thresholdDisplayName, columnSchemaId, operator);

        // set 'Use Recommended Thresholds' button
        $('#LatestBaselineDetails #UseRecommendedThresholds').unbind('click').bind('click', function () {
            module.getLatestBaselineThresholds(basename, id, columnSchemaId, thresholdName, baselineAvailableIn, baselineStatsFromTicks, true);
        });

        // open the dialog window
        openThresholdsDetailsDialog(basename, id, statisticColumnName, thresholdName, thresholdDisplayName, columnSchemaId);
    }

    function redrawPlotBandsInCharts() {
        module.redrawPlotBandsInChart($('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first(), 'x');
        module.redrawPlotBandsInChart($('#LatestBaselineDetails #Tabs #tabMetricOverTime').find('.hasChart').first(), 'y');
    }

    function redrawXAxis(title, unit) {
        var chart = $('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first().data();
        chart.xAxis[0].setTitle({ text: title });
        if (unit) {
            $('g.highcharts-axis-labels:first tspan').append(' ' + unit);
        }
    }

    function redrawPlotBandsInChart(parentDiv, axis) {
        var warningLevel = $('#tblStatistics div.warning').text();
        var criticalLevel = $('#tblStatistics div.critical').text();
        var operator = $('#LatestBaselineDetails select.thresholdDialogOperator').val();

        var chart = parentDiv.data();

        try {
            if (chart) {
                var axisChart = (axis === 'x') ? chart.xAxis[0] : chart.yAxis[0];

                if (axisChart) {
                    axisChart.removePlotBand('warning');
                    axisChart.removePlotBand('critical');

                    if (warningLevel && criticalLevel) {
                        if (operator === '0' || operator === '1') {
                            axisChart.addPlotBand({ id: 'warning', from: warningLevel, to: axisChart.max, color: '#FAFABA' });
                            axisChart.addPlotBand({ id: 'critical', from: criticalLevel, to: axisChart.max, color: '#EEC4C3' });
                        } else if (operator === '3' || operator === '4') {
                            axisChart.addPlotBand({ id: 'warning', from: axisChart.min, to: warningLevel, color: '#FAFABA' });
                            axisChart.addPlotBand({ id: 'critical', from: axisChart.min, to: criticalLevel, color: '#EEC4C3' });
                        }
                    }
                }
            }
        } catch (ignore) {
        }
    }

    function redrawPlotLineInCharts(valueToDisplay, clear) {
        var activeTab = $("#LatestBaselineDetails #Tabs").tabs('option', 'selected');

        // Area chart
        if (activeTab === 0) {
            module.redrawPlotLineInChart($('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first(), valueToDisplay, clear, 'x');
        }
        // MinMax chart
        if (activeTab === 1) {
            module.redrawPlotLineInChart($('#LatestBaselineDetails #Tabs #tabMetricOverTime').find('.hasChart').first(), valueToDisplay, clear, 'y');
        }
    }

    function redrawPlotLineInChart(parentDiv, valueToDisplay, clear, axis) {
        var chart = parentDiv.data();
        if (chart) {
            try {
                var axisChart = (axis === 'x') ? chart.xAxis[0] : chart.yAxis[0];
                if (axisChart) {
                    axisChart.removePlotLine('thresholdPreview');
                    if (!clear) {
                        axisChart.addPlotLine({ id: 'thresholdPreview', value: valueToDisplay, width: 1, color: '#000000', dashStyle: 'dash', zIndex: 10 });
                    }
                }
            } catch (ignore) {
            }
        }
    }

    function highlightRecommendedThresholds(operator) {
        $('#tblStatistics div.warning').removeClass('warning');
        $('#tblStatistics div.critical').removeClass('critical');
        $('#tblStatistics div.recommendedWarning').removeClass('recommendedWarning');
        $('#tblStatistics div.recommendedCritical').removeClass('recommendedCritical');

        var tblStatistics = $('#LatestBaselineDetails #tblStatistics tbody');

        if (operator === 0 || operator === 1) {
            tblStatistics.find('tr.all td:nth-child(10) div.statisticValue').addClass('warning').addClass('recommendedWarning');
            tblStatistics.find('tr.all td:nth-child(11) div.statisticValue').addClass('critical').addClass('recommendedCritical');
        } else if (operator === 3 || operator === 4) {
            tblStatistics.find('tr.all td:nth-child(5) div.statisticValue').addClass('critical').addClass('recommendedCritical');
            tblStatistics.find('tr.all td:nth-child(6) div.statisticValue').addClass('warning').addClass('recommendedWarning');
        }

        module.redrawPlotBandsInCharts();
    }


    // publish module

    module.DEFAULT_BASELINE_FORMULA = '${USE_BASELINE}';
    // handler for Get Latest Baseline Thresholds buttons
    module.getLatestBaselineThresholds = getLatestBaselineThresholds;
    // check if thresholds' inputs contain suported formulas
    module.useBaselineFormulas = useBaselineFormulas;
    module.showThresholdMoreInfoDialog = showThresholdMoreInfoDialog;
    module.showLatestBaselineDetailsDialog = showLatestBaselineDetailsDialog;
    module.redrawPlotBandsInCharts = redrawPlotBandsInCharts;
    module.redrawXAxis = redrawXAxis;
    module.redrawPlotBandsInChart = redrawPlotBandsInChart;
    module.redrawPlotLineInCharts = redrawPlotLineInCharts;
    module.redrawPlotLineInChart = redrawPlotLineInChart;
    module.highlightRecommendedThresholds = highlightRecommendedThresholds;
});
