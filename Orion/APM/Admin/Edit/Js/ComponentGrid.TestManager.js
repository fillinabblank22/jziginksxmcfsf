/*jslint browser: true, indent: 4, plusplus: true*/
/*global APMjs: false*/
/// <reference path="ComponentGrid.js" />

APMjs.initGlobal('SW.APM.EditApp.Grid.TestManager', function (TestManager, APMjs) {
    'use strict';

    var EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        EventManager = APMjs.assertGlobal('SW.APM.EventManager'),
        Ext = APMjs.assertExt(),
        ORION = APMjs.assertGlobal('ORION'),
        State = {
            RunningTestInfo: [],
            PollingForCompletedTests: false,
            PollingFrequency: 2000
        };


    function getSettingValue(settingPropertyName, item, itemTemplate) {
        var appSetting, haveTemplate, isOverriden, useAppValue;
        appSetting = item[settingPropertyName];
        haveTemplate = !!itemTemplate;
        isOverriden = (appSetting.SettingLevel === EditApp.SettingLevel.Instance);
        useAppValue = haveTemplate ? isOverriden : true;

        return EditApp.getValue(useAppValue ? appSetting : itemTemplate[settingPropertyName]);
    }

    function removeTestResults(testId) {
        var index = APMjs.linqSelect(State.RunningTestInfo, function (item, key) {
            return (item.TestId === testId) && key;
        })[0];

        if (APMjs.isNum(index)) {
            State.RunningTestInfo.splice(index, 1);
        }
    }

    function updateUi(testResult) {
        var model = EditApp.getModel(),
            component = model.getComponent(testResult.Id) || {},
            record;

        if (APMjs.isFn(component.saveFromForm)) {
            component.saveFromForm(model);
        }

        record = State.Store.getById(testResult.Id);

        testResult.testStatus = testResult.TestPassed ? 'success' : 'fail';
        testResult.status = testResult.Status;
        testResult.statusDescription = testResult.StatusDescription;
        testResult.messages = testResult.Messages;
        testResult.nodeId = record.json.TestResults.nodeId;
        testResult.nodeName = record.json.TestResults.nodeName;
        testResult.nodeStatus = record.json.TestResults.nodeStatus;
        /*jslint nomen: true*/
        delete testResult.__type;
        /*jslint nomen: false*/

        record.json.TestResults = testResult;
        record.set('TestResults', record.json.TestResults);
    }

    function pollForCompletedTests(callbackAfterFinish) {

        function onFinishedGetTestResults(results) {

            function submitTestResult(testResult) {
                var index = EditApp.ComponentsThatTestingNow.indexOf(testResult.Id);
                EditApp.ComponentsThatTestingNow.splice(index, 1);
                EventManager.fire('onComponentTesting');
                updateUi(testResult);
                EditApp.Grid.Handlers.initRetrieveUi(testResult);
                removeTestResults(testResult.TestId);
            }

            function rescheduleTimer() {
                if (State.RunningTestInfo.length) {
                    // still have outstanding tests...
                    APMjs.launch(function () {
                        pollForCompletedTests(callbackAfterFinish);
                    }, State.PollingFrequency);
                } else {
                    State.PollingForCompletedTests = false;
                    APMjs.launch(callbackAfterFinish);
                }
            }

            if (results && results.length) {
                EditApp.Utility.chunk(results, submitTestResult, rescheduleTimer, null, null);
            } else {
                rescheduleTimer();
            }
        }

        ORION.callWebService(
            '/Orion/APM/Admin/Edit/Edit.asmx',
            'GetTestsResults',
            {
                inprogressTests: State.RunningTestInfo
            },
            onFinishedGetTestResults,
            EditApp.Utility.handleError
        );
    }

    function startTestsCompleted(results, callbackAfterFinish) {
        APMjs.linqEach(results, function (result) {
            var allowPush = !APMjs.linqAny(State.RunningTestInfo, function (test) {
                return test.Id === result.Id;
            });
            if (allowPush) {
                State.RunningTestInfo.push(result);
            }
        });

        if (!State.PollingForCompletedTests) {
            State.PollingForCompletedTests = true;
            APMjs.launch(function () {
                pollForCompletedTests(callbackAfterFinish);
            }, State.PollingFrequency);
        }
    }


    function init(grid) {
        State.Store = grid.getStore();
    }

    /*jslint unparam: true*/
    function showErrorMessage(unusedSender, rec) {
        var wnd;

        wnd = new Ext.Window({
            renderTo: Ext.getBody(),
            title: '@{R=APM.Strings;K=APMWEBJS_ComponentTestResultsTitle;E=js}',
            layout: 'fit',
            modal: true,
            autoScroll: true,
            resizable: true,
            closable: true,
            closeAction: 'close',
            bodyStyle: 'padding-left:10px;padding-top:5px;',
            width: parseInt(Ext.getBody().getWidth() * 0.7, 10),
            height: parseInt(Ext.getBody().getHeight() * 0.5, 10),
            html: rec.get('TestResults').Messages.join('<br/>'),
            buttons: [{
                text: '@{R=APM.Strings;K=APMWEBJS_VB1_88;E=js}',
                handler: function () {
                    wnd.close();
                    wnd = null;
                }
            }]
        });

        wnd.show();
    }
    /*jslint unparam: false*/

    function beginTests(componentsToTest, model, callbackAfterFinish) {

        var template = model.Template;

        ORION.callWebService(
            '/Orion/APM/Admin/Edit/Edit.asmx',
            'StartTests',
            {
                editModels: componentsToTest,
                settings: {
                    Use64Bit: getSettingValue('Use64Bit', model, template),
                    ExecutePollingMethod: getSettingValue('ExecutePollingMethod', model, template),
                    Frequency: getSettingValue('Frequency', model, template),
                    NumberOfLogFilesToKeep: getSettingValue('NumberOfLogFilesToKeep', model, template),
                    CustomLogsEnabled: getSettingValue('DebugLoggingEnabled', model, template),
                    ModelId: model.Id,
                    IsTemplate: model.IsTemplate,
                    PollingTimeoutSeconds: getSettingValue('Timeout', model, template)
                }
            },
            function (results) {
                startTestsCompleted(results, callbackAfterFinish);
            },
            function (error) {
                EditApp.Utility.handleError(error);
                APMjs.launch(callbackAfterFinish);
            }
        );
    }


    // publish methods
    TestManager.init = init;
    TestManager.showErrorMessage = showErrorMessage;
    TestManager.beginTests = beginTests;

});
