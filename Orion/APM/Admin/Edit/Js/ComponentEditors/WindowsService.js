/*jslint indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.WindowsService', function (WindowsService, APMjs) {
    'use strict';

    var EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            settings: ['FetchingMethod', 'WinRmAuthenticationMechanism'],
            threshold: [
                { id: 'CPUThreshold', name: 'CPU' },
                { id: 'PMemThreshold', name: 'PMem' },
                { id: 'VMemThreshold', name: 'VMem' },
                { id: 'IOReadOPSThreshold', name: 'IOReadOperationsPerSec' },
                { id: 'IOWriteOPSThreshold', name: 'IOWriteOperationsPerSec' },
                { id: 'IOTotalOPSThreshold', name: 'IOTotalOperationsPerSec' }
            ]
        }),
        PropsDev = Editors.initProps({
            template: [{ id: 'Credential', name: 'CredentialSetId' }],
            settings: ['ServiceName', 'NotRunningStatusMode']
        }),
        PropsMin = Editors.initProps({ settings: ['MinRequestingVersion'] }),
        PropsMax = Editors.initProps({ settings: ['MaxRequestingVersion'] });


    function getProps(isDevMode, isMinVisible, isMaxVisible) {
        var props = Props.slice();
        if (isDevMode) {
            Array.prototype.push.apply(props, PropsDev);
        }
        if (isMinVisible) {
            Array.prototype.push.apply(props, PropsMin);
        }
        if (isMaxVisible) {
            Array.prototype.push.apply(props, PropsMax);
        }
        return props;
    }

    function onFetchingMethodChanged() {
        var jEl = $(this);
        Editors.changeWinRmRowVisibility(jEl, 2);
    }

    function onMultiFetchingMethodChanged() {
        var jEl = $(this);
        Editors.changeWinRmRowVisibility(jEl, 1);
    }

    function initSingle(cmpId, isDev, isMinVisible, isMaxVisible) {

        function onLoad(model) {
            Editors.chunkInitSingle({
                cmp: this,
                cmpId: cmpId,
                template: model.getComponentTemplate(this.TemplateId),
                props: getProps(isDev, isMinVisible, isMaxVisible),
                handlers: [
                    { id: 'FetchingMethod', handler: onFetchingMethodChanged, trigger: true }
                ],
                status: 'NotRunningStatusMode'
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: getProps(isDev, isMinVisible, isMaxVisible)
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function initMulti(cmpId, cmpIds, isDev, isMinVisible, isMaxVisible) {

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.WindowsService,
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(isDev, isMinVisible, isMaxVisible),
                handlers: [
                    { id: 'FetchingMethod', handler: onMultiFetchingMethodChanged, trigger: true }
                ],
                status: 'NotRunningStatusMode'
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(isDev, isMinVisible, isMaxVisible)
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds, isDev, isMinVisible, isMaxVisible) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        isDev = Editors.parseBool(isDev);
        isMinVisible = Editors.parseBool(isMinVisible);
        isMaxVisible = Editors.parseBool(isMaxVisible);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds, isDev, isMinVisible, isMaxVisible);
        } else {
            initSingle(cmpId, isDev, isMinVisible, isMaxVisible);
        }
    }

    // publish methods
    WindowsService.init = init;
});
