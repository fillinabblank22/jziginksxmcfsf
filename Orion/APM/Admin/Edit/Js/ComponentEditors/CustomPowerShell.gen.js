/// <reference path="../Edit.d.ts" />
var SW;
(function (SW) {
    var APM;
    (function (APM) {
        var EditApp;
        (function (EditApp) {
            var Editors;
            (function (Editors) {
                var CustomPowerShell;
                (function (CustomPowerShell) {
                    var Props = Editors.initProps({
                        template: [
                            { id: 'Desc', name: 'UserDescription' },
                            { id: 'Disabled', name: 'IsDisabled' },
                            { id: 'Notes', name: 'UserNote' }
                        ]
                    });
                    var PropsDev = Editors.initProps({
                        template: [
                            { id: 'Credential', name: 'CredentialSetId' }
                        ],
                        settings: [
                            { id: 'Script', name: 'ScriptBody' }
                        ]
                    });
                    var PropsDevTemplate = Editors.initProps({
                        settings: [
                            { name: 'VisibilityMode' }
                        ],
                        template: [
                            { id: 'CanBeDisabled', name: '_BB_CanBeDisabled' }
                        ]
                    });
                    function getProps(isDev, isTemplate, isBlackBoxEdit, thrKeys) {
                        var props = Props.slice(), propsThr;
                        if (isBlackBoxEdit) {
                            props.some(function (prop) {
                                if (prop.name === 'UserDescription') {
                                    prop.readonly = true;
                                    return true;
                                }
                            });
                        }
                        if (isDev) {
                            props = props.concat(PropsDev);
                            if (isTemplate) {
                                props = props.concat(PropsDevTemplate);
                            }
                        }
                        propsThr = Editors.initPropsThr(thrKeys);
                        props = props.concat(propsThr);
                        return props;
                    }
                    function initSingle(cmpId, isDev, isTemplate, isBlackBoxEdit, thrKeys) {
                        function onLoad(model) {
                            var cmp = this;
                            var tpl = model.getComponentTemplate(cmp.TemplateId);
                            Editors.chunkInitSingle({
                                cmp: cmp,
                                cmpId: cmpId,
                                template: tpl,
                                props: getProps(isDev, isTemplate, isBlackBoxEdit, thrKeys)
                            });
                        }
                        function onSave(model) {
                            Editors.chunkUpdateSingle({
                                cmp: this,
                                cmpId: cmpId,
                                isTemplate: !model.haveTemplate(),
                                props: getProps(isDev, isTemplate, isBlackBoxEdit, thrKeys)
                            });
                        }
                        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
                    }
                    function init(isMulti, cmpId, unused_cmpIds, isDev, isTemplate, isBlackBoxEdit, thrKeys) {
                        isMulti = Editors.parseBool(isMulti);
                        isDev = Editors.parseBool(isDev);
                        isTemplate = Editors.parseBool(isTemplate);
                        isBlackBoxEdit = Editors.parseBool(isBlackBoxEdit);
                        cmpId = Editors.parseId(cmpId);
                        thrKeys = Editors.parseList(thrKeys);
                        if (!isMulti) {
                            initSingle(cmpId, isDev, isTemplate, isBlackBoxEdit, thrKeys);
                        }
                    }
                    CustomPowerShell.init = init;
                })(CustomPowerShell = Editors.CustomPowerShell || (Editors.CustomPowerShell = {}));
            })(Editors = EditApp.Editors || (EditApp.Editors = {}));
        })(EditApp = APM.EditApp || (APM.EditApp = {}));
    })(APM = SW.APM || (SW.APM = {}));
})(SW || (SW = {}));
//# sourceMappingURL=CustomPowerShell.gen.js.map