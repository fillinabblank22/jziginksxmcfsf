/*jslint indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.Https', function (Https, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        SF = APMjs.format,
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' },
                { id: 'Credential', name: 'CredentialSetId' }
            ],
            settings: [
                'Url', 'HostHeader', 'HostRequest', 'ContentType', 'RequestBody',
                'FollowRedirect', 'UseProxy', 'ProxyAddress', 'UserAgent',
                'CertificateSubject', 'IgnoreCA', 'IgnoreCN', 'IgnoreCRL',
                'SearchString', 'FailIfFound', 'HeadRequest',
                'AcceptCompression', 'AuthMode'
            ],
            threshold: [
                { id: 'ResponseThreshold', name: 'Response' }
            ]
        }),
        PropsHideable = Editors.initProps({
            settings: ['PortNumber']
        });


    function onHostRequestChangedInternal(event, cmpId) {
        var jEl, jEditor, val;

        jEl = $(event.target);
        jEditor = jEl.parent();
        val = jEditor.is(':visible') ? jEl.val() : jEditor.next('.viewer').text();

        Editors.toggleSubEditor($(SF('#postPutCnt{0}', cmpId)), val === 'POST' || val === 'PUT');
        Editors.toggleSubEditor($(SF('#getHeadCnt{0}', cmpId)), val === 'GET' || val === 'HEAD');
    }

    function getProps(includeHideable) {
        var props = Props.slice();
        if (includeHideable) {
            Array.prototype.push.apply(props, PropsHideable);
        }
        return props;
    }


    function initSingle(cmpId, handleHideable) {

        function onHostRequestChanged(event) {
            onHostRequestChangedInternal(event, cmpId);
        }

        function onLoad(model) {
            Editors.chunkInitSingle({
                cmp: this,
                cmpId: cmpId,
                template: model.getComponentTemplate(this.TemplateId),
                props: getProps(handleHideable),
                handlers: [{ id: 'HostRequest', handler: onHostRequestChanged, trigger: true }]
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: getProps(handleHideable)
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function initMulti(cmpId, cmpIds, handleHideable) {

        function onHostRequestChanged(event) {
            onHostRequestChangedInternal(event, cmpId);
        }

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.Https,
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(handleHideable),
                handlers: [{ id: 'HostRequest', handler: onHostRequestChanged, trigger: true }]
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(handleHideable)
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds, handleHideable) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        handleHideable = Editors.parseBool(handleHideable);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds, handleHideable);
        } else {
            initSingle(cmpId, handleHideable);
        }
    }

    // publish methods
    Https.init = init;
});
