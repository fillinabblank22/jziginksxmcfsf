/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.SqlConnection', function (SqlConnection, APMjs) {
    'use strict';

    var EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            settings: [
                { id: 'MinFreq', name: 'MinimalFrequency' },
                { id: 'RowCount', name: 'IntParameter1' },
                'QueryTimeout'
            ]
        }),
        PropsDev = Editors.initProps({
            transform: [
                'ConvertValue'
            ],
            settings: [
                'PortNumber', 'CountAsDifference', 'InitialCatalog', 'SqlServerInstance', 'UseSharedConnection', 'WindowsAuthentication', 'PortType',
                { id: 'Credential', name: 'CredentialSetId' }
            ]
        }),
        PropsDevTemplate = Editors.initProps({
            settings: [
                'VisibilityMode'
            ]
        });


    function getProps(isDev, isTemplate, thrKeys) {
        var props = Props.slice(), propsThr;
        if (isDev) {
            props = props.concat(PropsDev);
            if (isTemplate) {
                props = props.concat(PropsDevTemplate);
            }
        }
        propsThr = Editors.initPropsThr(thrKeys);
        props = props.concat(propsThr);
        return props;
    }


    function initSingle(cmpId, isDev, isTemplate, thrKeys) {

        function onLoad(model) {
            var cmp = this,
                tpl = model.getComponentTemplate(cmp.TemplateId);

            Editors.chunkInitSingle({
                cmp: cmp,
                cmpId: cmpId,
                template: tpl,
                props: getProps(isDev, isTemplate, thrKeys)
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                thrKeys: thrKeys,
                isTemplate: !model.haveTemplate(),
                props: getProps(isDev, isTemplate, thrKeys)
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function initMulti(cmpId, cmpIds, isDev, isTemplate, thrKeys) {

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.SqlConnection,
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(isDev, isTemplate, thrKeys)
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(isDev, isTemplate, thrKeys)
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds, isDev, isTemplate, thrKeys) {
        isMulti = Editors.parseBool(isMulti);
        isDev = Editors.parseBool(isDev);
        isTemplate = Editors.parseBool(isTemplate);
        cmpId = Editors.parseId(cmpId);
        thrKeys = Editors.parseList(thrKeys);
        if (!isMulti) {
            initSingle(cmpId, isDev, isTemplate, thrKeys);
        } else {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds, isDev, isTemplate, thrKeys);
        }
    }


    // publish methods
    SqlConnection.init = init;
});
