/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.WindowsTaskScheduler', function (WindowsTaskScheduler, APMjs) {
    'use strict';

    var EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription', readonly: true },
                { id: 'Notes', name: 'UserNote' }
            ]
        });


    function initSingle(cmpId) {

        function onLoad(model) {
            var cmp = this,
                tpl = model.getComponentTemplate(cmp.TemplateId);

            Editors.chunkInitSingle({
                cmp: cmp,
                cmpId: cmpId,
                template: tpl,
                props: Props
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: Props
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function initMulti(cmpId, cmpIds) {

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.PerformanceCounter,
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: Props
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: Props
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds);
        } else {
            initSingle(cmpId);
        }
    }


    // publish methods
    WindowsTaskScheduler.init = init;
});
