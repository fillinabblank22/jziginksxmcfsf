/*jslint indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.WindowsPowerShell', function (WindowsPowerShell, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' },
                { id: 'Credential', name: 'CredentialSetId' }
            ],
            settings: ['ScriptArguments', 'ScriptBody', 'ExecutionMode', 'WrmUseSSL', 'WrmUrlPrefix', 'WrmPort', 'ImpersonateForLocalMode', 'CountAsDifference', 'StatusRollupType']
        }),
        TEXT = {
            RemoteHostValue: 'RemoteHost',
            RemoteHostText: 'Remote Host',
            ClassRequired: 'required',
            SelectMultiCheckBox: '[id*=MultiChekbox]',
            SelectSetCtr: '[apmsettctr=true]',
            AttrDisabled: 'disabled'
        };


    function getProps() {
        return Props.slice();
    }


    function disableExecutionMode(jMultiCheckBox, jSubEditor, jEditor) {
        var isChecked = jMultiCheckBox.is(':checked');
        if (isChecked) {
            jSubEditor
                .find(TEXT.SelectMultiCheckBox)
                .removeAttr(TEXT.AttrDisabled);
            jEditor
                .find('[id]')
                .removeAttr(TEXT.AttrDisabled);
        } else {
            jSubEditor
                .find(TEXT.SelectMultiCheckBox)
                .each(function () {
                    var jEl = $(this);
                    if (jEl.is(':checked')) {
                        jEl.attr('checked', false);
                        jEl.click();
                        jEl.attr('checked', false);
                    }
                });
            jSubEditor
                .find(TEXT.SelectMultiCheckBox)
                .attr(TEXT.AttrDisabled, TEXT.AttrDisabled);
            jEditor
                .find('[id]')
                .attr(TEXT.AttrDisabled, TEXT.AttrDisabled);
        }
        return true;
    }

    function metaShowSubEditorForExecutionMode(jEl, useMulti) {
        var jEditor, jCurrentBody, jSubEditor, show, jMultiCheckBox;

        jEditor = jEl.parent();
        jCurrentBody = jEl.parents('tbody').eq(0);
        jSubEditor = jCurrentBody.next('tbody.subEditor');

        show = jEditor.is(':visible') ? jEl.val() === TEXT.RemoteHostValue : jEditor.next('.viewer').text() === TEXT.RemoteHostText;

        if (show) {
            jSubEditor
                .show()
                .find(TEXT.SelectSetCtr)
                .addClass(TEXT.ClassRequired);
        } else {
            jSubEditor
                .hide()
                .find(TEXT.SelectSetCtr)
                .removeClass(TEXT.ClassRequired);
        }

        if (useMulti) {
            jMultiCheckBox = jEditor.parents('tr').eq(0).find(TEXT.SelectMultiCheckBox);
            jMultiCheckBox
                .bind('click', function () {
                    disableExecutionMode(jMultiCheckBox, jSubEditor, jEditor);
                });
        }
    }

    function initSingle(cmpId) {

        function showSubEditorForExecutionMode() {
            metaShowSubEditorForExecutionMode($(this), false);
        }

        function onLoad(model) {
            Editors.chunkInitSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                template: model.getComponentTemplate(this.TemplateId),
                props: getProps(),
                handlers: [{ id: 'ExecutionMode', handler: showSubEditorForExecutionMode, trigger: true }],
                colDynamic: 'btnEdit'
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                template: model.getComponentTemplate(this.TemplateId),
                props: getProps(),
                colDynamic: 'btnEdit'
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function initMulti(cmpId, cmpIds) {

        function showSubEditorForExecutionMode() {
            metaShowSubEditorForExecutionMode($(this), true);
        }

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.WindowsPowerShell,
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(),
                handlers: [{ id: 'ExecutionMode', handler: showSubEditorForExecutionMode, trigger: true }]
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps()
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds);
        } else {
            initSingle(cmpId);
        }
    }

    // publish methods
    WindowsPowerShell.init = init;
});
