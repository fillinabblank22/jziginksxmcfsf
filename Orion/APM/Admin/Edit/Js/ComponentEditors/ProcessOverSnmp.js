/*jslint indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.ProcessOverSnmp', function (Process, APMjs) {
    'use strict';

    var EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            settings: ['CommandLineFilter', 'ProcessName', 'NotRunningStatusMode'],
            threshold: [
                { id: 'CPUThreshold', name: 'CPU' },
                { id: 'PMemThreshold', name: 'PMem' },
                { id: 'VMemThreshold', name: 'VMem' },
                { id: 'IOReadOPSThreshold', name: 'IOReadOperationsPerSec' },
                { id: 'IOWriteOPSThreshold', name: 'IOWriteOperationsPerSec' },
                { id: 'IOTotalOPSThreshold', name: 'IOTotalOperationsPerSec' }
            ]
        });


    function initSingle(cmpId) {

        function onLoad(model) {
            Editors.chunkInitSingle({
                cmp: this,
                cmpId: cmpId,
                props: Props,
                template: model.getComponentTemplate(this.TemplateId),
                status: 'NotRunningStatusMode'
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                props: Props,
                isTemplate: !model.haveTemplate()
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }

    function initMulti(cmpId, cmpIds) {

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.ProcessOverSNMP,
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: Props,
                status: 'NotRunningStatusMode'
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: Props
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds);
        } else {
            initSingle(cmpId);
        }
    }


    // publish methods
    Process.init = init;
});
