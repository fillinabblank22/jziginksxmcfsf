/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.CustomWMI', function (CustomWMI, APMjs) {
    'use strict';

    var EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            settings: ['WinRmAuthenticationMechanism']
        }),
        PropsDev = Editors.initProps({
            template: [
                { id: 'Credential', name: 'CredentialSetId' }
            ],
            settings: [
                'ExecutorName', 'ExecutorAssemblyName', 'AdditionalData'
            ]
        }),
        PropsDevTemplate = Editors.initProps({
            settings: [
                'VisibilityMode'
            ]
        });


    function getProps(isDev, isTemplate, thrKeys) {
        var props = Props.slice(), propsThr;
        if (isDev) {
            props = props.concat(PropsDev);
            if (isTemplate) {
                props = props.concat(PropsDevTemplate);
            }
        }
        propsThr = Editors.initPropsThr(thrKeys);
        props = props.concat(propsThr);
        return props;
    }


    function initSingle(cmpId, isDev, isTemplate, thrKeys) {

        function onLoad(model) {
            var cmp = this,
                tpl = model.getComponentTemplate(cmp.TemplateId);

            Editors.chunkInitSingle({
                cmp: cmp,
                cmpId: cmpId,
                template: tpl,
                props: getProps(isDev, isTemplate, thrKeys)
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: getProps(isDev, isTemplate, thrKeys)
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds, isDev, isTemplate, thrKeys) {
        isMulti = Editors.parseBool(isMulti);
        isDev = Editors.parseBool(isDev);
        isTemplate = Editors.parseBool(isTemplate);
        cmpId = Editors.parseId(cmpId);
        thrKeys = Editors.parseList(thrKeys);
        if (!isMulti) {
            initSingle(cmpId, isDev, isTemplate, thrKeys);
        }
    }


    // publish methods
    CustomWMI.init = init;
});
