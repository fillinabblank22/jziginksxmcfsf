/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.Soap', function (Soap, APMjs, global) {
    'use strict';

    var $ = APMjs.assertQuery(),
        SF = APMjs.format,
        SW = APMjs.assertGlobal('SW'),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        isDemo = global.IsDemoMode,
        demoMessage = global.DemoModeMessage,
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' },
                { id: 'Credential', name: 'CredentialSetId' }
            ],
            settings: [
                'SoapLoadType', 'WsdlUrl', 'Binding',
                'MethodNamespace', 'SchemaNamespace', 'ServiceName', 'MethodName',
                'HostHeader', 'UserAgent', 'ContentType', 'UseProxy', 'ProxyAddress', 'AcceptCompression',
                'CertificateSubject', 'IgnoreCA', 'IgnoreCN', 'IgnoreCRL',
                'SearchString', 'FailIfFound', 'AuthMode'
            ],
            threshold: [
                { id: 'ResponseThreshold', name: 'Response' }
            ]
        }),
        PropsMulti = Editors.initProps({
            settings: [
                'UserAgent', 'ContentType',
                'UseProxy', 'ProxyAddress', 'AcceptCompression',
                'CertificateSubject', 'IgnoreCA', 'IgnoreCN', 'IgnoreCRL',
                'SearchString', 'FailIfFound', 'AuthMode', 'HostHeader'
            ],
            threshold: [
                { id: 'ResponseThreshold', name: 'Response' }
            ]
        }),
        PropsAuto = Editors.initProps({
            settings: [
                { id: 'ServiceUrlAuto', name: 'ServiceUrl' },
                { id: 'SoapVersionAuto', name: 'SoapVersion' },
                { id: 'SoapActionAuto', name: 'SoapAction' },
                { id: 'RequestBodyAuto', name: 'RequestBody' }
            ]
        }),
        PropsManual = Editors.initProps({
            settings: [
                'ServiceUrl', 'SoapVersion', 'SoapAction', 'RequestBody'
            ]
        }),
        UrlWsdlImporter = '/Orion/APM/Services/WsdlImporter.asmx',
        C = {
            valAuto: 'Automatically',
            textAuto: 'Load from WSDL',
            valManual: 'Manually',
            textManual: 'Manually enter SOAP XML Request',
            propType: '__type'
        };



    function makeNullValueString(sett) {
        if (sett && sett.Value === null) {
            sett.Value = '';
        }
    }

    function stringifyNullValues(opts, props) {
        APMjs.linqEach(props || opts.props, function (prop) {
            var data = Editors.getData(opts, prop);
            makeNullValueString(data[prop.name]);
        });
    }


    function initSingle(cmpId) {

        var mWsdl, mCmp, mParams, mTpl;

        function toggleEl(selector, show) {
            var jEl = $(SF('#wsdlAutoLoad{0}', cmpId)).find(selector);
            if (show) {
                jEl.show();
            } else {
                jEl.hide();
            }
            return jEl;
        }

        function getSettingValue(data, name, defVal) {
            return EditApp.getValue(data && data.Settings && data.Settings[name], defVal);
        }

        function isAutoFromModel() {
            return getSettingValue(mCmp, 'SoapLoadType') === C.valAuto;
        }

        function isAutoFromControl(cmpId, jEl) {
            jEl = jEl || Editors.getEl('SoapLoadType', cmpId);

            var isAutoControlVisible = jEl.parent().css('display') !== 'none';
            var autoControlVisibleCondition = function () { return jEl.val() === C.valAuto; }
            var autoControlHiddenCondition = function () { return jEl.parents('tr:first').find('.viewer').text() === C.textAuto; }
            var isAutoModeOn = isAutoControlVisible ? autoControlVisibleCondition() : autoControlHiddenCondition();
            return isAutoModeOn;
        }

        function toggleEditorsViewers() {
            var jEl, jAuto, jManual, jSel, isAuto;

            jEl = Editors.getEl('SoapLoadType', cmpId);
            jAuto = $(SF('#wsdlAutoLoad{0}', cmpId));
            jManual = $(SF('#wsdlManualLoad{0}', cmpId));

            isAuto = isAutoFromControl(cmpId, jEl);
            jSel = isAuto ? jAuto : jManual;

            if (jEl.is(':visible')) {
                jSel.find('.editor, .editor .grippie').show();
                jSel.find('.viewer, .viewer-cnt').hide();
            } else {
                jSel.find('.editor').hide();
                jSel.find('.viewer, .viewer .grippie, .viewer-cnt, .viewer-cnt .grippie').show();
            }
        }

        function callFail(method, message) {
            toggleEl('.loading-cnt-load, .loading-cnt-generate');

            switch (method) {
            case 'GetSchemaObjectStructure':
            case 'GetSchemaObjectStructureFromFile':
                toggleEl('.error-cnt-load', true).html(message);
                break;
            case 'GetSoapEnvelope':
                toggleEl('.error-cnt-generate', true).html(message);
                break;
            }
        }

        function callWsdl(method, args, success) {

            function onSuccess(data) {
                toggleEl('.loading-cnt-load, .loading-cnt-generate');
                if (success) {
                    success(data);
                }
            }

            function onFail(message) {
                callFail(method, message);
            }

            toggleEl('.error-cnt-load, .error-cnt-generate');

            switch (method) {
            case 'GetSchemaObjectStructure':
            case 'GetSchemaObjectStructureFromFile':
                toggleEl('.loading-cnt-load', true);
                break;
            case 'GetSoapEnvelope':
                toggleEl('.loading-cnt-generate', true);
                break;
            }

            SW.Core.Services.callWebService(UrlWsdlImporter, method, args, onSuccess, onFail);
        }

        function clearWsdl() {
            mWsdl = null;
            mCmp.Settings.ParsedWsdl.Value = '';
            mCmp.Settings.ParsedWsdl.SettingLevel = 2;

            mParams.clear();

            APMjs.linqEach(['Binding', 'ServiceName', 'MethodName'], function (name) {
                Editors.getEl(name, cmpId).empty();
            });
            APMjs.linqEach(['ServiceUrlAuto', 'MethodNamespace', 'SchemaNamespace', 'SoapActionAuto', 'RequestBodyAuto'], function (name) {
                Editors.getEl(name, cmpId).val('');
            });
            toggleEl('.wsdl-info');
        }

        function getContract(wsdl, index) {
            if (!wsdl && !wsdl.Contracts) {
                return;
            }
            if (APMjs.isNum(index)) {
                wsdl.SelContract = index;
            }
            return wsdl.Contracts[wsdl.SelContract || 0];
        }

        function getOperation(wsdl, index) {
            if (!wsdl || !wsdl.Contracts) {
                return;
            }
            if (APMjs.isNum(index)) {
                wsdl.SelOperation = index;
            }
            var contract = getContract(wsdl);
            return contract && contract.Operations[wsdl.SelOperation || 0];
        }

        function bindSelect(elName, items, indexName, propName) {
            var jEl = Editors.getEl(elName, cmpId).empty();
            APMjs.linqEach(items, function (item, i) {
                $('<option/>')
                    .attr(indexName, i)
                    .val(item[propName])
                    .text(item[propName])
                    .appendTo(jEl);
            });
            return jEl;
        }

        function onLoadWsdlLoad(data) {
            mWsdl = data;
            mCmp.Settings.ParsedWsdl.Value = JSON.stringify(data);
            mCmp.Settings.ParsedWsdl.SettingLevel = EditApp.SettingLevel.Instance;

            toggleEl('.wsdl-info', true);
            toggleEl('.loading-cnt-load, .loading-cnt-generate, .error-cnt-load, .error-cnt-generate');

            var contract = getContract(mWsdl);
            Editors.getEl('ServiceUrlAuto', cmpId).val(contract.ServiceAddress);

            bindSelect('Binding', mWsdl.Contracts, 'soap-binding-index', 'BindingName').change();
        }

        function onLoadWsdlClick() {
            if (isDemo()) {
                return demoMessage();
            }
            clearWsdl();
            callWsdl('GetSchemaObjectStructure', { schemaUrl: Editors.getEl('WsdlUrl', cmpId).val() }, onLoadWsdlLoad);
            return false;
        }

        function getParsedWsdl(json) {
            var isOverriden = EditApp.getEditor(Editors.getEl('SoapLoadType', cmpId)).is(':visible');

            json = json || getSettingValue((isOverriden ? mCmp : mTpl), 'ParsedWsdl');

            if (!(json && json.length)) {
                toggleEl('.wsdl-info', false);
                return;
            }

            return JSON.parse(json);
        }

        function onLoadTypeChanged() {
            var jEl, jAuto, jManual, isAuto;

            jEl = Editors.getEl('SoapLoadType', cmpId);
            jAuto = $(SF('#wsdlAutoLoad{0}', cmpId));
            jManual = $(SF('#wsdlManualLoad{0}', cmpId));

            isAuto = isAutoFromControl(cmpId, jEl);

            Editors.toggleSubEditor(jAuto, isAuto);
            Editors.toggleSubEditor(jManual, !isAuto);

            if (isAuto) {
                mWsdl = getParsedWsdl();
                mParams.clear();
                if (mWsdl) {
                    mParams.renderData(getOperation(mWsdl));
                }
            }

            toggleEl('.wsdl-info', mWsdl && isAuto);

            toggleEditorsViewers();
        }

        function onBindingChanged() {
            var index, contract;
            index = $(this).find(':selected').attr('soap-binding-index');
            contract = getContract(mWsdl, parseInt(index, 10));
            bindSelect('ServiceName', [{ Name: contract.Name }], 'soap-service-name-index', 'Name').change();
        }

        function onServiceNameChanged() {
            var contract = getContract(mWsdl);
            bindSelect('MethodName', contract.Operations, 'soap-operation-index', 'Name').change();
        }

        function onGenerateWsdlLoad(data) {
            Editors.getEl('RequestBodyAuto', cmpId).val(data);
        }

        function onGenerateWsdl() {
            var contract, operation, params;

            contract = getContract(mWsdl);
            operation = getOperation(mWsdl);

            params = {
                serviceInfo: mWsdl,
                contractName: contract.Name,
                operationName: operation.Name,
                soapVersion: contract.SoapVersion,
                parameters: mParams.getParams(operation)
            };

            APMjs.linqEach(params.parameters, function (parameter, i) {
                operation.Parameters[i].Value = EditApp.getValue(parameter);
            });

            callWsdl('GetSoapEnvelope', params, onGenerateWsdlLoad);
            return false;
        }

        function onMethodNameChanged() {
            var index, contract, operation;

            index = $(this).find(':selected').attr('soap-operation-index');

            contract = getContract(mWsdl);
            operation = getOperation(mWsdl, parseInt(index, 10));

            Editors.getEl('SchemaNamespace', cmpId).val(mWsdl.SchemaNamespace);
            Editors.getEl('SoapVersionAuto', cmpId).val(contract.SoapVersion);
            Editors.getEl('MethodNamespace', cmpId).val(operation.NameSpace);
            Editors.getEl('SoapActionAuto', cmpId).val(operation.SoapAction);
            Editors.getEl('RequestBodyAuto', cmpId).val('');

            mParams.clear();
            mParams.renderData(operation);

            toggleEditorsViewers();

            onGenerateWsdl();
        }

        function bindWsdl(json) {
            var contract, operation;

            mWsdl = getParsedWsdl(json);

            if (!mWsdl) {
                return;
            }

            contract = getContract(mWsdl);
            operation = getOperation(mWsdl);

            Editors.getEl('ServiceUrlAuto', cmpId).val(contract.ServiceAddress);

            bindSelect('Binding', mWsdl.Contracts, 'soap-binding-index', 'BindingName');
            bindSelect('ServiceName', [{ Name: contract.Name }], 'soap-service-name-index', 'Name');
            bindSelect('MethodName', contract.Operations, 'soap-operation-index', 'Name');

            mParams.clear();
            mParams.renderData(operation);
        }

        function saveWsdl() {
            var operation;

            if (!mWsdl) {
                return;
            }

            delete mWsdl[C.propType];
            APMjs.linqEach(mWsdl.Contracts, function (contract) {
                delete contract[C.propType];
                APMjs.linqEach(contract.Operations, function (operation) {
                    delete operation[C.propType];
                    APMjs.linqEach(operation.Parameters, function (parameter) {
                        delete parameter[C.propType];
                        parameter.Value = null;
                    });
                });
            });

            operation = getOperation(mWsdl);
            APMjs.linqEach(mParams.getParams(), function (parameter, i) {
                operation.Parameters[i].Value = EditApp.getValue(parameter);
            });

            mCmp.Settings.ParsedWsdl.Value = JSON.stringify(mWsdl);
            mCmp.Settings.ParsedWsdl.SettingLevel = EditApp.SettingLevel.Instance;
        }

        function getProps(isAuto) {
            var props = Props.slice();
            if (isAuto) {
                Array.prototype.push.apply(props, PropsAuto);
            } else {
                Array.prototype.push.apply(props, PropsManual);
            }
            return props;
        }

        function onLoad(model) {
            mCmp = this;
            mTpl = model.getComponentTemplate(mCmp.TemplateId);

            Editors.chunkInitSingle({
                cmp: mCmp,
                cmpId: cmpId,
                template: mTpl,
                props: getProps(isAutoFromModel()),
                handlers: [
                    { id: 'SoapLoadType', handler: onLoadTypeChanged },
                    { id: 'WsdlUrl', idPost: 'Button', event: 'click', handler: onLoadWsdlClick },
                    { id: 'Binding', handler: onBindingChanged },
                    { id: 'ServiceName', handler: onServiceNameChanged },
                    { id: 'MethodName', handler: onMethodNameChanged }
                ],
                postInit: function () {
                    bindWsdl();
                    // Function bindWsdl creates options to DOM for following three DropDownList so we have to set values again
                    SW.APM.EditApp.initUiState($('#cmpBinding' + cmpId), 'Binding', mCmp.Settings, mTpl && mTpl.Settings);
                    SW.APM.EditApp.initUiState($('#cmpServiceName' + cmpId), 'ServiceName', mCmp.Settings, mTpl && mTpl.Settings);
                    SW.APM.EditApp.initUiState($('#cmpMethodName' + cmpId), 'MethodName', mCmp.Settings, mTpl && mTpl.Settings);
                    onLoadTypeChanged();
                }
            });
        }

        function onSave(model) {
            var isAuto, opts;

            mCmp = this;

            isAuto = isAutoFromControl(cmpId);

            opts = {
                cmp: mCmp,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: getProps(isAuto)
            };

            Editors.chunkUpdateSingle(opts);
            stringifyNullValues(opts);

            if (isAuto) {
                saveWsdl();
            } else {
                clearWsdl();
            }
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);

        mParams = new Soap.SoapParameters(cmpId, onGenerateWsdl);
    }

    function initMulti(cmpId, cmpIds) {

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.Soap,
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: PropsMulti
            });
        }

        function onSave(app) {
            var opts = {
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: PropsMulti
            };

            Editors.chunkUpdateMulti(opts);
            stringifyNullValues(opts);
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }

    function init(isMulti, cmpId, cmpIds) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds);
        } else {
            initSingle(cmpId);
        }
    }


    // publish methods
    Soap.init = init;
});
