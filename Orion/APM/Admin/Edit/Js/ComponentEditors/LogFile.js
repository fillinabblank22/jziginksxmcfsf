/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.EditApp.Editors.LogFile', function (LogFile, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            settings: [
                'DirectoryPath',
                'FilePattern',
                {
                    id: 'FilePatternUseRegex',
                    name: 'FilePatternContainsRegEx'
                },
                'SearchPattern',
                {
                    id: 'SearchPatternUseRegex',
                    name: 'SearchPatternContainsRegEx'
                },
                'MatchOnType',
                'StatusType'
            ]
        }),
        Res = {
            clsHintToggle: 'apmHintToggle',
            clsActive: 'active',
            textToggleHintsShow: 'show hints',
            textToggleHintsHide: 'hide hints'
        };


    function updateRadioState(state) {
        var jSelected, jFileEditor, jFileRow, isDirectory;

        jSelected = Editors.getInput(state.cmpId, Props.map.MatchOnType).find(':checked');
        jFileEditor = Editors.getInput(state.cmpId, Props.map.FilePattern);
        jFileRow = jFileEditor.closest('tr').next('tr.helpContainer').andSelf();

        isDirectory = (jSelected.val() === 'DirectoryPath');

        jFileEditor.attr('apmsettctr', !isDirectory);
        APMjs.setVisibility(jFileRow, !isDirectory);
    }

    function updateHelpByRegexState(state, opts) {
        var jSelected, jHelp, isRegex;

        jSelected = Editors.getInput(state.cmpId, Props.map[opts.regex]);
        jHelp = jSelected.closest('tr').next('tr.helpContainer');

        isRegex = jSelected.is(':visible') ? jSelected.is(':checked') : state.templateValue[opts.regex];

        APMjs.setVisibility(jHelp, !isRegex);
    }

    // initialize "joined fields" - regex field override button is hidden and linked to pattern field
    function initJoinedFieldsMeta(state, opts) {
        var jPattern, jRegex, jLinkPattern, jLinkRegex;

        function updateSyncedState() {
            var show = jPattern.is(':visible');
            EditApp.Utility.toggleOverrideUi(jRegex, show);
            if (APMjs.isFn(opts.callback)) {
                opts.callback(state, opts);
            }
        }

        jPattern = Editors.getInput(state.cmpId, Props.map[opts.pattern]);
        jRegex = Editors.getInput(state.cmpId, Props.map[opts.regex]);

        jLinkPattern = EditApp.getOverrideLink(jPattern);
        jLinkPattern.click(updateSyncedState);

        jLinkRegex = EditApp.getOverrideLink(jRegex);
        jLinkRegex.hide();

        updateSyncedState();
    }

    function initJoinedFields(state, joinedFields) {
        initJoinedFieldsMeta(state, joinedFields.file);
        initJoinedFieldsMeta(state, joinedFields.search);
    }

    function getProps(state, joinedFields) {
        var props = Editors.cloneProps(Props);
        state.templateValue = {};
        // save template value while initializing fields
        props.map[joinedFields.file.regex].callback = function (name, unused_item, itemTemplate) {
            state.templateValue[joinedFields.file.regex] = EditApp.parseValue(itemTemplate && itemTemplate[name], false);
        };
        props.map[joinedFields.search.regex].callback = function (name, unused_item, itemTemplate) {
            state.templateValue[joinedFields.search.regex] = EditApp.parseValue(itemTemplate && itemTemplate[name], false);
        };
        return props;
    }


    function convertToSelectable(jContainer, jDivs, jRest) {
        var jSelect, jContent, divs = [];

        function updateHintVisibility() {
            var value = jSelect.find('option:selected').attr('value');
            value = parseInt(value, 10);
            APMjs.linqEach(divs, function (div, index) {
                APMjs.setVisibility(div, index === value);
            });
        }

        jSelect = $('<select>')
            .appendTo(jContainer);

        jContent = $('<div>')
            .addClass('content')
            .appendTo(jContainer);

        APMjs.linqEach(jDivs, function (div, index) {
            var jSection = $(div), title, content;

            title = jSection.attr('title');
            content = jSection.html();

            $('<option>')
                .appendTo(jSelect)
                .attr('value', index)
                .text(title);

            divs[index] = $('<div>')
                .appendTo(jContent)
                .html(content);

            jSection.remove();
        });

        jRest.insertAfter(jContent);

        jSelect.change(updateHintVisibility);

        updateHintVisibility();
    }

    function createTipsContainer(jHint) {
        var jContainer, jSubSelect;

        jContainer = $('<div>')
            .addClass('apmPatternHint')
            .attr('title', jHint.attr('title'));

        jSubSelect = $('<div>')
            .addClass('hints')
            .appendTo(jContainer);

        convertToSelectable(jSubSelect, jHint.children('.section'), jHint.children(':not(.section)'));

        APMjs.setVisibility(jContainer, false);

        return jContainer;
    }

    function createTipsToggle(callback) {
        return $('<a>')
            .addClass(Res.clsHintToggle)
            .text(Res.textToggleHintsShow)
            .click(callback);
    }

    function positionDialog(jTips, jTarget) {
        jTips.dialog('widget')
            .addClass('apmPatternHintDialog')
            .position({
                my: 'right top',
                at: 'left top',
                of: jTarget
            });
    }

    function initPatternHint(jInput, jHint, jTarget) {
        var jTips, jRow, jToggle, shown;

        function updateToggle() {
            if (shown) {
                jToggle.text(Res.textToggleHintsHide);
                jToggle.addClass(Res.clsActive);
            } else {
                jToggle.text(Res.textToggleHintsShow);
                jToggle.removeClass(Res.clsActive);
            }
        }

        function closeDialog() {
            if (shown) {
                shown = false;
            }
            updateToggle();
        }

        function toggleHints() {
            shown = !shown;
            jTips.dialog(shown ? 'open' : 'close');
            positionDialog(jTips, jTarget);
            updateToggle();
        }

        jRow = jHint.closest('tr');

        jHint.detach();
        jRow.remove();

        jTips = createTipsContainer(jHint);
        jToggle = createTipsToggle(toggleHints);

        jTips.dialog({
            width: 350,
            modal: false,
            zIndex: 900,
            close: closeDialog
        });

        jTips.dialog('close');

        jToggle.insertAfter(jInput);
    }

    function initPatternHint_FilePattern(cmpId) {
        var jInput, jParent;

        jInput = Editors.getInput(cmpId, Props.map.FilePattern);
        jParent = jInput.closest('tbody');

        initPatternHint(
            jInput,
            jParent.find('.apmPatternHint_FilePattern'),
            jParent.find('tr td:nth-child(3)')
        );
    }


    function getLoadOpts(opts) {
        var joinedFields = {
            file: { pattern: 'FilePattern', regex: 'FilePatternUseRegex', callback: updateHelpByRegexState },
            search: { pattern: 'SearchPattern', regex: 'SearchPatternUseRegex', callback: updateHelpByRegexState }
        };
        return {
            cmp: opts.cmp,
            cmpId: opts.cmpId,
            cmpIds: opts.cmpIds,
            template: opts.tpl,
            handlers: [
                {
                    id: 'MatchOnType',
                    handler: function () {
                        updateRadioState(opts.state);
                    },
                    trigger: true
                },
                {
                    id: 'FilePatternUseRegex',
                    handler: function () {
                        updateHelpByRegexState(opts.state, joinedFields.file);
                    },
                    trigger: true
                },
                {
                    id: 'SearchPatternUseRegex',
                    handler: function () {
                        updateHelpByRegexState(opts.state, joinedFields.search);
                    },
                    trigger: true
                }
            ],
            postInit: function () {
                initJoinedFields(opts.state, joinedFields);
                initPatternHint_FilePattern(opts.cmpId);
            },
            props: getProps(opts.state, joinedFields)
        };
    }


    function initSingle(cmpId) {

        var state = {
            cmpId: cmpId
        };

        function onLoad(model) {
            Editors.chunkInitSingle(getLoadOpts({
                cmp: this,
                cmpId: cmpId,
                tpl: model.getComponentTemplate(this.TemplateId),
                state: state
            }));
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: Props
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function initMulti(cmpId, cmpIds) {

        var state = {
            cmpId: cmpId
        };

        function onLoad() {
            Editors.chunkInitMulti(getLoadOpts({
                cmp: EditApp.ComponentDefinitions.LogFile,
                cmpId: cmpId,
                cmpIds: cmpIds,
                state: state
            }));
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: Props
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds);
        } else {
            initSingle(cmpId);
        }
    }


    // publish methods
    LogFile.init = init;
});
