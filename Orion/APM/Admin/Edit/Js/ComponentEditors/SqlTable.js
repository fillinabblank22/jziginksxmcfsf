/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.SqlTable', function (SqlTable, APMjs) {
    'use strict';

    var EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            settings: [
                { id: 'MinFreq', name: 'MinimalFrequency' },
                { id: 'RowCount', name: 'RowCount' },
                { id: 'IntParameter1', name: 'IntParameter1' }
            ]
        }),
        PropsNotAppItem = Editors.initProps({             
            settings: [                 
                'QueryTimeout'
            ]
        }),
        PropsDev = Editors.initProps({
            transform: [
                'ConvertValue'
            ],
            settings: [
                'PortType', 'CountAsDifference', 'InitialCatalog', 'PortNumber', 'SqlQuery', 'SqlServerInstance', 'UseSharedConnection', 'WindowsAuthentication',
                { id: 'Credential', name: 'CredentialSetId' }
            ]
        }),
        PropsDevTemplate = Editors.initProps({
            settings: [
                'VisibilityMode'
            ],
            template: [
                { id: 'CanBeDisabled', name: '_BB_CanBeDisabled' },
                { id: 'CanHaveMinFrequency', name: '_BB_CanHaveMinFrequency' }
            ]
        });


    function getProps(isAppItem, isDev, isTemplate, thrKeys) {
        var props = Props.slice(), propsThr;
        if (!isAppItem) {
            props = props.concat(PropsNotAppItem);            
        }
        if (isDev) {
            props = props.concat(PropsDev);
            if (isTemplate) {
                props = props.concat(PropsDevTemplate);
            }
        }
        propsThr = Editors.initPropsThr(thrKeys);
        props = props.concat(propsThr);
        return props;
    }


    function initSingle(cmpId, isAppItem, isDev, isTemplate, thrKeys) {

        function onLoad(model) {
            var cmp = this,
                tpl = model.getComponentTemplate(cmp.TemplateId);

            Editors.chunkInitSingle({
                cmp: cmp,
                cmpId: cmpId,
                template: tpl,
                props: getProps(isAppItem, isDev, isTemplate, thrKeys)
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: getProps(isAppItem, isDev, isTemplate, thrKeys)
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds, isAppItem, isDev, isTemplate, thrKeys) {
        isMulti = Editors.parseBool(isMulti);
        isAppItem = Editors.parseBool(isAppItem);
        isDev = Editors.parseBool(isDev);
        isTemplate = Editors.parseBool(isTemplate);
        cmpId = Editors.parseId(cmpId);
        thrKeys = Editors.parseList(thrKeys);
        if (!isMulti) {
            initSingle(cmpId, isAppItem, isDev, isTemplate, thrKeys);
        }
    }


    // publish methods
    SqlTable.init = init;
});
