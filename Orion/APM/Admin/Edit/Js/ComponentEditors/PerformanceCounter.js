/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.PerformanceCounter', function (PerformanceCounter, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        Props = Editors.initProps({
            template: [
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            threshold: [
                { id: 'Threshold', name: 'StatisticData' }
            ]
        }),
        PropsEditable = Editors.initProps({
            template: [
                { id: 'Credential', name: 'CredentialSetId' }
            ],
            settings: [
                'CountAsDifference', 'Counter', 'Instance', 'Category', 'PreferredPollingMethod', 'WinRmAuthenticationMechanism'
            ],
            transform: [
                'ConvertValue'
            ]
        }),
        PropsBBTemplate = Editors.initProps({
            settings: [
                'ComponentCategory'
            ]
        });


    function getProps(isEditable, isBBTemplate) {
        var props = Props.slice();
        if (isEditable) {
            props = props.concat(PropsEditable);
            if (isBBTemplate) {
                props = props.concat(PropsBBTemplate);
            }
        }
        return props;
    }

    function onFetchingMethodChanged() {
        var jEl = $(this), jEditor = jEl.parent(), jHelp = jEditor.parents('tr.dropdownEditorContainer').next('tr.helpContainer').find('.warning-message');
        if (jEditor.is(':visible') && jEditor.parents(':not(:visible)').length === 0) {
            if (jEl.val() === 'Wmi') {
                jHelp.show();
            } else {
                jHelp.hide();
            }
        }
    }

    function initSingle(cmpId, isEditable, isBBTemplate) {

        function onLoad(model) {
            var cmp = this,
                tpl = model.getComponentTemplate(cmp.TemplateId);

            Editors.chunkInitSingle({
                cmp: cmp,
                cmpId: cmpId,
                template: tpl,
                handlers: [
                    { id: 'PreferredPollingMethod', handler: onFetchingMethodChanged, trigger: true }
                ],
                props: getProps(isEditable, isBBTemplate)
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: getProps(isEditable, isBBTemplate)
            });
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
    }


    function initMulti(cmpId, cmpIds, isEditable, isBBTemplate) {

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.PerformanceCounter,
                cmpId: cmpId,
                cmpIds: cmpIds,
                handlers: [
                    { id: 'PreferredPollingMethod', handler: onFetchingMethodChanged, trigger: true }
                ],
                props: getProps(isEditable, isBBTemplate)
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps(isEditable, isBBTemplate)
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds, isEditable, isBBTemplate) {
        isMulti = Editors.parseBool(isMulti);
        isEditable = Editors.parseBool(isEditable);
        isBBTemplate = Editors.parseBool(isBBTemplate);
        cmpId = Editors.parseId(cmpId);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds, isEditable, isBBTemplate);
        } else {
            initSingle(cmpId, isEditable, isBBTemplate);
        }
    }


    // publish methods
    PerformanceCounter.init = init;
});
