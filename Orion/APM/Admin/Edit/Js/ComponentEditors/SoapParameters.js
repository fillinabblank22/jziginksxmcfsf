/*jslint indent: 4*/
/*global APMjs: false*/
APMjs.withGlobal('SW.APM.EditApp.Editors.Soap', function (Soap, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        SF = APMjs.format,
        SW = APMjs.assertGlobal('SW');


    function contains(text, needle) {
        return APMjs.isStr(text) && text.indexOf(needle) > 0;
    }

    function getValue() {
        if (this.IsComplex) {
            return this.Value || '';
        }
        var value = this.Value, type = this.JsValueType;
        if (!APMjs.isSet(value) || value === '') {
            if (contains(type, '.String') || contains(type, '.Char')) {
                value = '';
            } else if (contains(type, '.Int') || contains(type, '.Byte')) {
                value = '0';
            } else if (contains(type, '.Double') || contains(type, '.Single') || contains(type, '.Decimal')) {
                value = '0.0';
            } else if (contains(type, '.Boolean')) {
                value = 'false';
            }
        }
        return value;
    }


    function SoapParameters(cmpId, onGeneratedWsdlHandler) {
        this.cmpId = cmpId;
        this.generateWsdlHandler = onGeneratedWsdlHandler;
        this.operation = null;
    }


    function clear() {
        this.operation = null;
        Editors.getEl('Arguments', this.cmpId, 'SoapArgsContainer')
            .find('tbody')
            .empty();
    }

    function renderData(operation) {
        var that = this, htmldata = [], html, jEl;

        this.operation = operation || this.operation;

        APMjs.linqEach(this.operation.Parameters, function (parameter) {
            var template, result;
            template = parameter.IsComplex ? $('script#edit-parameter-complex').html() : $('script#edit-parameter-simple').html();
            result = SF(template, that.cmpId, parameter.Name, parameter.Position, getValue.call(parameter), parameter.JsValueType);
            htmldata.push(result);
        });

        html = htmldata.join('');

        jEl = Editors.getEl('Arguments', that.cmpId, 'SoapArgsContainer');
        jEl.find('tbody').html(html);
        jEl.find('div.editor input, div.editor textarea').focusout(function () {
            if (APMjs.isFn(that.generateWsdlHandler)) {
                that.generateWsdlHandler();
            }
        });

        SW.APM.TextAreaResizer.makeTextAreasResizable(jEl);
    }

    function getParams(operation) {
        var params = [], cmpId = this.cmpId;

        this.operation = operation || this.operation;

        APMjs.linqEach(this && this.operation && this.operation.Parameters, function (parameter) {
            var obj = JSON.parse(JSON.stringify(parameter));
            obj.Value = $(SF('#soap-arg-{0}-{1}-{2}', cmpId, obj.Name, obj.Position)).val();
            params.push(obj);
        });
        return params;
    }

    function getParamsCount() {
        return (this.operation && this.operation.Parameters && this.operation.Parameters.length) || 0;
    }

    function init(cmpId, generateWsdlHandler) {
        this.cmpId = cmpId;
        this.generateWsdlHandler = generateWsdlHandler;
    }


    SoapParameters.prototype.clear = clear;
    SoapParameters.prototype.renderData = renderData;
    SoapParameters.prototype.getParamsCount = getParamsCount;
    SoapParameters.prototype.getParams = getParams;
    SoapParameters.prototype.init = init;

    Soap.SoapParameters = SoapParameters;
});
