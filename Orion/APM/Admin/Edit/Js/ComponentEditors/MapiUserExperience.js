/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.Editors.MapiUserExperience', function (MapiUserExperience, APMjs) {
    'use strict';

    var EventManager = APMjs.assertGlobal('SW.APM.EventManager'),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Editors = APMjs.assertGlobal('SW.APM.EditApp.Editors'),
        SF = APMjs.format,
        Props = Editors.initProps({
            template: [
                { id: 'Credential', name: 'CredentialSetId' },
                { id: 'Desc', name: 'UserDescription' },
                { id: 'Disabled', name: 'IsDisabled' },
                { id: 'Notes', name: 'UserNote' }
            ],
            settings: [
                { id: 'SendEmailFrom', name: 'EmailFrom' },
                { id: 'SendEmailTo', name: 'EmailTo' },
                { id: 'SMTPServer', name: 'SmtpSvr' },
                { id: 'SMTPPort', name: 'SmtpPortNumber' },
                { id: 'UseCredentialsForSMTP', name: 'SmtpAuth' },
                { id: 'SMTPEncryption', name: 'SmtpSsl' },
                'MapiProfileName',
                { id: 'CredentialSMTP', name: '_CredentialSetId_SMTP', credential: 'smtp' }
            ],
            threshold: [
                { id: 'ResponseThreshold', name: 'Response' }
            ]
        });


    function getProps() {
        var props = Editors.cloneProps(Props);
        return props;
    }


    function updateSmtpCredentialsUsing(cmpId) {
        var jUseCred, jCred, use;

        jUseCred = Editors.getInput(cmpId, Props.map.UseCredentialsForSMTP);
        jCred = Editors.getInput(cmpId, Props.map.CredentialSMTP);
        use = jUseCred.is(':checked');

        jCred.attr('apm-me-was-changed', use);
        APMjs.setVisibility(EditApp.getEditRow(jCred), use);
    }


    function initSingle(cmpId) {

        var state = {};

        function processSmtpCredentialsVisibilty(cmp) {
            var jCred = Editors.getInput(cmpId, Props.map.CredentialSMTP);
            APMjs.setVisibility(EditApp.getEditRow(jCred), String(cmp.Settings.SmtpAuth.Value) === 'false');
        }

        function onLoad(model) {
            var cmp = this,
                tpl = model.getComponentTemplate(cmp.TemplateId);

            state.Model = model;

            Editors.chunkInitSingle({
                cmp: cmp,
                cmpId: cmpId,
                template: tpl,
                props: getProps(),
                handlers: [
                    { id: 'UseCredentialsForSMTP', handler: function () { updateSmtpCredentialsUsing(cmpId); }, trigger: true }
                ],
                postInit: function () {
                    processSmtpCredentialsVisibilty(cmp);
                }
            });
        }

        function onSave(model) {
            Editors.chunkUpdateSingle({
                cmp: this,
                cmpId: cmpId,
                isTemplate: !model.haveTemplate(),
                props: getProps()
            });
        }


        function onToolbarCredentialsClick(record) {
            if (record.id === cmpId) {
                var val, jSel, newCred;
                val = String(record.get('CredentialSetId').Value);
                jSel = Editors.getInput(record.id, Props.map.CredentialSMTP);
                if (jSel.length) {
                    if (!jSel.is(':visible')) {
                        EditApp.Utility.toggleOverrideUi(jSel, true);
                    }
                    if (jSel.val() !== val) {
                        if (jSel.find(SF('[value={0}]', val)).length === 0) {
                            newCred = state.Model.Credentials.slice(-1)[0];
                            jSel.append(SF('<option value={0}>{1}</option>', val, newCred.Name));
                        }
                        jSel.val(val);
                    }
                }
            }
        }

        EditApp.Grid.registerEditor(cmpId, onLoad, onSave);
        EventManager.on('onToolbarCredentialsClick', onToolbarCredentialsClick);
    }


    function initMulti(cmpId, cmpIds) {

        function updateSubEditorForUseCredentialSmtp() {
            var jUseCred, jCred, jMultiCheck, jEditor, isMultiChecked;

            jUseCred = Editors.getInput(cmpId, Props.map.UseCredentialsForSMTP);
            jCred = Editors.getInput(cmpId, Props.map.CredentialSMTP);
            jMultiCheck = EditApp.getEditRow(jUseCred).find('input[id$=MultiChekbox]');
            jEditor = EditApp.getEditor(jUseCred);
            isMultiChecked = jMultiCheck.is(':checked');

            EditApp.$enable(jEditor.find('[id]'), isMultiChecked);

            jUseCred.attr('apm-me-was-changed', isMultiChecked);

            if (!isMultiChecked) {
                jCred.attr('apm-me-was-changed', isMultiChecked);
                EditApp.getEditRow(jCred).hide();
            }

            if (isMultiChecked && jUseCred.is(':checked')) {
                EditApp.getEditRow(jCred).show();
            }

            return true;
        }

        function onLoad() {
            Editors.chunkInitMulti({
                cmp: EditApp.ComponentDefinitions.MapiUserExperience,
                cmpId: cmpId,
                cmpIds: cmpIds,
                handlers: [
                    { id: 'UseCredentialsForSMTP', handler: function () { updateSmtpCredentialsUsing(cmpId); }, trigger: true }
                ],
                props: getProps(),
                postInit: function () {
                    var jUseCred, jMulti;

                    jUseCred = Editors.getInput(cmpId, Props.map.UseCredentialsForSMTP);
                    jMulti = EditApp.getEditRow(jUseCred).find('input[id$=MultiChekbox]');

                    jMulti
                        .unbind('click')
                        .bind('click', updateSubEditorForUseCredentialSmtp);
                }
            });
        }

        function onSave(app) {
            Editors.chunkUpdateMulti({
                cmp: app.getMultiEditComponent(),
                cmpId: cmpId,
                cmpIds: cmpIds,
                props: getProps()
            });
        }

        EditApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
    }


    function init(isMulti, cmpId, cmpIds) {
        isMulti = Editors.parseBool(isMulti);
        cmpId = Editors.parseId(cmpId);
        if (isMulti) {
            cmpIds = Editors.parseIdList(cmpIds);
            initMulti(cmpId, cmpIds);
        } else {
            initSingle(cmpId);
        }
    }


    // publish methods
    MapiUserExperience.init = init;
});
