/*jslint browser: true*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.EditApp.PollingMethod', function (PM, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        preferredPollingTypes = {
            'LogFile': 'Agent'
        },
        TXT = {
            AppLevelMsgWarning: '@{R=APM.Strings;K=ApmApplicationPreferredPollingMethodMismatch;E=js}',
            AppLevelMsgInfo: '@{R=APM.Strings;K=ApmApplicationPreferredPollingMethodPresent;E=js}',
            CmpLevelMsgWarning: '@{R=APM.Strings;K=ApmApplicationComponentPreferredPollingMethodMismatch;E=js}',
            methodTitle: {
                'Agent': '@{R=APM.Strings;K=APMWEBJS_Agent;E=js}',
                'Agentless': '@{R=APM.Strings;K=APMWEBJS_Agentless;E=js}'
            }
        },
        SEL = {
            clsPMI: 'pollingMethodInfo',
            clsBox: 'sw-suggestion',
            clsWarn: 'sw-suggestion-warn',
            clsInfo: 'sw-suggestion-info',
            clsIcon: 'sw-suggestion-icon'
        };

    SEL.selPMI = '.' + SEL.clsPMI;


    function isVisible(jEl) {
        return EditApp.isVisible(jEl.closest('.editor'));
    }

    function getEditor(opts) {
        return opts.jRoot.find('#appExecutePollingMethod');
    }

    function getViewer(opts) {
        return opts.jRoot.find('.viewer');
    }

    function setSuggestionBox(opts) {
        var jBox = opts.jBox;
        jBox.addClass(SEL.clsBox);
        if (opts.warn) {
            jBox.removeClass(SEL.clsInfo);
            jBox.addClass(SEL.clsWarn);
        } else {
            jBox.removeClass(SEL.clsWarn);
            jBox.addClass(SEL.clsInfo);
        }
        jBox.html(opts.msg);
        $('<span>')
            .addClass(SEL.clsIcon)
            .prependTo(jBox);
    }

    function getOrCreateContainer(opts) {
        var jEditor, jParent, jWarn;
        jEditor = getEditor(opts);
        jParent = (isVisible(jEditor) ? jEditor : getViewer(opts)).closest('.edit');
        jWarn = jParent.children(SEL.selPMI);
        if (!jWarn.length) {
            jWarn = $('<div>')
                .appendTo(jParent)
                .addClass(SEL.clsPMI);
        }
        return jWarn;
    }

    function getMethodName(method) {
        return TXT.methodTitle[method];
    }

    function formatNames(cmps) {
        return APMjs.linqSelectUnique(cmps, function (cmp) {
            return cmp.name;
        }).join(',');
    }

    function formatWarning(failedCmps) {
        var names, method;
        names = formatNames(failedCmps);
        method = failedCmps[0].method;
        return APMjs.format(TXT.AppLevelMsgWarning, names, method);
    }

    function formatInfo(cmps) {
        var names, method;
        names = formatNames(cmps);
        method = cmps[0].method;
        return APMjs.format(TXT.AppLevelMsgInfo, names, method);
    }

    function hideWarning(opts) {
        var jContainer = getOrCreateContainer(opts);
        jContainer.hide();
    }

    function getFailedComponents(opts, value) {
        var failedCmps = [];
        APMjs.linqEach(opts.components, function (cmp) {
            if (cmp.method !== value) {
                failedCmps.push(cmp);
            }
        });
        return failedCmps;
    }

    function processPollingInfoApp(opts) {
        var jWarn, boxopts;
        if (opts.components && opts.components.length) {
            jWarn = getOrCreateContainer(opts);
            boxopts = { jBox: jWarn };
            if (opts.failedCmps && opts.failedCmps.length) {
                boxopts.warn = true;
                boxopts.msg = formatWarning(opts.failedCmps);
            } else {
                boxopts.msg = formatInfo(opts.components);
            }
            setSuggestionBox(boxopts);
            jWarn.show();
        } else {
            hideWarning(opts);
        }
    }

    function hideCmpWarnings(opts) {
        opts.jCmpTable.parent().find(SEL.selPMI).hide();
    }

    function getCmpRows(jCmpTable) {
        return jCmpTable.find('tr').filter(function () {
            return $(this).children('td.x-grid3-td-expander').length > 0;
        });
    }

    function getOrCreateCmpContainer(jRow) {
        var jTable, jWarn;
        jTable = jRow.closest('table');
        jWarn = jTable.prev();
        if (!jWarn.length || !jWarn.is(SEL.selPMI)) {
            jWarn = $('<div>')
                .insertBefore(jTable)
                .addClass(SEL.clsPMI);
        }
        return jWarn;
    }

    function showCmpWarnings(opts) {
        var jCmpRows;
        hideCmpWarnings(opts);
        jCmpRows = getCmpRows(opts.jCmpTable);
        jCmpRows.each(function (i) {
            var match, msg, jWarn, jRow = $(this);
            match = APMjs.linqFirst(opts.failedCmps, function (cmp) {
                return cmp.index === i;
            });
            if (match) {
                msg = APMjs.format(TXT.CmpLevelMsgWarning, getMethodName(match.method));
                jWarn = getOrCreateCmpContainer(jRow);
                setSuggestionBox({
                    jBox: jWarn,
                    msg: msg,
                    warn: true
                });
                jWarn.show();
            }
        });
    }

    function getComponentInfo(cmp, index) {
        var method = preferredPollingTypes[cmp.ComponentType];
        return method ? {
            id: cmp.Id,
            name: cmp.Name,
            index: index,
            method: method
        } : undefined;
    }

    function parseComponents(app) {
        return APMjs.linqSelect(app.Components, getComponentInfo);
    }

    function updateComponents(opts) {
        opts.components = parseComponents(opts.app);
    }

    function processPollingInfoCmp(opts) {
        var jGrid, jTable;
        jGrid = $('#grid');
        jTable = jGrid.find('table.x-grid3-row-table');
        opts.jCmpTable = jTable;
        showCmpWarnings(opts);
    }

    function getPollingMethodValue(opts) {
        var jEditor, jViewer, value;
        jEditor = getEditor(opts);
        jViewer = getViewer(opts);
        value = isVisible(jEditor) ? jEditor.val() : jViewer.text();
        return value;
    }

    function processPollingInfo(opts) {
        var value = getPollingMethodValue(opts);
        updateComponents(opts);
        opts.failedCmps = getFailedComponents(opts, value);
        processPollingInfoApp(opts);
        processPollingInfoCmp(opts);
    }

    function rebind(jEl, event, handler) {
        jEl.unbind(event, handler).bind(event, handler);
    }

    function getGrid() {
        return $('#grid .x-panel-body');
    }

    function init(options) {
        var opts, jRoot, jEditor, jGrid;

        function update() {
            processPollingInfo(opts);
        }

        function updateLater() {
            APMjs.launch(update);
        }

        jRoot = $('#' + options.idEditorRoot).closest('td');

        if (jRoot.data('sam-ppm')) {
            return;
        }

        jRoot.data('sam-ppm', true);

        opts = {
            app: options.app,
            jRoot: jRoot
        };

        // reevaluate on editor change (other changes are bound to it by IsComples setting)
        jEditor = getEditor(opts);
        rebind(jEditor, 'change', updateLater);

        // reevaluate on grid change (component added etc)
        jGrid = getGrid(opts);
        rebind(jGrid, 'update', updateLater);

        // evaluate the initial option
        updateLater();
    }

    PM.init = init;
});
