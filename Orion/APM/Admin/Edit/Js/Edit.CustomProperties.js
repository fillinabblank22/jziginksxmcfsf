﻿Ext.namespace("SW.APM.Admin");

SW.APM.Admin.CustomProperties = function (appModel, renderTo) {
    var mPanel;
    var mAppModel = appModel;

    /*helper members*/
    var getClassName = function(model) {
        return "CP_" + model.PropertyName;
    };

    var isEnumProperty = function(model) { return Ext.isArray(model.EnumValues) && model.EnumValues.length > 0; };

    var getEmptyProperty = function () {
        return {
            items: [
                { xtype: "textfield", width: 200, fieldLabel: "", hideLabel: true, hidden: true },
                { xtype: "label", html: "<b>" + "@{R=APM.Strings;K=APMWEBJS_NotAvailableForCurrentApplicationProperties;E=js}"+"</b>" }
            ]
        };
    };

    var getEnumProperty = function (model) {
        var values = [["", "None"]];
        $.each(model.EnumValues, function (index, item) {
            values.push([item, item]);
        });
        return {
            labelSeparator: '',
            items: [
                {
                    id: model.buildId(), xtype: "combo", width: 200, fieldLabel: getFieldLabelHtml(model.PropertyName),
                    typeAhead: false, triggerAction: "all", anchor: "95%", editable: false, selectOnFocus: false, allowBlank: true,
                    queryMode: "local", mode: "local", valueField: "comboValue", displayField: "comboText", value: model.PropertyValue,
                    store: {
                        xtype: "arraystore",
                        fields: ["comboValue", "comboText"],
                        data: values
                    },
                    listeners: {
                        afterrender: renderValidationErrorContainer
                    }
                },
                { xtype: "label", text: model.PropertyDescription }
            ]
        };
    };

    var getInt32Property = function (model) {
        if (isEnumProperty(model)) {
            return getEnumProperty(model);
        }
        return {
            labelSeparator: '',
            items: [
                {
                    id: model.buildId(), xtype: "numberfield", width: 200, allowDecimals: false, fieldLabel: getFieldLabelHtml(model.PropertyName), value: model.PropertyValue,
                    stripCharsRe: /(?!^)\-|[^0-9\-]/g, cls: getClassName(model),
                    listeners: {
                        afterrender: renderValidationErrorContainer
                    }
                },
                { xtype: "label", text: model.PropertyDescription }
            ]
        };
    };
    var getSingleProperty = function (model) {
        if (isEnumProperty(model)) {
            return getEnumProperty(model);
        }
        return {
            labelSeparator: '',
            items: [
                {
                    id: model.buildId(), xtype: "numberfield", width: 200, cls: getClassName(model), fieldLabel: getFieldLabelHtml(model.PropertyName),
                    value: model.PropertyValue.replace(",", "."), stripCharsRe: /(?!^)\-|\.(?=[0-9]*\.)/g,
                    listeners: {
                        afterrender: renderValidationErrorContainer
                    }
                },
                { xtype: "label", text: model.PropertyDescription }
            ]
        };
    };
    var getDateTimeProperty = function (model) {
        var parts = model.PropertyValue.split(" ");
        while (parts.length < 2) {
            parts.push("");
        }
        return {
            labelSeparator: '',
            items: [
                {
                    id: model.buildId("Date"), xtype: "datefield", width: 100, altFormats: "Y-m-d", value: parts[0],
                    fieldLabel: getFieldLabelHtml(model.PropertyName), editable: false, cls: getClassName(model), 
                },
                {
                    id: model.buildId("Time"), xtype: "timefield", width: 95, altFormats: "H:i", value: parts[1], editable: false,
                    listeners: {
                        afterrender: renderValidationErrorContainer
                    }
                },
                { xtype: "label", text: model.PropertyDescription }
            ]
        };
    };
    var getBooleanProperty = function (model, items) {
        return {
            labelSeparator: '',
            items: [
                {
                    id: model.buildId(), xtype: "combo", width: 200, fieldLabel: getFieldLabelHtml(model.PropertyName),
                    typeAhead: false, triggerAction: "all", anchor: "95%", editable: false, selectOnFocus: false, allowBlank: true,
                    queryMode: "local", mode: "local", valueField: "comboValue", displayField: "comboText", value: model.PropertyValue,
                    store: {
                        xtype: "arraystore", fields: ["comboValue", "comboText"], data: [["True", "Yes"], ["False", "No"]]
                    }
                },
                { xtype: "label", text: model.PropertyDescription }
            ]
        };
    };

    var getStringProperty = function (model) {
        if (isEnumProperty(model)) {
            return getEnumProperty(model);
        }
        return {
            labelSeparator: '',
            items: [
                { id: model.buildId(), xtype: "textfield", width: 200, fieldLabel: getFieldLabelHtml(model.PropertyName),
                    value: model.PropertyValue, cls: getClassName(model),
                    listeners: {
                        afterrender: renderValidationErrorContainer
                    }
                },
                { xtype: "label", text: model.PropertyDescription }
            ]
        };
    };

    var getProperty = function (model) {
        if (!model) {
            return getEmptyProperty();
        }
        if (model.PropertyType == "System.Int32") {
            return getInt32Property(model);
        }
        if (model.PropertyType == "System.Single") {
            return getSingleProperty(model);
        }
        if (model.PropertyType == "System.DateTime") {
            return getDateTimeProperty(model);
        }
        if (model.PropertyType == "System.Boolean") {
            return getBooleanProperty(model);
        }
        return getStringProperty(model);
    };

    var getFieldLabelHtml = function (propertyName) {
        return '<label class= "cp-tooltip label" title="' + propertyName + '">' + propertyName + "</label>";
    };

    var renderValidationErrorContainer = function(input) {
        $("#" + input.id).parents(".x-form-composite")
            .append('<div class="validationMsg"><span class="validationError sw-validation-error"></span></div>');
    };

    /*public members*/
    this.render = function () {
        var items = [];

        if (mAppModel.CustomProperties.length == 0) {
            items.push(getEmptyProperty());
        } else {
            $.each(mAppModel.CustomProperties, function () {
                this.buildId = function (suffix) {
                    return SF("CP_{0}{1}", this.PropertyName, suffix || "");
                }
                items.push(getProperty(this));
            });
        }
        mPanel = new Ext.Panel({
            layout: "anchor", labelWidth: 75, frame: false, border: false, width: ($("div#grid").width() - 200),
            renderTo: document.body,
            items: [{
                xtype: "fieldset", collapsible: false, autoHeight: true, border: false,
                defaults: { anchor: "95%", border: false, xtype: "compositefield" },
                items: items
            }]
        });

        //workaround for IE7 and IE8
        setTimeout(function () {
            $(mPanel.el.dom).prependTo(renderTo);

        }, 1000);
    };
    this.save = function () {
        $.each(mAppModel.CustomProperties, function (i, cp) {
            var value;
            if (this.PropertyType == "System.DateTime") {
                var datePart = Ext.getCmp(this.buildId("Date")).getValue();
                if (datePart != "") {
                    value = SF("{0}-{1}-{2}", datePart.getFullYear(), datePart.getMonth() + 1, datePart.getDate());
                }
                var timePart = Ext.getCmp(this.buildId("Time")).getValue();
                if (timePart != "") {
                    value = SF("{0} {1}", value, timePart);
                }
            } else {
                value = Ext.getCmp(this.buildId()).getValue();
            }

            this.PropertyValue = value != null ? value : "";
        });
    }
};