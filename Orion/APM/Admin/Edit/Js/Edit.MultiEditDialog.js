/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.EditApp.MultiEditDialog', function (MultiEditDialog, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        SF = APMjs.format,
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        isDemoMode = APMjs.isDemoMode,
        demoModeMessage = APMjs.demoModeMessage;


    function init(cmpType, cmpIds, model) {

        var validator, extDialog;

        // helper members

        function initUi() {
            var cmp = model.getMultiEditComponent();
            if (cmp && APMjs.isFn(cmp.loadToForm)) {
                cmp.loadToForm(model);
            }
        }

        function updateModel() {
            var cmp = model.getMultiEditComponent();
            if (cmp && APMjs.isFn(cmp.saveFromForm)) {
                cmp.saveFromForm(model);
            }
        }

        function loadEditorError(response) {
            extDialog.buttons[0].disable();
            EditApp.Utility.handleException('multi', response.responseText, $(extDialog.items.first().body.dom).empty());
        }

        function loadEditor(html) {
            var jContent = $(html),
                jRowBody = $(extDialog.items.first().body.dom).empty(),
                editorHtml;

            if (EditApp.Utility.isValidResponse(jContent)) {

                editorHtml = jContent.find('div.ComponentEditorParent').html();
                jContent = $(SF('<div class="componentEditContent"><form id="meForm">{0}</form></div>', editorHtml));
                jRowBody.append(jContent);

                validator = $('#meForm').validate({
                    ignore: '[apmsettctr=false]',
                    onkeyup: false,
                    errorPlacement: function (error, element) {
                        var selector = $(element).parents('tr').is('.thresholdEditor') ? 'tr' : 'td';
                        error.appendTo(element.parents(selector).eq(0).find('.validationError'));
                    },
                    highlight: function (element) {
                        var selector = $(element).parents('tr').is('.thresholdEditor') ? 'tr' : 'td';
                        $(element).parents(selector).eq(0).find('.validationMsg').show();
                    },
                    unhighlight: function (element) {
                        var selector = $(element).parents('tr').is('.thresholdEditor') ? 'tr' : 'td';
                        $(element).parents(selector).eq(0).find('.validationMsg').hide();
                    }
                });

                initUi();
            } else {
                loadEditorError({ responseText: html });
            }
        }

        function onBeforeComponentLoaded(html) {
            var jRowBody = $(extDialog.items.first().body.dom).empty(),
                jContent = $(html),
                url = SF('/Orion/APM/Admin/Edit/ComponentEditors/{0}.aspx?mode=multi&ids={1}', cmpType, cmpIds.join(','));

            if (EditApp.Utility.isValidResponse(jContent)) {
                jContent = $('<div class="componentEditContentF">');
                jRowBody.append(jContent);

                jRowBody
                    .find('.componentEditContentF')
                    .load(url, function () {
                        loadEditor(html);
                    });
            } else {
                loadEditorError({ responseText: html });
            }
        }


        // dialog's button events

        function onCancelClick() {
            if (extDialog) {
                extDialog.close();
                extDialog = null;
            }
            return false;
        }


        function onSaveClick() {
            var messages = [],
                textMsgLine = '&nbsp;&nbsp;&nbsp;<b>{0}</b> {1}';

            if (isDemoMode()) {
                return demoModeMessage();
            }

            if (validator.form()) {
                updateModel();
                onCancelClick();
            } else {
                if (validator.errorList.length === 1) {
                    messages.push('@{R=APM.Strings;K=APMWEBJS_ThereIsAnErrorOnThePage;E=js}');
                } else {
                    messages.push(SF('@{R=APM.Strings;K=APMWEBJS_ThereAreErrorsOnThePage;E=js}', validator.errorList.length));
                }

                $.each(validator.errorList, function () {
                    var message = this.message,
                        label = $(this.element)
                            .parents('tr:first')
                            .find('td.columnLabel.edit.label').text(),
                        tr = $(this.element).parents('tr');

                    if (tr.is('.thresholdEditor')) {
                        if (tr.is('.thresholdEditor.warningThreshold')) {
                            messages.push(SF(textMsgLine, 'Warning:', $.trim(message)));
                        } else {
                            messages.push(SF(textMsgLine, 'Critical: ', $.trim(message)));
                        }
                    } else {
                        messages.push(SF(textMsgLine, $.trim(label), $.trim(message)));
                    }
                });

                Ext.Msg.show({
                    title: '@{R=APM.Strings;K=APMWEBJS_FailedToSave;E=js}',
                    msg: messages.join('<br/>'),
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
            return false;
        }


        // public members

        function show() {
            var body = Ext.getBody(),
                width = Math.round(body.getWidth() * 0.7),
                height = Math.round(body.getHeight() * 0.8),
                top = $(window).scrollTop() + 100;

            if (width < 850) {
                width = 850;
            }

            extDialog = new Ext.Window({
                title: '@{R=APM.Strings;K=APMWEBJS_EditMultipleComponentMonitors;E=js}',
                renderTo: Ext.getBody(),
                y: top,
                width: width,
                height: height,
                bodyCssClass: 'ComponentEditorParent',
                border: false,
                region: 'center',
                layout: 'fit',
                modal: true,
                items: [{
                    xtype: 'panel',
                    height: height,
                    autoScroll: true,
                    html: '<div style="padding:10px; font-weight:bold;"><img src="/Orion/APM/Images/loading_gen_16x16.gif"> ' + '@{R=APM.Strings;K=APMWEBJS_LoadingSettings;E=js}'+'<div>'
                }],
                buttons: [
                    { text: '@{R=APM.Strings;K=APMWEBJS_Save;E=js}', handler: onSaveClick },
                    { text: '@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}', handler: onCancelClick}
                ]
            });

            extDialog.show();

            $.ajax({
                type: 'GET',
                url: SF('/Orion/APM/Admin/Edit/ComponentEditors/{0}.aspx?mode=multi&ids={1}&IsBlackBox={2}&ApplicationItemId={3}',
                    cmpType,
                    cmpIds.join(','),
                    model.IsReadOnly,
                    model.ApplicationItemId
                    ),
                data: {},
                success: onBeforeComponentLoaded,
                error: loadEditorError
            });
        }

        return APMjs.seal({
            show: show
        });
    }


    // publish module methods

    MultiEditDialog.show = function (cmpType, cmpIds, model) {
        var dialog = init(cmpType, cmpIds, model);
        dialog.show();
        return false;
    };

    APMjs.seal(MultiEditDialog);
});
