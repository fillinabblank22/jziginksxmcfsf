/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
/// <reference path="../../../APM.js"/>
/// <reference path="Edit.DynamicEvidence.js"/>
/// <reference path="Edit.Thresholds.js"/>
/// <reference path="Edit.Utility.js"/>
APMjs.initGlobal('SW.APM.EditApp', function (EditApp, APMjs, global) {
    'use strict';

    var SW = APMjs.assertGlobal('SW'),
        $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        EM = APMjs.assertGlobal('SW.APM.EventManager'),
        SF = APMjs.format,
        setBlockVisibility = APMjs.setBlockVisibility,
        AppValidator = null,
        ComponentValidators = {},
        Model = null,
        SETTING_LEVEL = {
            Template: 1,
            Instance: 2
        },
        SETTING_VALUETYPE = {
            'String': 0,
            'Integer': 1,
            'DateTime': 2,
            'Boolean': 3,
            'External': 4,
            'Option': 5,
            'Double': 6
        },
        SETT_NAME = {
            OriginalFormula: '__DataTransformOriginalFormula',
            Enabled: '__DataTransformEnabled',
            Checked: '__DataTransformCheckedRadioButton',
            FormulaIndex: '__DataTransformCommonFormulaIndex',
            FormulaOptions: '__DataTransformCommonFormulaOptions',
            Expanded: '__DataTransformTestExpanded',
            Input: '__DataTransformInput',
            Output: '__DataTransformOutput'
        },
        // TODO: resources
        MSG = {
            SaveLoading: '@{R=APM.Strings;K=APMWEBJS_SavingChangesPleaseWait;E=js}',
            SaveFailed: '@{R=APM.Strings;K=APMWEBJS_FailedToSave;E=js}',
            ErrorOnPage: '@{R=APM.Strings;K=APMWEBJS_ThereIsAnErrorOnThePage;E=js}',
            ErrorsOnPage: '@{R=APM.Strings;K=APMWEBJS_ThereAreErrorsOnThePage;E=js}',
            ErrorDuringSave: '@{R=APM.Strings;K=APMWEBJS_ErrorOccurredDuringSaveChanges;E=js}',
            AddAtLeastOne: '@{R=APM.Strings;K=APMWEBJS_AddAtLeastOneComponent;E=js}',
            EnableAtLeastOne: '@{R=APM.Strings;K=APMWEBJS_AtLeastOneComponentShouldBeEnabled;E=js}',
            RedirectingToWizard: '@{R=APM.Strings;K=APMWEBJS_RedirectingToComponentMonitorWizardPleaseWait;E=js}'
        },
        selRadioGroup = '.radioGroup';

    function pushContent(arr1, arr2) {
        if (arr1 && arr2 && arr2.length) {
            Array.prototype.push.apply(arr1, arr2);
        }
    }

    function getSettingValue(o, defval) {
        return (o && APMjs.isDef(o.Value)) ? o.Value : defval;
    }

    function parseSettingValue(setting, defval) {
        if (!APMjs.isSet(setting)) {
            return defval;
        }
        var value = getSettingValue(setting, defval);
        if (setting.ValueType === SETTING_VALUETYPE.Boolean) {
            value = EditApp.Utility.parseBoolean(value);
        }
        return value;
    }

    function isVisible(jTarget) {
        return !APMjs.linqAny(jTarget, function (el) {
            return !APMjs.isSet(el) || el.style.display === 'none';
        });
    }

    function hideErrorSummary() {
        $('#formErrors').hide();
    }

    function validateEditComponent(cmpId) {
        var componentValidator, isValid, recIds;

        componentValidator = ComponentValidators[cmpId];

        if (!componentValidator) {
            return true;
        }

        isValid = true;
        recIds = [];

		componentValidator.enabledErrorAlert = true;
		
		if (!componentValidator.form()) {
            isValid = false;
        }
		
		APMjs.linqEach(componentValidator.invalidElements(), function (el) {
		    var cmpId = $(el).attr('cmpid');
            if (cmpId === String(cmpId)) {
                isValid = false;
                recIds.push(cmpId);
            }
        });
		
        if (recIds.length) {
            EM.fire('onValidationFailed', { failedRecordIds: recIds });
        }
		
		if(isValid === true){
			componentValidator.enabledErrorAlert = false;
		}
        return isValid;
    }
	
    function validateEditApp() {
        var isValid;
        hideErrorSummary();
        isValid = true;
        /*jslint unparam: true*/
        APMjs.linqEach(Object.keys(ComponentValidators), function (cmpId) {
            if (!validateEditComponent(cmpId)) {
                isValid = false;
            }
        });
        /*jslint unparam: false*/
        if (!AppValidator || !AppValidator.form()) {
            isValid = false;
        }

        if (Model.CustomProperties && !customPropertiesValidator()) {
            isValid = false;
        }
        return isValid;
    }

    function customPropertiesValidator() {
        var valid = true;
        $.each(Model.CustomProperties, function (index, element) {
            var inputElement = $("#CP_" + element.PropertyName);
            var isElementValid = true;

            if (element.Mandatory) {
                switch (element.PropertyType) {
                    case "System.DateTime": {
                        isElementValid = validateDateTime(element.PropertyValue);
                    } break;
                    default: {
                        isElementValid = element.PropertyValue !== "";
                    }
                }

                if (!isElementValid) {
                    notifyCustomPropertyValidationError(inputElement, "@{R=APM.Strings;K=APMWEBJS_ThisFieldIsRequired;E=js}");
                    valid = isElementValid;
                    return true;
                }
            }

            if (element.PropertyValue) {
                var errorMessage = "";
                switch (element.PropertyType) {
                    case "System.Int32": {
                        if (!validateInt32Size(element.PropertyValue)) {
                            isElementValid = false;
                            errorMessage = "@{R=APM.Strings;K=APMWEBJS_ValueMustBeaValidInteger;E=js}";
                        }
                    } break;
                    case "System.Single": {
                        if (!validateSingleSize(element.PropertyValue)) {
                            isElementValid = false;
                            errorMessage = "@{R=APM.Strings;K=APMWEBJS_ValueMustBeaValidFloatingPointNumber;E=js}";
                        }
                    } break;
                    case "System.DateTime": {
                        if (!validateDateTime(element.PropertyValue)) {
                            isElementValid = false;
                            errorMessage = "@{R=APM.Strings;K=APMWEBJS_ThisFieldIsRequired;E=js}";
                        }
                    } break;
                    case "System.String": {
                        if (element.Size !== -1 && element.PropertyValue.length > element.Size) {
                            isElementValid = false;
                            errorMessage = SF("@{R=APM.Strings;K=APMWEBJS_ValueMustBeEqualOrLessThanCharacters;E=js}", element.Size);
                        }
                    } break;
                }
            }


            if (!isElementValid) {
                notifyCustomPropertyValidationError(inputElement, errorMessage);
                valid = isElementValid;
            }
        });
        return valid;
    }

    function validateDateTime(date) {
        return date.indexOf("undefined") === -1;
    }

    function validateInt32Size(value) {
        var parsedValue = parseInt(value);
        return parsedValue <= 2147483647 && parsedValue >= -2147483648;
    }

    function validateSingleSize(value) {
        var parsedValue = parseFloat(value);
        return parsedValue <= 3.402823e38 && parsedValue >= -3.402823e38;
    }

    function notifyCustomPropertyValidationError(input, errorMessage) {
        AppValidator.errorList.push({
            message: errorMessage,
            element: input
        });

        input.parents(".x-form-element").find(".validationMsg").show();
        input.parents(".x-form-element").find(".validationError").text(errorMessage);
    }

    function componentsIsLoading() {
        return (EditApp.ComponentsThatLoadingNow.length > 0);
    }

    function saveComponentOrderingFromUi() {
        if (Model.ComponentOrderSettingLevel !== 1) {
            APMjs.linqEach(Model.Components, function (cmp) {
                var val = parseInt($('#orderInput' + cmp.Id).val(), 10);
                if (!isNaN(val)) {
                    cmp.ComponentOrder = val;
                }
            });
        }
    }

    function isComponentTestingNow(componentId) {
        var index = EditApp.ComponentsThatTestingNow.indexOf(componentId);
        return (index !== -1);
    }

    function showErrorSummary(validator) {
        var errors = [], messages = [], msg = MSG.ErrorOnPage;

        if (typeof validator === 'undefined' || !(validator || AppValidator)) {
            return;
        }

        if (!validator) {
            pushContent(errors, AppValidator.errorList);
            APMjs.linqEach(ComponentValidators, function (cval) {
                pushContent(errors, cval.errorList);
            });
            validator = AppValidator;
            validator.enabledErrorAlert = true;
        } else {
            pushContent(errors, validator.errorList);
        }

        if (errors.length === 1) {
            messages.push(msg);
        } else {
            msg = SF(MSG.ErrorsOnPage, errors.length);
            messages.push(msg);
        }

        APMjs.linqEach(errors, function (error) {
            var jEl, jRow, message, label;
            message = APMjs.trim(error.message);
            jEl = $(error.element);
            jRow = jEl.parents('tr');
            if (jRow.is('.thresholdEditor')) {
                if (jRow.is('.warningThreshold')) {
                    messages.push(SF('&nbsp;&nbsp;&nbsp;<b>{0}</b> {1}', '@{R=APM.Strings;K=APM_Status_Warning;E=js}'+':', message));
                } else {
                    messages.push(SF('&nbsp;&nbsp;&nbsp;<b>{0}</b> {1}', '@{R=APM.Strings;K=APM_Status_Critical;E=js}'+':', message));
                }
            } else {
                var labelToFind = [];
                while (labelToFind.length === 0) {
                    jEl = jEl.parent();
                    labelToFind = jEl.find(".label:first");
                }
                label = labelToFind.text();
                label = APMjs.trim(label);
                messages.push(SF('&nbsp;&nbsp;&nbsp;<b>{0}</b> {1}', label, message));
            }
        });

        if (validator.enabledErrorAlert) {
            Ext.Msg.show({ title: MSG.SaveFailed, msg: messages.join('<br/>'), icon: Ext.MessageBox.ERROR, buttons: Ext.MessageBox.OK });
        }

        $('#formErrors-txt').text(msg);
        $('#formErrors').show();
    }

    function hasTemplateEnableComponents() {
        return APMjs.linqAny(Model.Components, function (cmp) {
            var value = EditApp.Utility.parseBoolean(cmp.IsDisabled && cmp.IsDisabled.Value);
            return value === false;
        });
    }

    function onSaveToServer(handleSuccess) {

        function handleError() {
            EditApp.Grid.hideLoadMask();
        }

        function saveToFormDelegate() {
            Model.saveToServer(handleSuccess, handleError);
        }

        var asyncPostProcessingCalled = false;
        Model.callCustomEditor(function (editor) {
            if (editor && editor.asyncPostProcessing) {
                try {
                    if (editor.asyncPostProcessing(Model, Model.template, saveToFormDelegate, handleError, saveToFormDelegate)) {
                        asyncPostProcessingCalled = true;
                    }
                } catch (ignore) {
                }
            }
        }, Model.CustomApplicationType);
        if (!asyncPostProcessingCalled) {
            saveToFormDelegate();
        }
    }

    // edit page events
    function applyChangesInternal(handleSuccess) {
        try {
            // Save General App setting back into the model
            Model.saveFromForm();
            if (hasTemplateEnableComponents()) {
                EditApp.Grid.showLoadMask(MSG.SaveLoading);
                if (validateEditApp()) {
                    onSaveToServer(handleSuccess);
                } else {
                    EditApp.Grid.hideLoadMask();
                    showErrorSummary();
                }
            } else {
                if (Model.Components.length === 0) {
                    $('#formErrors-txt').text(MSG.AddAtLeastOne);
                } else {
                    $('#formErrors-txt').text(MSG.EnableAtLeastOne);
                }
                $('#formErrors').show();
            }
        } catch (e) {
            EditApp.Grid.hideLoadMask();
            throw SF(MSG.ErrorDuringSave, e);
        }
        return false;
    }

    function quitFromPage(loadMsg, flowBtnId) {
        EditApp.Grid.showLoadMask(loadMsg);
        $(SF('div.buttonBar input[id$={0}]', flowBtnId)).click();
    }

    function populateRetrieveValue(res) {
        function action() {
            var cmps = EditApp.ComponentsThatLoadingNow,
                loading = cmps.indexOf(res.Id) !== -1;
            if (!(res.TestPassed === false && res.StatisticValue === 0)) {
                if (loading) {
                    setTimeout(action, 100);
                } else {
                    $('#cmpConvertValue' + res.Id + 'InputTextbox').val(res.StatisticValue);
                }
            } else {
                $('#cmpConvertValue' + res.Id + 'InputTextbox').val('');
            }
        }
        setTimeout(action, 100);
    }

    function populateDynamicRetrieveValue(res, retriveDynamicComponents) {
        var retriveDynamicComponentsCount = 0;
        APMjs.linqEach(retriveDynamicComponents, function () {
            retriveDynamicComponentsCount += 1;
        });
        if (res.TestPassed && retriveDynamicComponentsCount > 0) {
            APMjs.linqEach(retriveDynamicComponents, function (cmp, i) {
                var j, cmp2;
                if (cmp && cmp.id === res.Id) {
                    setTimeout(function () {
                        var id = SF('#{0}-{1}InputTextbox', cmp.column, cmp.id);
                        $(id).val(res.MultiStatisticValues[cmp.columnName]);
                    }, 2300);
                    for (j = 0; j < retriveDynamicComponents.length; j += 1) {
                        cmp2 = retriveDynamicComponents[j];
                        if (cmp2 && cmp2.id === res.Id && cmp2.column === cmp.column) {
                            delete retriveDynamicComponents[i];
                            break;
                        }
                    }
                }
            });
        }
        return retriveDynamicComponents;
    }


    function getBasename(name) {
        if (name && name.jquery) {
            name = name.attr('id');
        }
        return (name || '').replace(/Operator$/, '');
    }


    function getEditCol(jChild) {
        return jChild.closest('td.columnEditor');
    }

    function getEditRow(jChild) {
        var jRow = getEditCol(jChild).closest('tr');
        if (!jRow.length) {
            // TODO remove this hack after fixing mess in DynamicEvidence
            jRow = jChild.closest('tr');
        }
        return jRow;
    }

    function getOverrideLink(jChild, jRow) {
        jRow = jRow || getEditRow(jChild);
        return jRow.find('.overrideLink');
    }

    function getEditor(jChild, jRow) {
        jRow = jRow || getEditRow(jChild);
        return jRow.find('.editor');
    }

    function getViewer(jChild, jRow) {
        jRow = jRow || getEditRow(jChild);
        return jRow.find('.viewer');
    }


    function initDataTransform(jChild, item, itemTemplate) {
        var isOverriden, haveTemplate, jRow, baseName, jLink, jViewer, settings, templateValue, transformExpression, originalFormula,
            enabled, checkedRadioButton, testInput, testOutput, commonFormulaIndex, commonFormulaOptions, expandedTestFormula, templateValueEnabled,
            parseBool, decimalPlaces;

        isOverriden = false;
        haveTemplate = APMjs.isSet(itemTemplate);
        jRow = getEditRow(jChild);
        baseName = getBasename(jChild);
        jViewer = getViewer(jChild, jRow);
        jLink = getOverrideLink(jChild, jRow);
        jLink = EditApp.Utility.convertLinkToButton(jLink);
        parseBool = EditApp.Utility.parseBoolean;

        settings = item.Settings;

        // is dynamic
        if (item.DataTransform) {
            transformExpression = item.DataTransform.TransformExpression;
            originalFormula = item.DataTransform.OriginalFormula || '';
            enabled = parseBool(item.DataTransform.Enabled || 0);
            checkedRadioButton = item.DataTransform.CheckedRadioButton || 0;
            expandedTestFormula = parseBool(item.DataTransform.TestExpanded || 0);
            testInput = item.DataTransform.TestInput || '';
            testOutput = item.DataTransform.TestOutput || '';
            commonFormulaIndex = item.DataTransform.CommonFormulaIndex || 0;
            commonFormulaOptions = item.DataTransform.CommonFormulaOptions || '';

            if (transformExpression) {
                isOverriden = item.DataTransformOverridden;
            }

            haveTemplate = (item.ComponentID > 0 && item.ParentID > 0);
            if (haveTemplate && itemTemplate.DataTransform) {
                templateValue = itemTemplate.DataTransform.TransformExpression;
            }
        } else {
            transformExpression = settings.TransformExpression;
            isOverriden = (transformExpression && transformExpression.SettingLevel === SETTING_LEVEL.Instance);
            transformExpression = getSettingValue(transformExpression);

            originalFormula = getSettingValue(settings[SETT_NAME.OriginalFormula], '');
            enabled = parseBool(getSettingValue(settings[SETT_NAME.Enabled], false));
            checkedRadioButton = getSettingValue(settings[SETT_NAME.Checked], 0);
            expandedTestFormula = parseBool(getSettingValue(settings[SETT_NAME.Expanded], true));
            testInput = getSettingValue(settings[SETT_NAME.Input], '');
            testOutput = getSettingValue(settings[SETT_NAME.Output], '');
            commonFormulaIndex = parseInt(getSettingValue(settings[SETT_NAME.FormulaIndex], 0), 10);
            commonFormulaOptions = getSettingValue(settings[SETT_NAME.FormulaOptions], '');

            if (haveTemplate) {
                templateValue = getSettingValue(itemTemplate.Settings.TransformExpression);
                templateValueEnabled = parseBool(getSettingValue(itemTemplate.Settings[SETT_NAME.Enabled], false));
                if (templateValueEnabled && templateValue === '') {
                    templateValue = 'Yes';
                }
            }
        }

        $('#' + baseName + 'CustomFormulaTextbox').val(originalFormula === '' ? transformExpression : originalFormula);

        $('#' + baseName).prop('checked', enabled);
        setBlockVisibility(baseName + 'TransformSettingsEditor', enabled);

        if (!APMjs.isDef(checkedRadioButton)) {
            checkedRadioButton = 0;
        }

        $('input[name="' + baseName + 'Radio"][value="' + String(checkedRadioButton) + '"]')
            .click();

        $('#' + baseName + 'ExpandCollapseTestImage')
            .attr('src', expandedTestFormula ? '/Orion/images/Button.Expand.gif' : '/Orion/images/Button.Collapse.gif')
            .click();

        $('#' + baseName + 'InputTextbox').val(testInput);
        $('#' + baseName + 'OutputSpan').val(testOutput);

        $('#' + baseName + 'CommonFormulasList')
            .val(String(commonFormulaIndex))
            .click();

        if (commonFormulaIndex === 1 || commonFormulaIndex === 2) {
            decimalPlaces = (commonFormulaOptions === '') ? '0' : commonFormulaOptions;
            $('#' + baseName + 'DecimalPlacesList').val(String(decimalPlaces));
        }

        // Show the editor if we are overriding the template.
        // If there is no template then always show the editor
        EditApp.Utility.toggleOverrideUi(jChild, !haveTemplate || isOverriden);

        if (haveTemplate) {
            templateValue = templateValue || '';
            jViewer.text(templateValue === '' ? 'No' : templateValue);
            jLink.show();
        } else {
            jLink.hide();
        }
    }

    function initDynamicColumnsModel(cmp, template, isTemplate, showEditScriptButton) {
        var editDynamicEvidence, compId, cmpColumnSettings, tmplColumnSettings,
            colTmplPattern, colItemIdPattern, colNum,
            jTemp;

        editDynamicEvidence = new EditApp.DynamicEvidence(Model);
        compId = cmp.Id;
        cmpColumnSettings = cmp.DynamicColumnSettings;

        if (!isTemplate && template) {
            tmplColumnSettings = template.DynamicColumnSettings;
        }

        colTmplPattern = 'column-edit-container-template-' + compId;
        colItemIdPattern = 'column-edit-container-item-' + compId;

        jTemp = $('#' + colTmplPattern);

        // remove existing elements
        $('tbody[id^="' + colItemIdPattern + '"]', jTemp.parent()).remove();

        colNum = 1;
        APMjs.linqEach(cmpColumnSettings, function (column) {
            var jNew, jExisting, templateColumn, dynTransEl, threshEl;

            if (column.Type !== 1) {
                return;
            }

            jNew = jTemp.clone();
            jNew.attr('id', SF('{0}-{1}', colItemIdPattern, column.ID));
            jNew.show();
            jNew.fadeIn('slow');

            editDynamicEvidence.updateDynamicColumnItem(jNew, cmp, column, tmplColumnSettings, colNum);
            colNum += 1;

            jExisting = $('tbody[id^="' + colItemIdPattern + '"]', jTemp.parent()).last();
            jNew.insertAfter(jExisting.length ? jExisting : jTemp);

            editDynamicEvidence.initDynamicColumnsConvertValueEvents(jNew, column, cmp);

            templateColumn = null;
            if (tmplColumnSettings) {
                templateColumn = EditApp.DynamicEvidence.getParentColumnFromTemplate(column, tmplColumnSettings);
            }

            dynTransEl = jNew.find('.dataTransformEnableCheckbox');
            initDataTransform(dynTransEl, column, templateColumn);

            threshEl = jNew.find('.thresholdOperator');
            EditApp.Thresholds.initThreshold(threshEl, column.Name, column, templateColumn);
        });

        showEditScriptButton.unbind('click').bind('click', function () {
            editDynamicEvidence.showEditScriptDialog(cmp);
            return false;
        });
    }

    function withRadio(jItem, handleRadio, handleOther) {
        if (jItem.is(selRadioGroup)) {
            var jRadio = jItem.find(':radio');
            return APMjs.isFn(handleRadio) && handleRadio(jRadio);
        }
        return APMjs.isFn(handleOther) && handleOther(jItem);
    }

    function getDisplayValue(jChild, templateValue) {
        var selector, display, jTmp;
        display = templateValue;
        if (jChild.is('select')) {
            // For select dropdowns, we want to show the display text not the value.
            if (jChild.attr('apm-data-type') === 'bool') {
                selector = SF('option[value="{0}"]', templateValue.toLowerCase());
            } else {
                selector = SF('option[value="{0}"]', templateValue);
            }
            display = jChild.children(selector).text();
        } else if (jChild.is(selRadioGroup)) {
            selector = SF('input[value="{0}"]', templateValue);
            display = jChild.find(selector).parent().text();
        } else if (jChild.is(':checkbox')) {
            selector = SF('label[for="{0}"]', jChild.attr('id'));
            jTmp = jChild.parent().find(selector);
            display = EditApp.Utility.parseBoolean(templateValue);
            display = jTmp.length ? (display ? jTmp.text() : '') : String(display);
        }
        return display;
    }


    /*
        function getMultiValue(jOptions) {
            return jOptions.filter(function () {
                return $(this).is(':checked');
            }).map(function () {
                return $(this).attr('value');
            }).get().join(',');
        }
    
        function setMultiValue(jOptions, value) {
            var values = APMjs.isSet(value) ? String(value).split(',') : [];
            jOptions.each(function () {
                var jIn = $(this);
                jIn.prop('checked', values.indexOf(jIn.attr('value')) !== -1);
            });
        }
    */

    function ensureString(value) {
        return APMjs.isSet(value) ? String(value) : '';
    }

    function getRadioValue(jRadio) {
        return jRadio.filter(function () {
            return $(this).is(':checked');
        }).val();
    }

    function setRadioValue(jRadio, value) {
        value = ensureString(value);
        jRadio.each(function () {
            var jIn = $(this);
            jIn.prop('checked', jIn.attr('value') === value);
        });
    }

    function getOtherValue(jOther) {
        return jOther.is(':checkbox') ? String(jOther.is(':checked')) : jOther.val();
    }

    function setOtherValue(jOther, value) {
        if (jOther.is(':checkbox')) {
            jOther.prop('checked', Boolean(value));
        } else {
            jOther.val(ensureString(value));
        }
    }


    function reportMissingSetting(key, jRow) {
        APMjs.console.warn(SF('setting `{0}` not found', key));
        jRow.children('td').css({
            'background': '#fcc'
        });
        jRow.find('*').attr('disabled', 'disabled');
    }

    function checkMissingSetting(setting, key, jRow) {
        if (APMjs.isObj(setting)) {
            return false;
        }
        reportMissingSetting(key, jRow);
        return true;
    }

    function initUiState(jChild, settingPropertyName, item, itemTemplate, isReadOnly) {
        var jRow, jLink, jViewer, jEditor,
            appSetting, haveTemplate, isOverriden, showEditor, valueToSet,
            credId, templateValue, display;

        isReadOnly = APMjs.isBool(isReadOnly) ? isReadOnly : false;

        jRow = getEditRow(jChild);
        jViewer = getViewer(jChild, jRow);
        jEditor = getEditor(jChild, jRow);

        jLink = getOverrideLink(jChild);
        jLink = EditApp.Utility.convertLinkToButton(jLink);

        appSetting = item[settingPropertyName];

        if (checkMissingSetting(appSetting, settingPropertyName, jRow)) {
            return;
        }

        haveTemplate = APMjs.isSet(itemTemplate);
        isOverriden = appSetting.SettingLevel === SETTING_LEVEL.Instance;

        // Show the editor if we are overriding the template.
        // If there is no template then always show the editor
        showEditor = haveTemplate ? isOverriden : true;

        EditApp.Utility.toggleOverrideUi(jChild, showEditor, jRow);

        valueToSet = parseSettingValue(appSetting);

        if (!jChild.length) {
            return;
        }

        withRadio(jChild, function (jRadio) {
            setRadioValue(jRadio, valueToSet);
        }, function (jOther) {
            setOtherValue(jOther, valueToSet);
        });

        if (!jChild.is(':checkbox')) {
            if ((appSetting.IsRequired || appSetting.Required) && isVisible(jChild) && isVisible(jChild.parents())) {
                jChild.addClass('required');
            }
            if (settingPropertyName === 'CredentialSetId') {
                jChild.removeClass('required');
                credId = jChild.val();
                if (valueToSet > 0 && (credId === '0' || credId === null) && jChild.selector.indexOf('PrivateKey') === -1) {
                    if (EditApp.ScriptMonitorCredentials && EditApp.ScriptMonitorCredentials.name) {
                        jChild.append(SF('<option value="{0}">{1}</option>', valueToSet, EditApp.ScriptMonitorCredentials.name));
                        jChild.val(String(valueToSet));
                    }
                }
            }
        }

        if (haveTemplate) {
            templateValue = String(getSettingValue(itemTemplate[settingPropertyName]));
            display = getDisplayValue(jChild, templateValue);

            getEditCol(jChild)
                .find('#isMultilineEditor .grippie')
                .show()
                .css('width', '98%')
                .end()
                .find('#isMultilineViewer .grippie')
                .hide()
                .css('width', '98%');

            jViewer.text(display);

            APMjs.setVisibility(jLink, !isReadOnly);
        } else {
            jLink.hide();
        }

        // the editor/viewer cell is initially hidden to keep from flashing.
        jEditor.parent().show();
    }

    function $enable(jElem, enable) {
        var attrName = 'disabled';
        if (enable === undefined || !!enable) {
            jElem.removeAttr(attrName);
        } else {
            jElem.attr(attrName, attrName);
        }
    }

    function initMultiUiState(jEl, settings, key) {
        var jRow, jEditor, setting, value;

        function disableEditor() {
            var checked, jNodes, jInclude, jExclude;

            checked = this.checked || false;
            jNodes = jEditor.children();

            if (jNodes.length) {
                jEl.attr('apm-me-was-changed', checked);

                jNodes.each(function () {
                    var jNode = $(this), jRadio;
                    $enable(jNode, checked);
                    if (jNode.hasClass('stringEditor')) {
                        $enable(jNode.children().eq(1), checked);
                    } else if (jNode.is(selRadioGroup)) {
                        jRadio = jNode.find(':radio');
                        $enable(jRadio, checked);
                        if (checked) {
                            if (!jRadio.find(':checked').length) {
                                jRadio.eq(0).prop('checked', true);
                            }
                        }
                    }
                    $enable(jNode.find('.apm_Resizable:first'), checked);
                });

                jInclude = jEditor.find('select[id^=cmpEntryIncludeOperation]');
                if (jInclude.length && jInclude.val() === 'Disable') {
                    $enable(jEditor.find('textarea[id^=cmpEntryIncludeOperation]'), false);
                }

                jExclude = jEditor.find('select[id^=cmpEntryExcludeOperation]');
                if (jExclude.length && jExclude.val() === 'Disable') {
                    $enable(jEditor.find('textarea[id^=cmpEntryExcludeOperation]'), false);
                }
            }

            return true;
        }

        jRow = getEditRow(jEl);
        getOverrideLink(jEl).hide();
        getViewer(jEl).hide();

        jEditor = getEditor(jEl);
        jEditor.show();

        jRow.find('input[id$=MultiChekbox]')
            .click(disableEditor)
            .each(disableEditor);

        setting = settings[key];

        if (checkMissingSetting(setting, key, jRow)) {
            return;
        }

        value = parseSettingValue(setting);

        withRadio(jEl, function (jRadio) {
            setRadioValue(jRadio, value);
        }, function (jOther) {
            setOtherValue(jOther, value);
        });

        if (!jEl.is(':checkbox') && (setting && (setting.IsRequired || setting.Required))) {
            jEl.addClass('required');
        }

        jEditor.parent().show();
    }

    function initMultiDataTransform(jEl, settings) {
        var jRow, jEditor, baseName,
            transformExpression, originalFormula, transformEnabled, checkedRadioButton, expandedTestFormula,
            testInput, testOutput, commonFormulaIndex, commonFormulaOptions, decimalPlaces;

        function disableEditor() {
            var checked, jNodes, selector = '#' + baseName + ',#' + baseName + 'TransformSettingsEditor';

            checked = this.checked || false;
            jNodes = jEditor.children();

            if (jNodes.length) {
                jEl.attr('apm-me-was-changed', checked);
                $enable($(selector), checked);
            }

            return true;
        }

        jRow = getEditRow(jEl);
        jEditor = getEditor(jEl).show();
        getOverrideLink(jEl).hide();
        getViewer(jEl).hide();

        EM.fire('onGridRowUpdated');

        baseName = jEl.attr('id');

        jRow.find('input[id$=MultiChekbox]')
            .click(disableEditor)
            .each(disableEditor);

        transformExpression = getSettingValue(settings.TransformExpression);
        originalFormula = getSettingValue(settings[SETT_NAME.OriginalFormula], '');
        $(SF('#{0}CustomFormulaTextbox', baseName)).val(originalFormula || transformExpression);

        transformEnabled = EditApp.Utility.parseBoolean(getSettingValue(settings[SETT_NAME.Enabled], false));
        $(SF('#{0}', baseName)).prop('checked', transformEnabled);
        setBlockVisibility(SF('{0}TransformSettingsEditor', baseName), transformEnabled);

        checkedRadioButton = getSettingValue(settings[SETT_NAME.Checked], 0);
        $(SF('input[name={0}Radio]', baseName)).prop('checked', false);
        $(SF("input[name={0}Radio][value='{1}']", baseName, String(checkedRadioButton))).click();

        expandedTestFormula = EditApp.Utility.parseBoolean(getSettingValue(settings[SETT_NAME.Expanded], true));
        $(SF('#{0}ExpandCollapseTestImage', baseName))
            .attr('src', expandedTestFormula ? '/Orion/images/Button.Expand.gif' : '/Orion/images/Button.Collapse.gif')
            .click();

        testInput = getSettingValue(settings[SETT_NAME.Input], '');
        testOutput = getSettingValue(settings[SETT_NAME.Output], '');
        $(SF('#{0}InputTextbox', baseName)).val(testInput);
        $(SF('#{0}OutputSpan', baseName)).val(testOutput);

        commonFormulaIndex = getSettingValue(settings[SETT_NAME.FormulaIndex], 0);
        commonFormulaOptions = getSettingValue(settings[SETT_NAME.FormulaOptions], '');
        $(SF('#{0}CommonFormulasList', baseName))
            .val(String(commonFormulaIndex))
            .click();

        if (commonFormulaIndex === 1 || commonFormulaIndex === 2) {
            decimalPlaces = commonFormulaOptions === '' ? '0' : commonFormulaOptions;
            $(SF('#{0}DecimalPlacesList', baseName)).val(String(decimalPlaces));
        }

        $(SF('#{0}ExpandCollapse,#{0}InputTableRow,#{0}OutputTableRow', baseName)).hide();
    }

    function setSetting(target, value, valueType, settingLevel) {
        if (target) {
            if (APMjs.isDef(value)) {
                target.Value = value;
            }
            if (APMjs.isSet(valueType)) {
                target.ValueType = valueType;
            }
            if (APMjs.isSet(settingLevel)) {
                target.SettingLevel = settingLevel;
            }
        }
    }

    function copySetting(target, source) {
        if (target && source) {
            setSetting(target, source.Value, source.ValueType, source.SettingLevel);
        }
    }


    function updateMultiModelFromUiInternal(jEl, key, cmpId) {
        var cmp, value;

        cmp = Model.getComponent(cmpId);

        if (!cmp) {
            return;
        }

        if (key === 'CredentialSetId') {
            setSetting(cmp.CredentialSetId, jEl.val(), undefined, Model.DefaultSettingLevel);
        } else {
            withRadio(jEl, function (jRadio) {
                value = getRadioValue(jRadio);
            }, function (jOther) {
                value = getOtherValue(jOther);
            });
            setSetting(cmp.Settings[key], value, undefined, Model.DefaultSettingLevel);
        }

        if (cmp.loadToForm) {
            cmp.loadToForm(Model);
        }
    }

    /*jslint unparam: true*/
    function updateMultiModelFromUi(jEl, settings, key, cmpIds) {
        if (!EditApp.Utility.parseBoolean(jEl.attr('apm-me-was-changed'))) {
            return;
        }
        APMjs.linqEach(cmpIds, function (cmpId) {
            updateMultiModelFromUiInternal(jEl, key, cmpId);
        });
    }
    /*jslint unparam: false*/


    function updateMultiDataTransformModelFromUiInternal(baseName, cmpId) {
        var cmp, dataTransformEnabled, checkedRadioButton, commonFormulaIndex, commonFormulaOptions, transformExpression, currentValueProcess;
        cmp = Model.getComponent(cmpId);
        if (!cmp) {
            return false;
        }

        dataTransformEnabled = $('#' + baseName).is(':checked') || 'false';
        checkedRadioButton = $(SF('input[name={0}Radio]:checked', baseName)).val();
        commonFormulaIndex = $(SF('#{0}CommonFormulasList', baseName)).val();
        commonFormulaOptions = $(SF('#{0}DecimalPlacesList', baseName)).val();
        transformExpression = '';
        currentValueProcess = $(SF('#{0}GetCurrentValueProcess', baseName)).val();

        if (!EditApp.Utility.parseBoolean(currentValueProcess)) {
            if (dataTransformEnabled) {
                if (checkedRadioButton === '0') {
                    switch (commonFormulaIndex) {
                        case '1':
                            transformExpression = SF('Truncate(${Statistic}, {0})', commonFormulaOptions);
                            break;
                        case '2':
                            transformExpression = SF('Round(${Statistic}, {0})', commonFormulaOptions);
                            break;
                    }
                } else {
                    transformExpression = $(SF('#{0}CustomFormulaTextbox', baseName)).val();
                }
            }
        }

        setSetting(cmp.Settings.TransformExpression, transformExpression, undefined, Model.DefaultSettingLevel);
        setSetting(cmp.Settings[SETT_NAME.Enabled], dataTransformEnabled, undefined, Model.DefaultSettingLevel);
        setSetting(cmp.Settings[SETT_NAME.Checked], checkedRadioButton, undefined, Model.DefaultSettingLevel);
        setSetting(cmp.Settings[SETT_NAME.FormulaIndex], commonFormulaIndex, undefined, Model.DefaultSettingLevel);
        setSetting(cmp.Settings[SETT_NAME.FormulaOptions], commonFormulaOptions, undefined, Model.DefaultSettingLevel);

        if (cmp.loadToForm) {
            cmp.loadToForm(Model);
        }
    }

    /*jslint unparam: true*/
    function updateMultiDataTransformModelFromUi(jEl, settings, cmpIds) {
        if (!EditApp.Utility.parseBoolean(jEl.attr('apm-me-was-changed'))) {
            return;
        }
        var baseName = jEl.attr('id');
        APMjs.linqEach(cmpIds, function (cmpId) {
            updateMultiDataTransformModelFromUiInternal(baseName, cmpId);
        });
    }
    /*jslint unparam: false*/

    function updateDataTransformModelFromUi(jItem, cmp, isTemplate, isDynamicColumn, column, templateColumn) {
        var jEditor, isOveridden, baseName, settingLevel, templateDataTransform, dataTransformEnabled,
            checkedRadioButton, expandedTestFormula, testInput, commonFormulaIndex, commonFormulaOptions, transformExpression,
            compId, component, template;

        jEditor = getEditor(jItem);
        isOveridden = isVisible(jEditor);

        if (isDynamicColumn) {
            column.DataTransformOverridden = (!isTemplate && isOveridden);
        }

        if (isOveridden) {
            baseName = jItem.attr('id');
            dataTransformEnabled = String($('#' + baseName).prop('checked'));
            checkedRadioButton = $('input[name="' + baseName + 'Radio"]:checked').val();
            expandedTestFormula = ($('#' + baseName + 'ExpandCollapseTestImage').attr('src') || '').indexOf('Collapse') > 0;
            testInput = $('#' + baseName + 'InputTextbox').val();
            commonFormulaIndex = $('#' + baseName + 'CommonFormulasList').val();
            commonFormulaOptions = $('#' + baseName + 'DecimalPlacesList').val();
            transformExpression = '';

            if (dataTransformEnabled) {
                if (checkedRadioButton === '0') {
                    switch (commonFormulaIndex) {
                        case '0':
                            transformExpression = '';
                            break;
                        case '1':
                            transformExpression = 'Truncate(${Statistic}, ' + commonFormulaOptions + ')';
                            break;
                        case '2':
                            transformExpression = 'Round(${Statistic}, ' + commonFormulaOptions + ')';
                            break;
                        default:
                            transformExpression = '';
                            break;
                    }
                } else {
                    transformExpression = $('#' + baseName + 'CustomFormulaTextbox').val();
                }
            }

            if (EditApp.Utility.parseBoolean($('#' + baseName + 'GetCurrentValueProcess').val())) {
                transformExpression = '';
            }

            if (!isDynamicColumn) {
                settingLevel = (isTemplate || !isOveridden) ? SETTING_LEVEL.Template : SETTING_LEVEL.Instance;

                if (dataTransformEnabled === 'false') {
                    transformExpression = '';
                }

                setSetting(cmp.Settings.TransformExpression, transformExpression, undefined, settingLevel);
                setSetting(cmp.Settings[SETT_NAME.Enabled], dataTransformEnabled, SETTING_VALUETYPE.Boolean, settingLevel);
                setSetting(cmp.Settings[SETT_NAME.Checked], checkedRadioButton, SETTING_VALUETYPE.Boolean, settingLevel);
                setSetting(cmp.Settings[SETT_NAME.FormulaIndex], commonFormulaIndex, SETTING_VALUETYPE.Integer, settingLevel);
                setSetting(cmp.Settings[SETT_NAME.FormulaOptions], commonFormulaOptions, SETTING_VALUETYPE.String, settingLevel);
            } else {
                column.DataTransform.TransformExpression = transformExpression;
                column.DataTransform.OriginalFormula = transformExpression;
                column.DataTransform.Enabled = dataTransformEnabled;
                column.DataTransform.CheckedRadioButton = checkedRadioButton;
                column.DataTransform.TestExpanded = expandedTestFormula.toString();
                column.DataTransform.TestInput = testInput;
                column.DataTransform.CommonFormulaIndex = commonFormulaIndex;
                column.DataTransform.CommonFormulaOptions = commonFormulaOptions;
            }
        } else {
            //save override state
            if (isDynamicColumn) {
                templateDataTransform = templateColumn ? templateColumn.DataTransform : null;

                if (!templateDataTransform) {
                    templateDataTransform = {
                        TransformExpression: '',
                        OriginalFormula: '',
                        Enabled: false,
                        CheckedRadioButton: 0,
                        TestExpanded: false,
                        TestInput: '',
                        CommonFormulaIndex: '',
                        CommonFormulaOptions: ''
                    };
                }

                column.DataTransform.TransformExpression = templateDataTransform.TransformExpression;
                column.DataTransform.OriginalFormula = templateDataTransform.OriginalFormula;
                column.DataTransform.Enabled = templateDataTransform.Enabled;
                column.DataTransform.CheckedRadioButton = templateDataTransform.CheckedRadioButton;
                column.DataTransform.TestExpanded = templateDataTransform.TestExpanded;
                column.DataTransform.TestInput = templateDataTransform.TestInput;
                column.DataTransform.CommonFormulaIndex = templateDataTransform.CommonFormulaIndex;
                column.DataTransform.CommonFormulaOptions = templateDataTransform.CommonFormulaOptions;
            } else {
                if (!isTemplate) {
                    compId = jItem.eq(0).attr('cmpid');
                    if (compId) {
                        component = Model.getComponent(compId);
                        template = Model.getComponentTemplate(component.TemplateId);
                        if (template) {
                            copySetting(cmp.Settings.TransformExpression, template.Settings.TransformExpression);
                            copySetting(cmp.Settings[SETT_NAME.Enabled], template.Settings[SETT_NAME.Enabled]);
                            copySetting(cmp.Settings[SETT_NAME.Checked], template.Settings[SETT_NAME.Checked]);
                            copySetting(cmp.Settings[SETT_NAME.FormulaIndex], template.Settings[SETT_NAME.FormulaIndex]);
                            copySetting(cmp.Settings[SETT_NAME.FormulaOptions], template.Settings[SETT_NAME.FormulaOptions]);
                        }
                    }
                }
            }
        }
    }

    function updateDynamicColumnsModelFromUi(cmp, template, isTemplate) {
        var compId = cmp.Id,
            cmpColumnSettings = cmp.DynamicColumnSettings,
            colItemIdPattern = 'column-edit-container-item-' + compId;

        APMjs.linqEach(cmpColumnSettings, function (column) {
            if (column.Type !== 1) {
                return;
            }
            var colEl, templateColumn, dataTrEl, thresholdElement;

            colEl = $('#' + colItemIdPattern + '-' + column.ID);
            templateColumn = null;

            if (!isTemplate && template) {
                templateColumn = EditApp.DynamicEvidence.getParentColumnFromTemplate(column, template.DynamicColumnSettings);
            }

            EditApp.DynamicEvidence.saveDynamicColumnItem(colEl, cmp, column, isTemplate, templateColumn);

            dataTrEl = $('#' + column.ID + '-' + compId);
            updateDataTransformModelFromUi(dataTrEl, null, isTemplate, true, column, templateColumn);

            thresholdElement = $('#' + column.ID + '-' + compId + 'Operator');
            EditApp.Thresholds.updateThresholdModelFromUi(thresholdElement, column.Threshold, isTemplate, true, column, templateColumn);
        });
    }

    function isCredential(jItem) {
        return jItem && jItem.selector.indexOf('cmpCredential') !== -1 && jItem.is('select');
    }

    function updateModelFromUi(jItem, appSetting, isTemplate) {
        var jEditor, isOveridden, compId, component, template, templateSettings;

        if (!appSetting) {
            APMjs.console.warn('appSetting undefined');
            return;
        }

        jEditor = getEditor(jItem);

        // If the editor is visible, then we are overriding the template (or there isn't one);
        isOveridden = isVisible(jEditor);

        //fix overridden state when user click Test button few times
        if (isCredential(jItem)) {
            if ((jItem.val() === null) && (appSetting.SettingLevel === SETTING_LEVEL.Template)) {
                isOveridden = false;
            }
        }

        appSetting.SettingLevel = (isTemplate || !isOveridden) ? SETTING_LEVEL.Template : SETTING_LEVEL.Instance;

        if (isOveridden) {
            //fix credentials value when user click test button few times
            if (isCredential(jItem)) {
                if (jItem.val() !== null) {
                    appSetting.Value = getOtherValue(jItem);
                }
            } else {
                withRadio(jItem, function (jRadio) {
                    appSetting.Value = getRadioValue(jRadio);
                }, function (jOther) {
                    appSetting.Value = getOtherValue(jOther);
                });
            }
        } else {
            if (!isTemplate && appSetting.Key) {
                if (jItem.is(':text,:checkbox,textarea') || jItem[0].type === 'select-one') {
                    compId = jItem.attr('cmpid');
                    if (compId) {
                        component = Model.getComponent(compId);
                        template = Model.getComponentTemplate(component.TemplateId);
                        templateSettings = template && template.Settings;
                        if (templateSettings) {
                            appSetting.Value = getSettingValue(templateSettings[appSetting.Key]);
                        }
                    }
                }
            }
        }
    }

    function createSubmitButtons() {
        var cancel, back, next, submit, apply;

        if (EditApp.AppModel.editMode === 'FindProcessesWizard') {
            back = SW.Core.Widgets.Button('@{R=APM.Strings;K=APMWEBJS_Back;E=js}', { type: 'secondary', id: 'backBtn' });
            next = SW.Core.Widgets.Button('@{R=APM.Strings;K=APMWEBJS_Next;E=js}', { type: 'primary', id: 'nextBtn' });
            cancel = SW.Core.Widgets.Button('@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}', { type: 'secondary', id: 'cancelBtn', cls: 'sw-btn-cancel' });

            $('.buttonBar').append([back, next, cancel].join('\n'));

            $('#backBtn').click(function () {
                $('div.apm_WizardButtons input[id$=btnBack]').click();
            });

            $('#nextBtn').click(function () {
                hideErrorSummary();
                applyChangesInternal(function () {
                    EditApp.Grid.showLoadMask(MSG.SaveLoading);
                    $('div.apm_WizardButtons input[id$=btnNext]').click();
                });
                return false;
            });

            $('#cancelBtn').click(function () {
                $('div.apm_WizardButtons input[id$=btnCancel]').click();
            });
        } else {
            submit = SW.Core.Widgets.Button('@{R=APM.Strings;K=APMWEBJS_VB1_38;E=js}', { type: 'primary', id: 'submitBtn' });
            apply = SW.Core.Widgets.Button('@{R=APM.Strings;K=APMWEBJS_SaveAndContinueWorking;E=js}', { type: 'secondary', id: 'applyBtn' });
            cancel = SW.Core.Widgets.Button('@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}', { type: 'secondary', id: 'cancelBtn', cls: 'sw-btn-cancel' });

            $('.buttonBar').append([submit, apply, cancel].join('\n'));

            $('#applyBtn').unbind('click').click(EditApp.applyChanges);
            $('#cancelBtn').unbind('click').click(EditApp.cancel);
        }
    }

    function updateCommonTestSettings(settings) {
        var use64Bit = false, jTmp, executePollingMethod = '';

        jTmp = $('#appMainProp #appUse64Bit');
        if (jTmp.length) {
            if (jTmp.parent().css('display') === 'none') {
                // inherited
                jTmp = jTmp.parent().parent().find('span.viewer');
                use64Bit = jTmp.text() === 'x64';
            } else {
                // overriden
                use64Bit = jTmp.val();
            }
        }
        settings.Use64Bit = use64Bit;

        jTmp = $('#appMainProp #appExecutePollingMethod');
        if (jTmp.length) {
            if (jTmp.parent().css('display') === 'none') {
                // inherited
                jTmp = jTmp.parent().parent().find('span.viewer');
                executePollingMethod = jTmp.text();
            } else {
                // overriden
                executePollingMethod = jTmp.val();
            }
        }
        settings.ExecutePollingMethod = executePollingMethod;
    }

    function deleteComponentValidator(id) {
        delete ComponentValidators[id];
    }

    function getModel() {
        return Model;
    }

    function setModel(model) {
        Model = model;
    }

    function validatorInvalidHandler(validator) {
        var errors;
        hideErrorSummary();
        errors = validator.numberOfInvalids();
        if (errors) {
            showErrorSummary(validator);
        }
    }

    function getValidatorContainer(element) {
        var jEl = $(element), jParent, selContainer, jContainer, reParent = /^\:\:parent\((\d*)\)/, match, parentCount;
        selContainer = jEl.attr('data-validator-container-selector');
        if (selContainer) {
            match = reParent.exec(selContainer);
            if (match) {
                parentCount = parseInt(match[1] || '1', 10);
                selContainer = selContainer.replace(reParent, '');
                while (parentCount--) {
                    jParent = (jParent || jEl).parent();
                }
            }
            jContainer = $(selContainer, jParent);
            if (jContainer.length) {
                return jContainer;
            }
        }
        return jEl.closest('td').find('.validationMsg');
    }

    function validatorErrorPlacement(jError, element) {
        var jContainer = getValidatorContainer(element);
        jError.appendTo(jContainer);
    }

    function validatorHighlight(element) {
        getValidatorContainer(element).show();
    }

    function validatorUnhighlight(element) {
        getValidatorContainer(element).hide();
    }

    function setupUiEvents(component, jParentContainer) {
        var componentValidator;

        // hook up the override links
        $('a.overrideLink', jParentContainer).unbind('click').click(EditApp.Utility.onOverrideLinkClick);

        // set the static critical text to change when the dropdown for the warning threshold changes
        $('select.thresholdOperator', jParentContainer).unbind('change').change(function () {
            var selectedText = $(this).find('option:selected').text();
            $('#' + this.id + 'CriticalOp', jParentContainer).text(selectedText);
            $('#' + this.id + 'BaselineApplied').css('visibility', 'hidden');
            $('#' + this.id + 'WarningBaselineValue').val('');
            $('#' + this.id + 'CriticalBaselineValue').val('');
        });

        // baseline dialog start ---------------------------------

        // set the static critical text to change when the dropdown for the warning threshold changes
        $('#LatestBaselineDetails select.thresholdDialogOperator').unbind('change').change(function () {
            var jSelected = $(this).find('option:selected');
            $('#LatestBaselineDetails #CriticalOp').text(jSelected.text());
            $('#LatestBaselineDetails img.baseline').css('visibility', 'hidden');
            $('#LatestBaselineDetails #WarningBaselineValue').val('');
            $('#LatestBaselineDetails #CriticalBaselineValue').val('');

            EditApp.ThresholdsBaseline.highlightRecommendedThresholds(parseInt(jSelected.val(), 10));
        });

        // baseline icon indication displaying
        $('#LatestBaselineDetails #WarningLevel, #LatestBaselineDetails #CriticalLevel').unbind('change').bind('change', function () {
            var visible = ($('#LatestBaselineDetails #WarningBaselineValue').val() !== '' &&
                $('#LatestBaselineDetails #CriticalBaselineValue').val() !== '' &&
                $('#LatestBaselineDetails #WarningBaselineValue').val() === $('#LatestBaselineDetails #WarningLevel').val() &&
                $('#LatestBaselineDetails #CriticalBaselineValue').val() === $('#LatestBaselineDetails #CriticalLevel').val());
            $('#LatestBaselineDetails img.baseline').css('visibility', visible ? 'visible' : 'hidden');
        });

        // reset to recommended thresholds link
        $('#LatestBaselineDetails #ResetToRecommendedThresholds').unbind('click').bind('click', function () {
            $('#tblStatistics div.warning').removeClass('warning');
            $('#tblStatistics div.critical').removeClass('critical');
            $('#tblStatistics div.recommendedWarning').addClass('warning');
            $('#tblStatistics div.recommendedCritical').addClass('critical');

            EditApp.ThresholdsBaseline.redrawPlotBandsInCharts();
        });

        // baseline dialog end ------------------------------------
        SW.APM.TextAreaResizer.rebind(jParentContainer.find('.apm_Resizable'));

        if (component) {
            /*jslint unparam: true*/
            componentValidator = $('#componentEditorForm' + component.Id).validate({
                debug: true,
                onkeyup: false,
                invalidHandler: function (form, validator) {
                    validatorInvalidHandler(validator);
                },
                errorPlacement: validatorErrorPlacement,
                highlight: validatorHighlight,
                unhighlight: validatorUnhighlight
            });
            /*jslint unparam: false*/

            componentValidator.enabledErrorAlert = true;
            ComponentValidators[component.Id] = componentValidator;
        }
    }

    function addCredentialSets(jDropDowns, callback) {
        var creds = [];

        if (Model.Credentials && Model.Credentials.length) {
            creds = Ext.decode(Ext.encode(Model.Credentials));
        }

        creds.unshift({ Id: -3, Name: "@{R=APM.Strings;K=APMWEBJS_AK1_72;E=js}" });
        creds.unshift({ Id: 0, Name: "@{R=APM.Strings;K=APMWEBJS_None;E=js}" });

        jDropDowns.each(function () {
            var jDropdown = $(this).empty();
            APMjs.linqEach(creds, function (cred) {
                $('<option/>').val(cred.Id).text(cred.Name).appendTo(jDropdown);
            });
        });

        if (callback) {
            callback();
        }
    }

    function addSMTPCredentialSets(jDropDowns, callback) {
        var creds = [];

        if (Model.Credentials && Model.Credentials.length > 0) {
            creds = Ext.decode(Ext.encode(Model.Credentials));
        }

        creds.unshift({ Id: -3, Name: "@{R=APM.Strings;K=APMWEBJS_AK1_72;E=js}" });
        creds.unshift({ Id: 0, Name: '<Use Monitoring Credentials>' });

        jDropDowns.each(function () {
            var jDropdown = $(this).empty();
            APMjs.linqEach(creds, function (cred) {
                $('<option/>').val(cred.Id).text(cred.Name).appendTo(jDropdown);
            });
        });

        if (callback) {
            callback();
        }
    }

    function addCertificateSets(jDropDowns, callback) {
        var creds = [];

        if (Model.Certificates && Model.Certificates.length > 0) {
            creds = Ext.decode(Ext.encode(Model.Certificates));
        }

        creds.unshift({ Id: 0, Name: '@{R=APM.Strings;K=APMWEBJS_None;E=js}' });

        jDropDowns.each(function () {
            var jDropdown = $(this).empty();
            APMjs.linqEach(creds, function (cred) {
                $('<option/>').val(cred.Id).text(cred.Name).appendTo(jDropdown);
            });
        });

        if (callback) {
            callback();
        }
    }

    function save() {
        if (global.IsDemoMode()) {
            return global.DemoModeMessage();
        }
        hideErrorSummary();
        saveComponentOrderingFromUi();
        applyChangesInternal(function () {
            quitFromPage(MSG.SaveLoading, 'fakeBtnSubmit');
        });
        return false;
    }

    function saveAndBrowse() {
        if (global.IsDemoMode()) {
            return global.DemoModeMessage();
        }
        hideErrorSummary();
        saveComponentOrderingFromUi();
        if (Model.Components.length === 0) {
            quitFromPage(MSG.RedirectingToWizard, 'fakeBtnBrowseMonitors');
            return false;
        }
        applyChangesInternal(function () {
            quitFromPage(MSG.SaveLoading, 'fakeBtnBrowseMonitors');
        });
        return false;
    }

    function applyChanges() {
        if (global.IsDemoMode()) {
            return global.DemoModeMessage();
        }
        hideErrorSummary();
        saveComponentOrderingFromUi();
        applyChangesInternal(function () {
            EditApp.Grid.hideLoadMask();
        });
        return false;
    }

    function cancel() {
        hideErrorSummary();
        quitFromPage('@{R=APM.Strings;K=APMWEBJS_RevertingChangesPleaseWait;E=js}', 'fakeBtnCancel');
        return false;
    }

    function initModule() {
        $('#appEditContent').wrap('<form />');

        //TODO: we should extend EditApp class to use multiple validators
        //      (with automatically created validator on 'appMainProp' element),
        //      if we do so we can allow plugins to register their own validators
        //      (they will have their own elements forms with validators)
        //      it will allows the plugin to validate only their portion of UI if needed!

        if ($.fn.validate) {
            /*jslint unparam: true*/
            AppValidator = $('#appEditContent').parent('form').validate({
                debug: true,
                ignore: '[apmsettctr=false]',
                invalidHandler: function (form, validator) {
                    hideErrorSummary();
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        showErrorSummary(AppValidator);
                    }
                },
                errorPlacement: function (error, element) {
                    error.appendTo(element.parents('td').eq(0).find('.validationError'));
                },
                highlight: function (element) {
                    $(element).parents('td').eq(0).find('.validationMsg').show();
                },
                unhighlight: function (element) {
                    $(element).parents('td').eq(0).find('.validationMsg').hide();
                }
            });
            /*jslint unparam: false*/
            if (AppValidator) {
                AppValidator.enabledErrorAlert = true;
            } else {
                APMjs.console.warn('validator object not created for #appEditContent parent form');
            }
        } else {
            APMjs.console.warn('validator jquery plugin not present');
        }

        $('img.collapsable').click(function () {
            var $img, $target;

            $img = $(this);
            $target = $('#' + $img.attr('data-collapse-target'));

            if ($target.is(':visible')) {
                $target.hide();
                $img.attr('src', '/Orion/APM/Images/Button.Expand.gif');
            } else {
                $target.show();
                $img.attr('src', '/Orion/APM/Images/Button.Collapse.gif');
            }

            return false;
        });

        createSubmitButtons();

        EditApp.Grid.init();
        setTimeout(function () {
            EditApp.Grid.showLoadMask('@{R=APM.Strings;K=APMWEBJS_LoadingComponentsPleaseWait;E=js}');

            EditApp.AppModel.LoadFromServer(
                function (model) {
                    Model = model;
                    if (Model.IsReadOnly) {
                        $('#componentHeader').hide();
                    } else {
                        $('#componentHeader').show();
                    }
                    EditApp.Grid.loadData(Model);
                    Model.loadToForm();
                    EditApp.Grid.hideLoadMask();
                },
                function (message) {
                    EditApp.Utility.handleError(message);
                    EditApp.Grid.hideLoadMask();
                }
            );

            // Hook up all the override links.
            EditApp.setupUiEvents(null, $('#aspnetForm div.mainAppEditContent table#appEditContent table#appMainProp'));
        }, 1000);
    }


    // public structures
    EditApp.ComponentsThatLoadingNow = [];
    EditApp.ComponentsThatTestingNow = [];
    EditApp.ScriptMonitorCredentials = {};

    EditApp.SettingLevel = SETTING_LEVEL;
    EditApp.SettingValueType = SETTING_VALUETYPE;

    EditApp.init = initModule;

    // public methods
    EditApp.getValue = getSettingValue;
    EditApp.parseValue = parseSettingValue;
    EditApp.isVisible = isVisible;
    EditApp.$enable = $enable;

    EditApp.getBasename = getBasename;
    EditApp.getEditCol = getEditCol;
    EditApp.getEditRow = getEditRow;
    EditApp.getEditor = getEditor;
    EditApp.getViewer = getViewer;
    EditApp.getOverrideLink = getOverrideLink;

    EditApp.deleteComponentValidator = deleteComponentValidator;
    EditApp.LoadingInProgressNow = componentsIsLoading;
    EditApp.OverrideTemplateLabel = "@{R=APM.Strings;K=APMWEBJS_OverrideTemplate;E=js}";
    EditApp.InheritFromTemplateLabel = "@{R=APM.Strings;K=APMWEBJS_InheritFromTemplate;E=js}";
    EditApp.isComponentTestingNow = isComponentTestingNow;
    EditApp.initUiState = initUiState;
    EditApp.initDataTransform = initDataTransform;
    EditApp.populateRetrieveValue = populateRetrieveValue;
    EditApp.populateDynamicRetrieveValue = populateDynamicRetrieveValue;
    EditApp.initDynamicColumnsModel = initDynamicColumnsModel;

    EditApp.initThreshold = EditApp.Thresholds && EditApp.Thresholds.initThreshold;
    EditApp.updateThresholdModelFromUi = EditApp.Thresholds && EditApp.Thresholds.updateThresholdModelFromUi;
    EditApp.initMultiUiThreshold = EditApp.Thresholds && EditApp.Thresholds.initMultiUiThreshold;
    EditApp.updateMultiThresholdFromUi = EditApp.Thresholds && EditApp.Thresholds.updateMultiThresholdFromUi;

    EditApp.showThresholdMoreInfoDialog = EditApp.ThresholdsBaseline && EditApp.ThresholdsBaseline.showThresholdMoreInfoDialog;
    EditApp.redrawPlotBandsInCharts = EditApp.ThresholdsBaseline && EditApp.ThresholdsBaseline.redrawPlotBandsInCharts;
    EditApp.redrawXAxis = EditApp.ThresholdsBaseline && EditApp.ThresholdsBaseline.redrawXAxis;

    EditApp.updateModelFromUi = updateModelFromUi;
    EditApp.updateDataTransformModelFromUi = updateDataTransformModelFromUi;
    EditApp.updateDynamicColumnsModelFromUi = updateDynamicColumnsModelFromUi;

    EditApp.initMultiUiState = initMultiUiState;
    EditApp.initMultiDataTransform = initMultiDataTransform;
    EditApp.updateMultiModelFromUi = updateMultiModelFromUi;
    EditApp.updateMultiDataTransformModelFromUi = updateMultiDataTransformModelFromUi;

    EditApp.updateCommonTestSettings = updateCommonTestSettings;

    EditApp.getModel = getModel;
    EditApp.setModel = setModel;

    EditApp.setupUiEvents = setupUiEvents;

    EditApp.addCredentialSets = addCredentialSets;
    EditApp.addSMTPCredentialSets = addSMTPCredentialSets;
    EditApp.addCertificateSets = addCertificateSets;

    EditApp.validateEditApp = validateEditApp;
    EditApp.validateEditComponent = validateEditComponent;

    EditApp.save = save;
    EditApp.saveAndBrowse = saveAndBrowse;
    EditApp.applyChanges = applyChanges;
    EditApp.cancel = cancel;
});
