/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
/// <reference path="../../../APM.js"/>
/// <reference path="Edit.js"/>
APMjs.initGlobal('SW.APM.EditApp.Editors', function (Editors, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        SF = APMjs.format,
        SW = APMjs.assertGlobal('SW'),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        MSG = {
            NotFoundNotRunning: '@{R=APM.Strings;K=APMWEBJS_NotFoundNotRunning;E=js}',
            NotFoundDown: '@{R=APM.Strings;K=APMWEBJS_NotFoundDown;E=js}',
            LoadingSettings: '@{R=APM.Strings;K=APMWEBJS_LoadingComponentEditorSettings;E=js}'
        },
        TYPE = {
            template: 0,
            settings: 1,
            threshold: 2,
            transform: 3
        },
        NameCredential = 'CredentialSetId',
        SETT_NAME = {
            CanBeDisabled: '_BB_CanBeDisabled',
            CanBeDisabledOnAppItemLevel: '_BB_CanBeDisabledOnAppItemLevel',
            CanHaveMinFreq: '_BB_CanHaveMinFrequency'
        },
        EditorSettings = {},
        isVisible = EditApp.isVisible,
        console = APMjs.console;


    // error handling helpers

    function tryit(fn, args) {
        try {
            fn.apply(null, args);
        } catch (error) {
            console.error(error);
        }
    }


    // helpers

    function getEl(name, cmpId, suffix) {
        return $(SF('#cmp{0}{1}{2}', name, cmpId, suffix || ''));
    }

    function toggleSubEditor(jEl, visible) {
        function setRequired() {
            var jSub = $(this);
            if (isVisible(jSub) && isVisible(jSub.parents())) {
                jSub.addClass('required');
            }
        }
        if (visible) {
            jEl.show().find('*[apmsettctr=true]').each(setRequired);
        } else {
            jEl.hide().find('*[apmsettctr]').removeClass('required');
        }
    }

    function getMessage(key) {
        if (key === 'process-not-found-notrunning') {
            return MSG.NotFoundNotRunning;
        }
        if (key === 'process-not-found-down') {
            return MSG.NotFoundDown;
        }
        throw SF("key '{0}' not found", key);
    }

    function onNotRunningStatusChanged() {
        var jEl, jEditor, jHelp, status;

        jEl = $(this);
        jEditor = jEl.parent();
        jHelp = jEditor.parents('tr.dropdownEditorContainer').next('tr.helpContainer').find('.helpText');

        if (isVisible(jEditor) && isVisible(jEditor.parents())) {
            status = jEl.val();
        } else {
            status = jEditor.next('.viewer').text();
        }

        if (status === 'Down') {
            jHelp.html(getMessage('process-not-found-down'));
        } else {
            jHelp.html(getMessage('process-not-found-notrunning'));
        }
    }

    function parseBool(value) {
        if (APMjs.isStr(value)) {
            value = value.toLowerCase();
        }
        return JSON.parse(value);
    }

    function parseId(id) {
        id = (APMjs.isStr(id) && id.length) ? JSON.parse(id) : null;
        return APMjs.isNum(id) ? id : -1;
    }

    function parseIdList(idList) {
        return JSON.parse(idList);
    }

    function parseList(list) {
        return JSON.parse(list);
    }


    // *** init editor base helpers

    function hideEnableSettingIfNeeded(component, jContainer) {
        var applicationItemId, sett;

        if (!APMjs.isSet(EditorSettings.appItemId)) {
            return;
        }

        if (!EditorSettings.isBlackBoxEdit) {
            return;
        }

        applicationItemId = EditorSettings.appItemId;
        sett = component && component.Settings;

        if (sett) {
            if (applicationItemId !== 0) {
                if (EditApp.parseValue(sett[SETT_NAME.CanBeDisabledOnAppItemLevel], false) === false) {
                    jContainer.find('.EnableComponent').hide();
                }
            } else {
                if (EditApp.parseValue(sett[SETT_NAME.CanBeDisabled], false) === false) {
                    jContainer.find('.EnableComponent').hide();
                }
            }
            if (EditApp.parseValue(sett[SETT_NAME.CanHaveMinFreq], false) === false) {
                jContainer.find('.PollingFreqComponent').hide();
            }
            if (EditorSettings.isAppItemEdit || EditApp.parseValue(sett.CanSetRowCount, false) === false) {
                jContainer.find('.RowCount').hide();
            }
            if (EditorSettings.isAppItemEdit || EditApp.parseValue(sett.CanSetIntParameter1, false) === false) {
                jContainer.find('.IntParameter1').hide();
            }
        }
    }

    function initEditorBase(isBlackBoxEdit, isAppItemEdit, appItemId) {
        EditorSettings.isBlackBoxEdit = parseBool(isBlackBoxEdit);
        EditorSettings.isAppItemEdit = parseBool(isAppItemEdit);
        EditorSettings.appItemId = parseId(appItemId);
    }


    // *** chunk helpers

    function getInput(cmpId, item) {
        var id = item.id || item.name;
        if (!APMjs.isStr(id) || id === '') {
            throw SF('id value `{0}` not valid', id);
        }
        if (!APMjs.isNum(cmpId)) {
            throw SF('cmpId value `{0}` not valid', cmpId);
        }
        return getEl(id, cmpId, item.suffix);
    }

    function getName(item) {
        var name = item.name || item.id;
        if (!APMjs.isStr(name) || name === '') {
            throw SF('name `{0}` not valid', name);
        }
        return name;
    }

    function getData(opts, item) {
        var data = opts.cmp;
        switch (item.type) {
        case TYPE.template:
            return data;
        case TYPE.settings:
            return data && data.Settings;
        case TYPE.threshold:
            return data && data.Thresholds;
        case TYPE.transform:
            return data;
        }
        return null;
    }

    function getData2(opts, item) {
        var data = opts.template;
        switch (item.type) {
        case TYPE.template:
            return data;
        case TYPE.settings:
            return data && data.Settings;
        case TYPE.threshold:
            return data && data.Thresholds;
        case TYPE.transform:
            return data;
        }
        return null;
    }

    function initLoadingStart(cmpId) {
        var jContainer, jParent, xLoading;

        EditApp.ComponentsThatLoadingNow.push(cmpId);
        SW.APM.EventManager.fire('onComponentLoading');

        jContainer = $('#componentEditorForm' + cmpId);
        if (jContainer.length) {
            jParent = ($.browser.msie) ? jContainer.parent().parent() : jContainer.parent();
            xLoading = new Ext.LoadMask(jParent[0], { msg: MSG.LoadingSettings });
            xLoading.show();
        }

        return xLoading;
    }

    function initLoadingStop(cmpId, xLoading) {
        var index;

        if (xLoading) {
            xLoading.hide();
        }
        index = EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
        EditApp.ComponentsThatLoadingNow.splice(index, 1);
        SW.APM.EventManager.fire('onComponentLoading');
    }


    // handlers

    function ensureHandlers(opts) {
        opts.handlers = (APMjs.isArr(opts.handlers) && opts.handlers) || [];
    }

    function addStatusHandler(opts) {
        if (APMjs.isStr(opts.status)) {
            ensureHandlers(opts);
            opts.handlers.push({
                id: opts.status,
                event: 'change',
                handler: Editors.onNotRunningStatusChanged,
                trigger: true
            });
        }
    }

    function metaInitHandlers(opts) {
        APMjs.linqEach(opts.handlers, function (info) {
            var eventName = info.event || 'change', jEl;
            jEl = getEl(info.id, opts.cmpId, info.idPost)
                .unbind(eventName)
                .bind(eventName, info.handler);
            if (info.trigger) {
                APMjs.launch(function () {
                    jEl.trigger(eventName);
                });
            }
        });
    }

    function changeWinRmRowVisibility(fetchingMethodElement, indexOfDropdown) {
        const jTable = fetchingMethodElement.parents('tbody').first();
        const jTableDropdowns = jTable.find('tr.dropdownEditorContainer');
        const jDropdown = jTableDropdowns.eq(indexOfDropdown);

        const value = fetchingMethodElement.val().toLowerCase();
        if (value === 'winrm' || value === 'wmi') {
            jDropdown.show();
        } else {
            jDropdown.hide();
        }
    }

    // batch single

    function metaInitSingle(opts, item) {
        var jInput, name, data, data2;

        jInput = getInput(opts.cmpId, item);
        name = getName(item);
        data = getData(opts, item);
        data2 = getData2(opts, item);

        if (!jInput.length) {
            APMjs.console.warn('missing input ' + jInput.selector);
            return;
        }

        if (name === NameCredential || item.credential === true) {
            EditApp.addCredentialSets(jInput, function () {
                EditApp.initUiState(jInput, name, data, data2, item.readonly);
            });
        } else if (String(item.credential).toLowerCase() === 'smtp') {
            EditApp.addSMTPCredentialSets(jInput, function () {
                EditApp.initUiState(jInput, name, data, data2, item.readonly);
            });
        } else {
            switch (item.type) {
            case TYPE.threshold:
                EditApp.Thresholds.initThreshold(jInput, name, data, data2, item.readonly);
                break;
            case TYPE.transform:
                EditApp.initDataTransform(jInput, data, data2, item.readonly);
                break;
            default:
                EditApp.initUiState(jInput, name, data, data2, item.readonly);
            }
        }

        if (APMjs.isFn(item.callback)) {
            item.callback(name, data, data2);
        }
    }

    function metaUpdateSingle(opts, item) {
        var jInput, name, data, propdata;

        jInput = getInput(opts.cmpId, item);
        name = getName(item);
        data = getData(opts, item);
        propdata = data && data[name];

        switch (item.type) {
        case TYPE.threshold:
            EditApp.Thresholds.updateThresholdModelFromUi(jInput, propdata, opts.isTemplate);
            break;
        case TYPE.transform:
            EditApp.updateDataTransformModelFromUi(jInput, data, opts.isTemplate);
            break;
        default:
            EditApp.updateModelFromUi(jInput, propdata, opts.isTemplate);
        }
    }

    function metaInitDynamic(opts) {
        if (opts.colDynamic) {
            EditApp.initDynamicColumnsModel(opts.cmp, opts.template, opts.isTemplate, $('#' + opts.colDynamic + opts.cmpId));
        }
    }

    function metaUpdateDynamic(opts) {
        if (opts.colDynamic) {
            EditApp.updateDynamicColumnsModelFromUi(opts.cmp, opts.template, opts.isTemplate);
        }
    }

    function metaInitSingleAll(opts, items) {
        var xLoading = initLoadingStart(opts.cmpId);
        APMjs.linqEachAsync(items || opts.props, function (item) {
            tryit(metaInitSingle, [opts, item]);
        }, function () {
            addStatusHandler(opts);
            metaInitHandlers(opts);
            hideEnableSettingIfNeeded(opts.cmp, $('#componentEditorForm' + opts.cmpId));
            metaInitDynamic(opts);
            if (APMjs.isFn(opts.postInit)) {
                opts.postInit();
            }
            initLoadingStop(opts.cmpId, xLoading);
        });
    }

    function metaUpdateSingleAll(opts, items) {
        APMjs.linqEach(items || opts.props, function (item) {
            tryit(metaUpdateSingle, [opts, item]);
        });
        metaUpdateDynamic(opts);
    }


    // batch multi

    function metaInitMulti(opts, item) {
        var jInput, name, defSett;

        jInput = getInput(opts.cmpId, item);
        name = getName(item);
        defSett = opts.cmp && opts.cmp.DefinitionSettings;

        if (name === NameCredential || item.credential === true) {
            EditApp.addCredentialSets(jInput, function () {
                EditApp.initMultiUiState(jInput, defSett, name);
            });
        } else if (String(item.credential).toLowerCase() === 'smtp') {
            EditApp.addSMTPCredentialSets(jInput, function () {
                EditApp.initMultiUiState(jInput, defSett, name);
            });
        } else {
            switch (item.type) {
            case TYPE.threshold:
                EditApp.initMultiUiThreshold(jInput, defSett, name);
                break;
            case TYPE.transform:
                EditApp.initMultiDataTransform(jInput, defSett);
                break;
            default:
                EditApp.initMultiUiState(jInput, defSett, name);
            }
        }

        if (APMjs.isFn(item.callback)) {
            item.callback(name, null, defSett);
        }
    }

    function metaUpdateMulti(opts, item) {
        var jInput, name, data;

        jInput = getInput(opts.cmpId, item);
        name = getName(item);
        data = getData(opts, item);

        switch (item.type) {
        case TYPE.threshold:
            EditApp.Thresholds.updateMultiThresholdFromUi(jInput, data, name, opts.cmpIds);
            break;
        case TYPE.transform:
            EditApp.updateMultiDataTransformModelFromUi(jInput, data, opts.cmpIds);
            break;
        default:
            EditApp.updateMultiModelFromUi(jInput, data, name, opts.cmpIds);
        }
    }

    function metaInitMultiAll(opts, items) {
        var xLoading = initLoadingStart(opts.cmpId);
        APMjs.linqEachAsync(items || opts.props, function (item) {
            if (item.type !== TYPE.template) {
                tryit(metaInitMulti, [opts, item]);
            }
        }, function () {
            addStatusHandler(opts);
            metaInitHandlers(opts);
            if (APMjs.isFn(opts.postInit)) {
                opts.postInit();
            }
            initLoadingStop(opts.cmpId, xLoading);
        });
    }

    function metaUpdateMultiAll(opts, items) {
        APMjs.linqEach(items || opts.props, function (item) {
            if (item.type !== TYPE.template) {
                tryit(metaUpdateMulti, [opts, item]);
            }
        });
    }


    /**
     * get property type or undefined
     * @param {string} key String denoting property type (one of values defined in TYPE)
     * @return {string} Valid property type or undefined
     */
    function getPropType(key) {
        var result;
        APMjs.linqAny(TYPE, function (t, tkey) {
            if (key === t || key === tkey) {
                result = t;
                return true;
            }
        });
        return result;
    }

    /**
     * get property description structure
     * @param {object or string} prop Object containing property info or string expressing property name
     * @param {string} type Type of property (one of values defined in TYPE)
     * @return {Array.<object>} List of property info objects for use in editpage framework
     */
    function getProp(prop, type) {
        // if string passed create property description with given name
        if (APMjs.isStr(prop)) {
            prop = { name: prop };
        }
        // set property type to given value
        prop.type = type;
        if (type === TYPE.threshold) {
            prop.suffix = prop.suffix || 'Operator';
        }
        return prop;
    }

    function initPropMap(props) {
        props.map = {};
        APMjs.linqEach(props, function (item) {
            props.map[item.name] = item;
            if (APMjs.isStr(item.id) && item.id.length) {
                props.map[item.id] = item;
            }
        });
    }

    /**
     * initialize property info for given fields
     * @param {object} propLists Map of arrays of property info objects or property names
     * @return {Array.<object>} List of property info objects for use in editpage framework
     */
    function initProps(propLists) {
        var props = [];
        APMjs.linqEach(propLists, function (list, key) {
            var propType = getPropType(key);
            if (APMjs.isSet(propType)) {
                APMjs.linqEach(list, function (prop) {
                    props.push(getProp(prop, propType));
                });
            }
        });
        initPropMap(props);
        return props;
    }

    /**
     * initialize property info for dynamic threshold fields
     * @param {Array.<string>} thrKeys List of threshold keys
     * @return {object} List of property info objects for use in editpage framework
     */
    function initPropsThr(thrKeys) {
        var propsThr = [];
        if (APMjs.isArr(thrKeys)) {
            propsThr = APMjs.linqSelect(thrKeys, function (key) {
                return { id: 'Thrs' + key, name: key };
            });
            propsThr = initProps({
                threshold: propsThr
            });
        }
        return propsThr;
    }

    function cloneProps(props) {
        var copy = APMjs.linqSelect(props, function (prop) {
            return $.extend({}, prop);
        });
        initPropMap(copy);
        return copy;
    }


    // publish

    Editors.parseBool = parseBool;
    Editors.parseId = parseId;
    Editors.parseIdList = parseIdList;
    Editors.parseList = parseList;

    Editors.getEl = getEl;
    Editors.getInput = getInput;
    Editors.toggleSubEditor = toggleSubEditor;
    Editors.getMessage = getMessage;
    Editors.onNotRunningStatusChanged = onNotRunningStatusChanged;

    Editors.initEditorBase = initEditorBase;
    Editors.hideEnableSettingIfNeeded = hideEnableSettingIfNeeded;

    Editors.PropType = TYPE;
    Editors.getData = getData;
    Editors.initProps = initProps;
    Editors.initPropsThr = initPropsThr;
    Editors.cloneProps = cloneProps;
    Editors.chunkInitSingle = metaInitSingleAll;
    Editors.chunkUpdateSingle = metaUpdateSingleAll;
    Editors.chunkInitMulti = metaInitMultiAll;
    Editors.chunkUpdateMulti = metaUpdateMultiAll;

    Editors.changeWinRmRowVisibility = changeWinRmRowVisibility;
});
