/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
/// <reference path="../../../APM.js"/>
/// <reference path="Edit.js"/>
APMjs.initGlobal('SW.APM.EditApp.AppModel', function (AppModel, APMjs) {
    'use strict';

    var ORION = APMjs.assertGlobal('ORION'),
        Ext = APMjs.assertExt(),
        $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp');

    function getQueryParams() {
        return Ext.urlDecode(window.location.search.slice(1));
    }

    function loadFromServer(onSuccess, onError) {
        var app = this,
            queryParams = getQueryParams();

        function handleResults(result) {
            result.NonEscapedName = result.Name;
            result.Name = _.escape(result.Name);
            var model = $.extend(true, result, AppModel);
            onSuccess(model);
        }

        ORION.callWebService(
            app.Config.webServiceUrl,
            app.Config.webServiceLoadMethod,
            {
                id: queryParams.id || 0,
                applicationItemId: queryParams.applicationItemId || 0
            },
            handleResults,
            onError
        );
    }

    function saveToServer(onSuccess, onError) {
        var app = this,
            queryParams = getQueryParams();

        ORION.callWebService(
            app.Config.webServiceUrl,
            app.Config.webServiceSaveMethod,
            {
                model: app,
                applicationItemId: queryParams.applicationItemId || 0
            },
            onSuccess,
            onError
        );
    }

    function loadToForm() {
        var app = this;
        app.loadAppSettingsToForm();
    }

    function saveFromForm() {
        var app = this;
        app.saveAppSettingsFromForm();
        APMjs.linqEach(app.Components, function (component) {
            if (APMjs.isFn(component.saveFromForm)) {
                component.saveFromForm(app);
            }
        });
    }

    function findIndexById(array, id) {
        var index = -1;

        APMjs.linqAny(array, function (item, i) {
            if (item.Id === id) {
                index = i;
                return true;
            }
        });

        return index;
    }

    function findById(array, id) {
        var result = null;
        if (APMjs.isStr(id)) {
            id = parseInt(id, 10);
        }
        APMjs.linqAny(array, function (item) {
            if (item.Id === id) {
                result = item;
                return true;
            }
        });
        return result;
    }

    function getComponent(id) {
        var app = this;
        return findById(app.Components, id);
    }

    function setMultiEditComponent(ids) {
        var app = this, meCmp = null;

        APMjs.linqAny(ids, function (id) {
            var cmp = app.getComponent(id);
            if (cmp) {
                meCmp = Ext.decode(Ext.encode(cmp));
                meCmp.Id = 0;
                return true;
            }
        });

        AppModel.multiEditComponent = meCmp;
    }

    function getMultiEditComponent() {
        return AppModel.multiEditComponent;
    }

    function getComponentTemplate(id) {
        var app = this;
        return (!app.haveTemplate() || !app.Template) ? null : findById(app.Template.Components, id);
    }

    function callCustomEditor(delegate, requstedAppType) {
        if (EditApp.CustomEditorsList && APMjs.isFn(delegate)) {
            APMjs.linqEach(EditApp.CustomEditorsList, function (pluginCls) {
                var pluginAppType = pluginCls.apptype, plugin;
                if ((requstedAppType === undefined || pluginAppType === undefined || pluginAppType === requstedAppType) && Ext.ComponentMgr.isPluginRegistered(pluginCls.ptype)) {
                    plugin = Ext.ComponentMgr.createPlugin({ ptype: pluginCls.ptype });
                    if (plugin) {
                        delegate(plugin);
                    }
                }
            });
        }
    }

    function setConfig(opts) {
        AppModel.Config = AppModel.Config || {};
        opts = opts || {};
        if (APMjs.isStr(opts.load)) {
            AppModel.Config.webServiceLoadMethod = opts.load;
        }
        if (APMjs.isStr(opts.save)) {
            AppModel.Config.webServiceSaveMethod = opts.save;
        }
        if (APMjs.isStr(opts.url)) {
            AppModel.Config.webServiceUrl = opts.url;
        }
        if (APMjs.isStr(opts.title)) {
            AppModel.pageTitleTemplate = opts.title;
        }
    }


    // initialize constants and defaults

    AppModel.SETTING_LEVEL = {
        Template: 1,
        Instance: 2
    };

    AppModel.multiEditComponent = null;
    AppModel.Components = AppModel.Components || [];

    AppModel.LoadFromServer = loadFromServer;
    AppModel.saveToServer = saveToServer;
    AppModel.loadAppSettingsToForm = APMjs.noop;
    AppModel.saveAppSettingsFromForm = APMjs.noop;
    AppModel.loadToForm = loadToForm;
    AppModel.saveFromForm = saveFromForm;
    AppModel.getComponent = getComponent;
    AppModel.setMultiEditComponent = setMultiEditComponent;
    AppModel.getMultiEditComponent = getMultiEditComponent;
    AppModel.getComponentTemplate = getComponentTemplate;
    AppModel.callCustomEditor = callCustomEditor;
    AppModel.setConfig = setConfig;

    AppModel.Components.findIndexById = function (id) {
        return findIndexById(this, id);
    };

    AppModel.haveTemplate = function () {
        return APMjs.isDef(this.Template);
    };


    setConfig({
        url: '/Orion/APM/Admin/Edit/Edit.asmx',
        load: '',
        save: ''
    });

});
