/*jslint browser: true, indent: 4, nomen: true*/
/*global APMjs: false*/
/// <reference path="ComponentGrid.js" />

APMjs.initGlobal('SW.APM.EditApp.Grid.Handlers', function (Handlers, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Ext = APMjs.assertExt(),
        EventManager = APMjs.assertGlobal('SW.APM.EventManager'),
        SF = APMjs.format,
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        Grid = APMjs.assertGlobal('SW.APM.EditApp.Grid'),
        Renderers = APMjs.assertGlobal('SW.APM.EditApp.Grid.Renderers'),
        APM = APMjs.assertGlobal('SW.APM'),
        ORION = APMjs.assertGlobal('ORION'),
        isDemoMode = APMjs.isDemoMode,
        demoModeMessage = APMjs.demoModeMessage,
        resources = {
            RetrievingCurrentValue: 'Retrieving...'
        },
        // global module state
        State = {
            NextNewComponentId: -1,
            RetriveComponents: [],
            RetriveDynamicComponents: []
        };


    function isFindProcessWizard() {
        return EditApp.AppModel.editMode === 'FindProcessesWizard';
    }

    function hideLoadMask() {
        State.CmpCrid.hideLoadMask();
    }

    function triggerGridUpdateEvent() {
        $(State.Grid.body.dom).trigger('update');
    }

    function saveFromForm(componentId) {
        var model = EditApp.getModel(),
            component = model.getComponent(componentId) || {};

        if (APMjs.isFn(component.saveFromForm)) {
            component.saveFromForm(model);
        }
    }

    function hideErrorSummary() {
        $('#formErrors').hide();
    }

    function hasTemplateEnableComponents() {
        return APMjs.linqAny(State.Store.data.items, function (item) {
            return String(item.json.IsDisabled.Value) === 'false';
        });
    }

    function isRetrieveComponent(retriveComponentsArr, id) {
        id = Number(id);
        return APMjs.linqAny(retriveComponentsArr, function (item) {
            return Number(item) === id;
        });
    }

    function initRetrieveUiInternal(res) {
        if (!isRetrieveComponent(State.RetriveComponents, res.Id)) {
            return;
        }

        var multiStatisticValuesCount = 0;
        APMjs.linqEach(res.MultiStatisticValues, function (value) {
            if (APMjs.isSet(value)) {
                multiStatisticValuesCount += 1;
            }
        });

        if (multiStatisticValuesCount > 0) {
            State.RetriveDynamicComponents = EditApp.populateDynamicRetrieveValue(res, State.RetriveDynamicComponents);
        } else {
            EditApp.populateRetrieveValue(res);
        }

        State.RetriveComponents.length = 0;
    }

    function singleComponentTest(id, columnId, columnName) {

        id = Number(id);

        var model = EditApp.getModel(),
            defaultTestNode = null,
            componentToTest = null,
            hasComponentTestNode,
            componentIndex;

        function submitTestsToStart() {
            model.saveAppSettingsFromForm();

            var testsToStart = [], transformationBack, results, idInput;

            idInput = SF('#cmpConvertValue{0}InputTextbox', id);

            if (componentToTest) {
                saveFromForm(componentToTest.id);

                results = componentToTest.json.TestResults;
                results.messages = null;

                if (!APMjs.isSet(results.nodeName)) {
                    results.nodeId = defaultTestNode.id;
                    results.nodeName = defaultTestNode.name;
                    results.nodeStatus = defaultTestNode.status;
                }

                if (String(componentToTest.data.IsDisabled.Value) === 'true') {
                    results.testStatus = 'skipped';
                } else {
                    results.testStatus = 'inprogress';
                    testsToStart.push(componentToTest.json);
                    State.RetriveComponents.push(componentToTest.id);
                    if (EditApp.ComponentsThatTestingNow.indexOf(componentToTest.id) === -1) {
                        EditApp.ComponentsThatTestingNow.push(componentToTest.id);
                        EventManager.fire('onComponentTesting');
                    }
                }

                $(idInput).val(resources.RetrievingCurrentValue).attr('disabled', true);
                componentToTest.set('TestResults', results);
            }

            if (testsToStart.length) {
                if (testsToStart[0].DynamicColumnSettings.length === 0) {
                    transformationBack = testsToStart[0].Settings.TransformExpression.Value;
                    testsToStart[0].Settings.TransformExpression.Value = '';
                } else {
                    APMjs.linqEach(testsToStart[0].DynamicColumnSettings, function (dcs) {
                        dcs.DataTransform.TransformExpression = '';
                    });
                }
            }

            Grid.TestManager.beginTests(testsToStart, model, function () {
                $(idInput).attr('disabled', false);
            });

            if (testsToStart[0]) {
                if (!testsToStart[0].DynamicColumnSettings.length) {
                    componentIndex = model.Components.findIndexById(testsToStart[0].Id);
                    model.Components[componentIndex].Settings.TransformExpression.Value = transformationBack;
                }
            } else {
                $(idInput).val('');
            }
        }

        if (isDemoMode()) {
            return demoModeMessage();
        }

        if (!EditApp.validateEditApp()) {
            return false;
        }

        State.Store.each(function (record) {
            if (record.id === id) {
                componentToTest = record;
            }
        });

        if (APMjs.isSet(columnId) && APMjs.isSet(columnName)) {
            State.RetriveDynamicComponents.push({
                id: id,
                column: columnId,
                columnName: columnName
            });
        }

        hasComponentTestNode = componentToTest && APMjs.isSet(componentToTest.json.TestResults.nodeName);

        if (!hasComponentTestNode) {
            APM.NodesDialog.show({
                onSuccess: function (node) {
                    defaultTestNode = node;
                    submitTestsToStart();
                }
            });
        } else {
            submitTestsToStart();
        }

        return false;
    }

    function createAndAddComponents(definitionIds) {
        var model = EditApp.getModel();

        if (isFindProcessWizard()) {
            State.NextNewComponentId -= model.Components.length;
        }

        function expandNewRows(gridView, firstRow, lastRow) {
            function delayed() {
                var i;
                for (i = firstRow; i <= lastRow; i += 1) {
                    State.RowExpander.expandRow(i);
                }
                gridView.focusRow(firstRow);
                gridView.un('rowsinserted', expandNewRows);
                Renderers.updateRowsAfterOrdering();
            }
            APMjs.launch(delayed);
        }

        function createAndAddComponentsCompleted(results) {
            State.NextNewComponentId -= results.length;
            var targetServer = EditApp.AppModel.targetNode;
            Ext.each(results, function (result) {

                if (isFindProcessWizard()) {
                    result.TestResults = {};
                    result.TestResults.nodeId = targetServer.Id;
                    result.TestResults.nodeName = targetServer.Name;
                    result.TestResults.nodeStatus = targetServer.Status;
                    result.TestResults.testStatus = null;
                    result.TestResults.messages = '';

                    var wizardNodeCredentials = EditApp.AppModel.wizardNodeCredentials;
                    if (wizardNodeCredentials.Id) {
                        result.CredentialSetId.Value = wizardNodeCredentials.Id;
                    }
                }
                model.Components.push(result);
            });

            State.Grid.getView().on('rowsinserted', expandNewRows);
            triggerGridUpdateEvent();
            State.Store.loadData({ Components: results }, true);
        }

        ORION.callWebService(
            '/Orion/APM/Admin/Edit/Edit.asmx',
            'CreateComponents',
            {
                definitionIds: definitionIds,
                isTemplate: model.IsTemplate,
                nextNewComponentId: State.NextNewComponentId
            },
            createAndAddComponentsCompleted,
            EditApp.Utility.handleError
        );
    }


    function init(cmpCrid, grid, rowExpander) {
        State.CmpCrid = cmpCrid;
        State.Grid = grid;
        State.Selection = grid.getSelectionModel();
        State.Store = grid.getStore();
        State.RowExpander = rowExpander;
    }


    function onBrowseAddClick() {
        if (isDemoMode()) {
            return demoModeMessage();
        }
        EditApp.saveAndBrowse();
        return true;
    }

    function onManuallyAddClick() {
        var hasEnableComponents;

        if (isDemoMode()) {
            return demoModeMessage();
        }

        hasEnableComponents = hasTemplateEnableComponents();

        APM.AddComponentDefinition.show({
            Callback: function (selectedDefinitions) {
                var definitionIds = [];
                APMjs.linqEach(selectedDefinitions, function (definition) {
                    var i;
                    for (i = 0; i < definition.Count; i += 1) {
                        definitionIds.push(definition.ID);
                    }
                });
                createAndAddComponents(definitionIds);
                if (!hasEnableComponents) {
                    hideErrorSummary();
                }
            }
        });

        return true;
    }

    function onMultiEditClick() {

        function doit() {
            var type = null, ids = [];

            State.Selection.each(function (record) {
                saveFromForm(record.id);
                type = record.json.ComponentType;
                ids.push(record.json.Id);
            });

            Grid.hideLoadMask();
            EditApp.MultiEditDialog.show(type, ids, EditApp.getModel());
        }

        Grid.showLoadMask('Applying Changes. Please wait...');
        APMjs.launch(doit, 100);

        return false;
    }

    function onFindClick() {
        if (isDemoMode()) {
            return demoModeMessage();
        }
        window.alert('TODO: Not implemented');
        return false;
    }


    function updateCredentialsForSelectedComponents(credential) {
        var records = State.Selection.getSelections(),
            model = EditApp.getModel(),
            level = model.DefaultSettingLevel;

        function updateRecord(record) {
            saveFromForm(record.id);

            record.json.CredentialSetId.Value = credential.id;
            record.json.CredentialSetId.SettingLevel = level;
            record.set('CredentialSetId', record.json.CredentialSetId);

            EventManager.fire('onToolbarCredentialsClick', record);
        }

        if (!model.Credentials) {
            model.Credentials = [];
        }

        if (credential.isNew) {
            model.Credentials.push({
                Id: credential.id,
                Name: credential.name,
                UserName: credential.user,
                Password: '********',
                Broken: false
            });
        }

        State.CmpCrid.showLoadMask('Applying Credentials. Please wait...');

        EditApp.Utility.chunk(
            records,
            updateRecord,
            hideLoadMask
        );

        return false;
    }

    function onCredentialsClick() {

        if (isDemoMode()) {
            return demoModeMessage();
        }

        var model = EditApp.getModel();
        APM.CredentialsDialog.show({
            addNoneCred: true,
            addInheritedWindows: model.NodeSubType === 'WMI',
            onSuccess: updateCredentialsForSelectedComponents
        });

        return false;
    }

    function onTestClick() {

        var records, testsToStart, model;

        function beginTests(args) {
            Grid.TestManager.beginTests(args[0], args[1]);
            hideLoadMask();
        }

        function submitRecordForTest(record, args) {
            var results, haveTestNode;

            saveFromForm(record.id);

            if (!EditApp.validateEditComponent(record.id)) {
                return;
            }

            results = record.json.TestResults;
            haveTestNode = APMjs.isSet(results.nodeName);

            if (!haveTestNode) {
                results.testStatus = 'skippedtestnodenotspecified';
            } else {
                results.messages = null;
                if (String(record.data.IsDisabled.Value) === 'true') {
                    results.testStatus = 'skipped';
                } else {
                    EditApp.ComponentsThatTestingNow.push(record.id);
                    EventManager.fire('onComponentTesting');

                    results.testStatus = 'inprogress';
                    args[0].push(record.json);
                }
            }

            record.set('TestResults', results);
        }

        if (isDemoMode()) {
            return demoModeMessage();
        }

        records = State.Selection.getSelections();
        testsToStart = [];

        model = EditApp.getModel();
        model.saveAppSettingsFromForm();

        State.CmpCrid.showLoadMask('Starting Test operation. Please wait...');

        EditApp.Utility.chunk(records, submitRecordForTest, beginTests, null, [testsToStart, model]);

        return false;
    }

    function onSetNodeClick() {
        if (isDemoMode()) {
            return demoModeMessage();
        }

        function updateTestNodeForSelectedComponents(node) {
            var records = State.Selection.getSelections();

            function updateRecord(record) {
                var results;

                saveFromForm(record.id);

                results = record.json.TestResults;
                results.nodeId = node.id;
                results.nodeName = node.name;
                results.nodeStatus = node.status;
                results.testStatus = null;
                results.messages = '';

                record.set('TestResults', results);
            }

            State.CmpCrid.showLoadMask('Setting Test Node. Please wait...');

            EditApp.Utility.chunk(
                records,
                updateRecord,
                hideLoadMask
            );

            return false;
        }

        APM.NodesDialog.show({
            onSuccess: updateTestNodeForSelectedComponents
        });

        return false;
    }

    function onEnableClick(mixed) {

        var records, level;

        function updateRecord(record) {
            if (String(record.json.IsDisabled.Value) !== 'true') {
                return;
            }

            saveFromForm(record.id);

            record.json.IsDisabled.Value = false;
            record.json.IsDisabled.SettingLevel = level;
            record.set('IsDisabled', record.json.IsDisabled);

            EventManager.fire('onToolbarEnableClick', record);
        }

        if (isDemoMode()) {
            return demoModeMessage();
        }

        records = State.Selection.getSelections();
        level = EditApp.getModel().DefaultSettingLevel;

        if (mixed) {
            if (mixed.recordToEnable) {
                records = [mixed.recordToEnable];
            }
            if (mixed.settingLevel) {
                level = mixed.settingLevel;
            }
        }

        State.CmpCrid.showLoadMask('Enabling Components. Please wait...');
        EditApp.Utility.chunk(records, updateRecord, hideLoadMask);

        return false;
    }

    function onDisableClick(mixed) {
        var records, level;

        if (isDemoMode()) {
            return demoModeMessage();
        }

        records = State.Selection.getSelections();
        level = EditApp.getModel().DefaultSettingLevel;

        if (mixed) {
            if (mixed.recordToDisable) {
                records = [mixed.recordToDisable];
            }
            if (mixed.settingLevel) {
                level = mixed.settingLevel;
            }
        }

        function updateRecord(record) {

            function setDisabled() {
                saveFromForm(record.id);
                record.json.IsDisabled.Value = true;
                record.json.IsDisabled.SettingLevel = level;
                record.set('IsDisabled', record.json.IsDisabled);
            }

            if (String(record.json.IsDisabled.Value) === 'false') {
                setDisabled();
                EventManager.fire('onToolbarDisableClick', record);
                if (record.json.IsDisabled.Value === false) {
                    setDisabled();
                }
            }
        }

        State.CmpCrid.showLoadMask('Disabling Components. Please wait...');
        EditApp.Utility.chunk(records, updateRecord, hideLoadMask);

        return false;
    }

    function onDeleteClick() {

        function callback(btn) {
            var model;

            if (btn !== 'yes') {
                return;
            }

            model = EditApp.getModel();

            State.Selection.each(function (record) {
                var index = model.Components.findIndexById(record.id);
                if (index >= 0) {
                    model.Components.splice(index, 1);
                    State.Store.remove([record]);
                    EditApp.deleteComponentValidator(record.id);
                }
            });

            EditApp.validateEditApp();
            Renderers.updateRowsAfterOrdering();
            triggerGridUpdateEvent();
        }

        if (isDemoMode()) {
            return demoModeMessage();
        }

        Ext.Msg.confirm(
            '@{R=APM.Strings;K=APMWEBJS_VB1_109;E=js}',
            '@{R=APM.Strings;K=APMWEBJS_VB1_110;E=js}',
            callback
        );

        return false;
    }

    function onRenameClick() {

        function callback(btn, newName) {
            if (btn !== 'ok' || !newName || newName.trim() === '') {
                return;
            }
            State.Selection.each(function (record) {
                saveFromForm(record.id);
                record.json.Name = newName;
                record.set('Name', record.json.Name);
            });
        }

        if (isDemoMode()) {
            return demoModeMessage();
        }

        Ext.Msg.prompt(
            '@{R=APM.Strings;K=APMWEBJS_ComponentName;E=js}',
            '@{R=APM.Strings;K=APMWEBJS_PleaseEnterComponentName;E=js}',
            callback,
            null,
            false,
            State.Selection.getSelected().json.Name
        );

        return false;
    }



    function setAllOrderingArrows(enable) {
        var model = EditApp.getModel(),
            loading = String(!enable),
            imgsrc = enable ? {
                up: 'up_arrow.png',
                dn: 'down_arrow.png'
            } : {
                up: 'up_arrow_disabled.png',
                dn: 'down_arrow_disabled.png'
            };

        APMjs.linqEach(model.Components, function (component) {
            $('#orderArrowUp' + component.Id).attr({
                src: '/Orion/APM/Images/' + imgsrc.up,
                apm_cmp_loading: loading
            });
            $('#orderArrowDown' + component.Id).attr({
                src: '/Orion/APM/Images/' + imgsrc.dn,
                apm_cmp_loading: loading
            });
        });
    }

    function onOverrideOrderClick() {
        var map, model, expandedRows;

        if (isDemoMode()) {
            return demoModeMessage();
        }

        map = State.Grid.getTopToolbar().items.map;
        map.OverrideOrder.setHandler(Grid.Handlers.onInheritOrderClick);

        model = EditApp.getModel();
        model.saveFromForm();
        model.ComponentOrderSettingLevel = 0;

        Renderers.setMaxComponentOrder(0);

        expandedRows = State.RowExpander.state;

        State.Store.loadData(model);
        State.Store.each(function (record) {
            if (APMjs.isSet(expandedRows[record.id]) || record.id < 0) {
                record.commit();
            }
        });

        Renderers.updateRowsAfterOrdering();
        setAllOrderingArrows(true);
        return false;
    }

    function onInheritOrderClick() {
        var map, model, max;

        if (isDemoMode()) {
            return demoModeMessage();
        }

        map = State.Grid.getTopToolbar().items.map;
        map.OverrideOrder.setHandler(Handlers.onOverrideOrderClick);

        model = EditApp.getModel();
        model.saveFromForm();

        model.ComponentOrderSettingLevel = 1;
        Renderers.setMaxComponentOrder(0);

        if (!model.Template) {
            return false;
        }

        max = 0;
        APMjs.linqEach(model.Components, function (component) {
            var template = APMjs.linqFirst(model.Template.Components, function (tmp) {
                return tmp.Id === component.TemplateId;
            });
            if (template) {
                component.ComponentOrder = template.ComponentOrder;
                if (max < component.ComponentOrder) {
                    max = component.ComponentOrder;
                }
            }
        });

        APMjs.linqEach(model.Components, function (component) {
            if (component.TemplateId === 0) {
                max = max + 1;
                component.ComponentOrder = max;
            }
        });

        Handlers.sortGridByComponentOrder(model);
        Handlers.loadGridByComponentOrder(model);
        setAllOrderingArrows(false);
        return false;
    }

    function compareComponentOrder(a, b) {
        return (a.ComponentOrder < b.ComponentOrder) ? -1 : ((a.ComponentOrder > b.ComponentOrder) ? 1 : 0);
    }

    function sortGridByComponentOrder(model) {
        model.Components.sort(compareComponentOrder);
        State.Store.loadData(model);
    }

    function loadGridByComponentOrder() {
        var expandedRows = State.RowExpander.state;

        State.Store.each(function (record) {
            if (APMjs.isSet(expandedRows[record.id]) || record.id < 0) {
                record.commit();
            }
        });

        Renderers.updateRowsAfterOrdering();
    }



    // publish methods

    Handlers.init = init;
    Handlers.singleComponentTest = singleComponentTest;
    Handlers.initRetrieveUi = initRetrieveUiInternal;

    Handlers.onBrowseAddClick = onBrowseAddClick;
    Handlers.onManuallyAddClick = onManuallyAddClick;
    Handlers.onMultiEditClick = onMultiEditClick;
    Handlers.onFindClick = onFindClick;
    Handlers.onCredentialsClick = onCredentialsClick;
    Handlers.onTestClick = onTestClick;
    Handlers.onSetNodeClick = onSetNodeClick;
    Handlers.onEnableClick = onEnableClick;
    Handlers.onDisableClick = onDisableClick;
    Handlers.onDeleteClick = onDeleteClick;
    Handlers.onRenameClick = onRenameClick;
    Handlers.onOverrideOrderClick = onOverrideOrderClick;
    Handlers.onInheritOrderClick = onInheritOrderClick;

    Handlers.sortGridByComponentOrder = sortGridByComponentOrder;
    Handlers.loadGridByComponentOrder = loadGridByComponentOrder;

});
