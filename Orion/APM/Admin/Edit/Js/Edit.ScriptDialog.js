﻿Ext.ns("SW.APM.Admin");
SW.APM.Admin.EditScriptDialog = function () {
    var SETTING_LEVEL = { Template: 1, Instance: 2 };
	var IT = "@{R=APM.Strings;K=APMWEBJS_InheritFromTemplate;E=js}"; 
	var OT = "@{R=APM.Strings;K=APMWEBJS_OverrideTemplate;E=js}"; 
	var NS = "@{R=APM.Strings;K=APMWEBJS_NotSpecified;E=js}".toUpperCase();
    var URL = "/Orion/APM/Admin/Edit/Edit.asmx";

    var mDlg = null, mGrid = null;
    var mCmpId = 0, mModel = null, mSuccessCallback = null;
    var mCmp = null, mTpl = null;
    var mRunningTest = null, mIsDirty = false, mRequest = null, mRequestTimeoutId = null;

    /*helper members*/
    function prepareCmpToEncode() {
        if (mCmp.DynamicColumnSettings) {
            $.each(mCmp.DynamicColumnSettings, function (i, column) {
                if (column.Type == 1 && column.Cells && column.Cells.length > 0) {
                    $.each(column.Cells, function (j, cell) {
                        if (cell.NumericData == null || isNaN(cell.NumericData)) {
                            cell.NumericData = 0;
                        }
                    });
                }
            });
        }
    }
    function prepareCmpToSave() {
        if (mCmp.DynamicColumnSettings) {
            $.each(mCmp.DynamicColumnSettings, function (i, column) {
                column.Cells = [];
            });
        }
    }

    function abort() {
        if (mRequest != null) { mRequest.abort(); mRequest = null; }
        if (mRequestTimeoutId) { clearTimeout(mRequestTimeoutId); }
        Ext.getCmp("bGetOutput").setText("Get script output");
    }
    function validateBody() {
        if (mCmp.Settings.ScriptBody.SettingLevel == SETTING_LEVEL.Instance || mTpl == null) {
            if ($.trim(mCmp.Settings.ScriptBody.Value).length == 0) {
                Ext.getCmp("taScriptBody").addClass('x-form-invalid');
                return false;
            }
        }
        return true;
    }

    function getShortText(message, size) {
        if (message.length > size) {
            return SF("{0}...", message.substr(0, size));
        }
        return message;
    }
    function getNodeStatusIcon() {
        return SF("/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={0}&status={1}&size=small", mModel.NodeID, mModel.NodeStatus);
    }
    function updateOutput(columns, messages, testProcessing) {
        var items = [], item = null;
        if (messages && messages.length > 0) {
            item = {
                xtype: "compositefield",
                items: [
					{ xtype: "displayfield", html: "<img src='/Orion/APM/Images/Admin/failed_16x16.gif' alt='ND' class='margin-bottom-m3'/> <i>Get Output Failed:</i>", width: 150, hideMode: "display" },
					{ xtype: "displayfield", value: messages.join("<br/>"), hideMode: "display" }
				]
            };
            items.push(item);
        } else {
            for (var i = 0; columns && i < columns.length; i++) {
                var column = columns[i], label = "", value = "";
                if (column.Type == 0/*String*/) {
                    label = SF("Message.{0}:", column.Name); value = "N/A";
                    if (column.Cells.length > 0) {
                        value = escapeHtml(column.Cells[0].StringData);
                    }
                } else {
                    label = SF("Statistic.{0}:", column.Name); value = "XXX.XX";
                    if (column.Cells.length > 0) {
                        value = escapeHtml(column.Cells[0].NumericData);
                    }
                }
                item = { xtype: "compositefield", items: [{ xtype: "displayfield", value: label, style: "word-wrap:break-word;", width: 260, autoHeight: true, hideMode: "display" }, { xtype: "displayfield", value: value, autoHeight: true, hideMode: "display"}] };
                items.push(item);
            }

            if (testProcessing) {
                item = { xtype: "displayfield", html: "<img src='/Orion/APM/Images/Admin/animated_loading_16x16.gif' alt='LPW' class='margin-bottom-m3'> <i>" + "@{R=APM.Strings;K=APMWEBJS_AK1_16;E=XML}" + "</i>", hideMode: "display" };
                items.push(item);
            }
            if (items.length == 0) {
                item = { xtype: "displayfield", html: "<img src='/Orion/APM/Images/Admin/failed_16x16.gif' alt='ND' class='margin-bottom-m3'/> <i>Not Defined</i>", hideMode: "display" };
                items.push(item);
            }
        }

        var panel = Ext.getCmp("resOutputPanel");
        if (panel != null) {
            panel.removeAll();
            panel.add(items);
            panel.doLayout();
        }
        return items;
    }

    function escapeHtml(html) {
        var str = html.toString().replace(/</g, "&lt;").replace(/>/g, "&gt;");
        return str;
    }

    /*show node/credential dialogs events*/
    function onNodeSelect(sender, callback) {
        var handler = function (node) {
            mModel.NodeId = node.id;
            mModel.NodeName = node.name;
            mModel.NodeStatus = node.status;

            sender.setIcon(getNodeStatusIcon());
            sender.setText(getShortText(node.name, 19));
            sender.setTooltip(node.name);

            mDlg.doLayout();

            if (typeof (callback) == "function") { callback(); }
            return false;
        };
        SW.APM.NodesDialog.show({ onSuccess: handler });
        return false;
    }
    function onCredSelect(sender, callback) {
        var handler = function (cred) {

            SW.APM.EditApp.ScriptMonitorCredentials = cred;
            
            mCmp.CredentialSetId.Value = cred.id;
            mCmp.CredentialSetId.SettingLevel = (mTpl == null && !mCmp.hasOwnProperty("TemplateId")) ? SETTING_LEVEL.Template : SETTING_LEVEL.Instance;

            sender.setText(getShortText(cred.name, 18));
            sender.setTooltip(cred.name);

            mDlg.doLayout();

            if (typeof (callback) == "function") { callback(); }
            return false;
        };
        var credId = mCmp.CredentialSetId ? mCmp.CredentialSetId.Value : -1;
        SW.APM.CredentialsDialog.show({ addNoneCred: true, credId: credId, onSuccess: handler });
        return false;
    }

    /*get script otput events*/
    function onBeginOutput() {
        if (mRequest == null) {
            if (validateBody()) {
                var success = function () {
                    updateOutput(null, null, true);

                    var appSettings =
                    {
                        Use64Bit: mModel.Use64Bit.Value,
                        Frequency: mModel.Frequency.Value,
                        NumberOfLogFilesToKeep: 0,
                        CustomLogsEnabled: false,
                        ModelId: mModel.Id,
                        IsTemplate: mModel.IsTemplate,
                        PollingTimeoutSeconds: mModel.Timeout.Value
                    };

                    mCmp.TestResults =
                    {
                        NodeId: mModel.NodeId,
                        NodeName: mModel.NodeName
                    };

                    mRunningTest = null;

                    prepareCmpToEncode();
                    mRequest = $.ajax({
                        type: "POST", url: "/Orion/APM/Admin/Edit/Edit.asmx/StartDefineSettings", contentType: "application/json; charset=utf-8", dataType: "json",
                        data: Ext.encode({ editModel: mCmp, settings: appSettings }),
                        success: function (resp) { onProcessOutput(resp.d); },
                        error: function (xhr) { abort(); updateOutput(null, [xhr.responseText], false); }
                    });
                    Ext.getCmp("bGetOutput").setText("@{R=APM.Strings;K=APMWEBJS_CANCEL_SCRIPT_OUTPUT;E=JS}");
                };

                var validateCred = function () { var btn = Ext.getCmp("bCredSelect"); if (btn.getText() == NS) { onCredSelect(btn, success); return false; } success(); return true; };
                var validateNode = function () { var btn = Ext.getCmp("bNodeSelect"); if (mModel.NodeId == 0) { onNodeSelect(btn, validateCred); return false; } return true; };

                if (validateNode()) {
                    mIsDirty = false;
                    validateCred();
                }
            }
        } else {
            abort();
            updateOutput(null, null, false);
        }
        return false;
    }

    function onProcessOutput(result) {
        if (mRunningTest == null) { mRunningTest = result; }
        if (result.TestPassed) {
            abort();
            if (result.Columns && result.Columns.length > 0) {
                mCmp.DynamicColumnSettings = result.Columns;
                updateOutput(result.Columns, null, false);
                mIsDirty = false;
            } else {
                updateOutput(null, result.Messages, false);
            }
        } else {
            mRequestTimeoutId = setTimeout(
				function () {
				    prepareCmpToEncode();
				    mRequest = $.ajax({
				        type: "POST", url: "/Orion/APM/Admin/Edit/Edit.asmx/GetDefineSettingsResults", contentType: "application/json; charset=utf-8", dataType: "json",
				        data: Ext.encode({ editModel: mCmp, inProgressTest: mRunningTest, isTemplate: mModel.IsTemplate }),
				        dataFilter: function (response) {
				        	return response.replace(/\"NumericData\"\:NaN/gi, "\"NumericData\":\"NaN\"");
				        },
				        success: function (resp) { onProcessOutput(resp.d); },
				        error: function (xhr) { abort(); updateOutput(null, [xhr.responseText], false); }
				    });
				},
				3108
			);
        }
    }

    /*script arguments and body events*/
    function onScriptArgumentsValueChanged(sender, newVal, oldValue) {
        if (mCmp.Settings.ScriptArguments) {
            if (mTpl != null && mCmp.Settings.ScriptArguments.SettingLevel == SETTING_LEVEL.Template) {
                mTpl.Settings.ScriptArguments.Value = newVal;
            } else {
                mCmp.Settings.ScriptArguments.Value = newVal;
            }
        } else {
            if (mTpl != null && mCmp.Settings.CommandLineToPass.SettingLevel == SETTING_LEVEL.Template) {
                mTpl.Settings.CommandLineToPass.Value = newVal;
            } else {
                mCmp.Settings.CommandLineToPass.Value = newVal;
            }
        }
    }
    function onScriptBodyValueChanged(sender, newVal, oldValue) {
        mIsDirty = true;
        if (mTpl != null && mCmp.Settings.ScriptBody.SettingLevel == SETTING_LEVEL.Template) {
            mTpl.Settings.ScriptBody.Value = newVal;
        } else {
            mCmp.Settings.ScriptBody.Value = newVal;
        }
    }
    /*inherit and override button events*/
    function onInheritClick(sender, el, cmpSett, tplSett) {
        if (cmpSett.SettingLevel == SETTING_LEVEL.Template) {
            cmpSett.SettingLevel = SETTING_LEVEL.Instance;
            sender.setText(IT);
            el.enable(); el.setValue(cmpSett.Value);
        } else {
            cmpSett.SettingLevel = SETTING_LEVEL.Template;
            sender.setText(OT);
            el.disable(); el.setValue(tplSett.Value);
        }
    }
    function onArgumentsInheritClick(sender, evt) {
        var cmpArg = null, tplArg = null;
        if (mCmp.Settings.ScriptArguments) {
            cmpArg = mCmp.Settings.ScriptArguments;
            if (mTpl != null) {
                tplArg = mTpl.Settings.ScriptArguments;
            }
        } else {
            cmpArg = mCmp.Settings.CommandLineToPass;
            if (mTpl != null) {
                tplArg = mTpl.Settings.CommandLineToPass;
            }
        }
        onInheritClick(sender, Ext.getCmp("tfScriptArguments"), cmpArg, tplArg);
    }
    function onBodyInheritClick(sender, evt) {
        onInheritClick(
			sender,
			Ext.getCmp("taScriptBody"),
			mCmp.Settings.ScriptBody,
			mTpl == null ? null : mTpl.Settings.ScriptBody
		);
    }

    /*dialog's button events*/
    function onSaveClick() {
        if (validateBody()) {
            var callback = function () {
                prepareCmpToSave(); mSuccessCallback(mModel); onCancelClick();
            };
            var messages = [];
            if (mCmp.DynamicColumnSettings.length == 0) {
                messages.push("@{R=APM.Strings;K=APMWEBJS_ScriptSaveMessage;E=js}");
            }
            if (mIsDirty) {
                messages.push("@{R=APM.Strings;K=APMWEBJS_ScriptSaveMessage3;E=js}");
                messages.push("@{R=APM.Strings;K=APMWEBJS_ScriptSaveMessage2;E=js}");
            }
            if (messages.length > 0) {
                messages.push(""); messages.push("@{R=APM.Strings;K=APMWEBJS_ScriptSaveMessage4;E=js}");
                Ext.Msg.show({ title: "@{R=APM.Strings;K=APMWEBJS_ScriptSaveMessage5;E=js}", msg: messages.join("<br/>"), buttons: Ext.Msg.YESNO, icon: Ext.Msg.WARNING, fn: function (action) { if (action == "yes") { callback(); } } });
                return false;
            }
            callback();
        }
        return false;
    }
    function onCancelClick() { if (mDlg) { mDlg.close(); mDlg = null; } return false; }

    /*show script edit dialog members*/
    function show() {
        var width = parseInt(Ext.getBody().getWidth() * 0.8), height = parseInt(Ext.getBody().getHeight() * 0.8), labelsHeight = 220, ReservedWidth = 300;
        if (width < 800) width = 800;
        var allowInherit = mTpl != null;
        var items = [];
        var 
			dl = "<div style='padding-left: " + (mCmp.ComponentType == "NagiosScript" || mCmp.ComponentType == "LinuxScript" ? 20 : 7) + "px; padding-right: 40px'>" + "<label class='x-form-item-label'>",
			ld = "</label>" + "</div>",
			dlr = "<div class='x-form-item '>" + "<label class='x-form-item-label' style='width: 105px;'>";
        if (mCmp.ComponentType == "NagiosScript") {
            items = [
				{ html: dl + "@{R=APM.Strings;K=APMWEBJS_ScriptNagiosMessage;E=js}" + ld },
				{ html: dl + "@{R=APM.Strings;K=APMWEBJS_ScriptNagiosMessage2;E=js}" + ld },
				{ html: dl + "@{R=APM.Strings;K=APMWEBJS_ScriptNagiosMessage3;E=js}" + ld },
				{ html: dl + "<span style='font-family: Courier New; font-size: 8pt;'>statusMessage [|'statisticName'=value]</span>" + ld + "<br/>" },
				{ html: dl + "@{R=APM.Strings;K=APMWEBJS_ScriptNagiosMessage4;E=js}" + ld + "<br/>" },
				{ html: dl + "<span style='font-family: Courier New; font-size: 8pt;'>" + "@{R=APM.Strings;K=APMWEBJS_ScriptNagiosMessage5;E=js}" + " | 'CPU%'=75.2 'MemoryRemainingInKB'=600784</span>" + ld + "<br/>" }
			];
        } else {
            items = [
				{ html: dl + "@{R=APM.Strings;K=APMWEBJS_ScriptInfoMessage;E=js}" + ld },
				{ html: dl + "&nbsp;&nbsp;&nbsp;Statistic.CPU: 31.08" + ld },
				{ html: dl + "&nbsp;&nbsp;&nbsp;Message.CPU: svchost.exe" + "@{R=APM.Strings;K=APMWEBJS_ScriptInfoMessage2;E=js}" + ld },
                { html: dl + "@{R=APM.Strings;K=APMWEBJS_ScriptInfoMessage3;E=js}" + "&raquo; <a style='color:#369;' href='" + SW.APM.Admin.EditScriptDialog.dynamicHelpUrls[mCmp.ComponentType] + "' target='_blank'>" + "@{R=APM.Strings; K=APMWEBJS_LearnHowToFormatUniqueIdentifiersInYourScript; E=js}" +"</a>" + ld + "<br/>" }
			];
        }

        var argField = null, bodyField = null, outField = null, resField = null;
        var argFieldText = null, argFieldValue = null;
        if (mCmp.Settings.ScriptArguments) {
            if (allowInherit && mCmp.Settings.ScriptArguments.SettingLevel == SETTING_LEVEL.Template) {
                argFieldText = OT;
                argFieldValue = mTpl.Settings.ScriptArguments.Value;
            } else {
                argFieldText = IT;
                argFieldValue = mCmp.Settings.ScriptArguments.Value;
            }
        } else {
            if (allowInherit && mCmp.Settings.CommandLineToPass.SettingLevel == SETTING_LEVEL.Template) {
                argFieldText = OT;
                argFieldValue = mTpl.Settings.CommandLineToPass.Value;
            } else {
                argFieldText = IT;
                argFieldValue = mCmp.Settings.CommandLineToPass.Value;
            }
        }
        if (allowInherit) {
            argField = {
                xtype: "compositefield", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_ScriptArgumentsLabel;E=js}",
                items: [
					{
					    xtype: "textfield", id: "tfScriptArguments",
					    value: argFieldValue,
					    disabled: (argFieldText == OT),
					    width: parseInt(width - ReservedWidth),
					    allowBlank: true, anchor: "95%", style: { fontFamily: "Courier New" },
					    listeners: { change: onScriptArgumentsValueChanged }
					},
					{ xtype: "button", width: 130, text: argFieldText, enableToggle: true, pressed: (argFieldText == IT), handler: onArgumentsInheritClick }
				]
            };
        } else {
            argField = {
                xtype: "textfield", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_ScriptArgumentsLabel;E=js}",
                value: argFieldValue, allowBlank: true, anchor: "95%", style: { fontFamily: "Courier New" },
                listeners: { change: onScriptArgumentsValueChanged }
            };
        }
        if (allowInherit) {
            var bodyFieldText = null, bodyFieldValue = null;
            if (mCmp.Settings.ScriptBody.SettingLevel == SETTING_LEVEL.Template) {
                bodyFieldText = OT;
                bodyFieldValue = mTpl.Settings.ScriptBody.Value;
            } else {
                bodyFieldText = IT;
                bodyFieldValue = mCmp.Settings.ScriptBody.Value;
            }

            bodyField = {
                xtype: "compositefield", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_ScriptBodyLabel;E=js}",
                items: [
						{
						    xtype: "textarea", id: "taScriptBody",
						    value: bodyFieldValue,
						    disabled: (bodyFieldText == OT),
						    width: parseInt(width - ReservedWidth), height: parseInt((height - labelsHeight) * 0.6), anchor: "95%", style: { fontFamily: "Courier New" },
						    listeners: { change: onScriptBodyValueChanged }
						},
						{ xtype: "button", width: 130, text: bodyFieldText, enableToggle: true, pressed: (bodyFieldText == IT), handler: onBodyInheritClick }
					]
            };
        } else {
            bodyField = {
                xtype: "textarea", id: "taScriptBody", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_ScriptBodyLabel;E=js}",
                value: mCmp.Settings.ScriptBody.Value, height: parseInt((height - labelsHeight) * 0.6), anchor: "95%", style: { fontFamily: "Courier New" },
                listeners: { change: onScriptBodyValueChanged }
            };
        }
        var credId = 0;
        if (allowInherit && mCmp.CredentialSetId.SettingLevel == SETTING_LEVEL.Template) {
            credId = parseInt(mTpl.CredentialSetId.Value);
        } else {
            credId = parseInt(mCmp.CredentialSetId.Value);
        }
        var cred = { Id: 0, Name: NS, UserName: NS, Password: "********", Broken: false };
        $.each(mModel.Credentials, function (i, item) {
            if (item.Id == credId) {
                cred = item; return false;
            }
        });
        outField = {
            xtype: "compositefield", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_ScriptOutputLabel;E=js}", width: (width - ReservedWidth + 164),
            items: [
				{ xtype: "button", id: "bGetOutput", text: "@{R=APM.Strings;K=APMWEBJS_ScriptField1;E=js}", width: 148, handler: onBeginOutput },
				{ xtype: "displayfield", value: "@{R=APM.Strings;K=APMWEBJS_ScriptField2;E=js}", style: "padding-top:5px;", hideMode: "display" },
				(function () {
				    if (mModel.IsTemplate) {
				        return { xtype: "button", id: "bNodeSelect", text: NS, tooltipType: "title", tooltip: NS, handler: onNodeSelect };
				    }
				    return { xtype: "button", id: "bNodeSelect", disabled: true, icon: getNodeStatusIcon(), text: mModel.NodeName, tooltipType: "title", tooltip: mModel.NodeName };
				})(),
				{ xtype: "displayfield", value: "@{R=APM.Strings;K=APMWEBJS_ScriptField3;E=js}", style: "padding-top:5px;", hideMode: "display" },
				{ xtype: "button", id: "bCredSelect", tooltipType: "title", tooltip: cred.Name, text: cred.Name, handler: onCredSelect },
				{ xtype: "displayfield", value: " @{R=APM.Strings;K=APMWEBJS_ScriptField4;E=js}", style: "padding-top:5px;", hideMode: "display" }
			]
        };

        resField = {
            xtype: "compositefield", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_ScriptOutputResult;E=js}",
            items: [
				{
				    id: "resOutputPanel", xtype: "panel",
				    autoScroll: true,
				    width: parseInt(width - ReservedWidth),
				    height: parseInt((height - labelsHeight) * 0.37),
				    items: updateOutput(mCmp.DynamicColumnSettings, null, false)
				}
			]
        };

        var nodeName = outField.items[2].text, credentialName = outField.items[4].text;
        outField.items[2].text = getShortText(nodeName, 19);
        outField.items[4].text = getShortText(credentialName, 18);

        items.push(argField);
        items.push(bodyField);
        items.push(outField);
        items.push(resField);

        mDlg = new Ext.Window({
            id: "editScriptDlg", title: "@{R=APM.Strings;K=APMWEBJS_EditScript;E=js}", renderTo: Ext.getBody(), width: width, height: height,
            border: false, region: "center", layout: "fit", modal: true, resizable: false,
            items: [{ xtype: "form", frame: true, labelWidth: 105, defaultType: "label", labelAlign: "right", autoScroll: false, items: items}],
            buttons: [{ text: "@{R=APM.Strings;K=APMWEBJS_Save;E=js}", handler: onSaveClick }, { text: "@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}", handler: onCancelClick}]
        });
        mDlg.show();

        return false;
    }

    /*public members*/
    this.show = show;
    this.init = function (cmpId, model, successCallback) {
        mCmpId = cmpId;
        mModel = Ext.decode(Ext.encode(model));
        mSuccessCallback = successCallback;
        for (var i = 0; i < mModel.Components.length; i++) {
            if (mModel.Components[i].Id == mCmpId) {
                mCmp = mModel.Components[i];
                i = mModel.Components.length;
                if (mCmp.TemplateId > 0) {
                    for (var j = 0; j < mModel.Template.Components.length; j++) {
                        if (mModel.Template.Components[j].Id == mCmp.TemplateId) {
                            mTpl = mModel.Template.Components[j];
                            j = mModel.Template.Components.length;
                        }
                    }
                }
            }
        }
        return false;
    };
};
SW.APM.Admin.EditScriptDialog.show = function (cmpId, model, successCallback) {
	var dialog = new SW.APM.Admin.EditScriptDialog();
	dialog.init(cmpId, model, successCallback);
	dialog.show();

	return false;
};