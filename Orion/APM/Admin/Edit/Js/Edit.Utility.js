/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
/// <reference path="../../../APM.js"/>
APMjs.initGlobal('SW.APM.EditApp.Utility', function (Utility, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        SW = APMjs.assertGlobal('SW'),
        SF = APMjs.format,
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        setBlockVisibility = APMjs.setBlockVisibility,
        MSG = {
            sessionExpired: 'Your session expired.'
        };

    function isValidResponse(jContent) {
        return !!jContent.find('div.ComponentEditorParent').length;
    }

    function handleException(editMode, html, rowBody, id, rowExpander) {
        var jContent = $(html), body = $(rowBody);

        if (jContent.find('*[id$=SessionExpired]').length) {
            window.alert(MSG.sessionExpired);
            window.location.reload(true);
            return;
        }

        if (jContent.find('#errorcontent').length) {
            jContent = jContent.find('#errorcontent');
            jContent
                .find('div.sw-btn-bar, p:last')
                .html('<br/>');
        } else {
            jContent = $('<div style="color:#f00; font-weight:bold;">Server error occurred.<div>');
        }

        if (editMode === 'single') {
            rowExpander.bodyContent[id] = body
                .find('.componentEditContent')
                .empty()
                .append(jContent)
                .addClass('loaded')
                .html();
        } else if (editMode === 'multi') {
            body.append(jContent);
        } else {
            window.alert(jContent.html());
        }
    }

    function handleError(message) {
        if (message) {
            window.alert('Error: ' + message);
        }
    }

    function parseBoolean(value) {
        if (APMjs.isBool(value)) {
            return value;
        }
        if (APMjs.isStr(value)) {
            value = value.toLowerCase();
            return value === 'true' || value === '1';
        }
        if (APMjs.isNum(value)) {
            return value !== 0;
        }
        return Boolean(value);
    }

    function convertLinkToButton(jLink) {
        var handler, newLinkString, jLinkNew;

        handler = jLink.attr('onOverrideNotify');

        newLinkString = SW.Core.Widgets.Button(
            'Test',
            {
                type: 'small',
                cls: 'overrideLink',
                id: jLink.attr('id')
            }
        );

        jLinkNew = $(newLinkString)
            .click(Utility.onOverrideLinkClick);

        jLink.replaceWith(jLinkNew);

        if (handler) {
            jLinkNew.attr('onOverrideNotify', handler);
        }

        return jLinkNew;
    }

    function toggleOverrideUi(jChild, showEditor, jRow) {
        var jLink, jLinkBn, jViewer, jEditor, jError, jEditorGrippie, jViewerGrippie, jCheckbox, baseName, cssIE;

        jRow = jRow || EditApp.getEditRow(jChild);

        jLink = jRow.find('.overrideLink');
        jLinkBn = jLink.find('.sw-btn-t');

        jViewer = EditApp.getViewer(jChild, jRow);
        jEditor = EditApp.getEditor(jChild, jRow);
        jError = jEditor.closest('td').find('.validationMsg.sw-suggestion.sw-suggestion-fail');

        if (showEditor === undefined) {
            showEditor = jEditor.is(':hidden');
        }

        jEditorGrippie = jEditor.find('div.grippie');
        jViewerGrippie = jViewer.parent().find('div.grippie');

        if (showEditor) {
            jEditor.show();
            jViewer.hide();

            jLinkBn.text(EditApp.InheritFromTemplateLabel);

            jEditorGrippie.show();
            jViewerGrippie.hide();

            jEditor
                .find('*[apmsettctr=true]')
                .not('[id*=Threshold], [id$=Warning], [id$=Critical], [id$=WarningPolls], [id$=CriticalPolls], [id$=WarningPollsInterval], [id$=CriticalPollsInterval]')
                .addClass('required');
        } else {
            jEditor.hide();
            jViewer.show();
            jError.hide();

            jLinkBn.text(EditApp.OverrideTemplateLabel);

            jEditorGrippie.hide();
            jViewerGrippie.show().css('width', '98%');

            jEditor
                .find('*[apmsettctr]')
                .removeClass('required');
        }
        
        if (jRow.is('.fileUpload')) {
            APMjs.setVisibility(jRow.next('tr').find(':first-child'), showEditor);
        }

        if (jRow.is('.warningThreshold')) {
            // We need to handle the critical threshold too
            toggleOverrideUi(jRow.next('tr').find('td.columnEditor:first-child'), showEditor);
        }

        if (jRow.is('.dataTransform')) {
            // We need to handle the ConvertValueEditor too
            toggleOverrideUi(jRow.next('tr').find('td.columnEditor:first-child'), showEditor);
            jCheckbox = jRow.find('.dataTransformEnableCheckbox');
            baseName = jCheckbox.attr('id');
            setBlockVisibility(baseName + 'TransformSettingsEditor', showEditor && jCheckbox.is(':checked'));
        }
    }

    function notify(jEl) {
        jEl.trigger('change');
    }

    function onOverrideLinkClick(event) {
        var jLink, selNotify;
        jLink = $(event.currentTarget);
        toggleOverrideUi(jLink);
        if (jLink.attr('ignoreOverrideClick') !== 'true') {
            selNotify = jLink.attr('onOverrideNotify');
            if (selNotify && selNotify.length) {
                notify($(selNotify));
            }
        }
        return false;
    }

    function chunk(array, process, callback, context, args) {
        if (!APMjs.isFn(process)) {
            throw new TypeError('process argument should be a function');
        }
        if (APMjs.isSet(callback) && !APMjs.isFn(callback)) {
            throw new TypeError('callback argument should be a function when set');
        }
        APMjs.linqEachAsync(array, function (item) {
            if (item) {
                process.call(context, item, args);
            }
        }, function () {
            if (callback) {
                callback.call(context, args);
            }
        });
    }

    // export public functions
    // TODO: we should get rid of this script and incorporate functions into Edit.js / SW.APM.EditApp
    // all referencing components are under EditApp anyway
    // does not make sense to keep it scattered when Utility script is even referenced in Edit.js and has to be always included
    Utility.chunk = chunk;  // ComponentGrid.TestManager, ComponentGrid.ToolbarHandlers, ComponentEditors
    Utility.isValidResponse = isValidResponse;  // used in: ComponentGrid, Edit.MultiEditDialog
    Utility.handleException = handleException;  // used in: ComponentGrid, Edit.MultiEditDialog
    Utility.handleError = handleError;          // used in: ComponentGrid.TestManager, ComponentGrid.ToolbarHandlers
    Utility.parseBoolean = parseBoolean;        // used in: Edit, Edit.Thresholds
    Utility.convertLinkToButton = convertLinkToButton;  // used in: Edit, Edit.DynamicEvidence, Edit.Thresholds
    Utility.onOverrideLinkClick = onOverrideLinkClick;  // used in: Edit
    Utility.toggleOverrideUi = toggleOverrideUi;    // used in: Edit, Edit.DynamicEvidence, Edit.Thresholds, ComponentEditors/LogFile, ComponentEditors/MapiUserExperience
});
