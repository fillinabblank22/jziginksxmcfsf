﻿using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.Configuration;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Logging;
using System;
using System.Web.UI;

public partial class Orion_APM_Admin_Edit_EditApplication : SolarWinds.APM.Web.EditPage.ApmEditPageBase
{
    private readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.ApplyHeadTemplateApplication();
        this.ApplyCustomControlsApplication(this.placeHolderMainProperties);
        if (!this.IsPostBack)
        {
            this.ctrFrequency.MinValue = Convert.ToInt32(ApplicationBase.MinPollingFrequencyValue.TotalSeconds);
            this.ctrTimeout.MinValue = Convert.ToInt32(ApplicationBase.MinPollingTimeoutValue.TotalSeconds);
        }
        if (ctrExecutePollingMethod.Options == null || ctrExecutePollingMethod.Options.Count == 0)
        {
            ctrExecutePollingMethod.Options = DropdownOptionsHelper.OptionsFromEnum<PollingMethod>();
        }
    }

    protected void LoadEditor(CustomEditorElement editor)
    {
        try
        {
            var pageHolder = new Page();
            Control control = pageHolder.LoadControl(editor.Src);
            this.log.DebugFormat("Loaded control '{0}' as application editor", editor);
            if (control != null)
            {
                this.appCustomEditors.Controls.Add(control);
            }
        }
        catch (Exception exc)
        {
            this.log.ErrorFormat("Cannot load control '{0}' as application editor: {1}", editor, exc);
        }
    }

    protected void appCustomEditor_OnInit(object sender, EventArgs e)
    {
        this.GetPluginsFromConfiguration().ForEach(this.LoadEditor);
    }
}
