﻿<%@ WebHandler Language="C#" Class="FileUpload" %>
using System;
using System.IO;
using System.Text;
using System.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web.Utility;

public class FileUpload : IHttpHandler
{
	private class Result
	{
		public string Data { get; set; }
		public string Error { get; set; }
	};
	
	public bool IsReusable
	{
		get { return false; }
	}   

    public void ProcessRequest(HttpContext context)
    {
		var result = new Result(); 
        try
        {
			var req = context.Request;
            if (req.Files.Count == 0 || string.IsNullOrWhiteSpace(req.Files[0].FileName))
            {
				throw new Exception("Please select a file.");
            }

			if (string.Equals(context.Request["method"], "GetHash", StringComparison.InvariantCultureIgnoreCase))
			{
				result.Data = GetHash(req.Files[0]);
			}
			else if (string.Equals(context.Request["method"], "GetContent", StringComparison.InvariantCultureIgnoreCase))
			{
				result.Data = GetContent(req.Files[0]);
			}
			else 
			{
				throw new Exception("Invalid method.");
			}
        }
        catch (Exception ex)
        {
			result.Error = ex.Message;
        }
		
		context.Response.ContentType = "text/plain";
		context.Response.Write(JsHelper.Serialize(result));
	}

	public string GetHash(HttpPostedFile file)
	{
		using (var stream = file.InputStream)
		{
			return FileHelper.CheckSumSHA1(stream);
		}
	}

	public string GetContent(HttpPostedFile file)
	{
		using (var stream = file.InputStream)
		using (var reader = new StreamReader(stream, Encoding.UTF8))
		{
			return reader.ReadToEnd();
		}
	}
}