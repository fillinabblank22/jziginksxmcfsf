﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType  virtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master"%>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="ProcessOverSNMP" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
        SW.APM.EditApp.Editors.ProcessOverSnmp.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>'
            );
    </script>

    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CommandLineFilter %>" BaseValueName="cmpCommandLineFilter" ElementId="<%# Master.ComponentId%>" Required="false" EditorMode="<%# Master.EditMode%>" runat="server"/>
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ProcessName %>" BaseValueName="cmpProcessName" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>

    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_StatusWhenProcessIsNotRunning %>" BaseValueName="cmpNotRunningStatusMode" ElementId="<%# Master.ComponentId %>" 
        Options="<%# DropdownOptionsHelper.OptionsFromEnum<ProcessInfo_NotRunningStatus>()%>" EditorMode="<%# Master.EditMode%>" HelpTextBottom="..." runat="server" />

    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CPU %>" BaseValueName="cmpCPUThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PhysicalMemory %>" BaseValueName="cmpPMemThreshold" ElementId="<%#Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VirtualMemory %>" BaseValueName="cmpVMemThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>" HelpTextTop="<%# Resources.APMWebContent.ThresholdEditor_ApplicableWhenPollingViaAnAgent %>"  runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IOReadOperationsSec %>" BaseValueName="cmpIOReadOPSThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>" HelpTextTop="<%# Resources.APMWebContent.ThresholdEditor_ApplicableWhenPollingViaAnAgent %>"  runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IOWriteOperationsSec %>" BaseValueName="cmpIOWriteOPSThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>" HelpTextTop="<%# Resources.APMWebContent.ThresholdEditor_ApplicableWhenPollingViaAnAgent %>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IOTotalOperationsSec %>" BaseValueName="cmpIOTotalOPSThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>" HelpTextTop="<%# Resources.APMWebContent.ThresholdEditor_ApplicableWhenPollingViaAnAgent %>" runat="server" />
</asp:Content>
