﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<script runat="server">
	protected override void OnLoad(EventArgs e)
	{
		DataBind();
	}
</script>

<asp:Content ID="c1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
      <apm:ComponentTypeEditor ComponentType="VmWarePerfCounter" runat="server"/>
</asp:Content>
<asp:Content ID="c2" ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
	<% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["VmWarePerfCounter"];
				editApp.initMultiUiState($("#cmpPortNumber" + cmpId), cmp.DefinitionSettings, "PortNumber");
                editApp.initMultiUiState($("#cmpUrl" + cmpId), cmp.DefinitionSettings, "Url");
                editApp.initMultiUiState($("#cmpIgnoreCA" + cmpId), cmp.DefinitionSettings, "IgnoreCA");
                editApp.initMultiUiState($("#cmpIgnoreCN" + cmpId), cmp.DefinitionSettings, "IgnoreCN");
                editApp.initMultiUiState($("#cmpIgnoreCRL" + cmpId), cmp.DefinitionSettings, "IgnoreCRL");
				editApp.initMultiUiState($("#cmpCountAsDifference"+ cmpId), cmp.DefinitionSettings, "CountAsDifference");
			    editApp.initMultiUiState($("#cmpEntryType" + cmpId), cmp.DefinitionSettings, "EntityType");
			    editApp.initMultiUiState($("#cmpEntityName" + cmpId), cmp.DefinitionSettings, "EntityName");
			    editApp.initMultiUiState($("#cmpGroup" + cmpId), cmp.DefinitionSettings, "Group");
			    editApp.initMultiUiState($("#cmpCounter" + cmpId), cmp.DefinitionSettings, "Counter");
			    editApp.initMultiUiState($("#cmpRollupType" + cmpId), cmp.DefinitionSettings, "RollupType");
			    editApp.initMultiUiState($("#cmpInstance" + cmpId), cmp.DefinitionSettings, "Instance");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                
				editApp.updateMultiModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings, "PortNumber", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpUrl" + cmpId), cmp.Settings, "Url", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpIgnoreCA" + cmpId), cmp.Settings, "IgnoreCA", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpIgnoreCN" + cmpId), cmp.Settings, "IgnoreCN", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpIgnoreCRL" + cmpId), cmp.Settings, "IgnoreCRL", cmpIds);
            	editApp.updateMultiModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpEntryType" + cmpId), cmp.Settings, "EntityType", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpEntityName" + cmpId), cmp.Settings, "EntityName", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpGroup" + cmpId), cmp.Settings, "Group", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCounter" + cmpId), cmp.Settings, "Counter", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpRollupType" + cmpId), cmp.Settings, "RollupType", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpInstance" + cmpId), cmp.Settings, "Instance", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
			};
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	<% } else { %>
        (function() {
            var cmpId = <%=Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$("#cmpDesc" + cmpId), "UserDescription", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpDisabled" + cmpId), "IsDisabled", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpNotes" + cmpId), "UserNote", cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$("#cmpCredential" + cmpId), function() {
                            editApp.initUiState($("#cmpCredential" + cmpId), "CredentialSetId", cmp, template);
                        }]
                    },                
                    { func: editApp.initUiState, args: [$("#cmpPortNumber" + cmpId), "PortNumber", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpUrl" + cmpId), "Url", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIgnoreCA' + cmpId), 'IgnoreCA', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIgnoreCN' + cmpId), 'IgnoreCN', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIgnoreCRL' + cmpId), 'IgnoreCRL', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpCountAsDifference" + cmpId), "CountAsDifference", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryType" + cmpId), "EntityType", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntityName" + cmpId), "EntityName", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpGroup" + cmpId), "Group", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpCounter" + cmpId), "Counter", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpRollupType" + cmpId), "RollupType", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpInstance" + cmpId), "Instance", cmp.Settings, templateSettings] },                
                    { func: editApp.initDataTransform, args: [$("#cmpConvertValue" + cmpId), cmp, template] },
                    { func: editApp.initThreshold, args: [$("#cmpStatisticThreshold" + cmpId), "StatisticData", cmp.Thresholds, templateThresholds] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($("#cmpDesc" + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($("#cmpDisabled" + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($("#cmpNotes" + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($("#cmpCredential" + cmpId), cmp.CredentialSetId, isTemplate);
                
                editApp.updateModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings.PortNumber, isTemplate);
                editApp.updateModelFromUi($("#cmpUrl" + cmpId), cmp.Settings.Url, isTemplate);
                editApp.updateModelFromUi($('#cmpIgnoreCA' + cmpId), cmp.Settings.IgnoreCA, isTemplate);
                editApp.updateModelFromUi($('#cmpIgnoreCN' + cmpId), cmp.Settings.IgnoreCN, isTemplate);
                editApp.updateModelFromUi($('#cmpIgnoreCRL' + cmpId), cmp.Settings.IgnoreCRL, isTemplate);
                editApp.updateModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings.CountAsDifference, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryType" + cmpId), cmp.Settings.EntityType, isTemplate);
                editApp.updateModelFromUi($("#cmpEntityName" + cmpId), cmp.Settings.EntityName, isTemplate);
                editApp.updateModelFromUi($("#cmpGroup" + cmpId), cmp.Settings.Group, isTemplate);
                editApp.updateModelFromUi($("#cmpCounter" + cmpId), cmp.Settings.Counter, isTemplate);
                editApp.updateModelFromUi($("#cmpRollupType" + cmpId), cmp.Settings.RollupType, isTemplate);
                editApp.updateModelFromUi($("#cmpInstance" + cmpId), cmp.Settings.Instance, isTemplate);
				
				editApp.updateDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds.StatisticData, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>

	<apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" MinValue="0" EditorMode="<%# Master.EditMode%>" runat="server"  />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Url %>" BaseValueName="cmpUrl" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCAErrors %>" BaseValueName="cmpIgnoreCA" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCAErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCAErrors %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCNErrors %>" BaseValueName="cmpIgnoreCN" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCNErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCNErrors %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCRLErrors %>" BaseValueName="cmpIgnoreCRL" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCRLErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCRLErrors %>" runat="server" />
	<apm:BooleanCheckboxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
	<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_EntityType %>" BaseValueName="cmpEntryType" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<VmWareVCenterEntityType>()%>" EditorMode="<%# Master.EditMode%>" runat="server" />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_EntityName %>" BaseValueName="cmpEntityName" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Group %>" BaseValueName="cmpGroup" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Counter %>" BaseValueName="cmpCounter" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_RollupType %>" BaseValueName="cmpRollupType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Instance %>" BaseValueName="cmpInstance" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>"  Required="false" runat="server"  />
	<apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />  
	<apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>

