﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor ComponentType="WebLink" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["WebLink"];
			    editApp.initMultiUiState($("#cmpUrl" + cmpId), cmp.DefinitionSettings, "Url");
			    editApp.initMultiUiState($("#cmpAcceptCompression" + cmpId), cmp.DefinitionSettings, "AcceptCompression");
			    editApp.initMultiUiState($("#cmpExclusionFilter" + cmpId), cmp.DefinitionSettings, "ExclusionFilter");
			    editApp.initMultiUiState($("#cmpIgnoreExternalLinks" + cmpId), cmp.DefinitionSettings, "IgnoreExternalLinks");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
			    editApp.updateMultiModelFromUi($("#cmpUrl" + cmpId), cmp.Settings, "Url", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpAcceptCompression" + cmpId), cmp.Settings, "AcceptCompression", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpExclusionFilter" + cmpId), cmp.Settings, "ExclusionFilter", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpIgnoreExternalLinks" + cmpId), cmp.Settings, "IgnoreExternalLinks", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;

            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },
                    { func: editApp.initUiState, args: [$('#cmpUrl' + cmpId), 'Url', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpAcceptCompression' + cmpId), 'AcceptCompression', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpExclusionFilter' + cmpId), 'ExclusionFilter', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIgnoreExternalLinks' + cmpId), 'IgnoreExternalLinks', cmp.Settings, templateSettings] },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initDataTransform, args: [$('#cmpConvertValue' + cmpId), cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] }
                ];
                 var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateModelFromUi($('#cmpUrl' + cmpId), cmp.Settings.Url, isTemplate);
                editApp.updateModelFromUi($('#cmpAcceptCompression' + cmpId), cmp.Settings.AcceptCompression, isTemplate);
                editApp.updateModelFromUi($('#cmpExclusionFilter' + cmpId), cmp.Settings.ExclusionFilter, isTemplate);
                editApp.updateModelFromUi($('#cmpIgnoreExternalLinks' + cmpId), cmp.Settings.IgnoreExternalLinks, isTemplate);
                editApp.updateDataTransformModelFromUi($('#cmpConvertValue' + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId),cmp.Thresholds.Response, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Url %>" BaseValueName="cmpUrl" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AcceptCompression %>" BaseValueName="cmpAcceptCompression" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExclusionFilter %>" BaseValueName="cmpExclusionFilter" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreExternalLinks %>" BaseValueName="cmpIgnoreExternalLinks" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />  
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
 </asp:Content>