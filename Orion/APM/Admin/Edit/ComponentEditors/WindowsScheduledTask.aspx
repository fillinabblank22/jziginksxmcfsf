﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>

<asp:Content ID="c1" ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server">
   <apm:componenttypeeditor ComponentType="WindowsScheduledTask" runat="server"/>
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">
    
    <script type="text/javascript">
        SW.APM.EditApp.Editors.WindowsTaskScheduler.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>');
    </script>

</asp:Content>
