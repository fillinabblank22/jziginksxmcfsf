﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Reference Control="ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.DAL" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI.Formatters" %>

<asp:Content ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor runat="server" ComponentType="CustomPerfCounter"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">
		<% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
        <% if (Master.IsTemplate) { %>
		<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ComponentCategory %>" BaseValueName="cmpComponentCategory" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.InsertNone(ComponentCategoryDAL.GetAllCategoryPairs())%>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VisibilityMode %>" BaseValueName="cmpVisibilityMode" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ComponentVisibilityMode>() %>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <% }%>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Category %>" BaseValueName="cmpCategory" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Counter %>" BaseValueName="cmpCounter" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" />
		<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CounterUnit %>" BaseValueName="cmpCounterUnit" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ChartFormatter>() %>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Instance %>" BaseValueName="cmpInstance" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" Required="False" />
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CounterType %>" BaseValueName="cmpCounterType" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<PerfCounterType>() %>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_BaseCounter %>" BaseValueName="cmpBaseCounter" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" Required="False" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_MinimalSupportedVersion %>" BaseValueName="cmpMinVersion" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" Required="False" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_MaximalSupportedVersion %>" BaseValueName="cmpMaxVersion" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" Required="False" />
   <% } %>
	<apm:ThresholdEditor ID="statThresholdEditor" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_StatisticData %>" BaseValueName="cmpStatisticThreshold" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
    <script type="text/javascript">
	    <% if (Master.IsMultiEditMode) { %>
	    (function () {
		    var cmpId = <%= Master.ComponentId %>;
		    var cmpIds = <%= Master.ComponentIds %>;
		    var editApp = SW.APM.EditApp;
            
		    var onLoad = function(app) {
		        var cmp = SW.APM.EditApp.ComponentDefinitions["CustomPerfCounter"];
		    	editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
		    	<% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
		    	<% if (Master.IsTemplate) { %>
		        editApp.initMultiUiState($("#cmpComponentCategory" + cmpId), cmp.DefinitionSettings, "ComponentCategory");
		        editApp.initMultiUiState($("#cmpVisibilityMode" + cmpId), cmp.DefinitionSettings, "VisibilityMode");
		        <% } %>
		        editApp.initMultiUiState($("#cmpCounter" + cmpId), cmp.DefinitionSettings, "Counter");
		    	editApp.initMultiUiState($("#cmpInstance" + cmpId), cmp.DefinitionSettings, "Instance");
		    	editApp.initMultiUiState($("#cmpCategory" + cmpId), cmp.DefinitionSettings, "Category");
		    	editApp.initMultiUiState($("#cmpCounterType" + cmpId), cmp.DefinitionSettings, "CounterType");
		    	editApp.initMultiUiState($("#cmpCounterUnit" + cmpId), cmp.DefinitionSettings, "CounterUnit");
		    	editApp.initMultiUiState($("#cmpBaseCounter" + cmpId), cmp.DefinitionSettings, "BaseCounter");
		    	editApp.initMultiUiState($("#cmpMinVersion" + cmpId), cmp.DefinitionSettings, "MinSupportedVersion");
		    	editApp.initMultiUiState($("#cmpMaxVersion" + cmpId), cmp.DefinitionSettings, "MaxSupportedVersion");
			    <% } %>
		    };
	    	var onSave = function(app) {
	    		var cmp = app.getMultiEditComponent();
	    		editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
	    	    <% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
		    	<% if (Master.IsTemplate) { %>
	    	    editApp.updateMultiModelFromUi($("#cmpComponentCategory" + cmpId), cmp.Settings, "ComponentCategory", cmpIds);
	    	    editApp.updateMultiModelFromUi($("#cmpVisibilityMode" + cmpId), cmp.Settings, "VisibilityMode", cmpIds);
	    	    <% } %>	    	    
	    		editApp.updateMultiModelFromUi($("#cmpCounter" + cmpId), cmp.Settings, "Counter", cmpIds);
	    		editApp.updateMultiModelFromUi($("#cmpCounterUnit" + cmpId), cmp.Settings, "CounterUnit", cmpIds);
	    		editApp.updateMultiModelFromUi($("#cmpInstance" + cmpId), cmp.Settings, "Instance", cmpIds);
	    		editApp.updateMultiModelFromUi($("#cmpCategory" + cmpId), cmp.Settings, "Category", cmpIds);
	    		editApp.updateMultiModelFromUi($("#cmpCounterType" + cmpId), cmp.Settings, "CounterType", cmpIds);
	    		editApp.updateMultiModelFromUi($("#cmpBaseCounter" + cmpId), cmp.Settings, "BaseCounter", cmpIds);
	    		editApp.updateMultiModelFromUi($("#cmpMinVersion" + cmpId), cmp.Settings, "MinSupportedVersion", cmpIds);
	    		editApp.updateMultiModelFromUi($("#cmpMaxVersion" + cmpId), cmp.Settings, "MaxSupportedVersion", cmpIds);
	    		<% } %> 
	    	};
			editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
	    })();
	    <% } else { %>
	    (function() {
		    var cmpId = <%= Master.ComponentId %> ;
		    var editApp = SW.APM.EditApp;
            
		    var onLoad = function(model) {
		        SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
		        SW.APM.EventManager.fire("onComponentLoading");
		        var componentContainer = $("#componentEditorForm" + cmpId);
		        var parentComponentContainer;
                
		        if ($.browser.msie) {
		            parentComponentContainer = componentContainer.parent().parent()[0];
		        } else {
		            parentComponentContainer = componentContainer.parent()[0];
		        }

		        var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
		        mLoading.show();
                
		        var cmp = this;
		        var template = model.getComponentTemplate(cmp.TemplateId);
		        var templateSettings = template ? template.Settings : null;
		        var templateThresholds = template ? template.Thresholds : null;

		        var chunkItemsToProceed = [
				    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template<% if (Master.IsBlackBoxEdit) { %>, true<% } %>] },
				    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
				    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] },
				    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
					<% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
					<% if (Master.IsTemplate) { %>
                    { func: editApp.initUiState, args: [$('#cmpComponentCategory' + cmpId), 'ComponentCategory', cmp.Settings, templateSettings] },
		            { func: editApp.initUiState, args: [$('#cmpVisibilityMode' + cmpId), 'VisibilityMode', cmp.Settings, templateSettings] },
			        <% } %>
					{ func: editApp.initUiState, args: [$('#cmpCategory' + cmpId), 'Category', cmp.Settings, templateSettings] },
					{ func: editApp.initUiState, args: [$('#cmpCounter' + cmpId), 'Counter', cmp.Settings, templateSettings] },
					{ func: editApp.initUiState, args: [$('#cmpCounterUnit' + cmpId), 'CounterUnit', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpInstance' + cmpId), 'Instance', cmp.Settings, templateSettings] },
			        { func: editApp.initUiState, args: [$('#cmpCounterType' + cmpId), 'CounterType', cmp.Settings, templateSettings] },
			        { func: editApp.initUiState, args: [$('#cmpBaseCounter' + cmpId), 'BaseCounter', cmp.Settings, templateSettings] },
		            { func: editApp.initUiState, args: [$('#cmpMinVersion' + cmpId), 'MinSupportedVersion', cmp.Settings, templateSettings] },
			        { func: editApp.initUiState, args: [$('#cmpMaxVersion' + cmpId), 'MaxSupportedVersion', cmp.Settings, templateSettings] },
				    <% } %>
			    ];
                
			    var initSetting = function (settingItem) {
				    var func = settingItem["func"];
				    var args = settingItem["args"];
				    func.apply(this, args);
			    };
			    SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
				    mLoading.hide(); 
				    var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
				    SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
				    SW.APM.EventManager.fire("onComponentLoading");

			    	//Load units
				    var stringUnit = cmp.Settings.CounterUnit.Value;
				    if ((stringUnit != "None") && (stringUnit != "Statistic") && (stringUnit != "Percent")){
				    	var unitSpans = $(".<%# statThresholdEditor.RootName %>_Unit");
				        $.each(unitSpans,function(index, value){ 				            
				            if (stringUnit) {
				                $(value).html('(' + stringUnit + ')');
				            }				    	    
				    	});
				    }
			    });
		    	SW.APM.EditApp.Editors.hideEnableSettingIfNeeded(cmp, componentContainer);
		    };

		    var onSave = function(model) {
			    var cmp = this;
			    var isTemplate = !model.haveTemplate();
                
			    editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
			    editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
			    editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
			    editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
		        <% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
		        <% if (Master.IsTemplate) { %>
		        editApp.updateModelFromUi($('#cmpComponentCategory' + cmpId), cmp.Settings.ComponentCategory, isTemplate);
		        editApp.updateModelFromUi($('#cmpVisibilityMode' + cmpId), cmp.Settings.VisibilityMode, isTemplate);
                <% }%>
		    	editApp.updateModelFromUi($('#cmpCategory' + cmpId), cmp.Settings.Category, isTemplate);
		    	editApp.updateModelFromUi($('#cmpCounter' + cmpId), cmp.Settings.Counter, isTemplate);
		    	editApp.updateModelFromUi($('#cmpCounterUnit' + cmpId), cmp.Settings.CounterUnit, isTemplate);
		    	editApp.updateModelFromUi($('#cmpInstance' + cmpId), cmp.Settings.Instance, isTemplate);
		    	editApp.updateModelFromUi($('#cmpCounterType' + cmpId), cmp.Settings.CounterType, isTemplate);
		    	editApp.updateModelFromUi($('#cmpBaseCounter' + cmpId), cmp.Settings.BaseCounter, isTemplate);
		    	editApp.updateModelFromUi($('#cmpMinVersion' + cmpId), cmp.Settings.MinSupportedVersion, isTemplate);
		    	editApp.updateModelFromUi($('#cmpMaxVersion' + cmpId), cmp.Settings.MaxSupportedVersion, isTemplate);
			    <% } %>
		    };
		    editApp.Grid.registerEditor(cmpId, onLoad, onSave);
	    })();
	    <% }%>
    </script>

</asp:Content>

