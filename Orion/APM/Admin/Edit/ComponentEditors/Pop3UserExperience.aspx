﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="Pop3UserExperience" runat="server" />
</asp:Content>

<asp:Content  ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
<% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            var handleSmtpCredentialsUsing = function() {
                var useCredentialsForSmtpCheckBox = $('#cmpUseCredentialsForSMTP0');
                var credentialsSmtpDropdown = $($('#cmpCredentialSMTP0').parents('tr')[0]);
                $('#cmpCredentialSMTP' +cmpId).attr("apm-me-was-changed", useCredentialsForSmtpCheckBox[0].checked);  
                if (useCredentialsForSmtpCheckBox[0].checked == false) {
                    credentialsSmtpDropdown.hide();
                } else {
                    credentialsSmtpDropdown.show();
                }
            };
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["Pop3UserExperience"];
				editApp.initMultiUiState($('#cmpSendEmailFrom' + cmpId), cmp.DefinitionSettings, "EmailFrom");
				editApp.initMultiUiState($('#cmpSendEmailTo' + cmpId), cmp.DefinitionSettings, "EmailTo");
				editApp.initMultiUiState($('#cmpPOP3Port' + cmpId), cmp.DefinitionSettings, "Pop3PortNumber");
				editApp.initMultiUiState($('#cmpPOP3Encryption' + cmpId), cmp.DefinitionSettings, "Pop3Ssl");
				editApp.initMultiUiState($('#cmpSMTPServer' + cmpId), cmp.DefinitionSettings, "SmtpSvr");
				editApp.initMultiUiState($('#cmpSMTPPort' + cmpId), cmp.DefinitionSettings, "SmtpPortNumber");
				editApp.initMultiUiState($('#cmpUseCredentialsForSMTP' + cmpId), cmp.DefinitionSettings, "SmtpAuth");
				editApp.initMultiUiState($('#cmpSMTPEncryption' + cmpId), cmp.DefinitionSettings, "SmtpSsl");
				editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
				editApp.initMultiUiState($('#cmpCredentialSMTP' + cmpId), cmp.DefinitionSettings, '_CredentialSetId_SMTP');
				editApp.addSMTPCredentialSets($('#cmpCredentialSMTP' + cmpId));
				var useCredentialsForSmtp = $('#cmpUseCredentialsForSMTP' + cmpId);
				var cmpCredentialSmtpRow = $('#cmpCredentialSMTP0').parents('tr')[0];
				
				var disableSubEditorForUseCredentialSmtp = function() {
				    var cmpUseCredentialSmtpRow = $($(useCredentialsForSmtp).parents('tr')[0]),
				        multiEditCheckBox = cmpUseCredentialSmtpRow.find("input[id$=MultiChekbox]"),
				        editor = cmpUseCredentialSmtpRow.find('.editor'),				    
				        isMultiEditCheckChecked = multiEditCheckBox[0].checked;
				    
				    isMultiEditCheckChecked ? editor.find("[id]").removeAttr("disabled") : editor.find("[id]").attr("disabled", "disabled");
				    useCredentialsForSmtp.attr("apm-me-was-changed", isMultiEditCheckChecked);  
				   
				    if(!isMultiEditCheckChecked) {  
				        $('#cmpCredentialSMTP' +cmpId).attr("apm-me-was-changed", isMultiEditCheckChecked);  
				        $(cmpCredentialSmtpRow).hide();
				    }

				    if(isMultiEditCheckChecked && useCredentialsForSmtp[0].checked) {
				        $(cmpCredentialSmtpRow).show();
				    }

				    return true;
				};
                
				var multiCheckBox = $($('#cmpUseCredentialsForSMTP0').parents('tr')[0]).find("input[id$=MultiChekbox]");
				$(multiCheckBox).unbind('click').bind('click',disableSubEditorForUseCredentialSmtp);
                
				if (cmp.DefinitionSettings.SmtpAuth.Value.toString() == "false") {
				    $(cmpCredentialSmtpRow).hide();
				} else {
				    $($('#cmpCredentialSMTP0').parents('tr')[0]).show();
				}

				$(useCredentialsForSmtp).click(handleSmtpCredentialsUsing);  
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                
				editApp.updateMultiModelFromUi($('#cmpSendEmailFrom' + cmpId), cmp.Settings, "EmailFrom", cmpIds);
				editApp.updateMultiModelFromUi($('#cmpSendEmailTo' + cmpId), cmp.Settings, "EmailTo", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpPOP3Port' + cmpId), cmp.Settings, "Pop3PortNumber", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpPOP3Encryption' + cmpId), cmp.Settings, "Pop3Ssl", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpSMTPServer' + cmpId), cmp.Settings, "SmtpSvr", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpSMTPPort' + cmpId), cmp.Settings, "SmtpPortNumber", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpUseCredentialsForSMTP' + cmpId), cmp.Settings, "SmtpAuth", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpSMTPEncryption' + cmpId), cmp.Settings, "SmtpSsl", cmpIds);
            	editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
            	editApp.updateMultiModelFromUi($('#cmpCredentialSMTP' + cmpId), cmp.Settings, "_CredentialSetId_SMTP", cmpIds);
			};
        	
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	<% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            var _model;
            var handleSmtpCredentialsUsing = function() {
                var useCredentialsForSmtpCheckBox = $('#cmpUseCredentialsForSMTP' + cmpId);
                var credentialsSmtpDropdown = $($('#cmpCredentialSMTP' + cmpId).parents('tr')[0]);

                if (useCredentialsForSmtpCheckBox[0].checked == false) {
                    credentialsSmtpDropdown.hide();
                } else {
                    credentialsSmtpDropdown.show();
                }
            };

            var precessSmtpCredentialsVisibilty = function(cmp) {
                var useCredentialsForSmtp = $('#cmpUseCredentialsForSMTP' + cmpId);
               
                if (cmp.Settings.SmtpAuth.Value.toString() == "false") {
                    $($('#cmpCredentialSMTP' + cmpId).parents('tr')[0]).hide();
                } else {
                    $($('#cmpCredentialSMTP' + cmpId).parents('tr')[0]).show();
                }
               
                $(useCredentialsForSmtp).click(handleSmtpCredentialsUsing);
            };
            
            var onLoad = function(model) {
                _model = model;
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpSendEmailFrom' + cmpId), 'EmailFrom', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSendEmailTo' + cmpId), 'EmailTo', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpPOP3Port' + cmpId), 'Pop3PortNumber', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpPOP3Encryption' + cmpId), 'Pop3Ssl', cmp.Settings, templateSettings] },                
                    { func: editApp.initUiState, args: [$('#cmpSMTPServer' + cmpId), 'SmtpSvr', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSMTPPort' + cmpId), 'SmtpPortNumber', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpUseCredentialsForSMTP' + cmpId), 'SmtpAuth', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSMTPEncryption' + cmpId), 'SmtpSsl', cmp.Settings, templateSettings] },                
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {

                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },
                    {
                        func: editApp.addSMTPCredentialSets,
                        args: [$('#cmpCredentialSMTP' + cmpId), function() {
                            editApp.initUiState($('#cmpCredentialSMTP' + cmpId), '_CredentialSetId_SMTP', cmp.Settings, templateSettings);
                        }]
                    },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
                precessSmtpCredentialsVisibilty(cmp);
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateModelFromUi($('#cmpSendEmailFrom' + cmpId), cmp.Settings.EmailFrom, isTemplate);
                editApp.updateModelFromUi($('#cmpSendEmailTo' + cmpId), cmp.Settings.EmailTo, isTemplate);
                editApp.updateModelFromUi($('#cmpPOP3Port' + cmpId), cmp.Settings.Pop3PortNumber, isTemplate);
                editApp.updateModelFromUi($('#cmpPOP3Encryption' + cmpId), cmp.Settings.Pop3Ssl, isTemplate);
                editApp.updateModelFromUi($('#cmpSMTPServer' + cmpId), cmp.Settings.SmtpSvr, isTemplate);
                editApp.updateModelFromUi($('#cmpSMTPPort' + cmpId), cmp.Settings.SmtpPortNumber, isTemplate);
                editApp.updateModelFromUi($('#cmpUseCredentialsForSMTP' + cmpId), cmp.Settings.SmtpAuth, isTemplate);
                editApp.updateModelFromUi($('#cmpCredentialSMTP' + cmpId), cmp.Settings._CredentialSetId_SMTP, isTemplate);
                editApp.updateModelFromUi($('#cmpSMTPEncryption' + cmpId), cmp.Settings.SmtpSsl, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId), cmp.Thresholds.Response, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
            };
            var onToolbarCredentialsClick = function(record) {
                if(record.id === cmpId){
                    var elBtn = $(SF("#overridecmpCredentialSMTP{0}", record.id));
                    if (elBtn.size() > 0 && elBtn.text() == "Override Template") {
                        elBtn.attr("ignoreOverrideClick", "true");
                        elBtn.click();
                        elBtn.removeAttr("ignoreOverrideClick");
                    }
                    var val = record.get("CredentialSetId").Value.toString(), elSel = $(SF("#cmpCredentialSMTP{0}", record.id));
                    if (elSel.size() > 0 && elSel.val() != val) {
                        if (elSel.find(SF("[value={0}]", val)).size() == 0) {
                            var newCred = _model.Credentials.slice(-1)[0];
                            elSel.append(SF("<option value={0}>{1}</option>", val, newCred.Name));
                        }
                        elSel.val(val);
                    }
                }
            }

            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
            SW.APM.EventManager.on("onToolbarCredentialsClick", onToolbarCredentialsClick);
        })();
	<% }%>
    </script>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SendEmailFrom %>" BaseValueName="cmpSendEmailFrom" ElementId="<%#Master.ComponentId%>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SendEmailTo %>" BaseValueName="cmpSendEmailTo" ElementId="<%#Master.ComponentId%>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_POP3Port %>" BaseValueName="cmpPOP3Port" ElementId="<%# Master.ComponentId%>" MinValue="0" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_POP3Encryption %>" BaseValueName="cmpPOP3Encryption" ElementId="<%#  Master.ComponentId %>" 
			Options="<%# DropdownOptionsHelper.OptionsFromEnum<UseSSLSettings>()%>" EditorMode="<%# Master.EditMode%>" runat="server"/>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SMTPServer %>" BaseValueName="cmpSMTPServer" ElementId="<%#Master.ComponentId%>" IsMultiline="false"  EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SMTPPort %>" BaseValueName="cmpSMTPPort" ElementId="<%# Master.ComponentId%>" MinValue="0" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseCredentialsforSMTP %>" BaseValueName="cmpUseCredentialsForSMTP" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CredentialforSMTP %>" BaseValueName="cmpCredentialSMTP"  ElementId="<%# Master.ComponentId%>" EditorMode ="<%#Master.EditMode%>" runat="server" />
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SMTPEncryption %>" BaseValueName="cmpSMTPEncryption" ElementId="<%#  Master.ComponentId %>" 
			Options="<%# DropdownOptionsHelper.OptionsFromEnum<UseSSLSettings>()%>" EditorMode="<%# Master.EditMode%>" runat="server"/>
		<apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId%>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>
