﻿using SolarWinds.APM.Common.Helpers;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common;
using SolarWinds.Orion.Web.Helpers;

using System;
using System.Collections.Generic;
using System.Linq;


public partial class Orion_APM_Admin_Edit_ComponentEditors_ComponentEditorBase : ApmMasterPage
{

    public List<String> ThresholdKeys { get; private set; }

    public string CustomApplicationType { get; private set; }

    // used for hiding threshold editor on the edit application page - ExchgBB needs
    public bool HideThresholdsForEditApplication { get; private set; }

    // used for hiding thresholds' sustained status controls
    public bool HideThresholdsSustainedStatusControls { get; private set; }

    public bool ShowCredentialsAlways { get; private set; }


    public string ThresholdKeysJson
    {
        get
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this.ThresholdKeys);
        }
    }


    public string ComponentId
    {
        get
        {
            var id = WebSecurityHelper.SanitizeHtmlV2(this.Request.QueryString["Id"]);
            return String.IsNullOrWhiteSpace(id) ? "0" : id;
        }
    }

    public bool IsBlackBoxEdit
    {
        get
        {
            var qs = WebSecurityHelper.SanitizeHtmlV2(this.Request.QueryString["IsBlackBox"]);
            return ParseHelper.TryParse(qs, false);
        }
    }

    public string ApplicationItemId
    {
        get
        {
            var appItemId = WebSecurityHelper.SanitizeHtmlV2(this.Request.QueryString["ApplicationItemId"]);
            return string.IsNullOrWhiteSpace(appItemId) ? "0" : appItemId;
        }
    }

    public bool IsTemplate
    {
        get
        {
            var qs = WebSecurityHelper.SanitizeHtmlV2(this.Request.QueryString["IsTemplate"]);
            return ParseHelper.TryParse(qs, false);
        }
    }

    public bool ShowThresholds
    {
        get
        {
            return this.IsTemplate || this.IsApplicationItemEdit || !this.HideThresholdsForEditApplication;
        }
    }

    public bool IsApplicationItemEdit
    {
        get
        {
            int id;
            return Int32.TryParse(this.ApplicationItemId, out id) && id != 0;
        }
    }

    public string ComponentIds
    {
        get
        {
            var ids = WebSecurityHelper.SanitizeHtmlV2(this.Request.QueryString["ids"]);
            if (string.IsNullOrWhiteSpace(ids))
            {
                return "[]";
            }
            return string.Format("[{0}]", ids);
        }
    }

    public SettingEditorControlBase.EditMode EditMode
    {
        get
        {
            var qs = WebSecurityHelper.SanitizeHtmlV2(this.Request.QueryString["mode"]);
            return ParseHelper.TryParseEnum(qs, SettingEditorControlBase.EditMode.Single);
        }
    }

    public bool IsSingleEditMode
    {
        get { return this.EditMode == SettingEditorControlBase.EditMode.Single; }
    }

    public bool IsMultiEditMode
    {
        get { return this.EditMode == SettingEditorControlBase.EditMode.Multi; }
    }


    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ThresholdKeys = new List<string>();
        this.DataBind();

        this.ComponentTypePlaceholder.Visible = !this.IsBlackBoxEdit || ComponentConstants.IsDevMode;

        this.edtEnableComponent.Visible = this.IsSingleEditMode;
        this.edtDescription.Visible = this.IsSingleEditMode;
        this.edtUserNotes.Visible = this.IsSingleEditMode;

        //BlackBox components have dynamic thresholds (SQL Table Probe). it has to be laoded from DB, but it is editable only in SingleEdit mode
        if (!this.IsSingleEditMode)
        {
            return;
        }

        long componentTemplateId;
        if (!this.IsBlackBoxEdit || !long.TryParse(this.ComponentId, out componentTemplateId))
        {
            return;
        }

        //Newly added components have negative ids - -1, -2, -3, ...
        if (componentTemplateId < 0)
        {
            return;
        }

        using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            if (!this.IsTemplate)
            {
                componentTemplateId = businessLayer.GetComponent(componentTemplateId).TemplateId;
            }

            var componentTemplate = businessLayer.GetComponentTemplate(componentTemplateId);
            var dal = new SolarWinds.APM.Web.DAL.ApplicationTemplateDAL();
            var template = dal.GetTemplateInfo(componentTemplate.ApplicationTemplateId);

            if (template != null)
            {
                this.CustomApplicationType = template.CustomAppType;
            }

            this.ThresholdKeys = componentTemplate.Thresholds.Keys.ToList();
            if (componentTemplate.Settings.Keys.Contains(InternalSettings.HideThresholdsForEditApplication))
            {
                this.HideThresholdsForEditApplication = componentTemplate.Settings[InternalSettings.HideThresholdsForEditApplication].GetValue<bool>();
            }

            if (componentTemplate.Settings.Keys.Contains(InternalSettings.HideThresholdsSustainedStatusControls))
            {
                this.HideThresholdsSustainedStatusControls = componentTemplate.Settings[InternalSettings.HideThresholdsSustainedStatusControls].GetValue<bool>();
            }

            if (componentTemplate.Settings.Keys.Contains(InternalSettings.ShowCredentialsAlways))
            {
                this.ShowCredentialsAlways = componentTemplate.Settings[InternalSettings.ShowCredentialsAlways].GetValue<bool>();
            }
        }
    }
}
