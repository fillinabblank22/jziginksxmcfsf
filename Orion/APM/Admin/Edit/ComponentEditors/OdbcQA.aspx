﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor ComponentType="OdbcQA" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
         <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["OdbcQA"];
			    editApp.initMultiUiState($("#cmpConnectionString" + cmpId), cmp.DefinitionSettings, "ConnectionString");
			    editApp.initMultiUiState($("#cmpSqlQuery" + cmpId), cmp.DefinitionSettings, "SqlQuery");
			    editApp.initMultiUiState($("#cmpQueryTimeout" + cmpId), cmp.DefinitionSettings, "QueryTimeout");
			    editApp.initMultiUiState($("#cmpCountStatisticDifference" + cmpId), cmp.DefinitionSettings, "CountAsDifference");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                editApp.updateMultiModelFromUi($("#cmpConnectionString" + cmpId), cmp.Settings, "ConnectionString", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpSqlQuery" + cmpId), cmp.Settings, "SqlQuery", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpQueryTimeout" + cmpId), cmp.Settings, "QueryTimeout", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCountStatisticDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;

            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },                
                    { func: editApp.initUiState, args: [$('#cmpConnectionString' + cmpId), 'ConnectionString', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSqlQuery' + cmpId), 'SqlQuery', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpQueryTimeout' + cmpId), 'QueryTimeout', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpCountStatisticDifference' + cmpId), 'CountAsDifference', cmp.Settings, templateSettings] },                
                    { func: editApp.initDataTransform, args: [$('#cmpConvertValue' + cmpId), cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                
                editApp.updateModelFromUi($('#cmpConnectionString' + cmpId), cmp.Settings.ConnectionString, isTemplate);
                editApp.updateModelFromUi($('#cmpSqlQuery' + cmpId), cmp.Settings.SqlQuery, isTemplate);
                editApp.updateModelFromUi($('#cmpQueryTimeout' + cmpId), cmp.Settings.QueryTimeout, isTemplate);
                editApp.updateModelFromUi($('#cmpCountStatisticDifference' + cmpId), cmp.Settings.CountAsDifference, isTemplate);
                
                editApp.updateDataTransformModelFromUi($('#cmpConvertValue' + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId),cmp.Thresholds.Response, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ConnectionString %>" BaseValueName="cmpConnectionString" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" IsMultiline="true"  />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SqlQuery %>" BaseValueName="cmpSqlQuery" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" IsMultiline="true"  />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_QueryTimeout %>" BaseValueName="cmpQueryTimeout" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="0" runat="server"  />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountStatisticDifference" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode%>" />  
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
 </asp:Content>