﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType virtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master"%>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models.LogFile" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="Resources" %>

<asp:Content ContentPlaceHolderID="ComponentTypePlaceholder" runat="Server">
    <apm:ComponentTypeEditor ComponentType="LogFile" runat="server"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="ComponentEditorPlaceholder" runat="Server">
    <orion:Include File="APM/Admin/Edit/Js/ComponentEditors/LogFile.js" runat="server" />
    <apm:ComponentEditorGroupSeparator runat="server"/>
    <tbody class="apmComponentEditorGroup">
        <apm:ComponentEditorGroupTitle runat="server"
            Title="<%$ Resources: APMWebContent, EditComponentEditors_ComponentEditorGroupTitle_Title %>"
            SubTitle="<%$ Resources: APMWebContent, EditComponentEditors_ComponentEditorGroupTitle_SubTitle %>" />
        <apm:RadioButtonEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FileDirectoryMatchDetails %>" BaseValueName="cmpMatchOnType" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<LogFileProbeMatchOnType>()%>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_DirectoryPath %>" BaseValueName="cmpDirectoryPath" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Filename %>" BaseValueName="cmpFilePattern" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:BooleanCheckBoxEditor DisplayText="" HelpTextBottom="<%# Resources.APMWebContent.EditComponentEditors_MatchIsCaseInsensitive %>" CheckBoxLabel="<%$ Resources: APMWebContent, EditComponentEditors_UseRegex %>" BaseValueName="cmpFilePatternUseRegex" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <tr><td colspan="3">
            <!-- { file pattern hint -->
        <div class="apmPatternHint_FilePattern" title="<%= APMWebContent.LogFileMonitor_CommonFilenameExamplesTitle %>">
            <%= APMWebContent.LogFileMonitor_CommonFilenameExamplesContent.Replace("${URL_HELP}", HelpHelper.GetHelpUrl("SAMAGLogFileMonitorFilePattern")) %>
        </div>
            <!-- file pattern hint } -->
        </td></tr>
        <apm:ComponentEditorGroupFooter runat="server"/>
    </tbody>
    <apm:ComponentEditorGroupSeparator runat="server"/>
    <tbody class="apmComponentEditorGroup">
        <apm:ComponentEditorGroupTitle runat="server"
            Title="What Do You Want to Watch for in the File?" />
        <apm:StringEditor DisplayText="Search Pattern" BaseValueName="cmpSearchPattern" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:BooleanCheckBoxEditor DisplayText="" HelpTextBottom="<%# Resources.APMWebContent.LogFileEditor_SearchIsCaseInsensitive %>" CheckBoxLabel="<%$ Resources: APMWebContent, EditComponentEditors_UseRegex %>" BaseValueName="cmpSearchPatternUseRegex" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:ComponentEditorGroupFooter runat="server"/>
    </tbody>
    <apm:ComponentEditorGroupSeparator runat="server"/>
    <tbody class="apmComponentEditorGroup">
        <apm:ComponentEditorGroupTitle runat="server"
            Title="<%$ Resources: APMWebContent, EditComponentEditors_ApmComponentEditGroup_Title %>"
            SubTitle="<%$ Resources: APMWebContent, EditComponentEditors_ApmComponentEditGroup_SubTitle %>"/>
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ChangeComponentMonitorStatusTo %>" BaseValueName="cmpStatusType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<LogFileProbeStatusType>() %>" runat="server"/>
        <apm:ComponentEditorGroupFooter runat="server"/>
    </tbody>
    <apm:ComponentEditorGroupSeparator runat="server"/>
    <script type="text/javascript">
        SW.APM.EditApp.Editors.LogFile.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>');
    </script>
</asp:Content>
