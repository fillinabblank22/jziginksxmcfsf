﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor ComponentType="LdapUserExperience" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["LdapUserExperience"];
			    editApp.initMultiUiState($("#cmpPortNumber" + cmpId), cmp.DefinitionSettings, "Port");
			    editApp.initMultiUiState($("#cmpEncryptionMethod" + cmpId), cmp.DefinitionSettings, "EncryptionMethod");
			    editApp.initMultiUiState($("#cmpAuthenticationMethod" + cmpId), cmp.DefinitionSettings, "AuthenticationMethod");
                editApp.initMultiUiState($("#cmpLDAPSearchRoot" + cmpId), cmp.DefinitionSettings, "LdapPath");
			    editApp.initMultiUiState($("#cmpRealmuserDomain" + cmpId), cmp.DefinitionSettings, "Realm");
                editApp.initMultiUiState($("#cmpLDAPFilter" + cmpId), cmp.DefinitionSettings, "LdapFilter");
                editApp.initMultiUiState($("#cmpLDAPAttributes" + cmpId), cmp.DefinitionSettings, "LdapAttributes");
			    editApp.initMultiUiState($("#cmpLDAPSizeLimit" + cmpId), cmp.DefinitionSettings, "LdapSizeLimit");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
			    editApp.updateMultiModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings, "Port", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpEncryptionMethod" + cmpId), cmp.Settings, "EncryptionMethod", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpAuthenticationMethod" + cmpId), cmp.Settings, "AuthenticationMethod", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpLDAPSearchRoot" + cmpId), cmp.Settings, "LdapPath", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpRealmuserDomain" + cmpId), cmp.Settings, "Realm", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpLDAPFilter" + cmpId), cmp.Settings, "LdapFilter", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpLDAPAttributes" + cmpId), cmp.Settings, "LdapAttributes");
                editApp.updateMultiModelFromUi($("#cmpLDAPSizeLimit" + cmpId), cmp.Settings, "LdapSizeLimit");
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;

            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },
                    { func: editApp.initUiState, args: [$('#cmpPortNumber' + cmpId), 'Port', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpEncryptionMethod' + cmpId), 'EncryptionMethod', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpAuthenticationMethod' + cmpId), 'AuthenticationMethod', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPSearchRoot' + cmpId), 'LdapPath', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpRealmuserDomain' + cmpId), 'Realm', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPFilter' + cmpId), 'LdapFilter', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPAttributes' + cmpId), 'LdapAttributes', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPSizeLimit' + cmpId), 'LdapSizeLimit', cmp.Settings, templateSettings] },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initDataTransform, args: [$('#cmpConvertValue' + cmpId), cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
               });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateModelFromUi($('#cmpPortNumber' + cmpId), cmp.Settings.Port, isTemplate);
                editApp.updateModelFromUi($('#cmpEncryptionMethod' + cmpId), cmp.Settings.EncryptionMethod, isTemplate);
                editApp.updateModelFromUi($('#cmpAuthenticationMethod' + cmpId), cmp.Settings.AuthenticationMethod, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPSearchRoot' + cmpId), cmp.Settings.LdapPath, isTemplate);
                editApp.updateModelFromUi($('#cmpRealmuserDomain' + cmpId), cmp.Settings.Realm, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPFilter' + cmpId), cmp.Settings.LdapFilter, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPAttributes' + cmpId), cmp.Settings.LdapAttributes, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPSizeLimit' + cmpId), cmp.Settings.LdapSizeLimit, isTemplate);
                editApp.updateDataTransformModelFromUi($('#cmpConvertValue' + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId),cmp.Thresholds.Response, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" MinValue="0"/> 
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_EncryptionMethod %>" BaseValueName="cmpEncryptionMethod" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" 
			Options="<%# DropdownOptionsHelper.OptionsFromEnum<UseLdapEncryptionSettings>()%>" runat="server"/>
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AuthenticationMethod %>" BaseValueName="cmpAuthenticationMethod" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" 
			Options="<%# DropdownOptionsHelper.OptionsFromEnum<UseLdapAuthenticationSettings>()%>" runat="server"/>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_RealmUserDomain %>" BaseValueName="cmpRealmuserDomain" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPSearchRoot %>" BaseValueName="cmpLDAPSearchRoot" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPFilter %>" BaseValueName="cmpLDAPFilter" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPAttributes %>" BaseValueName="cmpLDAPAttributes" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />    
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPSizeLimit %>" BaseValueName="cmpLDAPSizeLimit" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" MinValue="1"/>
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />  
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
 </asp:Content>