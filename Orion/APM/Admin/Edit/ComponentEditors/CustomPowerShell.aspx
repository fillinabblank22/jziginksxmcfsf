﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="CustomPowerShell" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" runat="Server">

    <asp:PlaceHolder runat="server" Visible="<%# this.Master.Master.IsSingleEditMode %>">
        <asp:PlaceHolder runat="server" Visible="<%# SolarWinds.APM.Web.ComponentConstants.IsDevMode %>">
            <asp:PlaceHolder runat="server" Visible="<%# this.Master.Master.IsTemplate %>">
                <apm:booleaneditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_CanBeDisabled %>" basevaluename="cmpCanBeDisabled" elementid="<%# Master.ComponentId %>" truedisplayvalue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" falsedisplayvalue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" iscomplexsetting="true" runat="server" />
                <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VisibilityMode %>" BaseValueName="cmpVisibilityMode" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ComponentVisibilityMode>() %>" EditorMode="<%# Master.EditMode%>" runat="server" />
            </asp:PlaceHolder>
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Script %>" BaseValueName="cmpScript" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%#Master.EditMode%>" IsMultiline="true" />
        </asp:PlaceHolder>
    </asp:PlaceHolder>

    <apm:ThresholdEditorList runat="server" ID="thresholdList" />

    <script type="text/javascript">
        SW.APM.EditApp.Editors.CustomPowerShell.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>',
            '<%= SolarWinds.APM.Web.ComponentConstants.IsDevMode %>',
            '<%= this.Master.IsTemplate %>',
            '<%= this.Master.Master.IsBlackBoxEdit %>',
            '<%= this.Master.ThresholdKeysJson %>');
    </script>

</asp:Content>
