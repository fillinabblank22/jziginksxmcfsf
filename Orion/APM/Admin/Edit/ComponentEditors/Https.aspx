﻿<%@ Page AutoEventWireup="true" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<%@ Reference VirtualPath="ComponentEditorBase.master" %>
<%@ MasterType VirtualPath="ComponentEditorWithCredentials.master" %>

<script runat="server">
    protected bool IsDevMode
    {
        get { return !this.Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.edtPortNumber.Visible = this.IsDevMode;
        this.edtHostRequests.Options = DropdownOptionsHelper.OptionsFromEnum<HostRequests>();
        this.edtAuthMode.Options = DropdownOptionsHelper.OptionsFromEnum<AuthMode>();
    }
</script>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="Https"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <apm:NumericEditor ID="edtPortNumber" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" MinValue="0" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Url %>" BaseValueName="cmpUrl" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HostHeader %>" BaseValueName="cmpHostHeader" Required="false" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode %>" />
    <apm:DropdownEditor ID="edtHostRequests" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HostRequest %>" BaseValueName="cmpHostRequest" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode %>" IsComplexSetting="true" runat="server" />
    <tbody id="postPutCnt<%=Master.ComponentId%>" class="subEditor">
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ContentType %>" BaseValueName="cmpContentType" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode %>" Required="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_RequestBody %>" BaseValueName="cmpRequestBody" ElementId="<%# Master.ComponentId  %>" EditorMode ="<%# Master.EditMode %>" Required="false" MaxLength="1048576" IsMultiline="true" runat="server" />
    </tbody>
    <tbody id="getHeadCnt<%=Master.ComponentId%>" class="subEditor">
        <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FollowRedirect %>" BaseValueName="cmpFollowRedirect" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, Edit_Follow %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_DoNotFollow %>" runat="server" />
    </tbody>
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseProxy %>" BaseValueName="cmpUseProxy" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_UseProxy %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_DoNotUseProxy %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ProxyAddress %>" BaseValueName="cmpProxyAddress" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CertificateSubject %>" BaseValueName="cmpCertificateSubject" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsMultiline="true" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCAErrors %>" BaseValueName="cmpIgnoreCA" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCAErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCAErrors %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCNErrors %>" BaseValueName="cmpIgnoreCN" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCNErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCNErrors %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCRLErrors %>" BaseValueName="cmpIgnoreCRL" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCRLErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCRLErrors %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UserAgent %>" BaseValueName="cmpUserAgent" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SearchString %>" BaseValueName="cmpSearchString" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FailIfFound %>" BaseValueName="cmpFailIfFound" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HeadRequest %>" BaseValueName="cmpHeadRequest" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, Edit_RequestHeadersOnly %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_FullRequest %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AcceptCompression %>" BaseValueName="cmpAcceptCompression" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" runat="server" />
    <apm:DropdownEditor ID="edtAuthMode" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AuthenticateMode %>" BaseValueName="cmpAuthMode" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />

    <script type="text/javascript">
        SW.APM.EditApp.Editors.Https.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId%>',
            '<%= this.Master.ComponentIds %>',
            '<%= this.IsDevMode %>');
    </script>
</asp:Content>
