﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.Plugins" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Management.WinRm" %>

<script runat="server">
    protected void Page_Init(object sender, EventArgs e)
    {
        var isDev = (this.Master.Master.IsBlackBoxEdit == false || ComponentConstants.IsDevMode);
        this.edtCommandLineFilter.Visible = isDev;
        this.edtProcessName.Visible = isDev;
        this.edtNotRunningStatusMode.Visible = isDev;
    }
</script>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
   <apm:ComponentTypeEditor runat="server" ComponentType="Process"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <% 
    if (ComponentConstants.IsDevMode)
    {
        WebPluginManager.Instance.CustomizeComponentEditors(Master.CustomApplicationType, cmpMinRequestingVersion, cmpMaxRequestingVersion);
    }
    %>

    <script type="text/javascript">
        SW.APM.EditApp.Editors.Process.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>',
            '<%= this.Master.Master.IsBlackBoxEdit == false || ComponentConstants.IsDevMode %>',
            '<%= this.cmpMinRequestingVersion.Visible %>',
            '<%= this.cmpMaxRequestingVersion.Visible %>'
            );
    </script>
    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FetchingMethod %>" BaseValueName="cmpFetchingMethod" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ProcessInfo_FetchingMethod>()%>" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:DropdownEditor ID="ctrWinRmAuthenticationMechanism" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WinRmAuthenticationMechanism %>" BaseValueName="cmpWinRmAuthenticationMechanism" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<WinRmAuthenticationMechanism>() %>" EditorMode="<%# Master.EditMode %>" runat="server" />   
    <apm:StringEditor ID="edtCommandLineFilter" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CommandLineFilter %>" BaseValueName="cmpCommandLineFilter" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>" Required="false" runat="server" />
    <apm:StringEditor ID="edtProcessName" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ProcessName %>" BaseValueName="cmpProcessName" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
    <apm:DropdownEditor ID="edtNotRunningStatusMode" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_StatusWhenProcessIsNotRunning %>" BaseValueName="cmpNotRunningStatusMode" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ProcessInfo_NotRunningStatus>()%>" EditorMode="<%# Master.EditMode%>" HelpTextBottom="..." runat="server" />
    <apm:StringEditor ID="cmpMinRequestingVersion" BaseValueName="cmpMinRequestingVersion" Visible="False" Required="False" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:StringEditor ID="cmpMaxRequestingVersion" BaseValueName="cmpMaxRequestingVersion" Visible="False" Required="False" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CPU %>" BaseValueName="cmpCPUThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"  runat="server"  />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PhysicalMemory %>" BaseValueName="cmpPMemThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>"  runat="server"  />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VirtualMemory %>" BaseValueName="cmpVMemThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"  runat="server"  />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IOReadOperationsSec %>" BaseValueName="cmpIOReadOPSThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"  runat="server"  />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IOWriteOperationsSec %>" BaseValueName="cmpIOWriteOPSThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"  runat="server"  />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IOTotalOperationsSec %>" BaseValueName="cmpIOTotalOPSThreshold" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"  runat="server"  />
</asp:Content>
