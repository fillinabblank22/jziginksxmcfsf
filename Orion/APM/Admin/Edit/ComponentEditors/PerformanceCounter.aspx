﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference Control="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>

<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.DAL" %>
<%@ Import Namespace="SolarWinds.APM.Common" %>
<%@ Import Namespace="SolarWinds.APM.Common.Management.WinRm" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<script runat="server">

    protected bool IsEditable
    {
        get { return !this.Master.Master.IsBlackBoxEdit || ComponentConstants.IsDevMode; }
    }

    protected bool IsBlackBoxTemplate
    {
        get { return this.Master.Master.IsBlackBoxEdit && this.Master.Master.IsTemplate; }
    }

    protected string GetHelpMessage()
    {
        string template = @"<a href=""{0}"" target=""_blank"">"+ Resources.APMWebContent.EditComponentEditors_HelpMeToChoose + @"</a>
 <div class=""warning-message"">" +  Resources.APMWebContent.EditComponentEditors_PerformanceCounter_WarningMessage + @"</div>";
        var url = SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionSAMPerfCounterFetch");
        return InvariantString.Format(template, url);
    }

</script>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="PerformanceCounter"/>
</asp:Content>

<asp:Content  ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">

    <asp:PlaceHolder runat="server" Visible="<%# this.IsEditable %>">
        <asp:PlaceHolder runat="server" Visible="<%# this.IsBlackBoxTemplate %>">
		    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ComponentCategory %>" BaseValueName="cmpComponentCategory" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.InsertNone(ComponentCategoryDAL.GetAllCategoryPairs()) %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        </asp:PlaceHolder>
        <apm:BooleanCheckboxEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Counter %>" BaseValueName="cmpCounter" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server"  />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Instance %>" BaseValueName="cmpInstance" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server"  />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Category %>" BaseValueName="cmpCategory" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server"  />
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>"  runat="server" />
        <apm:DropdownEditor ID="ctrPreferredPollingMethod" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FetchingMethod %>" HelpTextBottom="<%# this.GetHelpMessage() %>" BaseValueName="cmpPreferredPollingMethod" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<PerformanceCounterPreferredPollingMethod>() %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:DropdownEditor ID="ctrWinRmAuthenticationMechanism" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WinRmAuthenticationMechanism %>" BaseValueName="cmpWinRmAuthenticationMechanism" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<WinRmAuthenticationMechanism>() %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    </asp:PlaceHolder>

    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server"  />

    <script type="text/javascript">
        SW.APM.EditApp.Editors.PerformanceCounter.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>',
            '<%= this.IsEditable %>',
            '<%= this.IsBlackBoxTemplate %>');
    </script>

</asp:Content>
