﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference Control="ComponentEditorBase.master" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Management.WinRm" %>

<asp:Content ID="c1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="WindowsEventLog" runat="server" />
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">

    <script type="text/javascript">
<% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            var Editors = SW.APM.EditApp.Editors;

        	var toggleSubEditor = function(el, visible) {
        		if (visible) {
        			el.show().find("*[apmsettctr=true]").each(function() {
        				var subel = $(this);
        				if (subel.is(":visible") && subel.parents().size() == subel.parents(":visible").size()) {
        					subel.addClass("required");
        				}
        			});
        		} else {
        			el.hide().find("*[apmsettctr]").removeClass("required");
        		}
        	};
        	var onStateChanged = function() {
        		var el = $("#cmpLogToMonitor" + cmpId);
        		el.unbind("change").bind("change", showSubEditorForLogToMonitor);
        		el.change();

        		el = $("#cmpMatchDefinition" + cmpId);
        		el.unbind("change").bind("change", showSubEditorForMatchDefinition);
        		el.change();

        		el = $("#cmpEntryIDType" + cmpId);
        		el.unbind("change").bind("change", showSubEditorForEntryIDType);
        		el.change();

        		el = $("#cmpFetchingMethod" + cmpId);
        		el.unbind("change").bind("change", onFetchingMethodChanged);
        		el.change();

        		el = $("#cmpEntryIncludeOperation" + cmpId);
        		el.unbind("change").bind("change", onIncludeExcludeOperationChanged);
        		el.change();
        		el = $("#cmpEntryExcludeOperation" + cmpId);
        		el.unbind("change").bind("change", onIncludeExcludeOperationChanged);
        		el.change();
        	};

			var showSubEditorForLogToMonitor = function () {
				var el = $(this), editor = el.parent(), subEditor = el.parents("tbody:first").next("tbody.subEditor");
				var multiEditCheckBoxContainer= editor.parents('tr')[0];
				var multiEditCheckBox = $(multiEditCheckBoxContainer).find("[id*=MultiChekbox]");
			
				var multiEditSubEditorDisable = function() {
					disableSubEditor(multiEditCheckBox[0].id, subEditor, editor);
				};
				$(multiEditCheckBox).click(multiEditSubEditorDisable);
				multiEditSubEditorDisable();
				
				if (editor.is(":visible")) {
					toggleSubEditor(subEditor, el.val() == "Custom"); 
				} else {
					toggleSubEditor(subEditor, editor.next(".viewer").text() == "Custom"); 
				}
			};
			var showSubEditorForMatchDefinition = function () {
				var el = $(this), editor = el.parent(), subEditor = el.parents("tbody:first").next("tbody.subEditor");
				var multiEditCheckBoxContainer= editor.parents('tr')[0];
				var multiEditCheckBox = $(multiEditCheckBoxContainer).find("[id*=MultiChekbox]");
			
				var multiEditSubEditorDisable = function() {
					disableSubEditor(multiEditCheckBox[0].id, subEditor, editor);
				};
				$(multiEditCheckBox).click(multiEditSubEditorDisable);
				multiEditSubEditorDisable();
				
				if (editor.is(":visible")) {
					toggleSubEditor(subEditor, el.val() == "Custom"); 
				} else {
					toggleSubEditor(subEditor, editor.next(".viewer").text() == "Custom"); 
				}
				$("#cmpEntryIDType" + cmpId).change();
			};
        	var showSubEditorForEntryIDType = function() {
        		var el = $(this), editor = el.parent(), subEditor = el.next(".stringEditor");
        		if (editor.is(":visible") && editor.parents().size() == editor.parents(":visible").size()) {
        			toggleSubEditor(subEditor, el.val() != "AllEventIDs");
        		} else {
        			toggleSubEditor(subEditor, editor.next(".viewer").text() != "Find all IDs");
        		}
        	};
        	var onFetchingMethodChanged = function() {
                var el = $(this), editor = el.parent(), subEditor = el.next(".stringEditor"), help = editor.parents("tr.dropdownEditorContainer").next("tr.helpContainer");
                Editors.changeWinRmRowVisibility(el, 1);
        		if (editor.is(":visible") && editor.parents().size() == editor.parents(":visible").size()) {
        			if(el.val() == "Rpc") { help.show(); } else { help.hide(); }
        		} else {
        			if(editor.next(".viewer").text() == "RPC (Remote Procedure Call)") { help.show(); } else { help.hide(); }
        		}
        	};
        	var onIncludeExcludeOperationChanged = function() {
        		var el = $(this), editor = el.parent(), subEditor = el.next(".stringEditor").find("textarea,input");
        		if (editor.is(":visible") && editor.parents().size() == editor.parents(":visible").size()) {
        			if(el.val() == "Disable") { subEditor.attr("disabled", "disabled"); } else { subEditor.removeAttr("disabled"); }
        		} else {
        			if(editor.next(".viewer").text() == "Disable Keywords Matching") { subEditor.attr("disabled", "disabled"); } else { subEditor.removeAttr("disabled"); }
        		}
        	}

        	var disableSubEditor = function(multiEditCheckBox, subEditor, editor) {
        		var isMultiEditCheckChecked = $("#" + multiEditCheckBox).is(":checked");
        		if(!isMultiEditCheckChecked) {
        			var subEditorCheckBoxes=subEditor.find("[id*=MultiChekbox]");
        			subEditorCheckBoxes.each(function() {
        				var el = $(this);
        				if(el.is(':checked')) {
        					el.attr("checked", false);
        					el.click();
        					el.attr("checked", false);
        				}
        			});
        		}

        		if(isMultiEditCheckChecked) {
        			subEditor.find("[id*=MultiChekbox]").removeAttr("disabled");
        			editor.find("[id]").removeAttr("disabled");
        		} else {
        			subEditor.find("[id*=MultiChekbox]").attr("disabled", "disabled");
        			editor.find("[id]").attr("disabled", "disabled");
        		}
                return true;
			};
        	
			 var onLoad = function(app) {
			     var cmp = SW.APM.EditApp.ComponentDefinitions["WindowsEventLog"];
				<% if (!Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
				editApp.initMultiUiState($("#cmpFetchingMethod" + cmpId), cmp.DefinitionSettings, "FetchingMethod");
                editApp.initMultiUiState($("#cmpWinRmAuthenticationMechanism" + cmpId), cmp.DefinitionSettings, "WinRmAuthenticationMechanism");
				editApp.initMultiUiState($("#cmpLogToMonitor" + cmpId), cmp.DefinitionSettings, "LogName");
				editApp.initMultiUiState($("#cmpLogNameFilter" + cmpId), cmp.DefinitionSettings, "LogNameFilter");
				editApp.initMultiUiState($("#cmpMatchDefinition" + cmpId), cmp.DefinitionSettings, "EntryMatch");
                editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
				editApp.initMultiUiState($("#cmpEntrySource" + cmpId), cmp.DefinitionSettings, "EntrySource");
				editApp.initMultiUiState($("#cmpEntryIDType" + cmpId + "_String"), cmp.DefinitionSettings, "EntryID");
				editApp.initMultiUiState($("#cmpEntryIDType" + cmpId), cmp.DefinitionSettings, "EntryIDType");
				editApp.initMultiUiState($("#cmpEntryType" + cmpId), cmp.DefinitionSettings, "EntryType");
				editApp.initMultiUiState($("#cmpUsers" + cmpId), cmp.DefinitionSettings, "Users");
                editApp.initMultiUiState($("#cmpEntryIncludeOperation" + cmpId + "_String"), cmp.DefinitionSettings, "EntryIncludeFilter");
                editApp.initMultiUiState($("#cmpEntryIncludeOperation" + cmpId), cmp.DefinitionSettings, "EntryIncludeOperation");
				editApp.initMultiUiState($("#cmpEntryExcludeOperation" + cmpId + "_String"), cmp.DefinitionSettings, "EntryExcludeFilter");
                editApp.initMultiUiState($("#cmpEntryExcludeOperation" + cmpId), cmp.DefinitionSettings, "EntryExcludeOperation");
                editApp.initMultiUiState($("#cmpNumberOfFrequencies" + cmpId), cmp.DefinitionSettings, "NumberOfFrequencies");
                editApp.initMultiUiState($("#cmpCollectDetails" + cmpId), cmp.DefinitionSettings, "CollectDetails");
                
				<% } %>

                editApp.initMultiUiState($("#cmpStatusSetting" + cmpId), cmp.DefinitionSettings, "StatusSetting");

                <% if (Master.Master.IsBlackBoxEdit && !SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                editApp.initMultiUiState($("#cmpEntryIDType" + cmpId + "_String"), cmp.DefinitionSettings, "EntryID");
                editApp.initMultiUiState($("#cmpEntryIDType" + cmpId), cmp.DefinitionSettings, "EntryIDType");
                <% } %>

			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
				
			    onStateChanged();
             };
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
            	<% if (!Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                editApp.updateMultiModelFromUi($("#cmpFetchingMethod" + cmpId), cmp.Settings, "FetchingMethod", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpWinRmAuthenticationMechanism" + cmpId), cmp.Settings, "WinRmAuthenticationMechanism", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpLogToMonitor" + cmpId), cmp.Settings, "LogName", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpLogNameFilter" + cmpId), cmp.Settings, "LogNameFilter", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpMatchDefinition" + cmpId), cmp.Settings, "EntryMatch", cmpIds);
            	editApp.updateMultiModelFromUi($("#cmpEntrySource" + cmpId), cmp.Settings, "EntrySource", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
            	editApp.updateMultiModelFromUi($("#cmpEntryIDType" + cmpId + "_String"), cmp.Settings, "EntryID", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpEntryIDType" + cmpId), cmp.Settings, "EntryIDType", cmpIds);
            	editApp.updateMultiModelFromUi($("#cmpEntryType" + cmpId), cmp.Settings, "EntryType", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpUsers" + cmpId), cmp.Settings, "Users", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpEntryIncludeOperation" + cmpId), cmp.Settings, "EntryIncludeOperation", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpEntryIncludeOperation" + cmpId + "_String"), cmp.Settings, "EntryIncludeFilter", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpEntryExcludeOperation" + cmpId), cmp.Settings, "EntryExcludeOperation", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpEntryExcludeOperation" + cmpId + "_String"), cmp.Settings, "EntryExcludeFilter", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpNumberOfFrequencies" + cmpId), cmp.Settings, "NumberOfFrequencies", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpCollectDetails" + cmpId), cmp.Serrings, "CollectDetails", cmpIds);
				<% } %>

                <% if (Master.Master.IsBlackBoxEdit && !SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                editApp.updateMultiModelFromUi($("#cmpEntryIDType" + cmpId + "_String"), cmp.Settings, "EntryID", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpEntryIDType" + cmpId), cmp.Settings, "EntryIDType", cmpIds);
                <% } %>

                editApp.updateMultiModelFromUi($("#cmpStatusSetting" + cmpId), cmp.Settings, "StatusSetting", cmpIds);
                
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
			};
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        	
        	onStateChanged();
        })();
	<% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;
            var Editors = SW.APM.EditApp.Editors;
            var toggleSubEditor = function(el, visible) {
                if (visible) {
                    el.show().find("*[apmsettctr=true]").each(function() {
                        var subel = $(this);
                        if (subel.is(":visible") && subel.parents().size() == subel.parents(":visible").size()) {
                            subel.addClass("required");
                        }
                    });
                } else {
                    el.hide().find("*[apmsettctr]").removeClass("required");
                }
            };
            var onStateChanged = function() {
                var el = $("#cmpLogToMonitor" + cmpId);
                el.unbind("change").bind("change", showSubEditorForLogToMonitor);
                el.change();

                el = $("#cmpMatchDefinition" + cmpId);
                el.unbind("change").bind("change", showSubEditorForMatchDefinition);
                el.change();

                el = $("#cmpEntryIDType" + cmpId);
                el.unbind("change").bind("change", showSubEditorForEntryIDType);
                el.change();

                el = $("#cmpFetchingMethod" + cmpId);
                el.unbind("change").bind("change", onFetchingMethodChanged);
                el.change();
                
                el = $("#cmpEntryIncludeOperation" + cmpId);
                el.unbind("change").bind("change", onIncludeExcludeOperationChanged);
                el.change();
                el = $("#cmpEntryExcludeOperation" + cmpId);
                el.unbind("change").bind("change", onIncludeExcludeOperationChanged);
                el.change();
            };

			var showSubEditorForLogToMonitor = function () {
				var el = $(this), editor = el.parent(), subEditor = el.parents("tbody:first").next("tbody.subEditor");
				if (editor.is(":visible")) {
					toggleSubEditor(subEditor, el.val() == "Custom"); 
				} else {
					toggleSubEditor(subEditor, editor.next(".viewer").text() == "Custom"); 
				}
			};
			var showSubEditorForMatchDefinition = function () {
				var el = $(this), editor = el.parent(), subEditor = el.parents("tbody:first").next("tbody.subEditor");
				if (editor.is(":visible")) {
					toggleSubEditor(subEditor, el.val() == "Custom"); 
				} else {
					toggleSubEditor(subEditor, editor.next(".viewer").text() == "Custom"); 
				}
				$("#cmpEntryIDType" + cmpId).change();
			};
            var showSubEditorForEntryIDType = function() {
                var el = $(this), editor = el.parent(), subEditor = el.next(".stringEditor");
                if (editor.is(":visible") && editor.parents().size() == editor.parents(":visible").size()) {
                    toggleSubEditor(subEditor, el.val() != "AllEventIDs");
                } else {
                    toggleSubEditor(subEditor, editor.next(".viewer").text() != "Find all IDs");
                }
            };
            var onFetchingMethodChanged = function () {
                var el = $(this), editor = el.parent(), subEditor = el.next(".stringEditor"), help = editor.parents("tr.dropdownEditorContainer").next("tr.helpContainer");
                Editors.changeWinRmRowVisibility(el, 2);
            	if (editor.is(":visible") && editor.parents().size() == editor.parents(":visible").size()) {
            		if(el.val() == "Rpc") { help.show(); } else { help.hide(); }
            	} else {
            		if(editor.next(".viewer").text() == "RPC (Remote Procedure Call)") { help.show(); } else { help.hide(); }
            	}
            };
            var onIncludeExcludeOperationChanged = function() {
            	var el = $(this), editor = el.parent(), subEditor = el.next(".stringEditor").find("textarea,input");
            	if (editor.is(":visible") && editor.parents().size() == editor.parents(":visible").size()) {
            		if(el.val() == "Disable") { subEditor.attr("disabled", "disabled"); } else { subEditor.removeAttr("disabled"); }
            	} else {
            		if(editor.next(".viewer").text() == "Disable Keywords Matching") { subEditor.attr("disabled", "disabled"); } else { subEditor.removeAttr("disabled"); }
            	}
            }

            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
				var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    <% if (!Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                    { func: editApp.initUiState, args: [$("#cmpFetchingMethod" + cmpId), "FetchingMethod", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpWinRmAuthenticationMechanism" + cmpId), "WinRmAuthenticationMechanism", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpLogToMonitor" + cmpId), "LogName", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpLogNameFilter" + cmpId), "LogNameFilter", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpMatchDefinition" + cmpId), "EntryMatch", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntrySource" + cmpId), "EntrySource", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryIDType" + cmpId + "_String"), "EntryID", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryIDType" + cmpId), "EntryIDType", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryType" + cmpId), "EntryType", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpUsers" + cmpId), "Users", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryIncludeOperation" + cmpId + "_String"), "EntryIncludeFilter", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryIncludeOperation" + cmpId), "EntryIncludeOperation", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryExcludeOperation" + cmpId + "_String"), "EntryExcludeFilter", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryExcludeOperation" + cmpId), "EntryExcludeOperation", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpNumberOfFrequencies" + cmpId), "NumberOfFrequencies", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpCollectDetails" + cmpId), "CollectDetails", cmp.Settings, templateSettings] },
                    
                    {
                        func: editApp.addCredentialSets,
                        args: [$("#cmpCredential" + cmpId), function() {
                            editApp.initUiState($("#cmpCredential" + cmpId), "CredentialSetId", cmp, template);
                        }]
                    },
                    { func: editApp.initDataTransform, args: [$("#cmpConvertValue" + cmpId), cmp, template] },
                    <% } %>

                    <% if (Master.Master.IsBlackBoxEdit && !SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                    { func: editApp.initUiState, args: [$("#cmpEntryIDType" + cmpId + "_String"), "EntryID", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpEntryIDType" + cmpId), "EntryIDType", cmp.Settings, templateSettings] },
                    <% } %>

                    { func: editApp.initUiState, args: [$("#cmpStatusSetting" + cmpId), "StatusSetting", cmp.Settings, templateSettings] },                
                    
					{ func: editApp.initUiState, args: [$("#cmpDisabled" + cmpId), "IsDisabled", cmp, template] },                
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template<% if (Master.Master.IsBlackBoxEdit){ %>, true<% } %> ] },
                    { func: editApp.initUiState, args: [$("#cmpNotes" + cmpId), "UserNote", cmp, template] },                
                    { func: editApp.initThreshold, args: [$("#cmpStatisticThreshold" + cmpId), "StatisticData", cmp.Thresholds, templateThresholds] }
                ];
                
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];
                    func.apply(this, args);
				    onStateChanged();
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
					mLoading.hide(); 
					var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
					SW.APM.EditApp.ComponentsThatLoadingNow.splice(index, 1);
					SW.APM.EventManager.fire("onComponentLoading");
                });
                SW.APM.EditApp.Editors.hideEnableSettingIfNeeded(cmp, componentContainer);
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($("#cmpDesc" + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($("#cmpNotes" + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($("#cmpDisabled" + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateThresholdModelFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds.StatisticData, isTemplate);
                <% if (!Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                editApp.updateModelFromUi($("#cmpFetchingMethod" + cmpId), cmp.Settings.FetchingMethod, isTemplate);
                editApp.updateModelFromUi($("#cmpWinRmAuthenticationMechanism" + cmpId), cmp.Settings.WinRmAuthenticationMechanism, isTemplate);
                editApp.updateModelFromUi($("#cmpLogToMonitor" + cmpId), cmp.Settings.LogName, isTemplate);
                editApp.updateModelFromUi($("#cmpLogNameFilter" + cmpId), cmp.Settings.LogNameFilter, isTemplate);
                editApp.updateModelFromUi($("#cmpMatchDefinition" + cmpId), cmp.Settings.EntryMatch, isTemplate);
                editApp.updateModelFromUi($("#cmpEntrySource" + cmpId), cmp.Settings.EntrySource, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryIDType" + cmpId + "_String"), cmp.Settings.EntryID, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryIDType" + cmpId), cmp.Settings.EntryIDType, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryType" + cmpId), cmp.Settings.EntryType, isTemplate);
                editApp.updateModelFromUi($("#cmpUsers" + cmpId), cmp.Settings.Users, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryIncludeOperation" + cmpId), cmp.Settings.EntryIncludeOperation, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryIncludeOperation" + cmpId + "_String"), cmp.Settings.EntryIncludeFilter, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryExcludeOperation" + cmpId), cmp.Settings.EntryExcludeOperation, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryExcludeOperation" + cmpId + "_String"), cmp.Settings.EntryExcludeFilter, isTemplate);
                editApp.updateModelFromUi($("#cmpNumberOfFrequencies" + cmpId), cmp.Settings.NumberOfFrequencies, isTemplate);
                editApp.updateModelFromUi($("#cmpCollectDetails" + cmpId), cmp.Settings.CollectDetails, isTemplate);
                
                    editApp.updateModelFromUi($("#cmpCredential" + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp, isTemplate);
                <% } %>

                <% if (Master.Master.IsBlackBoxEdit && !SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                editApp.updateModelFromUi($("#cmpEntryIDType" + cmpId + "_String"), cmp.Settings.EntryID, isTemplate);
                editApp.updateModelFromUi($("#cmpEntryIDType" + cmpId), cmp.Settings.EntryIDType, isTemplate);
                <% } %>

                editApp.updateModelFromUi($("#cmpStatusSetting" + cmpId), cmp.Settings.StatusSetting, isTemplate);
            };
            
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
			
			onStateChanged();
        })();
		<% }%>
	</script>
	<style type="text/css">
		.wel-help-message {
			min-height: 30px;
			padding: 3px 3px 3px 25px; margin-top: 3px;
			color: #000;
			border-style: solid; border-radius: 6px; border-width: 1px; border-color: #EACA7F; background-color:#FFFDCC; 
			background-image: url("/Orion/Images/warning_16x16.gif");
			background-position: 5px 5px;
			background-repeat: no-repeat;
		}
	</style>
      <% if (!Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode) {%>
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FetchingMethod %>" BaseValueName="cmpFetchingMethod" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"
			Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_FetchingMethod>()%>" IsComplexSetting="true"
			HelpTextBottom=<%# string.Format(CultureInfo.InvariantCulture, "<div class='wel-help-message'>Windows Event log message details generated by 3rd party applications on Windows 2003/XP and earlier may not be available via RPC.<br/><a href='{0}' target='_blank'> &#8811; Learn about RPC limitations</a></div>", SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(4461)) %> runat="server" />
    
        <apm:DropdownEditor ID="ctrWinRmAuthenticationMechanism" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WinRmAuthenticationMechanism %>" BaseValueName="cmpWinRmAuthenticationMechanism" ElementId="<%# Master.ComponentId %>" 
                            Options="<%# DropdownOptionsHelper.OptionsFromEnum<WinRmAuthenticationMechanism>() %>" EditorMode="<%# Master.EditMode %>" runat="server" />

	   <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LogToMonitor %>" BaseValueName="cmpLogToMonitor" ElementId="<%# Master.ComponentId %>" IsComplexSetting="true" 
            Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_Name>()%>" EditorMode="<%# Master.EditMode%>" runat="server"/>

        <tbody class="subEditor">
			<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CustomLogtoMonitor %>" BaseValueName="cmpLogNameFilter" ElementId="<%# Master.ComponentId %>" 
				HelpTextBottom="<%$ Resources: APMWebContent, EditComponentEditors_CustomLogtoMonitor_HelpTextBottom %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        </tbody>

        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_MatchDefinition %>" BaseValueName="cmpMatchDefinition" ElementId="<%# Master.ComponentId %>" IsComplexSetting="true" 
            Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_EntryMatch>()%>" EditorMode="<%# Master.EditMode%>" runat="server" />

		<tbody class="subEditor">
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LogSource %>" BaseValueName="cmpEntrySource" ElementId="<%# Master.ComponentId %>" Required="false"
                HelpTextBottom="<%$ Resources: APMWebContent, EditComponentEditors_LogSource_HelpTextBottom %>" EditorMode="<%# Master.EditMode%>" runat="server" />
            <apm:DropdownAndStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_EventID %>" BaseValueName="cmpEntryIDType" IsComplexSetting="true" 
                ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_EventIDType>()%>" 
                HelpTextBottom="Separate multiple IDs with commas." EditorMode="<%# Master.EditMode%>" runat="server" />
            <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_EventType %>" BaseValueName="cmpEntryType" 
                ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_EntryType>()%>" EditorMode="<%# Master.EditMode%>" runat="server" />
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UserWhoGeneratedEvents %>" BaseValueName="cmpUsers" ElementId="<%# Master.ComponentId %>" Required="false"
                HelpTextBottom="<%$ Resources: APMWebContent, EditComponentEditors_UserWhoGeneratedEvents_HelpTextBottom %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
            <apm:DropdownAndStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IncludeEvents %>" BaseValueName="cmpEntryIncludeOperation" Required="false" IsComplexSetting="true"
                ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_EntryOperation>()%>" StringIsMultiline="true"
                HelpTextBottom="<%$ Resources: APMWebContent, EditComponentEditors_ExcludeEvents_HelpTextBottom %>" EditorMode="<%# Master.EditMode%>" runat="server" />
            <apm:DropdownAndStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExcludeEvents %>" BaseValueName="cmpEntryExcludeOperation" Required="false" IsComplexSetting="true"
                ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_EntryOperation>()%>" StringIsMultiline="true"
                HelpTextBottom="<%$ Resources: APMWebContent, EditComponentEditors_ExcludeEvents_HelpTextBottom %>" EditorMode="<%# Master.EditMode%>" runat="server" />
            <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_NumberOfPastPolling %>" BaseValueName="cmpNumberOfFrequencies" 
                ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
        </tbody>

        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CollectDetailedDataOfMatchedEvents %>" BaseValueName="cmpCollectDetails"
            HelpTextBottom="<%$ Resources: APMWebContent, EditComponentEditors_CollectDetailedData_HelpTextBottom %>"
            ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
      <% }%>
    

        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IfaMatchIsFoundIn %>" BaseValueName="cmpStatusSetting" 
            ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<MonitorStatusSetting>()%>" EditorMode="<%# Master.EditMode%>" runat="server" />

        <% if (Master.Master.IsBlackBoxEdit && !SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
        <apm:dropdownandstringeditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_EventID %>" basevaluename="cmpEntryIDType" iscomplexsetting="true"
            elementid="<%# Master.ComponentId %>" options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_EventIDType>()%>"
            helptextbottom="Separate multiple IDs with commas." editormode="<%# Master.EditMode%>" runat="server" />
        <% } %>

      <% if (!Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode) {%>
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
      <% }%>

	    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>