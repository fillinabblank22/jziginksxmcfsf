﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.Plugins" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Management.WinRm" %>

<script runat="server">

    protected bool IsDevMode
    {
        get { return this.Master.Master.IsBlackBoxEdit == false || ComponentConstants.IsDevMode; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.edtFetchingMethod.Options = DropdownOptionsHelper.OptionsFromEnum<ProcessInfo_FetchingMethod>();
        this.edtServiceName.Visible = this.IsDevMode;
        this.edtNotRunningStatus.Visible = this.IsDevMode;
        this.edtNotRunningStatus.Options = DropdownOptionsHelper.OptionsFromEnum<ProcessInfo_NotRunningStatus>();
    }

</script>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="WindowsService" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <%
    if (ComponentConstants.IsDevMode) {
        WebPluginManager.Instance.CustomizeComponentEditors(Master.CustomApplicationType, cmpMinRequestingVersion, cmpMaxRequestingVersion);
    }
    %>
    <script type="text/javascript">
        SW.APM.EditApp.Editors.WindowsService.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId%>',
            '<%= this.Master.ComponentIds %>',
            '<%= this.IsDevMode %>',
            '<%= this.cmpMinRequestingVersion.Visible %>',
            '<%= this.cmpMaxRequestingVersion.Visible %>'
        );
    </script>
    <apm:DropdownEditor ID="edtFetchingMethod" runat="server" displaytext="<%$ Resources: APMWebContent, EditComponentEditors_FetchingMethod %>" basevaluename="cmpFetchingMethod" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" />
    <apm:DropdownEditor ID="ctrWinRmAuthenticationMechanism" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WinRmAuthenticationMechanism %>" BaseValueName="cmpWinRmAuthenticationMechanism" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<WinRmAuthenticationMechanism>() %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:stringeditor ID="edtServiceName" displaytext="<%$ Resources: APMWebContent, EditComponentEditors_NetServiceName %>" basevaluename="cmpServiceName" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:DropdownEditor ID="edtNotRunningStatus" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_StatusWhenProcessIsNotRunning %>" BaseValueName="cmpNotRunningStatusMode" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" HelpTextBottom="..." runat="server" />
    <apm:stringeditor ID="cmpMinRequestingVersion" basevaluename="cmpMinRequestingVersion" Visible="False" Required="False" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:stringeditor ID="cmpMaxRequestingVersion" basevaluename="cmpMaxRequestingVersion" Visible="False" Required="False" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:thresholdeditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_CPU %>" basevaluename="cmpCPUThreshold" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:thresholdeditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_PhysicalMemory %>" basevaluename="cmpPMemThreshold" elementid="<%# Master.ComponentId %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:thresholdeditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_VirtualMemory %>" basevaluename="cmpVMemThreshold" elementid="<%# Master.ComponentId%>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:thresholdeditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_IOReadOperationsSec %>" basevaluename="cmpIOReadOPSThreshold" elementid="<%# Master.ComponentId  %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:thresholdeditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_IOWriteOperationsSec %>" basevaluename="cmpIOWriteOPSThreshold" elementid="<%# Master.ComponentId  %>" editormode="<%# Master.EditMode%>" runat="server" />
    <apm:thresholdeditor displaytext="<%$ Resources: APMWebContent, EditComponentEditors_IOTotalOperationsSec %>" basevaluename="cmpIOTotalOPSThreshold" elementid="<%# Master.ComponentId  %>" editormode="<%# Master.EditMode%>" runat="server" />
</asp:Content>