﻿using System;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_Edit_ComponentEditors_ComponentEditorWithCredentials : System.Web.UI.MasterPage
{
    public string ComponentId
    {
        get { return this.Master.ComponentId; }
    }

    public SettingEditorControlBase.EditMode EditMode
    {
        get { return this.Master.EditMode; }
    }

    public bool IsSingleEditMode
    {
        get { return this.Master.IsSingleEditMode; }
    }

    public bool IsMultiEditMode
    {
        get { return this.Master.IsMultiEditMode; }
    }

    public string ComponentIds
    {
        get { return this.Master.ComponentIds; }
    }

    public string ThresholdKeysJson
    {
        get { return this.Master.ThresholdKeysJson; }
    }

    public bool IsTemplate
    {
        get { return this.Master.IsTemplate; }
    }

    public string CustomApplicationType
    {
        get { return this.Master.CustomApplicationType; }
    }
    public bool ShowCredentialsAlways
    {
        get { return this.Master.ShowCredentialsAlways; }
    }
}
