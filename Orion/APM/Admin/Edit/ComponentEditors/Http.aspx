﻿<%@ Page AutoEventWireup="true" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<%@ Reference VirtualPath="ComponentEditorBase.master" %>
<%@ MasterType VirtualPath="ComponentEditorWithCredentials.master" %>

<script runat="server">

    protected bool IsDevMode
    {
        get { return (!this.Master.Master.IsBlackBoxEdit) || (SolarWinds.APM.Web.ComponentConstants.IsDevMode); }
    }

    protected override void OnPreRender(EventArgs e)
    {
        this.edtPortNumber.Visible = this.IsDevMode;

        this.edtHostRequest.Options = DropdownOptionsHelper.OptionsFromEnum<HostRequests>();
        this.edtAuthMode.Options = DropdownOptionsHelper.OptionsFromEnum<AuthMode>();

        base.OnPreRender(e);
    }

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="Http"/>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <apm:NumericEditor ID="edtPortNumber" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" MinValue="0" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Url %>" BaseValueName="cmpUrl" ElementId="<%# Master.ComponentId %>" EditorMode ="<%# Master.EditMode %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HostHeader %>" BaseValueName="cmpHostHeader" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>"  runat="server" />
    <apm:DropdownEditor ID="edtHostRequest" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HostRequest %>" BaseValueName="cmpHostRequest" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode %>" IsComplexSetting="true" runat="server" />
    <tbody id="postPutCnt<%=Master.ComponentId%>" class="subEditor">
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ContentType %>" BaseValueName="cmpContentType" ElementId="<%# Master.ComponentId%>" Required="false" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_RequestBody %>" BaseValueName="cmpRequestBody" ElementId="<%# Master.ComponentId%>" Required="false" EditorMode ="<%# Master.EditMode %>" MaxLength="1048576" IsMultiline="true" runat="server" />
    </tbody>
    <tbody id="getHeadCnt<%=Master.ComponentId%>" class="subEditor">
        <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FollowRedirect %>" BaseValueName="cmpFollowRedirect" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, Edit_Follow %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_DoNotFollow %>" runat="server"  />
    </tbody>
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseProxy %>" BaseValueName="cmpUseProxy" ElementId="<%# Master.ComponentId %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_UseProxy %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_DoNotUseProxy %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ProxyAddress %>" BaseValueName="cmpProxyAddress" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UserAgent %>" BaseValueName="cmpUserAgent" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SearchString %>" BaseValueName="cmpSearchString" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>"  runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FailIfFound %>" BaseValueName="cmpFailIfFound" ElementId="<%# Master.ComponentId %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HeadRequest %>" BaseValueName="cmpHeadRequest" ElementId="<%# Master.ComponentId %>" TrueDisplayValue="<%$ Resources: APMWebContent, Edit_RequestHeadersOnly %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_FullRequest %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AcceptCompression %>" BaseValueName="cmpAcceptCompression" ElementId="<%# Master.ComponentId %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:DropdownEditor ID="edtAuthMode" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AuthenticateMode %>" BaseValueName="cmpAuthMode" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />

    <script type="text/javascript">
        SW.APM.EditApp.Editors.Http.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>',
            '<%= !this.Master.Master.IsBlackBoxEdit || SolarWinds.APM.Web.ComponentConstants.IsDevMode %>'
        );
    </script>
</asp:Content>
