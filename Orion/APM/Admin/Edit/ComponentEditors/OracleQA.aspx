﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<asp:Content  ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor ComponentType="OracleQA" runat="server" />
</asp:Content>
<asp:Content  ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["OracleQA"];
			    editApp.initMultiUiState($("#cmpPortNumber" + cmpId), cmp.DefinitionSettings, "PortNumber");
			    editApp.initMultiUiState($("#cmpSqlQuery" + cmpId), cmp.DefinitionSettings, "SqlQuery");
                editApp.initMultiUiState($("#cmpDestinationPointType" + cmpId), cmp.DefinitionSettings, "DestinationPointType");
			    editApp.initMultiUiState($("#cmpDestinationPointName" + cmpId), cmp.DefinitionSettings, "DestinationPointName");
			    editApp.initMultiUiState($("#cmpCountStatisticDifference" + cmpId), cmp.DefinitionSettings, "CountAsDifference");
			    editApp.initMultiUiState($("#cmpOracleDriverType" + cmpId), cmp.DefinitionSettings, "OracleDriverType");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
			    editApp.updateMultiModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings, "PortNumber", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpSqlQuery" + cmpId), cmp.Settings, "SqlQuery", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpDestinationPointType" + cmpId), cmp.Settings, "DestinationPointType", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpDestinationPointName" + cmpId), cmp.Settings, "DestinationPointName", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCountStatisticDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpOracleDriverType" + cmpId), cmp.Settings, "OracleDriverType", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;

            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    { func: editApp.addCredentialSets, args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },                
                    { func: editApp.initUiState, args: [$('#cmpPortNumber' + cmpId), 'PortNumber', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSqlQuery' + cmpId), 'SqlQuery', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpDestinationPointType' + cmpId), 'DestinationPointType', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpDestinationPointName' + cmpId), 'DestinationPointName', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpOracleDriverType' + cmpId), 'OracleDriverType', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpCountStatisticDifference' + cmpId), 'CountAsDifference', cmp.Settings, templateSettings] },
                    { func: editApp.initDataTransform, args: [$('#cmpConvertValue' + cmpId), cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                
                editApp.updateModelFromUi($('#cmpPortNumber' + cmpId), cmp.Settings.PortNumber, isTemplate);
                editApp.updateModelFromUi($('#cmpSqlQuery' + cmpId), cmp.Settings.SqlQuery, isTemplate);
                editApp.updateModelFromUi($('#cmpDestinationPointType' + cmpId), cmp.Settings.DestinationPointType, isTemplate);
                editApp.updateModelFromUi($('#cmpDestinationPointName' + cmpId), cmp.Settings.DestinationPointName, isTemplate);
                editApp.updateModelFromUi($('#cmpOracleDriverType' + cmpId), cmp.Settings.OracleDriverType, isTemplate);
                editApp.updateModelFromUi($('#cmpCountStatisticDifference' + cmpId), cmp.Settings.CountAsDifference, isTemplate);
                
                editApp.updateDataTransformModelFromUi($('#cmpConvertValue' + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId),cmp.Thresholds.Response, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="0" runat="server"  /> 
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SqlQuery %>" BaseValueName="cmpSqlQuery" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" IsMultiline="true"  />
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_DestinationPointType %>" BaseValueName="cmpDestinationPointType" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<DestinationPointTypeSettings>()%>" EditorMode="<%# Master.EditMode%>" runat="server"/>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_DestinationPointName %>" BaseValueName="cmpDestinationPointName" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_OracleDriverType %>" BaseValueName="cmpOracleDriverType" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<OracleDriverTypeSettings>()%>" EditorMode="<%# Master.EditMode%>" runat="server"/>
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountStatisticDifference" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />  
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
 </asp:Content>