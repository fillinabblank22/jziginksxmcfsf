﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor runat="server" ComponentType="Hierarchy"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["Hierarchy"];
                <% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
		    	<% if (Master.IsTemplate) { %>
                editApp.initMultiUiState($("#cmpVisibilityMode" + cmpId), cmp.DefinitionSettings, "VisibilityMode");
                <% } %>
                <% } %>
            };
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                <% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
		    	<% if (Master.IsTemplate) { %>
                editApp.updateMultiModelFromUi($("#cmpVisibilityMode" + cmpId), cmp.Settings, "VisibilityMode", cmpIds);
                <% } %>	    	    
                <% } %>
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
        <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                if ($.browser.msie) {
                    parentComponentContainer = componentContainer.parent().parent()[0];
                } else {
                    parentComponentContainer = componentContainer.parent()[0];
                }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] }

                    <% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
                    ,{ func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] }
					<% if (Master.IsTemplate) { %>
                    ,{ func: editApp.initUiState, args: [$('#cmpVisibilityMode' + cmpId), 'VisibilityMode', cmp.Settings, templateSettings] }
			        <% } %>
                    <% } %>
                ];
                
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                    mLoading.hide(); 
                    var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                    SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                    SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);

                <% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
            	editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
		        <% if (Master.IsTemplate) { %>
                editApp.updateModelFromUi($('#cmpVisibilityMode' + cmpId), cmp.Settings.VisibilityMode, isTemplate);
                <% }%>
                <% } %>
                
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
        <% }%>
    </script>
    <% if (SolarWinds.APM.Web.ComponentConstants.IsDevMode) { %>
        <% if (Master.IsTemplate) { %>
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VisibilityMode %>" BaseValueName="cmpVisibilityMode" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ComponentVisibilityMode>() %>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <% }%>
    <% }%>
</asp:Content>

