﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Management.WinRm" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="CustomWmi" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" runat="Server">

    <asp:PlaceHolder runat="server" Visible="<%# this.Master.IsSingleEditMode %>">
        <asp:PlaceHolder runat="server" Visible="<%# SolarWinds.APM.Web.ComponentConstants.IsDevMode %>">
            <asp:PlaceHolder runat="server" Visible="<%# this.Master.Master.IsTemplate %>">
                <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CanBeDisabled %>" BaseValueName="cmpCanBeDisabled" ElementId="<%# Master.ComponentId %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" IsComplexSetting="true" runat="server" />
                <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VisibilityMode %>" BaseValueName="cmpVisibilityMode" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ComponentVisibilityMode>() %>" EditorMode="<%# Master.EditMode %>" runat="server" />
            </asp:PlaceHolder>
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExecutorName %>" BaseValueName="cmpExecutorName" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode %>" IsMultiline="False" />
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExecutorAssemblyName %>" BaseValueName="cmpExecutorAssemblyName" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode %>" IsMultiline="False" />
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AdditionalData %>" BaseValueName="cmpAdditionalData" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode %>" IsMultiline="True" Required="False" />
        </asp:PlaceHolder>
    </asp:PlaceHolder>

	<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WinRmAuthenticationMechanism %>" BaseValueName="cmpWinRmAuthenticationMechanism" ElementId="<%# Master.ComponentId %>" 
		EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<WinRmAuthenticationMechanism>() %>" runat="server" />

    <apm:ThresholdEditorList runat="server" />

    <script type="text/javascript">
        SW.APM.EditApp.Editors.CustomWMI.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>',
            '<%= SolarWinds.APM.Web.ComponentConstants.IsDevMode %>',
            '<%= this.Master.IsTemplate %>',
            '<%= this.Master.ThresholdKeysJson %>'
        );
    </script>

</asp:Content>
