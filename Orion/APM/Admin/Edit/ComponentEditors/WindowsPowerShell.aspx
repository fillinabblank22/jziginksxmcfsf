﻿<%@ Page AutoEventWireup="true" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<%@ Register Src="~/Orion/APM/Admin/Edit/Editors/BooleanCheckBoxEditor.ascx" TagPrefix="apm" TagName="BooleanCheckBoxEditor" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Editors/ColumnTemplate.ascx" TagPrefix="apm" TagName="ColumnTemplate" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Editors/ComponentTypeEditor.ascx" TagPrefix="apm" TagName="ComponentTypeEditor" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Editors/DropdownEditor.ascx" TagPrefix="apm" TagName="DropdownEditor" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Editors/NumericEditor.ascx" TagPrefix="apm" TagName="NumericEditor" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Editors/ScriptBody.ascx" TagPrefix="apm" TagName="ScriptBodyEditor" %>
<%@ Register Src="~/Orion/APM/Admin/Edit/Editors/StringEditor.ascx" TagPrefix="apm" TagName="StringEditor" %>

<script runat="server">
    void Page_Init(object sender, EventArgs e)
    {
        this.edtStatusRollup.Options = DropdownOptionsHelper.OptionsFromEnum<StatusRollupMethod>();
        this.edtExecutionMode.Options = DropdownOptionsHelper.OptionsFromEnum<PowerShell_ExecutionMode>();

        this.edtScriptArgs.DisplayText = Resources.APMWebContent.WinPowerShellEditor_ScriptArgs;
        this.edtScriptBody.DisplayText = Resources.APMWebContent.WinPowerShellEditor_ScriptBody;
        this.edtExecutionMode.DisplayText = Resources.APMWebContent.WinPowerShellEditor_ExecutionMode;
        this.edtExecutionMode.HelpTextBottom = Resources.APMWebContent.WinPowerShellEditor_ExecutionMode_HelpBottom;
        this.edtUseHttps.DisplayText = Resources.APMWebContent.WinPowerShellEditor_UseHttps;
        this.edtUseHttps.HelpTextBottom = Resources.APMWebContent.WinPowerShellEditor_UseHttps_HelpBottom;
        this.edtUrlPrefix.DisplayText = Resources.APMWebContent.WinPowerShellEditor_UrlPrefix;
        this.edtUrlPrefix.HelpTextBottom = Resources.APMWebContent.WinPowerShellEditor_UrlPrefix_HelpBottom;
        this.edtPortNumber.DisplayText = Resources.APMWebContent.WinPowerShellEditor_PortNumber;
        this.edtPortNumber.HelpTextBottom = Resources.APMWebContent.WinPowerShellEditor_PortNumber_HelpBottom;
        this.edtImpersonateForLocal.DisplayText = Resources.APMWebContent.WinPowerShellEditor_ImpersonateForLocal;
        this.edtImpersonateForLocal.HelpTextBottom = Resources.APMWebContent.WinPowerShellEditor_ImpersonateForLocal_HelpBottom;
        this.edtCountStatAsDiff.DisplayText = Resources.APMWebContent.WinPowerShellEditor_CountStatAsDiff;
        this.edtStatusRollup.DisplayText = Resources.APMWebContent.WinPowerShellEditor_StatusRollup;
    }
</script>

<asp:Content ID="c1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
   <apm:ComponentTypeEditor ComponentType="WindowsPowerShell" runat="server" />
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <orion:Include ID="Include1" File="APM/Admin/Edit/Js/ComponentEditors/WindowsPowerShell.js" runat="server" />
    <apm:StringEditor ID="edtScriptArgs" BaseValueName="cmpScriptArguments" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:ScriptBodyEditor ID="edtScriptBody" BaseValueName="cmpScriptBody" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:DropdownEditor ID="edtExecutionMode" BaseValueName="cmpExecutionMode" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsComplexSetting="true" runat="server" />
    <tbody class="subEditor">
        <apm:BooleanCheckBoxEditor ID="edtUseHttps" BaseValueName="cmpWrmUseSSL" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:StringEditor ID="edtUrlPrefix" BaseValueName="cmpWrmUrlPrefix" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:NumericEditor ID="edtPortNumber" BaseValueName="cmpWrmPort" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" MinValue="0" runat="server" />
    </tbody>
    <apm:BooleanCheckBoxEditor ID="edtImpersonateForLocal" BaseValueName="cmpImpersonateForLocalMode" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:BooleanCheckboxEditor ID="edtCountStatAsDiff" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:ColumnTemplate ElementId="<%# Master.ComponentId %>" runat="server" />
    <apm:DropdownEditor ID="edtStatusRollup" runat="server" BaseValueName="cmpStatusRollupType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" />
    <script type="text/javascript">
        SW.APM.EditApp.Editors.WindowsPowerShell.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>'
            );
    </script>
</asp:Content>
