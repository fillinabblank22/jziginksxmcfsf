﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Web.Plugins" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="SqlTable" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" runat="Server">

    <asp:PlaceHolder runat="server" Visible="<%# this.Master.Master.IsSingleEditMode %>">
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PollingFrequency %>" BaseValueName="cmpMinFreq" AdditionalEditorCss="PollingFreqComponent" ElementId="<%# Master.ComponentId %>" EditorMode="Single" IsComplexSetting="True" MinValue="0" runat="server" />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_NumberOfRecordsToRetrieve %>" BaseValueName="cmpRowCount" AdditionalEditorCss="RowCount" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" MinValue="0" MaxValue="1024" />
        <apm:NumericEditor DisplayText="<%# WebPluginManager.Instance.GetLabelByComponentId(Master.ComponentId, Master.IsTemplate) %>" BaseValueName="cmpIntParameter1" AdditionalEditorCss="IntParameter1" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" MinValue="0" />
        <asp:PlaceHolder runat="server" Visible="<%# SolarWinds.APM.Web.ComponentConstants.IsDevMode %>">
            <asp:PlaceHolder runat="server" Visible="<%# this.Master.Master.IsTemplate %>">
                <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VisibilityMode %>" BaseValueName="cmpVisibilityMode" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ComponentVisibilityMode>() %>" EditorMode="<%# Master.EditMode %>" runat="server" />
            </asp:PlaceHolder>
            <apm:BooleanCheckBoxEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" />
            <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" MinValue="0" runat="server" />
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_InitialCatalog %>" BaseValueName="cmpInitialCatalog" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode %>" />
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SqlQuery %>" BaseValueName="cmpSqlQuery" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode %>" IsMultiline="true" />
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ServerInstance %>" BaseValueName="cmpSqlServerInstance" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode %>" />
        </asp:PlaceHolder>
        <apm:NumericEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_QueryTimeout %>" BaseValueName="cmpQueryTimeout" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" MinValue="30" MaxValue="3600" Visible="<%# !Master.Master.IsApplicationItemEdit %>" />
        <asp:PlaceHolder runat="server" Visible="<%# SolarWinds.APM.Web.ComponentConstants.IsDevMode %>">
            <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
            <apm:BooleanCheckBoxEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseSharedConnection %>" BaseValueName="cmpUseSharedConnection" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" />
            <apm:BooleanCheckBoxEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WindowsAuthentication %>" BaseValueName="cmpWindowsAuthentication" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" />
            <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortType %>" BaseValueName="cmpPortType" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<SqlPortType>() %>" IsComplexSetting="true" runat="server" />
        </asp:PlaceHolder>
    </asp:PlaceHolder>

    <apm:ThresholdEditorList runat="server" />

    <script type="text/javascript">
        SW.APM.EditApp.Editors.SqlTable.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>',
            '<%= this.Master.Master.IsApplicationItemEdit %>',
            '<%= SolarWinds.APM.Web.ComponentConstants.IsDevMode %>',
            '<%= this.Master.Master.IsTemplate %>',
            '<%= this.Master.ThresholdKeysJson %>');
    </script>

</asp:Content>
