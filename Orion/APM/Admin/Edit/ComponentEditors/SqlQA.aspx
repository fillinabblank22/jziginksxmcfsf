﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="SqlQA" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
		var toggleSubEditorSqlQA = function(el, visible) {
			if (visible) {
				el.show().find("*[apmsettctr=true]").each(function() {
					var subel = $(this);
					if (subel.is(":visible") && subel.parents().size() == subel.parents(":visible").size()) {
						subel.addClass("required");
					}
				});
			} else {
				el.hide().find("*[apmsettctr]").removeClass("required");
			}
		};

        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
        	var disableSubEditor = function(multiEditCheckBox,subEditor,editor) {
        		var isMultiEditCheckChecked= $('#'+multiEditCheckBox).is(':checked');
        		if(!isMultiEditCheckChecked) {
					var subEditorCheckBoxes=subEditor.find("[id*=MultiChekbox]");
					subEditorCheckBoxes.each(function() {
						if($(this).is(':checked'))
						{
							$(this).attr('checked', false);
							$(this).click();
							$(this).attr('checked', false);
						}
					});
				}
        	    isMultiEditCheckChecked ? subEditor.find("[id*=MultiChekbox]").removeAttr("disabled") : subEditor.find("[id*=MultiChekbox]").attr("disabled", "disabled");
				isMultiEditCheckChecked ?  editor.find("[id]").removeAttr("disabled") :  editor.find("[id]").attr("disabled", "disabled");
	            return true;
			};
			var showSubEditorForPortType = function () {
				var el = $(this), editor = el.parent(), subEditor = el.parents("tbody:first").next("tbody.subEditor");
				var multiEditCheckBoxContainer= editor.parents('tr')[0];
				var multiEditCheckBox = $(multiEditCheckBoxContainer).find("[id*=MultiChekbox]");
			
				var multiEditSubEditorDisable = function() {
					disableSubEditor(multiEditCheckBox[0].id, subEditor, editor);
				};
				$(multiEditCheckBox).click(multiEditSubEditorDisable);
				multiEditSubEditorDisable();
				
				if (editor.is(":visible")) {
					toggleSubEditorSqlQA(subEditor, el.val() == "Static"); 
				} else {
					toggleSubEditorSqlQA(subEditor, editor.next(".viewer").text() == "Use static port"); 
				}
			};

			var onStateChanged = function() {
				var el = $("#cmpPortType" + cmpId);
				el.unbind("change").bind("change", showSubEditorForPortType);
				el.change();
			};

			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["SqlQA"];
				editApp.initMultiUiState($("#cmpPortType" + cmpId), cmp.DefinitionSettings, "PortType");
				editApp.initMultiUiState($("#cmpPortNumber" + cmpId), cmp.DefinitionSettings, "PortNumber");
			    editApp.initMultiUiState($("#cmpWindowsAuthentication" + cmpId), cmp.DefinitionSettings, "WindowsAuthentication");
			    editApp.initMultiUiState($("#cmpQueryTimeout" + cmpId), cmp.DefinitionSettings, "QueryTimeout");
			    editApp.initMultiUiState($("#cmpSqlQuery" + cmpId), cmp.DefinitionSettings, "SqlQuery");
			    editApp.initMultiUiState($("#cmpSQLServerInstance" + cmpId), cmp.DefinitionSettings, "SqlServerInstance");
			    editApp.initMultiUiState($("#cmpInitialCatalog" + cmpId), cmp.DefinitionSettings, "InitialCatalog");
			    editApp.initMultiUiState($("#cmpCountAsDifference" + cmpId), cmp.DefinitionSettings, "CountAsDifference");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");

				onStateChanged();
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
				editApp.updateMultiModelFromUi($("#cmpPortType" + cmpId), cmp.Settings, "PortType", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings, "PortNumber", cmpIds);
			    editApp.updateMultiModelFromUi($("#cmpWindowsAuthentication" + cmpId), cmp.Settings, "WindowsAuthentication", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpQueryTimeout" + cmpId), cmp.Settings, "QueryTimeout", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpSqlQuery" + cmpId), cmp.Settings, "SqlQuery", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpSQLServerInstance" + cmpId), cmp.Settings, "SqlServerInstance", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpInitialCatalog" + cmpId), cmp.Settings, "InitialCatalog", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        	onStateChanged();
        })();
	    <% } else { %>
		(function() {
			var cmpId = <%=Master.ComponentId%>;
            var editApp = SW.APM.EditApp;

			var showSubEditorForPortType = function () {
				var el = $(this), editor = el.parent(), subEditor = el.parents("tbody:first").next("tbody.subEditor");
				if (editor.is(":visible")) {
					toggleSubEditorSqlQA(subEditor, el.val() == "Static"); 
				} else {
					toggleSubEditorSqlQA(subEditor, editor.next(".viewer").text() == "Use static port"); 
				}
			};
			var onStateChanged = function() {
				var el = $("#cmpPortType" + cmpId);
				el.unbind("change").bind("change", showSubEditorForPortType);
				el.change();
			};
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },
                    { func: editApp.initUiState, args: [$('#cmpPortType' + cmpId), 'PortType', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpPortNumber' + cmpId), 'PortNumber', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpWindowsAuthentication' + cmpId), 'WindowsAuthentication', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpQueryTimeout' + cmpId), 'QueryTimeout', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSqlQuery' + cmpId), 'SqlQuery', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSQLServerInstance' + cmpId), 'SqlServerInstance', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpInitialCatalog' + cmpId), 'InitialCatalog', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpCountAsDifference' + cmpId), 'CountAsDifference', cmp.Settings, templateSettings] },
                    { func: editApp.initDataTransform, args: [$('#cmpConvertValue' + cmpId), cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
				    
					onStateChanged();
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
					mLoading.hide(); 
					var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
					SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
					SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateModelFromUi($('#cmpPortType' + cmpId), cmp.Settings.PortType, isTemplate);
                editApp.updateModelFromUi($('#cmpPortNumber' + cmpId), cmp.Settings.PortNumber, isTemplate);
                editApp.updateModelFromUi($('#cmpWindowsAuthentication' + cmpId), cmp.Settings.WindowsAuthentication, isTemplate);
                editApp.updateModelFromUi($('#cmpQueryTimeout' + cmpId), cmp.Settings.QueryTimeout, isTemplate);
                editApp.updateModelFromUi($('#cmpSqlQuery' + cmpId), cmp.Settings.SqlQuery, isTemplate);
                editApp.updateModelFromUi($('#cmpSQLServerInstance' + cmpId), cmp.Settings.SqlServerInstance, isTemplate);
                editApp.updateModelFromUi($('#cmpInitialCatalog' + cmpId), cmp.Settings.InitialCatalog, isTemplate);
                editApp.updateModelFromUi($('#cmpCountAsDifference' + cmpId), cmp.Settings.CountAsDifference, isTemplate);
                editApp.updateDataTransformModelFromUi($('#cmpConvertValue' + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId),cmp.Thresholds.Response, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);

			onStateChanged();
        })();
	<% }%>
    </script>
		<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortType %>" BaseValueName="cmpPortType" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"
			Options="<%# DropdownOptionsHelper.OptionsFromEnum<SqlPortType>()%>" IsComplexSetting="true" runat="server" />

		<tbody class="subEditor">
          <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="1"  MaxValue="65535" Required="true" runat="server" />
		</tbody>
		<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SqlQuery %>" BaseValueName="cmpSqlQuery" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="true" runat="server" />        
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SQLServerInstance %>" BaseValueName="cmpSQLServerInstance" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_InitialCatalog %>" BaseValueName="cmpInitialCatalog" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:BooleanCheckboxEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseWindowsAuthenticationFirst %>" BaseValueName="cmpWindowsAuthentication" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_QueryTimeout %>" BaseValueName="cmpQueryTimeout" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:BooleanCheckboxEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" />
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" /> 
		<apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId%>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>