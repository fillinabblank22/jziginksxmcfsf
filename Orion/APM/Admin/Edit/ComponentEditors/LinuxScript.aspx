﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Register TagPrefix="apm" TagName="ColumnTemplate" Src="~/Orion/APM/Admin/Edit/Editors/ColumnTemplate.ascx" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>

<script runat="server">
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server"> 
    <apm:ComponentTypeEditor ComponentType="LinuxScript" runat="server"/>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">
<script type="text/javascript">
    var toggleSubEditorCredentialLinuxScript = function(el, visible) {
			if (visible) {
				el.show().find("*[apmsettctr=true]").each(function() {
					var subel = $(this);
					if (subel.is(":visible") && subel.parents().size() == subel.parents(":visible").size()) {
						subel.addClass("required");
					}
				});
			} else {            
				el.hide().find("*[apmsettctr]").removeClass("required");
			}
		};

    var toggleMultiEditCredentialLinuxScriptSubEditor = function(el, visible) {
        if (visible) {
            el.find("td").each(function() { $(this).show(); });
        } else {
            el.find("td").each(function() { $(this).hide(); });
        }
    };
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
            var disableSubEditorForCredentialType = function(multiEditCheckBox,editor) {
        		var isMultiEditCheckChecked= $('#'+multiEditCheckBox).is(':checked');
                isMultiEditCheckChecked ? editor.find("[id]").removeAttr("disabled") : editor.find("[id]").attr("disabled", "disabled");
                return true;
			};
            
			var showSubEditorForCredentialType = function () {
				var el = $(this), editor = el.parent();

				var subEditorUsernamePassword = el.parents("tbody:first").next("tbody.subEditorUsernamePassword"),
                    subEditorUsernamePrivateKey = subEditorUsernamePassword.next("tbody.subEditorUsernamePrivateKey"),
                    multiEditCheckBoxContainer= editor.parents('tr')[0],
			        multiEditCheckBox = $(multiEditCheckBoxContainer).find("[id*=MultiChekbox]"),
			        multiEditUsernamePasswordCheckBox = $(subEditorUsernamePassword).find("[id*=MultiChekbox]"),
			        multiEditUsernamePrivateKeyCheckBox = $(subEditorUsernamePrivateKey).find("[id*=MultiChekbox]");
			    
			    var multiEditUsernamePasswordEditorDisable = function() {
			        var usernamePasswordEditor=subEditorUsernamePassword.find('.editor');
			        disableSubEditorForCredentialType(multiEditUsernamePasswordCheckBox[0].id, usernamePasswordEditor);
			        var cheked = $('#'+multiEditUsernamePasswordCheckBox[0].id).is(':checked');
			        $('#cmpCredentialUsernamePassword' +cmpId).attr("apm-me-was-changed", cheked);  
				};
			    
			    $(multiEditUsernamePasswordCheckBox).click(multiEditUsernamePasswordEditorDisable); multiEditUsernamePasswordEditorDisable();   

			    var multiEditUsernamePrivateKeyEditorDisable = function() {
			        var privateKeydEditor=subEditorUsernamePrivateKey.find('.editor');
			        disableSubEditorForCredentialType(multiEditUsernamePrivateKeyCheckBox[0].id, privateKeydEditor);
			        
			       var cheked = $('#'+multiEditUsernamePrivateKeyCheckBox[0].id).is(':checked');
			        $('#cmpCredentialUsernamePrivateKey' +cmpId).attr("apm-me-was-changed", cheked);  
				};
			    
			    $(multiEditUsernamePrivateKeyCheckBox).click(multiEditUsernamePrivateKeyEditorDisable); multiEditUsernamePrivateKeyEditorDisable(); 
				
			    var multiEditSubEditorDisable = function() {
			        toggleMultiEditCredentialLinuxScriptSubEditor(subEditorUsernamePassword, el.val() == "UsernamePassword");
                    toggleMultiEditCredentialLinuxScriptSubEditor(subEditorUsernamePrivateKey, el.val() == "UsernamePrivateKey");
				    disableSubEditorForCredentialType(multiEditCheckBox[0].id, editor);
				    var isMultiEditCheckChecked= $('#' +multiEditCheckBox[0].id).is(':checked');
			        
				    if(isMultiEditCheckChecked) {
				       multiEditUsernamePasswordCheckBox.removeAttr("disabled");
				       multiEditUsernamePrivateKeyCheckBox.removeAttr("disabled");
				    } else {
				        if(multiEditUsernamePasswordCheckBox.is(':checked')){
				             multiEditUsernamePasswordCheckBox.click();
				             multiEditUsernamePasswordEditorDisable();  
				         }
				        
				        if(multiEditUsernamePrivateKeyCheckBox.is(':checked')){
				             multiEditUsernamePrivateKeyCheckBox.click();
				             multiEditUsernamePrivateKeyEditorDisable();  
				         }
				        
				        multiEditUsernamePasswordCheckBox.attr("disabled", "disabled");
				        multiEditUsernamePrivateKeyCheckBox.attr("disabled", "disabled");			       
				    }  
				};
			    
				$(multiEditCheckBox).click(multiEditSubEditorDisable);multiEditSubEditorDisable();
			    
				if (editor.is(":visible")) {                
                    toggleSubEditorCredentialLinuxScript(subEditorUsernamePassword, el.val() == "UsernamePassword");
                    toggleSubEditorCredentialLinuxScript(subEditorUsernamePrivateKey, el.val() == "UsernamePrivateKey");
				} else {
					toggleSubEditorCredentialLinuxScript(subEditorUsernamePassword, editor.next(".viewer").text() == "Username and Password"); 
					toggleSubEditorCredentialLinuxScript(subEditorUsernamePrivateKey, editor.next(".viewer").text() == "Username and PrivateKey"); 
				}
			    
			};

			var onStateChanged = function() {
				var el = $("#cmpAuthenticationType" + cmpId);
				el.unbind("change").bind("change", showSubEditorForCredentialType);
				el.change();
			};

			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["LinuxScript"];
                editApp.initMultiUiState($("#cmpAuthenticationType" + cmpId), cmp.DefinitionSettings, "AuthenticationType");
                editApp.addCredentialSets($("#cmpCredentialUsernamePassword" + cmpId));
                editApp.addCertificateSets($("#cmpCredentialUsernamePrivateKey" + cmpId));
				editApp.initMultiUiState($("#cmpPort" + cmpId), cmp.DefinitionSettings, "Port");
			    editApp.initMultiUiState($("#cmpScriptDirectory" + cmpId), cmp.DefinitionSettings, "ScriptDirectory");
			    editApp.initMultiUiState($("#cmpCountAsDifference" + cmpId), cmp.DefinitionSettings, "CountAsDifference");
			    editApp.initMultiUiState($("#cmpCommandLineToPass" + cmpId), cmp.DefinitionSettings, "CommandLineToPass");
			    editApp.initMultiUiState($("#cmpScriptBody" + cmpId), cmp.DefinitionSettings, "ScriptBody");
			    editApp.initMultiUiState($("#cmpStatusRollupType" + cmpId), cmp.DefinitionSettings, "StatusRollupType");
                
                onStateChanged();
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
   				editApp.updateMultiModelFromUi($("#cmpAuthenticationType" + cmpId), cmp.Settings, "AuthenticationType", cmpIds);
                if ($('#cmpAuthenticationType' + cmpId).val() == "UsernamePassword") {
                    editApp.updateMultiModelFromUi($("#cmpCredentialUsernamePassword" + cmpId), cmp.Settings, "CredentialSetId", cmpIds);
                }
                else {
                    editApp.updateMultiModelFromUi($("#cmpCredentialUsernamePrivateKey" + cmpId), cmp.Settings, "CredentialSetId", cmpIds);
                }
                editApp.updateMultiModelFromUi($("#cmpPort" + cmpId), cmp.Settings, "Port", cmpIds);
			    editApp.updateMultiModelFromUi($("#cmpScriptDirectory" + cmpId), cmp.Settings, "ScriptDirectory", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCommandLineToPass" + cmpId), cmp.Settings, "CommandLineToPass", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpScriptBody" + cmpId), cmp.Settings, "ScriptBody", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpStatusRollupType" + cmpId), cmp.Settings, "StatusRollupType", cmpIds);
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
            onStateChanged();
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;

            var showSubEditorForCredentialType = function () {
				var el = $(this), editor = el.parent()
                var subEditorUsernamePassword = el.parents("tbody:first").next("tbody.subEditorUsernamePassword");
                var subEditorUsernamePrivateKey = subEditorUsernamePassword.next("tbody.subEditorUsernamePrivateKey");
                
				if (editor.is(":visible")) {                
                    toggleSubEditorCredentialLinuxScript(subEditorUsernamePassword, el.val() == "UsernamePassword");
                    toggleSubEditorCredentialLinuxScript(subEditorUsernamePrivateKey, el.val() == "UsernamePrivateKey");
				} else {
					toggleSubEditorCredentialLinuxScript(subEditorUsernamePassword, editor.next(".viewer").text() == "Username and Password"); 
					toggleSubEditorCredentialLinuxScript(subEditorUsernamePrivateKey, editor.next(".viewer").text() == "Username and PrivateKey"); 
				}
			};
			var onStateChanged = function() {
				var el = $("#cmpAuthenticationType" + cmpId);
				el.unbind("change").bind("change", showSubEditorForCredentialType);
				el.change();
			};
                        
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$("#cmpDesc" + cmpId), "UserDescription", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpDisabled" + cmpId), "IsDisabled", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpNotes" + cmpId), "UserNote", cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpAuthenticationType' + cmpId), 'AuthenticationType', cmp.Settings, templateSettings] },
                    { func: editApp.addCredentialSets, args: [$('#cmpCredentialUsernamePassword' + cmpId), function() {
                            editApp.initUiState($('#cmpCredentialUsernamePassword' + cmpId), 'CredentialSetId', cmp, template);}]
                    },                
                    { func: editApp.addCertificateSets, args: [$('#cmpCredentialUsernamePrivateKey' + cmpId), function() {
                            editApp.initUiState($('#cmpCredentialUsernamePrivateKey' + cmpId), 'CredentialSetId', cmp, template);}]
                    },
                    { func: editApp.initUiState, args: [$("#cmpPort" + cmpId), "Port", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpScriptDirectory" + cmpId), "ScriptDirectory", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpCountAsDifference" + cmpId), "CountAsDifference", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpCommandLineToPass" + cmpId), "CommandLineToPass", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpScriptBody" + cmpId), "ScriptBody", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpStatusRollupType' + cmpId), 'StatusRollupType', cmp.Settings, templateSettings] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);

                    onStateChanged();
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
                var isTemplate = !model.haveTemplate();
                editApp.initDynamicColumnsModel(cmp, template, isTemplate, $("#btnEdit<%#Master.ComponentId%>"));
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($("#cmpDesc" + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($("#cmpDisabled" + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpAuthenticationType' + cmpId), cmp.Settings.AuthenticationType, isTemplate);

                if ($('#cmpAuthenticationType' + cmpId).val() == "UsernamePassword") {
                    editApp.updateModelFromUi($("#cmpCredentialUsernamePassword" + cmpId), cmp.CredentialSetId, isTemplate);
                }
                else {
                    editApp.updateModelFromUi($("#cmpCredentialUsernamePrivateKey" + cmpId), cmp.CredentialSetId, isTemplate);
                }
                
                editApp.updateModelFromUi($("#cmpNotes" + cmpId), cmp.UserNote, isTemplate);
                
                editApp.updateModelFromUi($("#cmpPort" + cmpId), cmp.Settings.Port, isTemplate);
                editApp.updateModelFromUi($("#cmpScriptDirectory" + cmpId), cmp.Settings.ScriptDirectory, isTemplate);
                editApp.updateModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings.CountAsDifference, isTemplate);
                editApp.updateModelFromUi($("#cmpCommandLineToPass" + cmpId), cmp.Settings.CommandLineToPass, isTemplate);
                editApp.updateModelFromUi($("#cmpScriptBody" + cmpId), cmp.Settings.ScriptBody, isTemplate);
                editApp.updateModelFromUi($('#cmpStatusRollupType' + cmpId), cmp.Settings.StatusRollupType, isTemplate);
                
                var template = model.getComponentTemplate(cmp.TemplateId);
                editApp.updateDynamicColumnsModelFromUi(cmp, template, isTemplate);
            };
            
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);

            onStateChanged();
        })();
	<% }%>
</script>
    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AuthenticationType %>" BaseValueName="cmpAuthenticationType" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>"
			Options="<%# DropdownOptionsHelper.OptionsFromEnum<SshAuthenticationType>()%>" IsComplexSetting="true" runat="server" />
    <tbody class="subEditorUsernamePassword">
    	<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CredentialForMonitoring %>" BaseValueName="cmpCredentialUsernamePassword"  ElementId="<%# Master.ComponentId%>" EditorMode ="<%#Master.EditMode%>" runat="server" />
	</tbody>
    <tbody class="subEditorUsernamePrivateKey">
    	<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CertificateForMonitoring %>" BaseValueName="cmpCredentialUsernamePrivateKey"  ElementId="<%# Master.ComponentId%>" EditorMode ="<%#Master.EditMode%>" runat="server" />
	</tbody>
	<apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPort" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="0" runat="server"  />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ScriptWorkingDirectory %>" BaseValueName="cmpScriptDirectory" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
	<apm:BooleanCheckboxEditor ID="BooleanCheckboxEditor1" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CommandLine %>" BaseValueName="cmpCommandLineToPass" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
	<apm:ScriptBodyEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ScriptBody %>" BaseValueName="cmpScriptBody" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:ColumnTemplate ID="ColumnTemplate1" ElementId="<%# Master.ComponentId %>" runat="server" />
    <apm:DropdownEditor ID="DropdownEditor1" runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_StatusRollUp %>" BaseValueName="cmpStatusRollupType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>"
        Options="<%# DropdownOptionsHelper.OptionsFromEnum<StatusRollupMethod>()%>"/>
</asp:Content>
