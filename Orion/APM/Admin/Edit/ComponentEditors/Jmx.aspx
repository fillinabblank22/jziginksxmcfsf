﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<script runat="server">
	protected override void OnLoad(EventArgs e)
	{
		DataBind();
	}
</script>

<asp:Content ID="c1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
      <apm:ComponentTypeEditor ComponentType="Jmx" runat="server"/>
</asp:Content>
<asp:Content ID="c2" ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
	<script type="text/javascript">
<% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["Jmx"];
				editApp.initMultiUiState($("#cmpCountAsDifference" + cmpId), cmp.DefinitionSettings, "CountAsDifference");
				editApp.initMultiUiState($("#cmpPortNumber" + cmpId), cmp.DefinitionSettings, "JmxPortNumber");
				editApp.initMultiUiState($("#cmpProtocol" + cmpId), cmp.DefinitionSettings, "JmxProtocol");
			    editApp.initMultiUiState($("#cmpURLPath" + cmpId), cmp.DefinitionSettings, "JmxUrlPath");
			    editApp.initMultiUiState($("#cmpCustomURLFormat" + cmpId), cmp.DefinitionSettings, "JmxUrlFormat");
			    editApp.initMultiUiState($("#cmpObjectName" + cmpId), cmp.DefinitionSettings, "ObjectName");
			    editApp.initMultiUiState($("#cmpAttributeName" + cmpId), cmp.DefinitionSettings, "AttributeName");
			    editApp.initMultiUiState($("#cmpKey" + cmpId), cmp.DefinitionSettings, "Key");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                
				editApp.updateMultiModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings, "JmxPortNumber", cmpIds);
            	editApp.updateMultiModelFromUi($("#cmpProtocol" + cmpId), cmp.Settings, "JmxProtocol", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpURLPath" + cmpId), cmp.Settings, "JmxUrlPath", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCustomURLFormat" + cmpId), cmp.Settings, "JmxUrlFormat", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpObjectName" + cmpId), cmp.Settings, "ObjectName", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpAttributeName" + cmpId), cmp.Settings, "AttributeName", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpKey" + cmpId), cmp.Settings, "Key", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
			};
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	<% } else { %>
        (function() {
            var cmpId = <%=Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$("#cmpDesc" + cmpId), "UserDescription", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpDisabled" + cmpId), "IsDisabled", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpNotes" + cmpId), "UserNote", cmp, template] },
                    {
                        func: editApp.addCredentialSets, 
                        args: [$("#cmpCredential" + cmpId), function() {
                            editApp.initUiState($("#cmpCredential" + cmpId), "CredentialSetId", cmp, template);
                        }]
                    },                
                    { func: editApp.initUiState, args: [$("#cmpCountAsDifference" + cmpId), "CountAsDifference", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpPortNumber" + cmpId), "JmxPortNumber", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpProtocol" + cmpId), "JmxProtocol", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpURLPath" + cmpId), "JmxUrlPath", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpCustomURLFormat" + cmpId), "JmxUrlFormat", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpObjectName" + cmpId), "ObjectName", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpAttributeName" + cmpId), "AttributeName", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpKey" + cmpId), "Key", cmp.Settings, templateSettings] },                
                    { func: editApp.initDataTransform, args: [$("#cmpConvertValue" + cmpId), cmp, template] },
                    { func: editApp.initThreshold, args: [$("#cmpStatisticThreshold" + cmpId), "StatisticData", cmp.Thresholds, templateThresholds] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
               });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($("#cmpDesc" + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($("#cmpDisabled" + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($("#cmpNotes" + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($("#cmpCredential" + cmpId), cmp.CredentialSetId, isTemplate);
                
                editApp.updateModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings.CountAsDifference, isTemplate);
                editApp.updateModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings.JmxPortNumber, isTemplate);
                editApp.updateModelFromUi($("#cmpProtocol" + cmpId), cmp.Settings.JmxProtocol, isTemplate);
                editApp.updateModelFromUi($("#cmpURLPath" + cmpId), cmp.Settings.JmxUrlPath, isTemplate);
                editApp.updateModelFromUi($("#cmpCustomURLFormat" + cmpId), cmp.Settings.JmxUrlFormat, isTemplate);
                editApp.updateModelFromUi($("#cmpObjectName" + cmpId), cmp.Settings.ObjectName, isTemplate);
                editApp.updateModelFromUi($("#cmpAttributeName" + cmpId), cmp.Settings.AttributeName, isTemplate);
                editApp.updateModelFromUi($("#cmpKey" + cmpId), cmp.Settings.Key, isTemplate);
				
				editApp.updateDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds.StatisticData, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>

	<apm:BooleanCheckboxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
	<apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" MinValue="0" EditorMode="<%# Master.EditMode%>" runat="server"  />
	<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Protocol %>" BaseValueName="cmpProtocol" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<JmxProtocol>()%>" EditorMode="<%# Master.EditMode%>" runat="server" />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_URLPath %>" BaseValueName="cmpURLPath" Required="false" ElementId="<%# Master.ComponentId %>" 
		HelpTextBottom="<div style='background-color: #FFFDCC; font-size: 8pt; padding: 5px; margin-top: 3px; line-height: 16px;'><img alt='Lightbulb icon' src='/Orion/APM/Images/Admin/icon_lightbulb.gif' height='16px' style='top: 3px; position: relative; margin-right: 5px;'/>Need help finding the path to your MBeans? Use the <a href='/Orion/APM/Admin/MonitorLibrary/AppFinder/Default.aspx' target='_blank'>Find Processes, Services and Performance Counters</a> wizard.</div>"
		runat="server"  EditorMode="<%# Master.EditMode%>"/>
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CustomURLFormat %>" BaseValueName="cmpCustomURLFormat" Required="false" ElementId="<%# Master.ComponentId %>" 
		HelpTextBottom="<div style='background-color: #FFFDCC; font-size: 8pt; padding: 5px; margin-top: 3px; line-height: 16px;'>Available macros: ${IP}, ${PORT}, ${PROTOCOL}, ${PATH}. Default format will be used if left empty.</div>"
		runat="server"  EditorMode="<%# Master.EditMode%>"/>
	
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ObjectName %>" BaseValueName="cmpObjectName" ElementId="<%# Master.ComponentId %>" runat="server"  EditorMode="<%# Master.EditMode%>"/>
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AttributeName %>" BaseValueName="cmpAttributeName" ElementId="<%# Master.ComponentId %>" runat="server"  EditorMode="<%# Master.EditMode%>"/>
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Key %>" BaseValueName="cmpKey" Required="false" ElementId="<%# Master.ComponentId %>" runat="server"  EditorMode="<%# Master.EditMode%>"/>
	
	<apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode%>"/>  
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>

