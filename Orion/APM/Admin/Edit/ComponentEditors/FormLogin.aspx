﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="Resources" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<script runat="server">
	protected override void OnLoad(EventArgs e)
 {
     var loginHelpLink = string.Format(System.Globalization.CultureInfo.InvariantCulture,"<a href='{0}' target='_blank'>Regular Expression</a>",
                                       SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionAPMAGRegularExpressionPatternMatchingExamples"));
     loginFailed.DisplayText = APMWebContent.EditComponentEditors_LoginFailed + " " + loginHelpLink;
     loginSuccess.DisplayText = APMWebContent.EditComponentEditors_LoginSuccess + " " + loginHelpLink;
	 DataBind();
	}
</script>
<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor ComponentType="FormLogin" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
   <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["FormLogin"];

				editApp.initMultiUiState($('#cmpPortNumber' + cmpId), cmp.DefinitionSettings, "PortNumber");
				editApp.initMultiUiState($('#cmpUrl' + cmpId), cmp.DefinitionSettings, "Url");
				editApp.initMultiUiState($('#cmpHostHeader' + cmpId), cmp.DefinitionSettings, "HostHeader");
				editApp.initMultiUiState($('#cmpUseProxy' + cmpId), cmp.DefinitionSettings, "UseProxy");
				editApp.initMultiUiState($('#cmpProxyAddress' + cmpId), cmp.DefinitionSettings, "ProxyAddress");
				editApp.initMultiUiState($('#cmpIgnoreCA' + cmpId), cmp.DefinitionSettings, "IgnoreCA");
                editApp.initMultiUiState($('#cmpIgnoreCN' + cmpId), cmp.DefinitionSettings, "IgnoreCN");
                editApp.initMultiUiState($('#cmpIgnoreCRL' + cmpId), cmp.DefinitionSettings, "IgnoreCRL");
				editApp.initMultiUiState($('#cmpAcceptCompression' + cmpId), cmp.DefinitionSettings, "AcceptCompression");
				editApp.initMultiUiState($('#cmpLoginFormKeywords' + cmpId), cmp.DefinitionSettings, "FormKeywords");
				editApp.initMultiUiState($('#cmpLoginControlKeywords' + cmpId), cmp.DefinitionSettings, "LoginKeywords");
				editApp.initMultiUiState($('#cmpPasswordControlKeywords' + cmpId), cmp.DefinitionSettings, "PasswordKeywords");
				editApp.initMultiUiState($('#cmpLoginFailed' + cmpId), cmp.DefinitionSettings, "ErrorKeywords");
				editApp.initMultiUiState($('#cmpLoginSuccess' + cmpId), cmp.DefinitionSettings, "SuccessKeywords");
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                
				editApp.updateMultiModelFromUi($('#cmpPortNumber' + cmpId), cmp.Settings, "PortNumber", cmpIds);
				editApp.updateMultiModelFromUi($('#cmpUrl' + cmpId), cmp.Settings, "Url", cmpIds);
				editApp.updateMultiModelFromUi($('#cmpHostHeader' + cmpId), cmp.Settings, "HostHeader", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpUseProxy' + cmpId), cmp.Settings, "UseProxy", cmpIds);
				editApp.updateMultiModelFromUi($('#cmpProxyAddress' + cmpId), cmp.Settings, "ProxyAddress", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpIgnoreCA' + cmpId), cmp.Settings, "IgnoreCA", cmpIds);
                editApp.updateMultiModelFromUi($('#cmpIgnoreCN' + cmpId), cmp.Settings, "IgnoreCN", cmpIds);
                editApp.updateMultiModelFromUi($('#cmpIgnoreCRL' + cmpId), cmp.Settings, "IgnoreCRL", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpAcceptCompression' + cmpId), cmp.Settings, "AcceptCompression", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpLoginFormKeywords' + cmpId), cmp.Settings, "FormKeywords", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpLoginControlKeywords' + cmpId), cmp.Settings, "LoginKeywords", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpPasswordControlKeywords' + cmpId), cmp.Settings, "PasswordKeywords", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpLoginFailed' + cmpId), cmp.Settings, "ErrorKeywords", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpLoginSuccess' + cmpId), cmp.Settings, "SuccessKeywords", cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
            	
			};
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	<% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;
           
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },                
                    { func: editApp.initUiState, args: [$('#cmpPortNumber' + cmpId), 'PortNumber', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpUrl' + cmpId), 'Url', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpHostHeader' + cmpId), 'HostHeader', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpUseProxy' + cmpId), 'UseProxy', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpProxyAddress' + cmpId), 'ProxyAddress', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIgnoreCA' + cmpId), 'IgnoreCA', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIgnoreCN' + cmpId), 'IgnoreCN', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIgnoreCRL' + cmpId), 'IgnoreCRL', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpAcceptCompression' + cmpId), 'AcceptCompression', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLoginFormKeywords' + cmpId), 'FormKeywords', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLoginControlKeywords' + cmpId), 'LoginKeywords', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpPasswordControlKeywords' + cmpId), 'PasswordKeywords', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLoginFailed' + cmpId), 'ErrorKeywords', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLoginSuccess' + cmpId), 'SuccessKeywords', cmp.Settings, templateSettings] },               
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
               });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                
                editApp.updateModelFromUi($('#cmpPortNumber' + cmpId), cmp.Settings.PortNumber, isTemplate);
                editApp.updateModelFromUi($('#cmpUrl' + cmpId), cmp.Settings.Url, isTemplate);
                editApp.updateModelFromUi($('#cmpHostHeader' + cmpId), cmp.Settings.HostHeader, isTemplate);
                editApp.updateModelFromUi($('#cmpUseProxy' + cmpId), cmp.Settings.UseProxy, isTemplate);
                editApp.updateModelFromUi($('#cmpProxyAddress' + cmpId), cmp.Settings.ProxyAddress, isTemplate);
                editApp.updateModelFromUi($('#cmpIgnoreCA' + cmpId), cmp.Settings.IgnoreCA, isTemplate);
                editApp.updateModelFromUi($('#cmpIgnoreCN' + cmpId), cmp.Settings.IgnoreCN, isTemplate);
                editApp.updateModelFromUi($('#cmpIgnoreCRL' + cmpId), cmp.Settings.IgnoreCRL, isTemplate);
                editApp.updateModelFromUi($('#cmpAcceptCompression' + cmpId), cmp.Settings.AcceptCompression, isTemplate);
                editApp.updateModelFromUi($('#cmpLoginFormKeywords' + cmpId), cmp.Settings.FormKeywords, isTemplate);
                editApp.updateModelFromUi($('#cmpLoginControlKeywords' + cmpId), cmp.Settings.LoginKeywords, isTemplate);
                editApp.updateModelFromUi($('#cmpPasswordControlKeywords' + cmpId), cmp.Settings.PasswordKeywords, isTemplate);
                editApp.updateModelFromUi($('#cmpLoginFailed' + cmpId), cmp.Settings.ErrorKeywords, isTemplate);
                editApp.updateModelFromUi($('#cmpLoginSuccess' + cmpId), cmp.Settings.SuccessKeywords, isTemplate);

                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId),cmp.Thresholds.Response, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%# Master.EditMode%>" MinValue="0"/> 
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Url %>" BaseValueName="cmpUrl" ElementId="<%# Master.ComponentId %>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HostHeader %>" BaseValueName="cmpHostHeader" Required="false" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode="<%#Master.EditMode%>"  />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseProxy %>" BaseValueName="cmpUseProxy" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ProxyAddress %>" BaseValueName="cmpProxyAddress" Required="false" ElementId="<%# Master.ComponentId %>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCAErrors %>" BaseValueName="cmpIgnoreCA" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCNErrors %>" BaseValueName="cmpIgnoreCN" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCRLErrors %>" BaseValueName="cmpIgnoreCRL" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AcceptCompression %>" BaseValueName="cmpAcceptCompression" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LoginFormKeywords %>" BaseValueName="cmpLoginFormKeywords" Required="false" ElementId="<%# Master.ComponentId %>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LoginControlKeywords %>" BaseValueName="cmpLoginControlKeywords" Required="false" ElementId="<%# Master.ComponentId %>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PasswordControlKeywords %>" BaseValueName="cmpPasswordControlKeywords" Required="false" ElementId="<%# Master.ComponentId %>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor ID="loginFailed"  BaseValueName="cmpLoginFailed" Required="false" ElementId="<%# Master.ComponentId %>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor ID="loginSuccess"  BaseValueName="cmpLoginSuccess" Required="false" ElementId="<%# Master.ComponentId %>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
		<apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
 </asp:Content>