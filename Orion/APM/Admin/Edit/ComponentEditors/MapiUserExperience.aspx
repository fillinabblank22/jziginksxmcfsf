﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="MapiUserExperience" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SendEmailFrom %>" BaseValueName="cmpSendEmailFrom" ElementId="<%#Master.ComponentId%>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SendEmailTo %>" BaseValueName="cmpSendEmailTo" ElementId="<%#Master.ComponentId%>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_MapiProfileName %>" BaseValueName="cmpMapiProfileName" Required="false" ElementId="<%#Master.ComponentId%>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SMTPServer %>" BaseValueName="cmpSMTPServer" ElementId="<%#Master.ComponentId%>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SMTPPort %>" BaseValueName="cmpSMTPPort" ElementId="<%# Master.ComponentId%>" MinValue="0" EditorMode="<%# Master.EditMode%>" runat="server"  />
    <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseCredentialsforSMTP %>" BaseValueName="cmpUseCredentialsForSMTP" ElementId="<%#  Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CredentialforSMTP %>" BaseValueName="cmpCredentialSMTP"  ElementId="<%# Master.ComponentId%>" EditorMode ="<%#Master.EditMode%>" runat="server" />
    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SMTPEncryption %>" BaseValueName="cmpSMTPEncryption" ElementId="<%#  Master.ComponentId %>" 
		Options="<%# DropdownOptionsHelper.OptionsFromEnum<UseSSLSettings>()%>" EditorMode="<%# Master.EditMode%>" runat="server"/>
	<apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId%>" EditorMode="<%# Master.EditMode%>" runat="server"  />
    
    <script language="javascript">
        SW.APM.EditApp.Editors.MapiUserExperience.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>'
            );
    </script>
</asp:Content>
