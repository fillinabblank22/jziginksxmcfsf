﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Management.WinRm" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="DirectorySize" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["DirectorySize"];
                editApp.initMultiUiState($("#cmpFullDirectoryPath" + cmpId), cmp.DefinitionSettings, "FullDirPath");
			    editApp.initMultiUiState($("#cmpFileNamesFilter" + cmpId), cmp.DefinitionSettings, "FileNamesFilter");
			    editApp.initMultiUiState($("#cmpFileExtensionsFilter" + cmpId), cmp.DefinitionSettings, "FileExtensionsFilter");
			    editApp.initMultiUiState($("#cmpWinRmAuthenticationMechanism" + cmpId), cmp.DefinitionSettings, "WinRmAuthenticationMechanism");
			    editApp.initMultiUiState($("#cmpIncludeSubdirectories" + cmpId), cmp.DefinitionSettings, "IncludeSubDirs");
			    editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                editApp.updateMultiModelFromUi($("#cmpFullDirectoryPath" + cmpId), cmp.Settings, "FullDirPath", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpFileNamesFilter" + cmpId), cmp.Settings, "FileNamesFilter", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpFileExtensionsFilter" + cmpId), cmp.Settings, "FileExtensionsFilter", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpWinRmAuthenticationMechanism" + cmpId), cmp.Settings, "WinRmAuthenticationMechanism", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpIncludeSubdirectories" + cmpId), cmp.Settings, "IncludeSubDirs", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%=Master.ComponentId %>;
            var editApp = SW.APM.EditApp;

            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;

                if ($.browser.msie) {
                    parentComponentContainer = componentContainer.parent().parent()[0];
                } else {
                    parentComponentContainer = componentContainer.parent()[0];
                }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();

                var cmp = this;
                
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    { func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },
                    { func: editApp.initUiState, args: [$('#cmpFullDirectoryPath' + cmpId), 'FullDirPath', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpFileNamesFilter' + cmpId), 'FileNamesFilter', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpFileExtensionsFilter' + cmpId), 'FileExtensionsFilter', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpWinRmAuthenticationMechanism' + cmpId), 'WinRmAuthenticationMechanism', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpIncludeSubdirectories' + cmpId), 'IncludeSubDirs', cmp.Settings, templateSettings] },
                    { func: editApp.initDataTransform, args: [$('#cmpConvertValue' + cmpId), cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] }
                ];
                
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
               });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
				
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateModelFromUi($('#cmpFullDirectoryPath' + cmpId), cmp.Settings.FullDirPath, isTemplate);
                editApp.updateModelFromUi($('#cmpFileNamesFilter' + cmpId), cmp.Settings.FileNamesFilter, isTemplate);
                editApp.updateModelFromUi($('#cmpWinRmAuthenticationMechanism' + cmpId), cmp.Settings.WinRmAuthenticationMechanism, isTemplate);
                editApp.updateModelFromUi($('#cmpFileExtensionsFilter' + cmpId), cmp.Settings.FileExtensionsFilter, isTemplate);
                editApp.updateModelFromUi($('#cmpIncludeSubdirectories' + cmpId), cmp.Settings.IncludeSubDirs, isTemplate);
                editApp.updateDataTransformModelFromUi($('#cmpConvertValue' + cmpId), cmp, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
		<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WinRmAuthenticationMechanism %>" BaseValueName="cmpWinRmAuthenticationMechanism" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<WinRmAuthenticationMechanism>() %>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FullDirectoryPath %>" BaseValueName="cmpFullDirectoryPath" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false"  runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FileNamesFilter %>" BaseValueName="cmpFileNamesFilter" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false"  runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FileExtensionsFilter %>" BaseValueName="cmpFileExtensionsFilter" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false"  runat="server" />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IncludeSubdirectories %>" BaseValueName="cmpIncludeSubdirectories" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />  
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />   
</asp:Content>