﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor ComponentType="RadiusUserExperience" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
<% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["RadiusUserExperience"];
				editApp.initMultiUiState($('#cmpAuthenticationPort' + cmpId), cmp.DefinitionSettings, "AuthenticationPort");
				editApp.initMultiUiState($('#cmpAccountingPort' + cmpId), cmp.DefinitionSettings, "AccountingPort");
				editApp.initMultiUiState($('#cmpSecretKey' + cmpId), cmp.DefinitionSettings, "SecretKey");
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
             };
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                
				editApp.updateMultiModelFromUi($('#cmpAuthenticationPort' + cmpId), cmp.Settings, "AuthenticationPort", cmpIds);
				editApp.updateMultiModelFromUi($('#cmpAccountingPort' + cmpId), cmp.Settings, "AccountingPort", cmpIds);
            	editApp.updateMultiModelFromUi($('#cmpSecretKey' + cmpId), cmp.Settings, "SecretKey", cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
			};
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	<% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpAuthenticationPort' + cmpId), 'AuthenticationPort', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpAccountingPort' + cmpId), 'AccountingPort', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSecretKey' + cmpId), 'SecretKey', cmp.Settings, templateSettings] },                
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpCredential' + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateModelFromUi($('#cmpAuthenticationPort' + cmpId),cmp.Settings.AuthenticationPort, isTemplate);
                editApp.updateModelFromUi($('#cmpAccountingPort' + cmpId), cmp.Settings.AccountingPort, isTemplate);
                editApp.updateModelFromUi($('#cmpSecretKey' + cmpId), cmp.Settings.SecretKey, isTemplate);
                
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId), cmp.Thresholds.Response, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
   		<apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AuthenticationPort %>" BaseValueName="cmpAuthenticationPort" ElementId="<%# Master.ComponentId%>" MinValue="0" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AccountingPort %>" BaseValueName="cmpAccountingPort" ElementId="<%# Master.ComponentId%>" MinValue="0" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SecretKey %>" BaseValueName="cmpSecretKey" ElementId="<%#Master.ComponentId%>" IsMultiline="false" EditorMode="<%# Master.EditMode%>" runat="server" />
	    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%#Master.ComponentId%>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>
