﻿<%@ Page AutoEventWireup="true" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="ComponentEditorBase.master" %>
<%@ MasterType VirtualPath="ComponentEditorWithCredentials.master"%>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<script runat="server">
    protected override void OnPreRender(EventArgs e)
    {
        this.phSingleEdit.Visible = !this.Master.IsMultiEditMode;

        base.OnPreRender(e);
    }
</script>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" runat="server">
    <apm:ComponentTypeEditor ComponentType="Soap" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" runat="server">
    <asp:PlaceHolder runat="server" ID="phSingleEdit">
    <tr>
        <td colspan="3"><div class="settings-group-row"><%= Resources.APMWebContent.EditComponentEditors_SOAPSettings%></div></td>
    </tr>
    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SOAPXMLRequest %>" BaseValueName="cmpSoapLoadType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsComplexSetting="true" Options="<%# DropdownOptionsHelper.OptionsFromEnum<SoapLoadType>()%>" runat="server" />
    <tbody id="wsdlAutoLoad<%#Master.ComponentId%>" class="subEditor">
        <apm:SoapFileUpload DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SelectAnURL %>" BaseValueName="cmpWsdlUrl" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <tr style="display: none;">
            <td class="columnLabel edit label"></td>
            <td style="" class="columnEditor edit number">
                <span class="viewer"></span>
            </td>
            <td class="columnOverride">
            </td>
        </tr>
        <tr class="editor">
            <td>&nbsp;</td>
            <td colspan="2">
                <table style="width: 100%;">
                    <tr style="height: 47px;">
                        <td style="padding-left: 0px; width: 23%;">
                            <button id="cmpWsdlUrl<%= Master.ComponentId %>Button" class="sw-btn sw-btn-secondary">Load WSDL</button>
                        </td>
                        <td colspan="2">
                            <div class="loading-cnt-load"><%= Resources.APMWebContent.LoadingPleaseWait%></div>
                            <div class="error-cnt-load"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="wsdl-info">
            <td>&nbsp;</td>
            <td colspan="2">
                <table>
                    <tbody class="subEditor">
                        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WebServiceServerURL %>" BaseValueName="cmpServiceUrlAuto" Required="true" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" AdditionalLabelCss="innerSubEditorLabel" runat="server" />
                        <apm:SoapDropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Binding %>" BaseValueName="cmpBinding" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
                        <apm:SoapDropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ServiceName %>" BaseValueName="cmpServiceName" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
                        <apm:SoapDropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_MethodName %>" BaseValueName="cmpMethodName" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
                        <apm:SoapArgumentsEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Arguments %>" BaseValueName="cmpArguments" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
                        <tr style="height: 47px;" class="editor">
                            <td>&nbsp;</td>
                            <td>
                                <div class="loading-cnt-generate"><%= Resources.APMWebContent.LoadingPleaseWait%></div>
                                <div class="error-cnt-generate"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><%= Resources.APMWebContent.AdvancedSettings %></td>
                        </tr>
                        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_MethodNamespace %>" BaseValueName="cmpMethodNamespace" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsReadOnly="true" runat="server" />
                        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SchemaNamespace %>" BaseValueName="cmpSchemaNamespace" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsReadOnly="true" runat="server" />
                        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SOAPAction %>" BaseValueName="cmpSoapActionAuto" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsReadOnly="true" runat="server" />
                        <apm:SoapDropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SOAPVersion %>" BaseValueName="cmpSoapVersionAuto" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<SoapVersion>()%>" IsDisabled="true" runat="server" />
                        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SOAPXML %>" BaseValueName="cmpRequestBodyAuto" DisplayHelpText="Edit SOAP XML" Required="true" IsComplexSetting="true" ElementId="<%# Master.ComponentId %>" IsMultiline="true" EditorMode="<%# Master.EditMode %>" runat="server" />
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
    <tbody id="wsdlManualLoad<%#Master.ComponentId%>" class="subEditor">
        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WebServiceServerURL %>" BaseValueName="cmpServiceUrl" Required="true" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SOAPAction %>" BaseValueName="cmpSoapAction" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
        <apm:SoapDropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SOAPVersion %>" BaseValueName="cmpSoapVersion" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<SoapVersion>()%>" runat="server" />
        <apm:SoapStringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SOAPXML %>" BaseValueName="cmpRequestBody" Required="true" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsMultiline="true" runat="server" />
    </tbody>
    </asp:PlaceHolder>

    <tr>
        <td colspan="3"><div class="settings-group-row"><%= Resources.APMWebContent.EditComponentEditors_HttpSettings%></div></td>
    </tr>

    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HostHeader %>" BaseValueName="cmpHostHeader" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>"  runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HTTPUserAgent %>" BaseValueName="cmpUserAgent" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>"  runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_HTTPContentType %>" BaseValueName="cmpContentType" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>"  runat="server" />
    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AuthenticateMode %>" BaseValueName="cmpAuthMode" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" Options="<%# DropdownOptionsHelper.OptionsFromEnum<AuthMode>() %>" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_AcceptCompression %>" BaseValueName="cmpAcceptCompression" ElementId="<%# Master.ComponentId %>"  TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" EditorMode="<%# Master.EditMode %>" runat="server" />

    <tr>
        <td colspan="3"><div class="settings-group-row"><%= Resources.APMWebContent.EditComponentEditors_CertificateInformation%></div></td>
    </tr>
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CertificateSubject %>" BaseValueName="cmpCertificateSubject" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" IsMultiline="true" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCAErrors %>" BaseValueName="cmpIgnoreCA" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCAErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCAErrors %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCNErrors %>" BaseValueName="cmpIgnoreCN" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCNErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCNErrors %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCRLErrors %>" BaseValueName="cmpIgnoreCRL" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_IgnoreCRLErrors %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_DoNotIgnoreCRLErrors %>" runat="server" />

    <tr>
        <td colspan="3"><div class="settings-group-row"><%= Resources.APMWebContent.EditComponentEditors_ProxySettings%></div></td>
    </tr>
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UseProxy %>" BaseValueName="cmpUseProxy" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_UseProxy %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_DoNotUseProxy %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ProxyAddress %>" BaseValueName="cmpProxyAddress" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />

    <tr>
        <td colspan="3"><div class="settings-group-row"><%= Resources.APMWebContent.EditComponentEditors_ThresholdConfiguration%></div></td>
    </tr>
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SearchString %>" BaseValueName="cmpSearchString" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FailIfFound %>" BaseValueName="cmpFailIfFound" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />

    <script type="text/javascript">
        SW.APM.EditApp.Editors.Soap.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>'
        );
    </script>
</asp:Content>
