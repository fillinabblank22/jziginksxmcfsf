﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType  virtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master"%>

<asp:Content ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server"> 
 <apm:ComponentTypeEditor ComponentType="ServiceStateSnmp" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ComponentEditorPlaceholder" runat="server">
    <script type="text/javascript">
  <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["ServiceStateSnmp"];
			    editApp.initMultiUiState($('#cmpSvcName' + cmpId), cmp.DefinitionSettings, "ServiceDisplayName");
			    editApp.initMultiUiThreshold($("#cmpStatisticThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
              };
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
				editApp.updateMultiModelFromUi($('#cmpSvcName' + cmpId), cmp.Settings, "ServiceDisplayName", cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpStatisticThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds)
              };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpSvcName' + cmpId), 'ServiceDisplayName', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpStatisticThreshold' + cmpId), 'StatisticData', cmp.Thresholds, templateThresholds] }
                ];
                 var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpSvcName' + cmpId), cmp.Settings.ServiceDisplayName, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
				editApp.updateThresholdModelFromUi($('#cmpStatisticThreshold' + cmpId), cmp.Thresholds.StatisticData, isTemplate);
            };
            
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
            
        })();
     <% }%>
    </script>
		<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_NetServiceDisplayName %>" BaseValueName="cmpSvcName" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>