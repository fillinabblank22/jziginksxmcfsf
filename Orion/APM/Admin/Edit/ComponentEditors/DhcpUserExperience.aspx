﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType  virtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master"%>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server"> 
 <apm:ComponentTypeEditor ComponentType="DhcpUserExperience" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">

    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["DhcpUserExperience"];
			    editApp.initMultiUiState($("#cmpRequestPort" + cmpId), cmp.DefinitionSettings, "RequestPort");
			    editApp.initMultiUiState($("#cmpResponsePort" + cmpId), cmp.DefinitionSettings, "ResponsePort");
			    editApp.initMultiUiState($("#cmpSenderAdapterIP" + cmpId), cmp.DefinitionSettings, "SenderAdapterIPAddress");
			    editApp.initMultiUiState($("#cmpRequestType" + cmpId), cmp.DefinitionSettings, "DiscoveryRequestType");
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
			    editApp.updateMultiModelFromUi($("#cmpRequestPort" + cmpId), cmp.Settings, "RequestPort", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpResponsePort" + cmpId), cmp.Settings, "ResponsePort", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpSenderAdapterIP" + cmpId), cmp.Settings, "SenderAdapterIPAddress", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpRequestType" + cmpId), cmp.Settings, "DiscoveryRequestType", cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId%>;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;

                if ($.browser.msie) {
                    parentComponentContainer = componentContainer.parent().parent()[0];
                } else {
                    parentComponentContainer = componentContainer.parent()[0];
                }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();

                var cmp = this;

                
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;
                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$('#cmpRequestPort' + cmpId), 'RequestPort', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpResponsePort' + cmpId), 'ResponsePort', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpSenderAdapterIP' + cmpId), 'SenderAdapterIPAddress', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpRequestType' + cmpId), 'DiscoveryRequestType', cmp.Settings, templateSettings] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
               });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
				
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId), cmp.Thresholds.Response, isTemplate);

                editApp.updateModelFromUi($('#cmpRequestPort' + cmpId), cmp.Settings.RequestPort, isTemplate);
                editApp.updateModelFromUi($('#cmpResponsePort' + cmpId), cmp.Settings.ResponsePort, isTemplate);
                editApp.updateModelFromUi($('#cmpSenderAdapterIP' + cmpId), cmp.Settings.SenderAdapterIPAddress, isTemplate);
                editApp.updateModelFromUi($('#cmpRequestType' + cmpId), cmp.Settings.DiscoveryRequestType, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
       	<apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UDPRequestPort %>" BaseValueName="cmpRequestPort" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="0" HelpTextBottom="<i>IPv4 default port: <b>67</b>. IPv6 default port: <b>547</b>.</i>" runat="server"  />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_UDPResponsePort %>" BaseValueName="cmpResponsePort" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="0" HelpTextBottom="<i>IPv4 default port: <b>68</b>. IPv6 default port: <b>546</b>.</i>" runat="server"  />
		<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_SenderAdapterIPAddress %>" BaseValueName="cmpSenderAdapterIP" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
        <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_DHCPDiscoveryRequestType %>" BaseValueName="cmpRequestType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<DhcpDiscoveryRequestType>()%>" runat="server"/>
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />

 </asp:Content>