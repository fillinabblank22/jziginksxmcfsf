﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Management.WinRm" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="WmiPerformanceCounter"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">

	<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_FetchingMethod %>" BaseValueName="cmpFetchingMethod" ElementId="<%# Master.ComponentId  %>" 
		EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<EventLog_FetchingMethod>()%>" runat="server" />
	<apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WinRmAuthenticationMechanism %>" BaseValueName="cmpWinRmAuthenticationMechanism" ElementId="<%# Master.ComponentId %>" 
		EditorMode="<%# Master.EditMode %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<WinRmAuthenticationMechanism>() %>" runat="server" />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_WMINamespace %>" BaseValueName="cmpNamespace" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server"  />
    <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Query %>" BaseValueName="cmpQuery" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" IsMultiline="true"  />
    <apm:BooleanCheckboxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCntAsDiff" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server"/>
    <apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode %>" runat="server"  />
    
    <script type="text/javascript">
        SW.APM.EditApp.Editors.WmiPerformanceCounter.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>'
        );
    </script>

</asp:Content>

