﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ MasterType  virtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master"%>

<asp:Content ID="c1" ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server"> 
 <apm:ComponentTypeEditor ComponentType="SnmpGeneric" runat="server" />
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">
	<script type="text/javascript">
	<% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["SnmpGeneric"];
			    editApp.initMultiUiState($("#cmpSnmpMethodType" + cmpId), cmp.DefinitionSettings, "SnmpMethodType");
				editApp.initMultiUiState($("#cmpOID" + cmpId), cmp.DefinitionSettings, "OID");
				editApp.initMultiUiState($("#cmpSNMPPort" + cmpId), cmp.DefinitionSettings, "SNMPPort");
				editApp.initMultiUiState($("#cmpCountAsDifference"+ cmpId), cmp.DefinitionSettings, "CountAsDifference");
                editApp.initMultiDataTransform($("#cmpConvertValue" + cmpId), cmp.DefinitionSettings);
			    editApp.initMultiUiThreshold($("#cmpStatisticDataThreshold" + cmpId), cmp.DefinitionSettings,"StatisticData");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
                
                editApp.updateMultiModelFromUi($("#cmpSnmpMethodType" + cmpId), cmp.Settings, "SnmpMethodType", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpOID" + cmpId), cmp.Settings, "OID", cmpIds);
				editApp.updateMultiModelFromUi($("#cmpSNMPPort" + cmpId), cmp.Settings, "SNMPPort", cmpIds);
            	editApp.updateMultiModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
                editApp.updateMultiDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp.Settings, cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpStatisticDataThreshold" + cmpId), cmp.Thresholds,"StatisticData", cmpIds); 
			};
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	<% } else { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$("#cmpDesc" + cmpId), "UserDescription", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpDisabled" + cmpId), "IsDisabled", cmp, template] }, 
                    { func: editApp.initUiState, args: [$("#cmpSnmpMethodType" + cmpId), "SnmpMethodType", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpOID" + cmpId), "OID", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpSNMPPort" + cmpId), "SNMPPort", cmp.Settings, templateSettings] },
                    { func: editApp.initDataTransform, args: [$("#cmpConvertValue" + cmpId), cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpCountAsDifference" + cmpId), "CountAsDifference", cmp.Settings, templateSettings] },
                    { func: editApp.initThreshold, args: [$("#cmpStatisticDataThreshold" + cmpId), "StatisticData", cmp.Thresholds, templateThresholds] },
                    { func: editApp.initUiState, args: [$("#cmpNotes" + cmpId), "UserNote", cmp, template] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($("#cmpDesc" + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($("#cmpDisabled" + cmpId), cmp.IsDisabled, isTemplate);
                
                editApp.updateModelFromUi($("#cmpSnmpMethodType" + cmpId), cmp.Settings.SnmpMethodType, isTemplate);
				editApp.updateModelFromUi($("#cmpOID" + cmpId), cmp.Settings.OID, isTemplate);
				editApp.updateModelFromUi($("#cmpSNMPPort" + cmpId), cmp.Settings.SNMPPort, isTemplate);
                editApp.updateDataTransformModelFromUi($("#cmpConvertValue" + cmpId), cmp, isTemplate);
            	editApp.updateModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings.CountAsDifference, isTemplate);
				editApp.updateThresholdModelFromUi($("#cmpStatisticDataThreshold" + cmpId), cmp.Thresholds.StatisticData, isTemplate);
                editApp.updateModelFromUi($("#cmpNotes" + cmpId), cmp.UserNote, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
	</script>
    <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_MethodType %>" BaseValueName="cmpSnmpMethodType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<SnmpMethodType>()%>" runat="server"/>
	<apm:OidEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ObjectIdentifierOID %>" BaseValueName="cmpOID" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
	<apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CustomSNMPPort %>" BaseValueName="cmpSNMPPort" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="0" HelpTextBottom="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference_HelpTextBottom %>" runat="server"/>
	<apm:BooleanCheckboxEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>"/>
	<apm:ConvertValueEditor BaseValueName="cmpConvertValue" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
    <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Statistic %>" BaseValueName="cmpStatisticDataThreshold" EditorMode="<%# Master.EditMode%>" ElementId="<%# Master.ComponentId %>" runat="server"  />
	
</asp:Content>
