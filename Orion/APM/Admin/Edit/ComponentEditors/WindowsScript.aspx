﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
   <apm:ComponentTypeEditor runat="server" ComponentType="WindowsScript"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["WindowsScript"];
				editApp.initMultiUiState($("#cmpScriptArguments" + cmpId), cmp.DefinitionSettings, "ScriptArguments");
			    editApp.initMultiUiState($("#cmpScriptBody" + cmpId), cmp.DefinitionSettings, "ScriptBody");
			    editApp.initMultiUiState($("#cmpScriptEngine" + cmpId), cmp.DefinitionSettings, "ScriptEngine");
			    editApp.initMultiUiState($("#cmpCountAsDifference" + cmpId), cmp.DefinitionSettings, "CountAsDifference");
			    editApp.initMultiUiState($("#cmpStatusRollupType" + cmpId), cmp.DefinitionSettings, "StatusRollupType");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
				editApp.updateMultiModelFromUi($("#cmpScriptArguments" + cmpId), cmp.Settings, "ScriptArguments", cmpIds);
			    editApp.updateMultiModelFromUi($("#cmpScriptBody" + cmpId), cmp.Settings, "ScriptBody", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpScriptEngine" + cmpId), cmp.Settings, "ScriptEngine", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings, "CountAsDifference", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpStatusRollupType" + cmpId), cmp.Settings, "StatusRollupType", cmpIds);
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$("#cmpDesc" + cmpId), "UserDescription", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpDisabled" + cmpId), "IsDisabled", cmp, template] },
                    { func: editApp.initUiState, args: [$("#cmpNotes" + cmpId), "UserNote", cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$("#cmpCredential" + cmpId), function() {
                            editApp.initUiState($("#cmpCredential" + cmpId), "CredentialSetId", cmp, template);
                        }]
                    },                
                    { func: editApp.initUiState, args: [$("#cmpScriptArguments" + cmpId), "ScriptArguments", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpScriptBody" + cmpId), "ScriptBody", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpScriptEngine" + cmpId), "ScriptEngine", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$("#cmpCountAsDifference" + cmpId), "CountAsDifference", cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpStatusRollupType' + cmpId), 'StatusRollupType', cmp.Settings, templateSettings] }
                ];
                 var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });
                var isTemplate = !model.haveTemplate();
                editApp.initDynamicColumnsModel(cmp, template, isTemplate, $("#btnEdit<%#Master.ComponentId%>"));
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($("#cmpDesc" + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($("#cmpDisabled" + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($("#cmpCredential" + cmpId), cmp.CredentialSetId, isTemplate);
                editApp.updateModelFromUi($("#cmpNotes" + cmpId), cmp.UserNote, isTemplate);
                
                editApp.updateModelFromUi($("#cmpScriptArguments" + cmpId), cmp.Settings.ScriptArguments, isTemplate);
                editApp.updateModelFromUi($("#cmpScriptBody" + cmpId), cmp.Settings.ScriptBody, isTemplate);
                editApp.updateModelFromUi($("#cmpScriptEngine" + cmpId), cmp.Settings.ScriptEngine, isTemplate);
                editApp.updateModelFromUi($("#cmpCountAsDifference" + cmpId), cmp.Settings.CountAsDifference, isTemplate);
                editApp.updateModelFromUi($('#cmpStatusRollupType' + cmpId), cmp.Settings.StatusRollupType, isTemplate);
                
                var template = model.getComponentTemplate(cmp.TemplateId);
                editApp.updateDynamicColumnsModelFromUi(cmp, template, isTemplate);
            };
            
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ScriptArguments %>" BaseValueName="cmpScriptArguments" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
	<apm:ScriptBodyEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ScriptBody %>" BaseValueName="cmpScriptBody" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
	<apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ScriptEngine %>" BaseValueName="cmpScriptEngine" ElementId="<%# Master.ComponentId  %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
	<apm:BooleanCheckboxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CountStatisticAsDifference %>" BaseValueName="cmpCountAsDifference" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"/>
    <apm:ColumnTemplate ElementId="<%# Master.ComponentId %>" runat="server" />
    <apm:DropdownEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_StatusRollUp %>" BaseValueName="cmpStatusRollupType" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>"
        Options="<%# DropdownOptionsHelper.OptionsFromEnum<StatusRollupMethod>()%>"/>
</asp:Content>