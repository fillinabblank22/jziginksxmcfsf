﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ MasterType  virtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master"%>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ContentPlaceHolderID="ComponentTypePlaceholder" Runat="Server">
    <apm:ComponentTypeEditor runat="server" ComponentType="DnsQA"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="ComponentEditorPlaceholder" Runat="Server">

    <script type="text/javascript">
    <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["DnsQA"];
				editApp.initMultiUiState($("#cmpPortNumber" + cmpId), cmp.DefinitionSettings, "PortNumber");
			    editApp.initMultiUiState($("#cmpPortType" + cmpId), cmp.DefinitionSettings, "PortType");
			    editApp.initMultiUiState($("#cmpNameToResolve" + cmpId), cmp.DefinitionSettings, "NameToResolve");
			    editApp.initMultiUiState($("#cmpExpectedAnswerlist" + cmpId), cmp.DefinitionSettings, "ExpectedAnswerlist");
			    editApp.initMultiUiState($("#cmpExpectAll" + cmpId), cmp.DefinitionSettings, "ExpectAll");
			    editApp.initMultiUiThreshold($("#cmpResponseThreshold" + cmpId), cmp.DefinitionSettings,"Response");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
				editApp.updateMultiModelFromUi($("#cmpPortNumber" + cmpId), cmp.Settings, "PortNumber", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpPortType" + cmpId), cmp.Settings, "PortType", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpNameToResolve" + cmpId), cmp.Settings, "NameToResolve", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpExpectedAnswerlist" + cmpId), cmp.Settings, "ExpectedAnswerlist", cmpIds);
                editApp.updateMultiModelFromUi($("#cmpExpectAll" + cmpId), cmp.Settings, "ExpectAll", cmpIds);
                editApp.updateMultiThresholdFromUi($("#cmpResponseThreshold" + cmpId), cmp.Thresholds,"Response", cmpIds); 
            };
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %> ;
            var editApp = SW.APM.EditApp;
            
            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;
                
                var chunkItemsToProceed = [ 
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                
                    { func: editApp.initUiState, args: [$('#cmpPortNumber' + cmpId), 'PortNumber', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpPortType' + cmpId), 'PortType', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpNameToResolve' + cmpId), 'NameToResolve', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpExpectedAnswerlist' + cmpId), 'ExpectedAnswerlist', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpExpectAll' + cmpId), 'ExpectAll', cmp.Settings, templateSettings] },

                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
                    { func: editApp.initThreshold, args: [$('#cmpResponseThreshold' + cmpId), 'Response', cmp.Thresholds, templateThresholds] }
                ];
                
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
               });
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                
                editApp.updateModelFromUi($('#cmpPortNumber' + cmpId), cmp.Settings.PortNumber, isTemplate);
                editApp.updateModelFromUi($('#cmpPortType' + cmpId), cmp.Settings.PortType, isTemplate);
                editApp.updateModelFromUi($('#cmpNameToResolve' + cmpId), cmp.Settings.NameToResolve, isTemplate);
                editApp.updateModelFromUi($('#cmpExpectedAnswerlist' + cmpId), cmp.Settings.ExpectedAnswerlist, isTemplate);
                editApp.updateModelFromUi($('#cmpExpectAll' + cmpId), cmp.Settings.ExpectAll, isTemplate);

                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
                editApp.updateThresholdModelFromUi($('#cmpResponseThreshold' + cmpId), cmp.Thresholds.Response, isTemplate);
            };
            
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
            
        })();
	<% }%>
    </script>
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_PortNumber %>" BaseValueName="cmpPortNumber" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" MinValue="0" runat="server" />
        <apm:DropdownEditor runat="server" DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_Protocol %>" BaseValueName="cmpPortType" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<PortType>()%>"  EditorMode="<%# Master.EditMode%>"/>
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_NameIPAddressToResolve %>" BaseValueName="cmpNameToResolve" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExpectedDNSQueryResults %>" BaseValueName="cmpExpectedAnswerlist" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" IsMultiline="true"  />
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExpectAllMatches %>" BaseValueName="cmpExpectAll" ElementId="<%# Master.ComponentId %>"  EditorMode="<%# Master.EditMode%>" runat="server" />
        <apm:ThresholdEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ResponseTime %>" BaseValueName="cmpResponseThreshold" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
</asp:Content>

