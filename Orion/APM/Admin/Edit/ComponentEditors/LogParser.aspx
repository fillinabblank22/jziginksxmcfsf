﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>

<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="ComponentEditorBase.master" %>

<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor runat="server" ComponentType="LogParser" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">

    <asp:PlaceHolder runat="server" Visible="<%# this.Master.IsSingleEditMode %>">
        <asp:PlaceHolder runat="server" Visible="<%# SolarWinds.APM.Web.ComponentConstants.IsDevMode %>">
            <asp:PlaceHolder runat="server" Visible="<%# this.Master.Master.IsTemplate %>">
                <apm:BooleanEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_CanBeDisabled %>" BaseValueName="cmpCanBeDisabled" ElementId="<%# Master.ComponentId %>" TrueDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_Yes %>" FalseDisplayValue="<%$ Resources: APMWebContent, EditComponentEditors_No %>" IsComplexSetting="true" runat="server" />
	            <apm:DropdownEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_VisibilityMode %>" BaseValueName="cmpVisibilityMode" ElementId="<%# Master.ComponentId %>" Options="<%# DropdownOptionsHelper.OptionsFromEnum<ComponentVisibilityMode>() %>" EditorMode="<%# Master.EditMode%>" runat="server" />
            </asp:PlaceHolder>
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExecutorName %>" BaseValueName="cmpExecutorName" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" IsMultiline="False" />
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ExecutorAssemblyName %>" BaseValueName="cmpExecutorAssemblyName" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" IsMultiline="False" />
            <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_RowsLimit %>" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" BaseValueName="cmpRowsLimit"/>
            <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_ReadDate %>" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" BaseValueName="cmpReadDate" IsMultiline="False"/>
        </asp:PlaceHolder>
		<apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LogHistoryToRetrieveSeconds %>" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" BaseValueName="cmpTimeLimit" MinValue="0" MaxValue="86400"/>
	    <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_NumberOfLogRecordsToKeep %>" ElementId="<%# Master.ComponentId %>" runat="server" EditorMode ="<%#Master.EditMode%>" BaseValueName="cmpRequestsPerSiteLimit" MinValue="0"/>
    </asp:PlaceHolder>

    <apm:ThresholdEditorList runat="server"/>

    <script type="text/javascript">
        SW.APM.EditApp.Editors.LogParser.init(
            '<%= this.Master.IsMultiEditMode %>',
            '<%= this.Master.ComponentId %>',
            '<%= this.Master.ComponentIds %>',
            '<%= SolarWinds.APM.Web.ComponentConstants.IsDevMode %>',
            '<%= this.Master.Master.IsTemplate %>',
            '<%= this.Master.ThresholdKeysJson %>');
    </script>

</asp:Content>
