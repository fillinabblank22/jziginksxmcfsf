﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ MasterType VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorWithCredentials.master" %>
<%@ Reference VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Web.Plugins" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CredentialsMonitorComponentTypePlaceholder" Runat="Server">
     <apm:ComponentTypeEditor ComponentType="Ldap" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="CredentialsMonitorComponentEditorPlaceholder" Runat="Server">
    
    <script type="text/javascript">
        <% if (Master.IsMultiEditMode) { %>
        (function () {
            var cmpId = <%= Master.ComponentId %>;
            var cmpIds = <%= Master.ComponentIds %>;
            var editApp = SW.APM.EditApp;
            
			var onLoad = function(app) {
                var cmp = SW.APM.EditApp.ComponentDefinitions["Ldap"];
			    editApp.initMultiUiState($("#cmpLDAPSearchRoot" + cmpId), cmp.DefinitionSettings, "LdapPath");
			    editApp.initMultiUiState($("#cmpDomainName" + cmpId), cmp.DefinitionSettings, "DomainName");
			    editApp.initMultiUiState($("#cmpLDAPFilter" + cmpId), cmp.DefinitionSettings, "LdapFilter");
			    editApp.initMultiUiState($("#cmpLDAPAttributes" + cmpId), cmp.DefinitionSettings, "LdapAttributes");
			    editApp.initMultiUiState($("#cmpLDAPSizeLimit" + cmpId), cmp.DefinitionSettings, "LdapSizeLimit");
			    editApp.initMultiUiState($("#cmpUseBaseSearchScope" + cmpId), cmp.DefinitionSettings, "UseBaseSearchScope");
			};
            var onSave = function(app) {
                var cmp = app.getMultiEditComponent();
            };
            
            editApp.Grid.registerMultiEditor(cmpIds, onLoad, onSave);
        })();
	    <% } else { %>
        (function() {
            var cmpId = <%= Master.ComponentId %>;
            var editApp = SW.APM.EditApp;

            var onLoad = function(model) {
                SW.APM.EditApp.ComponentsThatLoadingNow.push(cmpId);
                SW.APM.EventManager.fire("onComponentLoading");
                var componentContainer = $("#componentEditorForm" + cmpId);
                var parentComponentContainer;
                
                 if ($.browser.msie) {
                     parentComponentContainer = componentContainer.parent().parent()[0];
                 } else {
                     parentComponentContainer = componentContainer.parent()[0];
                 }

                var mLoading = new Ext.LoadMask(parentComponentContainer, { msg: "<%= Resources.APMWebContent.LoadingComponentEditorSettings%>" });
                mLoading.show();
                
                var cmp = this;
                var template = model.getComponentTemplate(cmp.TemplateId);
                var templateSettings = template ? template.Settings : null;
                var templateThresholds = template ? template.Thresholds : null;

                var chunkItemsToProceed = [
                    { func: editApp.initUiState, args: [$('#cmpDesc' + cmpId), 'UserDescription', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpDisabled' + cmpId), 'IsDisabled', cmp, template] },
                    {
                        func: editApp.addCredentialSets,
                        args: [$('#cmpCredential' + cmpId), function() {
                            editApp.initUiState($('#cmpCredential' + cmpId), 'CredentialSetId', cmp, template);
                        }]
                    },
                    { func: editApp.initUiState, args: [$('#cmpNotes' + cmpId), 'UserNote', cmp, template] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPSearchRoot' + cmpId), 'LdapPath', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpDomainName' + cmpId), 'DomainName', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPFilter' + cmpId), 'LdapFilter', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPAttributes' + cmpId), 'LdapAttributes', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpLDAPSizeLimit' + cmpId), 'LdapSizeLimit', cmp.Settings, templateSettings] },
                    { func: editApp.initUiState, args: [$('#cmpUseBaseSearchScope' + cmpId), 'UseBaseSearchScope', cmp.Settings, templateSettings] }
                ];
                var initSetting = function (settingItem) {
                    var func = settingItem["func"];
                    var args = settingItem["args"];

                    func.apply(this, args);
                };

                SW.APM.EditApp.Utility.chunk(chunkItemsToProceed, initSetting, function () { 
                 mLoading.hide(); 
                 var index = SW.APM.EditApp.ComponentsThatLoadingNow.indexOf(cmpId);
                 SW.APM.EditApp.ComponentsThatLoadingNow.splice(index,1);
                 SW.APM.EventManager.fire("onComponentLoading");
                });

                SW.APM.EditApp.Editors.hideEnableSettingIfNeeded(cmp, componentContainer);
            };

            var onSave = function(model) {
                var cmp = this;
                var isTemplate = !model.haveTemplate();
                editApp.updateModelFromUi($('#cmpDesc' + cmpId), cmp.UserDescription, isTemplate);
                editApp.updateModelFromUi($('#cmpDisabled' + cmpId), cmp.IsDisabled, isTemplate);
                editApp.updateModelFromUi($('#cmpNotes' + cmpId), cmp.UserNote, isTemplate);
                editApp.updateModelFromUi($('#cmpDomainName' + cmpId), cmp.Settings.DomainName, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPSearchRoot' + cmpId), cmp.Settings.LdapPath, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPFilter' + cmpId), cmp.Settings.LdapFilter, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPAttributes' + cmpId), cmp.Settings.LdapAttributes, isTemplate);
                editApp.updateModelFromUi($('#cmpLDAPSizeLimit' + cmpId), cmp.Settings.LdapSizeLimit, isTemplate);
                editApp.updateModelFromUi($('#cmpUseBaseSearchScope' + cmpId), cmp.Settings.UseBaseSearchScope, isTemplate);
            };
            editApp.Grid.registerEditor(cmpId, onLoad, onSave);
        })();
	<% }%>
    </script>
     
    
    <asp:PlaceHolder runat="server" Visible="<%# SolarWinds.APM.Web.ComponentConstants.IsDevMode %>">
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_DomainName %>" BaseValueName="cmpDomainName" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPSearchRoot %>" BaseValueName="cmpLDAPSearchRoot" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPFilter %>" BaseValueName="cmpLDAPFilter" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:StringEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPAttributes %>" BaseValueName="cmpLDAPAttributes" Required="false" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" IsMultiline="false" runat="server" />
        <apm:NumericEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPSizeLimit %>" BaseValueName="cmpLDAPSizeLimit" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server" MinValue="1"/>
        <apm:BooleanCheckBoxEditor DisplayText="<%$ Resources: APMWebContent, EditComponentEditors_LDAPBaseSearchScope %>" BaseValueName="cmpUseBaseSearchScope" ElementId="<%# Master.ComponentId %>" EditorMode="<%# Master.EditMode%>" runat="server"  />
    </asp:PlaceHolder>
 </asp:Content>