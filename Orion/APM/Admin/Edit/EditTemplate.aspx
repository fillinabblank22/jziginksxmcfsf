﻿<%@ Page Title="New Edit Application Template" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/Edit.master" AutoEventWireup="true" CodeFile="EditTemplate.aspx.cs" Inherits="Orion_APM_Admin_Edit_EditTemplate" %>

<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="apmHeadPlaceholder" Runat="Server">
    <orion:Include ID="i1" File="APM/Admin/Edit/Js/AppModel.Template.js" SpecificOrder="111" runat="server"/>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionAPMPHConfigSettingsAppTemplatesEdit" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="apmTitleContentPlaceHolder" Runat="Server">
    <div id="pageTitle"/>
    <textarea id="pageTitleTemplate" name="pageTitleTemplate" style="display: none">
        <a href='/Orion/APM/Admin/ApplicationTemplates.aspx' title='Template {Name}'>
            <img src='/Orion/APM/Images/StatusIcons/large-template-icon.gif'/><span style='vertical-align:23px;'><b>&nbsp;{Name}&nbsp;</b><%= Resources.APMWebContent.APMWEBDATA_VB1_219%></span>
        </a>
    </textarea>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="apmMainPropertiesContentPlaceHolder" Runat="Server">
	<table id="appMainProp" class="appProperties" cellpadding="0" cellspacing="0">
		<tr>
			<td class="label"><%= Resources.APMWebContent.EditTemplate_TemplateName%></td>
			<td colspan="2">	
				<input id="appName" name="appName" class="edit name required" size="60" type="text" apmsettctr="true"/>
				<apm:ValidationErrorPlaceholder runat="server"/>
			</td>
		</tr>
		<tr>
			<td class="label"><%= Resources.APMWebContent.EditTemplate_TemplateAppliedTo%></td>		
			<td colspan="2" style="border:none;white-space:normal;"><div id="divAppliedApps">
			    <div class="loading-indicator"><%= Resources.APMWebContent.LoadingLabel%></div></div>

			</td>
          
		</tr>
		<tr id="trViewAllHrefContent"  runat="server">
			<td>&nbsp;</td>
			<td>
				<div id="moreContent" style="display:none;"> <a id="aViewAll"  href="javascript:void(0)" onclick="return ASH.showAll();">&nbsp;</a></div>
			</td>
		</tr>
		<tr>
			<td class="label"><%= Resources.APMWebContent.EditTemplate_Description%></td>
			<td class="apm_TemplateDescription">
				<textarea id="appDescription" class="apm_Resizable edit name " cols="60" rows="3" name="appDescription"></textarea>	
				<apm:ValidationErrorPlaceholder runat="server"/>
			</td>
		</tr>
		<tr>	    
			<td class="label"><%= Resources.APMWebContent.APMWEBDATA_TM0_37%>
				<br/>
				<span style="font-size:7pt;"><%= Resources.APMWebContent.EditTemplate_SeparateTagsWithCommas%></span>
			</td>
			<td>
				<input id="appTags" name="appTags" type="text" class="edit name" size="60"/>
				<apm:ValidationErrorPlaceholder runat="server"/>
			</td>
		</tr>
		<tr id="detailsViewRow">
			<td class="label"><%= Resources.APMWebContent.EditTempalte_CustomApplicationDetailsView%></td>
			<td>	
				<select id="appDetailsView" name="appDetailsView" style="width:500px;"></select>
			</td>
		</tr>

		<apm:NumericEditor ID="ctrFrequency" DisplayText="<%$ Resources: APMWebContent, Edit_PollingFrequency %>" BaseValueName="appFrequency" UnitsLabel="<%$ Resources: APMWebContent, Edit_UnitsLabel_Seconds %>" runat="server"/>
		<apm:NumericEditor ID="ctrTimeout" DisplayText="<%$ Resources: APMWebContent, Edit_PollingTimeout %>" BaseValueName="appTimeout" UnitsLabel="<%$ Resources: APMWebContent, Edit_UnitsLabel_Seconds %>" runat="server"/>

		<tr>
			<td>
				<div style="padding-top: 10px">
					<img src="../../Images/Button.Expand.gif" class="collapsable" data-collapse-target="advancedSettings"/>
					<span class="advancedTitle"><%= Resources.APMWebContent.APMWEBDATA_VB1_100%><br/></span>
				</div>
			</td>
		</tr>

		<tbody id="advancedSettings" style="display: none;">
			<apm:DropdownEditor id="ctrExecutePollingMethod" DisplayText="<%$ Resources: APMWebContent, Edit_PreferredPollingMethod %>" BaseValueName="appExecutePollingMethod" AdditionalLabelCss="indended" runat="server" />
			<apm:BooleanEditor runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_DebugLogging %>" BaseValueName="appDebugLogging" AdditionalLabelCss="indended"
				TrueDisplayValue="<%$ Resources: APMWebContent, Edit_On %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_Off %>"/>
			<apm:NumericEditor runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_NumberofLogFilesToKeep %>" BaseValueName="appLogFiles" MinValue="1" AdditionalLabelCss="indended"
					UnitsLabel="<%$ Resources: APMWebContent, TheNumberOfMostRecentLogFiles %>"/>
			<apm:BooleanEditor runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_PlatformToRunPollingJobOn %>" BaseValueName="appUse64Bit" AdditionalLabelCss="indended"
				TrueDisplayValue="<%$ Resources: APMWebContent, Edit_x64 %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_x86 %>"/>        
		</tbody>
	</table>

    <script type="text/javascript">
        var SHOW_ALL_LIMIT = 10;
        var templateId = <%=Id%>;
        var ASH = {
            init: function() {
                ORION.callWebService("/Orion/APM/Services/Templates.asmx", 
                    "GetAppsCount",
                    { templateId: templateId }, function (result) { 
                                   
                        if (result <= SHOW_ALL_LIMIT) {
                            if (result < 1) {
                                var msgTex = "[]";
                                if (result == 0) msgTex = "<%= Resources.APMWebContent.TemplateIsNotAppliedToAnyApplication%>";
                                $("#divAppliedApps").html(msgTex);
                            } else {
                                GetTemplateApplications(templateId, null);
                            }
                        }
                        else {
                            GetTemplateApplications(templateId, SHOW_ALL_LIMIT);
                            $("#moreContent").show();
                            $("#aViewAll").text(SF("View All({0})", result));
                        }
                    });
            },
            showAll: function() {
                GetTemplateApplications(templateId, null);
                $("#moreContent").hide();
            }
        };
        $(document).ready(ASH.init);
        
        function GetTemplateApplications(templateId, top) {
            ORION.callWebService("/Orion/APM/Services/Templates.asmx", 
                "GetApps", { templateId: templateId, top: top },
                function(resultAsJson) {
                    var source = $('#EditApmTemplate-AppList').html();
                    var listDiv = $("#divAppliedApps").empty();
                    for (var it in resultAsJson) {
                        var app = resultAsJson[it];
                        if (typeof app === 'undefined' || app === null || !app.hasOwnProperty('Name'))
                            break;
                        if (it > 0)
                            listDiv.append(",&nbsp;&nbsp;&nbsp;");
                        app.Name = $("<div/>").text(app.Name).html();
                        listDiv.append(_.template(source, app));
                    }
                    listDiv.append("<br/>");
                }
            );
        }

    </script>
    <script id="EditApmTemplate-AppList" type="text/x-template">
            <img src='{{ SmallApplicationImagePath }}'/>
            <a href='/Orion/APM/ApplicationDetails.aspx?NetObject=AA:{{ ApplicationID }}'>{{ Name }}</a> on
            <img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={{ NodeID }}&status=1'/>
            <a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{ NodeID }}'>{{ NodeName }}</a>
    </script>
</asp:Content>

