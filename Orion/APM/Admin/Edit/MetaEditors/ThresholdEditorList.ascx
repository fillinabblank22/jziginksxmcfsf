﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.APM.Web.UI.SettingEditorControlChild" %>

<%@ Reference VirtualPath="~/Orion/APM/Admin/Edit/ComponentEditors/ComponentEditorBase.master" %>

<%@ Import Namespace="SolarWinds.APM.Web.Plugins" %>

<script runat="server">

    public IEnumerable<string> ThresholdKeys { get; set; }

    protected void BindItems()
    {
        this.thresholdRepeater.DataSource = this.ThresholdKeys ?? this.Master.ThresholdKeys;
        this.thresholdRepeater.DataBind();
    }

    protected Orion_APM_Admin_Edit_ComponentEditors_ComponentEditorBase Master
    {
        get
        {
            for (var master = this.Page.Master; master != null; master = master.Master)
            {
                var master2 = master as Orion_APM_Admin_Edit_ComponentEditors_ComponentEditorBase;
                if (master2 != null)
                {
                    return master2;
                }
            }
            return null;
        }
    }

</script>
<% this.BindItems(); %>
<asp:Repeater runat="server" ID="thresholdRepeater" Visible="<%# this.Master.ShowThresholds %>">
    <ItemTemplate>
        <apm:ThresholdEditor DisplayText="<%# WebPluginManager.Instance.ThresholdKeyToName((string)Container.DataItem) %>" BaseValueName='<%# "cmpThrs" + Container.DataItem %>' ElementId="<%# this.Master.ComponentId %>" EditorMode="<%# this.Master.EditMode %>" runat="server" />
    </ItemTemplate>
</asp:Repeater>
