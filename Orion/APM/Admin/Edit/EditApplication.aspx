﻿<%@ Page Title="New Edit Application" Language="C#" MasterPageFile="~/Orion/APM/Admin/Edit/BaseEditApp.master" AutoEventWireup="true" CodeFile="EditApplication.aspx.cs" Inherits="Orion_APM_Admin_Edit_EditApplication" %>

<%@ Register TagPrefix="apm" TagName="NumericEditor" Src="~/Orion/APM/Admin/Edit/Editors/NumericEditor.ascx" %>
<%@ Register TagPrefix="apm" TagName="BooleanEditor" Src="~/Orion/APM/Admin/Edit/Editors/BooleanEditor.ascx" %>
<%@ Register TagPrefix="apm" TagName="DropdownEditor" Src="~/Orion/APM/Admin/Edit/Editors/DropdownEditor.ascx" %>
<%@ Register TagPrefix="apm" TagName="ValidationErrorPlaceholder" Src="~/Orion/APM/Admin/Edit/Controls/ValidationErrorPlaceholder.ascx" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content4" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionAPMPHConfigSettingsAppsEdit" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="apmCphMainPropertiesTop">
    <asp:PlaceHolder runat="server" ID="placeHolderMainProperties">
        <table id="appMainProp" class="appProperties" cellpadding="0" cellspacing="0">
            <tr>
                <td class="label"><%= Resources.APMWebContent.EditApplication_ApplicationMonitorName%></td>
                <td style="width: 775px;" colspan="2">
                    <input id="appName" name="appName" class="edit name required" size="60" type="text" apmsettctr="true" />
                    <apm:ValidationErrorPlaceholder runat="server" />
                </td>
            </tr>
            <tr>
                <td class="label"><%= Resources.APMWebContent.EditApplication_TemplateName%></td>
                <td style="width: 775px;">
                    <img style='margin-bottom: -5px;' src="../../Images/Admin/icon_application_template.gif" />
                    <span class="templateName editTemplateLink"></span>
                </td>
            </tr>

            <tr id="detailsViewRow">
                <td class="label">
                    <%= Resources.APMWebContent.EditApplication_CustomApplicationDetailsView%></td>
                <td>
                    <span id="detailsView"></span><span class="editTemplate">&#0187 <span class="editTemplateLink"><%= Resources.APMWebContent.EditApplication_ModifyTemplateSettings%></span></span>
                </td>
            </tr>

            <apm:NumericEditor ID="ctrFrequency" DisplayText="<%$ Resources: APMWebContent, Edit_PollingFrequency %>" BaseValueName="appFrequency" UnitsLabel="<%$ Resources: APMWebContent, Edit_UnitsLabel_Seconds %>" runat="server" />
            <apm:NumericEditor ID="ctrTimeout" DisplayText="<%$ Resources: APMWebContent, Edit_PollingTimeout %>" BaseValueName="appTimeout" UnitsLabel="<%$ Resources: APMWebContent, Edit_UnitsLabel_Seconds %>" runat="server" />

            <tr>
                <td>
                    <div style="padding-top: 10px">
                        <img src="../../Images/Button.Expand.gif" class="collapsable" data-collapse-target="advancedSettings" />
                        <span class="advancedTitle"><%= Resources.APMWebContent.APMWEBDATA_VB1_100%><br /></span>
                    </div>
                </td>
            </tr>

            <tbody id="advancedSettings" style="display: none;">
                <apm:DropdownEditor ID="ctrExecutePollingMethod" DisplayText="<%$ Resources: APMWebContent, Edit_PreferredPollingMethod %>" BaseValueName="appExecutePollingMethod" AdditionalLabelCss="indended" IsComplexSetting="true" runat="server" />
                <apm:BooleanEditor runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_DebugLogging %>" BaseValueName="appDebugLogging" AdditionalLabelCss="indended"
                    TrueDisplayValue="<%$ Resources: APMWebContent, Edit_On %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_Off %>" />
                <apm:NumericEditor runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_NumberofLogFilesToKeep %>" BaseValueName="appLogFiles" MinValue="1" AdditionalLabelCss="indended"
                    UnitsLabel="<%$ Resources: APMWebContent, TheNumberOfMostRecentLogFiles %>" />
                <apm:BooleanEditor runat="server" DisplayText="<%$ Resources: APMWebContent, Edit_PlatformToRunPollingJobOn %>" BaseValueName="appUse64Bit" AdditionalLabelCss="indended"
                    TrueDisplayValue="<%$ Resources: APMWebContent, Edit_x64 %>" FalseDisplayValue="<%$ Resources: APMWebContent, Edit_x86 %>" />
            </tbody>
        </table>
        <asp:PlaceHolder runat="server" ID="appCustomEditors" OnInit="appCustomEditor_OnInit"></asp:PlaceHolder>
        <table class="appProperties" cellpadding="0" cellspacing="0">
            <tr>
                <td class="label">
                    <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_25 %>" />
                </td>
                <td style="text-align: right">
                    <span><img src="/Orion/images/edit_16x16.gif" alt="" /><a href="/Orion/Admin/CPE/Default.aspx" class="RenewalsLink" target="_blank"><%= Resources.CoreWebContent.WEBDATA_SO0_4%></a></span>
                </td>
            </tr>
        </table>
        <div id="customProperties" style="width: 100%" />
    </asp:PlaceHolder>
</asp:Content>
