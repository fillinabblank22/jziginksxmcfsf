﻿using System;
using System.Linq;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.Utility;

public partial class Orion_APM_Admin_Edit_EditTemplate : System.Web.UI.Page
{
	private string customAppType;
	protected int Id
	{
		get { return int.Parse(Request.QueryString["id"] ?? "-1"); }
	}

	protected string CustomAppType
	{
		get
		{
			if (customAppType == null)
			{
				var dal = new ApplicationTemplateDAL();
				var template = dal.GetTemplateInfo(Id);
				customAppType = template == null ? string.Empty : template.CustomAppType;
			}
			return customAppType;

		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			ctrFrequency.MinValue = Convert.ToInt32(ApplicationBase.MinPollingFrequencyValue.TotalSeconds);
			ctrTimeout.MinValue = Convert.ToInt32(ApplicationBase.MinPollingTimeoutValue.TotalSeconds);
		}
		if (ctrExecutePollingMethod.Options == null || ctrExecutePollingMethod.Options.Count == 0)
		{
			ctrExecutePollingMethod.Options = DropdownOptionsHelper.OptionsFromEnum<PollingMethod>();
		}

		var scriptProviders = WebPluginManager.Instance.SupportedPlugins.Where(p => p.SupportedCustomApplicationTypes.
				Any(t => string.Equals(t, CustomAppType, StringComparison.InvariantCulture))).OfType<IEditApplicationPlugin>();
		foreach (var scriptProvider in scriptProviders)
		{
			scriptProvider.IncludeScripts(CustomAppType);
		}
	}
}