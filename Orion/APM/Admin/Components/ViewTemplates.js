﻿Ext.namespace('SW');
Ext.namespace('SW.APM');

SW.APM.templates = function () {

    GetGroupByValues = function (groupBy, onSuccess) {
        ORION.callWebService("/Orion/APM/Services/Components.asmx",
                             "GetTemplateValuesAndCountForProperty", { property: groupBy },
                             function (result) {
                                 onSuccess(ORION.objectFromDataTable(result));
                             });
    };

    deleteComponents = function (templateIds, onSuccess) {
        ORION.callWebService("/Orion/APM/Services/Components.asmx",
                             "DeleteTemplates", { templateIds: templateIds },
                             function (result) {
                                 onSuccess();
                             });
    };

    createTemplate = function (templateIds, onSuccess) {
        ORION.callWebService("/Orion/APM/Services/Components.asmx",
                             "CreateApplicationTemplateFromTemplates", { templateIds: templateIds },
                             function (result) {
                                 onSuccess(result);
                             });
    };


    LoadGroupByValues = function () {
        var prop = $("#groupByProperty option:selected").val();
        ORION.Prefs.save('GroupBy', prop);
        if (prop) {
            var groupItems = $(".GroupItems").text("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");

            GetGroupByValues(prop, function (result) {
                groupItems.empty();
                $(result.Rows).each(function () {
                    var value = this.theValue || this.ParentName;
                    var disp = (String.format("{0} ({1})", value, this.theCount) || String.format("@{R=APM.Strings;K=APMWEBJS_VB1_96;E=js}", this.theCount));
                    if (value === null) { disp = String.format("@{R=APM.Strings;K=APMWEBJS_VB1_96;E=js}", this.theCount); }
                    $("<a href='#'></a>").attr('value', value).text(disp).click(SelectGroup).appendTo(".GroupItems").wrap("<li class='GroupItem'/>");
                });

                var toSelect = $(".GroupItem a[value='" + ORION.Prefs.load("GroupByValue") + "']");

                if (toSelect.length === 0) {
                    toSelect = $(".GroupItem a:first");
                }

                toSelect.click();
            });
        } else {
            $(".GroupItems").empty();
            RefreshObjects();
        }
    };

    SelectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");
        grid.store.removeAll();
        RefreshObjects();
        return false;
    };

    RefreshObjects = function () {
        var prop = $("#groupByProperty option:selected").val();
        var value = $(".SelectedGroupItem a").attr('value');
        if (value) {
            ORION.Prefs.save('GroupByValue', value || null);
        }

        grid.store.proxy.conn.jsonData = { property: prop || "", value: value || "" };
        grid.store.load();
    };


    var getSelectedIds = function (items, templateIds) {
        Ext.each(items, function (item) {
            templateIds.push(item.data.UniqueId);
        });
    };

    var deleteSelectedItems = function (items) {
        var templateIds = [];
        getSelectedIds(items, templateIds);

        var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_VB1_95;E=js}");

        deleteComponents(templateIds, function () {
            waitMsg.hide();
            grid.store.reload();
        });
    };

    var createTemplateFromSelectedItems = function (items) {
        var templateIds = [];
        getSelectedIds(items, templateIds);

        createTemplate(templateIds, function (result) {
            window.location = APM.GetTplEditPage(result, ["mode=1"]);
        });
    };

    var startCreateApplicationWizard = function (items) {
        var templateIds = [];
        getSelectedIds(items, templateIds);
        ORION.postToTarget("Assign/EditApplication.aspx", { templateIds: templateIds, ownerPage: "/Orion/APM/Admin/Components/Templates.aspx" });
    };

    editComponent = function (item) {
        window.location = APM.GetTplEditPage(item.data.ParentId, [SF("selected={0}", item.data.UniqueId)]);
    };

    var showAppDialog = function (items, type) {
        var tplIDs = [];
        getSelectedIds(items, tplIDs);

        var config = { "type": type, "cmpIDs": [], "tplIDs": tplIDs, "updateHandler": LoadGroupByValues };

        var dlg = SW.APM.CopyToApplicationDialog.getInstance(config);
        dlg.show();
    };

    UpdateToolbarButtons = function () {
        var count = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;
        var copyToMenuMap = map.CopyToMenu.menu.items.map;

        var needsOnlyOneSelected = (count != 1);
        var needsAtLeastOneSelected = (count === 0);

        map.CreateNewTemplate.setDisabled(needsAtLeastOneSelected);
        map.AssignToNode.setDisabled(needsAtLeastOneSelected);
        map.EditButton.setDisabled(needsOnlyOneSelected);
        map.DeleteButton.setDisabled(needsAtLeastOneSelected);

        copyToMenuMap.CopyToAssignedApplication.setDisabled(needsAtLeastOneSelected);
        copyToMenuMap.CopyToApplicationTemplate.setDisabled(needsAtLeastOneSelected);
    };

    // Column rendering functions
    function renderName(value, meta, record) {
        return String.format('{0} {1}', getStatusIcon(record.data.Status), value);
    }

    function getStatusIcon(status) {
        return String.format('<img src="{0}" />',
               $("#ApmStatusIconPaths input[name=ApmStatus_" + status + "]").val());
    }



    ORION.prefix = "APM_ManageComponentsTmpl_";

    var selectorModel;
    var dataStore;
    var grid;


    return {
        init: function () {

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/APM/Services/Components.asmx/GetTemplatesPaged",
                                [
                                    { name: 'UniqueId', mapping: 0 },
                                    { name: 'Name', mapping: 1 },
                                    { name: 'ParentName', mapping: 2 },
                                    { name: 'ParentId', mapping: 3 },
                                    { name: 'ParentType', mapping: 4 },
                                    { name: 'CmpTypeName', mapping: 5 },
                                    { name: 'Status', mapping: 6 },
                                    { name: 'NodeName', mapping: 7 },
                                    { name: 'NodeStatus', mapping: 8 }
                                ],
                                "Name");

            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [selectorModel, {
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_54;E=js}',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'UniqueId'
                }, {
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_97;E=js}',
                    width: 180,
                    sortable: true,
                    dataIndex: 'Name',
                    renderer: renderName
                }, {
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_125;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'ParentName'
                }, {
                    header: '@{R=APM.Strings;K=APMWEBJS_VB1_98;E=js}',
                    width: 120,
                    sortable: true,
                    dataIndex: 'CmpTypeName'
                }],

                sm: selectorModel,

                viewConfig: {
                    forceFit: true
                },

                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'CreateNewTemplate',
                    text: '@{R=APM.Strings;K=APMWEBJS_VB1_58;E=js}',
                    tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_99;E=js}',
                    icon: '/Orion/APM/images/icon_newtemplate_16x16.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        createTemplateFromSelectedItems(grid.getSelectionModel().getSelections());
                    }
                }, '-', {
                    id: 'AssignToNode',
                    text: '@{R=APM.Strings;K=APMWEBJS_VB1_60;E=js}',
                    tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_100;E=js}',
                    icon: '/Orion/APM/images/icon_newappmon_16x16.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        startCreateApplicationWizard(grid.getSelectionModel().getSelections());
                    }
                }, '-', {
                    id: 'CopyToMenu',
                    text: '@{R=APM.Strings;K=APMWEBJS_VB1_101;E=js}',
                    icon: '/Orion/APM/images/Icon.Copy16x16.gif',
                    cls: 'x-btn-text-icon',
                    menu: [{
                        id: 'CopyToAssignedApplication',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_102;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_103;E=js}',
                        icon: '/Orion/APM/images/Icon.Copy16x16.gif',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            showAppDialog(grid.getSelectionModel().getSelections(), "app");
                        }
                    }, '-', {
                        id: 'CopyToApplicationTemplate',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_104;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_105;E=js}',
                        icon: '/Orion/APM/images/Icon.Copy16x16.gif',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            showAppDialog(grid.getSelectionModel().getSelections(), "tpl");
                        }
                    }]
                }, '-', {
                    id: 'EditButton',
                    text: '@{R=APM.Strings;K=APMWEBJS_VB1_126;E=js}',
                    tooltip: "@{R=APM.Strings;K=APMWEBJS_VB1_127;E=js}",
                    icon: '/Orion/APM/images/Icon.Edit16x16.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        editComponent(grid.getSelectionModel().getSelected());
                    }
                }, '-', {
                    id: 'DeleteButton',
                    text: '@{R=APM.Strings;K=APMWEBJS_TM0_15;E=js}',
                    tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_108;E=js}',
                    icon: '/Orion/Nodes/images/icons/icon_delete.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        Ext.Msg.confirm("@{R=APM.Strings;K=APMWEBJS_VB1_109;E=js}",
                                "@{R=APM.Strings;K=APMWEBJS_VB1_110;E=js}",
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        deleteSelectedItems(grid.getSelectionModel().getSelections());
                                    }
                                });
                    }
                }, '-'
        ],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: 25,
                    displayInfo: true,
                    displayMsg: '@{R=APM.Strings;K=APMWEBJS_VB1_111;E=js}',
                    emptyMsg: "@{R=APM.Strings;K=APMWEBJS_VB1_112;E=js}"
                })

            });

            grid.render('Grid');
            UpdateToolbarButtons();

            var fudgeFactor = 12;
            var groupItemsHeight = $("#Grid").height() - $(".apm_GroupSection").height() - fudgeFactor;
            $(".GroupItems").height(groupItemsHeight);

            $("#groupByProperty").val(ORION.Prefs.load("GroupBy", "")).change(LoadGroupByValues);
            $("#groupByProperty").change();

            $("form").submit(function () { return false; });
        }
    };

} ();

Ext.onReady(SW.APM.templates.init, SW.APM.templates);
