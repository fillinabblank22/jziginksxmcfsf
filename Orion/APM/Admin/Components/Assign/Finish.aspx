﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Components/Assign/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="Finish.aspx.cs" Inherits="Orion_APM_Admin_Components_Assign_Finish" %>
    
<%@ Register Src="~/Orion/APM/Controls/AssignComponentsFinished.ascx" TagPrefix="apm" TagName="AssignComponentsFinished" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <apm:AssignComponentsFinished runat="server" ID="finishedMessage" />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbNext" runat="server" DisplayType="Secondary" OnClick="OnCreateMoreApplications" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_29 %>"/>
        &nbsp; &nbsp;
        <orion:LocalizableButton ID="imgbCancel" runat="server" DisplayType="Primary" OnClick="OnDone" CausesValidation="False" LocalizedText="Done"/>
    </div>    
   
</asp:Content>

