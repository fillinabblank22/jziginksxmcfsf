﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_Components_Assign_SelectNodes : AssignComponentsPageBase
{
   protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
			imgbNext.AddEnterHandler(0);
        }
    }

    protected void OnBack(object sender, EventArgs e)
    {
        GotoPreviousPage();
    }
    
    protected void OnNext(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Workflow.SelectedNodesInfo = SearchableNodeSelection.GetSelectedNodes();
            GotoNextPage();
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected void valAtLeastOneNode_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = SearchableNodeSelection.GetSelectedNodes().Count > 0;
    }
}
