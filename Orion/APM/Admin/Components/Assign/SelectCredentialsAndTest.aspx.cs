﻿using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_Components_Assign_SelectCredentialsAndTest : SolarWinds.APM.Web.UI.AssignComponentsPageBase
{
	#region Event Handlers

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        // We need to recognize wizard entry point (Admin/Components/ViewComponents.js or ViewTemplates.js)
        // to use proper caption for "inherit" option
        bool appMode = Workflow.OwnerPage.ToLowerInvariant().EndsWith("default.aspx");

		ctrCoreCredSetter.SelectedComponentsInfo = Workflow.Components;
		ctrCoreCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;
        ctrCoreCredSetter.ApplicationMode = appMode;

		ctrApmCredSetter.SelectedComponentsInfo = Workflow.Components;
		ctrApmCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;
        ctrApmCredSetter.ApplicationMode = appMode;

		b2.AddEnterHandler(0);
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
		ClearSession();
		GotoPreviousPage();
	}

	protected void OnNext_Click(Object sender, EventArgs e)
	{
		if (ctrCoreCredSetter.IsValid && ctrApmCredSetter.IsValid)
		{
			if (ctrCoreCredSetter.Visible)
			{
				Workflow.UseCredentials(ctrCoreCredSetter.SelectedNodesInfo);
			}
			if (ctrApmCredSetter.Visible)
			{
				Workflow.UseCredentials(ctrApmCredSetter.SelectedNodesInfo);
			}
			Workflow.CreateApplications();

			ClearSession();
			GotoNextPage();
		}
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		ClearSession();
		CancelWizard();
	}

	#endregion

	#region Helper Members

	private void ClearSession()
	{
		ctrCoreCredSetter.ClearSession();
		ctrApmCredSetter.ClearSession();
	}

	#endregion
}