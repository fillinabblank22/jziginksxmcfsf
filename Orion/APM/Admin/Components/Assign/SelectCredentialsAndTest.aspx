﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Components/Assign/AssignWizard.master" AutoEventWireup="true" CodeFile="SelectCredentialsAndTest.aspx.cs" Inherits="Orion_APM_Admin_Components_Assign_SelectCredentialsAndTest" %>

<%@ Register TagPrefix="apm" TagName="CredSetter" Src="~/Orion/APM/Admin/CredentialSetter.ascx" %>
<%@ Register TagPrefix="apm" TagName="CredsTip" Src="~/Orion/APM/Controls/CredentialTips.ascx" %>

<asp:Content ID="c1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
	<table width="100%">
		<tr>
			<td valign="top">
				<apm:CredSetter ID="ctrCoreCredSetter" CredentialOwner="Core" runat="server" />
				<apm:CredSetter ID="ctrApmCredSetter" CredentialOwner="Apm" runat="server" />
			</td>
			<td width="1%" valign="top">
				<apm:CredsTip ID="ctrCredsTip" BackgroundColor="#FFFDCC" BorderColor="#EACA7F" runat="server"/>                
			</td>
		</tr>
	</table>
	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton ID="b1" LocalizedText="Back" DisplayType="Secondary" runat="server" OnClick="OnBack_Click" CausesValidation="false"/>
        <orion:LocalizableButton ID="b2" Text="<%$ Resources : APMWebContent , APMWEBDATA_AK1_70%>" DisplayType="Primary" runat="server" OnClick="OnNext_Click" OnClientClick="if (IsDemoMode()) return DemoModeMessage();"/>
        <orion:LocalizableButton ID="b3" LocalizedText="Cancel" DisplayType="Secondary"  runat="server" OnClick="OnCancel_Click" CausesValidation="false"/>
    </div>
</asp:Content>