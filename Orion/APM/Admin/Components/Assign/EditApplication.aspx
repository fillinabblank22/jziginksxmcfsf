﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Components/Assign/AssignWizard.master"
    AutoEventWireup="true" CodeFile="EditApplication.aspx.cs" Inherits="Orion_APM_Admin_Components_Assign_EditApplication" %>

<%@ Register Src="~/Orion/APM/Admin/EditApplicationBaseSettings.ascx" TagPrefix="apm"
    TagName="EditApplicationBaseSettings" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" runat="Server">
<style type="text/css">
#assignedComponents
{
    padding:5px;
	vertical-align: middle;
}
	
#assignedComponents span
{
    font-weight:bold;
	font-size: 9pt;
}
</style>
    <h2><%= Resources.APMWebContent.APMWEBDATA_VB1_232 %></h2>
    <p><%= Resources.APMWebContent.APMWEBDATA_VB1_233%></p>
    <table id="assignedComponents">
        <asp:Repeater ID="rptComponents" runat="server" OnItemDataBound="rptComponents_ItemDataBound">
            <ItemTemplate>
				<tr>
					<td>
                    <asp:Image ID="imgStatus" runat="server" />
					</td>
					<td>
                    <span><asp:Literal ID="labName" runat="server" /></span>
					</td>
				</tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <p style="padding-top: 5px; padding-bottom: 3px;"><%= Resources.APMWebContent.APMWEBDATA_VB1_234 %></p>
    <table id="appMainProp" cellpadding="0" cellspacing="0" width="100%">
        <tr class="label" style="height: 5px;">
        </tr>
        <tr>
            <td class="label" style="width:255px;">
                <%= Resources.APMWebContent.APMWEBDATA_VB1_235 %>
            </td>
            <td>
                <asp:TextBox ID="textBoxName" CssClass="edit name" runat="server" Columns="60" />
                <asp:RequiredFieldValidator ID="requireApplicationName" runat="server" ControlToValidate="textBoxName"
                    SetFocusOnError="true" Display="Static" ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_236 %>" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <apm:EditApplicationBaseSettings ID="EditApplicationBaseSettings" runat="server" />
            </td>
        </tr>
    </table>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack" CausesValidation="false"/>
        <orion:LocalizableButton ID="imgbNext" runat="server" LocalizedText="Next" DisplayType="Primary" OnClick="OnNext" />
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false"/>
    </div>
</asp:Content>
