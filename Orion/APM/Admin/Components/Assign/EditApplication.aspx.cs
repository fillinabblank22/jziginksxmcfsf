﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using Models = SolarWinds.APM.Common.Models;

public partial class Orion_APM_Admin_Components_Assign_EditApplication : AssignComponentsPageBase
{
	public static readonly String TestSessionKeyAssignCmp = "KeyTR_{0}"
		.FormatInvariant("orion_apm_admin_components_assign_selectcredentialsandtest_aspx".ToLowerInvariant().GetHashCode().ToString(System.Globalization.CultureInfo.InvariantCulture).Replace('-', 'F'));

    private DataTable _components;
    protected DataTable Components
    {
        get
        {
            return _components;
        }
        set
        {
            _components = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
		Session.RemoveThatStartsWith(TestSessionKeyAssignCmp);

        if (!IsPostBack)
        {
            InitApplication();
            InitWorkflow();

            BindData();

			imgbNext.AddEnterHandler(0);
        }  
    }

    private void BindData()
    {
        textBoxName.Text = Workflow.ApplicationToCreate.Name;
        EditApplicationBaseSettings.Update(Workflow.ApplicationToCreate, null);

        rptComponents.DataSource = Components;
        rptComponents.DataBind();
    }

    private void UpdateData()
    {
        //TODO: 1. maybe use getUnique here 2. Discuss the Desc field, move to separate routine then
        Workflow.ApplicationToCreate.Name = textBoxName.Text;
        //Master.UpdateApplicationFromViewState();
        EditApplicationBaseSettings.UpdateFromViewState(Workflow.ApplicationToCreate);
    }

    //TODO: Revise the appropriate host for Application object and initialization logic
    private void InitApplication()
    {
        if (Workflow.ApplicationToCreate == null)
        {
            //TODO: Revise this and following also! Some kind of lightweight ApplicationInfo struct could be used instead
            Models.Application app = new Models.Application();

            app.Name = Resources.APMWebContent.APMWEBCODE_VB1_97;
            app.Frequency = TimeSpan.FromSeconds(300);
            app.Timeout = TimeSpan.FromSeconds(300);

            app.DebugLoggingEnabled = false;
            app.NumberOfLogFilesToKeep = 30;
            app.Use64Bit = false;

            Workflow.ApplicationToCreate = app;
        }
    }

    private void InitWorkflow()
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        List<long> Ids = new List<long>();

        if (Request.Form["ownerPage"] != null)
        {
            Workflow.OwnerPage = new JavaScriptSerializer().Deserialize<string>(
                Server.UrlDecode(Request.Form["ownerPage"])
            );
        }
        string templIdsPostData = Server.UrlDecode(Request.Form["templateIds"]);
        string compIdsPostData = Server.UrlDecode(Request.Form["componentIds"]);
        if (templIdsPostData != null)
        {
            Workflow.Ids = serializer.Deserialize<List<long>>(templIdsPostData);
            Workflow.Mode = AssignComponentsWorkflow.WizardMode.Template;
        }
        else if (compIdsPostData != null)
        {
            Workflow.Ids = serializer.Deserialize<List<long>>(compIdsPostData);
            Workflow.Mode = AssignComponentsWorkflow.WizardMode.Component;
            
        }
        else if (!Workflow.HasStarted) //navigated from wrong page
        {
            Response.Redirect("~/Orion/Apm/Admin/Components/Default.aspx");
        }

        Workflow.HasStarted = true;

        Components = Workflow.GetSelectedComponentsTable();
    }

    protected void rptComponents_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView componentInfo = (DataRowView)item.DataItem;
            ApmStatus stat = new ApmStatus((Status)componentInfo["Status"]); 
            Image imgStatus = (Image)item.FindControl("imgStatus");
            imgStatus.ImageUrl = stat.ToString("smallimgpath", null);

            Literal labName = (Literal)item.FindControl("labName");
            labName.Text = componentInfo["Name"] as string;
        }
    }

    #region Wizard buttons navi

    protected void OnBack(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected void OnNext(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            UpdateData();

            GotoNextPage();
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    #endregion

}
