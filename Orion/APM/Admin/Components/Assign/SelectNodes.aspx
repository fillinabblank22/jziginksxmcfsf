﻿<%@ Page Title="Select Nodes" Language="C#" MasterPageFile="~/Orion/APM/Admin/Components/Assign/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="SelectNodes.aspx.cs" Inherits="Orion_APM_Admin_Components_Assign_SelectNodes" %>

<%@ Register Src="../../../Controls/SearchableNodeSelection.ascx" TagPrefix="apm" TagName="SearchableNodeSelection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <h2><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_237, "<b>", "</b>", Workflow.ApplicationToCreate.Name)%></h2>
    <p><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_238, Workflow.ApplicationToCreate.Name)%></p>

    <br />
    <apm:SearchableNodeSelection ID="SearchableNodeSelection" runat="server" />
    <br />
    <asp:CustomValidator ID="valAtLeastOneNode" runat="server" 
        ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_AK1_95 %>" OnServerValidate="valAtLeastOneNode_ServerValidate"/>
    <br />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack" CausesValidation="false" />
        <orion:LocalizableButton ID="imgbNext" runat="server" LocalizedText="Next" DisplayType="Primary" OnClick="OnNext"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false"/>
    </div>   
</asp:Content>

