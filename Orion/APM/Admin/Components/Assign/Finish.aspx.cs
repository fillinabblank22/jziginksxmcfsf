﻿using System;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_Components_Assign_Finish : AssignComponentsPageBase
{
	protected void Page_Load(object sender, EventArgs e)
	{
		finishedMessage.DataSource = Workflow.CreatedApplications;
		finishedMessage.DataBind();

		if (!IsPostBack)
		{
			imgbCancel.AddEnterHandler(0);
		}
	}

	protected void OnCreateMoreApplications(object sender, EventArgs e)
	{
		string ownerPageURL = Workflow.OwnerPage;
		ResetSession();
		Response.Redirect(ownerPageURL);
	}

	protected void OnDone(object sender, EventArgs e)
	{
		ResetSession();
		Response.Redirect("/Orion/Apm/Summary.aspx", true);
	}

}
