﻿<%@ Page Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_217%>" Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" 
    AutoEventWireup="true" CodeFile="Templates.aspx.cs" Inherits="Orion_APM_Admin_Components_Templates" %>

<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="apm" TagName="IncludeExtJs" %>

<asp:Content ID="c1" ContentPlaceHolderID="hPh" Runat="Server">

    <link rel="stylesheet" type="text/css" href="../ExistingElements.css" />

    <apm:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>

    <orion:Include runat="server" File="js/OrionCore.js"/>
    <orion:Include runat="server" Module="APM" File="Admin/Components/ViewTemplates.js"/>
    <orion:Include runat="server" Module="APM" File="Admin/Js/CopyToEntityDialog.js"/>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
	<a href="/Orion/APM/Admin/ApplicationTemplates.aspx" style="padding: 2px 0px 2px 25px; background: transparent url(/Orion/APM/Images/Admin/icon_application_template.gif) scroll no-repeat left center;"><%= Resources.APMWebContent.APMWEBDATA_VB1_220 %></a>
    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHConfigSettingsManageComponentMonitorsWithinTemplates" ID="HelpButton" runat="server" />
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="cPh" Runat="Server">

	<%foreach (string setting in new string[] { "GroupBy", "GroupByValue", "CopyCMPGroupBy", "CopyTPLGroupBy"}) { %>
		<input type="hidden" name="APM_ManageComponentsTmpl_<%=setting%>" id="APM_ManageComponentsTmpl_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("APM_ManageComponentsTmpl_" + setting)%>' />
	<%}%>

	<table width="100%" id="breadcrumb">
		<tr>
			<td>
				<h1><%=Page.Title%></h1>
			</td>
		</tr>
	</table>

	<table class="ExistingElements" style="width:800px;">
		<tr>
			<td colspan="2" style="font-size: 11px; padding-bottom: 10px;">
			<%= Resources.APMWebContent.APMWEBDATA_VB1_218 %>
			</td>
		</tr>
		<tr valign="top" align="left">

			<td style="border-right: #cccccc 1px solid; border-left: 0; border-top: 0; border-bottom: 0;">
				<div class="ElementGrouping">
					<div class="apm_GroupSection">
						<div style="width: 180px"><%= Resources.APMWebContent.APMWEBDATA_TM0_9%></div>
						<select id="groupByProperty" style="width:100%">
							<option value=""><%= Resources.APMWebContent.APMWEBDATA_TM0_10 %></option>
							<option value="cd.Name"><%= Resources.APMWebContent.APMWEBDATA_VB1_215 %></option>
							<option value="t.ParentName"><%= Resources.APMWebContent.APMWEBDATA_VB1_219 %></option>
						</select>
					</div>
					<ul class="GroupItems"></ul>
				</div>

			</td>
			<td>
                <div id="Grid"/>
			</td>
		</tr>
	</table>

	<div id="ApmStatusIconPaths">
	    <%foreach (ApmStatus status in ApmStatus.GetValues()) { %>
		    <input type="hidden" name="ApmStatus_<%=(int)status.Value%>" value='<%=status.ToString("smallimgpath", null)%>' />
        <%}%>
        
        <input type="hidden" name="ApmStatus_-1" value='/Orion/APM/Images/StatusIcons/Components/small-template.gif' />
        
    </div>      


	<div id="originalQuery"></div>	
	<div id="test"></div>
	<pre id="stackTrace"></pre>

</asp:Content>

