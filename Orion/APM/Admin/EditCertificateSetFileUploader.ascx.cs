﻿using System;
using System.Web.UI;

public partial class Orion_APM_Admin_EditCertificateSetFileUploader : UserControl
{
    public string FileContentElementId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        fileUploader.Attributes["onchange"] = string.Format("return ajaxFileUpload(this, '{0}')", FileContentElementId);
    }
}