using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common.Licensing;

public partial class Orion_APM_Admin_LicenseSummary : Page
{
    public LicensingSummary summary;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = InvariantString.Format(Resources.APMWebContent.APMWEBCODE_AK1_20, ApmConstants.ModuleShortName);

        if (!IsPostBack)
        {
            LoadSummary();
        }
    }

    private void LoadSummary()
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            summary = businessLayer.GetLicenseSummary();
        }
    }
}
