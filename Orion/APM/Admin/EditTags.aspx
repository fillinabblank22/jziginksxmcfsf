﻿<%@ Page Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_63 %>" Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" 
    AutoEventWireup="true" CodeFile="EditTags.aspx.cs" Inherits="Orion_APM_Admin_EditTags" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks">
        <orion:IconHelpButton HelpUrlFragment="OrionAPMPHTags" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="hPh" Runat="Server">
    <style type="text/css" media="screen">
        table.tagBody input{border:solid 1px #98AFC7;}
        table.tagBody tr td{font-size:9pt;} 
        table.tagBody div.content{margin:0px 0px 0px 20px;padding:5px;width:680px;background-color:#e4f1f8;}
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="cPh" Runat="Server">
	<table class="tagBody">
		<tbody>
			<tr>
				<td>
					<h2>					    
                        <asp:Label ID="labelTemplateNames" runat="server"/>
					</h2>
		        </td>
		    </tr>
			<tr>
				<td>
					<asp:UpdatePanel ID="upTags" UpdateMode="Conditional" runat="server">
						<ContentTemplate>
							<table>
								<tr>
									<td>
										<asp:RadioButton ID="radioButtonExistingTags" GroupName="TagRB" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_52 %>" Checked="true" OnCheckedChanged="OnRadioButtonTags_CheckedChanged" AutoPostBack="true" runat="server" />
									</td>
								</tr>
								<tr>
									<td id="contentExistingTags" runat="server">
										<div class="content">
											<asp:Repeater ID="repeaterExistingTags" runat="server">
												<HeaderTemplate>
													<table cellspacing="0" cellpadding="0">
												</HeaderTemplate>
												<ItemTemplate>
													<tr>
														<td style="width:30px;">
															<asp:CheckBox ID="tagSelected" runat="server"/>
															<input id="tagName" type="hidden" value='<%# Eval("Name")%>' runat="server" />
														</td>
														<td align="left"><%# HttpUtility.HtmlEncode(Eval("Name")) %></td>
													</tr>
												</ItemTemplate> 
												<FooterTemplate>
													</table>
												</FooterTemplate>
											</asp:Repeater>
										</div>
									</td>
								</tr>
								<tr>
									<td>
		        						<asp:RadioButton ID="radioButtonAddTags" GroupName="TagRB" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_53 %>" OnCheckedChanged="OnRadioButtonTags_CheckedChanged" AutoPostBack="true" runat="server" />
									</td>
								</tr>
								<tr>
									<td id="contentAddTags" runat="server">
										<div class="content">
											<table cellspacing="0" cellpadding="0">
												<tbody>
													<tr>
														<td>
															<asp:TextBox ID="textBoxAddTags" Width="500" runat="server"/>
														</td>
													</tr>
													<tr>
														<td style="font-size:7pt;"><%= Resources.APMWebContent.APMWEBDATA_AK1_54 %></td>
													</tr>
													<tr id="tagValidateMessage" visible="false" runat="server">
														<td style="color:Red;"><%= Resources.APMWebContent.APMWEBDATA_AK1_55 %></td>
													</tr>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td>
		        						<asp:RadioButton ID="radioButtonRemoveTags" GroupName="TagRB" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_56 %>" OnCheckedChanged="OnRadioButtonTags_CheckedChanged" AutoPostBack="true" runat="server" />
									</td>
								</tr>
								<tr>
									<td id="contentRemoveTags" runat="server">
										<div class="content">
											<asp:Repeater ID="repeaterRemoveTags" runat="server">
												<HeaderTemplate>
													<table cellspacing="0" cellpadding="0">
												</HeaderTemplate>
												<ItemTemplate>
													<tr>
														<td style="width:30px;">
															<asp:CheckBox ID="tagSelected" runat="server"/>
															<input id="tagName" type="hidden" value='<%# Eval("Name")%>' runat="server" />
														</td>
														<td align="left"><%# HttpUtility.HtmlEncode(Eval("Name")) %></td>
													</tr>
												</ItemTemplate> 
												<FooterTemplate>
													</table>
												</FooterTemplate>
											</asp:Repeater>
											<span id="contentRemoveTagsNA" visible="false" runat="server"><%= Resources.APMWebContent.APMWEBDATA_AK1_57 %></span>
										</div>
									</td>
								</tr>
							</table>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
		</tbody>
		<tfoot>
		    <tr>
		        <td>
		            <br/><br/>
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton runat="server" ID="buttonSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="OnSubmitButton_Click" CausesValidation="true"/>
		                <orion:LocalizableButton runat="server" ID="buttonCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancelButton_Click" CausesValidation="false"/>
                    </div>
		        </td>
		    </tr>
		</tfoot>
	</table>
</asp:Content>

