﻿using System;
using System.Collections.Generic;
using System.Text;

public partial class Orion_APM_Admin_CheckDuplicates : System.Web.UI.UserControl
{
	#region Fields

	protected String _json = String.Empty;

	protected static Boolean _allDuplicates = false;

	#endregion 

	#region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
	{
		if (this.IsPostBack)
		{
			this.updateData();
		}
		else 
		{
			_allDuplicates = this.isAllDuplicates();
		}
		this.initData();
	}

	#endregion

	#region Properties

	public Boolean SkipDuplicates
	{
		get;
		set;
	}
	
	public IList<Int32> NodeIDs
	{
		get;
		set;
	}

	public IList<Int32> TemplateIDs
	{
		get;
		set;
	}

	#endregion
	
	#region Helper Members

	private void initData() 
	{
		if (
			this.NodeIDs != null && this.NodeIDs.Count > 0 &&
			this.TemplateIDs != null && this.TemplateIDs.Count > 0
		) 
		{
			this._json = new StringBuilder()
				.Append("SW.APM.Duplicates.data = {")
				.AppendFormat("nodes:[{0}],", this.toCSV(this.NodeIDs))
				.AppendFormat("templates:[{0}],", this.toCSV(this.TemplateIDs))
				.AppendFormat("skip:{0},", this.SkipDuplicates.ToString().ToLower())
				.AppendFormat("allDuplicates:{0}", _allDuplicates.ToString().ToLower())
				.Append("};").ToString();
		}
	}
	
	private void updateData()
	{
		this.SkipDuplicates = string.IsNullOrEmpty(Request.Form["skipDuplicates"]) || bool.Parse(Request.Form["skipDuplicates"]);
	}

	private Boolean isAllDuplicates()
	{
		String swql = String.Format(@"
			SELECT DISTINCT a.ApplicationTemplateID AS TemplateID, a.NodeID
			FROM Orion.APM.Application as a
			WHERE 
				a.ApplicationTemplateID IN ({0}) AND
				a.NodeID IN ({1}) 
			", this.toCSV(this.TemplateIDs), this.toCSV(this.NodeIDs)
		);

		System.Data.DataTable res = SolarWinds.APM.Web.SwisProxy.ExecuteQuery(swql);
		if (res.Rows.Count < 1)
		{
			return false;
		}
		for (Int32 i = 0; i < this.NodeIDs.Count; i++)
		{
			for (Int32 j = 0; j < this.TemplateIDs.Count; j++)
			{
				if (res.Select(String.Format("TemplateID = {0} AND NodeID = {1}", this.TemplateIDs[j], this.NodeIDs[i])).Length < 1)
				{
					return false;
				}
			}
		}
		return true;
	}

	private String toCSV(IList<Int32> items) 
	{
		if (items == null || items.Count < 1) 
		{
			return String.Empty;
		}
		return String.Join(",", (items as List<Int32>).ConvertAll<String>(delegate(Int32 item) { return item.ToString(); }).ToArray());
	}

	#endregion
}
