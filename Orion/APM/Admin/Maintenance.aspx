﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true" 
    CodeFile="Maintenance.aspx.cs" Inherits="Orion_APM_Admin_Maintenance" Title="<%$ Resources: APMWebContent, APMWEBDATA_TM0_28 %>" %>
<%@ Register Src="~/Orion/APM/Controls/IconHelpButton.ascx" TagPrefix="apm" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionAPMAGDatabaseManagerCreatingDatabaseMaintenancePlan" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
	<orion:Include runat="server" File="APM/APM.css"/>
    <orion:Include runat="server" File="APM/APM-ORION-FIX.css"/>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<table id="breadcrumb">
		<tr>
            <td ><image src="/Orion/APM/Images/Admin/warning_32x32.gif"></image></td>
			<td style="border-bottom-width: 0px;">
				<h1 style="padding-bottom:5px;"><%=Page.Title%></h1>
                <span style="font-size:small;"><%= Resources.APMWebContent.APMWEBDATA_TM0_26%></span>
                <asp:HyperLink ID="MoreInfoHyperLink" runat="server" Text="<%$ Resources: APMWebContent, APMWEBDATA_TM0_27 %>" Target="_blank"></asp:HyperLink>
			</td>
		</tr>
	</table>

	<asp:ValidationSummary ID="ValidationSummary1" runat="server" />

    <div>
         <div style="font-size:small; padding-left: 43px;">
         <br />
          <%= Resources.APMWebContent.APMWEBDATA_TM0_25%>
         </div>
         <div style=" padding-left: 20px;">
            <div id="listOfTables" display="block" runat="server" />
         </div>
     </div>
</asp:Content>

