/*jslint browser: true, unparam: true, white: true, plusplus: true, sub: true*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.templates', function (module, APMjs) {
    'use strict';

    var ORION = APMjs.assertGlobal('ORION'),
        Ext = APMjs.assertExt(),
        $ = APMjs.assertQuery(),
        isDemoMode = APMjs.assertGlobal('IsDemoMode'),
        demoModeMessage = APMjs.assertGlobal('DemoModeMessage'),
        currentSearchTerm,
        selectorModel,
        dataStore,
        grid,
        pageSizeNum,
        topToolbarHandlers,
        columnRenderers;

    function getGroupByValues(groupBy, onSuccess) {
        ORION.callWebService(
            '/Orion/APM/Services/Templates.asmx',
            'GetValuesAndCountForProperty',
            { property: groupBy },
            function (result) {
                onSuccess(ORION.objectFromDataTable(result));
            }
        );
    }

    function deleteTemplates(ids, onSuccess) {
        ORION.callWebService(
            '/Orion/APM/Services/Templates.asmx',
            'DeleteTemplates',
            { templateIds: ids },
            function (result) {
                onSuccess();
            }
        );
    }

    function copyTemplates(ids, onSuccess) {
        ORION.callWebService(
            '/Orion/APM/Services/Templates.asmx',
            'CopyTemplates',
            { templateIds: ids },
            function (result) {
                onSuccess();
            }
        );
    }

    function uploadTemplates(ids, onSuccess) {
        ORION.callWebService(
            '/Orion/APM/Services/ThwackAPM.asmx',
            'UploadTemplates',
            { Ids: ids },
            function (result) {
                onSuccess(result);
            }
        );
    }

    function getSelectedTemplateIds(items) {
        var ids = [];
        Ext.each(items, function (item) {
            ids.push(item.data.TemplateID);
        });
        return ids;
    }

    function uploadSelectedTemplates(items) {
        var selected, waitMsg;
        selected = getSelectedTemplateIds(items);
        waitMsg = Ext.Msg.wait('@{R=APM.Strings;K=APMWEBJS_VB1_43;E=js}');
        uploadTemplates(selected, function (result) {
            var obj, dlg;
            function onClose() {
                dlg.hide();
                dlg.destroy();
                dlg = null;
            }
            waitMsg.hide();
            obj = Ext.decode(result);
            if (!obj.isSuccess) {
                obj.msg = String.format('<span style="color:#f00;">{0}</span>', obj.msg);
            }
            dlg = new Ext.Window({
                title: '@{R=APM.Strings;K=APMWEBJS_VB1_44;E=js}', html: obj.msg,
                width: 350, autoHeight: true, border: false, region: 'center', layout: 'fit', modal: true, resizable: false, plain: true, bodyStyle: 'padding:5px 0px 0px 10px;',
                buttons: [{ text: '@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}', handler: onClose }]
            });
            dlg.show();
        });
    }

    function refreshObjects(property, value, search, callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { property: property, value: value || '', search: search };
        currentSearchTerm = search;
        grid.store.load({ callback: callback });
    }

    function selectGroup() {
        $(this).parent().addClass('SelectedGroupItem').siblings().removeClass('SelectedGroupItem');
        var value = $('.SelectedGroupItem a').attr('value');
        ORION.Prefs.save('GroupByValue', value);
        refreshObjects('tags', value, '');
        return false;
    }

    function loadGroupByValues() {
        var groupItems;
        groupItems = $('.GroupItems').text('@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}');
        getGroupByValues('tags', function (result) {
            var toSelect;
            groupItems.empty();
            $(result.Rows).each(function () {
                var value, text, disp;
                value = this.theValue;
                text = $.trim(String(this.theValue));
                if (text.length > 22) {
                    text = String.format('{0}...', text.substr(0, 22));
                }
                disp = (text || '@{R=APM.Strings;K=APMWEBJS_VB1_46;E=js}') + ' (' + this.theCount + ')';
                if (value === null) {
                    disp = String.format('@{R=APM.Strings;K=APMWEBJS_VB1_47;E=js}', this.theCount);
                }
                $('<a href="#"></a>')
					.attr('value', value)
                    .attr('title', value)
					.text(disp)
                    .click(selectGroup)
                    .appendTo('.GroupItems')
                    .wrap('<li class="GroupItem"/>');
            });
            if ($('#search').val()) {
                $('#doSearch').click();
            } else {
                toSelect = $('.GroupItem a[value="' + ORION.Prefs.load('GroupByValue') + '"]');
                if (toSelect.length === 0) {
                    toSelect = $('.GroupItem a:first');
                }
                toSelect.click();
            }
        });
    }

    function deleteSelectedItems(items) {
        var toDelete, waitMsg;
        toDelete = getSelectedTemplateIds(items);
        waitMsg = Ext.Msg.wait('@{R=APM.Strings;K=APMWEBJS_VB1_48;E=js}');
        deleteTemplates(toDelete, function () {
            waitMsg.hide();
            grid.store.reload();
        });
    }

    function editTemplate(item) {
        var APM = APMjs.assertGlobal('APM');
        window.location = APM.GetTplEditPage(item.data.TemplateID);
    }

    function copySelectedTemplates(items) {
        var selectedIds, waitMsg;
        selectedIds = getSelectedTemplateIds(items);
        waitMsg = Ext.Msg.wait('@{R=APM.Strings;K=APMWEBJS_VB1_49;E=js}');
        copyTemplates(selectedIds, function () {
            waitMsg.hide();
            grid.store.reload();
        });
    }

    function exportSelectedTemplates(items) {
        var selectedIds = getSelectedTemplateIds(items);
        ORION.postToTarget('ExportTemplate.aspx', { postData: selectedIds });
    }

    function editTags(items) {
        var selectedIds = getSelectedTemplateIds(items);
        ORION.postToTarget('EditTags.aspx', { postData: selectedIds });
    }

    function isRecordReadOnly(record) {
        var devmode = APMjs.isApmDevMode();
        var customType = record.get('CustomApplicationType');		
		
        return  !devmode && !!customType;        
    }

    function updateToolbarButtons() {
        var count, map, nothingSelected, isReadOnlySelected, ieMap;
        count = grid.getSelectionModel().getCount();
        map = grid.getTopToolbar().items.map;
        nothingSelected = (count === 0);
        isReadOnlySelected = false;
        if (!nothingSelected) {
            grid.getSelectionModel().each(function (record) {
                isReadOnlySelected = isReadOnlySelected || isRecordReadOnly(record);
            });
        }
        map.AssignToNode.setDisabled(nothingSelected);
        map.AssignToGroupMenu.setDisabled(nothingSelected);
        map.EditButton.setDisabled(nothingSelected || (count > 1));
        map.EditTagsButton.setDisabled(nothingSelected || isReadOnlySelected);
        map.CopyButton.setDisabled(nothingSelected || isReadOnlySelected);
        map.DeleteButton.setDisabled(nothingSelected || isReadOnlySelected);
        ieMap = map.ImportExportMenu.menu.items.map;
        ieMap.UploadButton.setDisabled(nothingSelected || isReadOnlySelected);
        ieMap.ExportButton.setDisabled(nothingSelected || isReadOnlySelected);
    }

    function jumpToTag(tagName) {
        $('.GroupItem a[value="' + tagName + '"]').click();
    }

    function refreshGrid() {
        grid.store.reload();
    }

    ORION.prefix = 'APM_Templates_';

    function demoMode(action) {
        if (isDemoMode()) {
            APMjs.demoModeAction("ManageTemplates");
            return true;
        }
        if (APMjs.isFn(action)) {
            action();
        }
        return false;
    }

    function isAppInsight(element) {
        return APMjs.isStr(element.data.CustomApplicationType) && element.data.CustomApplicationType !== "";
    }

    topToolbarHandlers = {
        'CreateNewTemplate': function () {
			demoMode(function () {
				window.location = '/Orion/APM/Admin/CreateNewTemplate.aspx';
			});
        },
        'AssignToNode': function () {
            demoMode(function () {
                var selectedIds = getSelectedTemplateIds(grid.getSelectionModel().getSelections());
                ORION.postToTarget('Templates/Assign/SelectNodes.aspx', { postData: selectedIds });
            });
        },
        'AssignToGroup': function () {
            demoMode(function () {
                var selectedItems = grid.getSelectionModel().getSelections();
                var areAppInsightsSelected = selectedItems.some(isAppInsight);
                if (areAppInsightsSelected) {
                    Ext.Msg.alert("", "@{R=APM.Strings;K=ManageTemplates_AddToGroup_AppInsightSelected;E=js}");
                } else {
                    var selectedIds = getSelectedTemplateIds(selectedItems);
                    ORION.postToTarget('Groups/SelectGroups.aspx', { postData: selectedIds });
                }
            });
        },
        'UnassignFromGroup': function () {
            demoMode(function () {
                var selectedIds = getSelectedTemplateIds(grid.getSelectionModel().getSelections());
                var removeAssignmentsDlg = new SW.APM.TemplateGroupAssignment.RemoveAssignmentsDialog(selectedIds, refreshGrid);
                removeAssignmentsDlg.show();
            });
        },
        'ApplicationDiscovery': function () {
            window.location = '/Orion/APM/Admin/AutoAssign/SelectNodes.aspx';
        },
        'EditButton': function () {
            editTemplate(grid.getSelectionModel().getSelected());
        },
        'EditTagsButton': function () {
            demoMode(function () {
                editTags(grid.getSelectionModel().getSelections());
            });
        },
        'CopyButton': function () {
            demoMode(function () {
                copySelectedTemplates(grid.getSelectionModel().getSelections());
            });
        },
        'ImportButton': function () {
            demoMode(function () {
                window.location = '/Orion/APM/Admin/ImportApplicationTemplate.aspx';
            });
        },
        'UploadButton': function () {
            demoMode(function () {
                var apm = APMjs.assertGlobal('SW.APM');
                apm.logInToThwack(uploadSelectedTemplates, grid.getSelectionModel().getSelections(), false);
            });
        },
        'ExportButton': function () {
            demoMode(function () {
                exportSelectedTemplates(grid.getSelectionModel().getSelections());
            });
        },
        'AssignDetailsViews': function () {
            demoMode(function () {
                window.location = '/Orion/APM/Admin/ViewsByApplicationType.aspx';
            });
        },
        'DeleteButton': function () {
            demoMode(function () {
                Ext.Msg.confirm('@{R=APM.Strings;K=APMWEBJS_TM0_17;E=js}',
                    '@{R=APM.Strings;K=APMWEBJS_TM0_18;E=js}',
                    function (btn, text) {
                        if (btn === 'yes') {
                            deleteSelectedItems(grid.getSelectionModel().getSelections());
                        }
                    });
            });
        }
    };

    // Column renderers
    columnRenderers = {
        'RenderName': function (value, meta, record) {
            var img = 'icon_application_template.gif';
            if (isAppInsight(record)) {
                img = 'smart_application_monitor_icon.png';
            }
            var encodedValue = _.escape(value);
            return String.format('<img src="/Orion/APM/images/Admin/{1}" />&nbsp;<span class="searchable">{0}</span>', encodedValue, img);
        },
        'RenderCustomView': function (value, meta, record) {
            return value;
        },
        'RenderAssignedNodes': function (value, meta, record) {
            var nodeCount = record.data.NodeCount;
            var groupCount = record.data.GroupCount;
            var nodesInGroupCount = record.data.NodesInGroupCount;

            var nodeString, groupString, nodesInGroupString, nodeGroupCombineString;
            nodeString = groupString = nodesInGroupString = nodeGroupCombineString = "";

            if (nodeCount !== 0) {
                nodeString = String.format((nodeCount > 1) ?
                    "@{R=APM.Strings;K=APMWEBJS_ViewTemplates_NodeCountPlural;E=js}" :
                    "@{R=APM.Strings;K=APMWEBJS_ViewTemplates_NodeCount;E=js}",
                    nodeCount);
            }

            if (groupCount !== 0) {
                groupString = String.format((groupCount > 1) ?
                    "@{R=APM.Strings;K=APMWEBJS_ViewTemplates_GroupCountPlural;E=js}" :
                    "@{R=APM.Strings;K=APMWEBJS_ViewTemplates_GroupCount;E=js}",
                     groupCount);
            }
            nodesInGroupString = (nodesInGroupCount > 0) ? String.format(" ({0})", nodesInGroupCount) : "";

            nodeGroupCombineString = String.format("{0}{1}{2}", nodeString, (nodeString !== "" && groupString !== "") ?
                "@{R=APM.Strings;K=APMWEBJS_ViewTemplates_Comma;E=js}" :
                "",
                groupString);

            var message = nodeGroupCombineString.concat(nodesInGroupString);

            if (message === "") {
                return '@{R=APM.Strings;K=APMWEBJS_VB1_50;E=js}';
            }

            return String.format('{0}{1}{2}',
                String.format('<a href="#" class="assignNodes" value="{0}">', record.data.TemplateID),
                message,
                '</a>');
        },
        'RenderTags': function (value, meta, record) {
            var tags, links, i;

            try {
                tags = Ext.decode(value);
            } catch (ignore) { }

            tags = tags || [];

            if (tags.length === 0) {
                return '@{R=APM.Strings;K=APMWEBJS_VB1_51;E=js}';
            }

            links = [];
            for (i = 0; i < tags.length; ++i) {
                var encodedTag = _.escape(tags[i]);
                links.push(String.format('<a href="#" class="tagLink">{0}</a>', encodedTag));
            }

            return links.join(', ');
        }
    };

    // public accessible methods

    function pubInit() {
        pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '20'), 10);

        $('#Grid').click(function (e) {
            if ($(e.target).hasClass('assignNodes')) {
                var templateId = $(e.target).attr('value');
                var index = dataStore.find('TemplateID', templateId);
                var templateName = (index !== -1) ? dataStore.getAt(index).data.Name : '';

                var templateAssignmentsDlg = new SW.APM.TemplateGroupAssignment.TemplateAssignmentsDialog(templateId, templateName, refreshGrid);
                templateAssignmentsDlg.show();

                return false;
            }
            if ($(e.target).hasClass('tagLink')) {
                jumpToTag($(e.target).text());
                return false;
            }
        });

        selectorModel = new Ext.grid.CheckboxSelectionModel();
        selectorModel.on('selectionchange', updateToolbarButtons);

        dataStore = new ORION.WebServiceStore(
            '/Orion/APM/Services/Templates.asmx/GetTemplatesPaged',
            [
                { name: 'TemplateID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'GroupCount', mapping: 2 },
                { name: 'NodeCount', mapping: 3 },
                { name: 'NodesInGroupCount', mapping: 4 },
                { name: 'Tags', mapping: 5 },
                { name: 'CreatedDate', mapping: 6 },
                { name: 'LastModifiedDate', mapping: 7 },
                { name: 'CustomView', mapping: 8 },
                { name: 'ComponentsCount', mapping: 10 },
                { name: 'CustomApplicationType', mapping: 11 }
            ],
            'Name');

        grid = new Ext.grid.GridPanel({

            store: dataStore,

            columns: [selectorModel, {
                header: '@{R=APM.Strings;K=APMWEBJS_VB1_54;E=js}',
                width: 80,
                hidden: true,
                hideable: false,
                sortable: true,
                dataIndex: 'TemplateID'
            }, {
                header: '@{R=APM.Strings;K=APMWEBJS_VB1_55;E=js}',
                width: 280,
                sortable: true,
                dataIndex: 'Name',
                renderer: function (value, meta, record) {
                    return columnRenderers['RenderName'](value, meta, record);
                }
            }, {
                header: '@{R=APM.Strings;K=APMWEBJS_VB1_56;E=js}',
                width: 140,
                sortable: false,
                dataIndex: 'AssignedNodes',
                renderer: function (value, meta, record) {
                    return columnRenderers['RenderAssignedNodes'](value, meta, record);
                }
            }, {
                header: '@{R=APM.Strings;K=APMWEBJS_RM0_1;E=js}',
                width: 180,
                sortable: true,
                dataIndex: 'ComponentsCount',
                align: 'center'
            }, {
                header: '@{R=APM.Strings;K=APMWEBJS_TM0_27;E=js}',
                width: 90,
                sortable: false,
                dataIndex: 'CustomView',
                renderer: function (value, meta, record) {
                    return columnRenderers['RenderCustomView'](value, meta, record);
                }
            }, {
                header: '@{R=APM.Strings;K=APMWEBJS_VB1_57;E=js}',
                width: 280,
                sortable: false,
                dataIndex: 'Tags',
                renderer: function (value, meta, record) {
                    return columnRenderers['RenderTags'](value, meta, record);
                }
            }, {
                header: '@{R=APM.Strings;K=APMWEBJS_TM0_28;E=js}',
                width: 150,
                sortable: true,
                dataIndex: 'CreatedDate'
            }, {
                header: '@{R=APM.Strings;K=APMWEBJS_TM0_29;E=js}',
                width: 150,
                sortable: true,
                dataIndex: 'LastModifiedDate'
            }
            ],

            sm: selectorModel,

            viewConfig: {
                deferEmptyText: true,
                emptyText: ' '
            },

            layout: 'fit',
            autoScroll: 'true',

            width: 750,
            height: 500,
            stripeRows: true,

            tbar: new Ext.Toolbar({
                enableOverflow: true,
                items: [
                    {
                        id: 'CreateNewTemplate',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_58;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_59;E=js}',
                        icon: '/Orion/APM/images/icon_newtemplate_16x16.gif',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['CreateNewTemplate'](item);
                        }
                    }, '-', {
                        id: 'AssignToNode',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_60;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_61;E=js}',
                        icon: '/Orion/APM/Images/assign_to_node_icon16x16.png',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['AssignToNode'](item);
                        }
                    }, '-', {
                        id: 'AssignToGroupMenu',
                        text: '@{R=APM.Strings;K=APMWEBJS_AssignToGroup;E=js}',
                        icon: '/Orion/APM/Images/assign_to_group_icon16x16.png',
                        cls: 'x-btn-text-icon',
                        menu: [
                            {
                                id: 'AssignToGroupMenuItem',
                                text: '@{R=APM.Strings;K=APMWEBJS_AssignToGroup;E=js}',
                                tooltip: '@{R=APM.Strings;K=APMWEBJS_AssignToGroupTooltip;E=js}',
                                cls: 'x-btn-text-icon',
                                handler: function (item) {
                                    topToolbarHandlers['AssignToGroup'](item);
                                }
                            }, {
                                id: 'UnassignFromGroupMenuItem',
                                text: '@{R=APM.Strings;K=APMWEBJS_UnassignFromGroup;E=js}',
                                tooltip: '@{R=APM.Strings;K=APMWEBJS_UnassignFromGroupTooltip;E=js}',
                                cls: 'x-btn-text-icon',
                                handler: function (item) {
                                    topToolbarHandlers['UnassignFromGroup'](item);
                                }
                            }
                        ]
                    }, '-', {
                        id: 'ApplicationDiscovery',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_62;E=js}',
                        icon: '/Orion/APM/Images/icon_scan_apps.gif',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['ApplicationDiscovery'](item);
                        }
                    }, '-', {
                        id: 'EditButton',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_63;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_TM0_10;E=js}',
                        icon: '/Orion/APM/images/Icon.Edit16x16.gif',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['EditButton'](item);
                        }
                    }, '-', {
                        id: 'EditTagsButton',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_57;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_64;E=js}',
                        icon: '/Orion/APM/images/Admin/icon_tag.gif',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['EditTagsButton'](item);
                        }
                    }, '-', {
                        id: 'CopyButton',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_65;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_66;E=js}',
                        icon: '/Orion/APM/images/Icon.Copy16x16.gif',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['CopyButton'](item);
                        }
                    }, '-', {
                        id: 'ImportExportMenu',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_67;E=js}',
                        cls: 'x-btn-text-icon',
                        icon: '/Orion/APM/Images/Icon.ImportExport16x16.gif',
                        menu: [
                            {
                                id: 'ImportButton',
                                text: '@{R=APM.Strings;K=APMWEBJS_VB1_68;E=js}',
                                tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_69;E=js}',
                                icon: '/Orion/APM/images/Icon.Import16x16.gif',
                                cls: 'x-btn-text-icon',
                                handler: function (item) {
                                    topToolbarHandlers['ImportButton'](item);
                                }
                            }, {
                                id: 'UploadButton',
                                text: '@{R=APM.Strings;K=APMWEBJS_VB1_70;E=js}',
                                tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_71;E=js}',
                                icon: '/Orion/APM/images/icon_thwack.gif',
                                cls: 'x-btn-text-icon',
                                handler: function (item) {
                                    topToolbarHandlers['UploadButton'](item);
                                }
                            }, {
                                id: 'ExportButton',
                                text: '@{R=APM.Strings;K=APMWEBJS_VB1_72;E=js}',
                                tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_73;E=js}',
                                icon: '/Orion/APM/images/Icon.Export16x16.gif',
                                cls: 'x-btn-text-icon',
                                handler: function (item) {
                                    topToolbarHandlers['ExportButton'](item);
                                }
                            }
                        ]
                    }, '-', {
                        id: 'AssignDetailsViews',
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_74;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_75;E=js}',
                        icon: '/Orion/APM/images/Icon.AssignViews16x16.gif',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['AssignDetailsViews'](item);
                        }
                    }, '-', {
                        id: 'DeleteButton',
                        text: '@{R=APM.Strings;K=APMWEBJS_TM0_15;E=js}',
                        tooltip: '@{R=APM.Strings;K=APMWEBJS_TM0_16;E=js}',
                        icon: '/Orion/Nodes/images/icons/icon_delete.gif',
                        cls: 'x-btn-text-icon',
                        handler: function (item) {
                            topToolbarHandlers['DeleteButton'](item);
                        }
                    }
                ]
            }),

            bbar: new Ext.PagingToolbar({
                store: dataStore,
                pageSize: pageSizeNum,
                displayInfo: true,
                displayMsg: '@{R=APM.Strings;K=APMWEBJS_VB1_76;E=js}',
                emptyMsg: '@{R=APM.Strings;K=APMWEBJS_VB1_77;E=js}',
                items: [
                            new Ext.form.Label({ text: '@{R=APM.Strings;K=APMWEBJS_TM0_21;E=js}', style: 'margin-left: 5px; margin-right: 5px' }),
                            new Ext.form.ComboBox({
                                regex: /^\d*$/,
                                store: new Ext.data.SimpleStore({
                                    fields: ['pageSize'],
                                    data: [[10], [20], [30], [40], [50], [100]]
                                }),
                                displayField: 'pageSize',
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus: true,
                                width: 50,
                                editable: false,
                                value: pageSizeNum,
                                listeners: {
                                    'select': function (c, record) {
                                        grid.bottomToolbar.pageSize = record.get('pageSize');
                                        grid.bottomToolbar.cursor = 0;
                                        ORION.Prefs.save('PageSize', grid.bottomToolbar.pageSize);
                                        grid.bottomToolbar.doRefresh();
                                    }
                                }
                            })
                ]
            })
        });

        module.onPreRender();
        grid.render('Grid');
        updateToolbarButtons();

        var fudgeFactor, groupItemsHeight;

        // Set the height of the Group Items list
        fudgeFactor = 27;
        //a = $('#Grid').height();
        //b = $('.apm_GroupSection').height();
        //c = $('#apm_ThwackAd').outerHeight();
        groupItemsHeight = $('#Grid').height() - $('.apm_GroupSection').height() - $('#apm_ThwackAd').outerHeight() - fudgeFactor;
        $('.GroupItems').height(groupItemsHeight);

        // Set thhe width of the grid
        grid.setWidth($('#gridCell').width());

        loadGroupByValues();

        $('form').submit(function () { return false; });

        $('#search').keyup(function (e) {
            if (e.keyCode === 13) {
                $('#doSearch').click();
            }
        });

        $('#doSearch').click(function () {
            var search = $('#search').val();
            if ($.trim(search).length === 0) {
                return;
            }
            $('.GroupItem').removeClass('SelectedGroupItem');
            refreshObjects('', '', search);
        });

        // highlight the search term (if we have one)
        grid.getView().on('refresh', function () {
            var search, columnsToHighlight, apm = APMjs.assertGlobal('SW.APM');
            search = currentSearchTerm;
            if ($.trim(search).length === 0) {
                return;
            }
            columnsToHighlight = [2, 5];
            Ext.each(columnsToHighlight, function (columnNumber) {
                apm.highlightSearchText('#Grid .x-grid3-body .x-grid3-td-' + columnNumber, search);
            });
        });

        // Set the grid's empty text based on the data returned from the server.
        dataStore.on('load', function (store, records, options) {
            var t = dataStore.reader.jsonData.d.EmptyText;
            $('#Grid div.x-grid-empty').text(t);
        });
    }

    function hasConditionAndAction(obj) {
        return APMjs.isObj(obj) && APMjs.isFn(obj.Condition) && APMjs.isFn(obj.Action);
    }

    function pubRegisterHandler(buttonId, handler) {
        if (hasConditionAndAction(handler)) {
            var prevHandler = topToolbarHandlers[buttonId];
            topToolbarHandlers[buttonId] = function (item) {
                if (handler.Condition(item.ownerCt.ownerCt.selModel.selections.items)) {
                    handler.Action(item.ownerCt.ownerCt.selModel.selections.items);
                } else {
                    prevHandler(item);
                }
            };
        }
    }

    function pubRegisterRenderer(columnId, renderer) {
        if (hasConditionAndAction(renderer)) {
            var prevRenderer = columnRenderers[columnId];
            columnRenderers[columnId] = function (value, meta, record) {
                if (renderer.Condition(value, meta, record)) {
                    return renderer.Action(value, meta, record);
                }
                return prevRenderer(value, meta, record);
            };
        }
    }

    function customTypeCondition(customType) {
        return function generatedCustomTypeCondition(selectedItems) {
            if (!APMjs.isStr(customType)) {
                throw '[customType] has to be a string';
            }
            if (isDemoMode()) {
                return false;
            }
            return APMjs.linqAny(selectedItems, function (item) {
                return item.data.CustomApplicationType === customType;
            });
        };
    }

    function singleItemAction(path) {
        return function generatedSingleItemAction(selectedItems) {
            if (selectedItems.length > 1) {
                Ext.Msg.show({
                    msg: '@{R=APM.Strings;K=ApmWeb_JEL_SingleSmartAppCanBeAssignedErrorMessage;E=js}',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING,
                    title: '@{R=APM.Strings;K=APMWEBJS_VB1_61;E=js}'
                });
            } else {
                ORION.postToTarget(path, { postData: selectedItems[0].data.TemplateID });
            }
        };
    }

    function pubRegisterHandlerAssignWizard(customType, path) {
        var handler = {
            Condition: customTypeCondition(customType),
            Action: singleItemAction(path)
        };
        pubRegisterHandler('AssignToNode', handler);
    }

    function pubRegisterCustomTypeRenderers(customType, renderers) {
        var condition = function (value, meta, record) {
            return record.data.CustomApplicationType === customType;
        };
        APMjs.linqEach(renderers, function (action, name) {
            var renderer = {
                Condition: condition,
                Action: action
            };
            pubRegisterRenderer(name, renderer);
        });
    }

    // return module object
    module.registerHandler = pubRegisterHandler;
    module.registerRenderer = pubRegisterRenderer;
    // adds additional processing of data before rendering
    module.onPreRender = APMjs.noop;
    module.registerHandlerAssignWizard = pubRegisterHandlerAssignWizard;
    module.registerCustomTypeRenderers = pubRegisterCustomTypeRenderers;

    Ext.onReady(pubInit, module);
});
