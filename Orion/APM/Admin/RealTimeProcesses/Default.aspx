﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_APM_Admin_RealTimeProcesses_Test" MasterPageFile="~/Orion/OrionMinReqs.master" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="apm" TagName="IncludeExtJs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js"/>
    <orion:Include runat="server" Module="APM" File="CredentialsDialog.js"/>
    <orion:Include runat="server" Module="APM" File="Processes.js"/>

    <script type="text/javascript">
        SW.APM.processMonitor.SetNodeInfo(<%=GetNodeParams()%>);
        SW.APM.processMonitor.SetFeatureInfo(<%=GetFeatureParams()%>);
        SW.APM.processMonitor.SetSort(<%=GetSortParams()%>);
    </script>

    <orion:Include runat="server" File="APM/APM.css"/>    
    <orion:Include runat="server" File="APM/Admin/RealTimeProcesses/Processes.css"/> 
    <link rel="shortcut icon" href="/Orion/APM/Images/RT_process_explorer.png" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
    <form id="form1" runat="server">
    <input id="helplink1" runat="server" class="helplinkurl" type="hidden"/>
    <asp:ScriptManager runat="server" ID="scman">
        <Services>
            <asp:ServiceReference Path="~/Orion/APM/Services/Processes.asmx" />
        </Services>
    </asp:ScriptManager>
    </form>
</asp:Content>