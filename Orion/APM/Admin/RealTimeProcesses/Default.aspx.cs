﻿using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using System;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_Admin_RealTimeProcesses_Test : System.Web.UI.Page
{
	private string nodename = null;
	private string nodesubtype = null;
	private bool isRemoteForCredentialDialog = true;
	private bool isDemo = false;
	private string nodeIcon = null;
	private string nodeVendor = null;
	private int? credId = null;
	private string credName = null;

	protected void Page_Load(object sender, EventArgs e)
	{
		Uri uri = new Uri(Request.Url.AbsoluteUri, UriKind.Absolute);
		isRemoteForCredentialDialog = (uri.Scheme.Equals("http") && !Request.IsLocal);
		isDemo = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
		helplink1.Value = HelpLocator.CreateHelpUrl("OrionAPMPHWhyisthistakingsolong");
		Page.Title = String.Format(Resources.APMWebContent.APMWEBDATA_AK1_145, GetNodeName());
        ApmBaseResource.EnsureApmScripts();
	}

	public string GetSortParams()
	{
		string compId = string.IsNullOrEmpty(Request.QueryStringOrForm("cid")) ? "0" : Request.QueryStringOrForm("cid");
		if (string.IsNullOrEmpty(Request.QueryStringOrForm("sort")))
			return InvariantString.Format("'CPU', 'DESC', {0}", compId);
		else return InvariantString.Format("'{0}', 'DESC', {1}", Request.QueryStringOrForm("sort"), compId);
	}

	public string GetNodeParams()
	{
		if (nodename == null)
		{
			LoadNodeInfo();
		}

	    bool allowChangeCredentials = (!nodesubtype.Equals("SNMP")
	                                   || nodeVendor.StartsWith("windows", StringComparison.InvariantCultureIgnoreCase))
	                                  && !nodesubtype.Equals("Agent", StringComparison.InvariantCultureIgnoreCase);

		return InvariantString.Format("{0}, {1}, {2}, {3}, {4}, {5}",
									  GetNodeId(), JsHelper.Serialize(nodename), JsHelper.Serialize(nodesubtype), JsHelper.Serialize(nodeIcon),
									  allowChangeCredentials.ToString().ToLowerInvariant(),
									  allowChangeCredentials && credId.HasValue
										  ? string.Format("{0}, {1}", credId.Value, JsHelper.Serialize(credName))
										  : "null, null");
	}

	public string GetFeatureParams()
	{
		return InvariantString.Format("{0}, {1}, {2}, {3}",
									  isRemoteForCredentialDialog.ToString().ToLowerInvariant(),
									  isDemo.ToString().ToLowerInvariant(),
									  (ApmRoleAccessor.AllowRealTimeProcessExplorer && !isDemo).ToString().ToLowerInvariant(),
									  ApmRoleAccessor.AllowRealTimeProcessExplorerFullControl.ToString().ToLowerInvariant());
	}

	public int GetNodeId()
	{
		int id = 0;
		int.TryParse(Request.QueryString["NodeId"], out id);
		return id;
	}

	public string GetNodeName()
	{
		if (nodename == null)
		{
			LoadNodeInfo();
		}
		return nodename;
	}

	private void LoadNodeInfo()
	{
		int nodeId = GetNodeId();
		String swql = String.Format(@"SELECT n.NodeName, n.ObjectSubtype, n.Vendor, n.StatusIcon from Orion.Nodes as n
                WHERE n.NodeID = {0}", nodeId);

		System.Data.DataTable res = SolarWinds.APM.Web.SwisProxy.ExecuteQuery(swql);
		if (res.Rows.Count < 1)
		{
			throw new ArgumentException(
				InvariantString.Format("Node with id {0} does not exist or you do not have permission to view this node based on your account limitations. Please contact your administrator if you need access.", nodeId));
		}
		else
		{
			nodename = res.Rows[0][0].ToString();
			nodesubtype = res.Rows[0][1].ToString();
			nodeVendor = res.Rows[0][2].ToString();
			nodeIcon = res.Rows[0][3].ToString();

			//Demo Server needs to show list of processes also for ICMP nodes
			if (isDemo)
			{
				if (nodesubtype == "ICMP")
				{
					nodesubtype = "SNMP"; //we change node sub type to 'SNMP' to prevent showing of credential dialog
				}
			}

			if (nodesubtype != "WMI")
			{
				using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
				{
					CredentialSet cs = bl.GetWorkingWindowsCredentialForNode(nodeId);
					if (cs != null)
					{
						credId = cs.Id;
						credName = cs.Name;
					}
				}
			}
		}
	}
}