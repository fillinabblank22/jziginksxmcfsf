Ext.namespace('SW');
Ext.namespace('SW.APM');

SW.APM.processMonitor = function () {

    var nodeId = 0;
    var nodeName = 'unknown';
    var nodeSubType = 'unknown';
    var nodeStatusIcon = 'Unknown.gif';
    var highlightCompId = 0;
    var MAX_PROCESSES = 9999;

    var sortColumn = 'CPU';
    var sortOrder = 'DESC';
    var rowLimit = 10;

    var apmCredId = null;
    var apmCredName = null;

    var _isRemote = true;
    var _allowStartMonitoring = false;
    var _allowEndProcess = false;
    var _allowChangeCredentials = true;
    var _isDemo = false;

    var cacheKey = null;
    var timer = null;
    var timerEnabled = true;

    var trimNumberTail = 0;
    var showAllProcesses = false;
    var totalProcessCount = 0;

    var selectedPIDs = new Array(); // used to store selected processes between refreshing
    var killedPIDs = new Array(); // used to store killed processes comming in the result
    var locker = false; // used for stopping of refreshing during processes killing
    var selectionStopRefresh = false; // indicate if refreshing was running when a process was selected
    var essentialProcesses = new Array('lsass.exe', 'csrss.exe', 'smss.exe', 'winlogon.exe', 'svchost.exe', 'services.exe', 'System');

    var loaderMask;

    function StartStop() {
        var button = Ext.getCmp('StartStopButton');
        if (timerEnabled) {
            timerEnabled = false;
            clearTimeout(timer);
            button.setText('@{R=APM.Strings;K=APMWEBJS_AK1_56;E=js}');
            button.setIcon('/Orion/APM/Images/ProcessMonitor/icon_play.png');
            ShowRealTimePollingPaused();
        }
        else {
            timerEnabled = true;
            button.setText('@{R=APM.Strings;K=APMWEBJS_AK1_25;E=js}');
            button.setIcon('/Orion/APM/Images/StatusIcons/ServiceStatus_Paused_16x16.gif');
            Refresh();
            ShowRealTimePollingInProgress();
            Ext.getCmp('EndProcessButton').setDisabled(true);
            selectionStopRefresh = false;
            selectorModel.clearSelections();
        }
    }

    function StopPolling() {
        var button = Ext.getCmp('StartStopButton');
        if (timerEnabled) {
            timerEnabled = false;
            clearTimeout(timer);
        }
        button.setText('@{R=APM.Strings;K=APMWEBJS_AK1_56;E=js}');
        button.setIcon('/Orion/APM/Images/ProcessMonitor/icon_play.png');
        ClearGrid();
        ShowRealTimePollingStopped();
    }

    function Close() {
        window.close();
    }

    function Refresh() {
        clearTimeout(timer);

        if (nodeSubType == 'ICMP' && (apmCredId == null || apmCredId == 0)) {
			//apmCredID = 0 - NoCredentials
            timerEnabled = false;
            StopAndShowError('@{R=APM.Strings;K=APMWEBJS_AK1_53;E=js}',
            { yes: '@{R=APM.Strings;K=APMWEBJS_AK1_54;E=js}', cancel: '@{R=APM.Strings;K=APMWEBJS_AK1_41;E=js}' });
            return;
        }

        var rowLimitTextBox = Ext.getCmp('RowLimitTextBox');
        if (!showAllProcesses) {
            if (rowLimitTextBox.isValid())
                rowLimit = rowLimitTextBox.getValue();
            else
                rowLimit = 0;
        }
        else
            rowLimit = MAX_PROCESSES;

        if (locker == false) {
            ProcessMonitor.Processes.GetSystemInfo(nodeId, apmCredId, cacheKey, sortColumn, sortOrder, rowLimit, OnDataReceived, OnError);
        }
    }

    function Show() {
        var button = Ext.getCmp('ShowButton');
        var rowLimitTextBox = Ext.getCmp('RowLimitTextBox');
        var rowLimitSpacer = Ext.getCmp('RowLimitSpacer');
        showAllProcesses = !showAllProcesses;
        if (showAllProcesses) {
            rowLimitTextBox.hide();
            rowLimitSpacer.hide();
            button.setText("@{R=APM.Strings;K=APMWEBJS_AK1_55;E=js}");
        }
        else {
            rowLimitTextBox.setValue(10);
            rowLimitTextBox.show();
            rowLimitSpacer.show();
            button.setText("@{R=APM.Strings;K=APMWEBJS_AK1_45;E=js}");
        }
        UpdateProcessCountLabel();
        Refresh();
    }

    function checkIfApplicationAlreadyAdded(currentApps, newAppId) {
        for (var i = 0; i < currentApps.length; i++) {
            if(currentApps[i][2] == newAppId) {
                return true;
            }
        }
        return false;
    }
    
    function OnDataReceived(response) {
        if (locker == false) {
            totalProcessCount = response.TotalProcessCount;
            UpdateProcessCountLabel();
            cacheKey = response.CacheKey;
            if (nodeStatusIcon != response.NodeStatusIcon) {
                nodeStatusIcon = response.NodeStatusIcon;
                ShowRealTimePollingInProgress;
            }
            switch (response.Status) {
                case 1: // Initialized
                    break;
                case 2: // Polling      
                    var data = [];
                    var procs = response.SystemState.Processes;
                    var apps = response.AssignedApplications;
                    for (var i = 0; i < procs.Rows.length; i++) {
                        var PID = procs.Rows[i][0];

                        var procApps = [];
                        procApps.highLight = false;
                        for (var j = 0; j < apps.Rows.length; j++) {
                            if (apps.Rows[j][0] == PID) {
                                if (checkIfApplicationAlreadyAdded(procApps, apps.Rows[j][2]) == false) {
                                    procApps.push(apps.Rows[j]);
                                    if (apps.Rows[j][1] == highlightCompId) {
                                        procApps.highLight = true;
                                    }
                               }
                            }
                        }

                        var name = procs.Rows[i][1];
                        var commandLine = procs.Rows[i][7];
                        var path = procs.Rows[i][8];
                        var parameters = procs.Rows[i][9];

                        //build command line value for snmp polling
                        if (commandLine == null) {
                            commandLine = '';

                            if (path != null && path != '')
                                commandLine += path;

                            //for windows add process name at the end path
                            if (path != null && (path != '' || parameters != '') && path.search(new RegExp(name + "$")) == -1)
                                commandLine += name;

                            if (commandLine.length > 0)
                                commandLine += ' ';

                            if (parameters != null && parameters != '')
                                commandLine += parameters;
                        }

                        //do not fill killed processes (just for first OnDataReceived event after killing process)
                        if (killedPIDs.indexOf(PID) == -1) {
                            data.push([PID,
                                        name,
                                        procs.Rows[i][2], // CPU
                                        procs.Rows[i][3], // PhysicalMemory
                                        procs.Rows[i][4], // VirtualMemory
                                        procs.Rows[i][5], // DiskIO
                                        procs.Rows[i][6], // UserName
                                        commandLine,
                                        path,
                                        parameters,
                                        procApps
                                        ]);
                        }
                    }
                    //clear killedPIDs array
                    killedPIDs = new Array();

                    //hack to prevent re-sorting on client side    
                   if (!(nodeSubType == 'SNMP' && apmCredId == null && sortColumn == 'CommandLine')) {
                       grid.getStore().remoteSort = true;
                    } 
                    grid.getStore().loadData(data);
                    //restore back to prevent store using proxy (which is not defined)
                    grid.getStore().remoteSort = false;

                    //hide select/deselect all checkbox
                    $('div.x-grid3-header div.x-grid3-hd-checker').css('background-image', 'none');

                    loaderMask.hide();

                    if (timerEnabled)
                        ShowRealTimePollingInProgress();

                    break;
                case 3: // Error
                    StopAndShowError(response.ErrorMessage);
                    return;
            }

            if (timerEnabled)
                timer = setTimeout(Refresh, 5000);
        }
       
        setRowWidth();
    }
    
    function setRowWidth() {
        if (Ext.isChrome) {
            if ($(window).width() == 900 && $(window).height() == 360) { window.resizeTo(930, 360); }

            var headers = $(".x-grid3-hd"), width = 0, rows = $(".x-grid3-row"), columnsCount = 0;
            if (rows.length > 0) {
                for (var z = 0; z < headers.length; z++) {
                    if (headers[z].attributes[1].value.indexOf("display: none") < 0) {
                        columnsCount++;
                        width = width + headers[z].clientWidth;
                      }
                }
                columnsCount = columnsCount - 1;
                width = width + columnsCount * 2;
                for (var index = 0; index < rows.length; index++) {
                    $(rows[index]).removeAttr('style').attr('style', 'width:' + width + 'px');
                }
            }
        }
    }
    
    function OnKillDataReceived(response) {
        var procs = response.Processes;            
        var errorMessage = response.ErrorMessage;
        killedPIDs = new Array();

        if (procs != null) {
            for (var i = 0; i < procs.Rows.length; i++) {
                var PID = procs.Rows[i][0];
                var killedExitCode = procs.Rows[i][1];

                if (killedExitCode == 0)
                    killedPIDs.push(PID);
            }
        }
        
        var allItems = grid.getStore();
        var selectedItems = selectorModel.getSelections();

        //deselect items
        selectorModel.clearSelections();

        //remove killed processes from grid items collection
        for (i = 0; i < selectedItems.length; i++) {
            if (killedPIDs.indexOf(selectedItems[i].data.PID) > -1) {
                allItems.remove(selectedItems[i]);
            }
        }

        //unlock refreshing
        locker = false;

        if (errorMessage.length == 0) {
            Refresh();
        }
        else {
            //display error message
            var message = '@{R=APM.Strings;K=APMWEBJS_PS0_12;E=js}' + ' ' + response.ErrorMessage;
            Ext.Msg.show({ title: '@{R=APM.Strings;K=APMWEBJS_PS0_6;E=js}', msg: message, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR,
                fn: function (dr) {
                    Refresh();
                }
            });
        }
    }

    function OnError(error, context) {
        HideRealTimePollingProgress();

        var message = error._message;
        if (error.get_statusCode() == 401 || error.get_statusCode() == 403) {
            message = '@{R=APM.Strings;K=APMWEBJS_AK1_52;E=js}';
            if (window.opener && !window.opener.closed) {
                window.opener.location.reload();
            }
        }

        StopAndShowError(message);
    }

    function UpdateStatusMessage(message) {
        var progress = Ext.getCmp('PollingInRealtime');
        progress.update(message);
    }

    function ShowRealTimePollingInProgress() {
        var statusMessage = '<img style="vertical-align:bottom" src="/Orion/APM/Images/loading_gen_16x16.gif" height="16" width="16">&nbsp;';
        var node = String.format('<img style="vertical-align:bottom;margin-right:3px" src="/Orion/Images/StatusIcons/Small-{0}" height="16" width="16"/>{1}', nodeStatusIcon, nodeName);
        if (apmCredName == null) {
            statusMessage += String.format('@{R=APM.Strings;K=APMWEBJS_AK1_43;E=js}', node, nodeSubType);
        }
        else {
            statusMessage += String.format('@{R=APM.Strings;K=APMWEBJS_AK1_44;E=js}', node, apmCredName);
        }
        UpdateStatusMessage(statusMessage);
    }

    function ShowRealTimePollingPaused() {
        var statusMessage = String.format('@{R=APM.Strings;K=APMWEBJS_AK1_49;E=js}', String.format('<img style="vertical-align:bottom;margin-right:3px" src="/Orion/Images/StatusIcons/Small-{0}"/>{1}', nodeStatusIcon, nodeName));
        UpdateStatusMessage(statusMessage);
    }

    function ShowRealTimePollingStopped() {
        var statusMessage = String.format('@{R=APM.Strings;K=APMWEBJS_AK1_50;E=js}', String.format('<img style="vertical-align:bottom;margin-right:3px" src="/Orion/Images/StatusIcons/Small-{0}"/>{1}', nodeStatusIcon, nodeName));
        UpdateStatusMessage(statusMessage);
    }

    function HideRealTimePollingProgress() {
        UpdateStatusMessage('');
    }

    function UpdateProcessCountLabel() {
        var lab = Ext.getCmp('TotalProcessesLabel');
        if (showAllProcesses) {
            lab.setText(String.format('@{R=APM.Strings;K=APMWEBJS_AK1_47;E=js}', totalProcessCount));
        }
        else {
            lab.setText(String.format('@{R=APM.Strings;K=APMWEBJS_AK1_48;E=js}', totalProcessCount));
        }
    }

    function StopAndShowError(errorMessage, butt) {
        if (timerEnabled) StartStop();
        if (loaderMask) loaderMask.hide();

        if (!butt) {
            if (_allowChangeCredentials & !_isDemo) {
                butt = { ok: '@{R=APM.Strings;K=APMWEBJS_AK1_40;E=js}', yes: '@{R=APM.Strings;K=APMWEBJS_AK1_42;E=js}', cancel: '@{R=APM.Strings;K=APMWEBJS_AK1_41;E=js}' };
            }
            else {
                butt = { ok: '@{R=APM.Strings;K=APMWEBJS_AK1_40;E=js}', cancel: '@{R=APM.Strings;K=APMWEBJS_AK1_41;E=js}' };
            }
        }

        Ext.Msg.show({
            title: '@{R=APM.Strings;K=APMWEBJS_PS0_10;E=js}',
            msg: errorMessage,
            buttons: butt,
            icon: Ext.MessageBox.ERROR,
            fn: function (dr) {
                if (dr == 'ok') Retry();
                else if (dr == 'yes') RetryDifferentCredentials();
                else Close();
            }
        });
    }

    function PrepareLoadMask() {
        var messagePrefix = '<span>';
        var node = String.format('<img src="/Orion/Images/StatusIcons/Small-{0}"/> {1}', nodeStatusIcon, nodeName);
        if (apmCredName == null) {
            messagePrefix += String.format('@{R=APM.Strings;K=APMWEBJS_AK1_65;E=js}', node, nodeSubType);
        }
        else {
            messagePrefix += String.format('@{R=APM.Strings;K=APMWEBJS_AK1_66;E=js}', node, apmCredName);
        }
        var loaderMaskMessage = '{0}<br/>{1}<br/><a href="#" id="CancelPolling" onclick="SW.APM.processMonitor.CancelPolling();" class="sw-btn" style="float:right"><span class="sw-btn-c"><span class="sw-btn-t">@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}</span></span></a><br/><br/>';
        if (nodeSubType == "SNMP" && _allowChangeCredentials && apmCredId == null)
            loaderMaskMessage = String.format(loaderMaskMessage, messagePrefix, String.format('<br/><span class="helpLink">&#187;&nbsp;</span><a class="helpLink" href="{0}" target="help">@{R=APM.Strings;K=APMWEBJS_AK1_38;E=js}</a></span>', $("input.helplinkurl").val()));
        else
            loaderMaskMessage = String.format(loaderMaskMessage, messagePrefix, '');
        loaderMask = new Ext.LoadMask(Ext.getBody(), { msg: loaderMaskMessage });
    }

    function Retry() {
        if (loaderMask) loaderMask.show();
        cacheKey = null;
        StartStop();
        HideRealTimePollingProgress();
    }

    function RetryDifferentCredentials() {
        SelectCredentials();
    }

    function OnGridSortChange(grid, sortInfo) {
        sortColumn = sortInfo.field;
        sortOrder = sortInfo.direction;
        Refresh();
    }

    // Column rendering functions
    function numToLocaleString(number) {
        var result = Math.round(number).toLocaleString();
        if (trimNumberTail > 0) result = result.substr(0, result.length - trimNumberTail);
        return result;
    }

    function renderCpuGauge(value, meta, record) {
        if (value == null) return '<table><tr><td style="width: 60px">@{R=APM.Strings;K=APMWEBJS_AK1_37;E=js}</td><td><img src="/Orion/APM/Images/loading_gen_16x16.gif"/></td></tr></table>';
        var valRound = Math.round(Math.min(100, value));
        var type = 'Neutral'; // (valRound > 50) ? 'Error' : 'Normal';
        var result = '<table><tr>';
        result += String.format('<td style="width: 30px">{0}%</td>', valRound);
        result += String.format('<td class="apm_InlineBar {1}" style="width: 80px"><h3><div style="width: {0}%; height: 10px; font-size: 7px;">&nbsp;</div></h3></td>', valRound, type);
        result += '</tr></table>';
        return result;
    }

    function renderUserName(value, meta, record) {
        if (value == '') return '@{R=APM.Strings;K=APMWEBJS_AK1_37;E=js}&nbsp;<img src="/Orion/APM/Images/loading_gen_16x16.gif" height="16" width="16"/>';
        if (Ext.isChrome) {
            return '<span style= "padding-left:10px">' + value + '</span>';
        }
        return value;
    }
    
    function renderCommandLine(value, meta, record) {
        if (Ext.isChrome) {
            return '<span style= "padding-left:10px">' + value + '</span>';
        }
        return value;
    }

    function renderBytesToKilo(value, meta, record) {
        if (value == null) return '';
        return String.format('@{R=APM.Strings;K=APMWEBJS_AK1_36;E=js}', numToLocaleString(value / 1024));
    }

    function renderBytesToKiloPerSec(value, meta, record) {
        if (value == null) return '@{R=APM.Strings;K=APMWEBJS_AK1_37;E=js}&nbsp;<img src="/Orion/APM/Images/loading_gen_16x16.gif" height="16" width="16"/>';
        return String.format('@{R=APM.Strings;K=APMWEBJS_AK1_35;E=js}', numToLocaleString(value / 1024));
    }

    function renderAssignedApp(value, meta, record) {
        if (value.length == 0) {
            if (_allowStartMonitoring) {
                return '<a href="#" id="StartMonitoring" onclick="SW.APM.processMonitor.StartMonitoring(\'' + record.data.Name + '\');"><img class="vertical-alignment" src="/Orion/APM/Images/icon_add.gif" height="16" width="16"/> @{R=APM.Strings;K=APMWEBJS_AK1_34;E=js}</a>';
            }
            else {
                return '';
            }
        }

        var apps = '';
        for (var i = 0; i < value.length; i++) {
            if (i > 0) apps += '<br/>';
            apps += String.format('<a href="/Orion/APM/ApplicationDetails.aspx?NetObject=AA:{0}" id="ApplicationDetails" target="_blank">', value[i][2]);
            apps += String.format('<img src="/Orion/APM/Images/StatusIcons/Small-App-{0}.gif" height="17" width="24"/> {1}</a>', AppStatusToImgSuffix(value[i][4]), value[i][3]);
        }
        return apps;
    }

    function AppStatusToImgSuffix(status) {
        switch (status) {
            case 0: return 'Unknown';
            case 1: return 'Up';
            case 2: return 'Down';
            case 5: return 'Up-Warn';
            case 6: return 'Up-Critical';
            case 7: return 'Disabled';
            case 8: return 'Unmanaged';
            default: return 'Blank';
        }
    }
            
    function startMultiplePrecessesMonitoring () {
        if (_isDemo) {
            DemoModeAction("RTPE_Start_monitoring");
            return;
        }
        var selectedItems = grid.getSelectionModel().getSelections();
        var processesNames = [];
        
        for (var i = 0; i < selectedItems.length; i++) {
            processesNames.push(selectedItems[i].data.Name);
        }
            
        ProcessMonitor.Processes.InitWizardSession(nodeId, apmCredId, processesNames, OnSessionPrepared, OnSessionInitFailed);
    }
    
    var dataStore = new Ext.data.ArrayStore({
        id: 'dataStore',
        fields: [
                { name: 'PID' },
                { name: 'Name' },
                { name: 'CPU' },
                { name: 'PhysicalMemory' },
                { name: 'VirtualMemory' },
                { name: 'DiskIO' },
                { name: 'UserName' },
                { name: 'CommandLine' },
                { name: 'Path' },
                { name: 'Parameters' },
                { name: 'AssignedApps' }
                ],
        sortInfo: { field: sortColumn, direction: sortOrder },
        data: []
    });

    var selectorModel = new Ext.grid.CheckboxSelectionModel({ width: 25, checkOnly: true, header: '&nbsp;' });
    selectorModel.on("rowselect", OnSelectorSelect);
    selectorModel.on("rowdeselect", OnSelectorDeselect);

    var gridColumns = new Ext.grid.ColumnModel([
        selectorModel,
        { dataIndex: 'Name', header: '@{R=APM.Strings;K=APMWEBJS_AK1_33;E=js}', width: 100, sortable: true, hideable: false },
        { dataIndex: 'PID', header: '@{R=APM.Strings;K=APMWEBJS_AK1_32;E=js}', width: 40, sortable: true },
        { dataIndex: 'AssignedApps', header: '@{R=APM.Strings;K=APMWEBJS_AK1_31;E=js}', width: 100, sortable: false, renderer: renderAssignedApp },
        { dataIndex: 'CPU', header: '@{R=APM.Strings;K=APMWEBJS_AK1_30;E=js}', width: 70, sortable: true, renderer: renderCpuGauge },
        { dataIndex: 'PhysicalMemory', header: '@{R=APM.Strings;K=APMWEBJS_AK1_29;E=js}', width: 55, sortable: true, renderer: renderBytesToKilo, align: 'right' },
        { dataIndex: 'VirtualMemory', header: '@{R=APM.Strings;K=APMWEBJS_AK1_28;E=js}', width: 65, sortable: true, renderer: renderBytesToKilo, align: 'right' },
        { dataIndex: 'DiskIO', header: '@{R=APM.Strings;K=APMWEBJS_AK1_27;E=js}', width: 50, sortable: true, renderer: renderBytesToKiloPerSec, align: 'right' },
        { dataIndex: 'UserName', header: '@{R=APM.Strings;K=APMWEBJS_PS0_1;E=js}', width: 80, sortable: true, hidden: true, renderer: renderUserName },
        { dataIndex: 'CommandLine', header: '@{R=APM.Strings;K=APMWEBJS_PS0_2;E=js}', width: 150, sortable: true, hidden: true, renderer: renderCommandLine }
    ]);
    
   var grid = new Ext.grid.GridPanel({
        store: dataStore,
        cm: gridColumns,
        sm: selectorModel,
        tbar: [
            { xtype: 'button', id: 'StartStopButton', text: '@{R=APM.Strings;K=APMWEBJS_AK1_25;E=js}', icon: '/Orion/APM/Images/StatusIcons/ServiceStatus_Paused_16x16.gif', handler: StartStop },
            { xtype: 'button', id: 'ChangeCredButton', text: '@{R=APM.Strings;K=APMWEBJS_AK1_26;E=js}', icon: '/Orion/images/icon_assign_cred_16x16.gif', handler: SelectCredentials, hidden: !_allowChangeCredentials, disabled: _isDemo },
            { xtype: 'button', id: 'EndProcessButton', text: '@{R=APM.Strings;K=APMWEBJS_PS0_5;E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', handler: EndProcess },
            { xtype: 'button', id: 'StartMultipleProcessesMonitoring', text: '@{R=APM.Strings;K=APMWEBJS_AK1_34;E=js}', icon: '/Orion/APM/Images/icon_add.gif', handler: startMultiplePrecessesMonitoring },
            '->',
            { xtype: 'label', html: '<img src="/Orion/APM/Images/logo_SW_small.png"/>' }
        ],
        bbar: [
            { xtype: 'tbspacer', width: 6 },
            { xtype: 'label', text: '@{R=APM.Strings;K=APMWEBJS_AK1_46;E=js}' },
            { xtype: 'tbspacer', width: 4 },
            { xtype: 'numberfield', id: 'RowLimitTextBox', value: rowLimit, width: 30, maxLength: 4, allowNegative: false, allowBlank: false, allowDecimals: false },
            { xtype: 'tbspacer', id: 'RowLimitSpacer', width: 4 },
            { xtype: 'label', id: 'TotalProcessesLabel' },
            '-',
            { xtype: 'button', id: 'ShowButton', text: '@{R=APM.Strings;K=APMWEBJS_AK1_45;E=js}', handler: Show },
            '-',
            { xtype: 'button', id: 'RefreshButton', tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_93;E=js}', icon: '/Orion/APM/Images/icon_refresh.gif', handler: Refresh },
            '->',
            { xtype: 'label', id: 'PollingInRealtime' }
        ],
        viewConfig: { forceFit: false, autoFill: true, getRowClass: getGridRowClass, scrollToTop: Ext.emptyFn },
        listeners: { "sortChange": OnGridSortChange },
        stripeRows: true
   });
    
   if (Ext.isChrome) {
       grid.on('columnresize', setRowWidth, this);
       gridColumns.addListener('hiddenchange', function () { setTimeout(setRowWidth, 100); });
       gridColumns.addListener('columnmoved', function () { setTimeout(setRowWidth, 100); });
     }

    function getGridRowClass(record, index, rowParams, ds) {
        if (record.data.AssignedApps.highLight) {
            return "highlighted-row";
        }
    }

    function OnSessionPrepared(response) {
        var url = '/Orion/APM/Admin/MonitorLibrary/AppFinder/EditMonitorProperties.aspx';
        /*if (window.opener && !window.opener.closed) {
        //window.opener.location.replace(url); - browser does not store current page in history
        window.opener.location.href = url;
        }
        else */
        window.open(url, 'ProcessMonitorWizard');
    }

    function OnSessionInitFailed(error) {
        Ext.Msg.show({
            title: '@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}',
            msg: error._message,
            buttons: { ok: '@{R=APM.Strings;K=APMWEBJS_TM0_43;E=js}' },
            icon: Ext.MessageBox.ERROR
        });
    }

    function SelectCredentials() {
        SW.APM.CredentialsDialog.show({ addNoneCred: false, isRemote: _isRemote, onSuccess: CredentialSelected, addInheritedWindows: nodeSubType == 'WMI' });
    }

    function ClearGrid() {
        var data = [];
        grid.getStore().loadData(data);
    }

    function CredentialSelected(pCred) {
        apmCredId = pCred.id;
        apmCredName = pCred.name;

        SetWmiColumnVisibility(true);
        Ext.getCmp('EndProcessButton').setVisible(true);
        ClearGrid();
        if (timerEnabled) StartStop();
        PrepareLoadMask();
        Retry();
    }

    function EndProcess() {
        Ext.Msg.confirm("@{R=APM.Strings;K=APMWEBJS_PS0_3;E=js}", "@{R=APM.Strings;K=APMWEBJS_PS0_4;E=js}", function (btn, text) {
            if (btn == 'yes') {
                var selectedEssentialProcceses = GetSelectedEssentialProcessesNames();

                if (selectedEssentialProcceses != '') {
                    var message = "@{R=APM.Strings;K=APMWEBJS_PS0_9;E=js}";

                    Ext.Msg.confirm("@{R=APM.Strings;K=APMWEBJS_PS0_3;E=js}", message.replace('{0}', selectedEssentialProcceses), function (btn2, text2) {
                        if (btn2 == 'yes') {
                            KillProcesses();
                        }
                    });
                }
                else
                    KillProcesses();
            }
        });
    }

    function KillProcesses() {
        if (_isDemo) {
            DemoModeAction("RTPE_Kill_process");
            return;
        }

        //set locker for refreshing
        locker = true;

        //start killing job
        ProcessMonitor.Processes.KillProcesses(nodeId, selectedPIDs, apmCredId, OnKillDataReceived, OnError);

        //change text color for killed processes and display progress image                 
        $('div.x-grid3-row-selected div.x-grid3-col-1').css('color', 'red').css('font-weight', 'bold');
        $('div.x-grid3-row-selected div.x-grid3-col-1').each(function () {
            var processNameHtml = $(this).text() + '&nbsp;<img style="vertical-align:bottom" src="/Orion/APM/Images/loading_gen_16x16.gif" height="16" width="16">';
            $(this).html(processNameHtml);
        });
        //$('div.x-grid3-row-selected').removeClass('x-grid3-row-selected').addClass('x-grid3-row-selected-highlighted').css('border-color:', '#ACACAC').css('background-image', 'none');//.css('background-color', '#CCCCCC');
        //$('div.x-grid3-row-selected-highlighted div.x-grid3-col-1').css('background-color', 'red');

        //disable end process button
        Ext.getCmp('EndProcessButton').setDisabled(true);
    }

    function GetSelectedEssentialProcessesNames() {
        var result = '';
        var selectedItems = grid.getSelectionModel().getSelections();

        for (var i = 0; i < selectedItems.length; i++) {
            if (essentialProcesses.indexOf(selectedItems[i].data.Name) != -1) {
                if (result != '')
                    result += ', ' + selectedItems[i].data.Name;
                else
                    result = selectedItems[i].data.Name;
            }
        }

        return result;
    }

    function OnSelectorSelect() {
        var selectedItems = grid.getSelectionModel().getSelections();
        Ext.getCmp('StartMultipleProcessesMonitoring').setDisabled(false);
        //add checked item into selectedPids collection
        for (i = 0; i < selectedItems.length; i++) {
            if (selectedPIDs.indexOf(selectedItems[i].data.PID) == -1) {
                selectedPIDs.push(selectedItems[i].data.PID);
            }
            
            if (selectedItems[i].data.AssignedApps.length > 0) {
                Ext.getCmp('StartMultipleProcessesMonitoring').setDisabled(true);
            }
        }

        //stop refreshing and enable kill button
        if (selectedPIDs.length > 0) {
            if (timerEnabled == true) {
                StartStop();
                selectionStopRefresh = true;
            }
            Ext.getCmp('EndProcessButton').setDisabled(false);
       }
    };

    function OnSelectorDeselect() {
        var allItems = grid.getStore();
        var selectedItems = grid.getSelectionModel().getSelections();

        //remove unchecked item from selectedPids collection
        for (i = 0; i < allItems.data.length; i++) {
            if (selectedPIDs.indexOf(allItems.data.items[i].data.PID) > -1) {
                var selected = false;

                for (j = 0; j < selectedItems.length; j++) {
                    if (allItems.data.items[i].data.PID == selectedItems[j].data.PID) {
                        selected = true;
                    }
                }

                if (selected == false) {
                    selectedPIDs.splice(selectedPIDs.indexOf(allItems.data.items[i].data.PID), 1);
                }
            }
        }

        //start refreshing and disable kill button
        if (selectedPIDs.length == 0) {
            if (selectionStopRefresh == true) {
                StartStop();
                selectionStopRefresh = false;
            }
            Ext.getCmp('EndProcessButton').setDisabled(true);
        }
        
        Ext.getCmp('StartMultipleProcessesMonitoring').setDisabled(false);
        for (var index = 0; index < selectedItems.length; index++) {
            if (selectedItems[index].data.AssignedApps.length > 0) {
                Ext.getCmp('StartMultipleProcessesMonitoring').setDisabled(true);
            }
        }
        
        if (selectedItems.length == 0) Ext.getCmp('StartMultipleProcessesMonitoring').setDisabled(true);
       
    };

    function SetWmiColumnVisibility(visible) {
        // 6 = Virtual memory, 7 = DiskIO, 8 = Username, 9 = Command Line
        if (_isDemo) {
            gridColumns.setHidden(8, !visible);
            gridColumns.setHidden(9, false);
        }
        
        if (gridColumns.config[6].hideable == visible) return;
        gridColumns.config[6].hideable = visible;
        gridColumns.setHidden(6, !visible);
        gridColumns.config[7].hideable = visible;
        gridColumns.setHidden(7, !visible);
        gridColumns.config[8].hideable = visible;
        SetSelectorVisibility(visible);
    }

    function SetSelectorVisibility(visible) {
        gridColumns.setHidden(0, !visible);
    }

    return {
        Init: function () {
            var testNumber = 1;
            trimNumberTail = testNumber.toLocaleString().length - 1;                     
              
            if (nodeSubType == 'SNMP' && apmCredId == null) {
                SetWmiColumnVisibility(false);
                Ext.getCmp('EndProcessButton').setVisible(false);
            } else {
                if (_isDemo) {
                    SetWmiColumnVisibility(true);
                }
                if (!_allowEndProcess) {
                    SetSelectorVisibility(false);
                    Ext.getCmp('EndProcessButton').setVisible(false);
                }
            }

            Ext.getCmp('StartStopButton').setDisabled(_isDemo);
            Ext.getCmp('ChangeCredButton').setVisible(_allowChangeCredentials);
            Ext.getCmp('ChangeCredButton').setDisabled(_isDemo);
            Ext.getCmp('EndProcessButton').setDisabled(true);
            Ext.getCmp('StartMultipleProcessesMonitoring').setDisabled(true);

            PrepareLoadMask();
            if (nodeSubType != 'ICMP' || apmCredId != null) {
                loaderMask.show();
            }

            // layout
            grid.region = 'center';
            var viewPort = new Ext.Viewport({
                layout: 'border',
                items: [grid]
            });

            viewPort.render(Ext.getBody());

            //hide select/deselect all checkbox
            $('div.x-grid3-header div.x-grid3-hd-checker').css('background-image', 'none');
        },

        SetNodeInfo: function (id, name, subtype, statusIcon, allowChangeCredentials, credId, credName) {
            nodeId = id;
            nodeName = name;
            nodeSubType = subtype;
            nodeStatusIcon = statusIcon;
            _allowChangeCredentials = allowChangeCredentials;
            apmCredId = credId;
            apmCredName = credName;
        },

        SetFeatureInfo: function (remote, demo, allowStartMonitoring, allowEndProcess) {
            _isRemote = remote;
            _isDemo = demo;
            _allowStartMonitoring = allowStartMonitoring;
            _allowEndProcess = allowEndProcess;
        },

        SetSort: function (column, order, compId) {
            sortColumn = column;
            sortOrder = order;
            dataStore.sortInfo.field = column;
            dataStore.sortInfo.direction = order;
            highlightCompId = compId;
        },

        StartMonitoring: function (processname) {
            if (_isDemo) {
                DemoModeAction("RTPE_Start_monitoring");
                return;
            }
            var processesNames = [];
            processesNames.push(processname);
            ProcessMonitor.Processes.InitWizardSession(nodeId, apmCredId, processesNames, OnSessionPrepared, OnSessionInitFailed);
        },
       
        CancelPolling: function () {
            loaderMask.hide();
            StopPolling();
        }

    };

} ();

Ext.onReady(SW.APM.processMonitor.Init, SW.APM.processMonitor);
