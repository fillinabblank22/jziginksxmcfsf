<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true" 
    CodeFile="Settings.aspx.cs" Inherits="Orion_APM_Admin_Settings" Title="<%$ Resources: APMWebContent, APMWEBDATA_VB1_9%>" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="APM/APM.css"/>
    <orion:Include runat="server" File="APM/APM-ORION-FIX.css"/>
    <style type="text/css">
        #adminContent td { vertical-align: baseline; }
    </style>
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks">
        <orion:IconHelpButton HelpUrlFragment="OrionAPMPHConfigSettingsDatabaseSettings" ID="IconHelpButton1" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<table id="breadcrumb" style="width:auto;">
		<tr>
			<td style="border-bottom-width: 0px;">
				<h1><%=Page.Title%></h1>
			</td>
		</tr>
	</table>

	<asp:ValidationSummary ID="ValidationSummary1" runat="server" />

	<table cellspacing="0" id="adminContentTable" class="NeedsZebraStripes">
		<tr>
			<td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_10 %></td>
			<td class="Property" style="white-space:nowrap">
				<asp:TextBox ID="RetainDetail" runat="server" Width="60px" style="text-align:right;" MaxLength="5"></asp:TextBox>
					<%= Resources.APMWebContent.APMWEBDATA_VB1_11 %>
					<asp:CompareValidator ID="CompareValidator1" runat="server" 
					    ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_18 %>" ControlToValidate="RetainDetail" 
					    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RetainDetail"
						ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_19 %>">*</asp:RequiredFieldValidator>
			</td>
			<td class="Property"><%= Resources.APMWebContent.APMWEBDATA_VB1_12 %></td>
		</tr>
		<tr>
			<td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_25 %></td>
			<td class="Property" style="white-space:nowrap">
				<asp:TextBox ID="RetainHourly" runat="server" Width="60px" style="text-align:right;" MaxLength="5"></asp:TextBox>
					<%= Resources.APMWebContent.APMWEBDATA_VB1_11 %>
					<asp:CompareValidator ID="CompareValidator2" runat="server" 
					    ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_18 %>" ControlToValidate="RetainHourly" 
					    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RetainHourly"
						ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_20 %>">*</asp:RequiredFieldValidator>
			</td>
			<td class="Property"><%= Resources.APMWebContent.APMWEBDATA_VB1_13 %></td>
		</tr>
		<tr>
			<td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_14 %></td>
			<td class="Property" style="white-space:nowrap">
				<asp:TextBox ID="RetentionDays" runat="server" Width="60px" style="text-align:right;" MaxLength="5"></asp:TextBox>
					<%= Resources.APMWebContent.APMWEBDATA_VB1_11 %>
					<asp:CompareValidator ID="CompareValidator4" runat="server" 
					    ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_18 %>" ControlToValidate="RetentionDays" 
					    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RetentionDays"
						ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_21 %>">*</asp:RequiredFieldValidator>
			</td>
			<td class="Property"><%= Resources.APMWebContent.APMWEBDATA_VB1_15 %></td>
		</tr>
		<tr>
			<td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_PV0_1 %></td>
			<td class="Property" style="white-space:nowrap">
				<asp:TextBox ID="RetentionEventLogDays" runat="server" Width="60px" style="text-align:right;" MaxLength="5"></asp:TextBox>
					<%= Resources.APMWebContent.APMWEBDATA_VB1_11 %>
					<asp:CompareValidator ID="CompareValidator3" runat="server" 
					    ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_18 %>" ControlToValidate="RetentionEventLogDays" 
					    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="RetentionEventLogDays"
						ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_VB1_21 %>">*</asp:RequiredFieldValidator>
			</td>
			<td class="Property"><%= Resources.APMWebContent.APMWEBDATA_PV0_2 %></td>
		</tr>
		<tr>
			<td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_LB0_2 %></td>
			<td class="Property" style="white-space:nowrap">
				<asp:TextBox ID="BaselineDataDurationDays" runat="server" Width="60px" style="text-align:right;" MaxLength="5"></asp:TextBox>
					<%= Resources.APMWebContent.APMWEBDATA_VB1_11 %>
					<asp:CompareValidator ID="CompareValidator5" runat="server" 
					    ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_LB0_5 %>" ControlToValidate="BaselineDataDurationDays" 
					    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
					<asp:CustomValidator ID="CustomValidator1" runat="server" 
					    ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_LB0_6 %>" ControlToValidate="BaselineDataDurationDays" OnServerValidate="BaselineDataDurationDays_Validate">*</asp:CustomValidator>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="BaselineDataDurationDays"
						ErrorMessage="<%$ Resources:APMWebContent,APMWEBDATA_LB0_4 %>">*</asp:RequiredFieldValidator>
			</td>
			<td class="Property"><%= Resources.APMWebContent.APMWEBDATA_LB0_3 %></td>
		</tr>

		<asp:PlaceHolder id="placeHolderPlugins" runat="server"></asp:PlaceHolder>

	</table>
	
	<br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton ID="Submit" runat="server" OnClick="StoreData_Click" DisplayType="Primary" LocalizedText="Submit"/>
        <orion:LocalizableButton ID="Default" runat="server" OnClick="Default_Click" CausesValidation="False" DisplayType="Secondary" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_24 %>"/>
    </div>

</asp:Content>
