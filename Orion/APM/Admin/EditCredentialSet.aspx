<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true"
    CodeFile="EditCredentialSet.aspx.cs" Inherits="Orion_APM_Admin_EditCredentialSet"
    Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_158 %>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="APM/APM.css"/>
    <orion:Include runat="server" File="APM/APM-ORION-FIX.css"/>
    <script type="text/javascript">
        function checkPassword(oSrc, args) {
            var confpass = document.getElementById('<%= tbPassword.ClientID%>');
            var pass = document.getElementById('<%= tbPasswordConfirmation.ClientID %>');

            if ((confpass.value == pass.value))
                args.IsValid = true;
            else
                args.IsValid = false;
        }
    </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1>
        <asp:Label ID="lbTitle" runat="server" />
    </h1>
    <table class="form" cellspacing="0">
        <tr>
            <th>
                <%= Resources.APMWebContent.APMWEBDATA_VB1_151%></th>
            <td>
                <asp:TextBox ID="tbCredentialSetName" runat="server" Text="" Columns="40" />
                <asp:RequiredFieldValidator ID="tbCredentialSetNameValidator" runat="server" ControlToValidate="tbCredentialSetName"
                    ErrorMessage="*" Display="Dynamic" /></td>
        </tr>
        <tr>
            <th>
                <%= Resources.APMWebContent.APMWEBDATA_VB1_152 %></th>
            <td>
                <asp:TextBox ID="tbUserName" runat="server" Text="" Columns="40" />
                <asp:RequiredFieldValidator ID="userNameValidator" runat="server" ControlToValidate="tbUserName"
                    ErrorMessage="*" Display="Dynamic" /></td>
        </tr>
        <tr>
            <th>
                <%= Resources.APMWebContent.APMWEBDATA_VB1_153 %></th>
            <td>
                <asp:TextBox ID="tbPassword" runat="server" Text="" Columns="40" TextMode="Password" />
            </td>
        </tr>
        <tr>
            <th>
                <%= Resources.APMWebContent.APMWEBDATA_VB1_154 %></th>
            <td>
                <asp:TextBox ID="tbPasswordConfirmation" runat="server" Text="" Columns="40" TextMode="Password" />
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="passwordsMatchValidator" ClientValidationFunction="checkPassword" OnServerValidate="OnPasswordConfirmation_ServerValidate"
        ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_155 %>" Display="Dynamic" runat="server"/>    
    <asp:CustomValidator ID="nameExistsValidator" OnServerValidate="OnNameExists_ServerValidate" ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_177 %>" 
        Display="Dynamic" runat="server" />
    <br />
     <p><asp:Label ID="lbSequrityWarning" runat="server" ForeColor="red"><%= Resources.APMWebContent.APMWEBDATA_VB1_156 %><br />
     <%= Resources.APMWebContent.APMWEBDATA_VB1_157 %></asp:Label> 
    </p>
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" LocalizedText="Submit" DisplayType="Primary" ID="btnSubmit" OnClick="btnSubmit_Click" CausesValidation="true"/>
        <orion:LocalizableButton runat="server" LocalizedText="Cancel" DisplayType="Secondary" ID="btnCancel" CausesValidation="false" PostBackUrl="~/Orion/APM/Admin/CredentialsLibrary.aspx"/>
    </div>
</asp:Content>
