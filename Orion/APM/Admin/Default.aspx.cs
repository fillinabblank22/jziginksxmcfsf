﻿using System;
using System.Web.UI;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common;
using SolarWinds.Orion.Core.Common;

public partial class Orion_Admin_Default : Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.Title = InvariantString.Format(Resources.APMWebContent.APMWEBCODE_AK1_21, ApmConstants.ModuleShortName);
    }
}