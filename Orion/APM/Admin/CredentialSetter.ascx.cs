﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_CredentialSetter : System.Web.UI.UserControl
{
    private List<SelectNodeTreeItem> selNodesInfo, selTemplatesInfo;
    private List<ComponentBase> selComponentsInfo;

    private string selectedNodeTypes = string.Empty;

    private string key;

    private ListItem
        CredCore = new ListItem(@"{0} <a style=""font-weight:normal;font-size:8pt;color:#369;text-decoration:none;"" target=""_blank"" href=""{1}"">&#0187; {2}</a>"
            .FormatInvariant(Resources.APMWebContent.APMWEBCODE_AK1_98,
                HelpLocator.CreateHelpUrl("SAMAGAgentInheritCredsWDTM"),
                Resources.APMWebContent.APMWEBDATA_WhatDoesThisMean), "1"),
        CredTemplate = new ListItem(Resources.APMWebContent.APMWEBCODE_AK1_99, "2"),
        CredApplication = new ListItem(Resources.APMWebContent.APMWEBCODE_AK1_100, "2"),
        CredCustom = new ListItem(Resources.APMWebContent.APMWEBCODE_AK1_101, "3");

    public string CredentialOwner { get; set; }

    public bool ApplicationMode { get; set; }

    public bool AlwaysShown { get; set; }

    public int CredentialSetId
    {
        get
        {
            int id = ComponentBase.TemplateCredentialsId;
            if (ctrCredsTypes.SelectedValue == CredCustom.Value)
            {
                id = ctrCreds.SelectedCredentialSetId;
            }
            else if (ctrCredsTypes.SelectedValue == CredCore.Value)
            {
                id = ComponentBase.NodeWmiCredentialsId;
            }

            return id;
        }
    }

    public List<SelectNodeTreeItem> SelectedNodesInfo
    {
        get
        {
            if (AlwaysShown && selNodesInfo == null)
            {
                selNodesInfo = new List<SelectNodeTreeItem>();
            }

            if (Visible)
            {
                selNodesInfo.ForEach(item => item.CredentialId = CredentialSetId);
            }

            return selNodesInfo;
        }
        set
        {
            Visible = value != null && value.Count > 0;

            if (Visible)
            {
                string query = @"
SELECT NodeID, ObjectSubType
FROM Orion.Nodes
WHERE NodeID IN ({0})
".FormatInvariant(string.Join(",", value.Select(item => item.Id.ToString(CultureInfo.InvariantCulture)).ToArray()));

                DataRow[] nodes = SwisProxy.ExecuteQuery(query)
                    .Select("ObjectSubType {0} IN('WMI','Agent')".FormatInvariant(IsCoreCred ? "" : "NOT"));

                selNodesInfo = value
                    .Where(item => nodes.Contains(r => (int) r[0] == item.Id))
                    .Select(item => new SelectNodeTreeItem(item.Id, item.Name, item.Status))
                    .ToList();

                ctrExpander.Enabled = selNodesInfo.Any();

                if (AlwaysShown)
                {
                    Visible = true;
                    selectedNodeTypes = DefaultNodeTypes;
                }
                else
                {
                    Visible = ctrExpander.Enabled;
                    selectedNodeTypes = string.Join(" &amp; ", nodes.Select(r => r[1].ToString()).Distinct());
                }
            }
            else if (AlwaysShown)
            {
                Visible = true;
                ctrExpander.Enabled = false;
                selectedNodeTypes = DefaultNodeTypes;
            }
        }
    }

    public List<SelectNodeTreeItem> SelectedTemplatesInfo
    {
        get
        {
            if (Visible)
            {
                selTemplatesInfo = selTemplatesInfo ?? new List<SelectNodeTreeItem>();
            }

            return selTemplatesInfo;
        }
        set { selTemplatesInfo = value; }
    }

    public List<ComponentBase> SelectedComponentsInfo
    {
        get
        {
            if (Visible)
            {
                selComponentsInfo = selComponentsInfo ?? new List<ComponentBase>();
            }

            return selComponentsInfo;
        }
        set { selComponentsInfo = value; }
    }

    public bool IsValid
    {
        get
        {
            if (Visible)
            {
                if (ctrCredsTypes.SelectedItem.Value == CredCustom.Value)
                {
                    Page.Validate(ctrCreds.ValidationGroupName);

                    return ctrCreds.IsValid;
                }
            }

            return true;
        }
    }

    public bool HideTestControl { get; set; }

    private string Key
    {
        get
        {
            if (key.IsNotValid())
            {
                string hash = Page.GetType().Name.ToLowerInvariant().GetHashCode()
                    .ToString(System.Globalization.CultureInfo.InvariantCulture)
                    .Replace('-', 'F');
                if (IsCoreCred)
                {
                    return (key = "KeyTR_{0}Core".FormatInvariant(hash));
                }

                return (key = "KeyTR_{0}Apm".FormatInvariant(hash));
            }

            return key;
        }
    }

    private bool IsCoreCred
    {
        get { return "Core".Equals(CredentialOwner, StringComparison.InvariantCultureIgnoreCase); }
    }

    protected string Title
    {
        get
        {
            if (Visible)
            {
                int count = SelectedNodesInfo.Count;
                string nodeS = (count == 1) ? Resources.APMWebContent.APMWEBDATA_NodeSingle : Resources.APMWebContent.APMWEBDATA_NodePlural;
                return Resources.APMWebContent.APMWEBCODE_AK1_26.FormatInvariant(nodeS, count, selectedNodeTypes);
            }

            return null;
        }
    }

    protected string SubTitle
    {
        get
        {
            if (Visible)
            {
                string name = Resources.APMWebContent.APMWEBCODE_AK1_29;
                if (SelectedTemplatesInfo.Count > 0)
                {
                    name = HttpUtility.HtmlEncode(SelectedTemplatesInfo.First().Name);
                }

                return Resources.APMWebContent.APMWEBCODE_AK1_30.FormatInvariant(name);
            }

            return null;
        }
    }

    private SelectNodeTreeItem NodeInfo
    {
        get
        {
            if (ctrNodes.SelectedIndex == -1)
            {
                ctrNodes.SelectedIndex = 0;
            }

            return SelectedNodesInfo
                .FirstOrDefault(item => item.Id.ToString() == ctrNodes.SelectedValue);
        }
    }

    private List<SelectNodeTreeItem> Components
    {
        get
        {
            if (SelectedTemplatesInfo.Count > 0)
            {
                var cmps = new List<ComponentTemplate>();
                using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                {
                    foreach (SelectNodeTreeItem tpl in SelectedTemplatesInfo)
                    {
                        ApplicationTemplate tmpl = bl.GetApplicationTemplate(tpl.Id);

                        cmps.AddRange(tmpl.ComponentTemplates);
                    }
                }

                return cmps
                    .Select(item => new SelectNodeTreeItem((int) item.Id, item.Name) {CredentialId = item.CredentialSetId})
                    .ToList();
            }

            if (SelectedComponentsInfo.Count > 0)
            {
                return SelectedComponentsInfo
                    .Select(item => new SelectNodeTreeItem((int) item.Id, item.Name) {CredentialId = item.CredentialSetId})
                    .ToList();
            }

            return null;
        }
    }

    private string DefaultNodeTypes
    {
        get
        {
            return CredentialOwner == "Core"
                ? Resources.APMWebContent.ApplicationTemplateAssignment_AgentWmi
                : Resources.APMWebContent.ApplicationTemplateAssignment_SnmpIcmp;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        ctrCreds.ValidationSummaryParentClientID = ClientID;

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            ctrTest.Key = ctrValSummary.ValidationGroup = ctrCreds.ValidationGroupName = Key;

            if (!IsPostBack)
            {
                ctrCredCnt.Visible = ctrCreds.Visible = false;
                if (IsCoreCred)
                {
                    ctrCredsTypes.Items.Add(CredCore);
                    ctrCredsTypes.Items.Add(ApplicationMode ? CredApplication : CredTemplate);
                    ctrCredsTypes.Items.Add(CredCustom);

                    ctrCredsTypes.SelectedIndex = 0;
                }
                else
                {
                    ctrCredsTypes.Items.Add(ApplicationMode ? CredApplication : CredTemplate);
                    ctrCredsTypes.Items.Add(CredCustom);

                    ctrCredsTypes.SelectedIndex = 1;
                    ctrCredCnt.Visible = ctrCreds.Visible = true;
                }

                ListItem[] items = SelectedNodesInfo
                    .Select(item => new ListItem(@"<img src=""/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={0}""/> {1}".FormatInvariant(item.Status, item.Name), item.Id.ToString()))
                    .ToArray();
                ctrNodes.Items.AddRange(items);
                ctrNodes.SelectedIndex = 0;
            }

            this.UpdateTestResult(null);
        }
    }

    protected void OnCredValidate_Click(object sender, EventArgs e)
    {
        this.UpdateTestResult("run-test");
    }

    protected void OnNodes_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UpdateTestResult(null);
    }

    protected void OnCredsTypes_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ctrCredsTypes.SelectedValue == CredCustom.Value)
        {
            ctrCredCnt.Visible = ctrCreds.Visible = true;
        }
        else
        {
            ctrCredCnt.Visible = ctrCreds.Visible = false;
        }

        this.UpdateTestResult(null);
    }

    private void UpdateTestResult(string action)
    {
        if (Visible)
        {
            object credentialsId = ComponentBase.TemplateCredentialsId;
            if (ctrCredsTypes.SelectedItem.Value == CredCore.Value)
            {
                credentialsId = ComponentBase.NodeWmiCredentialsId;
            }
            else if (ctrCredsTypes.SelectedItem.Value == CredCustom.Value)
            {
                credentialsId = ComponentBase.NewCredentialsId;

                CredentialSet cred = ctrCreds.SelectedCredentialSet;
                if (cred.Id < 0)
                {
                    if (IsValid)
                    {
                        using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                        {
                            cred.Id = bl.CreateNewCredentialSet(cred);
                            ctrCreds.ReloadCredentialSets();
                            ctrCreds.DataSource = cred;
                            ctrCreds.DataBindNewCredentialSetForm();
                        }
                    }
                    else
                    {
                        action = null;
                    }
                }
            }

            if (!HideTestControl)
            {
                ctrTest.Initialize(ClientID, SelectedNodesInfo, Components, ApplicationMode);
                ctrTest.Update(NodeInfo.Id, NodeInfo.Name, credentialsId, action);
            }
        }
    }

    public void ClearSession()
    {
        ctrTest.ClearSession();
    }
}