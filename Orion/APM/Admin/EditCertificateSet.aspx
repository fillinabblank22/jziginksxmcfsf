﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true"
    CodeFile="EditCertificateSet.aspx.cs" Inherits="Orion_APM_Admin_EditCertificateSet"
    Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_158 %>" %>

<%@ Register src="EditCertificateSetFileUploader.ascx" tagname="EditCertificateSetFileUploader" tagprefix="uc1" %>
<%@ Register TagPrefix="apm" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <apm:IncludeExtJs runat="server" debug="false" Version="3.4"/>
    <orion:Include runat="server" File="APM/APM-ORION-FIX.css"/>
    <orion:Include runat="server" File="APM/Admin/Js/FileUpload.js" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="SAMAGAddingACertificate" ID="IconHelpButton1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 class="sw-hdr-title">
        <asp:Label ID="lbTitle" runat="server" />
    </h1>
    <table class="form" style="padding-top: 10px;">
        <tr>
            <th class="formLabelStrong">
                <%= Resources.APMWebContent.APMWEBDATA_VB1_151%>
            </th>
            <td>
                <asp:TextBox ID="tbCredentialSetName" runat="server" Text="" Columns="40" />
                <asp:RequiredFieldValidator ID="tbCredentialSetNameValidator" runat="server" ControlToValidate="tbCredentialSetName"
                    ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_345 %>" Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <th class="formLabelStrong">
                <%= Resources.APMWebContent.APMWEBDATA_VB1_152 %>
            </th>
            <td>
                <asp:TextBox ID="tbUserName" runat="server" Text="" Columns="40" />
                <asp:RequiredFieldValidator ID="userNameValidator" runat="server" ControlToValidate="tbUserName"
                    ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_346 %>" Display="Dynamic" />
             </td>
        </tr>
        <tr>
            <th class="formLabelStrong">
                <%= Resources.APMWebContent.APMWEBDATA_RB2_2 %>
            </th>
            <td>
                <div class="apm_blueBox">
                    <table class="form" style="padding-left: 10px;">
                        <tr>
                            <td colspan="2">
		                        <img src="/Orion/APM/Images/Admin/icon_lightbulb.gif" style="vertical-align:middle;"><span style="padding-left:15px"><%= Resources.APMWebContent.APMWEBDATA_RB2_3 %></span>
                            </td>
                        </tr>
                        <tr>
                            <th class="formLabel">
                                <%= Resources.APMWebContent.APMWEBDATA_RB2_4 %>
                            </th>
                            <td>
                                <uc1:EditCertificateSetFileUploader ID="fileUploader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <th class="formLabel">
                                <%= Resources.APMWebContent.APMWEBDATA_RB2_5 %>
                            </th>
                            <td>
                                <asp:TextBox ID="tbPrivateKey" runat="server" Text="" Columns="100" TextMode="MultiLine" Height="270px" style="font-family: Courier New" /><br />
                                <asp:RequiredFieldValidator ID="privateKeyValidator" runat="server" ControlToValidate="tbPrivateKey"
                                    ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_RB2_6 %>" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <th class="formLabel"><%= Resources.APMWebContent.APMWEBDATA_RB2_7 %></th>
                            <td>
                                <asp:DropDownList ID="ddlPrivateKeyType" runat="server">
                                    <asp:ListItem Value="RSA" Text="RSA" Selected="True" />
                                    <asp:ListItem Value="DSA" Text="DSA" />
                                </asp:DropDownList>
                            </td>
                        </tr>        <tr>
                            <th class="formLabel">
                                <%= Resources.APMWebContent.APMWEBDATA_RB2_8 %>
                            </th>
                            <td>
                                <asp:TextBox ID="tbFilePassword" runat="server" TextMode="Password" Columns="40" /><br/>
                                <%= Resources.APMWebContent.APMWEBDATA_RB2_9 %>
                            </td>
                        </tr>                        
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:CustomValidator ID="nameExistsValidator" OnServerValidate="OnNameExists_ServerValidate" ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_177 %>" 
                    Display="Dynamic" runat="server" />
                <asp:CustomValidator ID="decryptValidator" OnServerValidate="Decrypt_ServerValidate" ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_RB2_1 %>" 
                    Display="Dynamic" runat="server" />
            </td>
        </tr>
    </table>
    <p><asp:Label ID="lbSequrityWarning" runat="server" ForeColor="red"><%= Resources.APMWebContent.APMWEBDATA_VB1_156 %><br />
     <%= Resources.APMWebContent.APMWEBDATA_VB1_157 %></asp:Label> 
    </p>
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" LocalizedText="Submit" DisplayType="Primary" ID="btnSubmit" OnClick="btnSubmit_Click" CausesValidation="true"/>
        <orion:LocalizableButton runat="server" LocalizedText="Cancel" DisplayType="Secondary" ID="btnCancel" CausesValidation="false" PostBackUrl="~/Orion/APM/Admin/CertificatesLibrary.aspx"/>
    </div>
</asp:Content>
