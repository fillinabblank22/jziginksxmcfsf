﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckDuplicates.ascx.cs" Inherits="Orion_APM_Admin_CheckDuplicates" %>

<script type="text/javascript">
	if(typeof(SW) == "undefined") { SW = {}; }
	if(typeof(SW.APM) == "undefined") { SW.APM = {}; }
	
	/*Check Duplicates*/
	SW.APM.Duplicates = function(params) {
		var mItems, mDlg, mOrderBy = "T";

		function cutText(text) {
			if (text.length > 45) { return text.substr(0, 45) + "..."; } return text;
		}
		function sort() {
			var prop = (mOrderBy == "N" ? "NodeName" : "TplName");
			mItems.sort("ASC", function(item1, index1) { var is = false; mItems.each(function(item2, index2) { is = item1[prop] > item2[prop]; if (is) { return false; } }); return is ? 1 : -1; });
		}
		function getTip(rec) {
			return (String.format('<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_2)%>', rec.AppName) + String.format('<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_3)%>', rec.TplName) + String.format('<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_4)%>', rec.NodeName));
		}
		function getAppCount(groupItems, prop) { 
			var count = 0, arr = []; 
			groupItems.each(function(item) { if (!arr[item[prop]]) { arr[item[prop]] = true; count += item.AppCount; } }); 
			return count; 
		}

		function onBind() {
			if (!mDlg) { return; }
			var tree = mDlg.getComponent("tree"), root = tree.getRootNode();

			for (var i = root.childNodes.length - 1; i > -1; i--) { root.childNodes[i].remove(); }

			if (!mItems || mItems.getCount() < 1) {
				root.appendChild({ text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_5)%>', iconCls: "hidden", nodeType: "node", draggable: false, leaf: false });
				return;
			}

			sort();
			for (var i = 0, iC = mItems.getCount(); i < iC; ) {
				var groupItems, item = mItems.get(i), parent;

				if (mOrderBy == "T") {
					groupItems = mItems.filter("TplID", item.TplID);
					parent = root.appendChild({ apmRecord: item, text: item.TplName, qtip: String.format('<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_6)%>', getAppCount(groupItems, "NodeID"), item.TplName), iconCls: "icon", icon: "/Orion/APM/images/Admin/icon_application_template.gif", nodeType: "node", draggable: false, leaf: false, singleClickExpand: true });
				} else if (mOrderBy == "N") {
					groupItems = mItems.filter("NodeID", item.NodeID);
					parent = root.appendChild({ apmRecord: item, text: item.NodeName, qtip: String.format('<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_6)%>', getAppCount(groupItems, "TplID"), item.NodeName), nodeType: "node", iconCls: "icon", icon: String.format("/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={0}&status={1}&size=Small", item.NodeID, item.NodeLED), draggable: false, leaf: false, singleClickExpand: true });
				} 
				i += groupItems.getCount();
				for (var j = 0, jC = groupItems.getCount(); j < jC; j++) {
					var child = groupItems.get(j);
					parent.appendChild({ qtip: getTip(child), text: cutText(child.AppName), iconCls: "icon", icon: child.AppLED, nodeType: "node", draggable: false, leaf: true });
				}
			}
		}

		function onShow() {
			mOrderBy = "T";
		    emptyText = '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_9)%>' + ' ';
			mDlg = new Ext.Window({
				title: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_7)%>', width: 400, height: 450, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "padding:5px;",
				items: [new Ext.tree.TreePanel({ id: "tree", frame: false, autoScroll: true, animate: false, containerScroll: true, rootVisible: false, bodyStyle: 'padding-top:2px;', loader: new (Ext.extend(Ext.tree.TreeLoader, { load: function() { onBind(); } }))(), root: new Ext.tree.AsyncTreeNode({ id: "source", text: "text", draggable: false }), tbar: ['<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_8)%>', {id:'templateCmb', xtype: "combo", valueField: "groupBy", displayField: "displayText", mode: "local", triggerAction: "all", emptyText: emptyText, width: 255, typeAhead: true, editable: false, store: new Ext.data.SimpleStore({ fields: ["groupBy", "displayText"], data: [["T", '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_9)%>'], ["N", '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_10)%>']] }), listeners: { select: function(owner) { mOrderBy = owner.getValue(); onBind(); Ext.getCmp('templateCmb').setValue(store.getAt('0').get('groupBy'));} }}] })],
				buttons: [{ text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_11)%>', handler: onClose}]
			});
			mDlg.show();
		}
		function onClose() { mDlg.hide(); mDlg.destroy(); mDlg = null; }

		function ctor() {
			Ext.QuickTips.init();
			function onSuccess(json) {
				(mItems = new Ext.util.MixedCollection()).addAll(ORION.objectFromDataTable(json).Rows);

				var container = Ext.get("duplicates");
				if (mItems && mItems.getCount() > 0) {
					var HREF_TPL = "<br/><a href='#' style='color:#4779c4;font-size:8pt;text-decoration:underline;'><b>" + '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_12)%>' + "</b></a>";
					var count = 0, arr = []; mItems.each(function(item) { if (!arr[item.TplID]) { arr[item.TplID] = true; count += 1; } });

					var tds = container.select("tr/td");
					tds.item(0).update(String.format(HREF_TPL, count)).select("a").on("click", onShow);
					tds.item(1).update('<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_13)%>');

					var cb = new Ext.form.ComboBox({
						valueField: "skip", displayField: "displayText", mode: "local", value: params.skip, triggerAction: "all", width: 210, typeAhead: true, editable: false, renderTo: container.select("tr/td/div").item(0), store: new Ext.data.SimpleStore({ fields: ["skip", "displayText"], data: [[true, '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_14)%>'], [false, '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_15)%>']] }), listeners: { select: function(owner) { if (params.onSkip) { params.onSkip(owner.getValue()); } } }
					});
				} else {
					container.setStyle({"display": "none"});	
				}
			}
			ORION.callWebService("/Orion/APM/Services/Templates.asmx", "GetDuplicates", { nodeIDs: params.nodes, tplIDs: params.templates }, onSuccess);
		}; ctor();
	}
	
	Ext.onReady(
		function() {
			<%= this._json %>
			var d = SW.APM.Duplicates.data;
			if (d && d.nodes.length > 0 && d.templates.length > 0) { 
				function onSkipChange(val) {  d.skip = val; Ext.get("skipDuplicates").dom.value = val; }; onSkipChange(d.skip);
				
				var ctr = new SW.APM.Duplicates({ "nodes": d.nodes, "templates": d.templates, "skip": d.skip, "onSkip": onSkipChange });
			} else {
				Ext.get("duplicates").setStyle({"display": "none"});
			}
			SW.APM.Duplicates.checkDuplicates = function() {
				if (d && d.skip && d.allDuplicates) { 
					Ext.Msg.show({ title: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_16)%>', msg: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_17)%>', icon: Ext.Msg.WARNING, buttons: Ext.Msg.OK });
					return false;
				}
				return true;
			}
		}
	);
</script>

<table id="duplicates" width="100%">
	<tbody>
		<tr>
			<td colspan="2" align="right"></td>
		</tr>
		<tr>
			<td align="right"></td>
			<td style="width:140px"><div></div></td>
		</tr>
	</tbody>
</table>
<input id="skipDuplicates" name="skipDuplicates" type="hidden"/>
