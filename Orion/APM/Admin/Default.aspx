<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_64 %>" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Admin_Default" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI.AutoAssign" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHConfigSettings" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="hPh">
    <style type="text/css">
        #container{background-color: #dfe0e1 !important;}  
        #adminContent{background-color: #dfe0e1;}  
        #adminContent p { width: auto; padding: 0; }
       
        #adminContent * .sw-hdr-title{font-size: 18px;text-transform: capitalize;line-height: 15px;padding: 15px;margin-right: 15px;background: #ffffff;box-shadow: 0 1px 5px 0 #b9b9b9;}
        #adminContent * .sw-hdr-title * h1{font-weight: 700;}
        .sw-hdr-title {text-transform: none!important;}
        #adminContent * h2.sw-hdr-title{border-bottom: white 1px solid;margin-top: 0;}
        #adminContent * .column1of1 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left;}
        #adminContent * .column1of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left;}
        #adminContent * .column2of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%;}
        #adminContent * .column1of2 * img{ width: 75px;height: 75px;} 
        #adminContent * .column2of2 * img{ width: 75px;height: 75px;} 
    
    
        #adminContent * .blockHeader{ font-weight: 700;font-size: 16px;margin-top: 0;}
        #adminContent * td.column2of2 * .iconContainer{ padding-right: 10px;vertical-align: top;}
        #adminContent * .sw-settings-rightbox-content * .paragraphHeader{font-weight: 700;margin-bottom: 5px;}
        #adminContent * .sw-settings-rightbox-content p {margin-top: 0;margin-bottom: 0;}
        #adminContent * .sw-settings-rightbox-content .subParagraph{ margin-top: 20px;}
        #adminContent * .sw-settings-rightbox-content .collapseParagraph { margin-bottom: 5px;}
        #adminContent * .sw-settings-rightbox-content .collapseParagraph > span, #adminContent * .sw-settings-rightbox-content .collapseParagraph > img { cursor: pointer;}
        #adminContent * .sw-settings-rightbox-content .subParagraph > a{ cursor: pointer;display: block;margin-bottom: 5px;}
        #adminContent * .sw-settings-delimiter { border-top: 1px solid #fff;}
    
        #helpFromHumans * .collapseParagraph > div {margin: 10px 0px 15px 18px;display: none;}
        #helpFromHumans * .collapseParagraph > div > p > span{color:#888;}
        #helpFromHumans * .collapseParagraph > img {vertical-align: top;}
    
        #productEducation * .NewContentBucket { box-shadow: none;padding: 0;margin: 0;}
        #productEducation * .NewBucketLinkContainer { margin: 0;padding: 0;}
        #productEducation * .LinkColumn { padding-top: 0;}
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminBodyPlaceholder">
</asp:Content>

<asp:Content ContentPlaceHolderID="cPh" runat="server">
    <orion:Include File="js/AsyncView.js" runat="server" />
    <orion:Refresher ID="Refresher" runat="server" />
    <div id="adminContent">

        <table class="settingsTable" style="width: 100%;">
            <tr>
                <td class="column1of1" colspan="2">
                    <div class="sw-hdr-title">
                        <h1><%= Page.Title %></h1>
                        <div style="font-size: 11px;">
                            <span><%= SolarWinds.APM.Web.RegistrySettings.GetVersionDisplayString() %></span>
                            <span>
                                <%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_11, SolarWinds.APM.Web.RegistrySettings.GetBuildNumber()) %></span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="column1of2">


                    <!-- Begin Getting Started Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/images/SettingPageIcons/getting started.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_12, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %>
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_AK1_13 %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/AutoAssign/SelectNodes.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_15 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/QuickStart/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_16 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/MonitorLibrary/AppFinder/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_PS0_2 %></a>
                                        </p>
                                    </td>
                                </tr>
                                <% if ((new AutoAssignUtility(false)).StateIsAvailable)
                               { %>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/AutoAssign/AutoAssignScanStatus.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_18 %></a>
                                        </p>
                                    </td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                    <!-- End Getting Started Bucket -->

                    <!-- Begin Application Monitor Bucket -->

                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/APM/Images/Admin/SettingsHome/app-small.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.APMWebContent.APMWEBDATA_AK1_19 %>
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_AK1_20 %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/Applications/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_21 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End Application Monitor Bucket -->

                    <!-- Begin Template Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/APM/Images/Admin/SettingsHome/app-templates.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.APMWebContent.APMWEBDATA_AK1_22 %>
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_AK1_23 %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/ApplicationTemplates.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_24 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/CreateNewTemplate.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_25 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End Template Bucket -->

                    <!-- Begin Component Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/APM/Images/Admin/SettingsHome/component.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.APMWebContent.APMWEBDATA_AK1_26 %>
                            </div>
                            <p><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_27, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %>
                            </p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/MonitorLibrary/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_29 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/Components/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_30 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/Components/Templates.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_31 %></a>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End Component Bucket -->
                    <!-- Begin Groups Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/APM/Images/Admin/SettingsHome/folder.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.APMWebContent.APMWEBDATA_AK1_110 %>
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_AK1_32 %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/Admin/Containers/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_33 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End Groups Bucket -->
                </td>
                <td class="column2of2">
                    <!-- Begin Views Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/images/SettingPageIcons/layout_small.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.APMWebContent.APMWEBDATA_AK1_34 %>
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_AK1_35 %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/ViewsByApplicationType.aspx?redirectTo=apmSettings"><%= Resources.APMWebContent.APMWEBDATA_AK1_36 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End Views Bucket -->

                    <!-- Begin Settings Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/images/SettingPageIcons/product-specific.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_37, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %>
                            </div>
                            <p><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_39, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %>
                            </p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/CredentialsLibrary.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_41 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/CertificatesLibrary.aspx"><%= Resources.APMWebContent.APMWEBDATA_RB0_1 %></a>

                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/Settings.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_42 %></a>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End Settings Bucket -->

                    <!-- Begin License Details Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/APM/Images/Admin/SettingsHome/documents.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.APMWebContent.APMWEBDATA_AK1_43 %>
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_AK1_44 %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/LicenseSummary.aspx"><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_40, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End License Details Bucket -->

                    <!-- End Thwack Details Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/APM/Images/Admin/SettingsHome/blog.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.APMWebContent.APMWEBDATA_AK1_45 %>
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_AK1_46 %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <table>
                                <tr>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="/Orion/APM/Admin/Templates/thwack/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_47 %></a>
                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            <span class="LinkArrow">&#0187;</span> <a
                                                href="https://thwack.solarwinds.com/community/application-and-server_tht/server-and-application-monitor"><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_48, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %></a>

                                        </p>
                                    </td>
                                    <td class="LinkColumn" width="33%">
                                        <p>
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- Begin Thwack Details Bucket -->
                </td>
            </tr>
        </table>
    </div>
    <br />
</asp:Content>