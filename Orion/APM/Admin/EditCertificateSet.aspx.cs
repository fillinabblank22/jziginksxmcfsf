﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Credentials;
using SolarWinds.APM.Web.Enums;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_APM_Admin_EditCertificateSet : System.Web.UI.Page
{
    #region Properties

    private Int32 CredID
    {
        get
        {
            Int32 id;
            if (!Int32.TryParse(Request.Params["crdId"], out id))
            {
                id = -1;
            }
            return id;
        }
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        btnSubmit.AddEnterHandler(1);

        ApmBaseResource.EnsureApmScripts(true);

        Mode mode = ModeHelper.GetModeFromString(Request.Params["mode"].ToString());
        Uri uri = new Uri(Request.Url.AbsoluteUri, UriKind.Absolute);

        lbSequrityWarning.Visible = (
            uri.Scheme.Equals("http") && !Request.IsLocal
        );
        fileUploader.FileContentElementId = tbPrivateKey.ClientID;

        if (!IsPostBack)
        {
            if (mode == Mode.Edit)
            {
                CredentialSet credentialToEdit;
                using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                {
                    credentialToEdit = businessLayer.GetCredentialsById(this.CredID);
                }
                tbCredentialSetName.Text = credentialToEdit.Name;
                tbUserName.Text = credentialToEdit.UserName;
                lbTitle.Text = string.Format(Resources.APMWebContent.APMWEBCODE_VB1_74, credentialToEdit.Name);
            }
            else
            {
                lbTitle.Text = Resources.APMWebContent.APMWEBCODE_VB1_75;
            }
            btnSubmit.Focus();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid) return;

        CredentialSet credentialModel = new CredentialSet(typeof(ApmUsernamePrivateKeyCredential));
        Mode mode = (Mode)Enum.Parse(typeof(Mode), Request.Params["mode"].ToString(), true);

        //set credential model
        if (mode == Mode.Edit) credentialModel.Id = CredID;
        credentialModel.Name = WebSecurityHelper.SanitizeHtmlV2(tbCredentialSetName.Text);
        credentialModel.UserName = WebSecurityHelper.SanitizeHtmlV2(tbUserName.Text);

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            credentialModel.Password = businessLayer.DecryptCertificateFile(tbPrivateKey.Text, tbFilePassword.Text, ddlPrivateKeyType.SelectedValue);

            if (mode == Mode.Edit)
                businessLayer.UpdateCredentialSet(credentialModel);
            else
                businessLayer.CreateNewCredentialSet(credentialModel);
        }

        Response.Redirect("~/Orion/APM/Admin/CertificatesLibrary.aspx");
    }

    protected void OnNameExists_ServerValidate(Object source, ServerValidateEventArgs args)
    {
        Int32 credID = this.CredID;
        using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            foreach (var credName in bl.GetCertificateNames())
            {
                if (
                    credName.Key != credID &&
                    credName.Value.Equals(this.tbCredentialSetName.Text, StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    args.IsValid = false;
                    return;
                }
            }
        }
    }

    protected void Decrypt_ServerValidate(Object source, ServerValidateEventArgs args)
    {
        using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            try
            {
                var password = bl.DecryptCertificateFile(tbPrivateKey.Text, tbFilePassword.Text,
                                                         ddlPrivateKeyType.SelectedValue);
                args.IsValid = true;
            }
            catch
            {
                args.IsValid = false;
            }

        }
    }

    #endregion
}