﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using System.Data;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_ComponentTestResults : System.Web.UI.UserControl
{
    private bool _ignoreAsynchRegister = true;
    public bool IgnoreAsynchRegister 
    { 
        get { return _ignoreAsynchRegister; }
        set { _ignoreAsynchRegister = value; } 
    }

	public ComponentTestResult TestResult
	{
		get { return (ComponentTestResult)ViewState["test-results"]; }
		set
		{
			ViewState["test-results"] = value;
			UpdateControls();
		}
	}

    protected string TestSinglePostProcessingClientID
    {
        get
        {
            return this.ClientID.Substring(0, this.ClientID.LastIndexOf("_")) + "_TestSinglePostProcessing";
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (TestResult.TestStarted && !TestResult.TestComplete)
        {
            bool registerScriptForAsyncPostBack = false;
            
            ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
            if ((scriptManager != null) && scriptManager.IsInAsyncPostBack)
            {
                // don't register script on updating this panel - only if it's specially requested from upper control
                if (IgnoreAsynchRegister)
                {
                    base.OnPreRender(e);
                    return;
                }
                registerScriptForAsyncPostBack = true;
            }
            
            if (!Page.ClientScript.IsStartupScriptRegistered("QueueAsynchPostbacks"))
            {
                // Weird script below fixes two issues:
                // 1. Multiple asynch postbacks: http://disturbedbuddha.wordpress.com/2007/12/12/handling-multiple-asynchronous-postbacks/
                // 2. Page scroll after response comes: http://www.daniweb.com/forums/thread113318.html

                string scr =
                    @"
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        prm.add_initializeRequest(InitializeRequestHandler);
                        prm.add_beginRequest(BeginRequestHandler);
                        prm.add_endRequest(EndRequestHandler);        

                        var pbQueue = new Array();
                        var argsQueue = new Array();       

                        function InitializeRequestHandler(sender, args) {
                            if (prm.get_isInAsyncPostBack()) {
                                args.set_cancel(true);
                                pbQueue.push(args.get_postBackElement().id);
                                argsQueue.push(document.forms[0].__EVENTARGUMENT.value);
                            }
                        }       

                        function BeginRequestHandler() {
                            prm._scrollPosition = null;
                        }

                        function EndRequestHandler(sender, args) {
                            if (pbQueue.length > 0) {
                                __doPostBack(pbQueue.shift(), argsQueue.shift());
                            }

                            testSinglePostProcessingControl = document.getElementById('" + TestSinglePostProcessingClientID + @"');
                            if (testSinglePostProcessingControl != null)
                            {
                                try {
                                    strExec = testSinglePostProcessingControl.value;
                                    strExec = strExec.replace('#TestStatistic', document.getElementById('" + hfStatisticValue.ClientID + @"').value);
                                    strExec = strExec.replace('#TestSuccessful', document.getElementById('" + hfTestSuccessful.ClientID + @"').value);
                                    eval(strExec);
                                }
                                catch (ex) {
                                }
                                document.getElementById('" + TestSinglePostProcessingClientID + @"').value = '';
                            }
                        }
                    ";
                Page.ClientScript.RegisterStartupScript(GetType(), "QueueAsynchPostbacks", scr, true);
                if (registerScriptForAsyncPostBack)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, GetType(), "QueueAsynchPostbacks", scr, true);
                }
            }

            if (!Page.ClientScript.IsClientScriptBlockRegistered("TestTimerStart"))
            {
                string scr =
                    @"
                        var _apm_test_guids_started = new Array();

                        function GetFinishedJobIDs() {
                            var testIds = GetActiveIDs();
                            if (testIds.length > 0) {                            
                                ORION.callWebService(""/Orion/APM/Services/ComponentTest.asmx"",
                                    ""GetFinishedJobIDs"", { jobIDs: testIds },
                                    function(result) {
                                        OnGetFinishedJobIDsCompleted(result);
                                    }
                                );
                            }
                        }

                        function GetActiveIDs() {
                            var testIds = [];
                            for (var key in _apm_test_guids_started) {
                                if (typeof(_apm_test_guids_started[key]) == 'string') testIds.push(key);
                            }
                            return testIds;
                        }

                        function OnGetFinishedJobIDsCompleted(result) {
                            if (result != null) {
                                for (var i = 0; i < result.length; i++) {
                                    var id = result[i];
                                    var fakeButtonId = _apm_test_guids_started[id];
                                    if (fakeButtonId) {
                                        var fakeButton = $get(fakeButtonId);
                                        if (fakeButton) fakeButton.value = (new Date()).getTime();
                                        __doPostBack(fakeButtonId, '');
                                    }
                                    delete _apm_test_guids_started[id];
                                }
                            }

                            if (GetActiveIDs().length > 0) {
                                setTimeout(""GetFinishedJobIDs()"", 5000);
                            }
                        }                   

                        setTimeout(""GetFinishedJobIDs()"", 5000);
                    ";
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "TestTimerStart", scr, true);
                if (registerScriptForAsyncPostBack)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, GetType(), "TestTimerStart", scr, true);
                }
            }

            string scriptId = "TestIdEntry" + TestResult.TestId;
            if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptId))
            {
                string scr = string.Format("_apm_test_guids_started['{0}'] = '{1}';",
                     TestResult.TestId, fakeButton.ClientID);
                Page.ClientScript.RegisterClientScriptBlock(GetType(), scriptId, scr, true);
                if (registerScriptForAsyncPostBack)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, GetType(), scriptId, scr, true);
                }
            }
        }

        base.OnPreRender(e);
    }

	protected void RefreshResults(object sender, EventArgs e)
	{
		ComponentTestResult res = TestResult;
		if (!res.TestComplete) res.LoadResults();
		UpdateControls();
		DataBind();
	}

	private void UpdateControls()
	{
		ComponentTestResult res = TestResult;
		result.Visible = res.TestStarted;
		messages.Visible = res.TestStarted;
	}

	protected override void OnDataBinding(EventArgs e)
	{
		base.OnDataBinding(e);

		img.Visible = false;

		ComponentTestResult testResult = TestResult;
		if (testResult != null && testResult.TestStarted)
		{
			string statusMessage;
			img.Visible = true;

			if (testResult.Skipped)
			{
                statusMessage = Resources.APMWebContent.APMWEBCODE_VB1_109;
				messages.Visible = false;
				img.ImageUrl = "~/Orion/APM/Images/Admin/skipped_16x16.gif";
                statusDiv.Attributes["class"] = "apm_Test_Skipped";

                result.Text = string.Format(Resources.APMWebContent.APMWEBCODE_AK1_102);
			}
			else
			{
				if (!testResult.TestComplete)
				{
                    statusMessage = Resources.APMWebContent.APMWEBCODE_VB1_108;
					messages.Visible = false;
					img.ImageUrl = "~/Orion/APM/Images/Admin/animated_loading_16x16.gif";
                    statusDiv.Attributes["class"] = "apm_Test_Pending";
				}
				else
				{
					DataSet ds = testResult.Result;
					DataTable tResult = ds.Tables["Result"];
                    DataTable tMultiStatisticValues = ds.Tables["MultiStatisticValues"];
					bool success = tResult != null && (bool)tResult.Rows[0]["Success"];
					string msg;
					if (success)
					{
                        msg = Resources.APMWebContent.APMWEBCODE_VB1_110;
						img.ImageUrl = "~/Orion/APM/Images/Admin/icon_check2.gif";
                        statusDiv.Attributes["class"] = "apm_Test_Success";
					}
					else
					{
                        msg = Resources.APMWebContent.APMWEBCODE_VB1_111;
						img.ImageUrl = "~/Orion/APM/Images/Admin/failed_16x16.gif";
                        statusDiv.Attributes["class"] = "apm_Test_Failure";
					}

				    Status outcome;
                    if (tResult != null)
                    {
                        outcome = (Status) Enum.Parse(typeof (Status), tResult.Rows[0]["Status"].ToString());
                    }
                    else
                    {
                        outcome = Status.Undefined;
                    }
                    var outcomeDesc = (new SolarWinds.APM.Web.DisplayTypes.ApmStatus(outcome)).ToLocalizedString();
                    if (String.IsNullOrEmpty(outcomeDesc))
                    {
                        outcomeDesc = (new SolarWinds.APM.Web.DisplayTypes.ApmStatus(outcome)).ToLocalizedString();
                    }
                    statusMessage = string.Format(Resources.APMWebContent.APMWEBCODE_VB1_107, msg, outcomeDesc);

                    if(!ds.Tables.Contains("Message"))
                    {
                        var table = ds.Tables.Add("Message");
                        table.Columns.Add("Msg");
                    }

					if (!success && (ds.Tables["Message"].Rows.Count == 0))
					{
                        if (tResult != null && tResult.Rows[0]["ErrorCode"] != DBNull.Value)
                        {
                            ApmErrorCode errorCode =
                                (ApmErrorCode)
                                Enum.Parse(typeof (ApmErrorCode), tResult.Rows[0]["ErrorCode"].ToString());

                            DataRow dr = ds.Tables["Message"].NewRow();
                            dr[0] = EnumUtil.GetLocalizedDescription(errorCode);
                            ds.Tables["Message"].Rows.Add(dr);
                        }
                        else if (ds.Tables.Contains("Error"))
                        {
                            foreach (DataRow errRow in ds.Tables["Error"].Select())
                            {
                                ds.Tables["Message"].Rows.Add(errRow["Message"]);
                            }
                        }
					}

					var dataSource = ds.Tables["Message"];
					if (dataSource != null && dataSource.Rows.Count > 0) 
					{
						dataSource.DefaultView.RowFilter = "Msg IS NOT NULL AND Msg <> ''";
						dataSource = dataSource.DefaultView.ToTable(true);
					}

					messages.Visible = true;
					messages.DataSource = dataSource;
					messages.DataBind();

                    hfStatisticValue.Value = String.Empty;
                    hfTestSuccessful.Value = success.ToString().ToLowerInvariant();

                    if (success)
                    {
                        if (Convert.ToBoolean(tResult.Rows[0]["HasStatisticValue"]))
                        {
                            hfStatisticValue.Value = tResult.Rows[0]["StatisticValue"].ToString();
                        }
                        else if (tMultiStatisticValues.Rows.Count > 0)
                        {
                            List<Pair> list = new List<Pair>();
                            foreach (DataRow row in tMultiStatisticValues.Rows)
                            {
                                list.Add(new Pair(row["Name"], row["Statistic"]));
                            }
                            hfStatisticValue.Value = JsonSerialize<List<Pair>>(list).Replace("\"", "\\\"");
                        }
                    }
                }

				var nodeName = testResult.NodeName;
				if (string.IsNullOrEmpty(nodeName) && testResult.Node != null)
				{
					nodeName = testResult.Node.IpAddress;
				}
                result.Text = string.Format(Resources.APMWebContent.APMWEBCODE_VB1_106, nodeName, statusMessage);
			}
		}
	}

    private string JsonSerialize<T>(T graph)
    {
        string result = String.Empty;
        using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
        {
            System.Runtime.Serialization.Json.DataContractJsonSerializer jsc = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
            jsc.WriteObject(stream, graph);
            stream.Flush();
            stream.Position = 0;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }
        }
        return result;
    }
}