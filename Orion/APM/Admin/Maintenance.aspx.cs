﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models;
using System.Collections.Generic;
using System.Text;
using SolarWinds.APM.Common.Utility;

public partial class Orion_APM_Admin_Maintenance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = InvariantString.Format(Resources.APMWebContent.APMWEBCODE_TM0_1, ApmConstants.ModuleShortName);
        MoreInfoHyperLink.NavigateUrl = HelpLocator.CreateHelpUrl("OrionAPMAGDatabaseManagerCreatingDatabaseMaintenancePlan");

        if (!IsPostBack)
        {
            List<MaintenanceInfo> maintenanceInfoList = LoadData();

            listOfTables.InnerHtml = GetOverdueTableDescription(maintenanceInfoList);
        }
    }

    private List<MaintenanceInfo> LoadData()
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            return businessLayer.GetMaintenanceInfoList();
        }
    }

    public string GetOverdueTableDescription(List<MaintenanceInfo> maintenanceInfoList)
    {
        StringBuilder innerHtml = new StringBuilder();

        if(maintenanceInfoList != null && maintenanceInfoList.Count > 0)
        {
            string tableName = string.Empty;

            innerHtml.AppendLine("<UL>");

            foreach (MaintenanceInfo maintenanceInfo in maintenanceInfoList)
            {
                DateTime lastDate = maintenanceInfo.StatusLastestEntry;
                DateTime retainDate = maintenanceInfo.StatusRetainDate;

                TimeSpan overdue = retainDate - lastDate;

                innerHtml.AppendLine(string.Format("<LI>" + (overdue.Days > 1 ? Resources.APMWebContent.APMWEBCODE_TM0_8 : Resources.APMWebContent.APMWEBCODE_TM0_9) + "</LI>", GetTableDescription(maintenanceInfo.StatisticTable), overdue.Days));
            }

            innerHtml.AppendLine("</UL>");

        }
        return innerHtml.ToString();
    }

    private string GetTableDescription(HistoricStatisticTable tableObject)
    {
        string tableName = string.Empty;

        switch (tableObject)
        {
            case HistoricStatisticTable.ApplicationStatusDaily:
                tableName = Resources.APMWebContent.APMWEBCODE_TM0_2;
                break;
            case HistoricStatisticTable.ApplicationStatusDetail:
                tableName = Resources.APMWebContent.APMWEBCODE_TM0_3;
                break;
            case HistoricStatisticTable.ApplicationStatusHourly:
                tableName = Resources.APMWebContent.APMWEBCODE_TM0_4;
                break;
            case HistoricStatisticTable.ComponentStatusDaily:
                tableName = Resources.APMWebContent.APMWEBCODE_TM0_5;
                break;
            case HistoricStatisticTable.ComponentStatusDetail:
                tableName = Resources.APMWebContent.APMWEBCODE_TM0_6;
                break;
            case HistoricStatisticTable.ComponentStatusHourly:
                tableName = Resources.APMWebContent.APMWEBCODE_TM0_7;
                break;
        }

        return tableName; 
    }
}