﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_EditTags : System.Web.UI.Page
{

	#region Property

	private IList<Int32> TemplateIDs 
    {
        get 
        {
			if (this.Session["OAPMAET_tplIDs"] == null) 
            {
				if (string.IsNullOrEmpty(Request.Form["postData"]))
				{
					this.OnCancelButton_Click(null, null);
					return new List<Int32>();
				}
				this.Session["OAPMAET_tplIDs"] = new JavaScriptSerializer().Deserialize<IList<Int32>>(
					this.Server.UrlDecode(Request.Form["postData"])
				);
            }
			return this.Session["OAPMAET_tplIDs"] as IList<Int32>;
        }
    }

    #endregion

    #region Event Hendlers

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
			this.textBoxAddTags.Attributes["onfocus"] = String.Format(
				"javascript:var el=document.getElementById('{0}');el?el.style.display='none':null;", this.tagValidateMessage.ClientID
			);
            
			this.bindControls();
            this.OnRadioButtonTags_CheckedChanged(null, null);
        }
    }

    protected void OnRadioButtonTags_CheckedChanged(Object sender, EventArgs e)
    {
        this.contentExistingTags.Visible = this.radioButtonExistingTags.Checked;
        this.contentAddTags.Visible = this.radioButtonAddTags.Checked;
        this.contentRemoveTags.Visible = this.radioButtonRemoveTags.Checked;
    }

    protected void OnSubmitButton_Click(Object sender, EventArgs e) 
    {
		if (this.isValidNewTags())
		{
            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				businessLayer.DeleteTags(this.getSelectedTags(this.repeaterRemoveTags));
				businessLayer.InsertTags(this.getSelectedTags(this.repeaterExistingTags));
				businessLayer.InsertTags(this.getNewTags());
			}
			this.OnCancelButton_Click(null, null);
		}
		else 
		{
			this.tagValidateMessage.Visible = true;

			this.radioButtonExistingTags.Checked = false;
			this.radioButtonRemoveTags.Checked = false;
			this.radioButtonAddTags.Checked = true;
			this.OnRadioButtonTags_CheckedChanged(null, null);
		}
    }
    
    protected void OnCancelButton_Click(Object sender, EventArgs e) 
    {
		this.Session["OAPMAET_tplIDs"] = null;

		this.Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
    }

    #endregion

    #region Helper Members

    private void bindControls() 
    {
        using (IAPMBusinessLayer apmBL = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {            
            List<TagInfo> removeTags = new List<TagInfo>();
            foreach (Int32 templateID in this.TemplateIDs)
            {
				foreach (TagInfo tag in apmBL.GetTags(templateID, null))
                {
                    if (removeTags.FindIndex(delegate(TagInfo item) { return (tag.Name == item.Name); }) < 0)
                    {
                        removeTags.Add(tag);
                    }
                }
				this.labelTemplateNames.Text = String.Format("{0} {1},", this.labelTemplateNames.Text, HttpUtility.HtmlEncode(apmBL.GetApplicationTemplate(templateID).Name));
            }
			this.repeaterExistingTags.DataSource = apmBL.GetTags(-1, null);
            this.repeaterExistingTags.DataBind();

			this.contentRemoveTagsNA.Visible = removeTags.Count < 1;
			this.repeaterRemoveTags.DataSource = removeTags;
            this.repeaterRemoveTags.DataBind();

            this.labelTemplateNames.Text = this.labelTemplateNames.Text.TrimEnd(new Char[] { ',' });
            this.labelTemplateNames.Text = String.Format(Resources.APMWebContent.APMWEBDATA_AK1_58, this.labelTemplateNames.Text);
        }
    }

    private List<TagInfo> getSelectedTags(Repeater repeater)
    {
        List<TagInfo> selItems = new List<TagInfo>();
        foreach (RepeaterItem item in repeater.Items)
        {
            CheckBox cbSelected = item.FindControl("tagSelected") as CheckBox;
            if (cbSelected != null && cbSelected.Checked)
            {
                HtmlInputHidden ihName = item.FindControl("tagName") as HtmlInputHidden;
                if (ihName != null)
                {
                    foreach (Int32 templateID in this.TemplateIDs)
                    {
                        selItems.Add(new TagInfo(templateID, ihName.Value));
                    }
                }
            }
        }
        return selItems;
    }

	private IList<TagInfo> getNewTags()
    {
		List<TagInfo> newTags = new List<TagInfo>();
		foreach (String name in this.textBoxAddTags.Text.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
		{
			String vName = name.Trim();
			if (vName.Length > 0) 
			{
				foreach (Int32 templateID in this.TemplateIDs)
				{
					newTags.Add(new TagInfo(templateID, vName.Length > 100 ? vName.Substring(0, 100) : vName));
				}
			}
		}
		return newTags;
	}

	private Boolean isValidNewTags() 
	{
		String tags = this.textBoxAddTags.Text.Trim();
		if (tags.Length == 0) 
		{
			return true;
		}
		return new List<String>(tags.Split(new Char[] { ',' }))
			.FindIndex(delegate(String item) { return item.Trim().Length == 0; }) < 0;
	}

    #endregion

}
