﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';

    function templateAssignmentsDialog(templateId, templateName, onChangeHandler) {
        var encodedTemplateName = _.escape(templateName);
        var mDlg = null,
            tabPnl = null,
            applicationMonitorsGrid = null,
            groupsGrid = null,
            unassignedNodesGrid = null,
            applicationMonitorsTabId = "amp_appMonitorsTab_" + Ext.id(),
            groupsTabId = "amp_groupsTab" + Ext.id(),
            unassignedNodesTabId = "amp_unassignedNodesTab" + Ext.id(),
            isChangeOccured = false;

        this.show = function () {
            applicationMonitorsGrid = new SW.APM.TemplateGroupAssignment.ApplicationMonitorsGrid(templateId);
            groupsGrid = new SW.APM.TemplateGroupAssignment.GroupsGrid(templateId, encodedTemplateName);
            unassignedNodesGrid = new SW.APM.TemplateGroupAssignment.UnassignedNodesGrid(templateId);

            var tabPanel = new Ext.TabPanel({
                width: 618,
                height: 347,
                activeTab: 0,
                items: [
                    {
                        id: applicationMonitorsTabId,
                        title: String.format('@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_AppMonitors;E=js}', '…'),
                        items: [applicationMonitorsGrid.getControl()]
                    },
                    {
                        id: groupsTabId,
                        title: String.format('@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_Groups;E=js}', '…'),
                        items: [groupsGrid.getControl()]
                    },
                    {
                        id: unassignedNodesTabId,
                        title: String.format('@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_UnassignedNodes;E=js}', '…'),
                        items: [unassignedNodesGrid.getControl()]
                    }
                ],
                renderTo: Ext.getBody()
            });
            
            tabPnl = tabPanel;

            applicationMonitorsGrid.setOnLoadHandler(function () {
                tabPanel.setTabTitle(0, String.format('@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_AppMonitors;E=js}', applicationMonitorsGrid.getTotalCount()));
            });
            groupsGrid.setOnLoadHandler(function () {
                tabPanel.setTabTitle(1, String.format('@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_Groups;E=js}', groupsGrid.getTotalCount()));
            });
            unassignedNodesGrid.setOnLoadHandler(function () {
                tabPanel.setTabTitle(2, String.format('@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_UnassignedNodes;E=js}', unassignedNodesGrid.getTotalCount()));
            });

            groupsGrid.setOnChangeHandler(function () {
                applicationMonitorsGrid.refresh();
                unassignedNodesGrid.refresh();
                isChangeOccured = true;
            });
            unassignedNodesGrid.setOnChangeHandler(function () {
                applicationMonitorsGrid.refresh();
                groupsGrid.refresh();
                isChangeOccured = true;
            });

            var dlgConfig = {
                id: "apm_templateAssignments_" + Ext.id(),
                title: "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_Title;E=js}",
                width: 650,
                height: 460,
                minHeight: 460,
                border: false,
                region: "center",
                resizable: true,
                resizeHandles: 'n s',
                modal: true,
                constrain: true,
                constrainHeader: true,
                items: [
                    {
                        region: "head",
                        padding: 10,
                        border: false,
                        items: [
                            {
                                xtype: "label",
                                html: String.format("@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentsDialog_AssignmentsFor;E=js}", encodedTemplateName),
                                style: "text-transform: none; font-weight: normal;"
                            }
                        ]
                    },
                    {
                        region: "bottom",
                        style: "padding: 5px 10px 10px 10px",
                        border: false,
                        items: [tabPanel]
                    }
                ],
                buttons: [
                    { text: "@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}", handler: onClose }
                ],
                listeners: {
                    close: onClose,
                    resize: onResize,
                    scope: this
                }
            };

            mDlg = new Ext.Window(dlgConfig);
            mDlg.show();
        }

        /* Tab Panel extension */

        Ext.override(Ext.TabPanel, {
            setTabTitle: function (index, title) {
                var tabEl = this.getTabEl(index);
                if (index >= 0 && !Ext.isEmpty(tabEl)) {
                    Ext.query('.x-tab-strip-text', tabEl)[0].innerHTML = this.items.items[index].title = title;
                }
            }
        });

        /* Main dialog handlers */

        function onClose() {
            mDlg.hide();
            mDlg.destroy();
            mDlg = null;

            if (isChangeOccured) {
                onChangeHandler();
            }
        };
        
        function onResize(control, newWidth, newHeight) {
            var windowToGridPannelDiff = control.height - tabPnl.height;      // window height - grid pannel height
            var windowToGridDiff =
                control.height - applicationMonitorsGrid.getControl().height; // window height - grid height
                                                                              // these are heights of static areas above and under grid/grid panel

            var appMonGridWidth = applicationMonitorsGrid.getControl().width;
            var groupsGridWidth = groupsGrid.getControl().width;
            var nodesGridWidth = unassignedNodesGrid.getControl().width;

            var newGridPannelHeight = newHeight - windowToGridPannelDiff;
            var newGridHeight = newHeight - windowToGridDiff;
            
            //setting new height...
            
            //for tab panel
            tabPnl.setSize(undefined, newGridPannelHeight);

            //for grids
            applicationMonitorsGrid.getControl().setSize(appMonGridWidth, newGridHeight);
            groupsGrid.getControl().setSize(groupsGridWidth, newGridHeight);
            unassignedNodesGrid.getControl().setSize(nodesGridWidth, newGridHeight);

            //setting new page size
            setPageSizeForGrids(Math.floor((newGridHeight - 72) / 29)); // 72 - padding + grid header + grid paging bar | 29 - height of grid row
        }
        
        function setPageSizeForGrids(newPageSize) {
            //update page size
            applicationMonitorsGrid.getControl().getBottomToolbar().pageSize =
            groupsGrid.getControl().getBottomToolbar().pageSize =
            unassignedNodesGrid.getControl().getBottomToolbar().pageSize = newPageSize;

            //reset cursor
            applicationMonitorsGrid.getControl().getBottomToolbar().cursor = 
            groupsGrid.getControl().getBottomToolbar().cursor =
            unassignedNodesGrid.getControl().getBottomToolbar().cursor = 0;

            //refresh grids
            applicationMonitorsGrid.getControl().getBottomToolbar().doRefresh();
            groupsGrid.getControl().getBottomToolbar().doRefresh();
            unassignedNodesGrid.getControl().getBottomToolbar().doRefresh();
        }
    }

    module.TemplateAssignmentsDialog = templateAssignmentsDialog;
});