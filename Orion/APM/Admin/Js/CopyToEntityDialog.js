﻿SW.APM.CopyToApplicationDialog = function () {
	var mThis, mDefGroup, dlg, entityIDs, config, TreeLoader;
}
SW.APM.CopyToApplicationDialog.getInstance = function (config) {
	var obj = null;
	if (config.type == "app") { (obj = new SW.APM.CopyComponents()).init(config); }
	if (config.type == "tpl") { (obj = new SW.APM.CopyTemplates()).init(config); }
	return obj;
}
with ($p = SW.APM.CopyToApplicationDialog.prototype) {

	$p.loadEntityGroupByValues = function () {
		throw "Member Not Implement";
	}
	$p.copyEntity = function () {
		throw "Member Not Implement";
	}
	$p.getGroupValues = function () {
		throw "Member Not Implement";
	}
	$p.getConfig = function () {
		throw "Member Not Implement";
	}
	$p.getKey = function () {
		throw "Member Not Implement";
	}

	$p.onClose = function () {
		dlg.hide(); dlg.destroy(); dlg = null;
	};
	$p.onSubmit = function () {
		var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_VB1_158;E=sql}");
		var onSuccess = function (message) { waitMsg.hide(); Ext.Msg.alert('@{R=APM.Strings;K=APMWEBJS_TM0_26;E=sql}', message, function () { if (config.updateHandler) { config.updateHandler(); } }); };
		mThis.copyEntity(config.cmpIDs, config.tplIDs, entityIDs, onSuccess);
		mThis.onClose();
	};
	$p.onNodeExpand = function (node) {
		if (node.isExpanded()) { node.collapse(); } else { node.expand(); }
	}
	$p.onNodeClick = function (node) {
		var cb = node.ui.checkbox; cb.checked = !cb.checked; mThis.onNodeCheck(node);
	}
	$p.onNodeCheck = function (node) {
		var cb = node.ui.checkbox;
		if (cb.checked) { entityIDs.push(node.attributes.appID); } else { entityIDs.remove(node.attributes.appID); }
		dlg.buttons[0].setDisabled(entityIDs.length < 1);
	}
	$p.onGroupSelect = function (combo, record, index) {
		entityIDs = []; dlg.buttons[0].setDisabled(true);
		dlg.getComponent("tree").getLoader().load(record.data.groupBy);

		ORION.Prefs.save(mThis.getKey(), record.data.groupBy);
	}
	$p.onBind = function (groupBy, json) {
		eval("var nodes=" + json + ";");
		var tree = dlg.getComponent("tree"), root = tree.getRootNode(), loader = tree.getLoader();
		for (var i = root.childNodes.length - 1; i > -1; i--) { root.childNodes[i].remove(); }
		for (var i = 0, iLen = nodes.length; i < iLen; i++) {
			var parent = root, children = nodes[i].children;
			if (groupBy != "''") {
				parent = loader.createNode({
					text: nodes[i].text, icon: nodes[i].icon,
					nodeType: "node", iconCls: "apm_StatusIcon", draggable: false, leaf: false, listeners: { click: mThis.onNodeExpand }
				});
				root.appendChild(parent);
			}
			for (var j = 0, jLen = children.length; j < jLen; j++) {
				parent.appendChild(
						loader.createNode({
							appID: children[j].id, text: children[j].text, icon: children[j].icon,
							nodeType: "node", iconCls: "apm_StatusIcon", draggable: false, leaf: true, checked: false, listeners: { click: mThis.onNodeClick, checkchange: mThis.onNodeCheck }
						})
					);
			}
		}
		tree.render();
		root.expand();
	}
	$p.onLoad = function (groupBy) {
		var group = (groupBy && groupBy.constructor == String ? groupBy : mDefGroup);
		mThis.loadEntityGroupByValues(group, function (json) { mThis.onBind(group, json); });
	}

	$p.show = function () {
		var dlgConfig = {
			id: "apm_SelectAppDialog", title: "@{R=APM.Strings;K=APMWEBJS_VB1_123;E=js}",
			width: parseInt(Ext.getBody().getWidth() * 0.5), height: parseInt(Ext.getBody().getHeight() * 0.6),
			border: false, region: "center", layout: "fit", modal: true,
			tbar: [
					"@{R=APM.Strings;K=APMWEBJS_VB1_122;E=js}",
					new Ext.form.ComboBox({
						id: "ctrCopyGroupBy", displayField: "displayText", mode: "local", triggerAction: "all", emptyText: "@{R=APM.Strings;K=APMWEBJS_VB1_113;E=js}", width: 200, typeAhead: true, editable: false, selectOnFocus: true,
						store: new Ext.data.SimpleStore({
							fields: ["groupBy", "displayText"],
							data: mThis.getGroupValues()
						}),
						listeners: { select: mThis.onGroupSelect }
					})
				],
			items: [
					new Ext.tree.TreePanel({
						id: "tree", autoScroll: true, animate: false, containerScroll: true, rootVisible: false, style: 'margin:3px 0px 3px 0px;',
						loader: new TreeLoader(), root: new Ext.tree.AsyncTreeNode({ id: "source", text: "text", draggable: false })
					})
				],
			buttons: [
					{ text: "@{R=APM.Strings;K=APMWEBJS_VB1_38;E=js}", disabled: true, handler: mThis.onSubmit },
					{ text: "@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}", handler: mThis.onClose }
				]
		};
		Ext.apply(dlgConfig, mThis.getConfig());

		dlg = new Ext.Window(dlgConfig);
		dlg.show();
		for (var i = 0, items = mThis.getGroupValues(); i < items.length; i++) {
			if (items[i][0] == mDefGroup) {
				Ext.getCmp("ctrCopyGroupBy").setValue(items[i][1]);
			}
		}
	}

	$p.init = function (pConfig) {
		mThis = this;
		mDefGroup = ORION.Prefs.load(mThis.getKey(), "''");
		dlg = null; entityIDs = []; config = pConfig;
		TreeLoader = Ext.extend(Ext.tree.TreeLoader, { load: mThis.onLoad });
	}
}

SW.APM.CopyComponents = function () { }
with (SW.APM.CopyComponents.prototype = new SW.APM.CopyToApplicationDialog, $p = SW.APM.CopyComponents.prototype) {

	$p.loadEntityGroupByValues = function (groupBy, onSuccess) {
		ORION.callWebService("/Orion/APM/Services/Applications.asmx", "GetApplicationGroupByValues", { "groupBy": groupBy }, onSuccess);
	};
	$p.copyEntity = function (componentIDs, templateIDs, applicationIDs, onSuccess) {
		ORION.callWebService("/Orion/APM/Services/Components.asmx", "CopyToApplication", { "componentIDs": componentIDs, "templateIDs": templateIDs, "applicationIDs": applicationIDs }, onSuccess);
	};
	$p.getGroupValues = function () {
		return [
				["''", "@{R=APM.Strings;K=APMWEBJS_VB1_113;E=js}"],
				["at.Name", "@{R=APM.Strings;K=APMWEBJS_TM0_25;E=js}"],
				["n.Caption", "@{R=APM.Strings;K=APMWEBJS_VB1_114;E=js}"],
				["n.Vendor", "@{R=APM.Strings;K=APMWEBJS_VB1_115;E=js}"],
				["n.MachineType", "@{R=APM.Strings;K=APMWEBJS_VB1_116;E=js}"],
				["n.Contact", "@{R=APM.Strings;K=APMWEBJS_VB1_117;E=js}"],
				["n.Location", "@{R=APM.Strings;K=APMWEBJS_VB1_118;E=js}"],
				["n.ObjectSubType", "@{R=APM.Strings;K=APMWEBJS_VB1_119;E=js}"],
				["n.SysObjectID", "@{R=APM.Strings;K=APMWEBJS_VB1_120;E=js}"]
			];
	};
	$p.getConfig = function () {
		return { title: "@{R=APM.Strings;K=APMWEBJS_VB1_121;E=js}" };
	};
	$p.getKey = function () {
		return "CopyCMPGroupBy";
	};
}

SW.APM.CopyTemplates = function () { }
with (SW.APM.CopyTemplates.prototype = new SW.APM.CopyToApplicationDialog, $p = SW.APM.CopyTemplates.prototype) {

	$p.loadEntityGroupByValues = function (groupBy, onSuccess) {
		ORION.callWebService("/Orion/APM/Services/Applications.asmx", "GetApplicationTemplateGroupByValues", { "groupBy": groupBy }, onSuccess);
	};
	$p.copyEntity = function (componentIDs, templateIDs, applicationIDs, onSuccess) {
		ORION.callWebService("/Orion/APM/Services/Components.asmx", "CopyToApplicationTemplate", { "componentIDs": componentIDs, "templateIDs": templateIDs, "applicationIDs": applicationIDs }, onSuccess);
	};
	$p.getGroupValues = function () {
		return [
				["''", "@{R=APM.Strings;K=APMWEBJS_VB1_113;E=js}"],
				["t.TagName", "@{R=APM.Strings;K=APMWEBJS_VB1_124;E=js}"]
			];
	};
	$p.getConfig = function () {
		return { title: "@{R=APM.Strings;K=APMLIBCODE_TM0_1;E=js}" };
	};
	$p.getKey = function () {
		return "CopyTPLGroupBy";
	};
}
