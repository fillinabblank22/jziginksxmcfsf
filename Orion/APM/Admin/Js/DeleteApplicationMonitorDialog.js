﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';
    
    function deleteApplicatonMonitorDialog(selectedAppsWithGroupsInfo, selectedAppsCount, deleteApplications) {
        var mDlg = null;
        var headerText, messageText, bottomText;

        //removing one application assigned to one group
        if (selectedAppsCount == 1) {
            headerText = "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageHeader;E=js}";

            messageText = String.format(
                "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageSingleApp;E=js}",
                selectedAppsWithGroupsInfo[0].groupName,
                "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageTip;E=js}");

            bottomText = String.format(
                "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_ConfirmationTextSingleApp;E=js}",
                selectedAppsWithGroupsInfo[0].nodeName);
        } else {
            bottomText = "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_ConfirmationTextApps;E=js}";

            //removing several applications assigned to one group
            if (allAppsFromSameGroup(selectedAppsWithGroupsInfo) && (selectedAppsWithGroupsInfo.length == selectedAppsCount)) {
                headerText = "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageHeaderApps;E=js}";

                messageText = String.format(
                    "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageApps;E=js}",
                    selectedAppsWithGroupsInfo[0].groupName,
                    "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageTip;E=js}");

            } else {
                headerText = "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageHeaderAppsAndGroups;E=js}";

                //removing set of applications that contains application(s) not assigned to any group(s)
                if (selectedAppsWithGroupsInfo.length != selectedAppsCount) {
                    messageText = String.format(
                        "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageMixAppsAndGroups;E=js}",
                        "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageTipGroups;E=js}");
                    //removing several applications assigned to different groups
                } else {
                    messageText = String.format(
                        "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageAppsAndGroups;E=js}",
                        "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_MessageTipGroups;E=js}");
                }
            }
        }
        
        this.show = function() {
            var dlgConfig = {
                id: "apm_deleteApplicationMonitor",
                title: "@{R=APM.Strings;K=APMWEBJS_DeleteApplicationMonitorDlg_Header;E=js}",
                width: 530,
                height: 280,
                border: false,
                autoScroll: true,
                region: "center",
                resizable: false,
                modal: true,
                items: [
                    {
                        region: "message",
                        padding: 15,
                        border: false,
                        autoScroll: true,
                        items: [
                            {
                                xtype: "label",
                                html: String.format(
                                    "<img src=\"/orion/images/NotificationImages/notification_warning.gif\" style=\"float:left;margin-right:6px;height:25px;width:25px;\">" +
                                    "<p style=\"margin-top:5px;\"><b>{0}</b></p>",
                                    headerText),
                                style: "text-transform: none;"
                            },
                            {
                                xtype: "label",
                                html: String.format("<p style=\"margin-top: 10px; margin-left: 30px;\">{0}</p>", messageText),
                                style: "text-transform: none; font-weight: normal;"
                            },
                            {
                                xtype: "label",
                                html: String.format("<p style=\"margin-left: 30px;margin-top: 60px;\">{0}</p>", bottomText),
                                style: "text-transform: none; font-weight: normal;"
                            }
                        ]
                    }
                ],
                buttons: [
                    { text: "@{R=APM.Strings;K=MonitorSettings_Yes;E=js}", handler: onSubmit },
                    { text: "@{R=APM.Strings;K=MonitorSettings_No;E=js}", handler: onClose }
                ]
            };

            mDlg = new Ext.Window(dlgConfig);
            mDlg.show();
        }
        
        /* Main dialog handlers */

        function onSubmit() {
            deleteApplications();
            onClose();
        };

        function onClose() {
            mDlg.hide();
            mDlg.destroy();
            mDlg = null;
        };

        /* Helper functions */

        function allAppsFromSameGroup(selAppsInfo) {
            return selAppsInfo.every(function (s) {
                return s.groupName === selAppsInfo[0].groupName;
            });
        };
    }

    module.DeleteApplicatonMonitorDialog = deleteApplicatonMonitorDialog;
});