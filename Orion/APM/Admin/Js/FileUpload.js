/*jslint browser: true*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.FileUpload', function (module, APMjs) {
    'use strict';

    // *** uploadFile
    // usage:
    //
    // uploadFile({
    //     url: '/Orion/APM/Admin/EditCertificateSetFileUploader.ashx',
    //     secureuri: false,
    //     fileElementId: fileInputElement.id,
    //     dataType: 'text',
    //     data: { name: 'logan', id: 'id' },
    //     success: function (data, status) {
    //         var resMsg = $(data).html();
    //         if (resMsg.substring(0, 5) == "Error") {
    //             var errorMsg = $(data).html().substring(6);
    //             ErrorMsgBox("Error", errorMsg);
    //         } else {
    //             $('#' + fileContentElementId).val($(data).html());
    //             $('#' + fileContentElementId).focus();
    //             //clear input file
    //             $(containerElement).html($(containerElement).html());
    //         }
    //     }
    // });
    //
    // *** uploadFile

    var $ = APMjs.assertQuery(),
        SW = APMjs.assertGlobal('SW');

    function uploadFile(req) {
        var id, em, formData, jForm, newElement, uploadingFrameHtml, requestDone, xmlRequestObject;

        req = $().extend({}, $().ajaxSettings, req);
        id = new Date().getTime();
        em = SW.APM.EventManager;
        formData = req.data;

        jForm = $('<form  action="" method="POST" name="uploadingForm' + id + '" id="uploadingForm' + id + '" enctype="multipart/form-data"style="position:absolute; top:-1200px; left:-1200px" ></form>');
        if (formData) {
            APMjs.linqEach(formData, function (value, key) {
                $('<input type="hidden" name="' + key + '" value="' + value + '" />').appendTo(jForm);
            });
        }

        newElement = $('#' + req.fileElementId).clone();
        $('#' + req.fileElementId)
            .attr('id', 'uploadingFile' + id)
            .before(newElement)
            .appendTo(jForm);
        $(jForm)
            .appendTo('body');

        uploadingFrameHtml = '<iframe id="uploadingFrame' + id + '" name="uploadingFrame' + id + '" style="position:absolute; top:-9999px; left:-9999px"';
        if (window.ActiveXObject) {
            uploadingFrameHtml += ' src="javascript:false"';
        }
        uploadingFrameHtml += ' />';
        $(uploadingFrameHtml).appendTo(document.body);

        if (req.global && !($().active++)) {
            em.fire('ajaxStart');
        }

        requestDone = false;
        xmlRequestObject = {};
        if (req.global) {
            em.fire('ajaxSend', [xmlRequestObject, req]);
        }

        function uploadCallback(isTimeout) {
            var frm, frmdoc, status, data;

            frm = $('#uploadingFrame' + id).get(0);
            frmdoc = frm.contentWindow.document;
            xmlRequestObject.responseText = frmdoc.body || null;
            xmlRequestObject.responseXML = frmdoc.XMLDocument || frmdoc;

            if (xmlRequestObject || isTimeout === 'timeout') {
                requestDone = true;
                status = (isTimeout !== 'timeout') ? 'success' : 'error';
                if (status !== 'error') {
                    data = !req.dataType;
                    data = (req.dataType === 'xml' || data) ? xmlRequestObject.responseXML : xmlRequestObject.responseText;
                    if (req.success) {
                        req.success(data, status);
                    }
                    if (req.global) {
                        em.fire('ajaxSuccess', [xmlRequestObject, req]);
                    }
                } else {
                    // jquery.handleError has been deprecated
                    if (APMjs.isFn($.handleError)) {
                        $.handleError(req, xmlRequestObject, status);
                    }
                }

                if (req.global) {
                    em.fire('ajaxComplete', [xmlRequestObject, req]);
                }
                if (req.global && (--($().active)) === 0) {
                    em.fire('ajaxStop');
                }
                if (req.complete) {
                    req.complete(xmlRequestObject, status);
                }

                $('#uploadingFrame' + id).eq(0).unbind();
                setTimeout(function () {
                    $('#uploadingFrame' + id).eq(0).remove();
                    jForm.remove();
                }, 100);
                xmlRequestObject = null;
            }
        }

        if (req.timeout > 0) {
            setTimeout(function () {
                if (!requestDone) {
                    uploadCallback('timeout');
                }
            }, req.timeout);
        }

        jForm = $('#uploadingForm' + id);
        jForm.attr({
            'action': req.url,
            'method': 'POST',
            'target': 'uploadingFrame' + id,
            'enctype': 'multipart/form-data'
        });

        jForm.submit();
        $('#uploadingFrame' + id).load(uploadCallback);
        return {
            abort: APMjs.noop
        };
    }

    module.uploadFile = uploadFile;
});
