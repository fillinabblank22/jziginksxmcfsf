﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';

    function applicationMonitorsGrid(templateId) {
        var mainObj = this,
            gridControl = null;
        
        var dataStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: "/api/ApmTemplateGroupAssignment/GetApplicationMonitorsData",
                method: "POST",
                failure: function (xhr) { ORION.handleError(xhr); }
            }),
            reader: new Ext.data.JsonReader(
                {
                    totalProperty: "TotalRows",
                    root: "DataTable.Rows"
                },
                [
                    { name: "ApplicationName", mapping: 0 },
                    { name: "NodeName", mapping: 1 },
                    { name: "GroupName", mapping: 2 },
                    { name: "ApplicationStatus", mapping: 3 },
                    { name: "NodeStatus", mapping: 4 },
                    { name: "GroupStatus", mapping: 5 },
                    { name: "ApplicationID", mapping: 6 },
                    { name: "NodeID", mapping: 7 },
                    { name: "ContainerID", mapping: 8 }
                ]
            ),
            listeners: {
                beforeload: onStoreBeforeLoad,
                load: onStoreLoad
            }
        });

        var gridConfig = {
            enableColumnMove: false,
            style: "padding: 10px;",
            height: 304,
            width: 617,
            border: false,
            stripeRows: true,
            loadMask: true,
            maskDisabled: false,
            trackMouseOver: false,
            sort: false,
            store: dataStore,
            cm: new Ext.grid.ColumnModel([
                {
                    header: "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_AppMonitor;E=js}",
                    dataIndex: "ApplicationName",
                    sortable: false,
                    menuDisabled: true,
                    width: 290,
                    renderer: renderAppNameColumn
                },
                {
                    header: "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_Nodes;E=js}",
                    dataIndex: "NodeName",
                    sortable: false,
                    menuDisabled: true,
                    width: 160,
                    renderer: renderAssignedNodeColumn
                },
                {
                    header: "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_Group;E=js}",
                    dataIndex: "GroupName",
                    sortable: false,
                    menuDisabled: true,
                    width: 139,
                    renderer: renderAssignedGroupColumn
                }
            ]),
            bbar: new Ext.PagingToolbar({
                pageSize: 8,
                store: dataStore,
                displayInfo: true
            })
        };
        
        gridControl = new Ext.grid.GridPanel(gridConfig);
        
        dataStore.load();
        
        /* Grid renderers */

        function renderAppNameColumn(value, meta, record) {
            var data = record.data;
            var img = String.format("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.APM.Application&amp;id={0}&amp;status={1}&amp;size=Small' />",
                data.ApplicationID,
                data.ApplicationStatus);

            var encodedTemplateName = _.escape(data.ApplicationName);
            var link = String.format("<a href='/Orion/View.aspx?NetObject=AA:{0}' target='_blank'>{1}</a>", data.ApplicationID, encodedTemplateName);
            return String.format("{0} {1}", img, link);
        }

        function renderAssignedNodeColumn(value, meta, record) {
            var data = record.data;
            var img = String.format("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;id={0}&amp;status={1}&amp;size=Small' />",
                data.NodeID,
                data.NodeStatus);

            var encodedNodeName = _.escape(data.NodeName);
            var link = String.format("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}' target='_blank'>{1}</a>", data.NodeID, encodedNodeName);
            return String.format("{0} {1}", img, link);
        }

        function renderAssignedGroupColumn(value, meta, record) {
            var data = record.data;
            var iconAndName = "";
            if (data.ContainerID) {

                var img = String.format("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.Container&amp;id={0}&amp;status={1}&amp;size=Small' />",
                    data.ContainerID,
                    data.GroupStatus);

                var encodedGroupName = _.escape(data.GroupName);
                var link = String.format("<a href='/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:{0}' target='_blank'>{1}</a>", data.ContainerID, encodedGroupName);
                iconAndName = String.format("{0} {1}", img, link);
            }
            return iconAndName;
        }
        
        /* Data Store handlers */

        function onStoreBeforeLoad(pStore, pOpt) {
            var start = pOpt.params.start, limit = pOpt.params.limit;

            if (!start || !limit) {
                start = 0;
                limit = gridControl.getBottomToolbar().pageSize;
            }

            pStore.proxy.conn.jsonData = { templateId: templateId, startRowNumber: start, rowsCount: limit };
        }

        function onStoreLoad() {
            mainObj.onLoadHandler();
        }

        /* Base properties */
        
        mainObj.control = gridControl;
        mainObj.onLoadHandler = function() {};
        mainObj.onChangeHandler = function() {};
    }

    applicationMonitorsGrid.prototype = new SW.APM.TemplateGroupAssignment.TemplateAssignmentsGrid();

    module.ApplicationMonitorsGrid = applicationMonitorsGrid;
});