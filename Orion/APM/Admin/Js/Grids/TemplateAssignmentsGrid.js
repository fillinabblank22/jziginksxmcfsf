﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.TemplateGroupAssignment', function(module, APMjs) {
    'use strict';

    function templateAssignmentsGrid() {
        this.getControl = function() {
            return this.control;
        };
        this.getTotalCount = function () {
            return this.control.getStore().getTotalCount();
        };
        this.refresh = function () {
            return this.control.getStore().reload();
        };
        this.setOnLoadHandler = function (handler) {
            if (Ext.isFunction(handler)) {
                this.onLoadHandler = handler;
            }
        };
        this.setOnChangeHandler = function (handler) {
            if (Ext.isFunction(handler)) {
                this.onChangeHandler = handler;
            }
        };
    };

    module.TemplateAssignmentsGrid = templateAssignmentsGrid;
});