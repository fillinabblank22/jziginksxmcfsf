﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';

    function groupsGrid(templateId, templateName) {
        var mainObj = this,
            gridControl = null;

        var dataStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: "/api/ApmTemplateGroupAssignment/GetGroupsData",
                method: "POST",
                failure: function (xhr) { ORION.handleError(xhr); }
            }),
            reader: new Ext.data.JsonReader(
                {
                    totalProperty: "TotalRows",
                    root: "DataTable.Rows"
                },
                [
                    { name: "GroupName", mapping: 0 },
                    { name: "GroupStatus", mapping: 1 },
                    { name: "NodesCount", mapping: 2 },
                    { name: "GroupID", mapping: 3 },
                    { name: "TemplateID", mapping: 4 },
                    { name: "ServersOnly", mapping: 5 }
                ]
            ),
            listeners: {
                beforeload: onStoreBeforeLoad,
                load: onStoreLoad
            }
        });

        var gridConfig = {
            style: "padding: 10px;",
            enableColumnMove: false,
            height: 304,
            width: 617,
            border: false,
            autoscroll: false,
            stripeRows: true,
            loadMask: true,
            maskDisabled: false,
            trackMouseOver: false,
            sort: false,
            store: dataStore,
            cm: new Ext.grid.ColumnModel([
                { header: "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_GroupCol;E=js}", dataIndex: "GroupName", sortable: false, menuDisabled: true, width: 130, renderer: renderGroupColumn },
                { header: "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_NoOfNodesAssignedCol;E=js}", dataIndex: "NodesCount", sortable: false, menuDisabled: true, width: 177, align: "right", renderer: renderNodesCount },
                { header: "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_AssignedTo;E=js}", dataIndex: "ServersOnly", sortable: false, menuDisabled: true, width: 80, align: "center", renderer: renderAssignToColumn },
                { header: "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_UnassignTemplateCol;E=js}", dataIndex: "GroupID", sortable: false, menuDisabled: true, width: 200, align: "center", renderer: renderUnassignLink }
            ]),
            bbar: new Ext.PagingToolbar({
                pageSize: 8,
                store: dataStore,
                displayInfo: true
            })
        };

        gridControl = new Ext.grid.GridPanel(gridConfig);

        dataStore.load();

        /* Grid renderers */

        function renderGroupColumn(value, meta, record) {
            var groupId = record.data.GroupID,
                groupStatus = record.data.GroupStatus,
                groupName = record.data.GroupName,
                imgHtmlFormat = "<span class='entityIconBox'><img src='/Orion/StatusIcon.ashx?entity=Orion.Groups&id={0}&status={1}&size=small' /></span>{2}",
                linkHtmlFormat = "<a href='/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:{0}' target='_blank'>{1}</a>";
            return groupId > 0
                ? String.format(imgHtmlFormat, groupId, groupStatus,
                    String.format(linkHtmlFormat, groupId, groupName))
                : "";
        }

        function renderNodesCount(value, meta, record) {
            var nodesCount = record.data.NodesCount;
            var nodesCountText = nodesCount == 1
                ? String.format("@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Node;E=js}", 1)
                : String.format("@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Nodes;E=js}", nodesCount);
            return String.format("<div style='padding-right:15px;'>{0}</div>", nodesCountText);
        }
        
        function renderAssignToColumn(value, meta, record) {
            var assignedToValue = record.data.ServersOnly ?
                "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_AssignedTo_ServersOnly;E=js}" :
                "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_AssignedTo_AllNodes;E=js}";
            return String.format("<div>{0}</div>", assignedToValue);
        }

        function renderUnassignLink(value, meta, record) {
            var groupId = record.data.GroupID,
                groupName = record.data.GroupName,
                appsCount = record.data.NodesCount,
                linkHtmlFormat = "<a href='#' id='{0}'>{1}</a>",
                linkId = Ext.id();

            $(String.format("#{0}", linkId)).live('click', function () {
                var changeStateCallback = function () {
                    gridControl.getStore().reload();
                    mainObj.onChangeHandler();
                };
                var unassignTemplateDialog = new SW.APM.TemplateGroupAssignment
                    .UnassignTemplateDialog(templateId,
                        groupId,
                        templateName,
                        groupName,
                        appsCount,
                        changeStateCallback);
                unassignTemplateDialog.show();
            });

            return groupId > 0
                ? String.format(linkHtmlFormat, linkId, "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_UnassignLink;E=js}")
                : "";
        }

        /* Data Store handlers */

        function onStoreBeforeLoad(pStore, pOpt) {
            var start = pOpt.params.start, limit = pOpt.params.limit;

            if (!start || !limit) {
                start = 0;
                limit = gridControl.getBottomToolbar().pageSize;
            }

            pStore.proxy.conn.jsonData = { templateId: templateId, startRowNumber: start, rowsCount: limit };
        }

        function onStoreLoad() {
            mainObj.onLoadHandler();
        }

        /* Base properties */

        mainObj.control = gridControl;
        mainObj.onLoadHandler = function () { };
        mainObj.onChangeHandler = function () { };
    };

    groupsGrid.prototype = new SW.APM.TemplateGroupAssignment.TemplateAssignmentsGrid();

    module.GroupsGrid = groupsGrid;
});