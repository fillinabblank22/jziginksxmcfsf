﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';
    
    function unassignedNodesGrid(templateId) {
        var mainObj = this,
            gridControl = null;

        var dataStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: "/api/ApmTemplateGroupAssignment/GetBlacklistData",
                method: "POST",
                failure: function (xhr) { ORION.handleError(xhr); }
            }),
            reader: new Ext.data.JsonReader(
                {
                    totalProperty: "TotalRows",
                    root: "DataTable.Rows"
                },
                [
                    { name: "NodeID", mapping: 0 },
                    { name: "NodeName", mapping: 1 },
                    { name: "NodeStatus", mapping: 2 },
                    { name: "GroupID", mapping: 3 },
                    { name: "GroupName", mapping: 4 },
                    { name: "GroupStatus", mapping: 5 }
                ]
            ),
            listeners: {
                beforeload: onStoreBeforeLoad,
                load: onStoreLoad
            }
        });
        
        var gridConfig = {
            enableColumnMove: false,
            style: "padding: 10px;",
            height: 304,
            width: 617,
            border: false,
            stripeRows: true,
            loadMask: true,
            maskDisabled: false,
            trackMouseOver: false,
            sort: false,
            store: dataStore,
            cm: new Ext.grid.ColumnModel([
                {
                    header: "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_UnassignedNode;E=js}",
                    dataIndex: "NodeName",
                    sortable: false,
                    menuDisabled: true,
                    width: 200,
                    renderer: renderNodeNameColumn
                },
                {
                    header: "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_UnassignedGroup;E=js}",
                    dataIndex: "GroupName",
                    sortable: false,
                    menuDisabled: true,
                    width: 180,
                    renderer: renderGroupNameColumn
                },
                {
                    header: "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_ReassignTemplate;E=js}",
                    sortable: false,
                    menuDisabled: true,
                    width: 209,
                    align: "center",
                    renderer: renderReassingLinkColumn
                }
            ]),
            bbar: new Ext.PagingToolbar({
                pageSize: 8,
                store: dataStore,
                displayInfo: true
            })
        };
        
        gridControl = new Ext.grid.GridPanel(gridConfig);

        dataStore.load();
        
        /* Grid renderers */

        function renderNodeNameColumn(value, meta, record) {
            var data = record.data;
            var img = String.format("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;id={0}&amp;status={1}&amp;size=Small' />",
                data.NodeID,
                data.NodeStatus);
            
            var link = String.format("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}' target='_blank'>{1}</a>", data.NodeID, data.NodeName);
            return String.format("{0} {1}", img, link);
        }

        function renderGroupNameColumn(value, meta, record) {
            var data = record.data;
            var img = String.format("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.Container&amp;id={0}&amp;status={1}&amp;size=Small' />",
                    data.GroupID,
                    data.GroupStatus);
            
            var link = String.format("<a href='/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:{0}' target='_blank'>{1}</a>", data.GroupID, data.GroupName);
            return String.format("{0} {1}", img, link);
        }

        function renderReassingLinkColumn(value, meta, record) {
            var nodeId = record.data.NodeID,
                groupId = record.data.GroupID,
                linkHtmlFormat = "<a href='#' id='{0}'>{1}</a>",
                linkId = Ext.id();

            $(String.format("#{0}", linkId)).live('click', function () {
                var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_ReassignWaitText;E=js}");
                var onSuccess = function () {
                    waitMsg.hide();
                    gridControl.getStore().reload();
                    mainObj.onChangeHandler();
                };

                reassignApplication(nodeId, groupId, onSuccess);
            });

            return groupId > 0
                ? String.format(linkHtmlFormat, linkId, "@{R=APM.Strings;K=APMWEBJS_TemplateAssignmentDlg_Reassign;E=js}")
                : "";
        }

        function reassignApplication(nodeId, groupId, onSuccess) {
            SW.Core.Services.callController("/api/ApmTemplateGroupAssignment/ReassignApplication",
                {
                    nodeId: nodeId,
                    templateId: templateId,
                    groupId: groupId
                },
                onSuccess);
        };

        /* Data Store handlers */

        function onStoreBeforeLoad(pStore, pOpt) {
            var start = pOpt.params.start, limit = pOpt.params.limit;

            if (!start || !limit) {
                start = 0;
                limit = gridControl.getBottomToolbar().pageSize;
            }

            pStore.proxy.conn.jsonData = { templateId: templateId, startRowNumber: start, rowsCount: limit };
        }

        function onStoreLoad() {
            mainObj.onLoadHandler();
        }

        /* Base properties */

        mainObj.control = gridControl;
        mainObj.onLoadHandler = function() {};
        mainObj.onChangeHandler = function() {};
    }
    
    unassignedNodesGrid.prototype = new SW.APM.TemplateGroupAssignment.TemplateAssignmentsGrid();

    module.UnassignedNodesGrid = unassignedNodesGrid;
});