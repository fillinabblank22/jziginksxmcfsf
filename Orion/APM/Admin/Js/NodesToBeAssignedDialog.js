﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';

    function nodesToBeAssignedDialog(groupId, groupName, serversOnly) {
        var mDlg = null,
            nodesGrid = null;

        this.show = function () {
            var dataStore = new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: "/api/ApmTemplateGroupAssignment/GetNodesForGroup",
                    method: "POST",
                    failure: function (xhr) { ORION.handleError(xhr); }
                }),
                reader: new Ext.data.JsonReader(
                    {
                        totalProperty: "TotalRows",
                        root: "DataTable.Rows"
                    },
                    [
                        { name: "Id", mapping: 0 },
                        { name: "Name", mapping: 1 },
                        { name: "Status", mapping: 2 }
                    ]
                ),
                listeners: { beforeload: onStoreBeforeLoad },
                autoLoad: true
            });

            var assignGridConfig = {
                enableColumnMove: false,
                height: 334,
                border: false,
                autoscroll: false,
                stripeRows: true,
                loadMask: true,
                maskDisabled: false,
                trackMouseOver: false,
                sort: false,
                store: dataStore,
                cm: new Ext.grid.ColumnModel([
                    {
                        header: "@{R=APM.Strings;K=APMWEBJS_VB1_114;E=js}",
                        dataIndex: "Name",
                        sortable: false,
                        menuDisabled: true,
                        renderer: renderNodeNameColumn,
                        width: 524
                    }
                ]),
                bbar: new Ext.PagingToolbar({
                    pageSize: 10,
                    store: dataStore,
                    displayInfo: true
                })
            };

            var dlgConfig = {
                id: "apm_NodesToBeAssignedDialog_" + Ext.id(),
                title: "@{R=APM.Strings;K=AssignTempatesToGroupsWizard_NodesToBeAssignedDlg_Title;E=js}",
                width: 560,
                height: 450,
                border: false,
                region: "center",
                resizable: false,
                modal: true,
                items: [
                    {
                        region: "head",
                        padding: 10,
                        border: false,
                        items: [
                            {
                                xtype: "label",
                                html: String.format("@{R=APM.Strings;K=AssignTempatesToGroupsWizard_NodesToBeAssignedDlg_Text;E=js}", groupName, serversOnly
                                    ? "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_AssignedTo_ServersOnly;E=js}"
                                    : "@{R=APM.Strings;K=APMWEBJS_GroupsGrid_AssignedTo_AllNodes;E=js}"),
                                style: "text-transform: none; font-weight: normal;"
                            },
                            {
                                region: "grid",
                                border: false,
                                items: [(nodesGrid = new Ext.grid.GridPanel(assignGridConfig))],
                                style: "padding-top: 10px;"
                            }
                        ]
                    }
                ],
                buttons: [
                    { text: "@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}", handler: onClose }
                ]
            };

            mDlg = new Ext.Window(dlgConfig);
            mDlg.show();
        }

        /* Main dialog handlers */

        function onClose() {
            mDlg.hide();
            mDlg.destroy();
            mDlg = null;
        };

        /* Grid renderers */

        function renderNodeNameColumn(value, meta, record) {
            var data = record.data;
            var img = String.format("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;id={0}&amp;status={1}&amp;size=Small' />",
                data.Id,
                data.Status);
            var link = String.format("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}' target='_blank'>{1}</a>", data.Id, data.Name);
            return String.format("{0} {1}", img, link);
        }

        /* Data Store handlers */

        function onStoreBeforeLoad(pStore, pOpt) {
            var start = pOpt.params.start, limit = pOpt.params.limit;

            if (!start || !limit) {
                start = 0;
                limit = nodesGrid.getBottomToolbar().pageSize;
            }

            pStore.proxy.conn.jsonData = { groupId: groupId, serversOnly: serversOnly, startRowNumber: start, rowsCount: limit };
        }
    };

    module.NodesToBeAssignedDialog = nodesToBeAssignedDialog;
});