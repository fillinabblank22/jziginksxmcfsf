﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';

    function unassignTemplateDialog(templateId, groupId, templateName, groupName, appsCount, changeStateCallback) {
        var mDlg = null,
            mDlgId = "apm_deleteApplicationMonitor_" + Ext.id(),
            removeAppsCheckBoxId = mDlgId + "deleteAppsChkBoxId";

        this.show = function () {
            var dlgConfig = {
                id: mDlgId,
                title: "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Header;E=js}",
                width: 478,
                height: appsCount != 0 ? 244 : 180,
                border: false,
                autoScroll: true,
                region: "center",
                resizable: false,
                modal: true,
                items: [
                    {
                        region: "head",
                        padding: 15,
                        border: false,
                        autoScroll: true,
                        items: [
                            {
                                xtype: "label",
                                html: String.format(
                                    "<img src=\"/orion/images/icon.question_32x32.png\" style=\"float:left;margin-right:6px;height:25px;width:25px;\">" +
                                    "<p style=\"margin-top:5px;\"><b>{0}</b></p>",
                                    "@{R=APM.Strings;K=APMWEBJS_UnassignTemplateDlg_Head;E=js}"),
                                style: "text-transform: none;"
                            },
                            {
                                xtype: "label",
                                html: String.format("<p style=\"margin-top: 10px; margin-left: 30px;\">{0}</p>",
                                    String.format("@{R=APM.Strings;K=APMWEBJS_UnassignTemplateDlg_Body;E=js}", templateName, groupName)),
                                style: "text-transform: none; font-weight: normal;"
                            },
                            {
                                xtype: "label",
                                html: appsCount > 0
                                    ? String.format("<table style='margin-left: 30px; margin-top: 30px;'>" +
                                        "<tr>" +
                                        "<td style='vertical-align:top;'><input type='checkbox' id='{0}' style='margin-top: 3px;'></td>" +
                                        "<td>{1}</td>" +
                                        "</tr>" +
                                        "</table>",
                                        removeAppsCheckBoxId,
                                        String.format("@{R=APM.Strings;K=APMWEBJS_UnassignTemplateDlg_DeleteApps;E=js}",
                                            appsCount == 1
                                            ? String.format("@{R=APM.Strings;K=APMWEBJS_UnassignTemplateDlg_App;E=js}", appsCount)
                                            : String.format("@{R=APM.Strings;K=APMWEBJS_UnassignTemplateDlg_Apps;E=js}", appsCount)))
                                    : "",
                                style: "text-transform: none; font-weight: normal;"
                            }
                        ]
                    }
                ],
                buttons: [
                    { text: "@{R=APM.Strings;K=APMWEBJS_UnassignTemplateDlg_Yes;E=js}", width: 160, handler: onSubmit },
                    { text: "@{R=APM.Strings;K=APMWEBJS_UnassignTemplateDlg_No;E=js}", width: 160, handler: onClose }
                ]
            };

            mDlg = new Ext.Window(dlgConfig);
            mDlg.show();
        }

        /* Main dialog handlers */

        function onSubmit() {
            var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_WaitText;E=js}");
            var onSuccess = function () {
                waitMsg.hide();
                changeStateCallback();
            };

            unassignTemplate(onSuccess);

            onClose();
        };

        function onClose() {
            mDlg.hide();
            mDlg.destroy();
            mDlg = null;
        };

        function unassignTemplate(onSuccess) {
            SW.Core.Services.callController("/api/ApmTemplateGroupAssignment/DeleteAssignments",
                {
                    assignments: [{
                        templateId: templateId,
                        groupId: groupId
                    }],
                    deleteApplications: !Ext.isEmpty($(String.format('#{0}', removeAppsCheckBoxId)).attr('checked'))
                },
                onSuccess);
        };
    }

    module.UnassignTemplateDialog = unassignTemplateDialog;
});