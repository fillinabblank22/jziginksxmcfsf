﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.TemplateGroupAssignment', function (module, APMjs) {
    'use strict';

    function removeAssignmentsDialog(templatesIDs, onChangeHandler) {
        var mDlg = null,
            mAssignGrid = null,
            mSelItems = [];

        this.show = function () {
            var dataStore = new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: "/api/ApmTemplateGroupAssignment/GetAssignments",
                    method: "POST",
                    failure: function (xhr) { ORION.handleError(xhr); }
                }),
                reader: new Ext.data.JsonReader(
                    {
                        totalProperty: "TotalRows",
                        root: "DataTable.Rows"
                    },
                    [
                        { name: "GroupName", mapping: 0 },
                        { name: "TemplateName", mapping: 1 },
                        { name: "NodesCount", mapping: 2 },
                        { name: "GroupID", mapping: 3 },
                        { name: "TemplateID", mapping: 4 }
                    ]
                ),
                listeners: { beforeload: onStoreBeforeLoad, load: onStoreLoad },
                autoLoad: true
            });

            var ckBoxSelectionModel = new Ext.grid.CheckboxSelectionModel(
            {
                checkOnly: true,
                listeners: { rowselect: onSelect, rowdeselect: onDeselect, selectionchange: onSelectionChange }
            });

            var assignGridConfig = {
                enableColumnMove: false,
                height: 155,
                border: false,
                autoscroll: false,
                stripeRows: true,
                loadMask: true,
                maskDisabled: false,
                trackMouseOver: false,
                sort: false,
                store: dataStore,
                cm: new Ext.grid.ColumnModel([
                    ckBoxSelectionModel,
                    { header: "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Group;E=js}", dataIndex: "GroupName", sortable: false, menuDisabled: true, width: 110 },
                    {
                        header: "@{R=APM.Strings;K=APMXMLDATA_VB1_25;E=js}",
                        dataIndex: "TemplateName",
                        sortable: false,
                        menuDisabled: true,
                        width: 260,
                        renderer: function(value, meta, record) {
                            return _.escape(value);
                        }
                    },
                    { header: "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_AssignedToNodes;E=js}", dataIndex: "NodesCount", sortable: false, menuDisabled: true, width: 135, align: "right", css: "div {padding-right: 0px;}", renderer: renderNodesCount },
                    { dataIndex: "GroupId", sortable: false, menuDisabled: true, align: "center", renderer: renderViewGroupLink },
                    { dataIndex: "TemplateId", hidden: true }
                ]),
                sm: ckBoxSelectionModel,
                bbar: new Ext.PagingToolbar({
                    pageSize: 4,
                    store: dataStore,
                    displayInfo: true
                })
            };

            var dlgConfig = {
                id: "apm_RemoveTemplatesFromGroupsDialog",
                title: "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Header;E=js}",
                width: 660,
                height: 370,
                border: false,
                region: "center",
                resizable: false,
                modal: true,
                items: [
                    {
                        region: "head",
                        padding: 10,
                        border: false,
                        items: [
                            {
                                xtype: "label",
                                html: String.format("<h3>{0}</h3>", "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_HeadText;E=js}"),
                                style: "text-transform: none;"
                            },
                            {
                                xtype: "label",
                                html: String.format("<p>{0}</p>", "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Text;E=js}"),
                                style: "text-transform: none; font-weight: normal;"
                            }
                        ]
                    },
                    {
                        region: "grid",
                        padding: 10,
                        border: false,
                        items: [(mAssignGrid = new Ext.grid.GridPanel(assignGridConfig))]
                    },
                    {
                        region: "tip",
                        padding: 10,
                        border: false,
                        status: "Undefined",
                        items: [
                            {
                                xtype: "label",
                                html: String.format("<span class='sw-suggestion-icon'></span><p><b>{0}</b>&nbsp;{1}</p>",
                                    "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_TipHead;E=js}",
                                    "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_TipText;E=js}"),
                                style: "font-weight: normal; text-transform: none;",
                                cls: "sw-suggestion sw-suggestion-warn"
                            }
                        ]
                    }
                ],
                buttons: [
                    { text: "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_UnassignBtn;E=js}", handler: onSubmit, disabled: true },
                    { text: "@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}", handler: onClose }
                ]
            };

            mDlg = new Ext.Window(dlgConfig);
            mDlg.show();
        }

        /* Main dialog handlers */

        function onSubmit() {
            var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_WaitText;E=js}");
            var onSuccess = function () {
                waitMsg.hide();
                onChangeHandler();
            };

            removeAssignments(onSuccess);

            onClose();
        };

        function onClose() {
            mDlg.hide();
            mDlg.destroy();
            mDlg = null;
        };

        function removeAssignments(onSuccess) {
            SW.Core.Services.callController("/api/ApmTemplateGroupAssignment/DeleteAssignments",
                {
                    assignments: mSelItems.map(function (s) {
                        return { templateId: s.TemplateID, groupId: s.GroupID };
                    }),
                    deleteApplications: true
                },
                onSuccess);
        };

        /* Grid renderers */

        function renderViewGroupLink(value, meta, record) {
            var groupId = record.data.GroupID,
                linkHtmlFormat = "<a href='#' onclick='{0}'>{1}</a>",
                redirectJsFormat = "window.open(\"/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:{0}\"); return false;";
            return groupId > 0
                ? String.format(linkHtmlFormat,
                    String.format(redirectJsFormat, groupId),
                    "@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_ViewGroup;E=js}")
                : "";
        }

        function renderNodesCount(value, meta, record) {
            var nodesCount = record.data.NodesCount;
            return nodesCount == 1
                ? String.format("@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Node;E=js}", 1)
                : String.format("@{R=APM.Strings;K=APMWEBJS_RemoveTemplateGroupAssignmentDlg_Nodes;E=js}", nodesCount);
        }

        /* Data Store handlers */

        function onStoreBeforeLoad(pStore, pOpt) {
            var start = pOpt.params.start, limit = pOpt.params.limit;

            if (!start || !limit) {
                start = 0;
                limit = mAssignGrid.getBottomToolbar().pageSize;
            }

            pStore.proxy.conn.jsonData = { templatesIds: templatesIDs, startRowNumber: start, rowsCount: limit };
        }

        function onStoreLoad(pStore, pRecs) {
            var store = mAssignGrid.getStore();

            store.removeAll();

            if (pRecs.length > 0) {
                var i, count, rec, selRecs = [];

                for (i = 0, count = pRecs.length; i < count; i++) {
                    pRecs[i].data.GroupName = pRecs[i].json[0];
                    pRecs[i].data.TemplateName = pRecs[i].json[1];
                    pRecs[i].data.NodesCount = pRecs[i].json[2];
                    pRecs[i].data.GroupID = pRecs[i].json[3];
                    pRecs[i].data.TemplateID = pRecs[i].json[4];

                    store.add(pRecs[i]);
                }

                for (i = 0, rec = store.getAt(i) ; i < store.getCount() ; rec = store.getAt(++i)) {
                    if (getSelIndex(rec) > -1) {
                        selRecs.push(rec);
                    }
                }

                mAssignGrid.getSelectionModel().selectRecords(selRecs);
            }

            APMjs.Utility.manageChecker(mAssignGrid);
        }

        /* Selection Model handlers */

        function onSelect(pSender, pIndex, pRec) {
            if (getSelIndex(pRec) != -1) {
                return false;
            }

            mSelItems.push(getSelInfo(pRec));

            return true;
        }

        function onDeselect(pSender, pIndex, pRec) {
            var index = getSelIndex(pRec);

            if (index == -1) {
                return false;
            }

            mSelItems.splice(index, 1);

            return true;
        }

        function onSelectionChange() {
            APMjs.Utility.manageChecker(mAssignGrid);

            mDlg.buttons[0].setDisabled(mSelItems.length == 0);

            return true;
        }

        /* Helper methods */

        function getSelInfo(pRec) {
            return {
                GroupID: pRec.get("GroupID"),
                TemplateID: pRec.get("TemplateID")
            };
        }

        function getSelIndex(pRec) {
            for (var i = 0; i < mSelItems.length; i++) {
                if (mSelItems[i].GroupID == pRec.get("GroupID") &&
                    mSelItems[i].TemplateID == pRec.get("TemplateID")) {
                    return i;
                }
            }
            return -1;
        }
    };

    module.RemoveAssignmentsDialog = removeAssignmentsDialog;
});