using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common.Models;
using System.Collections.Generic;
using System.IO;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using System.Runtime.Serialization;
using System.Xml;

public partial class Orion_APM_Admin_ImportApplicationTemplate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (uploadTemplate.HasFile)
        {
            Stream data = uploadTemplate.PostedFile.InputStream;
            try
            {
                using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                {
                    businessLayer.ApplicationTemplateImportFromXmlStreamEx(data);
                }				
                Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
            }
            catch (SerializationException)
            {
                valTemplateFile.IsValid = false;
            }
            catch (XmlException)
            {
                valTemplateFile.IsValid = false;
            }
        }
        else
        {
            valTemplateFile.IsValid = false;
        }
    }
}
