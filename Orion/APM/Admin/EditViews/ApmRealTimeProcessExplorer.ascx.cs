﻿using System;
using System.Linq;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.Logging;

public partial class Orion_APM_Admin_EditViews_ApmRealTimeProcessExplorer : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    private static readonly Log log = new Log();

    public override String PropertyValue { get; set; }

    private const RealTimeProcessExplorerPermission DefaultRPTAccess = RealTimeProcessExplorerPermission.Disallow;

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
            // populate property value to listbox as selected item
			var selectedValue = ctrlAllow.SelectedValue;
			if (!String.IsNullOrEmpty(PropertyValue))
			{
                // check that the value is there
			    var foundItem = ctrlAllow.Items.FindByValue(PropertyValue);
                if (foundItem != null)
                    selectedValue = foundItem.Value;
                else
                    log.WarnFormat("Wrong {0} property value, value {1}; user {2}, using default {3}", PropertyName, PropertyValue, Profile.UserName, DefaultRPTAccess);
			}

            // propagate default value in case that the property hasn't any
            if (string.IsNullOrEmpty(selectedValue))
                selectedValue = ((int)DefaultRPTAccess).ToString();

            ctrlAllow.SelectedValue = selectedValue;
		}
		PropertyValue = ctrlAllow.SelectedValue;
	}

    protected override void OnDataBinding(EventArgs e)
    {
        // populate listbox with values from RealTimeProcessExplorerPermission enumeration
        ctrlAllow.DataSource = Enum.GetValues(typeof(RealTimeProcessExplorerPermission)).Cast<RealTimeProcessExplorerPermission>()
                                           .Select(code =>
                                               {
                                                   // get resource for enumeration value
                                                   var resourceText = EnumHelper.GetLocalizedName(code, Resources.APMWebContent.ResourceManager);
                                                   return new
                                                                       {
                                                                           Value = (Int32)code,
                                                                           Text = resourceText ?? code.ToString()
                                                                       };
                                               }).OrderBy(a => a.Value);
        ctrlAllow.DataBind();
        base.OnDataBinding(e);
    }
}