﻿using System;

public partial class Orion_APM_Admin_EditViews_ApmRealTimeEventExplorer : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
	public override string PropertyValue { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
			var isAllowed = false;
			if (!string.IsNullOrEmpty(PropertyValue))
			{
				if (!bool.TryParse(PropertyValue, out isAllowed))
				{
					isAllowed = false;
				}
			}
			ynAllowEventExplorer.Value = isAllowed;
		}
		PropertyValue = ynAllowEventExplorer.Value.ToString();
    }
}