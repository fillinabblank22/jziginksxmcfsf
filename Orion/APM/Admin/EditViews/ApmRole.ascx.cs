﻿using System;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_EditViews_ApmRole : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
	public override String PropertyValue { get; set; }

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			var isAdmin = false;
			if (!String.IsNullOrEmpty(PropertyValue))
			{
				if (!Boolean.TryParse(PropertyValue, out isAdmin))
				{
					isAdmin = false;
				}
			}
			ctrRoles.ClearSelection();
			HtmlHelper.SetListSelectedValue(ctrRoles, isAdmin.ToString(), Boolean.TrueString);
		}

		if (Boolean.TrueString.Equals(ctrRoles.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
		{
			PropertyValue = Boolean.TrueString;
		}
		else 
		{
			PropertyValue = Boolean.FalseString;
		}
	}
}