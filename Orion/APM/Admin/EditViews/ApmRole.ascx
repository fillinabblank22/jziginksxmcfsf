﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApmRole.ascx.cs" Inherits="Orion_APM_Admin_EditViews_ApmRole" %>

<asp:ListBox ID="ctrRoles" SelectionMode="Single" Rows="1" runat="server">
	<asp:ListItem Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_136 %>" Selected="True" Value="True"/>
	<asp:ListItem Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_137 %>" Value="False"/>
</asp:ListBox>