﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllowIisActionRights.ascx.cs" Inherits="Orion_APM_Admin_EditViews_AllowIisActionRights" %>
<%@ Register TagPrefix="orion" TagName="yesnocontrol" Src="~/Orion/Admin/YesNoControl.ascx" %>

<orion:yesnocontrol ID="ynAllowIisActions" runat="server" />

 <script type="text/javascript">
     $(window).load(function () {
         var allowAdmin = $('[id*=ctrRoles]');

           var adminChangeEvent = function () {
               var adminValue = allowAdmin.find('option:selected').val();
               var allowIisActions = $('#<%= ynAllowIisActions.ListBoxClientId %>');
               if (adminValue == "True") {
                   allowIisActions.val(adminValue);
               }
           };
           allowAdmin.change(adminChangeEvent);
     });
 </script>