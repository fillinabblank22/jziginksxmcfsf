﻿using System;

public partial class Orion_APM_Admin_EditViews_AllowIisActionRights : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    public override String PropertyValue { get; set; }

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var isAllowed = false;
            if (!String.IsNullOrEmpty(PropertyValue))
            {
                if (!Boolean.TryParse(PropertyValue, out isAllowed))
                {
                    isAllowed = false;
                }
            }
            ynAllowIisActions.Value = isAllowed;
        }
        PropertyValue = ynAllowIisActions.Value.ToString();
    }
}