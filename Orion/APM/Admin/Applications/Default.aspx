<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" AutoEventWireup="true" 
    CodeFile="Default.aspx.cs" Inherits="Orion_APM_Admin_Applications_Default" Title="<%$ Resources: APMWebContent, APMWEBDATA_TM0_6 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="apm" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/APM/Controls/IncludeDateFormats.ascx" TagPrefix="apm" TagName="IncludeDateFormats" %>
<%@ Register Src="~/Orion/APM/Controls/NavigationTabBar.ascx" TagPrefix="apm" TagName="NavigationTabBar" %>

<%@ Register tagPrefix="orion" TagName="MaintenanceSchedulerDialog" Src="~/Orion/Controls/MaintenanceSchedulerDialog.ascx"%>
<%@ Register TagPrefix="orion" TagName="ProgressDialog" Src="~/Orion/Controls/ProgressDialog.ascx" %>

<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes"%>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <a style="background-image: url('/Orion/APM/Images/StatusIcons/Components/small-blank.gif');" 
        href="/Orion/APM/Admin/Components/Default.aspx">
            <%= Resources.APMWebContent.APMWEBDATA_TM0_7 %>
    </a>
    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHConfigSettingsManageAssignedApplicationMonitors" ID="helpButton" runat="server" />
    
    <asp:ScriptManagerProxy ID="ScriptManager1" runat="server" >
        <Services>
	        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
			<asp:ServiceReference path="/Orion/APM/Services/Applications.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
</asp:Content>

<asp:Content ID="c1" ContentPlaceHolderID="hPh" Runat="Server">

<link rel="stylesheet" type="text/css" href="../ExistingElements.css" />

<apm:IncludeDateFormats runat="server" />
<apm:IncludeExtJs runat="server" debug="false" Version="3.4"/>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Admin/Applications/ExistingApps.js" Module="APM" />
<orion:Include runat="server" File="Admin/Js/DeleteApplicationMonitorDialog.js" Module="APM" />
    
<!-- Maintenance Mode for SAM -->
<orion:Include runat="server" File="MaintenanceMode/AlertSuppressionHandlers.js" />
<orion:Include runat="server" File="MaintenanceMode/MaintenanceModeUtils.js" />
<orion:Include runat="server" File="MaintenanceMode.js" Module="APM" />
<orion:Include runat="server" File="NodeMNG.css" />
<orion:Include runat="server" File="MaintenanceMode/MaintenanceModeUtils.js" />

<style type="text/css">
	#ElementGrouping .GroupItems {
		width: 300px;
	}
	#ElementGrouping .GroupItems .GroupItem {
		word-wrap: normal; white-space: normal;
	}
	#gridCell div#Grid { 
		width: 850px;
	}
    #gridCell div#Grid .edit {
        background-image:url(/Orion/Nodes/images/icons/icon_edit.gif) !important;
    }
    #gridCell div#Grid .delete {
        background-image:url(/Orion/Nodes/images/icons/icon_delete.gif) !important;
    }
    #gridCell div#Grid .maintenanceMode {
        background-image:url(/Orion/Nodes/images/icons/maintenance_mode_icon16.png) !important;
    }
    .x-menu-item-schedule {
        padding: 3px 5px !important;
        text-transform: uppercase;
        color: #204a63 !important; 
    }
    .x-menu-item-noicon .x-menu-item-icon {
        display: none;
    }
</style>
</asp:Content>




<asp:Content ID="c2" ContentPlaceHolderID="cPh" Runat="Server">
    <orion:MaintenanceSchedulerDialog ID="MaintenanceSchedulerDialog" runat="server" />
    <orion:ProgressDialog ID="ProgressDialog" runat="server" />

	<%foreach (string setting in new string[] { "GroupBy", "GroupByValue"}) { %>
		<input type="hidden" name="APM_ExistingApps_<%=setting%>" id="APM_ExistingApps_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("APM_ExistingApps_" + setting)%>' />
	<%}%>

	<input type="hidden" id="NodeCustomProperties" value='<%=string.Join(",", GetCustomProperties("Nodes"))%>' />
	<input type="hidden" id="ApplicationCustomProperties" value='<%=string.Join(",", GetCustomProperties("APM_ApplicationCustomProperties"))%>' />

    <div class="tableWidth">		

	    <table id="breadcrumb">
		    <tr>
			    <td>
				    <h1><%=Page.Title%></h1>
			    </td>
		    </tr>
	    </table>
	    
        <apm:NavigationTabBar ID="NavigationTabBar" runat="server" Visible="true"></apm:NavigationTabBar>

        <div id="tabPanel" class="tab-top">

	        <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%" style="padding-right: 10px;">
		        <tr>
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="font-size: 11px; padding-bottom: 10px;">
                			        <%= Resources.APMWebContent.APMWEBDATA_TM0_8 %>
                                </td>
                                <td align="right" style="padding-bottom:5px;display:none;">
                                    <label for="search"><%= Resources.APMWebContent.APMWEBDATA_TM0_19 %></label>
                                    <input id="search" type="text" size="20" />
                                    <input id="doSearch" type="button" value="Search" />			    
                                </td>
                            </tr>                    
                        </table>
                    </td>		        
		        </tr>
		        <tr valign="top" align="left">
			        <td style="padding-right: 10px;">
				        <div id="ElementGrouping" class="ElementGrouping">
					        <div class="apm_GroupSection">
						        <div><%= Resources.APMWebContent.APMWEBDATA_TM0_9 %></div>
						        <select id="groupByProperty" style="width:100%">
							        <option value=""><%= Resources.APMWebContent.APMWEBDATA_TM0_10 %></option>
							        <option value="at.Name"><%= Resources.APMWebContent.APMWEBDATA_TM0_11 %></option>
                                    <option value="c.Name"><%= Resources.APMWebContent.APMWEBDATA_YP0_1 %></option>

							        <option value="n.Caption"><%= Resources.APMWebContent.APMWEBDATA_TM0_12 %></option>
							        <option value="n.Vendor"><%= Resources.APMWebContent.APMWEBDATA_TM0_13 %></option>
							        <option value="n.MachineType"><%= Resources.APMWebContent.APMWEBDATA_TM0_14 %></option>
							        <option value="n.Contact"><%= Resources.APMWebContent.APMWEBDATA_TM0_15 %></option>
							        <option value="n.Location"><%= Resources.APMWebContent.APMWEBDATA_TM0_16 %></option>
							        <option value="n.ObjectSubType"><%= Resources.APMWebContent.APMWEBDATA_TM0_17 %></option>
                                    <option value="e.ServerName"><%= Resources.APMWebContent.APMWEBDATA_GROUPBY_POLLENG %></option>

							        <option value="n.SysObjectID"><%= Resources.APMWebContent.APMWEBDATA_TM0_18 %></option>
							        <%foreach (string prop in SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetPropNamesForTable("Nodes", false)) {
								        if (SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetTypeForProp("Nodes", prop) != typeof(System.Text.StringBuilder)) { %>
									        <option value="n.CustomProperties.<%=prop%>"><%= Resources.APMWebContent.APMWEBDATA_NodeSingle %> <%=prop%></option>
							        <%}}%>

							        <%foreach (string prop in SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetPropNamesForTable("APM_ApplicationCustomProperties", false)) { %>
									        <option value="a.CustomProperties.<%=prop%>"><%= Resources.APMWebContent.ApmWeb_CP_ApplicationPrefix %> <%=prop%></option>
							        <%}%>
        							
						        </select>
					        </div>
					        <ul class="GroupItems"></ul>
				        </div>

			        </td>
			        <td id="gridCell">
                        <div id="Grid"/>
			        </td>
		        </tr>
	        </table>
	        
	        <div id="originalQuery"></div>	
	        <div id="test"></div>
	        <pre id="stackTrace"></pre>
	        
    	</div>
    	
	    <div id="ApmStatusIconPaths">
	        <%foreach (ApmStatus status in ApmStatus.GetValues()) { %>
		        <input type="hidden" name="ApmStatus_<%=(int)status.Value%>" value='<%=status.ToString("smallappimgpath", null)%>' />
            <%}%>
        </div>    
    </div>
    <script type="text/javascript">
        SW.APM.apps.AllowUnmanage = '<%= Profile.AllowUnmanage.ToString() %>';
    </script>
</asp:Content>

