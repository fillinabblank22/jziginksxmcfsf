Ext.namespace('SW');
Ext.namespace('SW.APM');

SW.APM.apps = function () {
    var appAllInGroup = false, appCount = 0, appPropName = null, appPropVal = null;
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;

    var getGroupByValues = function (groupBy, onSuccess) {
        ORION.callWebService("/Orion/APM/Services/Applications.asmx",
                             "GetValuesAndCountForProperty", { property: groupBy },
                             function (result) {
                                 var table = ORION.objectFromDataTable(result);

                                 $(table.Rows).each(
                                     function () {
                                         if (this.theValue != null && this.theValue.length > 4 && this.theValue.substr(1, 5) == 'Date(') {
                                             this.theValue = new Date(parseInt(this.theValue.replace("/Date(", "").replace(")/", ""), 10));
                                         }
                                     });

                                 onSuccess(table);
                             });
    };

    var deleteApplications = function (appIds, onSuccess) {
        ORION.callWebService("/Orion/APM/Services/Applications.asmx", "DeleteApplications", { applicationIds: appIds }, function (result) { onSuccess(); });
    };

    var deleteAllApplications = function (prop, val, onSuccess) {
        ORION.callWebService("/Orion/APM/Services/Applications.asmx", "DeleteAllApplications", { propName: prop, propValue: val }, function (result) { onSuccess(); });
    };

    var loadAppsWithGroupsInfo = function (prop, val) {
        var appsWithGroupsInfo;

        var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");

        SW.Core.Services.callControllerSync("/api/ApmTemplateGroupAssignment/LoadAppsWithGroupsInfo",
            {
                propName: prop,
                propValue: val
            },
            function (result) {
                appsWithGroupsInfo = result;
                waitMsg.hide();
            });

        return appsWithGroupsInfo;
    };

    var getVendorIconFiles = function (onSuccess) {
        ORION.callWebService("/Orion/APM/Services/Applications.asmx", "GetVendorIconFileNames", { includeApmNodesOnly: true }, function (result) { onSuccess(result); });
    }
    
    var refreshObjects = function () {
        appPropName = $("#groupByProperty option:selected").val();
        appPropVal = $(".SelectedGroupItem a").attr('value');
        appCount = parseInt($(".SelectedGroupItem a").attr("appCountInGroup"));
        if (!appPropName) {
            appPropName = appPropVal = "";
            appCount = 0;
        }

        if (appPropVal) {
            ORION.Prefs.save('GroupByValue', appPropVal);
        }

        grid.store.proxy.conn.jsonData = { property: appPropName, value: ((appPropVal == undefined) || (appPropVal === null)) ? "" : appPropVal };
        grid.store.load();
    };

    var selectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");
        grid.store.removeAll();
        refreshObjects();

        onUpdateSelectAllMessage(selectorModel);
        manageChecker();
        return false;
    };

    var addVendorIcons = function () {

        getVendorIconFiles(function (result) {
            $(result).each(function () {
                $('<img src="/NetPerfMon/images/Vendors/' + this[1] + '" />').error(function () {
                    this.src = "/NetPerfMon/images/Vendors/Unknown.gif";
                    return true;
                }).prependTo(".GroupItems a[value='" + this[0] + "']").after(" ");
            });
        });
    };

    var loadGroupByValues = function () {
        var prop = $("#groupByProperty option:selected").val();
        if (prop != null && typeof (prop) != "undefined") {
            ORION.Prefs.save("GroupBy", prop);
        }
        if (prop) {
            var groupItems = $(".GroupItems").text("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");

            getGroupByValues(prop, function (result) {
                groupItems.empty();
                $(result.Rows).each(function () {
                    var value = this.theValue, count = this.theCount;
                    var disp = '';
                    var attr = value;
                    if (value instanceof Date) {
                        if (value.getFullYear() == 1900) {
                            disp = String.format('@{R=APM.Strings;K=APMWEBJS_TM0_9;E=js}', count);
                            attr = 'NULL';
                        }
                        else {
                            disp = ($.trim(value.dateFormat($SW.ExtLocale.DateShort) + " " + value.dateFormat($SW.ExtLocale.TimeLong)) || String.format('@{R=APM.Strings;K=APMWEBJS_TM0_9;E=js}', count));
                            attr = value.format($SW.ExtLocale.SortableDateTime);
                        }
                    }
                    else {
                        disp = $.trim(String(value)) + " (" + count + ")";
                    }

                    if (value === null || value === "") {
                        disp = prop === "c.Name"
                            ? String.format("@{R=APM.Strings;K=APMWEBJS_YP0_1;E=js}", count)
                            : String.format("@{R=APM.Strings;K=APMWEBJS_TM0_30;E=js}", count);
                    }
                    $("<a href='#'></a>")
						.attr('value', (value === null) ? "" : attr)
						.attr("appCountInGroup", count)
						.text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='GroupItem'/>");
                });

                var toSelect = $(".GroupItem a[value='" + ORION.Prefs.load("GroupByValue") + "']");

                if (toSelect.length === 0) {
                    toSelect = $(".GroupItem a:first");
                }

                toSelect.click();

                if (prop === "n.Vendor") {
                    addVendorIcons();
                }
            });
        } else {
            $(".GroupItems").empty();
            refreshObjects();
        }
    };

    var deleteSelectedItems = function (items) {
        var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_TM0_5;E=js}");

        function deleted() {
            waitMsg.hide();
            $("#groupByProperty").change();

            if (($("#groupByProperty option:selected").val() != '') && (appCount == 0)) {
                grid.store.reload();
            }
        };

        if (appAllInGroup) {
            deleteAllApplications(appPropName, appPropVal, function () { grid.store.reload(); deleted(); });
        } else {
            var toDelete = [];

            Ext.each(items, function (item) {
                toDelete.push(item.data.ApplicationID);
            });
            appCount -= toDelete.length;
            
            deleteApplications(toDelete, deleted);
        }
    };

    var editApplication = function (item) {
        window.location = APM.GetAppEditPage(item.data.ApplicationID);
    };

    var updateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        var unManCount = 0;
        selItems.each(function (rec) { unManCount += rec.data.UnmanagedStatus ? 1 : 0; });

        map.EditButton.setDisabled(count != 1);
        map.DeleteButton.setDisabled(count === 0);
        map.MaintenanceMode.setDisabled((SW.APM.apps.AllowUnmanage === "False" && !IsDemoMode())|| count === 0);
    };

    var sortGroupByValues = function () {
        //Handle special values (like '[No Grouping]'), which have value set to ''
        function selectSort(a, b) {
            if (a.value == '') {
                return -1;
            } else if (b.value == '') {
                return 1;
            }

            return (a.innerHTML > b.innerHTML) ? 1 : -1;
        };

        var selectedValue = $('#groupByProperty').val();
        $('#groupByProperty option').sort(selectSort).appendTo('#groupByProperty');
        $('#groupByProperty').val(selectedValue);
    };

    //Check uncheck global(select all) checkbox
    function manageChecker() {
        var rC = grid.getStore().getCount(), sC = grid.getSelectionModel().getCount();
        var el = $("div[class^='x-grid3-hd-inner x-grid3-hd-checker']", grid.body.dom), css = "x-grid3-hd-checker-on", index = el.attr("class").indexOf(css);
        if (index > 0 && (rC == 0 || sC == 0)) {
            el.removeClass(css);
        } else if ((index > 0 && rC != sC) || (index == -1 && rC > 0 && rC == sC)) {
            el.toggleClass(css);
        }
    }

    var renderers = {
        "RenderCustomView": function (value, meta, record) {
            return value;
        },
        "RenderName": function (value, meta, record) {
            var data = record.data;

            var encodedValue = _.escape(value);
            if (Ext.isEmpty(data.NetObject)) {
                return SF("{0} {1}", getStatusIcon(data.AppStatus), encodedValue);
            } else {
                return SF("{0} <a href='/Orion/APM/ApplicationDetails.aspx?NetObject={1}' target='_blank'>{2}</a>", getStatusIcon(data.AppStatus), data.NetObject, encodedValue);
            }
        },
        "RenderTemplateName": function (value, meta, record) {
            return  _.escape(value);
        },
        "RenderNodeName": function (value, meta, record) {
            var data = record.data;
            var img = SF("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;id={0}&amp;status={1}&amp;size=Small' />", data.NodeID, data.NodeStatus);
            var encodedNodeName = _.escape(data.NodeName);
            var link = SF("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}' target='_blank'>{1}</a>", data.NodeID, encodedNodeName);
            return SF("{0} {1}", img, link);
        },
        "RenderGroupName": function (value, meta, record) {
            var data = record.data;
            var iconAndName = "";
            if (data.GroupID) {
                var encodedGroupName = _.escape(data.GroupName);
                var img = SF("<img class='apm_StatusIcon' src='/Orion/StatusIcon.ashx?entity=Orion.Container&amp;id={0}&amp;status={1}&amp;size=Small' />", data.GroupID, data.GroupStatus);
                var link = SF("<a href='/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:{0}' target='_blank'>{1}</a>", data.GroupID, encodedGroupName);
                iconAndName = SF("{0} {1}", img, link);
            }
            return iconAndName;
        },
        "RenderUnmanagedStatus": function (value, meta, record) {
            var data = record.data;
            var status = (value ? "@{R=APM.Strings;K=APMWEBJS_TM0_6;E=js}" : "@{R=APM.Strings;K=APMWEBJS_TM0_7;E=js}");
            if (data.AlertSuppressionState === SW.Core.MaintenanceMode.EntityAlertSuppressionMode.SuppressedByItself ||
                data.AlertSuppressionState === SW.Core.MaintenanceMode.EntityAlertSuppressionMode.SuppressedByParent) {
                status = status +
                    "@{R=APM.Strings;K=ManageApplications_AlertSuppressionStatus_Separator;E=js}" +
                    "@{R=APM.Strings;K=ManageApplications_AlertSuppressionStatus_Suppressed;E=js}";
            }
            return status;
        },
        "RenderDateTime": function (value, meta, record) {
            if (!value)
                return '';

            a = reMsAjax.exec(value);
            if (a) {
                var b = a[1].split(/[,.]/);
                var dt = new Date(+b[0]);
                return $.trim(dt.dateFormat($SW.ExtLocale.DateShort) + " " + dt.dateFormat($SW.ExtLocale.TimeLong));
            }
            return '';
        }
    };

    function getStatusIcon(status) {
        return String.format('<img src="{0}" />', $("#ApmStatusIconPaths input[name=ApmStatus_" + status + "]").val());
    }

    function getManageItems() {
        var items = { Ids: [], Uris: [], Names: [] };
        grid.getSelectionModel().each(function (rec) {
            var d = rec.data;
            items.Ids.push(d.ApplicationID);
            items.Uris.push(d.EntityUri);
            items.Names.push(d.Name);
        });

        return items;
    }

    function getManageItemsFromManageApplicationInfoCollection(items) {
        var result = { Ids: [], Uris: [] };
        
        _.each(items, function (item) {
            result.Ids.push(item.ID);
            result.Uris.push(item.EntityUri);
        });

        return result;
    };

    function getItemsToApplyAndExecute(action, successCallback, failureCallback) {
        if (appAllInGroup) {
            // get all from backend
            ORION.callWebService("/Orion/APM/Services/Applications.asmx",
                "GetAllApplicationsIdsAndUris",
                { property: appPropName, value: appPropVal },
                function(result) {
                    var items = getManageItemsFromManageApplicationInfoCollection(result);
                    action(items, successCallback, failureCallback);
                });
        } else {
            var items = getManageItems();
            action(items, successCallback, failureCallback);
        }
    }

    function showRemanageAppOnUnmanagedNodeWarning(appIds) {
        if (appIds.length > 0) {
            var items = getManageItems();
            var appNames = [];
            appIds.forEach(function(item) {
                var i = items.Ids.indexOf(item);
                appNames.push(items.Names[i]);
            });

            Ext.Msg.show({
                title: '@{R=APM.Strings;K=APMWEBJS_TM0_45;E=js}',
                msg: String.format(appNames.length > 1 ? '@{R=APM.Strings;K=APMWEBJS_TM0_46;E=js}' : '@{R=APM.Strings;K=APMWEBJS_TM0_47;E=js}', appNames.join(', ')),
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.OK,
            });
        }
    }

    function onUnmanageNowClick() {
        if (IsDemoMode()) return APMjs.demoModeAction("Unmanage");

        return getItemsToApplyAndExecute(
            function(items, successCallback) {
                return SW.APM.MaintenanceMode.ScheduleUnmanage(items.Ids, null, null, successCallback);
            },
            loadGroupByValues);
    }

    function onRemanageClick() {
        if (IsDemoMode()) return APMjs.demoModeAction("Remanage");

        var appsFromUnmanagedNodes = [];
        var remanagingAppOnUnmanagedNodeCallback = function (result) {
            appsFromUnmanagedNodes = appsFromUnmanagedNodes.concat(result);
        }

        return getItemsToApplyAndExecute(
            function(items, successCallback) {
                return SW.APM.MaintenanceMode.ManageAgain(items.Ids, successCallback, remanagingAppOnUnmanagedNodeCallback);
            },
            function() {
                loadGroupByValues();
                showRemanageAppOnUnmanagedNodeWarning(appsFromUnmanagedNodes);
            });
    }

    function onSuppressNowClick() {
        if (IsDemoMode()) return APMjs.demoModeAction("Suppress");

        return getItemsToApplyAndExecute(
            function(items, successCallback) {
                return SW.Core.MaintenanceMode.ScheduleAlertSuppression(items.Uris, null, null, successCallback);
            },
            loadGroupByValues);
    }

    function onResumeClick() {
        if (IsDemoMode()) return APMjs.demoModeAction("Resume");

        return getItemsToApplyAndExecute(
            function (items, successCallback) {
                return SW.Core.MaintenanceMode.ResumeAlerts(items.Uris, successCallback);
            },
            loadGroupByValues);
    }

    function onScheduleClick() {        
       return getItemsToApplyAndExecute(
            function(items, c1, c2) {
                if (items.Ids.length === 1) {
                    SW.APM.MaintenanceMode.MaintenanceScheduler
                        .ShowDialogForSingleEntity(items.Uris[0], items.Ids[0], items.Names[0], c1);
                } else {
                    SW.APM.MaintenanceMode.MaintenanceScheduler.ShowDialogForMultipleEntities(items, c1, c2);
                }
            },
            loadGroupByValues
            );
    }

    function onUpdateSelectAllMessage(selModel) {
        appAllInGroup = false;
        var el = $("#apmEA_AM");
        if (el.size() == 0) {
            $(grid.getTopToolbar().el.dom).append("<div id='apmEA_AM' class='x-panel-body x-panel-body-noheader' style='padding:3px;display:none;'><span></span>&nbsp;&nbsp;<a href='#' style='color:#f00;text-decoration:underline;'></a></div>");

            el = $("#apmEA_AM");
            el.find("a").click(function () {
                el.find("span").html("@{R=APM.Strings;K=APMWEBJS_TM0_36;E=js}"); el.find("a").hide();
                appAllInGroup = true;
                return false;
            });
        }
        if (appCount == 0) {
            appCount = dataStore.totalLength;
        }
        var count = selModel.getCount();
        if (count > 0 && count == dataStore.getCount() && count != appCount) {
            el.find("span").html(String.format("@{R=APM.Strings;K=APMWEBJS_TM0_32;E=js}", selModel.getCount()));
            el.find("a").html(String.format("@{R=APM.Strings;K=APMWEBJS_TM0_33;E=js}", appCount)).show();
            el.show();
        } else {
            el.hide();
        }
    }

    ORION.prefix = "APM_ExistingApps_";

    return {
        registerRenderer: function (key, renderer) {
            if (renderer != undefined && renderer.Condition != undefined && renderer.Action != undefined) {
                var prevRenderer = renderers[key];
                renderers[key] = function (value, meta, record) {
                    if (renderer.Condition(value, meta, record)) {
                        return renderer.Action(value, meta, record);
                    } else {
                        return prevRenderer(value, meta, record);
                    }
                };
            }
        },
        //adds additional processing of data before rendering
        onPreRender: function () {
        },
        init: function () {

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '20'));

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);
            selectorModel.on("selectionchange", onUpdateSelectAllMessage);
            selectorModel.on("selectionchange", manageChecker);

            var dsItems = [
					{ name: 'ApplicationID', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'AppStatus', mapping: 2 },
                    { name: 'NodeID', mapping: 3 },
                    { name: 'NodeName', mapping: 4 },
                    { name: 'NodeStatus', mapping: 5 },
			        { name: 'CustomApplicationType', mapping: 6 },
                    { name: 'TemplateName', mapping: 7 },
                    { name: 'UnmanagedStatus', mapping: 8 },
                    { name: 'Created', mapping: 9 },
                    { name: 'LastModified', mapping: 10 },
                    { name: 'CustomView', mapping: 11 },
				    { name: 'ComponentsCount', mapping: 12 },
        			//Polling engine is column 13, it's not displayed
                    //ViewXml is column 14
					{ name: "NetObject", mapping: 15 },
                    { name: "GroupName", mapping: 16 },
                    { name: "GroupStatus", mapping: 17 },
                    { name: "GroupID", mapping: 18 },
					{ name: "EntityUri", mapping: 19 },
                    { name: "AlertSuppressionState", mapping: 20 }
            ];
			var customPropertyStartIndex = _.last(dsItems).mapping + 1;

            var gridColumns = [
		        selectorModel,
		        { header: 'ID', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'ApplicationID' },
		        {
		            header: '@{R=APM.Strings;K=APMWEBJS_TM0_23;E=js}',
		            width: 225,
		            sortable: true,
		            dataIndex: 'Name',
		            renderer: function (value, meta, record) {
		                return renderers["RenderName"](value, meta, record);
		            }
		        },
		        {
		            header: '@{R=APM.Strings;K=APMWEBJS_TM0_24;E=js}',
		            width: 180,
		            sortable: true,
		            dataIndex: 'NodeName',
		            renderer: function (value, meta, record) {
		                return renderers["RenderNodeName"](value, meta, record);
		            }
		        },
                {
                    header: '@{R=APM.Strings;K=APMWEBJS_ExistingApps_GroupNameHeader;E=js}',
                    width: 180,
                    sortable: true,
                    dataIndex: 'GroupName',
                    renderer: function (value, meta, record) {
                        return renderers["RenderGroupName"](value, meta, record);
                    }
                },
                { header: '@{R=APM.Strings;K=APMWEBJS_RM0_1;E=js}', width: 180, sortable: true, dataIndex: 'ComponentsCount', align: 'center' },
                {
                    header: '@{R=APM.Strings;K=APMWEBJS_TM0_25;E=js}',
                    width: 215,
                    sortable: true,
                    dataIndex: 'TemplateName',
                    renderer: function(value, meta, record) {
                        return renderers["RenderTemplateName"](value, meta, record);
                    }
                },
		        {
		            header: '@{R=APM.Strings;K=APMWEBJS_TM0_26;E=js}',
		            width: 125,
		            sortable: true,
		            dataIndex: 'UnmanagedStatus',
		            renderer: function (value, meta, record) {
		                return renderers["RenderUnmanagedStatus"](value, meta, record);
		            }
		        },
		        {
		            header: '@{R=APM.Strings;K=APMWEBJS_TM0_27;E=js}',
		            width: 95,
		            sortable: false,
		            dataIndex: 'CustomView',
		            renderer: function (value, meta, record) {
		                return renderers["RenderCustomView"](value, meta, record);
		            }
		        },
		        { header: '@{R=APM.Strings;K=APMWEBJS_TM0_28;E=js}', width: 145, sortable: true, dataIndex: 'Created' },
		        { header: '@{R=APM.Strings;K=APMWEBJS_TM0_29;E=js}', width: 145, sortable: true, dataIndex: 'LastModified' }
            ];

            var appCustomProperties = $('#ApplicationCustomProperties').val();

            $.each(appCustomProperties.split(','), function (i, cp) {
                if (cp != '') {
                    var cpItem = cp.split(':');
                    dsItems.push({ name: 'CustomProperties.' + cpItem[0], mapping: customPropertyStartIndex + i });  //Update mapping of Custom Properties here if DataStore is changed
                    if (cpItem[1] === 'System.DateTime') {
                        gridColumns.push({
                            header: cpItem[0], width: 100, sortable: true, dataIndex: 'CustomProperties.' + cpItem[0], renderer: function (value, meta, record) {
                                return renderers["RenderDateTime"](value, meta, record);
                            }
                        });
                    } else {
                        gridColumns.push({ header: cpItem[0], width: 100, sortable: true, dataIndex: 'CustomProperties.' + cpItem[0] });
                    }
                }
            });

            dataStore = new ORION.WebServiceStore(
				"/Orion/APM/Services/Applications.asmx/GetApplicationsPaged",
				dsItems,
				"Name");
								
            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: gridColumns,

                sm: selectorModel,

                viewConfig: {
                    forceFit: false
                },

                width: 700,
                height: 500,
                stripeRows: true,

                tbar: [
		        {
		            id: 'EditButton',
		            text: '@{R=APM.Strings;K=APMWEBJS_TM0_9;E=js}',
		            tooltip: '@{R=APM.Strings;K=APMWEBJS_TM0_10;E=js}',
		            iconCls: 'edit',
		            handler: function () {
		                if (IsDemoMode())  return APMjs.demoModeAction("EditProperties");
		                editApplication(grid.getSelectionModel().getSelected());
		            }
		        }, '-', {
                    id: 'MaintenanceMode',
                    text: '@{R=Core.Strings.2;K=WEBJS_LH0_3;E=js}',
                    iconCls: 'maintenanceMode',
					menu: {
						xtype: 'menu',
						items: [
							{
							    cls: 'suppress-item x-menu-item-schedule',
							    text: '@{R=APM.Strings;K=APMWEBJS_PS1_1;E=js}',
								handler: function() {
								    onSuppressNowClick();
								}
							},
							{
							    cls: 'resume-item x-menu-item-schedule',
							    text: '@{R=APM.Strings;K=APMWEBJS_PS1_2;E=js}',
								handler: function() {
								    onResumeClick();
								}
							},
                            '-',
							{
							    cls: 'unmanage-item x-menu-item-schedule',
							    text: '@{R=APM.Strings;K=APMWEBJS_PS1_3;E=js}',
								handler: function() {
								    onUnmanageNowClick();
								}
							},
							{
							    cls: 'remanage-item x-menu-item-schedule',
							    text: '@{R=APM.Strings;K=APMWEBJS_PS1_4;E=js}',
								handler: function() {
								    onRemanageClick();
								}
							},
                            '-',
							{
							    cls: 'schedule-item x-menu-item-schedule',
							    text: '@{R=APM.Strings;K=APMWEBJS_PS1_5;E=js}',
								handler: function() {
								    onScheduleClick();
								}
							}
						]
					}
				}, '-', {
		            id: 'DeleteButton',
		            text: '@{R=APM.Strings;K=APMWEBJS_TM0_15;E=js}',
		            tooltip: '@{R=APM.Strings;K=APMWEBJS_TM0_16;E=js}',
		            iconCls: 'delete',
		            handler: function () {
		                if (IsDemoMode())  return APMjs.demoModeAction("Delete");

		                var gridSelections = grid.getSelectionModel().getSelections();

		                var selectedAppsWithGroupsInfo, selectedAppsCount;

		                if (appAllInGroup && grid.store.totalLength > pageSizeNum) {
		                    selectedAppsWithGroupsInfo = loadAppsWithGroupsInfo(appPropName, appPropVal);
		                    selectedAppsCount = grid.store.totalLength;
		                } else {
		                    selectedAppsWithGroupsInfo = gridSelections
		                        .filter(function (s) {
		                            return s.data.GroupName !== null;
		                        })
		                        .map(function (s) {
		                            return {
		                                applicationId: s.data.ApplicationID,
		                                nodeName: s.data.NodeName,
		                                groupName: s.data.GroupName
		                            };
		                        });
		                    selectedAppsCount = gridSelections.length;
		                }

		                if (selectedAppsWithGroupsInfo.length > 0) {
		                    var deleteAppnMonitorsDlg = new SW.APM.TemplateGroupAssignment
		                        .DeleteApplicatonMonitorDialog(selectedAppsWithGroupsInfo,
		                            selectedAppsCount,
		                            function () {
		                                deleteSelectedItems(gridSelections);
		                            });
		                    deleteAppnMonitorsDlg.show();
		                } else {
		                    Ext.Msg.confirm("@{R=APM.Strings;K=APMWEBJS_TM0_17;E=js}",
		                        "@{R=APM.Strings;K=APMWEBJS_TM0_18;E=js}",
		                        function (btn) {
		                            if (btn == 'yes') {
		                                deleteSelectedItems(gridSelections);
		                            }
		                        });
		                }

		                return null;
		            }
		        }, '-'
                ],
                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=APM.Strings;K=APMWEBJS_TM0_19;E=js}',
                    emptyMsg: "@{R=APM.Strings;K=APMWEBJS_TM0_20;E=js}",
                    beforePageText: "@{R=APM.Strings;K=APMWEBJS_TM0_34;E=js}",
                    afterPageText: "@{R=APM.Strings;K=APMWEBJS_TM0_35;E=js}",
                    items: [
                        new Ext.form.Label({ text: '@{R=APM.Strings;K=APMWEBJS_TM0_21;E=js}', style: 'margin-left: 5px; margin-right: 5px' }),
                        new Ext.form.ComboBox({
                            regex: /^\d*$/,
                            store: new Ext.data.SimpleStore({
                                fields: ['pageSize'],
                                data: [[10], [20], [30], [40], [50], [100]]
                            }),
                            displayField: 'pageSize',
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus: true,
                            width: 50,
                            editable: false,
                            value: pageSizeNum,
                            listeners: {
                                'select': function (c, record) {
                                    grid.bottomToolbar.pageSize = record.get("pageSize");
                                    grid.bottomToolbar.cursor = 0;
                                    ORION.Prefs.save('PageSize', grid.bottomToolbar.pageSize);
                                    grid.bottomToolbar.doRefresh();
                                }
                            }
                        })
                    ]
                })
            });
            SW.APM.apps.onPreRender();
            grid.render('Grid');
            updateToolbarButtons();

            var fudgeFactor = 12;
            var groupItemsHeight = $("#Grid").height() - $(".apm_GroupSection").height() - fudgeFactor;
            $(".GroupItems").height(groupItemsHeight);

            // Set the width of the grid
            grid.setWidth($('#gridCell').width());

            sortGroupByValues();

            $("#groupByProperty").val(ORION.Prefs.load("GroupBy", "")).change(loadGroupByValues);
            $("#groupByProperty").change();

            $("form").submit(function () { return false; });

            //var resizer = new Ext.Resizable("ElementGrouping", {
            //	handles: "e", minWidth: 200, maxWidth: 500, minHeight: 100, maxHeight: 400, pinned: true
            //});
            //resizer.on("resize", function () {
            //	grid.setWidth($('#gridCell').width());
            //});
        }
    };

}();

Ext.onReady(SW.APM.apps.init, SW.APM.apps);
