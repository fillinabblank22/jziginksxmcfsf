using System;
using System.Linq;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Core.Common;
using SolarWinds.APM.Web.Utility;
using System.Globalization;

public partial class Orion_APM_Admin_Applications_Default : System.Web.UI.Page
{
	protected string[] GetCustomProperties(string table)
	{
        var properties =  CustomPropertyMgr.GetCustomPropertiesForTable(table);
        return (from cp in properties select string.Format(CultureInfo.InvariantCulture, "{0}:{1}", cp.PropertyName, cp.PropertyType)).ToArray();
	}

    protected override void OnInit(System.EventArgs e)
    {
        Response.Cookies.Add(DateTimeFormatHelper.CreatePageCookieForExtJsDateTimeFormatForUnmanagedDialog());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var plugins = WebPluginManager.Instance.SupportedPlugins.Where(p => p is IPluginScriptProvider);
        foreach (var webPlugin in plugins)
        {
            foreach (var appType in webPlugin.SupportedCustomApplicationTypes)
            {
                ((IPluginScriptProvider)webPlugin).IncludeScripts(appType);
            }
        }
    }
}
