﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ComponentTestResults.ascx.cs" Inherits="Orion_APM_Admin_ComponentTestResults" %>

<asp:UpdatePanel ID="testResultUpdater" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="margin-left: 20px; padding: 5px; font-size: 8pt;" runat="server" id="statusDiv"> 
            <asp:Image ID="img" Visible="false" runat="server" />
            <asp:Label ID="result" runat="server"></asp:Label>
            <div style="margin-left: 20px; margin-right: 20px">
				<asp:DataGrid ID="messages" runat="server" BorderWidth="0" CellPadding="0" CellSpacing="0" CssClass="apm_TestResultMessages" ShowFooter="false" ShowHeader="false"></asp:DataGrid>
			</div>
            <asp:HiddenField ID="fakeButton" runat="server" OnValueChanged="RefreshResults"/>
            <asp:HiddenField ID="hfStatisticValue" runat="server" />
            <asp:HiddenField ID="hfTestSuccessful" runat="server" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

