﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCertificateSetFileUploader.ascx.cs" Inherits="Orion_APM_Admin_EditCertificateSetFileUploader" %>

<div id="fileUploaderContainer" runat="server">
    <input id="fileUploader" runat="server" type="file" size="45" />
</div>

<script type="text/javascript">
    function ErrorMsgBox(title, msg) {
        Ext.Msg.show({
            title: title,
            msg: msg,
            minWidth: 250,
            modal: true,
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.OK
        });
    }

    function ajaxFileUpload(fileInputElement, fileContentElementId) {
        var containerElement = $(fileInputElement).parent();
        SW.APM.FileUpload.uploadFile({
            url: '/Orion/APM/Admin/EditCertificateSetFileUploader.ashx',
            secureuri: false,
            fileElementId: fileInputElement.id,
            dataType: 'text',
            data: { name: 'logan', id: 'id' },
            success: function (data, status) {
                var certificateUploadResult = $(data).children().html();
                if (certificateUploadResult.substring(0, 5) === 'Error') {
                    var errorMsg = certificateUploadResult.substring(6);
                    ErrorMsgBox('<%= Resources.APMWebContent.APMWEBDATA_TM0_47 %>', errorMsg);
                } else {
                    $('#' + fileContentElementId).val(certificateUploadResult);
                    $('#' + fileContentElementId).focus();
                    //clear input file
                    $(containerElement).html($(containerElement).html());
                }
            }
        });
        return false;
    }
</script>
