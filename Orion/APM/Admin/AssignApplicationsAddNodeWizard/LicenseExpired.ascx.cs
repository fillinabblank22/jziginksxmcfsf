﻿using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using System.Web.UI;

public partial class Orion_APM_Admin_AssignApplicationsAddNodeWizard_LicenseExpired :
	UserControl, IAddCoreNodePlugin, IAddNodePluginWizard, IAddNodePluginValidator
{
	public string Title
	{
		get { return Resources.APMWebContent.APMWEBCODE_VB1_105; }
	}

	public void Cancel()
	{
		NodeWorkflowHelper.Reset();
		Response.Redirect("../Default.aspx", true);
	}

	public void Next()
	{
	}

	public void Previous()
	{
	}

	public void Save(Node node)
	{
	}

	public bool ValidateInput()
	{
		return true;
	}

	public bool Validate()
	{
		return true;
	}
}