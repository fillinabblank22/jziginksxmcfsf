﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web;
using Node = SolarWinds.APM.Common.Models.Node;
using NodeSubType = SolarWinds.APM.Common.Models.NodeSubType;
using OrionNode = SolarWinds.Orion.Core.Common.Models.Node;
using SNMPVersion = SolarWinds.APM.Common.Models.SNMPVersion;

public partial class Orion_APM_Admin_Applications_AssignApplicationsAddNodeWizard : UserControl, IAddCoreNodePlugin, IAddNodePluginWizard, IAddNodePluginValidator
{
    public class TemplatesInfo
    {
        public int Id { get; private set; }

        public string Name { get; private set; }

        public int CmpCount { get; private set; }

        public TemplatesInfo(int id, string name, int cmpCount)
        {
            Id = id;
            Name = name;
            CmpCount = cmpCount;
        }
    }


    private readonly string SQL_CMP_COUNT =
        @"
            SELECT 
                at.ApplicationTemplateID AS ID, 
                at.Name AS Name, 
                COUNT(at.ApplicationTemplateID) AS CmpCount
            FROM 
                Orion.APM.ApplicationTemplate AS at
            JOIN 
                Orion.APM.ComponentTemplate AS ct ON 
                at.ApplicationTemplateID = ct.ApplicationTemplateID
            GROUP BY 
                at.Name, at.ApplicationTemplateID
        ";

    protected string _json = String.Empty;

    protected string CookieInfo
    {
        get
        {
            return string.Format(
                "{{\"path\":\"/sgn{0}{1}/\",\"domain\":{2}}}",
                Page.GetType().GetHashCode(),
                Profile.UserName.GetHashCode(),
                JsHelper.Serialize(Request.Url.Host)
            );
        }
    }

    public List<int> TemplateIDs
    {
        get
        {
            if (Session["AAaNW_TemplateIDs"] == null)
            {
                Session["AAaNW_TemplateIDs"] = new List<int>();
            }

            return (List<int>) Session["AAaNW_TemplateIDs"];
        }
        set
        {
            if (value == null)
            {
                if (Session["AAaNW_TemplateIDs"] != null)
                {
                    Session.Remove("AAaNW_TemplateIDs");
                }
            }
            else
            {
                Session["AAaNW_TemplateIDs"] = value;
            }
        }
    }

    public List<TemplatesInfo> TemplateInfos
    {
        get
        {
            if (Session["AAaNW_TemplateInfos"] == null)
            {
                var templatesInfos = new List<TemplatesInfo>();
                foreach (DataRow row in SwisProxy.ExecuteQuery(SQL_CMP_COUNT).Rows)
                {
                    templatesInfos.Add(new TemplatesInfo((int) row["ID"], (String) row["Name"], (int) row["CmpCount"]));
                }

                Session["AAaNW_TemplateInfos"] = templatesInfos;
            }

            return (List<TemplatesInfo>) Session["AAaNW_TemplateInfos"];
        }
        set
        {
            if (value == null)
            {
                if (Session["AAaNW_TemplateInfos"] != null)
                {
                    Session.Remove("AAaNW_TemplateInfos");
                }
            }
            else
            {
                Session["AAaNW_TemplateInfos"] = value;
            }
        }
    }

    public string SelectedTag
    {
        get
        {
            if (Session["AAaNW_SelectedTag"] == null)
            {
                Session["AAaNW_SelectedTag"] = "Popular";
            }

            return (string) Session["AAaNW_SelectedTag"];
        }
        set
        {
            if (value == null)
            {
                if (Session["AAaNW_SelectedTag"] != null)
                {
                    Session.Remove("AAaNW_SelectedTag");
                }
            }
            else
            {
                Session["AAaNW_SelectedTag"] = value;
            }
        }
    }

    public bool InheritCredentials
    {
        get { return (Session["AAaNW_InheritCredentials"] != null) ? (bool) Session["AAaNW_InheritCredentials"] : false; }
        set { Session["AAaNW_InheritCredentials"] = value; }
    }

    public CredentialSet SelectedCredentialSet
    {
        get
        {
            if (Session["AAaNW_SelectedCredentialSet"] == null)
            {
                Session["AAaNW_SelectedCredentialSet"] = selectCredentials.SelectedCredentialSet;
            }

            return (CredentialSet) Session["AAaNW_SelectedCredentialSet"];
        }
        set
        {
            if (value == null)
            {
                if (Session["AAaNW_SelectedCredentialSet"] != null)
                {
                    Session.Remove("AAaNW_SelectedCredentialSet");
                }
            }
            else
            {
                Session["AAaNW_SelectedCredentialSet"] = value;
            }
        }
    }

    public void Cancel()
    {
        TemplateIDs = null;
        TemplateInfos = null;
        SelectedTag = null;
        SelectedCredentialSet = null;
        InheritCredentials = false;

        NodeWorkflowHelper.Reset();
        Response.Redirect("../Default.aspx", true);
    }

    public void Next()
    {
        if (!Page.IsValid)
        {
            return;
        }

        UpdateData(false);
    }

    public void Previous()
    {
        if (!Page.IsValid)
        {
            return;
        }

        UpdateData(false);
    }

    public void Save(SolarWinds.Orion.Core.Common.Models.Node node)
    {
        if (SelectedCredentialSet == null)
        {
            return;
        }

        if (TemplateIDs.Count > 0)
        {
            int credentialSetId = InheritCredentials ? -1 : SelectedCredentialSet.Id;
            using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var apps = bl.CreateApplications(TemplateIDs, new List<int>(1) {node.ID}, credentialSetId, true);
                foreach (var app in apps)
                {
                    ApmViewManager.AssociateTemplateWithView(app.TemplateId, app.ViewId, bl);
                }
            }
        }

        TemplateIDs = null;
        TemplateInfos = null;
        SelectedTag = null;
        SelectedCredentialSet = null;
        InheritCredentials = false;
    }

    public string Title
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_105; }
    }

    public bool ValidateInput()
    {
        // this method is obsolete, replaced by IAddNodePluginValidator.Validate
        // but can not be removed because it is still member of IAddNodePlugin
        return true;
    }

    public bool Validate()
    {
        List<int> selTemplates;
        if (string.IsNullOrEmpty(Request.Form["selectedTemplates"]))
        {
            selTemplates = null;
        }
        else
        {
            selTemplates = new JavaScriptSerializer().Deserialize<List<int>>(string.Format("[{0}]", Request.Form["selectedTemplates"]));
        }

        TemplateIDs = selTemplates;

        if (selTemplates == null || selTemplates.Count == 0)
        {
            // no template selected, no need to validate credentials
            return true;
        }

        if (inheritCredentialsCheckBox.Checked)
        {
            // new credentials are not created
            return true;
        }

        Page.Validate(selectCredentials.ValidationGroupName);

        return selectCredentials.IsValid;
    }

    protected void Page_Load(Object sender, EventArgs e)
    {
        ApmBaseResource.EnsureApmScripts();

        ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
        if (scriptManager.IsInAsyncPostBack)
        {
            Page.Header.Title = Title;
        }

        if (!IsPostBack)
        {
            InitData();

            var node = NodeWorkflowHelper.Node;
            if (!string.IsNullOrWhiteSpace(node.Name))
                NodeIpLiteral.Text = node.Name;
            else if (!string.IsNullOrWhiteSpace(node.DNS))
                NodeIpLiteral.Text = node.DNS;
            else if (!string.IsNullOrWhiteSpace(node.SysName))
                NodeIpLiteral.Text = node.SysName;
            else
                NodeIpLiteral.Text = node.IpAddress;
        }

        TestButton.Enabled = true;
    }

    protected void testResultsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var item = e.Item;
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            var cred = InheritCredentials ? null : selectCredentials.SelectedCredentialSet;
            var node = ConvertToApmNode(NodeWorkflowHelper.Node, cred);

            var cmp = (ComponentTemplate) item.DataItem;
            var res = new ComponentTestResult(node, cmp.Type);

            var appSettings = new ApplicationSettings() {Mode = ApmProbeMode.Test};
            res.StartTest(cmp, appSettings, cred);

            var cmpName = (Literal) item.FindControl("cmpName");
            var cmpTestResult = (Orion_APM_Admin_ComponentTestResults) item.FindControl("cmpTestResult");

            cmpName.Text = cmp.Name;
            cmpTestResult.TestResult = res;
            cmpTestResult.DataBind();
        }
    }

    protected void TestButton_Click(object sender, EventArgs e)
    {
        UpdateData(true);

        List<ComponentTemplate> componentTemplates = new List<ComponentTemplate>();
        ApplicationTemplate tmpl;
        using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            foreach (var tmplId in TemplateIDs)
            {
                tmpl = bl.GetApplicationTemplate(tmplId);
                componentTemplates.AddRange(tmpl.ComponentTemplates);
            }
        }

        testResults.DataSource = componentTemplates;
        testResults.DataBind();
        selectCredentials.PreFillPasswords();
    }

    private void InitData()
    {
        using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            IList<TagInfo> tagInfos = bl.GetTagsWithoutCustomApplicationTypes(-1, null);
            tags.DataSource = tagInfos;
            tags.DataBind();
        }

        tags.SelectedValue = SelectedTag;
        selectCredentials.DataSource = SelectedCredentialSet;
        selectCredentials.DataBindNewCredentialSetForm();
        inheritCredentialsCheckBox.Checked = InheritCredentials;

        var node = NodeWorkflowHelper.Node;
        selectCredentials.AllowNodeWmiCredential =
            (node.NodeSubType == SolarWinds.Orion.Core.Common.Models.NodeSubType.WMI ||
             node.NodeSubType == SolarWinds.Orion.Core.Common.Models.NodeSubType.Agent);

        SetClientState(ToJson("tpl"));
    }

    private void UpdateData(bool testMode)
    {
        if (!Validate()) return;

        SelectedTag = tags.SelectedValue;
        InheritCredentials = inheritCredentialsCheckBox.Checked;

        SelectedCredentialSet = selectCredentials.SelectedCredentialSet;

        if (testMode)
        {
            PreFillCredentialsPassword.Value = SelectedCredentialSet.Id == -1 ? SelectedCredentialSet.Password : "";
        }
        else
        {
            if (TemplateIDs.Count > 0 && !InheritCredentials && selectCredentials.IsValid)
            {
                UseCredentials(selectCredentials.SelectedCredentialSet);
            }

            PreFillCredentialsPassword.Value = "";
        }
    }

    private void SetClientState(string json)
    {
        _json = json;
    }

    private string ToJson(string currPage)
    {
        var jsonObject = new
        {
            step = currPage,
            tag = SelectedTag,
            tplCount = TemplateIDs.Count,
            templates = TemplateIDs,
            templatesInfo = TemplateInfos.Select(item => new {tplID = item.Id, name = item.Name, cmpCount = item.CmpCount}),
            cookieInfo = CookieInfo
        };

        string result = string.Format("SW.APM.AssignApplicationsAddNodeWizard.state = {0};", JsHelper.Serialize(jsonObject));
        return result;
    }

    private void UseCredentials(CredentialSet crs)
    {
        if (crs.Id == -1)
        {
            using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                crs.Id = bl.CreateNewCredentialSet(crs);
            }
        }

        SelectedCredentialSet = crs;
    }

    private Node ConvertToApmNode(OrionNode oNode, CredentialSet cred)
    {
        Node apmNode = new Node();

        apmNode.EngineName = "";

        apmNode.Id = oNode.Id;
        apmNode.IpAddress = oNode.IpAddress;
        apmNode.IsUnmanaged = oNode.UnManaged;
        apmNode.Name = oNode.Name;

        NodeSubType apmNodeSubType = NodeSubType.None;
        switch (oNode.NodeSubType)
        {
            case SolarWinds.Orion.Core.Common.Models.NodeSubType.ICMP:
                apmNodeSubType = NodeSubType.ICMP;
                break;
            case SolarWinds.Orion.Core.Common.Models.NodeSubType.SNMP:
                apmNodeSubType = NodeSubType.SNMP;
                break;
            case SolarWinds.Orion.Core.Common.Models.NodeSubType.WMI:
                apmNodeSubType = NodeSubType.WMI;
                break;
            default:
                break;
        }

        apmNode.NodeSubType = apmNodeSubType;
        apmNode.SNMPPort = (int) oNode.SNMPPort;

        SNMPVersion apmSNMPVersion = SNMPVersion.None;
        switch (oNode.SNMPVersion)
        {
            case SolarWinds.Orion.Core.Common.Models.SNMPVersion.SNMP1:
                apmSNMPVersion = SNMPVersion.SNMP1;
                break;
            case SolarWinds.Orion.Core.Common.Models.SNMPVersion.SNMP2c:
                apmSNMPVersion = SNMPVersion.SNMP2c;
                break;
            case SolarWinds.Orion.Core.Common.Models.SNMPVersion.SNMP3:
                apmSNMPVersion = SNMPVersion.SNMP3;
                break;
            default:
                break;
        }

        apmNode.SNMPVersion = apmSNMPVersion;

        try
        {
            apmNode.Status = Convert.ToInt32(oNode.Status);
        }
        catch (Exception)
        {
            apmNode.Status = (int) NodeStatus.Undefined;
        }

        apmNode.UnmanageFrom = oNode.UnManageFrom;
        apmNode.UnmanageUntil = oNode.UnManageUntil;
        apmNode.Vendor = oNode.Vendor;

        if (cred != null && cred.Id == ComponentBase.NodeWmiCredentialsId)
        {
            var wcred = NodeWorkflowHelper.WmiCredential;
            if (wcred != null && wcred.ID.HasValue)
            {
                apmNode.CredentialId = wcred.ID.Value;
            }
        }
        else if (apmNode.SNMPVersion != SNMPVersion.None)
        {
            if (oNode.ReadOnlyCredentials != null)
                FillSNMPCredentials(apmNode, oNode.ReadOnlyCredentials);
        }

        return apmNode;
    }

    private void FillSNMPCredentials(Node targetNode, SolarWinds.Orion.Core.Common.Models.SnmpCredentials sourceCredentials)
    {
        targetNode.CommunityString = sourceCredentials.CommunityString;
        targetNode.SNMPv3AuthPassword = sourceCredentials.SNMPv3AuthPassword;
        targetNode.SNMPv3PrivacyPassword = sourceCredentials.SNMPv3PrivacyPassword;
        //SAM always uses None
        targetNode.SNMPv3PrivacyType = SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType.None;
        targetNode.SNMPv3UserName = sourceCredentials.SNMPv3UserName;
    }
}