﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignApplicationsAddNodeWizard.ascx.cs" Inherits="Orion_APM_Admin_Applications_AssignApplicationsAddNodeWizard" ClassName="AssignApplicationsAddNodeWizard"%>
<%@ Import Namespace="SolarWinds.APM.Web"%>
<%@ Import Namespace="SolarWinds.APM.Web.Configuration" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI"%>

<%@ Register TagPrefix="apm" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>

<%@ Register Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" TagPrefix="apm" TagName="SelectCredentials" %>
<%@ Register Src="~/Orion/APM/Controls/CredentialTips.ascx" TagPrefix="apm" TagName="CredentialTips" %>

<%@ Register Src="~/Orion/APM/Controls/SelectTestNode.ascx" TagPrefix="apm" TagName="SelectTestNode" %>
<%@ Register Src="~/Orion/APM/Admin/ComponentTestResults.ascx" TagPrefix="apm" TagName="ComponentTestResults" %>

<apm:IncludeExtJs ID="includeExtJs" runat="server" debug="false" Version="3.4"/>

<orion:Include runat="server" File="js/OrionCore.js"/>
<orion:Include runat="server" Module="APM" File="js/jquery.cookie.js"/>
<orion:Include runat="server" Module="APM" File="Admin/AssignApplicationsAddNodeWizard/AssignApplicationsAddNodeWizard.js"/>
<orion:Include runat="server" Module="APM" File="Admin/AssignApplicationsAddNodeWizard/AssignApplicationsAddNodeWizard.css"/>

<script type="text/javascript">
    SW.APM.AssignApplicationsAddNodeWizard.EditTemplatePageUrl = "<%=EditPageConfigurationSection.Config.EditTemplatePageUrl%>";
    $().ready(function(){
        // Hook to fix broken bradcrumb for custom dynamically loaded controls into "Add Node" wizard.
        // Remove this if wizard will be fixed for custom dynamically loaded controld!
        $("#addnodecontent tbody tr td:first > br:first").remove();
        $("#addnodecontent tbody tr td:first > br:first").remove();
        $("#addnodecontent tbody tr td:first > span:first").remove();
        $("#addnodecontent tbody tr td:first > span:first").remove();
        $("#addnodecontent tbody tr td:first > span:first").replaceWith(
            "<table id=\"breadcrumb\" width=\"100%\">" + 
                "<tbody>" +
                    "<tr>" +
                        "<td>" +
                            "<a href=\"../../Admin\">Admin</a>" + " > " +
                            "<a href=\"..\">Manage Nodes</a>" + " > " +
                            "<h1>" + "<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEBCODE_VB1_105)%>" +"</h1>" + "</td>" +
                        "<td align=\"right\" style=\"vertical-align: top;\">" +
                            "<span style=\"float: right; width: 40px;\">" +
                                "<a class=\"HelpButton\" target=\"_blank\"" + 
                                    'href=\"<%=SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("OrionPHNodeManagementAddNodeAddPollers")%>\">' +
                                    "<img src=\"/Orion/images/Button.HelpPage.gif\" alt=\"Online Manual\"/>" +
                                "</a>" +
                            "</span>" +
                        "</td>" +
                    "</tr>" +
                "</tbody>" +
            "</table>");
    });
    
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoadedHandler);
    function pageLoadedHandler(sender, args) {
        if (SW.APM.AssignApplicationsAddNodeWizard.state) {
            new SW.APM.AssignApplicationsAddNodeWizard();
            var step = SW.APM.AssignApplicationsAddNodeWizard.state.step;
            if (step == "tpl") { 
                window.$aa = new SW.APM.AssignApplicationsAddNodeWizard.Template(); 
            }            
        }
                SW.APM.AssignApplicationsAddNodeWizard.initValidator();
        
        var hidden =  $get('<%= PreFillCredentialsPassword.ClientID %>');
        if (hidden.value != "")
        {
            var tt = $("div#form table:first tbody tr td div > table[class='credTable']");
                           
            var tb1 = $("tbody tr td > input[id$='selectCredentials_Password']", tt);
            var tb2 = $("tbody tr td > input[id$='selectCredentials_ConfirmPassword']", tt);
            
            tb1.val(hidden.value);
            tb2.val(hidden.value);
        }
        
        $('#credTableDiv').height($('#credTableDiv table').height()); // hack to prevent IE from collapsing when hiding
        $('#inheritCredentialsCheckbox :checkbox').click(toggleCustomCredentialsForm);
        toggleCustomCredentialsForm();
    }
    
    function toggleCustomCredentialsForm()
    {
        var inheritCredentials = $('#inheritCredentialsCheckbox :checkbox').attr('checked');
        
        if (inheritCredentials) {
            $('#credTableDiv table').hide();
        } else {
            $('#credTableDiv table').show();
        }
    }
    
    <%= _json %>
</script>

<asp:HiddenField ID="PreFillCredentialsPassword" EnableViewState="true" runat="server"/>

<div class="GroupBox" style="margin-top:-20px;">
    <div class="apm_WizardContainer">
        <table width="100%">
            <tr>
                <td valign="top">
                    <span class="ActionName"><%= Resources.APMWebContent.APMWEBDATA_VB1_270 %>
                        <asp:Literal runat="server" ID="NodeIpLiteral"></asp:Literal>
                    <br />
                    </span>
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_271 %>
                    <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        
        <br />
                
        <table style="width:100%;">
            <tr>
                <td>
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_272 %>
                </td>
            </tr>
            <tr>
                <td style="width:auto; padding-right:10px;">
                    <asp:DropDownList ID="tags" DataValueField="Name" DataTextField="Name" AppendDataBoundItems="true" Width="20em" runat="server">
                        <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_TM0_48 %>" Value="" />
                    </asp:DropDownList><span style="font-size: 10px; color: #4d4b4b;"><%= Resources.APMWebContent.APMWEBDATA_VB1_273 %></span>
                </td>
              
            </tr>
        </table>
        
        <div style="padding-top: 10px;">
            <table style="width:100%;">
                <tr>
                    <td style="vertical-align:top; padding-right:10px;">
                        <ul id="templates" class="ZebraBackground"></ul>
                    </td>
                    <td id="selectedTemplatesContainer">
                        <div>
                            <ul id="selectedTemplates" style="font-size: 11px;"></ul>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
        <br />
        <br />
        
        <h2><%= Resources.APMWebContent.APMWEBDATA_VB1_274%></h2>
        <br />
        
        <div id="form">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td id="inheritCredentialsCheckbox">
                        <asp:CheckBox ID="inheritCredentialsCheckBox" runat="server"/>
                    </td>
                    <td style="padding-left: 3px;">
                        <%= Resources.APMWebContent.APMWEBDATA_VB1_275%>
                    </td>
                    <td rowspan="3" valign="top" align="right">
                        <apm:CredentialTips runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td style="padding-left: 3px; padding-top: 2px;"><a href="<%= HelpLocator.CreateHelpUrl("OrionAPMPHConfigSettingsAddNewApplicationMonitors") %>" target="_blank"><%= Resources.APMWebContent.APMWEBDATA_VB1_276%></a></td>
                </tr>
               
                <tr>
                    <td colspan="2" style="padding-top:10px;">
                        <div id="credTableDiv">
                            <table class="credTable">
                                <apm:SelectCredentials ID="selectCredentials" runat="server" />
                            </table>
                        </div>
                    </td>
                </tr>  
                <tr>
                    <td id="credErrorMsg" colspan="2" style="font-size:small;color:#f00;">
                    </td>
                </tr>  
            </table>
            <br />
            <asp:UpdatePanel ID="testComponentsUpdatePanel" UpdateMode="Conditional" ChildrenAsTriggers="false" runat="server">
                <ContentTemplate>
                    <div id="testPanel">
                        <div>
                            <orion:LocalizableButton ID="TestButton" runat="server" LocalizedText="Test" DisplayType="Small" CausesValidation="true" OnClick="TestButton_Click"/>
                        </div>
                        <div>
                            <asp:Repeater ID="testResults" runat="server" OnItemDataBound="testResultsRepeater_ItemDataBound">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <th id="cmpName"><%= Resources.APMWebContent.APMWEBDATA_VB1_277 %></th>
                                            <th id="cmpResults"><%= Resources.APMWebContent.APMWEBDATA_VB1_278%></th>
                                        </tr>
                                </HeaderTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><asp:Literal ID="cmpName" runat="server"></asp:Literal></td>
                                        <td><apm:ComponentTestResults ID="cmpTestResult" runat="server" IgnoreAsynchRegister="false" /></td>
                                        <asp:HiddenField ID="TestSinglePostProcessing" runat="server" />
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div> 
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
