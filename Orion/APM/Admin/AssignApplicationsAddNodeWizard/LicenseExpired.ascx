﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LicenseExpired.ascx.cs" Inherits="Orion_APM_Admin_AssignApplicationsAddNodeWizard_LicenseExpired" %>

<style type="text/css">
	.apm_WizardContainer { width: 785px; }
	.apm_WizardContainer {
		color: #f00; font-weight: bold;
	}
</style>
<div class="GroupBox" style="margin-top:-15px;">
	<div class="apm_WizardContainer">
        <%= string.Format(Resources.APMWebContent.LicenceExpired_Text, SolarWinds.APM.Common.ApmConstants.FullModuleNameHtml)%>
    </div>
</div>