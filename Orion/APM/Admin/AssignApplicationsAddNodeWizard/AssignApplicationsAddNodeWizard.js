﻿Ext.ns("SW.APM");

SW.APM.AssignApplicationsAddNodeWizard = function() {
    var mState = SW.APM.AssignApplicationsAddNodeWizard.state, mEM = SW.APM.EventManager;

    function onStateChange(args) {
        Ext.apply(mState, args);

        mState.tplCount = mState.templates.length;

        $("#nodeCount").text(mState.nodeCount);
        $("#applicationCount").text(mState.tplCount);
    }

    function ctor() {
        mEM.on("onCheckStateChange", onStateChange); onStateChange(mState);
    }; ctor();
}

SW.APM.AssignApplicationsAddNodeWizard.Template = function() {
    var mState = SW.APM.AssignApplicationsAddNodeWizard.state, mEM = SW.APM.EventManager;

    var ROW_TPL =
        "<li class='GroupItem'>" +
            "<table style='width: 100%'>" +
                "<tr>" +
                    "<td>" +
                        "<input type='checkbox' name='templates' value='{0}'/> " +
                        "<img src='/Orion/APM/images/Admin/icon_application_template.gif'> " +
                        "<a class='ShowTemplateInfo' href='#' value='{0}'>{1}</a>" +
                    "</td>" +
                    "<td style='width: 20px;'>" +
                        "<a href='" + SW.APM.AssignApplicationsAddNodeWizard.EditTemplatePageUrl + "?id={2}'>" +
                            "<img src='/Orion/APM/Images/Admin/icon_edit.gif' style='cursor:pointer;' title='@{R=APM.Strings;K=APMWEBJS_VB1_159;E=js}'>" +
                        "</a>" +
                    "</td>" +
                "</tr>" +
            "</table>" +
        "</li>";

    var SEL_ROW_TPL =
        "<li title='{0}'>" +
            "<table style='width: 100%'>" +
                "<td>" +
                    "<div>&#8226; {0}<div>" +
                    "<input type='checkbox' checked='true' name='selectedTemplates' value='{1}' style='display:none;white-space:nowrap'/>" +
                "</td>" +
                "<td style='width: 20px;'>" +
                    "<img src='/Orion/APM/Images/Admin/icon_edit.gif' style='cursor:pointer;display:none;' title='@{R=APM.Strings;K=APMWEBJS_VB1_159;E=js}'>" +
                "</td>" +
                "<td style='width: 20px;'>" +
                    "<img src='/Orion/Discovery/images/icon_delete.gif' style='cursor:pointer;' title='@{R=APM.Strings;K=APMWEBJS_VB1_3;E=js}'>" +
                "</td>" +
            "</table>" +
        "</li>";

    function loadData() {
        var container = $("#templates").text("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");
        function onSuccess(json) {
            container.empty();
            $(ORION.objectFromDataTable(json).Rows).each(
                function () {
                    var encodedTemplateName = _.escape(this.Name);
                    var rowEl = $(String.format(ROW_TPL, this.Id, encodedTemplateName, this.Id)).appendTo(container);
                    if (mState.templates.indexOf(this.Id) > -1) {
                        var tempInput = $("table tr td > input[type='checkbox']", rowEl).attr("checked", true);
                    }
                }
            );

            $("li.GroupItem table tr td > input[type='checkbox']").click(onTplCheck);
            $("li.GroupItem table tr td > a.ShowTemplateInfo").click(onDetailsShow);

            mEM.fire("onCheckStateChange", {});
        }
        ORION.callWebService("/Orion/APM/Services/Templates.asmx", "GetTemplatesForTag", { tagName: $("select[id$='tags']").val(), skipCustomAppTypes: true }, onSuccess);
    }
    function bindSelTpl() {
        var html = '';
        Ext.each(mState.templates, function(tplID) {
            Ext.each(mState.templatesInfo, function(tplInfo) {
                if (tplID == tplInfo.tplID) {
                    var encodedTemplateName = _.escape(tplInfo.name);
                    html += String.format(SEL_ROW_TPL, encodedTemplateName, tplInfo.tplID);
                }
            });
        });
        Ext.get("selectedTemplates").update(String.format("@{R=APM.Strings;K=APMWEBJS_VB1_5;E=js}", mState.tplCount, html));

        $("#selectedTemplates  img").click(onTplUnCheck);
    }

    function onTplUnCheck(evt) {
        var inEl = Ext.get(evt.target).parent("table").child("input").dom;
        inEl.checked = false;
        $(String.format("#templates input[type='checkbox'][value='{0}']", inEl.value)).attr("checked", false);
        return onTplCheck({ target: inEl });
    }

    function onTplCheck(evt) {
        var tplID = parseInt(evt.target.value); if (evt.target.checked) { mState.templates.push(tplID); } else { mState.templates.remove(tplID); }
        mEM.fire("onCheckStateChange", {});
        return true;
    }

    function onDetailsShow(evt) {
        evt.stopPropagation();
        function onSuccess(json) {
            if (!dlg) { return; }
            var cmpHTML = "<li style='color:#f00;'>@{R=APM.Strings;K=APMWEBJS_VB1_10;E=js}</li>";
            Ext.each(json.components, function(cmp, i) { if (i == 0) { cmpHTML = ""; } cmpHTML += String.format("<li>{0}</li>", cmp); });
            dlg.body.dom.innerHTML = ""; dlg.items.add(new Ext.Panel({ frame: true, autoScroll: true, defaultType: "label", items: [{ html: (json.description.length != 0 ? json.description + "<br/>" : "@{R=APM.Strings;K=APMWEBJS_VB1_11;E=js}") }, { html: String.format("@{R=APM.Strings;K=APMWEBJS_VB1_12;E=js}", json.tags.join(", ")) }, { html: String.format("@{R=APM.Strings;K=APMWEBJS_VB1_13;E=js}", json.frequency) }, { html: String.format("@{R=APM.Strings;K=APMWEBJS_VB1_14;E=js}", json.timeout) }, { html: "@{R=APM.Strings;K=APMWEBJS_VB1_15;E=js}" }, { html: String.format("<ul class='components'>{0}</ul>", cmpHTML)}] }));
            dlg.doLayout(true);
        }
        var dlg = new Ext.Window({
            title: evt.target.innerHTML, html: "@{R=APM.Strings;K=APMWEBJS_VB1_16;E=js}", width: 500, height: 350,
            border: false, region: "center", layout: "fit", bodyStyle: "padding:5px;", modal: true, resizable: false, plain: true,
            listeners: function() {
                var em = Ext.EventManager, bd = document.body, onS = function() { if (!dlg) { return; } dlg.center(); };
                return {
                    show: function() { em.on(bd, "mousewheel", onS); em.on(window, "scroll", onS); },
                    hide: function() { em.un(bd, "mousewheel", onS); em.un(window, "scroll", onS); }
                };
            } (),
            buttons: [{ text: "@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}", handler: function() { dlg.hide(); dlg.destroy(); dlg = null; } }]
        });
        dlg.show();
        ORION.callWebService("/Orion/APM/Services/Templates.asmx", "GetTemplateDetails", { templateId: $(evt.target).attr("value") }, onSuccess);
        return false;
    }

    function ctor() {
        var mState = SW.APM.AssignApplicationsAddNodeWizard.state, GB_ALL_VAL = "_GB_T_V_";

        var selTag = $.cookie("groupBy"), elTags = $("select[id$='tags']");
        if (selTag === null) {
            selTag = "Popular";
            $.cookie("groupBy", selTag, { expires: 7, path: mState.cookieInfo.path, domain: mState.cookieInfo.domain });
        }

        elTags.val(selTag == GB_ALL_VAL ? "" : selTag);

        elTags.change(function() {
            $.cookie("groupBy", this.value == "" ? GB_ALL_VAL : this.value);
            loadData();
        }).change();

        $("#detailsASS_rX").css("display", "none");
        mEM.on("onCheckStateChange", bindSelTpl);
    }; ctor();
};

SW.APM.AssignApplicationsAddNodeWizard.initValidator = function () {
    var mErrEl = $("#credErrorMsg");

    function text(pId) { return $(String.format("input[id$='{0}']", pId)).val().trim(); }

    function validate(pMode) {
        mErrEl.html("");

        var tplCount = $("input[name='selectedTemplates']:checked").length;
        if (pMode == "test" && tplCount == 0) {
            mErrEl.html("@{R=APM.Strings;K=APMWEBJS_VB1_160;E=js}");
            return false;
        }

        var selCredId = $("select[id$='CredentialSetList']").val();
        var isNewCred = (selCredId == -1 && $("input[id$='_inheritCredentialsCheckBox']:checked").length == 0);

        if (isNewCred && ((pMode == "test") || (pMode == "next" && tplCount > 0))) {
            if (text("_CredentialName").length < 1 && text("_UserName").length < 1) {
                mErrEl.html("@{R=APM.Strings;K=APMWEBJS_VB1_161;E=js}");
                return false;
            }
            if (text("_CredentialName").length < 1) {
                mErrEl.html("@{R=APM.Strings;K=APMWEBJS_VB1_162;E=js}");
                return false;
            }
            if (text("_UserName").length < 1) {
                mErrEl.html("@{R=APM.Strings;K=APMWEBJS_VB1_163;E=js}");
                return false;
            }
            if (text("_Password") != text("_ConfirmPassword")) {
                mErrEl.html("@{R=APM.Strings;K=APMWEBJS_VB1_164;E=js}");
                return false;
            }

            var credList = $("select[id$='CredentialSetList']:first option");
            var nameExists = false;
            $.each(credList,
                function (index, option) {
                    if ($.trim($(option).text()) == $.trim(text("_CredentialName"))) {
                        nameExists = true;
                    }
                });
            if (nameExists) {
                mErrEl.html("@{R=APM.Strings;K=APMWEBJS_VB1_165;E=js}");
                return false;
            }
        }
        return true;
    }
    $("[id$='_TestButton']").click(function () {
        return validate("test");
    });
    $("[id$='_imgbNext']").click(function () {
        return validate("next");
    });
}

Ext.onReady(
    function() {
        if (SW.APM.AssignApplicationsAddNodeWizard.state) {
            new SW.APM.AssignApplicationsAddNodeWizard();
            var step = SW.APM.AssignApplicationsAddNodeWizard.state.step;
            if (step == "tpl") { window.$aa = new SW.APM.AssignApplicationsAddNodeWizard.Template(); }
        }
    }
);

