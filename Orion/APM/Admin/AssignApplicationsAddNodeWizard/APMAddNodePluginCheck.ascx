﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.APM.Web.Controls.ApmAddNodePluginCheckBase" %>

<style type="text/css">
	.apm_WizardContainer { width: 785px; }
</style>
<div class="GroupBox" style="margin-top: -15px;">
	<div class="apm_WizardContainer">
		<span class="ActionName"><asp:Literal id="lblTitle" runat="server" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_279 %>" /><asp:Literal Text="<%#NodeIpLiteralText%>" runat="server" ID="NodeIpLiteral"/></span>
        <br /><br />
	    <asp:Literal id="lblMessage" runat="server" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_280 %>"/>
        <br /><br />
   </div>
</div>