using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.BlackBox;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Logging;

public partial class Orion_APM_Admin_Settings : System.Web.UI.Page
{
    protected readonly Log Log = new Log(typeof(Orion_APM_Admin_Settings));

    protected Dictionary<IWebPlugin, IBlackBoxConfigPlugin> mapPluginControls = new Dictionary<IWebPlugin, IBlackBoxConfigPlugin>();

    protected Config ConfigState
    {
        get { return this.ViewState.GetOrCreate<Config>("config"); }
        set { this.ViewState["config"] = value; }
    }

    private void Report(string message, Exception xcp = null)
    {
        this.Log.Error(message, xcp);
        var panelError = new Panel
            {
                CssClass = "error"
            };
        panelError.Controls.Add(new LiteralControl
        {
            Text = message
        });
        this.placeHolderPlugins.Controls.Add(panelError);
    }

    private void AddPluginControl(PlaceHolder parent, Control pluginControl)
    {
        var panel = new Panel()
            {
                CssClass = "pluginPanel"
            };
        panel.Controls.Add(pluginControl);
        parent.Controls.Add(panel);
    }

    protected override void OnInit(EventArgs e)
    {
        WebPluginManager.Instance.SupportedPlugins.ToList()
            .ForEach(plugin =>
                {
                    var pluginName = plugin.GetType().FullName;
                    try
                    {
                        var url = plugin.GetDataRetentionPluginUrl();
                        if (string.IsNullOrWhiteSpace(url))
                        {
                            return;
                        }
                        try
                        {
                            var control = this.Page.LoadControl(url);
                            this.mapPluginControls[plugin] = (IBlackBoxConfigPlugin)control;
                            this.AddPluginControl(this.placeHolderPlugins, control);
                        }
                        catch (Exception xcp)
                        {
                            var message = InvariantString.Format("failed to load resource '{0}' for plugin '{1}'", url, pluginName);
                            this.Report(message, xcp);
                        }
                    }
                    catch (Exception xcp)
                    {
                        var message = InvariantString.Format("failed to get edit resource url for plugin '{0}'", pluginName);
                        this.Report(message, xcp);
                    }
                });
        
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.Submit.AddEnterHandler(1);
            this.LoadConfig();
            this.RefreshData();
        }
    }

    private void RefreshData()
    {
        var conf = this.ConfigState;
        BlackBoxConfigPluginBase.PrintDays(this.RetentionDays, conf.DataRetention);
        BlackBoxConfigPluginBase.PrintDays(this.RetainDetail, conf.RetainDetail);
        BlackBoxConfigPluginBase.PrintDays(this.RetainHourly, conf.RetainHourly);
        BlackBoxConfigPluginBase.PrintDays(this.RetentionEventLogDays, conf.DataRetentionEventLog);
        BlackBoxConfigPluginBase.PrintDays(this.BaselineDataDurationDays, conf.BaselineDataCollectionDuration);

        this.mapPluginControls.Values.ToList().ForEach(pc => pc.RefreshData());       
    }

    private void StoreData()
    {
        var conf = this.ConfigState;
        conf.DataRetention = BlackBoxConfigPluginBase.ParseDays(this.RetentionDays);
        conf.RetainHourly = BlackBoxConfigPluginBase.ParseDays(this.RetainHourly);
        conf.RetainDetail = BlackBoxConfigPluginBase.ParseDays(this.RetainDetail);
        conf.DataRetentionEventLog = BlackBoxConfigPluginBase.ParseDays(this.RetentionEventLogDays);
        conf.BaselineDataCollectionDuration = BlackBoxConfigPluginBase.ParseDays(this.BaselineDataDurationDays);

        using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            businessLayer.EditConfig(conf);
        }

        this.mapPluginControls.Values.ToList().ForEach(pc => pc.SaveData());
        
        this.Response.Redirect("~/Orion/APM/Admin/Default.aspx");
    }

    protected void Default_Click(object sender, EventArgs e)
    {
        using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            this.ConfigState = (Config)businessLayer.RestoreDefaultConfig(this.ConfigState);            
        }
        this.mapPluginControls.Values.ToList().ForEach(pc => pc.RestoreDefaults());
        this.RefreshData();
    }

    protected void StoreData_Click(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            this.StoreData();
        }
    }

    private void LoadConfig()
    {
        using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            this.ConfigState = (Config)businessLayer.GetConfig(this.ConfigState);            
        }        

        // plugins data are loaded inside their control (see BlackBoxConfigPluginBase)        
    }

    protected void BaselineDataDurationDays_Validate(object source, ServerValidateEventArgs args)
    {
        int detailDays = 0, value = 0;
        if (int.TryParse(this.RetainDetail.Text, out detailDays) && int.TryParse(args.Value, out value))
        {
            args.IsValid = value <= detailDays;
        }
        else
        {
            // The invalid format is handled by CompareValidator
            args.IsValid = true;
        }
    }
}
