﻿using System;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_CreateNewTemplate : System.Web.UI.Page
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack) 
		{
			if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
			{
				Server.Transfer("~/Orion/Error.aspx?Message=You must be an Admin to access this page.");
				return;
			}

			using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				var id = bl.CreateNewApplicationTemplate();
				var url = ApmMasterPage.GetEditTemplatePageUrl(id, "mode=1");
				Response.Redirect(url);
			}
		}
	}

	#endregion
}