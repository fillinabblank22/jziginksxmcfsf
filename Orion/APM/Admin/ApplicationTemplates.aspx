﻿<%@ Page Title="<%$ Resources:APMWebContent,APMWEBDATA_VB1_60%>" Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" 
    AutoEventWireup="true" CodeFile="ApplicationTemplates.aspx.cs" 
    Inherits="Orion_APM_Admin_ApplicationTemplates" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes"%>
<%@ Import Namespace="SolarWinds.APM.Web.Extensions" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI" %>

<%@ Register Src="~/Orion/APM/Controls/IconHelpButton.ascx" TagPrefix="apm" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="apm" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/APM/Controls/NavigationTabBar.ascx" TagPrefix="apm" TagName="NavigationTabBar" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="c1" ContentPlaceHolderID="hPh" Runat="Server">
    <link rel="stylesheet" type="text/css" href="ExistingElements.css" />

    <apm:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include runat="server" File="OrionCore.js"/>
    <orion:Include Module="APM" runat="server" File="Admin/ViewTemplates.js"/>
    <orion:Include ID="RemoveTemplateFromGroupDlgInclude" runat="server" Module="APM" File="Admin/Js/RemoveTemplatesFromGroupsDialog.js"/>
    <orion:Include ID="TemplateAssignmentsDlgInclude" runat="server" Module="APM" File="Admin/Js/TemplateAssignmentsDialog.js"/>
    <orion:Include ID="TemplateAssignmentsGridInclude" runat="server" Module="APM" File="Admin/Js/Grids/TemplateAssignmentsGrid.js"/>
    <orion:Include ID="ApplicationMonitorsGridInclude" runat="server" Module="APM" File="Admin/Js/Grids/ApplicationMonitorsGrid.js"/>
    <orion:Include ID="UnassignTemplateDialogInclude" runat="server" Module="APM" File="Admin/Js/UnassignTemplateDialog.js"/>
    <orion:Include ID="GroupsGridInclude" runat="server" Module="APM" File="Admin/Js/Grids/GroupsGrid.js"/>
    <orion:Include ID="UnassignedNodesGridInclude" runat="server" Module="APM" File="Admin/Js/Grids/UnassignedNodesGrid.js"/>
    <script type="text/javascript">
        var thwackUserInfo = <%=_thwackUserInfo%>;
    </script>

    <style type="text/css">
			#assignedNodesBody
			{
				padding: 10px;
				height: 210px;
					overflow: auto;
			}    
			#assignedNodesBody #templateName            
			{
				font-weight: bold;
			}
			#assignedNodesBody table
			{
				padding: 5px 15px 5px 15px;
				width: 100%;
			}
			#gridCell div#Grid
			{
				width: 950px;
			}
			
			.x-grid-empty
            {
	            text-align: center;	
            }
            .tableWidth
            {
                width: 1200px;
                *width: 1610px;
            }
            div.tooltip-outer {
                z-index: 10000;
            }

			#ManageComponentsLink{
				padding: 2px 0px 2px 30px;
				background: transparent url(/Orion/APM/Images/Admin/small-template-component.gif) scroll no-repeat left center;
			}
    </style>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
	<a id="ManageComponentsLink" href="/Orion/APM/Admin/Components/Templates.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_51 %></a>
    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHConfigSettingsManageApplicationMonitorTemplates" ID="HelpButton" runat="server" />
</asp:Content>
<asp:Content ID="c2" ContentPlaceHolderID="cPh" Runat="Server">

	<%foreach (string setting in new string[] { "GroupByValue"}) { %>
		<input type="hidden" name="APM_Templates_<%=setting%>" id="APM_Templates_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("APM_Templates_" + setting)%>' />
	<%}%>


    <div class="tableWidth">		

	    <table width="100%" id="breadcrumb">
		    <tr>
			    <td>
				    <h1><%=Page.Title%></h1>
			    </td>
		    </tr>
	    </table>

        <apm:NavigationTabBar runat="server"></apm:NavigationTabBar>

        <div id="tabPanel" class="tab-top">

	        <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%" style="padding-right: 10px;">
		        <tr>
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="font-size: 11px; padding-bottom: 10px;width:75%;">
                                    <%= Resources.APMWebContent.APMWEBDATA_VB1_52 %>
                                </td>
                                <td align="right" style="padding-bottom:5px;">
                                    <label for="search"><%= Resources.APMWebContent.APMWEBDATA_VB1_53 %></label>
                                    <input id="search" type="text" value="<%=Request.QueryStringOrForm("search")%>" size="20" />
                                    <input id="doSearch" type="button" value="<%= Resources.APMWebContent.APMWEBDATA_VB1_54 %>" />			    
                                </td>
                            </tr>                    
                        </table>
                    </td>		        
		        </tr>
		        <tr valign="top" align="left">

			        <td style="padding-right: 10px; width: 10%;">
				        <div class="ElementGrouping">
					        <div class="apm_GroupSection" style="background-image:url('/Orion/APM/Images/Admin/bg.groupby.gif'); background-repeat: repeat-x;">
						        <div><%= Resources.APMWebContent.APMWEBDATA_VB1_55 %></div>
					        </div>
					        <ul class="GroupItems" ></ul>
				        </div>

                        <!-- The Thwack Advertisement -->
                        <div id="apm_ThwackAd" style="width: 180px;">
                            <div>
                                <a href="http://thwack.com/r.ashx?7"><img src="<%= LocalizationUrlHelper.GetUrlFromTemplate("/Orion/APM/images/admin/[lang]/thwack_share.gif") %>" alt="" /></a>                                               
                            </div>
                            <p><%= Resources.APMWebContent.APMWEBDATA_VB1_56 %></p>
                           
                            <span class="LinkArrow">&#0187;</span> <a href="http://thwack.com/r.ashx?7"><%= Resources.APMWebContent.APMWEBDATA_VB1_56 %></a>
                        </div>

			        </td>
			        <td id="gridCell" style="width: 100%">
                        <div id="Grid"/>
			        </td>
		        </tr>
	        </table>
        	
	        <div id="ApmStatusIconPaths">
	            <%foreach (ApmStatus status in ApmStatus.GetValues()) { %>
		            <input type="hidden" name="ApmStatus_<%=(int)status.Value%>" value='<%=status.ToString("smallappimgpath", null)%>' />
                <%}%>
            </div>      
              
        	
	        <div id="assignedNodesDialog" class="x-hidden">
	            <div class="x-window-header"><%= Resources.APMWebContent.APMWEBDATA_VB1_57 %></div>
	            <div id="assignedNodesBody" class="x-panel-body">
	                <p><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_58, string.Format(Resources.APMWebContent.APMWEBDATA_VB1_59, "<span id=\"templateName\">", "</span>")) %></p>
	                <table>
	                </table>
	            </div>
	        </div>

	        <div id="originalQuery"></div>	
	        <div id="test"></div>
	        <pre id="stackTrace"></pre>
	    </div>
	</div>
</asp:Content>

