﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="Finish.aspx.cs" Inherits="Orion_APM_Admin_Templates_Assign_Finish" %>

<%@ Register Src="~/Orion/APM/Controls/AssignComponentsFinished.ascx" TagPrefix="apm" TagName="AssignComponentsFinished" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <apm:AssignComponentsFinished runat="server" ID="finishedMessage" />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_29 %>" DisplayType="Secondary" OnClick="OnCreateMoreApplications"/>
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Done" DisplayType="Primary" OnClick="OnDone" CausesValidation="false"/>
    </div>    
   
</asp:Content>

