﻿using System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.AssignApps;

public partial class Orion_APM_Admin_Templates_Assign_Finish : AssignTemplatesPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        finishedMessage.DataSource = Workflow.CreatedApplications;
        finishedMessage.DataBind();

        if (!IsPostBack)
        {
            imgbCancel.AddEnterHandler(0);
        }
    }

    protected void OnCreateMoreApplications(object sender, EventArgs e)
    {
        ResetSession();
        Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
    }

    protected void OnDone(object sender, EventArgs e)
    {
        ResetSession();
        Response.Redirect("/Orion/Apm/Summary.aspx", true);
    }

}
