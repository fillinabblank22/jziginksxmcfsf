﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelectCredentialsAndTest.aspx.cs" Inherits="Orion_APM_Admin_Templates_Assign_SelectCredentialsAndTest" MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master" %>

<%@ Register TagPrefix="apm" TagName="CredSetter" Src="~/Orion/APM/Admin/CredentialSetter.ascx" %>
<%@ Register TagPrefix="apm" TagName="CredsTip" Src="~/Orion/APM/Controls/CredentialTips.ascx" %>
<%@ Register TagPrefix="apm" TagName="CheckDuplicates" Src="~/Orion/APM/Admin/CheckDuplicates.ascx" %>

<asp:Content ID="c1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <table width="100%">
        <tr>
            <td valign="top">
                <apm:CredSetter ID="ctrCoreCredSetter" CredentialOwner="Core" runat="server"/>
                <apm:CredSetter ID="ctrApmCredSetter" CredentialOwner="Apm" runat="server"/>
            </td>
            <td width="1%" valign="top">
                <apm:CredsTip ID="ctrCredsTip" BackgroundColor="#FFFDCC" BorderColor="#EACA7F" runat="server"/>
                <apm:CheckDuplicates ID="ctrCheckDuplicates" runat="server"/>
            </td>
        </tr>
    </table>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="b1" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack_Click"/>
        <orion:LocalizableButton ID="b2" runat="server" Text="<%$ Resources : APMWebContent , APMWEBDATA_AK1_70%>" DisplayType="Primary" OnClick="OnNext_Click" OnClientClick="if (IsDemoMode()) return DemoModeMessage();"/>
        <orion:LocalizableButton ID="b3" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click" CausesValidation="false"/>
    </div>
</asp:Content>