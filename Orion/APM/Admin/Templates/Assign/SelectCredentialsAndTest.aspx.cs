﻿using System;
using System.Linq;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.UI.AssignApps;

public partial class Orion_APM_Admin_Templates_Assign_SelectCredentialsAndTest : AssignTemplatesPageBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ctrCoreCredSetter.SelectedTemplatesInfo = Workflow.SelectedTemplates
            .Select(item => new SelectNodeTreeItem(item.ID, item.Name))
            .ToList();
        ctrCoreCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;

        ctrApmCredSetter.SelectedTemplatesInfo = Workflow.SelectedTemplates
            .Select(item => new SelectNodeTreeItem(item.ID, item.Name))
            .ToList();
        ctrApmCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;
    }

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ctrCheckDuplicates.SkipDuplicates = Workflow.SkipDuplicates;
        }

        ctrCheckDuplicates.NodeIDs = Workflow.SelectedNodesId;
        ctrCheckDuplicates.TemplateIDs = Workflow.SelectedTemplates.ConvertAll(tpl => tpl.ID);

        b2.AddEnterHandler(0);
    }

    protected void OnBack_Click(Object sender, EventArgs e)
    {
        Workflow.SkipDuplicates = ctrCheckDuplicates.SkipDuplicates;

        ClearSession();
        GotoPreviousPage();
    }

    protected void OnNext_Click(Object sender, EventArgs e)
    {
        Workflow.SkipDuplicates = ctrCheckDuplicates.SkipDuplicates;

        if (ctrCoreCredSetter.IsValid && ctrApmCredSetter.IsValid)
        {
            if (ctrCoreCredSetter.Visible)
            {
                Workflow.UseCredentials(ctrCoreCredSetter.SelectedNodesInfo);
            }

            if (ctrApmCredSetter.Visible)
            {
                Workflow.UseCredentials(ctrApmCredSetter.SelectedNodesInfo);
            }

            Workflow.CreateApplications();

            ClearSession();
            GotoNextPage();
        }
    }

    protected void OnCancel_Click(Object sender, EventArgs e)
    {
        ClearSession();
        CancelWizard();
    }

    private void ClearSession()
    {
        ctrCoreCredSetter.ClearSession();
        ctrApmCredSetter.ClearSession();
    }
}