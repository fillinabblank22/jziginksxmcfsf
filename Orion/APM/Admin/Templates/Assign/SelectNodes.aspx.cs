﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.UI.AssignApps;

public partial class Orion_APM_Admin_Templates_Assign_SelectNodes : AssignTemplatesPageBase
{
    public static readonly String TestSessionKeyAssignTpl = "KeyTR_{0}"
        .FormatInvariant("orion_apm_admin_templates_assign_selectcredentialsandtest_aspx".ToLowerInvariant().GetHashCode().ToString(System.Globalization.CultureInfo.InvariantCulture).Replace('-', 'F'));

    protected void Page_Load(object sender, EventArgs e)
    {
        Session.RemoveThatStartsWith(TestSessionKeyAssignTpl);

        if (!IsPostBack)
        {
            string postData = Server.UrlDecode(Request.Form["postData"]);
            if (postData != null)
            {
                Workflow.HasStarted = true;

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<int> templateIds = serializer.Deserialize<List<int>>(postData);
                Workflow.SelectedTemplates = new TemplateInfoList(templateIds);
            }
            else if (Workflow.SelectedNodesInfo != null && Workflow.SelectedNodesInfo.Count > 0)
            {
                this.SearchableNodeSelection.StoreSelectedNodes(Workflow.SelectedNodesInfo);
            }
            else
            {
                // This page is only valid if we came from the application template page.  We'll quit this
                // "wizard" and go back to the app template page
                Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
            }

            imgbNext.AddEnterHandler(0);
        }
    }

    protected void OnNext(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Workflow.SelectedNodesInfo = SearchableNodeSelection.GetSelectedNodes();
            //Workflow.SelectedNodes = SearchableNodeSelection.GetSelectedNodeInfo();
            GotoNextPage();
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected void valAtLeastOneNode_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = SearchableNodeSelection.GetSelectedNodes().Count > 0;
    }
}