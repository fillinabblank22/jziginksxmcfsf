﻿Ext.namespace('SW');
Ext.namespace('SW.APM');

SW.APM.templates = function () {

	var getGroupByValues = function (groupBy, onSuccess) {
		ORION.callWebService("/Orion/APM/Services/ThwackAPM.asmx",
                             "GetValuesAndCountForProperty", { property: groupBy },
                             function (result) {
                             	onSuccess(ORION.objectFromDataTable(result));
                             });
	};

	var importTemplates = function (ids, onSuccess) {
		ORION.callWebService("/Orion/APM/Services/ThwackAPM.asmx",
                             "ImportTemplates", { Ids: ids },
                             function (result) {
                             	onSuccess(result);
                             });
	};


	var loadGroupByValues = function () {
		var groupItems = $(".GroupItems").text("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");

		getGroupByValues("tags", function (result) {
			groupItems.empty();
			$(result.Rows).each(function () {
				var value = this.theValue;
				var text = $.trim(String(this.theValue));
				if (text.length > 22) { text = String.format("@{R=APM.Strings;K=APMWEBJS_VB1_128;E=js}", text.substr(0, 22)); };
				var disp = (String.format("{0} ({1})", text, this.theCount) || String.format("@{R=APM.Strings;K=APMWEBJS_VB1_129;E=js}", this.theCount));
				if (text === "") { disp = String.format("@{R=APM.Strings;K=APMWEBJS_VB1_129;E=js}", this.theCount); }
				$("<a href='#'></a>")
					.attr('value', value).attr('title', value)
					.text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='GroupItem'/>");
			});

			var toSelect = $(".GroupItem a[value='" + ORION.Prefs.load("GroupByValue") + "']");

			if (toSelect.length === 0) {
				toSelect = $(".GroupItem a:first");
			}

			toSelect.click();
		});
	};

	var selectGroup = function () {
		$(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");

		var value = $(".SelectedGroupItem a").attr('value');

		ORION.Prefs.save('GroupByValue', value);

		refreshObjects("tags", value, "");
		return false;
	};

	var currentSearchTerm = null;
	var refreshObjects = function (property, value, search, callback) {
		grid.store.removeAll();
		grid.store.proxy.conn.jsonData = { property: property, value: value || "", search: search };
		currentSearchTerm = search;
		grid.store.load({ callback: callback });
	};

	var getSelectedTemplateIds = function (items) {
		var ids = [];

		Ext.each(items, function (item) {
			ids.push(item.data.Id);
		});

		return ids;
	};

	var importSelectedTemplates = function (items) {
		var selected = getSelectedTemplateIds(items);
		var waitMsg = Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_VB1_130;E=js}");

		importTemplates(selected, function (result) {
			waitMsg.hide();

			var dlg, obj = Ext.decode(result);
			function onClose() { dlg.hide(); dlg.destroy(); dlg = null; }

			if (obj.isSuccess) {
				function onView() {
					onClose(); Ext.Msg.wait("@{R=APM.Strings;K=APMWEBJS_VB1_131;E=js}");
					ORION.callWebService("/Orion/Services/NodeManagement.asmx",
						"SaveUserSetting", { name: "APM_Templates_GroupByValue", value: "@{R=APM.Strings;K=APMWEBJS_VB1_132;E=js}" },
						function (result) { window.location = "/Orion/APM/Admin/ApplicationTemplates.aspx"; }
					);
				}

				dlg = new Ext.Window({
					title: "@{R=APM.Strings;K=APMWEBJS_VB1_133;E=js}", html: obj.msg,
					width: 400, autoHeight: true, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "padding:5px 0px 0px 10px;",
					buttons: [
						{ text: "@{R=APM.Strings;K=APMWEBJS_VB1_134;E=js}", handler: onView }, { text: "@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}", handler: onClose }
					]
				});
				dlg.show();
			} else {
				var dlg = new Ext.Window({
					title: "@{R=APM.Strings;K=APMWEBJS_VB1_133;E=js}", html: obj.msg,
					width: 350, autoHeight: true, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "color:#f00; padding:5px 0px 0px 10px;",
					buttons: [{ text: "@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}", handler: onClose}]
				});
				dlg.show();
			}
		});
	};
	
	function onImportClick() {
		if (IsDemoMode()) return APMjs.demoModeAction("ImportThwackTemplate");
		SW.APM.logInToThwack(importSelectedTemplates, grid.getSelectionModel().getSelections(), true);
	}

	var updateToolbarButtons = function () {
		var count = grid.getSelectionModel().getCount();
		var map = grid.getTopToolbar().items.map;

		var needsAtLeastOneSelected = (count === 0);

		map.ImportButton.setDisabled(needsAtLeastOneSelected);
	};

	// Column rendering functions

	function renderName(value, meta, record) {
		return String.format('<a href="#" class="templateName" value="{0}">{1}</a>', record.data.Id, value);
	}

	function renderTags(value, meta, record) {
		var tags;

		try {
			tags = Ext.decode(value);
		} catch (e) { }

		tags = tags || [];

		if (tags.length === 0) {
			return "@{R=APM.Strings;K=APMWEBJS_VB1_51;E=js}";
		}

		var links = [];
		for (var i = 0; i < tags.length; ++i) {
			links.push(String.format('<a href="#" class="tagLink">{0}</a>', tags[i]));
		}

		return links.join('@{R=APM.Strings;K=APMWEBJS_VB1_135;E=js}');
	}

	function jumpToTag(tagName) {
		$(".GroupItem a[value='" + tagName + "']").click();
	}

	var showDescriptionDialog;

	function showDescription(e) {

		var templateId = $(e.target).attr("value");
		var body = $("#templateDescriptionBody").text("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");

		ORION.callWebService("/Orion/APM/Services/ThwackAPM.asmx",
                             "GetTemplateDescription", { templateId: templateId },
                             function (result) {
                             	var desc = $.trim(result.description);
                             	desc = desc.length != 0 ? desc : "@{R=APM.Strings;K=APMWEBJS_VB1_137;E=js}";
                             	body.empty().html(String.format("@{R=APM.Strings;K=APMWEBJS_VB1_138;E=js}", '<div style="font-weight: bold;">', '</div>', desc));
                             });

		if (!showDescriptionDialog) {
			showDescriptionDialog = new Ext.Window({
				applyTo: 'templateDescriptionDialog',
				layout: 'fit',
				width: 500,
				height: 300,
				closeAction: 'hide',
				plain: true,
				resizable: false,
				modal: true,
				items: new Ext.Panel({
					applyTo: 'templateDescriptionBody',
					layout: 'fit',
					border: false
				}),
				buttons: [{
					text: '@{R=APM.Strings;K=APMWEBJS_VB1_45;E=js}',
					handler: function () { showDescriptionDialog.hide(); }
				}]
			});
		}

		// Set the window caption
		showDescriptionDialog.setTitle($(e.target).text());

		// Set the location
		showDescriptionDialog.alignTo(document.body, "c-c");
		showDescriptionDialog.show();

		return false;

	}


	ORION.prefix = "APM_Templates_Thwack_";

	var selectorModel;
	var dataStore;
	var grid;
	var pageSizeNum;

	return {
		init: function () {

			$("#Grid").click(function (e) {
				if ($(e.target).hasClass('tagLink')) {
					jumpToTag($(e.target).text());
					return false;
				}
				if ($(e.target).hasClass('templateName')) {
					showDescription(e);
					return false;
				}
			});

			pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '20'));

			selectorModel = new Ext.grid.CheckboxSelectionModel();
			selectorModel.on("selectionchange", updateToolbarButtons);

			dataStore = new ORION.WebServiceStore(
                                "/Orion/APM/Services/ThwackAPM.asmx/GetTemplatesPaged",
                                [
                                    { name: 'Id', mapping: 0 },
                                    { name: 'Title', mapping: 1 },
                                    { name: 'Link', mapping: 2 },
                                    { name: 'PubDate', mapping: 3 },
                                    { name: 'ViewCount', mapping: 5 },
                                    { name: 'Owner', mapping: 7 },
                                    { name: 'Tags', mapping: 8 },
                                    { name: 'HasView', mapping: 9 }
                                ],
                                "Title");

			grid = new Ext.grid.GridPanel({

				store: dataStore,

				columns: [selectorModel, {
					header: '@{R=APM.Strings;K=APMWEBJS_VB1_54;E=js}',
					width: 80,
					hidden: true,
					sortable: true,
					hideable: false,
					dataIndex: 'Id'
				}, {
					header: '@{R=APM.Strings;K=APMWEBJS_VB1_55;E=js}',
					width: 300,
					sortable: true,
					hideable: false,
					dataIndex: 'Title',
					renderer: renderName
				}, {
					header: '@{R=APM.Strings;K=APMWEBJS_VB1_139;E=js}',
					width: 100,
					sortable: true,
					hideable: false,
					dataIndex: 'Owner'
				}, {
					header: '@{R=APM.Strings;K=APMWEBJS_VB1_140;E=js}',
					width: 90,
					sortable: true,
					dataIndex: 'PubDate'
				}, {
					header: '@{R=APM.Strings;K=APMWEBJS_VB1_143;E=js}',
					width: 100,
					sortable: true,
					hidden: true,
					dataIndex: 'ViewCount'
				}, {
					header: '@{R=APM.Strings;K=APMWEBJS_VB1_144;E=js}',
					width: 90,
					sortable: false,
					hideable: true,
					dataIndex: 'HasView'
				}, {
					header: '@{R=APM.Strings;K=APMWEBJS_VB1_57;E=js}',
					width: 220,
					sortable: false,
					dataIndex: 'Tags',
					renderer: renderTags
				}],

				sm: selectorModel,

				viewConfig: {
					forceFit: false,
					deferEmptyText: true,
					emptyText: " "
				},

				width: 850,
				height: 500,
				stripeRows: true,

				tbar: [{
					id: 'ImportButton',
					text: '@{R=APM.Strings;K=APMWEBJS_VB1_68;E=js}',
					tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_69;E=js}',
					icon: '/Orion/APM/images/Icon.Import16x16.gif',
					cls: 'x-btn-text-icon',
					handler: onImportClick
				}, '-', {
					text: '@{R=APM.Strings;K=APMWEBJS_VB1_62;E=js}',
					icon: '/Orion/APM/Images/icon_scan_apps.gif',
					cls: 'x-btn-text-icon',
					handler: function () {
						window.location = "/Orion/APM/Admin/AutoAssign/SelectNodes.aspx";
					}
				}],

				bbar: new Ext.PagingToolbar({
					store: dataStore,
					pageSize: pageSizeNum,
					displayInfo: true,
					displayMsg: '@{R=APM.Strings;K=APMWEBJS_VB1_76;E=js}',
					emptyMsg: "@{R=APM.Strings;K=APMWEBJS_VB1_77;E=js}",
					items: [
                                    new Ext.form.Label({ text: '@{R=APM.Strings;K=APMWEBJS_TM0_21;E=js}', style: 'margin-left: 5px; margin-right: 5px' }),
                                    new Ext.form.ComboBox({
                                    	regex: /^\d*$/,
                                    	store: new Ext.data.SimpleStore({
                                    		fields: ['pageSize'],
                                    		data: [[10], [20], [30], [40], [50], [100]]
                                    	}),
                                    	displayField: 'pageSize',
                                    	mode: 'local',
                                    	triggerAction: 'all',
                                    	selectOnFocus: true,
                                    	width: 50,
                                    	editable: false,
                                    	value: pageSizeNum,
                                    	listeners: {
                                    		'select': function (c, record) {
                                    			grid.bottomToolbar.pageSize = record.get("pageSize");
                                    			grid.bottomToolbar.cursor = 0;
                                    			ORION.Prefs.save('PageSize', grid.bottomToolbar.pageSize);
                                    			grid.bottomToolbar.doRefresh();
                                    		}
                                    	}
                                    })
                                ]
				})

			});

			grid.render('Grid');
			updateToolbarButtons();

			// Set the height of the Group Items list
			var fudgeFactor = 12;
			var a = $("#Grid").height();
			var b = $(".apm_GroupSection").height();
			var groupItemsHeight = $("#Grid").height() - $(".apm_GroupSection").height() - fudgeFactor;
			$(".GroupItems").height(groupItemsHeight);

			// Set thhe width of the grid
			grid.setWidth($('#gridCell').width());

			loadGroupByValues();

			$("form").submit(function () { return false; });

			$("#search").keyup(function (e) {
				if (e.keyCode == 13) {
					$("#doSearch").click();
				}
			});

			$("#doSearch").click(function () {
				var search = $("#search").val();
				if ($.trim(search).length == 0) return;

				$(".GroupItem").removeClass("SelectedGroupItem");

				refreshObjects("", "", search);
			});

			// highlight the search term (if we have one)            
			grid.getView().on("refresh", function () {
				var search = currentSearchTerm;
				if ($.trim(search).length == 0) return;

				var columnsToHighlight = [2, 3, 9];
				Ext.each(columnsToHighlight, function (columnNumber) {
					SW.APM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
				});
			});

			// Set the grid's empty text based on the data returned from the server.
			dataStore.on("load", function (store, records, options) {
				var t = dataStore.reader.jsonData.d.EmptyText;
				$("#Grid div.x-grid-empty").text(t);
			});
		}
	};

} ();

Ext.onReady(SW.APM.templates.init, SW.APM.templates);





