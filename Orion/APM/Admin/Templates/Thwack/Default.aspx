﻿<%@ Page Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_225%>" Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" 
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_APM_Admin_Templates_Thwack_Default" %>

<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="apm" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/APM/Controls/NavigationTabBar.ascx" TagPrefix="apm" TagName="NavigationTabBar" %>

<asp:Content ID="c1" ContentPlaceHolderID="hPh" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../../ExistingElements.css" />

    <apm:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
		
    <orion:Include runat="server" File="js/OrionCore.js"/>

    <orion:Include runat="server" Module="APM" File="Admin/Templates/Thwack/ViewThwackTemplates.js" />
    <script type="text/javascript">
        var thwackUserInfo = <%=_thwackUserInfo%>;
    </script>
    <style type="text/css">
#assignedNodesBody
{
	padding: 10px;
	height: 210px;
    overflow: auto;
}    
            
#assignedNodesBody #templateName            
{
	font-weight: bold;
}

#assignedNodesBody table
{
	padding: 5px 15px 5px 15px;
	width: 100%;
}

.x-grid-empty
{
	text-align: center;	
}
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHthwackImport" ID="HelpButton" runat="server" />
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="cPh" Runat="Server">
	
	<%foreach (string setting in new string[] { "GroupByValue"}) { %>
		<input type="hidden" name="APM_Templates_Thwack_<%=setting%>" id="APM_Templates_Thwack_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("APM_Templates_Thwack_" + setting)%>' />
	<%}%>

    <div class="tableWidth">		

	    <table width="100%" id="breadcrumb">
		    <tr>
			    <td>
				    <h1><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_226, "<img src='/Orion/APM/images/logo_thwack_small.gif' />")%></h1>
			    </td>
		    </tr>
	    </table>
    		
        <apm:NavigationTabBar ID="NavigationTabBar2" runat="server" Visible="true"></apm:NavigationTabBar>

        <div id="tabPanel" class="tab-top">

            <table class="ExistingElements" cellpadding="0" cellspacing="0" style="padding-right: 10px; width: 100%">
                <tr>
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="font-size: 11px; padding-bottom: 10px;">
                                    <%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_227, "<a href='http://thwack.com/r.ashx?7' style=\"text-decoration:underline;\">", "</a>")%>
                                </td>
                                <td align="right" style="padding-bottom:5px;">
                                    <label for="search"><%= Resources.APMWebContent.APMWEBDATA_VB1_228 %></label>
                                    <input id="search" type="text" size="20" />
                                    <input id="doSearch" type="button" value="<%= Resources.APMWebContent.APMWEBDATA_VB1_54 %>" />			    
                                </td>
                            </tr>                    
                        </table>
                    </td>
                </tr>
                
                <tr valign="top" align="left">
	                <td style="padding-right: 10px;width: 180px;">
		                <div class="ElementGrouping">
			                <div class="apm_GroupSection">
				                <div><%= Resources.APMWebContent.APMWEBDATA_VB1_229 %></div>
			                </div>
			                <ul class="GroupItems" style="width:180px;"></ul>
		                </div>
	                </td>
	                <td id="gridCell">
                        <div id="Grid"/>
	                </td>
                </tr>            
            </table>

            <div id="originalQuery"></div>	
            <div id="test"></div>
            <pre id="stackTrace"></pre>
        </div>	        
    </div>	        
    
	<div id="templateDescriptionDialog" class="x-hidden">
	    <div class="x-window-header"><%= Resources.APMWebContent.APMWEBDATA_VB1_230 %></div>
	    <div id="templateDescriptionBody" class="x-panel-body" style="padding: 10px;height: 210px;overflow: auto;">
	        
	    </div>
	</div>
	
</asp:Content>

