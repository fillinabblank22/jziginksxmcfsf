﻿using SolarWinds.APM.Web.Utility;
using System;

public partial class Orion_APM_Admin_Templates_Thwack_Default : System.Web.UI.Page
{
	protected String _thwackUserInfo = JsHelper.Serialize(new { name = String.Empty, pass = String.Empty });

	protected void Page_Load(object sender, EventArgs e)
	{
		var nCred = Session["ThwackCredential"] as System.Net.NetworkCredential;
		if (nCred != null)
		{
			var uiCred = new
			{
				name = nCred.UserName,
				pass = nCred.Password
			};
			_thwackUserInfo = JsHelper.Serialize(uiCred);
		}
	}
}