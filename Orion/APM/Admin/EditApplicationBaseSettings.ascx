<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationBaseSettings.ascx.cs" 
    Inherits="Orion_APM_Admin_EditApplicationBaseSettings" %>

<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxCTK" %>

<style type="text/css">
    table#appMainProp .label {
        width: 255px;
    }
</style>

<table cellpadding="0" cellspacing="0">
	<tr style="display: <%= this.FrequencySettingDisplayStyle%>;">
		<td class="edit label"><%= Resources.APMWebContent.APMWEBDATA_VB1_94 %></td>
		<td class="edit number">
			<span id="editFrequencySpan" runat="server" style="white-space: nowrap;">
				<asp:TextBox ID="frequencyEdit" Text="@@@" runat="server" Columns="10"></asp:TextBox><%= Resources.APMWebContent.APMWEBDATA_VB1_95 %>
			</span>
			<span id="viewFrequencySpan" runat="server" style="white-space: nowrap;">
				<asp:Literal ID="frequencyEditTemplateValue" runat="server"></asp:Literal><%= Resources.APMWebContent.APMWEBDATA_VB1_95 %>
			</span>
		</td>
		<td>
			<asp:HiddenField ID="frequencyState" runat="server" />
			<asp:HiddenField ID="frequencyRequired" runat="server" />
			<asp:HiddenField ID="frequencyValidate" runat="server" />
			<a href="#" id="frequencyOverrideLink" runat="server"></a>
			&nbsp;
		</td>
		<td>
			<asp:RangeValidator ID="frequencyValueValidate" runat="server" ControlToValidate="frequencyEdit"
				Display="Dynamic" Type="Integer" MaximumValue='<%# Int32.MaxValue %>' MinimumValue='<%# ApplicationBase.MinPollingFrequencyValue.TotalSeconds %>'
				ErrorMessage="<%# FrequencySettingRangeErrorMessage %>" SetFocusOnError="true"></asp:RangeValidator>
			<asp:RequiredFieldValidator ID="requireFrequency" runat="server" SetFocusOnError="true" 
				ControlToValidate="frequencyEdit" Display="Dynamic" ErrorMessage="<%$ Resources : APMWebContent, APMWEBDATA_VB1_96 %>"
				ControlToCompare="frequencyValidate" TriggerValue="True"></asp:RequiredFieldValidator>		
		</td>
	</tr>
	<tr style="display: <%= this.TimeoutSettingDisplayStyle %>;">
		<td class="edit label"><%= Resources.APMWebContent.APMWEBDATA_VB1_97 %></td>
		<td class="edit number">
			<span id="editTimeoutSpan" runat="server" style="white-space: nowrap;">
				<asp:TextBox ID="timeoutEdit" Text="@@@" runat="server" Columns="10"></asp:TextBox><%= Resources.APMWebContent.APMWEBDATA_VB1_95 %>
			</span>
			<span id="viewTimeoutSpan" runat="server" style="white-space: nowrap;">
				<asp:Literal ID="timeoutEditTemplateValue" runat="server"></asp:Literal><%= Resources.APMWebContent.APMWEBDATA_VB1_95 %>
			</span>
		</td>
		<td>
			<asp:HiddenField ID="timeoutState" runat="server" />
			<asp:HiddenField ID="timeoutRequired" runat="server" />
			<asp:HiddenField ID="timeoutValidate" runat="server" />
			<a href="#" id="timeoutOverrideLink" runat="server"></a>
			&nbsp;
		</td>
		<td>
			<asp:RangeValidator ID="timeoutValueValidate" runat="server" ControlToValidate="timeoutEdit"
				Display="Dynamic" Type="Integer" MaximumValue='<%# Int32.MaxValue %>' MinimumValue="1"
				ErrorMessage="<%$ Resources : APMWebContent, APMWEBDATA_VB1_99 %>" SetFocusOnError="true"></asp:RangeValidator>
			<asp:RequiredFieldValidator ID="requireTimeout" runat="server" SetFocusOnError="true"
				ControlToValidate="timeoutEdit" Display="Dynamic" ErrorMessage="<%$ Resources : APMWebContent, APMWEBDATA_VB1_98 %>"
				ControlToCompare="timeoutValidate" TriggerValue="True"></asp:RequiredFieldValidator> 
		</td>
	</tr>
	</table>
	
<div style="padding-top: 10px">
<asp:ImageButton id="ExpandCollapseButton" ImageUrl="~/Orion/APM/Images/Button.Expand.gif" runat="server" ImageAlign="left" CausesValidation="false" />
<span style="padding-left:3px;"><b><%= Resources.APMWebContent.APMWEBDATA_VB1_100 %></b><br/></span>

</div>

<div style="padding-top:5px;">
<asp:Panel ID="ContentPanel" runat="server"> 
    <table cellpadding="0" cellspacing="0">
        <tr>
		<td class="edit label indended"><%= Resources.APMWebContent.APMWEBDATA_VB1_101 %></td>
		<td class="edit number">
			<span id="editDebugLoggingSpan" runat="server">
			    <asp:DropDownList ID="debugLoggingEnabledEdit" runat="server">
			        <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_102 %>" Value="True"></asp:ListItem>
			        <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_103 %>" Value="False"></asp:ListItem>
			    </asp:DropDownList>
				
			</span>
			<span id="viewDebugLoggingSpan" runat="server">
				<asp:Literal ID="debugLoggingEnabledTemplateValue" runat="server"></asp:Literal>
			</span>
		</td>
		<td>
			<asp:HiddenField ID="debugLoggingEnabledState" runat="server" />
			<asp:HiddenField ID="debugLoggingEnabledRequired" runat="server" />
			<asp:HiddenField ID="debugLoggingEnabledValidate" runat="server" />
			<a href="#" id="debugLoggingEnabledOverrideLink" runat="server"></a>
			&nbsp;
		</td>
		<td>
		</td>
	</tr>
	
	<tr style="">
		<td class="edit label indended"><%= Resources.APMWebContent.APMWEBDATA_VB1_104 %></td>
		<td class="edit number">
			<span id="editNumberOfLogFilesSpan" runat="server">
				<asp:TextBox ID="numberOfLogFilesEdit" Text="@@@" runat="server" Columns="10"></asp:TextBox>
                <br/>
			</span>
			<span id="viewNumberOfLogFilesSpan" runat="server">
				<asp:Literal ID="numberOfLogFilesTemplateValue" runat="server"></asp:Literal>
			</span>						
		    <span style="font-size:7pt;"><%= Resources.APMWebContent.APMWEBDATA_VB1_105 %></span>
		</td>
		<td>
			<asp:HiddenField ID="NumberOfLogFilesState" runat="server" />
			<asp:HiddenField ID="NumberOfLogFilesRequired" runat="server" />
			<asp:HiddenField ID="NumberOfLogFilesValidate" runat="server" />
			<a href="#" id="NumberOfLogFilesOverrideLink" runat="server"></a>
			&nbsp;
		</td>
		<td>
			<asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="numberOfLogFilesEdit"
				Display="Dynamic" Type="Integer" MaximumValue='<%# Int32.MaxValue %>' MinimumValue="1"
				ErrorMessage="<%$ Resources : APMWebContent, APMWEBDATA_VB1_106 %>" SetFocusOnError="true"></asp:RangeValidator>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" SetFocusOnError="true"
				ControlToValidate="numberOfLogFilesEdit" Display="Dynamic" ErrorMessage="<%$ Resources : APMWebContent, APMWEBDATA_VB1_106 %>"
				ControlToCompare="debugLoggingEnabledEdit" TriggerValue="True"></asp:RequiredFieldValidator> 
		</td>
	</tr>
	
    <tr>
		<td class="edit label indended"><%= Resources.APMWebContent.APMWEBDATA_VB1_107 %></td>
		<td class="edit number">
			<span id="editUse64BitSpan" runat="server">
			    <asp:DropDownList ID="use64BitEdit" runat="server">
			        <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_108 %>" Value="False"></asp:ListItem>
			        <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_109 %>" Value="True"></asp:ListItem>
			    </asp:DropDownList>
				
			</span>
			<span id="viewUse64BitSpan" runat="server">
				<asp:Literal ID="use64BitTemplateValue" runat="server"></asp:Literal>
			</span>
		</td>
		<td>
			<asp:HiddenField ID="use64BitState" runat="server" />
			<asp:HiddenField ID="use64BitRequired" runat="server" />
			<asp:HiddenField ID="use64BitValidate" runat="server" />
			<a href="#" id="use64BitOverrideLink" runat="server"></a>
			&nbsp;
		</td>
		<td>
		</td>
	</tr>

</table>

</asp:Panel>
</div>    

<ajaxCTK:CollapsiblePanelExtender ID="cpe" runat="Server" 
            TargetControlID="ContentPanel"
            ExpandControlID="ExpandCollapseButton" 
            CollapseControlID="ExpandCollapseButton" 
            Collapsed="True"
            ExpandedText="<%$ Resources : APMWebContent , APMWEBDATA_VB1_100 %>" 
            CollapsedText="<%$ Resources : APMWebContent , APMWEBDATA_VB1_100 %>"
            ImageControlID="ExpandCollapseButton" 
            ExpandedImage="~/Orion/images/Button.Collapse.gif" 
            CollapsedImage="~/Orion/images/Button.Expand.gif"
            SuppressPostBack="true">
    </ajaxCTK:CollapsiblePanelExtender>

    
