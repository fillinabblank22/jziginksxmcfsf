<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdminPage.master" AutoEventWireup="true" CodeFile="ViewsByApplicationType.aspx.cs" 
    Inherits="Orion_APM_Admin_ViewsByApplicationType" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_209 %>" %>
<%@ Import Namespace="SolarWinds.APM.Web.Models" %>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">   
    <h1><%= Page.Title %></h1>
    <span style="font-size: 11px;"><%= Resources.APMWebContent.APMWEBDATA_VB1_208 %></span>

    <br />
    <br />
        
    <div class="ResourceWrapper" style="width: 700px;">
        <table id="apmSettings">
            <tr>
                <td><b><%= Resources.APMWebContent.APMWEBDATA_VB1_210 %></b></td>
                <td><b><%= Resources.APMWebContent.APMWEBDATA_VB1_211 %></b></td>
            </tr>
            <asp:Repeater runat="server" ID="rptNodes">
                <ItemTemplate>
                    <tr>
                        <td><%# HttpUtility.HtmlEncode(((ApplicationTemplateInfo)Container.DataItem).Name) %></td>
                        <td>
                            <asp:ListBox runat="server" OnDataBinding="AppTemplateViewsDataBinding" Rows="1" SelectionMode="single" Width="400px" >
                            </asp:ListBox>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td></td>
                <td>
                    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary"
                        OnClick="SubmitClick" />
                </td>
            </tr>
        </table>
     </div>
</asp:Content>

