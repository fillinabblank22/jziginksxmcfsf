﻿<%@ WebHandler Language="C#" Class="EditCertificateSetFileUploader" %>

using System;
using System.IO;
using System.Web;
using Resources;

public class EditCertificateSetFileUploader : IHttpHandler
{
    private const int maxFileLength = 100000;
    private const string errorResponse = "Error:";
    
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var file = context.Request.Files[0];
            if (string.IsNullOrEmpty(file.FileName))
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write(errorResponse + APMWebContent.Error_Please_select_a_certificate_file);
            }
            else if (file.ContentLength > maxFileLength)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write(errorResponse + APMWebContent.Error_File_too_big);              
            }
            else
            {
                var fileContentBytes = new byte[file.ContentLength];
                using (var stream = file.InputStream)
                {
                    stream.Read(fileContentBytes, 0, file.ContentLength);
                }
                var output = System.Text.Encoding.UTF8.GetString(fileContentBytes);
                HttpContext.Current.Response.ContentType = "text/plain";
                HttpContext.Current.Response.Write(output);         
            }
        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write(errorResponse + ex.Message); 
        }
    }

    public bool IsReusable => false;
}