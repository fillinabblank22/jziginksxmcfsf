﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Script.Serialization;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.AutoAssign;

using AutoAssignMaster = Orion_APM_Admin_AutoAssign_AutoAssign;

public partial class Orion_APM_Admin_AutoAssign_EnterCredentials : AutoAssignPageBase
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			this.InitData();

			btnNext.AddEnterHandler(1);
		}
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
		this.UpdateData();

		base.GotoPreviousPage();
	}

	protected void OnNext_Click(Object sender, EventArgs e)
	{
		if (this.Page.IsValid)
		{
			this.UpdateData();

			base.GotoNextPage();
		}
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		base.CancelWizard();
	}

	#endregion

	#region Override Members (SolarWinds.APM.Web.UI.AutoAssign.AutoAssignPageBase)

	protected override void InitData()
	{
		CheckProcessSatus(Workflow);

		(this.Master as AutoAssignMaster).SetClientState(Workflow.ToJson("cred"));
	}

	protected override void UpdateData()
	{
		if (string.IsNullOrEmpty(Request.Form["credentials"]))
		{
			Workflow.CredentialIDs.Clear();
		}
		else
		{
			Workflow.CredentialIDs = new JavaScriptSerializer().Deserialize<List<Int32>>(string.Format("[{0}]", Request.Form["credentials"]));
		}
	}

	#endregion
}
