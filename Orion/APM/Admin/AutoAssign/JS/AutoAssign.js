﻿Ext.ns("SW.APM");

SW.APM.AutoAssign = function() {
	var mState = SW.APM.AutoAssign.state, mEM = SW.APM.EventManager;

	function onStateChange(args) {
		Ext.apply(mState, args);

		mState.tplCount = mState.templates.length;

		$("#nodeCount").text(mState.nodeCount);
		$("#applicationCount").text(mState.tplCount);			
	}

	function ctor() {
		mEM.on("onCheckStateChange", onStateChange); onStateChange(mState);
	}; ctor();
}

/*Select Nodes Step*/
SW.APM.AutoAssign.Node = function() {
	var mState = SW.APM.AutoAssign.state;
	
	$("#applicationCountRow").hide();

	this.onNextClick = function() {
		if (mState.nodeCount == 0) { Ext.Msg.show({ title: "@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}", msg: "@{R=APM.Strings;K=APMWEBJS_VB1_2;E=js}", icon: Ext.MessageBox.ERROR, buttons: Ext.MessageBox.OK }); }
		return mState.nodeCount > 0;
	}
};

/*Select Templates Step*/
SW.APM.AutoAssign.Template = function () {
    var mState = SW.APM.AutoAssign.state, mEM = SW.APM.EventManager;
    var ROW_TPL = "<li class='GroupItem'>" + "<input type='checkbox' name='templates' value='{0}'/> " + "<img src='/Orion/APM/images/Admin/icon_application_template.gif'> " + "<a href='#' value='{0}'>{1}</a>" + "</li>";
    var SEL_ROW_TPL = "<li title='{0}'>" + "<table style='width: 100%'><td> " + "<div>&#8226; {0}<div> <input type='checkbox' checked='true' name='selectedTemplates' value='{1}' style='display:none;'/></td><td style='width: 20px;'><img src='/Orion/images/cmd-close.png' style='cursor:pointer;' title='@{R=APM.Strings;K=APMWEBJS_VB1_3;E=js}'></td></table>" + "</li>";

    function loadData() {
        var setTag = $("select[id$='tags']").val();
        ORION.callWebService("/Orion/Services/NodeManagement.asmx", "SaveUserSetting",
			{ name: "APM_AutoAssign_Template_SelectedTag", value: setTag }, Ext.emptyFn
		);

        var container = $("#templates").text("@{R=APM.Strings;K=APMWEBJS_VB1_4;E=js}");
        ORION.callWebService(
			"/Orion/APM/Services/Templates.asmx", "GetTemplatesForTagWithSkipTags", { tagName: setTag, skipTagNames: ["'User Experience'"], skipCustomAppTypes: true },
			function (json) {
			    container.empty();
                $(ORION.objectFromDataTable(json).Rows).each(function () {
                    var encodedTemplateName = _.escape(this.Name);
                    var rowEl = $(String.format(ROW_TPL, this.Id, encodedTemplateName)).appendTo(container);
			        if (mState.templates.indexOf(this.Id) > -1) {
			            rowEl.children("input").attr("checked", true);
			        }
			    });
			    $("li.GroupItem > input[type='checkbox']").click(onTplCheck);
			    $("li.GroupItem > a").click(onDetailsShow);

			    mEM.fire("onCheckStateChange", {});
			}
		);
    }
    function bindSelTpl() {
        var html = "";
        Ext.each(mState.templates, function (tplID) {
            Ext.each(mState.templatesInfo, function (tplInfo) {
                if (tplID == tplInfo.tplID) {
                    var encodedTemplateName = _.escape(tplInfo.name);
                    html += String.format(SEL_ROW_TPL, encodedTemplateName, tplInfo.tplID);
                }
            });
        });
        html = String.format("@{R=APM.Strings;K=APMWEBJS_VB1_5;E=js}", mState.tplCount, html);
        Ext.get("selectedTemplates").update(html);

        $("#selectedTemplates  img").click(onTplUnCheck);
    }

    function onTplUnCheck(evt) {
        var inEl = Ext.get(evt.target).parent("table").child("input").dom;
        inEl.checked = false;
        $(String.format("#templates input[type='checkbox'][value='{0}']", inEl.value)).attr("checked", false);
        return onTplCheck({ target: inEl });
    }
    function onTplCheck(evt) {
        var tplID = parseInt(evt.target.value); if (evt.target.checked) { mState.templates.push(tplID); } else { mState.templates.remove(tplID); }
        mEM.fire("onCheckStateChange", {});
        return true;
    }
    function onAccuracyChange(sender, val) {
        var vals = ["@{R=APM.Strings;K=APMWEBJS_VB1_6;E=js}", "@{R=APM.Strings;K=APMWEBJS_VB1_7;E=js}", "@{R=APM.Strings;K=APMWEBJS_VB1_8;E=js}", "@{R=APM.Strings;K=APMWEBJS_VB1_9;E=js}"]; $("input[id='accuracy']").val(val); $("#lblAccuracy").text(vals[val]);
        return true;
    }

    function onDetailsShow(evt) {
        evt.stopPropagation();
        function onSuccess(json) {
            if (!dlg) { return; }
            var cmpHTML = "<li style='color:#f00;'>@{R=APM.Strings;K=APMWEBJS_VB1_10;E=js}</li>";
            Ext.each(json.components, function (cmp, i) { if (i == 0) { cmpHTML = ""; } cmpHTML += String.format("<li>{0}</li>", cmp); });
            dlg.body.dom.innerHTML = "";
            dlg.items.add(new Ext.Panel({ frame: true, autoScroll: true, defaultType: "label", items: [{ html: (json.description.length != 0 ? json.description + "<br/>" : "@{R=APM.Strings;K=APMWEBJS_VB1_11;E=js}") }, { html: String.format("@{R=APM.Strings;K=APMWEBJS_VB1_12;E=js}", json.tags.join(", ")) }, { html: String.format("@{R=APM.Strings;K=APMWEBJS_VB1_13;E=js}", json.frequency) }, { html: String.format("@{R=APM.Strings;K=APMWEBJS_VB1_14;E=js}", json.timeout) }, { html: "@{R=APM.Strings;K=APMWEBJS_VB1_15;E=js}" }, { html: String.format("<ul class='components'>{0}</ul>", cmpHTML)}] }));
            dlg.doLayout(true);
        }
        var dlg = new Ext.Window({
            title: evt.target.innerHTML, html: "@{R=APM.Strings;K=APMWEBJS_VB1_16;E=js}", width: 650, height: 350,
            border: false, region: "center", layout: "fit", bodyStyle: "padding:5px;", modal: true, resizable: false, plain: true,
            listeners: function () {
                var em = Ext.EventManager, bd = document.body, onS = function () { if (!dlg) { return; } dlg.center(); };
                return {
                    show: function () { em.on(bd, "mousewheel", onS); em.on(window, "scroll", onS); },
                    hide: function () { em.un(bd, "mousewheel", onS); em.un(window, "scroll", onS); }
                };
            } (),
            buttons: [{ text: "@{R=APM.Strings;K=APMWEBJS_VB1_88;E=js}", handler: function () { dlg.hide(); dlg.destroy(); dlg = null; } }]
        });
        dlg.show();
        ORION.callWebService("/Orion/APM/Services/Templates.asmx", "GetTemplateDetails", { templateId: $(evt.target).attr("value") }, onSuccess);
        return false;
    }

    this.onNextClick = function () {
        if (mState.tplCount == 0) { Ext.Msg.show({ title: "@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}", msg: "@{R=APM.Strings;K=APMWEBJS_VB1_17;E=js}", icon: Ext.MessageBox.ERROR, buttons: Ext.MessageBox.OK }); }
        return mState.tplCount > 0;
    }

    function ctor() {
        var slider = new Ext.Slider({
            renderTo: "sliderContainer", width: 240, minValue: 0, maxValue: 3, increment: 1, value: mState.accuracy, listeners: { change: onAccuracyChange }
        });
        onAccuracyChange(null, mState.accuracy);

        $("select[id$='tags']").change(loadData).change();
        $("#detailsASS_rX").css("display", "none");
        mEM.on("onCheckStateChange", bindSelTpl);
    }; ctor();
};

/*Enter Credentials Step*/
SW.APM.AutoAssign.Credential = function () {
    var mState = SW.APM.AutoAssign.state, mEM = SW.APM.EventManager;
    var mData = null, mGrid = null;
    var ROW_EMPTY = String.format("@{R=APM.Strings;K=APMWEBJS_VB1_21;E=js}", "<div class='empty'>", "<a href='#' style='text-decoration:underline;'>", "</a>.</div>");
    var ROW_TPL = "<a href='#' id='aaCMU_{0}' value='{0}' title='@{R=APM.Strings;K=APMWEBJS_VB1_18;E=js}'><img id='aaCMU_{0}' src='/Orion/Discovery/images/icon_up.gif'/>@{R=APM.Strings;K=APMWEBJS_VB1_18;E=js}</a>&nbsp;&nbsp;" +
		"<a href='#' id='aaCMD_{0}' value='{0}' title='@{R=APM.Strings;K=APMWEBJS_VB1_19;E=js}'><img id='aaCMD_{0}' src='/Orion/Discovery/images/icon_down.gif'>@{R=APM.Strings;K=APMWEBJS_VB1_19;E=js}</img></a>&nbsp;&nbsp;" +
		"<a href='#' id='aaCD_{0}' value='{0}' title='@{R=APM.Strings;K=APMWEBJS_VB1_20;E=js}'><img id='aaCD_{0}' src='/Orion/Discovery/images/icon_delete.gif'>@{R=APM.Strings;K=APMWEBJS_VB1_20;E=js}</img></a>" +
		"<input type='checkbox' name='credentials' value='{0}' style='display:none;'/>";
    var NEW_CRED_ID = -1;
    var NODE_CRED_ID = -3;

    function loadData() {
        mGrid.loadMask.show();
        function onSuccess(data) {
            mData = data;
            mData.splice(0, 0, { Id: NODE_CRED_ID, Name: "@{R=APM.Strings;K=APMWEBJS_VB1_22;E=js}", UserName: "", Password: "********" });
            var items = []; Ext.each(mData, function (item, index) { if (mState.credentials.indexOf(item.Id) > -1) { items.push([index, item.Name, item.UserName, String.format(ROW_TPL, item.Id), item.IsUserNamePasswordCredential]); } });
            mGrid.getStore().loadData(items);

            if (mData.length < 1) {
                mGrid.getStore().loadData([[0, "", "", ""]]);
                Ext.get(mGrid.getView().getRow(0)).update(ROW_EMPTY).on("click", onAdd);
            }
            $("input[name='credentials']").attr("checked", true); mEM.fire("onCheckStateChange", {});
            mGrid.loadMask.hide();
            ORION.handleError = onErrorOrig;
        }
        var onErrorOrig = ORION.handleError;
        function onError(xhr) {
            onErrorOrig(xhr);
            mGrid.loadMask.hide();
            ORION.handleError = onErrorOrig;
        }
        ORION.handleError = onError;
        ORION.callWebService("/Orion/APM/Services/Credentials.asmx", "GetCredentials", { credentialIDs: [] }, onSuccess);
    }

    function onAdd(evt) {
        if (IsDemoMode()) return DemoModeMessage();
        Ext.QuickTips.init();
        var btnNext = $("input[id$='_wizardContent_btnNext']:enabled");

        var cbData = [];
        cbData.push([NEW_CRED_ID, "@{R=APM.Strings;K=APMWEBJS_VB1_23;E=js}"]);
        Ext.each(mData, function (item, index) { if (mState.credentials.indexOf(item.Id) < 0) { cbData.push([item.Id, item.Name, item.UserName, item.IsUserNamePasswordCredential]); } });

        function ctr(index) { return dlg.items.get(0).items.get(index); }
        function val(index) { return ctr(index).getValue().trim(); }
        function onSelect(owner, rec, index) {
            var id = rec.get("Id"), name = rec.get("Name"), userName = rec.get("UserName"), psw = "********";
            var pwdVisible = true;
            var isNew = (id == NEW_CRED_ID);
            if (isNew) {
                name = ""; userName = ""; psw = "";
            }
            else {
                pwdVisible = rec.get("IsUserNamePasswordCredential");
                if (pwdVisible) {
                    psw = "********";
                }
            }
            var layoutChanges = (ctr(4).isVisible() !== pwdVisible);
            ctr(3).setVisible(pwdVisible);
            ctr(4).setVisible(pwdVisible);
            ctr(5).setVisible(pwdVisible);

            dlg.setHeight(APM.IsRemoteMachine() && isNew ? 285 : 255);

            ctr(1).setValue(name); ctr(2).setValue(userName); ctr(4).setValue(psw); ctr(5).setValue(psw);
            ctr(1).setDisabled(!isNew); ctr(2).setDisabled(!isNew); ctr(4).setDisabled(!isNew); ctr(5).setDisabled(!isNew); dlg.buttons[0].setDisabled(isNew);

            if (APM.IsRemoteMachine()) {
                if (isNew) { ctr(6).show(); }
                else { ctr(6).hide(); }
                layoutChanges = true;
            }
            if (layoutChanges) {
                dlg.setHeight(dlg.getHeight());
                dlg.doLayout(true);
            }
        }

        function newCredSelected() {
            var selectedId = ctr(0).getValue();
            if (typeof (selectedId) == "number" && selectedId == NEW_CRED_ID) return true;
            return false;
        }

        function onValidateCredentialName(text) {
            if (!newCredSelected()) return true;
            var trimmedText = Ext.util.Format.trim(text);
            if (trimmedText == "") {
                return "@{R=APM.Strings;K=APMWEBJS_VB1_24;E=js}";
            }
            var nameExists = false;
            for (var i = 0; i < mData.length; i++) {
                if (trimmedText.toLowerCase() == mData[i].Name.toLowerCase()) {
                    return "@{R=APM.Strings;K=APMWEBJS_VB1_25;E=js}";
                }
            }
            return true;
        }

        function onValidateUserName(text) {
            if (!newCredSelected()) return true;
            var trimmedText = Ext.util.Format.trim(text);
            if (trimmedText == "") {
                return "@{R=APM.Strings;K=APMWEBJS_VB1_26;E=js}";
            }
            return true;
        }

        function onValidateConfirmPassword(text) {
            if (val(4) != val(5)) {
                return "@{R=APM.Strings;K=APMWEBJS_VB1_27;E=js}";
            }
            return true;
        }

        function onFormFieldChange(sender, newValue, oldValue) {
            var credentialNameValid = onValidateCredentialName(val(1)) == true;
            var userNameValid = onValidateUserName(val(2)) == true;
            var confirmPasswordValid = onValidateConfirmPassword(val(5)) == true;

            var isValid = credentialNameValid && userNameValid && confirmPasswordValid;
            dlg.buttons[0].setDisabled(!isValid);
        }

        function getWinItems() {
            var items = [
				{ xtype: "combo", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_28;E=js}", store: new Ext.data.SimpleStore({ fields: ["Id", "Name", "UserName", "IsUserNamePasswordCredential"], data: cbData }), valueField: "Id", displayField: "Name", mode: "local", tpl: '<tpl for="."><div class="x-combo-list-item">{Name:htmlEncode}</div></tpl>', typeAhead: false, triggerAction: "all", anchor: "95%", editable: false, selectOnFocus: true, listeners: { select: onSelect} },
				{ fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_29;E=js}", anchor: "95%", selectOnFocus: true, validator: onValidateCredentialName, enableKeyEvents: true, listeners: { 'keyup': onFormFieldChange }, msgTarget: 'side' },
				{ fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_30;E=js}", anchor: "95%", selectOnFocus: true, validator: onValidateUserName, enableKeyEvents: true, listeners: { 'keyup': onFormFieldChange }, msgTarget: 'side' },
				{ id: "label", xtype: "label", html: "@{R=APM.Strings;K=APMWEBJS_VB1_31;E=js}", style: "margin-left:110px;color:#999;" },
				{ fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_32;E=js}", inputType: "password", anchor: "95%", selectOnFocus: true, enableKeyEvents: true, listeners: { 'keyup': onFormFieldChange }, msgTarget: 'side' },
				{ fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_33;E=js}", inputType: "password", anchor: "95%", selectOnFocus: true, validator: onValidateConfirmPassword, enableKeyEvents: true, listeners: { 'keyup': onFormFieldChange }, msgTarget: 'side' }
			];

            if (APM.IsRemoteMachine()) {
                var warnHtml = "@{R=APM.Strings;K=APMWEBJS_VB1_34;E=js}";
                if (location.protocol.toLowerCase().indexOf("https") > -1) {
                    warnHtml += String.format("@{R=APM.Strings;K=APMWEBJS_VB1_35;E=js}", "navigate to <a href='/Orion/APM/Admin/EditCredentialSet.aspx?mode=Insert' target='_blank'>", "</a>");
                } else {
                    warnHtml += "@{R=APM.Strings;K=APMWEBJS_VB1_36;E=js}";
                }
                items.push({ xtype: "label", html: warnHtml, style: "color:#f00;" });
            }
            return items;
        };
        function onWinShow(pWnd) { btnNext.attr("disabled", true); $(pWnd.buttons[0].el.dom).attr("apm_enterhandler", "0"); }
        function onWinHide() { btnNext.attr("disabled", false); }
        function onSubmit() {
            var selID = parseInt(ctr(0).getValue());
            var selName = val(1);
            if (selID != NEW_CRED_ID) {
                mState.credentials.push(selID);
                mState.credCount++;
                onCancel();
                loadData();
            }
            else {
                ORION.callWebService("/Orion/APM/Services/Credentials.asmx", "CreateCredential",
                    { name: val(1), userName: val(2), psw: val(4) },
                    function (credID) {
                        mState.credentials.push(credID);
                        mState.credCount++;
                        onCancel();
                        loadData();
                    }
                );
            }
        }
        function onCancel() { onWinHide(); if (dlg) { dlg.hide(); dlg.destroy(); dlg = null; } }

        var dlg = new Ext.Window({
            title: "@{R=APM.Strings;K=APMWEBJS_VB1_37;E=js}", width: 400, autoHeight: true, border: false, region: "center", layout: "fit", modal: true, resizable: false, bodyStyle: "padding:5px;",
            items: [
				new Ext.form.FormPanel({
				    frame: true, labelWidth: 115, defaultType: "textfield", autoHeight: true, labelAlign: "right", items: getWinItems()
				})
			],
            buttons: [
                { text: "@{R=APM.Strings;K=APMWEBJS_VB1_38;E=js}", disabled: true, handler: onSubmit },
                { text: "@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}", handler: onCancel}],
            monitorValid: true,
            listeners: { show: onWinShow, hide: onWinHide }
        });
        ctr(0).setValue(NEW_CRED_ID);
        dlg.show();
        return false;
    }

    function onCellClick(owner, rowIndex, colIndex, evt) {
        function val(subIndex) { return store.getAt(rowIndex + subIndex).get("order"); }
        var aID = evt.getTarget().id;
        if (colIndex == 4 && aID) {
            var store = owner.getStore();
            if (aID.indexOf("aaCMU_") == 0) {
                if (rowIndex > 0) {
                    var top = val(-1), curr = val(0); store.getAt(rowIndex).set("order", top); store.getAt(rowIndex - 1).set("order", curr);
                }
            } else if (aID.indexOf("aaCMD_") == 0) {
                if (rowIndex + 1 < store.getCount()) {
                    var curr = val(0), bottom = val(1); store.getAt(rowIndex).set("order", bottom); store.getAt(rowIndex + 1).set("order", curr);
                }
            } else if (aID.indexOf("aaCD_") == 0) {
                store.remove(store.getAt(rowIndex));
                var creds = []; $("input[name='credentials']").each(function (index, el) { creds.push(parseInt(el.value)); });
                mEM.fire("onCheckStateChange", { credCount: creds.length, credentials: creds });
            }
            store.sort("order", "ASC");
            $("input[name='credentials']").attr("checked", true)
        }
        evt.stopEvent();
        return false;
    }

    function ctor() {
        var config = {
            renderTo: "credentials", width: 850, height: 250, border: false, frame: true, autoScroll: true,
            trackMouseOver: false, disableSelection: true, stripeRows: true, viewConfig: { forceFit: true, enableRowBody: true },
            store: new Ext.data.Store({
                reader: new Ext.data.ArrayReader({}, [{ name: "order" }, { name: "Name" }, { name: "UserName" }, { name: "Actions"}])
            }),
            tbar: [{ text: "@{R=APM.Strings;K=APMWEBJS_VB1_40;E=js}", iconCls: "btnAdd", handler: onAdd}],
            cm: new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer({ header: "@{R=APM.Strings;K=APMWEBJS_VB1_89;E=js}", width: 80 }),
				{ header: "@{R=APM.Strings;K=APMWEBJS_VB1_41;E=js}", dataIndex: "order", hidden: true, hideable: false },
				{ header: "@{R=APM.Strings;K=APMWEBJS_VB1_29;E=js}", dataIndex: "Name", width: 120, renderer: 'htmlEncode', sortable: true },
				{ header: "@{R=APM.Strings;K=APMWEBJS_VB1_30;E=js}", dataIndex: "UserName", sortable: true },
				{ header: "@{R=APM.Strings;K=APMWEBJS_VB1_42;E=js}", dataIndex: "Actions", width: 150, sortable: true}]),
            listeners: { cellclick: onCellClick },
            loadMask: true
        };
        mGrid = new Ext.grid.GridPanel(config);
        loadData();
    }; ctor();
}

/*Review & Start Scan Step*/
SW.APM.AutoAssign.Review = function() {
	var mState = SW.APM.AutoAssign.state;
	var ROW_TPL = "<li class='GroupItem'>" + "<img src='{0}'> " + "{1}" + "</li>";

	function loadData() {
        function onSuccess(json) {
            var tplCntr = $("#templates"); $(ORION.objectFromDataTable(json).Rows).each(function () {
                var encodedTemplateName = _.escape(this.Name);
                $(String.format(ROW_TPL, "/Orion/APM/images/Admin/icon_application_template.gif", encodedTemplateName)).appendTo(tplCntr);
        }); }

		var nodeCntr = $("#nodes");
		Ext.each(mState.nodesInfo, function(node) {
            var encodedNodeName = _.escape(node.name);
            $(String.format(ROW_TPL,
                String.format("/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={0}&status={1}&size=Small", node.id, node.status), 
                encodedNodeName)).appendTo(nodeCntr);
		});
		ORION.callWebService("/Orion/APM/Services/Templates.asmx", "GetTemplates", { templateIDs: mState.templates }, onSuccess);
	}

	function ctor() {
		loadData();
	}; ctor();
};

Ext.onReady(
	function() {
		if (SW.APM.AutoAssign.state) {
			new SW.APM.AutoAssign();
			var step = SW.APM.AutoAssign.state.step;
			if (step == "node") { window.$aa = new SW.APM.AutoAssign.Node(); }
			else if (step == "tpl") { window.$aa = new SW.APM.AutoAssign.Template(); }
			else if (step == "adv") { window.$aa = new SW.APM.AutoAssign.AdvScanSettings(); }
			else if (step == "cred") { window.$aa = new SW.APM.AutoAssign.Credential(); }
			else if (step == "rev") { window.$aa = new SW.APM.AutoAssign.Review(); }
		}
	}
);