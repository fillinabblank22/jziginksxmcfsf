﻿/*jslint browser: true*/
/*global $: false, Ext: false, SW: false*/

'use strict';

Ext.namespace('SW.APM.AutoAssignStatusTree');

// We need to subclass the NodeUI class to hide the icon.  When the icon is set directly
// (as opposed to using a css class) the "loading" spinner icon shows up behind the icon.
// We will simply hide our icon while we wait.
SW.APM.AutoAssignStatusTree.SWTreeNodeUI = Ext.extend(Ext.tree.TreeNodeUI, {
    beforeLoad: function () {
        var icon = $(this.getIconEl());
        this.iconSrc = icon.attr('src');
        icon.attr('src', this.emptyIcon);
        SW.APM.AutoAssignStatusTree.SWTreeNodeUI.superclass.beforeLoad.call(this);
    },
    afterLoad: function () {
        SW.APM.AutoAssignStatusTree.SWTreeNodeUI.superclass.afterLoad.call(this);
        $(this.getIconEl()).attr('src', this.iconSrc);
    }
});

SW.APM.AutoAssignStatusTree.GroupTree = function (settings) {

    var tree;

    function reloadTree() {
        tree.getRootNode().reload();
    }

    tree = new Ext.tree.TreePanel({
        el: settings.renderTo,
        autoScroll: true,
        animate: true,
        containerScroll: true,
        rootVisible: false,
        lines: false,
        trackMouseOver: false,
        dataUrl: settings.dataUrl,
        root: {
            nodeType: 'async',
            text: 'HiddenRoot',
            draggable: false,
            id: '/'
        },
        tbar: (function () {
            var label = { xtype: "label", text: "", style: "font-weight:bold;" };
            return [
                label,
                "@{R=APM.Strings;K=APMWEBJS_VB1_90;E=js}",
                {
                    xtype: "combo",
                    id: "groupByComboBox",
                    valueField: "groupBy",
                    displayField: "groupByDisplay",
                    mode: "local",
                    width: 135,
                    typeAhead: false,
                    triggerAction: "all",
                    editable: false,
                    selectOnFocus: false,
                    value: 'Nodes',
                    store: new Ext.data.SimpleStore({
                        fields: ['groupBy', 'groupByDisplay'],
                        data: [['Nodes', '@{R=APM.Strings;K=APMWEBJS_VB1_91;E=js}'], ['Applications', '@{R=APM.Strings;K=APMWEBJS_VB1_92;E=js}']]
                    }),
                    listeners: {
                        select: reloadTree
                    }
                },
                {
                    xtype: "tbseparator"
                },
                {
                    id: 'refreshButton',
                    text: '@{R=APM.Strings;K=APMWEBJS_VB1_93;E=js}',
                    tooltip: '@{R=APM.Strings;K=APMWEBJS_VB1_94;E=js}',
                    icon: '/Orion/APM/images/icon_refresh.gif',
                    cls: 'x-btn-text-icon',
                    handler: reloadTree
                }
            ];
        }())
    });

    tree.getLoader().on('beforeload', function (treeLoader, node) {
        // Pass the groupby setting to the webservice when we load nodes
        treeLoader.baseParams.groupby = Ext.getCmp('groupByComboBox').value;

        node.on('beforechildrenrendered', function (parentNode) {
            parentNode.eachChild(function (node) {
                node.attributes.groupTreeSettings = settings;
            });
            parentNode.ui.onTextChange(parentNode, parentNode.text, parentNode.text);
        });
    });

    /*jslint unparam: true*/
    tree.getLoader().on('load', function (treeLoader, node) {
        if (!node.parentNode) {
            // We just loaded the root.  Add Zebra stripes
            $('.x-tree-root-node').addClass('apm_NeedsZebraStripes');
        }
    });
    /*jslint unparam: false*/

    tree.render();
};

SW.APM.AutoAssignStatusTree.SelectNodeTree = function () {
    var dataUrl = '/Orion/APM/Services/AutoAssignStatusTree.aspx';
    return new SW.APM.AutoAssignStatusTree.GroupTree({
        dataUrl: dataUrl,
        renderTo: 'apm_selectNodeTree',
        height: $('#apm_selectNodeTree').css('height')
    });
};
