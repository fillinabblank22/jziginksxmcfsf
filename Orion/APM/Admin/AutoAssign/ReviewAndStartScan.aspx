﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReviewAndStartScan.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_ReviewAndStartScan" MasterPageFile="~/Orion/APM/Admin/AutoAssign/_AutoAssign.master"%>
<%@ Register TagPrefix="apm" TagName="ScannedTime" Src="~/Orion/APM/Admin/AutoAssign/Controls/ScannedTime.ascx" %>
<%@ Register TagPrefix="apm" TagName="CheckDuplicates" Src="~/Orion/APM/Admin/CheckDuplicates.ascx" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="wizardContent" Runat="Server">
	<table width="100%">
		<tr>
			<td colspan="2" valign="top">
				<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_41 %></h2>
				<b style="font-size:9pt;"><%= Resources.APMWebContent.APMWEBDATA_VB1_42 %></b><br/>
				<%= Resources.APMWebContent.APMWEBDATA_VB1_43 %><br/><br/>
			</td>
			<td valign="top">
				<apm:ScannedTime ID="scannedTime" runat="server" />
			</td>
		</tr>
		<tr>
			<td style="width:17px;"><%= SolarWinds.APM.Web.HtmlHelper.GetTreeButtons("AA1")%></td>
			<td style="cursor:pointer; width: 100%;" onclick="return onCE(this);"><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_44, SolarWinds.APM.Common.ApmConstants.ModuleShortName, "<b>", this["nodesToScan"], "</b>")%></td>
			<td>&nbsp;</td>			
		</tr>
		<tr id="detailsAA1_rX" style="display:none;">
			<td>&nbsp;</td>
			<td colspan="2">
				<ul id="nodes" class="ZebraBackground" style="width:100%;"></ul>
			</td>
		</tr>
		<tr>
			<td><%= SolarWinds.APM.Web.HtmlHelper.GetTreeButtons("AA2")%></td>
			<td style="cursor:pointer; width: 100%;" onclick="return onCE(this);"><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_45, SolarWinds.APM.Common.ApmConstants.ModuleShortName, "<b>", this["appToScan"], "</b>")%></td>
			<td>&nbsp;</td>			
		</tr>
		<tr id="detailsAA2_rX" style="display:none;">
			<td>&nbsp;</td>
			<td colspan="2">
				<ul id="templates" class="ZebraBackground" style="width:100%;"></ul>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<%= Resources.APMWebContent.APMWEBDATA_VB1_46 %>
			</td>
		</tr>
	</table>
	
	<apm:CheckDuplicates ID="checkDuplicates" runat="server" />

	<div class="sw-btn-bar-wizard" style="padding-top:10px;">
	    <orion:LocalizableButton runat="server" ID="btnBack" OnClick="OnBack_Click" DisplayType="Secondary" LocalizedText="Back"/>
	    <orion:LocalizableButton runat="server" ID="btnNext" OnClick="OnStartScanNext_Click" OnClientClick="if (IsDemoMode()) return APMjs.demoModeAction('ApplicationDiscovery');  return SW.APM.Duplicates.checkDuplicates();" DisplayType="Primary" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_47 %>"/>
	    <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancel_Click" DisplayType="Secondary" LocalizedText="Cancel"/>
	</div>
</asp:Content>
