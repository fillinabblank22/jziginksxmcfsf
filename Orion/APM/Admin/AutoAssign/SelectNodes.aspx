﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelectNodes.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_SelectNodes" MasterPageFile="~/Orion/APM/Admin/AutoAssign/_AutoAssign.master" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_50%>" %>
<%@ Register TagPrefix="apm" TagName="ScannedTime" Src="~/Orion/APM/Admin/AutoAssign/Controls/ScannedTime.ascx" %>
<%@ Register TagPrefix="apm" TagName="NodeTree" Src="~/Orion/APM/Controls/SelectNodeTree.ascx" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="wizardContent" Runat="Server">

	<table width="100%">
		<tbody>
			<tr>
				<td valign="top">
					<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_180 %></h2><%= Resources.APMWebContent.APMWEBDATA_VB1_181 %>
				</td>
				<td>
					<apm:ScannedTime ID="scannedTime" runat="server" />
				</td>
			</tr>
		</tbody>
	</table>
	
	<apm:NodeTree ID="nodeTree" TreeWidth="848px" runat="server" />
	
	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton runat="server" LocalizedText="Back" DisplayType="Secondary" OnClientClick="javascript:history.go(-1)"/>
        <orion:LocalizableButton ID="btnNext" runat="server" LocalizedText="Next" DisplayType="Primary" OnClientClick="return $aa.onNextClick()" OnClick="OnNext_Click"/>
        <orion:LocalizableButton ID="btnCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click" CausesValidation="false"/>
	</div>
</asp:Content>
