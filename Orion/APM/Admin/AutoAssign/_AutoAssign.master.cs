﻿using System;

using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.AutoAssign;

public partial class Orion_APM_Admin_AutoAssign_AutoAssign : System.Web.UI.MasterPage
{
	#region Fields

	protected String _json = String.Empty;

	#endregion

	#region Helper Members

    public void SetClientState(String json)
	{
		_json = json;
	}
	
	#endregion
}
