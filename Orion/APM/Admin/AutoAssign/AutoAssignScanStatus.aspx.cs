﻿using System;
using System.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web.UI.AutoAssign;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_AutoAssign_ScanStatus : Page
{
    protected AutoAssignUtility AutoAssignUtility
    {
        get { return new AutoAssignUtility(true); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CancelAutoAssign.Visible = AutoAssignUtility.IsInProgress;
    }

	protected void OnStopProcess_Click(Object sender, EventArgs e)
	{
        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			bl.CancelAutoAssign();
		}
		Response.Redirect("~/Orion/APM/Admin/Default.aspx");
	}
}
