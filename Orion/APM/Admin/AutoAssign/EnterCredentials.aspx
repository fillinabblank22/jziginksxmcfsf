﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EnterCredentials.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_EnterCredentials" MasterPageFile="~/Orion/APM/Admin/AutoAssign/_AutoAssign.master" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_182%>"%>
<%@ Register TagPrefix="apm" TagName="ScannedTime" Src="~/Orion/APM/Admin/AutoAssign/Controls/ScannedTime.ascx" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="wizardContent" Runat="Server">
	<table width="100%">
		<tr>
			<td style="width:1%;"></td> <td style="width:98%;"></td> <td style="width:1%;"></td>
		</tr>
		<tr>
			<td valign="top" colspan="2">
				<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_190 %></h2>
				<%= Resources.APMWebContent.APMWEBDATA_VB1_191 %><br/><br/>
			</td>
			<td valign="top">
				<apm:ScannedTime ID="scannedTime" runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				<img alt="" src="../../Images/Admin/warning_32x32.gif" />
			</td>
			<td colspan="2">
				<%= Resources.APMWebContent.APMWEBDATA_VB1_192 %>
			</td>
		</tr>
	</table>
	<br/>
	
	<div id="credentials" style="width:100%;">
	</div>	
    
     <div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton ID="btnBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack_Click" CausesValidation="false"/>
        <orion:LocalizableButton ID="btnNext" runat="server" LocalizedText="Next" DisplayType="Primary" OnClick="OnNext_Click"/>
        <orion:LocalizableButton ID="btnCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click" CausesValidation="false"/>
	</div>
</asp:Content>
