﻿using System;

using SolarWinds.APM.Web.UI.AutoAssign;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_AutoAssign_AutoAssignInProcessInfo : System.Web.UI.Page
{
	#region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
    }

	protected void OnStopProcess_Click(Object sender, EventArgs e)
	{
        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			bl.CancelAutoAssign();
		}
		this.Response.Redirect("SelectNodes.aspx");
	}
	
	#endregion
}
