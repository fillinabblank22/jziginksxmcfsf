using System;
using System.Web.UI.WebControls;
using NPMNode = SolarWinds.Orion.NPM.Web.Node;

public partial class Orion_APM_Admin_AutoAssign_Controls_AutoAssignStatusNodesTree : System.Web.UI.UserControl
{
    private string treeHeight = "300px";
    private string treeWidth = "600px";
    private string treeBorder = "1px solid #d0d0d0";
    private string treeClass;

    public string TreeHeight
    {
        get { return treeHeight; }
        set { treeHeight = value; }
    }

    public string TreeWidth
    {
        get { return treeWidth; }
        set { treeWidth = value; }
    }

    public string TreeBorder
    {
        get { return treeBorder; }
        set { treeBorder = value; }
    }

    public string TreeCssClass
    {
        get { return treeClass; }
        set { treeClass = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
