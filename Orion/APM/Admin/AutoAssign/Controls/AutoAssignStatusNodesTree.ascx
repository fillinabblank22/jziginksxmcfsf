<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoAssignStatusNodesTree.ascx.cs" 
    Inherits="Orion_APM_Admin_AutoAssign_Controls_AutoAssignStatusNodesTree" %>

<style type="text/css">
    .x-tree-node-collapsed .x-tree-node-icon, 
    .x-tree-node-expanded .x-tree-node-icon, 
    .x-tree-node-leaf .x-tree-node-icon 
    {
        background-position: center center;
        background-repeat: no-repeat;
        border: 0 none;
        height: auto;
        margin: 0;
        padding: 0;
        vertical-align: top;
        width: auto;
    }
</style>

<orion:Include runat="server" Module="APM" File="Admin/AutoAssign/JS/AutoAssignStatusTree.js"/>
<script type="text/javascript">
    $().ready(function(){
        SW.APM.AutoAssignStatusTree.SelectNodeTree();
    });
</script>

<div id="apm_selectNodeTree" class="<%=TreeCssClass%>" style="position:relative; overflow-y: hidden; height:<%=TreeHeight %>;width:<%=TreeWidth%>;border:<%=TreeBorder%>;" />
<input type="text" id="selectedNodes" runat="server" style="width:800px;display:none; " value=''/>