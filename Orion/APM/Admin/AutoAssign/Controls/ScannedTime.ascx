﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScannedTime.ascx.cs" Inherits="Orion_APM_Admin_AutoAssign_Controls_ScannedTime" %>

<table style="width:250px;height:50px;float:right; padding:10px; background-color:#e4f1f8; background-image: url(../../Images/Admin/sidebar_bg_blue.gif); background-position: top; background-repeat: repeat-x;">
	<tr>
		<td align="center" valign="middle" style="width:30px;"> 
			<img src="../../Images/Admin/icon_lightbulb.gif" />
		</td>
		<td style="text-align:center;font-size:12pt;">
		    <%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_48, "<span id=\"nodeCount\" style=\"font-weight:bold;\">0</span>", "<div id=\"applicationCountRow\" style=\"width:225px;text-align:center;font-size:12pt;\">", "<span id=\"applicationCount\" style=\"font-weight:bold;\">0</span>", "</div>")%>
		</td>
	</tr>	
</table>