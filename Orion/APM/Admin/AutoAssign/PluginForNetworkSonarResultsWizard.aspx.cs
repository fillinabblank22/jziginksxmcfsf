﻿using System;
using System.Web.UI;

using SolarWinds.Orion.Core.Web.Discovery;

public partial class Orion_APM_Admin_AutoAssign_PluginForNetworkSonarResultsWizard : System.Web.UI.Page, 
	SolarWinds.Orion.Web.IStep
{
	#region IStep Members

	public String Step
	{
		get { return "PluginStep"; }
	}

	#endregion
	
	#region Event Handlers

	protected void OnBack_Click(Object sender, EventArgs e) 
	{
		this.Response.Redirect(ResultsWorkflowHelper.GetPreviousStepUrl());
	}

    protected void OnNext_Click(Object sender, EventArgs e)
    {
        this.Response.Redirect("~/Orion/APM/Admin/AutoAssign/SelectNodes.aspx");
    }

    protected void OnFinish_Click(Object sender, EventArgs e)
    {
        SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(ResultsWorkflowHelper.InitialPageUrl);
        // this.Response.Redirect(ResultsWorkflowHelper.);
    }

    protected void OnCancel_Click(Object sender, EventArgs e) 
	{
		this.Response.Redirect(ResultsWorkflowHelper.InitialPageUrl);
	}

	#endregion
}
