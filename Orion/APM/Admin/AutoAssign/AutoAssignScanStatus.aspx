﻿<%@ Page Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" AutoEventWireup="true"
    CodeFile="AutoAssignScanStatus.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_ScanStatus" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_182%>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="apm" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<%@ Register TagPrefix="apm" TagName="AutoAssignStatusNodesTree" Src="~/Orion/APM/Admin/AutoAssign/Controls/AutoAssignStatusNodesTree.ascx" %>

<asp:Content ID="c1" ContentPlaceHolderID="hPh" Runat="Server">
    <apm:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include File="js/OrionCore.js" runat="server"/>      
    <orion:Include File="js/AsyncView.js" runat="server"/>      
    <style type="text/css">
        .tableLeftPosition
        {
            position: relative;
            left: 10px;            
        }
      
        table tr td.StatusMessageHeader
        {
            font-size: 10pt;
            font-weight: bold;            
            white-space:nowrap;
        }
        
        #apmAppSummaryButton a
        {
            color: #336699;
            font-size:16px;    
			font-weight: bold;
        }
        
        #apmAppSummaryButton a:hover
        {
            background-attachment: scroll;
            background-color: #E4F1F8;
            background-image: none;
            background-position: 0 50%;
            background-repeat: repeat;
		}
		table#breadcrumb div.apm_dateArea { padding-top:0px; }
		/* This class purposely has the same name as class in Orion. It is overwriting class owned by Orion. */
		.x-panel-body  {
			border-color:white;
			border: 0;
			height: 271px;
		}
    </style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionAPMAGApplicationDiscovery" ID="IconHelpButton1" runat="server" />
</asp:Content>
<asp:Content ID="c2" ContentPlaceHolderID="cPh" Runat="Server">
    <orion:Refresher ID="Refresher" runat="server" />
	<table width="100%" id="breadcrumb" style="width:100%; margin:0px;" class="tableLeftPosition">
		<tr>
            <td align="left">
                <h1><%= Resources.APMWebContent.APMWEBDATA_VB1_200 %></h1>
            </td>
		</tr>
	</table>
       
    <table style="width: 900px; border: 1px solid; border-color: #dfdfde; background-color: White;" cellpadding="0" cellspacing="10px" class="tableLeftPosition">
        <tr>
            <td class="StatusMessageHeader">
                <span style="vertical-align:top;"><%= AutoAssignUtility.Text %></span> 
            </td>
			<td align="right">
			    <orion:LocalizableButton runat="server" ID="CancelAutoAssign" OnClick="OnStopProcess_Click" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_201 %>" DisplayType="Secondary"/>
			</td>
        </tr>
        <tr>
            <td colspan="2">
                <apm:AutoAssignStatusNodesTree ID="autoAssignStatusNodesTree" TreeHeight="300px" TreeWidth="100%" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="apmAppSummaryButton" class="apm_WizardButtons">
                    <a href="/Orion/APM/Summary.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_202 %></a>
                </div>
            </td>
        </tr>
    </table>    
</asp:Content>