﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelectApplications.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_SelectApplications" MasterPageFile="~/Orion/APM/Admin/AutoAssign/_AutoAssign.master" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_182%>" %>
<%@ Register TagPrefix="apm" TagName="ScannedTime" Src="~/Orion/APM/Admin/AutoAssign/Controls/ScannedTime.ascx" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="wizardContent" Runat="Server">
	<table width="100%">
		<tr>
			<td valign="top">
				<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_183 %></h2><%= Resources.APMWebContent.APMWEBDATA_VB1_184 %>
			</td>
			<td>
				<apm:ScannedTime ID="scannedTime" runat="server" />
			</td>
		</tr>
	</table>
	
	<table width="100%">
		<tr>
			<td style="width:20px;"><%= SolarWinds.APM.Web.HtmlHelper.GetTreeButtons("ASS")%></td>
			<td style="float:left;cursor:pointer;" onclick="return onCE(this);"><%= Resources.APMWebContent.APMWEBDATA_VB1_185 %></td>
		</tr>
	</table>		
	<table width="100%">
		<tr id="detailsASS_rX">
			<td colspan="2">
				<table border="0">
					<tr>
						<td style="width:20px;">&nbsp;</td>
						<td>
							<div id="apm_sliderTicks">
							<div id="sliderContainer" />
								<input id="accuracy" name="accuracy" type="hidden"/>
							</div>
						</td>
					</tr>
					<tr>
						<td />
						<td style="font-size:10pt;">									
							<span id="lblAccuracy" style="font-weight:bold;"></span><br/>
								<a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("OrionAPMAGApplicationDiscovery")%>" target="_blank" style="color:#336699; text-decoration:underline; font-size: 10px;"><%= Resources.APMWebContent.APMWEBDATA_VB1_186 %></a>
							</td>
						</tr>
				</table> 
			</td>
		</tr>
	</table>
	<br />
			
	<table style="width:100%;">
	    <tr>
	        <td>
	            <%= Resources.APMWebContent.APMWEBDATA_VB1_187 %>	                    
	        </td>
	    </tr>
	    <tr>
            <td style="width: auto; padding-right: 10px;">
                <asp:DropDownList ID="tags" DataValueField="Name" DataTextField="Name" AppendDataBoundItems="true"
                    Width="20em" runat="server">
                    <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_188 %>" Value="" />
                </asp:DropDownList>
            </td>
            <td>
                <span style="font-size: 10px; color: #4d4b4b;">
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_189 %></span>
            </td>
	    </tr>
	</table>
	<div style="padding-top: 10px;">
		<table style="width:100%;">
			<tr>
				<td style="vertical-align:top; padding-right:10px;">
					<ul id="templates" class="ZebraBackground"></ul>
				</td>
				<td id="selectedTemplatesContainer">
					<div>
						<ul id="selectedTemplates" style="font-size: 11px;"></ul>
					</div>
				</td>
			</tr>
		</table>
	</div>
    
    <div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton ID="btnBack" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack_Click" CausesValidation="false"/>
        <orion:LocalizableButton ID="btnNext" runat="server" LocalizedText="Next" DisplayType="Primary" OnClientClick="return $aa.onNextClick();" OnClick="OnNext_Click"/>
        <orion:LocalizableButton ID="btnCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click" CausesValidation="false"/>
	</div>
</asp:Content>
