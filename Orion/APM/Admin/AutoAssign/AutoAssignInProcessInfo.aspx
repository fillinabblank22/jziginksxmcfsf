﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AutoAssignInProcessInfo.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_AutoAssignInProcessInfo" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_182%>" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master"%>

<asp:Content ContentPlaceHolderID="hPh" runat="server">
    <style type="text/css">
			div.apm_WizardButtons a { 
				color: #336699; 
				text-decoration: underline;
			}
			div.apm_WizardButtons a:hover {
				color: #ff8040;
			}			
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="cPh" Runat="Server">

	<h2 style="margin-bottom:0px;padding-left:0px;"><%= Resources.APMWebContent.APMWEBDATA_VB1_193 %></h2>
	<%= Resources.APMWebContent.APMWEBDATA_VB1_194 %>
	<br/><br/>
	
	<div class="apm_WizardButtons" style="padding-top:10px; text-align: left">
		<span class="LinkArrow">»&nbsp;</span>
			<a href="#" onserverclick="OnStopProcess_Click" style="font-weight:bold;text-decoration:none;" runat="server"><%= Resources.APMWebContent.APMWEBDATA_VB1_195 %></a>
		<br/>
		<%= Resources.APMWebContent.APMWEBDATA_VB1_196 %>
   			<a href="/Orion/APM/Admin/AutoAssign/AutoAssignScanStatus.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_197 %></a>
            <br/>
			<a href="/Orion/APM/Admin/Default.aspx"><%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_198, SolarWinds.APM.Common.ApmConstants.ModuleShortName)%></a>
            <br/>
			<a href="/Orion/APM/Admin/QuickStart/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_199 %></a>
		<br/>
	</div>
</asp:Content>
