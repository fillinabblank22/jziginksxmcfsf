﻿using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.AutoAssign;

public partial class Orion_APM_Admin_AutoAssign_SelectApplications : AutoAssignPageBase
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack) 
		{
			this.InitData();

			btnNext.AddEnterHandler(1);
		}
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
		this.UpdateData();
		
		base.GotoPreviousPage();
	}
	
	protected void OnNext_Click(Object sender, EventArgs e)
	{
		if (Page.IsValid)
		{
			this.UpdateData();

			base.GotoNextPage();
		}
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		base.CancelWizard();
	}

	#endregion

	#region Override Members (SolarWinds.APM.Web.UI.AutoAssign.AutoAssignPageBase)

	protected override void InitData()
	{
		var master = this.Master as Orion_APM_Admin_AutoAssign_AutoAssign;
		CheckProcessSatus(Workflow);

        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			var tagInfos = bl.GetTagsWithoutCustomApplicationTypes(-1, null);
		    tagInfos.Remove(new TagInfo(-1, "User Experience"));
            
            tags.DataSource = tagInfos;
			tags.DataBind();
		}
		tags.SelectedValue = Workflow.SelectedTag;

		master.SetClientState(Workflow.ToJson("tpl"));
	}

	protected override void UpdateData()
	{
		Workflow.MatchAccuracy = int.Parse(Request.Form["accuracy"]);
        
		Workflow.TemplateIDs = null;
		if (!String.IsNullOrEmpty(Request.Form["selectedTemplates"]))
		{
			Workflow.TemplateIDs = SolarWinds.APM.Web.Utility.JsHelper
				.Deserialize<List<Int32>>(String.Format("[{0}]", Request.Form["selectedTemplates"]));
		}
	}

	#endregion
}
