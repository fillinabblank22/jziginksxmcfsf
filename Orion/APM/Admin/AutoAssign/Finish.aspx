﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Finish.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_Finish" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_182%>" MasterPageFile="~/Orion/APM/Admin/AutoAssign/_AutoAssign.master"%>

<asp:Content ID="bodyContent" ContentPlaceHolderID="wizardContent" Runat="Server">
	<table width="100%">
		<tr>
			<td valign="top">
				<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_203 %></h2><br/>
			</td>
		</tr>
		<tr>
			<td>
				<%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_205, SolarWinds.APM.Common.ApmConstants.ModuleShortName)%>
			</td>
		</tr>
	</table>
	
	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton runat="server" ID="btnFinish" OnClick="OnFinish_Click" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_207 %>" DisplayType="Primary"/>
	</div>
</asp:Content>
