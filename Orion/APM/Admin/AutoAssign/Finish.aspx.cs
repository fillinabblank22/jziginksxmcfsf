﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.AutoAssign;
using SolarWinds.Orion.Web.Helpers;
using AutoAssignMaster = Orion_APM_Admin_AutoAssign_AutoAssign;

public partial class Orion_APM_Admin_AutoAssign_Finish : AutoAssignPageBase
{
    private new static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			this.InitData();

			btnFinish.AddEnterHandler(0);
		}
	}

	protected void OnFinish_Click(Object sender, EventArgs e)
	{
		const string defaultPage = "/Orion/Apm/Summary.aspx";

		try
		{
			DiscoveryCentralHelper.Return(defaultPage);
		}
		catch(Exception ex)
		{
			log.Error("Failed to redirect using DiscoveryCentralHelper, probably running with older Core version", ex);
			Response.Redirect(defaultPage);
		}
	}

	#endregion

	#region Override Members (SolarWinds.APM.Web.UI.AutoAssign.AutoAssignPageBase)

	protected override void InitData()
	{
		CheckProcessSatus(Workflow);

        using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			bl.StartAutoAssign(Workflow.ToSettings());
		}
		ResetSession();
	}

	protected override void UpdateData()
	{
	}

	#endregion
}
