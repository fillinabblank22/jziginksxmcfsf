﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" CodeFile="PluginForNetworkSonarResultsWizard.aspx.cs" Inherits="Orion_APM_Admin_AutoAssign_PluginForNetworkSonarResultsWizard" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
	<span class="ActionName"><%= Resources.APMWebContent.APMWEBDATA_VB1_221%></span>
	<asp:UpdatePanel ID="_ctrUpdatePanel" runat="server">
		<ContentTemplate>
    		<div style="padding-left: 20px; padding-bottom: 20px; padding-top: 10px; padding-right: 30px;">
                <div class="sw-suggestion sw-suggestion-pass" style="border: none; background: none;  color: Black; font-weight: normal; font-size: small;">
                    <span class="sw-suggestion-icon"></span><%= Resources.APMWebContent.APMWEBDATA_VB1_222 %>
                </div>
                <div style="clear: both"></div>
<%if (SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin)
  {%>
                <div>
                    <%= Resources.APMWebContent.APMWEBDATA_VB1_223 %>
                </div>
<% } else { %>
                <div class="sw-suggestion sw-suggestion-fail">
                    <span class="sw-suggestion-icon"></span><%= Resources.APMWebContent.APMWEBDATA_VB1_224 %>
                </div>
<% } %>
                <div style="clear: both;"></div>
            </div>
			<table style="width: 100%">
				<tr>
					<td class="padding40">
					</td>
					<td style="text-align: right">
                        <div class="sw-btn-bar-wizard">
                            <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" DisplayType="Secondary"
                                OnClick="OnBack_Click" />
                            <%if (SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin)
                              {%>
                            <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" DisplayType="Primary"
                                OnClick="OnNext_Click" />
                            <% }
                              else
                              { %>
                            <orion:LocalizableButton runat="server" ID="imgbFinish" LocalizedText="Finish" DisplayType="Primary"
                                OnClick="OnFinish_Click" />
                            <% } %>
                            <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel_Click"
                                CausesValidation="false" />
                        </div>
					</td>
				</tr>
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>