﻿using System;
using System.Web.UI;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.AutoAssign;

using AutoAssignMaster = Orion_APM_Admin_AutoAssign_AutoAssign;

public partial class Orion_APM_Admin_AutoAssign_ReviewAndStartScan : AutoAssignPageBase
{
	#region Properties

	public String this[String key]
	{
		get
		{
			if ("nodesToScan".Equals(key, StringComparison.InvariantCultureIgnoreCase))
			{
				return Workflow.NodeIDs.Count.ToString();
			}
			if ("appToScan".Equals(key, StringComparison.InvariantCultureIgnoreCase))
			{
				return Workflow.TemplateIDs.Count.ToString();
			}
			return String.Empty;
		}
	}

	#endregion

	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			this.InitData();

			btnNext.AddEnterHandler(1);
		}
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
		this.UpdateData();
		
		base.GotoPreviousPage();
	}

	protected void OnStartScanNext_Click(Object sender, EventArgs e)
	{
		if ((Workflow.NodeIDs.Count == 0) || (Workflow.TemplateIDs.Count == 0))
			base.CancelWizard();

		this.UpdateData();
		
		base.GotoNextPage();
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		base.CancelWizard();
	}

	#endregion

	#region Override Members (SolarWinds.APM.Web.UI.AutoAssign.AutoAssignPageBase)

	protected override void InitData()
	{
		CheckProcessSatus(Workflow);

		(this.Master as AutoAssignMaster).SetClientState(Workflow.ToJson("rev"));

		this.checkDuplicates.SkipDuplicates = Workflow.SkipDuplicates;
		this.checkDuplicates.NodeIDs = Workflow.NodeIDs;
		this.checkDuplicates.TemplateIDs = Workflow.TemplateIDs;
	}

	protected override void UpdateData()
	{
		Workflow.SkipDuplicates = this.checkDuplicates.SkipDuplicates;
	}

	#endregion
}
