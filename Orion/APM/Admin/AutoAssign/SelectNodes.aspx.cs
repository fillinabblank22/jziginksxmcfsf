﻿using System;
using System.Web.UI;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.AutoAssign;

using AutoAssignMaster = Orion_APM_Admin_AutoAssign_AutoAssign;

public partial class Orion_APM_Admin_AutoAssign_SelectNodes : AutoAssignPageBase
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			this.InitData();

			btnNext.AddEnterHandler(1);
		}
	}

	protected void OnNext_Click(Object sender, EventArgs e)
	{
		if (this.Page.IsValid)
		{
			this.UpdateData();

			base.GotoNextPage();
		}
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		base.CancelWizard();
	}

	#endregion

	#region Override Members (SolarWinds.APM.Web.UI.AutoAssign.AutoAssignPageBase)

	protected override void InitData()
	{
		CheckProcessSatus(Workflow);
		
		(this.Master as AutoAssignMaster).SetClientState(Workflow.ToJson("node"));

		if (Workflow.SelectedNodesInfo != null)
		{
			this.nodeTree.NodeToSelectByDefault = Workflow.SelectedNodesToJson();
		}
	}

	protected override void UpdateData()
	{
		Workflow.NodeIDs = nodeTree.GetSelectedNodeIds();
		Workflow.SelectedNodesInfo = nodeTree.GetSelectedNodeInfo();
	}

	#endregion
}
