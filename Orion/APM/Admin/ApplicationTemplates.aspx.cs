﻿using System;
using System.Linq;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Admin_ApplicationTemplates : System.Web.UI.Page
{
	protected String _thwackUserInfo = @"{""name"":"""",""pass"":""""}";

	protected void Page_Load(object sender, EventArgs e)
	{
        var cred = Session["ThwackCredential"] as System.Net.NetworkCredential;
		if (cred != null)
		{
            _thwackUserInfo = InvariantString.Format(@"{{""name"":{0},""pass"":{1}}}", JsHelper.Serialize(cred.UserName), JsHelper.Serialize(cred.Password));
		}

        OrionInclude.ModuleFile(ApmConstants.InternalModuleName, "APM.js", OrionInclude.Section.Top);

	    UpdateCustomToolbarButtonsHandlers();
	}

    private void UpdateCustomToolbarButtonsHandlers()
    {
        var plugins = WebPluginManager.Instance.SupportedPlugins.Where(p => p is IPluginScriptProvider);
        foreach (var webPlugin in plugins)
        {
            foreach (var appType in webPlugin.SupportedCustomApplicationTypes)
            {
                ((IPluginScriptProvider) webPlugin).IncludeScripts(appType);
            }
        }
    }
}