﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialSetter.ascx.cs" Inherits="Orion_APM_Admin_CredentialSetter" %>

<%@ Register TagPrefix="apm" TagName="Creds" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="Test" Src="~/Orion/APM/Admin/CmpTestResult.ascx" %>

<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>

<asp:UpdatePanel ID="up1" UpdateMode="Conditional" runat="server">
	<ContentTemplate>
		<table width="100%">
			<tr>
				<td style="width:17px;">
					<asp:ImageButton id="ctrExpander" ImageAlign="left" ImageUrl="~/Orion/APM/Images/Button.Expand.gif" CausesValidation="false" runat="server"/>
				</td>
				<td colspan="2" style="cursor:pointer;"><%=Title%></td>
			</tr>
			<tr>
				<td colspan="3">
					<%=SubTitle%>
					<asp:Panel ID="ctrNodesPanel" runat="server">
						<asp:RadioButtonList ID="ctrNodes" OnSelectedIndexChanged="OnNodes_SelectedIndexChanged" AutoPostBack="true" runat="server"/>
						<hr style="width:98%;float:left;"/> <br/>
					</asp:Panel>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2">
					<asp:RadioButtonList ID="ctrCredsTypes" OnSelectedIndexChanged="OnCredsTypes_SelectedIndexChanged" AutoPostBack="true" runat="server"/>
				</td>
			</tr>
			<tr id="ctrCredCnt" runat="server">
				<td>&nbsp;</td>
				<td colspan="2">
					<asp:UpdatePanel ID="up2" UpdateMode="Conditional" runat="server">
						<ContentTemplate>
							<table style="margin-left:10px;padding:5px;background-color:#e4f1f8;">
								<apm:Creds ID="ctrCreds" runat="server"/>  
							</table>
							<asp:ValidationSummary ID="ctrValSummary" DisplayMode="BulletList" ValidationGroup="SelectCredentialsValidationGroup" runat="server"/>
							<asp:Button ID="ctrCredValidate" CssClass="x-hidden" OnClick="OnCredValidate_Click" AutoPostBack="true" runat="server" />
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
		</table>
		<ajax:CollapsiblePanelExtender ID="ctrCpe" 
			TargetControlID="ctrNodesPanel" ExpandControlID="ctrExpander" CollapseControlID="ctrExpander" ImageControlID="ctrExpander" 
			ExpandedText="<%$ Resources: APMWebContent, APMWEBDATA_AK1_79 %>" CollapsedText="<%$ Resources: APMWebContent, APMWEBDATA_AK1_80 %>" ExpandedImage="~/Orion/images/Button.Collapse.gif" CollapsedImage="~/Orion/images/Button.Expand.gif" 
			Collapsed="True" ExpandDirection="Vertical" SuppressPostBack="true" runat="Server"/>
	</ContentTemplate>
</asp:UpdatePanel>
<apm:Test ID="ctrTest" runat="server" />