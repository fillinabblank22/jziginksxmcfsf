﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Utility;
using SolarWinds.Logging;

public partial class Orion_Admin_ApmAdmin : SolarWinds.APM.Web.UI.ApmMasterPage
{
	static readonly Log log = new Log();

	private static readonly List<string> AllowedGuestPages = new List<string>
	{
		"/Orion/APM/Admin/Default.aspx",
		"/Orion/APM/Admin/Applications/Default.aspx",
		"/Orion/APM/Admin/ApplicationTemplates.aspx",
        "/Orion/APM/Admin/EditApplicationTemplate.aspx",
		"/Orion/APM/Admin/Edit/EditTemplate.aspx",
		"/Orion/APM/Admin/Templates/thwack/Default.aspx",

		"/Orion/APM/Admin/QuickStart/GettingStarted.aspx",
		"/Orion/APM/Admin/QuickStart/Default.aspx",
		"/Orion/APM/Admin/QuickStart/SelectTemplate.aspx",
		"/Orion/APM/Admin/QuickStart/SelectNodes.aspx",
		"/Orion/APM/Admin/QuickStart/SelectCredentials.aspx",
                               
		"/Orion/APM/Admin/AutoAssign/SelectNodes.aspx",
		"/Orion/APM/Admin/AutoAssign/SelectApplications.aspx",
		"/Orion/APM/Admin/AutoAssign/EnterCredentials.aspx",
		"/Orion/APM/Admin/AutoAssign/ReviewAndStartScan.aspx"
	};

	#region Page methods

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        this.phDemoMode.Visible = false;

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            this.phDemoMode.Visible = true;

            if (AllowedGuestPages.Exists(s => String.Equals(s, Request.Path, StringComparison.OrdinalIgnoreCase)))
            {
                return;
            }
        }

        if (SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin)
		{
			return;
		}

		Server.Transfer(InvariantString.Format("~/Orion/Error.aspx?Message=You must be an {0} Administrator to access this page.", ApmConstants.ModuleShortName));
	}

	#endregion Page methods

	protected void ScriptAsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
	{
		log.Error("ScriptAsyncPostBackError", e.Exception);
		this.MasterScriptManager.AsyncPostBackErrorMessage = e.Exception.Message;
	}
}