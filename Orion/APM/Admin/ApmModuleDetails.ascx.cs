﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Licensing;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.APM.Web;

public partial class Orion_APM_Admin_ApmModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log Logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo(ApmConstants.ModuleShortName);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error while displaying details for Orion Core module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductShortName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            ApmDetails.Name = module.ProductDisplayName;

            values.Add(Resources.APMWebContent.APMWEBCODE_VB1_64, module.ProductName);
            values.Add(Resources.APMWebContent.APMWEBCODE_VB1_65, module.Version);
            values.Add(Resources.APMWebContent.APMWEBCODE_VB1_66, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.APMWebContent.APMWEBCODE_VB1_67 : module.HotfixVersion);
            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add(Resources.APMWebContent.APMWEBCODE_VB1_68, module.LicenseInfo);

            AddApmInfo(values);

            ApmDetails.DataSource = values;
			break; // we support only one "Primary" engine
        }
    }

    private void AddApmInfo(Dictionary<string, string> values)
    {
        LicensingSummary summaryData;

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            summaryData = businessLayer.GetLicenseSummary();
        }

        values.Add(summaryData.LicenseLimitWording, summaryData.LicenseLimitDisplay);
        values.Add(summaryData.ElementsCountWording, summaryData.ElementsCount.ToString());
        values.Add(summaryData.LicensedElementsWording, summaryData.LicensedElements.ToString());
        values.Add(summaryData.UnlicensedElementsWording, summaryData.UnlicensedElements.ToString());
        values.Add(summaryData.AvailableElementsWording, summaryData.AvailableElementsDisplay);
    }
}