using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using System.Globalization;

public partial class Orion_APM_Admin_EditApplicationBaseSettings : UserControl, IApplicationEditor
{
    private ApplicationBase application;
    private ApplicationTemplate applicationTemplate;

    public string FrequencySettingRangeErrorMessage
    {
        get
        {
            return string.Format(Resources.APMWebContent.APMWEBCODE_VB1_30, ComponentBase.MinPollingFrequencyValue.TotalSeconds);
        }
    }

    public string FrequencySettingDisplayStyle
    {
        get
        {
            return "";
        }
    }

    public string TimeoutSettingDisplayStyle
    {
        get
        {
            return "";
        }
    }

    public new ApplicationBase Application
    {
        get { return application; }
        set { application = value; }
    }

    public ApplicationTemplate ApplicationTemplate
    {
        get { return applicationTemplate; }
        set { applicationTemplate = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       if (!this.IsPostBack)
       {
           this.DataBind();
       }

    }

    public void SetControlValues()
    {
        frequencyEdit.Text = Application.Frequency.TotalSeconds.ToString();
        timeoutEdit.Text = Application.Timeout.TotalSeconds.ToString();

        debugLoggingEnabledEdit.SelectedValue = Application.DebugLoggingEnabled.ToString(CultureInfo.InvariantCulture);
        numberOfLogFilesEdit.Text = Application.NumberOfLogFilesToKeep.ToString();
        use64BitEdit.SelectedValue = Application.Use64Bit.ToString(CultureInfo.InvariantCulture);

        if (ApplicationTemplate != null)
        {
            frequencyEditTemplateValue.Text = ApplicationTemplate.Frequency.TotalSeconds.ToString();
            timeoutEditTemplateValue.Text = ApplicationTemplate.Timeout.TotalSeconds.ToString();

            debugLoggingEnabledTemplateValue.Text = ApplicationTemplate.DebugLoggingEnabled ? Resources.APMWebContent.APMWEBDATA_VB1_102 : Resources.APMWebContent.APMWEBDATA_VB1_103;
            numberOfLogFilesTemplateValue.Text = ApplicationTemplate.NumberOfLogFilesToKeep.ToString();
            use64BitTemplateValue.Text = ApplicationTemplate.Use64Bit ? Resources.APMWebContent.APMWEBDATA_VB1_109 : Resources.APMWebContent.APMWEBDATA_VB1_108;
        }

        bool allowEditing = Level2State(this.Application, this.Application.FrequencySettingLevel);
        frequencyValidate.Value = (allowEditing && Application.FrequencySettingRequired).ToString();
        frequencyRequired.Value = Application.FrequencySettingRequired.ToString();
        SetupOverrideableSetting(allowEditing, frequencyOverrideLink, editFrequencySpan, viewFrequencySpan, frequencyState, frequencyRequired, frequencyValidate);

        allowEditing = Level2State(this.Application, this.Application.TimeoutSettingLevel);
        timeoutValidate.Value = (allowEditing && Application.TimeoutSettingRequired).ToString();
        timeoutRequired.Value = Application.TimeoutSettingRequired.ToString();
        SetupOverrideableSetting(allowEditing, timeoutOverrideLink, editTimeoutSpan, viewTimeoutSpan, timeoutState, timeoutRequired, timeoutValidate);

        allowEditing = Level2State(this.Application, this.Application.DebugLoggingEnabledSettingLevel);
        debugLoggingEnabledValidate.Value = (allowEditing && Application.DebugLoggingEnabledRequired).ToString();
        debugLoggingEnabledRequired.Value = Application.DebugLoggingEnabledRequired.ToString();
        SetupOverrideableSetting(allowEditing, debugLoggingEnabledOverrideLink, editDebugLoggingSpan, viewDebugLoggingSpan, debugLoggingEnabledState, debugLoggingEnabledRequired, debugLoggingEnabledValidate);

        allowEditing = Level2State(this.Application, this.Application.NumberOfLogFilesToKeepSettingLevel);
        NumberOfLogFilesValidate.Value = (allowEditing && Application.NumberOfLogFilesToKeepRequired).ToString();
        NumberOfLogFilesRequired.Value = Application.NumberOfLogFilesToKeepRequired.ToString();
        SetupOverrideableSetting(allowEditing, NumberOfLogFilesOverrideLink, editNumberOfLogFilesSpan, viewNumberOfLogFilesSpan, NumberOfLogFilesState, NumberOfLogFilesRequired, NumberOfLogFilesValidate);

        allowEditing = Level2State(this.Application, this.Application.Use64BitSettingLevel);
        use64BitValidate.Value = (allowEditing && Application.Use64BitRequired).ToString();
        use64BitRequired.Value = Application.Use64BitRequired.ToString();
        SetupOverrideableSetting(allowEditing, use64BitOverrideLink, editUse64BitSpan, viewUse64BitSpan, use64BitState, use64BitRequired, use64BitValidate);

    }

    //TODO: two following methods should be moved into ComponentEditorHelper
    //      (and changed to extension methods after solution settings upgrade)
    private static bool Level2State(ApplicationBase app, SettingLevel level)
    {
        return app is ApplicationTemplate || level == SettingLevel.Instance;
    }

    private static SettingLevel State2Level(ApplicationBase app, bool state)
    {
        return app is ApplicationTemplate || !state ?
            SettingLevel.Template : SettingLevel.Instance;
    }

    private void SetupOverrideableSetting(bool allowEdit, HtmlAnchor overrideLink, HtmlControl editControl,
                                          HtmlControl viewControl, HiddenField stateControl, HiddenField requiredControl, HiddenField validateStateControl)
    {
        // If we don't have a template, we can't override anything.  Don't show the link
        bool allowOverride = (applicationTemplate != null);

        ComponentEditorHelper.SetupOverrideableSetting(allowEdit, allowOverride, overrideLink, editControl, viewControl,
                                                       stateControl, requiredControl, validateStateControl);
    }

    public void Init(ApplicationEditorContext ctx) {  }

    public void Update(ApplicationBase app, ApplicationTemplate template)
    {
        Application = app;
        ApplicationTemplate= template;

        SetControlValues();
    }
    
    public void UpdateFromViewState(ApplicationBase app)
    {
        double totalSeconds;
        double value;
        //
        // Frequency
        //
        string editValue = frequencyEdit.Text;
        string viewValue = frequencyEditTemplateValue.Text;

        bool useComponentValue = Convert.ToBoolean(frequencyState.Value);

        if (((Double.TryParse(editValue, out value)) && useComponentValue) || ((Double.TryParse(viewValue, out value)) && !(useComponentValue)))
        {
            totalSeconds = Convert.ToDouble(useComponentValue ? editValue : viewValue);
            app.Frequency = TimeSpan.FromSeconds(totalSeconds);
            app.FrequencySettingLevel = State2Level (this.Application, useComponentValue);

			UpdateFrequency(app, totalSeconds);
        }

        //
        // Timeout
        //
        editValue = timeoutEdit.Text;
        viewValue = timeoutEditTemplateValue.Text;
        useComponentValue = Convert.ToBoolean(timeoutState.Value);

        if (((Double.TryParse(editValue, out value)) && useComponentValue) || ((Double.TryParse(viewValue, out value)) && !(useComponentValue)))
        {
            totalSeconds = Convert.ToDouble(useComponentValue ? editValue : viewValue);
            app.Timeout = TimeSpan.FromSeconds(totalSeconds);
            app.TimeoutSettingLevel = State2Level(this.Application, useComponentValue);
        }

        editValue = debugLoggingEnabledEdit.SelectedValue;
        viewValue = debugLoggingEnabledTemplateValue.Text;
        useComponentValue = Convert.ToBoolean(debugLoggingEnabledState.Value);
        bool templateValue = viewValue == "On" ? true : false;
        bool result;

        if ((bool.TryParse(editValue, out result) && useComponentValue) || (!useComponentValue))
        {
            bool enabled = useComponentValue ? bool.Parse(editValue) : templateValue;
            app.DebugLoggingEnabled = enabled;
            app.DebugLoggingEnabledSettingLevel = State2Level(this.Application, useComponentValue);
        }

        editValue = numberOfLogFilesEdit.Text;
        viewValue = numberOfLogFilesTemplateValue.Text;
        useComponentValue = Convert.ToBoolean(NumberOfLogFilesState.Value);
        int intVal;

        if (((int.TryParse(editValue, out intVal)) && useComponentValue) || ((int.TryParse(viewValue, out intVal)) && !(useComponentValue)))
        {
            int numberOfFiles = Convert.ToInt32(useComponentValue ? editValue : viewValue);
            app.NumberOfLogFilesToKeep = numberOfFiles;
            app.NumberOfLogFilesToKeepSettingLevel = State2Level(this.Application, useComponentValue);
        }

        editValue = use64BitEdit.SelectedValue;
        viewValue = use64BitTemplateValue.Text;
        useComponentValue = Convert.ToBoolean(use64BitState.Value);
        templateValue = viewValue == "64bit" ? true : false;

        if ((bool.TryParse(editValue, out result) && useComponentValue) || (!useComponentValue))
        {
            bool enabled = useComponentValue ? bool.Parse(editValue) : templateValue;
            app.Use64Bit = enabled;
            app.Use64BitSettingLevel = State2Level(this.Application, useComponentValue);
        }
    }

	private void UpdateFrequency(ApplicationBase app, double frequency) 
	{
		var tpl = app as ApplicationTemplate;
		if (tpl != null && tpl.ComponentTemplates != null && tpl.ComponentTemplates.Count > 0)
		{
			foreach (ComponentBase cmp in tpl.ComponentTemplates)
			{
				if (cmp.Settings.ContainsKey("__Frequency"))
				{
					cmp.Settings["__Frequency"].Value = frequency.ToString();
				}
			}
		}
	}
}
