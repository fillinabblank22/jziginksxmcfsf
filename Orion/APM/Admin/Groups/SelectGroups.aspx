﻿<%@ Page Title="Select Groups" Language="C#" MasterPageFile="~/Orion/APM/Admin/Groups/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="SelectGroups.aspx.cs" Inherits="Orion_APM_Admin_Templates_Assign_SelectGroups" %>

<%@ Register Src="../../Controls/SearchableGroupSelection.ascx" TagPrefix="apm" TagName="SearchableGroupSelection" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="core" TagName="HelpLink" %>

<asp:Content ID="SelectGroupsStepContent" ContentPlaceHolderID="WizardContentPlaceHolder" Runat="Server">
    <orion:Include runat="server" File="APM/js/SelectGroups.js"/>

    <style type="text/css">
        #checks label{margin-left: 4px}

        .divider {
            border-bottom: 1px solid #ddd;
            height: 1px;
            margin-bottom: 15px;
            width: 100%;
        }

        .serverOnlyTooltipCls {
            display: none;
            width: auto;
            max-width: 300px;
            min-width: 180px;
            height: auto;
            position: absolute;
            background-color: #F6F6F6;
            border: 1px #EAEAEA solid;
            border-collapse: separate;
            color: #333333;
            box-shadow: 2px 2px 3px #DDDDDD;
            z-index: 999;
            padding: 10px;
            margin-left: 3px;
            margin-top: -30px;
        }

        .serverOnlyTooltipCls div.serverOnlyTooltipArrowCls {
            top: 27px;
            left: -5px;
            width: 50px;
            height: 29px;
            background-image: url("/Orion/APM/Images/Tooltips/tip-anchor.png");
            position: absolute;
            background-repeat: no-repeat;
            margin-top: 7px;
        }

        .imgHelpCls {
            padding-left: 40px;
        }

        .autoDeleteCaption {
            color: #999999;
        }

        .rblStyle {
            padding-left: 10px; 
            padding-top: 10px;
        }
    </style>

    <h2 style="font-weight:normal"><%= string.Format(Resources.APMWebContent.AssignTemplatesToGroupsWizard_SelectGroupsStepTitle, HttpUtility.HtmlEncode(TemplateNames))%></h2>
    <p><%= string.Format(Resources.APMWebContent.AssignTemplatesToGroupsWizard_SelectGroupsStepDescription)%></p>
    <br />    
    <apm:SearchableGroupSelection ID="SearchableGroupSelection" runat="server" />
    <br />
    <p>
        <b><%= Resources.APMWebContent.AssignTemplatesToGroupsWizard_SelectGroupsStepLabel %></b>
        &nbsp;
        <asp:HyperLink ID="AddGroupLink" 
            runat="server" 
            Text="<%$ Resources : APMWebContent , AssignTemplatesToGroupsWizard_SelectGroupsStepLink %>" 
            NavigateUrl="/Orion/Admin/Containers/Default.aspx" />
    </p>
    <br/>
    <div class="divider"></div>
    <div class="acb-advancedSection" style="margin-top: 10px">
        <asp:Image ID="advancedOptionsButton" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif" style="cursor:pointer" data-form="AdvancedOptionsButton"/>
        <%-- Advanced--%><%= Resources.APMWebContent.APMWEBDATA_VB1_100 %><br/>
        <div data-container="advancedOptionsContainer" class="hidden" style="padding-left: 15px;padding-top: 10px;">
            <div style="display: flex">
                <div id="checks">
                    <asp:CheckBox ID="ServersOnly" runat="server" AutoPostBack="False" Text="<%$ Resources : APMWebContent, SelectServersOnlyTooltipCheckbox %>" Checked="True"/>
                </div>
                <div id="imgHelpDiv">
                    <img id="chbHelpImg" src="/Orion/APM/Images/Help_Icon.png" data-form="serversOnlyImg" class="imgHelpCls"/>
                </div>
                <div class="serverOnlyTooltipCls" id="serverOnlyTooltip" data-form="serverOnlyTooltip">
                        <%= Resources.APMWebContent.SelectServersOnlyTooltip %>
                        <br><br>
                        <core:HelpLink ID="cloudMonitoringHelpLink" CssClass="clm_wizardHelpLink" 
                            runat="server" HelpUrlFragment="OrionSAMAssignTempToGroupOS" 
                            LocalizedText="CustomText" HelpDescription="<%$  Resources : APMWebContent, SelectServersOnlyTooltipLearnMore %>"/>
                        <div class="serverOnlyTooltipArrowCls"></div>
                </div>
            </div>
            <br/>
            <div>
                <div class="autoDeleteCaption">
                    <%= Resources.APMWebContent.SelectGroups_autoDelete %>
                </div>
                <div>
                    <%= Resources.APMWebContent.SelectGroups_confirmMessage %>
                    <core:HelpLink ID="AutoDeleteHelpLink" CssClass="clm_wizardHelpLink"
                            runat="server" HelpUrlFragment="OrionSAMNotDeleteAppsAutoLM" 
                            LocalizedText="CustomText" HelpDescription="<%$  Resources : APMWebContent, SelectServersOnlyTooltipLearnMore %>"/>
                </div>
                <div class="rblStyle">
                    <asp:RadioButtonList ID="rblAutoDelete" runat="server">
                        <asp:ListItem Text="<%$ Resources : APMWebContent, SelectGroups_AutoDeleteYes %>" Value="yes" Selected="True"/>
                        <asp:ListItem Text="<%$ Resources: APMWebContent, SelectGroups_AutoDeleteNo %>" Value="no" />
                    </asp:RadioButtonList>
                </div>
            </div>
        </div>
    </div>
    <br />
    <asp:CustomValidator ID="AtLeastOneGroupValidator" 
        runat="server" 
        ErrorMessage="<%$ Resources : APMWebContent , AssignTemplatesToGroupsWizard_SelectGroupsValidation %>" 
        OnServerValidate="AtLeastOneGroupValidator_ServerValidate"/>
    <br />
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="NextButton" runat="server" LocalizedText="Next" DisplayType="Primary" OnClick="NextButton_OnClick"/>
        <orion:LocalizableButton ID="CancelButton" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelButton_OnClick" CausesValidation="false" />
    </div>
    <script type="text/javascript">
            _getAdvancedOptionsButton = function () {
                var element = $('img[data-form="AdvancedOptionsButton"]');
                return element;
            };
            _getContainer = function (containerName, additionalSelector) {
                if (typeof additionalSelector === "undefined") { additionalSelector = ''; }
                var element = $('div[data-container="' + containerName + '"]' + additionalSelector);
                return element;
            };

            _toggleAdvancedOptions = function () {
                var $container = this._getContainer('advancedOptionsContainer');
                var $button = this._getAdvancedOptionsButton();

                $container.toggleClass('hidden');
                if ($container.hasClass('hidden')) {
                    $button.attr({ src: '/Orion/images/Button.Expand.gif' });
                } else {
                    $button.attr({ src: '/Orion/images/Button.Collapse.gif' });
                }
            };

            _getAdvancedOptionsButton().click(function () {
                return _toggleAdvancedOptions();
            });

            showTooltip = function () {
                $('div[data-form="serverOnlyTooltip"]').css({ display: "block", opacity: 1.0 });
                
            };
            hideTooltip = function() {
                $('div[data-form="serverOnlyTooltip"]').css({ display: "none", opacity: 0.0 });
            };
            
            /* Tooltip event handler assignment */
            $('img[data-form="serversOnlyImg"]').on("hover", function(e) {
                if (e.type == "mouseenter") {
                    showTooltip();
                } else {
                    hideTooltip();
                }
            });

            $('div[data-form="serverOnlyTooltip"]').on("hover", function(e) {
                if (e.type == "mouseenter") {
                    showTooltip();
                } else {
                    hideTooltip();
                }
            });
    </script>   
</asp:Content>

