﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Groups/AssignWizard.master"
    AutoEventWireup="true" CodeFile="SelectCredentials.aspx.cs" Inherits="Orion_APM_Admin_Templates_Assign_SelectCredentials" %>

<%@ Register TagPrefix="apm" TagName="CredSetter" Src="~/Orion/APM/Admin/CredentialSetter.ascx" %>
<%@ Register TagPrefix="apm" TagName="CredsTip" Src="~/Orion/APM/Controls/CredentialTips.ascx" %>

<asp:Content ID="SelectCredsStepContent" ContentPlaceHolderID="WizardContentPlaceholder" Runat="Server">
    <style type="text/css">
	    input[type='radio'][id*='ctrNodes'] 
        {
	        display: none;
	    }
    </style>
	<table width="100%">
		<tr>
			<td valign="top">
				<apm:CredSetter ID="WmiAgentCredSetter" CredentialOwner="Core" HideTestControl="True" AlwaysShown="True" runat="server" />
                <br/>
				<apm:CredSetter ID="SnmpIcmpCredSetter" CredentialOwner="Apm" HideTestControl="True" AlwaysShown="True"  runat="server" />
			</td>
			<td width="1%" valign="top">
				<apm:CredsTip ID="CredsTip" BackgroundColor="#FFFDCC" BorderColor="#EACA7F" runat="server"/>  
			</td>
		</tr>
	</table>
	<div class="sw-btn-bar-wizard">
	    <orion:LocalizableButton ID="BackButton" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="BackButton_OnClick"/>
        <orion:LocalizableButton ID="AssignButton" runat="server" Text="<%$ Resources : APMWebContent , AssignTemplatesToGroupsWizard_AssignGroupsButton %>" DisplayType="Primary" OnClick="AssignButton_OnClick" OnClientClick="if (IsDemoMode()) return DemoModeMessage();"/>
        <orion:LocalizableButton ID="CancelButton" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelButton_OnClick" CausesValidation="false"/>
    </div>
</asp:Content>