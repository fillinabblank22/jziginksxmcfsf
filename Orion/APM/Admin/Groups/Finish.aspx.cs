﻿using System;

using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Groups;

public partial class Orion_APM_Admin_Templates_Assign_Finish : AssignTemplatesToGroupsPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GroupFinishedMessage.Text = InvariantString.Format(
            Resources.APMWebContent.AssignTemplatesToGroupsWizard_FinishPageTitle,
            Workflow.SelectedTemplates.Count, Workflow.SelectedGroupsInfo.Count);

        if (!IsPostBack)
        {
            DoneButton.AddEnterHandler(0);
        }
    }

    protected void AssignMoreButton_OnClick(object sender, EventArgs e)
    {
        ResetSession();
        Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
    }

    protected void DoneButton_OnClick(object sender, EventArgs e)
    {
        ResetSession();
        Response.Redirect("/Orion/Apm/Summary.aspx", true);
    }

    protected void TemplatesTable_OnInit(object sender, EventArgs e)
    {
        TemplatesTable.DataSource = Workflow.SelectedTemplates;
        TemplatesTable.DataBind();
    }

    protected void GroupsTable_OnInit(object sender, EventArgs e)
    {
        GroupsTable.DataSource = Workflow.SelectedGroupsInfo;
        GroupsTable.DataBind();
    }

    protected string GetGroupStatusIconLink(int id, int status)
    {
        return InvariantString.Format("/Orion/StatusIcon.ashx?entity=Orion.Groups&id={0}&status={1}&size=small", id, status);
    }

    protected string GetGroupLink(int id)
    {
        return InvariantString.Format("/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:{0}", id);
    }

    protected string GetLearnMoreLink()
    {
        return SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionSAMAssignTempToGroupLMFinish");
    }
}
