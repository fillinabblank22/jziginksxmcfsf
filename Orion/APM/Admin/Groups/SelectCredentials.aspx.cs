﻿using System;
using System.Linq;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.UI.Groups;

public partial class Orion_APM_Admin_Templates_Assign_SelectCredentials : AssignTemplatesToGroupsPageBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var selectedTemplatesInfo = Workflow.SelectedTemplates.Select(item => new SelectNodeTreeItem(item.ID, item.Name)).ToList();

        WmiAgentCredSetter.SelectedTemplatesInfo = selectedTemplatesInfo;
        WmiAgentCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;

        SnmpIcmpCredSetter.SelectedTemplatesInfo = selectedTemplatesInfo;
        SnmpIcmpCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;
    }

    protected void Page_Load(Object sender, EventArgs e) 
	{
		AssignButton.AddEnterHandler(0);
	}

	protected void BackButton_OnClick(Object sender, EventArgs e)
	{
        ClearSession();
        GotoPreviousPage();
	}

	protected void AssignButton_OnClick(Object sender, EventArgs e)
	{
        if (!WmiAgentCredSetter.IsValid || !SnmpIcmpCredSetter.IsValid)
	    {
	        return;
	    }

        Workflow.UseCredentials(WmiAgentCredSetter.SelectedNodesInfo);
        Workflow.AgentWmiCredentialsId = WmiAgentCredSetter.CredentialSetId;

        Workflow.UseCredentials(SnmpIcmpCredSetter.SelectedNodesInfo);
        Workflow.SnmpIcmpCredentialsId = SnmpIcmpCredSetter.CredentialSetId;

	    Workflow.CreateApplications();

	    ClearSession();
	    GotoNextPage();
	}

    protected void CancelButton_OnClick(Object sender, EventArgs e)
    {
        ClearSession();
        CancelWizard();
    }

    private void ClearSession()
    {
        WmiAgentCredSetter.ClearSession();
        SnmpIcmpCredSetter.ClearSession();
    }
}