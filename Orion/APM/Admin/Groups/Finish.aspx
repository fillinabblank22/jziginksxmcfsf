﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/Groups/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="Finish.aspx.cs" Inherits="Orion_APM_Admin_Templates_Assign_Finish" %>

<%@ Import Namespace="SolarWinds.APM.Common.Extensions.System" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI" %>

<asp:Content ID="FinishStepContent" ContentPlaceHolderID="WizardContentPlaceholder" Runat="Server">
    <orion:Include ID="Styles" runat="server" File="APM/Admin/Groups/AssignWizard.css" />
    <orion:Include ID="NodesToBeAssignedDialogInclude" runat="server" Module="APM" File="Admin/Js/NodesToBeAssignedDialog.js"/>
    <h3><asp:Label runat="server" ID="GroupFinishedMessage"></asp:Label></h3>
    <br/>
    <div class="tablesPanel">
        <h4><%= Resources.APMWebContent.AssignTemplatesToGroupsWizard_AppTemplatesGridTitle %></h4>
        <asp:Repeater ID="TemplatesTable" runat="server" OnInit="TemplatesTable_OnInit">
            <HeaderTemplate>
                <table class="dataTable">
                    <thead>
                        <tr>
                            <td class="firstColumn" colspan="2"><%= Resources.APMWebContent.AssignTemplatesToGroupsWizard_AppTemplatesGridNameCol %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="<%# Container.ItemIndex % 2 == 0 ? "" : "oddRow" %>">
                        <td class="firstColumn imgColumn">
                            <img src="/Orion/APM/images/Admin/icon_application_template.gif" />
                        </td>
                        <td>
                            <%#HttpUtility.HtmlEncode(((TemplateInfo)Container.DataItem).Name)%>
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <br/>
        <h4><%= Resources.APMWebContent.AssignTemplatesToGroupsWizard_GroupsGridTitle %></h4>
        <asp:Repeater ID="GroupsTable" runat="server" OnInit="GroupsTable_OnInit">
            <HeaderTemplate>
                <table class="dataTable">
                    <thead>
                        <tr>
                            <td class="firstColumn" colspan="2"><%= Resources.APMWebContent.AssignTemplatesToGroupsWizard_GroupsGridNameCol %></td>
                            <td class="lastColumn"><%= Resources.APMWebContent.AssignTemplatesToGroupsWizard_GroupsGridCountCol %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="<%# Container.ItemIndex % 2 == 0 ? "" : "oddRow" %>">
                        <td class="firstColumn imgColumn">
                            <img src="<%# GetGroupStatusIconLink(((SelectNodeTreeItem)Container.DataItem).Id,((SelectNodeTreeItem)Container.DataItem).Status) %>" />
                        </td>
                        <td>
                            <a href="<%# GetGroupLink(((SelectNodeTreeItem)Container.DataItem).Id) %>"><%#((SelectNodeTreeItem)Container.DataItem).Name%></a>
                        </td>
                        <td class="lastColumn countColumn">
                            <a href="#" onclick="
                                var nodesToBeAssignedDlg = new SW.APM.TemplateGroupAssignment
                                    .NodesToBeAssignedDialog(<%#((SelectNodeTreeItem)Container.DataItem).Id%>, 
                                                             '<%#((SelectNodeTreeItem)Container.DataItem).Name%>', 
                                                             <%#Workflow.SelectServersOnly.ToInvariantString().ToLower()%>);
                                nodesToBeAssignedDlg.show();
                                return false;
                                ">
                                <%#((SelectNodeTreeItem)Container.DataItem).Children.Count%>

                            </a>
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <div class="tipMessage">
            <table class="sw-help-hint">
                <tr>
                    <td>
                        <img class="tipImage" src="/Orion/images/lightbulb_tip_16x16.gif"/>
                    </td>
                    <td>
                        <p>
                            <%= Resources.APMWebContent.AssignTemplatesToGroupsWizard_InfoText %><br>
                            <a href="<%= GetLearnMoreLink()%>" target='_blank'><%= Resources.APMWebContent.SelectServersOnlyTooltipLearnMore %></a>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="AssignMoreButton" runat="server" Text="<%$ Resources : APMWebContent , AssignTemplatesToGroupsWizard_AssignMoreButton %>" DisplayType="Secondary" OnClick="AssignMoreButton_OnClick"/>
        <orion:LocalizableButton ID="DoneButton" runat="server" LocalizedText="Done" DisplayType="Primary" OnClick="DoneButton_OnClick" CausesValidation="false"/>
    </div>
</asp:Content>