﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.UI.Groups;

public partial class Orion_APM_Admin_Templates_Assign_SelectGroups : AssignTemplatesToGroupsPageBase
{
   protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string postData = Server.UrlDecode(Request.Form["postData"]);
            if (postData != null)
            {
                Workflow.HasStarted = true;

                var serializer = new JavaScriptSerializer();
                var templateIds = serializer.Deserialize<List<int>>(postData);

                Workflow.SelectedTemplates = new TemplateInfoList(templateIds);
            }
            else if (Workflow.SelectedGroupsInfo != null && Workflow.SelectedGroupsInfo.Count > 0)
            {
                SearchableGroupSelection.StoreSelectedGroups(Workflow.SelectedGroupsInfo);
            }
            else
            {
                Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
            }

            NextButton.AddEnterHandler(0);
        }
    }

    protected void NextButton_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Workflow.SelectServersOnly = ServersOnly.Checked;
            Workflow.AutoDelete = rblAutoDelete.SelectedItem.Value == "yes";

            Workflow.SelectedGroupsInfo.Clear();
            Workflow.SelectedNodesInfo.Clear();

            SearchableGroupSelection.GetSelectedGroups()
                .ForEach(g =>
                {
                    Workflow.SelectedGroupsInfo.Add(g);

                    g.Children = new TemplateGroupAssignmentDAL()
                        .GetNodesForGroup(g.Id, Workflow.SelectServersOnly)
                        .Select(n =>
                        {
                            n.Parrent = g;
                            return n;
                        }).ToList();

                    Workflow.SelectedNodesInfo.AddRange(g.Children.Where(n => Workflow.SelectedNodesInfo.All(s => s.Id != n.Id)));
                });

            GotoNextPage();
        }
    }

    protected void CancelButton_OnClick(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected void AtLeastOneGroupValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = SearchableGroupSelection.GetSelectedGroups().Count > 0;
    }
}
