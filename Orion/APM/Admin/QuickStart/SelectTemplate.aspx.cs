﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Common.Extensions.System;

public partial class Orion_APM_Admin_QuickStart_SelectTemplate : QuickStartPageBase
{
	public static readonly String TestSessionKeyQuickStart = "KeyTR_{0}"
		.FormatInvariant("orion_apm_admin_quickstart_selectcredentialsandtest_aspx".ToLowerInvariant().GetHashCode().ToString(System.Globalization.CultureInfo.InvariantCulture).Replace('-', 'F'));

    public string NoNodes
    {
        get
        {
            var count = SwisDAL.GetNodesCount();
            return (count == 0).ToString().ToLower();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
		Session.RemoveThatStartsWith(TestSessionKeyQuickStart);

        if(!IsPostBack)
        {
            LoadTagsCombo();

			imgbNext.AddEnterHandler(0);
        }
    }

    private void LoadTagsCombo()
    {
        using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            this.Tags.DataSource = bl.GetTagsWithoutCustomApplicationTypes(-1, null);
            this.Tags.DataBind();
        }

        ListItem item = this.Tags.Items.FindByValue(Workflow.SelectedTemplateTag);
        if (item != null)
            item.Selected = true;
    }

    protected void OnNext(object sender, EventArgs e)
    {
        string templateId = this.Request.Form["template"];
        string selectedTag = Tags.SelectedValue;

        if (templateId == null)
            return;

        Workflow.HasStarted = true;
        Workflow.SelectedTemplateTag = selectedTag;
        Workflow.SelectedTemplateId = Int32.Parse(templateId, CultureInfo.InvariantCulture);

        GotoNextPage();
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

}
