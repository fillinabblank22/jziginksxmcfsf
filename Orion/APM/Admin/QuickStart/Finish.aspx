﻿<%@ Page Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_100 %>" Language="C#" MasterPageFile="~/Orion/APM/Admin/QuickStart/QuickStartWizard.master" AutoEventWireup="true" CodeFile="Finish.aspx.cs" Inherits="Orion_APM_Admin_QuickStart_Finish" %>

<%@ Register Src="~/Orion/APM/Controls/AssignComponentsFinished.ascx" TagPrefix="apm" TagName="AssignComponentsFinished" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <apm:AssignComponentsFinished runat="server" ID="finishedMessage" />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="CustomText" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_99 %>" DisplayType="Secondary" OnClick="OnCreateMoreApplications"/>
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Done" OnClick="OnDone" DisplayType="Primary" CausesValidation="false" />
    </div>
   
</asp:Content>

