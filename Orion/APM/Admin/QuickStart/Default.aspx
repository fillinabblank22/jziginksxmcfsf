﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/APM/Admin/QuickStart/QuickStartWizard.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_APM_Admin_QuickStart_Default" %>
    
<%@ Import namespace="SolarWinds.APM.Web.UI"%>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        ResetSession();
        Response.Redirect(Workflow.WizardSteps.FirstStep(), true);
    }
</script>

