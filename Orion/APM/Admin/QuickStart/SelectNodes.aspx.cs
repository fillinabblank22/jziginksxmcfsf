﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_QuickStart_SelectNodes : QuickStartPageBase
{
   protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string postData = Server.UrlDecode(Request.Form["template"]);

            if (!Workflow.HasStarted)
            {   // This page is only valid if we came from the template selection page that sets "wizard" to Started". 
                Response.Redirect("SelectTemplate.aspx");
            }
			if (Workflow.SelectedNodesInfo != null)
			{
				SearchableNodeSelection.StoreSelectedNodes(Workflow.SelectedNodesInfo);
			}
			imgbNext.AddEnterHandler(0);
        }
    }

    protected void OnBack(object sender, EventArgs e)
    {
        GotoPreviousPage();
    }
    
    protected void OnNext(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Workflow.SelectedNodesInfo = SearchableNodeSelection.GetSelectedNodes();
            GotoNextPage();
        }
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected string TemplateName
    {
        get
        {
            return Workflow.SelectedTemplateName;
        }
    }
   
    protected void valAtLeastOneNode_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = SearchableNodeSelection.GetSelectedNodes().Count > 0;
    }
}
