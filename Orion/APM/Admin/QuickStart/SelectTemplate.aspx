<%@ Page Title="<%$ Resources: APMWebContent, APMWEBDATA_TM0_30 %>" Language="C#" MasterPageFile="~/Orion/APM/Admin/QuickStart/QuickStartWizard.master" 
    AutoEventWireup="true" CodeFile="SelectTemplate.aspx.cs" Inherits="Orion_APM_Admin_QuickStart_SelectTemplate" %>
<%@ Import Namespace="Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
<link rel="stylesheet" type="text/css" href="../ExistingElements.css" />

<style type="text/css">
#templateDetailsBody
{
	padding: 10px;
	height: 210px;
	overflow: auto;
}    

#templateDetailsLayout div
{
	font-size: 8pt;
}
            
#templateDetailsLayout .Label
{
	font-weight: bold;
	font-size: 8pt;

}

#templateDetailsLayout #components
{
	list-style: none;
	padding: 5px 15px;
	border: solid 1px #dfdfde;
	margin-top: 5px;
}

#templateDetailsLayout #components li
{
	background: url(/Orion/APM/Images/StatusIcons/Components/small-template.gif) left center no-repeat;
	padding-left: 25px;
	height: 23px;
	line-height: 20px;
}

.instructions
{
	background-color: #E4F1F8;
	background-image: url(../../Images/Admin/icon_lightbulb.gif);
    background-position: 10px 10px;
    background-repeat: no-repeat;	
    padding: 10px 40px;
	font-size: 8pt;
	width: 250px;
	text-align:left;
	float: right;
}

.instructions a
{
	text-decoration: underline;
}

    
    </style>


<script type="text/javascript">
    function loadTemplateList() {
        var tagName = $('select.TagCombo').val();

        var templateList = $("#TemplateList").text("<%= APMWebContent.APMWEBDATA_TM0_45 %>");

        var rowSnippet = '<li class="GroupItem">' +
                         '<input style="vertical-align: middle; margin-left: 5px;" type="radio" name="template" value="{0}" /> ' +
                         '<img style="vertical-align: middle; margin-left: 3px;" src="/Orion/APM/images/Admin/icon_application_template.gif"> ' +
                         '<a href="#" value="{0}">{1}</a></li>';

        var selectedTemplateId = $('input[name="SelectedTemplateId"]').val();
        ORION.callWebService("/Orion/APM/Services/Templates.asmx",
                             "GetTemplatesForTag", { tagName: tagName, skipCustomAppTypes: true },
                             function(result) {
                                 var data = ORION.objectFromDataTable(result);
                                 templateList.empty();
                                 $(data.Rows).each(function () {
                                     var encodedTemplateName = $("<div/>").text(this.Name).html();
                                     var row = String.format(rowSnippet, this.Id, encodedTemplateName);
                                     var j = $(row).appendTo(templateList);
                                     if (this.Id == selectedTemplateId) {
                                         var ip = j.find('input[name="template"]');
                                         ip.attr("checked", true);
                                     }
                                 });

                                 $("li.GroupItem > a").click(showTemplateDetails);
                             });

    }

    var showTemplateDetailsDialog;

    function showTemplateDetails(e) {
        
        var templateId = $(e.target).attr("value");
        var body = $("#templateDetailsBody").text("<%= APMWebContent.APMWEBDATA_TM0_45 %>");
    
        ORION.callWebService("/Orion/APM/Services/Templates.asmx",
                             "GetTemplateDetails", { templateId: templateId },
                             function(result) {
                                body.empty();
                                
                                var layout = $("#templateDetailsLayout").clone().appendTo(body).removeClass("x-hidden");
                                
                                var desc = result.description.length != 0 ? result.description : "<%= APMWebContent.APMWEBDATA_TM0_43%>";
                                
                                layout.find("#description").text(desc);
                                layout.find("#tags").text(result.tags.join(", "));
                                layout.find("#frequency").text(String.format("<%= APMWebContent.APMWEBDATA_TM0_44 %>", result.frequency));
                                layout.find("#timeout").text(String.format("<%= APMWebContent.APMWEBDATA_TM0_44 %>", result.timeout));
                                
                                var componentList = layout.find("#components");
                                var itemFormat = '<li>{0}</li>';
                                Ext.each(result.components, function(c) {
                                    $(String.format(itemFormat, c)).appendTo(componentList);                                    
                                });
                                 
                             });
    

        if (!showTemplateDetailsDialog) {
            showTemplateDetailsDialog = new Ext.Window({
                applyTo: 'templateDetailsDialog',
                layout: 'fit',
                width: 500,
                height: 300,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.Panel({
                    applyTo: 'templateDetailsBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [{
                    text: '<%= APMWebContent.APMWEB_JS_CODE_VB1_11 %>',
                    handler: function() { showTemplateDetailsDialog.hide(); }
                }]
            });
        }
        
        // Set the window caption
        showTemplateDetailsDialog.setTitle($(e.target).text());
        
        // Set the location
        showTemplateDetailsDialog.alignTo(document.body, "c-c");
        showTemplateDetailsDialog.show(e.target);
        
        return false;
    }

    function OnNextClicked() {
        var checkedTemplate = $('input[name="template"]:checked');

        if (checkedTemplate.length == 0) {
            Ext.Msg.show({
                title: '<%= APMWebContent.APMWEBDATA_TM0_47 %>',
                msg: "<%= APMWebContent.APMWEBDATA_TM0_42 %>",
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.MessageBox.OK
            });
            return false;   
        }

        return true;
    }
    

    $().ready(function() {
        $('select.TagCombo').change(loadTemplateList).change();
        if (<%=  this.NoNodes %>) {
            Ext.Msg.show({
                title: '<%= APMWebContent.APMWEBDATA_TM0_47 %>',
                msg: "<%= APMWebContent.APMWEBDATA_TM0_41 %>",
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.MessageBox.OK,
                fn: function(buttonId, text, opt) { window.location = '/Orion/Discovery/Default.aspx'; }
            });
            
        }
    });
</script>

    <input type="hidden" name="SelectedTemplateId" value="<%= Workflow.SelectedTemplateId %>" />
        
    <table width="100%" >
        <tr>
            <td>
                <h2><%= APMWebContent.APMWEBDATA_TM0_31%></h2>
                <p><%= APMWebContent.APMWEBDATA_TM0_32%></p>
                <br />
                <%= APMWebContent.APMWEBDATA_TM0_33%><br />
                <asp:DropDownList ID="Tags" runat="server"  CssClass="TagCombo" DataTextField="Name" 
                                  DataValueField="Name" AppendDataBoundItems="true" Width="20em">
                    <asp:ListItem Text="<%$ Resources: APMWebContent, APMWEBDATA_TM0_48 %>" Value="" />
                </asp:DropDownList>
            </td>
            <td>
                <div class="instructions">
                    <%= String.Format(APMWebContent.APMWEBDATA_TM0_34, "<a href=\"/Orion/APM/Admin/ApplicationTemplates.aspx\">", "</a>")%>
                </div>
            
            
            </td>
        </tr>
    
    </table>
    
    <br />
    <br />
    <ul id="TemplateList" class="ZebraBackground"></ul>
   
    <br />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButtonLink runat="server" LocalizedText="Back" 
                                     NavigateUrl="javascript:history.go(-1)" DisplayType="Secondary" />
        
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" OnClick="OnNext" 
                                 OnClientClick="return OnNextClicked();" DisplayType="Primary" />
        
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" 
                                 OnClick="OnCancel" DisplayType="Secondary" CausesValidation="false"  />

    </div> 

	<div id="templateDetailsDialog" class="x-hidden">
	    <div class="x-window-header"><%= APMWebContent.APMWEBDATA_TM0_35 %></div>
	    <div id="templateDetailsBody" class="x-panel-body">
	        
	    </div>
	</div>
	
	<div id="templateDetailsLayout" class="x-hidden">
	    <div id="description"><%= APMWebContent.APMWEBDATA_TM0_36 %></div>
	    <br />
	    <div>
	        <span class="Label"><%= APMWebContent.APMWEBDATA_TM0_37 %> </span>
	        <span id="tags"></span>
	    </div>
	    <div>
	        <span class="Label"><%= APMWebContent.APMWEBDATA_TM0_38 %> </span>
	        <span id="frequency"></span>
	    </div>
	    <div>
	        <span class="Label"><%= APMWebContent.APMWEBDATA_TM0_39 %> </span>
	        <span id="timeout"></span>
	    </div>
	    
	    <div style="padding-top:5px;">
	        <%= APMWebContent.APMWEBDATA_TM0_40 %>
	        <ul id="components"></ul>
	    </div>
	    
	    <br />
	    <br />
	</div>

</asp:Content>

