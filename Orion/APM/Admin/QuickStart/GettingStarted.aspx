﻿<%@ Page Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_102 %>" Language="C#" MasterPageFile="~/Orion/APM/Admin/ApmAdmin.master" 
         AutoEventWireup="true" CodeFile="GettingStarted.aspx.cs" Inherits="Orion_APM_Admin_QuickStart_GettingStarted" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="apm" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<asp:Content ID="c0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
    <orion:IconHelpButton HelpUrlFragment="OrionAPMPHConfigSettingsCredLibrary" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ID="c1" ContentPlaceHolderID="hPh" runat="Server">
	<apm:IncludeExtJs ID="IncludeExtJs1" runat="server" Debug="false" Version="3.4"/>
    <orion:Include runat="server" module="APM" File="/Orion/js/OrionCore.js"/>
</asp:Content>

<asp:Content ID="c2" ContentPlaceHolderID="cPh" runat="Server">
	
	<style type="text/css">
		.tableLeftPosition { position: relative; left: 10px; }
		table tr td.RadioButtonsHeader { font-size: 12pt; font-weight: bold; }
		table tr td.RadioButtonsText { font-size: 10pt; }
		table tr td input[type="radio"] { margin-top: 3px; }
		table tr td.RadioButtonsText b { background-color: #fff29b; }
		.instructions { float: right; padding: 10px 10px 10px 40px; font-size: 8pt; width: 250px; text-align: left; background: #E4F1F8 url(../../Images/Admin/icon_lightbulb.gif) no-repeat 10px 10px; }
		.instructions a { text-decoration: underline; }
	</style>
	
	<table width="100%" id="breadcrumb" class="tableLeftPosition">
		<tr>
			<td style="border-bottom-width: 0px; padding: 0px;">
				<a href="/Orion/Admin"><%= Resources.APMWebContent.APMWEBDATA_AK1_103 %></a> &gt; <a href="/Orion/APM/Admin/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_AK1_104 %></a>
				<h1><%= Resources.APMWebContent.APMWEBDATA_AK1_102 %></h1>
			</td>
		</tr>
	</table>
	
	<table style="width: 700px; border: solid 1px #dfdfde; background-color: white;" cellpadding="0" cellspacing="10px" class="tableLeftPosition">
		<tr>
			<td colspan="3" class="RadioButtonsHeader">
			    <%= Resources.APMWebContent.APMWEBDATA_AK1_105 %>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<asp:RadioButton ID="autoAssignRadioButton" runat="server" GroupName="WizardType" />
			</td>
			<td class="RadioButtonsText" valign="top" style="padding-left: 3px;">
				<%= Resources.APMWebContent.APMWEBDATA_AK1_106 %>
			</td>
			<td rowspan="3" valign="top" align="right">
				<div class="instructions">					
                    <%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_109, 
                                      SolarWinds.APM.Common.ApmConstants.ModuleShortName, 
                                      "<img style=\"vertical-align: middle;\" src=\"/Orion/APM/Images/StatusIcons/Small-App-Blank.gif\" />", 
                                      "<img style=\"vertical-align: middle;\" src=\"/Orion/APM/Images/StatusIcons/Components/small-blank.gif\" />") %>
                </div>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<asp:RadioButton ID="manuallyAssignRadioButton" runat="server" GroupName="WizardType" />
			</td>
			<td class="RadioButtonsText" valign="top" style="padding-left: 3px;">
				<%=Resources.APMWebContent.APMWEBDATA_AK1_108 %>
			</td>
		</tr>
		<tr>
			<td>
				<br />
			</td>
		</tr>
		<tr>
			<td colspan="3">
                <div class="sw-btn-bar-wizard">
					<orion:LocalizableButton runat="server" ID="nextImageButton" OnClick="OnNextClick" DisplayType="Primary" LocalizedText="Next" /> 
					<orion:LocalizableButton runat="server" ID="cancelImageButton" LocalizedText="Cancel" CausesValidation="false" DisplayType="Secondary" OnClientClick="javascript:history.back(); return false;" />
				</div>
			</td>
		</tr>
	</table>
</asp:Content>
