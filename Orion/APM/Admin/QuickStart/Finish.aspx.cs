﻿using System;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_QuickStart_Finish : QuickStartPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        finishedMessage.DataSource = Workflow.CreatedApplications;
        finishedMessage.DataBind();

		imgbCancel.AddEnterHandler(0);
    }

    protected void OnCreateMoreApplications(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx", true);
    }

    protected void OnDone(object sender, EventArgs e)
    {
        ResetSession();
        Response.Redirect("/Orion/Apm/Summary.aspx", true);
    }

}
