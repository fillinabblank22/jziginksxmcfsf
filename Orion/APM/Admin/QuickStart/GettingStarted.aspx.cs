﻿using System;
using System.Web.UI;

public partial class Orion_APM_Admin_QuickStart_GettingStarted : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            autoAssignRadioButton.Checked = true;
        }
    }

    protected void OnNextClick(object sender, EventArgs e)
    {
        if (autoAssignRadioButton.Checked)
        {
            Response.Redirect("/Orion/APM/Admin/AutoAssign/SelectNodes.aspx", true);
        }
        else if (manuallyAssignRadioButton.Checked)
        {
            Response.Redirect("/Orion/APM/Admin/QuickStart/Default.aspx", true);
        }        
    }
}
