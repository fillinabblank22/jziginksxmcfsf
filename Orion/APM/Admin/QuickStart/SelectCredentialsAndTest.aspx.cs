﻿using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_QuickStart_SelectCredentialsAndTest : SolarWinds.APM.Web.UI.QuickStartPageBase
{
	#region Event Handlers

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		ctrCoreCredSetter.SelectedTemplatesInfo = new List<SelectNodeTreeItem> { new SelectNodeTreeItem { Id = Workflow.SelectedTemplateId, Name = Workflow.SelectedTemplateName } };
		ctrCoreCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;

        ctrApmCredSetter.SelectedTemplatesInfo = new List<SelectNodeTreeItem> { new SelectNodeTreeItem { Id = Workflow.SelectedTemplateId, Name = Workflow.SelectedTemplateName } };
		ctrApmCredSetter.SelectedNodesInfo = Workflow.SelectedNodesInfo;

		if (!IsPostBack)
        {
            ctrCheckDuplicates.SkipDuplicates = Workflow.SkipDuplicates;
        }
        ctrCheckDuplicates.NodeIDs = Workflow.SelectedNodesId;
        ctrCheckDuplicates.TemplateIDs = new List<int> { Workflow.SelectedTemplateId };

		b2.AddEnterHandler(0);
	}

	protected void OnBack_Click(Object sender, EventArgs e)
	{
        Workflow.SkipDuplicates = ctrCheckDuplicates.SkipDuplicates;

		ClearSession();
		GotoPreviousPage();
	}

	protected void OnNext_Click(Object sender, EventArgs e)
	{
        Workflow.SkipDuplicates = ctrCheckDuplicates.SkipDuplicates;
		if (ctrCoreCredSetter.IsValid && ctrApmCredSetter.IsValid) 
		{
			if (ctrCoreCredSetter.Visible)
			{
				Workflow.UseCredentials(ctrCoreCredSetter.SelectedNodesInfo);
			}
			if (ctrApmCredSetter.Visible)
			{
				Workflow.UseCredentials(ctrApmCredSetter.SelectedNodesInfo);
			}
			Workflow.CreateApplications();

			ClearSession();
			GotoNextPage();
		}
	}

	protected void OnCancel_Click(Object sender, EventArgs e)
	{
		ClearSession();
		CancelWizard();
	}

	#endregion
	
	#region Helper Members

	private void ClearSession()
	{
		ctrCoreCredSetter.ClearSession();
		ctrApmCredSetter.ClearSession();
	}

	#endregion
}