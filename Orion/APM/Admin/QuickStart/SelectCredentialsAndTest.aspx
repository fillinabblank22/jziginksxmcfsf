﻿<%@ Page Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_101 %>" Language="C#" AutoEventWireup="true" CodeFile="SelectCredentialsAndTest.aspx.cs" 
Inherits="Orion_APM_Admin_QuickStart_SelectCredentialsAndTest" MasterPageFile="~/Orion/APM/Admin/QuickStart/QuickStartWizard.master" %>

<%@ Register TagPrefix="apm" TagName="CredSetter" Src="~/Orion/APM/Admin/CredentialSetter.ascx" %>
<%@ Register TagPrefix="apm" TagName="CredsTip" Src="~/Orion/APM/Controls/CredentialTips.ascx" %>
<%@ Register TagPrefix="apm" TagName="CheckDuplicates" Src="~/Orion/APM/Admin/CheckDuplicates.ascx" %>
<asp:Content ID="c1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
	<table width="100%">
		<tr>
			<td valign="top">
				<apm:CredSetter ID="ctrCoreCredSetter" CredentialOwner="Core" runat="server" />
				<apm:CredSetter ID="ctrApmCredSetter" CredentialOwner="Apm" runat="server" />
			</td>
			<td width="1%" valign="top">
				<apm:CredsTip ID="ctrCredsTip" BackgroundColor="#FFFDCC" BorderColor="#EACA7F" runat="server"/>  
                <apm:CheckDuplicates ID="ctrCheckDuplicates" runat="server" />   
                <br/>      
			</td>
		</tr>
	</table>

	<div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="b1" OnClick="OnBack_Click"  LocalizedText = "Back" DisplayType="Secondary" CausesValidation="false"  />
        <orion:LocalizableButton runat="server" ID="b2" OnClick="OnNext_Click"  LocalizedText="CustomText" Text = "<%$ Resources: APMWebContent, APMWEBDATA_AK1_70 %>" DisplayType="Primary" OnClientClick="if (IsDemoMode()) return DemoModeMessage();" />
        <orion:LocalizableButton runat="server" ID="b3" OnClick="OnCancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
    </div>
    
</asp:Content>