﻿<%@ Page Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_65 %>" Language="C#" MasterPageFile="~/Orion/APM/Admin/QuickStart/QuickStartWizard.master" 
    AutoEventWireup="true" CodeFile="SelectNodes.aspx.cs" Inherits="Orion_APM_Admin_QuickStart_SelectNodes" %>

<%@ Register Src="../../Controls/SearchableNodeSelection.ascx" TagPrefix="apm" TagName="SearchableNodeSelection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    
    <h2><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_66, HttpUtility.HtmlEncode(this.TemplateName)) %></h2>
    <p><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_67, HttpUtility.HtmlEncode(this.TemplateName)) %></p>

    <br />
    <apm:SearchableNodeSelection ID="SearchableNodeSelection" runat="server" TreeWidth="100%" TreeBorder="" />
    <br />
    <asp:CustomValidator ID="valAtLeastOneNode" runat="server"
        ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_95 %>" OnServerValidate="valAtLeastOneNode_ServerValidate"/>
    <br />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" LocalizedText="Back" DisplayType="Secondary" OnClick="OnBack" CausesValidation="false" />
        <orion:LocalizableButton runat="server" ID="imgbNext" LocalizedText="Next" DisplayType="Primary" OnClick="OnNext"/>
        <orion:LocalizableButton runat="server" ID="imgbCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false" />
    </div>

</asp:Content>