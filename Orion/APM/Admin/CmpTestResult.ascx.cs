﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.Utility;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_CmpTestResult : System.Web.UI.UserControl
{
	#region Nested Types

	class TestResultConfig 
	{
		public String sessionKey { get; set; }
		public String renderTo { get; set; }
		public String cntID { get; set; }

		public Boolean isAppMode { get; set; }

		public Int32 credID { get; set; }

		public Object[] nodes { get; set; }
		public Object[] components { get; set; }
	}

	#endregion 

	#region Properties

	public String Key { get; set; }

	private String ContainerClientID { get; set; }

	private Boolean IsApplicationMode { get; set; }
	
	private IEnumerable<SelectNodeTreeItem> Nodes { get; set; }

	private IEnumerable<SelectNodeTreeItem> Components { get; set; }

	private TestResultConfig Config 
	{
		get 
		{
			if (Visible) 
			{
				var config = new TestResultConfig
				{
					sessionKey = SessionKey,
					renderTo = RenderTo,
					cntID = ContainerClientID,
					isAppMode = IsApplicationMode,

					credID = Nodes.First().CredentialId,

					nodes = Nodes.Select(item => new Object[] { item.Id, item.Name, item.Status, item.CredentialId }).ToArray(),
					components = Components.Select(item => new Object[] { item.Id, item.Name, item.Status, item.CredentialId }).ToArray()
				};
				return config;
			}
			return null;
		}
	}

	protected String SessionKey
	{
		get { return Key; }
	}
	
	protected String RenderTo
	{
		get { return "ctrTr{0}".FormatInvariant(Key); }
	}

	public override Boolean Visible
	{
		get { return Key.IsValid() && Nodes != null && Nodes.Count() > 0 && Components != null && Components.Count() > 0; }
	}

	#endregion

	#region Initialize Members

	public void Initialize(String containerClientID, IEnumerable<SelectNodeTreeItem> nodes, IEnumerable<SelectNodeTreeItem> components) 
	{ 
		Initialize(containerClientID, nodes, components, false);
	}

	public void Initialize(String containerClientID, IEnumerable<SelectNodeTreeItem> nodes, IEnumerable<SelectNodeTreeItem> components, Boolean isAppMode) 
	{
		ContainerClientID = containerClientID;
		Nodes = nodes;
		Components = components;
		IsApplicationMode = isAppMode;
		
		if (Visible)
		{
			var js = @"
$().ready(function(){{
	SW.APM.Admin.TestResult.show({0});
}});
".FormatInvariant(JsHelper.Serialize(Config));

			ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), js.GetHashCode().ToString(), js, true);
		}
	}

	public void Update(Object nodeID, Object nodeName, Object credID, String action)
	{
		if (Visible) 
		{
			if (credID != null)
			{
				var id = Convert.ToInt32(credID);
				foreach (var node in Nodes)
				{
					node.CredentialId = id;
				}
			}

			var js = @"
$().ready(function(){{
	window[""{0}""].update({1},{2},{3},{4});
}});
".FormatInvariant(SessionKey, JsHelper.Serialize(nodeID), JsHelper.Serialize(nodeName), JsHelper.Serialize(credID), JsHelper.Serialize(action));

			ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), js.GetHashCode().ToString(), js, true);
		}
	}

	public void ClearSession() 
	{
		Session.Remove(SessionKey);
	} 
	
	#endregion
}