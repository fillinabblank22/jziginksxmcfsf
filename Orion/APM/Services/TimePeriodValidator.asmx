﻿<%@ WebService Language="C#" Class="TimePeriodValidatorService" %>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models.ProcessMonitor;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TimePeriodValidatorService : WebService
{

    /// <summary>
    /// WebMethod for validating the absolute time period
    /// </summary>
    /// <param name="startDateString"></param>
    /// <param name="startTimeString"></param>
    /// <param name="endDateString"></param>
    /// <param name="endTimeString"></param>
    /// <returns></returns>
    [WebMethod]
    public SolarWinds.APM.Web.Utility.TimePeriodHelper.ValidateAbsoluteTimePeriodResult ValidateTime(string startDateString, string startTimeString, string endDateString, string endTimeString)
    {
        return TimePeriodHelper.DoValidateTime(startDateString, startTimeString, endDateString, endTimeString);
    }

}
