﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.Extensions" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<script runat="server">

private static Dictionary<string, string> vendorImageFileNames;

// This should really be in an HttpHandler (.ashx) file.  The SwisProxy class
// doesn't handle this well.  We get a Null Exception at HttpContext.Current.Session[""]
// For now, we'll just do this in a page.
// TODO: change to http handler and implement System.Web.SessionState.IRequiresSessionState, see http://stackoverflow.com/questions/1058568/asp-net-how-to-access-session-from-handler
protected void Page_Load(object sender, EventArgs e)
{
	string groupCounts = Request.QueryStringOrForm("GroupCounts") ?? "";
	string groupBy = Request.QueryStringOrForm("groupby") ?? "Vendor";
	string rootNode = Request.QueryStringOrForm("node") ?? "/";
	string uiProvider = Request.QueryStringOrForm("uiProvider");
	
	object data = null;
	if ("getGroupByProps".Equals(Request.QueryStringOrForm("action"), StringComparison.OrdinalIgnoreCase))
	{
		data = GetGroupByProps();
	}
	else 
	{
		if (String.IsNullOrEmpty(groupCounts))
		{
			data = GetTreeNodes(groupBy, rootNode, uiProvider);
		}
		else
		{
			// Load initial group counts
			string itemString = Request.QueryStringOrForm("items") ?? "";
			int[] items = JsHelper.Deserialize<int[]>(itemString);
			data = LoadGroupCounts(groupBy, items);
		}
	}
    
	string content = JsHelper.Serialize(data);

	Response.ContentType = "text/html";
	Response.Write(content);
	Response.End();
}

private List<KeyValuePair<String, String>> GetGroupByProps() 
{
	var usableCustomProperties = new List<String>();
	foreach (var prop in SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetPropNamesForTable("Nodes", false))
	{
		if (SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetTypeForProp("Nodes", prop) != typeof(StringBuilder)) 
		{
			usableCustomProperties.Add(prop);
		}
	}

	var result = new List<KeyValuePair<String, String>>();
    result.Add(new KeyValuePair<String, String>(Resources.APMWebContent.APMWEBCODE_VB1_41, "Vendor"));
    result.Add(new KeyValuePair<String, String>(Resources.APMWebContent.APMWEBCODE_VB1_42, "MachineType"));
    result.Add(new KeyValuePair<String, String>(Resources.APMWebContent.APMWEBCODE_VB1_43, "SNMPVersion"));
	foreach (string customProperty in usableCustomProperties)
	{
		result.Add(new KeyValuePair<String, String>(customProperty, String.Format("CustomProperties.{0}", customProperty)));
	}
	return result;
}
	
private Dictionary<string, int[]> LoadGroupCounts(string groupBy, int[] items)
{
	Dictionary<string, int[]> results = new Dictionary<string, int[]>();

	const int PropertyIndex = 0;
	const int AllCountIndex = 1;
	const int SelCountIndex = 2;

	DataTable table = GetNodePropertiesForCateogry(groupBy, items);

	foreach (DataRow row in table.Rows)
	{
		string group = row[PropertyIndex].ToString();
		results[group] = new int[] { Convert.ToInt32(row[AllCountIndex]), Convert.ToInt32(row[SelCountIndex]) };
	}
	return results;
}

// NOTE: This should be part of SwisDAL (or maybe just alter the existing method)
public DataTable GetNodePropertiesForCateogry(string category, int[] filterNodeIds)
{
	String allQuery = @"
SELECT n.{0} AS theProperty, COUNT(n.NodeID) AS allCount
FROM Orion.Nodes n
WHERE {1}
GROUP BY n.{0}
";
	String selQuery = @"
SELECT n.{0} AS theProperty, COUNT(n.NodeID) AS selCount
FROM Orion.Nodes n
WHERE n.NodeID IN ({1}) AND {2}
GROUP BY n.{0}
";
	Action<DataTable, String> prepareData = (data, key) =>
	{
		for (Int32 i = 0; i < data.Rows.Count; i++)
		{
			var name = data.Rows[i]["theProperty"];
			if (name == DBNull.Value || name.ToString().Trim().Length == 0) 
			{
                name = Resources.APMWebContent.APMWEBCODE_VB1_77;
			}
			
			var row = data.Select(String.Format("theProperty = '{0}'", name)).FirstOrDefault();
			if (row == null || row == data.Rows[i])
			{
				data.Rows[i]["theProperty"] = name;
			}
			else
			{
				//FB#18673 Error in node assignment page
				data.Rows[i][key] = Convert.ToInt32(data.Rows[i][key]) + Convert.ToInt32(row[key]);
				data.Rows.Remove(row);
				i -= 1;
			}
		}
	};
	
	String whereClause = "-1";
	if (filterNodeIds != null && filterNodeIds.Length > 0)
	{
		whereClause = String.Join(",", new List<Int32>(filterNodeIds).ConvertAll(id => id.ToString()).ToArray());
	}
	String filter = Request.QueryStringOrForm("nodeFilter");
	if (String.IsNullOrEmpty(filter)) 
	{
		filter = "1=1";
	}

	DataTable allTable = SwisProxy.ExecuteQuery(String.Format(allQuery, category, filter));
	prepareData(allTable, "allCount");

	DataTable selTable = SwisProxy.ExecuteQuery(String.Format(selQuery, category, whereClause, filter));
	prepareData(selTable, "selCount");

	allTable.Columns.Add("selCount", typeof(Int32));
	foreach (DataRow row in allTable.Rows)
	{
		row["selCount"] = 0;

		var rows = selTable.Select(String.Format("theProperty = '{0}'", row["theProperty"]));
		if (rows.Length > 0)
		{
			row["selCount"] = rows[0]["selCount"];
		}
	}
	return allTable;
}

private List<ExtTreeNode> GetTreeNodes(string groupBy, string rootNode, string uiProvider)
{
	List<ExtTreeNode> nodes = new List<ExtTreeNode>();
	if (rootNode == "/")
	{
		AddRootNodes(nodes, groupBy, uiProvider);
	}
	else
	{
		GetChildNodes(nodes, groupBy, rootNode, uiProvider);
	}


	return nodes;
}

private void AddRootNodes(List<ExtTreeNode> nodes, string groupBy, string uiProvider)
{
	bool showVendorIcons = (groupBy.Equals("vendor", StringComparison.OrdinalIgnoreCase));

	if ((vendorImageFileNames == null) && showVendorIcons)
	{
		vendorImageFileNames = SwisDAL.GetVendorIconFileNames(false);

		// Replace empty vendors with 'Unknown' text
		if (vendorImageFileNames != null)
		{
			vendorImageFileNames.Remove(" ");
			vendorImageFileNames["Unknown"] = "Unknown.gif";
		}
	}

	DataTable table = SwisDAL.GetNodePropertiesForCateogry(groupBy, Request.QueryStringOrForm("nodeFilter"));
    string propertyText = String.Empty;

	// Replace empty vendors with 'Unknown' text
	foreach (DataRow row in table.Rows)
        if (row["theProperty"].ToString().Trim() == string.Empty)
        {
            row["theProperty"] = "Unknown";
            propertyText = Resources.APMWebContent.APMWEBCODE_VB1_77;//"Unknown" - text that will be displayed on UI
        }

    Dictionary<string, bool> uniquePropertyValues = new Dictionary<string, bool>(StringComparer.CurrentCultureIgnoreCase);

	foreach (DataRow dataRow in table.Rows)
	{
		string property = dataRow["theProperty"].ToString();
		if (!uniquePropertyValues.ContainsKey(property))
			uniquePropertyValues[property] = true;
	}

	foreach (string property in uniquePropertyValues.Keys)
	{
		// FB31069 - slash in group name is parsed as path separator, replaced with double slash
        ExtTreeNode node = new ExtTreeNode("/" + property.Replace("/", "//"), (string.IsNullOrEmpty(propertyText) ? property : (propertyText.ToLower().Contains("unknown") ? property : propertyText)), false, uiProvider);

		if (showVendorIcons)
			node.icon = GetImageForKey(property);

			nodes.Add(node);
		}
	}

	protected string GetImageForKey(string key)
	{
		const string vendorImageDir = "/NetPerfMon/images/Vendors/";
		if (vendorImageFileNames != null && vendorImageFileNames.ContainsKey(key))
		{
			string filename = vendorImageFileNames[key];
			if (!string.IsNullOrEmpty(filename))
			{
				string virtualPath = vendorImageDir + vendorImageFileNames[key].ToString().Trim();
				string physicalPath = Server.MapPath(virtualPath);
				if (File.Exists(physicalPath))
					return virtualPath;
			}
		}

		return vendorImageDir + "Unknown.gif";
	}


	private void GetChildNodes(List<ExtTreeNode> nodes, string groupBy, string rootNode, string uiProvider)
	{
		// FB31069 - encode "//", which represents single slash in group id
		string[] pathParts = rootNode.Replace("//", "&#47;").Split(new char[] { '/' }, StringSplitOptions.None);
		pathParts = pathParts.Select(o => o.Replace("&#47;", "/")).ToArray();

		if (pathParts.Length == 0)
			return;

		DataTable table = SwisDAL.GetNodesByProperty(groupBy, pathParts[pathParts.Length - 1], Request.QueryStringOrForm("nodeFilter"));

		foreach (DataRow dataRow in table.Rows)
		{
			string name = HttpUtility.HtmlEncode(dataRow["Caption"].ToString());
		    var nodeId = (int) dataRow["NodeId"];
			ExtTreeNode node = new ExtTreeNode(rootNode + "/" + nodeId, name, true, uiProvider);
            node.icon = SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL((int)dataRow["NodeId"], (int)dataRow["Status"], (int)dataRow["ChildStatus"]);
			node.nodeId = nodeId;
			node.ipAddress = dataRow["IPAddress"].ToString();
			node.status = (int)dataRow["Status"];
			
			nodes.Add(node);
		}
	}

	public class ExtTreeNode
	{
		public ExtTreeNode(string id, string text, bool leaf, string uiProvider)
		{
			this.id = id;
			this.text = text;
			this.leaf = leaf;
			this.icon = null;
			this.uiProvider = string.IsNullOrEmpty(uiProvider) ? "SW.APM.SWTreeNodeUI" : uiProvider;
			this.ipAddress = null;
		}

		public string id;
		public string text;
		public int status;
		public bool leaf;
		public string icon;
		public string uiProvider;

		public string ipAddress;
		public int nodeId;
	}    

</script>

