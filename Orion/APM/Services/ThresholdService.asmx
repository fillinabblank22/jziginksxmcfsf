﻿<%@ WebService Language="C#" Class="SolarWinds.APM.Web.ThresholdService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DAL;

namespace SolarWinds.APM.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class ThresholdService : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetCriticalOrDefaultThresholdByName(string uniqueId, string thresholdName, int? appId, int? appitemiId, double defaultLevel)
        {
            var threshold = ThresholdManager.GetThreshold(uniqueId, thresholdName, appId, appitemiId);
            return threshold == null ? defaultLevel : threshold.CriticalLevel;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetCriticalThresholdByName(string uniqueId, string thresholdName, int? appId, int? appitemiId)
        {
            var threshold = ThresholdManager.GetThreshold(uniqueId, thresholdName, appId, appitemiId);
            return threshold == null ? (object) null : threshold.CriticalLevel;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetWarningOrDefaultThresholdByName(string uniqueId, string thresholdName, int? appId, int? appitemiId, double defaultLevel)
        {
            var threshold = ThresholdManager.GetThreshold(uniqueId, thresholdName, appId, appitemiId);
            return threshold == null ? defaultLevel : threshold.WarnLevel;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetWarningThresholdByName(string uniqueId, string thresholdName, int? appId, int? appitemiId)
        {
            var threshold = ThresholdManager.GetThreshold(uniqueId, thresholdName, appId, appitemiId);
            return threshold == null ? (object)null : threshold.WarnLevel;
        }
    }
}
