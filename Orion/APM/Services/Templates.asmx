﻿<%@ WebService Language="C#" Class="Templates" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.Models;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.Swis;
using SolarWinds.APM.Web.Swis.Templates;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Templates : ApmWebService
{
    private readonly IPageableQueryService pageableQueryService;

        
    public Templates() : this(new TemplatePageableQueryService())
    {
    }
        
    public Templates(IPageableQueryService pageableQueryService)
    {
        this.pageableQueryService = pageableQueryService;
    }


    [WebMethod(EnableSession = true)]
    public bool IsTemplateExported()
    {
        if (HttpContext.Current.Session["templatesReadyToDownload"] != null && (bool)HttpContext.Current.Session["templatesReadyToDownload"] == true)
        {
            HttpContext.Current.Session["templatesReadyToDownload"] = null;
            return true;            
        }
        
        return false;
    }
    
    [WebMethod(EnableSession = true)]
    public SolarWinds.APM.Web.PageableDataTable GetTemplatesPaged(string property, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;
            

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        value = value.Replace("'", "''");
        search = search.Replace("'", "''");

        string swql = pageableQueryService.GenerateQuery(pageSize, startRowNumber, sortColumn, sortDirection, property, value, search);

        DataTable pageData = SwisProxy.ExecuteQuery(swql);
        AddTagDataToTemplateTable(pageData);
		PreparePagedTemplates(pageData);

        var emptyText = string.IsNullOrEmpty(search)
            ? Resources.APMWebContent.APMWEBCODE_VB1_11
            : Resources.APMWebContent.APMWEBCODE_VB1_12;
        return new SolarWinds.APM.Web.PageableDataTable(pageData, GetRowCount(property, value, search), emptyText);
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetTemplatesForTag(string tagName, bool skipCustomAppTypes)
    {
        return GetTemplatesForTagWithSkipTagsInternal(tagName, null, skipCustomAppTypes);
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetTemplatesForTagWithSkipTags(string tagName, List<string> skipTagNames, bool skipCustomAppTypes)
    {
        return GetTemplatesForTagWithSkipTagsInternal(tagName, skipTagNames, skipCustomAppTypes);
    }

    private DataTable GetTemplatesForTagWithSkipTagsInternal(string tagName, List<string> skipTagNames, bool skipCustomAppTypes)
    {
        string swql = @"
            SELECT DISTINCT A.ApplicationTemplateID AS Id, A.Name
            FROM Orion.APM.ApplicationTemplate A
            LEFT JOIN Orion.APM.Tag T ON A.ApplicationTemplateID = T.TemplateId 
            WHERE A.IsMockTemplate <> 1";

        if (skipCustomAppTypes)
        {
            swql += " AND A.CustomApplicationType IS NULL";
        }

        if (!String.IsNullOrEmpty(tagName))
        {
            swql += String.Format(" AND T.TagName = '{0}' AND", tagName.Replace("'", "''"));
        }

        if ((skipTagNames != null) && (skipTagNames.Count > 0))
        {
            if (!swql.EndsWith("AND"))
            {
								swql += " AND";
            }
            swql += String.Format(" A.ApplicationTemplateID NOT IN (SELECT T.TemplateId FROM Orion.APM.Tag T WHERE T.TagName IN ({0}))", String.Join(",", skipTagNames.ToArray()));
        }

        if (swql.EndsWith("AND"))
        {
            swql = swql.Remove(swql.Length - 3, 3);
        }

        swql += " ORDER BY A.Name";

        return SwisProxy.ExecuteQuery(swql);
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetTemplates(List<Int32> templateIDs)
    {
        string swql = @"
SELECT DISTINCT A.ApplicationTemplateID AS Id, A.Name
FROM Orion.APM.ApplicationTemplate A
";
        if (templateIDs != null && templateIDs.Count > 0)
        {
            swql += String.Format(" WHERE A.ApplicationTemplateID IN ({0})", String.Join(",", templateIDs.ConvertAll<String>(delegate(Int32 id) { return id.ToString(); }).ToArray()));
        }

        swql += " ORDER BY A.Name";

        return SwisProxy.ExecuteQuery(swql);
    }

    
    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        // return "All" as the blank/unknown value
        string swql = @"
SELECT tmp.theValue, tmp.theCount
FROM (
	SELECT '' as theValue, count(AT.ApplicationTemplateID) as theCount
	FROM Orion.APM.ApplicationTemplate AT
	WHERE AT.IsMockTemplate <> 1
	UNION ALL (        
	  SELECT T.TagName as theValue, count(T.TagName) as theCount
	  FROM Orion.APM.Tag T
	  GROUP BY T.TagName
	)
) as tmp
ORDER BY tmp.theValue ASC
";

        return SwisProxy.ExecuteQuery(swql);
    }

    [WebMethod]
    public void DeleteTemplates(IList<int> templateIds)
    {
        VerifyRights();
        
        try
        {
            List<int> viewIds = new List<int>();
            using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                foreach (int id in templateIds)
                {
                    ApplicationTemplate template = bl.GetApplicationTemplate(id);
                    if (template.ViewId > 0) viewIds.Add(template.ViewId);
                }
                bl.DeleteApplicationTemplates(templateIds);
            }
            viewIds.ForEach(id => ViewManager.DeleteView(ViewManager.GetViewById(id)));
        }
        catch
        {
            //do nothing
        }
    }

    [WebMethod(EnableSession = true)]
    public int CreateNewTemplate()
    {
        VerifyRights();

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            int templateId = businessLayer.CreateNewApplicationTemplate();
            return templateId;
        }
    }

    [WebMethod]
    public void CopyTemplates(IList<int> templateIds)
    {
        VerifyRights();

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            foreach (int id in templateIds)
            {
                businessLayer.DuplicateApplicationTemplate(id);
            }
        }
    }


    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetTemplateDetails(int templateId)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            ApplicationTemplate template = businessLayer.GetApplicationTemplate(templateId);

            result["description"] = template.Description;
            result["frequency"] = (int)template.Frequency.TotalSeconds;
            result["timeout"] = (int)template.Timeout.TotalSeconds;

            result["components"] = template.ComponentTemplates.ConvertAll<string>(delegate(ComponentTemplate t)
            {
                return t.Name;
            });

            result["tags"] = template.Tags.ConvertAll<string>(delegate(TagInfo t)
            {
                return t.Name;
            });

        }


        return result;
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetDuplicates(List<Int32> nodeIDs, List<Int32> tplIDs)
    {
        String QUERY_TPL = @"
SELECT DISTINCT 
	at.ApplicationTemplateID AS TplID, at.Name AS TplName, 
	n.NodeID, n.Caption AS NodeName, n.Status AS NodeLED,
	a.ApplicationID AS AppID, a.Name AS AppName, aps.Availability, '' AS AppLED, tmpCount.AppCount
FROM (SELECT ApplicationID, NodeID, ApplicationTemplateID, Name FROM Orion.APM.Application) AS a
JOIN Orion.APM.ApplicationTemplate AS at ON at.ApplicationTemplateID = a.ApplicationTemplateID
JOIN Orion.Nodes AS n ON n.NodeID = a.NodeID
JOIN (
	SELECT a1.ApplicationTemplateID AS TplID, a1.NodeID, COUNT(a1.ApplicationID) AS AppCount
	FROM  (SELECT ApplicationID, NodeID, ApplicationTemplateID, Name FROM Orion.APM.Application) AS a1
	WHERE a1.ApplicationTemplateID IN ({0}) AND a1.NodeID IN ({1})
	GROUP BY a1.ApplicationTemplateID, a1.NodeID
) AS tmpCount ON tmpCount.TplID = a.ApplicationTemplateID AND tmpCount.NodeID = a.NodeID
LEFT JOIN Orion.APM.CurrentApplicationStatus AS aps ON aps.ApplicationID = a.ApplicationID
";
        DataTable res = SwisProxy.ExecuteQuery(
          String.Format(
              QUERY_TPL,
              String.Join(",", tplIDs.ConvertAll<String>(id => id.ToString()).ToArray()),
              String.Join(",", nodeIDs.ConvertAll<String>(id => id.ToString()).ToArray())
          )
      );
        SolarWinds.APM.Web.DisplayTypes.ApmStatus status = new SolarWinds.APM.Web.DisplayTypes.ApmStatus();
        foreach (System.Data.DataRow row in res.Rows)
        {
            status.Value = DBNull.Value.Equals(row["Availability"]) ? Status.Undefined : (Status)row["Availability"];
            row["AppLED"] = status.ToString("smallappimgpath", null);
        }
        res.Columns.Remove("Availability");
        res.AcceptChanges();

        return res;
    }

		[WebMethod]
		public Int32 AssociateTemplateWithView(Int32 tplID, Boolean createNew)
		{
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				var tpl = bl.GetApplicationTemplate(tplID);
				if (tpl.IsMockTemplate)
				{
					return ApmApplication.DefaultViewID;
				}
				if (createNew)
				{
                    ViewInfo newView = ApmViewManager.CreateNewView(tpl);
                    tpl.ViewId = newView.ViewID;
				}
                else
                {
                    tpl.ViewId = ApmApplication.DefaultViewID;
                }

				bl.UpdateApplicationTemplateEx(tpl, false);

				return tpl.ViewId;
			}
		}
	
		[WebMethod]
		public void DeleteViews(Int32[] tplIDs)
		{
            VerifyRights();

            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				foreach (var tplID in tplIDs)
				{
					var tpl = bl.GetApplicationTemplate(tplID);
					if (tpl.ViewId > 0)
					{
						var view = ViewManager.GetViewById(tpl.ViewId);
						if (view != null && !view.IsSystem)
						{
							ViewManager.DeleteView(view);
						}
					}
					tpl.ViewId = 0;
					tpl.ViewXmlAsString = null;
					bl.UpdateApplicationTemplateEx(tpl, false);
				}
			}
		}
        
        [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public IEnumerable<TemplateApplication> GetApps(int templateId, int? top)
        {
            return ApplicationTemplateDAL.GetTemplateApplications(templateId, top);
        }
        
        [WebMethod]
        public int GetAppsCount(int templateId)
        {
            return ApplicationTemplateDAL.GetTemplateApplicationsCount(templateId);
        }

    private static bool HasValidNodeBasedLicense()
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            return businessLayer.HasValidNodeBasedLicense();
        }
    }

    private static string GetSearchClause(string tableAlias, string search)
    {
        if (String.IsNullOrEmpty(search))
            return "(1=1)";

        string swqlFormat = @"
            ({0}.Name LIKE '%{1}%' 
             OR {0}.ApplicationTemplateID IN (
                    SELECT T.TemplateId
                    FROM Orion.APM.Tag T
                    WHERE T.TagName LIKE '%{1}%'
                )             
            )";

        string clause = String.Format(swqlFormat, tableAlias, search);
        return clause;
    }

    private static long GetRowCount(string property, string value, string search)
    {
        const string swqlFormat = @"
            SELECT Count(A.ApplicationTemplateID) as theCount
            FROM Orion.APM.ApplicationTemplate A
            WHERE {0}
            AND {1}            
            AND A.IsMockTemplate = 0";

        string swql = String.Format(swqlFormat, GetTagWhereClause("A", value), GetSearchClause("A", search));
        DataTable rowCountTable = SwisProxy.ExecuteQuery(swql);

        if (rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private static string GetTagWhereClause(string tableAlias, string value)
    {
        string whereClause = "1=1";

        if (String.IsNullOrEmpty(value) || value.Equals("null", StringComparison.OrdinalIgnoreCase))
            return whereClause;

        whereClause = string.Format(@"{0}.ApplicationTemplateID IN (
	                                SELECT T.TemplateId
	                                FROM Orion.APM.Tag T
	                                WHERE T.TagName = '{1}')", tableAlias, value);
        return whereClause;

    }


    private void AddTagDataToTemplateTable(DataTable table)
    {
        DataColumn tagsColumn = table.Columns["Tags"];

        if (table.Rows.Count == 0)
            return;

        //
        // Load the tags
        //
        List<string> ids = new List<string>(table.Rows.Count);
        int idColumn = table.Columns["ApplicationTemplateID"].Ordinal;

        foreach (DataRow row in table.Rows)
        {
            ids.Add(row[idColumn].ToString());
        }

        string swql = String.Format(@"SELECT T.TemplateId, T.TagName
                                  FROM Orion.APM.Tag T
                                  WHERE T.TemplateID in ({0})",
                                      String.Join(",", ids.ToArray()));

        DataTable tags = SwisProxy.ExecuteQuery(swql);

        //
        // Parse the tags into a lookup by id
        //
        Dictionary<int, List<string>> tagLookup = new Dictionary<int, List<string>>(table.Rows.Count);

        foreach (DataRow row in tags.Rows)
        {
            int id = Convert.ToInt32(row[0]);
            string tag = row[1].ToString();

            if (!tagLookup.ContainsKey(id))
                tagLookup[id] = new List<string>();

            tagLookup[id].Add(tag);
        }

        //
        // Add the tags as JSON to the Tags column
        //
        JavaScriptSerializer serializer = new JavaScriptSerializer();

        foreach (DataRow row in table.Rows)
        {
            int id = Convert.ToInt32(row[idColumn]);

            if (tagLookup.ContainsKey(id))
            {
                row[tagsColumn] = serializer.Serialize(tagLookup[id]);
            }
        }
    }

	private void PreparePagedTemplates(DataTable data)
	{
		DataTable apps = null;
		if (data != null && data.Rows.Count > 0)
		{
            Func<Object, String> cast = (date => date is DBNull ? String.Empty : ((DateTime)date).ToLocalTime().ToString(CultureInfo.CurrentCulture));
			var viewIdToViewName = new Dictionary<int, string>();
			
			foreach (DataRow row in data.Rows)
			{
                //Created, LastModified Columns
                row["Created"] = cast(row["CreatedDate"]);
                row["LastModified"] = cast(row["LastModifiedDate"]);

				//CustomView Column			
				var viewID = Convert.ToInt32(row["ViewID"]);
				if (viewID != ApmApplication.DefaultViewID) 
				{
					var hasImportedView = Convert.ToBoolean(row["HasImportedView"]);
					if (hasImportedView || viewID > 0) 
					{
						if (apps == null)
						{
							const String query = @"
SELECT a.ApplicationID, a.ApplicationTemplateID
FROM (SELECT ApplicationID, ApplicationTemplateID FROM Orion.APM.Application) AS a
WHERE a.ApplicationTemplateID IN ({0})	
";
							var ids = new StringBuilder();
							foreach (DataRow srow in data.Rows)
							{
								ids.AppendFormat("{0},", srow["ApplicationTemplateID"]);
							}
							apps = SwisProxy.ExecuteQuery(String.Format(query, ids.Remove(ids.Length - 1, 1)));
						}

						var assApps = apps.Select(String.Format("ApplicationTemplateID = {0}", row["ApplicationTemplateID"]));
						
						string viewTitle;
						if (!viewIdToViewName.TryGetValue(viewID, out viewTitle))
						{
                            var viewSpec = ApmViewManager.DefaultViewSpec(viewID, hasImportedView, (string)row["Name"], !System.DBNull.Value.Equals(row["ViewXml"]) ? System.Xml.Linq.XElement.Parse((string)row["ViewXml"]) : null, false);
							viewTitle = viewSpec.Value;
							if (viewID > 0)
							{
								viewIdToViewName[viewID] = viewTitle;
							}
						}


                        string encodedViewTitle = HttpUtility.HtmlEncode(viewTitle);

						if (assApps.Length > 0)
						{
							const string appTpl = "<a href='/Orion/APM/ApplicationDetails.aspx?NetObject=AA:{0}' target='_blank'>{1}</a>";
							row["CustomView"] = String.Format(appTpl, assApps[0]["ApplicationID"], encodedViewTitle);
						}
						else
						{
							const string mockupTpl = "<a href='#' onclick=\"SW.APM.ShowPopupWindow({0}, \'{2}\', '/Orion/APM/MockupApplicationDetails.aspx?AppTemplateID={0}', 800, 600); return false;\">{1}</a>";
							row["CustomView"] = String.Format(mockupTpl, row["ApplicationTemplateID"], encodedViewTitle, Resources.APMWebContent.APMWEBCODE_VB1_13);
						}
					}
				}
				if (row["CustomView"].ToString() == String.Empty)
				{
                    string defaultCaption = Resources.APMWebContent.APMWEBCODE_VB1_15;
                    const string mockupTpl = "<a href='#' onclick=\"SW.APM.ShowPopupWindow({0}, \'{2}\', '/Orion/APM/MockupApplicationDetails.aspx?UseDefaultViewID=true', 800, 600); return false;\">{1}</a>";
                    row["CustomView"] = String.Format(mockupTpl, row["ApplicationTemplateID"], defaultCaption, Resources.APMWebContent.APMWEBCODE_VB1_14);
				}
                
                // FB255575 | override component counts per sales request
                var customAppType = Convert.ToString(row["CustomApplicationType"]);
                if (!string.IsNullOrEmpty(customAppType) && !LicenseHelper.HasValidNodeBasedLicense())
                {
                    var count = WebPluginManager.Instance.GetApplicationLicenseConsumption(customAppType);
                    if (count > 0)
                        row["ComponentsCount"] = count;
                }
            }
			data.Columns.Remove("CreatedDate");
			data.Columns.Remove("LastModifiedDate");
			data.Columns.Remove("ViewID");
			data.AcceptChanges();
		}
	}
}