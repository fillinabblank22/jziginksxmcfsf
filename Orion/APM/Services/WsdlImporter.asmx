﻿<%@ WebService Language="C#" Class="WsdlImporter" %>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.WsdlImporter;
using Wsdl = SolarWinds.APM.Common.Models.Wsdl;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WsdlImporter : System.Web.Services.WebService {

    [WebMethod]
    public Wsdl.ServiceInfo GetSchemaObjectStructure(string schemaUrl) 
	{
		try
		{
			var mexAddress = new Uri(schemaUrl);

			// For MEX endpoints use a MEX address and a mexMode of .MetadataExchange  
			MetadataExchangeClientMode mexMode = MetadataExchangeClientMode.HttpGet;
			var importer = new WsdlServiceImporter(mexMode);
			importer.Init(mexAddress);

			var serviceInfo = GetServiceInfo(importer);
		    return serviceInfo;
		}
		catch (Exception ex) 
		{
			var messages = new System.Text.StringBuilder();
			messages
				.Append("<div>")
				.Append("The current WSDL file is not valid or formatted incorrectly.<hr/>")
				.Append("Original message(s):<br/>");
			for (; ex != null; ex = ex.InnerException)
			{
                messages.AppendInvariantFormat("&nbsp;&nbsp;&nbsp;&nbsp;{0}<br/>", ex.Message.Replace(Environment.NewLine, "<br/>"));
			}
			messages.Append("</div>");
			throw new Exception(messages.ToString());
		} 
    }

    private static Wsdl.ServiceInfo GetServiceInfo(WsdlServiceImporter importer)
    {
        var serviceInfo = importer.GetServiceInfo();
        foreach (var contract in serviceInfo.Contracts)
        {
            foreach (var operation in contract.Operations)
            {
                operation.AddParameters(importer.BuildParameters(contract.Name, operation.Name));
            }
        }
        return serviceInfo;
    }

    [WebMethod]
    public Wsdl.ServiceInfo GetSchemaObjectStructureFromFile(string schemaFileName, string schemaFileContent)
    {
        try
        {
            var importer = new WsdlServiceImporter(MetadataExchangeClientMode.HttpGet);
            importer.InitFromFile(schemaFileName, schemaFileContent);

            var serviceInfo = GetServiceInfo(importer);
            return serviceInfo;
        }
        catch (Exception ex)
        {
            var messages = new System.Text.StringBuilder();
            messages
                .Append("<div>")
                .Append("The current WSDL file is not valid or formatted incorrectly.<hr/>")
                .Append("Original message(s):<br/>");
            for (; ex != null; ex = ex.InnerException)
            {
                messages.AppendInvariantFormat("&nbsp;&nbsp;&nbsp;&nbsp;{0}<br/>", ex.Message);
            }
            messages.Append("</div>");
            throw new Exception(messages.ToString());
        }
    }

    [WebMethod]
	public string GetSoapEnvelope(string contractName, string operationName, string soapVersion, Wsdl.ServiceInfo serviceInfo, Wsdl.ValueInfo[] parameters)
    {
        StringBuilder envelope = new StringBuilder();
		
		bool isSoap12 = string.Equals(soapVersion, "V12");
		string soapTag = isSoap12 ? "soap12" : "soap";
		string soapNamespace = isSoap12 ? "http://www.w3.org/2003/05/soap-envelope" : "http://schemas.xmlsoap.org/soap/envelope/";

        // parameters
        var contract = serviceInfo.Contracts.First(c => c.Name == contractName);
        var operation = contract.Operations.First(op => op.Name == operationName);
                
        envelope.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        envelope.AppendLine(string.Format(CultureInfo.InvariantCulture, "<{0}:Envelope xmlns:{0}=\"{1}\">", soapTag, soapNamespace));
        envelope.AppendLine(string.Format(CultureInfo.InvariantCulture, "\t<{0}:Body>", soapTag));
        envelope.AppendLine(string.Format(CultureInfo.InvariantCulture, "\t\t<{0} xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"{1}\">", operationName, operation.NameSpace));

		foreach (var parameter in parameters) 
		{
			ValidateParameter(operation.Parameters, parameter);
			if (parameter.IsComplex)
			{
				envelope.Append(parameter.Value);
			}
			else
			{
				envelope.AppendLine(string.Format(CultureInfo.InvariantCulture, "\t\t\t<{0}>{1}</{0}>", parameter.Name, parameter.Value));
			}
		}

        envelope.AppendLine(string.Format(CultureInfo.InvariantCulture, "\t\t</{0}>", operationName));
        envelope.AppendLine(string.Format(CultureInfo.InvariantCulture, "\t</{0}:Body >", soapTag));
        envelope.AppendLine(string.Format(CultureInfo.InvariantCulture, "</{0}:Envelope >", soapTag));
        
        return envelope.ToString();
    }

	public void ValidateParameter(IList<Wsdl.ValueInfo> definitions, Wsdl.ValueInfo parameter)
	{
		if (definitions.Contains(item => item.Name == parameter.Name && item.Position == parameter.Position && item.IsComplex == parameter.IsComplex)) 
		{
			return;
		}
		throw new Exception(InvariantString.Format("Incorrect parameter {0};", parameter.Name));
	}
}
