﻿<%@ WebService Language="C#" Class="ApplicationTree" %>

using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Linq;

using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DAL.AppTreeModels;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.APM.Web.Utility;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ApplicationTree : WebService
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	const int MaxNodesPerPage = 100;

	private ResourceInfo Resource { get; set; }
	private bool GroupNodesWithNullPropertiesAsUnknown;
	private bool RememberExpandedGroups;

	private void Init(int resourceId)
	{
		this.Resource = ResourceManager.GetResourceByID(resourceId);
		this.Context.Items[typeof(ViewInfo).Name] = this.Resource.View;
	}

	[WebMethod(EnableSession = true)]
	public string GetTreeSection(int resourceId, string rootId, object[] keys, int startIndex)
	{
		log.DebugFormat("GetTreeSection({1}, {2}, {3}, {4}) from user {0}", Context.User.Identity.Name, resourceId, rootId, JsHelper.Serialize(keys), startIndex);

		Init(resourceId);

		if (!bool.TryParse(Resource.Properties["GroupNodesWithNullPropertiesAsUnknown"], out GroupNodesWithNullPropertiesAsUnknown))
			GroupNodesWithNullPropertiesAsUnknown = true;

		if (!bool.TryParse(Resource.Properties["RememberCollapseState"], out RememberExpandedGroups))
			RememberExpandedGroups = true;

		string filter = GetFilterClause();
		try
		{
			try
			{
				using (var htmlWriter = new AllApplicationsRenderer(Resource.ID))
				{
					BuildTreeLevel(htmlWriter, rootId, keys.Length + 1, keys, startIndex, filter);
					return htmlWriter.GetHtml();
				}
			}
			catch (SwisException ex)
			{
				using (var htmlWriter = new AllApplicationsRenderer(Resource.ID))
				{
					BuildTreeLevel(htmlWriter, rootId, keys.Length + 1, keys, startIndex, string.Empty);
				}
				// No exception when running without the filter. Inform the user.
				return string.Format("Bad filter '{0}' specified. {1}", filter, ex.Message);
			}
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in GetTreeSection({1}, {2}, {3}, {4}) from user {0}. {5}", Context.User.Identity.Name, resourceId, rootId, JsHelper.Serialize(keys), startIndex, ex);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public string GetApplication(int resourceId, string rootId, int applicationId)
	{
		log.DebugFormat("GetApplication({1}, {2}) from user {0}", Context.User.Identity.Name, resourceId, applicationId);

		Init(resourceId);

		try
		{
			var manager = new TreeStateManager(this.Context.Session, this.Resource.ID);
			manager.Expand(this.MakeApplicationKey(applicationId));

			using (var writer = new AllApplicationsRenderer(this.Resource.ID))
			{
				// we will display uncategorized components later
				writer.RenderDiv(() =>
					{
						var categories = ComponentCategoryDAL.GetCategories(applicationId);
						for (var index = 0; index < categories.Count; index++)
						{
							var childId = string.Format("{0}-{1}", rootId, index);
							var isExpanded = manager.IsExpanded(this.MakeCategoryKey(applicationId, categories[index].ComponentCategoryID, null));
							var componentStatuses = ComponentCategoryDAL.GetCategoryStatuses(applicationId, categories[index].ComponentCategoryID.Value);
							var categoryStatus = ApmStatus.Summarize(componentStatuses, true);
							writer.RenderCategory(childId, applicationId, categories[index], isExpanded, categoryStatus);
						}
					});

				var components = ApplicationTreeDAL.GetComponents(applicationId, this.Resource.Properties["CmpOrder"]);
                
                // read application item creators
                IEnumerable<Guid> appItemCreators = null;                
                ICustomApplicationTypePlugin customApplicationTypePlugin = null;
                
				if (components.Any())
				{
					var ctmp = components.First();
					customApplicationTypePlugin = WebPluginManager.Instance.GetPluginForCustomAppType<ICustomApplicationTypePlugin>(ctmp.CustomApplicationType);
                    if (customApplicationTypePlugin != null)
                    {
                        appItemCreators = customApplicationTypePlugin.ApplicationItemCreators;
                    }
				}                

				// render uncategorized component list
				foreach (var component in components)
				{
				    if (appItemCreators == null || (component.TemplateUniqueId != null && !appItemCreators.Any(c => c == (Guid)component.TemplateUniqueId)))
				    {
				        if (string.IsNullOrWhiteSpace(component.ShortName))
                            component.ShortName = component.Name ?? String.Empty;
				        writer.RenderComponent(component, applicationId, null);
				    }
				}

				// display AppitemCreator last
				if (appItemCreators != null)
				{
                    foreach (var appItemCreator in appItemCreators)
                    {
                        var appItemCreatorComponent = components.SingleOrDefault(c => c.TemplateUniqueId == appItemCreator);
                        if (appItemCreatorComponent == null)
                        {
                            continue;
                        }
                        
                        appItemCreatorComponent.ApplicationItemType = customApplicationTypePlugin.GetApplicationItemCreatorType(appItemCreator);
                        var childId = string.Format("{0}-c{1}", rootId, appItemCreatorComponent.ComponentID);
                        var isExpanded = manager.IsExpanded(this.MakeComponentKey(applicationId, appItemCreatorComponent.ComponentID));
                        writer.RenderAppItemCreator(childId, applicationId, appItemCreatorComponent, isExpanded);
                    }
				}

				return writer.GetHtml();
			}
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in GetApplication({1}, {2}) from user {0}. {3}", this.Context.User.Identity.Name, resourceId, applicationId, ex);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public string GetComponentsByCategory(int resourceId, string rootId, int applicationId, int categoryId, int? appItemId = null)
	{
		log.DebugFormat("GetComponentsByCategory({1}, {2}, {3}, {4}) from user {0}", Context.User.Identity.Name, resourceId, applicationId, categoryId, appItemId);

		Init(resourceId);

		try
		{
			var manager = new TreeStateManager(this.Context.Session, this.Resource.ID);
			manager.Expand(this.MakeCategoryKey(applicationId, categoryId, appItemId));

			using (var writer = new AllApplicationsRenderer(this.Resource.ID))
			{
				// render component list based on category
				IList<Component> components;
				if (appItemId == null)
					components = ApplicationTreeDAL.GetComponents(applicationId, this.Resource.Properties["CmpOrder"], categoryId);
				else
					components = ApplicationTreeDAL.GetComponentsForApplicationItem(applicationId, appItemId.Value, this.Resource.Properties["CmpOrder"], categoryId);
				foreach (var component in components)
					writer.RenderComponent(component, applicationId, appItemId);

				return writer.GetHtml();
			}
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in GetComponentsByCategory({1}, {2}, {3}, {5}) from user {0}. {4}", this.Context.User.Identity.Name, resourceId, applicationId, categoryId, ex, appItemId);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public string GetComponentsByApplicationItem(int resourceId, string rootId, int applicationId, int appItemId, string applicationItemType)
	{
		log.DebugFormat("GetComponentsByApplicationItem({1}, {2}, {3}, {4}) from user {0}", Context.User.Identity.Name, resourceId, applicationId, appItemId, applicationItemType);

		Init(resourceId);

		try
		{
			var manager = new TreeStateManager(this.Context.Session, this.Resource.ID);
			manager.Expand(this.MakeApplicationItemKey(appItemId));

			using (var writer = new AllApplicationsRenderer(this.Resource.ID))
			{
				// display categories first
				// we will display uncategorized components later
				writer.RenderDiv(() =>
				{
					var categories = ComponentCategoryDAL.GetCategories(applicationId, appItemId);
					for (var index = 0; index < categories.Count; index++)
					{
						var childId = string.Format("{0}-{1}", rootId, index);
						var isExpanded = manager.IsExpanded(this.MakeCategoryKey(applicationId, categories[index].ComponentCategoryID, appItemId));
						var componentStatuses = ComponentCategoryDAL.GetCategoryStatuses(applicationId, categories[index].ComponentCategoryID.Value, appItemId);
						var categoryStatus = ApmStatus.Summarize(componentStatuses, true);
						writer.RenderCategory(childId, applicationId, appItemId, categories[index], isExpanded, categoryStatus);
					}
				});

				// render uncategorized component list
				var components = ApplicationTreeDAL.GetComponentsForApplicationItem(applicationId, appItemId, this.Resource.Properties["CmpOrder"]);
                foreach (var component in components)
                {
                    component.ApplicationItemType = applicationItemType;
                    writer.RenderComponent(component, applicationId, appItemId);
                }

				return writer.GetHtml();
			}
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in GetComponentsByApplicationItem({1}, {2}, {3}) from user {0}. {4}", this.Context.User.Identity.Name, resourceId, applicationId, appItemId, ex);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public string GetApplicationItems(int resourceId, string rootId, int applicationId, long componentId, string applicationItemType)
	{
		log.DebugFormat("GetApplicationItems({1}, {2}, {3}, {4}) from user {0}", Context.User.Identity.Name, resourceId, applicationId, componentId, applicationItemType);

		Init(resourceId);

		try
		{
			var manager = new TreeStateManager(this.Context.Session, this.Resource.ID);
			manager.Expand(this.MakeComponentKey(applicationId, componentId));

			using (var writer = new AllApplicationsRenderer(this.Resource.ID))
			{
				// display application items
				writer.RenderDiv(() =>
				{
					var appItems = ApplicationTreeDAL.GetApplicationItems(applicationId, applicationItemType);
                    
					for (var index = 0; index < appItems.Count; index++)
					{                                                
						var childId = string.Format("{0}-AppItem-{1}", rootId, index);
						var isExpanded = manager.IsExpanded(this.MakeApplicationItemKey(appItems[index].ItemID));
						writer.RenderApplicationItem(childId, applicationId, appItems[index], isExpanded);
					}
				});

				return writer.GetHtml();
			}
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in GetApplicationItems({1}, {2}, {3}) from user {0}. {4}", this.Context.User.Identity.Name, resourceId, applicationId, componentId, ex);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public void CollapseCategory(int resourceId, int applicationId, int categoryId, int? appItemId)
	{
		try
		{
			var manager = new TreeStateManager(Context.Session, resourceId);
			string categoryKey = this.MakeCategoryKey(applicationId, categoryId, appItemId);
			manager.Collapse(categoryKey);
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in CollapseCategory({1}, {2}, {4}) from user {0}. {3}", Context.User.Identity.Name, resourceId, applicationId, ex, categoryId);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public void CollapseApplicationItem(int resourceId, int applicationId, int applicationItemId)
	{
		try
		{
			var manager = new TreeStateManager(Context.Session, resourceId);
			string appItemKey = this.MakeApplicationItemKey(applicationItemId);
			manager.Collapse(appItemKey);
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in CollapseApplicationItem({1}, {2}, {4}) from user {0}. {3}", Context.User.Identity.Name, resourceId, applicationId, ex, applicationItemId);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public void CollapseApplication(int resourceId, int applicationId)
	{
		try
		{
			var manager = new TreeStateManager(Context.Session, resourceId);
			string appKey = MakeApplicationKey(applicationId);
			manager.Collapse(appKey);
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in CollapseApplication({1}, {2}) from user {0}. {3}", Context.User.Identity.Name, resourceId, applicationId, ex);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public void CollapseApplicationItems(int resourceId, int applicationId, long componentId)
	{
		try
		{
			var manager = new TreeStateManager(Context.Session, resourceId);
			string appKey = MakeComponentKey(applicationId, componentId);
			manager.Collapse(appKey);
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in CollapseApplication({1}, {2}) from user {0}. {3}", Context.User.Identity.Name, resourceId, applicationId, ex);
			throw;
		}
	}

	[WebMethod(EnableSession = true)]
	public void CollapseTreeSection(int resourceId, object[] keys)
	{
		try
		{
			var manager = new TreeStateManager(Context.Session, resourceId);
			string groupKey = JsHelper.Serialize(keys);
			manager.Collapse(groupKey);
		}
		catch (Exception ex)
		{
			log.ErrorFormat("Exception in CollapseTreeSection({1}, {2}) from user {0}. {3}", Context.User.Identity.Name, resourceId, JsHelper.Serialize(keys), ex);
			throw;
		}
	}

	private void BuildTreeLevel(AllApplicationsRenderer writer, string rootId, int treeLevel, object[] keys, int startIndex, string filter)
	{
		string whereClause = GetGroupingWhereClause(keys, filter);
		string configuredGroup = Resource.Properties[string.Format("Grouping{0}", treeLevel)];

		var manager = new TreeStateManager(Context.Session, Resource.ID);
		var treeSectionKey = JsHelper.Serialize(keys);
		if (this.RememberExpandedGroups) manager.Expand(treeSectionKey);

		// render applications if no grouping configured on this level
		if (string.IsNullOrEmpty(configuredGroup) || configuredGroup.Equals("none", StringComparison.OrdinalIgnoreCase))
		{
			var applications = ApplicationTreeDAL.GetApplications(whereClause, Resource.Properties["AppCmpOrder"]);
			writer.Writer.RenderBeginTag(HtmlTextWriterTag.Div);
			for (var index = startIndex; index < applications.Count; index++)
			{
				if (index - startIndex >= MaxNodesPerPage)
				{
					writer.RenderGetMoreLink(rootId, index, "applications", treeSectionKey, MaxNodesPerPage);
					break;
				}
				var childId = string.Format("{0}-{1}", rootId, index - startIndex);
				var appKey = MakeApplicationKey(applications[index].ApplicationId);
                writer.RenderApplication(childId, applications[index], manager.IsExpanded(appKey));
			}
			writer.Writer.RenderEndTag();
			// finish if applications are rendered
			return;
		}

		// render groups
		var groupProperties = ApplicationTreeDAL.GetPropertyValues(configuredGroup, Resource.Properties["AppCmpOrder"], whereClause);
		Action atEnd = null;
		for (int index = 0, renderedGroupIndex = 0; renderedGroupIndex < groupProperties.Count; ++renderedGroupIndex)
		{
			// skip groups before index
			if (renderedGroupIndex < startIndex)
				continue;

			var childId = string.Format("{0}-{1}", rootId, index++);
			if (renderedGroupIndex - startIndex >= MaxNodesPerPage)
			{
				// render Get more ... link in case that MaxNodesPerPage is reached
				writer.RenderGetMoreLink(childId, renderedGroupIndex, "groups", treeSectionKey, MaxNodesPerPage);
				break;
			}

			// if currently rendered group is null or unknown push its rendering at the end
			// according to "Put nodes with null values for the grouping property at the bottom of the list, in no group" settings
			var renderedGroup = groupProperties[renderedGroupIndex];
			if ((String.IsNullOrEmpty(renderedGroup) || renderedGroup.Equals("Unknown", StringComparison.OrdinalIgnoreCase)) && !GroupNodesWithNullPropertiesAsUnknown)
			{
				// for null keys, just render the next level in place
				// defer until the end of the list
				atEnd = delegate { BuildTreeLevel(writer, childId, treeLevel + 1, ArrayAppend(keys, renderedGroup), 0, filter); };
			}
			else
			{
				var groupName = FormatGroupName(renderedGroup, configuredGroup);
				object[] newKeys = ArrayAppend(keys, renderedGroup);
				string newGroupKey = JsHelper.Serialize(newKeys);
				var statusCounts = ApplicationTreeDAL.GetGroupStatus(this.GetGroupingWhereClause(newKeys, filter));
				var isOdd = (treeLevel == 1) && (renderedGroupIndex % 2 == 1);
				writer.RenderGroup(childId, treeLevel, isOdd, statusCounts, newGroupKey, manager.IsExpanded(newGroupKey), groupName);
			}
		}
		if (atEnd != null) atEnd();
	}

	private static string FormatGroupName(string renderedGroup, string selectedGroup)
	{
		string displayKey = renderedGroup ?? string.Empty;
		if ((displayKey.Length == 0) || (selectedGroup == "MachineType" && displayKey == "Unknown"))
		{
			displayKey = Resources.APMWebContent.APMWEBCODE_AK1_112;
		}
		if (displayKey == "None")
		{
			displayKey = Resources.APMWebContent.APMWEBCODE_VB1_67;
		}
		string name = displayKey;
		if (String.Equals(selectedGroup, "Nodes.Status", StringComparison.OrdinalIgnoreCase))
		{
			name = SolarWinds.Orion.Web.DisplayTypes.Status.FromStringInt(displayKey).ToLocalizedString();
		}
		return name;
	}

	private string MakeCategoryKey(int applicationId, object categoryId, int? appItemId)
	{
		return string.Format("-Cat-{0}-{1}-{2}", applicationId, categoryId, appItemId != null ? appItemId.ToString() : "null");
	}

	private string MakeComponentKey(int applicationId, long componentId)
	{
		return string.Format("-Comp-{0}-{1}", applicationId, componentId);
	}

	private string MakeApplicationKey(object applicationId)
	{
		return "-App-" + applicationId;
	}

	private string MakeApplicationItemKey(object appItemId)
	{
		return "-AppItem-" + appItemId;
	}

	private string GetGroupingWhereClause(object[] keys, string filter)
	{
		List<string> clauses = new List<string>();

		if (!string.IsNullOrEmpty(filter))
			clauses.Add(filter);

		for (int level = 0; level < Math.Min(keys.Length, 3); ++level)
		{
			string property = Resource.Properties[string.Format("Grouping{0}", level + 1)];
			if (string.IsNullOrEmpty(property) || property.Equals("none", StringComparison.OrdinalIgnoreCase))
				break;
			//clauses.Add(string.Format("ISNULL({0},'')='{1}'", property, keys[level]));

			Type cpType = CustomPropertyMgr.GetTypeForProp("Nodes", property);
			object value = CustomPropertyMgr.ChangeType(keys[level], cpType);
			if (keys[level] == null)
			{
				clauses.Add(string.Format("{0} IS NULL", property));
			}
			else
			{
				string cleanValue = value.ToString().Replace("'", "''");
				clauses.Add(string.Format("{0}='{1}'", property, cleanValue));
			}
		}
		if (clauses.Count == 0)
		{
			return "1=1";
		}
		return string.Join(" AND ", clauses.ToArray());
	}

	private string GetFilterClause()
	{
		string filter = (Resource.Properties["Filter"] ?? string.Empty)
			.Trim().Replace('*', '%').Replace('?', '_');
		if (filter.Length > 0)
			filter = "(" + filter + ")";
		return filter;
	}

	private static object[] ArrayAppend(object[] array, object toAppend)
	{
		object[] newArray = new object[array.Length + 1];
		array.CopyTo(newArray, 0);
		newArray[array.Length] = toAppend;
		return newArray;
	}

}

