﻿<%@ WebService Language="C#" Class="Components" %>

using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Components : ApmWebService
{

	#region Getters

	[WebMethod(EnableSession = true)]
	public PageableDataTable GetComponentsPaged(string property, string value)
	{
		return GetDataPaged(property, value, ComponentsQuery, ", t.NodeId ");
	}

	[WebMethod(EnableSession = true)]
	public PageableDataTable GetTemplatesPaged(string property, string value)
	{
		return GetDataPaged(property, value, TemplatesQuery, "");
	}

	[WebMethod(EnableSession = true)]
	public DataTable GetComponentValuesAndCountForProperty(string property)
	{
		string swql = string.Format(ComponentGroupsCountsQueryFormat, property);
		return SwisProxy.ExecuteQuery(swql);
	}

	[WebMethod(EnableSession = true)]
	public DataTable GetTemplateValuesAndCountForProperty(string property)
	{
		string swql = string.Format(TemplateGroupsCountsQueryFormat, property);
		return SwisProxy.ExecuteQuery(swql);
	}

	#endregion

	[WebMethod]
	public void DeleteComponents(IList<long> componentIds)
	{
		VerifyRights();

		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			businessLayer.DeleteComponents(componentIds);
		}
	}

	[WebMethod]
	public void DeleteTemplates(IList<long> templateIds)
	{
		VerifyRights();

		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			businessLayer.DeleteComponentTemplates(templateIds);
		}
	}

	[WebMethod]
	public string CopyToApplication(IList<long> componentIDs, IList<long> templateIDs, IList<int> applicationIDs)
	{
		VerifyRights();

		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			if (componentIDs.Count > 0)
			{
				businessLayer.CopyComponentsToApplication(componentIDs, applicationIDs);

				return Resources.APMWebContent.APMWEBCODE_VB1_98;
			}
			if (templateIDs.Count > 0)
			{
				businessLayer.CopyComponentsTemplatesToApplication(templateIDs, applicationIDs);

				return Resources.APMWebContent.APMWEBCODE_VB1_99;
			}
		}
		return Resources.APMWebContent.APMWEBCODE_VB1_100;
	}

	[WebMethod]
	public string CopyToApplicationTemplate(IList<long> componentIDs, IList<long> templateIDs, IList<int> applicationIDs)
	{
		VerifyRights();

		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			if (componentIDs.Count > 0)
			{
				businessLayer.CopyComponentsToApplicationTemplate(componentIDs, applicationIDs);

				return Resources.APMWebContent.APMWEBCODE_VB1_98;
			}
			if (templateIDs.Count > 0)
			{
				businessLayer.CopyComponentsTemplatesToApplicationTemplate(templateIDs, applicationIDs);

				return Resources.APMWebContent.APMWEBCODE_VB1_99;
			}
		}
		return Resources.APMWebContent.APMWEBCODE_VB1_100;
	}

	[WebMethod]
	public int CreateApplicationTemplateFromComponents(IList<long> componentIds)
	{
		VerifyRights();

		int templateID;
		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			templateID = businessLayer.CreateApplicationTemplateBasedOnComponentIDs(componentIds);
		}
		return templateID;
	}

	[WebMethod]
	public int CreateApplicationTemplateFromTemplates(IList<long> templateIds)
	{
		VerifyRights();

		int templateID;
		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			templateID = businessLayer.CreateApplicationTemplateBasedOnTemplateIDs(templateIds);
		}
		return templateID;
	}

	private double? ConvertToDouble(string input, System.Globalization.CultureInfo culture)
	{
		try
		{
			// Neutral culture ("de", "ru", ...) cannot be used for formatting and parsing
			if (culture.IsNeutralCulture)
				culture = System.Globalization.CultureInfo.CreateSpecificCulture(culture.Name);

			return Convert.ToDouble(input, culture);
		}
		catch (Exception)
		{
			return null;
		}
	}

	[WebMethod]
	public string DataTransformEvaluateTest(string expression, string input)
	{
		double? statistic = null;

		// Try to convert the input with all prefered languages
		if (HttpContext.Current != null && HttpContext.Current.Request != null &&
		    HttpContext.Current.Request.UserLanguages != null)
		{
			int index = 0;
			while (index < HttpContext.Current.Request.UserLanguages.Length && !statistic.HasValue)
			{
				try
				{
					// UserLanguage value can contain also quality parameter. For example: "en-us;q=0.5"
					string language = HttpContext.Current.Request.UserLanguages[index].Split(';')[0];
					statistic = ConvertToDouble(input, new System.Globalization.CultureInfo(language));
				}
				catch (Exception)
				{
				}
				index++;
			}
		}

		// Try to convert input with invariant culture
		if (!statistic.HasValue)
		{
			statistic = ConvertToDouble(input, System.Globalization.CultureInfo.InvariantCulture);
		}

		if (!statistic.HasValue)
		{
			return "Invalid input";
		}

		List<string> result;
		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			result = businessLayer.TestTransform(expression, statistic.Value);
		}
		return String.Join("\n", result.ToArray());
	}

	[WebMethod]
	public string DataTransformParseTest(string expression)
	{
		List<string> result;
		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			result = businessLayer.TestTransformExpression(expression);
		}
		return String.Join("\n", result.ToArray());
	}

	[WebMethod]
	public IList<SolarWinds.APM.Transformation.Contract.UIFunctionInfo> GetTransformFuctions()
	{
		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			return businessLayer.GetTransformFunctions().OrderBy(f => f.Category).ToList();
		}
	}

    private IEnumerable<SolarWinds.APM.Common.Models.ComponentDefinition> GetDispalyableComponentDefinitions()
    {
        if (SolarWinds.APM.Web.ComponentConstants.IsDevMode)
            return APMCache.ComponentDefinitions.Values;
        else
            return APMCache.ComponentDefinitions.Values.Where(cd => ReflectionHelper.GetCustomEnumValueAttribute<IsInternalAttribute>(cd.Type) == null);
    }

	[WebMethod]
	public DataTable GetDefinitionValuesAndCountForProperty(String property)
	{
		var cats = new Dictionary<String, Int32>();
        foreach (var def in GetDispalyableComponentDefinitions())
		{
			foreach (var cat in def.ComponentCategoryMembership)
			{
				if (!cats.ContainsKey(cat.Name))
				{
					cats[cat.Name] = 0;
				}
				cats[cat.Name] += 1;
			}
		}

		var data = new DataTable();
		data.Columns.Add(new DataColumn("theValue", typeof(String)));
		data.Columns.Add(new DataColumn("theCount", typeof(Int32)));
		foreach (var pair in cats)
		{
			var row = data.NewRow();
			row.ItemArray = new Object[] { pair.Key, pair.Value };
			data.Rows.Add(row);
		}
		data.AcceptChanges();

		return data;
	}

	[WebMethod]
	public PageableDataTable GetDefinitions(String property, String value, String filterText, String filterRule, Int32 start, Int32 limit, String direction)
	{
		const StringComparison StrCmp = StringComparison.InvariantCultureIgnoreCase;

		var defs = GetDispalyableComponentDefinitions();
		if (filterText.IsValid())
		{
			if (filterRule == "Starts With")
			{
				defs = defs.Where(item => item.Name.StartsWith(filterText, StrCmp));
			}
			else if (filterRule == "Ends With")
			{
				defs = defs.Where(item => item.Name.EndsWith(filterText, StrCmp));
			}
			else
			{
				defs = defs.Where(item => item.Name.IndexOf(filterText, StrCmp) > -1);
			}
		}
		if (filterText.IsNotValid() && value.IsValid()) 
		{
			defs = defs.Where(item => item.ComponentCategoryMembership.FirstOrDefault(item2 => item2.Name.Equals(value, StrCmp)) != null);
		}
		
		direction = (direction ?? "ASC").ToUpper();
		if (direction.Equals("ASC"))
		{
			defs = defs.OrderBy(item => item.Name);
		}
		else 
		{
			defs = defs.OrderByDescending(item => item.Name);
		}

		var data = new DataTable();
		data.Columns.Add(new DataColumn("ID", typeof(Int32)));
		data.Columns.Add(new DataColumn("Name", typeof(String)));
		data.Columns.Add(new DataColumn("Count", typeof(Int32)));
		for (Int32 begin = Math.Max(0, start), end = Math.Min(begin + Math.Max(10, limit), defs.Count()); begin < end; begin++)
		{
			var def = defs
				.ElementAt(begin);
			
			var row = data.NewRow();
			row.ItemArray = new Object[] { def.Id, def.Name, 1 };
			data.Rows.Add(row);
		}
		return new PageableDataTable(data, defs.Count());
	}
    
    #region string Queries Constants

    private const string ComponentGroupsCountsQueryFormat = @"
SELECT
	{0} as theValue, Count({0}) as theCount
FROM
	Orion.APM.Component c INNER JOIN
	Orion.APM.ComponentDefinition cd ON c.ComponentType = cd.ComponentType LEFT JOIN
	( 
		SELECT a.ApplicationID, a.Name AS ParentName, at.CustomApplicationType
		FROM Orion.APM.Application a LEFT JOIN
		Orion.APM.ApplicationTemplate at ON a.ApplicationTemplateID = at.ApplicationTemplateID
	) AS t ON c.ApplicationID = t.ApplicationID
	WHERE CustomApplicationType IS NULL
GROUP BY
	{0}
ORDER BY
    {0}
";

    private const string TemplateGroupsCountsQueryFormat = @"
SELECT
	{0} as theValue, Count({0}) as theCount
FROM
	Orion.APM.ComponentTemplate c INNER JOIN
	Orion.APM.ComponentDefinition cd ON c.ComponentType = cd.ComponentType LEFT JOIN
	( 
		SELECT a.ApplicationTemplateID, a.Name AS ParentName, a.CustomApplicationType
		FROM Orion.APM.ApplicationTemplate a
	) AS t ON c.ApplicationTemplateID = t.ApplicationTemplateID 
WHERE t.CustomApplicationType IS NULL
GROUP BY
	{0}
ORDER BY
    {0}";

    private const string ComponentsQuery = @"
SELECT
    'Application' as ParentType, C.ComponentID as ID, 'C' as CmpType, C.ComponentID as UniqueId,
    C.Name, C.ComponentType, C.ApplicationID as ParentId, A.Name as ParentName, ISNULL(s.Availability, 0) as Status,
    N.Caption as NodeName, N.Status as NodeStatus, N.NodeId AS NodeId
FROM
    Orion.APM.Component C
LEFT JOIN Orion.APM.CurrentComponentStatus s ON C.ComponentID = s.ComponentID
INNER JOIN Orion.APM.Application A ON C.ApplicationID = A.ApplicationID
LEFT JOIN Orion.APM.ApplicationTemplate AT on A.ApplicationTemplateID = AT.ApplicationTemplateID
INNER JOIN Orion.Nodes N ON A.NodeId = N.NodeId
WHERE AT.CustomApplicationType IS NULL
";

    private const string TemplatesQuery = @"
SELECT
    'Template' as ParentType, C.ID as ID,  'T' as CmpType,  C.ID as UniqueId,
    C.Name, C.ComponentType, C.ApplicationTemplateID as ParentId, A.Name as ParentName, -1 as Status,
    'Template' as NodeName, '' as NodeStatus
FROM
    Orion.APM.ComponentTemplate C
INNER JOIN  Orion.APM.ApplicationTemplate A ON C.ApplicationTemplateID = A.ApplicationTemplateID
WHERE A.CustomApplicationType IS NULL
";
    private const string PageableQueryFormat = @"
SELECT TOP {0} t.UniqueId, 
               t.Name, 
               t.ParentName, 
               t.ParentId, 
               t.ParentType, 
               cd.Name as CmpTypeName, 
               t.Status,
               t.NodeName,
               t.NodeStatus,
               t.CmpType {7}             
FROM
(
       {6}
) as t
INNER JOIN Orion.APM.ComponentDefinition cd ON t.ComponentType = cd.ComponentType
WHERE t.UniqueId NOT IN (

	SELECT TOP {1} sub_t.UniqueId
	FROM
    (
       {6}
    ) as sub_t	
	INNER JOIN Orion.APM.ComponentDefinition sub_cd ON sub_t.ComponentType = sub_cd.ComponentType
	WHERE {4}
	ORDER BY sub_{2} {3}
)	
AND {5}
ORDER BY {2} {3}";

    #endregion

    private PageableDataTable GetDataPaged(string property, string value, string subqueryConst, string extraColumns)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        int.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        int.TryParse(Context.Request.QueryString["limit"], out pageSize);

        value = value.Replace("'", "''");

        string swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, property, value, subqueryConst, extraColumns);

        DataTable pageData = SwisProxy.ExecuteQuery(swql);

        return new PageableDataTable(pageData, GetRowCount(property, value, subqueryConst));
    }

    private static string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderbyDirection,
        string property, string value, string subqueryConst, string extraColumns)
    {
        pageSize = Math.Max(pageSize, 25);
        int pageNumber = (startRowNumber / pageSize);

        string qualifiedOrderByColumn = "t." + orderByColumn;

				if (orderByColumn.Equals("CmpTypeName", StringComparison.OrdinalIgnoreCase)) qualifiedOrderByColumn = "cd.Name";

        string swql = string.Format(PageableQueryFormat,
                                    pageSize,
                                    pageNumber * pageSize,
                                    qualifiedOrderByColumn,
                                    orderbyDirection,
                                    GetWhereClause("sub_", property, value),
                                    GetWhereClause("", property, value),
                                    subqueryConst, extraColumns);


        return swql;
    }

    private static string GetWhereClause(string prefix, string property, string value)
    {
        string whereClause = "1=1";

        string valuePart = string.Format(" = '{0}'", value);
        if (value.Equals("null", StringComparison.OrdinalIgnoreCase))
            valuePart = " IS NULL";

        if (!string.IsNullOrEmpty(property))
            whereClause = string.Format("{0}{1} {2}", prefix, property, valuePart);
        return whereClause;
    }

    private static long GetRowCount(string property, string value, string subqueryConst)
    {
        const string swqlFormat = @"
            SELECT count(t.ID) as RowCount
            FROM 
            (
                {1}            
            ) as t
            INNER JOIN Orion.APM.ComponentDefinition cd ON t.ComponentType = cd.ComponentType
            WHERE {0}";

        string swql = string.Format(swqlFormat, GetWhereClause("", property, value), subqueryConst);

        DataTable rowCountTable = SwisProxy.ExecuteQuery(swql);

        if (rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }
}

