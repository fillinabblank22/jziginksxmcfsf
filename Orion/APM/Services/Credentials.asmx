﻿<%@ WebService Language="C#" Class="Credentials" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models;

/// <summary>
/// Summary description for Credentials
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Credentials : ApmWebService
{
    [WebMethod(EnableSession = true)]
    public CredentialSet GetCredentialsById(int credentialSetId)
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            CredentialSet credentialSet = businessLayer.GetCredentialsById(credentialSetId);
            
            // We don't want the passoword going over the wire, so we'll blank it out.
			credentialSet.Password = CredentialSet.FakePassword;

            return credentialSet;
        }
    }

		[WebMethod(EnableSession = true)]
		public IList<CredentialSet> GetCredentials(List<Int32> credentialIDs)
		{
            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				var credentials = new List<CredentialSet>();
				foreach (CredentialSet cred in businessLayer.GetCredentials()) 
				{
					cred.Password = CredentialSet.FakePassword;
					if (credentialIDs.Count < 1 || credentialIDs.Contains(cred.Id))
					{
						credentials.Add(cred);
					}
				}
                foreach (CredentialSet cred in businessLayer.GetCertificates()) 
				{
					cred.Password = CredentialSet.FakePassword;
					if (credentialIDs.Count < 1 || credentialIDs.Contains(cred.Id))
					{
					    cred.Name = string.Format("{0} ({1})", cred.Name, Resources.APMWebContent.APMWEBDATA_RB3_1);
						credentials.Add(cred);
					}
				}
                credentials.Sort((x, y) => System.String.CompareOrdinal(x.Name, y.Name));
			    return credentials;
			}
		}

		[WebMethod(EnableSession = true)]
		public Int32 CreateCredential(String name, String userName, String psw)
		{
            VerifyRights();
            VerifyDemo();

            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				CredentialSet cred = new CredentialSet();
				cred.Name = name;
				cred.UserName = userName;
				cred.Password = psw;

				return businessLayer.CreateNewCredentialSet(cred);
			}
		}
	
		[WebMethod(EnableSession = true)]
        public IList<CredentialSetInfo> GetCredentialNames()
		{
            VerifyRights();
            VerifyDemo();

            using (IAPMBusinessLayer businesLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                return businesLayer.GetCredentialNames().Select(pair => new CredentialSetInfo {ID = pair.Key, Name = pair.Value}).ToList();
            }
        }

        public class CredentialSetInfo
        {
            public string Name { get; set; }

            public int ID { get; set; }
        }
}

