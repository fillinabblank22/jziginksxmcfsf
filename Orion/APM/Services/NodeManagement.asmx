﻿<%@ WebService Language="C#" Class="Orion.APM.Services.NodeManagement" %>

using System;
using System.Web.Script.Services;
using System.Web.Services;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;

namespace Orion.APM.Services
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class NodeManagement : WebService
    {
        private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

        [WebMethod(EnableSession = true)]
        public RebootNodeResult Reboot(int nodeId)
        {
            this.log.Info(this.CreateAuditMessage(ApmRoleAccessor.AllowNodeReboot));

            if (!ApmRoleAccessor.AllowNodeReboot) 
                throw new ApplicationLocalizedException(() => Resources.APMWebContent.ApmWeb_JEL_NodeManagementAccessDenied);

            using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                return businessLayer.RebootNode(nodeId);
            }
        }

        private string CreateAuditMessage(bool accessGranted)
        {
            var userName = "Unknown";
            if (this.Context.User != null
                && this.Context.User.Identity != null
                && this.Context.User.Identity.IsAuthenticated
                && !string.IsNullOrWhiteSpace(this.Context.User.Identity.Name))
                userName = this.Context.User.Identity.Name;
            var access = accessGranted ? "granted" : "denied";
            return string.Format("User {0} from IP {1} access to APM Node Management service, access {2}.", userName, this.Context.Request.UserHostAddress, access);
        }

    }
}