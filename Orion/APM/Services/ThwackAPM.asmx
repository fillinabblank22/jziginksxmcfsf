
<%@ WebService Language="C#" Class="ThwackAPM" %>

// NOTE:  This is a copy of Core\Src\Web\Orion\Services\Thwack.asmx.  That version doesn't support
//        other Forums or Content Zones so I had to make my own copy here.  Once that version has
//        that support, this code should be removed and we should use the new code.

using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Caching;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ThwackAPM : ApmWebService
{
    private const string CachePrefix = "ThwackFeed_";
    private readonly TimeSpan CacheDuration = TimeSpan.FromMinutes(30);
    protected static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    [WebMethod]
    public string GetRecentPosts(int forumId, int postsCount)
    {
        string url = String.Format(ThwackHelper.ThwackCachingService + "/forums/rss.aspx?ForumID={0}&Mode=0", forumId);
        PostItemList posts = GetPostItems(url);
        if (posts != null && posts.ItemList != null)
        {
            return FormatRecentPostsTable(posts.ItemList, postsCount);
        }
        return GetErrorMessage();
    }

    [WebMethod]
    public string GetRecentlyPostedContent(string contentFolder, int postsCount, DateTime oldestAllowed, string sortBy)
    {
        string url = String.Format(ThwackHelper.ThwackCachingService + "/productfeeds/contentlibrary.aspx?showfolder={0}", Uri.EscapeUriString(contentFolder));

        PostItemList posts = GetPostItems(url);
        if (posts != null && posts.ItemList != null)
        {            
            // sort items
            if (String.Equals(sortBy, "Title", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByTitle);
            }
            else if (String.Equals(sortBy, "Views", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByViews);
            }
            else
            {
                posts.ItemList.Sort(SortByDateTime);
            }
            // filter older items than allowed
            // format the result table            
            return FormatRecentlyPostedContentTable(posts.ItemList.FindAll(i => i.PubDate >= oldestAllowed), postsCount);
        }
        return GetErrorMessage();
    }

    [WebMethod]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        DataTable table = new DataTable();
        table.Columns.Add("theValue");
        table.Columns.Add("theCount");

        List<PostItem> list = GetAPMTemplatePostItems();

        // return "All" as the blank/unknown value
        table.Rows.Add("", list.Count);

        Dictionary<string, int> postsForTag = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        foreach (PostItem item in list)
        {
            foreach (string tag in item.Tags)
            {
                if (!postsForTag.ContainsKey(tag))
                    postsForTag[tag] = 1;
                else
                    postsForTag[tag] += 1;
            }
        }

        List<string> allTags = new List<string>(postsForTag.Keys);
        allTags.Sort(StringComparer.OrdinalIgnoreCase);

        foreach (string tag in allTags)
        {
            table.Rows.Add(tag, postsForTag[tag]);
        }

        return table;
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetTemplateDescription(int templateId)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();

        List<PostItem> list = GetAPMTemplatePostItems();
        PostItem postItem = list.Find(item => item.Id == templateId);

        if (postItem != null)
            result["description"] = postItem.Description.Trim();

        return result;
    }

    [WebMethod]
    public PageableDataTable GetTemplatesPaged(string property, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (pageSize <= 0)
            pageSize = 20;

        List<PostItem> list = GetAPMTemplatePostItems();
        bool isInitialListEmpty = (list.Count == 0);
        
        list = FilterItemList(list, value, search);
        bool isFilteredListEmprt = (list.Count == 0);

        Comparison<PostItem> compareMethod = SortByTitle;

        if (String.Equals(sortColumn, "PubDate", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByDateTime;
        else if (String.Equals(sortColumn, "DownloadCount", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByDownloads;
        else if (String.Equals(sortColumn, "ViewCount", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByViews;
        else if (String.Equals(sortColumn, "Rating", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByRating;
        else if (String.Equals(sortColumn, "Owner", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByOwner;

        list.Sort(compareMethod);

        if (String.Equals(sortDirection, "desc", StringComparison.OrdinalIgnoreCase))
        {
            list.Reverse();
        }

        string emptyText;
        if (isInitialListEmpty)
            emptyText = Resources.APMWebContent.APMWEBCODE_VB1_86;
        else if (isFilteredListEmprt)
            emptyText = Resources.APMWebContent.APMWEBCODE_VB1_87;
        else
            emptyText = Resources.APMWebContent.APMWEBCODE_VB1_88;

        DataTable table = PostItemToDataTable(list, startRowNumber, startRowNumber + pageSize);

        return new PageableDataTable(table, list.Count, emptyText);
    }

	[WebMethod(EnableSession = true)]
	public string UploadTemplates(IList<int> Ids)
	{
        VerifyRights();
        
        //
		// Check Thwack Credentials
		//
        var cred = Session["ThwackCredential"] as NetworkCredential;
		if (cred == null)
		{
            return BuildImportExportResponse(false, Resources.APMWebContent.APMWEBCODE_VB1_89);
		}

        string uploadUrl = ThwackHelper.ThwackApiUrl + "/api/uploadfromproduct.ashx";
		IList<ApplicationTemplate> forUpload = LoadTemplatesForUpload(Ids);

		foreach (ApplicationTemplate template in forUpload)
		{
            if (template.ViewId > 0 && template.ViewXml == null)
            {
                var vi = SolarWinds.Orion.Web.ViewManager.GetViewById(template.ViewId);
                template.ViewXml = ViewImportHelper.ToXml(vi, SolarWinds.Orion.Web.ViewManager.GetResourcesForView(vi));
            }
            
            var lst = new List<ApplicationTemplate> { template };
		    byte[] uploadFileContents;
            
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var str = bl.ApplicationTemplateExportToStream(lst);
                using (var reader = XmlReader.Create(new StringReader(str)))
                {
                    using (var s = new MemoryStream())
                    {
                        var xmlDocument = new XmlDocument();
                        xmlDocument.Load(reader);
                        xmlDocument.Save(s);
                        
                        uploadFileContents = s.ToArray();
                    }
                }
            }
            
			try
			{
                string templateExtension = ".apm-template";
                string uploadedFile = string.Concat(template.Name, templateExtension);
				var inputs = new Dictionary<string, object>();
				inputs.Add("Username", cred.UserName);
				inputs.Add("Password", cred.Password);
				inputs.Add("LibraryID", "71");
                inputs.Add("swProductVersion", RegistrySettings.GetVersionDisplayString());
				inputs.Add("FileSubject", template.Name);
				inputs.Add("FileDescription", string.IsNullOrEmpty(template.Description) ? template.Name : template.Description);
				inputs.Add("FileTags", string.Join(",", template.Tags.Select(c=> c.Name).ToArray<string>()));
                inputs.Add(uploadedFile, uploadFileContents);

				var data = PostsManager<PostItem>.UploadFileData(uploadUrl, inputs);
				string result = System.Text.Encoding.UTF8.GetString(data);
				if (result != "File Uploaded!")
					throw new Exception("Unable to upload file to " + uploadUrl);
				
				ClearThwackTplCache();
			}
			catch (WebException ex)
			{
				var response = ex.Response as HttpWebResponse;
				if (response != null)
				{
					if (response.StatusCode == HttpStatusCode.InternalServerError)
						return BuildImportExportResponse(false, Resources.APMWebContent.APMWEBCODE_VB1_16);
				}
				log.WarnFormat("Unable to upload template {0}.  {1}", template.Id, ex.Message);
				return BuildImportExportResponse(false, ex.Message);
			}
			catch (Exception ex)
			{
				log.WarnFormat("Unable to upload template {0}. {1}", template.Id, ex.Message);
				return BuildImportExportResponse(false, ex.Message);
			}
		}
        return BuildImportExportResponse(true, Resources.APMWebContent.APMWEBCODE_VB1_90);
	}

	private void ClearThwackTplCache() 
	{
        string key = string.Format("{0}{1}{2}", CachePrefix, ThwackHelper.ThwackCachingService, "/productfeeds/contentlibrary.aspx");
		Context.Cache.Remove(key);
	}

    private static IList<ApplicationTemplate> LoadTemplatesForUpload(IList<int> templateIds)
	{
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            return businessLayer.GetApplicationTemplates(templateIds);
        }
	}

	[WebMethod(EnableSession = true)]
    public string ImportTemplates(IList<int> Ids)
    {
        VerifyRights();

        //
        // Check Thwack Credentials
        //
        var cred = Session["ThwackCredential"] as NetworkCredential;
        if (cred == null)
        {
            return BuildImportExportResponse(false, Resources.APMWebContent.APMWEBCODE_VB1_89);
        }
        //
        // Load the templates from thwack
        //
        List<PostItem> allThwackTemplates = GetAPMTemplatePostItems();
        List<ApplicationTemplate> allImportedTemplates = new List<ApplicationTemplate>(Ids.Count);

        foreach (int id in Ids)
        {
            ApplicationTemplate template;
            try
            {
                template = ThwackHelper.LoadTemplateFromThwack(ThwackHelper.ThwackDownloadUrl, id, cred);
            }
            catch (WebException ex)
            {
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError)
                        return BuildImportExportResponse(false, Resources.APMWebContent.APMWEBCODE_VB1_16);
                }
                log.WarnFormat("Unable to import template {0}.  {1}", id, ex.Message);
                return BuildImportExportResponse(false, ex.Message);
            }
            catch (ApmBusinessLayerException ex)
            {
                log.WarnFormat("Unable to import template {0}.  {1}", id, ex.Message);

                if (ex.ErrorType == ApmBusinessLayerErrorType.SerializationError)
                {
                    return BuildImportExportResponse(false, Resources.APMWebContent.ThwackImport_InvalidFormat);
                }
                return BuildImportExportResponse(false, ex.Message);
            }
            catch (Exception ex)
            {
                log.WarnFormat("Unable to import template {0}.  {1}", id, ex.Message);
                return BuildImportExportResponse(false, ex.Message);
            }

            //
            // Added Thwack Tags to template
            //
            PostItem thwackTemplate = allThwackTemplates.Find(thwackTpl => thwackTpl.Id == id);
            if (thwackTemplate != null)
            {
                template.Tags = template.Tags.Union(thwackTemplate.Tags.Select(name => new TagInfo(template.Id, name))).ToList();
            }

            //
            // Add the "Imported from Thwack" tag
            //
            string ThwackTag = Resources.APMWebContent.APMWEBCODE_VB1_91;

            bool hasThwackTag = template.Tags.Exists(ti => (ti.Name == ThwackTag));
            if (!hasThwackTag)
            {
                template.Tags.Add(new TagInfo(template.Id, ThwackTag));
            }

            allImportedTemplates.Add(template);
        }

        //
        // Add the templates to the system
        //
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            businessLayer.ImportApplicationTemplates(allImportedTemplates);
        }

        return BuildImportExportResponse(true, Resources.APMWebContent.ThwackIntegration_ApplicationTemplateWasSuccessfullyImported);
	}
   		
	[WebMethod(EnableSession = true)]
	public void LogIn(String userName, String password)
	{
        Session["ThwackCredential"] = new NetworkCredential(userName, password);
	}
    
    private static DataTable PostItemToDataTable(List<PostItem> list, int startIndex, int endIndex)
    {
        DataTable table = new DataTable();
        table.Columns.Add("Id");
        table.Columns.Add("Title");
        table.Columns.Add("Link");
        table.Columns.Add("PubDate");
        table.Columns.Add("DownloadCount");
        table.Columns.Add("ViewCount");
        table.Columns.Add("Rating", typeof(double));
        table.Columns.Add("Owner");
        table.Columns.Add("Tags");
        table.Columns.Add("HasView");

        JavaScriptSerializer serializer = new JavaScriptSerializer();

        endIndex = Math.Min(endIndex, list.Count);

        for (int i = startIndex; i < endIndex; i++)
        {
            PostItem item = list[i];

            string tagList = serializer.Serialize(item.Tags);

            var hasViewPopupUrl = Resources.APMWebContent.APMWEBCODE_VB1_92;
            if (item.HasView.Equals("Yes", StringComparison.OrdinalIgnoreCase))
            {
                const string mockupTpl = "<a href='#' onclick=\"if (IsDemoMode()) return DemoModeMessage(); SW.APM.logInToThwack(function(items){{SW.APM.ShowPopupWindow({0}, '{2}', '/Orion/APM/MockupApplicationDetails.aspx?AppTemplateID={0}&FromThwackPreview={1}', 800, 600); return false;}}, null, true);\">{3}</a>";
                hasViewPopupUrl = String.Format(mockupTpl, item.Id, true, Resources.APMWebContent.APMWEBCODE_VB1_93, Resources.APMWebContent.APMWEBCODE_VB1_94);
            }
            
            // FB#4363 added InvariantCulture to havbe date in english format
            table.Rows.Add(item.Id, item.Title, item.Link, item.PubDate.ToLocalTime().ToShortDateString(),
                           item.DownloadCount, item.ViewCount, item.Rating,
                           item.Owner, tagList, hasViewPopupUrl);
        }

        return table;
    }

    private static List<PostItem> FilterItemList(List<PostItem> list, string tag, string searchString)
    {
        List<PostItem> filteredList = new List<PostItem>(list.Count);

        foreach (PostItem item in list)
        {
            if (ContainsTag(item, tag))
            {
                if (PassesSearchString(item, searchString))
                {
                    filteredList.Add(item);
                }
            }
        }

        return filteredList;
    }

    private static bool ContainsTag(PostItem item, string tag)
    {
        if (String.IsNullOrEmpty(tag))
            return true;

        return item.Tags.Exists(delegate(string currentTag)
                             {
                                 return String.Equals(currentTag, tag, StringComparison.OrdinalIgnoreCase);
                             });
    }

    private static bool PassesSearchString(PostItem item, string searchString)
    {
        if (String.IsNullOrEmpty(searchString))
            return true;

        List<string> searchValues = new List<string>();
        searchValues.AddRange(new string[] { item.Title, item.Owner, item.Description });
        searchValues.AddRange(item.Tags);

        foreach (string value in searchValues)
        {
            if (value.IndexOf(searchString, StringComparison.CurrentCultureIgnoreCase) != -1)
                return true;
        }

        // Look for the download id prefixed with a #.
        string itemDownloadIdString = "#" + item.Id;
        
        if (searchString == itemDownloadIdString)
            return true;

        return false;
    }

    #region Query methods
    private List<PostItem> GetAPMTemplatePostItems()
    {
        PostItemList list = GetPostItems(ThwackHelper.ThwackCachingService + "/productfeeds/contentlibrary.aspx");
        return (list != null) ? list.ItemList : new List<PostItem>();
    }

    private PostItemList GetPostItems(string url)
    {
        //todo: move caching to a shared PostsManager<PostItemList>
        //todo: add caching header to HTTP request.
        string cacheKey = CachePrefix + url;
        PostItemList list = Context.Cache[cacheKey] as PostItemList;

        if (list != null)
            return list;


        ConfigurePostsManager();
        try
        {
            list = PostsManager<PostItemList>.GetPostItemsList(url);
			
			if(list != null && list.ItemList != null && list.ItemList.Count>0)
			{
				Context.Cache.Insert(cacheKey, list, null, DateTime.UtcNow.Add(CacheDuration), Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, null);
			}
            
            return list;
        }
        catch (Exception ex)
        {
            log.Error("Failed to get thwack post items from " + url, ex);
            return null;
        }
    }

    private static void ConfigurePostsManager()
    {
        // todo: remove Fiddler testing code
        if (false)
        {
            PostsManager<PostItemList>.ProxyAvailable = true;
            PostsManager<PostItemList>.SetProxyInfo("127.0.0.1", 8888, "", "");
            return;
        }

        try
        {
            if (ConfigurationManager.AppSettings["proxyAvailable"].ToLowerInvariant().Equals("true", StringComparison.InvariantCulture))
            {
                PostsManager<PostItemList>.ProxyAvailable = true;
                PostsManager<PostItemList>.SetProxyInfo(ConfigurationManager.AppSettings["proxyAddress"], Convert.ToInt32(ConfigurationManager.AppSettings["proxyPort"]),
                ConfigurationManager.AppSettings["userName"], ConfigurationManager.AppSettings["password"]);
                return;
            }
        }
        catch { }
        // no need to handle this one, just set proxy to false
        PostsManager<PostItemList>.ProxyAvailable = false;
    }
    #endregion

    #region Formatting methods
		
		private static string BuildImportExportResponse(bool success, string message)
		{
		    var response = new {isSuccess = success, msg = message};
		    return JsHelper.Serialize(response);
		}
	
    private static string GetErrorMessage()
    {
        return string.Format("<div class=\"ThwackConnectError\">{0}</div>", Resources.APMWebContent.APMWEBCODE_VB1_95);
    }

    private static string FormatRecentPostsTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);

        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            if (i % 2 != 0)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, "#e4f1f8");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#5C6972");
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#5C6972");
            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(posts[i].Title);

            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }

    private static string FormatRecentlyPostedContentTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);

        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.RenderBeginTag(HtmlTextWriterTag.Ul);
        writer.Write(Resources.APMWebContent.APMWEBCODE_AK1_135);
        writer.RenderEndTag();
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write(Resources.APMWebContent.APMWEBCODE_AK1_136);
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, "center");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write(Resources.APMWebContent.APMWEBCODE_AK1_138);
        writer.RenderEndTag();

        writer.RenderEndTag();


        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            
            writer.AddAttribute("style", "border-width:1px; border-bottom-style:dashed; padding-top:5px; padding-bottom:5px; padding-left:5px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.AddAttribute("class", "sw-link");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(!String.IsNullOrEmpty(posts[i].Title) ? posts[i].Title : "-");
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.AddAttribute("style", "border-width:1px; border-bottom-style:dashed; padding-top:5px; padding-bottom:5px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.WriteEncodedText(posts[i].PubDate.ToShortDateString());
            writer.RenderEndTag();

            writer.AddAttribute("style", "text-align:right; border-width:1px; border-bottom-style:dashed; padding-right:10px; padding-top:5px; padding-bottom:5px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(posts[i].ViewCount);
            writer.RenderEndTag();

            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }
    #endregion

    #region Posts sorting methods
    private static int SortByDateTime(PostItem x, PostItem y)
    {
        return y.PubDate.CompareTo(x.PubDate);
    }

    private static int SortByTitle(PostItem x, PostItem y)
    {
        return x.Title.CompareTo(y.Title);
    }

    private static int SortByViews(PostItem x, PostItem y)
    {
        return y.ViewCount.CompareTo(x.ViewCount);
    }

    private static int SortByDownloads(PostItem x, PostItem y)
    {
        return y.DownloadCount.CompareTo(x.DownloadCount);
    }
    private static int SortByRating(PostItem x, PostItem y)
    {
        return y.Rating.CompareTo(x.Rating);
    }
    private static int SortByOwner(PostItem x, PostItem y)
    {
        return y.Owner.CompareTo(x.Owner);
    }
    #endregion

}
