﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;

public partial class Orion_APM_Services_AppFinderSelectItemsGrid : System.Web.UI.Page
{
	#region Nested Types

	private class AfRecord
	{
		public Object mode { get; set; }
		public Object selItems { get; set; }

		public Object uID { get; set; }
		public Object AdditionalInfo { get; set; }
		public Object Name { get; set; }
		public Object InstanceCount { get; set; }
		public Object Category { get; set; }
		public Object DisplayName { get; set; }
		public Object RealName { get; set; }
		public Object RollupType { get; set; }
		public Object Instance { get; set; }
		public Object Status { get; set; }
	}

	private class AfResponse 
	{
		public Int32 total { get; set; }
		public Object error { get; set; }
		public Object data { get; set; }
	}

	#endregion 

	#region Properties

	private AppFinderCurrentSettings _sett;
	private AppFinderCurrentSettings Sett
	{
		get { return _sett ?? (_sett = (Session[AppFinderWorkflow.SessionKey] as AppFinderWorkflow).CurrentSettings); }
	}

	#endregion
    
    #region Event Handlers
    
	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			var response = new AfResponse();
			try
			{
				var action = Request.QueryStringOrForm("action");
				if (action.IsValid()) 
				{
					if ("getConfig".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildConfig(response);
					}
					else if ("getPCData".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildPerformanceCounter(response);
					}
					else if ("getPOData".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildPerformanceObject(response);
					}
					else if ("getPMSMData".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildServiceAndProcessWmiAndSnmp(response);
					}
					else if ("getVmWarePOData".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildVmWarePerformanceObject(response);
					}
					else if ("getVmWarePCData".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildVmWarePerformanceCounter(response);
					}
					else if ("getVmWarePIData".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildVmWarePerformanceInstance(response);
					}
					else if ("getVmWareSelCat".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildVmWareSelCategories(response);
					}
					else if ("getVmWareSelCnt".Equals(action, StringComparison.InvariantCultureIgnoreCase))
					{
						this.BuildVmWareSelCounters(response);
					}
				}
			}
			catch (Exception ex)
			{
				new SolarWinds.Logging.Log().Error(ex);

				var error = ex.Message; 
				if (Sett.LastBrowseError.IsValid())
				{
					error = Sett.LastBrowseError;
				}
				response.error = error;
			}

			Response.ContentType = "text/html";
			Response.Write(JsHelper.Serialize(response));
			Response.End();
		}
	}

	#endregion

	#region Response Builders

	private void BuildConfig(AfResponse response)
	{
		response.data = new AfRecord
		{
			mode = Sett.BrowseMode.ToString(),
			selItems = new object[] { }
		};
	}

	private void BuildPerformanceObject(AfResponse response)
	{
		var rows = Sett.PerformanceCountersCategoriesTable
			.Select(null, "Name ASC");
		if (this.Validate(rows, response))
		{
			var data = rows
				.Select(row => new[] { row[0].ToString(), row[1].ToString() })
				.ToArray();
			response.data = data;
		}
	}

	private void BuildPerformanceCounter(AfResponse response)
	{
		var name = Request.QueryStringOrForm("name");
		if (name.IsNotValid() && Sett.PerformanceCategory.IsNotValid())
		{
			return;
		}
		if (name.IsValid())
		{
			Sett.PerformanceCategory = name;
		}

		var rows = this.GetRows(Sett.GetPerformanceCountersData(name));
		if (this.Validate(rows, response))
		{
			Int32 start, size;
			this.ParsePagingRange(out start, out size);

			var items = new List<AfRecord>();
			for (Int32 len = Math.Min(start + size, rows.Length); start < len; start++)
			{
				var item = new AfRecord
				{
					uID = rows[start]["UniqueId"],
					AdditionalInfo = rows[start]["AdditionalInfo"],
					Name = rows[start]["Name"]
				};
				items.Add(item);
			}
			response.total = rows.Length;
			response.data = items;
		}
	}

	private void BuildVmWarePerformanceObject(AfResponse response)
    {
		var name = this.Request.QueryStringOrForm("name");
		if (name.IsNotValid() && Sett.VmWareEntity.IsNotValid())
		{
			return;
		}
		if (name.IsValid() && !name.Equals(Sett.VmWareEntity))
		{
			if (name == "undefined")
			{
				var row = Sett.VmWareEntitiesTable.Select()
					.FirstOrDefault();
				if (row == null)
				{
					return;
				}
				name = row["Name"].ToString();
			}
			Sett.VmWareEntity = name;
			Sett.ClearVmWarePerfGroupsCountersInstancesForEntity();
		}

		var rows = Sett.VmWarePerfGroupsForEntityTable.Select(null, "Name ASC");
		if (this.Validate(rows, response))
        {
			var data = rows
				.Select(row => new[] { row[2].ToString() })
				.ToArray();
			response.data = data;
        }
    }

	private void BuildVmWarePerformanceCounter(AfResponse response)
    {
		var category = Request.QueryStringOrForm("category");
		if (category.IsNotValid() && Sett.VmWarePerfGroup.IsNotValid())
        {
            return;
        }
		if (category.IsValid() && category != Sett.VmWarePerfGroup)
		{
			Sett.VmWarePerfGroup = category;
			Sett.ClearVmWarePerfCountersForGroup();
		}

		var rows = this.GetRows(Sett.VmWarePerfCountersForGroupTable);
		if (this.Validate(rows, response))
        {
            Int32 start, size;
			ParsePagingRange(out start, out size);

			var items = new List<AfRecord>();
			for (Int32 len = Math.Min(start + size, rows.Length); start < len; start++)
            {
				var item = new AfRecord 
				{
					Category = category,
					DisplayName = rows[start]["Name"],
					RealName = rows[start]["RealCounterName"],
					RollupType = rows[start]["RollupType"]
				};
				items.Add(item);
            }
			response.data = items;
        }
    }

	private void BuildVmWarePerformanceInstance(AfResponse response) 
	{
		String category = Request.QueryStringOrForm("category"), counter = Request.QueryStringOrForm("counter");
		if (category.IsNotValid() || counter.IsNotValid())
		{
			return;
		}
		if (category != Sett.VmWarePerfGroup)
		{
			Sett.ClearVmWarePerfCountersForGroup();
			Sett.VmWarePerfGroup = category;
		}
		if (counter != Sett.VmWarePerfCounter)
		{
			Sett.ClearVmWarePerfInstancesForCounter();
			Sett.VmWarePerfCounter = counter;
		}
		
		var rows = this.GetRows(Sett.VmWarePerfInstancesForCounterTable);
		if (rows.Length > 0)
		{
			Int32 start, size;
			ParsePagingRange(out start, out size);

			var items = new List<AfRecord>();
			for (Int32 len = Math.Min(start + size, rows.Length); start < len; start++)
			{
                var defaultInstance = (
					rows[start]["Name"] == null || 
					rows[start]["Name"] == DBNull.Value || 
					rows[start]["Name"].ToString().IsNotValid()
				);

				var item = new AfRecord
				{
					Instance = defaultInstance ? AppFinderCurrentSettings.EMPTY_VMWARE_INSTANCE : rows[start]["Name"]
				};
				items.Add(item);
			}
			response.total = rows.Length + 1;
			response.data = items;
		}
        else
        {
			response.total = 1;
			response.data = new[] { AppFinderCurrentSettings.EMPTY_VMWARE_INSTANCE };
        }
	}

	private void BuildVmWareSelCategories(AfResponse response)
	{
		response.data = Sett.VmWareSelectedItems.GetCategories();
	}

	private void BuildVmWareSelCounters(AfResponse response)
    {
		var info = Sett.VmWareSelectedItems;
		
		List<String> categories = null;
		if (Request.QueryStringOrForm("category") == "All Counters")
		{
			categories = info.GetCategories();
		}
		else 
		{
			categories = new List<String>() { Request.QueryStringOrForm("category") };
		}

		var items = new List<AfRecord>();
		foreach (var category in categories)
		{
			foreach (var counter in info.GetCounters(category))
			{
				var item = new AfRecord
				{
					Category = category,
					DisplayName = counter.CounterDisplayName,
					RealName = counter.CounterRealName,
					RollupType = counter.CounterRollupType
				};
				items.Add(item);
			}
		}
		response.data = items;
    }

	private void BuildServiceAndProcessWmiAndSnmp(AfResponse response)
	{
		var rows = this.GetRows(Sett.ProcessOrServicesTable);
		if (this.Validate(rows, response))
		{
			Int32 start, size;
			this.ParsePagingRange(out start, out size);

			var items = new List<AfRecord>();
			for (Int32 len = Math.Min(start + size, rows.Length); start < len; start++)
			{
				if (Sett.BrowseMode == AppFinderBrowseMode.ServiceMonitor)
				{
					var item = new AfRecord
					{
						uID = rows[start]["UniqueId"],
						Name = rows[start]["Name"],
						DisplayName = rows[start]["DisplayName"],
						Status = rows[start]["Status"]
					};
					items.Add(item);
				}
				else
				{
					var item = new AfRecord
					{
						uID = rows[start]["UniqueId"],
						Name = rows[start]["Name"],
						InstanceCount = rows[start]["InstanceCount"]
					};
					items.Add(item);
				}
			}
			response.total = rows.Length;
			response.data = items;
		}
	}

	#endregion

	#region Helper Members

	private DataRow[] GetRows(DataTable data)
	{
		if (data == null) 
		{
			return new DataRow[] { };
		}

		String sort = Request.QueryStringOrForm("sort"), dir = "DESC".Equals(Request.QueryStringOrForm("dir"), StringComparison.InvariantCultureIgnoreCase) ? "DESC" : "ASC";
		if (sort.IsNotValid() || !data.Columns.Contains(sort))
		{
			if (data.Columns.Contains("Name"))
			{
				sort = "Name";
			}
			else 
			{
				for (Int32 i = 0, count = data.Columns.Count; i < count; i++) 
				{
					if (data.Columns[i].DataType == typeof(String)) 
					{
						sort = data.Columns[i].ColumnName;
						i = count;
					}
				}
			}
		}
		return data.Select(null, String.Format("{0} {1}", sort, dir));
	}

	private void ParsePagingRange(out Int32 start, out Int32 size)
	{
		Int32.TryParse(Request.QueryStringOrForm("start"), out start);
		if (!Int32.TryParse(Request.QueryStringOrForm("limit"), out size))
		{
			size = 25;
		}
	}

	private Boolean Validate(DataRow[] rows, AfResponse response)
	{
		if (Sett.LastBrowseError.IsValid()) 
		{
			response.error = Sett.LastBrowseError;
		}
		return (rows != null && rows.Length > 0);
	}

	#endregion
}