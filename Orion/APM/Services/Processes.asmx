﻿<%@ WebService Language="C#" Class="ProcessMonitor.Processes" %>
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models.ProcessMonitor;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.UI;

namespace ProcessMonitor
{
    /// <summary>
    /// Summary description for Applications
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class Processes: WebService
    {
        private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

        [WebMethod(EnableSession = true)]
        public void InitWizardSession(int nodeId, int? credentialId, List<string> processesNames)
        {
             if (!ApmRoleAccessor.AllowRealTimeProcessExplorer)
            {
                log.Error(this.CreateAccessDeniedMessage());
                throw this.CreateAccessDeniedException();
            }

            Node node;
            ComponentType ctype;

            using (
                IAPMBusinessLayer businessLayer =
                    ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                node = businessLayer.GetNode(nodeId);
            }

            var workflow = new AppFinderWorkflow();

            if (credentialId.HasValue)
            {
                workflow.CurrentSettings.CredentialSet =
                    new CredentialSet()
                    {
                        Broken = false,
                        Id = credentialId.Value,
                        Name = string.Empty,
                        UserName = string.Empty,
                        Password = string.Empty
                    };
                ctype = ComponentType.Process;
            }
            else
            {
                if (node.NodeSubType == NodeSubType.SNMP || node.IsLinuxAgent)
                {
                    ctype = ComponentType.ProcessOverSNMP;
                }
                else if (node.NodeSubType == NodeSubType.WMI || node.NodeSubType == NodeSubType.Agent)
                {
                    workflow.CurrentSettings.CredentialSet =
                        new CredentialSet()
                        {
                            Broken = false,
                            Id = ComponentBase.NodeWmiCredentialsId,
                            Name = string.Empty,
                            UserName = string.Empty,
                            Password = string.Empty
                        };
                    ctype = ComponentType.Process;
                }
                else
                {
                    throw new NotImplementedException(InvariantString.Format("Unsupported node type: {0}",
                        node.NodeSubType));
                }
            }

            workflow.CurrentSettings.CurrentComponentType = ctype;
            workflow.CurrentSettings.TargetServer = node;
            workflow.SelectedNodeIds = new List<int>(nodeId);
	        var id = 0;
            foreach (var processName in processesNames)
            {
	            id--;
                var ct = new ComponentTemplate { Type = ctype, Name = processName, Id = id };

	            ct.Settings.Clear();
	            var def = APMCache.ComponentDefinitions[ctype];
	            foreach (var sett in def.DefinitionSettings)
	            {
		            var val = sett.Value;
		            ct.Settings[sett.Key] = new SettingValue(sett.Key, val.Value, val.ValueType, val.SettingLevel,
		                                                     val.Required);
	            }
	            ct.Settings["ProcessName"] = new SettingValue(processName, SettingValueType.String, SettingLevel.Template,
	                                                          true);
	            if (ctype != ComponentType.ProcessOverSNMP)
	            {
		            ct.Settings[InternalSettings.CredentialSetKeyName] =
			            new SettingValue(workflow.CurrentSettings.CredentialSet.Id.ToString(CultureInfo.InvariantCulture), SettingValueType.String,
			                             SettingLevel.Template, true);
	            }
	            workflow.CurrentSettings.MonitorToEdit.Add(ct);
            }
	        Session[AppFinderWorkflow.SessionKey] = workflow;
        }

        [WebMethod(EnableSession = true)]
        public RealTimeSystemInfo GetSystemInfo(int nodeId, int? credentialId, Guid? cacheKey, string sortColumn, string sortOrder, int rowLimit)
        {
            if (!ApmRoleAccessor.AllowRealTimeProcessExplorer)
            {
                log.Error(this.CreateAccessDeniedMessage());
                throw CreateAccessDeniedException();
            }
            
            string accountId = Context.User.Identity.Name;
            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                return businessLayer.GetSystemInfo(nodeId, credentialId, accountId, cacheKey, sortColumn, sortOrder, rowLimit);
            }
        }

        [WebMethod(EnableSession = true)]
        public RealTimeKillProcessInfo KillProcesses(int nodeId, int[] pids, int? credentialId)
        {
            if (!ApmRoleAccessor.AllowRealTimeProcessExplorerFullControl)
            {
                log.Error(this.CreateAccessDeniedMessage());
                throw new ApplicationLocalizedException(() => Resources.APMWebContent.APMWEBDATA_JELI_02);
            }
            
            using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                return businessLayer.KillProcesses(nodeId, pids, credentialId);
            }
        }

        private string CreateAccessDeniedMessage()
        {
            var userName = "Unknown";
            if (this.Context.User != null 
                && this.Context.User.Identity != null 
                && this.Context.User.Identity.IsAuthenticated 
                && !string.IsNullOrWhiteSpace(this.Context.User.Identity.Name))
                userName = this.Context.User.Identity.Name;
            return string.Format(APMWebContent.Processes_CreateAccessDeniedMessage_User__0__from_IP__1__doesn_t_have_permission_to_access_RTPE_service_, userName, this.Context.Request.UserHostAddress);
        }

        private Exception CreateAccessDeniedException()
        {
            return new ApplicationLocalizedException(() => Resources.APMWebContent.APMWEBCODE_JELI_01);
        }
    }

}
