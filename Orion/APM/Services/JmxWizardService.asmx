﻿<%@ WebService Language="C#" Class="JmxWizard.JmxWizardService" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.UI.AppFinder;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;

namespace JmxWizard
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class JmxWizardService : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetStatusIconUrl(int nodeId)
        {
            if (nodeId == 0)
            {
                return null;
            }
            return StatusIconHelper.GetNodeStatusIconUrl(nodeId);
        }

        [WebMethod]
        public string GetTargetEndpointUrl(string urlFormat, string ipAddress, int portNumber, JmxProtocol jmxProtocol, string urlPath)
        {
            if (string.IsNullOrEmpty(ipAddress)
                || (portNumber <= 0 || portNumber > 65535))
            {
                return null;
            }
            
            IPAddress address;
            if (!IPAddress.TryParse(ipAddress, out address))
            {
                return null;
            }
            else
            {
                var node = new SolarWinds.APM.Common.Models.Node
                    {
                        IpAddress = ipAddress,
                        IpAddressFamily = address.AddressFamily
                    };
                return JmxHelper.GetUrl(urlFormat, node, portNumber, jmxProtocol, urlPath);
            }
        }
        
        [WebMethod(EnableSession = true)]
        public PartialResponse<IEnumerable<JmxExtJsTreeNodeInfo>> GetChildrenTreeNodes(Guid taskId, IEnumerable<string> parentNames, JmxEntityType entityType)
        {
            var settings = AppFinderWorkflow.SessionInstance.CurrentSettings;
            return new JmxTreeBuilder(settings).GetChildrenNodes(taskId, parentNames, entityType);
        }

        [WebMethod(EnableSession = true)]
        public PartialResponse<IEnumerable<JmxExtJsTreeNodeInfo>> GetChildrenNodesForCompositeAttribute(Guid taskId, IEnumerable<string> parentNames, string objectName, string attributeName)
        {
            var settings = AppFinderWorkflow.SessionInstance.CurrentSettings;
            return new JmxTreeBuilder(settings).GetChildrenNodesForCompositeAttribute(taskId, parentNames, objectName, attributeName);
        }

        [WebMethod(EnableSession = true)]
        public void ChangeAttributeSelection(IEnumerable<string> names, string displayName, string objectName, string attributeName, string key, bool isSelected)
        {
            var settings = AppFinderWorkflow.SessionInstance.CurrentSettings;
            var domain = names.ElementAt(names.Count() - 2); //the last is root, domain is just under the root
            lock (settings.JmxSelectedEntities)
            {
                var item =
                    settings.JmxSelectedEntities.Find(
                        match =>
                        match.Domain == domain && match.ObjectName == objectName && match.AttributeName == attributeName &&
                        match.Key == key);
                if (isSelected)
                {
                    if (item == null)
                    {
                        settings.JmxSelectedEntities.Add(new JmxMonitorSettings(displayName, domain, objectName, attributeName, key));
                    }
                }
                else
                {
                    if (item != null)
                    {
                        settings.JmxSelectedEntities.Remove(item);
                    }
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public void UnselectAttribute(string domain, string objectName, string attributeName, string key)
        {
            var settings = AppFinderWorkflow.SessionInstance.CurrentSettings;
            lock (settings.JmxSelectedEntities)
            {
                var item =
                    settings.JmxSelectedEntities.Find(
                        match =>
                        match.Domain == domain && match.ObjectName == objectName && match.AttributeName == attributeName &&
                        match.Key == key);
                if (item != null)
                {
                    settings.JmxSelectedEntities.Remove(item);
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public PartialResponse<IEnumerable<JmxGridAttributeItem>> GetSelectedAttributes()
        {
            var settings = AppFinderWorkflow.SessionInstance.CurrentSettings;
            return new JmxAttributesGridLoader(settings).GetSelectedAttributes();
        }
        
        [WebMethod(EnableSession = true)]
        public PartialResponse<double> GetAttributeValue(Guid taskId, string objectName, string attributeName, string key)
        {
            var settings = AppFinderWorkflow.SessionInstance.CurrentSettings;
            return new JmxAttributesGridLoader(settings).GetAttributeValue(taskId, objectName, attributeName, key);
        }

        [WebMethod(EnableSession = true)]
        public PartialResponse<bool> TestJmxConnection(Guid taskId)
        {
            var settings = AppFinderWorkflow.SessionInstance.CurrentSettings;
            var serverUri = JmxHelper.GetUrl(settings.JmxUrlFormat, settings.TargetServer,
                    settings.JmxPortNumber,
                    settings.JmxProtocol,
                    settings.JmxUrlPath);
            var target = settings.TargetServer;
            var credentialSet = settings.CredentialSet;
            var use64Bit = settings.Use64Bit;

            var taskResult =  ServiceLocatorForWeb.GetServiceForWeb<IJmxDataProvider>().TestJmxConnection(taskId, target, credentialSet, use64Bit, serverUri);

            var response = new PartialResponse<bool>()
            {
                TaskId = taskResult.Id,
                IsNotFinished = taskResult.TaskStatus == BusinessLayerTaskStatus.Running,
                ResponseData = taskResult.Result
            };

            return response;

        }

    }
}