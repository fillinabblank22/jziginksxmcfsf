﻿<%@ WebService Language="C#" Class="EventLogViewer.Events" %>
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Models.ProcessMonitor;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Script.Services;
using System.Web.Services;

namespace EventLogViewer
{
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ScriptService]
	public class Events : ApmWebService
	{
		private string accountId;
		private string AccountId
		{
			get { return accountId ?? (accountId = Context.User.Identity.Name); }
		}

		[WebMethod(EnableSession = true)]
		public void RestartPolling(int nodeId, int credId)
        {
            VerifySystemEventExplorerRights();

			using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				bl.RealTimeRestartEventsPolling(AccountId, nodeId, credId);
			}
		}

		[WebMethod(EnableSession = true)]
		public void InitWizardSession(int nodeId, string nodeIp, NodeSubType nodeSubType, int credId, List<RealTimeEventInfo> eventsInfo)
		{
			var workflow = new AppFinderWorkflow
			{
				SelectedNodeIds = new List<int> { nodeId }
			};
			var workflowSett = workflow.CurrentSettings;
			workflowSett.CurrentComponentType = ComponentType.WindowsEventLog;

            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                workflowSett.TargetServer = bl.GetNode(nodeId);
            }
			workflowSett.CredentialSet = new CredentialSet
			{
				Broken = false,
				Id = credId,
				Name = string.Empty,
				UserName = string.Empty,
				Password = string.Empty
			};

			foreach (var eventInfo in eventsInfo)
			{
				var template = new ComponentTemplate
				{
					Type = ComponentType.WindowsEventLog,
					Name = string.Format("{0} - {1}", eventInfo.SourceName, eventInfo.EventCode),
					Id = -(1 + workflowSett.MonitorToEdit.Count)
				};
				template.Settings.Clear();

				var definition = APMCache.ComponentDefinitions[ComponentType.WindowsEventLog];
				foreach (var sett in definition.DefinitionSettings)
				{
					var val = sett.Value;
					template.Settings[sett.Key] = new SettingValue(sett.Key, val.Value, val.ValueType, val.SettingLevel, val.Required);
				}

				var message = eventInfo.Message;
				if (!string.IsNullOrWhiteSpace(message))
				{
					var lines = message
						.Split(new[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries)
						.Select(item => string.Format(@"""{0}""", item.Trim()))
						.Distinct(StringComparer.InvariantCultureIgnoreCase);

					message = string.Join(", ", lines);
				}

				template.Settings["EntryMatch"] = new SettingValue(EventLog_EntryMatch.Custom.ToString(), SettingValueType.Option, SettingLevel.Template, true);
				template.Settings["LogName"] = new SettingValue(EventLog_Name.Custom.ToString(), SettingValueType.Option, SettingLevel.Template, true);
				template.Settings["LogNameFilter"] = new SettingValue(eventInfo.Logfile, SettingValueType.String, SettingLevel.Template, false);
				template.Settings["EntrySource"] = new SettingValue(eventInfo.SourceName, SettingValueType.String, SettingLevel.Template, false);
				template.Settings["EntryIDType"] = new SettingValue(EventLog_EventIDType.IncludeIDs.ToString(), SettingValueType.Option, SettingLevel.Template, false);
				template.Settings["EntryID"] = new SettingValue(eventInfo.EventCode.ToString(), SettingValueType.String, SettingLevel.Template, false);
				template.Settings["EntryType"] = new SettingValue(eventInfo.EventType.ToString(), SettingValueType.String, SettingLevel.Template, false);
				template.Settings["EntryIncludeOperation"] = new SettingValue(EventLog_EntryOperation.Disable.ToString(), SettingValueType.Option, SettingLevel.Template, false);
				template.Settings["EntryIncludeFilter"] = new SettingValue(message, SettingValueType.String, SettingLevel.Template, false);

				workflowSett.MonitorToEdit.Add(template);
			}
			Session[AppFinderWorkflow.SessionKey] = workflow;
		}
	}
}
