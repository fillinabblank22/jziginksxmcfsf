<%@ WebService Language="C#" Class="Orion.APM.Services.WindowsEvent" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.Common.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;

namespace Orion.APM.Services
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class WindowsEvent: WebService
    {
        //private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

        private class SortInfo
        {
            public string property;
            public string direction;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public PageableDataTable GetEventsPaged(long componentId, int timezoneOffset, int eventType, string searchText, int limit, int page, int start, string sort)
        {
            string orderBy = "TimeGeneratedUtc";
            string orderDir = "DESC";

            if (!string.IsNullOrEmpty(sort))
            {
                var s = new JavaScriptSerializer();
                var sortInfo = s.Deserialize<SortInfo[]>(sort);
                if (sortInfo.Length > 0)
                {
                    orderBy = sortInfo[0].property;
                    orderDir = sortInfo[0].direction;

                    if (orderBy == "TimeGenerated")
                        orderBy = "TimeGeneratedUtc";
                }
            }

            int limitFrom = start + 1;
            int limitTo = limitFrom + limit - 1;

            var parameters = new Dictionary<string, object>();
            parameters["ComponentId"] = componentId;

            StringBuilder whereCondition = new StringBuilder("(we.ComponentId = @ComponentId)");
            if (eventType > 0)
            {
                whereCondition.Append(" AND (we.EventType = @eventType)");
                parameters["eventType"] = eventType;
            }
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                whereCondition.Append(" AND (we.Message like @searchText)");
                parameters["searchText"] = InvariantString.Format("%{0}%", searchText);
            }

            var swql = InvariantString.Format("SELECT COUNT(ComponentId) as cnt FROM Orion.APM.WindowsEvent we WHERE {0}", whereCondition.ToString());

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var countTable = swis.Query(swql, parameters);
                int totalRows = (int) countTable.Rows[0][0];

                // joining Orion.APM.Application to apply account limitations
                swql = InvariantString.Format(
                    @"SELECT we.LogFile, we.EventType, we.EventCode, we.TimeGeneratedUtc, we.ComputerName, we.SourceName, we.User, we.Message
FROM Orion.APM.WindowsEvent we
INNER JOIN Orion.APM.Component c ON c.ComponentID = we.ComponentID
INNER JOIN Orion.APM.Application a ON a.ApplicationID = c.ApplicationID
WHERE {0}
ORDER BY we.{1} {2} WITH ROWS @limitFrom TO @limitTo",
                    whereCondition.ToString(), orderBy, orderDir);

                parameters["limitFrom"] = limitFrom;
                parameters["limitTo"] = limitTo;

                var data = swis.Query(swql, parameters);

                Func<Object, String> convertUtcTime = (date => date is DBNull ? String.Empty : ((DateTime)date).AddMinutes(-timezoneOffset).ToString(CultureInfo.CurrentCulture));
                data.Columns.Add("TimeGenerated", typeof (string));
                foreach (DataRow row in data.Rows)
                {
                    row["TimeGenerated"] = convertUtcTime(row["TimeGeneratedUtc"]);
                }
                data.Columns.Remove("TimeGeneratedUtc");

                return new PageableDataTable(data, totalRows);
            }
        }
    }
}
