﻿<%@ WebService Language="C#" Class="ChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Caching;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using ChartDataResults = SolarWinds.Orion.Web.Charting.v2.ChartDataResults;
using V2DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;
using V2DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;
using Charting = SolarWinds.APM.Web.Charting;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ChartData : ChartDataWebService
{
    private ChartingDal dal;
    private ChartingDal ChartDal
    {
        get
        {
            if (dal == null)
            {
                dal = new ChartingDal();
            }
            return dal;
        }
    }
    
    [WebMethod]
    public ChartDataResults MMAvgStatisticData(ChartDataRequest request)
    {
        if (Context.Request.Headers.Get("User-Agent") == null)
        {
            request.AllSettings["Exporting"] = bool.TrueString;
        }

        return ChartDataHelper.MMAvgStatisticData(request);
    }

    [WebMethod]
    public ChartDataResults MMAvgIOTotalData(ChartDataRequest request)
    {
        return ChartDataHelper.MMAvgIOTotalData(request);
    }
    
    [WebMethod]
    public ChartDataResults GetApplicationAvailabilityDataSeries(ChartDataRequest request)
    {
        return GetAvailabilitySeries(request, true);
    }

    [WebMethod]
    public ChartDataResults GetComponentAvailabilityDataSeries(ChartDataRequest request)
    {
        return GetAvailabilitySeries(request, false);
    }

    [WebMethod]
    public ChartDataResults GetComponentPercentAvailabilityDataSeries(ChartDataRequest request)
    {
        return GetPercentAvailabilitySeries(request, false);
    }
    
    private ChartDataResults GetAvailabilitySeries(ChartDataRequest request, bool isApplication)
    {
		DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
		dateRange = MultiChartDataHelper.AdjustDateRange(request, dateRange);
		
        int objectId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out objectId);
        }
        if (objectId == 0)
        {
            throw new ArgumentException("No Object ID is defined in request.");
        }

        DataTable dt = ChartDal.GetAvailability(objectId, dateRange.StartDate, dateRange.EndDate, isApplication);

        var sampledSeries = ChartDataHelper.GetBasicAvailabilitySeries(dt, request, true, dateRange);
		var result = new ChartDataResults(sampledSeries);
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        return dynamicResult;
    }

    private ChartDataResults GetPercentAvailabilitySeries(ChartDataRequest request, bool isApplication)
    {
		DateRange dateRange = ChartDataHelper.SetDateRange(request);

        int objectId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out objectId);
        }
        if (objectId == 0)
        {
            throw new ArgumentException("No Object ID is defined in request.");
        }

        DataTable dt = ChartDal.GetPercentAvailability(objectId, dateRange.StartDate, dateRange.EndDate, isApplication);

		var sampledSeries = ChartDataHelper.GetPercentAvailabilitySeries(dt, request, true, dateRange);
        var result = new ChartDataResults(sampledSeries);
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        return dynamicResult;
    }
    
    [WebMethod]
    public Charting.ChartDataResults ApplicationHealthOverview(ChartDataRequest request)
    {
        ResourceInfo resource = null;
        if ((int) request.AllSettings["ResourceID"] > 0)
        {
            resource = ResourceManager.GetResourceByID((int) request.AllSettings["ResourceID"]);
            Context.Items[typeof (ViewInfo).Name] = resource.View;
        }
        
        DataTable data = SwisDAL.GetApplicationHealthOverviewInformation(this.GetFilter(resource, request, false));

        var dataSeriesColection = new List<Charting.DataSeries>();
        dataSeriesColection.Add(new Charting.DataSeries()
        {
            TagName = "Default"
        });

        if (data == null || data.Rows.Count == 0)
        {
            return new Charting.ChartDataResults(dataSeriesColection);
        }

        DataRow row = data.Rows[0];

        dataSeriesColection[0].Data.Add(new Charting.DataPoint(Resources.APMWebContent.APMWEBDATA_AK1_173,
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row["UpStatus"]},
                                                                  {"count", row["UpStatus"]},
                                                                  {"size", row["ApplicationCount"]},
                                                                  {"legendLabel", Resources.APMWebContent.APMWEBDATA_AK1_173},
                                                                  {"icon", "Small-App-Up.gif"},
                                                                  {"color", "#77BD2D"},
                                                                  {"limitation", Limitation.GetCurrentViewLimitationID()},
                                                                  {"filter", this.GetFilter(resource, request, true)},
                                                                  {"viewid", GetViewID(resource)},
                                                                  {"visible", IsSeriesVisible((int)row["UpStatus"])}
                                                              }));

        dataSeriesColection[0].Data.Add(new Charting.DataPoint(Resources.APMWebContent.APMWEBDATA_AK1_174,
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row["WarningStatus"]},
                                                                  {"count", row["WarningStatus"]},
                                                                  {"size", row["ApplicationCount"]},
                                                                  {"legendLabel", Resources.APMWebContent.APMWEBDATA_AK1_174},
                                                                  {"icon", "Small-App-Up-Warn.gif"},
                                                                  {"color", "#FCD928"},
                                                                  {"limitation", Limitation.GetCurrentViewLimitationID()},
                                                                  {"filter", this.GetFilter(resource, request, true)},
                                                                  {"viewid", GetViewID(resource)},
                                                                  {"visible", IsSeriesVisible((int)row["WarningStatus"])}
                                                              }));

        dataSeriesColection[0].Data.Add(new Charting.DataPoint(Resources.APMWebContent.APMWEBDATA_AK1_175,
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row["CriticalStatus"]},
                                                                  {"count", row["CriticalStatus"]},
                                                                  {"size", row["ApplicationCount"]},
                                                                  {"legendLabel", Resources.APMWebContent.APMWEBDATA_AK1_175},
                                                                  {"icon", "Small-App-Up-Critical.gif"},
                                                                  {"color", "#F99D1C"},
                                                                  {"limitation", Limitation.GetCurrentViewLimitationID()},
                                                                  {"filter", this.GetFilter(resource, request, true)},
                                                                  {"viewid", GetViewID(resource)},
                                                                  {"visible", IsSeriesVisible((int)row["CriticalStatus"])}
                                                              }));

        dataSeriesColection[0].Data.Add(new Charting.DataPoint(Resources.APMWebContent.APMWEBDATA_AK1_176,
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row["DownStatus"]},
                                                                  {"count", row["DownStatus"]},
                                                                  {"size", row["ApplicationCount"]},
                                                                  {"legendLabel", Resources.APMWebContent.APMWEBDATA_AK1_176},
                                                                  {"icon", "Small-App-Down.gif"},
                                                                  {"color", "#E61929"},
                                                                  {"limitation", Limitation.GetCurrentViewLimitationID()},
                                                                  {"filter", this.GetFilter(resource, request, true)},
                                                                  {"viewid", GetViewID(resource)},
                                                                  {"visible", IsSeriesVisible((int)row["DownStatus"])}
                                                              }));

        dataSeriesColection[0].Data.Add(new Charting.DataPoint(Resources.APMWebContent.APMWEBDATA_AK1_177,
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row["UnknownStatus"]},
                                                                  {"count", row["UnknownStatus"]},
                                                                  {"size", row["ApplicationCount"]},
                                                                  {"legendLabel", Resources.APMWebContent.APMWEBDATA_AK1_177},
                                                                  {"icon", "Small-App-Unknown.gif"},
                                                                  {"color", "#B4B1B2"},
                                                                  {"limitation", Limitation.GetCurrentViewLimitationID()},
                                                                  {"filter", this.GetFilter(resource, request, true)},
                                                                  {"viewid", GetViewID(resource)},
                                                                  {"visible", IsSeriesVisible((int)row["UnknownStatus"])}
                                                              }));
        
        dataSeriesColection[0].Data.Add(new Charting.DataPoint(Resources.APMWebContent.APMWEBDATA_AK1_178,
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row["OtherStatus"]},
                                                                  {"count", row["OtherStatus"]},
                                                                  {"size", row["ApplicationCount"]},
                                                                  {"legendLabel", Resources.APMWebContent.APMWEBDATA_AK1_178},
                                                                  {"icon", "Small-App-Unknown.gif"},
                                                                  {"color", "#2D5EC5"},
                                                                  {"limitation", Limitation.GetCurrentViewLimitationID()},
                                                                  {"filter", this.GetFilter(resource, request, true)},
                                                                  {"viewid", GetViewID(resource)},
                                                                  {"visible", IsSeriesVisible((int)row["OtherStatus"])}
                                                              }));

        return new Charting.ChartDataResults(dataSeriesColection);

    }

    private int GetViewID(ResourceInfo resource)
    {
        return resource != null ? resource.View.ViewID : 0;
    }

    private bool IsSeriesVisible(int statusCount)
    {
        return statusCount > 0;
    }

    protected String GetFilter(ResourceInfo resource, ChartDataRequest request, Boolean encode)
    {
        var filter = String.Empty;
        if (resource != null)
        {
            if ("NodeDetails".Equals(resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase))
            {
                var nodeId = (int) request.AllSettings["NodeId"];
                filter = String.Format("Application.NodeID = {0}", nodeId);
            }
            if (encode)
            {
                return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(filter));
            }
        }
        return filter;
    }

    [WebMethod]
    public ChartDataResults GetResponseTimeDataSeries(ChartDataRequest request)
    {
        // it will be always a component
		DateRange dateRange = ChartDataHelper.SetDateRange(request);

        int objectId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out objectId);
        }
        if (objectId == 0)
        {
            throw new ArgumentException("No Object ID is defined in request.");
        }

        DataTable dt = new DataTable();

        if (request.AllSettings.ContainsKey("HasResponseTimeSupport"))
        {
            dt = ChartDal.GetResponceTimeData(objectId, dateRange.StartDate, dateRange.EndDate);
        }

		var sampledSeries = ChartDataHelper.GetBasicSeries(dt, request, dateRange);
        var result = new ChartDataResults(sampledSeries);
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        return dynamicResult;
    }

    [WebMethod]
    public ChartDataResults GetStatisticDataDataSeries(ChartDataRequest request)
    {
        // it will be always a component
		var dateRange = ChartDataHelper.SetDateRange(request);

        int objectId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out objectId);
        }
        if (objectId == 0)
        {
            throw new ArgumentException("No Object ID is defined in request.");
        }

        string columnName = string.Empty;
        bool isDynamic = false;
        
        if (request.AllSettings.ContainsKey("StatisticColumn") && !string.IsNullOrEmpty((string)request.AllSettings["StatisticColumn"]))
        {
            columnName = (string)request.AllSettings["StatisticColumn"];
            isDynamic = true;
        }

        DataTable dt = new DataTable();

        if (request.AllSettings.ContainsKey("HasStatisticData"))
        {
            dt = ChartDal.GetStatisticDataData(objectId, columnName, dateRange.StartDate, dateRange.EndDate, isDynamic);
        }

		var sampledSeries = ChartDataHelper.GetBasicSeries(dt, request, dateRange);
        var result = new ChartDataResults(sampledSeries);
        
        ChartDataHelper.SetCustomChartOptionsOverride(request, result);
        
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
        return dynamicResult;
    }
        
    [WebMethod]
    public ChartDataResults GetMultipleStatisticData(ChartDataRequest request)
    {
        return ChartDataHelper.GetMultipleStatisticData(request);
    }
    
    [WebMethod]
    public ChartDataResults MultiStatisticData(ChartDataRequest request)
    {
        return ChartDataHelper.MultiStatisticData(request);
    }

    [WebMethod]
    public ChartDataResults GetBaselineMinMaxChartsData(ChartDataRequest request)
    {
        ChartDataResults result;        

        if (request.HasKeys("DaysOfDataToLoad", "StatisticColumn", "netObjectIds") 
            && request.AllSettings["netObjectIds"] != null
            && ((object[])request.AllSettings["netObjectIds"]).Length > 0)
        {
            var key = InvariantString.Format("MinMax#{0}#{1}#{2}", ((object[])request.AllSettings["netObjectIds"])[0], request.AllSettings["StatisticColumn"], request.AllSettings["DaysOfDataToLoad"]);

            result = System.Web.HttpContext.Current.Cache[key] as ChartDataResults;

            if (result == null)
            {
                result = ChartDataHelper.GetBaselineMinMaxChartsData(request);

                if (result != null)
                {
                    System.Web.HttpContext.Current.Cache.Insert(key, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 2, 0));
                }
            }            
        }
        else
        {
            result = ChartDataHelper.GetBaselineMinMaxChartsData(request);
        }

        return result;
    }

    [WebMethod]
    public ChartDataResults GetBaselineAreaChartsData(ChartDataRequest request)
    {
        ChartDataResults result;        

        if (request.HasKeys("DaysOfDataToLoad", "StatisticColumn", "columnSchemaId", "netObjectIds")
            && request.AllSettings["netObjectIds"] != null
            && ((object[])request.AllSettings["netObjectIds"]).Length > 0)
        {
            var key = InvariantString.Format("Area#{0}#{1}#{2}#{3}", ((object[])request.AllSettings["netObjectIds"])[0], request.AllSettings["columnSchemaId"], request.AllSettings["StatisticColumn"], request.AllSettings["DaysOfDataToLoad"]);

            result = System.Web.HttpContext.Current.Cache[key] as ChartDataResults;

            if (result == null)
            {
                result = ChartDataHelper.GetBaselineAreaChartsData(request);

                if (result != null)
                {
                    System.Web.HttpContext.Current.Cache.Insert(key, result, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 2, 0));
                }
            }
        }
        else
        {
            result = ChartDataHelper.GetBaselineAreaChartsData(request);            
        }

        return result;
    }
}
