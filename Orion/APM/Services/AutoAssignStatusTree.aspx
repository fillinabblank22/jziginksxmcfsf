﻿<%@ Page Language="C#" %>

<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Common" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.i18n.Registrar" %>
<script runat="server">
	
    readonly String TplStatusPass = string.Format(" <br/><span class='ad-status-info'>{0}</span>", Resources.APMWebContent.APMWEBCODE_VB1_76);
    readonly String TplStatusWait = string.Format(" <br/><span class='ad-status-info'>{0}</span>", Resources.APMWebContent.APMWEBCODE_VB1_78);
    readonly String TplStatusProc = string.Format(" <br/><span class='ad-status-info'>{0}</span>", Resources.APMWebContent.APMWEBCODE_VB1_79);
    readonly String TplStatusFail = string.Format(" <br/><span class='ad-status-info'>{0}</span>", Resources.APMWebContent.APMWEBCODE_VB1_80);
	
	protected void Page_Load(object sender, EventArgs e)
	{
		string groupBy = ((Request.Form["groupby"] == null) || Request.Form["groupby"].Equals("undefined", StringComparison.OrdinalIgnoreCase)) ? "Nodes" : Request.Form["groupby"];
		string rootNode = Request.Form["node"] ?? "/";

		JavaScriptSerializer serializer = new JavaScriptSerializer();

		object data = GetTreeNodes(groupBy, rootNode);

		string content = serializer.Serialize(data);

		Response.ContentType = "text/html";
		Response.Write(content);
		Response.End();
	}

	private List<ExtTreeNode> GetTreeNodes(string groupBy, string rootNode)
	{
		List<ExtTreeNode> nodes = new List<ExtTreeNode>();

		if (rootNode == "/")
		{
			AddRootNodes(nodes, groupBy);
		}
		else
		{
			GetChildNodes(nodes, groupBy, rootNode);
		}

		return nodes;
	}

	private void AddRootNodes(List<ExtTreeNode> nodes, string groupBy)
	{
		bool groupByNodes = (groupBy.Equals("Nodes", StringComparison.OrdinalIgnoreCase));

		using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			AutoAssignState state = bl.GetAutoAssignState();
			if (state != null)
			{
				if (groupByNodes)
				{
					foreach (int nodeID in state.Settings.NodeIDs)
					{
						nodes.Add(GetTreeNodeForNode("", nodeID, false, null, state.IsNodeCompleted(nodeID), state.GetApplicationsFoundNumberForNode(nodeID)));
					}
				}
				else
				{
					foreach (int templateID in state.Settings.TemplateIDs)
					{
						bool isFound = false;

						foreach (int nodeID in state.Settings.NodeIDs)
						{
							Dictionary<int, AutoAssignResult> results = state.GetResultByNodeID(nodeID);
							foreach (KeyValuePair<int, AutoAssignResult> result in results)
							{
								if (result.Key == templateID)
								{
									nodes.Add(GetTreeNodeForAppTemplate("", templateID, false, result.Value));

									isFound = true;
									break;
								}
							}

							if (isFound)
							{
								break;
							}
						}
					}
				}
			}
		}
	}

	private void GetChildNodes(List<ExtTreeNode> nodes, string groupBy, string rootNode)
	{
		string[] pathParts = rootNode.Split(new char[] { '/' });
		if (pathParts.Length == 0)
		{
			return;
		}

		bool groupByNodes = (groupBy.Equals("Nodes", StringComparison.OrdinalIgnoreCase));

		using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			AutoAssignState state = bl.GetAutoAssignState();
			if (state != null)
			{
				if (groupByNodes)
				{
					int nodeID = Convert.ToInt32(pathParts[pathParts.Length - 1]);
					if (state.Settings.NodeIDs.Contains(nodeID))
					{
						Dictionary<int, AutoAssignResult> results = state.GetResultByNodeID(nodeID);
						foreach (KeyValuePair<int, AutoAssignResult> result in results)
						{
							nodes.Add(GetTreeNodeForAppTemplate(rootNode, result.Key, true, result.Value));
						}
					}
				}
				else
				{
					int appTemplateID = Convert.ToInt32(pathParts[pathParts.Length - 1]);

					foreach (int nodeID in state.Settings.NodeIDs)
					{
						Dictionary<int, AutoAssignResult> results = state.GetResultByNodeID(nodeID);
						foreach (KeyValuePair<int, AutoAssignResult> result in results)
						{
							if (result.Key == appTemplateID)
							{
								nodes.Add(GetTreeNodeForNode(rootNode, nodeID, true, result.Value, state.IsNodeCompleted(nodeID), state.GetApplicationsFoundNumberForNode(nodeID)));
								break;
							}
						}
					}
				}
			}
		}
	}

	private ExtTreeNode GetTreeNodeForNode(string rootNode, int nodeID, bool isLeaf, AutoAssignResult autoAssignResult, bool isNodeCompleted, int appsFoundNumber)
	{
		DataTable table = SwisDAL.GetNodesByProperty("NodeId", nodeID.ToString());
		DataRow dataRow = table.Rows[0];

		string nodeName = dataRow["Caption"].ToString();
        string nodeIcon = SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL(nodeID, (int)dataRow["Status"], (int)dataRow["ChildStatus"]);

		ExtTreeNode treeNode = new ExtTreeNode(
            rootNode + "/" + nodeID, (!isLeaf) ? String.Format("<b>{0}</b> - {1}", HttpUtility.HtmlEncode(nodeName), (isNodeCompleted) ? String.Format(Resources.APMWebContent.APMWEBCODE_VB1_81,
            appsFoundNumber) : Resources.APMWebContent.APMWEBCODE_VB1_82) : nodeName, isLeaf);
		treeNode.icon = nodeIcon;
		treeNode.nodeId = nodeID.ToString();

		if (isLeaf)
		{
			if (autoAssignResult.Passed)
			{
				treeNode.status = "assigned";
				treeNode.text += String.Format(TplStatusPass, treeNode.status);
			}
			else
			{
				treeNode.status = "unassigned";
				treeNode.failureReason = autoAssignResult.FailureReason.ToString();
				if (autoAssignResult.ExecuteState == ExecuteState.Done)
				{
                    treeNode.text += String.Format(TplStatusFail, GetLocalizedProperty("TreeNodeStatus", treeNode.status), GetLocalizedProperty("FailureReason", treeNode.failureReason));
				}
				else if (autoAssignResult.ExecuteState == ExecuteState.Processing)
				{
                    treeNode.text += String.Format(TplStatusProc, GetLocalizedProperty("TreeNodeStatus", treeNode.status));
				}
				else 
				{
                    treeNode.text += String.Format(TplStatusWait, GetLocalizedProperty("TreeNodeStatus", treeNode.status));
				}
			}
			var href = BaseResourceControl.GetViewLink(String.Format("N:{0}", nodeID));
			treeNode.text = String.Format("<a href=\"{0}\" NoTip=\"true\">{1}</a>", href, treeNode.text);
		}

		return treeNode;
	}

	private ExtTreeNode GetTreeNodeForAppTemplate(string rootNode, int appTemplateID, bool isLeaf, AutoAssignResult autoAssignResult)
	{
		using (IAPMBusinessLayer bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			ApplicationTemplate appTemplate = bl.GetApplicationTemplate(appTemplateID);

			ExtTreeNode treeNode = new ExtTreeNode(rootNode + "/" + appTemplate.Id, isLeaf ? HttpUtility.HtmlEncode(appTemplate.Name) : String.Format("<b>{0}</b>", HttpUtility.HtmlEncode(appTemplate.Name)), isLeaf);

			if (autoAssignResult.Passed)
			{
				Application application = bl.GetApplication(autoAssignResult.ApplicationID);
				ApmStatus appStatus = new ApmStatus(application.Status);

				treeNode.icon = appStatus.ToString("smallappimgpath", null);
				treeNode.applicationID = application.Id.ToString();
				treeNode.templateID = appTemplate.Id.ToString();
				treeNode.status = "assigned";

				if (isLeaf)
				{
                    string nodeText = treeNode.text + String.Format(TplStatusPass, GetLocalizedProperty("TreeNodeStatus", treeNode.status));
					string appNetObjectId = ApmApplication.GetNetObjectId(application.Id, application.CustomApplicationType);
					treeNode.text = String.Format("<a href=\"{0}\">{1}</a>", BaseResourceControl.GetViewLink(appNetObjectId), nodeText);
				}
			}
			else
			{
				treeNode.icon = "/Orion/APM/images/Admin/icon_application_template.gif";
				treeNode.templateID = appTemplate.Id.ToString();
				treeNode.status = "unassigned";
				treeNode.failureReason = autoAssignResult.FailureReason.ToString();

				if (isLeaf)
				{
					string nodeText = treeNode.text;
					if (autoAssignResult.ExecuteState == ExecuteState.Done)
					{
                        nodeText += String.Format(TplStatusFail, GetLocalizedProperty("TreeNodeStatus", treeNode.status), GetLocalizedProperty("FailureReason", treeNode.failureReason));
					}
					else if (autoAssignResult.ExecuteState == ExecuteState.Processing)
					{
                        nodeText += String.Format(TplStatusProc, GetLocalizedProperty("TreeNodeStatus", treeNode.status));
					}
					else 
					{
                        nodeText += String.Format(TplStatusWait, GetLocalizedProperty("TreeNodeStatus", treeNode.status));
					}
					var url = SolarWinds.APM.Web.UI.ApmMasterPage.GetEditTemplatePageUrl(appTemplate.Id);
					treeNode.text = String.Format(@"<a href=""{0}"">{1}</a>", url, nodeText);
				}
			}
			return treeNode;
		}
	}

	public class ExtTreeNode
	{
		public ExtTreeNode(string id, string text, bool leaf)
		{
			this.id = id;
			this.text = text;
			this.leaf = leaf;
			icon = null;
			uiProvider = "SW.APM.AutoAssignStatusTree.SWTreeNodeUI";
			status = null;
			failureReason = null;
			applicationID = null;
			templateID = null;
			nodeId = null;
		}

		public string id;
		public string text;
		public bool leaf;
		public string icon;
		public string uiProvider;

		public string status;
		public string failureReason;
		public string templateID;
		public string applicationID;
		public string nodeId;
	}

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }        
</script>
