﻿<%@ WebService Language="C#" Class="Applications" %>
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.Models;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.Swis;
using SolarWinds.APM.Web.Swis.Applications;
using SolarWinds.APM.Web.Swis.Templates;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Common.Models.Alerts;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

using PageableDataTable = SolarWinds.APM.Web.PageableDataTable;

/// <summary>
/// Summary description for Applications
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Applications : ApmWebService
{
    private static readonly DateTime unmanageInfiniteDate = new DateTime(9999, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    private readonly IPageableQueryService pageableQueryService;

    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public Applications() : this(new ApplicationsPageableQueryService())
    {
    }
        
    public Applications(IPageableQueryService pageableQueryService)
    {
        this.pageableQueryService = pageableQueryService;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetApplicationsPaged(string property, string value)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        value = GetSqlString(value);

        string swql = pageableQueryService.GenerateQuery(pageSize, startRowNumber, sortColumn, sortDirection, property, value, String.Empty);

        DataTable pageData = SwisProxy.ExecuteQuery(swql);

        pageData = AddAlertSuppressionStatesToDataTable(pageData);

        PreparePagedApplications(pageData);

        return new PageableDataTable(pageData, GetRowCount(property, value));
    }

    private DataTable AddAlertSuppressionStatesToDataTable(DataTable dataTable)
    {
        if (dataTable == null)
        {
            log.Error("Recieved DataTable object is null");
            return null;
        }

        string[] entityUris = dataTable.AsEnumerable().Select(row => row.Field<string>("EntityUri")).ToArray();
        ISwisConnectionProxyCreator proxyCreator = new SwisConnectionProxyCreator(() => new SwisConnectionProxyFactory().CreateConnection());
        Dictionary<string, EntityAlertSuppressionMode> alertSuppressionModeLookup;
        using (var swis = proxyCreator.Create())
        {
            var alertSuppressionStates = swis.Invoke<EntityAlertSuppressionState[]>("Orion.AlertSuppression", "GetAlertSuppressionState", new object[] {entityUris});
            alertSuppressionModeLookup = alertSuppressionStates.ToDictionary(s => s.EntityUri, s => s.SuppressionMode);
        }

        foreach(DataRow row in dataTable.Rows)
        {
            EntityAlertSuppressionMode mode;
            if (alertSuppressionModeLookup.TryGetValue(row["EntityUri"] as string, out mode))
            {
                row["AlertSuppressionState"] = (int) mode;
            }
        }
        return dataTable;
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        string format = @"
SELECT {0} as theValue, Count(a.NodeID) as theCount         
FROM Orion.Nodes n
JOIN (SELECT app.NodeID, app.ApplicationTemplateID, app.ApplicationID, app.PrimaryGroupID FROM Orion.APM.Application app) a ON 
    a.NodeId = n.NodeId
JOIN Orion.APM.ApplicationTemplate AS at ON 
    a.ApplicationTemplateID = at.ApplicationTemplateID
JOIN Orion.Engines AS e ON
    e.EngineID = n.EngineID
LEFT JOIN Orion.APM.ApplicationCustomProperties acup ON acup.ApplicationId = a.ApplicationID
LEFT JOIN Orion.Container c ON c.ContainerID = a.PrimaryGroupID
GROUP BY {0}
ORDER BY {0} ASC
";
        Type propertyType = GetPropertyType(property);
        if (propertyType == typeof(string))
        {
            property = String.Format("ISNULL({0}, '')", property);
        }
        return SwisProxy.ExecuteQuery(String.Format(format, property.Replace("a.CustomProperties.", "acup.")));
    }

    [WebMethod]
    public void DeleteApplications(IList<int> applicationIds)
    {
        try
        {
            VerifyRights();

            var applicationInGroupsIds = this.GetApplicationInGroupsIds(applicationIds);

            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                if (applicationInGroupsIds != null && applicationInGroupsIds.Count > 0)
                {
                    businessLayer.CreateApplicationNodesExclusions(applicationInGroupsIds);
                    businessLayer.DeleteApplications(applicationIds);
                    businessLayer.DeleteApplicationGroupRelations(applicationInGroupsIds);
                }
                else
                {
                    businessLayer.DeleteApplications(applicationIds);
                }
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception caught in Application.asmx in method DeleteApplications", ex);
            throw;
        }
    }

    [WebMethod]
    public void DeleteAllApplications(String propName, String propValue)
    {
        var filter = "1=1";
        if (propName.IsValid())
        {
            filter = String.Format("{0} = '{1}'", propName, GetSqlString(propValue));
        }
        var ids = SwisDAL.GetApplicationIDs(filter);
        if (ids != null)
        {
            DeleteApplications(ids);
        }
    }

    [WebMethod]
    public void ManageApplicationItem(string applicationCustomType, int applicationId, int itemId, bool managed)
    {
        try
        {
            VerifyUnmanageRights();

            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                businessLayer.ManageApplicationItem(applicationCustomType, applicationId, itemId, managed);
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception caught in Application.asmx in method ManageApplications", ex);
            throw;
        }
    }

    [WebMethod]
    public List<int> ManageApplications(List<int> appIds, bool managed, DateTime? unmanageFrom, DateTime? unmanageUntil)
    {
        if (unmanageFrom == null)
        {
            unmanageFrom = DateTime.UtcNow;
        }
        if (unmanageUntil == null)
        {
            unmanageUntil = unmanageInfiniteDate;
        }

        try
        {
            VerifyUnmanageRights();

            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                List<Application> apps = businessLayer.ManageApplications(appIds, managed, unmanageFrom.Value, unmanageUntil.Value);
                return apps.Select(app => app.Id).ToList();
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception caught in Application.asmx in method ManageApplications", ex);
            throw;
        }
    }

    [WebMethod]
    public List<Int32> ManageAllApplications(String propName, String propValue, Boolean managed, DateTime unmanageFrom, DateTime unmanageUntil)
    {
        var filter = "1=1";
        if (propName.IsValid())
        {
            filter = String.Format("{0} = '{1}'", propName, GetSqlString(propValue));
        }
        var ids = SwisDAL.GetApplicationIDs(String.Format("ISNULL(a.Unmanaged, 'False')  = '{0}' AND {1}", managed, filter));
        if (ids != null)
        {
            return ManageApplications(ids, managed, unmanageFrom, unmanageUntil);
        }
        return new List<Int32>();
    }

    [WebMethod]
    public IEnumerable<ManageApplicationInfo> GetAllApplicationsIdsAndUris(string property, string value)
    {
        DataTable data = SwisDAL.GetApplicationIDsAndUris(GetWhereClause("", property, value));

        return data.Select()
            .Select(row => new ManageApplicationInfo(row.Field<int>("ID"), row.Field<string>("EntityUri")));
    }

    [WebMethod]
    public void PollApplications(List<int> appIds)
    {
        try
        {
            VerifyRights();

            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                businessLayer.PollApplications(appIds);
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception caught in Application.asmx in method PollApplications", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public List<string[]> GetVendorIconFileNames(bool includeApmNodesOnly)
    {
        Dictionary<string, string> icons = SwisDAL.GetVendorIconFileNames(includeApmNodesOnly);
        List<string[]> list = new List<string[]>(icons.Count);

        foreach (KeyValuePair<string, string> icon in icons)
        {
            list.Add(new string[] { icon.Key, icon.Value.TrimEnd() });
        }

        return list;
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetApplicationsForTemplate(int templateId)
    {
        string format = @"
SELECT A.ApplicationID, A.Name as AppName, ISNULL(C.Availability, 0) as AppStatus, A.NodeID, N.Caption as NodeName, N.Status
FROM (SELECT ApplicationID, NodeID, Name, ApplicationTemplateID FROM Orion.APM.Application) A
INNER JOIN Orion.Nodes N ON A.NodeID = N.NodeID
LEFT JOIN Orion.APM.CurrentApplicationStatus C ON A.ApplicationID = C.ApplicationID
WHERE A.ApplicationTemplateID = {0}        
";

        string swql = String.Format(format, templateId);
        return SwisProxy.ExecuteQuery(swql);
    }

    [WebMethod(EnableSession = true)]
    public String GetApplicationGroupByValues(String groupBy)
    {
        const string Query =
            @"SELECT DISTINCT
                {0} AS OrderColumn,
                '' AS Name, 
                a.Name AS AppName, 
                n.Caption AS NodeName, 
                a.ApplicationID AS ID, 
                ISNULL(cas.Availability, 0) AS EntityStatus, 
                ISNULL(n.StatusLED, 'Unknown.gif') AS StatusLED 
            FROM Orion.APM.Application AS a 
            JOIN Orion.Nodes AS n ON 
                n.NodeID = a.NodeID 
            LEFT JOIN Orion.APM.CurrentApplicationStatus AS cas ON 
                cas.ApplicationID = a.ApplicationID 
            LEFT JOIN Orion.APM.ApplicationTemplate AS at ON 
                at.ApplicationTemplateID = a.ApplicationTemplateID 
            WHERE at.CustomApplicationType IS NULL
            {1}";
        var query = String.Format(Query, groupBy,
                                  groupBy.IndexOf('.') > 0 ? "ORDER BY " + groupBy : String.Empty);
        var result = SwisProxy.ExecuteQuery(query);
        foreach (DataRow row in result.Rows)
        {
            var img = String.Format(
                @"<img class=""apm_StatusIcon"" src=""/Orion/images/StatusIcons/Small-{0}""/> ", row["StatusLED"]
            );
            row["Name"] = String.Format(Resources.APMWebContent.APMWEBCODE_VB1_85, row["AppName"], img, row["NodeName"]);
        }
        result.Columns.Remove("AppName");
        result.Columns.Remove("NodeName");
        result.AcceptChanges();

        return this.BuildEntityGroupJSON(result, true);
    }

    [WebMethod(EnableSession = true)]
    public String GetApplicationTemplateGroupByValues(String groupBy)
    {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder
            .AppendLine("SELECT DISTINCT")
            .AppendFormat("	{0} AS OrderColumn,", groupBy).AppendLine()
            .AppendLine("	at.Name AS Name,")
            .AppendLine("	at.ApplicationTemplateID AS ID,")
            .AppendLine("	0 AS EntityStatus")
            .AppendLine("FROM Orion.APM.ApplicationTemplate AS at")
            .AppendLine("LEFT JOIN Orion.APM.Tag AS t ON")
            .AppendLine("	t.TemplateID = at.ApplicationTemplateID")
            .AppendLine("WHERE")
            .AppendLine("	at.IsMockTemplate <> 1 AND at.CustomApplicationType IS NULL");

        if (groupBy.IndexOf('.') > 0)
        {
            queryBuilder
                .AppendLine("ORDER BY")
                .AppendFormat("	{0}", groupBy)
                .AppendLine();
        }
        return this.BuildEntityGroupJSON(SwisProxy.ExecuteQuery(queryBuilder.ToString()), false);
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true)]
    public String IsAutoAssignCompleted()
    {
        var info = new SolarWinds.APM.Web.UI.AutoAssign.AutoAssignUtility(false);
        var result = new {IsCompleted = info.IsCompleted, Msg = info.Text};
        return JsHelper.Serialize(result);
    }

    private static Dictionary<string, Type> standardPropertiesTypes = new Dictionary<string, Type>()
                {
                    {"at.Name", typeof(string)},
                    {"n.Caption", typeof(string)},
                    {"n.Vendor", typeof(string)},
                    {"n.MachineType", typeof(string)},
                    {"n.Contact", typeof(string)},
                    {"n.Location", typeof(string)},
                    {"n.ObjectSubType", typeof(string)},
                    {"n.SysObjectID", typeof(string)},
                    {"e.ServerName", typeof(string)},
                    {"c.Name", typeof(string)}
                };

    private static Type GetPropertyType(string propertyName)
    {
        if (standardPropertiesTypes.ContainsKey(propertyName))
            return standardPropertiesTypes[propertyName];

        Type propertyType = SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetTypeForProp("Nodes", propertyName);
        if (propertyType == null)
            propertyType = SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetTypeForProp("APM_ApplicationCustomProperties", propertyName);

        return propertyType;
    }

    private static string GetWhereClause(string prefix, string property, string value)
    {
        if (String.IsNullOrEmpty(property)) { return "1=1"; }

        string whereClause;

        if (String.IsNullOrEmpty(value) || value == "NULL")
        {
            Type propertyType = GetPropertyType(property);
            if (propertyType == typeof(string))
            {
                whereClause = String.Format("ISNULL({0}{1}, '') = ''", prefix, property);
            }
            else
            {
                whereClause = String.Format("{0}{1} IS NULL", prefix, property);
            }
        }
        else
        {
            whereClause = String.Format("{0}{1} = '{2}'", prefix, property, value);
        }

        return whereClause;
    }

    private static long GetRowCount(string property, string value)
    {
        const string swql = @"
            SELECT count(a.ApplicationID) as theCount
            FROM (SELECT ApplicationID, NodeID, ApplicationTemplateID, PrimaryGroupID FROM Orion.APM.Application) a 
            JOIN Orion.Nodes n ON a.NodeID = n.NodeId 
            JOIN Orion.Engines e ON n.EngineID = e.EngineID
            JOIN Orion.APM.ApplicationTemplate at ON a.ApplicationTemplateID = at.ApplicationTemplateID
            LEFT JOIN Orion.Container c ON c.ContainerID = a.PrimaryGroupID
            LEFT JOIN Orion.APM.ApplicationCustomProperties acup ON acup.ApplicationId = a.ApplicationID
            WHERE {0}";

        DataTable rowCountTable = SwisProxy.ExecuteQuery(String.Format(swql, GetWhereClause("", property, value).Replace("a.CustomProperties.", "acup.")));

        if (rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private String BuildEntityGroupJSON(DataTable data, Boolean isApp)
    {
        foreach (DataRow row in data.Rows)
        {
            if (row["OrderColumn"] == DBNull.Value || row["OrderColumn"].ToString().Trim().Length < 1)
            {
                row["OrderColumn"] = "Unknown";
            }
        }
        String TPL_ICON = "/Orion/APM/images/Admin/icon_application_template.gif";

        var itemsBuilder = new StringBuilder("[");
        for (Int32 i = 0, count = data.Rows.Count; i < count; )
        {
            DataRow[] groupRows = data.Select(String.Format("OrderColumn = '{0}'", GetSqlString(data.Rows[i]["OrderColumn"])));

            var items =
                groupRows.Select(
                    row =>
                    new
                    {
                        id = DbToString(row["ID"]),
                        text = DbToString(row["Name"]),
                        icon =
                        isApp
                            ? new ApmStatus((Status) Convert.ToInt32(row["EntityStatus"])).ToString(
                                "smallappimgpath", null)
                            : TPL_ICON
                    });

            var sumStatus = groupRows.Select(row => new ApmStatus((Status)Convert.ToInt32(row["EntityStatus"])));

            String sumIcon = (isApp ? ApmStatus.Summarize(sumStatus, false).ToString("smallappimgpath", null) : TPL_ICON);
            var jsonRow = new { text = GetLocalizedProperty("GroupByResult", DbToString(data.Rows[i]["OrderColumn"]).ToString()), icon = sumIcon, children = items };
            itemsBuilder
                .Append(JsHelper.Serialize(jsonRow))
                .Append(",");

            i += groupRows.Length;
        }
        if (itemsBuilder.Length > 2)
        {
            itemsBuilder.Remove(itemsBuilder.Length - 1, 1);
        }
        itemsBuilder.Append("]");

        return itemsBuilder.ToString();
    }

    private Object DbToString(object val)
    {
        return (val == null || val == DBNull.Value) ? string.Empty : val;
    }

    private Object GetStringValue(System.Web.Script.Serialization.JavaScriptSerializer serializer, object val)
    {
        object obj = ((val == null || val == DBNull.Value) ? String.Empty : val);
        string js = serializer.Serialize(obj.ToString());
        return js;
    }


    private void PreparePagedApplications(DataTable data)
    {
        if (data != null && data.Rows.Count > 0)
        {
            Func<Object, String> cast = (
                date => ((DateTime)date).ToLocalTime().ToString(CultureInfo.CurrentCulture)
            );


            var viewIdToViewName = new Dictionary<int, string>();

            foreach (DataRow row in data.Rows)
            {
                //Created Column			
                Object record = row["CreatedDate"];
                if (!(record is DBNull))
                {
                    row["Created"] = cast(record);
                }

                //LastModified Column			
                record = row["LastModifiedDate"];
                if (!(record is DBNull))
                {
                    row["LastModified"] = cast(record);
                }

                var netObjectId = ApmApplication.GetNetObjectId((int)row["ApplicationID"], row.IsNull("CustomApplicationType") ? null : (string)row["CustomApplicationType"]);
                row["NetObject"] = netObjectId;

                //CustomView Column			
                Int32 viewID = Convert.ToInt32(row["ViewID"]);
                string viewTitle;
                if (viewID > 0)
                {
                    ViewInfo vi = ViewManager.GetViewById(viewID);
                    viewTitle = vi.ViewTitle;
                }
                else
                {
                    bool hasImportedView = Convert.ToBoolean(row["HasImportedView"]);
                    if (hasImportedView)
                    {
                        if (!viewIdToViewName.TryGetValue(viewID, out viewTitle))
                        {

                            var viewSpec = ApmViewManager.DefaultViewSpec(
                                viewID,
                                hasImportedView,
                                (string)row["TemplateName"],
                                System.Xml.Linq.XElement.Parse((string)row["ViewXml"]),
                                false);
                            // The name of the Template view could be localizable. But for now its not
                            viewTitle = viewSpec.Value;
                            if (viewID > 0)
                            {
                                viewIdToViewName[viewID] = viewTitle;
                            }
                        }
                    }
                    else
                    {
                        viewTitle = APMWebContent.APMWEBDATA_TM0_24;// "Default Application Details View";
                    }
                }
                if (!String.IsNullOrEmpty(netObjectId))
                {
                    var url = BaseResourceControl.GetViewLink(netObjectId);
                    row["CustomView"] = String.Format("<a href='{0}' target='_blank'>{1}</a>", url, HttpUtility.HtmlEncode(viewTitle));
                }
                else
                {
                    row["CustomView"] = String.Empty;
                }
                // FB246837 | override component counts per sales request
                string customAppType = Convert.ToString(row["CustomApplicationType"]);
                if (!string.IsNullOrEmpty(customAppType)  && !LicenseHelper.HasValidNodeBasedLicense())
                {
                    var count = WebPluginManager.Instance.GetApplicationLicenseConsumption(customAppType);
                    if (count > 0)
                        row["ComponentsCount"] = count;
                }
            }

            data.Columns.Remove("CreatedDate");
            data.Columns.Remove("LastModifiedDate");
            data.Columns.Remove("ViewID");
            data.Columns.Remove("ApplicationTemplateID");
            data.Columns.Remove("HasImportedView");
            data.AcceptChanges();
        }
    }

    private String GetSqlString(Object val)
    {
        if(val == null)
        {
            return String.Empty;
        }
        return val.ToString().Replace("'", "''");
    }


    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    private List<int> GetApplicationInGroupsIds(IList<int> applicationIds)
    {
        const string swql =
@"SELECT ApplicationID
FROM Orion.APM.Application
WHERE
ApplicationID IN ({0}) AND
PrimaryGroupID IS NOT NULL";

        var appIds = string.Join(",", applicationIds);

        var result = SwisProxy.ExecuteQuery(String.Format(swql, appIds));

        return result.Rows.Cast<DataRow>().Select(r => r.GetValue<int>("ApplicationID")).ToList();
    }
}