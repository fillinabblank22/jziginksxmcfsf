﻿<%@ WebService Language="C#" Class="ComponentTest" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Common;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ComponentTest : System.Web.Services.WebService
{
	public class CTR 
	{
		public Int64 ID { get; set; }
		public String Name { get; set; }
		public String Message { get; set; }
		public String Status { get; set; }

		[System.Web.Script.Serialization.ScriptIgnore]
		public ComponentTestResult TestResult { get; set; }
	}
	
    [WebMethod(EnableSession = true)]
    public Guid[] GetFinishedJobIDs(Guid[] jobIDs)
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            return businessLayer.GetFinishedJobIDs(jobIDs);
        }
    }

	[WebMethod(EnableSession = true)]
	public List<CTR> StartTestResult(String key, Int32 nodeID, String nodeName, Boolean isAppMode, Int32 credID, Int64[] cmpIDs)
	{
		if (OrionConfiguration.IsDemoServer)
		{
			throw new ApplicationException("Testing Components is not supported on the Demo server");
		}

        // Cache to save ApplicationTemplateID and Use64Bit flag
		var cachedSettings = new Dictionary<int, ApplicationSettings>();
	    cachedSettings.Add(0, new ApplicationSettings { Mode = ApmProbeMode.Test }); // For case when ApplicationTemplateId is not set

        // The list of pairs (ComponentTemplate object, ApplicationSettings object)
		var cmps = new List<System.Web.UI.Pair>();
		using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			foreach(var cmpID in cmpIDs)
			{
				int id;
				ComponentBase cmp;
				if (isAppMode)
				{
					var component = bl.GetComponent(cmpID);
					id = component.Application.TemplateId;
					
					cmp = component;
				}
				else 
				{
					var template = bl.GetComponentTemplate(cmpID);
					id = template.ApplicationTemplateId;
					
					cmp = template;
				}

			    var settings = cachedSettings[0];
                if (!cachedSettings.ContainsKey(id))
                {
                    try
                    {
                        ApplicationTemplate appTemplate = bl.GetApplicationTemplate(id);
                        settings = new ApplicationSettings
                            {
                                Mode = ApmProbeMode.Test,
                                ExecutePollingMethod = appTemplate.ExecutePollingMethod,
                                Use64Bit = appTemplate.Use64Bit
                            };
                    }
                    catch
                    {
                        // Do nothing (this should be rare case)
                    }
                    cachedSettings.Add(id, settings);
                }

                cmps.Add(new System.Web.UI.Pair(cmp, cachedSettings[id]));
			}
		}

		if (credID != ComponentBase.TemplateCredentialsId) 
		{
            cmps.ForEach(item => (item.First as ComponentBase).CredentialSetId = credID);
		}
		
		var ress = new List<CTR>();
		foreach (var pair in cmps) 
		{
            var cmp = (ComponentBase)pair.First;
            var settings = (ApplicationSettings)pair.Second;
            
			var res = new ComponentTestResult(nodeID, nodeName, cmp.Type);
		    res.StartTest(cmp, settings, default(CredentialSet));

			var escapedComponentName = HttpUtility.HtmlEncode(cmp.Name);
			var ctr = new CTR { ID = cmp.Id, Name = escapedComponentName, TestResult = res };
			if (res.Skipped) 
			{
				ctr.Status = Status.IsDisabled.ToString();
				ctr.Message = String.Format(@"<img src=""/Orion/APM/Images/Admin/skipped_16x16.gif""/> {0}", Resources.APMWebContent.APMWEBCODE_AK1_102);  
			}
			ress.Add(ctr);
		}
		Session[key] = ress;

		return ress;
	}

    [WebMethod(EnableSession = true)]
	public List<CTR> LoadTestResult(String key)
    {
		if (OrionConfiguration.IsDemoServer)
		{
			throw new ApplicationException("Testing Components is not supported on the Demo server");
		}
		if (Session[key] == null) 
		{
			return null;
		}
		var ress = Session[key] as List<CTR>;
		if (ress == null) 
		{
			Session[key] = ress = new List<CTR>();
		}

		if (ress.Exists(item => item.TestResult.TestComplete == false)) 
		{
			using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				var ids = bl.GetFinishedJobIDs(ress.Select(item => item.TestResult.TestId).ToArray());
				foreach (var res in ress.Where(item => ids.Contains(item.TestResult.TestId)))
				{
					res.TestResult.LoadResults();
					
					this.PrepareTestResult(res);
				}
			}
		}
		return ress;
    }
	
	[WebMethod(EnableSession = true)]
	public void ClearTestResult(String key) 
	{
		Session.Remove(key);
	}

	private void PrepareTestResult(CTR ctr) 
	{
		var ds = ctr.TestResult.Result;

		var outcome = Status.Undefined;
		if (ds.Tables["Result"] != null) 
		{
			outcome = (Status)Enum.Parse(typeof(Status), ds.Tables["Result"].Rows[0]["Status"].ToString());
		}

		var messages = new List<String>();
		if (ds.Tables["Message"] != null && ds.Tables["Message"].Rows.Count > 0)
		{
			var msgs = ds.Tables["Message"].Select()
				.Select(item => (item.ItemArray.First() ?? String.Empty).ToString())
				.Where(item => item.Trim() != String.Empty);

			messages.AddRange(msgs);
		}
		if (ds.Tables["Error"] != null && ds.Tables["Error"].Rows.Count > 0)
		{
			var msgs = ds.Tables["Error"].Select()
				.Select(item => (item.ItemArray.First() ?? String.Empty).ToString())
				.Where(item => item.Trim() != String.Empty);

			messages.AddRange(msgs);
		}

	    var outcomeDesc = (new ApmStatus(outcome)).ToLocalizedString();
        
		if (outcome == Status.Available || outcome == Status.Warning || outcome == Status.Critical)
		{
			messages.Insert(0, String.Format(@"<img src=""/Orion/APM/Images/Admin/icon_check2.gif""/> {0}", Resources.APMWebContent.APMWEBCODE_AK1_103).FormatInvariant(ctr.TestResult.NodeName, outcomeDesc));
		}
		else 
		{
			messages.Insert(0, String.Format(@"<img src=""/Orion/APM/Images/Admin/failed_16x16.gif""/> {0}", Resources.APMWebContent.APMWEBCODE_AK1_104).FormatInvariant(ctr.TestResult.NodeName, outcomeDesc));
		}
		ctr.Status = outcome.ToString();
		ctr.Message = String.Join("<br/>", messages.Distinct(StringComparer.InvariantCultureIgnoreCase).ToArray());
	}
}