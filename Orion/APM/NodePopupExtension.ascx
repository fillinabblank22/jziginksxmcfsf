﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePopupExtension.ascx.cs" Inherits="Orion_APM_NodePopupExtension" %>

<tr>
	<th style="font-size:11px;"><%= Resources.APMWebContent.APMWEBDATA_TM0_63%></th>
	<td style="font-size:11px;"><%= String.Format(Resources.APMWebContent.APMWEBDATA_PS0_7, healthBar.NormalPercentage)%></td>
	<orion:InlineMultiBar runat="server" ID="healthBar"/>
</tr>