using System;
using System.Linq;
using System.Drawing;
using System.IO;
using Infragistics.WebUI.UltraWebGauge;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.APM.Common.Models;

public partial class GaugePage : System.Web.UI.Page
{
	#region Properties

	private String NetObjectPrefix
	{
		get
		{
			if (NetObject.IsValid())
			{
				return NetObject.Split(new[] { ":" }, StringSplitOptions.RemoveEmptyEntries)[0];
			}
			return String.Empty;
		}
	}

	private Int64 NetObjectID
	{
		get
		{
			if (NetObject.IsValid())
			{
				var parts = NetObject.Split(new[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
				if (parts.Length == 2)
				{
					Int64 id;
					Int64.TryParse(parts[1], out id);
					return id;
				}
			}
			return 0;
		}
	}

	private String NetObject
	{
		get { return Request.QueryString["NetObject"]; }
	}

	#endregion

	protected void Page_Load(object sender, EventArgs e)
	{
		var style = Request.QueryString["Style"];
		var gaugeName = Request.QueryString["GaugeName"];
		var gaugeType = Request.QueryString["GaugeType"];

		var units = Request.QueryString["Units"];
		var unitsLabel = Request.QueryString["UnitsLabel"];

		var statisticName = Request.QueryString["StatisticValueName"];

		int scale;
		long max, min, tickCount, unitsVal;
		float warningThreshold, errorThreshold;
		bool reverseThreshold;

		if (!float.TryParse(Request.QueryString["WarningThreshold"], out warningThreshold))
		{
			warningThreshold = 0;
		}

		if (!float.TryParse(Request.QueryString["ErrorThreshold"], out errorThreshold))
		{
			errorThreshold = 0;
		}

		if (!bool.TryParse(Request.QueryString["ReverseThreshold"], out reverseThreshold))
		{
			reverseThreshold = false;
		}

		if (!long.TryParse(Request.QueryString["Min"], out min))
		{
			min = 0;
		}

		if (!long.TryParse(Request.QueryString["Max"], out max))
		{
			max = 100;
		}

		if (!long.TryParse(Request.QueryString["TickCount"], out tickCount))
		{
			tickCount = 10;
		}

		if (!int.TryParse(Request.QueryString["Scale"], out scale))
		{
			scale = 100;
		}

		if (string.IsNullOrEmpty(style))
		{
			style = "Elegant Black";
		}

		double value = 0;
        GetDataByName(gaugeName, Request.QueryString["Period"], statisticName, ref value, ref warningThreshold, ref errorThreshold, ref reverseThreshold);


		BaseGaugeGenerator generator;
		if (gaugeType.Equals("Linear"))
		{
			generator = new LinearGaugeGenerator();
		}
		else
		{
			generator = new GaugeGenerator();
		}

		if (long.TryParse(units, out unitsVal) && unitsVal > 0)
		{
			value = Math.Round(value / unitsVal, 3);
			warningThreshold = warningThreshold / unitsVal;
			errorThreshold = errorThreshold / unitsVal;
		}
		string valueFormatString = value.ToString();
		if (unitsLabel.IsValid())
		{
			valueFormatString = String.Format("{0} {1}", value, unitsLabel);
		}

		UltraGauge gauge = generator.GenerateGauge(scale, style, value, min, max, valueFormatString.ToLower(),
			Request.QueryString["Property"], warningThreshold, errorThreshold, reverseThreshold);

		using (var stream = new MemoryStream())
		using (var image = gauge.GetImage(Infragistics.UltraGauge.Resources.GaugeImageType.Png, new Size((int)(gauge.Width.Value), (int)(gauge.Height.Value + 15))))
		{
			image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
			stream.WriteTo(Response.OutputStream);
		}
		Response.ContentType = "image/png";
		Response.End();
	}

    private void GetDataByName(string gaugeName, string period, string statisticName, ref double value, ref  float warningThreshold, ref float errorThreshold, ref bool reverseThreshold)
	{
		switch (NetObjectPrefix.Trim().ToUpperInvariant())
		{
			case "AM":
				GetDataForMonitorGauge(gaugeName, period, ref value, ref warningThreshold, ref errorThreshold, ref reverseThreshold, statisticName);
				break;
		}
	}

    private bool IsReverseThresholdValue(ThresholdOperator op)
    {
        return op == ThresholdOperator.Less || op == ThresholdOperator.LessOrEqual;
    }

	private void GetDataForMonitorGauge(string gaugeName, string period, ref  double value, ref float warningThreshold, ref float errorThreshold, ref bool reverseThreshold, string statisticName)
	{
		var apmMonitor = new ApmMonitor(NetObject);

		var name = gaugeName.Trim().ToUpperInvariant();
		switch (name)
		{
			case "AVAILABILITY":
				using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
				{
					Boolean isCustom = false;
					Int32 sSize = 0; String sName = String.Empty;
					DateTime startDate = DateTime.MinValue, endDate = DateTime.MinValue;
					Periods.Parse(ref period, ref startDate, ref endDate, ref sSize, ref sName, ref isCustom);

					var ds = bl.GetComponentAvailabilityHistory(
						NetObjectID, startDate, endDate, TimeSpan.FromMinutes(sSize)
					);
					if (ds.Tables[0].Rows.Count > 0)
					{
						Double.TryParse(ds.Tables[0].Compute("AVG(PercentAvailable)", null).ToString(), out value);
						value = Math.Round(value, 3);
					}
				}
				warningThreshold = 90;
				errorThreshold = 80;
				break;
			case "PERCENTCPU":
				value = Math.Round(apmMonitor.GetStatisticValue("PercentCPU"), 2);
				warningThreshold = (float)apmMonitor.BaseComponent.CpuThreshold.WarnLevel;
				errorThreshold = (float)apmMonitor.BaseComponent.CpuThreshold.CriticalLevel;
                reverseThreshold = IsReverseThresholdValue(apmMonitor.BaseComponent.CpuThreshold.ThresholdOperator);
				break;
			case "PERCENTMEMORY":
				value = Math.Round(apmMonitor.GetStatisticValue("PercentMemory"), 2);				
				warningThreshold = (float)apmMonitor.BaseComponent.PhysicalMemoryThreshold.WarnLevel;
				errorThreshold = (float)apmMonitor.BaseComponent.PhysicalMemoryThreshold.CriticalLevel;
                reverseThreshold = IsReverseThresholdValue(apmMonitor.BaseComponent.PhysicalMemoryThreshold.ThresholdOperator);				
				break;
			case "PERCENTVIRTUAL":
				value = apmMonitor.GetStatisticValue("PercentVirtual");
				warningThreshold = (float)apmMonitor.BaseComponent.VirtualMemoryThreshold.WarnLevel;
				errorThreshold = (float)apmMonitor.BaseComponent.VirtualMemoryThreshold.CriticalLevel;
                reverseThreshold = IsReverseThresholdValue(apmMonitor.BaseComponent.VirtualMemoryThreshold.ThresholdOperator);				
				break;
			case "STATISTICDATA":
				value = apmMonitor.GetStatisticValue("StatisticData");
				warningThreshold = (float)apmMonitor.BaseComponent.StatisticDataThreshold.WarnLevel;
				errorThreshold = (float)apmMonitor.BaseComponent.StatisticDataThreshold.CriticalLevel;
		        reverseThreshold = IsReverseThresholdValue(apmMonitor.BaseComponent.StatisticDataThreshold.ThresholdOperator);
				break;
			case "RESPONSETIME":
				value = apmMonitor.GetStatisticValue("ResponseTime");
				warningThreshold = (float)apmMonitor.BaseComponent.ResponseTimeThreshold.WarnLevel;
				errorThreshold = (float)apmMonitor.BaseComponent.ResponseTimeThreshold.CriticalLevel;
                reverseThreshold = IsReverseThresholdValue(apmMonitor.BaseComponent.ResponseTimeThreshold.ThresholdOperator);				
				break;
			case "DYNAMICSTATISTICDATAMIN":
			case "DYNAMICSTATISTICDATAAVG":
			case "DYNAMICSTATISTICDATAMAX":
				var evidance = apmMonitor.CurrentEvidence
					.FirstOrDefault(item => item is DynamicEvidence) as DynamicEvidence;
				if (evidance != null)
				{
					var column = evidance.Columns
						.FirstOrDefault(item => item.Type == DynamicEvidenceColumnDataType.Numeric && item.Name.Equals(statisticName, StringComparison.InvariantCultureIgnoreCase));
					if (column != null)
					{
						value = column.Cells.Average(item => item.NumericData);
						if (column.Threshold == null)
						{
							warningThreshold = errorThreshold = (float)Threshold.DefaultLevel(ThresholdOperator.Greater);
						}
						else
						{
							warningThreshold = (float)column.Threshold.WarnLevel;
							errorThreshold = (float)column.Threshold.CriticalLevel;
                            reverseThreshold = IsReverseThresholdValue(column.Threshold.ThresholdOperator);
                        }
					}
				}
				break;
		}
		if (value == long.MinValue || value == double.MinValue)
		{
			value = 0;
		}
	}
}
