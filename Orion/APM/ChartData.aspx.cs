using System;
using System.Data;
using System.IO;
using System.Web.UI;
using SolarWinds.Orion.Common;

public partial class Orion_APM_ChartData : Page
{
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Response.AddHeader("Content-Disposition", "attachment; filename=ChartData.csv");
            Response.ContentType = "application/octet-stream";

            // DataTableToCSV doesn't handle conveting DateTime columns to 
            // Local Time, so we need to force the values to be in Local Time.
            DataTable data = CustomChart.ChartData.Tables[0];
            ConvertDateTimeColumnsToLocalTime(data);

            using (StreamWriter writer = new StreamWriter(Response.OutputStream))
                DatabaseFunctions.DataTableToCSV(data, writer);

            Response.End();
        }
    }

    private static void ConvertDateTimeColumnsToLocalTime(DataTable data)
    {
        // The DataTable is set to have a datetime values set as UTC.  This means
        // any datetimes we set that are local will be converted back to UTC.
        // So, we need to create a new datetime that has its Kind set to unspecified.

        foreach (DataColumn column in data.Columns)
        {
            if (column.DataType == typeof(DateTime))
            {
                foreach (DataRow row in data.Rows)
                {
                    DateTime dt = (DateTime)row[column];
                    DateTime local = dt.ToLocalTime();
                    row[column] = new DateTime(local.Ticks, DateTimeKind.Unspecified);
                }
            }
        }        
    }

    
}
