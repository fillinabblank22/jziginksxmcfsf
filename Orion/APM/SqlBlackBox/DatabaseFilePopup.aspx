﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DatabaseFilePopup.aspx.cs" Inherits="Orion_APM_SqlBlackBox_DatabaseFilePopup" MasterPageFile="~/Orion/APM/Popup.master" %>
<%@ Import namespace="SolarWinds.APM.Common.Extensions.System" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>
<%@ Import namespace="Resources" %>
<asp:Content runat="server" ContentPlaceHolderID="NetObjectTipBody">
    <%-- JEL: sorry for the inline CSS implementation, but net object tooltip implementation from Core 
              doesn't allow to embed scripts and styles (probably because of performance) --%>
    <div id="barArea" style="position:relative; width:270px; height:115px; margin:8px 3px 12px 3px;">
        <%-- "used space pointer" will be aligned absolutely to the left ([PointersThickness]px thick, text 5px from the pointer edge) --%>
        <div style="position:absolute; left:<%= PositionUsedSpace %>px; top:0; width:<%= PointersThickness %>px; height:50px; background-color:<%= UsedSpaceColor %>;"></div>
        <div style="position:absolute; left:<%= PositionUsedSpace+5 %>px; top:0; font-size: 11px;"><%= APM_SQLBBContent.DatabaseFile_SpaceUsedByData.FormatInvariant(FormatSpaceUsedByDataText(NetObjectData.UsedSpace.ToInvariantString())) %></div>
        <%-- "white space pointer" position must be calculated, it depends if it's in left or right side of the tooltip:
               - if pointer is more on left, the text should be aligned on the right side from the pointer, thus we calculate position from left
               - if pointer is more on right, the text should be aligned on the left side from the pointer, thus we calculate position from right 
            (the pointer will be [PointersThickness]px thick and the text will be aligned 5px from the pointer edge) --%>
        <div style="position:absolute; <%= IsFilePointerOnTheLeft ? "left" : "right" %>:<%= PositionWhiteSpace %>px; top:23px; width:<%= PointersThickness %>px; height:27px; background-color:#808080;"></div>
        <div style="position:absolute; <%= IsFilePointerOnTheLeft ? "left" : "right" %>:<%= PositionWhiteSpace+5 %>px; top:23px; font-size:11px;"><%= APM_SQLBBContent.DatabaseFile_WhiteSpace.FormatInvariant(NetObjectData.WhiteSpace.ToInvariantString()) %></div>
        <div id="percentageBar" style="position:absolute; left:0; top:52px">
            <span style="float:right; width:270px; height:24px; background-image:url('/Orion/APM/Images/TooltipBar.Background.gif'); background-position:bottom; background-repeat:repeat-x; background-color:#eae9e7;">
                <span style="float:left; width:<%= UsedSpaceWidth %>px; height:24px; background-image:url('<%= UsedSpaceImgUrl %>'); background-position:bottom; background-repeat:repeat-x; background-color:<%= UsedSpaceColor %>;" />
                <span style="float:left; width:<%= WhiteSpaceWidth %>px; height:24px; background-image:url('/Orion/APM/Images/TooltipBar.DarkGray.gif'); background-position:bottom; background-repeat:repeat-x; background-color:#606060;" />
            </span>
        </div>
        <%-- "available autogrow space pointer" will be aligned absolutely to the right ([PointersThickness]px thick, text 5px from the pointer edge) --%>
        <div style="position:absolute; right:<%= PositionAutoGrow %>px; top:79px; background-color:#b5b6b5; width:<%= PointersThickness %>px; height:27px;"></div>
        <div style="position:absolute; right:<%= PositionAutoGrow+5 %>px; top:92px; font-size:11px;"><%= APM_SQLBBContent.DatabaseFile_AvailableAutoGrow.FormatInvariant(FormatAvailableAutoGrowText(NetObjectData.AvailableAutoGrow != null ? NetObjectData.AvailableAutoGrow.ToInvariantString() : APM_SQLBBContent.DatabaseBySizeFile_VolumeUsageNotAvailable)) %></div>
    </div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="NetObjectTipFooter">
    <div style="font-size:11px; border-top:#b5b6b5 solid 1px; padding:7px 7px 7px 7px;"><%= APM_SQLBBContent.DatabaseFile_AvailableVolumeSpace.FormatInvariant(FormatFreeVolumeSpaceText(NetObjectData.VolumeSpaceAvailable != null ? new ApmBytes(NetObjectData.VolumeSpaceAvailable).ToInvariantString() : APM_SQLBBContent.DatabaseBySizeFile_VolumeUsageNotAvailable)) %></div>
</asp:Content>