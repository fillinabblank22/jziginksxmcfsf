﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.BlackBox.Sql.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using System;

public partial class Orion_APM_SqlBlackBoxDatabaseDetails : SqlBlackBoxOrionView, ISqlBlackBoxDatabaseProvider, ITimePeriodProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.resHost.SqlBlackBoxDatabase = this.SqlBlackBoxDatabase;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {         
        // title
        this.title.ViewTitle = this.ViewInfo.ViewTitle;
        this.title.ViewSubTitle = this.SqlBlackBoxDatabase.Name;

        // status icon
        this.title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.Database, this.SqlBlackBoxDatabase.StatusId);

        // customize
        if (this.Profile.AllowCustomize)
            this.topRightLinks.CustomizeViewHref = this.CustomizeViewHref;

        // edit
        if (ApmRoleAccessor.AllowAdmin)
        {
            this.topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationItemPageUrl(this.SqlBlackBoxApplication.Id, this.SqlBlackBoxDatabase.Id);
            this.topRightLinks.EditNetObjectText = Resources.APM_SQLBBContent.EditApplicationItemTitle;
	    }

        // help
        this.topRightLinks.HelpUrlFragment = "OrionSAMAGSQLBBDDView";
    }

    protected string CustomizeViewHref
    {
        get { return HtmlHelper.GetDefaultCustomizeViewHref(ViewInfo.ViewID); }
    }

    public override string ViewKey
    {
        get { return "Sql BlackBox Database Details"; }
    }

    public override string ViewType
    {
        get { return "Sql BlackBox Database Details"; }
    }

    #region ISqlBlacBoxDatabaseProvider, ISqlBlackBoxApplicationProvider, INodeProvider members

    public SqlBlackBoxDatabase SqlBlackBoxDatabase
    {
        get { return (SqlBlackBoxDatabase)this.NetObject; }
    }

    public override SqlBlackBoxApplication SqlBlackBoxApplication
    {
        get { return this.SqlBlackBoxDatabase.SqlBlackBoxApplication; }
    }

	public int ApplicationItemId
	{
		get
		{
			return this.SqlBlackBoxDatabase.Database.Id;
		}
	}

    #endregion

    #region ITimePeriodProvider

    public DateTime? TimePeriodStartDate
    {
        get
        {
            return this.TimePeriodPicker.PeriodFilter.GetStartDate().Value;
        }
    }

    public DateTime? TimePeriodEndDate
    {
        get
        {
            return this.TimePeriodPicker.PeriodFilter.GetEndDate().GetValueOrDefault(DateTime.Now);
        }
    }

    public DateTime? TimePeriodStartDateUTC
    {
        get
        {
            return this.TimePeriodPicker.PeriodFilter.GetStartDateUTC().Value;
        }
    }

    public DateTime? TimePeriodEndDateUTC
    {
        get
        {
            return this.TimePeriodPicker.PeriodFilter.GetEndDateUTC().GetValueOrDefault(DateTime.UtcNow);
        }
    }

    public int GetRelativeTimePeriodMinutes()
    {
        return this.TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutes();
    }

    public int GetRelativeTimePeriodMinutesUTC()
    {
        return this.TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutesUTC();
    }

    public Boolean TimePeriodIsSetByUser()
    {
        return this.TimePeriodPicker.PeriodFilter.IsSetInQueryString(this.Request);
    }

    #endregion

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}
