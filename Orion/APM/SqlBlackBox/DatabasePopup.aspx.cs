﻿using System;
using System.Globalization;
using System.Linq;
using Resources;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_SqlBlackBox_DatabasePopup : PopupPage<SqlBlackBoxDatabase>
{
    protected const int TopComponentsCount = 5;
    private readonly Log log = new Log();

    protected SqlBlackBoxDatabase SqlDb
    {
        get { return NetObjectData; }
    }

    protected SqlBlackBoxApplication SqlApp
    {
        get { return (SqlBlackBoxApplication)SqlDb.Parent; }
    }

    private long SqlBlackBoxDatabaseID = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        var popup = (Orion_APM_Popup) this.Master;

        if (popup == null)
        {
            throw new ArgumentNullException("Master");
        }

        popup.PopupTitle = APM_SQLBBContent.DatabaseTooltip_Title;
        popup.StatusCssClass = this.StatusCssClass;

        this.AppStatusIcon.StatusValue = this.SqlApp.Status;
        this.AppStatusIcon.CustomApplicationType = this.SqlApp.CustomType;
        this.DbStatusIcon.StatusValue = this.GetStatus();
        this.DbStatusIcon.CustomApplicationType = this.SqlApp.CustomType;
        this.ServerStatusIcon.StatusValue = this.SqlApp.NPMNode.Status;

        this.CPULoad.Value = this.SqlApp.NPMNode.CPULoad;
        this.phCpuLoad.Visible = (this.CPULoad.Value != -2);
        SetupBarPercentage(
            this.CPULoadBar,
            this.SqlApp.NPMNode.CPULoad,
            100,
            Thresholds.CPULoadWarning.SettingValue,
            Thresholds.CPULoadError.SettingValue);

        this.MemoryUsed.Value = (short)this.SqlApp.NPMNode.PercentMemoryUsed;
        this.phMemoryUsed.Visible = (this.MemoryUsed.Value != -2);
        SetupBarPercentage(
            this.MemoryUsedBar,
            this.SqlApp.NPMNode.PercentMemoryUsed,
            100,
            Thresholds.PercentMemoryWarning.SettingValue,
            Thresholds.PercentMemoryError.SettingValue);

        this.componentList.BindComponents(
            this.SqlApp.ComponentsWithProblems.Where(x => x.ApplicationItemID == this.SqlBlackBoxDatabaseID),
            TopComponentsCount,
            GetStatusImagePath
            );
    }

    #region Overriden methods

    protected override ApmStatus GetStatus()
    {
        return new ApmStatus(this.NetObjectData.StatusId.OrionApmStatus());
    }

    protected override SqlBlackBoxDatabase CreateNetObjectData(string netObjectId)
    {
        var result = this.CreateNetObject() as SqlBlackBoxDatabase;

        if (result == null)
        {
            this.log.ErrorFormat(CultureInfo.InvariantCulture,
                "Cannot render database popup, because cannot get database info from netObject with netObjectID '{0}'",
                netObjectId);
        }
        else
        {
            this.SqlBlackBoxDatabaseID = result.Id;
        }
        return result;
    }

    #endregion

    protected string GetStatusImagePath(Component component)
    {
        var status = new ApmStatus(component.Status);
        return status.ToString("smallimgpath", SqlApp.CustomType);
    }
}
