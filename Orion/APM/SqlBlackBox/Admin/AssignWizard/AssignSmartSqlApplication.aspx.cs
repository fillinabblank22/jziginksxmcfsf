﻿using System.Linq;

using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.Models;
using SolarWinds.APM.BlackBox.Sql.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.Services;
using SolarWinds.APM.BlackBox.Sql.Web.DAL;

public partial class Orion_APM_Admin_SmartSqlApplication_AssignSmartSqlApplication : WizardPage<AssignSmartSqlApplicationWorkflow>, IPostBackEventHandler
{
    private readonly Log log = new Log();
    private static readonly Log slog = new Log();

    protected string TemplateName
    {
        get
        {
            return Workflow.SelectedTemplate.Name;
        }
    }

    private SelectNodeTreeItem selectedNode;
    private SelectNodeTreeItem SelectedNode
    {
        get
        {
            if (selectedNode == null)
            {
                var node = new SelectNodeTreeItem(this.serverIP.SelectedNode);
                var selectedCredSetId = this.selectCredentials.SelectedCredentialSetId;
                if (selectedCredSetId != ComponentBase.NewCredentialsId)
                {
                    node.CredentialId = selectedCredSetId;
                }

                selectedNode = node;
            }

            return selectedNode;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Workflow.SelectedNodesInfo != null && Workflow.SelectedNodesInfo.Count > 0)
            {
                if (Workflow.SelectedNodesInfo.Count > 1)
                    log.DebugFormat("Found more than one nodes in workflow state, using just first, because the wizard can assign only one at a time, ignoring the rest.");
                var nodeInfo  = Workflow.SelectedNodesInfo[0];
                serverIP.RestoreSelectedNodeById(nodeInfo.Id);
            }
            this.instanceName.Text = Workflow.SqlServerInstance;
            if (Workflow.PortNumber > 0)
            {
                this.portType.SelectedValue = "Static";
                this.portNumber.Value = Workflow.PortNumber;
            }
            else 
            {
                this.portType.SelectedValue = "Default";
            }
            portType.Attributes["onChange"] = "ChangePortType();";
            selectCredentials.ReloadCredentialSets();
        }
        selectCredentials.ValidationSummaryParentClientID = ValidationSummary2.ClientID;

        if (!IsPostBack)
        {
            string postData = Server.UrlDecode(Request.Form["postData"]);
            if (postData != null)
            {
                Workflow.HasStarted = true;

                var serializer = new JavaScriptSerializer();
                var templateId = serializer.Deserialize<int>(postData);
                Workflow.SelectedTemplate = new TemplateInfo(templateId);
            }
			else 
			{
				// This page is only valid if we came from the application template page.  We'll quit this
				// "wizard" and go back to the app template page
				Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
			}
			imgbNext.AddEnterHandler(0);
        }
    }

    protected void OnNext()
    {
        HideTestInfo();
        this.Validate(this.PageValidationSummary.ValidationGroup);
        this.Validate(selectCredentials.ValidationGroupName);

        if (Page.IsValid && selectCredentials.IsValid)
        {
            BlackBoxSqlTestConnectionResult testResult;
            if (this.TestBlackBoxSqlConnection(out testResult))
            {
                this.CreateApplication(testResult);
                GotoNextPage();
            }
            else
            {
                this.AddErrorMessage(Resources.APM_SQLBBContent.SqlBbWeb_ValidationMessage_InvalidSqlCredentials, this.selectCredentials.ValidationGroupName);
                this.SetUiTestedCredentialControls(null);
            }
        }
    }

    /// <summary>
    /// Adds error message within validator messages
    /// </summary>
    private void AddErrorMessage(string message, string validationGroupName)
    {
        var err = new CustomValidator
                      {
                          ValidationGroup = validationGroupName,
                          IsValid = false,
                          ErrorMessage = message
                      };
        Page.Validators.Add(err);
    }

    /// <summary>
    /// Tests Sql connection using filled values and returns true when the test was successful.
    /// </summary>
    /// <returns></returns>
    private bool TestBlackBoxSqlConnection()
    {
        BlackBoxSqlTestConnectionResult testResult;
        return this.TestBlackBoxSqlConnection(out testResult);
    }

    /// <summary>
    /// Tests Sql connection using filled values and returns true when the test was successful.
    /// </summary>
    /// <param name="testResult">Out parameter contains details about credentials connection test</param>
    /// <returns></returns>
    private bool TestBlackBoxSqlConnection(out BlackBoxSqlTestConnectionResult testResult)
    {
        var retval = false;
        testResult = null;
        
        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            var appSettings = new BlackBoxSqlSettings(
                this.instanceName.Text, SelectedNode.CredentialId, SettingLevel.None, this.portNumber.Value ?? 0);

            //invoke test connection one time job
            BusinessLayerTaskInfo<string> result = null;
            try
            {
                result = bl.TestBlackBoxConnection(
                    this.SelectedNode.Id,
                    this.selectCredentials.SelectedCredentialSet,
                    ApplicationSettings.DefaultForBlackBoxAssignTemplateWizards,
                    SqlBlackBoxConstants.SqlBlackBoxApplicationTemplateCustomType,
                    appSettings.ToXmlString());
            }
            catch (Exception)
            {
                // should be already logged by proxy
                this.AddErrorMessage("Error while testing SQL BB connection", this.selectCredentials.ValidationGroupName);
            }

            if (result != null)
            {
                if (result.Outcome == Status.Available && result.Result != null)
                {
                    testResult = BlackBoxSqlTestConnectionResult.FromXmlString(result.Result);
                    retval = testResult != null && (testResult.IsValidForSql || testResult.IsValidForSqlWindows);
                }
                else
                    foreach (var error in result.Errors)
                        this.AddErrorMessage(error.Message, this.selectCredentials.ValidationGroupName);
            }
        }

        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static bool InstanceExists(int nodeId, string ipAddress, string instanceName)
    {
        Node node = GetNode(nodeId, ipAddress);

        if (node == null)
            return false;

        string thisInstance = string.IsNullOrWhiteSpace(instanceName)
                                  ? SqlBlackBoxConstants.DefaultInstanceName
                                  : instanceName;

        return ServiceLocatorForWeb.GetServiceForWeb<ISqlBlackBoxApplicationDal>()
                                   .ApplicationExists(node.Id, thisInstance);
    }

    /// <summary>
    /// Creates Sql BB application after the connection test using filled values.
    /// </summary>
    /// <param name="testResult"></param>
    private void CreateApplication(BlackBoxSqlTestConnectionResult testResult)
    {
        Workflow.SelectedNodesInfo = new List<SelectNodeTreeItem> { SelectedNode };
        Workflow.SqlServerInstance = this.instanceName.Text;
        Workflow.PortNumber = this.portNumber.Value ?? 0;
        Workflow.TestConnectionResult = testResult;

        if (this.selectCredentials.SelectedCredentialSetId == ComponentBase.NewCredentialsId)
        {
            Workflow.UseCredentials(this.selectCredentials.SelectedCredentialSet);
        }
        else if (this.selectCredentials.SelectedCredentialSetId == ComponentBase.NodeWmiCredentialsId)
        {
            Workflow.InheritCredentials();
        }
        else
        {
            Workflow.UseCredentials(new List<SelectNodeTreeItem> { SelectedNode });
        }

        Workflow.CreateApplications();
    }

    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected override void CancelWizard()
    {
        base.CancelWizard();
        Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx", true);
    }

    /// <summary>
    /// Triggered when user presses Test button.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnTest(object sender, EventArgs e)
    {
        bool? testSucceeded = null;
        HideTestInfo();

        // without these validations we're not able to call test credentials (i.e. wrong IP address specified)
        this.Validate(this.PageValidationSummary.ValidationGroup);
        if (!Page.IsValid)
        {
            /* the test button is called within the UpdatePanel, but
             * the PageValidationSummary is out of the UpdatePanel
             * so any invalid validators outside the UpdatePanel are not displayed
             *   --> dirty hack: we have to move validators from one group to another to be displayed
             *   --> TODO: we should use pure javascript validation instead of UpdatePanel
             *       we cannot include SelectServerIp control into UpdatePanel, because the control will stop working
             *       the best way seems to me to use pure javascript instead of UpdatePanels
             */
            this.MoveInvalidValidators(this.PageValidationSummary.ValidationGroup, this.ValidationSummary2.ValidationGroup);
        }
        else
        {
            this.Validate(selectCredentials.ValidationGroupName);
            if (this.selectCredentials.IsValid)
            {
                testSucceeded = TestBlackBoxSqlConnection();
            }
        }

        this.SetUiTestedCredentialControls(testSucceeded);
    }

    private void MoveInvalidValidators(string sourceValidationGroup, string targetValidationGroup)
    {
        var invalidValidators = this.GetValidators(sourceValidationGroup)
                                    .OfType<IValidator>()
                                    .Where(v => !v.IsValid);
        foreach (var val in invalidValidators)
        {
            Page.Validators.Remove(val);
            this.AddErrorMessage(val.ErrorMessage, targetValidationGroup);
        }
    }

    /// <summary>
    /// Sets credential controls according to connection test result.
    /// </summary>
    /// <param name="testSucceeded"></param>
    private void SetUiTestedCredentialControls(bool? testSucceeded)
    {
        if (testSucceeded.HasValue)
        {
            testSuccessful.Visible = testSucceeded.Value;
            testFailed.Visible = !testSucceeded.Value;
        }

        selectCredentials.PreFillPasswords();
    }

    private void HideTestInfo()
    {
        testSuccessful.Visible = false;
        testFailed.Visible = false;
    }

    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Next":
                OnNext();
                break;
        }
    }
}
