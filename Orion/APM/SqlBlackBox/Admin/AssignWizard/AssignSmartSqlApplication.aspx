﻿<%@ Page Title="Assign Application Monitor" Language="C#" MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="AssignSmartSqlApplication.aspx.cs" Inherits="Orion_APM_Admin_SmartSqlApplication_AssignSmartSqlApplication" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="apm" TagName="CredsTip" Src="~/Orion/APM/Controls/CredentialTips.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectServerIp" Src="~/Orion/APM/Controls/SelectServerIp.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectPortNumber" Src="~/Orion/APM/Controls/SelectPortNumber.ascx" %>
<%@ Register TagPrefix="apm" TagName="ImageTooltip" Src="~/Orion/APM/Controls/ImageTooltip.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/Styles/AssignWizard.css"/>
    <orion:Include ID="Include1" runat="server" File="Nodes/js/TimeoutHandling.js" />
    
     <script type="text/javascript">
         function HideTestResults() {
             $(".testSuccessful").hide();
             $(".testFailed").hide();
         }

         function TestDuplicateInstance() {
             $.ajax({
                 type: "POST",
                 url: "AssignSmartSqlApplication.aspx/InstanceExists",
                 data: Ext.encode({ nodeId: $get('<%= serverIP.ClientID %>' + '_selectedNodeId').value, ipAddress: $get('<%= serverIP.ClientID %>' + '_targetServer').value, instanceName: $get('<%= instanceName.ClientID %>').value }),
                 contentType: 'application/json; charset=utf-8',
                 dataType: "json",
                 success: function (dataResponse) {
                     if (!dataResponse.d) {
                         Ext.MessageBox.hide();
                         <%= Page.ClientScript.GetPostBackEventReference(this, "Next") %>;
                     } else {
                         Ext.Msg.show({
                             msg: "<%=Resources.APM_SQLBBContent.AssignWizard_DuplicateFound%>",
                             buttons: Ext.Msg.YESNO,
                             fn: function (btn) {
                                 if (btn == 'yes') {
                                     <%= Page.ClientScript.GetPostBackEventReference(this, "Next") %>;
                                 }
                             }
                         });
                     }
                 },
                 error: function (response) {
                     Ext.Msg.alert("Error checking duplicates", response.statusText);
                 }
             });
         }

         function NextClick() {
             if (IsDemoMode()) return DemoModeMessage();
             HideTestResults();
             TestDuplicateInstance();
             return false;
         }

         function ChangePortType() {
             if ($('#<%= portType.ClientID %>').val() === 'Default') {
                 $('#portNumberLine').hide();
                 $('#portNumber').val('');
             }
             else {
                 $('#portNumberLine').show();
             }
         }

         $(document).ready(function () {
             ChangePortType();
         });
    </script>

    <h2><%=  string.Format(Resources.APM_SQLBBContent.AssignWizard_MonitorTitle, this.TemplateName)%></h2>
    <p><%= string.Format(Resources.APM_SQLBBContent.AssignWizard_MonitorDescription, this.TemplateName)%></p>
    <br />
    <p class="note">
        <%= Resources.APMWebContent.APMWEBDATA_VB1_242 %>
        <a id="addLink" href="/Orion/Nodes/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_243 %></a>
    </p>
    
        <div id="selectSqlInstanceStep">
            <ul id="selectSqlInstanceForm">
                <li>
                    <asp:ValidationSummary ID="PageValidationSummary" runat="server" />
                </li>
                <li>
                    <label><%=  Resources.APMWebContent.APMWEBDATA_AK1_117 %></label>
                        <apm:SelectServerIp ID="serverIP" runat="server" ValidateVmware="false" CssClass="apm_Target" />
                </li>
                <li>
                    <label><%=  Resources.APM_SQLBBContent.EditApplicationSettings_InstanceName %></label>
                    <asp:TextBox runat="server" ID="instanceName"></asp:TextBox>
                    <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_SQLBBContent, AssignWizard_InstanceNameTooltip %>" />
                </li>
                <li>
                    <label><%=  Resources.APM_SQLBBContent.EditApplicationSettings_SqlServerPortTyle %></label>
                    <asp:DropDownList id="portType" runat="server" AutoPostBack="false">
                        <asp:ListItem Value="Default" Text="<%$ Resources: APM_SQLBBContent, EditApplicationSettings_SqlServerPortType_Default %>" />
                        <asp:ListItem Value="Static" Text="<%$ Resources: APM_SQLBBContent, EditApplicationSettings_SqlServerPortType_Static %>" />
                    </asp:DropDownList>
                </li>
                <li id="portNumberLine">
                    <label><%=  Resources.APM_SQLBBContent.EditApplicationSettings_PortNumber %></label>
                    <apm:SelectPortNumber runat="server" ID="portNumber" CssClass="intInput" />
                    <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_SQLBBContent, AssignWizard_PortNumberTooltip %>" />
                </li>
                <li>
                    <label><%=  Resources.APM_SQLBBContent.EditApplicationSettings_InstanceCredentials %></label>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional" Class="credentialsContainer ie7addLayout">
                        <ContentTemplate>            
                        <table class="selectCreds">
                            <%--TODO: add testing credentials capability (extend SelectCredentials.ascx)--%>
                            <apm:SelectCredentials runat="server" ID="selectCredentials" 
                                ValidationGroupName="SelectCredentialsValidationGroup" 
                                AllowNodeWmiCredential="True" />
                            <tr>
                                <td colspan="2" class="testRow">
                                    <div runat="server" id="testSuccessful" class="testSuccessful" visible="false">
                                        <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_55 %>" />
                                    </div>
                                    <div runat="server" id="testFailed" class="testFailed" visible="false">
                                        <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_56 %>" />
                                    </div>
                                    <asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="true" DisplayAfter="0">
                                        <ProgressTemplate>
                                            <img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources: APMWebContent, ApmWeb_TestingProgress %>" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="testButtonRow">
                                    <orion:LocalizableButton runat="server" ID="TestButton" LocalizedText="Test" DisplayType="Small" OnClick="OnTest" OnClientClick="HideTestResults()" />
                                </td>
                            </tr>
                            <tr><td colspan="2">
                                <%--TODO: move validation summary out of the table (needs some css styling)--%>
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SelectCredentialsValidationGroup"/>
                            </td></tr>
                        </table>
                        <%--TODO: add support for check duplicates (SQL instance duplicates on one node) --%>
                        <%--<apm:CheckDuplicates ID="ctrCheckDuplicates" runat="server" />--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </li>
            </ul>
            <div class="credentialTips">
                <%-- TODO: move BackgroundColor="#FFFDCC" BorderColor="#EACA7F" to css file --%>
                <apm:CredsTip ID="ctrCredsTip" BackgroundColor="#FFFDCC" BorderColor="#EACA7F" runat="server"/>  
            </div>
        </div>
        <div style="clear: both;"></div>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APM_SQLBBContent, AssignWizard_ApplicationMonitorButton %>" DisplayType="Primary" OnClientClick="NextClick();return false;" />
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false" />
    </div>   

</asp:Content>

