﻿using System;

using SolarWinds.APM.BlackBox.Sql.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_SmartSqlApplication_Finish : WizardPage<AssignSmartSqlApplicationWorkflow>
{
    protected string TemplateName
    {
        get
        {
            return Workflow.SelectedTemplate.Name;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        finishedMessage.DataSource = Workflow.CreatedApplications;
        finishedMessage.DataBind();

        if (!IsPostBack) 
            imgbDone.AddEnterHandler(0);
    }

    protected void OnDone(object sender, EventArgs e)
    {
        this.ResetSession();
        Response.Redirect("~/Orion/Apm/Summary.aspx", true);
    }

}