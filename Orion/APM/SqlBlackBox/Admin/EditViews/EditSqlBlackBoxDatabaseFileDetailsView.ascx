﻿<%@ Control Language="C#" ClassName="EditSqlBlackBoxDatabaseFileDetailsView" Inherits="SolarWinds.Orion.Web.UI.ProfilePropEditUserControl"%>
<%@ Register Src="~/Orion/Controls/SelectViewForViewType.ascx" TagPrefix="orion" TagName="SelectView" %>

<script runat="server">
	public override string PropertyValue
	{
		get { return ViewSelector.PropertyValue; }
		set { ViewSelector.PropertyValue = value; }
	}
</script>

<orion:SelectView runat="server" ID="ViewSelector" AllowViewsByDeviceType="false" ViewType="Sql BlackBox Database File Details" />

