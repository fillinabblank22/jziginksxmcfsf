﻿<%@ WebService Language="C#" Class="SqlBlackBoxChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.Models;
using SolarWinds.APM.BlackBox.Sql.Web.DAL;
using SolarWinds.APM.BlackBox.Sql.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using Charting = SolarWinds.APM.Web.Charting;
using ApmChartDataHelper = SolarWinds.APM.Web.Charting.ChartDataHelper;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class SqlBlackBoxChartData : ChartDataWebService
{

    private SqlBlackBoxChartingDal dal;
    private SqlBlackBoxChartingDal ChartDal
    {
        get
        {
            if (dal == null)
            {
                dal = new SqlBlackBoxChartingDal();
            }
            return dal;
        }
    }

    [WebMethod]
    public Charting.ChartDataResults GeTopXxDatabasesByActiveUserConnections(ChartDataRequest request)
    {
        var resource = ResourceManager.GetResourceByID((int)request.AllSettings["ResourceID"]);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        Int32 objectId;
        DataTable data = null;
        if (request.NetObjectIds.Length > 0)
        {
            if (Int32.TryParse(request.NetObjectIds[0], out objectId))
            {
                data = ChartDal.GetTopXxDatabasesByActiveUserConnections(objectId, 10);
            }
        }

        var dataSeriesColection = new List<Charting.DataSeries>();
        dataSeriesColection.Add(new Charting.DataSeries()
        {
            TagName = "Default"
        });

        if (data == null || data.Rows.Count == 0)
        {
            return new Charting.ChartDataResults(dataSeriesColection);
        }

        var rows = data.Rows.Cast<DataRow>().ToList();
        // sort by user count desc + database name asc
        rows.Sort((x, y) =>
            {
                var xCount = Convert.ToInt32(x["UsersCount"]);
                var yCount = Convert.ToInt32(y["UsersCount"]);

                var retval = xCount.CompareTo(yCount) * -1; // reversion
                if (retval == 0)
                {
                    var xName = (string)x["Name"];
                    var yName = (string)y["Name"];

                    retval = String.Compare(xName, yName, StringComparison.InvariantCultureIgnoreCase);
                }

                return retval;
            });
        
        foreach (var row in rows)
        {
            dataSeriesColection[0].Data.Add(new Charting.DataPoint((string)row["Name"],
                                                                   new Dictionary<string, object> {
                                                                           {"y", row["UsersCount"]},
                                                                           {"count", row["UsersCount"]},
                                                                           {"status", row["Status"]},
                                                                           {"applicationId", row["ApplicationID"]},
                                                                           {"databaseId", row["DatabaseID"]},
                                                                           {"netObjectId", request.NetObjectIds[0]},
                                                                           {"uniqueId", this.ComputeUniqueResourceId(resource.ID, Convert.ToInt32(row["DatabaseID"]))},
                                                                           {"netObject", request.AllSettings["NetObject"]}
                                                                       }));
        }

        return new Charting.ChartDataResults(dataSeriesColection);
    }

    private int ComputeUniqueResourceId(int baseId, int keyValue)
    {
        var numOfDigits = baseId.ToString(CultureInfo.InvariantCulture).Length;
        var retval = keyValue * (int)Math.Pow(10, numOfDigits) + baseId;
        
        return retval;
    }

    [WebMethod]
    public ChartDataResults GetDatabaseAvailabilityDataSeries(ChartDataRequest request)
    {
        DateRange dateRange = ApmChartDataHelper.SetDateRange(request);

        int objectId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out objectId);
        }
        if (objectId == 0)
        {
            throw new ArgumentException("No Object ID is defined in request.");
        }

        DataTable dt = ChartDal.GetDatabaseAvailability(objectId, dateRange.StartDate, dateRange.EndDate);

        var sampledSeries = GetDatabaseAvailabilitySeries(dt, request, true);

        return sampledSeries.CreateDynamicResult(request);
    }


    private static IEnumerable<DataSeries> GetDatabaseAvailabilitySeries(DataTable availabilityDataTable, ChartDataRequest request, bool generateNavigatorSeries)
    {
        var dateRange = ApmChartDataHelper.SetDateRange(request);
        var series = new Dictionary<DatabaseCombinedStatus, DataSeries>()
        {
            {DatabaseCombinedStatus.Unknown, new DataSeries(){TagName = "Unknown_Count"}},
            {DatabaseCombinedStatus.Copying, new DataSeries(){TagName = "Copying_Count"}},
            {DatabaseCombinedStatus.Restoring, new DataSeries(){TagName = "Restoring_Count"}},
            {DatabaseCombinedStatus.Recovering, new DataSeries(){TagName = "Recovering_Count"}},
            {DatabaseCombinedStatus.Recovery_Pending, new DataSeries(){TagName = "Recovery_Pending_Count"}},
            {DatabaseCombinedStatus.Suspect, new DataSeries(){TagName = "Suspect_Count"}},
            {DatabaseCombinedStatus.Emergency, new DataSeries(){TagName = "Emergency_Count"}},
            {DatabaseCombinedStatus.Offline, new DataSeries(){TagName = "Offline_Count"}},
            {DatabaseCombinedStatus.Critical, new DataSeries(){TagName = "Critical_Count"}},
            {DatabaseCombinedStatus.Warning, new DataSeries(){TagName = "Warning_Count"}},
            {DatabaseCombinedStatus.Up, new DataSeries(){TagName = "Up_Count"}}
        };

        foreach (DataRow row in availabilityDataTable.Rows)
        {
            var time = row.Field<DateTime>("DateTime");
            var status = row.Field<DatabaseCombinedStatus>("CombinedStatus");
            var key = series.ContainsKey(status) ? status : DatabaseCombinedStatus.Unknown;
            series[key].AddPoint(DataPoint.CreatePoint(time, 1));
        }

        var sampledSeries = series.Values.ResampleSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Total).ToList();

        if (generateNavigatorSeries)
        {
            var navigatorSeries = Charting.ChartDataHelper.GetAvailabilitySerie(sampledSeries, 
                new[] { "Up_Count", "Warning_Count", "Critical_Count" },
                new[] { "Unknown_Count", "Restoring_Count", "Recovering_Count", "Recovery_Pending_Count", "Suspect_Count", "Emergency_Count", "Offline_Count", "Copying_Count", });

            sampledSeries.Add(navigatorSeries);
        }

        return sampledSeries;
    }

    [WebMethod]
    public ChartDataResults GetDatabaseFileSizeDataSeries(ChartDataRequest request)
    {
        var objectId = ParseNetObjectId(request);
        if (objectId.Prefix != SqlBlackBoxConstants.SqlBlackBoxDatabaseFilePrefix)
            throw new ArgumentException("Wrong prefix in net object ID " + request.NetObjectIds[0] + ", expecting " + SqlBlackBoxConstants.SqlBlackBoxDatabaseFilePrefix);

        var dateRange = ApmChartDataHelper.SetDateRange(request);
        
        var filesStatistics = SqlBlackBoxDatabaseFileDAL.DefaultProxy.GetDatabaseFileSizeChartStatistics((int)objectId.Id, dateRange);

        var sampledSeries = filesStatistics.Values.ResampleSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Min);

        return sampledSeries.CreateDynamicResult(request);
    }

    #region General helper methods

    public static NetObjectHelper ParseNetObjectId(ChartDataRequest request)
    {
        if (request == null)
            throw new ArgumentNullException("request");

        if (request.NetObjectIds == null || request.NetObjectIds.Length == 0)
            throw new ArgumentException("No net object ID is defined in request");

        NetObjectHelper objectId;
        if (!NetObjectHelper.TryCreate(request.NetObjectIds[0], out objectId))
            throw new ArgumentException("Cannot parse net object ID from " + request.NetObjectIds[0]);

        return objectId;
    }

    #endregion

    [WebMethod]
    public string GetActiveUserConnectionsTableControl(string applicationId, string databaseId, string parentNetObject, string uniqueId)
    {
        var retval = String.Empty;
        
        using (var page = new Page())
        {
            var connectionsTable = page.LoadControl("~/Orion/APM/SqlBlackBox/Controls/ActiveUserConnectionsTable.ascx") as IDatabaseResourceControl;
            if (connectionsTable != null)
            {
                connectionsTable.ApplicationId = int.Parse(applicationId, CultureInfo.InvariantCulture);
                connectionsTable.DatabaseId = int.Parse(databaseId, CultureInfo.InvariantCulture);
                connectionsTable.ParentNetObject = parentNetObject;
                connectionsTable.UniqueId = int.Parse(uniqueId, CultureInfo.InvariantCulture);

                var tempForm = new HtmlForm();
                tempForm.Controls.Add(new ScriptManager());
                tempForm.Controls.Add((Control)connectionsTable);
                page.Controls.Add(tempForm);

                using (var writer = new StringWriter())
                {
                    HttpContext.Current.Server.Execute(page, writer, false);
                    retval = writer.ToString();
                }
            }
        }

        return retval;
    }
}
