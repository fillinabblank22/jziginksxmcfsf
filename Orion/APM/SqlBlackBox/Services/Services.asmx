﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Sql.Web.Service" %>
using System.Web.Script.Services;
using System.Web.Services;

namespace SolarWinds.APM.BlackBox.Sql.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class Service : SolarWinds.APM.BlackBox.Sql.Web.Services.SqlBlackBoxWebService
    {
    }
}
