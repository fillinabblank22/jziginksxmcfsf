﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Sql.Web.UnknownCredentialsEditorService" %>

using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.BlackBox.Sql.Web.Services;
using SolarWinds.APM.Common;
using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.Models;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Services;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Core.Common;

namespace SolarWinds.APM.BlackBox.Sql.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class UnknownCredentialsEditorService : SolarWinds.APM.BlackBox.Sql.Web.Services.SqlBlackBoxWebService
    {
        [WebMethod(EnableSession = true)]
        public IList<CredentialSet> GetCredentials()
        {
            using (var bl = SolarWinds.APM.Web.ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var credentials = new List<CredentialSet>();
                foreach (var cred in bl.GetCredentials())
                {
                    cred.Password = CredentialSet.FakePassword;
                    credentials.Add(cred);
                }
                foreach (var cred in bl.GetCertificates())
                {
                    cred.Password = CredentialSet.FakePassword;
                    cred.Name = string.Format("{0} ({1})", cred.Name, Resources.APMWebContent.APMWEBDATA_RB3_1);
                    credentials.Add(cred);
                }
                credentials.Sort((x, y) => string.CompareOrdinal(x.Name, y.Name));
                return credentials;
            }
        }

        [WebMethod(EnableSession = true)]
        public int CreateCredential(string name, string userName, string psw)
        {
            VerifyRights();
            VerifyDemo();

            using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var cred = new CredentialSet();
                cred.Name = name;
                cred.UserName = userName;
                cred.Password = psw;

                return businessLayer.CreateNewCredentialSet(cred);
            }
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true)]
        public object GetSqlBbApplicationsWithoutCred(int appId)
        {
            return ServiceLocatorForWeb.GetServiceForWeb<SolarWinds.APM.BlackBox.Sql.Web.DAL.ISqlBlackBoxApplicationDal>().GetSqlBbApplicationsWithoutCred(appId);
        }

        [WebMethod]
        public TestResultInfo[] TestSqlBlackBoxConnections(TestBlackBoxSettings[] settings)
        {
            var tasks = new List<Task>();
            var items = new List<TestResultInfo>(settings.Length);
            foreach (var setting in settings)
            {
                Action doTest = () =>
                {
                    this.Log.InfoFormat("Begin run test {0} on node {1}.", setting.TestId, setting.NodeId);

                    var item = TestSqlBlackBoxConnection(setting);

                    this.Log.InfoFormat("Test {0} on node {1} completed.", setting.TestId, setting.NodeId);

                    item.TestId = setting.TestId;
                    items.Add(item);
                };
                var task = Task.Factory.StartNew(doTest);
                tasks.Add(task);
            }
            if (tasks.Count > 0)
            {
                const string BbTestTimeoutKey = "APM-Web-BlackBoxSqlTestConnection-TimeoutInSeconds";

                var timeout = 0;
                if (!int.TryParse(ConfigurationManager.AppSettings[BbTestTimeoutKey], NumberStyles.Integer, CultureInfo.InvariantCulture, out timeout) || timeout < 10)
                {
                    timeout = 30;
                }
                try
                {
                    Task.WaitAll(tasks.ToArray(), TimeSpan.FromSeconds(timeout));
                }
                catch (Exception ex)
                {
                    this.Log.Error("Test sql black box connections failed", ex);
                }
            }
            foreach (var setting in settings)
            {
                if (items.All(item => item.TestId != setting.TestId))
                {
                    this.Log.WarnFormat("Timeout occurred for test {0} on node {1}.", setting.TestId, setting.NodeId);

                    items.Add(new TestResultInfo(false) { TestId = setting.TestId });
                }
            }
            return items.ToArray();
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true)]
        public TestResultInfo ApplyCredToSqlBbApplications(CredentialSet cred, string[] info)
        {
            VerifyDemo();

            const int IndexNodeId = 0;
            const int IndexAppId = 1;
            const int IndexAppInstance = 2;
            const int IndexAppPortNumber = 3;
            const int IndexAppPortType = 4;

            var testRes = new TestResultInfo(false);
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                if (cred.Id == -1)
                {
                    cred.Id = bl.CreateNewCredentialSet(cred);
                }

                var testSett = new TestBlackBoxSettings
                {
                    AppType = SqlBlackBoxConstants.SqlBlackBoxApplicationTemplateCustomType,
                    NodeId = Convert.ToInt32(info[IndexNodeId], CultureInfo.InvariantCulture),
                    CredentialSet = new TestCredentialSet
                    {
                        Id = cred.Id,
                        Login = cred.UserName,
                        Password = cred.Password
                    },
                    CustomSettings = new Dictionary<string, string> 
					{
						{ "InstanceName", info[IndexAppInstance] },
						{ "PortNumber", info[IndexAppPortNumber] },
						{ "PortType", info[IndexAppPortType] }
					}
                };

                testRes = TestSqlBlackBoxConnections(new[] { testSett })
                    .First();
                if (testRes.TestSuccessfull)
                {
                    var result = testRes.Result as BlackBoxSqlTestConnectionResult;
                    if (result != null && (result.IsValidForSql || result.IsValidForSqlWindows))
                    {
                        var app = bl.GetApplication(Convert.ToInt32(info[IndexAppId], CultureInfo.InvariantCulture));
                        app.Settings[SqlBlackBoxConstants.SqlCredentialSetIdKey] = new SettingValue
                        {
                            Key = SqlBlackBoxConstants.SqlCredentialSetIdKey,
                            SettingLevel = SettingLevel.Instance,
                            Value = cred.Id.ToString(CultureInfo.InvariantCulture),
                            ValueType = SettingValueType.Integer,
                            Required = true
                        };
						app.Settings[SqlBlackBoxConstants.WindowsAuthenticationKey] = new SettingValue
						{
							Key = SqlBlackBoxConstants.WindowsAuthenticationKey,
							SettingLevel = SettingLevel.Instance,
							Value = result.IsValidForSqlWindows.ToString(CultureInfo.InvariantCulture),
							ValueType = SettingValueType.Boolean,
							Required = true
						};
						app.Settings[SqlBlackBoxConstants.PortTypeKey] = new SettingValue
						{
							Key = SqlBlackBoxConstants.PortTypeKey,
							SettingLevel = SettingLevel.Instance,
							Value = info[IndexAppPortType],
							ValueType = SettingValueType.Option,
							Required = true
						};
						app.Settings[SqlBlackBoxConstants.PortNumberKey] = new SettingValue
						{
							Key = SqlBlackBoxConstants.PortNumberKey,
							SettingLevel = SettingLevel.Instance,
							Value = info[IndexAppPortNumber],
							ValueType = SettingValueType.Integer,
							Required = true
						};
						if (result.IsValidForWmi && app.Node.NodeSubType == NodeSubType.WMI)
                        {
                            app.Settings[SqlBlackBoxConstants.WmiCredentialSetIdKey] = new SettingValue
                            {
                                Key = SqlBlackBoxConstants.WmiCredentialSetIdKey,
                                SettingLevel = SettingLevel.Instance,
                                Value = cred.Id.ToString(CultureInfo.InvariantCulture),
                                ValueType = SettingValueType.Integer,
                                Required = true
                            };
                        }
                        bl.UpdateApplicationEx(app, true, true);
                    }
                }
            }
            return testRes;
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true)]
        public void DeleteSqlBbApplicationsWithoutCred(int[] appIds)
        {
            VerifyDemo();
            VerifyRights();

            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                bl.DeleteApplications(appIds);
            }
        }
    }
}