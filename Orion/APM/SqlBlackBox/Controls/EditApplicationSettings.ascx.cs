﻿using SolarWinds.APM.BlackBox.Sql.Common.Models;
using SolarWinds.Internationalization.Extensions;
using System;
using System.Web.UI.WebControls;

public partial class Orion_APM_SqlBlackBox_Controls_EditApplicationSettings : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        sqlInstanceName.Text = Resources.APM_SQLBBContent.EditApplicationSettings_InstanceName;
        defaultInstance.Text = Resources.APM_SQLBBContent.EditApplicationSettings_DefaultInstanceSelected;
        sqlPortNumber.Text = Resources.APM_SQLBBContent.EditApplicationSettings_PortNumber;
        sqlCredentialsLabel.Text = Resources.APM_SQLBBContent.EditApplicationSettings_InstanceCredentials;
        sqlCredentialsAreWindows.Text = Resources.APM_SQLBBContent.EditApplicationSettings_InstanceCredentialsType;
		sqlPortType.Text = Resources.APM_SQLBBContent.EditApplicationSettings_SqlServerPortTyle;

        // generate dropdown list content
        foreach (CredentialTypeSelection credentialType in Enum.GetValues(typeof(CredentialTypeSelection)))
        {
            sqlCredentialsType.Items.Add(new ListItem
            {
                Text = credentialType.LocalizedLabel(),
                Value = credentialType.ToString()
            });
        }
    }
}