﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveUserConnectionsTable.ascx.cs" Inherits="Orion_APM_SqlBlackBox_Controls_ActiveUserConnectionsTable" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:Include ID="Include1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
<orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/Styles/Resources.css" />

<apm:CustomQueryTable runat="server" ID="CustomTable" />

<script type="text/javascript">
    $(function () {
        SW.APM.Resources.CustomQuery.initialize(
            {
                cls: 'activeUserConnections',
                uniqueId: <%= UniqueId %>,
                initialPage: 0,
                rowsPerPage: 100,                        
                allowSort: true,
                allowSearch: false,
                headerTitles: ["<%= Resources.APM_SQLBBContent.ActiveUserConnections_Login %>","<%= Resources.APM_SQLBBContent.ActiveUserConnections_Host %>","<%= Resources.APM_SQLBBContent.ActiveUserConnections_ConnectionDuration %>","<%= Resources.APM_SQLBBContent.ActiveUserConnections_IdleTime %>","<%= Resources.APM_SQLBBContent.ActiveUserConnections_BytesTransferred %>"],
                defaultOrderBy: "[ConnectionDurationInSecs] DESC",
                underscoreTemplateId: "#contentTemplate-<%= UniqueId %>"
            }); 
        
        var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= UniqueId %>); };
        SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
        refresh();
    });

    var convertToHoursAndMinutes = function(hours, minutes) {
        var retval = (hours == 0 && minutes == 0) ? "<%= Resources.APM_SQLBBContent.ActiveUserConnections_ConnectionDuration_LessThanMinute %>" : "";

        if (hours > 0) {
        	var hourFormat = (hours > 1) ? "<%= Resources.APM_SQLBBContent.ActiveUserConnections_ConnectionDuration_HourPlural %>" : "<%= Resources.APM_SQLBBContent.ActiveUserConnections_ConnectionDuration_HourSingular %>";
        	retval += hourFormat.replace('{0}', hours);
        }

        if (minutes > 0) {
        	if (hours > 0) retval += ", ";
        	var minFormat = (minutes > 1 ? "<%= Resources.APM_SQLBBContent.ActiveUserConnections_ConnectionDuration_MinutePlural %>" : "<%= Resources.APM_SQLBBContent.ActiveUserConnections_ConnectionDuration_MinuteSingular %>");
        	retval += minFormat.replace('{0}', minutes);
        }

        return retval;
    };
</script>

<script id="contentTemplate-<%= UniqueId %>" type="text/template">
    <tr>
        <td class="column0"><a href="{{ Columns[9] }}">{{ Columns[0] }}</a></td>
        <td class="column1">{{{ Columns[1] }}}</td>
        <td class="column2">{{ convertToHoursAndMinutes(Columns[3], Columns[4]) }}</td>
        <td class="column3">{{ convertToHoursAndMinutes(Columns[6], Columns[7]) }}</td>
        <td class="column4">{{ SW.Core.Charts.dataFormatters.byte(Columns[8], 0) }}</td>
    </tr>
</script>
