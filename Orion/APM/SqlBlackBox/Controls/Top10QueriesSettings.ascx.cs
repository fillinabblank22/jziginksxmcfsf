﻿using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_APM_SqlBlackBox_Controls_Top10QueriesSettings : BaseResourceEditControl
{
    protected bool ShowOrderBy 
    {
        get
        {
            return Request.QueryString["ShowOrderBy"] == bool.TrueString;
        }
    }

    protected bool ShowMaxCount 
    {
        get
        {
            return Request.QueryString["ShowMaxCount"] == bool.TrueString;
        }
    }

    protected bool ShowFilter
    {
        get
        {
            return Request.QueryString["ShowFilter"] == bool.TrueString;
        }
    }


    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (ShowOrderBy)
        {
            ddlOrderBy.Parent.Visible = true;
            ddlOrderBy.SelectedValue = Resource.Properties["orderBy"];
        }

        if (ShowMaxCount)
        {
            tbMaxRows.Parent.Visible = true;
            tbMaxRows.Text = Resource.Properties["maxCount"] ?? "10";
        }

        if (ShowFilter)
        {
            tbFilter.Parent.Visible = true;
            tbFilter.Text = Resource.Properties["filter"];
        }
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
		    var retval = new Dictionary<string, object>();

		    if (ShowOrderBy)
		    {
                retval.Add("orderBy", ddlOrderBy.SelectedValue);
		    }

		    if (ShowFilter)
		    {
                retval.Add("filter", tbFilter.Text);
		    }

            if (ShowMaxCount)
            {
                retval.Add("maxCount", tbMaxRows.Text);
            }

		    return retval;
		}
	}
}