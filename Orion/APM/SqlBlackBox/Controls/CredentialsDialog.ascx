﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsDialog.ascx.cs" Inherits="Orion_APM_SqlBlackBox_Controls_CredentialsDialog" %>

<orion:Include ID="i1" File="APM/SqlBlackBox/js/CredentialsDialog.js" Section="Bottom" runat="server" />

<style type="text/css">
	#sqlBbCredConfirmDialog { display: none; }
	#sqlBbCredConfirmDialog .delimiter td { height: 2px !important; font-size: 2px !important; border-top: 1px solid #ccc; }
	
	#sqlBbCredDialog { display: none; }
	#sqlBbCredDialog select[id^=sqlBb] { width: 205px; }
	#sqlBbCredDialog input[id^=sqlBb] { width: 200px; }
	#sqlBbCredDialog input[type=checkbox] { margin-left: 0px; }
	#sqlBbCredDialog a { color: #369; }

	#sqlBbCredDialog #sqlBbApps { width: 100%; display: none; }
	#sqlBbCredDialog #sqlBbApps .app-rows { margin-left: 20px; max-height: 100px; overflow-y: auto; }
	#sqlBbCredDialog #sqlBbApps .app-rows .app-row { cursor: default; word-wrap: normal; }
	#sqlBbCredDialog .error td { color: #f33; height: 2px !important; font-size: 2px !important; }
	#sqlBbCredDialog .error td div { font-size: small !important; }
	#sqlBbCredDialog .delimiter td  { height: 2px !important; font-size: 2px !important; border-top: 1px solid #ccc; }
	#sqlBbCredDialog .field-notes { font-size: 8pt; color: #999; }
</style>
<script id="sql-bb-app-name" type="text/x-sw-template">
	{0} on <img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={1}&status={2}&size=small" /> {3} 
</script>
<script id="sql-bb-app-row" type="text/x-sw-template">
	<div class="app-row" app-id="{0}">
		{1} <span class="test"></span>
	</div>
</script>
<script id="sql-bb-app-test-process" type="text/x-sw-template">
	<img width="16" height="16" src="/Orion/images/animated_loading_sm3_whbg.gif"> Testing...
</script>
<script id="sql-bb-app-test-success" type="text/x-sw-template">
	<img width="16" height="16" src="/Orion/images/nodemgmt_art/icons/icon_OK.gif"> Test Successful!
</script>
<script id="sql-bb-app-test-fail" type="text/x-sw-template">
	<img width="16" height="16" src="/Orion/images/nodemgmt_art/icons/icon_warning.gif"> Test Failed.
</script>
<script id="sql-bb-app-assign-process" type="text/x-sw-template">
	<img width="16" height="16" src="/Orion/images/animated_loading_sm3_whbg.gif"> Assigning...
</script>
<script id="sql-bb-app-assign-fail" type="text/x-sw-template">
	<img width="16" height="16" src="/Orion/images/nodemgmt_art/icons/icon_warning.gif"> Assign Failed (Test not completed).
</script>

<div id="sqlBbCredConfirmDialog" title="Delete Application(s)?">
	<table>
		<colgroup>
			<col width="40px" />
			<col width="360px" />
		</colgroup>
		<tbody>
			<tr>
				<td colspan="2"><%= Resources.APM_SQLBBContent.FollowingApplication%></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><div class="app-rows"></div></td>
			</tr>
			<tr>
				<td colspan="2"><%= Resources.APM_SQLBBContent.WillBeDeletedAreYouSure%></td>
			</tr>
			<tr class="delimiter">
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td style="text-align: right;">
					<orion:LocalizableButton ID="btnConfirmOk" Text="Confirm" DisplayType="Primary" runat="server" />
					<orion:LocalizableButton ID="btnConfirmCancel" Text="Cancel" DisplayType="Secondary" runat="server" />
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div id="sqlBbCredDialog" title="Enter Credentials">
	<table>
		<colgroup>
			<col width="20px" />
			<col width="200px" />
			<col width="300px" />
			<col width="20px" />
		</colgroup>
		<thead>
			<tr>
				<td colspan="4">
                    <%= Resources.APM_SQLBBContent.MicrosoftSQLServerCredentialsAreRequiredToMonitor%> <span class="current-app-name"><%= Resources.APM_SQLBBContent.AppOnNode%></span> <br />
					<a href='http://www.solarwinds.com/NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file=OrionAPMAGUnderstandingCredentialsLibrary.htm' target='_blank'> &#0187; Help me find these credentials</a>  
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_SQLBBContent.ChooseCredentials%></td>
				<td>
					<select id="sqlBbCreds" size="1">
					</select>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_SQLBBContent.CredentialName%></td>
				<td> <input id="sqlBbCredName" type="text" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_SQLBBContent.UserName%></td>
				<td> <input id="sqlBbUserName" type="text" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_SQLBBContent.Password%></td>
				<td><input id="sqlBbPassword1" type="password" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_SQLBBContent.ConfirmPassword%></td>
				<td><input id="sqlBbPassword2" type="password" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_SQLBBContent.SQLServerPortType%></td>
				<td>
					<select id="sqlBbPortType">
						<option value="Default"><%= Resources.APM_SQLBBContent.UseDefaultPort%></option>
						<option value="Static"><%= Resources.APM_SQLBBContent.UseStaticPort%></option>
					</select>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr id="sqlBbPortNumberContainer" style="display: none;">
				<td>&nbsp;</td>
				<td><%= Resources.APM_SQLBBContent.SQLServerPortNumber%></td>
				<td>
					<input id="sqlBbPortNumber" type="text" /> 
					<div class="field-notes"><%= Resources.APM_SQLBBContent.ToUseDefaultPortLeaveThisFieldEmpty%></div>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">
					<table id="sqlBbApps">
						<tr>
							<td> <div class="app-rows"></div></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: right;"> 
					<orion:LocalizableButton ID="btnTest" Text="<%$ Resources : APMWebContent , ApmWeb_TestButton %>" DisplayType="Small" runat="server" />
				</td>
			</tr>
			<tr class="error">
				<td colspan="2">&nbsp;</td>
				<td colspan="2">
					<div></div>
				</td>
			</tr>
			<tr class="delimiter">
				<td colspan="4">&nbsp;</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="#" id="btnDelete"> &#0187; <%= Resources.APM_SQLBBContent.RemoveThisApplicationMonitor%></a>  
				</td>
				<td colspan="2" style="text-align: right;">
					<orion:LocalizableButton ID="btnAssign" Text="Assign Credential" DisplayType="Primary" runat="server" />
					<orion:LocalizableButton ID="btnCancel" Text="Cancel" DisplayType="Secondary" runat="server" />
				</td>
			</tr>
		</tfoot>
	</table>
</div>