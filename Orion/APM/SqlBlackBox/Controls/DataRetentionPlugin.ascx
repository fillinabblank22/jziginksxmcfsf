﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataRetentionPlugin.ascx.cs" Inherits="Orion_APM_SqlBlackBox_Controls_DataRetentionPlugin" %>

		<tr class="header">
			<td colspan="3"><h2><%= Resources.APM_SQLBBContent.ConfigDataRetention_Title %></h2></td>
		</tr>

		<tr>
            <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainDetachedDbDays %></td>
            <td class="Property" style="white-space: nowrap">
                <asp:TextBox ID="RetainDetachedDbDays" runat="server" Width="60px" Style="text-align: right;" MaxLength="5"></asp:TextBox>
                <%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainDetachedDbDays_Unit %>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="RetainDetachedDbDays"
                    ErrorMessage="<%$ Resources:APM_SQLBBContent,ConfigDataRetention_RetainDetachedDbDays_ErrorCompare %>"
                    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RetainDetachedDbDays"
                    ErrorMessage="<%$ Resources:APM_SQLBBContent,ConfigDataRetention_RetainDetachedDbDays_ErrorRequired %>">*</asp:RequiredFieldValidator>
            </td>
            <td class="Property"><%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainDetachedDbDays_Description %></td>
        </tr>
        <tr>
            <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainHistoryDays %></td>
            <td class="Property" style="white-space: nowrap">
                <asp:TextBox ID="RetainHistoryDays" runat="server" Width="60px" Style="text-align: right;" MaxLength="5"></asp:TextBox>
                <%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainHistoryDays_Unit %>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="RetainHistoryDays"
                    ErrorMessage="<%$ Resources:APM_SQLBBContent,ConfigDataRetention_RetainHistoryDays_ErrorCompare %>"
                    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RetainHistoryDays"
                    ErrorMessage="<%$ Resources:APM_SQLBBContent,ConfigDataRetention_RetainHistoryDays_ErrorRequired %>">*</asp:RequiredFieldValidator>
            </td>
            <td class="Property"><%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainHistoryDays_Description %></td>
        </tr>
        <tr>
            <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainDetailTablesDays %></td>
            <td class="Property" style="white-space: nowrap">
                <asp:TextBox ID="RetainDetailTablesDays" runat="server" Width="60px" Style="text-align: right;" MaxLength="5"></asp:TextBox>
                <%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainDetailTablesDays_Unit %>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="RetainDetailTablesDays"
                    ErrorMessage="<%$ Resources:APM_SQLBBContent,ConfigDataRetention_RetainDetailTablesDays_ErrorCompare %>"
                    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RetainDetailTablesDays"
                    ErrorMessage="<%$ Resources:APM_SQLBBContent,ConfigDataRetention_RetainDetailTablesDays_ErrorRequired %>">*</asp:RequiredFieldValidator>
            </td>
            <td class="Property"><%= Resources.APM_SQLBBContent.ConfigDataRetention_RetainDetailTablesDays_Description %></td>
        </tr>
