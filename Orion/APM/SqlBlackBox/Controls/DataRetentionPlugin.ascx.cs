﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.BlackBox;
using SolarWinds.APM.BlackBox.Sql.Common.Models;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;

public partial class Orion_APM_SqlBlackBox_Controls_DataRetentionPlugin : BlackBoxConfigPluginBase
{
    protected Config ConfigState
    {
        get { return this.ViewState.GetOrCreate<Config>("config_sqlbb"); }
        set { this.ViewState["config_sqlbb"] = value; }
    }

    public override void SaveData()
    {
        var config = this.ConfigState;
        config.RetainDetachedDays = ParseDays(this.RetainDetachedDbDays);
        config.RetainDetailTablesDays = ParseDays(this.RetainDetailTablesDays);
        config.RetainHistoryDays = ParseDays(this.RetainHistoryDays);
        WithBL(bl => this.ConfigState = (Config)bl.EditConfig(config));
    }

    public override void LoadData()
    {
        WithBL(bl => this.ConfigState = (Config)bl.GetConfig(this.ConfigState));
    }

    public override void RestoreDefaults()
    {
        WithBL(bl => this.ConfigState = (Config) bl.RestoreDefaultConfig(this.ConfigState));
    }

    public override void RefreshData()
    {
        var config = this.ConfigState;
        PrintDays(this.RetainDetachedDbDays, config.RetainDetachedDays);
        PrintDays(this.RetainDetailTablesDays, config.RetainDetailTablesDays);
        PrintDays(this.RetainHistoryDays, config.RetainHistoryDays);
    }
}
