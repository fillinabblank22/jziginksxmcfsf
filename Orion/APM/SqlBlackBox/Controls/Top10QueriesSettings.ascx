﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Top10QueriesSettings.ascx.cs" Inherits="Orion_APM_SqlBlackBox_Controls_Top10QueriesSettings" %>

<orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>

<p runat="server" style="padding-bottom: 0px;" Visible="False">
    <b><%= Resources.APM_SQLBBContent.Top10Queries_Edit_OrderBy %></b><br />
	<asp:DropDownList runat="server" ID="ddlOrderBy">
		<asp:ListItem Value="cpu" Text="<%$ Resources:APM_SQLBBContent, Top10Queries_CpuTime %>" />
		<asp:ListItem Value="reads" Text="<%$ Resources:APM_SQLBBContent, Top10Queries_LogicalReads %>" />
		<asp:ListItem Value="writes" Text="<%$ Resources:APM_SQLBBContent, Top10Queries_LogicalWrites %>" />
	</asp:DropDownList>
</p>

<p runat="server" Visible="False">
    <b><%= Resources.APM_SQLBBContent.Top10Queries_Edit_MaxCount %></b><br />
    <asp:TextBox runat="server" ID="tbMaxRows" Text="10" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbMaxRows"
        SetFocusOnError="true">*</asp:RequiredFieldValidator>
    <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="tbMaxRows"
        Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="1" ErrorMessage="Invalid entry"
        SetFocusOnError="true"></asp:RangeValidator>
</p>

<p runat="server" Visible="False">
    <b><%= Resources.APMWebContent.APMWEBDATA_AK1_150 %></b><br>
    <asp:TextBox runat="server" ID="tbFilter" MaxLength="200" Width="300px" /><br />
    <span class="filterNotes"><%= Resources.APM_SQLBBContent.Top10Queries_Edit_FilterNotes %><br />
    <a href="http://www.solarwinds.com/netperfmon/SolarWinds/OrionFilters_SQLExamples.htm" id="filterHelpLink" target="_blank"><%= Resources.APMWebContent.APMWEBDATA_AK1_152 %></a>.</span>
</p>
