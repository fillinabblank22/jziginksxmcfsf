﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Globalization;

public partial class Orion_APM_SqlBlackBox_Controls_ActiveUserConnectionsTable : System.Web.UI.UserControl, IDatabaseResourceControl
{
    public int DatabaseId { get; set; }

    public int ApplicationId { get; set; }

    public string ParentNetObject { get; set; }

    public int UniqueId { get; set; }

    private string LinkToExpensiveQueries
    {
        get
        {
            var info = ViewManager.GetViewByViewKey(SqlBlackBoxConstants.ViewKeyExpensiveQueries);

            var urlBuilder = new UrlBuilder(BaseResourceControl.GetViewLink(ParentNetObject));
            urlBuilder["ViewID"] = info.ViewID.ToString(CultureInfo.InvariantCulture);

            return urlBuilder.Url;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = UniqueId;

        var query = @"SELECT
                        Login,
                        IpAddress, 
                        ConnectionDurationInSecs,
                        ConnectionDurationHourPart AS _ConnectionDurationHourPart, 
                        ConnectionDurationMinutePart AS _ConnectionDurationMinutePart, 
                        IdleTimeInSecs,
                        IdleTimeHourPart AS _IdleTimeHourPart, 
                        IdleTimeMinutePart AS _IdleTimeMinutePart,
                        TransferredBytes,
                        '{1}' AS [_LinkFor_Login]
                    FROM Orion.APM.SqlConnection
                    WHERE SqlDatabaseID = {0} AND ApplicationID = {2}";

        CustomTable.SWQL = String.Format(query,
                        this.DatabaseId,
                        LinkToExpensiveQueries,
                        ApplicationId);
    }
}