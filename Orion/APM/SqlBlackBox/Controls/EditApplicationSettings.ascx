﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationSettings.ascx.cs" Inherits="Orion_APM_SqlBlackBox_Controls_EditApplicationSettings" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Common" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Common.Models" %>
<%@ Register src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" tagPrefix="apm" tagName="SelectCredentials" %>
<%@ Register src="~/Orion/APM/Controls/TestButtonSummary.ascx" tagPrefix="apm" tagName="TestButtonSummary" %>

<script type="text/javascript">
    (function() {
        var CredentialTypeSelection = {
            AutoDetect: 0,
            WindowsAuthentication: 1,
            SqlAuthentication: 2
        };

        function getTestSettingsFromUi(testingNodeId) {
            var portType = $("#appEditSqlCredentials #portType").val(), portNumber = "0";
            if (portType == "Static") {
                portNumber = $('#appEditSqlCredentials #portNumber').val();
            }

            var result = {
                NodeId: testingNodeId,
                AppType: '<%=SqlBlackBoxConstants.SqlBlackBoxApplicationTemplateCustomType%>',
                CredentialSet: {
                    Id: $('#appEditSqlCredentials table[class="selectCreds"] select[id$="CredentialSetList"]').val(),
                    Login: $('.selectCreds input[name$=UserName]').val(),
                    Password: $('.selectCreds input[name$=Password]').val()
                },
                CustomSettings: {
                    InstanceName: $('#appEditSqlCredentials #instanceName').html(),
                    PortNumber: portNumber,
                    PortType: portType
                },
                CredentialTypeSelection: CredentialTypeSelection[$("#<%=sqlCredentialsType.ClientID %>").val()]
            };

            SW.APM.EditApp.updateCommonTestSettings(result);

            return { settings: result };
        }

        Ext.namespace('SW.APM.BB.SQL');

        SW.APM.BB.SQL.CredentialsEditor = function() {
            var settingLevelInstance = 2; // SettingLevel.Instance enumeration
            var credSetIdElSelector = "#appEditSqlCredentials select[id$=CredentialSetList]";

            function findSetting(settingPropertyName, settingsCollection) {
                var foundSetting = null;
                Ext.each(settingsCollection, function(sett) {
                    if (sett.Key == settingPropertyName) {
                        foundSetting = sett;
                        return false;
                    }
                });
                return foundSetting;
            }

            function createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError) {
                var jqSelCredTable = $(".selectCreds");
                var newCredentials = {
                    name: jqSelCredTable.find("input[name$=CredentialName]").val(),
                    userName: jqSelCredTable.find("input[name$=UserName]").val(),
                    psw: jqSelCredTable.find("input[name$=Password]").val()
                };
                var onCredentialsCreated = function(credentialSetId) {
                    credentialSetIdSetting.Value = credentialSetId;
                    setTimeout(function() {
                        SW.APM.SelectCredentials.ReloadCredentialSets(credSetIdElSelector);
                    }, 500);
                    onSuccess();
                };
                SW.Core.Services.callWebService("/Orion/APM/Services/Credentials.asmx", "CreateCredential", newCredentials, onCredentialsCreated, onError);
            }

            this.initUiState = function(item) {
                // show the dialog 
                $('#appEditSqlCredentials').show();

                // and bind data

                var foundSetting = findSetting("<%= SqlBlackBoxConstants.SqlCredentialSetIdKey %>", item.Settings);
                if (foundSetting) {
                    $(credSetIdElSelector)
                        .val(foundSetting.Value)
                        .trigger('change');

                    // remember initial value
                    $('#appEditSqlCredentials').data('initialCredSetId', foundSetting.Value);
                }
                var portTypeSetting = findSetting("<%= SqlBlackBoxConstants.PortTypeKey %>", item.Settings);
                foundSetting = findSetting("PortNumber", item.Settings);
                if ((portTypeSetting && portTypeSetting.Value != 'Default') && (foundSetting && foundSetting.Value != '0')) {
                    $('#appEditSqlCredentials #portNumber').val(foundSetting.Value);
                    $('#portType').val('Static');
                } else {
                    $('#portType').val('Default');
                }

                $('#portType').change();


                foundSetting = findSetting("<%= SqlBlackBoxConstants.SelectedCredentialTypeKey %>", item.Settings);
                var selectedCredentialType = (foundSetting && foundSetting.Value != '') ? foundSetting.Value : '<%=CredentialTypeSelection.AutoDetect%>';
                $('#appEditSqlCredentials select[name="<%=sqlCredentialsType.UniqueID%>"]').val(selectedCredentialType);

                foundSetting = findSetting("InstanceName", item.Settings);
                if (foundSetting && foundSetting.Value != '')
                    $('#appEditSqlCredentials #instanceName').html(foundSetting.Value);

                var testButtonHandler = new SW.APM.BB.SQL.TestButtonHandler(item.NodeId, '<%= testSummary.ClientID %>');
                $('#<%= testButton.ClientID %>')
                    .unbind('click')
                    .click(testButtonHandler.onClick);
            };

            this.updateModelFromUi = function(item) {
                var foundSetting = findSetting("<%= SqlBlackBoxConstants.SqlCredentialSetIdKey %>", item.Settings);
                if (foundSetting) {
                    // override template level to instance
                    foundSetting.SettingLevel = settingLevelInstance;
                    foundSetting.Value = $(credSetIdElSelector).val();
                }

                foundSetting = findSetting("<%= SqlBlackBoxConstants.PortNumberKey %>", item.Settings);
                if (foundSetting) {
                    // override template level to instance
                    foundSetting.SettingLevel = settingLevelInstance;
                    foundSetting.Value = $('#appEditSqlCredentials #portNumber').val();
                    var portTypeSetting = findSetting("PortType", item.Settings);
                    if (portTypeSetting)
                        portTypeSetting.Value = foundSetting.Value != '' && foundSetting.Value != '0' ? 'Static' : 'Default';
                }

                var selectedCredentialType = $('#appEditSqlCredentials select[name="<%=sqlCredentialsType.UniqueID%>"]').val();
                foundSetting = findSetting("<%= SqlBlackBoxConstants.WindowsAuthenticationKey %>", item.Settings);
                if (foundSetting) {
                    // override template level to instance
                    foundSetting.SettingLevel = settingLevelInstance;
                    if (selectedCredentialType == '<%= CredentialTypeSelection.WindowsAuthentication %>') {
                        foundSetting.Value = 'True';
                    } else if (selectedCredentialType == '<%= CredentialTypeSelection.SqlAuthentication %>') {
                        foundSetting.Value = 'False';
                    }
                }

                foundSetting = findSetting("<%= SqlBlackBoxConstants.SelectedCredentialTypeKey %>", item.Settings);
                if (foundSetting) {
                    // override template level to instance
                    foundSetting.SettingLevel = settingLevelInstance;
                    foundSetting.Value = selectedCredentialType;
                }
            };

            this.asyncPostProcessing = function(app, template, onSuccess, onError) {
                var selectedCredentialType = $('#appEditSqlCredentials select[name="<%=sqlCredentialsType.UniqueID%>"]').val();

                // FB285050 - the connectionTestSucceeded should stay undefined
                var connectionTestSucceeded; 
                var windowsAuthenticationSetting = findSetting("<%= SqlBlackBoxConstants.WindowsAuthenticationKey %>", app.Settings);
                var credentialSetIdSetting = findSetting("<%= SqlBlackBoxConstants.SqlCredentialSetIdKey %>", app.Settings);

                if (windowsAuthenticationSetting && credentialSetIdSetting) {

                    var testSett = getTestSettingsFromUi(app.NodeId);
                    var isNewCredAssigned = testSett.settings.CredentialSet.Id == SW.APM.SelectCredentials.NewCredentialId;

                    // test connection for new creds or when auto-detect is selected
                    if (selectedCredentialType == '<%= CredentialTypeSelection.AutoDetect %>' || isNewCredAssigned || credentialSetIdSetting.Value == SW.APM.SelectCredentials.NewCredentialId) {
                        SW.Core.Services.callWebServiceSync("/Orion/APM/SqlBlackBox/Services/Services.asmx", "TestSqlBlackBoxConnection",
                            testSett,
                            function(result) {
                                if (connectionTestSucceeded = result.TestSuccessfull && (result.Result.IsValidForSql || result.Result.IsValidForSqlWindows)) {
                                    windowsAuthenticationSetting.Value = result.Result.IsValidForSqlWindows ? 'True' : 'False';
                                } else {
                                    windowsAuthenticationSetting.Value = ''; // test wasn't successful
                                }
                            },
                            function() {
                                // test failed - we can't decide whether creds are win or not
                                connectionTestSucceeded = false;
                                windowsAuthenticationSetting.Value = '';
                            }
                        );
                    }

                    // display question whether to save failing creds
                    // FB285050 - the connectionTestSucceeded is tested defined only for certain cases !!!
                    if (typeof connectionTestSucceeded != 'undefined' && !connectionTestSucceeded && (isNewCredAssigned || $('#appEditSqlCredentials').data('initialCredSetId') != credentialSetIdSetting.Value)) {

                        Ext.Msg.show({
                            title: '<%= Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Title %>',
                            msg: '<%= Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Msg %>',
                            buttons: Ext.Msg.OKCANCEL,
                            fn: function(btn) {
                                if (btn == 'ok') {
                                    if (isNewCredAssigned) {
                                        createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                                    } else {
                                        onSuccess();
                                    }
                                } else {
                                    onError();
                                }
                            },
                            animEl: 'elId',
                            icon: Ext.MessageBox.WARNING
                        });

                        return true;
                    }

                    // create new creds /*credentialSetIdSetting.Value == SW.APM.SelectCredentials.NewCredentialId*/
                    if (isNewCredAssigned) {
                        createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                        return true;
                    }
                }
                return false;
            };
        };

        Ext.ComponentMgr.registerPlugin('SW.APM.BB.SQL.CredentialsEditor', SW.APM.BB.SQL.CredentialsEditor);

        $('#portType').change(function(select) {
            if ($('#portType').val() === 'Default') {
                $('#portNumberRow').hide();
                $('#portNumber').val('');
            } else {
                $('#portNumberRow').show();
            }
        });

        //TODO: retest in IE (which versions?)
        SW.APM.BB.SQL.TestButtonHandler = function(nodeId, summaryId) {
            var customMessageCls = 'customMsg', errorHideTimeout = 0;
            var jqSummary = $('#' + summaryId);

            function validatePort(obj) {
                if (errorHideTimeout) {
                    clearTimeout(errorHideTimeout);
                }

                var errorCnt = $("#portNumberError").hide();

                var message = "", sett = obj.settings.CustomSettings;
                if (sett.PortType == "Static") {
                    var port = Number(sett.PortNumber);
                    if (isNaN(port)) {
                        message += "Incorrect port number value.";
                    } else if (port < 1 || port > 65535) {
                        message += "Port number should be in range from 1 to 65535.";
                    }
                }
                if (message.length > 0) {
                    errorCnt.show()
                        .find(".error")
                        .html(message);
                    errorHideTimeout = setTimeout(function() {
                        errorCnt.hide();
                        errorHideTimeout = 0;
                    }, 6000);
                }
                return message.length == 0;
            }

            function callTestBlackBoxConnection(settings, onSuccess, onError) {
                SW.Core.Services.callWebService("/Orion/APM/SqlBlackBox/Services/Services.asmx", "TestSqlBlackBoxConnection", settings, onSuccess, onError);
            }

            function displayTestingSummary(testSuccessfull, msg) {
                //TODO: move close to TestButtonSummary.ascx to allow reuse
                var jqCustomMessageSpan = null;
                jqSummary.find('.' + customMessageCls).remove();
                if (msg != null && msg != "")
                    jqCustomMessageSpan = $('<span />').addClass(customMessageCls).html(msg);
                if (testSuccessfull) {
                    jqSummary.find('.testSuccessful').show().append(jqCustomMessageSpan);
                    jqSummary.find('.testFailed').hide();
                    jqSummary.find('.progress').hide();
                } else {
                    jqSummary.find('.testSuccessful').hide();
                    jqSummary.find('.testFailed').show().append(jqCustomMessageSpan);
                    jqSummary.find('.progress').hide();
                }
            }

            function displayProgress() {
                jqSummary.find('.testSuccessful').hide();
                jqSummary.find('.testFailed').hide();
                if (!jqSummary.is(':visible'))
                    jqSummary.slideDown();
                jqSummary.find('.progress').show();
            }

            this.onClick = function() {
                var testSett = getTestSettingsFromUi(nodeId);
                if (validatePort(testSett) && $('#appEditSqlCredentials').closest("form").valid()) {
                    displayProgress(jqSummary);
                    callTestBlackBoxConnection(
                        testSett,
                        function(result) {
                            var testSuccessfull = result.TestSuccessfull && (result.Result.IsValidForSql || result.Result.IsValidForSqlWindows);
                            var message = null;
                            if (testSuccessfull) {
                                var selectedCredentialType = $('#appEditSqlCredentials select[name="<%=sqlCredentialsType.UniqueID%>"]').val();
                                var isAutodetectSelected = selectedCredentialType == '<%= CredentialTypeSelection.AutoDetect %>';
                                var isWindowsAuthenticationSelected = selectedCredentialType == '<%= CredentialTypeSelection.WindowsAuthentication %>';
                                var detectedAuthenticationText = result.Result.IsValidForSqlWindows ? "<%= Resources.APM_SQLBBContent.Windows_credential %>" : "<%= Resources.APM_SQLBBContent.SQL_credential %>";
                                if (isAutodetectSelected
                                    || result.Result.IsValidForSqlWindows && isWindowsAuthenticationSelected
                                    || result.Result.IsValidForSql && !isWindowsAuthenticationSelected) {
                                    message = '(' + detectedAuthenticationText + ')';
                                } else {
                                    testSuccessfull = false;
                                    message = '('+'<%= Resources.APM_SQLBBContent.Different_type_detected %>'+' <b>' + detectedAuthenticationText + '</b>)';
                                }
                            }
                            displayTestingSummary(testSuccessfull, message);
                        },
                        function() {
                            displayTestingSummary(false,  '<%= Resources.APM_SQLBBContent.Error_while_testing_connection %>');
                        }
                    );
                }
                return false;
            };
        };
    }());
</script>
<style type="text/css">
    #appEditSqlCredentials ul.sqlCredsType li {
        text-align: left;
        margin-left: 5px;
    }

    #appEditSqlCredentials ul.sqlCredsType input {
        margin-right: 10px;
    }

    #appEditSqlCredentials ul.sqlCredsType label {
        display: inline-block;
        height: 20px;
        vertical-align: top;
    }

    #appEditSqlCredentials .testButtonRow, #appEditSqlCredentials .testRow {
        text-align: center;
    }

        #appEditSqlCredentials .testRow div {
            height: 20px;
            margin-bottom: 5px;
            padding-top: 4px;
        }

            #appEditSqlCredentials .testRow div img {
                height: 16px;
                vertical-align: bottom;
                margin-right: 5px;
            }

            #appEditSqlCredentials .testRow div.testSuccessful {
                background-color: #D6F6C6;
            }

            #appEditSqlCredentials .testRow div.testFailed {
                background-color: #FEEE90;
                color: Red;
            }

    #appEditSqlCredentials #portNumberRow {
        background-color: #F0F0F0;
    }

        #appEditSqlCredentials #portNumberRow #portNumberError {
            display: none;
        }
</style>
<table id="appEditSqlCredentials" class="appProperties" style="display: none" cellspacing="0">
    <tr>
        <td class="label"><asp:Literal ID="sqlInstanceName" runat="server" /></td>
        <td><div id="instanceName"><asp:Literal runat="server" ID="defaultInstance"></asp:Literal></div></td>
    </tr>
    <tr>
        <td class="label">
            <asp:Literal ID="sqlPortType" runat="server" />
        </td>
        <td>
            <select id="portType" name="portType">
                <option value="Default"><%= Resources.APM_SQLBBContent.UseDefaultPort%></option>
                <option value="Static"><%= Resources.APM_SQLBBContent.UseStaticPort%></option>
            </select>
        </td>
    </tr>
    <tr id="portNumberRow" >
        <%--TODO: add port number javascript validation between 0 and some max port number --%>
        <td class="label"><asp:Literal ID="sqlPortNumber" runat="server" /></td>
        <td>
            <input id="portNumber" name="portNumber" class="number" maxlength="5" />

            <span id="portNumberError" class="validationMsg sw-suggestion sw-suggestion-fail">
                <span class="sw-suggestion-icon"></span>
                <span class="validationError">
                    <label class="error"></label>
                </span>
            </span>
        </td>
    </tr>
    <tr>
        <td class="label"><asp:Literal ID="sqlCredentialsLabel" runat="server" /></td>
        <td>
            <table class="selectCreds" style="width: 430px">
                <apm:SelectCredentials runat="server" ID="selectCredentials" 
                    ValidationGroupName="SelectCredentialsValidationGroup" 
                    AllowNodeWmiCredential="True" />
                <tr>
                    <td class="label"><asp:Literal ID="sqlCredentialsAreWindows" runat="server" />: </td>
                    <td><asp:DropDownList runat="server" ID="sqlCredentialsType" /></td>
                </tr>
                <tr>
                    <td colspan="2" class="testRow">
                        <apm:TestButtonSummary runat="server" ID="testSummary" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="testButtonRow">
                        <orion:LocalizableButton runat="server" ID="testButton" LocalizedText="Test" DisplayType="Small" />
                    </td>
                </tr>
                <tr><td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SelectCredentialsValidationGroup"/>
                </td></tr>
            </table>
        </td>
    </tr>
</table>
