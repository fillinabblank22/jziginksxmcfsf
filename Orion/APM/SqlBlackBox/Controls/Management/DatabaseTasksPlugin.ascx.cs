﻿using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_APM_Controls_Management_DatabaseTasksPlugin : UserControl, IManagementResourcePlugin
{
    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private const string LogMessageTemplate = "Creating management task '{0}' in section '{1}' for NetObject {2}";

    private const string ImgSrcTemplate = "<img src='{0}' class='apmNodeManagementTask' />&nbsp;{1}";

    protected const int CoreStatusUnmanaged = 9;

    public IEnumerable<ManagementTaskItem> GetManagementTasks(ApmBaseResource resource, NetObject netObject, string returnUrl)
    {
        var dbProvider = resource.GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>();
        SqlBlackBoxDatabase db = (dbProvider != null) ? dbProvider.SqlBlackBoxDatabase : null;
        if (db == null || db.SqlBlackBoxApplication == null || db.SqlBlackBoxApplication.Status == null)
        {
            yield break;
        }

        string sectionName = Resources.APM_SQLBBContent.Management_Database;
        SqlBlackBoxApplication app = db.SqlBlackBoxApplication;

        bool isApmAdmin = ApmRoleAccessor.AllowAdmin;

        if (isApmAdmin)
        {
            log.DebugFormat(LogMessageTemplate, "Edit", sectionName, db.NetObjectID);
            yield return new ManagementTaskItem
            {
                Section = sectionName,
                LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, "/Orion/Nodes/images/icons/icon_edit.gif", Resources.APMWebContent.Management_Edit),
                LinkUrl = ApmMasterPage.GetEditApplicationItemPageUrl(app.Id, db.Id)
            };
        }

        if (isApmAdmin || Profile.AllowUnmanage)
        {
            log.DebugFormat(LogMessageTemplate, "Manage", sectionName, db.NetObjectID);
            string toggleIcon, toggleText, jsOnClick;
            if (db.StatusId.OrionApmStatus() == Status.Unmanaged)
            {
                toggleIcon = "/Orion/Nodes/images/icons/icon_remanage.gif";
                toggleText = Resources.APMWebContent.APMWEBDATA_VB1_294;

                if (db.Database.ApplicationStatus == CoreStatusUnmanaged)
                {
                    jsOnClick = "return showApplicationUnmanagedMessage()";
                }
                else
                {
                    jsOnClick = InvariantString.Format("return manageApplicationItem(true, '{0}', {1}, {2}, '{3}')", db.Name, db.Id, app.Id,
                        Resources.APM_SQLBBContent.ManageDbManageProgress);
                }
            }
            else
            {
                toggleIcon = "/Orion/Nodes/images/icons/icon_manage.gif";
                toggleText = Resources.APMWebContent.APMWEBDATA_VB1_295;
                jsOnClick = InvariantString.Format("return manageApplicationItem(false, '{0}', {1}, {2}, '{3}')", db.Name, db.Id, app.Id,
                        Resources.APM_SQLBBContent.ManageDbUnmanageProgress);
            }

            yield return new ManagementTaskItem
            {
                Section = sectionName,
                LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, toggleIcon, toggleText),
                OnClick = jsOnClick,
                ClientID = "manage_toggle_image"
            };
        }
    }
}
