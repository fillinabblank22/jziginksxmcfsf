﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseTasksPlugin.ascx.cs" Inherits="Orion_APM_Controls_Management_DatabaseTasksPlugin" %>

<orion:Include ID="Include3" runat="server" File="APM/Styles/Resources.css" />

<script type="text/javascript">
    function manageApplicationItem(managed, dbName, dbId, appId, message) {
        var message = managed ? "<%=Resources.APM_SQLBBContent.ManageDbRemanageQuestion%>" : "<%=Resources.APM_SQLBBContent.ManageDbUnmanageQuestion%>";
        message = SF(message, dbName);
        Ext.Msg.show({
            msg: message,
            buttons: Ext.Msg.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {
                    var config = {
                        applicationCustomType: "ABSA",
                        applicationId: appId,
                        itemId: dbId,
                        managed: managed,
                        message: message,
                        onComplete: function() { window.location.reload(true); }
                    };
                    SW.APM.ManageAppItem.manage(config);
                }
            }
        });
        return false;
    }

    function showApplicationUnmanagedMessage() {
        Ext.Msg.show({
            title: "<%=Resources.APM_SQLBBContent.ManageDbDialogCaption%>",
            msg: "<%=Resources.APM_SQLBBContent.ManageDbUnmanagedApplication%>",
            icon: Ext.Msg.INFO,
            buttons: Ext.Msg.OK
        });
        return false;
    }
</script>
