﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllApplicationsPlugin.ascx.cs" Inherits="Orion_APM_SqlBlackBox_Controls_AllApplicationsPlugin" %>
<%@ Register TagPrefix="apm" TagName="SqlBbCredDlg" Src="~/Orion/APM/SqlBlackBox/Controls/CredentialsDialog.ascx" %>
<apm:SqlBbCredDlg runat="server" />

<script type="text/javascript">
	//<![CDATA[
	var showSqlBbCredDialog = function (applicationId) {
	    try {
	    	SW.APM.SqlBB.CredDialog.show(applicationId);
	    } catch (ex) {
	    	alert(ex);
	    }
	    return false;
	};
	//]]>
</script>