﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXxDatabasesByActiveUserConnectionsLegend.ascx.cs" Inherits="Orion_APM_Controls_Charts_TopXxDatabasesByActiveUserConnectionsLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.apmSqlBB_TopXxDatabasesByActiveUserConnectionsLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.APM.Charts.SqlBBTopXxDatabasesByActiveUserConnectionsLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
</script>

<div runat="server" id="legend" class="chartLegend APM_TopXxDatabasesByActiveUserConnections" />
<div style="float:right;">
    <img src="/orion/images/SolarWinds.Logo.Footer.gif"/>
</div>
