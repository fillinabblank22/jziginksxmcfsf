﻿using SolarWinds.Orion.Web.Charting.v2;
using System;
using System.Web.UI;

public partial class Orion_APM_Controls_Charts_TopXxDatabasesByActiveUserConnectionsLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer
    {
        get { return "apmSqlBB_TopXxDatabasesByActiveUserConnectionsLegendInitializer__" + legend.ClientID; } 
    }
}