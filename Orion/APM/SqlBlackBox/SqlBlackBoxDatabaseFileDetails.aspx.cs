﻿using System;

using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_SqlBlackBox_SqlBlackBoxDatabaseFileDetails : ApmViewPage
{
    protected override void OnInit(EventArgs e)
    {
        this.resHost.SqlBlackBoxDatabaseFile = this.NetObjectAsDatabaseFile;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // title
        this.title.ViewTitle = this.ViewInfo.ViewTitle;
        this.title.ViewSubTitle = this.NetObjectAsDatabaseFile.Model.PhysicalName;

        this.topRightLinks.CustomizeViewHref = HtmlHelper.GetDefaultCustomizeViewHref(this.ViewInfo.ViewID);
        this.topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
        this.topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationItemPageUrl(this.NetObjectAsDatabaseFile.Model.ApplicationID, this.NetObjectAsDatabaseFile.Model.DatabaseID);

        this.topRightLinks.HelpUrlFragment = "OrionSAMAGSQLBBDBDetails";
    }

    public override string ViewType
    {
        get { return "Sql BlackBox Database File Details"; }
    }

    public SqlBlackBoxDatabaseFile NetObjectAsDatabaseFile
    {
        get { return (SqlBlackBoxDatabaseFile)this.NetObject; }
    }
}