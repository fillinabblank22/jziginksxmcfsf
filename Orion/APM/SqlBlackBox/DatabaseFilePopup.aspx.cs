﻿using System;
using System.Globalization;

using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Web.DAL;
using SolarWinds.APM.BlackBox.Sql.Web.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;

public partial class Orion_APM_SqlBlackBox_DatabaseFilePopup : PopupPage<DatabaseFileSize>
{
    protected const string CriticalColor = "#e22323";
    protected const string WarningColor = "#f1d200";
    protected const string UpColor = "#00b84a";
    protected const string UpImageUrl = "/Orion/APM/Images/TooltipBar.Green.gif";
    protected const string CriticalImageUrl = "/Orion/APM/Images/TooltipBar.Red.gif";
    protected const string WarningImageUrl = "/Orion/APM/Images/TooltipBar.Yellow.gif";

    protected const int BarSize = 270;
    // the coefficient is based on barArea width
    protected const double BarSizeCoefficient = 2.7;

    // thickness of each pointer, pointer == vertical line
    protected const int PointersThickness = 1;

    // label pointer offset --- distance from bar edge to hint that numerical value is not meant as an axis value at that point
    protected const int PointersOffset = 20;
    
    // minimal bar width so that area is visible and pointer is in the middle
    protected const int MinimalBarWidth = 3;


    private readonly Log log = new Log();

    #region Properties

    protected double UsedSpacePercentage
    {
        get
        {
            double availableAutoGrow = NetObjectData.AvailableAutoGrow.Value.GetValueOrDefault(0);
            double fileSize = NetObjectData.FileSize.Value.GetValueOrDefault(0);
            return this.CalculatePercents(NetObjectData.UsedSpace, fileSize + availableAutoGrow);
        }
    }

    protected double FileSizePercentage
    {
        get
        {
            double availableAutoGrow = NetObjectData.AvailableAutoGrow.Value.GetValueOrDefault(0);
            double fileSize = NetObjectData.FileSize.Value.GetValueOrDefault(0);
            return this.CalculatePercents(NetObjectData.FileSize, fileSize + availableAutoGrow);
        }
    }

    protected double WhiteSpacePercentage
    {
        get
        {
            return FileSizePercentage - UsedSpacePercentage;
        }
    }

    protected int UsedSpaceWidth
    {
        get
        {
            // In case of very very small number we still want to display a bar
            // minimal thickness can be set through MinimalBarWidth
            return (int)Math.Max(this.UsedSpacePercentage * BarSizeCoefficient, MinimalBarWidth);
        }
    }

    protected int WhiteSpaceWidth
    {
        get
        {
            // In case of very very small number we still want to display a bar
            // minimal thickness can be set through MinimalBarWidth
            return (int)Math.Max(this.WhiteSpacePercentage * BarSizeCoefficient, MinimalBarWidth);
        }
    }

    protected int AutoGrowWidth
    {
        get
        {
            // In case of very very small number we still want to display a bar
            // minimal thickness can be set through MinimalBarWidth
            return (int)Math.Max(BarSize - UsedSpaceWidth - WhiteSpaceWidth, MinimalBarWidth);
        }
    }

    protected string UsedSpaceColor
    {
        get
        {
            var color = UpColor;
            var ranking = NetObjectData.Status.GetStatusRanking();
            if (ranking <= 210)
                color = CriticalColor;
            else if (ranking <= 230)
                color = WarningColor;
            return color;
        }
    }

    protected string UsedSpaceImgUrl
    {
        get
        {
            var url = UpImageUrl;
            var ranking = NetObjectData.Status.GetStatusRanking();
            if (ranking <= 210)
                url = CriticalImageUrl;
            else if (ranking <= 230)
                url = WarningImageUrl;
            return url;
        }
    }

    protected bool IsFilePointerOnTheLeft
    {
        get
        {
            return this.FileSizePercentage < 50;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_APM_Popup)Master).PopupTitle = System.IO.Path.GetFileName(NetObjectData.PhysicalName);
        ((Orion_APM_Popup)Master).StatusCssClass = StatusCssClass;
    }

    protected int PositionUsedSpace
    {
        get { return Math.Min(PointersOffset, UsedSpaceWidth/2); }
    }

    protected int PositionWhiteSpace
    {
        get { 
            var offset = Math.Min(PointersOffset, WhiteSpaceWidth/2);
            return IsFilePointerOnTheLeft
                       ? UsedSpaceWidth + WhiteSpaceWidth - offset - PointersThickness
                       : BarSize - UsedSpaceWidth - WhiteSpaceWidth + offset;
        }
    }

    protected int PositionAutoGrow
    {
        get { return Math.Min(PointersOffset, AutoGrowWidth/2); }
    }

    private double CalculatePercents(double part, double full, int decimals = 0)
    {
        return Math.Round((part * 100 / full), decimals);
    }

    protected string FormatSpaceUsedByDataText(string text)
    {
        var formatted = text;
        var ranking = NetObjectData.Status.GetStatusRanking();
        if (ranking <= 210)
            formatted = "<span style='color:" + CriticalColor + "; font-weight:bold;'>" + text + "</span>";
        else if (ranking <= 230)
            formatted = "<span style='color:" + CriticalColor + ";'>" + text + "</span>";
        return formatted;
    }

    protected string FormatAvailableAutoGrowText(string text)
    {
        if (NetObjectData.VolumeSpaceAvailable == null || NetObjectData.RestrictedSize == null) return text;

        var formatted = text;
        if (NetObjectData.VolumeSpaceAvailable.Value < NetObjectData.RestrictedSize.Value)
        {
            var text4Restricted = InvariantString.Format("restricted to {0}", NetObjectData.RestrictedSize.ToString(CultureInfo.InvariantCulture));
            formatted = InvariantString.Format("<span style='color:red; font-weight:bold;'>{0} ({1})</span>", text, text4Restricted);
        }
        return formatted;
    }

    protected string FormatFreeVolumeSpaceText(string text)
    {
        if (NetObjectData.VolumeStatus == null) return text;

        var formatted = text;
        var ranking = NetObjectData.VolumeStatus.Value.OrionApmStatus().GetStatusRanking();
        if (ranking <= 210)
            formatted = "<span style='color:red;'>" + text + "</span>";
        else if (ranking <= 230)
            formatted = "<span style='color:red; font-weight:bold;'>" + text + "</span>";
        return formatted;
    }

    #region Overriden methods

    protected override DatabaseFileSize CreateNetObjectData(string netObjectId)
    {
        NetObjectHelper netObject;
        if (!NetObjectHelper.TryCreate(netObjectId, out netObject))
        {
            this.log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render popup, invalid net object ID '{0}'", netObjectId);
            return null;
        }

        if (netObject.Prefix != SqlBlackBoxConstants.SqlBlackBoxDatabaseFilePrefix)
        {
            this.log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render popup, invalid net object prefix '{0}'", netObjectId);
            return null;
        }

        var result = SqlBlackBoxDatabaseFileDAL.DefaultProxy.GetDatabaseFileSize((int)netObject.Id);
        if (result == null)
            this.log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render popup, because provided net object ID is not SQL BB database file object: '{0}'", netObjectId);

        return result;
    }

    protected override ApmStatus GetStatus()
    {
        return new ApmStatus(NetObjectData.Status);
    }

    #endregion

}