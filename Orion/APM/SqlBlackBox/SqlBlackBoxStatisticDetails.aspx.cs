﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.BlackBox.Sql.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using System;
using System.Globalization;

public partial class Orion_APM_SqlBlackBoxStatisticDetails : SqlBlackBoxOrionView, ISqlBlackBoxStatisticProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.resHost.SqlBlackBoxStatistic = this.SqlBlackBoxStatistic;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
     
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {       
        // title
        this.title.ViewTitle = this.ViewInfo.ViewTitle;
        this.title.ViewSubTitle = this.SqlBlackBoxStatistic.Name;

        // status icon
        this.title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.Component, this.SqlBlackBoxStatistic.StatusId);

        // customize
        if (Profile.AllowCustomize)
            this.topRightLinks.CustomizeViewHref = CustomizeViewHref;

        // edit
        if (ApmRoleAccessor.AllowAdmin)
        {
            this.topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationPageUrl(this.SqlBlackBoxApplication.Id, string.Format(CultureInfo.InvariantCulture, "selected={0}", this.SqlBlackBoxStatistic.Id));
            this.topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
        }

        // help
        this.topRightLinks.HelpUrlFragment = "OrionSAMAGSQLBBStatisticDetails";
    }

    protected string CustomizeViewHref
    {
        get { return HtmlHelper.GetDefaultCustomizeViewHref(ViewInfo.ViewID); }
    }

    public override string ViewKey
    {
        get { return "Sql BlackBox Statistic Details"; }
    }

    public override string ViewType
    {
        get { return "Sql BlackBox Statistic Details"; }
    }

    #region ISqlBlacBoxStatisticProvider, ISqlBlackBoxApplicationProvider, INodeProvider members

    public SqlBlackBoxStatistic SqlBlackBoxStatistic
    {
        get { return (SqlBlackBoxStatistic)this.NetObject; }
    }

    public override SqlBlackBoxApplication SqlBlackBoxApplication
    {
        get { return this.SqlBlackBoxStatistic.SqlBlackBoxApplication; }
    }

    public ApmApplicationBase ApmApplication
    {
        get { return SqlBlackBoxApplication; }
    }

    #endregion

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}
