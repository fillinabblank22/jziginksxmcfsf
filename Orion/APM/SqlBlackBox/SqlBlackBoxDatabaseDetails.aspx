﻿<%@ Page Language="C#" MasterPageFile="..\ApmView.master" AutoEventWireup="true" CodeFile="SqlBlackBoxDatabaseDetails.aspx.cs" Inherits="Orion_APM_SqlBlackBoxDatabaseDetails" %>
<%@ Register TagPrefix="sqlbb" Namespace="SolarWinds.APM.BlackBox.Sql.Web.UI" Assembly="SolarWinds.APM.BlackBox.Sql.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="apm" TagName="BreadcrumbBar" Src="~/Orion/APM/Controls/BreadcrumbBar.ascx" %>
<%@ Register TagPrefix="apm" TagName="TopRightLinks" Src="~/Orion/APM/Controls/Views/TopRightPageLinks.ascx" %>
<%@ Register TagPrefix="apm" TagName="ViewTitle" Src="~/Orion/APM/Controls/Views/ViewTitle.ascx" %>
<%@ Register  TagPrefix="apm" Src="~/Orion/APM/Controls/TimePeriodPicker/TimePeriodPicker.ascx" TagName="TimePeriodPicker" %>

<asp:Content ID="cntLinks" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <apm:TopRightLinks runat="server" ID="topRightLinks" />
</asp:Content>

<asp:Content ID="cntTitle" ContentPlaceHolderID="ApmPageTitle" Runat="Server">
    <apm:BreadcrumbBar ID="Breadcrumbs" runat="server" Provider="SqlBlackBoxSitemapProvider" SiteMapFilePath="\Orion\APM\SqlBlackBox\SqlBlackBox.sitemap" />
    <apm:ViewTitle runat="server" ID="title" />
    <div class="period" style="">        
       <apm:TimePeriodPicker runat="server" id="TimePeriodPicker" TimePeriodControlSubmitBehaviorMode="Redirect" />
    </div>        
</asp:Content>

<asp:Content ID="cntMain" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<sqlbb:SqlBlackBoxDatabaseResourceHost ID="resHost" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</sqlbb:SqlBlackBoxDatabaseResourceHost>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="OutsideFormPlaceHolder" ID="contentNote">
    <orion:Include ID="I1" File="APM/APM.css" runat="server" />
    <div class="contentNote">
        <%= Resources.APM_SQLBBContent.ExpertKnowledgeAttribution %>
    </div>
</asp:Content>