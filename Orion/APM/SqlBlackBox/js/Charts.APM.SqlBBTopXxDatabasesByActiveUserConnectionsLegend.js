﻿SW = SW || {};
SW.APM = SW.APM || {};
SW.APM.Charts = SW.APM.Charts || {};
SW.APM.Charts.SqlBBTopXxDatabasesByActiveUserConnectionsLegend = SW.APM.Charts.SqlBBTopXxDatabasesByActiveUserConnectionsLegend || {};
(function (legend) {
    legend.createStandardLegend = function (chart, legendContainerId) {

        var expandNode = function (uniqueId) {
            // change legend item visibility
            var legendItem = $('#legendItem_' + uniqueId);
            legendItem.addClass('collapse').removeClass('expand');

            // change detail visibility
            var legendDetail = legendItem.find('.legendDetail');

            // change event handler for legend link
            var link = legendItem.find('.legendLine a');
            link.unbind('click');
            link.click(function () {
                collapseNode(uniqueId);
                return false;
            });

            // load detail data if needed
            if (!legendDetail.is(':has(form)')) {
                legendDetail.data('loadData')();
            }
        };

        var collapseNode = function (uniqueId) {
            // change legend item visibility
            var legendItem = $('#legendItem_' + uniqueId);
            legendItem.addClass('expand').removeClass('collapse');

            // change event handler for legend link
            var link = legendItem.find('.legendLine a');
            link.unbind('click');
            link.click(function () {
                expandNode(uniqueId);
                return false;
            });
        };

        var loadServerControl = function (point) {
            var data = "{applicationId: '" + point.applicationId + "', databaseId: '" + point.databaseId + "', parentNetObject: '" + point.netObject + "', uniqueId: '" + point.uniqueId + "'}";

            $.ajax({
                type: "POST",
                url: "/Orion/APM/SqlBlackBox/Services/ChartData.asmx/GetActiveUserConnectionsTableControl",
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                uniqueId: point.uniqueId,
                success: function (r) {
                    // replace content of legend detail
                    $('#legendItem_' + this.uniqueId + ' div.legendDetail').html(r.d);
                }
            });
        };

        // function for formatting user count value
        var formatUsersCount = function (count) {
            var usersLabel = (count > 1) ? '@{R=APM.BlackBox.Sql.Strings;K=ChartTemplate_Users;E=js}' : '@{R=APM.BlackBox.Sql.Strings;K=ChartTemplate_User;E=js}';
            return count + ' ' + usersLabel;
        };

        // function for getting expanded node ids
        var getExpandedItemIds = function (container) {

            var ids = [];
            container.find('div.legendItem.collapse').each(function () {
                ids.push(this.id);
            });

            return ids;
        };

        var legendContainer = $('#' + legendContainerId);
        var idsToExpand = getExpandedItemIds(legendContainer);
        legendContainer.empty();
        var totalUsers = 0;

        // loop over all chart points
        $.each(chart.series[0].data, function (index, point) {
            totalUsers += point.count;

            var legendItem = $('<div id="legendItem_' + point.uniqueId + '" class="legendItem" />');
            legendItem.addClass('expand');

            var legendLine = $('<div class="legendLine" />');

            var link = $('<a href="#" />');
            link.appendTo(legendLine);
            link.click(function () {
                expandNode(point.uniqueId);
                return false;
            });

            var expandIcon = '<div class="expandCollapseIcon" />';
            var legendColor = '<div class="smallLegendColorIcon" style="background-color: ' + point.color + '" />';
            var usersCount = '<div class="usersCount">' + formatUsersCount(point.count) + '</div>';
            var databaseStatus = '<div class="databaseStatus"><img src="/Orion/StatusIcon.ashx?entity=Orion.APM.SqlDatabase&status=' + point.status + '"/></div>';
            var databaseName = '<div class="databaseName">' + point.name + '</div>';

            $(expandIcon + legendColor + usersCount + databaseStatus + databaseName).appendTo(link);

            legendLine.appendTo(legendItem);


            var legendDetail = $('<div class="legendDetail">' + '@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}' + '</div>');
            legendDetail.data({
                loadData: function () {
                    loadServerControl(point);
                }
            });

            legendDetail.appendTo(legendItem);

            legendItem.appendTo(legendContainer);
        });

        // add summary
        $('<div class="ReportHeader"><span class="connectionsSummary">' + formatUsersCount(totalUsers) + ' ' + '@{R=APM.Strings;K=APMWEBJS_Total;E=js}'+'</span></div>').appendTo(legendContainer); // TODO: localize

        // expand items again
        $.each(idsToExpand, function (index, value) {
            $('#' + value + ' div.legendLine a').click();
        });
    };

}(SW.APM.Charts.SqlBBTopXxDatabasesByActiveUserConnectionsLegend));
