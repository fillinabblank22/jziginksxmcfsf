﻿MakeNamespace("SW.APM.SqlBB.CredDialog");

SW.APM.SqlBB.CredDialog = function () {
	var mUrl = "/Orion/APM/SqlBlackBox/Services/UnknownCredentialsEditorService.asmx";
	var mDlgEl = null;
	var mApp = null, mCreds = null;
	var mErrorTimeout;

	/*helper members*/
	var call = function (method, args, onSuccess) {
		SW.Core.Services.callWebService(mUrl, method, args, onSuccess, showError);
	}
	var showError = function (message) {
		var el = $("#sqlBbCredDialog .error div")
			.empty().html(message);
		if (el && el.fadeIn) {
			el.fadeIn(400, function () {
				if (mErrorTimeout) {
					clearTimeout(mErrorTimeout);
				}
				mErrorTimeout = setTimeout(function () { $("#sqlBbCredDialog .error div").hide().empty(); }, 6000);
			});
		}
	}
	var validate = function () {
		$("#sqlBbApps .app-row .app-row .test")
			.empty();
		$("#sqlBbCredDialog .error div")
			.empty().hide();

		var message = "";
		if ($("#sqlBbCreds :selected").val() == -1) {
			var value = $.trim($("#sqlBbCredName").val());
			if (value.length == 0) {
                message += "@{R=APM.Strings;K=APMWEBJS_EnterCredentialName;E=js}"+"<br />";
			}
			value = $.trim($("#sqlBbUserName").val());
			if (value.length == 0) {
                message += "@{R=APM.Strings;K=APMWEBJS_EnterUserName;E=js}"+"<br />";
			}
			if ($("#sqlBbPassword1").val() != $("#sqlBbPassword2").val()) {
                message += "@{R=APM.Strings;K=APMWEBJS_ConfirmPasswordIsDifferent;E=js}"+"<br />";
			}
		}

		var val = $("#sqlBbPortType").val();
		if (val == "Static") {
			var port = Number($("#sqlBbPortNumber").val());
			if (isNaN(port)) {
                message += "@{R=APM.Strings;K=APMWEBJS_IncorrectPortNumberValue;E=js}"+"<br />";
			} else if (port < 1 || port > 65535) {
                message += "@{R=APM.Strings;K=APMWEBJS_PortNumberShouldBeInRangeFrom1to65535;E=js}"+"<br />";
			}
		}

		if (message.length > 0) {
			showError(message);
		}
		return message.length == 0;
	}

	/*ajax event handlers*/
	var onTestSuccess = function (tests) {
		$.each(tests, function () {
			if (this.TestSuccessfull && (this.Result.IsValidForSql || this.Result.IsValidForSqlWindows)) {
				$("#sqlBbApps .app-rows .app-row span.test")
					.empty()
					.html($("#sql-bb-app-test-success").html());
			} else {
				$("#sqlBbApps .app-rows .app-row span.test")
					.empty()
					.html($("#sql-bb-app-test-fail").html());
			}
		})
	}
	var onAssignSuccess = function (result) {
		if (result.TestSuccessfull && (result.Result.IsValidForSql || result.Result.IsValidForSqlWindows)) {
			window.location.reload();
		} else {
			$("#sqlBbApps .app-rows .app-row span.test")
				.empty()
				.html($("#sql-bb-app-assign-fail").html());
		}
	}

	var onCredentialsLoad = function (creds) {
		mCreds = {};

		var el = $("#sqlBbCreds");
		el.removeAttr("disabled").empty();
        el.append($("<option value='-1'>" + "@{R=APM.Strings;K=APMWEBJS_NewCredential;E=js}" +"</option>"));
		$.each(creds, function () {
			mCreds[this.Id] = this;
			el.append($(SF("<option value='{0}'>{1}</option>", this.Id, this.Name)));
		});
		el.unbind("change").bind("change", onCredSelect);
		el.change();
	}
	var onAplicationLoad = function (app) {
		mApp = app;
		if (mApp) {
			$("#sqlBbPortType").removeAttr("disabled");
			if (mApp.PortType == "Default") {
				$("#sqlBbPortNumberContainer").hide();
			} else {
				$("#sqlBbPortNumberContainer").show();
			}
			$("#sqlBbPortType").val(mApp.PortType).change();

			var name = SF($("#sql-bb-app-name").html(), mApp.Name, mApp.NodeID, mApp.NodeStatus, mApp.NodeName);

			$("#sqlBbCredDialog .current-app-name").html(name);
			$("#sqlBbApps").show();
			$("#sqlBbApps .app-rows")
				.empty()
				.html(SF($("#sql-bb-app-row").html(), mApp.ID, name));
		}
	}

	/*dom event handlers*/
	var onCredSelect = function () {
		var id = $("#sqlBbCreds :selected").val();
		if (id == -1) {
			$("#sqlBbCredName,#sqlBbUserName,#sqlBbPassword1,#sqlBbPassword2,#sqlBbPortType")
				.removeAttr("disabled")
				.val("");
		} else {
			$("#sqlBbCredName").val(mCreds[id].Name).attr("disabled", "disabled");
			$("#sqlBbUserName").val(mCreds[id].UserName).attr("disabled", "disabled");
			$("#sqlBbPassword1").val(mCreds[id].Password).attr("disabled", "disabled");
			$("#sqlBbPassword2").val(mCreds[id].Password).attr("disabled", "disabled");
		}
	};

	var onPortTypeSelect = function () {
		var val = $("#sqlBbPortType").val();
		if (val == "Default") {
			$("#sqlBbPortNumberContainer").hide();
		} else {
			$("#sqlBbPortNumberContainer").show();
		}
		$("#sqlBbPortNumber").val(mApp.PortNumber || "1433");
	}

	var onButtonDeleteClick = function () {
		$("#sqlBbCredConfirmDialog div.app-rows")
			.html(SF($("#sql-bb-app-name").html(), mApp.Name, mApp.NodeID, mApp.NodeStatus, mApp.NodeName));

		var dlg = $("#sqlBbCredConfirmDialog").dialog({
			width: 400, autoOpen: true, modal: true
		});
		$("#sqlBbCredConfirmDialog a[id$=btnConfirmOk]")
			.unbind("click")
			.bind("click", function () { call("DeleteSqlBbApplicationsWithoutCred", { appIds: [mApp.ID] }, function () { window.location.reload(); }); });
		$("#sqlBbCredConfirmDialog a[id$=btnConfirmCancel]")
			.unbind("click")
			.bind("click", function () { dlg.dialog("close"); return false; });
	};

	var onButtonTestClick = function () {
		if (validate()) {
			$("#sqlBbApps .app-rows .app-row .test")
				.empty();
			$(SF("#sqlBbApps .app-rows .app-row[app-id={0}]", mApp.ID))
				.find("span.test")
				.html($("#sql-bb-app-test-process").html())

			var portType = $("#sqlBbPortType").val(), portNumber = null;
			if (portType == "Static") {
				portNumber = parseInt($("#sqlBbPortNumber").val());
			}

			var setting = {
				AppType: mApp.Type,
				NodeId: mApp.NodeID,
				CredentialSet: {
					Id: $("select#sqlBbCreds :selected").val(),
					Login: $("#sqlBbUserName").val(),
					Password: $("#sqlBbPassword1").val()
				},
				CustomSettings: {
					InstanceName: mApp.Instance,
					PortNumber: portNumber || mApp.PortNumber,
					PortType: portType || mApp.PortType
				}
			};
			call("TestSqlBlackBoxConnections", { settings: [setting] }, onTestSuccess);
		}
		return false;
	};
	var onButtonAssignClick = function () {
		if (validate()) {
			$(SF("#sqlBbApps .app-rows .app-row[app-id={0}]", mApp.ID))
				.find("span.test")
				.html($("#sql-bb-app-assign-process").html())

			var portType = $("#sqlBbPortType").val(), portNumber = null;
			if (portType == "Static") {
				portNumber = parseInt($("#sqlBbPortNumber").val());
			}

			var args = {
				cred: { Id: $("#sqlBbCreds :selected").val(), Name: $("#sqlBbCredName").val(), UserName: $("#sqlBbUserName").val(), Password: $("#sqlBbPassword1").val() },
				info: [
					mApp.NodeID, mApp.ID, mApp.Instance, portNumber || mApp.PortNumber, portType || mApp.PortType
				]
			};
			call("ApplyCredToSqlBbApplications", args, onAssignSuccess);
		}
		return false;
	}
	var onButtonCancelClick = function () {
		mDlgEl.dialog("destroy");
		return false;
	}

	/*public members*/
	this.init = function (appId) {
		mDlgEl = $("#sqlBbCredDialog").dialog({
			width: 560, modal: true, autoOpen: false,
			overlay: { "background-color": "black", opacity: 0.4 }
		});

		$("#sqlBbCreds").empty();
		$("#sqlBbCredDialog .current-app-name").empty();
		$("#sqlBbApps .app-rows").empty();

		$("#sqlBbCredDialog .error div")
			.empty().hide();

		$("#sqlBbCredDialog select#sqlBbPortType")
			.unbind("change")
			.bind("change", onPortTypeSelect);

		$("#sqlBbCredDialog a[id$=btnTest]")
			.unbind("click")
			.bind("click", onButtonTestClick);
		$("#sqlBbCredDialog a#btnDelete")
			.unbind("click")
			.bind("click", onButtonDeleteClick);
		$("#sqlBbCredDialog a[id$=btnAssign]")
			.unbind("click")
			.bind("click", onButtonAssignClick);
		$("#sqlBbCredDialog a[id$=btnCancel]")
			.unbind("click")
			.bind("click", onButtonCancelClick);

		call("GetCredentials", {}, onCredentialsLoad);
		call("GetSqlBbApplicationsWithoutCred", { appId: appId }, onAplicationLoad);
	};

	this.show = function () {
		mDlgEl.dialog("open");

		$("sqlBbPortNumberContainer").hide();
		$("#sqlBbCreds, #sqlBbCredName, #sqlBbUserName, #sqlBbPassword1, #sqlBbPassword2, #sqlBbPortType")
			.attr("disabled", "disabled");
	};
	this.hide = function () {
		mDlgEl.dialog("destroy");
	};
};

/*static members*/
SW.APM.SqlBB.CredDialog.instance = null;
SW.APM.SqlBB.CredDialog.show = function (appId) {
	if (SW.APM.SqlBB.CredDialog.instance == null) {
		SW.APM.SqlBB.CredDialog.instance = new SW.APM.SqlBB.CredDialog();
	}
	SW.APM.SqlBB.CredDialog.instance.init(appId);
	SW.APM.SqlBB.CredDialog.instance.show();
};
SW.APM.SqlBB.CredDialog.hide = function () {
	if (SW.APM.SqlBB.CredDialog.instance == null) {
		return;
	}
	SW.APM.SqlBB.CredDialog.instance.hide();
};