﻿/*jslint browser: true*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.templates', function (module) {
    'use strict';
    module.registerHandlerAssignWizard('ABSA', '../SqlBlackBox/Admin/AssignWizard/AssignSmartSqlApplication.aspx');
});
