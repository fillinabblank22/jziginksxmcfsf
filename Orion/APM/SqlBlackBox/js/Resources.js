﻿SW.APM = SW.APM || {};
SW.APM.SQLBB = SW.APM.SQLBB || {};
SW.APM.SQLBB.Resources = SW.APM.SQLBB.Resources || {};
(function (singleton) {
    singleton.autoGrowthValues = [
        "@{R=APM.BlackBox.Sql.Strings;K=DatabaseBySizeFile_AutoGrowthDisabled;E=js}",
        "@{R=APM.BlackBox.Sql.Strings;K=DatabaseBySizeFile_AutoGrowthRestricted;E=js}",
        "@{R=APM.BlackBox.Sql.Strings;K=DatabaseBySizeFile_AutoGrowthUnrestricted;E=js}"
    ];
    singleton.getSimpleStatus = function (apmStatus) {
        var status = 'up';
        var ranking = SW.APM.Converters.getStatusRanking(apmStatus);
        if (ranking <= 210)
            status = 'critical';
        else if (ranking <= 230)
            status = 'warning';
        return status;
    };
})(SW.APM.SQLBB.Resources);
