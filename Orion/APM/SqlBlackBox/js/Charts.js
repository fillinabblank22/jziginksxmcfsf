﻿SW.APM = SW.APM || {};
SW.APM.SQLBB = SW.APM.SQLBB || {};
SW.APM.SQLBB.Charts = SW.APM.SQLBB.Charts || {};
(function (common) {
    common.DatabaseAvailabilitySeriesTemplates = {
        Unknown_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Unknown;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(90,90,90)"],
                    [0.5, "rgb(145,145,145)"],
                    [1, "rgb(183,183,183)"]
                ]
            }
        },
        Up_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Up;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(62,99,23)"],
                    [0.5, "rgb(93,147,35)"],
                    [1, "rgb(119,189,45)"]
                ]
            }
        },
        Warning_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Warning;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(196,163,0)"],
                    [0.5, "rgb(228,193,16)"],
                    [1, "rgb(252,217,40)"]
                ]
            }
        },
        Critical_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Critical;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(84,25,26)"],
                    [0.5, "rgb(132,39,41)"],
                    [1, "rgb(164,49,50)"]
                ]
            }
        },
        Restoring_Count: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=Enum_DatabaseState_Restoring;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(25,0,76)"],
                    [0.5, "rgb(40,0,122)"],
                    [1, "rgb(49,0,145)"]
                ]
            }
        },
        Recovering_Count: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=Enum_DatabaseState_Recovering;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(37,108,122)"],
                    [0.5, "rgb(58,166,188)"],
                    [1, "rgb(73,210,242)"]
                ]
            }
        },
        Recovery_Pending_Count: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=Enum_DatabaseState_Recovery_Pending;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,54,84)"],
                    [0.5, "rgb(0,87,135)"],
                    [1, "rgb(0,108,169)"]
                ]
            }
        },
        Suspect_Count: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=Enum_DatabaseState_Suspect;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(71,0,67)"],
                    [0.5, "rgb(117,0,111)"],
                    [1, "rgb(147,0,139)"]
                ]
            }
        },
        Emergency_Count: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=Enum_DatabaseState_Emergency;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(40,40,40)"],
                    [0.5, "rgb(65,65,65)"],
                    [1, "rgb(81,81,81)"]
                ]
            }
        },
        Offline_Count: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=Enum_DatabaseState_Offline;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(145,0,0)"],
                    [0.5, "rgb(205,0,16)"],
                    [1, "rgb(230,25,41)"]
                ]
            }
        },
        Copying_Count: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=Enum_DatabaseState_Copying;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(75,58,117)"],
                    [0.5, "rgb(119,93,186)"],
                    [1, "rgb(150,115,234)"]
                ]
            }
        },
        Availability: {
            name: "@{R=APM.Strings;K=APM_ChartSettings_yAxis_Availability;E=js}",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.DatabaseFileSizeSeriesTemplates = {
        FileUsedSpace: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=ChartTemplate_SpaceUsedByData;E=js}",
            type: "area",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(153,119,51)"],
                    [0.5, "rgb(189,147,63)"],
                    [1, "rgb(199,162,87)"]
                    //[0, "rgb(63,134,0)"],
                    //[0.5, "rgb(93,163,19)"],
                    //[1, "rgb(119,189,45)"]
                ]
            }
        },
        FileWhiteSpace: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=ChartTemplate_WhiteSpace;E=js}",
            type: "area",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(16,190,229)"],
                    [0.5, "rgb(73,210,242)"],
                    [1, "rgb(160,232,248)"]
                ]
            }
        },
        AutoGrowSpace: {
            name: "@{R=APM.BlackBox.Sql.Strings;K=ChartTemplate_AvailableAutoGrow;E=js}",
            type: "area",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,52,82)"],
                    [0.5, "rgb(0,108,169)"],
                    [1, "rgb(0,155,245)"]
                ]
            }
        }
    };

})(SW.APM.SQLBB.Charts);
