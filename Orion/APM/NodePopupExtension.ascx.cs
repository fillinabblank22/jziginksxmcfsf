﻿using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using System;
using System.Data;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_NodePopupExtension : System.Web.UI.UserControl
{
	protected int NormalPercentage { get; set; }
	protected int WarningPercentage { get; set; }
	protected int ErrorPercentage { get; set; }
	protected int UnknownPercentage { get; set; }

	protected long NodeId
	{
		get
		{
			NetObjectHelper netobject = new NetObjectHelper(Request.QueryStringOrForm("NetObject"));

			return netobject.Id;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		LoadData(NodeId);
	}

	private void LoadData(long nodeId)
	{
		string filter = String.Format("Application.NodeID = {0}", nodeId);
		DataTable data = SwisDAL.GetApplicationsByCurrentStatus(filter, String.Empty);

		SetApplicationHealthBar(data);
	}

	private void SetApplicationHealthBar(DataTable data)
	{
		if (data.Rows.Count == 0)
		{
			Visible = false;
			return;
		}
		int up = 0;
		int warning = 0;
		int down = 0;
		int unknown = 0;

		foreach (DataRow row in data.Rows)
		{
			object rowValue = row["Availability"];
			Status rowStatus = Convert.IsDBNull(rowValue) ? Status.Undefined : (Status)(int)rowValue;

			switch (rowStatus)
			{
				case Status.Available:
					up++;
					break;
				case Status.NotAvailable:
					down++;
					break;
				case Status.Warning:
				case Status.Critical:
					warning++;
					break;
				case Status.Undefined:
				default:
					unknown++;
					break;
			}
		}

		double total = up + warning + down + unknown;

		healthBar.NormalPercentage = (int)(100 * up / total);
		healthBar.WarningPercentage = (int)(100 * warning / total);
		healthBar.ErrorPercentage = (int)(100 * down / total);
		healthBar.UnknownPercentage = 100 - healthBar.NormalPercentage - healthBar.WarningPercentage - healthBar.ErrorPercentage;
	}
}