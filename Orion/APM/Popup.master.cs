﻿public partial class Orion_APM_Popup : System.Web.UI.MasterPage, SolarWinds.APM.Web.UI.IApmPopupMaster
{
    public string StatusCssClass { get; set; }

    public string PopupTitle { get; set; }
}
