﻿using System;
using System.Globalization;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_ApplicationPopup : PopupPage<ApmApplicationBase>
{
    private readonly Log log = new Log();

    protected const int TopComponentsCount = 5;

    protected ApmApplicationBase App
    {
        get { return this.NetObjectData; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var popup = (Orion_APM_Popup) this.Master;

        if (popup == null)
        {
            throw new ArgumentNullException("Master");
        }

        popup.PopupTitle = Resources.APMWebContent.APMWEBDATA_TM0_49;
        popup.StatusCssClass = StatusCssClass;

        this.AppStatusIcon.StatusValue = this.App.Status;
        this.AppStatusIcon.CustomApplicationType = this.App.CustomType;
        this.ServerStatusIcon.StatusValue = this.App.NPMNode.Status;

        this.CPULoad.Value = this.App.NPMNode.CPULoad;
        SetupBarPercentage(
            this.CPULoadBar,
            this.App.NPMNode.CPULoad,
            100,
            Thresholds.CPULoadWarning.SettingValue,
            Thresholds.CPULoadError.SettingValue
            );

        this.MemoryUsed.Value = (short) this.App.NPMNode.PercentMemoryUsed;
        SetupBarPercentage(
            this.MemoryUsedBar,
            this.App.NPMNode.PercentMemoryUsed,
            100,
            Thresholds.PercentMemoryWarning.SettingValue,
            Thresholds.PercentMemoryError.SettingValue
            );

        phCpuLoad.Visible = (this.App.NPMNode.CPULoad != -2);
        phMemUsed.Visible = (this.App.NPMNode.MemoryUsed != -2);


        this.componentList.BindComponents(
            this.App.ComponentsWithProblems,
            TopComponentsCount,
            this.App.CustomType
            );
    }

    protected override ApmStatus GetStatus()
    {
        return NetObjectData.Status;
    }

    protected override ApmApplicationBase CreateNetObjectData(string netObjectId)
    {
        var netObject = this.CreateNetObject();
        var result = netObject as ApmApplicationBase ?? (netObject is ApmMonitor ? ((ApmMonitor)netObject).Application : null);

        if (result == null)
        {
            this.log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render application popup, because cannot get application info from netObject with netObjectID '{0}'", netObjectId);
        }
        return result;
    }
}
