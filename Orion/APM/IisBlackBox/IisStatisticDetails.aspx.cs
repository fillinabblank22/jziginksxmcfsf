﻿using System;
using System.Globalization;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_IisBlackBox_IisStatisticDetails : IisOrionView, IIisStatisticProvider
{
	public override string ViewKey
	{
		get { return "IIS BlackBox Statistic Details"; }
	}

	public override string ViewType
	{
		get { return "IIS BlackBox Statistic Details"; }
	}

	public IisStatistic IisBlackBoxStatistic
	{
		get { return (IisStatistic)NetObject; }
	}

	public override IisApplication IisApplication
	{
		get { return IisBlackBoxStatistic.IisApplication; }
	}

	protected override void OnInit(EventArgs e)
	{
		resHost.LoadFromRequest();

		resContainer.DataSource = ViewInfo;
		resContainer.DataBind();

		base.OnInit(e);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
        title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        title.ViewSubTitle = IisBlackBoxStatistic.Name;
        title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.IISComponent, IisBlackBoxStatistic.StatusId);

        topRightLinks.HelpUrlFragment = "OrionSAMAGSQLBBStatisticDetails";  // common BB help link, misleading SQL in url due to SQL being first BB
		if (Profile.AllowCustomize)
		{
			topRightLinks.CustomizeViewHref = CustomizeViewHref;
		}
		if (ApmRoleAccessor.AllowAdmin)
		{
		    topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationPageUrl(IisApplication.Id,
		        string.Format(CultureInfo.InvariantCulture, "selected={0}", IisBlackBoxStatistic.Id));
			topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
		}
	}

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}