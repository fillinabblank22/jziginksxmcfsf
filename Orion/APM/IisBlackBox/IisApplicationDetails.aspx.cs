﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_APM_IISBlackBox_IISApplicationDetails : IisOrionView, IIisApplicationProvider, ITimePeriodProvider
{
    protected override void OnInit(EventArgs e)
    {
        resHost.IisApplication = IisApplication;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        // DateTime picker support
        TimePeriodPicker.Visible = ViewInfo.ViewKey == "IIS BlackBox Application Summary";

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // title
        title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        title.ViewSubTitle = IisApplication.Name;

        // status icon
        this.title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.IISApplication, this.IisApplication.StatusId);

        // customize
        if (Profile.AllowCustomize)
            topRightLinks.CustomizeViewHref = CustomizeViewHref;

        // edit
        if (ApmRoleAccessor.AllowAdmin)
        {
            topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationPageUrl(IisApplication.Id);
            topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
        }

        // help
        topRightLinks.HelpUrlFragment = "samagappinsiisdetailstop";
    }

    public override string ViewKey
    {
        get { return "IIS BlackBox Application Summary"; }
    }

    public override string ViewType
    {
        get { return "IIS BlackBox Application Details"; }
    }

    public override void SelectView()
    {
        base.SelectView();

        if (IsPreview)
        {
            Int32 viewId = Convert.ToInt32(Request.QueryString["ViewId"]);
            // FB#4117 - wrong application and view were selected by Core code for specified VievID
            var filter = new Dictionary<string, object> { { "ViewId", viewId } };
            var data = ApplicationDAL.GetAllApplicationsAsLazy(filter);
            SolarWinds.APM.Common.Models.Application app = data.Count == 0 ? null : data[0];
            if (app == null)
            {
                // no application for current view - leave application as is and just change view
                ViewInfo vi = ViewManager.GetViewById(viewId);
                if (vi != null)
                {
                    this.ViewInfo = vi;
                }
                return;
            }
            var application = new IisApplication(app);

            NetObject = application;
            Template = new ApplicationTemplateDAL().GetTemplateInfo(application.TemplateId);

            SelectViewByViewKey();
        }
    }

    public override IisApplication IisApplication
    {
        get { return (IisApplication)NetObject; }
    }


    #region ITimePeriodProvider

    public DateTime? TimePeriodStartDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDate().Value; }
    }

    public DateTime? TimePeriodEndDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDate().GetValueOrDefault(DateTime.Now); }
    }

    public DateTime? TimePeriodStartDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDateUTC().Value; }
    }

    public DateTime? TimePeriodEndDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDateUTC().GetValueOrDefault(DateTime.UtcNow); }
    }

    public int GetRelativeTimePeriodMinutes()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutes();
    }

    public int GetRelativeTimePeriodMinutesUTC()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutesUTC();
    }

    public Boolean TimePeriodIsSetByUser()
    {
        return TimePeriodPicker.PeriodFilter.IsSetInQueryString(Request);
    }

    #endregion

    #region IDynamicInfoProvider
    public object GetValue(string key)
    {
        return this.GetDynamicInfoValue(key);
    }
    #endregion
}
