﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApplicationPoolPopup.aspx.cs" Inherits="Orion_APM_IisBlackBox_ApplicationPoolPopup" MasterPageFile="~/Orion/APM/Popup.master"%>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.IIS.Common.Models" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmItemStatusIcon" Src="~/Orion/APM/Controls/SmallApmItemStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="PopupComponentList" Src="~/Orion/APM/Controls/PopupComponentList.ascx" %>
<asp:Content runat="server" ContentPlaceHolderID="NetObjectTipBody">
    <p class="StatusDescription"><%= String.Format(APMWebContent.APMWEBDATA_TM0_58, IisApp.Name, IisApp.NodeName) %></p>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= APM_IisBBContent.ApplicationPool_ToolTip_Title %></th>
            <td align="center"><apm:SmallApmItemStatusIcon ID="AppPoolStatusIcon" runat="server" /></td>
            <td><%= String.Format(APM_IisBBContent.ApplicationPool_ToolTip_Label, GetStatus().ToLocalizedString()) %></td>
        </tr>
        <tr>
            <th><%= APM_IisBBContent.IISBB_Application_Pool_State %></th>
            <td align="center"></td>
            <td style ="<%= AppPool.State == EntityState.Stopped ? "font-weight: bold !important; color: red !important;" : string.Empty %>"><%= AppPool.State %></td>
        </tr>
        <tr>
            <th><%= APMWebContent.APMWEBDATA_TM0_50 %></th>
            <td align="center"><apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server" /></td>
            <td><%= String.Format(APMWebContent.APMWEBDATA_TM0_56, IisApp.Status.ToLocalizedString()) %></td>
        </tr>
        <tr>
            <th><%= APMWebContent.APMWEBDATA_TM0_51 %></th>
            <td align="center"><orion:SmallNodeStatus ID="ServerStatusIcon" runat="server" /></td>
            <td><%= String.Format(APMWebContent.APMWEBDATA_TM0_57, IisApp.NPMNode.Status.ParentStatus.ToLocalizedString()) %></td>
        </tr>
    </table>
    <apm:PopupComponentList ID="componentList" runat="server"/>
</asp:Content>
