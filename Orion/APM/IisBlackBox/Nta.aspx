<%@ Page Language="C#" MasterPageFile="~/Orion/APM/ApmView.master" AutoEventWireup="true" CodeFile="Nta.aspx.cs" Inherits="Orion_APM_IisBlackBox_Nta" %>
<%@ Register TagPrefix="iisbb" Namespace="SolarWinds.APM.BlackBox.IIS.Web.UI" Assembly="SolarWinds.APM.BlackBox.IIS.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="apm" TagName="BreadcrumbBar" Src="~/Orion/APM/Controls/BreadcrumbBar.ascx" %>

<%@ Register TagPrefix="apm" TagName="ViewTitle" Src="~/Orion/APM/Controls/Views/ViewTitle.ascx" %>
<%@ Register TagPrefix="apm" Src="~/Orion/APM/Controls/TimePeriodPicker/TimePeriodPicker.ascx" TagName="TimePeriodPicker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <div>
        <a href='<%=CustomizeViewHref%>' style="padding: 2px 0px 2px 20px; background: transparent url(/Orion/APM/Images/Page.Icon.CustomPg.gif) scroll no-repeat left center;">
            <%= Resources.APMWebContent.APMWEBDATA_VB1_161 %></a>
    </div>
</asp:Content>

<asp:Content ID="cntTitle" ContentPlaceHolderID="ApmPageTitle" Runat="Server">   
    <apm:ViewTitle runat="server" ID="title" />

</asp:Content>

<asp:Content ID="cntMain" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<iisbb:IisApplicationResourceHost ID="resHost" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</iisbb:IisApplicationResourceHost>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="OutsideFormPlaceHolder" ID="contentNote">
    <orion:Include ID="I1" File="APM/APM.css" runat="server" />
    <div class="contentNote">
         <%= Resources.APM_IisBBContent.ExpertKnowledgeAttribution %>
    </div>
</asp:Content>