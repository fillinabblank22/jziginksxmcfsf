﻿<%@ Page Language="C#" MasterPageFile="..\ApmView.master" AutoEventWireup="true" CodeFile="IisApplicationPoolDetails.aspx.cs" Inherits="Orion_APM_IisBlackBox_IisApplicationPoolDetails" %>

<%@ Register TagPrefix="iisbb" Namespace="SolarWinds.APM.BlackBox.IIS.Web.UI" Assembly="SolarWinds.APM.BlackBox.IIS.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="apm" TagName="BreadcrumbBar" Src="~/Orion/APM/Controls/BreadcrumbBar.ascx" %>
<%@ Register TagPrefix="apm" TagName="TopRightLinks" Src="~/Orion/APM/Controls/Views/TopRightPageLinks.ascx" %>
<%@ Register TagPrefix="apm" TagName="ViewTitle" Src="~/Orion/APM/Controls/Views/ViewTitle.ascx" %>
<%@ Register TagPrefix="apm" Src="~/Orion/APM/Controls/TimePeriodPicker/TimePeriodPicker.ascx" TagName="TimePeriodPicker" %>

<asp:Content ID="cntLinks" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
  <orion:Include ID="IFullDataView" File="APM/IisBlackBox/Styles/IisApplicationPoolDetails.css" runat="server" />
    <%if (ViewInfo.ViewKey == SolarwindsApmSubviewKey)
      { %>
   <a  target="_blank" href="<%="/api2/samappoptics/integrations/GoToAppOptics?appPoolId="+ ApplicationPool.Id %>">
        <img class="full-data-view-icon" src="/Orion/Images/SubViewIcons/NetFlow03.png" />
        <%= Resources.APM_IisBBContent.ApplicationPoolFullDataView %> 
    </a> 
    <% } %>
    <apm:TopRightLinks runat="server" ID="topRightLinks" />
</asp:Content>

<asp:Content ID="cntTitle" ContentPlaceHolderID="ApmPageTitle" Runat="Server">   
    <apm:BreadcrumbBar ID="Breadcrumbs" runat="server" Provider="IisSiteMapProvider" SiteMapFilePath="\Orion\APM\IisBlackBox\IisBlackBox.sitemap" />
    <apm:ViewTitle runat="server" ID="title" />
	<%if (ViewInfo.ViewKey != SolarwindsApmSubviewKey)
      { %>
		<div class="period" style="">        
		   <apm:TimePeriodPicker runat="server" id="TimePeriodPicker" TimePeriodControlSubmitBehaviorMode="Redirect" MinNamedPeriodValue="LastHour" />
		</div>  
    <% } %>    
</asp:Content>

<asp:Content ID="cntMain" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<iisbb:IisApplicationPoolResourceHost ID="resHost" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</iisbb:IisApplicationPoolResourceHost>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="OutsideFormPlaceHolder" ID="contentNote">
    <orion:Include ID="I1" File="APM/APM.css" runat="server" />
    <div class="contentNote">
        <%= Resources.APM_IisBBContent.ExpertKnowledgeAttribution %>
    </div>
</asp:Content>
