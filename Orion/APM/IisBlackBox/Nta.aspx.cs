using System;

using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_APM_IisBlackBox_Nta : ApmViewPage, IDynamicInfoProvider
{
	public override string ViewType
	{
		get { return "NTA Subview"; }
	}

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public object GetValue(string key)
    {
        throw new NotImplementedException();
    }
}
