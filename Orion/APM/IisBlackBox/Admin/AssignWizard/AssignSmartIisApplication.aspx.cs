﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Common.Models;
using SolarWinds.APM.BlackBox.IIS.Web.DAL;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;

public partial class Orion_APM_IisBlackBox_Admin_AssignWizard_AssignSmartIisApplication : WizardPage<AssignSmartIisApplicationWorkflow>, IPostBackEventHandler
{
	#region Fields and Properties

	private static readonly Log log = new Log();

	protected string TemplateName
	{
		get { return Workflow.SelectedTemplate.Name; }
	}

	private SelectNodeTreeItem _selectedNode;
	private SelectNodeTreeItem SelectedNode
	{
		get
		{
			if (this._selectedNode == null)
			{
				var node = new SelectNodeTreeItem(this.serverIP.SelectedNode);
				var selectedCredSetId = this.selectCredentials.SelectedCredentialSetId;
				if (selectedCredSetId != ComponentBase.NewCredentialsId)
				{
					node.CredentialId = selectedCredSetId;
				}

				this._selectedNode = node;
			}

			return this._selectedNode;
		}
	}

	private string SelectedPsUrl
	{
		get
		{
			if (string.IsNullOrWhiteSpace(psUrl.Text))
			{
				return Constants.DefaultPsUrlWindows;
			}
			return psUrl.Text;
		}
	}

	private Settings TestJobSettings
	{
		get { return new Settings { NodeIpAddress = serverIP.SelectedNode.IpAddress, PsUrlWindowsValue = SelectedPsUrl }; }
	}

	#endregion

	#region Events handlers

	protected void Page_Load(object sender, EventArgs e)
	{
	    CredentialsValidationSummary.Visible = false;

	    if (!IsPostBack)
	    {
	        psUrl.Text = SelectedPsUrl;
	        if (Workflow.SelectedNodesInfo != null &&
	            Workflow.SelectedNodesInfo.Count > 0)
	        {
	            if (Workflow.SelectedNodesInfo.Count > 1)
	            {
	                log.Debug("Found more than one nodes in workflow state, using just first, because the wizard can assign only one at a time, ignoring the rest.");
	            }
	            var nodeInfo = Workflow.SelectedNodesInfo[0];
	            serverIP.RestoreSelectedNodeById(nodeInfo.Id);
	        }
	        selectCredentials.ReloadCredentialSets();
	    }
	    selectCredentials.ValidationSummaryParentClientID = CredentialsValidationSummary.ClientID;

	    if (IsPostBack)
	        return;

	    Workflow.HasStarted = true;
	    var template = GetTemplate();
	    Workflow.SelectedTemplate = new TemplateInfo(template.Id);

	    this.GetNodeFromRequest();

	    imgbNext.AddEnterHandler(0);
	}

    private static  ApplicationTemplate GetTemplate()
    {
        using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            var template = businessLayer.GetApplicationTemplateByCustomApplicationType(SolarWinds.APM.BlackBox.IIS.Common.Constants.ApplicationTemplateCustomType);
            if (template == null)
                throw new ApplicationException("Could not found AppInsight for IIS template.");
            return template;
        }
    }

    private void GetNodeFromRequest()
    {
        var node = this.Request.QueryString["Node"];
        var address = this.Request.QueryString["Address"];
        int nodeId;

        if (String.IsNullOrEmpty(node)) return;
        if (String.IsNullOrEmpty(address)) return;
        if (!Int32.TryParse(node, out nodeId)) return;

        this.serverIP.SelectedNode = GetNode(nodeId, address);
    }

	protected void OnTest(object sender, EventArgs e)
	{
		HideTestInfo();
		Validate(PageValidationSummary.ValidationGroup);
		if (!Page.IsValid)
		{
			MoveInvalidValidators(PageValidationSummary.ValidationGroup, ValidationSummary2.ValidationGroup);
		}
		else
		{
			Validate(selectCredentials.ValidationGroupName);
			if (selectCredentials.IsValid)
			{
				TestBlackBoxIisConnection();
			}
		}
	}

	protected void OnCancel(object sender, EventArgs e)
	{
		CancelWizard();
	}

	protected override void CancelWizard()
	{
		base.CancelWizard();
		Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx", true);
	}

	protected void OnNext(object sender, EventArgs e)
	{
		HideTestInfo();
		Validate(PageValidationSummary.ValidationGroup);
		Validate(selectCredentials.ValidationGroupName);
		if (Page.IsValid && selectCredentials.IsValid)
		{
			var testResult = TestBlackBoxIisConnection();
			if (testResult.IsServiceRunning)
			{
				CreateApplication(testResult);
				GotoNextPage();
			}
		}
	}

	#endregion

	#region Privete methods

	private void MoveInvalidValidators(string sourceValidationGroup, string targetValidationGroup)
	{
		var invalidValidators = GetValidators(sourceValidationGroup)
			.OfType<IValidator>()
			.Where(v => !v.IsValid);
		foreach (var val in invalidValidators)
		{
			Page.Validators.Remove(val);
			AddErrorMessage(val.ErrorMessage, targetValidationGroup);
		}
	}
    
	protected TestConnectionResult TestBlackBoxIisConnection()
	{
		var res = GetTestBlackBoxIisConnectionResult();
		testFailedNode.Visible = !res.IsValidCredential;
		if (!testFailedNode.Visible)
		{
            if (!res.IsValidPsUrlWindows)
            {
                testFailedPs.Visible = true;
                testFailedPsLiteral.Text = res.ErrorMessage;
            }
            else
            {
                testSuccessful.Visible = res.IsServiceRunning;
                testFailedIIS.Visible = string.IsNullOrEmpty(res.IISVersion);
            }
		}
		selectCredentials.PreFillPasswords();
		panelWizardCredentials.Update();
		return res;
	}

	private static TestConnectionResult GetTestBlackBoxIisConnectionResult(int nodeId, Settings settings, CredentialSet credentials, List<string> errors)
	{
        var settingFromTemplate = ApplicationSettings.DefaultForBlackBoxAssignTemplateWizards;
        
        //get true settings from template
        var template = GetTemplate();
	    settingFromTemplate.Use64Bit = template.Use64Bit;
	    settingFromTemplate.ExecutePollingMethod = template.ExecutePollingMethod;
        
		using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			try
			{
                BusinessLayerTaskInfo<string> result;
			    result = bl.TestBlackBoxConnection(nodeId, credentials, settingFromTemplate, Constants.ApplicationTemplateCustomType, settings.ToXmlString());

			    if (result != null)
				{
					if (result.Result != null)
					{
						return TestConnectionResult.FromXmlString(result.Result);
					}
					if (errors != null)
					{
						errors.AddRange(result.Errors.Select(e => e.Message));
					}
				}
			}
			catch (Exception xcp)
			{
				log.Error(xcp);
				if (errors != null)
				{
					errors.Add(Resources.APM_IisBBContent.AssignWizard_ServerCredentialsError);
				}
			}

			return new TestConnectionResult
				{
					IISVersion = null
				};
		}
	}

	/// <summary>
	/// Tests Iis connection using filled values and returns true when the test was successful.
	/// </summary>
    private TestConnectionResult GetTestBlackBoxIisConnectionResult()
	{
		var errors = new List<string>();
		var result = GetTestBlackBoxIisConnectionResult(SelectedNode.Id, TestJobSettings, selectCredentials.SelectedCredentialSet, errors);
		errors.ForEach(error => AddErrorMessage(error, selectCredentials.ValidationGroupName));
		return result;
	}

	/// <summary>
	/// Adds error message within validator messages
	/// </summary>
	private void AddErrorMessage(string message, string validationGroupName)
	{
		var err = new CustomValidator { ValidationGroup = validationGroupName, IsValid = false, ErrorMessage = message };
		Page.Validators.Add(err);
	}

	/// <summary>
	/// Creates IIS BB application after the connection test using filled values.
	/// </summary>
	private void CreateApplication(TestConnectionResult testResult)
	{
		Workflow.SelectedNodesInfo = new List<SelectNodeTreeItem> { SelectedNode };

		if (selectCredentials.SelectedCredentialSetId == ComponentBase.NewCredentialsId)
		{
			Workflow.UseCredentials(selectCredentials.SelectedCredentialSet);
		}
		else if (selectCredentials.SelectedCredentialSetId == ComponentBase.NodeWmiCredentialsId)
		{
			Workflow.InheritCredentials();
		}
		else
		{
			Workflow.UseCredentials(new List<SelectNodeTreeItem> { SelectedNode });
		}

		var settings = TestJobSettings;
		settings.IisVersionValue = testResult.IISVersion;
		settings.WindowsServices = testResult.RunningWindowsServices;
	    settings.WindowsFeatures = testResult.InstalledWindowsFeatures;
        settings.IsPowerShellInstalledValue = testResult.IsPowerShellInstalled;
		Workflow.IisSettings = settings;
		Workflow.CreateApplications();
	}

	private void HideTestInfo()
	{
		testSuccessful.Visible = false;
		testFailedIIS.Visible = false;
		testFailedNode.Visible = false;
        testFailedPs.Visible = false;
	}

	#endregion

	#region Web methods

    [WebMethod(EnableSession = true)]
    public static bool InstanceExists(int nodeId, string ipAddress)
    {
        var node = GetNode(nodeId, ipAddress);
        return (node != null) && ServiceLocatorForWeb.GetServiceForWeb<IIisApplicationDAL>().ApplicationExists(node.Id);
    }

    [WebMethod(EnableSession = true)]
    public static TestConnectionResult ConnectionWorks(int nodeId, string ipAddress, string psUrl, int credId, string credUser, string credPassword)
    {
        Node node = GetNode(nodeId, ipAddress);

	    if (node == null)
		{
			throw new Exception(Resources.APM_IisBBContent.ApmWeb_SelNodeNotFound);
		}

		var errors = new List<string>();

        var result = GetTestBlackBoxIisConnectionResult(node.Id, new Settings { NodeIpAddress = node.IpAddress, PsUrlWindowsValue = psUrl },
            new CredentialSet { Id = credId, UserName = credUser, Password = credPassword}, errors);
		if (errors.Count > 0)
		{
			throw new Exception(string.Join("<br />", errors));
		}
		return result;
	}

	#endregion

	protected class PagePostbackEvents
	{
		public const string Next = "Next";
	}

	public void RaisePostBackEvent(string eventArgument)
	{
		if (eventArgument == PagePostbackEvents.Next)
		{
			OnNext(this, new EventArgs());
			return;
		}
		throw new NotImplementedException();
	}
}
