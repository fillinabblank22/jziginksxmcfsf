﻿<%@ Page Title="Assign Application Monitor" Language="C#" MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master"
    AutoEventWireup="true" CodeFile="AssignSmartIisApplication.aspx.cs" Inherits="Orion_APM_IisBlackBox_Admin_AssignWizard_AssignSmartIisApplication" %>

<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectServerIp" Src="~/Orion/APM/Controls/SelectServerIp.ascx" %>
<%@ Register TagPrefix="apm" TagName="ImageTooltip" Src="~/Orion/APM/Controls/ImageTooltip.ascx" %>

<asp:content id="Content1" contentplaceholderid="wizardContentPlaceholder" runat="Server">
    <orion:IncludeExtJs runat="server" debug="false" Version="3.4"/>
    <orion:Include runat="server" File="APM/js/jquery.validate.js" SpecificOrder="4" />
    <orion:Include runat="server" File="Nodes/js/TimeoutHandling.js" />
    <orion:Include runat="server" File="APM/IisBlackBox/js/Resources.js" />
    <orion:Include runat="server" File="APM/IisBlackBox/Styles/AssignWizard.css" />

    <script type="text/javascript">
        APMjs.initGlobal('IISBB_AssignWizardPage', function (module, APMjs) {
            'use strict';
            var $ = APMjs.assertQuery(),
                Ext = APMjs.assertExt(),
                isDemoMode = APMjs.assertGlobal('IsDemoMode'),
                demoModeMessage = APMjs.assertGlobal('DemoModeMessage'),
                ctlId = {
            	progress: "#<%= TestInProgress.ClientID %>",

                    panel: '#<%= panelWizardCredentials.ClientID %>',
                    serverIp: '#<%= serverIP.ClientID %>',

                    psUrl: '#<%= psUrl.ClientID %>',

                credential: "#<%= selectCredentials.ClientID %>_CredentialSetList",
                user: "#<%= selectCredentials.ClientID%>_UserName",
                pass: "#<%= selectCredentials.ClientID%>_Password",
            	credentialValidationGroup: "<%= selectCredentials.ValidationGroupName %>"
                },
                msg = {
            	loadTest: "<%= Resources.APMWebContent.ApmWeb_TestingProgress %>",
            	loadValidateAndAssign: "<%= Resources.APM_IisBBContent.ApmWeb_ValidatingAndAssigningApplication %>",

            	errorTitle: "<%= Resources.APM_IisBBContent.ZeroConfig_EditPage_ConnError %>",
            	errorNode: "<%= Resources.APM_IisBBContent.ApmWeb_SelNodeNotFound %>",
            	errorCred: "<%= Resources.APM_IisBBContent.ZeroConfig_EditPage_CredErrorMsg %>",
            	errorIisVersion: "<%= Resources.APM_IisBBContent.ApmWeb_FailedDetectIisVersion %>",
            	errorIisTools: "<%= Resources.APM_IisBBContent.ZeroConfig_EditPage_PowerShellWebAdministrationFailed %>",
            	errorIisPsValidation: "<%= Resources.APM_IisBBContent.ApmWeb_IisNoPowershellWarningMessage %>",
                interrogativeMessage: "<%= Resources.APM_IisBBContent.ApmWeb_IisModuleWarningInterrogativeMessage %>",
                    duplicateFound: '<%= Resources.APM_IisBBContent.AssignWizard_AlreadyMonitored %>',
                    duplicateErrorTitle: '<%= Resources.APM_IisBBContent.ApmWeb_ErrorCheckingDuplicates %>'
                };

            function progressVisibility(show, message) {
                var panel = $(ctlId.progress);
    		panel.find("span").html(message || msg.loadTest);
                if (show) {
                    panel.show();
                } else {
                    panel.hide();
                }
            }
            function hideErrors() {
                progressVisibility(false);
                $(ctlId.panel).find('.testInfo').hide();
            }
            function validate() {
    		return Page_ClientValidate("") && ($(ctlId.credential).val() !== "-1" || Page_ClientValidate(ctlId.credentialValidationGroup));
            }

            function onTestConnectionSuccess(rawResponse) {
    		var data = rawResponse.d, success = function () { eval("<%= Page.ClientScript.GetPostBackEventReference(this, PagePostbackEvents.Next) %>"); };

    		if (data.IsValidCredential == false || data.IISVersion == null) {
    			progressVisibility(false);

    			Ext.Msg.show({ title: msg.errorTitle, msg: msg.errorIisVersion, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR });
                    return false;
                }

    		var error = null;
    		if (data.IsValidPsUrlWindows == false && data.IsPowerShellWebAdministrationInstalled == false) {
    			error = APMjs.format("{0}<br/>{1}", msg.errorIisTools, msg.errorIisPsValidation);
    		} else if (data.IsPowerShellWebAdministrationInstalled == false) {
    		    error = APMjs.format("{0}<br/>{1}", msg.errorIisTools, msg.interrogativeMessage);
    		} else if (data.IsValidPsUrlWindows == false) {
    			error = APMjs.format("{0}<br/><br/>{1}", msg.errorIisPsValidation, data.ErrorMessage);
                }
    		if (error) {
    			progressVisibility(false);

    			Ext.Msg.show({ title: msg.errorTitle, msg: error, fn: function (btn) { if (btn == "yes") { success(); } }, buttons: Ext.Msg.YESNO, icon: Ext.Msg.QUESTION });
                    return false;
                }

                success();
                return false;
            }
            function onTestConnectionError(response) {
                progressVisibility(false);

    		var message = response.responseText;
                if (message) {
    			var ex = JSON.parse(message);
    			if (ex && ex.Message) { message = ex.Message; }
                    }
    		Ext.Msg.show({ title: msg.errorTitle, msg: message, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR });
                return false;
            }
    	function onTestConnection() {

                progressVisibility(true, msg.loadValidateAndAssign);
    		var data = {
                    nodeId: $(ctlId.serverIp + '_selectedNodeId').val(),
                    ipAddress: $(ctlId.serverIp + '_targetServer').val(),
                    psUrl: $(ctlId.psUrl).val(),
                    credId: $(ctlId.credential).val(),
                    credUser: $(ctlId.user).val(),
                    credPassword: $(ctlId.pass).val()
                };
                $.ajax({
                    type: 'POST',
                    url: 'AssignSmartIisApplication.aspx/ConnectionWorks',
                    data: Ext.encode(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: onTestConnectionSuccess,
                    error: onTestConnectionError
                });
            }

            function onTestDuplicateInstanceSuccess(rawResponse) {
                var data = rawResponse.d;
                if (data) {
                    progressVisibility(false);
    			Ext.Msg.show({ msg: msg.duplicateFound, buttons: Ext.Msg.YESNO, fn: function (btn) { if (btn == 'yes') { onTestConnection(); } } });
                } else {
                    onTestConnection();
                }
            }
            function onTestDuplicateInstanceError(response) {
                progressVisibility(false);
    		Ext.Msg.show({ title: msg.duplicateErrorTitle, msg: response.statusText, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR });
            }
            function onTestDuplicateInstance() {
                progressVisibility(true, msg.loadValidateAndAssign);
    		var data = {
                    nodeId: $(ctlId.serverIp + '_selectedNodeId').val(),
                    ipAddress: $(ctlId.serverIp + '_targetServer').val()
                };
                $.ajax({
                    type: 'POST',
                    url: 'AssignSmartIisApplication.aspx/InstanceExists',
                    data: Ext.encode(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: onTestDuplicateInstanceSuccess,
                    error: onTestDuplicateInstanceError
                });
            }

        module.hideErrors = hideErrors;
        module.nextClick = function () {
                if (isDemoMode()) {
                    return demoModeMessage();
                }
                hideErrors();
                if (validate()) {
                    onTestDuplicateInstance();
                }
                return false;
        };
        });
    </script>

    <h2><%= Resources.APM_IisBBContent.AssignWizard_Title %></h2>
    <p><%= Resources.APM_IisBBContent.AssignWizard_InstanceDescription %></p> <br />
    <p class="note">
        <%= Resources.APMWebContent.APMWEBDATA_VB1_242 %>
        <a id="addLink" href="/Orion/Nodes/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_243 %></a>
    </p>
    <div id="selectIisServerStep">
        <ul id="selectIisServer">
            <li><asp:ValidationSummary ID="PageValidationSummary" runat="server" /></li>
            <li>
                <label><%=  Resources.APMWebContent.APMWEBDATA_AK1_117 %></label>
                <apm:SelectServerIp ID="serverIP" runat="server" ValidateVmware="false" CssClass="apm_IisTarget" />
            </li>

            <li style="margin-bottom: 5px;">
                <label><%= Resources.APM_IisBBContent.AssignWizard_PowerShellWindowsUrl%></label>
                <asp:TextBox ID="psUrl" runat="server" CssClass="apm_IisUrls" />
                <asp:RequiredFieldValidator ID="psUrlValidator" ControlToValidate="psUrl" ErrorMessage="<%$ Resources: APM_IisBBContent, AssignWizard_ErrorPowerShellEnterUrl %>" runat="server">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="psUrlRexExpValidator" ControlToValidate="psUrl" ErrorMessage="<%$ Resources: APM_IisBBContent, AssignWizard_ErrorPowerShellInvalidUrl %>" ValidationExpression="^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+|(\$\{IP\}))(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$" runat="server">*</asp:RegularExpressionValidator>

                <apm:ImageTooltip Text="<%$ Resources: APM_IisBBContent, AssignWizard_PowerShellInfoTooltip %>" runat="server" />
            </li>

            <li id="exchangeServerLink" style="margin: 0;">
                <a style="font-size: 11px; padding-left: 190px; color: #369;" href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("SAMAGAppIISHelpMeFindTheseURLSettings") %>" target="_blank">&#0187;<%= Resources.APM_IisBBContent.AssignedAppWizard_HelpLink %></a>
            </li>

            <li>
                <label><%= Resources.APM_IisBBContent.AssignWizard_AdminCredsTitle %></label>
                <asp:UpdatePanel runat="server" ID="panelWizardCredentials" UpdateMode="conditional" Class="credentialsContainer ie7addLayout">
                    <ContentTemplate>
                        <table class="selectCreds">
                            <apm:SelectCredentials runat="server" ID="selectCredentials" ValidationGroupName="SelectCredentialsValidationGroup" AllowNodeWmiCredential="True" />
                            <tr><td colspan="2">
                                    <asp:ValidationSummary ID="CredentialsValidationSummary" runat="server" ValidationGroup="SelectCredentialsValidationGroup" />
                            </td></tr>
                        </table>
                        <div class="testRow" style="padding-left: 190px; padding-top: 15px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SelectCredentialsValidationGroup" />
                                        <orion:LocalizableButton runat="server" ID="TestButton" Text="<%$ Resources: APM_IisBBContent, AssignWizard_TestConnectionButton %>" DisplayType="Secondary" OnClick="OnTest" OnClientClick="window.IISBB_AssignWizardPage.hideErrors()" />
                                    </td>
                                    <td style="padding-left: 10px">
                                        <asp:UpdateProgress runat="server" ID="TestInProgress" DynamicLayout="true" DisplayAfter="0">
                                            <ProgressTemplate>
                                                <img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                                <span>
													<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: APMWebContent, ApmWeb_TestingProgress %>" />
                                                </span>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                        <div runat="server" id="testSuccessful" class="testInfo testSuccessful" visible="false">
                                            <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                                            <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_55 %>" />
                                        </div>
                                        <div runat="server" id="testFailedNode" class="testInfo testFailed" visible="false">
                                            <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                            <asp:Literal runat="server" Text="<%$ Resources: APM_IisBBContent, AssignWizard_ServerCredentialsError %>" />
                                        </div>
                                        <div runat="server" id="testFailedIIS" class="testInfo testFailed" visible="false">
                                            <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: APM_IisBBContent, AssignWizard_TestVersionError %>" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div runat="server" id="testFailedPs" class="testInfo testFailed" visible="false">
                                            <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                            <asp:Literal ID="testFailedPsLiteral" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </li>
        </ul>

        <div class="credentialTips">
            <div class="CredentialTipsInstructions" runat="server" id="credentialsContainer" style="background-color: #FFFDCC; border-color: #EACA7F;">
                <h2><b><%= Resources.APM_IisBBContent.AssignedAppWizard_ConfigIISServerTitle %></b></h2>
                <label>
                    <%= Resources.APM_IisBBContent.AssignedAppWizard_ConfigIISServerDescription %>
                </label>
                <br />
                <label><a style="color: #369;" href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("SAMAGAppInforIISManuallyEnable") %>" target="_blank">&#0187;<%= Resources.APM_IisBBContent.AssignedAppWizard_ConfigIISServerHelpLink %></a></label>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APM_IisBBContent, AssignWizard_ApplicationMonitorButton %>" DisplayType="Primary" OnClientClick="return window.IISBB_AssignWizardPage.nextClick();" />
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false" />
    </div>
</asp:content>
