﻿MakeNamespace("SW.APM.IisBB");
SW.APM.IisBB.ConfigExecutor = function () {
    var loadElement, statusElement, errorElement;
    var mUrl = "/Orion/APM/IisBlackBox/Services/IisServerConfiguratorServices.asmx";
    var mGetStateTimeoutId = null;
    var that;
    var exitCodeOk = 0;
    var mState = null;
    var newCredentialsHandler;
    var mAppId;
    var onConfigurationSuccess;
    var onConfigurationFail;
    var confErrMsgContainer = "iisConfiguratorErrorMessageContainer";
    var confErrMsg = "iisConfiguratorErrorMessage";

    var onGetSetupApplicationState = function (state) {
        mState = state;
        if (state != null && state.CredentialId != 0) {
            that.CredentialId = state.CredentialId;
        }

        if (mState == null || mState.ExitCode == null) {
            mGetStateTimeoutId = setTimeout(function () {
                var key;
                if (mState == null) {
                    key = "SolarWinds.APM.RemoteIISConfiguratorFull.exe-" + that.NodeIp;
                } else {
                    key = mState.Key;
                }
                that.CallWebService("GetSetupApplicationStatus", { "executeKey": key, "appId": mAppId }, onGetSetupApplicationState);
            }, 10 * 1000);
            return;
        }
        var message;
        if (mState.ExitCode == exitCodeOk) {
            newCredentialsHandler();
            onConfigurationSuccess();
        } else {
            if (mState.LogMessages.length == 1) {
                message = mState.LogMessages[0];
            } else {
                if (mState.ExitCode < 0) {
                    mState.ExitCode = 0xFFFFFFFF + mState.ExitCode + 1;
                }
                message = SF("Failed with error code: 0x{0}", mState.ExitCode.toString(16).toUpperCase());
            }
            newCredentialsHandler();
            onConfigurationFail();
            that.ToggleError(message);
        }
    };
    
    this.CredentialId = null;
    this.PowerShellHelpLink = null;
    this.NodeIp = null;
    this.IisVersion = null;    
    this.Init = function (loadElementId, statusElementId, errorElementId, appId) {
        mAppId = appId;
        loadElement = loadElementId;
        statusElement = statusElementId;
        errorElement = errorElementId;
        that = this;
    };
    this.ToggleLoad = function (show) {
        var el = $("#" + loadElement);
        if (show) { el.show(); }
        else { el.hide(); }
    };
    this.ToggleStatus = function (message) {

        $("#" + statusElement).html(message);
        if (mGetStateTimeoutId) {
            clearTimeout(mGetStateTimeoutId);
        }
        mGetStateTimeoutId = null;
    };

    this.ToggleError = function (msg, title) {
        if (title == null) {
            title = "@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_ServerConfigurationFailed_ErrorTitle;E=js}";
        }
        
        if (msg == "" || !msg || msg == 'undefined') {
            $("#" + confErrMsgContainer).hide();
            $("#" + confErrMsg).html("");
        } else {
            var message = "<b>" + title + "</b> </br>" + msg;
            $("#" + confErrMsgContainer).show();
            $("#" + confErrMsg).html(message);
            that.ToggleLoad(false);
            
            if (onConfigurationFail) onConfigurationFail();
        }

        if (mGetStateTimeoutId) {
            clearTimeout(mGetStateTimeoutId);
        }
        mGetStateTimeoutId = null;
    };
    this.ToggleTestError = function (msg, machine, appId) {
        $(".connection-fail").show();
        setTimeout(function () {
            if (msg == "" || !msg || msg == 'undefined') {
                $("#" + confErrMsgContainer).hide();
                $("#" + confErrMsg).html("");
            } else {
                var message = "<b>" + "@{R=APM.Strings;K=APMWEBJS_ConnectionTestFailed;E=js}" + "</b> </br>" + "@{R=APM.Strings;K=APMWEBJS_CouldNotConnectTo;E=js}" + " " + machine + ". " + msg;

                if (appId) message += " " + "@{R=APM.Strings;K=APMWEBJS_IfYouAreUsingUserDefinedConnection;E=js}" + " <a href=\"/Orion/APM/Admin/Edit/EditApplication.aspx?id=" + appId + "\">»Edit Application</a>";

                $("#" + confErrMsgContainer).show();
                $("#" + confErrMsg).html(message);
                that.ToggleLoad(false);
                onConfigurationFail();
            }
        }, 2 * 1000);
        if (mGetStateTimeoutId) {
            clearTimeout(mGetStateTimeoutId);
        }
        mGetStateTimeoutId = null;
    };
    this.CallWebService = function (method, args, success, fail) {
        if (!fail) {
            fail = function (message) {
                that.ToggleLoad(false); that.ToggleError(message);
            };
        }
        SW.Core.Services.callWebService(mUrl, method, args, success, fail);
    };

    this.TryGetSetupApplicationState = function (nodeIp, selectCredentialsHandler, newCredHandler, confSuccessHandler, confFailHandler) {
        newCredentialsHandler = newCredHandler;
        onConfigurationSuccess = confSuccessHandler;
        onConfigurationFail = confFailHandler;

        that.ToggleLoad(false);
        that.ToggleStatus("");
        that.ToggleError("");

        var success = function(state) {
            if (state == null) {
                that.ToggleLoad(false);
                confFailHandler();
                return;
            }
            if (state.ExitCode == null) {
                that.ToggleLoad(true);
            }
            selectCredentialsHandler(state.CredentialId);
            onGetSetupApplicationState(state);
        };
        var fail = function(message) {
            that.ToggleLoad(false);
            that.ToggleError(message);
            confFailHandler();
        };
        that.CallWebService("TryGetSetupApplicationStatus", { nodeIp: nodeIp }, success, fail);
    };

    this.StartConfigurator = function (mAppInfo, cred, newCredHandler, confSuccessHandler, confFailHandler) {
        newCredentialsHandler = newCredHandler;
        onConfigurationSuccess = confSuccessHandler;
        onConfigurationFail = confFailHandler;
        if (mGetStateTimeoutId == null) {
            that.ToggleLoad(true);
            that.ToggleStatus("");
            that.ToggleError("");

            var params = {
                nodeName: mAppInfo.NodeIp,
                nodeIp: mAppInfo.NodeIp,
                nodeId: mAppInfo.NodeId,
                cred: cred,
                useAgent: mAppInfo.UseAgent
            };
            that.CallWebService("SetupApplication", params, onGetSetupApplicationState);
        }
        return false;
    };
};