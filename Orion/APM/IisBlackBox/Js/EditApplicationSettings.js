/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.withGlobal('SW.APM.BB.IIS', function (IisBB, APMjs) {
    'use strict';

    var
        // module references
        SW = APMjs.assertGlobal('SW'),
        Ext = APMjs.assertExt(),
        SF = APMjs.format,
        $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        EditBbApp = APMjs.assertGlobal('SW.APM.EditBbApp'),
        findSetting = EditBbApp.findSetting,
        SETT = {},
        LoadedCredentials,
        ServiceUrl = {
            IisConfigurator: '/Orion/APM/IisBlackBox/Services/IisServerConfiguratorServices.asmx'
        },
        iisVersion,
        appSet;

    // SW.APM.BB.IIS.EditAppSettings -->
    APMjs.initGlobal('SW.APM.BB.IIS.EditAppSettings', function (IisBbEditApp) {

        function init(data) {
            var s = SETT;
            s.AppType = data.AppType;
            s.PsUrlWindowsKey = data.PsUrlWindowsKey;
            s.SettingLevelInstance = parseInt(data.SettingLevelInstance, 10);
            s.AgentPollingMethodName = data.AgentPollingMethodName;
            s.CredentialSetIdKey = data.CredentialSetIdKey;
            s.TestSummaryClientId = data.TestSummaryClientId;
            s.TestButtonClientId = data.TestButtonClientId;
            s.ConfigButtonClientId = data.ConfigButtonClientId;
            s.WindowsTooltipClientId = data.WindowsTooltipClientId;
            s.WindowsTooltipContent = data.WindowsTooltipContent;
            s.SaveCredentialChangesTitle = data.SaveCredentialChangesTitle;
            s.SaveCredentialChangesMsg = data.SaveCredentialChangesMsg;

            s.MsgTestSuccessful = data.MsgTestSuccessful;
            s.MsgCredTestFailed = data.MsgCredTestFailed;
            s.MsgWebAdminFailed = data.MsgWebAdminFailed;
            s.MsgPsUrlWindowsFailed = data.MsgPsUrlWindowsFailed;
            s.MsgVersionCheckFailed = data.MsgVersionCheckFailed;
            s.MsgZeroConfigConnError = data.MsgZeroConfigConnError;
            s.MsgZeroConfigTestConf = data.MsgZeroConfigTestConf;
            s.TitleZeroConfigTestConfFailed = data.TitleZeroConfigTestConfFailed;
            s.MsgZeroConfigTestCred = data.MsgZeroConfigTestCred;
            s.TitleZeroConfigTestCredFailed = data.TitleZeroConfigTestCredFailed;
            s.MsgZeroConfigPsAlreadyPrepared = data.MsgZeroConfigPsAlreadyPrepared;
            s.MsgZeroConfigCredError = data.MsgZeroConfigCredError;
            s.MsgZeroConfigCompatibilityFailed = data.MsgZeroConfigCompatibilityFailed;
            s.TitleZeroConfigCompatibilityFailed = data.TitleZeroConfigCompatibilityFailed;
            s.MsgConfiguringServer = data.MsgConfiguringServer;
        }

        IisBbEditApp.init = init;
    });
    // <-- SW.APM.BB.IIS.EditAppSettings

    function getTestSettingsFromUi(testingNodeId) {
        var jSettings, result, s;

        jSettings = $('#appEditIisSettings');

        result = {
            NodeId: testingNodeId,
            AppType: SETT.AppType,
            CredentialSet: {
                Id: jSettings.find('select[id$=iisCredentials_CredentialSetList]').val(),
                Login: jSettings.find('input[id$=iisCredentials_UserName]').val(),
                Password: jSettings.find('input[id$=iisCredentials_Password]').val()
            },
            CustomSettings: {}
        };

        s = result.CustomSettings;
        s[SETT.PsUrlWindowsKey] = jSettings.find('#psUrlWindows').val();

        EditApp.updateCommonTestSettings(result);

        return { settings: result };
    }

    function validateUrls(obj, messages) {
        var sett = obj.settings.CustomSettings,
            errors = [];

        EditBbApp.validateUrl(sett[SETT.PsUrlWindowsKey], errors, $('#psIisUrlWindowsError'));

        if (messages) {
            Array.prototype.push.apply(messages, errors);
        }

        return (errors.length === 0);
    }

    function callTestBlackBoxConnection(settings, onSuccess, onError) {
        SW.Core.Services.callWebService('/Orion/APM/IisBlackBox/Services/TestConnectionServices.asmx', 'TestIisBlackBoxConnection', settings, onSuccess, onError);
    }

    function displayTestSummary(jSumTest, testSuccessfull, msg, showIcon) {
        var jMsg = '', jCustMsg, customMessageCls = 'customMsg', res;

        jSumTest.find('.' + customMessageCls).remove();

        if (EditBbApp.isNonEmptyText(msg)) {
            jMsg = $('<span />').addClass(customMessageCls).html(msg);
        }

        if (testSuccessfull) {
            res = '<img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif"> ' + msg;
            jSumTest.find('.testSuccessful').show().html(res);
            jSumTest.find('.testFailed').hide();
            jSumTest.find('.progress').hide();
        } else {
            jSumTest.find('.testSuccessful').hide();
            jSumTest.find('.testFailed').show().append(jMsg);
            jSumTest.find('.progress').hide();

            jCustMsg = $('.' + customMessageCls);
            res = jCustMsg.html();
            jCustMsg.remove();

            showIcon = (showIcon === undefined) ? true : showIcon;

            if (showIcon) {
                res = '<img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif">' + res;
            }
            jSumTest.find('.testFailed').html(res);
        }
    }

    function displayProgress(jSumTest) {
        jSumTest.find('.testSuccessful').hide();
        jSumTest.find('.testFailed').hide();
        if (!jSumTest.is(':visible')) {
            jSumTest.slideDown();
        }
        jSumTest.find('.progress').show();
    }

    function joinMsg(arr) {
        return arr.join('\n<br />');
    }

    function onTestClick(jSumTest, nodeId) {
        var testSett, testResult, jSettings, msgs;

        function onTestSuccess(result) {

            function onVersionSuccess(version) {
                msgs = [];

                if (version.IsVersionSupported) {
                    if (!result.Result.IsValidCredential) {
                        msgs.push(SETT.MsgCredTestFailed);
                    } else if (!result.Result.IsValidPsUrlWindows) {
                        msgs.push(SETT.MsgPsUrlWinFailed);
                    } else if (!result.Result.IsPowerShellWebAdministrationInstalled) {
                        msgs.push(SETT.MsgWebAdminFailed);
                    }
                    if (result.Result.ErrorMessage) {
                        msgs.push(result.Result.ErrorMessage);
                    }
                } else {
                    msgs.push(SETT.MsgVersionCheckFailed);
                }

                if (msgs.length) {
                    displayTestSummary(jSumTest, false, joinMsg(msgs), false);
                } else {
                    displayTestSummary(jSumTest, true, SETT.MsgTestSuccessful, false);
                }
            }

            if (result && result.Result && result.TestSuccessfull) {
                SW.Core.Services.callWebService(
                    ServiceUrl.IisConfigurator,
                    'CheckIisVersion',
                    { iisVersion: result.Result.IISVersion },
                    onVersionSuccess
                );
            } else {
                msgs = [];
                msgs.push(SETT.MsgCredTestFailed);
                if (result && result.Result) {
                    if (result.Result.ErrorMessage) {
                        msgs.push(result.Result.ErrorMessage);
                    }
                }
                displayTestSummary(jSumTest, false, joinMsg(msgs), true);
            }

            return false;
        }

        function onTestError(error) {
            error = APMjs.trim(error);
            if (!error.length) {
                error = SETT.MsgZeroConfigConnError;
            }
            displayTestSummary(jSumTest, false, error);
        }

        function onVersionSuccess2(result) {
            if (result && result.IsVersionSupported) {
                callTestBlackBoxConnection(testSett, onTestSuccess, onTestError);
                return false;
            }
            displayTestSummary(jSumTest, false, "@{R=APM.Strings;K=APMWEBJS_IisBlackBox_IISVersion7IsRequired;E=js}", false);
        }

        jSettings = $('#appEditIisSettings');
        testSett = getTestSettingsFromUi(nodeId);

        msgs = [];
        testResult = validateUrls(testSett, msgs) && jSettings.closest('form').valid();

        if (testResult) {
            displayProgress(jSumTest);

            if (!appSet.IsNodeAvailable) {
                displayTestSummary(jSumTest, false, appSet.NodeIsNotAvailableMsg, false);
            }

            SW.Core.Services.callWebService(
                ServiceUrl.IisConfigurator,
                'CheckIisVersion',
                { iisVersion: iisVersion },
                onVersionSuccess2
            );
        }

        return false;
    }

    APMjs.initGlobalClass('SW.APM.BB.IIS.TestButtonHandler', function (nodeId, testSummaryId) {
        var jSumTest = $('#' + testSummaryId);

        this.onClick = function () {
            return onTestClick(jSumTest, nodeId);
        };
    });


    function getApplicationSettings(appId) {
        SW.Core.Services.callWebServiceSync(
            ServiceUrl.IisConfigurator,
            'GetIisBbApplicationWithoutCred',
            { appId: appId },
            function (app) {
                appSet = app;
            },
            function (err) {
                APMjs.console.error(err);
                appSet = {};
            }
        );
    }

    function noAction() {
        return false;
    }

    function runTest(nodeId, onSuccess, onFail) {
        $('.connection-success').hide();
        $.ajax({
            type: 'POST',
            url: '/Orion/APM/IisBlackBox/Services/TestConnectionServices.asmx/TestIisBlackBoxConnection',
            data: JSON.stringify(getTestSettingsFromUi(nodeId)),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            timeout: 10 * 60 * 1000,
            success: onSuccess,
            error: onFail
        });
    }

    function runConfigurator(configuratorExecutor, nodeId, xMask) {
        var appInfo, cred, uiSettings, jLoadingMask;

        function onError() {
            xMask.hide();
        }

        function onTestSuccess(result) {
            if (result && result.d && result.d.Result && result.d.Result.IsPowerShellWebAdministrationInstalled) {
                configuratorExecutor.CallWebService(
                    'SetServerConfigurationResult',
                    {
                        appId: appSet.Id,
                        credId: cred.Id,
                        iisVersion: result.d.Result.IISVersion
                    },
                    noAction
                );
                $('.iis-conf-success').show();
                xMask.hide();
            } else {
                xMask.hide();
                configuratorExecutor.ToggleError(
                    SF(result.d.Result.ErrorMessage + ' <a target="_blank" href="{1}">&raquo; ' + "@{R=APM.Strings;K=APMWEBJS_IisBlackBox_HelpMeCorrectThis;E=js}"+'</a>', appInfo.NodeName, appInfo.PowerShellHelpLink),
                    SETT.MsgZeroConfigTestFailed
                );
            }
        }

        function onConfiguratorSuccess() {
            var jLoading;

            xMask.msg = '<label id="configuratorLoadMask">' + SETT.MsgZeroConfigTestConf + '</label>';
            xMask.show();

            jLoading = $('#configuratorLoadMask').parent('div').eq(0);
            jLoading.parent('div').eq(0).css('top', ($(window).height() / 2) + 'px');

            runTest(
                nodeId,
                onTestSuccess,
                onError
            );
        }

        appInfo = {
            NodeName: appSet.NodeName,
            NodeId: appSet.NodeId,
            NodeIp: appSet.NodeIp,
            PowerShellHelpLink: appSet.PowerShellHelpLink,
            UseAgent: appSet.UseAgent
        };

        cred = {
            Id: $('select[id$=iisCredentials_CredentialSetList]').val(),
            Name: $('input[id$=iisCredentials_CredentialName]').val(),
            UserName: $('input[id$=iisCredentials_UserName]').val(),
            Password: $('input[id$=iisCredentials_Password]').val()
        };

        uiSettings = {};
        EditApp.updateCommonTestSettings(uiSettings);
        appInfo.UseAgent = (uiSettings.ExecutePollingMethod === SETT.AgentPollingMethodName);

        xMask.msg = '<label id="configuratorLoadMask">' + SETT.MsgConfiguringServer + '</label>';
        xMask.show();

        jLoadingMask = $('#configuratorLoadMask').parent('div').eq(0);
        jLoadingMask.parent('div').eq(0).css('top', ($(window).height() / 2) + 'px');

        configuratorExecutor.StartConfigurator(appInfo, cred, noAction, onConfiguratorSuccess, onError);
    }

    APMjs.initGlobalClass('SW.APM.BB.IIS.ApplicationEditor', function () {
        var bkpInitialCredSetId = -1,
            settingLevelInstance = SETT.SettingLevelInstance,
            credSetIdElSelector = 'select[id$=iisCredentials_CredentialSetList]';

        function reloadCredentials() {
            SW.APM.SelectCredentials.ReloadCredentialSets(credSetIdElSelector);
        }

        function createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError) {
            var newCredentials;

            function onCredentialsCreated(credentialSetId) {
                credentialSetIdSetting.Value = credentialSetId;
                setTimeout(reloadCredentials, 500);
                onSuccess();
            }

            newCredentials = {
                name: $('input[id$=iisCredentials_CredentialName]').val(),
                userName: $('input[id$=iisCredentials_UserName]').val(),
                psw: $('input[id$=iisCredentials_Password]').val()
            };

            SW.Core.Services.callWebService('/Orion/APM/Services/Credentials.asmx', 'CreateCredential', newCredentials, onCredentialsCreated, onError);

            return true;
        }

        function configureIisServer(item, xMask, jSettings, configuratorExecutor) {
            var testResult = jSettings.closest('form').valid();

            $('.iis-conf-success').hide();
            configuratorExecutor.ToggleError(false);

            function onVersionSuccess(item, xMask, res1, res2) {
                var jLoading;
                if (res2 && res2.IsVersionSupported && res1) {
                    if (res1.IsValidCredential && res1.IsPowerShellWebAdministrationInstalled && res1.IsPowerShellInstalled) {
                        configuratorExecutor.CallWebService(
                            'SetServerConfigurationResult',
                            {
                                appId: appSet.Id,
                                credId: $('select[id$=iisCredentials_CredentialSetList]').val(),
                                iisVersion: res1.IISVersion
                            },
                            noAction
                        );
                        xMask.msg = SF('<label id="configuratorLoadMask">{0}</label>', SETT.MsgZeroConfigPsAlreadyPrepared);
                        xMask.show();
                        jLoading = $('#configuratorLoadMask').parent('div').eq(0);
                        jLoading.parent('div').eq(0).css('top', ($(window).height() / 2) + 'px');
                        setTimeout(function () {
                            $('.iis-conf-success').show();
                            xMask.hide();
                        }, 2000);
                    } else {
                        if (res1.IsValidCredential) {
                            runConfigurator(configuratorExecutor, item.NodeId, xMask);
                        } else {
                            xMask.hide();
                            configuratorExecutor.ToggleError(
                                SF('{0}<a target="_blank" href="{1}">&raquo; ' +"@{R=APM.Strings;K=APMWEBJS_IisBlackBox_HelpMeCorrectThis;E=js}"+'</a>', SETT.MsgZeroConfigCredError, appSet.CredentialsTestHelpLink),
                                SETT.TitleZeroConfigTestCredFailed
                            );
                        }
                    }
                } else {
                    xMask.hide();
                    if (res1) {
                        configuratorExecutor.ToggleError(res2.Message, SETT.MsgZeroConfigCompatibilityFailed);
                    } else {
                        configuratorExecutor.ToggleError(SETT.TitleZeroConfigTestCredFailed);
                    }
                }
            }

            function onCredentialsTestSuccess(item, xMask, result) {
                var res1 = result && result.d && result.d.Result;
                $('.iis-conf-success').hide();
                configuratorExecutor.CallWebService(
                    'CheckIisVersion',
                    { iisVersion: configuratorExecutor.IisVersion },
                    function (res2) {
                        onVersionSuccess(item, xMask, res1, res2);
                    }
                );
            }

            if (testResult) {
                if (appSet.IsNodeAvailable) {
                    runTest(
                        item.NodeId,
                        function (result) {
                            onCredentialsTestSuccess(item, xMask, result);
                        },
                        noAction
                    );
                } else {
                    xMask.hide();
                    configuratorExecutor.ToggleError(appSet.NodeIsNotAvailableMsg, SETT.TitleZeroConfigTestCredFailed);
                }
            }

            return false;
        }

        function initUiState(item) {
            var jSettings = $('#appEditIisSettings'),
                testButtonHandler = new IisBB.TestButtonHandler(item.NodeId, SETT.TestSummaryClientId),
                foundSetting,
                configuratorExecutor,
                xMask;

            function checkIisVersion() {
                var testResult = jSettings.closest('form').valid(), jLoadingMask;
                if (testResult) {
                    xMask = new Ext.LoadMask(
                        Ext.get('container'),
                        { msg: '<label id="configuratorLoadMask">' + SETT.MsgZeroConfigTestCred + '</label>' }
                    );
                    xMask.show();

                    jLoadingMask = $('#configuratorLoadMask').parent('div').eq(0);
                    jLoadingMask.parent('div').eq(0).css('top', ($(window).height() / 2) + 'px');

                    if (APMjs.isSet(configuratorExecutor.IisVersion)) {
                        configuratorExecutor.CallWebService(
                            'CheckIisVersion',
                            { iisVersion: configuratorExecutor.IisVersion },
                            function (res) {
                                if (res && res.IsVersionSupported) {
                                    configureIisServer(item, xMask, jSettings, configuratorExecutor);
                                } else {
                                    configuratorExecutor.ToggleError(res.Message, SETT.TitleZeroConfigCompatibilityFailed);
                                    xMask.hide();
                                }
                            }
                        );
                    } else {
                        configureIisServer(item, xMask, jSettings, configuratorExecutor);
                    }
                }
                return false;
            }

            foundSetting = findSetting('CredentialSetId', item.Settings);
            if (foundSetting && foundSetting.Value) {
                LoadedCredentials = parseInt(foundSetting.Value, 10);
            }

            jSettings.show();
            jSettings.find('#' + SETT.TestButtonClientId).unbind('click').click(testButtonHandler.onClick);

            foundSetting = findSetting(SETT.PsUrlWindowsKey, item.Settings);
            if (foundSetting && foundSetting.Value) {
                jSettings
                    .find("#psUrlWindows")
                    .val(foundSetting.Value);
            }

            foundSetting = findSetting(SETT.CredentialSetIdKey, item.Settings);
            if (foundSetting && foundSetting.Value) {
                jSettings
                    .find('select[id$=iisCredentials_CredentialSetList]')
                    .val(foundSetting.Value)
                    .change();
                bkpInitialCredSetId = parseInt(foundSetting.Value, 10);
            }

            configuratorExecutor = new SW.APM.IisBB.ConfigExecutor();
            configuratorExecutor.Init('.iis-load', '.iis-status', '.iis-error', item.Id);

            if (item.NodeSubType === 'SNMP') {
                $('select[id$=iisCredentials_CredentialSetList] option[value=-3]').remove();
                $('.selectCreds').attr('style', 'width: 360px');
                $('div.testSuccessful').attr('style', 'width: 360px');
                $('div.testFailed').attr('style', 'width: 360px');
            }

            configuratorExecutor.ToggleError(false);

            getApplicationSettings(item.Id);
            EditBbApp.initToolTip(SETT.WindowsTooltipClientId, SETT.WindowsTooltipContent);

            configuratorExecutor.IisVersion = appSet.IisVersion;
            iisVersion = appSet.IisVersion;

            jSettings.find('#' + SETT.ConfigButtonClientId).unbind('click').click(checkIisVersion);
        }

        function updateModelFromUi(item) {
            var foundSetting, validationResult, jSettings, testSett, messages = [];
            testSett = getTestSettingsFromUi(item.NodeId);
            jSettings = $('#appEditIisSettings');

            validationResult = validateUrls(testSett, messages) && jSettings.closest('form').valid();

            if (!validationResult) {
                throw messages;
            }

            foundSetting = findSetting(SETT.CredentialSetIdKey, item.Settings);
            if (foundSetting) {
                foundSetting.SettingLevel = settingLevelInstance;
                foundSetting.Value = jSettings.find('select[id$=iisCredentials_CredentialSetList]').val();
            }

            foundSetting = findSetting(SETT.PsUrlWindowsKey, item.Settings);
            if (foundSetting) {
                foundSetting.SettingLevel = settingLevelInstance;
                foundSetting.Value = jSettings.find('#psUrlWindows').val();
            }
        }

        function asyncPostProcessing(app, template, onSuccess, onError, saveToFormDelegate) {
            var wasCredentialsChanged, credentialSetIdSetting, credentialSetIdValue, testSett, isNewCredAssigned;

            function newCredentialsHandler(isNewCred, connectionSucceeded) {
                if (connectionSucceeded === false && (isNewCred || bkpInitialCredSetId !== credentialSetIdValue)) {
                    Ext.Msg.show({
                        title: SETT.SaveCredentialChangesTitle,
                        msg: SETT.SaveCredentialChangesMsg,
                        buttons: Ext.Msg.OKCANCEL,
                        fn: function (btn) {
                            if (btn === 'ok') {
                                LoadedCredentials = credentialSetIdValue;
                                if (isNewCred) {
                                    createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                                } else {
                                    onSuccess();
                                }
                            } else {
                                onError();
                            }
                        },
                        icon: Ext.MessageBox.WARNING
                    });
                    return true;
                }
                if (isNewCred) {
                    createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                    return true;
                }
                return true;
            }

            credentialSetIdSetting = findSetting(SETT.CredentialSetIdKey, app.Settings);
            if (credentialSetIdSetting && credentialSetIdSetting.Value) {
                credentialSetIdValue = parseInt(credentialSetIdSetting.Value, 10);
                testSett = getTestSettingsFromUi(app.NodeId);
                isNewCredAssigned = credentialSetIdValue === SW.APM.SelectCredentials.NewCredentialId;

                if (LoadedCredentials !== undefined) {
                    wasCredentialsChanged = (LoadedCredentials !== credentialSetIdValue);
                }

                if (isNewCredAssigned || wasCredentialsChanged) {
                    SW.Core.Services.callWebService(
                        '/Orion/APM/IisBlackBox/Services/TestConnectionServices.asmx',
                        'TestIisBlackBoxConnection',
                        testSett,
                        function (result) {
                            var connectionTestSucceeded = result && result.Result && result.Result.IsValidCredential;
                            newCredentialsHandler(isNewCredAssigned, connectionTestSucceeded);
                            if (connectionTestSucceeded && !isNewCredAssigned) {
                                saveToFormDelegate();
                            }
                        },
                        noAction
                    );
                } else {
                    saveToFormDelegate();
                }
            }
            return true;
        }

        this.initUiState = initUiState;
        this.updateModelFromUi = updateModelFromUi;
        this.asyncPostProcessing = asyncPostProcessing;
    });

    Ext.ComponentMgr.registerPlugin('SW.APM.BB.IIS.ApplicationEditor', IisBB.ApplicationEditor);
});
