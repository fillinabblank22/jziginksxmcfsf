/*jslint browser: true, indent: 4, unparam: true*/
/*global APMjs: false*/
APMjs.withGlobal('SW.APM.IisBB', function (iisbb, APMjs, global) {

    var SW = global.SW;

    // private helper methods
    function fnShowLoading(data, message) {
        var inprogress = $('#inprogress-container-' + data.resourceId);
        inprogress.css({ 'display': '' });
        $('#message-' + data.resourceId).html(message);
    }

    function fnHideLoading(data) {
        var inprogress = $('#inprogress-container-' + data.resourceId);
        inprogress.css({ 'display': 'none' });
    }

    function fnHandleResults(data, res, successMsg, errorMsg) {
        var successContainer = $('#managementSucess-' + data.resourceId);
        var errorContainer = $('#managementError-' + data.resourceId);
        if (res.ActionResult === 'Successful') {
            errorContainer.hide();
            successContainer.show();
            $('#manage-action-success-' + data.resourceId).html(successMsg);
            setTimeout(function () {
                successContainer.hide();
                window.location.reload(true);
            }, 2000);
            return;
        }
        errorContainer.show();
        successContainer.hide();
        $('#manage-action-error-' + data.resourceId).html(errorMsg + " " + res.ErrorMessage);
    }

    function fnShowDialog(data, title, message, handler) {
        var mDlgEl = $('#iisConfirmDialog-' + data.resourceId).dialog({
            width: 360,
            position: [$(window).width() / 2 - 255, 100],
            modal: true,
            autoOpen: false,
            overlay: { 'background-color': 'black', opacity: 0.4 },
            title: title
        });

        $('#actionMessage-' + data.resourceId).html(message);
        mDlgEl.dialog('open');

        $('#iisConfirmDialog-' + data.resourceId + ' a[id$=btnYes]')
            .unbind('click')
            .bind('click', function () {
                mDlgEl.dialog('close');
                setTimeout(function () {
                    handler(data);
                }, 20);
                return false;
            });

        $('#iisConfirmDialog-' + data.resourceId + ' a[id$=btnNo]')
            .unbind('click')
            .bind('click', function () {
                mDlgEl.dialog('destroy');
                return false;
            });
    }

    function fnSetBtnVisibility(data) {
        var stopCnt = $('#stop-container-' + data.resourceId),
            startCnt = $('#start-container-' + data.resourceId),
            restartCnt = $('#restart-container-' + data.resourceId);

        switch (data.state) {
        case 'Started':
        case 'Starting':
            stopCnt.show();
            restartCnt.show();
            startCnt.hide();
            break;
        case 'Stopped':
        case 'Stopping':
            stopCnt.hide();
            restartCnt.hide();
            startCnt.show();
            break;
        default:
            break;
        }
    }

    function fnManage(mdata, texts, isDemo) {
        if (isDemo)
            DemoModeAction("AppItemManagement_" + mdata.method);
        else
            fnShowLoading(mdata.data, texts.loading);
            setTimeout(function () {
                SW.Core.Services.callWebService('/Orion/APM/IisBlackBox/Services/IisManagementServices.asmx', mdata.method,
                    mdata.formatter(mdata),
                    function (res) {
                        fnHideLoading(mdata.data);
                        fnHandleResults(mdata.data, res, texts.success, texts.error);
                    });
            }, 20);
    }


    // *** ApplicationPoolManagement
    iisbb.ApplicationPoolManagement = function (initData) {

        function fnPoolFormatter(mdata) {
            return {
                action: mdata.action,
                entityName: mdata.data.poolName,
                nodeId: mdata.data.nodeId,
                appId: mdata.data.appId,
                tempId: mdata.data.tempId,
                poolId: mdata.data.poolId
            };
        }

        function fnManagePool(data, action, texts) {
            var mdata = {
                data: data,
                formatter: fnPoolFormatter,
                action: action,
                method: 'ManageApplicationPool'
            };
            fnManage(mdata, texts, data.isDemo);
        }

        function fnStartPool(data) {
            var texts = {
                loading: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StartAppPoolLoading;E=js}', data.poolName),
                success: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StartAppPoolSuccess;E=js}', data.poolName),
                error: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StartAppPoolError;E=js}', data.poolName)
            };
            fnManagePool(data, 'Start', texts);
        }

        function fnStopPool(data) {
            var texts = {
                loading: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StopAppPoolLoading;E=js}', data.poolName),
                success: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StopAppPoolSuccess;E=js}', data.poolName),
                error: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StopAppPoolError;E=js}', data.poolName)
            };
            fnManagePool(data, 'Stop', texts);
        }

        function fnRestartPool(data) {
            var texts = {
                loading: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_RecycleAppPoolLoading;E=js}', data.poolName),
                success: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_RecycleAppPoolSuccess;E=js}', data.poolName),
                error: String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_RecycleAppPoolError;E=js}', data.poolName)
            };
            fnManagePool(data, 'Restart', texts);
        }

        var data = {
            resourceId: initData.resourceId,
            appId: initData.applicationId,
            tempId: initData.templateId,
            poolName: initData.poolName,
            poolId: initData.poolId,
            nodeId: initData.nodeId,
            show: initData.show,
            state: initData.poolState,
            isDemo: initData.isDemo
        };

        this.setButtonsVisibility = function (state) {
            data.state = state;
            fnSetBtnVisibility(data);
        };

        if (data.show) {
            $('#start-' + data.resourceId).click(function () {
                fnStartPool(data);
            });
            $('#stop-' + data.resourceId).click(function () {
                fnShowDialog(data, '@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StopButtonConfirmDialogHeader;E=js}',
                    String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_StopButtonConfirmDialogMessage;E=js}', data.poolName), fnStopPool);
            });
            $('#restart-' + data.resourceId).click(function () {
                fnShowDialog(data, '@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_RestartButtonConfirmDialogHeader;E=js}',
                    String.format('@{R=APM.BlackBox.IIS.Strings;K=AppPoolManagement_RestartButtonConfirmDialogMessage;E=js}', data.poolName), fnRestartPool);
            });
            fnSetBtnVisibility(data);
        } else {
            $(function () {
                $('#stop-container-' + data.resourceId).hide();
                $('#start-container-' + data.resourceId).hide();
                $('#restart-container-' + data.resourceId).hide();
            });
        }
    };


    // *** SiteManagement
    iisbb.SiteManagement = function (initData) {

        function fnSiteFormatter(mdata) {
            return {
                action: mdata.action,
                entityName: mdata.data.siteName,
                nodeId: mdata.data.nodeId,
                appId: mdata.data.appId,
                tempId: mdata.data.tempId,
                siteId: mdata.data.siteId
            };
        }

        function fnManageSite(data, action, texts) {
            var mdata = {
                data: data,
                formatter: fnSiteFormatter,
                action: action,
                method: 'ManageSite'
            };
            fnManage(mdata, texts, data.isDemo);
        }

        function fnStartSite(data) {
            var texts = {
                loading: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StartSiteLoading;E=js}', data.siteName),
                success: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StartSiteSuccess;E=js}', data.siteName),
                error: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StartSiteError;E=js}', data.siteName)
            };
            fnManageSite(data, 'Start', texts);
        }

        function fnStopSite(data) {
            var texts = {
                loading: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StopSiteLoading;E=js}', data.siteName),
                success: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StopSiteSuccess;E=js}', data.siteName),
                error: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StopSiteError;E=js}', data.siteName)
            };
            fnManageSite(data, 'Stop', texts);
        }

        function fnRestartSite(data) {
            var texts = {
                loading: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_RestartSiteLoading;E=js}', data.siteName),
                success: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_RestartSiteSuccess;E=js}', data.siteName),
                error: String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_RestartSiteError;E=js}', data.siteName)
            };
            fnManageSite(data, 'Restart', texts);
        }

        var data = {
            resourceId: initData.resourceId,
            appId: initData.applicationId,
            tempId: initData.templateId,
            siteName: initData.siteName,
            siteId: initData.siteId,
            nodeId: initData.nodeId,
            show: initData.show,
            state: initData.poolState,
            isDemo: initData.isDemo
        };

        this.setButtonsVisibility = function (state) {
            data.state = state;
            fnSetBtnVisibility(data);
        };

        if (data.show) {
            $('#start-' + data.resourceId).click(function () {
                fnStartSite(data);
            });
            $('#stop-' + data.resourceId).click(function () {
                fnShowDialog(data, '@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StopButtonConfirmDialogHeader;E=js}',
                    String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_StopButtonConfirmDialogMessage;E=js}', data.siteName), fnStopSite);
            });
            $('#restart-' + data.resourceId).click(function () {
                fnShowDialog(data, '@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_RestartButtonConfirmDialogHeader;E=js}',
                    String.format('@{R=APM.BlackBox.IIS.Strings;K=SiteManagement_RestartButtonConfirmDialogMessage;E=js}', data.siteName), fnRestartSite);
            });
            fnSetBtnVisibility(data);
        } else {
            $(function () {
                $('#stop-container-' + data.resourceId).hide();
                $('#start-container-' + data.resourceId).hide();
                $('#restart-container-' + data.resourceId).hide();
            });
        }
    };
});
