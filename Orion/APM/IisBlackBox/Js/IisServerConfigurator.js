﻿MakeNamespace("SW.APM.IisBB");

SW.APM.IisBB.ConfigDialog = function () {
    var mAppId = null, mAppInfo = null, mCreds = null, clearError = true, loadedProcessingState = false, mDlgEl = null, configuratorExecutor = null;

    var getTestSettingsFromUi = function () {
        return {
            settings: {
                NodeId: mAppInfo.NodeId,
                AppType: mAppInfo.AppType,
                CredentialSet: {
                    Id: $("select#iisBbCreds :selected").val(),
                    Login: $("#iisBbUserName").val(),
                    Password: $("#iisBbPassword1").val()
                },
                CustomSettings: {
                    'PsUrlWindows': mAppInfo.PsUrlWindows
                }
            }
        };
    };
    
    var noAction = function () {
        return false;
    };

    /*helper members*/
    var validate = function () {
        configuratorExecutor.ToggleLoad(false);
        configuratorExecutor.ToggleStatus("");
        $(".config-validation-error").html("");

        var message = "";
        if (parseInt($("#iisBbCreds :selected").val()) == -1) {
            var value = $.trim($("#iisBbCredName").val());
            if (value.length == 0) {
                message += "@{R=APM.Strings;K=APMWEBJS_EnterCredentialName;E=js}"+"<br />";
            }
            value = $.trim($("#iisBbUserName").val());
            if (value.length == 0) {
                message += "@{R=APM.Strings;K=APMWEBJS_EnterUserName;E=js}"+"<br />";
            }
            if ($("#iisBbPassword1").val() != $("#iisBbPassword2").val()) {
                message += "@{R=APM.Strings;K=APMWEBJS_ConfirmPasswordIsDifferent;E=js}"+"<br />";
            }
        }
        if (message.length > 0) {
            $(".config-validation-error").html(message);
        }
        return message.length == 0;
    };

    var loadProcessingState = function () {
        if (loadedProcessingState || mCreds == null || mAppInfo == null) {
            return;
        }
        
        loadedProcessingState = true;
        disableDialog();
        configuratorExecutor.TryGetSetupApplicationState(mAppInfo.NodeIp, selectCredentialsHandler, newCredentialsHandler, testWebAdministration, enableDialog);
    };

    var newCredentialsHandler = function () {
        if (parseInt($("select#iisBbCreds :selected").val() == -1)) {
            configuratorExecutor.CallWebService("GetCredentials", {}, loadNewCredentials);
        }
    };
    var selectCredentialsHandler = function (credId) {
        $("select#iisBbCreds").val(credId);
        onCredSelect();
    };

    var disableDialog = function () {
        $(".btn-setup").attr("enabled", "false")
            .removeClass("sw-btn sw-btn-primary")
            .addClass("sw-btn-primary sw-btn sw-btn-disabled").unbind("click").bind("click", noAction);

        $(".btn-cancel").attr("enabled", "false")
            .removeClass(" sw-btn sw-btn-cancel")
            .addClass("sw-btn sw-btn-cancel sw-btn-disabled")
            .unbind("click").unbind("click").bind("click", noAction);
        $("#iisBbCreds, #iisBbCredName, #iisBbUserName, #iisBbPassword1, #iisBbPassword2").attr("disabled", "disabled");
    };

    var enableDialog = function () {
        $(".btn-setup").attr("enabled", "true")
            .removeClass("sw-btn-primary sw-btn sw-btn-disabled")
            .addClass("sw-btn sw-btn-primary")
            .unbind("click")
            .bind("click", onButtonSetupClick);
        $(".btn-cancel").attr("enabled", "true")
            .removeClass("sw-btn sw-btn-cancel sw-btn-disabled")
            .addClass(" sw-btn sw-btn-cancel")
            .unbind("click")
            .bind("click", onButtonCancelClick);
        if (parseInt($("#iisBbCreds :selected").val()) == -1) {
            $("#iisBbCreds, #iisBbCredName, #iisBbUserName, #iisBbPassword1, #iisBbPassword2").removeAttr("disabled");
        } else {
            $("#iisBbCreds").removeAttr("disabled");
        }
    };

    /*ajax event handlers*/
    var onCredentialsLoad = function (creds) {
        mCreds = {};

        var el = $("#iisBbCreds");
        el.removeAttr("disabled").empty();
        if (mAppInfo.NodeHasWindowsCredentials) {
            el.append($("<option value='-3'  selected='selected'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_InheritWindowsCredentialFromNode;E=js}"+"&gt;</option>"));
            el.append($("<option value='-1'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_NewCredential;E=js}" +"&gt;</option>"));
        } else {
            el.append($("<option value='-1' selected='selected'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_NewCredential;E=js}" +"&gt;</option>"));
        }

        $.each(creds, function () {
            mCreds[this.Id] = this;
            el.append($(SF("<option value='{0}'>{1}</option>", this.Id, this.Name)));
        });
        el.unbind("change").bind("change", onCredSelect);
        el.change();
        loadedProcessingState = false;
        loadProcessingState();
    };
    
    var onApplicationLoad = function (app) {
        mAppInfo = app;
        configuratorExecutor.PowerShellHelpLink = app.PowerShellHelpLink;
        configuratorExecutor.IisVersion = app.IisVersion;
        configuratorExecutor.NodeIp = mAppInfo.NodeIp;               
        configuratorExecutor.AgentPollingMethod = mAppInfo.AgentPollingMethod;
        $("#iisConfigEditAppLink").attr("href", "/Orion/Apm/Admin/Edit/EditApplication.aspx?id=" + mAppInfo.Id);
        $("#iisBbApps").html(SF($("script#iis-bb-app-name-tpl").html(), mAppInfo.Name, mAppInfo.NodeId, mAppInfo.NodeStatus, mAppInfo.NodeName, mAppInfo.AppStatus));
        configuratorExecutor.CallWebService("GetCredentials", {}, onCredentialsLoad);
        loadProcessingState();
    };
    var callTestWebService = function(methodName, nodeId, onSuccess, onFail, failMessage) {
        $(".connection-success").hide();
        $.ajax({
            type: "POST",
            url: "/Orion/APM/IisBlackBox/Services/TestConnectionServices.asmx/" + methodName,
            data: JSON.stringify(getTestSettingsFromUi(nodeId)),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            timeout: 10 * 60 * 1000,
            success: function (result) {
                if (result.d.TestSuccessfull) {
                    onSuccess(result);
                } else {
                    onFail(failMessage);
                }
            },
            error: function (error) {
                onFail(error);
            }
        });

    }
    var runTest = function (nodeId, onSuccess, onFail) {
        callTestWebService('TestIisBlackBoxConnection', nodeId, onSuccess, onFail, "@{R=APM.Strings;K=APMWEBJS_ConnectionTestWasNotSuccessfull;E=js}");
    };

    var runCredentialTest = function (nodeId, onSuccess, onFail) {
        callTestWebService('TestIisBlackBoxCredential', nodeId, onSuccess, onFail, "@{R=APM.Strings;K=APMWEBJS_CredentialsTestWasNotSuccessfull;E=js}");
    };

    var loadNewCredentials = function (creds) {
        mCreds = {};
        var value = $.trim($("#iisBbCredName").val());
        var el = $("#iisBbCreds");
        el.removeAttr("disabled").empty();
        if (mAppInfo.NodeHasWindowsCredentials) {
            el.append($("<option value='-3'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_InheritWindowsCredentialFromNode;E=js}" + "&gt;</option>"));
        }
        el.append($("<option value='-1'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_NewCredential;E=js}" + "&gt;</option>"));
        $.each(creds, function () {
            mCreds[this.Id] = this;
            if (value == this.Name) {
                el.append($(SF("<option value='{0}' selected='selected'>{1}</option>", this.Id, this.Name)));
            } else {
                el.append($(SF("<option value='{0}'>{1}</option>", this.Id, this.Name)));
            }
        });
        clearError = false;
        el.unbind("change").bind("change", onCredSelect);
        el.change();
    };

    /*dom event handlers*/
    var onCredSelect = function () {
        configuratorExecutor.ToggleStatus("");
        if (clearError == true) {
            configuratorExecutor.ToggleError("");
        }
        $('.iis-test-success').hide();
        $('.iis-test-success-more').hide();
        $('.iis-test-fail').hide();
        clearError = true;

        var id = parseInt($("#iisBbCreds :selected").val());
        if (id == -1) {
            $("#iisBbCredName, #iisBbUserName, #iisBbPassword1, #iisBbPassword2")
				.removeAttr("disabled")
				.val("");
        } else if (id == -3) {
            $("#iisBbCredName, #iisBbUserName, #iisBbPassword1, #iisBbPassword2")
				.attr("disabled", "disabled")
				.val("");
        }
        else {
            $("#iisBbCredName").val(mCreds[id].Name).attr("disabled", "disabled");
            $("#iisBbUserName").val(mCreds[id].UserName).attr("disabled", "disabled");
            $("#iisBbPassword1").val(mCreds[id].Password).attr("disabled", "disabled");
            $("#iisBbPassword2").val(mCreds[id].Password).attr("disabled", "disabled");
        }
    };

    var testWebAdministration = function () {
        var onTestSucess = function (result) {
            if (result.d.Result.IsPowerShellWebAdministrationInstalled == true) {
                $(".iis-config-in-progress-msg").html("@{R=APM.Strings;K=APMWEBJS_TestingconfigurationSuccessful;E=js}");

                var credentialsId = $("select#iisBbCreds :selected").val();
                if (configuratorExecutor.CredentialId) {
                    credentialsId = configuratorExecutor.CredentialId;
                }
                configuratorExecutor.CallWebService("SetServerConfigurationResult", { "appId": mAppId, "credId": credentialsId, "iisVersion": result.d.Result.IISVersion }, noAction);
                setTimeout(function() {
                    window.open('/Orion/APM/IisBlackBox/IisApplicationDetails.aspx?NetObject=ABIA:' + mAppInfo.Id, '_self', false);
                }, 2 * 2000);
                enableDialog();
            } else {
                configuratorExecutor.ToggleError(SF(result.d.Result.ErrorMessage + " <a target='_blank' href= '{1}'>" + "@{R=APM.Strings;K=APMWEBJS_HelpMeCorrectThis_LinkText;E=js}" + "</a>", mAppInfo.NodeName, configuratorExecutor.PowerShellHelpLink),
                    "@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_ConfigurationTestFailed_ErrorTitle;E=js}");
            }
        };
        $(".config-validation-error").html("");
        $(".connection-fail").hide();
        configuratorExecutor.ToggleError(false);
        configuratorExecutor.ToggleLoad(true);
        
        $(".iis-config-in-progress-msg").html("@{ R=APM.Strings;K=APMWEBJS_TestingConfiguration; E=js}");
        runTest(mAppInfo.NodeId, onTestSucess, onTestFail);
    };

    var onButtonSetupClick = function () {
        var inProgressClass = ".iis-config-in-progress-msg";
        if (validate()) {
            $(".config-validation-error").html("");
            $(".connection-fail").hide();
            configuratorExecutor.ToggleError(false);
            $('.iis-test-success').hide();
            $('.iis-test-success-more').hide();
            $('.iis-test-fail').hide();
            disableDialog();

            var onTestSucess = function (result) {
                if (result.d.Result.IsValidCredential == true && result.d.Result.IsPowerShellWebAdministrationInstalled && result.d.Result.IsPowerShellInstalled) {
                    $(inProgressClass).html("@{R=APM.Strings;K=APMWEBJS_TestingCredentialsSuccessful;E=js}");

                    var credentialsId = $("select#iisBbCreds :selected").val();
                    if (configuratorExecutor.CredentialId) {
                        credentialsId = configuratorExecutor.CredentialId;
                    }

                    if (credentialsId == -1) {
                        var creds = {
                            Id: $("select#iisBbCreds :selected").val(),
                            Name: $("#iisBbCredName").val(),
                            UserName: $("#iisBbUserName").val(),
                            Password: $("#iisBbPassword1").val()
                        };
                        configuratorExecutor.CallWebService("CreateNewCredentials", { "cred": creds }, function(credId) {
                            configuratorExecutor.CallWebService("SetServerConfigurationResult", { "appId": mAppId, "credId": credId, "iisVersion": result.d.Result.IISVersion }, noAction);

                            setTimeout(function() {
                                $(inProgressClass).html("@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_PowerShell_Already_Prepared_Message;E=js}");
                                setTimeout(function() {
                                    window.open('/Orion/APM/IisBlackBox/IisApplicationDetails.aspx?NetObject=ABIA:' + mAppInfo.Id, '_self', false);
                                }, 1 * 1000);
                            }, 2 * 1000);
                        });
                    } else {
                        configuratorExecutor.CallWebService("SetServerConfigurationResult", { "appId": mAppId, "credId": credentialsId, "iisVersion": result.d.Result.IISVersion }, noAction);

                        setTimeout(function() {
                            $(inProgressClass).html("@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_PowerShell_Already_Prepared_Message;E=js}");
                            setTimeout(function() {
                                window.open('/Orion/APM/IisBlackBox/IisApplicationDetails.aspx?NetObject=ABIA:' + mAppInfo.Id, '_self', false);

                            }, 1 * 1000);
                        }, 2 * 1000);
                    }

                } else {
                    if (result.d.Result.IsValidCredential == true) {
                        $(inProgressClass).html("@{R=APM.Strings;K=APMWEBJS_TestingCredentialsSuccessful;E=js}");
                        setTimeout(function () {
                            $(inProgressClass).html("@{R=APM.Strings;K=APMWEBJS_ConfiguringServerThisMayTakeFewMinutes;E=js}");
                            var cred = {
                                Id: $("select#iisBbCreds :selected").val(),
                                Name: $("#iisBbCredName").val(),
                                UserName: $("#iisBbUserName").val(),
                                Password: $("#iisBbPassword1").val()
                            };
                            configuratorExecutor.StartConfigurator(mAppInfo, cred, newCredentialsHandler, testWebAdministration, enableDialog);
                        }, 1000);
                    } else {
                        configuratorExecutor.ToggleError(SF("@{R=APM.Strings;K=APMWEBJS_TheCredentialsGivenAreNotValid;E=js}" + " <a target='_blank' href = '{0}'>" + "@{R=APM.Strings; K=APMWEBJS_HelpMeCorrectThis_LinkText;E=js}"+"</a>", mAppInfo.CredentialsTestHelpLink), "@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_CredentialsTestFailed_ErrorTitle;E=js}");
                    }
                }
            };

            var checkIisVersion = function (result) {
                configuratorExecutor.CallWebService("CheckIisVersion", { "iisVersion": result.d.Result.IISVersion }, function (res) {
                    if (res.IsVersionSupported == true) {
                        onTestSucess(result);
                    } else {
                        configuratorExecutor.ToggleError(res.Message, "@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_VersionCompatibilityTestFailed_ErrorTitle;E=js}");
                    }
                });
            };
            
            var startTest = function() {
                configuratorExecutor.ToggleLoad(true);
                $(inProgressClass).html("@{R=APM.Strings;K=APMWEBJS_TestingCredentials;E=js}");
                if (mAppInfo.IsNodeAvailable == true) {
                    runTest(mAppInfo.NodeId, checkIisVersion, onTestFail);
                } else {
                    configuratorExecutor.ToggleError(mAppInfo.NodeIsNotAvailableMsg, "@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_CredentialsTestFailed_ErrorTitle;E=js}");
                }
            };

            if (configuratorExecutor.IisVersion != null) {
                configuratorExecutor.CallWebService("CheckIisVersion", { "iisVersion": configuratorExecutor.IisVersion }, function(res) {
                    if (res.IsVersionSupported == true) {
                        startTest();
                    } else {
                        configuratorExecutor.ToggleError(res.Message, "@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_VersionCompatibilityTestFailed_ErrorTitle;E=js}");
                    }
                });
            } else {
                startTest();
            }
        }
        return false;
    };
    var showTestSuccessMessage = function () {
        $('.iis-test-success').show();
        $('.iis-test-success-more').show();
        $('.iis-test-fail').hide();
    };
    
    var showTestFailMessage = function (msg) {
        $('.iis-test-fail').show();
        $('#test-failed').html(msg);
        $('.iis-test-success').hide();
        $('.iis-test-success-more').hide();
    };
    
    var handleInitialState = function () {
        $('.iis-test-success').hide();
        $('.iis-test-success-more').hide();
        $('.iis-test-fail').hide();
        $("#iisConfiguratorErrorMessageContainer").hide();
        $('#iis-test-inprogress-container').show();
    };

    var onTestFail = function (error) {
        showTestFailMessage(error);
        $('#iis-test-inprogress-container').hide();
    };

    var onButtonTestClick = function () {
        var onTestSucess = function (result) {
            $('#iis-test-inprogress-container').hide();
                    var jobResult = result.d.Result;
                    if (jobResult.IsValidCredential) {
                        showTestSuccessMessage();
                    } else {
                        var message1 = "", message2 = "";
                        if (!jobResult.IsValidCredential) {
                            message1 = "@{R=APM.BlackBox.IIS.Strings;K=IIS_ZeroConfig_CredentialsTestFailed_ErrorTitle;E=js}";
                        } 
                        else if (jobResult.ErrorMessage) {
                            message1 += "&nbsp;&nbsp;<a href='#'>more</a>";
                            if (!jobResult.IsValidCredential) {
                                message2 = jobResult.ErrorMessage + " <a target='_blank' href= '" + mAppInfo.CredentialsTestHelpLink + "'>" + "@{R=APM.Strings;K=APMWEBJS_HelpMeCorrectThis_LinkText;E=js}"+"</a>";
                            } else if (!jobResult.IsPowerShellWebAdministrationInstalled) {
                                message2 = jobResult.ErrorMessage + " <a target='_blank' href= '" + configuratorExecutor.PowerShellHelpLink + "'>" + "@{R=APM.Strings;K=APMWEBJS_HelpMeCorrectThis_LinkText;E=js}"+"</a>";
                            } else {
                                message2 = jobResult.ErrorMessage;
                            }
                        }
                        showTestFailMessage(message1);
                        if (message2.length > 0) {
                            $("#test-failed a").click(function () {
                                $(".iis-test-fail-more p").html(message2);
                                $(".iis-test-fail-more").dialog({
                                    width: 400
                                });
                            });
                        }
                    }
        };

        if (validate()) {
            handleInitialState();
            if (mAppInfo.IsNodeAvailable == true) {
                runCredentialTest(mAppInfo.NodeId, onTestSucess, onTestFail);
            } else {
                $('#iis-test-inprogress-container').hide();
                showTestFailMessage(mAppInfo.NodeIsNotAvailableMsg);
            }
        }
        return false;
    };
    
    var onButtonCancelClick = function () {
        configuratorExecutor.ToggleLoad(false);
        configuratorExecutor.ToggleStatus("");
        configuratorExecutor.ToggleError("");
        mDlgEl.dialog("destroy");
        return false;
    };

    /*public members*/
    this.init = function (appId) {
        mAppId = appId;
        configuratorExecutor = new SW.APM.IisBB.ConfigExecutor();
        configuratorExecutor.Init("iisBbConfigDialog .iis-setup-info div.load", "iisBbConfigDialog .iis-setup-info div.status", "iisBbConfigDialog .iis-setup-info div.error", appId);
        mDlgEl = $("#iisBbConfigDialog").dialog({
            width: 520,
            position: [$(window).width() / 2 - 255, 100],
            modal: true,
            autoOpen: false,
            overlay: { "background-color": "black", opacity: 0.4 }
        });
        $("#iisBbApps").html($("script#iis-bb-app-load").html());

        $("#iisBbCreds").empty();
        enableDialog();

        $("#iisBbConfigDialog a[id$=btnSetup]").unbind("click").bind("click", onButtonSetupClick);
        $("#iisBbConfigDialog a[id$=btnCancel]").unbind("click").bind("click", onButtonCancelClick);
        $("#iisBbConfigDialog a[id$=btnTest]").unbind("click").bind("click", onButtonTestClick);
        
        $("#iisBbConfigDialog .iis-setup-info div.load").hide();

        $("#iisBbConfigDialog .iis-setup-info div.status").empty();
        $("#errorMessage").empty();
        $("#iisConfiguratorErrorMessageContainer").hide();
        $('.iis-test-success').hide();
        $('.iis-test-success-more').hide();
        $('.iis-test-fail').hide();
        
        $('#iis-expander').unbind('click');
        $("#iis-expander").click(function () {
            if (this.attributes["state"].value == "collapsed") {
                this.attributes["src"].value = "/Orion/images/Button.Collapse.gif";
                this.attributes["state"].value = "expanded";
                $(".iis-expander-container").show();
                return;
            }
            this.attributes["state"].value = "collapsed";
            this.attributes["src"].value = "/Orion/images/Button.Expand.gif";
            $(".iis-expander-container").hide();
            return;
        });

        configuratorExecutor.CallWebService("GetIisBbApplicationWithoutCred", { appId: mAppId }, onApplicationLoad);

        $("#iisServerIsAlreadyConfiguredLink").click(function () {
            configuratorExecutor.CallWebService("SetServerConfigurationResult", { "appId": appId, "credId": 0, "iisVersion": "" }, function () {
                window.open('/Orion/APM/IisBlackBox/IisApplicationDetails.aspx?NetObject=ABIA:' + mAppInfo.Id, '_self', false);
                mDlgEl.dialog("destroy");
                return false;
            });
        });
    };
    this.show = function () {
        mDlgEl.dialog("open");
        $("#iisBbCredName, #iisBbUserName, #iisBbPassword1, #iisBbPassword2").val("").attr("disabled", "disabled");
    };
    this.hide = function () {
        mDlgEl.dialog("destroy");
    };
};

/*static members*/
SW.APM.IisBB.ConfigDialog.instance = null;
SW.APM.IisBB.ConfigDialog.show = function (appId) {
    if (SW.APM.IisBB.ConfigDialog.instance == null) {
        SW.APM.IisBB.ConfigDialog.instance = new SW.APM.IisBB.ConfigDialog();
    }
    SW.APM.IisBB.ConfigDialog.instance.init(appId);
    SW.APM.IisBB.ConfigDialog.instance.show();
};
SW.APM.IisBB.ConfigDialog.hide = function () {
    if (SW.APM.IisBB.ConfigDialog.instance == null) {
        return;
    }
    SW.APM.IisBB.ConfigDialog.instance.hide();
};
