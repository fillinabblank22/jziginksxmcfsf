﻿/*jslint browser: true*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.templates', function (module) {
    'use strict';
    module.registerHandlerAssignWizard('ABIA', '../IisBlackBox/Admin/AssignWizard/AssignSmartIisApplication.aspx');
});
