/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.IisBB.Resources', function (module) {
    'use strict';

    var texts = {
        linkedTemplate: '<a href="{0}" title="{1}" {2}>{3}</a>',
        siteState: {
            '0': '@{R=APM.BlackBox.IIS.Strings;K=SiteStatusText0Starting;E=js}',
            '1': '@{R=APM.BlackBox.IIS.Strings;K=SiteStatusText1Started;E=js}',
            '2': '@{R=APM.BlackBox.IIS.Strings;K=SiteStatusText2Stopping;E=js}',
            '3': '@{R=APM.BlackBox.IIS.Strings;K=SiteStatusText3Stopped;E=js}'
        },
        siteStateUndefined: '@{R=APM.BlackBox.IIS.Strings;K=SiteStatusTextUndefined;E=js}',
        valueNA: '@{R=APM.BlackBox.IIS.Strings;K=ValueNA;E=js}'
    };
    
    function measureString(str) {
        var span, result;
        span = document.createElement('span');
        span.style.visibility = 'hidden';
        span.style.padding = '0px';
        span.innerHTML = str;
        document.body.appendChild(span);
        result = span.offsetWidth;
        document.body.removeChild(span);
        return result;
    }

    function getTextStateCss(value) {
        switch (value) {
        case 6:
            return 'redAndBold';
        case 5:
            return 'red';
        default:
            return '';
        }
    }

    function msConverter(duration) {
        if (duration !== null) {
            var milliseconds = Math.floor(duration % 1000);
            var totalSeconds = Math.floor(duration / 1000);
            var seconds = Math.floor(totalSeconds % 60);
            var totalMinutes = Math.floor(totalSeconds / 60);

            if (totalMinutes !== 0) {
                return String.format('@{R=APM.BlackBox.IIS.Strings;K=TopXXPageRequestsByAvrServerExecutionTimeTimeStr3;E=js}', totalMinutes, seconds, milliseconds);
            }
            if (totalSeconds !== 0) {
                return String.format('@{R=APM.BlackBox.IIS.Strings;K=TopXXPageRequestsByAvrServerExecutionTimeTimeStr2;E=js}', totalSeconds, milliseconds);
            }

            return String.format('@{R=APM.BlackBox.IIS.Strings;K=TopXXPageRequestsByAvrServerExecutionTimeTimeStr1;E=js}', duration);
        }
        return '@{R=APM.BlackBox.IIS.Strings;K=TopXXPageRequestsByAvrServerExecutionTimeNA;E=js}';
    }

    function urlToDisplay(url) {
        return url ? url.replace(/&/g, '&amp;').replace(/>/g, '&gt;').replace(/</g, '&lt;').replace(/"/g, '&quot;') : '';
    }

    function wrapUrl(str, allowedWidth) {
        if (!str)
            return '';
        var result, urlParts, lines, lineResult, i, j, k, rest;
        str = str.toString();
        result = str;
        if (measureString(str) > allowedWidth) {
            str = str.replace(/\?/g, '? ');
            str = str.replace(/\//g, '/ ');
            urlParts = str.split(' ');
            lines = [];
            lineResult = '';
            for (i = 0, j = 0; i < urlParts.length; i++) {
                lineResult += urlParts[i];
                if (measureString(lineResult) > allowedWidth) {
                    lines[j++] = lineResult.substring(0, lineResult.length - urlParts[i].length);
                    lineResult = '';
                    for (k = 0; k < urlParts[i].length; k++) {
                        lineResult += urlParts[i].charAt(k);
                        if (measureString(lineResult) >= allowedWidth) {
                            lines[j++] = lineResult;
                            rest = urlParts[i].substring(k + 1, urlParts[i].length);
                            if (measureString(rest) >= allowedWidth) {
                                lineResult = '';
                            } else {
                                if (i === (urlParts.length - 1)) {
                                    lines[j++] = rest;
                                    lineResult = '';
                                } else {
                                    lineResult = rest;
                                }
                                break;
                            }
                        }
                    }
                }
                if (i === (urlParts.length - 1)) {
                    lines[j++] = lineResult;
                    lineResult = '';
                }
            }
            result = lines.join('<br/>');
        }
        return result;
    }

    function cutUrl(str, allowedWidth, linesCount) {
        if (!str)
            return '';
        var breakTag = '<br/>', dotsSymbol = '&#8230;', slashSymbol = '/', askSymbol = '?',
            result, lines, i, lineParts, lineToAdd;

        result = module.wrapUrl(str, allowedWidth);
        lines = result.split(breakTag);
        if (lines.length > linesCount) {
            result = '';
            for (i = 0; i < linesCount - 2; i++) {
                result += lines[i] + breakTag;
            }

            if ((lines[linesCount - 2].endsWith(slashSymbol) || lines[linesCount - 2].endsWith(askSymbol)) &&
                    (lines[lines.length - 2].endsWith(slashSymbol) || lines[lines.length - 2].endsWith(askSymbol))) {

                lineParts = lines[linesCount - 2].split(slashSymbol);
                lineToAdd = '';
                for (i = 0; i < lineParts.length - 1; i++) {
                    if (measureString(lineToAdd + lineParts[i] + slashSymbol + dotsSymbol + slashSymbol) > allowedWidth) {
                        if (i === 0) {
                            lineToAdd += slashSymbol;
                        }
                        break;
                    }
                    lineToAdd += lineParts[i] + slashSymbol;
                }

                lineToAdd += dotsSymbol + slashSymbol;

                lineParts = lines[lines.length - 2].split(slashSymbol);
                var extraPart = '';
                if (lineParts.length > 1) {
                    for (i = lineParts.length - 2; i > 0; i--) {
                        if (measureString(lineToAdd + extraPart + lineParts[i] + slashSymbol) > allowedWidth) {
                            break;
                        }
                        extraPart = lineParts[i] + slashSymbol + extraPart;
                    }
                }

                lineToAdd += extraPart;

                lineParts = lines[lines.length - 1].split(slashSymbol);
                if (lineParts.length > 1) {
                    for (i = 0; i < lineParts.length - 1; i++) {
                        if (measureString(lineToAdd + lineParts[i] + slashSymbol) > allowedWidth) {
                            break;
                        }
                        lines[lines.length - 1] = lines[lines.length - 1].replace(lineParts[i] + slashSymbol, '');
                        lineToAdd += lineParts[i] + slashSymbol;
                    }
                }

                result += lineToAdd + breakTag + lines[lines.length - 1];
            } else {
                result += lines[linesCount - 2] + breakTag + lines[linesCount - 1].substring(0, lines[linesCount - 1].length) + dotsSymbol;
            }
        }

        return result;
    }

    function addTitle(str, title) {
        if (!str)
            return '';
        return str.indexOf('&#8230;') === -1 ? str : "<a title='" + title + "'>" + str + "</a>";
    }

    function getUrlColumnWidth(resourceWidth) {
        return resourceWidth * 0.5;
    }

    function getUrlColumnWidthForSiteDetails(resourceWidth) {
        return resourceWidth * 0.65;
    }

    function getAvgExecTimePercentLinearGauge(value, status, warnThreshold, critThreshold) {
        var lg = new SW.APM.Controls.LinearGauge();
        lg.Value = value;
        lg.Status = status;
        lg.WarningThreshold = warnThreshold;
        lg.CriticalThreshold = critThreshold;
        lg.Width = 151;
        return lg.BuildLinearGauge();
    }

    function getVolumeLink(redirectLink, text) {
    	var html = getLinkWithTitle(redirectLink, "", text, false);
    	if (redirectLink == null) {
    		return String.format("<div title='Can not poll volumes'>{0}</div>", html);
    	}
    	return html;
    }

    function getLink(redirectLink, text, newTab) {
        return getLinkWithTitle(redirectLink, '', text, newTab);
    }
    
    function getLinkWithTitle(redirectLink, title, text, newTab) {
        if (typeof newTab !== "boolean") {
            newTab = false;
        }
        return redirectLink === '' || redirectLink === null
            ? text
            : String.format(texts.linkedTemplate, redirectLink, text.indexOf('&#8230;') === -1 ? "" : title, newTab ? "target='_blank'" : "", text);
    }
    
    function getVolumeStatusBar(usedSpace, status) {
        var p = new SW.APM.Controls.PercentStatusBar();
        p.BuildByStatus = true;
        p.Status = status;
        p.UsedSpacePercentage = usedSpace;
        return p.BuildPercentStatusBar();
    }

    function getWebsiteLink(protocol, nodeIp, siteIp, hostName, port) {
        var urlPath;
        if (hostName) {
            urlPath = hostName;
        } else {
            var ipv6Pattern = new RegExp("(([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))");
            //if site IP Address is any or loopback, node IP address will be used
            if (!siteIp || siteIp == '*' || siteIp == "127.0.0.1" || siteIp == "0:0:0:0:0:0:0:1") {
                //we can access web source using ipV6 if it is placed in brackets
                if (ipv6Pattern.test(nodeIp)) {
                    if (nodeIp.charAt(0) == "[") {
                        urlPath = nodeIp;
                    } else {
                        urlPath = String.format("[{0}]", nodeIp);
                    }
                } else {
                    urlPath = nodeIp;
                }
            } else {
                //we can access web source using ipV6 if it is placed in brackets
                if (ipv6Pattern.test(siteIp)) {

                    if (siteIp.charAt(0) == "[") {
                        urlPath = siteIp;
                    } else {
                        urlPath = String.format("[{0}]", siteIp);
                    }

                } else
                    urlPath = siteIp;
            }
        }
        var link = String.format("{0}:{1}", urlPath, port);
        if (protocol)
            return String.format("{0}://{1}", protocol, link);
        return String.format("http://{0}", link);
    }

    function getSiteStateText(stateCode) {
        return texts.siteState[stateCode] || texts.siteStateUndefined;
    }
    function getValueText(value) {
        return (value === null || value === undefined) ? texts.valueNA : Math.round(value);
    }

    function getTextStatusCss(value) {
        switch (value) {
            case 14:
                return 'redAndBold';
            case 3:
                return 'red';
            default:
                return '';
        }
    }

    module.getTextStateCss = getTextStateCss;
    module.msConverter = msConverter;
    module.urlToDisplay = urlToDisplay;
    module.wrapUrl = wrapUrl;
    module.cutUrl = cutUrl;
    module.addTitle = addTitle;
    module.getUrlColumnWidth = getUrlColumnWidth;
    module.getAvgExecTimePercentLinearGauge = getAvgExecTimePercentLinearGauge;
    module.getUrlColumnWidthForSiteDetails = getUrlColumnWidthForSiteDetails;
    module.getLink = getLink;
    module.getLinkWithTitle = getLinkWithTitle;
    module.getVolumeLink = getVolumeLink;
    module.getVolumeStatusBar = getVolumeStatusBar;
    module.getWebsiteLink = getWebsiteLink;
    module.getSiteStateText = getSiteStateText;
    module.getValueText = getValueText;
    module.getTextStatusCss = getTextStatusCss;
});
