﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.IISBB.Charts', function (common) {
    common.AvailabilitySeriesTemplates = {
        Up_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Up;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(63,134,0)"],
                    [0.5, "rgb(93,163,19)"],
                    [1, "rgb(119,189,45)"]
                ]
            }
        },
        Warning_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Warning;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(196,163,0)"],
                    [0.5, "rgb(228,193,16)"],
                    [1, "rgb(252,217,40)"]
                ]
            }
        },
        Critical_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Critical;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(187,95,1)"],
                    [0.5, "rgb(224,132,3)"],
                    [1, "rgb(249,157,28)"]
                ]
            }
        },
        Down_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Down;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(145,0,0)"],
                    [0.5, "rgb(205,0,16)"],
                    [1, "rgb(230,25,41)"]
                ]
            }
        },
        Other_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Other;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(90,90,90)"],
                    [0.5, "rgb(145,145,145)"],
                    [1, "rgb(183,183,183)"]
                ]
            }
        },
        NotRunning_Count: {
        	name: "@{R=APM.Strings;K=APM_Status_NotRunning;E=js}",
        	type: "column",
        	zIndex: 2,
        	color: {
        		linearGradient: {
        			x1: 1,
        			y1: 0.8,
        			x2: 0,
        			y2: 0
        		},
        		stops: [
                    [0, "rgb(63,134,136)"],
                    [0.5, "rgb(95,158,160)"],
                    [1, "rgb(119,189,191)"]
        		]
        	}
        },
        Unknown_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Unknown;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,0,0)"],
                    [0.5, "rgb(50,50,50)"],
                    [1, "rgb(100,100,100)"]
                ]
            }
        },
        Availability: {
            name: "@{R=APM.Strings;K=APM_ChartSettings_yAxis_Availability;E=js}",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.SiteDirectorySizeChart = {
    		subtitle: { align: "left", x: -7 },
    		yAxis: [{
                title: { margin: 10, text: "@{R=APM.Strings;K=APMWEBJS_IisBlackBox_Chart_DirectorySize;E=js}" },
    			labels: { align: "right", x: -8 },
    			startOnTick: false,
    			endOnTick: true,
                unit: "byte"
    		}],
    		plotOptions: {
    			area: {
    				stacking: "normal",
    				pointPadding: 0,
    				borderColor: "#000000",
    				borderWidth: 0.5,
    				groupPadding: 0
    			}
    		},
    		seriesTemplates: {
    			UsedSpace: {
    				name: "@{R=APM.BlackBox.IIS.Strings;K=ChartTemplate_SpaceUsed;E=js}",
    				type: "area",
    				zIndex: 1,
    				color: "#006ca9"
    			}
    		}
    	};

    common.IISAverageNetworkTraffic = {
            chart: {
                type: 'area'
            },
            yAxis: [{
                title: { margin: 10, text: "@{R=APM.BlackBox.IIS.Strings;K=IISAvgNetworTraffic_Chart_NetworkTrafic;E=sql}" },
                labels: { align: "right", x: -8 },
                startOnTick: false,
                endOnTick: true,
                unit: 'bps',
                min: 0
            }],
            plotOptions: {
                area: {
                    stacking: "normal",
                    pointPadding: 0,
                    borderColor: "#000000",
                    borderWidth: 0.5,
                    groupPadding: 0
                }
            },
            seriesTemplates: {
                NetworkTraffic: {
                    name: "@{R=APM.BlackBox.IIS.Strings;K=ChartTemplate_Traffic;E=js}", 
                    type: "area"          
                }
            },
        };
});
