/*jslint indent: 4, plusplus: true, vars: true*/
/*global APMjs: false*/
/// <reference path="../../../APM.js"/>
APMjs.initGlobal('SW.APM.IISBB.Charts.IisProcessTop10ChartLegend', function (module, APMjs, global) {
    'use strict';

    var $ = APMjs.assertQuery(),
        SW = APMjs.assertGlobal('SW'),
        CONST;

    CONST = {
        cssname: {
            table: 'APM_IIS_ProcessTop10',
            text: 'text',
            colorbox: 'colorbox',
            field: 'right',
            hilite: 'highlight',
            total: 'total',
            clickable: 'clickable'
        },
        defColumn: {
            Name: {
                name: 'name',
                description: '@{R=APM.BlackBox.IIS.Strings;K=ProcessTop10_Legend_ProcessName;E=js}',
                captionClass: 'name'
            },
            Cpu: {
                name: 'cpu',
                description: '@{R=APM.BlackBox.IIS.Strings;K=ProcessTop10_Legend_Cpu;E=js}'
            },
            PhysicalMemory: {
                name: 'physicalmemory',
                description: '@{R=APM.BlackBox.IIS.Strings;K=ProcessTop10_Legend_PhysicalMemory;E=js}'
            },
            VirtualMemory: {
                name: 'virtualmemory',
                description: '@{R=APM.BlackBox.IIS.Strings;K=ProcessTop10_Legend_VirtualMemory;E=js}'
            }
        },
        text: {
            noValue: '@{R=APM.BlackBox.IIS.Strings;K=ValueNA;E=js}'
        },
        valueColumns: [
            'Cpu',
            'PhysicalMemory',
            'VirtualMemory'
        ],
        decimalPlaces: 2,
        defAppendFieldOpts: {
            elementType: '<td/>',
            isHtml: false
        }
    };

    function appendTableField(opts) {
        var $field;
        opts = $.extend({}, CONST.defAppendFieldOpts, opts);
        if (!opts.$row || !opts.$row.length) {
            APMjs.console.warn('opts.$row missing');
            return;
        }
        $field = $(opts.elementType)
            .addClass(opts.cssClass)
            .appendTo(opts.$row);
        if (opts.isHtml) {
            $field.html(opts.text);
        } else {
            $field.text(opts.text);
        }
        return $field;
    }

    function getHeaderClass(colDef, name) {
        var classes = [];
        if (colDef.captionClass && colDef.captionClass.length) {
            classes.push(colDef.captionClass);
        }
        if (colDef.name === name) {
            classes.push(CONST.cssname.hilite);
        }
        return classes.join(' ');
    }

    function createLegendDescriptions(nameHilite) {
        var $tableRow = $('<tr />');
        APMjs.linqEach(CONST.defColumn, function (columnDefinition) {
            var cssClass = getHeaderClass(columnDefinition, nameHilite);
            appendTableField({
                text: columnDefinition.description,
                $row: $tableRow,
                cssClass: cssClass,
                elementType: '<th />'
            });
        });
        return $tableRow;
    }

    function findChartSerieWithName(chart, name) {
        var foundSerie = null;
        APMjs.linqAny(chart.series, function (serie) {
            if (serie.name === name) {
                foundSerie = serie;
                return true;
            }
        });
        return foundSerie;
    }

    function linkChartSeriesWithLegendSeries(legendData, chart) {
        APMjs.linqEach(legendData.Data, function (legendSerie) {
            var chartSerie = findChartSerieWithName(chart, legendSerie.Name);
            legendSerie.ChartSerie = chartSerie;
        });
    }

    function hndToggleSerie(evt) {
        var serie = $(evt.target).data('serie');
        if (serie.visible) {
            serie.hide();
        } else {
            serie.show();
        }
    }

    function nameCheckBox($column, serie) {
        $('<input type="checkbox" checked="checked" />')
            .data('serie', serie)
            .click(hndToggleSerie)
            .appendTo($column);
    }

    function nameColorBox($column, serie) {
        $('<div />')
            .addClass(CONST.cssname.colorbox)
            .css('background-color', serie.color)
            .appendTo($column);
    }

    function nameText($column, text) {
        $('<span />')
            .addClass(CONST.cssname.text)
            .text(text)
            .appendTo($column);
    }

    function appendNameField(text, $tableRow, chartSerie) {
        var $column = $('<td />')
            .addClass(CONST.defColumn.Name.captionClass);
        if (chartSerie) {
            nameCheckBox($column, chartSerie);
            nameColorBox($column, chartSerie);
        }
        nameText($column, text);
        $column.appendTo($tableRow);
    }

    function getFieldClass(colDef, name) {
        var classes = [CONST.cssname.field];
        if (colDef.captionClass && colDef.captionClass.length) {
            classes.push(colDef.captionClass);
        }
        if (colDef.name === name) {
            classes.push(CONST.cssname.hilite);
        }
        return classes.join(' ');
    }

    function getAxisRange(legendData) {
        var axis = {
            minData: Number.MAX_VALUE,
            maxData: Number.MIN_VALUE
        };

        APMjs.linqEach(legendData.Data, function (data) {
            APMjs.linqEach(CONST.valueColumns, function (valueColumn) {
                var value = data[valueColumn];
                axis.minData = Math.min(axis.minData, value);
                axis.maxData = Math.max(axis.maxData, value);
            });
        });

        return axis;
    }

    function guessNodeTitle(legendData, chart) {
        var data, nodeTitle = CONST.text.noValue;
        if (legendData && legendData.Data && chart && chart.title) {
            data = legendData.Data[legendData.Data.length - 1];
            if (data) {
                nodeTitle = (chart.title && (chart.title.text || (chart.title.element && chart.title.element.innerText))) || nodeTitle;
            }
        }
        return nodeTitle;
    }

    function appendNodeNameToLastRow($row, nodeTitle, nodeViewLink) {
        var $cell, text;
        $cell = $row.find('td.name');
        text = $cell.text();
        if (APMjs.isStr(nodeViewLink) && nodeViewLink.length) {
            $cell.text(text + ' ');
            $('<a>').attr('href', nodeViewLink).text(nodeTitle).appendTo($cell);
        } else {
            $cell.text(text + ' ' + nodeTitle);
        }
    }

    function findTabs($table) {
        return $table
            .closest('.tabContents')
            .siblings('.tabHeaders')
            .find('a.item');
    }

    function makeHeadersClickable($table) {
        var $hdrs = $table.find('th');
        $hdrs.each(function () {
            var $th = $(this), $tabs = findTabs($table), textCell;
            textCell = $th.text().toLowerCase();
            $tabs.each(function () {
                var $tab = $(this), textTab = $tab.text().toLowerCase();
                if (textCell.indexOf(textTab) >= 0) {
                    $th
                        .addClass(CONST.cssname.clickable)
                        .click(function () {
                            global.location = $tab.attr('href');
                        });
                    return false;
                }
            });
        });
    }

    function createStandardLegend(chart, legendContainerId, nodeTitle, nodeViewLink) {
        var $table, $tableRow, legendData, axis, nameHilite;

        $table = $('#' + legendContainerId);
        $table.addClass(CONST.cssname.table);
        $table.empty();

        legendData = chart.options.APM.IIS.Top10Process;

        nameHilite = legendData.Highlight.toLowerCase();
        createLegendDescriptions(nameHilite).appendTo($table);
        makeHeadersClickable($table);
        linkChartSeriesWithLegendSeries(legendData, chart);

        axis = getAxisRange(legendData);

        APMjs.linqEach(legendData.Data, function (data) {
            $tableRow = $('<tr />');
            appendNameField(data.Name, $tableRow, data.ChartSerie);

            APMjs.linqEach(CONST.valueColumns, function (valueColumn) {
                var value = data[valueColumn];
                if (APMjs.isSet(value)) {
                    value = SW.Core.Charts.dataFormatters.percent(value, axis, CONST.decimalPlaces);
                } else {
                    value = CONST.text.noValue;
                }
                appendTableField({
                    text: value,
                    $row: $tableRow,
                    cssClass: getFieldClass(CONST.defColumn[valueColumn], nameHilite),
                    isHtml: true
                });
            });

            $tableRow.appendTo($table);
        });

        // mark last table row as total
        if ($tableRow) {
            $tableRow.addClass(CONST.cssname.total);
            nodeTitle = nodeTitle || guessNodeTitle(legendData, chart);
            appendNodeNameToLastRow($tableRow, nodeTitle, nodeViewLink);
        }
    }

    module.constants = CONST;
    module.createStandardLegend = createStandardLegend;
});
