﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.Charts.IISTrafficLegend', function (IISTrafficLegend) {
    'use strict';

    var $ = APMjs.assertQuery(),
        Highcharts = APMjs.assertGlobal('Highcharts'),
        CoreCharts = APMjs.assertGlobal('SW.Core.Charts'),
        cnst = {
            symbolWidth: 16,
            symbolHeight: 12
        };

    function toggleSeries(series) {
        if (series.visible) {
            series.hide();
        } else {
            series.show();
        }
    }

    function addCheckbox(series, container) {
        var $check = $('<input type="checkbox"/>');

        $check.click(function () {
            toggleSeries(series);
        });

        if (series.visible) {
            $check.attr('checked', true);
        }

        $check.appendTo(container);
    }

    function addLegendSymbol(series, container, colorOverride) {
        var $label, renderer, symbolColor;

        $label = $('<span class="legendSymbol" />');
        renderer = new Highcharts.Renderer($label[0], cnst.symbolWidth, cnst.symbolHeight + 3);
        symbolColor = colorOverride || series.color;

        renderer
            .rect(0, 3, cnst.symbolWidth, cnst.symbolHeight, 2)
            .attr({
                stroke: symbolColor,
                fill: symbolColor
            })
            .add();

        $label.appendTo(container);
    }

    function createStandardLegend(chart, legendContainerId, nodeName, nodeViewLink) {
        var $table, $row, $td, totalTrafficStr,
            totalValue = 0,
            formatter = CoreCharts.dataFormatters.bps;

        $table = $('#' + legendContainerId);
        $table.css('border-collapse', 'collapse');
        $row = $('<tr />');
        $td = $('<td />');

        $table.empty();

        $td = $('<td colspan="3" />')
            .html('@{R=APM.BlackBox.IIS.Strings;K=IISAvgNetworTraffic_Legend_TrafficSource;E=sql}')
            .appendTo($row)
            .addClass('IisTrafficHeaderRow');

        $td = $('<td />')
            .html('@{R=APM.BlackBox.IIS.Strings;K=IISAvgNetworTraffic_Legend_LatestTraffic;E=sql}')
            .appendTo($row)
            .addClass('IisTrafficHeaderRow')
            .addClass('IisTrafficValueRow');

        $row.appendTo($table);

        APMjs.linqEach(chart.series, function (seria) {
            var value;

            if (seria.options.id === 'highcharts-navigator-series') {
                return;
            }

            value = seria.options.customProperties.CurrentTraffic;

            if (APMjs.isSet(value)) {
                totalValue += value;
                value = formatter(value, chart.yAxis);
            } else {
                value = '@{R=APM.BlackBox.IIS.Strings;K=IISAvgNetworTraffic_Legend_NA;E=sql}';
            }

            $row = $('<tr/>').appendTo($table);
            addCheckbox(
                seria,
                $('<td width="10px" class="IisTrafficRow"/>')
                    .appendTo($row)
            );

            addLegendSymbol(
                seria,
                $('<td width="10px" class="IisTrafficRow"/>')
                    .appendTo($row)
            );

            $('<td class="IisTrafficRow"/>')
                .html(seria.name)
                .appendTo($row);

            $('<td class="IisTrafficRow IisTrafficValueRow"/>')
                .html(value)
                .appendTo($row);
        });

        $row = $('<tr class="IisTrafficRowSummary"/>')
            .appendTo($table);

        totalTrafficStr = '@{R=APM.BlackBox.IIS.Strings;K=IISAvgNetworTraffic_Legend_TotalTraffic;E=sql}';
        if (nodeName) {
            totalTrafficStr += '@{R=APM.BlackBox.IIS.Strings;K=IISAvgNetworTraffic_Legend_ON;E=sql}';
            if (nodeViewLink) {
                totalTrafficStr += '<a href="' + nodeViewLink + '">' + nodeName + '<a/>';
            } else {
                totalTrafficStr += nodeName;
            }
        }

        $('<td colspan=3 class="IisTrafficRow"/>')
            .html(totalTrafficStr)
            .appendTo($row);

        $('<td class="IisTrafficRow IisTrafficValueRow"/>')
            .html(formatter(totalValue, chart.yAxis))
            .appendTo($row);
    }

    // publish methods
    IISTrafficLegend.createStandardLegend = createStandardLegend;
});
