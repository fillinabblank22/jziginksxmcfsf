/*jslint indent: 4, plusplus: true, vars: true*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.IISBB.Charts.IisProcessTop10Chart', function (module, APMjs) {
    'use strict';

    var SW = APMjs.assertGlobal('SW'), CONST;

    function createChartOptions(title, subtitle) {
        return {
            title: {
                text: '${Caption}'
            },
            subtitle: {
                text: subtitle
            },
            yAxis: [{
                title: {
                    margin: 10,
                    text: title
                },
                unit: '%',
                labels: {
                    align: 'right',
                    x: -8
                },
                min: 0,
                maxRange: 102
            }],
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#000000',
                    lineWidth: 0.1,
                    marker: {
                        lineWidth: 0.1,
                        lineColor: '#000000'
                    }
                }
            },
            tooltip: SW.APM.Charts.Common.TopProcessDataTooltip,
            seriesTemplates: SW.APM.Charts.Common.StackAreaChartSeriesTemplates
        };
    }

    CONST = {
        statisticColumns: {
            'CPU': 'PercentCPU',
            'PMem': 'PercentMemory',
            'VMem': 'PercentVirtualMemory'
        },
        units: {
            'CPU': '%',
            'PMem': '%',
            'VMem': '%'
        },
        options: {
            CPU: createChartOptions('@{R=APM.BlackBox.IIS.Strings;K=IISAPMSQL_DATA_DO06;E=sql}', '@{R=APM.BlackBox.IIS.Strings;K=IISAPMSQL_DATA_DO05;E=sql}'),
            PHM: createChartOptions('@{R=APM.BlackBox.IIS.Strings;K=IISAPMSQL_DATA_DO04;E=sql}', '@{R=APM.BlackBox.IIS.Strings;K=IISAPMSQL_DATA_DO03;E=sql}'),
            VIM: createChartOptions('@{R=APM.BlackBox.IIS.Strings;K=IISAPMSQL_DATA_DO01;E=sql}', '@{R=APM.BlackBox.IIS.Strings;K=IISAPMSQL_DATA_DO02;E=sql}')
        }
    };

    function getStatisticColumn(thresholdName) {
        return CONST.statisticColumns[thresholdName] || '';
    }

    function getUnit(thresholdName) {
        return CONST.units[thresholdName] || '';
    }

    function transformSettings(parameters, settings) {
        settings.netObjectIds[0] = parameters[0];
        if (parameters[1]) {
            settings.StatisticColumn = parameters[1];
        } else {
            settings.StatisticColumn = getStatisticColumn(parameters[2]);
        }
        settings.columnSchemaId = parameters[5];
        return settings;
    }

    function transformOptions(parameters, options) {
        var unit = getUnit(parameters[2]);
        options.chart.events.load = function () {
            SW.APM.EditApp.redrawPlotBandsInCharts();
            SW.APM.EditApp.redrawXAxis(parameters[3], unit);
        };
        options.xAxis.events.afterSetExtremes = function () {
            SW.APM.EditApp.redrawPlotBandsInCharts();
            SW.APM.EditApp.redrawXAxis(parameters[3], unit);
        };
        if (parameters[4]) {
            options.chart.width = parameters[4];
        }
        if (unit) {
            options.tooltip.headerFormat = '<div id=""highchartTooltip"">' + "@{R=APM.Strings;K=APMWEBJS_IisBlackBox_ChartTooltip_AValueOfPointKey;E=js}"+" " + unit + '<br />';
        }
        return options;
    }

    function transformCustomChart(controlId) {
        SW.APM.Charts.CustomChart.setTransform(controlId, transformSettings, transformOptions);
    }

    module.ChartOptions = CONST.options;
    module.transformCustomChart = transformCustomChart;
});
