﻿using System.Globalization;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.APM.BlackBox.IIS.Common;

public partial class Orion_APM_IisBlackBox_SitePopup : PopupPage<Site>
{
    protected const int TopComponentsCount = 5;

    private readonly Log log = new Log();

    protected Site IisSite
    {
        get { return NetObjectData; }
    }

    protected IisApplication IisApp
    {
        get { return IisSite.IisApplication; }
    }

    protected long SiteId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.PopupMaster.PopupTitle = APM_IisBBContent.Site_ToolTip_Header;
        this.PopupMaster.StatusCssClass = StatusCssClass;

        AppStatusIcon.StatusValue = IisApp.Status;
        AppStatusIcon.CustomApplicationType = IisApp.CustomType;

        SiteStatusIcon.StatusValue = GetStatus();
        SiteStatusIcon.CustomApplicationType = IisApp.CustomType;
        SiteStatusIcon.ApplicationItemType = ApplicationItemType.ABIA_WebSite.ToString();
        ServerStatusIcon.StatusValue = IisApp.NPMNode.Status;

        this.componentList.BindComponents(
            this.IisApp.ComponentsWithProblems.Where(x => x.ApplicationItemID == SiteId),
            TopComponentsCount,
            IisApp.CustomType,
            ApplicationItemType.ABIA_WebSite.ToString()
            );
    }

    #region Overriden methods

    protected override ApmStatus GetStatus()
    {
        return new ApmStatus(NetObjectData.ApmStatus.Value);
    }

    protected override Site CreateNetObjectData(string netObjectId)
    {
        var netObject = CreateNetObject();
        var result = netObject as Site;

        if (result == null)
        {
            log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render site popup, because cannot get site info from netObject with netObjectID '{0}'", netObjectId);
        }
        else
        {
            SiteId = result.SiteModel.Id;
        }

        return result;
    }

    #endregion
}
