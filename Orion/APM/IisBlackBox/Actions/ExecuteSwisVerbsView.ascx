﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExecuteSwisVerbsView.ascx.cs" Inherits="Orion_APM_IisBlackBox_Actions_ExecuteSwisVerbsView" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.IIS.Actions.ExecuteSwisVerbs" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.IIS.Common" %>

<orion:Include ID="i0" File="APM/IisBlackBox/Actions/Css/ExecuteSwisVerbs.css" runat="server" />
<orion:Include ID="i1" File="APM/IisBlackBox/Actions/Js/ExecuteSwisVerbsController.js" runat="server" />

<div id="container" class="apm-IisExecuteSwisVerbs" runat="server">

	<h3><%= Resources.APMWebContent.IISBackBox_RestartIISSiteApplicationPoolSettings%></h3>

	<div id="apmIisExecuteSwisVerbs" class="section required">

		<div class="verb-name-container">

			IIS Action to Perform: <br />
			<asp:DropDownList ID="ctrVerbName" CssClass="control" data-form="<%$ Code: ExecuteSwisVerbsConstants.VerbName %>" runat="server">
				<asp:ListItem Text="<%$ Resources: APMWebContent, IISBackBox_Start %>" Value="Start" />
				<asp:ListItem Text="<%$ Resources: APMWebContent, IISBackBox_Stop %>" Value="Stop" />
				<asp:ListItem Text="<%$ Resources: APMWebContent, IISBackBox_RestartRecycle %>" Value="Restart" Selected="True" />
			</asp:DropDownList>
		</div>

		<div class="server-container">

            <%= Resources.APMWebContent.IISBackBox_SpecifyTheSiteApplicationPooltoUse%> <br />
			<div class="server-type">
				<asp:RadioButtonList CssClass="server-type" RepeatLayout="OrderedList" runat="server">
                    <asp:ListItem Value="from-trigger" Selected="True" Text="&nbsp;<%$ Resources: APMWebContent, IISBackBox_TakenFromTheAlertTrigger %>"/>
                    <asp:ListItem Value="custom" Text=">&nbsp;<%$ Resources: APMWebContent, IISBackBox_UseDifferentIISServer %>"/>
				</asp:RadioButtonList>
			</div>
			
			<div class="server-info-container">
				<div class="loading"></div>

				<div class="servers">

                    <%= Resources.APMWebContent.IISBackBox_IISServer%> <br />
					<asp:DropDownList ID="ctrNodes" CssClass="control" data-form="<%$ Code: ExecuteSwisVerbsConstants.NodeId %>" runat="server" />
				</div>

				<div class="entities">
					
                    <%= Resources.APMWebContent.IISBackBox_SiteApplicationPool%> <br />
					<asp:DropDownList ID="ctrEntities" CssClass="control" data-form="<%$ Code: ExecuteSwisVerbsConstants.EntityId %>" runat="server" />
				</div>
			</div>
        </div>

		<div class="error-container">
		</div>
	</div>
</div>

<script type="text/javascript">

	(function() {
		
		var controller = new SW.Core.Actions.ApmIisExecuteSwisVerbsController({
            
			containerID : "<%= container.ClientID %>", 
			
			onReadyCallback : <%= OnReadyJsCallback %>,
			actionDefinition :  <%= ToJson(ActionDefinition) %>,
			viewContext :  <%= ToJson(ViewContext) %>,
			multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
			
			SWIS_SITE_NAME: "<%= SwisEntities.IISSite %>",
			SWIS_POOL_NAME: "<%= SwisEntities.IISApplicationPool %>",
			
			dataFormMapping : {
				EntityType: "<%= ExecuteSwisVerbsConstants.EntityType %>",
				VerbName: "<%= ExecuteSwisVerbsConstants.VerbName%>",
				NodeId: "<%= ExecuteSwisVerbsConstants.NodeId %>",
				EntityId: "<%= ExecuteSwisVerbsConstants.EntityId %>"
        	}
		});
		controller.init();
		SW.Core.MacroVariablePickerController.BindPicker();
	})();
</script>