﻿SW.Core.namespace("SW.Core.Actions").ApmIisExecuteSwisVerbsController = function (config) {

	var mThat = this, mConfig = config;
	var mContainer = null, mData = null;

	var SF = function(format) {
		var args = arguments;
		return format.replace(/\{(\d+)\}/g, function (m, i) {
			return (i < 0) ? "" : args[Number(i) + 1];
		});
	}

	var getTriggerEntityType = function () {
		var entityType = "";
		if (mConfig.viewContext) {
			entityType = mConfig.viewContext.EntityType;
		}
		return entityType || getActionProperty("TriggerEntityType")
	};

	var getVerbName = function () { return mContainer.find("select[id*=ctrVerbName]").val(); };
	var getServerType = function () { return mContainer.find("div.server-type :checked").val(); };

	var getNodeId = function () { return mContainer.find("select[id*=ctrNodes]").val(); }
	var getEntityType = function () {
		var val = mContainer.find("select[id*=ctrEntities]").val();
		if (val && val.indexOf("-") > -1) {
			return val.split("-")[0];
		}
		return "";
	};
	var getEntityId = function () {
		var val = mContainer.find("select[id*=ctrEntities]").val();
		if (val && val.indexOf("-") > -1) {
			return val.split("-")[1];
		}
		return "";
	}

	var getActionProperty = function (name) {
		var def = mConfig.actionDefinition;
		if (def && def.ActionProperties) {
			for (var i = 0; i < def.ActionProperties.length; i++) {
				var prop = def.ActionProperties[i];
				if (prop.PropertyName == name) {
					return prop.PropertyValue || "";
				}
			}
		}
		return "";
	}
	var getActionDefinition = function () {
		var data = {
			TriggerEntityType: getTriggerEntityType(),
			VerbName: getVerbName(),
			NodeId: "",
			EntityType: "",
			EntityId: ""
		};
		if (getServerType() == "custom") {
			data.NodeId = getNodeId();
			data.EntityType = getEntityType();
			data.EntityId = getEntityId();
		}
		return data;
	}

	/*event handlers*/
	var loadData = function () {
		var el = mContainer.find(".server-info-container");
		el.find(".server, .entities").hide();
		el.find(".loading").show();

		var onSuccess = function (result) {
			mData = result;

			el.find("div.server, div.entities").show();
			el.find("div.loading").hide();

			if (mData == null || mData.length == 0) {
				mData = null;
				renderError("Could not find any IIS Server.");
				return;
			}
			renderNodes(getActionProperty("NodeId"));
			renderEntities(getActionProperty("EntityType"), getActionProperty("EntityId"));
		};
		var onFail = function (error) {
			el.find(".server, .entities").hide();
			el.find(".loading").hide();

			renderError(error);
		};

		SW.Core.Services.callControllerSync("/api/ExecuteSwisVerbs/LoadData", {}, onSuccess, onFail);
	};

	var renderNodes = function (nodeId) {
		var elNodes = mContainer.find("select[id*=ctrNodes]");
		elNodes.empty();

		if (mData == null) { return; }

		$.each(mData, function () {
			elNodes.append($("<option>", { value: this.ID, text: this.Name, selected: this.ID == nodeId }));
		});
	};
	var renderEntities = function (entityType, entityId) {
		var elEntities = mContainer.find("select[id*=ctrEntities]");
		elEntities.empty();

		if (mData == null) { return; }

		var id = SF("{0}-{1}", entityType, entityId);
		$.each(mData, function () {
			if (getNodeId() == this.ID) {

				$.each(this.Sites, function () {
					var value = SF("{0}-{1}", mConfig.SWIS_SITE_NAME, this.ID);
					var html = "<img src='/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small' class='apm_StatusIcon'>" +
						" {2} (<b>Site</b>)";
					html = SF(html, mConfig.SWIS_SITE_NAME, this.Status, this.Name);
					elEntities.append($("<option>", { value: value, text: "", selected: value == id }).html(html));
				});
				$.each(this.Pools, function () {
					var value = SF("{0}-{1}", mConfig.SWIS_POOL_NAME, this.ID);
					var html = "<img src='/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small' class='apm_StatusIcon'>" +
						" {2} (<b>Application Pool</b>)";
					html = SF(html, mConfig.SWIS_POOL_NAME, this.Status, this.Name);
					elEntities.append($("<option>", { value: value, text: "", selected: value == id }).html(html));
				});
				return false;
			}
		});
	};
	var renderError = function (error) {
		mContainer.find(".error-container").html(error);
	};

	var initState = function () {
		if (mConfig.actionDefinition) {
			var verbName = getActionProperty("VerbName");
			mContainer.find("select[id*=ctrVerbName]").val(verbName)

			var nodeId = getActionProperty("NodeId");
			if (nodeId == null || nodeId == "") {
				return;
			}
			mContainer.find("div.server-type input[value=custom]").click();
		}
	};

	/*event handlers*/
	var onNodeChanged = function () {
		renderError("");
		renderEntities();
	};
	var onServerTypeChanged = function () {
		renderError("");
		var el = mContainer.find(".server-info-container");

		if (getServerType() == "from-trigger") { el.hide(); return; }

		el.show();
		if (mData == null) {
			loadData();
		}
	}

	/*public methods*/
	this.validateSectionAsync = function (sectionID, callback) {
		var valid = true;
		if (sectionID == "apmIisExecuteSwisVerbs") {

			if (mConfig.multiEditMode) {
				valid = false;
                renderError("@{R=APM.Strings;K=APMWEBJS_CurrentActionDoesNotSupportMultiEditMode;E=js}");
			} else {
				var serverType = getServerType();
				if (serverType == "from-trigger") {

					var entityType = getTriggerEntityType();
					if (entityType != mConfig.SWIS_SITE_NAME && entityType != mConfig.SWIS_POOL_NAME) {
						valid = false;
                        renderError("@{R=APM.Strings;K=APMWEBJS_IisBlackBox_InvalidTriggerNetObject;E=js}");
                    }
				} else if (serverType == "custom") {

					var definition = getActionDefinition();
					if (definition.NodeId == "" || definition.EntityType == "" || definition.EntityId == "") {
						valid = false;
					}
				}
			}
		}

		if ($.isFunction(callback)) {
			callback(valid);
		}
	};
	this.getUpdatedActionProperies = function (callback) {
        renderError("@{R=APM.Strings;K=APMWEBJS_CurrentActionDoesNotSupportMultiEditMode;E=js}");
    };

	this.getActionDefinitionAsync = function (callback) {

		var onSuccess = function (result) {
			if ($.isFunction(callback)) { callback({ isError: false, actionDefinition: result }); }
		};
		var onFail = function (error) {
			if ($.isFunction(callback)) { callback({ isError: true, ErrorMessage: error }); }
		};

		var definition = getActionDefinition();
		if (mConfig.actionDefinition) {

			definition.Action = mConfig.actionDefinition;
			SW.Core.Services.callController("/api/ExecuteSwisVerbs/Update", definition, onSuccess, onFail);
		} else {
			SW.Core.Services.callController("/api/ExecuteSwisVerbs/Create", definition, onSuccess, onFail);
		}
	};

	this.init = function () {

		mContainer = $("#" + mConfig.containerID);

		mContainer.find("div.server-type input")
			.on("change", onServerTypeChanged)
			.change();
		mContainer.find("select[id*=ctrNodes]")
			.on("change", onNodeChanged);

		if (mConfig.multiEditMode) {
            renderError("@{R=APM.Strings;K=APMWEBJS_CurrentActionDoesNotSupportMultiEditMode;E=js}");
		}

		if ($.isFunction(mConfig.onReadyCallback)) {
			mConfig.onReadyCallback(mThat);
		}
		initState();
	};
};