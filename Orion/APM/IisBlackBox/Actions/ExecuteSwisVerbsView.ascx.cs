﻿using System;
using SolarWinds.APM.BlackBox.IIS.Actions.ExecuteSwisVerbs;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_APM_IisBlackBox_Actions_ExecuteSwisVerbsView : ActionPluginBaseView
{
	public override string ActionTypeID
	{
		get { return ExecuteSwisVerbsConstants.ActionTypeID; }
	}
}