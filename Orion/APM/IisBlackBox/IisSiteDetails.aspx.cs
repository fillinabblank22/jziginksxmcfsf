﻿using System;

using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_APM_IisBlackBox_IisSiteDetails : IisOrionView, ISiteProvider, ITimePeriodProvider, IDynamicInfoProvider
{
	public override string ViewKey
	{
		get { return "IIS BlackBox Site Details"; }
	}

	public override string ViewType
	{
		get { return "IIS BlackBox Site Details"; }
	}

	public Site IisSite
	{
		get { return NetObject as Site; }
	}

	public override IisApplication IisApplication
	{
		get { return IisSite.IisApplication; }
	}

	public int ApplicationId
	{
		get { return IisSite.ApplicationId; }
	}
	
	public int ApplicationItemId
	{
		get { return IisSite.SiteModel.Id; }
	}

	protected override void OnInit(EventArgs e)
	{
		resHost.Site = Site;
		resContainer.DataSource = ViewInfo;
		resContainer.DataBind();

		base.OnInit(e);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
		title.ViewSubTitle = IisSite.Name;
	    title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.IISSite,
	        IisSite.SiteModel.Status.HasValue ? IisSite.SiteModel.Status.Value : 0);

		if (Profile.AllowCustomize) 
		{
			topRightLinks.CustomizeViewHref = CustomizeViewHref;
		}

		if (ApmRoleAccessor.AllowAdmin && IisApplication != null)
		{
			topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationItemPageUrl(ApplicationId, ApplicationItemId);
            topRightLinks.EditNetObjectText = Resources.APM_IisBBContent.EditApplicationItemTitleWebSite;
		}
        topRightLinks.HelpUrlFragment = "SAMAGAppInsIISWebDetTop";
	}

    #region ITimePeriodProvider

    public DateTime? TimePeriodStartDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDate().Value; }
    }

    public DateTime? TimePeriodEndDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDate().GetValueOrDefault(DateTime.Now); }
    }

    public DateTime? TimePeriodStartDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDateUTC().Value; }
    }

    public DateTime? TimePeriodEndDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDateUTC().GetValueOrDefault(DateTime.UtcNow); }
    }

    public int GetRelativeTimePeriodMinutes()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutes();
    }

    public int GetRelativeTimePeriodMinutesUTC()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutesUTC();
    }

    public Boolean TimePeriodIsSetByUser()
    {
        return TimePeriodPicker.PeriodFilter.IsSetInQueryString(Request);
    }

    #endregion

    public object GetValue(string key)
    {
		var result = new IisSiteDynamicInfoHelper(this, ApplicationItemId).GetValue(key);
		if (result == null)
			result = this.GetDynamicInfoValue(key);

		return result;
    }
}