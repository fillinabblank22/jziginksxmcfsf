﻿using System;
using System.Reflection;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;

public partial class Orion_APM_IisBlackBox_IisApplicationPoolDetails : IisOrionView, IApplicationPoolProvider, ITimePeriodProvider
{
    protected const string SolarwindsApmSubviewKey = "IIS BlackBox Application Pool Solarwinds APM";

    private readonly Log log = new Log();

    protected override void OnInit(EventArgs e)
    {
        resHost.ApplicationPool = ApplicationPool;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // title
        title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        title.ViewSubTitle = ApplicationPool.Name;
        title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.IISApplicationPool, (int)ApplicationPool.ApmStatus.Value);

        // customize
        if (Profile.AllowCustomize)
            topRightLinks.CustomizeViewHref = CustomizeViewHref;

        // edit
        if (ViewInfo.ViewKey == SolarwindsApmSubviewKey)
        {
            topRightLinks.EditNetObjectVisible = false;
        }
        else
        {
            if (ApmRoleAccessor.AllowAdmin && IisApplication != null)
            {
                topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationItemPageUrl(IisApplication.Id, ApplicationPool.Id);
                topRightLinks.EditNetObjectText = Resources.APM_IisBBContent.EditApplicationItemTitleApplicationPool;
            }
        }
        topRightLinks.HelpUrlFragment = "SAMAGAppInforIISAppPoolDetails";
    }

    public override string ViewKey
    {
        get { return "IIS BlackBox Application Pool Summary"; }
    }

    public override string ViewType
    {
        get { return "IIS BlackBox Application Pool Details"; }
    }

    #region Interface members

    public ApplicationPool ApplicationPool
    {
        get { return (ApplicationPool)NetObject; }
    }

    public override IisApplication IisApplication
    {
        get { return ApplicationPool.IisApplication; }
    }

    public int ApplicationItemId
    {
        get { return ApplicationPool.Id; }
    }

    #endregion

    public override void SelectView()
    {
        try
        {
            base.SelectView();
        }
        catch (TargetInvocationException e)
        {
            if (e.InnerException != null)
                //rethrow inner exception but keep also original call stack
                //we want to show on UI the inner exception - like "You have no permission to open the page"
                System.Runtime.ExceptionServices.ExceptionDispatchInfo.Capture(e.InnerException).Throw();
            throw;
        }
    }

    #region ITimePeriodProvider

    public DateTime? TimePeriodStartDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDate().Value; }
    }

    public DateTime? TimePeriodEndDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDate().GetValueOrDefault(DateTime.Now); }
    }

    public DateTime? TimePeriodStartDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDateUTC().Value; }
    }

    public DateTime? TimePeriodEndDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDateUTC().GetValueOrDefault(DateTime.UtcNow); }
    }

    public int GetRelativeTimePeriodMinutes()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutes();
    }

    public int GetRelativeTimePeriodMinutesUTC()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutesUTC();
    }

    public Boolean TimePeriodIsSetByUser()
    {
        return TimePeriodPicker.PeriodFilter.IsSetInQueryString(Request);
    }

    #endregion

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}