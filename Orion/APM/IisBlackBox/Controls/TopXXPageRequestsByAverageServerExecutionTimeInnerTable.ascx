﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXPageRequestsByAverageServerExecutionTimeInnerTable.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_TopXXPageRequestsByAverageServerExecutionTimeInnerTable" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:Include ID="JSResources" runat="server" Module="APM" File="/IisBlackBox/Js/Resources.js"/>
<orion:Include ID="CssResources" runat="server" Module="APM" File="/IisBlackBox/Styles/Resources.css"/>
    <apm:CustomQueryTable runat="server" ID="CustomTable"/>
    <script type="text/javascript">
        $(function () {
            SW.APM.Resources.CustomQuery.initialize(
                {
                    uniqueId: "<%= UniqueId %>",
                    cls: "iisInnerGrid",
                    initialPage: 0,
                    rowsPerPage: 5,                        
                    allowSort: true,
                    allowPaging: true,
                    allowSearch: false,
                    defaultOrderBy: '[RequestDate] DESC',
                    underscoreTemplateId: "#contentTemplate-<%= UniqueId %>",
                    headerTitles: [
                        "<%= Resources.APM_IisBBContent.TopXXPageRequestByAvrServerExecutionTimeInnerTabVerb%>",
                        "<%= Resources.APM_IisBBContent.TopXXPageRequestByAvrServerExecutionTimeInnerTabDate%>",
                        "<%= Resources.APM_IisBBContent.TopXXPageRequestByAvrServerExecutionTimeInnerTabTime%>",
                        "<%= Resources.APM_IisBBContent.TopXXPageRequestByAvrServerExecutionTimeInnerTabIP%>",
                        "<%= Resources.APM_IisBBContent.TopXXPageRequestByAvrServerExecutionTimeInnerTabURLQuery%>"]
                });
            var refresh = function () { SW.APM.Resources.CustomQuery.refresh(<%= UniqueId %>); };
            SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
            refresh();
        });
    </script>

    <script id="contentTemplate-<%= UniqueId %>" type="text/template">
        {#
            var verb          = [Columns[0]];
            var requestDate   = Columns[1]==null?"":Columns[1].toLocaleString();
            var elapsedTime   = SW.APM.IisBB.Resources.msConverter(Columns[2]);
            var clientIp      = [Columns[3]];
            var wraper        = $( ".ResourceWrapper " );
            var urlQuery      = SW.APM.IisBB.Resources.wrapUrl(SW.APM.Resources.htmlEscape([Columns[4]]), wraper.width() * 0.3);
            var url           = SW.APM.Resources.htmlEscape(SW.APM.IisBB.Resources.getWebsiteLink(Columns[6],Columns[7],Columns[8],Columns[9],Columns[10]) + [Columns[5]]);
        #}  
        <tr>
            <td><span>{{ verb }}</span></td>
            <td class="requestDateCell"><span>{{ requestDate }}</span></td>
            <td class="positionCenter"><span>{{ elapsedTime }}</span></td>
            <td><span>{{ clientIp }}</span></td>
            <td><span>{{ SW.APM.IisBB.Resources.getLink(url, urlQuery, true) }}</span></td>
        </tr>
    </script>
