﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IisServerConfigurator.ascx.cs" Inherits="Orion_APM_IisBlackBox_Controls_IisServerConfigurator" %>

<%@ Import Namespace="SolarWinds.APM.Web" %>
<orion:Include ID="Include2" runat="server" File="APM/IisBlackBox/Styles/IisServerConfigurator.css"/>
<orion:Include ID="i1" File="APM/IisBlackBox/Js/IisServerConfigurator.js" Section="Bottom" runat="server" />
<orion:Include ID="Include1" File="APM/IisBlackBox/Js/IisServerConfiguratorExecutor.js" Section="Bottom" runat="server" />

<script id="iis-bb-app-name-tpl" type="text/x-sw-template">
	<img src="/Orion/StatusIcon.ashx?entity=Orion.APM.IIS.Application&status={4}&size=small" /> {0} <%= Resources.APMWebContent.ON %> <img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={1}&status={2}&size=small" /> {3} 
</script>
<script id="iis-bb-app-load" type="text/x-sw-template">
	<img width="16" height="16" src="/Orion/images/animated_loading_sm3_whbg.gif"><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_LoadingApp%>
</script>
<div id="iisBbConfigDialog" title="<%= Resources.APM_IisBBContent.ZeroConfig_Dialog_Title%>" style="border:1px solid lightgray; margin-top: 5px; background-color: #F8F8F8; display: none; z-index: 99999; ">
	<div class="label"><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_Description%>
        <a target="_blank" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInforIISWhatChangesMade") %>">  » <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_InfoLink%></a>
        <a target="_blank" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInforIISLearnMore") %>">  » <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_LearnMore%></a>
 </div>
    <div class="sub-title"><div><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_EnterCred%></div>
</div>
<div id="line2" class="line horizontal"></div>
    	<div class="label" style="padding-top: 3px; ">
        <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_CredsDescr%><a target="_blank" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInforIISFindCreds") %>">» <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_FindCreds%></a>
	</div>
	<table>
		<colgroup>
			<col width="20px" />
			<col width="200px" />
			<col width="380px" />
			<col width="20px" />
		</colgroup>
		<tbody>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ChooseCredentials%></td>
				<td>
					<select id="iisBbCreds" size="1"></select>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_CredentialName%></td>
				<td> <input id="iisBbCredName" type="text" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_UserName%></td>
				<td> <input id="iisBbUserName" type="text" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_Password%></td>
				<td><input id="iisBbPassword1" type="password" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfirmPassword%></td>
				<td><input id="iisBbPassword2" type="password" /> </td>
				<td>&nbsp;</td>
			</tr>
            <tr>
				<td>&nbsp;</td>
				<td></td>
				<td style="padding-top: 10px;">
				   <orion:LocalizableButton ID="btnTest" LocalizedText="CustomText" Text="<%$ Resources: APM_IisBBContent, ZeroConfig_Dialog_TestCredentialsButton %>" DisplayType="Small" runat="server" CssClass="btn-test" />
                   <span id="iis-test-inprogress-container" style="display: none;"> &nbsp; <img src="/Orion/APM/Images/loading_gen_16x16.gif"/> <label> <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_TestingCredentials%> </label></span>
                   <span class="iis-test-success"> <img src="/Orion/Apm/Images/config_test_passed.png"/> <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_CredentialsOk%></span>
                   <span class="iis-test-fail"> 
					   <img src="/Orion/Apm/Images/config_test_failed.png"/> 
					   <label id="test-failed"><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_CredentialsFailed%></label>
                   </span>
					<div class="iis-test-fail-more" id = "iisTestFailMoreContainer" title="Test Details" style="display: none;">
						<p></p>
					</div>
                    <div  style="padding-top: 1px">&nbsp;</div>
					<div class="iis-test-success-more" id = "IisTestSuccessMoreContainer" title="Test Success Details">
						<p><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_CredentialsOkMore%></p>
					</div>
				</td>
				<td>&nbsp;</td>
			</tr>
            <tr>
                <td colspan="4" style="padding-top: 15px">
                    <div><img id="iis-expander" src="/Orion/images/Button.Expand.gif" state ="collapsed"/>
                        <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_AdvancedLabel%></div>
                    <div class = "iis-expander-container" >
                        <div class="conf-sec"><b><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfigureIisServer_ManualLabel%></b></div>
                        <div class="gray-label"><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfigureIisServer_ManualInfo%></div>
                        <div class="conf-sec">
                            <a target="_blank" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInforIISManuallyEnable") %>"> <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfigureIisServer_ManualLink%></a>
                            <a  style ="padding-left: 65px" href="#" id="iisServerIsAlreadyConfiguredLink" > <%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfigureIisServer_AlreadyConfiguredLink%></a>
                        </div>
                        <div class="conf-sec"><b><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfigureIisServer_UserConnTitle%></b></div>
                        <div class="gray-label"><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfigureIisServer_UserConnInfo%></div>
                        <div class="conf-sec"><a target="_blank" id="iisConfigEditAppLink" href="#"><%= Resources.APM_IisBBContent.ZeroConfig_Dialog_ConfigureIisServer_EditAppLink%></a></div>
                    </div>
                </td>
            </tr>
            <tr class="iis-setup-info">
				<td colspan="4">
					<div class="load">
					    <table style="width:100%">
                            <tr>
                                <td style="width: 50px"><div><img src="/Orion/Apm/Images/animated_loading45x45.gif"/> </div></td>
                                <td>
                                    <div class="iis-config-in-progress-msg" style="font-size: 8px;" ></div>
                                </td>
                                <td>
                                    <div class ="connection-success"></div>
                                    <div class ="connection-fail"></div>
                                </td>
                            </tr>

					    </table>
					</div>
					<div class="status"> </div>
                    <div class="config-validation-error"></div>
                    <div class="error">
                        <div id="iisConfiguratorErrorMessageContainer" class="status-details-container" status="Undefined" style="display: none">
                            <table>
                                <tr>
                                    <td style="width: 15px;vertical-align: top;"><img src="/orion/apm/images/cofig_error_ico.png" /></td>
                                    <td><label id="iisConfiguratorErrorMessage" class="config-error-message"></label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><div id="iisBbApps"></div></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" style="text-align: right;">
					<orion:LocalizableButton ID="btnSetup" Text="<%$ Resources: APMWebContent, CONFIGURE_SERVER %>" DisplayType="Primary" runat="server" CssClass="btn-setup" />
					<orion:LocalizableButton ID="btnCancel" Text="<%$ Resources: APMWebContent, CLOSE %>" DisplayType="Secondary" runat="server" CssClass="btn-cancel" />
				</td>
			</tr>
		</tfoot>
	</table>
</div>

