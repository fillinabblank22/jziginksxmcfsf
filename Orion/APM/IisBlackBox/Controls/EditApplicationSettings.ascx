﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationSettings.ascx.cs" Inherits="Orion_APM_IisBlackBox_Controls_EditApplicationSettings" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.IIS.Common" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="TestButtonSummary" Src="~/Orion/APM/Controls/TestButtonSummary.ascx" %>

<orion:include runat="server" file="APM/js/EditBBApplication.js" />
<orion:include runat="server" file="APM/IisBlackBox/js/EditApplicationSettings.js" />
<orion:include runat="server" file="APM/IisBlackBox/Styles/EditApplicationSettings.css" />
<orion:include runat="server" file="APM/IisBlackBox/js/IisServerConfiguratorExecutor.js" section="Bottom" />

<script type="text/javascript">
    SW.APM.BB.IIS.EditAppSettings.init({
        AppType: '<%= ControlHelper.EncodeJsString(Constants.ApplicationTemplateCustomType) %>',
        PsUrlWindowsKey: '<%= ControlHelper.EncodeJsString(Constants.PsUrlWindowsKey) %>',
        SettingLevelInstance: '<%= (int) SettingLevel.Instance %>',
        AgentPollingMethodName: '<%= ControlHelper.EncodeJsString(AgentPollingMethodName) %>',
        CredentialSetIdKey: '<%= ControlHelper.EncodeJsString(InternalSettings.CredentialSetIdKey) %>',
        TestSummaryClientId: '<%= ControlHelper.EncodeJsString(this.iisTestSummary.ClientID) %>',
        TestButtonClientId: '<%= ControlHelper.EncodeJsString(this.testButton.ClientID) %>',
        ConfigButtonClientId: '<%= ControlHelper.EncodeJsString(this.ConfigureIisServerButton.ClientID) %>',
        WindowsTooltipClientId: '<%= ControlHelper.EncodeJsString(this.windowsTooltip.ClientID) %>',
        WindowsTooltipContent: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_ToolTip_WndorsURL) %>',
        SaveCredentialChangesTitle: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Title) %>',
        SaveCredentialChangesMsg: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Msg) %>',
        MsgTestSuccessful: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_ConnTestSuccess) %>',
        MsgCredTestFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_CredFailed) %>',
        MsgWebAdminFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_PowerShellWebAdministrationFailed) %>',
        MsgPsUrlWindowsFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_PsUrlWindowsFailed) %>',
        MsgVersionCheckFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_VersionCheckFail) %>',
        MsgZeroConfigConnError: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_ConnError) %>',
        MsgZeroConfigTestConf: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_TestingConf) %>',
        TitleZeroConfigTestConfFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_ConfigurationTestFailed_ErrorTitle) %>',
        MsgZeroConfigTestCred: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_TestingCred) %>',
        TitleZeroConfigTestCredFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_CredentialsTestFailed_ErrorTitle) %>',
        MsgZeroConfigPsAlreadyPrepared: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_PowerShell_Already_Prepared_Message) %>',
        MsgZeroConfigCredError: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_CredErrorMsg) %>',
        MsgZeroConfigCompatibilityFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_VersionCompatibilityTestFailed_ErrorTitle) %>',
        TitleZeroConfigCompatibilityFailed: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_VersionCompatibilityTestFailed_ErrorTitle) %>',
        MsgConfiguringServer: '<%= ControlHelper.EncodeJsString(Resources.APM_IisBBContent.ZeroConfig_EditPage_ConfiguringServer) %>'
    });
</script>
<table id="appEditIisSettings" class="appProperties" cellspacing="0" style="display: none;">
    <tr>
        <td class="label"></td>
        <td></td>
        <td rowspan="4" style="padding-left: 10px">
            <div class="IisTipsInstructions" runat="server" id="credentialsContainer" style="font-size: 11px;">
                <h2><b><%= Resources.APM_IisBBContent.ZeroConfig_EditPage_Title%></b></h2>
                <label><%= Resources.APM_IisBBContent.ZeroConfig_EditPage_Description%></label>
                <div class="ConfigBtnContainer">
                    <orion:localizablebutton runat="server" id="ConfigureIisServerButton" text="<%$ Resources: APMWebContent, CONFIGURE_SERVER %>" displaytype="Secondary" onclientclick="" />
                </div>
                <table>
                    <tr>
                        <div class="iis-config-result">
                            <div class="iis-conf-success">
                                <div style="font-size: 11px;">
                                    <img src="/Orion/Apm/Images/config_test_passed.png" /><%= Resources.APM_IisBBContent.ZeroConfig_EditPage_SuccessConf%></div>
                            </div>
                        </div>
                        <div class="config-validation-error"></div>
                    </tr>
                    <tr class="iis-setup-info">
                        <div class="iis-error" style="padding-bottom: 15px; word-wrap: break-word;">
                            <div id="iisConfiguratorErrorMessageContainer" class="status-details-container" status="Undefined">
                                <img src="/orion/apm/images/cofig_error_ico.png" />
                                <label id="iisConfiguratorErrorMessage" class="iis-config-error-message"></label>
                            </div>
                        </div>
                    </tr>
                </table>
                <div class="ConfigHelpLink" style="font-size: 11px;">
                    <label><a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("SAMAGAppInforIISManuallyEnable") %>" target="_blank">&#0187;<%= Resources.APM_IisBBContent.ZeroConfig_EditPage_ConfLink%></a></label>
                </div>
            </div>
        </td>
    </tr>
    <tr id="psUrlWindowsRow">
        <td class="label">
            <asp:Literal ID="psUrlWindowsLabel" runat="server" />
        </td>
        <td style="width: 450px;">
            <input class="ShortInput" id="psUrlWindows" name="psUrlWindows" class="text" size="50" maxlength="250" />
            <asp:Image runat="server" ID="windowsTooltip" ImageUrl="~/Orion/APM/Images/Admin/icon_lightbulb.gif" CssClass="tooltipImage" />
            <span id="psIisUrlWindowsError" class="validationMsg sw-suggestion sw-suggestion-fail">
                <span class="sw-suggestion-icon"></span>
                <span class="validationError">
                    <label class="error"></label>
                </span>
            </span>
            <br />
        </td>
    </tr>
    <tr>
        <td class="label"><%= Resources.APM_IisBBContent.ZeroConfig_EditPage_RowLabel%>
        </td>
        <td style="height: 170px">
            <table class="selectCreds" style="width: 410px">
                <apm:selectcredentials runat="server" id="iisCredentials" validationgroupname="SelectCredentialsValidationGroup" allownodewmicredential="True" />
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td class="testRow" style="width: 430px">
            <apm:testbuttonsummary runat="server" id="iisTestSummary" />
            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="SelectCredentialsValidationGroup" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="testButtonRow">
            <orion:localizablebutton runat="server" id="testButton" text="<%$ Resources: APMWebContent, TEST_CONNECTION %>" displaytype="Secondary" />
        </td>
        <td></td>
    </tr>
</table>
