﻿using System;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;

public partial class Orion_APM_IisBlackBox_Controls_EditApplicationSettings : System.Web.UI.UserControl
{
    protected string AgentPollingMethodName
    {
        get
        {
            return EnumHelper.GetLocalizedDescription(PollingMethod.Agent);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.psUrlWindowsLabel.Text = Resources.APM_IisBBContent.ZeroConfig_EditPage_WindowsUrlLabel;
    }
}