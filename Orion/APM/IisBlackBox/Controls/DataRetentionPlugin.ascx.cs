﻿using SolarWinds.APM.BlackBox;
using SolarWinds.APM.BlackBox.IIS.Common.Models;
using SolarWinds.APM.Web;

public partial class Orion_APM_IisBlackBox_Controls_DataRetentionPlugin : BlackBoxConfigPluginBase
{
    protected Config ConfigState
    {
        get { return this.ViewState.GetOrCreate<Config>("config_iisbb"); }
        set { this.ViewState["config_iisbb"] = value; }
    }


    public override void SaveData()
    {
        var config = this.ConfigState;
        config.RetainDetailTablesDays = ParseDays(this.RetainDetailTablesDays);
        WithBL(bl => this.ConfigState = (Config) bl.EditConfig(config));
    }

    public override void LoadData()
    {
        WithBL(bl => this.ConfigState = (Config) bl.GetConfig(this.ConfigState));
    }

    public override void RestoreDefaults()
    {
        WithBL(bl => this.ConfigState = (Config) bl.RestoreDefaultConfig(this.ConfigState));
    }

    public override void RefreshData()
    {
        var config = this.ConfigState;
        PrintDays(this.RetainDetailTablesDays, config.RetainDetailTablesDays);
    }
}
