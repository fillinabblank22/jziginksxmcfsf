﻿<%@ Control Language="C#" ClassName="AllApplicationsPlugin" %>
<%@ Register TagPrefix="apm" TagName="IisBbConfigDlg" Src="~/Orion/APM/IisBlackBox/Controls/IisServerConfigurator.ascx" %>
<apm:IisBbConfigDlg runat="server" />

<script type="text/javascript">
    //<![CDATA[
    var showIisBBConfigDialog = function (appId) {
        try {
            SW.APM.IisBB.ConfigDialog.show(appId);
        } catch (ex) {
            alert(ex);
        }
        return false;
    };
    //]]>
</script>
