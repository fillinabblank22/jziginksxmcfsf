﻿using System;

using SolarWinds.APM.BlackBox.IIS.Web.UI;

public partial class Orion_APM_Resources_IisBlackBox_TopXXPageRequestsByAverageServerExecutionTimeInnerTable : System.Web.UI.UserControl, IPageRequestDetails
{
    public int RequestID { get; set; }
    public int UniqueId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var swqlQuery = string.Format(@"
SELECT 
    rd.Verb, 
    toLocal(rd.RequestDate) as RequestDate, 
    rd.ElapsedTime, 
    rd.ClientIP, 
    rd.URLQuery,
    CASE 
	    WHEN (rd.URLQuery IS NULL) THEN r.URLStem 
		ELSE (r.URLStem + '?' + rd.URLQuery) 
	END as [_url],
    s.Protocol as [_protocol],
    s.nodeIPAddress as [_nodeIPAddress],
    s.IpAddress as [_ipAddress],
    s.hostName as [_hostName], 
    s.Port as [_port]
FROM Orion.APM.IIS.RequestDetails rd
LEFT JOIN Orion.APM.IIS.Request r ON rd.RequestID = r.ID
LEFT JOIN (
SELECT TOP 1
    b.SiteID,
    b.Protocol, 
    b.IpAddress, 
    b.hostName, 
    b.Port,
    b.Site.Application.Node.IP_Address as nodeIPAddress
    FROM Orion.APM.IIS.SiteBinding b
    INNER JOIN Orion.APM.IIS.Request sr ON b.SiteID = sr.SiteID
    WHERE b.Protocol Like 'http%' AND sr.ID = {0}) s ON s.SiteID = r.SiteID
WHERE rd.RequestID = {0}", RequestID);

        CustomTable.UniqueClientID = UniqueId;
        CustomTable.SWQL = swqlQuery;
    }
}