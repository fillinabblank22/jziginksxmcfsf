﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataRetentionPlugin.ascx.cs" Inherits="Orion_APM_IisBlackBox_Controls_DataRetentionPlugin" %>

		<tr class="header">
			<td colspan="3"><h2><%= Resources.APM_IisBBContent.ConfigDataRetention_Title %></h2></td>
		</tr>

        <tr>
            <td class="PropertyHeader"><%= Resources.APM_IisBBContent.ConfigDataRetention_RetainDetailTablesDays %></td>
            <td class="Property" style="white-space: nowrap">
                <asp:TextBox ID="RetainDetailTablesDays" runat="server" Width="60px" Style="text-align: right;" MaxLength="5"></asp:TextBox>
                <%= Resources.APM_IisBBContent.ConfigDataRetention_RetainDetailTablesDays_Unit %>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="RetainDetailTablesDays"
                    ErrorMessage="<%$ Resources:APM_IisBBContent,ConfigDataRetention_RetainDetailTablesDays_ErrorCompare %>"
                    Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RetainDetailTablesDays"
                    ErrorMessage="<%$ Resources:APM_IisBBContent,ConfigDataRetention_RetainDetailTablesDays_ErrorRequired %>">*</asp:RequiredFieldValidator>
            </td>
            <td class="Property"><%= Resources.APM_IisBBContent.ConfigDataRetention_RetainDetailTablesDays_Description %></td>
        </tr>
