﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Top10ProcessesChart.ascx.cs" Inherits="Orion_APM_IisBlackBox_Controls_Top10ProcessesChart" %>
<%@ Register TagPrefix="apm" TagName="CustomChart" Src="~/Orion/APM/Controls/Charts/CustomChart.ascx" %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>
<orion:NetObjectTips runat="server"/>
<apm:CustomChart runat="server" id="ccStack" LoadOnDemand="false" />
