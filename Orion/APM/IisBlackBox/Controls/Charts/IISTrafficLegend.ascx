﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IISTrafficLegend.ascx.cs" Inherits="Orion_APM_IisBlackBox_Charts_IISTrafficLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.apm_iistrafficLegendInitializer__<%= legend.ClientID %> = function (chart) {
        $('#<%= legend.ClientID %>').empty();
        SW.APM.Charts.IISTrafficLegend.createStandardLegend(chart, '<%= legend.ClientID %>', '<%= NodeName %>', '<%= NodeViewLink %>');
    };

</script>

<table runat="server" id="legend" class="chartLegend"></table>

<div style="float:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>