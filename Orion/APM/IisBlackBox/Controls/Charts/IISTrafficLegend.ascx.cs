﻿using System;
using System.Web.UI;

using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_IisBlackBox_Charts_IISTrafficLegend : UserControl, IChartLegendControl
{
    private Node node;

    public string NodeViewLink
    {
        get { return node != null ? BaseResourceControl.GetViewLink(node.NetObjectID) : null; }
    }

    public string NodeName
    {
        get { return node != null ? node.Name : null; }
    }

    public string LegendInitializer { get { return "apm_iistrafficLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        var parentResource = FindParent<StandardChartResource>(this);
        if (parentResource != null)
        {
            var provider = parentResource.GetInterfaceInstance<IIisApplicationProvider>();
            if (provider != null)
            {
                node = provider.Node;
            }
        }

        OrionInclude.ModuleFile("APM", "IisBlackBox/Js/Charts/Charts.APM.IISTrafficLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "IisBlackBox/Styles/Charts/IisTrafficLegend.css");
    }

    private static T FindParent<T>(Control control)
    {
        var current = control.Parent;

        while (current != null && !(current is T))
        {
            current = current.Parent;
        }

        return (T) (object) current;
    }
}