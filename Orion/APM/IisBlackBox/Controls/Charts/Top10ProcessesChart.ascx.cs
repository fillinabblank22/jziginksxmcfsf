﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.BlackBox.IIS.Web.Charts;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Helpers;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_IisBlackBox_Controls_Top10ProcessesChart : System.Web.UI.UserControl, IPropertyProvider
{
    private class ChartSettings
    {
        public string DataUrl { get; set; }
        public string ChartOptions { get; set; }
        public string ChartEditOptions { get; set; }
        public string LegendUserControl { get; set; }
    }

    public static class PropName
    {
        public const string NetObjectPrefix = "netobjectprefix";
        public const string ChartInitialZoom = "chartinitialzoom";
        public const string ChartDateSpan = "chartdatespan";
        public const string ChartSampleSize = "chartsamplesize";
        public const string ChartSubtitle = "chartsubtitle";
        public const string ChartTitle = "charttitle";
    }

    private ChartSettings settings;

    private readonly Log log = new Log();

    private readonly Dictionary<string, object> dataProperties = new Dictionary<string, object>();

    public IDictionary<string, object> Properties
    {
        get { return this.dataProperties; }
    }

    public string NetObjectPrefix
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.NetObjectPrefix, string.Empty)); }
        set { this.dataProperties[PropName.NetObjectPrefix] = value; }
    }

    public string ChartInitialZoom
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartInitialZoom, string.Empty)); }
        set { this.dataProperties[PropName.ChartInitialZoom] = value; }
    }

    public string ChartDateSpan
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartDateSpan, string.Empty)); }
        set { this.dataProperties[PropName.ChartDateSpan] = value; }
    }

    public string ChartSampleSize
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartSampleSize, string.Empty)); }
        set { this.dataProperties[PropName.ChartSampleSize] = value; }
    }

    public string ChartSubtitle
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartSubtitle, string.Empty)); }
        set { this.dataProperties[PropName.ChartSubtitle] = value; }
    }

    public string ChartTitle
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartTitle, string.Empty)); }
        set { this.dataProperties[PropName.ChartTitle] = value; }
    }

    public Orion_APM_IisBlackBox_Controls_Top10ProcessesChart()
    {
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.InitAreaChart();
    }

    private void InitAreaChart()
    {
        OrionInclude.ModuleFile("APM", "IisBlackBox/js/Charts/Charts.APM.Top10ProcessesChart.js");

        LegendInitializerHelper.AddInitializer_IisProcessTop10ChartLegend(this.ccStack.LegendTable, this.GetNodeName(), this.GetNodeViewLink());
        this.ccStack.CustomLegendInitializer = LegendInitializerHelper.GetInitializerName_Top10ChartLegend(this.ccStack.LegendTable);

        this.settings = this.GetChartSettings();
        this.ccStack.CustomChartOptions = this.settings.ChartOptions;
        this.ccStack.CustomChartSettings = this.GetCustomChartSettings();
    }

    protected override void OnPreRender(EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.UniqueID, this.GetAreaChartScript(), true);

        base.OnPreRender(e);
    }

    private ChartSettings GetChartSettings()
    {
        const string Query = @"
SELECT [DataUrl], [ChartOptions], [LegendUserControl], [ChartEditOptions]
FROM [ChartSettings]
WHERE [NetObjectPrefix] = @NetObjectPrefix";

        var data = new ChartSettings();

        try
        {
            using (var cmd = SqlHelper.GetTextCommand(Query))
            {
                cmd.Parameters.AddWithValue("@NetObjectPrefix", this.NetObjectPrefix);

                using (var reader = SqlHelper.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        data.DataUrl = DatabaseFunctions.GetString(reader, 0);
                        data.ChartOptions = DatabaseFunctions.GetString(reader, 1);
                        data.LegendUserControl = DatabaseFunctions.GetString(reader, 2);
                        data.ChartEditOptions = DatabaseFunctions.GetString(reader, 3);
                    }
                }
            }
        }
        catch (Exception xcp)
        {
            log.Warn(xcp);
        }

        return data;
    }

    private IisApplication TryGetApplication()
    {
        var brc = this.GetAncestorOfType<BaseResourceControl>();
        if (brc != null)
        {
            var nop = brc.GetInterfaceInstance<IIisApplicationProvider>();
            if (nop != null)
            {
                return nop.IisApplication;
            }
        }
        return null;
    }

    private string GetNetObjectId()
    {
        var iisapp = this.TryGetApplication();
        return (iisapp == null) ? string.Empty : iisapp.NetObjectID;
    }

    private Node dataNode;

    private Node TryGetNode()
    {
        var brc = this.GetAncestorOfType<BaseResourceControl>();
        if (brc != null)
        {
            var np = brc.GetInterfaceInstance<INodeProvider>();
            if (np != null)
            {
                return np.Node;
            }
        }
        return null;
    }

    private Node Node
    {
        get { return this.dataNode = this.dataNode ?? this.TryGetNode(); }
    }

    private string GetNodeName()
    {
        return (this.Node == null) ? string.Empty : this.Node.Name;
    }

    private string GetNodeViewLink()
    {
        return (this.Node == null) ? string.Empty : BaseResourceControl.GetViewLink(this.Node.NetObjectID);
    }

    private Dictionary<string, object> GetCustomChartSettings()
    {
        var details = new Dictionary<string, object>();

        details["dataUrl"] = this.settings.DataUrl;
        details["netObjectIds"] = new[] {this.GetNetObjectId()};
        details["showTitle"] = true;
        details["title"] = string.IsNullOrEmpty(this.ChartTitle) ? this.GetNodeName() : this.ChartTitle;

        details["DaysOfDataToLoad"] = ParseHelper.TryParse(this.ChartDateSpan, 5);
        details["sampleSizeInMinutes"] = ParseHelper.TryParse(this.ChartSampleSize, 5);
        details["initialZoom"] = this.ChartInitialZoom;
        details["subtitle"] = this.ChartSubtitle;

        return details;
    }

    private const string TemplateAreaScript = @"SW.APM.IISBB.Charts.IisProcessTop10Chart.transformCustomChart('{0}');";

    private string GetAreaChartScript()
    {
        return InvariantString.Format(TemplateAreaScript, ControlHelper.EncodeJsString(this.ccStack.ID));
    }
}
