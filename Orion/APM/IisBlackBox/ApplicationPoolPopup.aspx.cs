﻿using System.Globalization;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.APM.BlackBox.IIS.Common;

public partial class Orion_APM_IisBlackBox_ApplicationPoolPopup : PopupPage<ApplicationPool>
{
    protected const int TopComponentsCount = 5;

    private readonly Log log = new Log();

    protected ApplicationPool AppPool
    {
        get { return NetObjectData; }
    }

    protected IisApplication IisApp
    {
        get { return AppPool.IisApplication; }
    }

    protected long PoolId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.PopupMaster.PopupTitle = APM_IisBBContent.ApplicationPool_ToolTip_Header;
        this.PopupMaster.StatusCssClass = StatusCssClass;

        AppStatusIcon.StatusValue = IisApp.Status;
        AppStatusIcon.CustomApplicationType = IisApp.CustomType;

        AppPoolStatusIcon.StatusValue = GetStatus();
        AppPoolStatusIcon.CustomApplicationType = IisApp.CustomType;
        AppPoolStatusIcon.ApplicationItemType = ApplicationItemType.ABIA_ApplicationPool.ToString();
        ServerStatusIcon.StatusValue = IisApp.NPMNode.Status;

        this.componentList.BindComponents(
            IisApp.ComponentsWithProblems.Where(x => x.ApplicationItemID == PoolId),
            TopComponentsCount,
            IisApp.CustomType,
            ApplicationItemType.ABIA_ApplicationPool.ToString());
    }

    #region Overriden methods

    protected override ApmStatus GetStatus()
    {
        return new ApmStatus(NetObjectData.ApmStatus.Value);
    }

    protected override ApplicationPool CreateNetObjectData(string netObjectId)
    {
        var netObject = CreateNetObject();
        var result = netObject as ApplicationPool;

        if (result == null)
        {
            log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render application pool popup, because cannot get application pool info from netObject with netObjectID '{0}'", netObjectId);
        }
        else
        {
            PoolId = result.Id;
        }
        return result;
    }

    #endregion
}
