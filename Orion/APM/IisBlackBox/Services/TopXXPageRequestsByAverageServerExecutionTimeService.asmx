﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.IIS.Web.TopXXPageRequestsByAverageServerExecutionTimeService" %>

namespace SolarWinds.APM.BlackBox.IIS.Web
{
    using System.IO;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.Script.Services;
    
    using UI;
    
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class TopXXPageRequestsByAverageServerExecutionTimeService : WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object LoadInnerTable(string uniqueClientID, int requestID)
        {
            using (var page = new Page())
            {
                var innerTableControl = (IPageRequestDetails)page.LoadControl("/Orion/APM/IisBlackBox/Controls/TopXXPageRequestsByAverageServerExecutionTimeInnerTable.ascx");
                innerTableControl.UniqueId = int.Parse(uniqueClientID);
                innerTableControl.RequestID = requestID;

                var tempForm = new System.Web.UI.HtmlControls.HtmlForm();
                var manager = new ScriptManager {ScriptMode = ScriptMode.Release};
                tempForm.Controls.Add(manager);
                tempForm.Controls.Add((UserControl)innerTableControl);
                page.Controls.Add(tempForm);
                
                using (var writer = new StringWriter())
                {
                    HttpContext.Current.Server.Execute(page, writer, false);
                    
                    return writer.ToString();
                }
            }
        }
    }
}