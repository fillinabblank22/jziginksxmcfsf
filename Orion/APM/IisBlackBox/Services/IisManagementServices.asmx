﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.IIS.Web.IisManagementServices" %>

using System;
using SolarWinds.APM.Common.Credentials;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;

namespace SolarWinds.APM.BlackBox.IIS.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1), ScriptService]
    public class IisManagementServices : ApmWebService
    {
        [WebMethod(EnableSession = true)]
        public EntityManagementState ManageApplicationPool(string entityName, int nodeId, string action, int poolId, int appId, int tempId)
        {
            var state = ToEntityState(entityName, action, poolId, appId);
            state.EntityType = (int)ApplicationItemType.ABIA_ApplicationPool;
            Action<IAPMBusinessLayer, int, int> updateStatus = (bl, s, id) => bl.UpdateApplicationPoolStatus(s, id);
            return ManageEntity(nodeId, tempId, state, updateStatus);
        }

        [WebMethod(EnableSession = true)]
        public EntityManagementState ManageSite(string entityName, int nodeId, string action, int siteId, int appId, int tempId)
        {
            var state = ToEntityState(entityName, action, siteId, appId);
            state.EntityType = (int)ApplicationItemType.ABIA_WebSite;
            Action<IAPMBusinessLayer, int, int> updateStatus = (bl, s, id) => bl.UpdateIisSiteStatus(s, id);
            return ManageEntity(nodeId, tempId, state, updateStatus);
        }

        private EntityManagementState ManageEntity(int nodeId, int tempId,
            EntityManagementState state, Action<IAPMBusinessLayer, int, int> updateStatus)
        {
            VerifyDemo();
            if (ApmRoleAccessor.AllowIisActionRights)
            {
                var factory = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>();
                using (IAPMBusinessLayer businessLayer = factory.Create())
                {
                    var cred = GetApplicationCredentials(businessLayer, state.ApplicationId, tempId, nodeId);
                    var result = businessLayer.ManageEntity(nodeId, cred, false, state);

                    if (result.ActionResult == "Successful" && state.Action != "Restart")
                    {
                        updateStatus(businessLayer, result.EntityState, state.EntityId);
                    }

                    return result;
                }
            }
            return new EntityManagementState();
        }

        private static EntityManagementState ToEntityState(string entityName, string action, int entityId, int appId)
        {
            return new EntityManagementState
            {
                Action = action,
                EntityName = entityName,
                EntityId = entityId,
                ApplicationId = appId,
                ApplicationTypeId = Constants.ApplicationTemplateCustomType
            };
        }

        private CredentialSet GetApplicationCredentials(IAPMBusinessLayer businessLayer, int appId, int tempId, int nodeId)
        {
            var settings = businessLayer.GetApplicationSettings(appId, tempId);
            return CredentialHelper.GetCredentialsFromSettings(businessLayer, nodeId, settings);
        }
    }
}
