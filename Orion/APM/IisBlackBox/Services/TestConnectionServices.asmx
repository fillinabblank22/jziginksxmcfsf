﻿
<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Iis.Web.TestConnectionServices" %>
using System;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.Services;
using TestConnectionResult = SolarWinds.APM.BlackBox.IIS.Common.Models.TestConnectionResult;

namespace SolarWinds.APM.BlackBox.Iis.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class TestConnectionServices : BlackBoxTestWebService
    {
        [WebMethod]
        public TestResultInfo TestIisBlackBoxConnection(TestBlackBoxSettings settings)
        {
            return TestIisBlackbox(settings);
        }
        
        [WebMethod]
        public TestResultInfo TestIisBlackBoxCredential(TestBlackBoxSettings settings)
        {
            settings.TestCredentialOnly = true;
            return TestIisBlackbox(settings);
        }

        private TestResultInfo TestIisBlackbox(TestBlackBoxSettings settings)
        {
            if (settings.NodeId < 1)
            {
                throw new ArgumentException("Node Id is required.");
            }
            if (string.IsNullOrWhiteSpace(settings.CredentialSet.Login) &&
                settings.CredentialSet.Id != ComponentBase.NodeWmiCredentialsId)
            {
                throw new ArgumentException("Credential is required.");
            }
            try
            {
                // Force 64bit. If we are on 32bit system, we will use implicitly 32bit.
                settings.Use64Bit = true;
                var xmlSettings = SerializeAppSettings(settings);

                return TestBlackBoxConnectionInternal(settings, xmlSettings, typeof (TestConnectionResult));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error while serializing app settings: {0}, error: {1}", RequestToString(settings), ex);
                var result = new TestConnectionResult {IsValidCredential = true};
                result.AddErrorMessage(APM_IisBBContent.TestConnectionServices_TestIisBlackbox_Detailed_error_information, ex.Message);

                return new TestResultInfo(false, result);
            }
        }

        protected override string SerializeAppSettings(TestBlackBoxSettings settings)
        {
            var credentialSetId = -3;
			if (!string.IsNullOrWhiteSpace(settings.CredentialSet.Login)) 
			{
				credentialSetId = settings.CredentialSet.Id;
			}
            var sett = new IIS.Common.Models.Settings(null, credentialSetId, psurlWindowsValue: settings.CustomSettings[IIS.Common.Constants.PsUrlWindowsKey]);
                       
			return sett.ToXmlString();
        }

        protected override object DeserializeTestResults(string result)
        {
            return TestConnectionResult.FromXmlString(result);
        }
    }
}
