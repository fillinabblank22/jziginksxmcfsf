﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Iis.Web.IisServerConfiguratorServices" %>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Services;

namespace SolarWinds.APM.BlackBox.Iis.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1), ScriptService]
    public class IisServerConfiguratorServices : BlackBoxWebService
    {
        [Obsolete]
        private const string ConfiguratorExeName = "SolarWinds.APM.RemoteIISConfiguratorFull.exe";

        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true)]
        public object GetIisBbApplicationWithoutCred(int appId)
        {
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var app = bl.GetApplication(appId);
                var iisVersion = string.Empty;

                if (app.Settings.ContainsKey(Constants.IISVersionKey))
                {
                    iisVersion = app.Settings[Constants.IISVersionKey].Value;
                }

                return new
                {
                    app.Id,
                    app.Name,
                    AppStatus = (int)app.Status,
                    NodeId = app.Node.Id,
                    NodeName = !string.IsNullOrEmpty(app.Node.Name) ? app.Node.Name : app.Node.IpAddress,
                    NodeIp = app.Node.IpAddress,
                    NodeStatus = app.Node.Status,
                    NodeHasWindowsCredentials =
                        app.Node.NodeSubType == NodeSubType.WMI || app.Node.NodeSubType == NodeSubType.Agent,
                    AppType = Constants.ApplicationTemplateCustomType,
                    PowerShellHelpLink = HelpLocator.CreateHelpUrl("SAMAGAppInforIISErrWMI"),
                    CredentialsTestHelpLink = HelpLocator.CreateHelpUrl("SAMAGAppInforIISErrCredTest"),
                    IisVersion = iisVersion,
                    IsNodeAvailable = app.Node.Status.OrionApmStatus().IsAvailable(),
                    NodeIsNotAvailableMsg = APM_IisBBContent.ZeroConfig_Dialog_NodeIsNotAccessible,
                    PsUrlWindows = ParseMacros(app.Settings.Get("PsUrlWindows").Value, app.Node.IpAddress),
                    UseAgent = app.ExecutePollingMethod == PollingMethod.Agent
                };
            }
        }

        [WebMethod(EnableSession = true)]
        public RemoteExecutableState TryGetSetupApplicationStatus(string nodeIp)
        {
            if (string.IsNullOrWhiteSpace(nodeIp))
            {
                throw new ArgumentException("nodeIp");
            }
            return GetSetupApplicationStatus(RemoteExecutableInfo.BuildKey(ConfiguratorExeName, nodeIp), 0);
        }

        [WebMethod(EnableSession = true)]
        public RemoteExecutableState GetSetupApplicationStatus(string executeKey, int appId)
        {
            VerifyDemo();

            RemoteExecutableState state;
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                state = bl.GetExecutableRemoteProgramStatus(executeKey);
            }
            if (state != null)
            {
                if (state.ExitCode.HasValue)
                {
                    var exitCode = state.ExitCode.Value;

                    if (exitCode != IisRemoteExecutableExitCodes.ExitCodeOk)
                    {
                        var message = GetConfiguratorErrorMessage(exitCode);
                        state.LogMessages.Clear();
                        state.LogMessages.Add(message);
                    }
                }
            }
            return state;
        }

        [WebMethod(EnableSession = true)]
        public int CreateNewCredentials(CredentialSet cred)
        {
            VerifyDemo();
            if (cred == null)
            {
                throw new ArgumentException("Credential");
            }

            if (string.IsNullOrWhiteSpace(cred.UserName))
            {
                throw new ArgumentException("UserName");
            }

            using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                return businessLayer.CreateNewCredentials(Type, cred);
            }
        }

        [WebMethod(EnableSession = true)]
        public void SetServerConfigurationResult(int appId, int credId, string iisVersion)
        {
            VerifyDemo();
            VerifyRights();

            if (appId < 0)
            {
                throw new ArgumentException(string.Format(@"Could not find application with id ""{0}"".", appId));
            }
            using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var applicationSettings = new Dictionary<string, string>();
                applicationSettings[Constants.IISVersionKey] = iisVersion;

                businessLayer.SetServerConfigurationResult(Type, appId, credId, applicationSettings);
                businessLayer.DoZeroConfigPostAction(Type, appId);
            }
        }

        [WebMethod(EnableSession = true)]
        public object CheckIisVersion(string iisVersion)
        {
            Version version;
            Version.TryParse(iisVersion, out version);

            if (version == null)
            {
                return new
                {
                    IsVersionSupported = true,
                    Message = string.Empty
                };
            }

            if (version < new Version(7, 0))
            {
                return new
                {
                    IsVersionSupported = false,
                    Message = string.Format(CultureInfo.InstalledUICulture, APM_IisBBContent.ZeroConfig_VersionTestFailed_ErrorMessage, version)
                };
            }

            return new
            {
                IsVersionSupported = true,
                Message = string.Empty
            };
        }

        private string GetConfiguratorErrorMessage(int exitCode)
        {
            var errorDescription = IisRemoteExecutableExitCodes.GetErrorDescription(exitCode);
            var helpLinkFragment = IisRemoteExecutableExitCodes.GetHelpLinkFragment(exitCode);

            switch (exitCode)
            {
                case IisRemoteExecutableExitCodes.AccessIsDenied:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0} {1}. <a target='_blank' href = '{2}'> {3} </a>",
                        APM_IisBBContent.IIS_ZeroConfig_ErrorHeader, errorDescription,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.Win32InvalidLogonType:
                case IisRemoteExecutableExitCodes.ExitCodeWin32InvalidCred:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0}. <a target='_blank' href = '{1}'>{2}</a>",
                        errorDescription, HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.InvalidSignature:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0} {1}. <a target='_blank' href = '{2}'> {3} </a>",
                        APM_IisBBContent.IIS_ZeroConfig_ErrorHeader, errorDescription,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.NetworkPathNotFound:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0} {1}. {2} <a target='_blank' href = '{3}'> {4} </a>",
                        APM_IisBBContent.IIS_ZeroConfig_ErrorHeader, errorDescription,
                        APM_IisBBContent.IIS_ZeroConfig_OrionLogMessage,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.CustomExeFileNotFoundExitCode:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0} {1}. {2} <a target='_blank' href = '{3}'> {4} </a>",
                        APM_IisBBContent.IIS_ZeroConfig_ErrorHeader, errorDescription,
                        APM_IisBBContent.IIS_ZeroConfig_OrionLogMessage,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.CustomExeConfigFileNotFoundExitCode:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0} {1}. {2} <a target='_blank' href = '{3}'> {4} </a>",
                        APM_IisBBContent.IIS_ZeroConfig_ErrorHeader, errorDescription,
                        APM_IisBBContent.IIS_ZeroConfig_OrionLogMessage,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.RemoteDeploymentServiceFileNotFoundExitCode:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0} {1}. {2} <a target='_blank' href = '{3}'> {4} </a>",
                        APM_IisBBContent.IIS_ZeroConfig_ErrorHeader, errorDescription,
                        APM_IisBBContent.IIS_ZeroConfig_OrionLogMessage,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.AdminShareNotAvailable:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0} {1}. <a target='_blank' href = '{2}'> {3} </a>",
                        APM_IisBBContent.IIS_ZeroConfig_ErrorHeader, errorDescription,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.CantReuseAlreadyExistedListener:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0}. <a target='_blank' href = '{1}'> {2}</a>",
                        errorDescription, HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.CantCreateSelfSignedCertificate:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0}. <a target='_blank' href = '{1}'> {2}</a>",
                        errorDescription, HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.CantCreateWsManListener:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0}. <a target='_blank' href = '{1}'> {2}</a>",
                        errorDescription, HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.NoPowerShell20OrHigherInstalled:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0}. <a target='_blank' href = '{1}'> {2}</a>",
                        errorDescription, HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.CantStartWinRmService:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0}. <a target='_blank' href = '{1}'> {2}</a>",
                        errorDescription, HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.ExitCodeUnknown:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{0}. <a target='_blank' href = '{1}'> {2}</a>", errorDescription,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                case IisRemoteExecutableExitCodes.RemoteProcedureCallFailedExitCode:
                    return string.Format(CultureInfo.InvariantCulture,
                        @"{1}. {0}. <a target='_blank' href = '{2}'> {3}</a>",
                        new Win32Exception(exitCode).Message, errorDescription,
                        HelpLocator.CreateHelpUrl(helpLinkFragment),
                        APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                default:
                    if (exitCode >= IisRemoteExecutableExitCodes.Win32InvalidCredStartNumber &&
                        exitCode <= IisRemoteExecutableExitCodes.Win32InvalidCredEndNumber)
                    {
                        return string.Format(CultureInfo.InvariantCulture,
                            @"{0}. <a target='_blank' href = '{1}'> {2}</a>",
                            errorDescription, HelpLocator.CreateHelpUrl(helpLinkFragment),
                            APM_IisBBContent.IIS_ZeroConfig_HelpMe_Link);
                    }

                    return string.Format(CultureInfo.InvariantCulture, @"{1}. {0}.",
                        new Win32Exception(exitCode).Message, errorDescription);
            }
        }

        protected override string Type
        {
            get { return Constants.ApplicationTemplateCustomType; }
        }
    }
}
