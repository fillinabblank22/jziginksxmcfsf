﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true" CodeFile="SmartApplications.aspx.cs" Inherits="Orion_APM_Discovery_SmartApplications" Title="<%$ Resources: CoreWebContent, WEBDATA_VB0_23 %>" %>

<%@ Register Src="~/Orion/APM/Discovery/Controls/SmartApplicationList.ascx" TagPrefix="apm" TagName="SmartApplicationList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <span class="ActionName"><%= Resources.APMWebContent.ApmWeb_Discovery_BBStep_Name %></span>
    <br />
    <%= Resources.APMWebContent.ApmWeb_Discovery_BBStep_Text %>
		
	<div class="contentBlock">
	    <apm:SmartApplicationList ID="SmartApplicationList" runat="server" />
	</div>
	
	<table class="NavigationButtons"><tr><td>
    <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary" runat="server" ID="imgBack" OnClick="imgbBack_Click" />
                    <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary"  runat="server" ID="imgbNext" OnClick="imgbNext_Click" />
                    <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
    </div>
    </td></tr></table>    
</asp:Content>
