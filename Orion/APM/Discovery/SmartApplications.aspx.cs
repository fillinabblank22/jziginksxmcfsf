﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;

public partial class Orion_APM_Discovery_SmartApplications : ResultsWizardBasePage, IStep
{
    protected override void Next()
    {
        if (!ValidateUserInput())
            return;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        InitButtons(imgbCancel);
    }


    #region IStep Members
    public string Step
    {
        get
        {
            return "SmartApplications";
        }
    }
    #endregion
}