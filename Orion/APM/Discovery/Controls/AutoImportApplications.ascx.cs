﻿using SolarWinds.APM.Common.Models.Discovery;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Common.Snmp;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Orion_APM_Discovery_Controls_AutoImportApplications : AutoImportWizardStep
{
    private static Log log = new Log();

    protected DiscoveryConfiguration DiscoveryConfigurationInfo
    {
        get { return ConfigWorkflowHelper.DiscoveryConfigurationInfo; }
    }

    protected List<IDiscoveryConfigurationPlugin> Plugins
    {
        get
        {
            return WebPluginManager.Instance.GetPlugins<IDiscoveryConfigurationPlugin>();
        }
    }

    protected string LearnMoreUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionApmAGManagingTemplates"); }
    }

    public override string Name
    {
        get
        {
            return Resources.APMWebContent.AutoImportWizardApplicationsStepName;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        EnsureDataBound();
    }

    private void EnsureDataBound()
    {
        if (ApplicationTemplatesCheckboxes.Items.Count == 0)
        {
            ApplicationTemplatesCheckboxes.DataTextField = "DisplayName";
            ApplicationTemplatesCheckboxes.DataValueField = "DiscoveryConfigurationType";
            ApplicationTemplatesCheckboxes.DataSource = this.Plugins.Select(plugin => new
            {
                DisplayName = plugin.DisplayName,
                DiscoveryConfigurationType = plugin.GetDiscoveryConfiguration(this.DiscoveryConfigurationInfo).GetType().FullName
            }).OrderBy(app => app.DisplayName, StringComparer.CurrentCultureIgnoreCase);
            ApplicationTemplatesCheckboxes.DataBind();
        }
    }

    public override void LoadData()
    {
        EnsureDataBound();

        foreach(var plugin in this.Plugins)
        {
            var discoveryPluginConfiguration = plugin.GetDiscoveryConfiguration(this.DiscoveryConfigurationInfo);
            ListItem item = ApplicationTemplatesCheckboxes.Items.FindByValue(discoveryPluginConfiguration.GetType().FullName);
            item.Selected = discoveryPluginConfiguration.EnableAutoImport;
        }
    }

    public override void SaveData()
    {
        EnsureDataBound();

        foreach (var plugin in this.Plugins)
        {
            var discoveryPluginConfiguration = plugin.GetDiscoveryConfiguration(this.DiscoveryConfigurationInfo);
            ListItem item = ApplicationTemplatesCheckboxes.Items.FindByValue(discoveryPluginConfiguration.GetType().FullName);
            discoveryPluginConfiguration.EnableAutoImport = item.Selected;
        }
    }
}