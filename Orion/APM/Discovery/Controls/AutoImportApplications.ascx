﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoImportApplications.ascx.cs" Inherits="Orion_APM_Discovery_Controls_AutoImportApplications" %>

<style type="text/css">
    .application-templates-title { padding: 5px 0 5px 0; }
    .application-templates-suggestion { margin-top: 32px; }
    ul.application-templates {
        list-style: none;
        padding: 0; 
        margin: 0;
        max-height: 320px;
    }
    ul.application-templates li {
        padding: 2px 0;
    }
    ul.application-templates li img {
        padding-right: 5px;
        position: relative;
        top: 2px;
    }
</style>

<div class="application-templates-title"><%= Resources.APMWebContent.AutoImportWizardApplicationsTitle %></div>

<asp:CheckBoxList runat="server" ID="ApplicationTemplatesCheckboxes" RepeatLayout="UnorderedList" CssClass="application-templates" />

<div class="application-templates-suggestion">
    <span class="sw-suggestion">
        <span class="sw-suggestion-icon"></span>
        <%= Resources.APMWebContent.AutoImportWizardApplicationsHintText %>
        <a href="<%= LearnMoreUrl %>" target="_blank" class="sw-link">&raquo; <%= Resources.APMWebContent.AutoImportWizardApplicationsHintLink %></a>
    </span>
</div>
