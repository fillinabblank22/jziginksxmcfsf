﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDiscoveryPlugin.ascx.cs" Inherits="APM_Discovery_Controls_ApplicationDiscoveryPlugin" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI.AutoAssign" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
	<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/APM/Images/discovery.svg"/>
</div>

<div class="coreDiscoveryPluginBody">
	<h2><%= Resources.APMWebContent.APMWEBDATA_VB1_182 %></h2>
    <div><%= Resources.APMWebContent.APMWEBDATA_TM0_65%></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="ApplicationDiscoveryHelpLink" runat="server" HelpUrlFragment="OrionAPMPHConfigSettingsAppDiscovery"
        HelpDescription="<%$ Resources: APMWebContent, APMWEBDATA_TM0_66 %>" CssClass="helpLink" />
    <br />
    <%if (_autoAssignUtility.IsInProgress){%> 
		<asp:HiddenField ID="ctrScanProgress" runat="server" />
        <div class="managedObjInfoWrapper">   
            <span id="apmMonitoredObjectsInfo" runat="server">
                <asp:Image ID="loadingImage" runat="server" ImageUrl="~/Orion/APM/Images/Admin/animated_loading_16x16.gif" CssClass="stateImage"></asp:Image>
                <span><%=_autoAssignUtility.Text%></span>
            </span>
            <span class="LinkArrow">&#0187;</span> 
            <a class="helpLink" href="/Orion/APM/Admin/AutoAssign/AutoAssignScanStatus.aspx"><%= Resources.APMWebContent.APMWEBDATA_TM0_67 %></a>
        </div>
    <%} else {%>
        <orion:ManagedNetObjectsInfo ID="applicationInfo" runat="server" EntityName="<%$ Resources: APMWebContent, APMWEBDATA_AK1_61 %>" NumberOfElements="<%# ApplicationCount %>" />
    <%}%>
        <orion:LocalizableButton ID="discoverApplicationsButton" runat="server" LocalizedText="CustomText"
            Text="<%$ Resources: ApmWebContent, WEBDATA_RM0_1 %>" DisplayType="Secondary"
            OnClick="DiscoverApplicationsButton_Click" />
        <orion:LocalizableButton runat="server" ID="manuallyAssignApplicationMonitors" LocalizedText="CustomText"
            Text="<%$ Resources: ApmWebContent, WEBDATA_RM0_2 %>" DisplayType="Secondary"
            OnClick="ManuallyAssignApplicationMonitors_Click" />
        <orion:LocalizableButton runat="server" ID="findProcessesServicesAndPerformanceCounters"
            LocalizedText="CustomText" Text="<%$ Resources: ApmWebContent, WEBDATA_RM0_3 %>"
            DisplayType="Secondary" OnClick="FindProcessesServicesAndPerformanceCounters_Click" />
</div>
<script type="text/javascript">
	var el = $("input[id$=ctrScanProgress]");
	if (el.size() > 0 && el.val().toLowerCase() == "true") {
		var URL = "/Orion/APM/Services/Applications.asmx/IsAutoAssignCompleted";
		function spLoad() {
			$.ajax({ type: "post", contentType: "application/json", dataType: "json", url: URL, data: "{}", success: spLoaded });
		}
		function spLoaded(pResp) {
			eval("var state = " + pResp.d);

			if (state.IsCompleted) {
				setTimeout(function () { location.reload(true); }, 5 * 1000); return;
			}
			$("span[id$=apmMonitoredObjectsInfo] span").html(state.Msg);
			
			setTimeout(function () { spLoad(); }, 60 * 1000);
		}
		spLoad();
}

	$(document).ready(function() {
	    if ($('#discoveryCentral').width() < 1050) {
	        $('#discoveryCentral').css('width', '1050px');
	    }
	});
    
</script>
