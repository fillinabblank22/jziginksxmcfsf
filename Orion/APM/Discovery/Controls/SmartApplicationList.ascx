﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SmartApplicationList.ascx.cs" Inherits="Orion_APM_Discovery_Controls_SmartApplicationList" %>
<%@ Register TagPrefix="apm" Namespace="SolarWinds.APM.Web.Discovery" Assembly="SolarWinds.APM.Web" %>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function () {
        $(".CheckAllSelector input[type=checkbox]").click(function () {
            var checked_status = this.checked;
            $(".Selector input[type=checkbox]").each(function () {
                this.checked = checked_status;
            });
        });
    });
    //]]>
</script>


<div>
    <apm:BlackBoxGridView ID="BlackBoxTableGrid" runat="server" AutoGenerateColumns="False" GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow"  AllowSorting="true" >
        <Columns>
            <asp:TemplateField ItemStyle-Width="20px">
                <HeaderTemplate>
                    <asp:CheckBox ID="CheckAllSelector" runat="server" class="CheckAllSelector" OnCheckedChanged="CheckAllSelector_OnCheckedChanged" OnPreRender="CheckAllSelector_OnPreRender" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="Selector" runat="server" Checked='<%# Eval("IsSelected") %>' CssClass="Selector" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Count" HeaderText="<%$ Resources: CoreWebContent, WEBDATA_VB0_39 %>" ItemStyle-Width="50px" SortExpression="Count" />
            <asp:ImageField DataImageUrlField="Icon" ItemStyle-Width="16px"></asp:ImageField>
            <asp:BoundField DataField="Description" HeaderText="<%$ Resources: ApmWebContent, ApmWeb_Discovery_BBStep_ColumnApplication %>" SortExpression="Description" />
        </Columns>
    </apm:BlackBoxGridView>
</div>




