﻿using System;
using System.Web.UI;
using SolarWinds.APM.Web.UI.AutoAssign;
using SolarWinds.Orion.Web.Helpers;

public partial class APM_Discovery_Controls_ApplicationDiscoveryPlugin : System.Web.UI.UserControl
{
    protected AutoAssignUtility _autoAssignUtility;

    protected int ApplicationCount
    {
        get { return SolarWinds.APM.Web.SwisDAL.GetApplicationCount(string.Empty); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _autoAssignUtility = new AutoAssignUtility(false);
		
		ctrScanProgress.Value = _autoAssignUtility.IsInProgress.ToString();

        DataBind();
    }

    protected void DiscoverApplicationsButton_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/APM/Admin/AutoAssign/SelectNodes.aspx");
    }

    protected void ManuallyAssignApplicationMonitors_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/APM/Admin/QuickStart/Default.aspx");
    }
    protected void FindProcessesServicesAndPerformanceCounters_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/APM/Admin/MonitorLibrary/AppFinder/Default.aspx");
    }
}