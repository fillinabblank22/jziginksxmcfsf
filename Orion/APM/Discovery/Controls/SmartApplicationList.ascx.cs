﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models.Discovery;
using SolarWinds.APM.Web.Discovery;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_APM_Discovery_Controls_SmartApplicationList : System.Web.UI.UserControl
{
    private static class SessionKeys
    {
        public const string CheckAllApplicationsIsSelected = "ResultsDiscoveryCheckAllApplicationsIsSelected";
        public const string DiscoverySmartApplicationTypeList = "ResultsDiscoverySmartApplicationTypeListWorkflow";
        public const string LastImportId = "ResultsDiscoveryLastImportId";
    }

    /// <summary>
    /// Comparer for purpose of grouping applications by type
    /// </summary>
    private class BlackBoxApplicationGroupingComparer : IEqualityComparer<BlackBoxApplicationTypeInfo>
    {
        public bool Equals(BlackBoxApplicationTypeInfo x, BlackBoxApplicationTypeInfo y)
        {
            return string.Equals(x.Code, y.Code);
        }

        public int GetHashCode(BlackBoxApplicationTypeInfo obj)
        {
            return ((object) obj.Code ?? obj).GetHashCode();
        }
    }

    private static readonly Log log = new Log();

    protected void CheckAllSelector_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckAllApplicationsIsSelected = ((CheckBox)this.BlackBoxTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked;
    }

    protected void CheckAllSelector_OnPreRender(object sender, EventArgs e)
    {
        ((CheckBox)this.BlackBoxTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked = CheckAllApplicationsIsSelected;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.BlackBoxTableGrid.OnGetDataSource += SmartApplicationTableGrid_GetDataSource;
    }

    private List<BlackBoxApplicationTypeGroup> SmartApplicationTableGrid_GetDataSource()
    {
        return DiscoverySmartApplicationTypeList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (LastImportId == Guid.Empty || ResultsWorkflowHelper.ImportID != LastImportId)
        {
            DiscoverySmartApplicationTypeList = null;
            LastImportId = ResultsWorkflowHelper.ImportID;
        }

        if (!IsPostBack)
        {
            // clear list of VolumeTypeList in Session, because I can enter this page after action, which changed DiscoveredNodes collection
            if (ResultsWorkflowHelper.DiscoveryOnlyNotImportedResults)
            {
                DiscoverySmartApplicationTypeList = null;
            }

            CreateSmartApplicationTypeList();
            this.BlackBoxTableGrid.DataSource = DiscoverySmartApplicationTypeList;
            this.BlackBoxTableGrid.DataBind();
        }

        StoreSelectedSmartApplicationTypes();
    }

    private List<IDiscoveredBlackBoxApplication> GetDiscoveredApplications(DiscoveryResultBase discoveryResult)
    {
        var blackBoxResults = discoveryResult.PluginResults.OfType<IBlackBoxDiscoveryPluginResultInfo>();
        return blackBoxResults
            .Where(bbr => bbr != null && bbr.DiscoveredApplications != null)
            .SelectMany(bbr => bbr.DiscoveredApplications)
            .ToList();
    }

    private void CreateSmartApplicationTypeList()
    {
        // Return if there is no result at all
        if (ResultsWorkflowHelper.DiscoveryResults == null || ResultsWorkflowHelper.DiscoveryResults.Count == 0)
        {
            log.Error("DiscoveryResult is null");
            return;
        }

        // if we have some previous result, store info about selected items
        var oldGroups = new Dictionary<string, bool>();
        if (DiscoverySmartApplicationTypeList != null)
        {
            oldGroups = DiscoverySmartApplicationTypeList.ToDictionary<BlackBoxApplicationTypeGroup, string, bool>(a => a.TypeCode, a => a.IsSelected);
            DiscoverySmartApplicationTypeList = null;
        }

        //take all discovered applications for selected nodes
        var allApplications = new List<IDiscoveredBlackBoxApplication>();
        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            var coreResult = discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();
            if (coreResult == null)
            {
                log.Error("Unable to get Core discovery plugin result for profile");
                return;
            }

            var selectedNodeIDs = new HashSet<int>(coreResult.DiscoveredNodes.Where(n => n.IsSelected).Select(n => n.NodeID));

            var applications = GetDiscoveredApplications(discoveryResult).Where(a => selectedNodeIDs.Contains(a.NodeId));
            allApplications.AddRange(applications);
        }

        //group application by type
        var applicationGroups = allApplications
            .GroupBy(a => a.ApplicationType, new BlackBoxApplicationGroupingComparer())
            .Select(g => new BlackBoxApplicationTypeGroup
            {
                TypeCode = g.Key.Code,
                Description = g.Key.Description,
                Icon = g.Key.Icon,
                IsSelected = oldGroups.ContainsKey(g.Key.Code) ? oldGroups[g.Key.Code] : g.All(i => i.IsSelected),
                Count = g.Count()
            }).ToList();

        DiscoverySmartApplicationTypeList = applicationGroups;
    }

    private void StoreSelectedSmartApplicationTypes()
    {
        for (var i = 0; i < BlackBoxTableGrid.Rows.Count; i++)
        {
            var item = BlackBoxTableGrid.Rows[i];
            var checkBox = (CheckBox) item.FindControl("Selector");
            DiscoverySmartApplicationTypeList[i].IsSelected = checkBox.Checked;
        }

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            var coreResult = discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();
            if (coreResult == null)
            {
                log.Error("Unable to get Core discovery plugin result for profile");
                return;
            }

            var selectedNodeIDs = new HashSet<int>(coreResult.DiscoveredNodes.Where(n => n.IsSelected).Select(n => n.NodeID));
            var groups = DiscoverySmartApplicationTypeList.ToDictionary(a => a.TypeCode, a => a.IsSelected);

            GetDiscoveredApplications(discoveryResult)
                .ForEach(app =>
                    {
                        app.IsSelected = selectedNodeIDs.Contains(app.NodeId) &&
                            (!groups.ContainsKey(app.ApplicationType.Code) || groups[app.ApplicationType.Code]);
                    });
        }
    }

    private static bool CheckAllApplicationsIsSelected
    {
        get { return SessionHelper.GetSessionValue<bool>(SessionKeys.CheckAllApplicationsIsSelected); }
        set { SessionHelper.SetSession(SessionKeys.CheckAllApplicationsIsSelected, value); }
    }

    private static Guid LastImportId
    {
        get { return SessionHelper.GetSessionValue<Guid>(SessionKeys.LastImportId); }
        set { SessionHelper.SetSession(SessionKeys.LastImportId, value); }
    }

    public static List<BlackBoxApplicationTypeGroup> DiscoverySmartApplicationTypeList
    {
        get { return SessionHelper.GetSession<List<BlackBoxApplicationTypeGroup>>(SessionKeys.DiscoverySmartApplicationTypeList); }
        set { SessionHelper.SetSession(SessionKeys.DiscoverySmartApplicationTypeList, value); }
    }
}
