<%@ Page Language="C#" MasterPageFile="~/Orion/APM/ApmView.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_APM_Summary" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>



<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <div>
        <%if (Profile.AllowCustomize)
          {%>
        <a href='<%=CustomizeViewHref%>' style="padding: 2px 0px 2px 20px; background: transparent url(/Orion/APM/Images/Page.Icon.CustomPg.gif) scroll no-repeat left center;">
            <%= Resources.APMWebContent.APMWEBDATA_VB1_161 %></a>
        <%}%>
        <%if (SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
          { %>
        <a href="/Orion/APM/Admin/Default.aspx" style="padding: 2px 0px 2px 20px; background: transparent url(/Orion/APM/Images/Button.EditApplication.gif) scroll no-repeat left center;">
            <%= String.Format(Resources.APMWebContent.APMWEBDATA_VB1_162, SolarWinds.APM.Common.ApmConstants.ModuleShortName)%></a>
        <%}%>
        <%if (Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
          { %>
        <a href="/Orion/Nodes/Add/Default.aspx" style="padding: 2px 0px 2px 20px; background: transparent url(/Orion/APM/Images/Button.AddNode.gif) scroll no-repeat left center;">
            <%= Resources.APMWebContent.APMWEBDATA_VB1_163 %></a>
        <%}%>
        <orion:IconHelpButton HelpUrlFragment="OrionAPMPHAppSummary" ID="btnHelp" runat="server" />
        <br/>
        <div style="padding-top: 5px; font-size: 8pt; text-align: right;">
            <%=DateTime.Now.ToString("F")%>
        </div>
    </div>
</asp:Content>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="ApmPageTitle" runat="Server">
    <h1>
        <%= Page.Title %></h1>
    <br />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
	
</asp:Content>

