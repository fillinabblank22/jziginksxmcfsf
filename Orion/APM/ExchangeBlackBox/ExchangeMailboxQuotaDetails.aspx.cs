﻿using System;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Web;

public partial class Orion_APM_ExchangeBlackBox_ExchangeMailboxQuotaDetails : ApmViewPage
{
    protected override void OnInit(EventArgs e)
    {
        resHost.ExchangeMailbox = NetObjectAsMailbox;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        title.ViewTitle = ViewInfo.ViewTitle;
        title.ViewSubTitle = NetObjectAsMailbox.MailboxModel.UserLogonName;

        topRightLinks.CustomizeViewHref = HtmlHelper.GetDefaultCustomizeViewHref(ViewInfo.ViewID);

        topRightLinks.HelpUrlFragment = "OrionSAMAGExchangeBBMBQuotaDetails";
    }

    public override string ViewType
    {
        get { return ExchangeConstants.MailboxQuotaDetailsViewType; }
    }

    public Mailbox NetObjectAsMailbox
    {
        get { return (Mailbox) NetObject; }
    }
}