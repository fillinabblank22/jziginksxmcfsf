﻿<%@ WebService Language="C#" Class="ExchangeBlackBoxChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.DAL;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web.Charting.v2;
using ApmChartDataHelper = SolarWinds.APM.Web.Charting.ChartDataHelper;
using ChartDataResults = SolarWinds.Orion.Web.Charting.v2.ChartDataResults;
using V2DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;
using V2DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ExchangeBlackBoxChartData : ChartDataWebService
{
    [WebMethod]
    public ChartDataResults GetDatabaseAvailabilityDataSeries(ChartDataRequest request)
    {
        var service = new ChartDataService(DatabaseDAL.Default);
        return service.GetDatabaseAvailabilityDataSeries(request);
    }

    [WebMethod]
    public ChartDataResults GetMailboxSizeDataSeries(ChartDataRequest request)
    {
        var service = new ChartDataService(MailboxDal.Default);
        return service.GetMailboxSizeDataSeries(request);
    }

    [WebMethod]
    public ChartDataResults GetDatabaseFileSizeDataSeries(ChartDataRequest request)
    {
        var objectId = ParseNetObjectId(request);
        if (objectId.Prefix != ExchangeConstants.DatabaseFilePrefix)
        {
            throw new ArgumentException(InvariantString.Format("Wrong prefix in net object ID {0}, expecting {1}",
                request.NetObjectIds[0], ExchangeConstants.DatabaseFilePrefix));
        }
        
        var dateRange = ApmChartDataHelper.SetDateRange(request);

        var filesStatistics = new DatabaseFileDal().GetDatabaseFileSizeChartStatistics((int)objectId.Id, dateRange);

        var sampledSeries = filesStatistics.Values.ResampleSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Min);

        return sampledSeries.CreateDynamicResult(request);
    }

	[WebMethod]
	public ChartDataResults GetSentMailDataSeries(ChartDataRequest request)
	{
		return GetSentReceivedMailData(request, "MessagesSentInternal", "MessagesSentExternal", true);
	}

	[WebMethod]
	public ChartDataResults GetReceivedMailDataSeries(ChartDataRequest request)
	{
		return GetSentReceivedMailData(request, "MessagesReceivedInternal", "MessagesReceivedExternal", true);
	}

    [WebMethod]
    public ChartDataResults GetPercentUsedQuotaDataSeries(ChartDataRequest request)
    {
        var objectId = ParseNetObjectId(request);
        if (objectId.Prefix != ExchangeConstants.MailboxPrefix)
        {
            throw new ArgumentException(InvariantString.Format("Wrong prefix in net object ID {0}, expecting {1}",
                request.NetObjectIds[0], ExchangeConstants.MailboxPrefix));
        }

        var dateRange = ApmChartDataHelper.SetDateRange(request, true);

        var maiboxStatistics = new MailboxStatisticsDal().GetPercentUsedQuotaChartData((int)objectId.Id, dateRange);

        var sampledSeries = maiboxStatistics.Values.ResampleSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Min);

        return sampledSeries.CreateDynamicResult(request);
    }
    
	private static ChartDataResults GetSentReceivedMailData(ChartDataRequest request, string internalColumn, string externalColumn, bool timeToMidnight)
	{
        var objectId = ParseNetObjectId(request);
        if (objectId.Prefix != ExchangeConstants.MailboxPrefix)
        {
            throw new ArgumentException(InvariantString.Format("Wrong prefix in net object ID {0}, expecting {1}", request.NetObjectIds[0], ExchangeConstants.MailboxPrefix));
        }
		
		var series = new List<V2DataSeries>(3);
		series.Add(new V2DataSeries() { TagName = "Internal" });
		series.Add(new V2DataSeries() { TagName = "External" });
		series.Add(new V2DataSeries() { TagName = "Data_Navigator" });

		var dateRange = ApmChartDataHelper.SetDateRange(request, true);
        
		var data = new MailboxStatisticsDal().GetSendResiveMails(objectId.Id, dateRange);
		if (data.Rows.Count > 0) 
		{
			foreach (DataRow row in data.Rows)
			{
				var time = (DateTime)row["TimeStamp"];                
                
                if (timeToMidnight)
                {                    
                    time = time.TimeOfDay <= new TimeSpan(11, 0, 0) ? time.Add(-time.TimeOfDay) : time.Add(new TimeSpan(24, 0, 0) - time.TimeOfDay);                    
                }
                
				var value = ConvertNullable(row[internalColumn]);
				if (value.HasValue)
				{
					series[0].AddPoint(V2DataPoint.CreatePoint(time, value.Value));
				}
				else
				{
					series[0].AddPoint(V2DataPoint.CreateNullPoint(time));
				}

				value = ConvertNullable(row[externalColumn]);
				if (value.HasValue)
				{
					series[1].AddPoint(V2DataPoint.CreatePoint(time, value.Value));
				}
				else
				{
					series[1].AddPoint(V2DataPoint.CreateNullPoint(time));
				}
			}
		}
		
		var sampledSeries = series
			.Select(serie => serie.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Average))
			.ToList();

		// Core is not adding last sample if it is null point so we need to add it ourselves, basically make series same length
		var lastSample = sampledSeries
			.Select(s => new KeyValuePair<int, V2DataPoint>(s.Data.Count, s.Data.LastOrDefault()))
			.Aggregate((l, r) => l.Key > r.Key ? l : r);
		if (lastSample.Value != null)
		{
			foreach (var s in sampledSeries)
			{
				if (s.Data.Count != lastSample.Key)
				{
					s.Data.Add(V2DataPoint.CreateNullPoint(lastSample.Value.UnixTime));
				}
			}
		}

		var result = new ChartDataResults(sampledSeries);

		ApmChartDataHelper.SetChartOptionsOverride(request, result, false);

		return new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
	}
	
    private static NetObjectHelper ParseNetObjectId(ChartDataRequest request)
    {
        if (request == null) throw new ArgumentNullException("request");

        if (request.NetObjectIds == null || request.NetObjectIds.Length == 0)
        {
            throw new ArgumentException("No net object ID is defined in request");
        }
        
        NetObjectHelper objectId;
        if (!NetObjectHelper.TryCreate(request.NetObjectIds[0], out objectId))
        {
            throw new ArgumentException(InvariantString.Format("Cannot parse net object ID from {0}", request.NetObjectIds[0]));
        }
        
        return objectId;
    }

	private static double? ConvertNullable(object value)
	{
		if (value == null || value == DBNull.Value) 
		{
			return null;
		}
		return Convert.ToDouble(value, CultureInfo.InvariantCulture);
	}
}
