﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Exchg.Web.Services" %>
using System;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Common.Models;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Services;

namespace SolarWinds.APM.BlackBox.Exchg.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class Services : BlackBoxTestWebService
    {
        [WebMethod]
        public TestResultInfo TestExchangeBlackBoxConnection(TestBlackBoxSettings settings)
        {
			if (settings.NodeId < 1)
			{
				throw new ArgumentException("Node Id is required.");
			}
            if (string.IsNullOrWhiteSpace(settings.CredentialSet.Login) && settings.CredentialSet.Id != ComponentBase.NodeWmiCredentialsId)
			{
				throw new ArgumentException("Credential is required.");
			}
			try
            {
                var xmlSettings = SerializeAppSettings(settings);

				return TestBlackBoxConnectionInternal(settings, xmlSettings, null);
            }
            catch (Exception ex)
            {
				Log.ErrorFormat("Error while serializing app settings: {0}, error: {1}", RequestToString(settings), ex);
				throw;
            }
        }

        protected override string SerializeAppSettings(TestBlackBoxSettings settings)
        {
            var sett = new ExchangeSettings 
			{
				PsUrlExchangeValue = settings.CustomSettings[ExchangeConstants.PsUrlExchangeKey],
				PsUrlWindowsValue = settings.CustomSettings[ExchangeConstants.PsUrlWindowsKey],
				ServerIdentity = settings.CustomSettings[ExchangeConstants.ServerIdentityKey]
            };
			if (!string.IsNullOrWhiteSpace(settings.CredentialSet.Login)) 
			{
				sett.CredentialSetIdValue = settings.CredentialSet.Id;
			}
			using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				var node = bl.GetNode(settings.NodeId);

				sett.NodeDnsName = node.Dns;
                sett.NodeIpAddress = node.IpAddress;
			}
			return sett.ToXmlString();
        }

        protected override object DeserializeTestResults(string result)
        {
            return TestConnectionResult.FromXmlString(result);
        }
    }
}
