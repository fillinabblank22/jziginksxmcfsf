﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Exchg.Web.ExchangeServerConfigurator" %>

using System;
using System.ComponentModel;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Services;
using SolarWinds.Orion.Web.Helpers;

namespace SolarWinds.APM.BlackBox.Exchg.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1), ScriptService]
    public class ExchangeServerConfigurator : BlackBoxWebService
    {
        [Obsolete]
        private const string ConfiguratorExeName = "SolarWinds.APM.RemoteWinRmConfiguratorFull.exe";

        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true)]
        public object GetExchgBbApplicationWithoutCred(int appId)
        {
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var app = bl.GetApplication(appId);
                return new
                {
                    app.Id,
                    app.Name,
                    AppStatus = (int)app.Status,

                    NodeId = app.Node.Id,
                    NodeName = !string.IsNullOrEmpty(app.Node.Name) ? app.Node.Name : app.Node.IpAddress,
                    NodeIp = app.Node.IpAddress,
                    NodeStatus = app.Node.Status,
                    NodeType = app.Node.NodeSubType,

                    PsUrlExchange = ParseMacros(app.Settings.Get("PsUrlExchange").Value, app.Node.IpAddress),
                    PsUrlWindows = ParseMacros(app.Settings.Get("PsUrlWindows").Value, app.Node.IpAddress),
                    AppType = ExchangeConstants.ApplicationTemplateCustomType,
                    ManualHelpLink = HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeManuallyConfigureExchangeServer"),
                    UseAgent = app.ExecutePollingMethod == PollingMethod.Agent
                };
            }
        }

        [WebMethod(EnableSession = true)]
        public RemoteExecutableState TryGetSetupApplicationStatus(string nodeIp)
        {
            if (string.IsNullOrWhiteSpace(nodeIp))
            {
                throw new ArgumentException("nodeIp");
            }
            return GetSetupApplicationStatus(RemoteExecutableInfo.BuildKey(ConfiguratorExeName, nodeIp), 0);
        }

        [WebMethod(EnableSession = true)]
        public RemoteExecutableState GetSetupApplicationStatus(string executeKey, int appId)
        {
            VerifyDemo();

            RemoteExecutableState state;
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                state = bl.GetExecutableRemoteProgramStatus(executeKey);
            }
            if (state != null)
            {
                if (state.ExitCode.HasValue)
                {
                    var exitCode = state.ExitCode.Value;
                    if (exitCode != RemoteExecutableConstants.ExitCodeOk)
                    {
                        var message = GetConfiguratorErrorMessage(exitCode);
                        state.LogMessages.Clear();
                        state.LogMessages.Add(message);
                    }
                    else
                    {
                        if (appId != 0 && state.CredentialId != 0)
                        {
                            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                            {
                                var app = bl.GetApplication(appId);
                                app.Settings[InternalSettings.CredentialSetIdKey] =
                                    new SettingValue(InternalSettings.CredentialSetIdKey,
                                        state.CredentialId.ToString(CultureInfo.InvariantCulture),
                                        SettingValueType.Integer, SettingLevel.Instance, false);
                                bl.UpdateApplication(app);
                            }
                        }
                    }
                }
            }
            return state;
        }

        [WebMethod(EnableSession = true)]
        public void SetExchangeConfigurationDiscoveryResult(int appId, int credId)
        {
            VerifyDemo();
            VerifyRights();

            if (appId < 0)
            {
                throw new ArgumentException(string.Format(@"Could not find application with id ""{0}"".", appId));
            }
            using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                businessLayer.SetServerConfigurationResult(Type, appId, credId, null);
                businessLayer.DoZeroConfigPostAction(ExchangeConstants.ApplicationTemplateCustomType, appId);
            }
        }

        public string GetConfiguratorErrorMessage(int exitCode)
        {
            string helpLinkFragment = null;

            switch (exitCode)
            {
                case ExchangeRemoteExecutableExitCodes.NoPowerShell20Installed:
                case ExchangeRemoteExecutableExitCodes.NotLocalAdmin:
                case ExchangeRemoteExecutableExitCodes.AccessIsDenied:
                case ExchangeRemoteExecutableExitCodes.Win32InvalidLogonType:
                case ExchangeRemoteExecutableExitCodes.ExitCodeWin32InvalidCred:
                case ExchangeRemoteExecutableExitCodes.CantDetectPowerShellVersion:
                    helpLinkFragment = @"SAMAGAPPEXNoPSInstall";
                    break;

                case ExchangeRemoteExecutableExitCodes.NoExchangeAccess:
                    helpLinkFragment = @"SAMAGAPPEXNoVO";
                    break;

                case ExchangeRemoteExecutableExitCodes.InvalidSignature:
                case ExchangeRemoteExecutableExitCodes.NetworkPathNotFound:
                case ExchangeRemoteExecutableExitCodes.CustomExeFileNotFoundExitCode:
                case ExchangeRemoteExecutableExitCodes.CustomExeConfigFileNotFoundExitCode:
                case ExchangeRemoteExecutableExitCodes.RemoteDeploymentServiceFileNotFoundExitCode:
                case ExchangeRemoteExecutableExitCodes.CantDetectGroupMembership:
                case ExchangeRemoteExecutableExitCodes.CantCreateWsManListener:
                case ExchangeRemoteExecutableExitCodes.AdminShareNotAvailable:
                case ExchangeRemoteExecutableExitCodes.CantStartWinRmService:
                    helpLinkFragment = @"SAMAGAppInforExch-ErrorCodes";
                    break;

                case ExchangeRemoteExecutableExitCodes.CantReuseAlreadyExistedListener:
                    helpLinkFragment = @"OrionSAMAppInsightExchangeConfig";
                    break;

                case ExchangeRemoteExecutableExitCodes.CantCreateSelfSignedCertificate:
                    helpLinkFragment = @"SAMAGAPPEXNoSSC";
                    break;

                case ExchangeRemoteExecutableExitCodes.CantCheckOrEnableFirewallRule:
                    helpLinkFragment = @"SAMAGAPPEXFW";
                    break;

                case ExchangeRemoteExecutableExitCodes.CantSetupPsLanguageModeSetting:
                    helpLinkFragment = @"SAMAGAPPEXPSLang";
                    break;

                case ExchangeRemoteExecutableExitCodes.CantSetupWindowsAuthentication:
                    helpLinkFragment = @"SAMAGAPPEXPSweb";
                    break;

                case ExchangeRemoteExecutableExitCodes.CantPreparePowerShellWebSite:
                    helpLinkFragment = @"SAMAGAPPEXPSweb";
                    break;

                default:
                    if (exitCode >= ExchangeRemoteExecutableExitCodes.Win32InvalidCredStartNumber &&
                        exitCode <= ExchangeRemoteExecutableExitCodes.Win32InvalidCredEndNumber)
                    {
                        helpLinkFragment = @"SAMAGAPPEXNoADLA";
                    }
                    break;
            }

            if (string.IsNullOrEmpty(helpLinkFragment))
            {
                return ExchangeRemoteExecutableExitCodes.GetErrorDescription(exitCode);
            }

            return CreateErrorMessage(exitCode, helpLinkFragment);
        }

        private string CreateErrorMessage(int exitCode, string helpLinkFragment)
        {
            var errorMessage = @"{0} <a target='_blank' href = '{1}'>{2}</a>";
            var learnHowToCorrect = Resources.APM_ExBBContent.Web_ConfigureExchange_Error_Learn_How_Correct;
            var helpLinkUrl = HelpLocator.CreateHelpUrl(helpLinkFragment);

            return string.Format(CultureInfo.InvariantCulture,
                errorMessage,
                ExchangeRemoteExecutableExitCodes.GetErrorDescription(exitCode),
                helpLinkUrl,
                learnHowToCorrect);
        }

        protected override string Type
        {
            get { return ExchangeConstants.ApplicationTemplateCustomType; }
        }
    }
}