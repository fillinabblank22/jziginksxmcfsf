﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExchangeDatabaseFilePopup.aspx.cs" Inherits="Orion_APM_ExchangeBlackBox_ExchangeDatabaseFilePopup" MasterPageFile="~/Orion/APM/Popup.master"%>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.Common.Extensions.System" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="NetObjectTipBody">
     <%-- Inline CSS implementation is necessary, but net object tooltip implementation from Core 
              doesn't allow to embed scripts and styles (probably because of performance) --%>
    <div>
        <div style="border-left:solid 1px; text-align: left; margin-left:2px; padding-left:4px;">
            <div style="white-space: nowrap; font-size: 11px;"><%=APM_ExBBContent.SpaceUsedByData %><br/><%= new ApmBytes(Size-AvailableWhiteSpace,2).ToInvariantString()%></div>
            <div style="<%= AvailableSpaceStyle %>"><%=APM_ExBBContent.WhiteSpace %><br/><%= new ApmBytes(this.AvailableWhiteSpace,2).ToInvariantString()%></div>
        </div>
        <div style="height:20px; margin-top:4px; margin-bottom: 4px; background-image:url('/Orion/APM/Images/TooltipBar.Background.gif'); background-position:bottom; background-repeat:repeat-x; background-color:#eae9e7;">
            <span style="float:left;width:<%= (this.UsedSpacePercentage ==0 && this.Size !=0) ? "0":this.UsedSpacePercentage.ToString("n2",CultureInfo.InvariantCulture) %>%; height:20px; background-image:url('<%= UsedSpaceImgUrl %>'); background-position:bottom; background-repeat:repeat-x; background-color:#00b84a;"></span>
            <span style="float: left;width:<%= (this.WhiteSpacePercentage ==0 && this.AvailableWhiteSpace != 0) ? "0":this.WhiteSpacePercentage.ToString("n2",CultureInfo.InvariantCulture)%>%; height:20px; background-image:url('/Orion/APM/Images/TooltipBar.DarkGray.gif'); background-position:bottom; background-repeat:repeat-x; background-color:#606060;"></span>   
        </div>
        <div style="white-space: nowrap; border-right:solid 1px; text-align: right; margin-right:2px; padding-right:4px;font-size: 11px;<%= (!NetObjectData.VolumeAvailableSpace.HasValue)?"display:none;":string.Empty%>"><%=APM_ExBBContent.FreeVolumeSpace %><br/><%= new ApmBytes(this.VolumeAvailableSpace,2).ToInvariantString()%></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="NetObjectTipFooter">
    <div style="font-size:11px; border-top:#b5b6b5 solid 1px; padding:7px 7px 7px 7px;"><%= APM_ExBBContent.DatabaseFilePopup_AvailableVolumeSpace.FormatInvariant(NetObjectData.VolumeAvailableSpace != null ? new ApmBytes(NetObjectData.VolumeAvailableSpace,2).ToInvariantString() : APM_ExBBContent.DatabaseFilePopup_VolumeUsageNotAvailable) %></div>
</asp:Content>


