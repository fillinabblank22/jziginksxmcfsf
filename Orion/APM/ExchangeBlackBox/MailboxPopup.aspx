﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MailboxPopup.aspx.cs" Inherits="Orion_APM_ExchangeBlackBox_MailboxPopup" MasterPageFile="~/Orion/APM/Popup.master" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= APM_ExBBContent.MailboxTooltip_Database %></th>
            <td class="statusAndText" colspan="2">
                <img ID="DBStatusIcon" src="<%= GetDatabaseIconUrl() %>" class="apm_StatusIcon" alt="status"/>&nbsp;<%= Db.Name %>
            </td>
        </tr>
        <tr>
            <th><%= APM_ExBBContent.MailboxTooltip_Size %></th>
            <td colspan="2"><%= GetSize(Mailbox.MailboxModel.TotalItemSize) %></td>
        </tr>
        <tr>
            <th><%= APM_ExBBContent.MailboxTooltip_QuotaUsed %></th>
            <% if (Mailbox.IssueWarningQuota.HasValue || Mailbox.ProhibitSendQuota.HasValue || Mailbox.ProhibitSendReceiveQuota.HasValue) { %>
            <td>
                <span style="white-space: nowrap; width: 5em"><%= CalculateQuota() %></span>
                <orion:InlineMultiBar runat="server" ID="QuotaLoadBar" />
            </td>
            <% } else { %>
            <td colspan="2" style="font-style: italic;">
                <%=  APMWebContent.NoLimitValule %>
            </td>
            <% } %>
        </tr>
        <tr>
            <th><%= APM_ExBBContent.MailboxTooltip_AttachmentSize %></th>
            <td colspan="2"><%= GetSize(Mailbox.MailboxModel.AttachmentsSize) %></td>
        </tr>
        <tr>
            <th><%= APM_ExBBContent.MailboxTooltip_LastAccessed %></th>
            <% if (Mailbox.MailboxModel.LastLogonTime.HasValue) { %>
            <td colspan="2">
                <%= Mailbox.MailboxModel.LastLogonTime.GetValueOrDefault().ToLocalTime().ToString("f", CultureInfo.CurrentUICulture) %>
            </td>
            <% } else if (MailboxStatus == Status.Unplugged) { %>
            <td colspan="2" style="font-style: italic;">
                <%=  APMWebContent.UnknownValue %>
            </td>
            <% } else { %>
            <td colspan="2" style="font-style: italic;">
                <%= APM_ExBBContent.MailboxDetails_NeverLoggedOn %>
            </td>
            <% } %>
        </tr>
        <tr>
            <th><%= APM_ExBBContent.MailboxTooltip_Limits %></th>
            <td colspan="2"><%= Mailbox.MailboxModel.UseDatabaseQuotaDefaults
                ? APM_ExBBContent.Mailbox_DatabaseLimits
                : APM_ExBBContent.Mailbox_CustomLimits %></td>
        </tr>
        <tr>
            <th style="padding-left: 2em;">
                <%= APM_ExBBContent.MailboxTooltip_Limits_Warning %>
            </th>
            <% if (Mailbox.IssueWarningQuota.HasValue) { %>
            <td colspan="2">
                <%= GetSize(Mailbox.IssueWarningQuota) %>
            </td>
            <% } else { %>
            <td colspan="2" style="font-style: italic;">
                <%=  APMWebContent.NoLimitValule %>
            </td>
            <% } %>
        </tr>
        <tr>
            <th style="padding-left: 2em;">
                <%= APM_ExBBContent.MailboxTooltip_Limits_Send %>
            </th>
            <% if (Mailbox.ProhibitSendQuota.HasValue) { %>
            <td colspan="2">
                <%= GetSize(Mailbox.ProhibitSendQuota) %>
            </td>
            <% } else { %>
            <td colspan="2" style="font-style: italic;">
                <%=  APMWebContent.NoLimitValule %>
            </td>
            <% } %>
        </tr>
        <tr>
            <th style="padding-left: 2em; white-space: normal">
                <%= APM_ExBBContent.MailboxTooltip_Limits_Send_Receive %>
            </th>
            <% if (Mailbox.ProhibitSendReceiveQuota.HasValue) { %>
            <td colspan="2">
                <%= GetSize(Mailbox.ProhibitSendReceiveQuota) %>
            </td>
            <% } else { %>
            <td colspan="2" style="font-style: italic;">
                <%=  APMWebContent.NoLimitValule %>
            </td>
            <% } %>
        </tr>
        <tr>
            <th style="white-space: normal"><%= APM_ExBBContent.MailboxTooltip_MessagesSentYesterday %></th>
            <% if (Mailbox.MailboxModel.MessagesSent.HasValue) { %>
            <td colspan="2">
                <%= String.Format(CultureInfo.CurrentCulture, APM_ExBBContent.MailboxTooltip_MessagesSent, Mailbox.MailboxModel.MessagesSent.GetValueOrDefault()) %>
            </td>
            <% } else { %>
            <td colspan="2" style="font-style: italic;">
                <%= APMWebContent.UnknownValue %>
            </td>
            <% } %>
        </tr>
    </table>
</asp:Content>
