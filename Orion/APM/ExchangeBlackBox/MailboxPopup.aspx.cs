﻿using System;
using System.Globalization;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;

public partial class Orion_APM_ExchangeBlackBox_MailboxPopup : PopupPage<Mailbox>
{
    private readonly Log log = new Log();
    protected Database Db
    {
        get { return Mailbox.MailboxDatabase; }
    }

    protected Mailbox Mailbox
    {
        get { return NetObjectData; }
    }

    protected Status MailboxStatus
    {
        get { return Mailbox.MailboxModel.Status.GetValueOrDefault().OrionApmStatus(); }
    }

    protected string QuotaUsage { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_APM_Popup)Master).PopupTitle = Mailbox.MailboxModel.DisplayName;
        ((Orion_APM_Popup)Master).StatusCssClass = StatusCssClass;

        QuotaUsage = CalculateQuota();
    }

    #region Overriden methods

    protected override ApmStatus GetStatus()
    {
        return new ApmStatus(MailboxStatus);
    }

    protected override Mailbox CreateNetObjectData(string netObjectId)
    {
        var netObject = CreateNetObject();
        var result = netObject as Mailbox;

        if (result == null)
        {
            this.log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render mailbox popup, because cannot get mailbox info from netObject with netObjectID '{0}'", netObjectId);
        }
        return result;
    }

    #endregion

    protected string GetSize(ulong? bytes)
    {
        return new ApmBytes(bytes.GetValueOrDefault(), ApmBytesBase.Unit.GB, 2);
    }

    protected string GetSize(long? bytes)
    {
        return new ApmBytes(bytes.GetValueOrDefault(), ApmBytesBase.Unit.GB, 2);
    }

    protected string GetDatabaseIconUrl()
    {
        return InvariantString.Format("/Orion/StatusIcon.ashx?entity={0}&size=small&status={1}", SwisEntities.Database, Db.StatusId);
    }

    protected string CalculateQuota()
    {
        var value = new ApmPercentage(Decimal.ToDouble(Mailbox.MailboxModel.PercentageOfUsedQuota.GetValueOrDefault()));
        int percentage = (int) value.Percentage.GetValueOrDefault(0);

        switch (MailboxStatus)
        {
            case Status.Warning:
                QuotaLoadBar.WarningPercentage = percentage;
                break;
            case Status.Critical:
                QuotaLoadBar.ErrorPercentage = percentage;
                break;
            default:
                QuotaLoadBar.NormalPercentage = percentage;
                break;
        }

        return value.ToString();
    }
}
