﻿using System;
using System.Globalization;

using Resources;

using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Common.Models;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.DAL;
using SolarWinds.APM.BlackBox.Exchg.Web.Models;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;


public partial class Orion_APM_ExchangeBlackBox_DatabasePopup : PopupPage<DatabaseCopy>
{
    private const string BoldRedStyle = "color: red; font-weight: bold;";
    private const string RedStyle = "color: red;";
    private const string YellowStyle = "color: #FCD928;";

    protected const int TopComponentsCount = 5;
    private readonly Log log = new Log();

    protected DatabaseCopy DatabaseCopy
    {
        get { return NetObjectData; }
    }

    protected ExchangeApplication ActiveDatabaseCopyApplication { get; set; }

    protected DatabaseCopyModel ActiveDatabaseCopy { get; set; }

    protected int DatabaseCopiesCount { get; set; }

    protected int DatabaseUsageWarningLevel { get; set; }

    protected int DatabaseUsageErrorLevel { get; set; }

    protected int LastFullBackupCriticalLevel { get; set; }

    protected int LastFullBackupWarningLevel { get; set; }

    protected int AvailableSpaceCriticalLevel { get; set; }

    protected int AvailableSpaceWarningLevel { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        InitData();
    }

    private static int GetThrCrit(Threshold thr)
    {
        return (thr != null) ? Convert.ToInt32(thr.CriticalLevel) : int.MaxValue;
    }

    private static int GetThrWarn(Threshold thr)
    {
        return (thr != null) ? Convert.ToInt32(thr.WarnLevel) : int.MaxValue;
    }

    private void InitData()
    {
        if (DatabaseCopy == null)
        {
            return;
        }

        Threshold fullBackupDagThreshold = ThresholdManager.GetThreshold(
            ComponentIdentifiers.IDDatabase.ToString(),
            ExchangeConstants.ThresholdNames.LastFullBackupDAGServer,
            DatabaseCopy.ExchangeApplication.Id,
            DatabaseCopy.Model.Id);

        Threshold fullBackupThreshold = ThresholdManager.GetThreshold(
            ComponentIdentifiers.IDDatabase.ToString(),
            ExchangeConstants.ThresholdNames.LastFullBackupServer,
            DatabaseCopy.ExchangeApplication.Id,
            DatabaseCopy.Model.Id);

        Threshold databseUsageThreshold =
            ThresholdManager.GetThreshold(
                ComponentIdentifiers.IDDatabaseFileAndLogInfo.ToString(),
                ExchangeConstants.ThresholdNames.DatabaseFileUsage,
                DatabaseCopy.ExchangeApplication.Id,
                DatabaseCopy.Model.Id);

        Threshold availableWhiteSpaceThreshold =
            ThresholdManager.GetThreshold(
                ComponentIdentifiers.IDDatabaseFileAndLogInfo.ToString(),
                ExchangeConstants.ThresholdNames.DatabaseFileWhitespace,
                DatabaseCopy.ExchangeApplication.Id,
                DatabaseCopy.Model.Id);

        DatabaseUsageErrorLevel = GetThrCrit(databseUsageThreshold);
        DatabaseUsageWarningLevel = GetThrWarn(databseUsageThreshold);
        AvailableSpaceCriticalLevel = GetThrCrit(availableWhiteSpaceThreshold);
        AvailableSpaceWarningLevel = GetThrWarn(availableWhiteSpaceThreshold);

        LastFullBackupCriticalLevel = (DatabaseCopy.DatabaseModel.MasterDagId != null)
            ? GetThrCrit(fullBackupDagThreshold)
            : GetThrCrit(fullBackupThreshold);

        LastFullBackupWarningLevel = (DatabaseCopy.DatabaseModel.MasterDagId != null)
            ? GetThrWarn(fullBackupDagThreshold)
            : GetThrWarn(fullBackupThreshold);

        this.PopupMaster.PopupTitle = (DatabaseCopy.Model.IsActive.HasValue && DatabaseCopy.Model.IsActive.Value)
            ? InvariantString.Format(Resources.APM_ExBBContent.DatabasePopup_ActiveCopyTitle, DatabaseCopy.Name)
            : DatabaseCopy.Name;
        this.PopupMaster.StatusCssClass = StatusCssClass;

        //get information about active copy of database
        ActiveDatabaseCopy = DatabaseDAL.Default.GetDatabaseActiveCopyById(DatabaseCopy.Model.Id);

        //get information about application where active copy is hosted
        if (ActiveDatabaseCopy != null)
        {
            ActiveDatabaseCopyApplication =
                ExchangeApplication.GetApplicationInfoById(ActiveDatabaseCopy.ApplicationId);
        }

        DatabaseCopiesCount = DatabaseDAL.Default.GetAllDatabaseCopiesCountById(DatabaseCopy.Model.Id) ?? 1;

        VolumePercentBar.DbFileUsagePercentage = DatabaseFileSizeCalculations.GetDbFileUsage(
            DatabaseCopy.Model.Size,
            DatabaseCopy.Model.AvailableWhiteSpace,
            DatabaseCopy.Model.AvailableVolumeSpace);

        VolumePercentBar.AvailableSpacePercentage =
            DatabaseFileSizeCalculations.GetDbFileWhiteSpaceUsage(
                DatabaseCopy.Model.Size,
                DatabaseCopy.Model.AvailableWhiteSpace,
                DatabaseCopy.Model.AvailableVolumeSpace);

        VolumePercentBar.UsedSpaceErrorLevel = DatabaseUsageErrorLevel;
        VolumePercentBar.UsedSpaceWarningLevel = DatabaseUsageWarningLevel;
        VolumePercentBar.AvailableSpaceErrorLevel = AvailableSpaceCriticalLevel;
        VolumePercentBar.AvailableSpaceWarningLevel = AvailableSpaceWarningLevel;
        VolumePercentBar.Width = 100;
        VolumePercentBar.Height = 10;

        phActiveDb.Visible = (DatabaseCopy.Model.IsActive.HasValue && !DatabaseCopy.Model.IsActive.Value && ActiveDatabaseCopyApplication != null);
        phMailboxes.Visible = !phActiveDb.Visible && ((DatabaseCopy.DatabaseModel.TotalMailboxes ?? 0) > 0);
        phAvgMailbox.Visible = ((DatabaseCopy.DatabaseModel.AvgMailBoxSize ?? 0) > 0);

        this.componentList.BindComponents(
            DatabaseCopy.ExchangeApplication.ComponentsWithProblems,
            TopComponentsCount,
            DatabaseCopy.ExchangeApplication.CustomType,
            null);
    }

    protected int GetLastFullBackupDaysPassed(DateTime? dateTime)
    {
        if (dateTime == null)
        {
            return 0;
        }
        var timePassed = DateTime.Now - dateTime.Value;
        return (timePassed.Milliseconds < 0)
            ? 0
            : timePassed.Days;
    }

    protected string GetFriendlyLastFullBackup(DateTime? dateTime)
    {
        if (dateTime == null)
        {
            return string.Empty;
        }
        var timePassed = DateTime.Now - dateTime.Value;
        if (timePassed.Milliseconds < 0)
        {
            return string.Empty;
        }
        var daysPassed = timePassed.Days;
        if (daysPassed == 0)
        {
            return string.Empty;
        }
        if (daysPassed == 1)
        {
            return Resources.APM_ExBBContent.DatabasePopup_yesterday + "<br/>";
        }
        if (daysPassed > 1)
        {
            return InvariantString.Format(Resources.APM_ExBBContent.DatabasePopup_daysAgo, daysPassed) + "<br/>";
        }
        return string.Empty;
    }

    protected string GetContentIndexStateCss(int? contentIndexState, bool? isActive)
    {
        if (!isActive.HasValue || !contentIndexState.HasValue)
        {
            return string.Empty;
        }
        if (isActive.Value)
        {
            switch (contentIndexState.Value)
            {
                case (int)ContentIndexStatusType.Failed:
                case (int)ContentIndexStatusType.FailedAndSuspended:
                case (int)ContentIndexStatusType.DiskUnavailable:
                    return BoldRedStyle;
                case (int)ContentIndexStatusType.Crawling:
                case (int)ContentIndexStatusType.Suspended:
                case (int)ContentIndexStatusType.AutoSuspended:
                    return RedStyle;
                default:
                    return string.Empty;
            }
        }
        switch (contentIndexState.Value)
        {
            case (int)ContentIndexStatusType.Failed:
            case (int)ContentIndexStatusType.FailedAndSuspended:
            case (int)ContentIndexStatusType.Crawling:
            case (int)ContentIndexStatusType.Suspended:
            case (int)ContentIndexStatusType.DiskUnavailable:
            case (int)ContentIndexStatusType.AutoSuspended:
                return RedStyle;
            default:
                return string.Empty;
        }
    }

    protected string GetLastFullBackupCss(int daysPassed)
    {
        if (daysPassed > LastFullBackupCriticalLevel)
        {
            return RedStyle;
        }
        if (daysPassed > LastFullBackupWarningLevel)
        {
            return YellowStyle;
        }
        return string.Empty;
    }

    protected string GetSize(long? bytes)
    {
        return !bytes.HasValue ? APM_ExBBContent.UnknownValue : new ApmBytes(bytes.Value).ToInvariantString();
    }

    protected string GetVolumeSpacePercentageCss(long? space, long? size)
    {
        if (!space.HasValue || !size.HasValue)
        {
            var usagePercentage = 100 * (double) space / (double) size;
            if (usagePercentage > this.DatabaseUsageErrorLevel)
            {
                return RedStyle;
            }
            if (usagePercentage > this.DatabaseUsageWarningLevel)
            {
                return YellowStyle;
            }
        }
        return string.Empty;
    }

    protected string GetStatusCss(long? status)
    {
        if (status.HasValue)
        {
            switch (status)
            {
                case (int)Status.Critical:
                    return BoldRedStyle;
                case (int)Status.Warning:
                    return RedStyle;
            }
        }
        return string.Empty;
    }

    protected string DbWhiteSpacePercentageLabel(long? availableSpace, long? size)
    {
        if (availableSpace.HasValue && size.HasValue)
        {
            var availableSpacePercentage = (int) Math.Round(100*(double) availableSpace.Value/size.Value);
            if (availableSpacePercentage > 0)
            {
                return InvariantString.Format(Resources.APM_ExBBContent.DatabasePopup_whiteSpacePercent, availableSpacePercentage);
            }
            return string.Empty;
        }
        return string.Empty;
    }

    protected string GetDbCopyParentsMarkup(int appStatusId, string appName, int nodeStatusId, string nodeName)
    {
        var text = Resources.APM_ExBBContent.DatabasePopup_NodeAppCaption;
        var appImage = string.Format(
            "<img src=\"/Orion/StatusIcon.ashx?entity={0}&status={1}\"/>",
            SwisEntities.Application,
            appStatusId);
        var nodeImage = string.Format(
            "<img src=\"/Orion/StatusIcon.ashx?entity={0}&status={1}\"/>",
            SolarWinds.Orion.Core.Common.NetObjectHelper.NodeEntity,
            nodeStatusId);
        return string.Format(
            text,
            string.Format("<span style=\"white-space: nowrap;\">{0}&nbsp;{1}</span>", appImage, appName),
            string.Format("<span style=\"white-space: nowrap;\">{0}&nbsp;{1}</span>", nodeImage, nodeName));
    }

    protected string GetDbActiveCopyParentsMarkup(int databaseId, string databaseName, int appStatusId, string appName, int nodeStatusId, string nodeName)
    {
        var text = Resources.APM_ExBBContent.DatabasePopup_DBNodeAppCaption;
        var activeDbImage = string.Format(
            "<img src=\"/Orion/StatusIcon.ashx?entity={0}&status={1}\"/>",
            SwisEntities.DatabaseCopy,
            databaseId);
        var appImage = string.Format(
            "<img src=\"/Orion/StatusIcon.ashx?entity={0}&status={1}\"/>",
            SwisEntities.Application,
            appStatusId);
        var nodeImage = string.Format(
            "<img src=\"/Orion/StatusIcon.ashx?entity={0}&status={1}\"/>",
            SolarWinds.Orion.Core.Common.NetObjectHelper.NodeEntity,
            nodeStatusId);
        return String.Format(
            text,
            String.Format("<span style=\"white-space: nowrap;\">{0}&nbsp;{1}", activeDbImage, databaseName), "</span></br>",
            String.Format("<span style=\"white-space: nowrap;\">{0}&nbsp;{1}", appImage, appName), "</span></br>",
            String.Format("<span style=\"white-space: nowrap;\">{0}&nbsp;{1}</span>", nodeImage, nodeName));
    }

    protected override DatabaseCopy CreateNetObjectData(string netObjectId)
    {
        var netObject = CreateNetObject();
        var result = netObject as DatabaseCopy;

        if (result == null)
        {
            this.log.ErrorFormat(CultureInfo.InvariantCulture,
                "Cannot render database popup, because cannot get database info from netObject with netObjectID '{0}'",
                netObjectId);
        }

        return result;
    }

    protected override ApmStatus GetStatus()
    {
        return new ApmStatus(NetObjectData.Status);
    }
}
