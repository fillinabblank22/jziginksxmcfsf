﻿using System;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_ExchangeBlackBox_ExchangeDatabaseFileDetails : ApmViewPage
{
    protected override void OnInit(EventArgs e)
    {
        resHost.ExchangeDatabaseFile = NetObjectAsDatabaseFile;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        title.ViewTitle = ViewInfo.ViewTitle;
        title.ViewSubTitle = NetObjectAsDatabaseFile.DatabaseFileModel.PhysicalName;

        topRightLinks.CustomizeViewHref = HtmlHelper.GetDefaultCustomizeViewHref(ViewInfo.ViewID);
        topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
        topRightLinks.EditNetObjectHref =
            ApmMasterPage.GetEditApplicationItemPageUrl(NetObjectAsDatabaseFile.DatabaseFileModel.ApplicationID,
                NetObjectAsDatabaseFile.DatabaseFileModel.DatabaseCopyID);

        topRightLinks.HelpUrlFragment = "OrionSAMAGExchangeBBDBFileDetails";
    }

    public override string ViewType
    {
        get { return ExchangeConstants.DatabaseFileSizeDetailsViewType; }
    }

    public DatabaseFile NetObjectAsDatabaseFile
    {
        get { return (DatabaseFile) NetObject; }
    }
}