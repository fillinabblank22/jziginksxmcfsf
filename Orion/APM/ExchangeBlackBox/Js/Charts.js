﻿SW.APM = SW.APM || {};
SW.APM.EXBB = SW.APM.EXBB || {};
SW.APM.EXBB.Charts = SW.APM.EXBB.Charts || {};
(function(common) {
    common.DatabaseAvailabilitySeriesTemplates = {
        Unknown_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Unknown;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,0,0)"],
                    [0.5, "rgb(50,50,50)"],
                    [1, "rgb(100,100,100)"]
                ]
            }
        },
        Up_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Up;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(63,134,0)"],
                    [0.5, "rgb(93,163,19)"],
                    [1, "rgb(119,189,45)"]
                ]
            }
        },
        Down_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Down;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(145,0,0)"],
                    [0.5, "rgb(205,0,16)"],
                    [1, "rgb(230,25,41)"]
                ]
            }
        },
        Warning_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Warning;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(196,163,0)"],
                    [0.5, "rgb(228,193,16)"],
                    [1, "rgb(252,217,40)"]
                ]
            }
        },
        Critical_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Critical;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(187,95,1)"],
                    [0.5, "rgb(224,132,3)"],
                    [1, "rgb(249,157,28)"]
                ]
            }
        },
        Offline_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Offline;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(90,90,90)"],
                    [0.5, "rgb(145,145,145)"],
                    [1, "rgb(183,183,183)"]
                ]
            }
        },
        Availability: {
            name: "@{R=APM.Strings;K=APM_ChartSettings_yAxis_Availability;E=js}",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };
    common.DatabaseFileSizeSeriesTemplates = {
        UsedSpace: {
            name: "@{R=APM.BlackBox.Exchg.Strings;K=ChartTemplate_SpaceUsedByData;E=js}",
            type: "area",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(153,119,51)"],
                    [0.5, "rgb(189,147,63)"],
                    [1, "rgb(199,162,87)"]
                ]
            }
        },
        WhiteSpace: {
            name: "@{R=APM.BlackBox.Exchg.Strings;K=ChartTemplate_WhiteSpace;E=js}",
            type: "area",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(16,190,229)"],
                    [0.5, "rgb(73,210,242)"],
                    [1, "rgb(160,232,248)"]
                ]
            }
        },
        VolumeFreeSpace: {
            name: "@{R=APM.BlackBox.Exchg.Strings;K=ChartTemplate_FreeVolumeSpace;E=js}",
            type: "area",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,52,82)"],
                    [0.5, "rgb(0,108,169)"],
                    [1, "rgb(0,155,245)"]
                ]
            }
        }
    };

    common.SentReceivedMailChart = function () {
    	return {
    		subtitle: { align: "left", x: -7 },
    		yAxis: [{
    			title: { margin: 10, text: "@{R=APM.BlackBox.Exchg.Strings;K=ChartTemplate_MessagesSentReceived;E=js}" },
    			labels: { align: "right", x: -8 },
    			startOnTick: false,
    			endOnTick: true
    		}],
    		plotOptions: {
    			column: {
    				stacking: "normal",
    				pointPadding: 0,
    				borderColor: "#ffffff",
    				borderWidth: 1,
    				groupPadding: 0.15
    			}
    		},
    		rangeSelector: {
    			enabled: true,
    			buttonTheme: {
    				width: 25
    			},
    			buttons: [
					{ type: "week", count: 1, text: "1w" },
					{ type: "month", count: 1, text: "1m" },
					{ type: "month", count: 3, text: "3m" }
    			],
    			inputEnabled: false,
    			inputEditDateFormat: "%Y-%m-%d",
    			inputDateFormat: "%b %e, %Y",
    			inputStyle: { width: "80px" }
    		},
    		seriesTemplates: {
    		    Internal: {
    		        name: "@{R=APM.BlackBox.Exchg.Strings;K=ChartTemplate_Internal;E=js}",
    		        type: "column",
    		        zIndex: 1,
    		        color: "#006ca9"
    		    },
    		    External: {
    		        name: "@{R=APM.BlackBox.Exchg.Strings;K=ChartTemplate_External;E=js}",
    		        type: "column",
    		        zIndex: 1,
    		        color: "#49D2F2"
    		    },
    			Data_Navigator: {
    				name: "@{R=APM.Strings;K=APM_ChartSettings_yAxis_Availability;E=js}",
    				id: "navigator",
    				showInLegend: false,
    				visible: false
    			}
    		}
    	};
    }();

    common.PercentUsedQuotaSeriesTemplates = {
        QuotaUsed: {
            name: "@{R=APM.BlackBox.Exchg.Strings;K=ChartTemplate_QuotaUsed;E=js}",
            type: "area",
            zIndex: 2,
            color: "#006ca9"
        }
    };
	
})(SW.APM.EXBB.Charts);
