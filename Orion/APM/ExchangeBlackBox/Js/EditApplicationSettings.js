/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
/// <reference path="../../APM.js"/>
APMjs.withGlobal('SW.APM.BB.EX', function (ExchgBB, APMjs) {
    'use strict';

    var
        // module references
        SW = APMjs.assertGlobal('SW'),
        Ext = APMjs.assertExt(),
        $ = APMjs.assertQuery(),
        EditApp = APMjs.assertGlobal('SW.APM.EditApp'),
        EditBbApp = APMjs.assertGlobal('SW.APM.EditBbApp'),
        SETT = {},
        TimerId,
        LoadedCredentials,
        exchangeUrlBase = 'https://localhost';

    // SW.APM.BB.EX.EditAppSettings -->
    APMjs.initGlobal('SW.APM.BB.EX.EditAppSettings', function (ExBbEditApp) {

        function init(data) {
            var s = SETT;
            s.AppType = data.AppType;
            s.ServerIdentityKey = data.ServerIdentityKey;
            s.PsUrlExchangeKey = data.PsUrlExchangeKey;
            s.PsUrlWindowsKey = data.PsUrlWindowsKey;
            s.SettingLevelInstance = parseInt(data.SettingLevelInstance, 10);
            s.AgentPollingMethodName = data.AgentPollingMethodName;
            s.CredentialSetIdKey = data.CredentialSetIdKey;
            s.TestSummaryClientId = data.TestSummaryClientId;
            s.TestButtonClientId = data.TestButtonClientId;
            s.ConfigButtonClientId = data.ConfigButtonClientId;
            s.WindowsTooltipClientId = data.WindowsTooltipClientId;
            s.WindowsTooltipContent = data.WindowsTooltipContent;
            s.ExchangeTooltipClientId = data.ExchangeTooltipClientId;
            s.ExchangeTooltipContent = data.ExchangeTooltipContent;
            s.SaveCredentialChangesTitle = data.SaveCredentialChangesTitle;
            s.SaveCredentialChangesMsg = data.SaveCredentialChangesMsg;
        }

        ExBbEditApp.init = init;
    });
    // <-- SW.APM.BB.EX.EditAppSettings


    // SW.APM.BB.EX.TestButtonHandler -->
    // TODO: retest in IE (which versions?)
    function getTestSettingsFromUi(testingNodeId) {
        var jSettings = $('#appEditExchangeSettings'),
            result = {
                NodeId: testingNodeId,
                AppType: SETT.AppType,
                CredentialSet: {
                    Id: jSettings.find('select[id$=exchangeCredentials_CredentialSetList]').val(),
                    Login: jSettings.find('input[id$=exchangeCredentials_UserName]').val(),
                    Password: jSettings.find('input[id$=exchangeCredentials_Password]').val()
                },
                CustomSettings: {}
            },
            sett = result.CustomSettings;

        sett[SETT.ServerIdentityKey] = jSettings.find('#exchangeServerIdentity').val();
        sett[SETT.PsUrlExchangeKey] = makeFinalExchangeUrl(jSettings.find('#psUrlExchange').val());
        sett[SETT.PsUrlWindowsKey] = jSettings.find('#psUrlWindows').val();

        EditApp.updateCommonTestSettings(result);

        return { settings: result };
    }

    function showErrorSummary(messages, jSummary) {
        var messageHtml;

        jSummary = jSummary || $('#errorSummary');

        if (TimerId) {
            clearTimeout(TimerId);
        }

        messageHtml = messages.join('<br/>');

        jSummary
            .show()
            .find('.error')
            .html(messageHtml);

        TimerId = setTimeout(function () {
            jSummary.hide();
            TimerId = null;
        }, 6000);
    }

    function extractEditablePartOfExchangeUrl(url) {
        return url.replace(/https:\/\/[^\/]+/i, "");
    }

    function makeFinalExchangeUrl(url) {
        return exchangeUrlBase + url;
    }

    function validateUrls(obj, messages) {
        var sett = obj.settings.CustomSettings,
            errors = [];

        EditBbApp.validateUrl(sett[SETT.PsUrlExchangeKey], errors, $('#psUrlExchangeError'));
        EditBbApp.validateUrl(sett[SETT.PsUrlWindowsKey], errors, $('#psUrlWindowsError'));

        if (messages) {
            Array.prototype.push.apply(messages, errors);
        }

        return (errors.length === 0);
    }


    function displayTestSummary(jSumTest, testSuccessfull, msg, showIcon) {
        var jMsg, res, customMessageCls = 'customMsg';

        jSumTest
            .find('.' + customMessageCls)
            .remove();

        if (EditBbApp.isNonEmptyText(msg)) {
            jMsg = $('<span />').addClass(customMessageCls).html(msg);
        }

        if (testSuccessfull) {
            jSumTest.find('.testSuccessful').show().append(jMsg);
            jSumTest.find('.testFailed').hide();
            jSumTest.find('.progress').hide();
        } else {
            jSumTest.find('.testSuccessful').hide();
            jSumTest.find('.testFailed').show().append(jMsg);
            jSumTest.find('.progress').hide();

            res = $('.customMsg').html();
            $('.customMsg').remove();

            showIcon = (showIcon === undefined) ? true : showIcon;

            if (showIcon) {
                $('.testFailed').html('<img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif">' + res);
            } else {
                $('.testFailed').html(res);
            }
        }
    }

    function formatMessage(successMessage, message, success, errorMessage) {
        var resMessage;
        if (success) {
            resMessage = successMessage;
            resMessage += ' successful.';
        } else {
            resMessage = message;
            resMessage += ' failed with the following error:';
            if (errorMessage) {
                resMessage += '<br /><small class="exceptionText">' + errorMessage + '</small>';
            }
        }
        return resMessage;
    }

    function displayProgress(jSumTest) {
        jSumTest.find('.testSuccessful').hide();
        jSumTest.find('.testFailed').hide();
        if (!jSumTest.is(':visible')) {
            jSumTest.slideDown();
        }
        jSumTest.find('.progress').show();
    }

    APMjs.initGlobalClass('SW.APM.BB.EX.TestButtonHandler', function (nodeId, testSummaryId, validationSummaryId) {
        var jSumTest, jSumVldt;

        jSumTest = $('#' + testSummaryId);
        jSumVldt = $('#' + validationSummaryId);

        function callTestBlackBoxConnection(settings, onSuccess, onError) {
            SW.Core.Services.callWebService('/Orion/APM/ExchangeBlackBox/Services/Services.asmx', 'TestExchangeBlackBoxConnection', settings, onSuccess, onError);
        }

        function clickHandler() {
            var testSett, messages, testResult, jqSettings;

            jqSettings = $('#appEditExchangeSettings');
            testSett = getTestSettingsFromUi(nodeId);
            messages = [];

            testResult = validateUrls(testSett, messages)
                && jqSettings.closest('form').valid();

            showErrorSummary(messages, jSumVldt);

            if (testResult) {
                displayProgress(jSumTest);
                callTestBlackBoxConnection(
                    testSett,
                    function (result) {
                        var tableTemplate, message, res;
                        if (result.Result.IsValidPsUrlExchange && result.Result.IsValidPsUrlWindows) {
                            displayTestSummary(jSumTest, true, "");
                        } else {
                            tableTemplate = '<table style="display: inline-block">'
                                + '<tr><td><img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif"></td><td>WIN_RM_MESSAGE</td></tr>'
                                + '<tr><td></td><td>EXCHANGE_MESSAGE</td></tr>'
                                + '</table>';
                            message = tableTemplate;
                            res = result.Result;

                            message = message
                                .replace(
                                    'WIN_RM_MESSAGE',
                                    formatMessage(
                                        'WinRM test was',
                                        'WinRM testing',
                                        res.IsValidPsUrlWindows,
                                        res.PsUrlWindowsExceptionMessage
                                    )
                                );

                            message = message
                                .replace(
                                    'EXCHANGE_MESSAGE',
                                    formatMessage(
                                        'PowerShell Exchange web site test was',
                                        'PowerShell Exchange web site testing',
                                        res.IsValidPsUrlExchange,
                                        res.PsUrlExchangeExceptionMessage
                                    )
                                );

                            displayTestSummary(jSumTest, false, message, false);
                        }
                    },
                    function (message) {
                        message = $.trim(message);
                        if (!message.length) {
                            message = '@{R=APM.Strings;K=APMWEBJS_ErrorWhileTestingConnection;E=js}';
                        }
                        displayTestSummary(jSumTest, false, message);
                    }
                );
            }
            showErrorSummary(messages);
            return false;
        }

        this.onClick = clickHandler;
    });
    // <-- SW.APM.BB.EX.TestButtonHandler


    // SW.APM.BB.EX.ApplicationEditor -->
    function findSetting(settingPropertyName, settingsCollection) {
        var foundSetting = null;
        APMjs.linqAny(settingsCollection, function (sett) {
            if (sett.Key === settingPropertyName) {
                foundSetting = sett;
                return true;
            }
        });
        return foundSetting;
    }

    function noopFalse() {
        return false;
    }

    APMjs.initGlobalClass('SW.APM.BB.EX.ApplicationEditor', function () {
        var bkpInitialCredSetId = -1,
            settingLevelInstance = SETT.SettingLevelInstance,
            credSetIdElSelector = 'select[id$=exchangeCredentials_CredentialSetList]',
            appSet;

        function getApplicationSettings(appId) {
            SW.Core.Services.callWebServiceSync(
                '/Orion/APM/ExchangeBlackBox/Services/ExchangeServerConfigurator.asmx',
                'GetExchgBbApplicationWithoutCred',
                {
                    appId: appId
                },
                function (app) {
                    appSet = app;
                },
                APMjs.noop
            );
        }

        function updateConfigurationSettings(appId) {
            SW.Core.Services.callWebService(
                '/Orion/APM/ExchangeBlackBox/Services/ExchangeServerConfigurator.asmx',
                'SetExchangeConfigurationDiscoveryResult',
                {
                    'appId': appId,
                    'credId': 0
                },
                noopFalse,
                noopFalse
            );
        }

        function runTest(item, configuratorExecutor, jMask) {

            function testSuccess(result) {
                var message = '';
                jMask.hide();

                if (result.d.Result.IsValidPsUrlExchange === false || result.d.Result.IsValidPsUrlWindows === false) {
                    if (result.d.Result.IsValidPsUrlExchange) {
                        message = '@{R=APM.Strings;K=APMWEBJS_WinRMTestingFailedWithTheFollowingError;E=js}' + result.d.Result.PsUrlWindowsExceptionMessage + '\r\n ' + '@{R=APM.Strings;K=APMWEBJS_ThePowerShellExchangeWebSiteTestWasSuccessful;E=js}';
                    } else if (result.d.Result.IsValidPsUrlWindows) {
                        message = 'WinRM test was successful. ' + '@{R=APM.Strings;K=APMWEBJS_PowerShellExchangeWebSiteTestingFailedWithTheFollowingError;E=js}' + result.d.Result.PsUrlExchangeExceptionMessage;
                    } else {
                        message = '@{R=APM.Strings;K=APMWEBJS_WinRMTestingFailedWithTheFollowingError;E=js}'
                            + (result.d.Result.PsUrlWindowsExceptionMessage || '')
                            + '@{R=APM.Strings;K=APMWEBJS_PowerShellExchangeWebSiteTestingFailedWithTheFollowingError;E=js}'
                            + (result.d.Result.PsUrlExchangeExceptionMessage || '');
                    }
                    configuratorExecutor.ToggleError(message + '"');
                    return false;
                }

                $('.config-result').show();
                updateConfigurationSettings(item.Id);

                return false;
            }

            function testError(error) {
                jMask.hide();
                configuratorExecutor.ToggleTestError(error, item.NodeName, null);
                return false;
            }

            $.ajax({
                type: 'POST',
                url: '/Orion/APM/ExchangeBlackBox/Services/Services.asmx/TestExchangeBlackBoxConnection',
                data: JSON.stringify(getTestSettingsFromUi(item.NodeId)),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                timeout: 10 * 60 * 1000,
                success: testSuccess,
                error: testError
            });
        }

        function initConfiguratorExecutor(item, configServerButtonId) {

            function configClick() {
                var jSettings, testSett, messages, validationResult, configuratorExecutor, xMask, jLoadingMask, appInfo, cred, uiSettings;

                jSettings = $(this);
                testSett = getTestSettingsFromUi(item.NodeId);
                messages = [];
                validationResult = validateUrls(testSett, messages) && jSettings.closest('form').valid();

                if (!validationResult) {
                    return false;
                }

                configuratorExecutor = new SW.APM.ExchgBB.ConfigExecutor();
                configuratorExecutor.Init('.setup-info div.load', '.setup-info div.status', '.setup-info div.error', item.Id);
                $('.config-result').hide();
                configuratorExecutor.ToggleError('');
                configuratorExecutor.ManualHelpLink = '<%=HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeManuallyConfigureExchangeServer")%>';

                xMask = new Ext.LoadMask(Ext.get('container'), { msg: '<label id="configuratorLoadMask">' + '@{R=APM.Strings;K=APMWEBJS_ConfiguringServerThisMayTakeFewMinutes;E=js}' + '</label>' });
                xMask.show();

                jLoadingMask = $('#configuratorLoadMask').parent('div').eq(0);
                jLoadingMask.parent('div').eq(0).css('top', ($(window).height() / 2) + 'px');

                appInfo = {
                    NodeName: appSet.NodeName,
                    NodeId: appSet.NodeId,
                    NodeIp: appSet.NodeIp,
                    UseAgent: appSet.UseAgent
                };

                cred = {
                    Id: $('select[id$=exchangeCredentials_CredentialSetList]').val(),
                    Name: $('input[id$=exchangeCredentials_CredentialName]').val(),
                    UserName: $('input[id$=exchangeCredentials_UserName]').val(),
                    Password: $('input[id$=exchangeCredentials_Password]').val()
                };

                uiSettings = {};
                EditApp.updateCommonTestSettings(uiSettings);
                appInfo.UseAgent = uiSettings.ExecutePollingMethod === SETT.AgentPollingMethodName;

                configuratorExecutor.StartConfigurator(
                    appInfo,
                    cred,
                    noopFalse,
                    function () {
                        xMask.msg = '<label id="configuratorLoadMask">' + '@{R=APM.Strings;K=APMWEBJS_TestingConnection;E=js}' + '</label>';
                        xMask.show();
                        jLoadingMask = $('#configuratorLoadMask').parent('div').eq(0);
                        jLoadingMask.parent('div').eq(0).css('top', ($(window).height() / 2) + 'px');
                        runTest(item, configuratorExecutor, xMask);
                    },
                    function () {
                        xMask.hide();
                    }
                );

                return false;
            }

            $('#configuratorErrorMessageContainer').hide();
            $('.config-result').hide();
            $('#appEditExchangeSettings')
                .find('#' + configServerButtonId)
                .unbind('click')
                .click(configClick);
        }

        function reloadCredentials() {
            SW.APM.SelectCredentials.ReloadCredentialSets(credSetIdElSelector);
        }

        function createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError) {
            function onCredentialsCreated(credentialSetId) {
                credentialSetIdSetting.Value = credentialSetId;
                setTimeout(reloadCredentials, 500);
                onSuccess();
            }
            var newCredentials = {
                name: $('input[id$=exchangeCredentials_CredentialName]').val(),
                userName: $('input[id$=exchangeCredentials_UserName]').val(),
                psw: $('input[id$=exchangeCredentials_Password]').val()
            };
            SW.Core.Services.callWebService(
                '/Orion/APM/Services/Credentials.asmx',
                'CreateCredential',
                newCredentials,
                onCredentialsCreated,
                onError
            );
        }

        function initUiState(item) {
            var jSettings, foundSetting, testButtonHandler;

            foundSetting = findSetting('CredentialSetId', item.Settings);
            if (foundSetting && foundSetting.Value) {
                LoadedCredentials = parseInt(foundSetting.Value, 10);
            }

            // show the dialog +
            jSettings = $('#appEditExchangeSettings');
            jSettings.show();

            foundSetting = findSetting(SETT.ServerIdentityKey, item.Settings);
            if (foundSetting && foundSetting.Value) {
                jSettings
                    .find('#exchangeServerIdentity')
                    .html(foundSetting.Value);
            }

            foundSetting = findSetting(SETT.PsUrlExchangeKey, item.Settings);
            if (foundSetting && foundSetting.Value) {
                jSettings
                    .find('#psUrlExchange')
                    .val(extractEditablePartOfExchangeUrl(foundSetting.Value));
            }

            foundSetting = findSetting(SETT.PsUrlWindowsKey, item.Settings);
            if (foundSetting && foundSetting.Value) {
                jSettings
                    .find('#psUrlWindows')
                    .val(foundSetting.Value);
            }

            foundSetting = findSetting(SETT.CredentialSetIdKey, item.Settings);
            if (foundSetting && foundSetting.Value) {
                jSettings
                    .find('select[id$=exchangeCredentials_CredentialSetList]')
                    .val(foundSetting.Value)
                    .change();
                bkpInitialCredSetId = parseInt(foundSetting.Value, 10);
            }

            testButtonHandler = new ExchgBB.TestButtonHandler(item.NodeId, SETT.TestSummaryClientId);
            initConfiguratorExecutor(item, SETT.ConfigButtonClientId);

            jSettings
                .find('#' + SETT.TestButtonClientId)
                .unbind('click')
                .click(testButtonHandler.onClick);

            if (item.NodeSubType === 'SNMP') {
                $('select[id$=exchangeCredentials_CredentialSetList] option[value=-3]').remove();
                $('.selectCreds').attr('style', 'width: 360px');
                $('div.testSuccessful').attr('style', 'width: 360px');
                $('div.testFailed').attr('style', 'width: 360px');
            }

            if (parseInt(jSettings.find('select[id$=exchangeCredentials_CredentialSetList]').val(), 10) === -1) {
                jSettings.find('input[id$=exchangeCredentials_UserName]').removeAttr('disabled');
                jSettings.find('input[id$=exchangeCredentials_Password]').removeAttr('disabled');
                jSettings.find('input[id$=exchangeCredentials_CredentialName]').removeAttr('disabled');
                jSettings.find('input[id$=exchangeCredentials_ConfirmPassword]').removeAttr('disabled');
            }

            getApplicationSettings(item.Id);

            EditBbApp.initToolTip(SETT.WindowsTooltipClientId, SETT.WindowsTooltipContent);
            EditBbApp.initToolTip(SETT.ExchangeTooltipClientId, SETT.ExchangeTooltipContent);
        }

        function updateModelFromUi(item) {
            var foundSetting, validationResult, jSettings, testSett, messages = [];

            testSett = getTestSettingsFromUi(item.NodeId);
            jSettings = $('#appEditExchangeSettings');

            validationResult = validateUrls(testSett, messages) && jSettings.closest('form').valid();

            if (!validationResult) {
                throw messages;
            }

            foundSetting = findSetting(SETT.CredentialSetIdKey, item.Settings);
            if (foundSetting) {
                foundSetting.SettingLevel = settingLevelInstance;
                foundSetting.Value = jSettings.find('select[id$=exchangeCredentials_CredentialSetList]').val();
            }

            foundSetting = findSetting(SETT.PsUrlExchangeKey, item.Settings);
            if (foundSetting) {
                foundSetting.SettingLevel = settingLevelInstance;
                foundSetting.Value = makeFinalExchangeUrl(jSettings.find('#psUrlExchange').val());
            }

            foundSetting = findSetting(SETT.PsUrlWindowsKey, item.Settings);
            if (foundSetting) {
                foundSetting.SettingLevel = settingLevelInstance;
                foundSetting.Value = jSettings.find('#psUrlWindows').val();
            }
        }

        function asyncPostProcessing(app, unused_template, onSuccess, onError) {
            var connectionTestSucceeded, wasCredentialsChanged, credentialSetIdSetting, credentialSetIdValue, testSett, isNewCredAssigned;

            credentialSetIdSetting = findSetting(SETT.CredentialSetIdKey, app.Settings);

            if (credentialSetIdSetting) {
                credentialSetIdValue = parseInt(credentialSetIdSetting.Value, 10);
                testSett = getTestSettingsFromUi(app.NodeId);
                isNewCredAssigned = (credentialSetIdValue === SW.APM.SelectCredentials.NewCredentialId);

                if (LoadedCredentials !== undefined) {
                    wasCredentialsChanged = (LoadedCredentials !== credentialSetIdValue);
                }

                if (isNewCredAssigned || wasCredentialsChanged) {
                    SW.Core.Services.callWebServiceSync(
                        '/Orion/APM/ExchangeBlackBox/Services/Services.asmx',
                        'TestExchangeBlackBoxConnection',
                        testSett,
                        function (result) {
                            connectionTestSucceeded = result.TestSuccessfull
                                && result.Result.IsValidPsUrlExchange
                                && result.Result.IsValidPsUrlWindows;
                            if (connectionTestSucceeded) {
                                LoadedCredentials = credentialSetIdValue;
                            }
                        },
                        function () {
                            connectionTestSucceeded = false;
                        }
                    );
                }

                if (APMjs.isDef(connectionTestSucceeded) && (isNewCredAssigned || bkpInitialCredSetId !== credentialSetIdValue) && !connectionTestSucceeded) {
                    Ext.Msg.show({
                        title: SETT.SaveCredentialChangesTitle,
                        msg: SETT.SaveCredentialChangesTitle,
                        buttons: Ext.Msg.OKCANCEL,
                        fn: function (btn) {
                            if (btn === 'ok') {
                                LoadedCredentials = credentialSetIdValue;
                                if (isNewCredAssigned) {
                                    createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                                } else {
                                    onSuccess();
                                }
                            } else {
                                onError();
                            }
                        },
                        icon: Ext.MessageBox.WARNING
                    });
                    return true;
                }
                if (isNewCredAssigned) {
                    createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                    return true;
                }
            }
            return false;
        }

        this.initUiState = initUiState;
        this.updateModelFromUi = updateModelFromUi;
        this.asyncPostProcessing = asyncPostProcessing;
    });
    // <-- SW.APM.BB.EX.ApplicationEditor

    Ext.ComponentMgr.registerPlugin('SW.APM.BB.EX.ApplicationEditor', ExchgBB.ApplicationEditor);
});
