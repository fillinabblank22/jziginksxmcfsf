﻿MakeNamespace("SW.APM.ExchgBB");

SW.APM.ExchgBB.ConfigDialog = function () {
	var mAppId = null, mAppInfo = null, mCreds = null, clearError = true, loadedProcessingState = false;
	var mDlgEl = null;
	var configuratorExecutor = null;

        
    var getTestSettingsFromUi = function() {
        return {
            settings: {
                NodeId: mAppInfo.NodeId,
                AppType: mAppInfo.AppType,
                CredentialSet: {
                    Id: $("select#excBbCreds :selected").val(),
                    Login: $("#excBbUserName").val(),
                    Password: $("#excBbPassword1").val()
                },
                CustomSettings: {
                    "ServerIdentity": mAppInfo.NodeName,
                    'PsUrlExchange': mAppInfo.PsUrlExchange,
                    'PsUrlWindows': mAppInfo.PsUrlWindows
                }
            }
        };
    };
    /*helper members*/
    var validate = function() {
        configuratorExecutor.ToggleLoad(false);
        configuratorExecutor.ToggleStatus("");
        $(".config-validation-error").html("");

        var message = "";
        if (parseInt($("#excBbCreds :selected").val()) == -1) {
            var value = $.trim($("#excBbCredName").val());
            if (value.length == 0) {
                message += '@{R=APM.Strings;K=APMWEBJS_EnterCredentialName;E=js}'+"<br />";
            }
            value = $.trim($("#excBbUserName").val());
            if (value.length == 0) {
                message += '@{R=APM.Strings;K=APMWEBJS_EnterUserName;E=js}'+"<br />";
            }
            if ($("#excBbPassword1").val() != $("#excBbPassword2").val()) {
                message += '@{R=APM.Strings;K=APMWEBJS_ConfirmPasswordIsDifferent;E=js}'+ "<br />";
            }
        }
        if (message.length > 0) {
            $(".config-validation-error").html(message);
        }
        return message.length == 0;
    };

    var loadProcessingState = function () {
    	if (loadedProcessingState || mCreds == null || mAppInfo == null) {
    		return;
    	}
    	loadedProcessingState = true;
    	disableDialog();
    	configuratorExecutor.TryGetSetupApplicationState(mAppInfo.NodeIp, selectCredentialsHandler, newCredentialsHandler, onConfigurationSuccess, enableDialog);
    };

    var runTest = function (nodeId) {
        $(".connection-success").hide();
        
        $.ajax({
            type: "POST",
            url: "/Orion/APM/ExchangeBlackBox/Services/Services.asmx/TestExchangeBlackBoxConnection",
            data: JSON.stringify(getTestSettingsFromUi(nodeId)),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            timeout:10*60*1000,
            success: function (result) {
                if (result.d.Result.IsValidPsUrlExchange == false || result.d.Result.IsValidPsUrlWindows == false)
                {
                    var message = '@{R=APM.Strings;K=APMWEBJS_WinRMTestingFailedWithTheFollowingError;E=js}';
                    message += result.d.Result.PsUrlWindowsExceptionMessage || '';
                    message += '@{R=APM.Strings;K=APMWEBJS_PowerShellExchangeWebSiteTestingFailedWithTheFollowingError;E=js}';
                    message += result.d.Result.PsUrlExchangeExceptionMessage || '';

                    if (result.d.Result.IsValidPsUrlExchange) {
                        message = '@{R=APM.Strings;K=APMWEBJS_WinRMTestingFailedWithTheFollowingError;E=js}' + result.d.Result.PsUrlWindowsExceptionMessage + '\r\n PowerShell Exchange web site test was successful.';
                    } else if (result.d.Result.IsValidPsUrlWindows) {
                        message = '@{R=APM.Strings;K=APMWEBJS_WinRMTestWasSuccessful;E=js}' + '@{R=APM.Strings;K=APMWEBJS_PowerShellExchangeWebSiteTestingFailedWithTheFollowingError;E=js}' + result.d.Result.PsUrlExchangeExceptionMessage;
                    }

                    configuratorExecutor.ToggleError(message + "\"");
                    return;
                }
                
                var credId = parseInt($("#excBbCreds :selected").val());
                if (credId == -1) {
                    updateApplicationNewCredentials(mAppId);
                } else {
                    configuratorExecutor.CallWebService("SetExchangeConfigurationDiscoveryResult", { "appId": mAppId, "credId": credId }, function () { return false; });
                }
                configuratorExecutor.ToggleError("");
                $(".connection-success").show();
                setTimeout(function () {
                    window.open('/Orion/APM/ExchangeBlackBox/ExchangeApplicationDetails.aspx?NetObject=ABXA:' + mAppInfo.Id, '_self', false);
                }, 2 * 1000);
            },
            error: function (error) {
                configuratorExecutor.ToggleTestError(error, mAppInfo.NodeName, mAppInfo.Id);
                enableDialog();
            }
        });
    };

    var onConfigurationSuccess = function () {
        $(".config-in-progress-msg").html("Testing connection...");
        runTest();
    };
    
    var newCredentialsHandler = function () {
        if (parseInt($("select#excBbCreds :selected").val() == -1)) {
    	    configuratorExecutor.CallWebService("GetCredentials", {}, loadNewCredentials);

    	}
    };
    var selectCredentialsHandler = function (credId) {
    	$("select#excBbCreds").val(credId);
    	onCredSelect();
    };
    var updateApplicationNewCredentials = function (appId) {
        configuratorExecutor.CallWebService("GetCredentials", {}, function (creds) {
                var value = $.trim($("#excBbCredName").val());
                $.each(creds, function () {
                   if (value == this.Name) {
                       configuratorExecutor.CallWebService("SetExchangeConfigurationDiscoveryResult", { "appId": appId, "credId": this.Id }, function () { return false; });
                    }
                });
            });
    };

    var disableDialog = function() {
        $(".btn-setup").attr("enabled", "false")
            .removeClass("sw-btn sw-btn-primary")
            .addClass("sw-btn-primary sw-btn sw-btn-disabled").unbind("click").bind("click", function () { return false; });

        $(".btn-cancel").attr("enabled", "false")
            .removeClass(" sw-btn sw-btn-cancel")
            .addClass("sw-btn sw-btn-cancel sw-btn-disabled")
            .unbind("click").unbind("click").bind("click", function () { return false; });
        $("#excBbCreds, #excBbCredName, #excBbUserName, #excBbPassword1, #excBbPassword2").attr("disabled", "disabled");
    };

    var enableDialog = function () {
        $(".btn-setup").attr("enabled", "true")
            .removeClass("sw-btn-primary sw-btn sw-btn-disabled")
            .addClass("sw-btn sw-btn-primary")
            .unbind("click")
            .bind("click", onButtonSetupClick);
        $(".btn-cancel").attr("enabled", "true")
            .removeClass("sw-btn sw-btn-cancel sw-btn-disabled")
            .addClass(" sw-btn sw-btn-cancel")
            .unbind("click")
            .bind("click", onButtonCancelClick);
        if (parseInt($("#excBbCreds :selected").val()) == -1) {
            $("#excBbCreds, #excBbCredName, #excBbUserName, #excBbPassword1, #excBbPassword2").removeAttr("disabled");
        } else {
            $("#excBbCreds").removeAttr("disabled");
        }
     };

    /*ajax event handlers*/
    var onCredentialsLoad = function(creds) {
        mCreds = {};
        
        var el = $("#excBbCreds");
        el.removeAttr("disabled").empty();
        if (mAppInfo.NodeType && mAppInfo.NodeType == 3) {
            el.append($("<option value='-3'  selected='selected'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_InheritWindowsCredentialFromNode;E=js}" + "&gt;</option>"));
            el.append($("<option value='-1'>&lt;" +"@{R=APM.Strings;K=APMWEBJS_NewCredential;E=js}"+"&gt;</option>"));
        } else {
            el.append($("<option value='-1' selected='selected'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_NewCredential;E=js}"+"&gt;</option>"));
        }
        
        $.each(creds, function() {
            mCreds[this.Id] = this;
            el.append($(SF("<option value='{0}'>{1}</option>", this.Id, this.Name)));
        });
        el.unbind("change").bind("change", onCredSelect);
        el.change();
        loadedProcessingState = false;
        loadProcessingState();
    };
    var onApplicationLoad = function(app) {
        mAppInfo = app;
        configuratorExecutor.ManualHelpLink = app.ManualHelpLink;
        $("#configEditAppLink").attr("href", "/Orion/Apm/Admin/Edit/EditApplication.aspx?id=" + mAppInfo.Id);
        $("#excBbApps").html(SF($("script#exc-bb-app-name-tpl").html(), mAppInfo.Name, mAppInfo.NodeId, mAppInfo.NodeStatus, mAppInfo.NodeName, mAppInfo.AppStatus));
        configuratorExecutor.CallWebService("GetCredentials", {}, onCredentialsLoad);
        loadProcessingState();
    };

    var loadNewCredentials = function(creds) {
        mCreds = {};
        var value = $.trim($("#excBbCredName").val());
        var el = $("#excBbCreds");
        el.removeAttr("disabled").empty();
        if (mAppInfo.NodeType && mAppInfo.NodeType == 3) {
            el.append($("<option value='-3'>&lt;" + "@{R=APM.Strings;K=APMWEBJS_InheritWindowsCredentialFromNode;E=js}"+"&gt;</option>"));
        }
        el.append($("<option value='-1'>&lt;" + '@{R=APM.Strings;K=APMWEBJS_NewCredential;E=js}'+"&gt;</option>"));
        $.each(creds, function() {
            mCreds[this.Id] = this;
            if (value == this.Name) {
                el.append($(SF("<option value='{0}' selected='selected'>{1}</option>", this.Id, this.Name)));
            } else {
                el.append($(SF("<option value='{0}'>{1}</option>", this.Id, this.Name)));
            }
        });
        clearError = false;
        el.unbind("change").bind("change", onCredSelect);
        el.change();
    };
    
	/*dom event handlers*/
    var onCredSelect = function () {
        configuratorExecutor.ToggleStatus("");
        if (clearError == true) {
            configuratorExecutor.ToggleError("");
        }
        clearError = true;
        
        var id = parseInt($("#excBbCreds :selected").val());
        if (id == -1) {
			$("#excBbCredName, #excBbUserName, #excBbPassword1, #excBbPassword2")
				.removeAttr("disabled")
				.val("");
		} else if (id == -3) {
			$("#excBbCredName, #excBbUserName, #excBbPassword1, #excBbPassword2")
				.attr("disabled", "disabled")
				.val("");
		}
        else {
			$("#excBbCredName").val(mCreds[id].Name).attr("disabled", "disabled");
			$("#excBbUserName").val(mCreds[id].UserName).attr("disabled", "disabled");
			$("#excBbPassword1").val(mCreds[id].Password).attr("disabled", "disabled");
			$("#excBbPassword2").val(mCreds[id].Password).attr("disabled", "disabled");
		}
    };
    
    var onButtonSetupClick = function () {
        if (validate()) {
            $(".config-validation-error").html("");
            $(".connection-fail").hide();
            disableDialog();
            $(".config-in-progress-msg").html("@{R=APM.Strings;K=APMWEBJS_ConfiguringServer;E=js}");
	            var cred = {
	                Id: $("select#excBbCreds :selected").val(),
	                Name: $("#excBbCredName").val(),
	                UserName: $("#excBbUserName").val(),
	                Password: $("#excBbPassword1").val()
	            };
	            configuratorExecutor.StartConfigurator(mAppInfo, cred, newCredentialsHandler, onConfigurationSuccess, enableDialog);
        }
	    return false;
    };
    var onButtonCancelClick = function() {
        configuratorExecutor.ToggleLoad(false);
        configuratorExecutor.ToggleStatus("");
        configuratorExecutor.ToggleError("");
        mDlgEl.dialog("destroy");
        return false;
    };

	/*public members*/
    this.init = function(appId) {
        mAppId = appId;
        configuratorExecutor = new SW.APM.ExchgBB.ConfigExecutor();
        configuratorExecutor.Init("excBbConfigDialog .setup-info div.load", "excBbConfigDialog .setup-info div.status", "excBbConfigDialog .setup-info div.error", appId);
        mDlgEl = $("#excBbConfigDialog").dialog({
            width: 560,
            position: [$(window).width() / 2 - 255, 100],
            modal: true,
            autoOpen: false,
            overlay: { "background-color": "black", opacity: 0.4 }
        });
        $("#excBbApps").html($("script#exc-bb-app-load").html());

        $("#excBbCreds").empty();
        enableDialog();
        
        $("#excBbConfigDialog a[id$=btnSetup]")
            .unbind("click")
            .bind("click", onButtonSetupClick);
        $("#excBbConfigDialog a[id$=btnCancel]")
            .unbind("click")
            .bind("click", onButtonCancelClick);

        $("#excBbConfigDialog .setup-info div.load")
            .hide();
        $("#excBbConfigDialog .setup-info div.status")
            .empty();
        $("#errorMessage").empty();
        $("#configuratorErrorMessageContainer").hide();
        $('#expander').unbind('click');
        $("#expander").click(function () {
            if (this.attributes["state"].value == "collapsed") {
                this.attributes["src"].value = "/Orion/images/Button.Collapse.gif";
                this.attributes["state"].value = "expanded";
                $(".expander-container").show();
                return;
            }
            this.attributes["state"].value = "collapsed";
            this.attributes["src"].value = "/Orion/images/Button.Expand.gif";
            $(".expander-container").hide();
            return;
        });
        
        configuratorExecutor.CallWebService("GetExchgBbApplicationWithoutCred", { appId: mAppId }, onApplicationLoad);
        $("#serverIsAlreadyConfiguredLink").click(function () {
            configuratorExecutor.CallWebService("SetExchangeConfigurationDiscoveryResult", { "appId": appId, "credId": 0 }, function () {
                window.open('/Orion/APM/ExchangeBlackBox/ExchangeApplicationDetails.aspx?NetObject=ABXA:' + mAppInfo.Id, '_self', false);
                mDlgEl.dialog("destroy");
                return false;
            });
        });
        
    };
    this.show = function() {
        mDlgEl.dialog("open");
    	$("#excBbCredName, #excBbUserName, #excBbPassword1, #excBbPassword2")
			.val("").attr("disabled", "disabled");
    };
    this.hide = function() {
        mDlgEl.dialog("destroy");
    };
};

/*static members*/
SW.APM.ExchgBB.ConfigDialog.instance = null;
SW.APM.ExchgBB.ConfigDialog.show = function (appId) {
	if (SW.APM.ExchgBB.ConfigDialog.instance == null) {
		SW.APM.ExchgBB.ConfigDialog.instance = new SW.APM.ExchgBB.ConfigDialog();
	}
	SW.APM.ExchgBB.ConfigDialog.instance.init(appId);
	SW.APM.ExchgBB.ConfigDialog.instance.show();
};
SW.APM.ExchgBB.ConfigDialog.hide = function () {
	if (SW.APM.ExchgBB.ConfigDialog.instance == null) {
		return;
	}
	SW.APM.ExchgBB.ConfigDialog.instance.hide();
};