/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.EXBB.Resources.Constants', function (Constants) {
    'use strict';

    Constants.SHORT_DATE_FORMAT = 'M dd, yy';
    Constants.NOT_AVAILABLE_DATA = '@{R=APM.BlackBox.Exchg.Strings;K=NotAvailableData;E=js}';
});

APMjs.initGlobal('SW.APM.EXBB.Resources', function (Resources, APMjs) {
    'use strict';

    var inner,
        checkIconRoot = '/Orion/APM/ExchangeBlackBox/Images/CheckIcons/',
        replStatusCssBase = 'exbb-repl-status-',
        SW = APMjs.assertGlobal('SW'),
        $ = APMjs.assertQuery();

    inner = {
        coreStatusInfo: {
            0: {
                code: 'Undefined',
                name: '@{R=APM.Strings;K=APMWEBJS_TM0_31;E=js}'
            },
            1: {
                code: 'Available',
                name: '@{R=APM.Strings;K=Status_Available;E=js}'
            },
            2: {
                code: 'NotAvailable',
                name: '@{R=APM.Strings;K=Status_NotAvailable;E=js}'
            },
            15: {
                code: 'PartlyAvailable',
                name: '@{R=APM.Strings;K=Status_PartlyAvailable;E=js}'
            },
            28: {
                code: 'NotLicensed',
                name: '@{R=APM.Strings;K=Status_NotLicensed;E=js}'
            },
            3: {
                code: 'Warning',
                name: '@{R=APM.Strings;K=Status_Warning;E=js}'
            },
            14: {
                code: 'Critical',
                name: '@{R=APM.Strings;K=Status_Critical;E=js}'
            },
            27: {
                code: 'IsDisabled',
                name: '@{R=APM.Strings;K=Status_IsDisabled;E=js}'
            },
            9: {
                code: 'Unmanaged',
                name: '@{R=APM.Strings;K=APMWEBJS_TM0_6;E=js}'
            },
            12: {
                code: 'Unreachable',
                name: '@{R=APM.Strings;K=Status_Unreachable;E=js}'
            }
        },

        replicationStatusInfo: {
            0: {
                css: 'undef',
                name: ' ',
                icon: ''
            },
            1: {
                css: 'check',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=ReplicationStatus_Passed;E=js}',
                icon: checkIconRoot + 'passed.png'
            },
            14: {
                css: 'fail',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=ReplicationStatus_Failed;E=js}',
                icon: checkIconRoot + 'failed.png'
            },
            3: {
                css: 'warn',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=ReplicationStatus_Warning;E=js}',
                icon: checkIconRoot + 'warning.png'
            }
        },

        contentIndexStatusType: {
            0: {
                code: 'Unknown',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_Unknown;E=js}'
            },
            1: {
                code: 'Healthy',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_Healthy;E=js}'
            },
            2: {
                code: 'Crawling',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_Crawling;E=js}'
            },
            3: {
                code: 'Failed',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_Failed;E=js}'
            },
            4: {
                code: 'Seeding',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_Seeding;E=js}'
            },
            5: {
                code: 'FailedAndSuspended',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_FailedAndSuspended;E=js}'
            },
            6: {
                code: 'Suspended',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_Suspended;E=js}'
            },
            7: {
                code: 'Disabled',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_Disabled;E=js}'
            },
            8: {
                code: 'AutoSuspended',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_AutoSuspended;E=js}'
            },
            9: {
                code: 'HealthyAndUpgrading',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_HealthyAndUpgrading;E=js}'
            },
            10: {
                code: 'DiskUnavailable',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_DiskUnavailable;E=js}'
            },
            11: {
                code: 'NotApplicable',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_ContentIndexStatusType_NotApplicable;E=js}'
            },
                
        },

        copyStatusInfo: {
            0: {
                code: 'Unknown',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Unknown;E=js}'
            },
            3: {
                code: 'Failed',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Failed;E=js}'
            },
            4: {
                code: 'Seeding',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Seeding;E=js}'
            },
            5: {
                code: 'Suspended',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Suspended;E=js}'
            },
            6: {
                code: 'Healthy',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Healthy;E=js}'
            },
            7: {
                code: 'ServiceDown',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_ServiceDown;E=js}'
            },
            8: {
                code: 'Initializing',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Initializing;E=js}'
            },
            9: {
                code: 'Resynchronizing',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Resynchronizing;E=js}'
            },
            11: {
                code: 'Mounted',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Mounted;E=js}'
            },
            12: {
                code: 'Dismounted',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Dismounted;E=js}'
            },
            13: {
                code: 'Mounting',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Mounting;E=js}'
            },
            14: {
                code: 'Dismounting',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Dismounting;E=js}'
            },
            15: {
                code: 'DisconnectedAndHealthy',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_DisconnectedAndHealthy;E=js}'
            },
            16: {
                code: 'FailedAndSuspended',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_FailedAndSuspended;E=js}'
            },
            17: {
                code: 'DisconnectedAndResynchronizing',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_DisconnectedAndResynchronizing;E=js}'
            },
            18: {
                code: 'NonExchangeReplication',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_NonExchangeReplication;E=js}'
            },
            19: {
                code: 'SeedingSource',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_SeedingSource;E=js}'
            },
            20: {
                code: 'Misconfigured',
                name: '@{R=APM.BlackBox.Exchg.Strings;K=Enum_CopyStatus_Misconfigured;E=js}'
            }
        }
    };

    function getCoreStatusInfo(status) {
        return inner.coreStatusInfo[status] || {
            code: 'Unknown',
            name: 'Unknown'
        };
    }

    function getReplicationStatusInfo(status) {
        return inner.replicationStatusInfo[status] || {
            css: 'unknown',
            icon: ''
        };
    }

    function getReplicationStatusName(status) {
        return status && (getReplicationStatusInfo(status).name || getCoreStatusInfo(status).name);
    }

    function getReplicationStatusClass(status) {
        return status && (replStatusCssBase + getReplicationStatusInfo(status).css);
    }

    function getReplicationStatusIcon(status) {
        return status && (getReplicationStatusInfo(status).icon);
    }

    function getCopyStatus(status) {
        return status && (inner.copyStatusInfo[status] === undefined ? inner.copyStatusInfo[0] : inner.copyStatusInfo[status]);
    }

    function getContentIndexStatusType(status) {
        return status && (inner.contentIndexStatusType[status] === undefined ? inner.contentIndexStatusType[0] : inner.contentIndexStatusType[status]);
    }

    function formatValue(value) {
        if (value === undefined || value === null) {
            return Resources.Constants.NOT_AVAILABLE_DATA;
        }
        return String(value);
    }

    function trimReplicationStatusErrors($errors) {
        if (!$errors) {
            return;
        }
        $errors.each(function () {
            SW.APM.trimElementText({
                $element: $(this),
                moreHtml: '@{R=APM.BlackBox.Exchg.Strings;K=ReplicationStatusGridMoreButton;E=js}',
                handler: SW.APM.getDefaultRevealTrimmedHandler({
                    title: '@{R=APM.BlackBox.Exchg.Strings;K=ReplicationStatusMoreDetailTitle;E=js}'
                })
            });
        });
    }

    // publish methods & settings
    Resources.limitReplicationStatusError = 40;
    Resources.getReplicationStatusName = getReplicationStatusName;
    Resources.getReplicationStatusClass = getReplicationStatusClass;
    Resources.getReplicationStatusIcon = getReplicationStatusIcon;
    Resources.getCopyStatus = getCopyStatus;
    Resources.getContentIndexStatusType = getContentIndexStatusType;
    Resources.formatValue = formatValue;
    Resources.trimReplicationStatusErrors = trimReplicationStatusErrors;
});

APMjs.initGlobal('SW.APM.EXBB.Resources.DB', function (DB, APMjs) {
    'use strict';

    var linkedTemplate = '<a href="{0}">{1}</a>',
        linkedIconTemplate = '<a href="{0}"><img src="{1}"/></a>',
        linkedIconCaptionTemplate = '<a href="{0}" style="white-space: nowrap;"><img src="{1}"/> {2}</a>',
        Constants = APMjs.assertGlobal('SW.APM.EXBB.Resources.Constants'),
        $ = APMjs.assertQuery(),
        APM = APMjs.assertGlobal('SW.APM');

    function createPercentStatusBar(status, usedSpace) {
        var psb = new APM.Controls.PercentStatusBar();
        psb.BuildByStatus = true;
        psb.Status = status;
        psb.UsedSpacePercentage = usedSpace;
        return psb;
    }

    function getLink(redirectLink, text) {
        return redirectLink === '' || redirectLink === null ? text : String.format(linkedTemplate, redirectLink, text);
    }

    function getLinkedIcon(redirectLink, iconLink) {
        return String.format(linkedIconTemplate, redirectLink, iconLink);
    }

    function getLinkedIconCaption(redirectLink, iconLink, caption) {
        if (iconLink === null || iconLink === undefined || iconLink === '') {
            if (redirectLink === null || redirectLink === undefined || redirectLink === '') {
                return caption;
            }
            return String.format(linkedTemplate, redirectLink, caption);
        }
        return String.format(linkedIconCaptionTemplate, redirectLink, iconLink, caption);
    }

    function getAppOnNodeCaption(appLink, appIconLink, appName, nodeLink, nodeIconLink, nodeCaption) {
        if (DB.shortAppOnNodeCaption) {
            return String.format(linkedIconCaptionTemplate, appLink, appIconLink, nodeCaption);
        }
        return String.format(
            '@{R=APM.BlackBox.Exchg.Strings;K=ApplicationOnServer;E=js}',
            String.format(linkedIconCaptionTemplate, appLink, appIconLink, appName),
            String.format(linkedIconCaptionTemplate, nodeLink, nodeIconLink, nodeCaption)
        );
    }

    function getVolumePercentStatusBar(usedSpace, status) {
        var p = createPercentStatusBar(status, usedSpace);
        return p.BuildPercentStatusBar();
    }

    function getMailboxPercentStatusBar(usedSpace, status) {
        var p = createPercentStatusBar(status, usedSpace);
        p.Width = 68;
        return p.BuildPercentStatusBar();
    }

    function getDatabasePercentStatusBar(usedSpacePercentage, availableSpacePercentage, status) {
        var p = createPercentStatusBar(status, usedSpacePercentage);
        p.AvailableSpacePercentage = availableSpacePercentage;
        return p.BuildPercentStatusBar();
    }

    function getPercentageLabel(value, unlimited) {
        if ((typeof value === 'number' || (typeof value === 'string' && value !== '')) && (unlimited === undefined || typeof unlimited === 'boolean')) {
            value = Math.max(0, unlimited ? parseFloat(value) || 0 : Math.min(100, parseFloat(value) || 0));
            return String.format('@{R=APM.BlackBox.Exchg.Strings;K=DatabaseFileSize_PercentLabel;E=js}', value.toLocaleString());
        }
        return Constants.NOT_AVAILABLE_DATA;
    }

    //according to https://cp.solarwinds.com/pages/viewpage.action?pageId=34813202
    function getIndexStateCss(value, isActive) {
        if (isActive) {
/*
    2 - Crawling
    3 - Failed
    5 - FailedAndSuspended
    6 - Suspended
    8 - AutoSuspended
    10 - DiskUnavailable
*/
            switch (value) {
            case '3':
            case '5':
            case '10':
                return 'boldAndRed';
            case '2':
            case '6':
            case '8':
                return 'red';
            default:
                return '';
            }
        }
        switch (value) {
        case '3':
        case '5':
        case '2':
        case '6':
        case '8':
        case '10':
            return 'red';
        default:
            return '';
        }
    }

    //checks if db copy is mounted on it's prederred server
    //can be only used for active copies
    function isDbOnDifferentServer(masterType, mountedServerId, preferredServerId, serverName, preferredServer) {
        switch (masterType) {
        //DAG
        case 1:
            if (mountedServerId === preferredServerId) {
                if (!mountedServerId) {
                    return false;
                }
                return serverName !== preferredServer;
            }
            return true;
        //Server
        case 0:
            return false;
        default:
            return false;
        }
    }

    function getCriticalThresholdByName(uniqueId, thresholdName, appId, appitemiId) {
        var criticalLevel, data;
        data = {
            uniqueId: uniqueId,
            thresholdName: thresholdName,
            appId: appId,
            appitemiId: appitemiId
        };
        $.ajax({
            type: 'POST',
            url: '/Orion/APM/Services/ThresholdService.asmx/GetCriticalThresholdByName',
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: false,
            cache: true,
            success: function (result) {
                criticalLevel = result.d;
            },
            error: function () {
                criticalLevel = 0;
            }
        });
        return criticalLevel;
    }

    function getWarningThresholdByName(uniqueId, thresholdName, appId, appitemiId) {
        var warningLevel, data;
        data = {
            uniqueId: uniqueId,
            thresholdName: thresholdName,
            appId: appId,
            appitemiId: appitemiId
        };
        $.ajax({
            type: 'POST',
            url: '/Orion/APM/Services/ThresholdService.asmx/GetWarningThresholdByName',
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: false,
            cache: true,
            success: function (result) {
                warningLevel = result.d;
            },
            error: function () {
                warningLevel = 0;
            }
        });
        return warningLevel;
    }

    function utcToLocalTime(dateIn) {
        var date, newDate, offset, hours;
        date = new Date(dateIn);
        offset = date.getTimezoneOffset() / 60;
        hours = date.getHours();
        newDate = new Date(date.getTime());
        newDate.setHours(hours - offset);
        return newDate;
    }

    function getLimitationCss(value, warningThreshold, criticalThreshold) {
        if (criticalThreshold !== null && criticalThreshold !== '') {
            if (value > criticalThreshold) {
                return 'boldAndRed';
            }
        }
        if (warningThreshold !== null && warningThreshold !== '') {
            if (value > warningThreshold) {
                return 'red';
            }
        }
        return '';
    }

    function formatDateWithRegoinalSettings(date, format, regionalSettings) {
        var result,
            picker = APMjs.assertGlobal('$.datepicker');
        picker.setDefaults(regionalSettings);
        result = picker.formatDate(format, date);
        picker.setDefaults(picker.regional['']);
        return result;
    }

    function combineStatus() {
        var statuses, result = 1;
        statuses = APMjs.linqSelect(arguments, function (s) {
            return Number(s);
        });
        APMjs.linqAny([14, 3], function (value) {
            return APMjs.linqAny(statuses, function (s) {
                if (s === value) {
                    result = value;
                    return true;
                }
            });
        });
        return result;
    }

    // publish methods
    DB.getLink = getLink;
    DB.getLinkedIcon = getLinkedIcon;
    DB.getLinkedIconCaption = getLinkedIconCaption;
    DB.getAppOnNodeCaption = getAppOnNodeCaption;
    DB.getVolumePercentStatusBar = getVolumePercentStatusBar;
    DB.getMailboxPercentStatusBar = getMailboxPercentStatusBar;
    DB.getDatabasePercentStatusBar = getDatabasePercentStatusBar;
    DB.getPercentageLabel = getPercentageLabel;
    DB.getIndexStateCss = getIndexStateCss;
    DB.isDbOnDifferentServer = isDbOnDifferentServer;
    DB.getCriticalThresholdByName = getCriticalThresholdByName;
    DB.getWarningThresholdByName = getWarningThresholdByName;
    DB.utcToLocalTime = utcToLocalTime;
    DB.getLimitationCss = getLimitationCss;
    DB.formatDateWithRegoinalSettings = formatDateWithRegoinalSettings;
    DB.combineStatus = combineStatus;
});
