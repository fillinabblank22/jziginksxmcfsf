﻿using System;
using System.Reflection;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_ExchangeBlackBox_DatabaseDetails : ExchangeOrionView, IDatabaseProvider, ITimePeriodProvider
{
    protected override void OnInit(EventArgs e)
    {
        resHost.ExchangeDatabase = ExchangeDatabase;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // title
        title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        title.ViewSubTitle = ExchangeDatabase.Name;

        // status icon
        title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.Database, ExchangeDatabase.StatusId);

        // customize
        if (Profile.AllowCustomize)
            topRightLinks.CustomizeViewHref = CustomizeViewHref;

        // edit
        if (ApmRoleAccessor.AllowAdmin && ExchangeApplication != null)
        {
            topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationItemPageUrl(ExchangeApplication.Id, ExchangeDatabase.DatabaseCopy.Id);
            topRightLinks.EditNetObjectText = APM_ExBBContent.EditApplicationItemTitle;
        }

        // help
        topRightLinks.HelpUrlFragment = "SAMAGAppInsightForExchangeDBView";

        ClientScript.RegisterClientScriptBlock(GetType(), GetType().ToString(),
                    "SW.APM.EXBB.Resources.DB.shortAppOnNodeCaption = true", true);
    }

    public override string ViewKey
    {
        get { return "Exchange BlackBox Database Details"; }
    }

    public override string ViewType
    {
        get { return "Exchange BlackBox Database Details"; }
    }

    #region Interface members

    public Database ExchangeDatabase
    {
        get { return (Database) NetObject; }
    }

    public override ExchangeApplication ExchangeApplication
    {
        get { return ExchangeDatabase.MountedServer; }
    }

    public int ApplicationItemId
    {
        get { return ExchangeDatabase.DatabaseCopy.Id; }
    }

    #endregion

    #region ITimePeriodProvider

    public DateTime? TimePeriodStartDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDate().Value; }
    }

    public DateTime? TimePeriodEndDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDate().GetValueOrDefault(DateTime.Now); }
    }

    public DateTime? TimePeriodStartDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDateUTC().Value; }
    }

    public DateTime? TimePeriodEndDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDateUTC().GetValueOrDefault(DateTime.UtcNow); }
    }

    public int GetRelativeTimePeriodMinutes()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutes();
    }

    public int GetRelativeTimePeriodMinutesUTC()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutesUTC();
    }

    public Boolean TimePeriodIsSetByUser()
    {
        return TimePeriodPicker.PeriodFilter.IsSetInQueryString(Request);
    }

    #endregion

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
    #endregion

    public override void SelectView()
    {
        try
        {
            base.SelectView();
        }
        catch (TargetInvocationException e)
        {
            if (e.InnerException != null)
                //rethrow inner exception but keep also original call stack
                //we want to show on UI the inner exception - like "You have no permission to open the page"
                System.Runtime.ExceptionServices.ExceptionDispatchInfo.Capture(e.InnerException).Throw();
            throw;
        }
    }
}
