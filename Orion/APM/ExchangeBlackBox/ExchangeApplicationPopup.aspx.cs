﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_ExchangeBlackBox_ExchangeApplicationPopup : PopupPage<ExchangeApplication>
{
    protected const int TopComponentsCount = 5;

    private readonly Log log = new Log();

    protected ExchangeApplication App
    {
        get { return NetObjectData; }
    }

    protected string DagValue
    {
        get
        {
            if (this.App.DAG == null)
            {
                return string.Empty;
            }
            if (this.App.Domain == null)
            {
                return this.App.DAG.Name;
            }
            return string.Format(APM_ExBBContent.ExchangeServer_DAGFormat, App.DAG.Name, App.Domain.Name);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var popup = (Orion_APM_Popup) this.Master;

        if (popup == null)
        {
            throw new ArgumentNullException("Master");
        }

        popup.PopupTitle = Resources.APMWebContent.APMWEBDATA_TM0_49;
        popup.StatusCssClass = StatusCssClass;

        this.AppStatusIcon.StatusValue = this.App.Status;
        this.AppStatusIcon.CustomApplicationType = this.App.CustomType;
        this.ServerStatusIcon.StatusValue = this.App.NPMNode.Status;

        this.CPULoad.Value = this.App.NPMNode.CPULoad;
        this.phCpuLoad.Visible = (this.CPULoad.Value != -2);
        SetupBarPercentage(
            this.CPULoadBar,
            this.App.NPMNode.CPULoad,
            100,
            Thresholds.CPULoadWarning.SettingValue,
            Thresholds.CPULoadError.SettingValue);

        this.MemoryUsed.Value = (short) this.App.NPMNode.PercentMemoryUsed;
        this.phMemoryUsed.Visible = (this.MemoryUsed.Value != -2);
        SetupBarPercentage(
            this.MemoryUsedBar,
            this.App.NPMNode.PercentMemoryUsed,
            100,
            Thresholds.PercentMemoryWarning.SettingValue,
            Thresholds.PercentMemoryError.SettingValue);

        this.componentList.BindComponents(
            this.App.ComponentsWithProblems,
            TopComponentsCount,
            GetStatusImagePath
            );
    }

    #region Overriden methods

    protected override ApmStatus GetStatus()
    {
        return NetObjectData.Status;
    }

    protected override ExchangeApplication CreateNetObjectData(string netObjectId)
    {
        var netObject = CreateNetObject();
        var result = netObject as ExchangeApplication ?? (netObject is ApmMonitor ? (ExchangeApplication)((ApmMonitor)netObject).Application : null);

        if (result == null)
            this.log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render application popup, because cannot get application info from netObject with netObjectID '{0}'", netObjectId);
        return result;
    }
    #endregion

    protected string GetStatusImagePath(Component component)
    {
        var status = new ApmStatus(component.Status);
        return status.ToString("smallimgpath", App.CustomType);
    }
}
