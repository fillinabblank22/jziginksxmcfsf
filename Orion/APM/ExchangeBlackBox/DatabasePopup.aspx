﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DatabasePopup.aspx.cs" Inherits="Orion_APM_ExchangeBlackBox_DatabasePopup" MasterPageFile="~/Orion/APM/Popup.master"%>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/APM/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="apm" TagName="PopupComponentList" Src="~/Orion/APM/Controls/PopupComponentList.ascx" %>
<asp:Content runat="server" ContentPlaceHolderID="NetObjectTipBody">
    <p class="StatusDescription"><%= GetDbCopyParentsMarkup(this.DatabaseCopy.ExchangeApplication.StatusId, this.DatabaseCopy.ExchangeApplication.Name, this.DatabaseCopy.ExchangeApplication.Node.Status, this.DatabaseCopy.ExchangeApplication.Node.Name) %></p>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_PerfStatus %></th>
            <td>&nbsp;</td>
            <td style="<%= GetStatusCss(DatabaseCopy.Model.Availability)%>"><%= DatabaseCopy.Model.LocalizedAvailability %></td>
        </tr>
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_CopyStatus %></th>
            <td>&nbsp;</td>
            <td><%= DatabaseCopy.Model.LocalizedCopyStatus %></td>
        </tr>
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_ContentIndexState %></th>
            <td>&nbsp;</td>
            <td style="<%= GetContentIndexStateCss(DatabaseCopy.Model.ContentIndexState, DatabaseCopy.Model.IsActive)%>"><%= DatabaseCopy.Model.LocalizedContentIndexState %></td>
        </tr>
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_DatabaseSize %></th>
            <td>&nbsp;</td>
            <td><span style="<%=  DatabaseCopy.Model.AvailableVolumeSpace.HasValue ? GetVolumeSpacePercentageCss(DatabaseCopy.Model.Size, DatabaseCopy.Model.AvailableVolumeSpace) : "" %>"><%= GetSize(DatabaseCopy.Model.Size) %></span>
                <span><%= DbWhiteSpacePercentageLabel(DatabaseCopy.Model.AvailableWhiteSpace, DatabaseCopy.Model.Size) %></span>
                <div><orion:PercentStatusBar ID="VolumePercentBar" Show="True" runat="server"/></div>
            </td>
        </tr>
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_AvailableSpace %></th>
            <td>&nbsp;</td>
            <td style="<%= DatabaseCopy.Model.AvailableVolumeSpace.HasValue ? GetVolumeSpacePercentageCss(DatabaseCopy.Model.Size, DatabaseCopy.Model.AvailableVolumeSpace) : "" %>"><%= DatabaseCopy.Model.AvailableVolumeSpace.HasValue ? GetSize(DatabaseCopy.Model.AvailableVolumeSpace) : Resources.APM_ExBBContent.UnknownValue%></td>
        </tr>
        <asp:PlaceHolder ID="phActiveDb" runat="server">
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_ActiveCopy %></th>
            <td>&nbsp;</td>
            <td>
                <%= GetDbActiveCopyParentsMarkup(this.ActiveDatabaseCopy.Availability.GetValueOrDefault(), ActiveDatabaseCopy.DatabaseName, this.ActiveDatabaseCopyApplication.StatusId, ActiveDatabaseCopyApplication.Name, this.ActiveDatabaseCopyApplication.NodeStatusId, ActiveDatabaseCopyApplication.NodeName) %>
            </td>
        </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phMailboxes" runat="server">
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_Mailboxes %></th>
            <td>&nbsp;</td>
            <td><span><%= DatabaseCopy.DatabaseModel.TotalMailboxes %></span></td>
        </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phAvgMailbox" runat="server">
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_AveMailboxSize %></th>
            <td>&nbsp;</td>
            <td><span><%= GetSize(DatabaseCopy.DatabaseModel.AvgMailBoxSize) %></span></td>
        </tr>
        </asp:PlaceHolder>
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_DatabaseCopies %></th>
            <td>&nbsp;</td>
            <td><span><%= this.DatabaseCopiesCount %></span></td>
        </tr>
        <tr>
            <th><%= Resources.APM_ExBBContent.DatabasePopup_LastfullBackup %></th>
            <td>&nbsp;</td>
            <td>
                <span style="<%= GetLastFullBackupCss(GetLastFullBackupDaysPassed(DatabaseCopy.DatabaseModel.LastFullBackup)) %>"><%= GetFriendlyLastFullBackup(DatabaseCopy.DatabaseModel.LastFullBackup) %></span>
                <span><%= this.DatabaseCopy.DatabaseModel.LastFullBackup != null ? this.DatabaseCopy.DatabaseModel.LastFullBackup.Value.ToString("f") : Resources.APM_ExBBContent.NeverLabel %></span>
            </td>
        </tr>
    </table>
    <apm:PopupComponentList ID="componentList" Title="<%# Resources.APM_ExBBContent.DatabasePopup_PerformanceProblems %>" runat="server"/>
</asp:Content>
