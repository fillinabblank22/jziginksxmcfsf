﻿<%@ Page Language="C#" MasterPageFile="..\ApmView.master" AutoEventWireup="true" CodeFile="ExchangeStatisticDetails.aspx.cs" Inherits="Orion_APM_ExchangeBlackBox_ExchangeStatisticDetails" %>
<%@ Register TagPrefix="exbb" Namespace="SolarWinds.APM.BlackBox.Exchg.Web.UI" Assembly="SolarWinds.APM.BlackBox.Exchg.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="apm" TagName="BreadcrumbBar" Src="~/Orion/APM/Controls/BreadcrumbBar.ascx" %>
<%@ Register TagPrefix="apm" TagName="TopRightLinks" Src="~/Orion/APM/Controls/Views/TopRightPageLinks.ascx" %>
<%@ Register TagPrefix="apm" TagName="ViewTitle" Src="~/Orion/APM/Controls/Views/ViewTitle.ascx" %>

<asp:Content ID="cntLinks" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <apm:TopRightLinks runat="server" ID="topRightLinks" />
</asp:Content>

<asp:Content ID="cntTitle" ContentPlaceHolderID="ApmPageTitle" Runat="Server">
    <apm:BreadcrumbBar ID="breadcrumbs" Provider="ExchangeSiteMapProvider" SiteMapFilePath="\Orion\APM\ExchangeBlackBox\ExchangeBlackBox.sitemap" runat="server" />   
    <apm:ViewTitle ID="title" runat="server" />
</asp:Content>

<asp:Content ID="cntMain" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<exbb:ExchangeStatisticResourceHost ID="resHost" runat="server">
		<orion:ResourceContainer ID="resContainer" runat="server" />
	</exbb:ExchangeStatisticResourceHost>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="OutsideFormPlaceHolder" ID="contentNote">
    <orion:Include ID="I1" File="APM/APM.css" runat="server" />
    <div class="contentNote">
        <%= Resources.APM_ExBBContent.ExpertKnowledgeAttribution %>
    </div>
</asp:Content>