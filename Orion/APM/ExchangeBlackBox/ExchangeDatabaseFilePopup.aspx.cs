﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;

public partial class Orion_APM_ExchangeBlackBox_ExchangeDatabaseFilePopup : PopupPage<DatabaseFile>
{
    private const string template = @"border-{1}:solid 1px; text-align: {1}; margin-{1}:{0}%; padding-{1}: 4px; font-size: 11px; {2}";

    private const string greenUrl = @"/Orion/APM/Images/TooltipBar.Green.gif";

    private const string yellowUrl = @"/Orion/APM/Images/TooltipBar.Yellow.gif";

    private const string redUrl = @"/Orion/APM/Images/TooltipBar.Red.gif";

    private Status compositeStatus;
    
    private readonly Log log = new Log();

    protected float WhiteSpacePercentage { get; set; }

    protected float UsedSpacePercentage { get; set; }
    
    protected long Size { get; set; }

    protected long AvailableWhiteSpace { get; set; }

    protected long VolumeAvailableSpace { get; set; }

    protected string AvailableSpaceStyle { get; set; }

    protected string UsedSpaceImgUrl { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_APM_Popup)Master).PopupTitle = String.Format(
            APMWebContent.APMWEBDATA_TM0_58,
            NetObjectData.Name,
            NetObjectData.DatabaseCopy.ExchangeApplication.Server.ServerIdentity);
        this.AvailableWhiteSpace = GetValidSize(NetObjectData.AvailableWhiteSpace);
        this.VolumeAvailableSpace = GetValidSize(NetObjectData.VolumeAvailableSpace);
        this.Size = GetValidSize(NetObjectData.Size);
        
        this.UsedSpacePercentage = DatabaseFileSizeCalculations.GetDbFileUsage(
    Size,
    AvailableWhiteSpace,
    VolumeAvailableSpace);

        this.WhiteSpacePercentage =
            DatabaseFileSizeCalculations.GetDbFileWhiteSpaceUsage(
                 Size,
    AvailableWhiteSpace,
    VolumeAvailableSpace);

        AvailableSpaceStyle = InvariantString.Format(
            template,
            this.UsedSpacePercentage <= 50 ? this.UsedSpacePercentage : 100 - this.UsedSpacePercentage - this.WhiteSpacePercentage,
            this.UsedSpacePercentage <= 50 ? "left" : "right",
            Convert.ToInt32(this.WhiteSpacePercentage) == 0 && this.AvailableWhiteSpace== 0 ? "display:none;" : string.Empty);
        
        compositeStatus = NetObjectData.StatusDbUsage == Status.Critical
                          || NetObjectData.StatusDbWhitespaceSize == Status.Critical
            ? Status.Critical
            : NetObjectData.StatusDbUsage == Status.Warning || NetObjectData.StatusDbWhitespaceSize == Status.Warning
                ? Status.Warning
                : Status.Available;

        UsedSpaceImgUrl = GetUsedSpaceImage();
        ((Orion_APM_Popup) Master).StatusCssClass = StatusCssClass;
    }

    protected override DatabaseFile CreateNetObjectData(string netObjectId)
    {
        var netObject = CreateNetObject();
        var result = netObject as DatabaseFile;

        if (result == null)
            this.log.ErrorFormat(CultureInfo.InvariantCulture, "Cannot render database file popup, because cannot get database file info from netObject with netObjectID '{0}'", netObjectId);
        return result;
    }

    protected override ApmStatus GetStatus()
    {
        return new ApmStatus(NetObjectData.Status);
    }

    private static long GetValidSize(long? size)
    {
        if (!size.HasValue || size.Value < 0)
        {
            return 0;
        }
        return size.Value;
    }

    private string GetUsedSpaceImage()
    {
        if (compositeStatus == Status.Critical)
            return redUrl;
        if (compositeStatus == Status.Warning)
            return yellowUrl;
        return greenUrl;
    }
}