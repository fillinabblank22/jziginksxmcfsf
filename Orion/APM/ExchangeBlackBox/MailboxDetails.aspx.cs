﻿using System;
using System.Web;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.UI;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;

public partial class Orion_APM_ExchangeBlackBox_MailboxDetails : ExchangeOrionView, IMailboxProvider
{
    private readonly Log log = new Log();

    protected override void OnInit(EventArgs e)
    {
        this.resHost.ExchangeMailbox = this.ExchangeMailbox;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        this.title.ViewSubTitle = this.ExchangeMailbox.Name;

        this.title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.Mailbox, 
            this.ExchangeMailbox.MailboxModel.Status.GetValueOrDefault());

        if (this.Profile.AllowCustomize)
            this.topRightLinks.CustomizeViewHref = this.CustomizeViewHref;

        this.topRightLinks.HelpUrlFragment = "SAMAGAppInsightForExchangeMailboxDetailsView";
    }

    public override string ViewKey
    {
        get { return "Exchange BlackBox Mailbox Details"; }
    }

    public override string ViewType
    {
        get { return "Exchange BlackBox Mailbox Details"; }
    }

    public Mailbox ExchangeMailbox
    {
        get { return (Mailbox)this.NetObject; }
    }

    public override ExchangeApplication ExchangeApplication
    {
        get { return this.ExchangeMailbox.MailboxDatabase.MountedServer; }
    }

    public int ApplicationItemId
    {
        get { return this.ExchangeMailbox.MailboxDatabase.DatabaseCopy.Id; }
    }

    public override void SelectView()
    {
        try
        {
            base.SelectView();
        }
        catch (Exception xcp)
        {
            this.log.Error(InvariantString.Format("Mailbox[{0}] not found", this.NetObjectIDForASP), xcp);
            HttpContext.Current.Server.Transfer("~/Orion/View.aspx", false);
        }
    }

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}
