﻿<%@ Page Language="C#" MasterPageFile="..\ApmView.master" AutoEventWireup="true" CodeFile="ExchangeMailboxQuotaDetails.aspx.cs" Inherits="Orion_APM_ExchangeBlackBox_ExchangeMailboxQuotaDetails" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="apm" TagName="BreadcrumbBar" Src="~/Orion/APM/Controls/BreadcrumbBar.ascx" %>
<%@ Register TagPrefix="apm" TagName="TopRightLinks" Src="~/Orion/APM/Controls/Views/TopRightPageLinks.ascx" %>
<%@ Register TagPrefix="apm" TagName="ViewTitle" Src="~/Orion/APM/Controls/Views/ViewTitle.ascx" %>
<%@ Register TagPrefix="exchgbb" Namespace="SolarWinds.APM.BlackBox.Exchg.Web.UI" Assembly="SolarWinds.APM.BlackBox.Exchg.Web" %>

<asp:Content ID="cntLinks" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <apm:TopRightLinks runat="server" ID="topRightLinks" />
</asp:Content>

<asp:Content ID="cntTitle" ContentPlaceHolderID="ApmPageTitle" Runat="Server">
    <apm:BreadcrumbBar ID="Breadcrumbs" runat="server" Provider="ExchangeBlackBoxSitemapProvider" SiteMapFilePath="\Orion\APM\ExchangeBlackBox\ExchangeBlackBox.sitemap" />
    <apm:ViewTitle runat="server" ID="title" />
</asp:Content>

<asp:Content ID="cntMain" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<exchgbb:MailboxResourceHost ID="resHost" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</exchgbb:MailboxResourceHost>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="OutsideFormPlaceHolder" ID="contentNote">
    <orion:Include ID="I1" File="APM/APM.css" runat="server" />
    <div class="contentNote">
        <%= Resources.APM_ExBBContent.ExpertKnowledgeAttribution %>
    </div>
</asp:Content>