﻿using System;
using System.Globalization;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_APM_ExchangeBlackBox_ExchangeStatisticDetails : ExchangeOrionView, IExchangeStatisticProvider, IExchangeApplicationProvider
{
	public override string ViewKey
	{
		get { return "Exchange BlackBox Statistic Details"; }
	}

	public override string ViewType
	{
		get { return "Exchange BlackBox Statistic Details"; }
	}

	public ExchangeStatistic ExchangeBlackBoxStatistic
	{
		get { return (ExchangeStatistic)NetObject; }
	}

	public override ExchangeApplication ExchangeApplication
	{
		get { return ExchangeBlackBoxStatistic.ExchangeApplication; }
	}

	protected override void OnInit(EventArgs e)
	{
		resHost.LoadFromRequest();

		resContainer.DataSource = ViewInfo;
		resContainer.DataBind();

		// DateTime picker support
		//TimePeriodPicker.Visible = this.ViewInfo.ViewKey == "Exchange BlackBox Application Summary";

		base.OnInit(e);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
        title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        title.ViewSubTitle = ExchangeBlackBoxStatistic.Name;
        title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.Component, ExchangeBlackBoxStatistic.StatusId);

		topRightLinks.HelpUrlFragment = "SAMAGAppInsightForExchangeDetailsView";
		if (Profile.AllowCustomize)
		{
			topRightLinks.CustomizeViewHref = CustomizeViewHref;
		}
		if (ApmRoleAccessor.AllowAdmin)
		{
		    topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationPageUrl(ExchangeApplication.Id,
		        string.Format(CultureInfo.InvariantCulture, "selected={0}", ExchangeBlackBoxStatistic.Id));
			topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
		}
	}

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}