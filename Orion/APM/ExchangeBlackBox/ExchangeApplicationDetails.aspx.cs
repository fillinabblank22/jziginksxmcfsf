﻿using System.Collections.Generic;

using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.MonitorProviders;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web;
using System;

public partial class Orion_APM_ExchangeBlackBox_ExchangeApplicationDetails : ExchangeOrionView, IExchangeApplicationProvider, IEventLogMonitorProvider, ITimePeriodProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.resHost.ExchangeApplication = this.ExchangeApplication;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        // DateTime picker support
        this.TimePeriodPicker.Visible = this.ViewInfo.ViewKey == "Exchange BlackBox Application Summary";

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // title
        this.title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        this.title.ViewSubTitle = this.ExchangeApplication.Name;

        // status icon
        this.title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(SwisEntities.Application, this.ExchangeApplication.StatusId);

        // customize
        if (Profile.AllowCustomize)
            this.topRightLinks.CustomizeViewHref = CustomizeViewHref;

        // edit
        if (ApmRoleAccessor.AllowAdmin)
        {
            this.topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationPageUrl(this.ExchangeApplication.Id);
            this.topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
        }

        // help
        this.topRightLinks.HelpUrlFragment = "SAMAGAppInsightForExchangeDetailsView";
    }

    public override string ViewKey
    {
        get { return "Exchange BlackBox Application Summary"; }
    }

    public override string ViewType
    {
        get { return "Exchange BlackBox Application Details"; }
    }

    public override void SelectView()
    {
        base.SelectView();

        if (IsPreview)
        {
            Int32 viewId = Convert.ToInt32(Request.QueryString["ViewId"]);
            // FB#4117 - wrong application and view were selected by Core code for specified VievID
            var data = ApplicationDAL.GetAllApplicationsAsLazy(new Dictionary<string, object> { { "ViewId", viewId } });
            SolarWinds.APM.Common.Models.Application app = data.Count == 0 ? null : data[0];
            if (app == null)
            {
                // no application for current view - leave application as is and just change view
                ViewInfo vi = ViewManager.GetViewById(viewId);
                if (vi != null)
                {
                    this.ViewInfo = vi;
                }
                return;
            }
            var application = new ExchangeApplication(app);

            NetObject = application;
            Template = new ApplicationTemplateDAL().GetTemplateInfo(application.TemplateId);

            this.SelectViewByViewKey();
        }
    }

    public override ExchangeApplication ExchangeApplication
    {
        get { return (ExchangeApplication) this.NetObject; }
    }

    public ApmMonitor EventLogMonitor
    {
        get
        {
            var componentId = new ComponentDal().GetComponentIdByTemplateUid(ExchangeApplication.Id, ApmConstants.IDEventLog);
            if (componentId >= 0)
            {
                return new ApmMonitor(ApmMonitor.GetComponent(componentId));
            }
            return null;
        }
    }

    #region ITimePeriodProvider

    public DateTime? TimePeriodStartDate
    {
        get { return this.TimePeriodPicker.PeriodFilter.GetStartDate().Value; }
    }

    public DateTime? TimePeriodEndDate
    {
        get { return this.TimePeriodPicker.PeriodFilter.GetEndDate().GetValueOrDefault(DateTime.Now); }
    }

    public DateTime? TimePeriodStartDateUTC
    {
        get { return this.TimePeriodPicker.PeriodFilter.GetStartDateUTC().Value; }
    }

    public DateTime? TimePeriodEndDateUTC
    {
        get { return this.TimePeriodPicker.PeriodFilter.GetEndDateUTC().GetValueOrDefault(DateTime.UtcNow); }
    }

    public int GetRelativeTimePeriodMinutes()
    {
        return this.TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutes();
    }

    public int GetRelativeTimePeriodMinutesUTC()
    {
        return this.TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutesUTC();
    }

    public Boolean TimePeriodIsSetByUser()
    {
        return this.TimePeriodPicker.PeriodFilter.IsSetInQueryString(this.Request);
    }

    #endregion

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}
