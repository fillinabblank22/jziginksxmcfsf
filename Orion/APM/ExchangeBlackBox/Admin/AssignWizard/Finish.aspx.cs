﻿using System;
using SolarWinds.APM.BlackBox.Exchg.Web.UI;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web;

public partial class Orion_APM_ExchangeBlackBox_Admin_AssignWizard_Finish : WizardPage<AssignSmartExchgApplicationWorkflow>
{
    protected string TemplateName
    {
        get
        {
            return Workflow.SelectedTemplate.Name;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        finishedMessage.DataSource = Workflow.CreatedApplications;
        finishedMessage.DataBind();

        if (!IsPostBack)
            imgbDone.AddEnterHandler(0);
    }

    protected void OnDone(object sender, EventArgs e)
    {
        this.ResetSession();
        Response.Redirect("~/Orion/Apm/Summary.aspx", true);
    }
}