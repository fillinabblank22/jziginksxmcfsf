﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Finish.aspx.cs" Inherits="Orion_APM_ExchangeBlackBox_Admin_AssignWizard_Finish" 
    MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master" EnableViewState="false" 
    Title="Finish" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/APM/Controls/AssignComponentsFinished.ascx" TagPrefix="apm" TagName="AssignComponentsFinished" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/Styles/AssignWizard.css"/>
    
    <apm:AssignComponentsFinished runat="server" ID="finishedMessage" EnableEditLink="false" NetObjectType="<%# ExchangeConstants.ApplicationTemplateCustomType %>" />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbDone" runat="server" LocalizedText="Done" DisplayType="Primary" OnClick="OnDone"/>
    </div>   

</asp:Content>


