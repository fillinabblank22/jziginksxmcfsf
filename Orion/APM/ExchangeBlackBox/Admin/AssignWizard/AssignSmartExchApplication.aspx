﻿<%@ Page Title="Assign Application Monitor" Language="C#" MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master"
    AutoEventWireup="true" CodeFile="AssignSmartExchApplication.aspx.cs" Inherits="Orion_APM_ExchangeBlackBox_Admin_AssignWizard_AssignSmartExchApplication" %>

<%@ Import Namespace="SolarWinds.APM.Web" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="apm" TagName="ExchangeServerTips" Src="~/Orion/APM/ExchangeBlackBox/Controls/ExchangeServerTips.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectServerIp" Src="~/Orion/APM/Controls/SelectServerIp.ascx" %>
<%@ Register TagPrefix="apm" TagName="ImageTooltip" Src="~/Orion/APM/Controls/ImageTooltip.ascx" %>
<asp:content id="Content1" contentplaceholderid="wizardContentPlaceholder" runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include ID="w4" File="APM/js/jquery.validate.js" SpecificOrder="4" runat="server" />
    <orion:Include ID="Include2" runat="server" File="APM/ExchangeBlackBox/Styles/AssignWizard.css"/>
    <orion:Include ID="Include1" runat="server" File="Nodes/js/TimeoutHandling.js" />
    <orion:Include ID="Include3" runat="server" File="APM/ExchangeBlackBox/js/Resources.js" />
    
    <script type="text/javascript">
        Ext.ns('swipg');
        swipg = (function (global, module) {
            'use strict';

            var cnst, $ = global.$, $get = global.$get;
                
            cnst = {
                cidServerIP: '<%= serverIP.ClientID %>',
                cidCredentials: '<%= selectCredentials.ClientID %>',
                cidUrlPsExchg: '<%= powershellExchangeURL.ClientID %>',
                cidUrlPsWsman: '<%= powershellWindowsURL.ClientID %>',
                cidBusyPanel: '<%= TestInProgress.ClientID %>',
                credentialValidationGroupName: '<%= selectCredentials.ValidationGroupName %>',
                errNode: '<%= ControlHelper.EncodeJsString(Resources.APM_ExBBContent.APMWEBCODE_YP1_4c) %>',
                errExchange: '<%= ControlHelper.EncodeJsString(Resources.APM_ExBBContent.APMWEBCODE_YP1_4a) %>',
                errPowerShell: '<%= ControlHelper.EncodeJsString(Resources.APM_ExBBContent.APMWEBCODE_YP1_4b) %>'
            };

            function runCommandNext() {
                <%= Page.ClientScript.GetPostBackEventReference(this, "Next") %>;
            }

            function progressVisibility(show) {
                var $panel = $('#' + cnst.cidBusyPanel);
                if (show) {
                    $panel.show();
                } else {
                    $panel.hide();
                }
            }

            function onErrorInner(text) {
                progressVisibility(false);
                global.Ext.Msg.show({
                    title: 'Error',
                    msg: text,
                    icon: global.Ext.Msg.WARNING,
                    buttons: global.Ext.Msg.OK
                });
            }

            function onError(response) {
                var message = response.statusText, tmp;
                if (response.responseText) {
                    tmp = JSON.parse(response.responseText);
                    if (tmp && tmp.Message) {
                        message += "\n" + tmp.Message;
                    }
                }
                onErrorInner(message);
            }

            function onSuccessTestConn(dataResponse) {
                var action = runCommandNext, res;
                progressVisibility(false);
                res = dataResponse && dataResponse.d;
                if (!res) {
                    return onError(dataResponse);
                }
                if (!res.IsNode) {
                    return onErrorInner(cnst.errNode);
                }
                if (!res.IsExchange) {
                    return onErrorInner(cnst.errExchange);
                }
                if (!res.IsPowerShell) {
                    global.Ext.Msg.show({
                        title: '<%= ControlHelper.EncodeJsString(Resources.APM_ExBBContent.ExchangeNoPowershellWarningTitle) %>',
                        msg: '<%= ControlHelper.EncodeJsString(Resources.APM_ExBBContent.ExchangeNoPowershellWarningMessage) %>',
                        buttons: global.Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                action();
                            }
                        }
                    });
                } else {
                    global.Ext.MessageBox.hide();
                    action();
                }
            }

            function onSuccessTestInstance(dataResponse) {
                var action = testConnection, res;
                progressVisibility(false);
                res = dataResponse.d;
                if (res === undefined) {
                    return onError(dataResponse);
                }
                if (res === true) {
                    global.Ext.Msg.show({
                        msg: '<%= Resources.APM_ExBBContent.APMWEBCODE_YP1_11 %>',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.INFO
                    });
                    return false;
                }
                global.Ext.MessageBox.hide();
                return action();
            }

            function testConnection() {
                var data = {
                    nodeId: $get(cnst.cidServerIP + '_selectedNodeId').value,
                    ipAddress: $get(cnst.cidServerIP + '_targetServer').value,
                    credId: $get(cnst.cidCredentials + '_CredentialSetList').value,
                    credUser: $get(cnst.cidCredentials + '_UserName').value,
                    credPassword: $get(cnst.cidCredentials + '_Password').value,
                    urlPsExchg: $get(cnst.cidUrlPsExchg).value,
                    urlPsWsman: $get(cnst.cidUrlPsWsman).value
                };
                progressVisibility(true);
                $.ajax({
                    type: 'POST',
                    url: 'AssignSmartExchApplication.aspx/ConnectionWorks',
                    data: Ext.encode(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: onSuccessTestConn,
                    error: onError
                });
            };

            function testDuplicateInstance() {
                progressVisibility(true);
                var data = {
                    nodeId: $get(cnst.cidServerIP + '_selectedNodeId').value,
                    ipAddress: $get(cnst.cidServerIP + '_targetServer').value
                };
                $.ajax({
                    type: 'POST',
                    url: 'AssignSmartExchApplication.aspx/InstanceExists',
                    data: Ext.encode(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: onSuccessTestInstance,
                    error: onError
                });
            };

            function isClientValidAll() {
                return global.Page_ClientValidate('')
                    && ($get(cnst.cidCredentials + '_CredentialSetList').value !== '-1' || global.Page_ClientValidate(cnst.credentialValidationGroupName));
            }

            module = module || {};

            module.hideTestResults = function hideTestResults() {
                $('.testSuccessful').hide();
                $('.testFailed').hide();
            };

            module.nextClick = function nextClick() {
                if (global.IsDemoMode()) {
                    return global.DemoModeMessage();
                }
                this.hideTestResults();
                if (!isClientValidAll()) { return false; }
                testDuplicateInstance();
                return false;
            };

            return module;
        }(window));
    </script>

    <h2><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_0 %></h2>
    <p><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_1 %></p> <br />
    <p class="note">
        <%= Resources.APMWebContent.APMWEBDATA_VB1_242 %>
        <a id="addLink" href="/Orion/Nodes/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_243 %></a>
    </p>
        <div id="selectExchangeServerStep">
            <ul id="selectExchangeServer">
                <li><asp:ValidationSummary ID="PageValidationSummary" runat="server" /></li>
                <li>
                    <label><%=  Resources.APMWebContent.APMWEBDATA_AK1_117 %></label>
                    <apm:SelectServerIp ID="serverIP" runat="server" ValidateVmware="false" CssClass="apm_ExchangeTarget"  />
                </li>
                <li id="powershellWindowsURLContainer" style="margin-bottom: 5px;">
                    <label><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_9%></label>
                    <asp:TextBox runat="server" ID="powershellWindowsURL" CssClass="apm_WinRMUrls" />
                    <asp:RequiredFieldValidator ID="PowershellWindowsURLValidato" runat="server" ControlToValidate="powershellWindowsURL" ErrorMessage="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_3 %>">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="powershellWinUrlregexValidator" runat="server"  ErrorMessage="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_19 %>" ControlToValidate="powershellWindowsURL">*</asp:RegularExpressionValidator>                    
                    <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_6 %>" />
                </li>
                <li id="powershellExchangeURLContainer">
                    <label><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_8%></label>
                    <span>https://localhost</span>
                    <asp:TextBox runat="server" ID="powershellExchangeURL" CssClass="apm_ExchangeUrls" />
                    <asp:RequiredFieldValidator ID="PowershellExchangeURLValidator" runat="server" ControlToValidate="powershellExchangeURL" ErrorMessage="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_2 %>">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="powershellExchgangeUrlRegexValidator" runat="server"  ErrorMessage="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_18 %>" ControlToValidate="powershellExchangeURL">*</asp:RegularExpressionValidator>
                    <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_5 %>" />
                </li>
                <li id="exchangeServerLink" style="margin: 0;">
                     <a style="font-size: 11px; padding-left: 190px;color: #369;" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeHelpMeFindTheseURLSettings") %>" target="_blank">&#0187;<%=  Resources.APM_ExBBContent.APMWEBCODE_YP1_17 %></a> 
                </li>
                <li>
                    <label><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_10%></label>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional" class="credentialsContainer ie7addLayout">
                        <ContentTemplate>            
                            <table class="selectCreds">
                                <apm:SelectCredentials runat="server" ID="selectCredentials" ValidationGroupName="SelectCredentialsValidationGroup" AllowNodeWmiCredential="True" />
                                <tr><td colspan="2">
                                    <asp:ValidationSummary ID="CredentialsValidationSummary" runat="server" ValidationGroup="SelectCredentialsValidationGroup"/>
                                </td></tr>
                            </table>
                            <div class="testRow" style="padding-left: 190px; padding-top: 15px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SelectCredentialsValidationGroup"/>
                                            <orion:LocalizableButton runat="server" ID="TestButton" Text="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_7 %>" DisplayType="Secondary" OnClick="OnTest" OnClientClick="window.swipg.hideTestResults()" />
                                        </td>
                                        <td style="padding-left: 10px">
                                            <asp:UpdateProgress runat="server" ID="TestInProgress" DynamicLayout="true" DisplayAfter="0">
                                                <ProgressTemplate>
                                                    <img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources: APMWebContent, ApmWeb_TestingProgress %>" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                            <div runat="server" id="testSuccessful" class="testSuccessful" visible="false">
                                                <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_55 %>" />
                                            </div>
                                            <div runat="server" id="testFailedExchg" class="testFailed" visible="false">
                                                <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_4a %>" />
                                            </div>
                                            <div runat="server" id="testFailedPowershell" class="testFailed" visible="false">
                                                <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: APM_ExBBContent, APMWEBCODE_YP1_4b %>" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>  
                    </asp:UpdatePanel>
                </li>
            </ul>
            <div class="credentialTips"> <apm:ExchangeServerTips ID="ctrExchangeServerTips" BackgroundColor="#FFFDCC" BorderColor="#EACA7F" runat="server"/> </div>
        </div>
    <div style="clear: both;"></div>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APM_ExBBContent, AssignWizard_ApplicationMonitorButton %>" DisplayType="Primary" OnClientClick="return window.swipg.nextClick();" />
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false" />
    </div>

</asp:content>
