﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Common.Models;
using SolarWinds.APM.BlackBox.Exchg.Web.DAL;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.APM.BlackBox.Exchg.Web.UI;
using System.Text.RegularExpressions;

public partial class Orion_APM_ExchangeBlackBox_Admin_AssignWizard_AssignSmartExchApplication : WizardPage<AssignSmartExchgApplicationWorkflow>,
                                                                                                IPostBackEventHandler
{
    #region Fields and Properties

    private static readonly Log log = new Log();

    protected string TemplateName
    {
        get { return Workflow.SelectedTemplate.Name; }
    }

    private SelectNodeTreeItem _selectedNode;

    private SelectNodeTreeItem SelectedNode
    {
        get
        {
            if (_selectedNode == null)
            {
                var node = new SelectNodeTreeItem(serverIP.SelectedNode);
                var selectedCredSetId = selectCredentials.SelectedCredentialSetId;
                if (selectedCredSetId != ComponentBase.NewCredentialsId)
                {
                    node.CredentialId = selectedCredSetId;
                }

                _selectedNode = node;
            }

            return _selectedNode;
        }
    }

    #endregion

    #region Events handlers

    /// <summary>
    /// Triggered when user presses Test button.
    /// </summary>
    protected void OnTest(object sender, EventArgs e)
    {
        testSuccessful.Visible = false;
        testFailedExchg.Visible = false;
        testFailedPowershell.Visible = false;
        Validate(PageValidationSummary.ValidationGroup);
        if (!Page.IsValid)
        {
            MoveInvalidValidators(PageValidationSummary.ValidationGroup, ValidationSummary2.ValidationGroup);
        }
        else
        {
            Validate(selectCredentials.ValidationGroupName);
            if (selectCredentials.IsValid)
            {
                TestBlackBoxExchangeConnection();
            }
        }
    }

    private void MoveInvalidValidators(string sourceValidationGroup, string targetValidationGroup)
    {
        var invalidValidators = GetValidators(sourceValidationGroup)
            .OfType<IValidator>()
            .Where(v => !v.IsValid);
        foreach (var val in invalidValidators)
        {
            Page.Validators.Remove(val);
            AddErrorMessage(val.ErrorMessage, targetValidationGroup);
        }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(powershellExchangeURL.Text))
        {
            powershellExchangeURL.Text = ExtractEditablePartOfExchangeUrl(ExchangeConstants.DefaultPsUrlExchange);
        }

        CredentialsValidationSummary.Visible = false;
        if (string.IsNullOrEmpty(powershellWindowsURL.Text))
        {
            powershellWindowsURL.Text = ExchangeConstants.DefaultPsUrlWindows;
        }

        powershellExchgangeUrlRegexValidator.ValidationExpression = UrlParser.RegexStringUrlOnlyPath;
        powershellWinUrlregexValidator.ValidationExpression = UrlParser.RegexStringUrl;

        if (!IsPostBack)
        {
            if (Workflow.SelectedNodesInfo != null && Workflow.SelectedNodesInfo.Count > 0)
            {
                if (Workflow.SelectedNodesInfo.Count > 1)
                {
                    log.Debug("Found more than one nodes in workflow state, using just first, because the wizard can assign only one at a time, ignoring the rest.");
                }
                var nodeInfo = Workflow.SelectedNodesInfo[0];
                serverIP.RestoreSelectedNodeById(nodeInfo.Id);
            }
            selectCredentials.ReloadCredentialSets();
        }
        selectCredentials.ValidationSummaryParentClientID = CredentialsValidationSummary.ClientID;

        if (IsPostBack) return;
        var postData = Server.UrlDecode(Request.Form["postData"]);
        if (postData != null)
        {
            Workflow.HasStarted = true;

            var serializer = new JavaScriptSerializer();
            var templateId = serializer.Deserialize<int>(postData);
            Workflow.SelectedTemplate = new TemplateInfo(templateId);
        }
        else
        {
            Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
        }
        imgbNext.AddEnterHandler(0);
    }
    #endregion

    #region Protected methods

    protected TestConnectionResult TestBlackBoxExchangeConnection()
    {
        var res = GetTestBlackBoxExchangeConnectionResult();
        testSuccessful.Visible = res.TestPassedCompletely;
        testFailedExchg.Visible = !res.IsVersionPresent;
        testFailedPowershell.Visible = !res.IsPowershellPresent;
        selectCredentials.PreFillPasswords();
        return res;
    }

    /// <summary>
    /// Triggered when user presses Cancel button.
    /// </summary>
    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    /// <summary>
    /// Cancels the wizard.
    /// </summary>
    protected override void CancelWizard()
    {
        base.CancelWizard();
        Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx", true);
    }

    /// <summary>
    /// Processes entered data from current wizard page and switches to the next page.
    /// </summary>
    protected void OnNext()
    {
        Validate(PageValidationSummary.ValidationGroup);
        Validate(selectCredentials.ValidationGroupName);
        if (Page.IsValid && selectCredentials.IsValid)
        {
            var testResult = TestBlackBoxExchangeConnection();
            if (testResult.IsVersionPresent)
            {
                CreateApplication(testResult);
                GotoNextPage();
            }
            else
            {
                AddErrorMessage(Resources.APM_ExBBContent.APMWEBCODE_YP1_12, selectCredentials.ValidationGroupName);
            }
        }

    }

    #endregion

    #region Privete methods

    private static TestConnectionResult GetTestBlackBoxExchangeConnectionResult(int nodeId, ExchangeSettings settings, CredentialSet credentials, List<string> errors)
    {
        settings.TryWmiToGetVersion = true;
        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            try
            {
                var result = bl.TestBlackBoxConnection(nodeId, credentials, ApplicationSettings.DefaultForTest, ExchangeConstants.ApplicationTemplateCustomType, settings.ToXmlString());
                if (result != null)
                {
                    if (result.Result != null)
                    {
                        return TestConnectionResult.FromXmlString(result.Result);
                    }
                    if (errors != null)
                    {
                        foreach (var error in result.Errors)
                        {
                            errors.Add(error.Message);
                        }
                    }
                }
            }
            catch (Exception xcp)
            {
                log.Error(xcp);
                if (errors != null)
                {
                    errors.Add(Resources.APM_ExBBContent.APMWEBCODE_YP1_13);
                }
            }

            return new TestConnectionResult
                {
                    IsValidPsUrlExchange = false,
                    IsValidPsUrlWindows = false,
                    ExchangeVersion = null
                };
        }
    }

    /// <summary>
    /// Tests Exchg connection using filled values and returns true when the test was successful.
    /// </summary>
    private TestConnectionResult GetTestBlackBoxExchangeConnectionResult()
    {
        var errors = new List<string>();
        var result = GetTestBlackBoxExchangeConnectionResult(SelectedNode.Id, GetTestJobSettings(), selectCredentials.SelectedCredentialSet, errors);
        foreach (var error in errors)
        {
            AddErrorMessage(error, selectCredentials.ValidationGroupName);
        }
        return result;
    }

    /// <summary>
    /// Adds error message within validator messages
    /// </summary>
    private void AddErrorMessage(string message, string validationGroupName)
    {
        var err = new CustomValidator {ValidationGroup = validationGroupName, IsValid = false, ErrorMessage = message};
        Page.Validators.Add(err);
    }

    /// <summary>
    /// Creates Exchg BB application after the connection test using filled values.
    /// </summary>
    private void CreateApplication(TestConnectionResult testResult)
    {
        Workflow.SelectedNodesInfo = new List<SelectNodeTreeItem> {SelectedNode};

        if (selectCredentials.SelectedCredentialSetId == ComponentBase.NewCredentialsId)
        {
            Workflow.UseCredentials(selectCredentials.SelectedCredentialSet);
        }
        else if (selectCredentials.SelectedCredentialSetId == ComponentBase.NodeWmiCredentialsId)
        {
            Workflow.InheritCredentials();
        }
        else
        {
            Workflow.UseCredentials(new List<SelectNodeTreeItem> {SelectedNode});
        }

        Workflow.ServerIdentity = !string.IsNullOrWhiteSpace(testResult.ComputerName) ? testResult.ComputerName : serverIP.SelectedNode.Dns;

        var settings = GetTestJobSettings();
        settings.ExchangeConfigurationDiscoveryResult = testResult.IsPowershellPresent ? "PS" : "WMI";
        settings.WindowsServices = testResult.RunningWindowsServices;
        settings.ServerVersion = testResult.ExchangeVersion;
        Workflow.ExchangeSettings = settings;
        Workflow.CreateApplications();
    }

    /// <summary>
    /// Provides exchange settings info from UI
    /// </summary>
    private ExchangeSettings GetTestJobSettings()
    {
        return new ExchangeSettings
            {
                ServerIdentity = serverIP.SelectedNode.Dns,
                PsUrlExchangeValue = MakeFinalExchangeUrl(powershellExchangeURL.Text),
                PsUrlWindowsValue = powershellWindowsURL.Text,
                NodeIpAddress = serverIP.SelectedNode.IpAddress,
                TryWmiToGetVersion = true
            };
    }

    private string ExtractEditablePartOfExchangeUrl(string url)
    {
        return Regex.Replace(url, @"https:\/\/[^\/]+", "", RegexOptions.IgnoreCase);
    }

    private string MakeFinalExchangeUrl(string urlPart)
    {
        return "https://localhost" + urlPart;
    }

    #endregion

    #region IPostBackEventHandler implementation

    /// <summary>
    /// Enables a server control to process an event raised when a form is posted to the server.
    /// </summary>
    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Next":
                OnNext();
                break;
        }
    }

    #endregion

    #region Web methods

    //private static readonly JavaScriptSerializer jser = new JavaScriptSerializer();

    public class ResultConnectionTest
    {
        public bool IsNode { get; set; }
        public bool IsExchange { get; set; }
        public bool IsPowerShell { get; set; }
        public string[] Errors { get; set; }
    }

    /// <summary>
    /// Check whether Exchange BB application already assigned to specified node.
    /// </summary>
    [WebMethod(EnableSession = true)]
    public static bool InstanceExists(int nodeId, string ipAddress)
    {
        var node = GetNode(nodeId, ipAddress);
        return (node != null)
               && ServiceLocatorForWeb.GetServiceForWeb<IExchangeApplicationDal>().ApplicationExists(node.Id);
    }

    [WebMethod(EnableSession = true)]
    public static ResultConnectionTest ConnectionWorks(int nodeId, string ipAddress, int credId, string credUser, string credPassword, string urlPsExchg, string urlPsWsman)
    {
        var result = new ResultConnectionTest
            {
                IsNode = false,
                IsExchange = false,
                IsPowerShell = false
            };

        Node node = GetNode(nodeId, ipAddress);

        if (node == null)
        {
            return result;
        }

        result.IsNode = true;

        var errors = new List<string>();
        
        var credentials = new CredentialSet
            {
                Id = credId,
                UserName = credUser,
                Password = credPassword
            };

        var appSettings = new ExchangeSettings
            {
                NodeDnsName = node.Dns,
                NodeIpAddress = node.IpAddress,
                PsUrlExchangeValue = urlPsExchg,
                PsUrlWindowsValue = urlPsWsman
            };

        var res = GetTestBlackBoxExchangeConnectionResult(node.Id, appSettings, credentials, errors);
        result.IsExchange = res.IsVersionPresent;
        result.IsPowerShell = res.IsPowershellPresent;
        result.Errors = errors.ToArray();
        return result;
    }

    #endregion
}
