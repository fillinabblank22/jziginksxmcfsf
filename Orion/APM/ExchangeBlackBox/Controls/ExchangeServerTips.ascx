﻿<%@ Control Language="C#" ClassName="ExchangeServerTips" %>

<script runat="server">	    
	public string BackgroundColor
	{
		get; set;
	}
    
	public string BorderColor
	{
		get; set;
	}

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        
        if (!string.IsNullOrEmpty(this.BackgroundColor) || !string.IsNullOrEmpty(this.BorderColor))
            this.credentialsContainer.Attributes.Add("style", string.Format("background-color:{0}; border-color:{1};", this.BackgroundColor, this.BorderColor));
    }
</script>

<orion:Include runat="server" File="APM/APM.css" />

<div class="CredentialTipsInstructions" runat="server" id="credentialsContainer">
    <h2><b><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_16 %></b></h2>
        <label><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_14 %>
        </label> <br/>
		<label><a style="color: #369;" href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeManuallyConfigureExchangeServer") %>" target="_blank">&#0187;<%= Resources.APM_ExBBContent.APMWEBCODE_YP1_15 %></a></label>
</div>