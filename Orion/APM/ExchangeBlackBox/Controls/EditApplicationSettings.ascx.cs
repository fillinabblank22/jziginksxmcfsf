﻿using System;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;

public partial class Orion_APM_ExchangeBlackBox_Controls_EditApplicationSettings : System.Web.UI.UserControl
{
    protected string AgentPollingMethodName
    {
        get
        {
            return EnumHelper.GetLocalizedDescription(PollingMethod.Agent);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.exchangeServerIdentityLabel.Text = Resources.APM_ExBBContent.EditApplicationSettings_ServerIdentity;
        this.psUrlExchangeLabel.Text = Resources.APM_ExBBContent.APMWEBCODE_YP1_8;
        this.psUrlWindowsLabel.Text = Resources.APM_ExBBContent.APMWEBCODE_YP1_9;
    }
}
