﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_AssetInventory_Controls_EditDatabaseResourceControl : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int selectedIndex;
            if (!String.IsNullOrEmpty(Resource.Properties["ShowDatabases"]) && 
                Int32.TryParse(Resource.Properties["ShowDatabases"], out selectedIndex))
            {
                ShowDatabasesDropDown.SelectedIndex = selectedIndex;
            }
            else
            {
                ShowDatabasesDropDown.SelectedIndex = 0;
            }
        }

        ShowDatabasesDropDown.Attributes.Add("onchange",
            String.Format("SaveData('ShowDatabases', document.getElementById('{0}').value);",
                ShowDatabasesDropDown.ClientID));
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();

            properties["ShowDatabases"] = ShowDatabasesDropDown.SelectedIndex;

            return properties;
        }
    }
}