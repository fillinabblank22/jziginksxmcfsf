﻿using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_APM_ExchangeBlackBox_Controls_Management_DatabaseTasksPlugin : UserControl, IManagementResourcePlugin
{
    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private const string LogMessageTemplate = "Creating management task '{0}' in section '{1}' for NetObject {2}";

    private const string ImgSrcTemplate = "<img src='{0}' class='apmNodeManagementTask' />&nbsp;{1}";

    public IEnumerable<ManagementTaskItem> GetManagementTasks(ApmBaseResource resource, NetObject netObject, string returnUrl)
    {
        var dbProvider = resource.GetInterfaceInstance<IDatabaseProvider>();
        Database db = (dbProvider != null) ? dbProvider.ExchangeDatabase : null;
        if (db == null || !ApmRoleAccessor.AllowAdmin || db.MountedServer == null || db.MountedServer.Status == null)
        {
            yield break;
        }

        string sectionName = Resources.APM_ExBBContent.Management_Database;
        ExchangeApplication app = db.MountedServer;


        log.DebugFormat(LogMessageTemplate, "Edit", sectionName, db.NetObjectID);
        yield return new ManagementTaskItem
        {
            Section = sectionName,
            LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, "/Orion/Nodes/images/icons/icon_edit.gif", Resources.APMWebContent.Management_Edit),
            LinkUrl = ApmMasterPage.GetEditApplicationItemPageUrl(app.Id, db.DatabaseCopy.Id)
        };
    }
}
