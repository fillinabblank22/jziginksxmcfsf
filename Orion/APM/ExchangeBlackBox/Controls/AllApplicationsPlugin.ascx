﻿<%@ Control Language="C#" ClassName="AllApplicationsPlugin" %>
<%@ Register TagPrefix="apm" TagName="ExcBbConfigDlg" Src="~/Orion/APM/ExchangeBlackBox/Controls/ExchangeServerConfigurator.ascx" %>
<apm:ExcBbConfigDlg runat="server" />

<script type="text/javascript">
	//<![CDATA[
	var showExchgBBConfigDialog = function (appId) {
		try {
			SW.APM.ExchgBB.ConfigDialog.show(appId);
		} catch (ex) {
			alert(ex);
		}
		return false;
	};
	//]]>
</script>