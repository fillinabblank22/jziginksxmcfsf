﻿using SolarWinds.APM.BlackBox;
using SolarWinds.APM.BlackBox.Exchg.Common.Models;
using SolarWinds.APM.Web;

public partial class Orion_APM_ExchangeBlackBox_Controls_DataRetentionPlugin : BlackBoxConfigPluginBase
{
    protected Config ConfigState
    {
        get { return this.ViewState.GetOrCreate<Config>("config_exbb"); }
        set { this.ViewState["config_exbb"] = value; }
    }


    public override void SaveData()
    {
        var config = this.ConfigState;
        config.RetainDeletedDBDays = ParseDays(this.RetainDeletedDbDays);
        config.RetainDetailTablesDays = ParseDays(this.RetainDetailTablesDays);
        config.RetainMailboxHistoryDays = ParseDays(this.RetainMailboxHistoryDays);
        WithBL(bl => this.ConfigState = (Config) bl.EditConfig(config));
    }

    public override void LoadData()
    {
        WithBL(bl => this.ConfigState = (Config) bl.GetConfig(this.ConfigState));
    }

    public override void RestoreDefaults()
    {
        WithBL(bl => this.ConfigState = (Config) bl.RestoreDefaultConfig(this.ConfigState));
    }

    public override void RefreshData()
    {
        var config = this.ConfigState;
        PrintDays(this.RetainDeletedDbDays, config.RetainDeletedDBDays);
        PrintDays(this.RetainDetailTablesDays, config.RetainDetailTablesDays);
        PrintDays(this.RetainMailboxHistoryDays, config.RetainMailboxHistoryDays);
    }
}
