﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExchangeServerConfigurator.ascx.cs" Inherits="Orion_APM_ExchangeBlackBox_Controls_ExchangeServerConfigurator" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>

<orion:Include ID="i1" File="APM/ExchangeBlackBox/Js/ExchangeServerConfigurator.js" Section="Bottom" runat="server" />
<orion:Include ID="i2" File="APM/ExchangeBlackBox/Js/ExchangeServerConfiguratorExecutor.js" Section="Bottom" runat="server" />
<style type="text/css">
	#excBbConfigDialog { display: none; }
	#excBbConfigDialog div, td { font-size: 11px; }
	#excBbConfigDialog a { color: #336699; }
    #excBbConfigDialog a:hover {color:orange;}
	#excBbConfigDialog select[id^=excBb] { width: 220px; }
	#excBbConfigDialog input[id^=excBb] { width: 215px; }

	#excBbConfigDialog .sub-title { font-weight: bold;padding-left: 10px;padding-top: 15px;padding-bottom: 2px }
	#excBbConfigDialog .label { color: black; }
    #excBbConfigDialog .gray-label { color: #666666; }
	
	#excBbConfigDialog #excBbApps {
		margin: 10px 0px 10px 0px;
	}

	#excBbConfigDialog .separator {
		padding: 3px 0px 0px 3px;
		background-image: url("/Orion/APM/ExchangeBlackBox/Images/ConfigDialogBgSeparator.png");
	}
	#excBbConfigDialog .separator div {
		padding: 3px 0px 3px 20px;
		background-repeat: no-repeat;
	}
	#excBbConfigDialog .separator div.settings-icon {
		background-image: url("/Orion/APM/ExchangeBlackBox/Images/Icon.Settings.png");
	}
	#excBbConfigDialog .separator div.credentials-icon {
		background-image: url("/Orion/APM/ExchangeBlackBox/Images/Icon.Credentials.png");
	}

	#excBbConfigDialog .setup-info td { height: 2px !important; font-size: 2px !important; }
	#excBbConfigDialog .setup-info td div {  }
	#excBbConfigDialog .setup-info td div.load { color: #000; padding-top: 5px;}
	#excBbConfigDialog .setup-info td div.status { color: #008000; padding-top: 5px;}
	#excBbConfigDialog .setup-info td div.error { color: #f33;}
    
    #excBbConfigDialog .line { background-color: lightgray;}
    #excBbConfigDialog .horizontal {width: 535px;height: 1px;}
    #excBbConfigDialog .conf-sec{padding-top: 5px}
	#excBbConfigDialog .expander-container{ padding-left: 16px;display: none;}
    #excBbConfigDialog .config-in-progress-msg{ font-size: 11px !important;}
    #excBbConfigDialog .connection-success {text-align: right;display: none;}
    #excBbConfigDialog .connection-fail{text-align: right;display: none;}
    #excBbConfigDialog .config-error-message{ font-family: 'Arial Regular','Arial';font-size: 11px;font-style: normal;font-weight: 400;line-height: normal;text-align: left;}
    #excBbConfigDialog .config-validation-error{color: #f33;}
</style>
<script id="exc-bb-app-name-tpl" type="text/x-sw-template">
	<img src="/Orion/StatusIcon.ashx?entity=Orion.APM.Exchange.Application&status={4}&size=small" /> {0} on <img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={1}&status={2}&size=small" /> {3} 
</script>
<script id="exc-bb-app-load" type="text/x-sw-template">
	<img width="16" height="16" src="/Orion/images/animated_loading_sm3_whbg.gif"><%= Resources.APM_ExBBContent.LoadApplicationInProgressMsg %>
</script>

<div id="excBbConfigDialog" title="<%= Resources.APM_ExBBContent.Web_ConfigureExchange_Title %>" style="border:1px solid lightgray; margin-top: 5px; background-color: #F8F8F8 ">
	<div class="label">
		<%= Resources.APM_ExBBContent.ConfigureExchServer_Title %>  <a target="_blank" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeWhatChangesWillBeMade") %>">  <%= Resources.APM_ExBBContent.ConfigureExchServer_TitleLink %></a>
	</div>


	<div class="sub-title">
		<div><%= Resources.APM_ExBBContent.ConfigureExchServer_Cred_Title %> </div>
	</div>
    <div id="line2" class="line horizontal"></div>
	<div class="label" style="padding-top: 3px; ">
        <%= Resources.APM_ExBBContent.ConfigureExchServer_Cred_Info %><a target="_blank" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeHelpMeFindTheseCredentials") %>">   <%= Resources.APM_ExBBContent.ConfigureExchServer_Cred_Link %></a>
	</div>
	<table>
		<colgroup>
			<col width="20px" />
			<col width="200px" />
			<col width="300px" />
			<col width="20px" />
		</colgroup>
		<tbody>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_ExBBContent.ConfigureExchServer_ChooseCred%></td>
				<td>
					<select id="excBbCreds" size="1"></select>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_ExBBContent.ConfigureExchServer_Cred_Name%></td>
				<td> <input id="excBbCredName" type="text" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_ExBBContent.ConfigureExchServer_Cred_UserName%></td>
				<td> <input id="excBbUserName" type="text" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_ExBBContent.ConfigureExchServer_Cred_Password%></td>
				<td><input id="excBbPassword1" type="password" /> </td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><%= Resources.APM_ExBBContent.ConfigureExchServer_Cred_ConfPassword%></td>
				<td><input id="excBbPassword2" type="password" /> </td>
				<td>&nbsp;</td>
			</tr>
            <tr>
                <td colspan="4" style="padding-top: 15px">
                    <div><img id="expander" src="/Orion/images/Button.Expand.gif" state ="collapsed"/>
                        <%= Resources.APM_ExBBContent.ConfigureExchServer_AdvancedLabel%></div>
                    <div class = "expander-container" >
                        <div class="conf-sec"><b><%= Resources.APM_ExBBContent.ConfigureExchServer_ManualLabel%></b></div>
                        <div class="gray-label"><%= Resources.APM_ExBBContent.ConfigureExchServer_ManualInfo%></div>
                        <div class="conf-sec">
                            <a target="_blank" href="<%= HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeManuallyConfigureExchangeServer") %>"> <%= Resources.APM_ExBBContent.ConfigureExchServer_ManualLink%></a>
                            <a  style ="padding-left: 90px" href="#" id="serverIsAlreadyConfiguredLink" > <%= Resources.APM_ExBBContent.ConfigureExchServer_AlreadyConfiguredLink%></a>
                        </div>
                        <div class="conf-sec"><b><%= Resources.APM_ExBBContent.ConfigureExchServer_UserConnTitle%></b></div>
                        <div class="gray-label"><%= Resources.APM_ExBBContent.ConfigureExchServer_UserConnInfo%></div>
                        <div class="conf-sec"><a target="_blank" id="configEditAppLink" href="#"><%= Resources.APM_ExBBContent.ConfigureExchServer_EditAppLink%></a></div>
                    </div>
                </td>
            </tr>
			<tr class="setup-info">
				<td colspan="4">
					<div class="load">
					    <table style="width:100%">
                            <tr>
                                <td style="width: 50px"><div><img src="/Orion/Apm/Images/animated_loading45x45.gif"/> </div></td>
                                <td><div class="config-in-progress-msg" style="font-size: 8px;" > <%= Resources.APM_ExBBContent.ConfigureExchServer_ConfigInProgress%></div></td>
                                <td>
                                    <div class ="connection-success"> <img src="/Orion/Apm/Images/config_test_passed.png"/> <%= Resources.APM_ExBBContent.ConfigureExchServer_ConfigOk%></div>
                                    <div class ="connection-fail"> <img src="/Orion/Apm/Images/config_test_failed.png"/> <%= Resources.APM_ExBBContent.ConfigureExchServer_ConfigFail%></div>
                                </td>
                            </tr>

					    </table>
					</div>
					<div class="status"> </div>
                    <div class="config-validation-error"></div>
                    <div class="error">
                        <div id="configuratorErrorMessageContainer" class="status-details-container" status="Undefined">
                            <table>
                                <tr>
                                    <td style="width: 15px;vertical-align: top;"><img src="/orion/apm/images/cofig_error_ico.png" /></td>
                                    <td><label id="configuratorErrorMessage" class="config-error-message"></label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><div id="excBbApps"></div></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" style="text-align: right;">
					<orion:LocalizableButton ID="btnSetup" Text="<%$ Resources: APMWebContent, CONFIGURE_SERVER %>" DisplayType="Primary" runat="server" CssClass="btn-setup" />
					<orion:LocalizableButton ID="btnCancel" Text="<%$ Resources: APMWebContent, APMWEBDATA_PS0_53 %>" DisplayType="Secondary" runat="server" CssClass="btn-cancel" />
				</td>
			</tr>
		</tfoot>
	</table>
</div>
