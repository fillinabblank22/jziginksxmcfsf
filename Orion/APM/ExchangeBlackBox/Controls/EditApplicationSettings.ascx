﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationSettings.ascx.cs" Inherits="Orion_APM_ExchangeBlackBox_Controls_EditApplicationSettings" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Register Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" TagPrefix="apm" TagName="SelectCredentials" %>
<%@ Register Src="~/Orion/APM/Controls/TestButtonSummary.ascx" TagPrefix="apm" TagName="TestButtonSummary" %>
<orion:Include runat="server" File="APM/ExchangeBlackBox/Styles/EditApplicationSettings.css" />
<orion:Include runat="server" Module="APM" File="EditBBApplication.js"/>
<orion:Include runat="server" Module="APM" File="ExchangeBlackBox/js/EditApplicationSettings.js"/>
<orion:Include runat="server" Module="APM" File="ExchangeBlackBox/js/ExchangeServerConfiguratorExecutor.js" Section="Bottom" />
<script type="text/javascript">
    SW.APM.BB.EX.EditAppSettings.init({
        AppType: '<%= ControlHelper.EncodeJsString(ExchangeConstants.ApplicationTemplateCustomType) %>',
        ServerIdentityKey: '<%= ControlHelper.EncodeJsString(ExchangeConstants.ServerIdentityKey) %>',
        PsUrlExchangeKey: '<%= ControlHelper.EncodeJsString(ExchangeConstants.PsUrlExchangeKey) %>',
        PsUrlWindowsKey: '<%= ControlHelper.EncodeJsString(ExchangeConstants.PsUrlWindowsKey) %>',
        SettingLevelInstance: '<%= (int) SettingLevel.Instance %>',
        AgentPollingMethodName: '<%= ControlHelper.EncodeJsString(this.AgentPollingMethodName) %>',
        CredentialSetIdKey: '<%= ControlHelper.EncodeJsString(InternalSettings.CredentialSetIdKey) %>',
        TestSummaryClientId: '<%= ControlHelper.EncodeJsString(this.testSummary.ClientID) %>',
        TestButtonClientId: '<%= ControlHelper.EncodeJsString(this.testButton.ClientID) %>',
        ConfigButtonClientId: '<%= ControlHelper.EncodeJsString(this.ConfigureServerButton.ClientID) %>',
        WindowsTooltipClientId: '<%= ControlHelper.EncodeJsString(this.windowsTooltip.ClientID) %>',
        ExchangeTooltipClientId: '<%= ControlHelper.EncodeJsString(this.exchangeTooltip.ClientID) %>',
        WindowsTooltipContent: '<%= ControlHelper.EncodeJsString(Resources.APM_ExBBContent.APMWEBCODE_YP1_6) %>',
        ExchangeTooltipContent: '<%= ControlHelper.EncodeJsString(Resources.APM_ExBBContent.APMWEBCODE_YP1_5) %>',
        SaveCredentialChangesTitle: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Title) %>',
        SaveCredentialChangesMsg: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Msg) %>'
    });
</script>
<table id="appEditExchangeSettings" class="appProperties" cellspacing="0" style="display: none">
    <tr id="exchangeServerIdentityRow">
        <td class="label">
            <asp:Literal ID="exchangeServerIdentityLabel" runat="server" /></td>
        <td>
            <label id="exchangeServerIdentity"/>
        </td>
        <td rowspan="5" style="padding-left: 10px">
            <div class="ExchTipsInstructions" runat="server" id="credentialsContainer" style="font-size: 11px;">
                <h2><b><%= Resources.APM_ExBBContent.APMWEBCODE_YP1_16 %></b></h2>
                <label><%= Resources.APM_ExBBContent.EditAppConfigServer_TopLabel %></label>
                <div class="ConfigBtnContainer">
                    <orion:localizablebutton runat="server" id="ConfigureServerButton" text="<%$ Resources: APMWebContent, CONFIGURE_SERVER %>" displaytype="Secondary" onclientclick="" />
                </div>
                <table>
                    <tr class="setup-info">
                       <div class="config-result">
                            <div class="conf-success">
                                <div style="font-size: 11px;" > <img src="/Orion/Apm/Images/config_test_passed.png"/><%= Resources.APM_ExBBContent.EditAppConfigServer_ConfigSuccess %></div>
                            </div>
                        </div>
                        <div class="config-validation-error"></div>
                    </tr>
                    <tr>
                        <div class="error" style="padding-bottom: 15px;word-wrap: break-word;">
                            <div id="configuratorErrorMessageContainer" class="status-details-container" status="Undefined">
                               <img src="/orion/apm/images/cofig_error_ico.png" />
                               <label id="configuratorErrorMessage" class="config-error-message"></label>
                            </div>
                        </div>
                    </tr>
                </table>
                <label><%= Resources.APM_ExBBContent.EditAppConfigServer_BottomLabel%></label><br />
                <div class="ConfigHelpLink" style="font-size: 11px;">
                    <label><a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeManuallyConfigureExchangeServer") %>" target="_blank">&#0187;<%= Resources.APM_ExBBContent.EditAppConfigServer_HelpLink%></a></label>
                </div>
            </div>
        </td>
    </tr>
    <tr id="psUrlWindowsRow">
        <td class="label" >
            <asp:Literal ID="psUrlWindowsLabel" runat="server" /></td>
        <td style="width: 450px;">
            <input class="ShortInput" id="psUrlWindows" name="psUrlWindows" class="text" size="50" maxlength="250" />
            <asp:Image runat="server" ID="windowsTooltip" ImageUrl="~/Orion/APM/Images/Admin/icon_lightbulb.gif" CssClass="tooltipImage" />
            <span id="psUrlWindowsError" class="validationMsg sw-suggestion sw-suggestion-fail">
                <span class="sw-suggestion-icon"></span>
                <span class="validationError">
                    <label class="error"></label>
                </span>
            </span>
        </td>
    </tr>
    <tr id="psUrlExchangeRow">
        <td class="label">
            <asp:Literal ID="psUrlExchangeLabel" runat="server" /></td>
        <td style="width: 450px;">
            <span>https://localhost</span>
            <input style="width: 221px;" class="ShortInput" id="psUrlExchange" name="psUrlExchange" class="text" size="50" maxlength="250" />
            <asp:Image runat="server" ID="exchangeTooltip" ImageUrl="~/Orion/APM/Images/Admin/icon_lightbulb.gif" CssClass="tooltipImage" />
            <span id="psUrlExchangeError" class="validationMsg sw-suggestion sw-suggestion-fail">
                <span class="sw-suggestion-icon"></span>
                <span class="validationError">
                    <label class="error"></label>
                </span>
            </span>
        </td>
    </tr>
    <tr>
        <td class="label"></td>
        <td>
           <label style="font-size: 11px"> <a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("SAMAGAppInsightForExchangeHelpMeFindTheseURLSettings") %>" >» <%= Resources.APM_ExBBContent.EditAppConfigServer_UrlHelpLink %></a></label>
        </td>
    </tr>
    <tr>
        <td class="label"><%= Resources.APM_ExBBContent.EditAppCred_Title %>
        </td>
        <td style="height: 170px">
            <table class="selectCreds" style="width: 410px">
                <apm:SelectCredentials runat="server" ID="exchangeCredentials" ValidationGroupName="SelectCredentialsValidationGroup" AllowNodeWmiCredential="True" />
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td class="testRow" style="width: 430px">
            <apm:TestButtonSummary runat="server" ID="testSummary" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SelectCredentialsValidationGroup" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="testButtonRow">
             <orion:LocalizableButton runat="server" ID="testButton" Text="<%$ Resources: APMWebContent, TEST_CONNECTION %>" DisplayType="Secondary" />
        </td>
        <td></td>
    </tr>
</table>
