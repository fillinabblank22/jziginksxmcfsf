﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditDatabaseResourceControl.ascx.cs" Inherits="Orion_AssetInventory_Controls_EditDatabaseResourceControl" %>
<%@ Import Namespace="Resources" %>
<div>
    <div style="padding-right: 20px; font-weight:bold"><%= APM_ExBBContent.EditDatabaseResource_ShowDatabases %></div>
    <asp:DropDownList id="ShowDatabasesDropDown" runat="server">
        <asp:ListItem Text="<%$ Resources: APM_ExBBContent, EditDatabaseResource_ShowDatabases_AllDatabases %>"/>
        <asp:ListItem Text="<%$ Resources: APM_ExBBContent, EditDatabaseResource_ShowDatabases_ActiveDatabases %>"/>
        <asp:ListItem Text="<%$ Resources: APM_ExBBContent, EditDatabaseResource_ShowDatabases_PassiveDatabases %>"/>
    </asp:DropDownList>
</div>
