﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeTestNodeRow.ascx.cs" Inherits="Orion_APM_Controls_ChangeTestNodeRow" %>

<table>
    <tr>
        <td>
            <span id="<%=MsgId%>" style="margin-left: 50px; margin-right: 20px;" class="ChangeTestNodeMessage">
                <%=TestNodeMsg%>
            </span> 
        </td>
		<%if (ShowChangeNodeMsg) {%>
        <td style="min-width: 30%; text-align: right;">
            <a id="href<%=MsgId%>" onclick="<%=TestNodeScript%>" href="#" style="display:<%=ChangeTestNodeScriptDisplayStyle%>;" ><%=ChangeNodeMsg%></a>
        </td>
		<%} %>
    </tr>
</table>
<asp:HiddenField ID="hidTestNode" runat="server" />