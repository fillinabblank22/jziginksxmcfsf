﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApmCustomQueryTable.ascx.cs" Inherits="Orion_APM_Controls_ApmCustomQueryTable" %>

<orion:Include ID="Include1" runat="server" File="APM/js/ApmCustomQuery.js" />

<asp:ScriptManagerProxy runat="server" >
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
    </Services>
</asp:ScriptManagerProxy>
        
<div id="ErrorMsg-<%= UniqueClientID %>"></div>

<table id="Grid-<%= UniqueClientID %>" class="sw-custom-query-table NeedsZebraStripes" cellpadding="2" cellspacing="0" width="100%" ></table>
<span id="LoaderSpan-<%= UniqueClientID %>" style="font-weight: bold; color: #777;  text-align: center; display: block; margin-left: auto; margin-right: auto">
    <img src="/Orion/images/AJAX-Loader.gif" alt="" />
    <%=Resources.APMWebContent.LoadingLabel %>
</span>

<div id="Pager-<%= UniqueClientID %>" class="ReportFooter ResourcePagerControl hidden"></div>

<textarea id="SWQL-<%= UniqueClientID %>" style="display:none;">
    <%= HttpUtility.HtmlEncode(SWQL) %>
</textarea>

<textarea id="SearchSWQL-<%= UniqueClientID %>" style="display:none;">
    <%= HttpUtility.HtmlEncode(SearchSWQL) %>
</textarea>
        
<input type="hidden" id="OrderBy-<%= UniqueClientID %>" />
<input type="hidden" id="BaseOrderBy-<%= UniqueClientID %>" />
