﻿using System;
using SolarWinds.APM.Web.Utility;

public partial class Orion_APM_Controls_ChangeTestNodeRow : System.Web.UI.UserControl
{
	class Node
	{
		public int id;
		public string name;
		public int status;
	}

	private Node selectedNode;
	private Node SelectedNodeInfo 
	{
		get 
		{
			if (selectedNode == null) 
			{
				if (String.IsNullOrEmpty(hidTestNode.Value))
				{
					selectedNode = new Node();
				}
				else 
				{
					selectedNode = JsHelper.Deserialize<Node>(hidTestNode.Value);
				}
			}
			return selectedNode;
		}
	}

	public string TestButtonId { get; set; }

	public bool AllowChange { get; set; }

    public bool ChangeAllTestNodes { get; set; }

	public string MsgId { get; private set; }

    public bool NodeSet
    {
		get { return SelectedNodeInfo.id > 0; }
    }

	public int NodeId
	{
		get { return SelectedNodeInfo.id; }
		set
		{
			SelectedNodeInfo.id = value;
			hidTestNode.Value = JsHelper.Serialize(SelectedNodeInfo);
		}
	}
	public string NodeName
	{
		get { return SelectedNodeInfo.name; }
		set
		{
			SelectedNodeInfo.name = value;
			hidTestNode.Value = JsHelper.Serialize(SelectedNodeInfo);
		}
	}
	public int NodeStatus
	{
		get { return SelectedNodeInfo.status; }
		set
		{
			SelectedNodeInfo.status = value;
			hidTestNode.Value = JsHelper.Serialize(SelectedNodeInfo);
		}
	}

	public string NodeControlClientID
	{
		get { return hidTestNode.ClientID; }
	}

    protected string TestNodeMsg
    {
        get
        {
            if (!ChangeAllTestNodes)
            {
                return NodeSet ? String.Format(Resources.APMWebContent.APMWEBCODE_AK1_81, NodeName) : Resources.APMWebContent.APMWEBCODE_AK1_82;
            }
            return string.Empty;
        }
    }

	public bool ShowChangeNodeMsg
	{
		get  {  return Convert.ToBoolean(ViewState["ShowChangeNodeMsg"] ?? (ViewState["ShowChangeNodeMsg"] = true)); }
		set { ViewState["ShowChangeNodeMsg"] = value; }
	}

    protected string ChangeNodeMsg
    {
        get { return NodeSet ? Resources.APMWebContent.APMWEBCODE_AK1_83 : Resources.APMWebContent.APMWEBCODE_AK1_84; }
    }

	protected string TestNodeScript
	{
		get { return string.Format("APMjs.SelectNode.showNodeSelectionDialog('{0}', '{1}', '{2}', this, '{3}');", MsgId, TestButtonId, ChangeAllTestNodes, NodeControlClientID); }
	}

    protected string ChangeTestNodeScriptDisplayStyle
    {
        get { return AllowChange ? "" : "none"; }
    }

	protected void Page_Init()
	{
		// id for javascript
		MsgId = Guid.NewGuid().ToString();
	}
}