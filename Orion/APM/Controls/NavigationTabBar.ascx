<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationTabBar.ascx.cs" Inherits="Orion_APM_Controls_NavigationTabBar" %>
<%@ Import Namespace="Resources" %>
    
<orion:Include runat="server" File="APM/APM.css"/>

<%
    this.AddTab(APMWebContent.APMWEBDATA_TM0_20, VirtualPathUtility.ToAbsolute("~/Orion/APM/Admin/Applications/Default.aspx"), IsPageRegexMatch("(^|/)Applications/Default.aspx$"));
    this.AddTab(APMWebContent.APMWEBDATA_TM0_21, VirtualPathUtility.ToAbsolute("~/Orion/APM/Admin/ApplicationTemplates.aspx"), IsPageRegexMatch("(^|/)ApplicationTemplates.aspx$"));
    this.AddTab(APMWebContent.APMWEBDATA_TM0_22, VirtualPathUtility.ToAbsolute("~/Orion/APM/Admin/Templates/thwack/Default.aspx"), IsPageRegexMatch("(^|/)thwack/Default.aspx$"));
%>

<script type="text/javascript">
$(document).ready(function(){
  $(".sw-tabs li.sw-tab").bind('mouseover', function(){ $(this).addClass('x-tab-strip-over'); });
  $(".sw-tabs li.sw-tab").bind('mouseout', function(){ $(this).removeClass('x-tab-strip-over'); });
});
</script>

<div class="sw-tabs">
    <div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">
        <div class="x-tab-strip-wrap">
            <%-- 
            <div class="x-tab-strip-spacer" style="display: none;"><!-- --></div>
            --%>

            <ul class="x-tab-strip x-tab-strip-top">
                <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />
                <li class="x-tab-edge"></li>
                <div class="x-clear"><!-- --></div>
            </ul>
            <div class="x-tab-strip-spacer" style="border-bottom: none;"><!-- --></div>
        </div>
    </div>
</div>

    
