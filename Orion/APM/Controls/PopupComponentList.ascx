﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PopupComponentList.ascx.cs" Inherits="Orion_APM_Controls_BlackBox_PopupComponentList" %>
<%@ Import Namespace="Resources" %>
<orion:Include Module="APM" File="APM.css" runat="server" />
<table cellpadding="0" cellspacing="0" class="apm_NeedsZebraStripes">
    <thead>
        <tr>
            <th colspan="3" class="componentListTitle"><%= this.ComponentListTitle %></th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater ID="repComponents" runat="server">
            <ItemTemplate>
                <tr class="zebraRow">
                    <td style="width: 20px;">&nbsp;</td>
                    <td><img src="<%# this.GetComponentStatusImagePath(Container.DataItem) %>" /></td>
                    <td><%# this.GetComponentName(Container.DataItem) %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
    <tbody>
        <asp:PlaceHolder ID="phMoreComp" runat="server">
            <tr class="MoreRow">
                <td colspan="3"><%= this.MoreComponentsMessage %></td>
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phNoComps" runat="server">
            <tr class="MoreRow">
                <td colspan="3"><%= APMWebContent.APMWEBDATA_TM0_55 %></td>
            </tr>
        </asp:PlaceHolder>
    </tbody>
</table>
