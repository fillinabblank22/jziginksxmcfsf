﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_APM_Controls_MessageBox : System.Web.UI.UserControl
{
    #region Properties

    public enum MessageBoxType
    {
        Info,
        Hint,
        Pass,
        Warn,
        Fail
    }

    public MessageBoxType Type
    {
        get;
        set;
    }

    public string Width
    {
        get;
        set;
    }

    public string Height
    {
        get;
        set;
    }

    public string Margin
    {
        get;
        set;
    }

    public string Padding
    {
        get;
        set;
    }

    public string MaxWidth
    {
        get;
        set;
    } 

    public string MessageHtml
    {
        get;
        set;
    }

    #endregion

    #region Protected methods

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            switch (this.Type)
            {
                case MessageBoxType.Hint:
                    this.pnlContainer.CssClass = "sw-suggestion";
                    break;
                case MessageBoxType.Info:
                    this.pnlContainer.CssClass = "sw-suggestion sw-suggestion-info";
                    break;
                case MessageBoxType.Pass:
                    this.pnlContainer.CssClass = "sw-suggestion sw-suggestion-pass";
                    break;
                case MessageBoxType.Warn:
                    this.pnlContainer.CssClass = "sw-suggestion sw-suggestion-warn";
                    break;
                case MessageBoxType.Fail:
                    this.pnlContainer.CssClass = "sw-suggestion sw-suggestion-fail";
                    break;
                default:
                    this.pnlContainer.CssClass = "sw-suggestion";
                    break;                
            }            

            if (!string.IsNullOrEmpty(this.Height))
            {
                this.pnlContainer.Attributes.CssStyle.Add("height", this.Height.Trim());
            }

            if (!string.IsNullOrEmpty(this.Width))
            {
                this.pnlContainer.Attributes.CssStyle.Add("width", this.Width.Trim());
            }

            if (!string.IsNullOrEmpty(this.MaxWidth))
            {
                this.pnlContainer.Attributes.CssStyle.Add("max-width", this.MaxWidth.Trim());
            }

            if (!string.IsNullOrEmpty(this.MessageHtml))
            {
                this.spanSuggestionText.InnerHtml = this.MessageHtml.Trim();
            }

            if (!string.IsNullOrEmpty(this.Margin))
            {
                this.pnlContainer.Attributes.CssStyle.Add("margin", this.Margin.Trim());
            }

            if (!string.IsNullOrEmpty(this.Padding))
            {
                this.pnlContainer.Attributes.CssStyle.Add("padding", this.Padding.Trim());
            }
        }
    }

    #endregion
}