﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequiresComponentName.ascx.cs" Inherits="Orion_APM_Controls_RequiresComponentName" %>

<orion:resourceWrapper ID="Wrapper" runat="server">
	<Content>
		<div id="ctrInvalidTpl" visible="false" runat="server">
		    <%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_328, "<b>", _resourceName, "</b>")%>
		</div>
		<div id="ctrEditResource" visible="false" runat="server">
			<asp:HyperLink ID="ctrEditURL" Font-Underline="true" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_329 %>" runat="server"/>
			<asp:Label ID="message" runat="server"/>
		</div>
    </Content>
</orion:resourceWrapper>