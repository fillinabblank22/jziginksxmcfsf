﻿using System.Web.UI;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;

public partial class Orion_APM_Controls_RequiresNetObject : System.Web.UI.UserControl, SolarWinds.APM.Web.IConfigurationRequiredControl
{
	#region IConfigurationRequiredControl Members

	public void SetMonitorData(ApmBaseResource res)
	{
		var wrapper = default(Orion_ResourceWrapper);
		FindResourceWrapper(this, out wrapper);
		if (wrapper != null)
		{
			wrapper.ShowEditButton = false;
		}

		ctrMsg.Text = @"Net Object is required for ""{0}"" resource.".FormatInvariant(res.Resource.Name);
	}
	
	#endregion

	#region Helper Members

	private void FindResourceWrapper(Control parent, out Orion_ResourceWrapper wrapper)
	{
		wrapper = default(Orion_ResourceWrapper);
		foreach (Control ctr in parent.Controls)
		{
			if ((wrapper = ctr as Orion_ResourceWrapper) != null)
			{
				return;
			}
			if (ctr.Controls.Count > 0)
			{
				FindResourceWrapper(ctr.Parent, out wrapper);
			}
		}
	}

	#endregion
}