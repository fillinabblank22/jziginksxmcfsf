﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTopXX.ascx.cs" Inherits="Orion_APM_Controls_EditTopXX" %>
<orion:Include ID="CssResources" runat="server" Module="APM" File="APM/APM.css"/>
<div>
    <span class="editXX"><%=Resources.APM_IisBBContent.EditTopXXItemsToDisplay %></span><br/><asp:TextBox runat="server" ID="txtRowsPerPage" Width="50px"/>
	<asp:RangeValidator id="RowsPerPageRangeValidator" runat="server" controltovalidate="txtRowsPerPage" errormessage="<%$ Resources: APM_IisBBContent, EditTopXXError %>" MinimumValue="1" MaximumValue="100000" Type="Integer" />
</div>
