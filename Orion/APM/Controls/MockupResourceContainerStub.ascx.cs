using System;
using System.Web.UI;

public partial class Orion_APM_Controls_MockupResourceContainerStub : UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        try
        {
            var mockupResourceContainer = LoadControl("/Orion/APM/Resources/MockupResourceContainer.ascx");
            MockupResourcePlaceHolder.Controls.Add(mockupResourceContainer);
            mockupResourceContainer.DataBind();
        }
        catch (Exception ex)
        {
            Session["MockupResourceErrorMessage"] = ex.Message;
            var errorControl = LoadControl("/Orion/APM/Resources/MockupResourceCustomError.ascx");
            MockupResourcePlaceHolder.Controls.Add(errorControl);
        }
    }
}
