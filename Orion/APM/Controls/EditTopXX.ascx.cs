﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

public partial class Orion_APM_Controls_EditTopXX : BaseResourceEditControl
{
    private ResourceInfo _resource;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (!String.IsNullOrEmpty(this.Resource.Properties["RowsPerPage"]))
            {
                this.txtRowsPerPage.Text = this.Resource.Properties["RowsPerPage"];
            }
            else
            {
                this.txtRowsPerPage.Text = "20";
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            int rowsPerPage;
            properties["RowsPerPage"] = int.TryParse(this.txtRowsPerPage.Text, out rowsPerPage) ? rowsPerPage : 20;
            return properties;
        }
    }
}