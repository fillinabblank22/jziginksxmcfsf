﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BaselineDetails.ascx.cs" Inherits="Orion_APM_Controls_BaselineDetails" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Register TagPrefix="apm" TagName="MessageBox" Src="~/Orion/APM/Controls/MessageBox.ascx" %>
<%@ Register TagPrefix="apm" TagName="CustomChart" Src="~/Orion/APM/Controls/Charts/CustomChart.ascx" %>
<%@ Register TagPrefix="apm" TagName="TimeFrameTooltip" Src="~/Orion/APM/Controls/TimeFrameTooltip.ascx" %> 


<div id="LatestBaselineDetails" class="baseline baselineDialog">
    <div class="container">
        <apm:timeframetooltip runat="server" id="tftWorkDays" IconUrl="/Orion/APM/images/baselineDay.png" TimeFrameName="APM_WorkHours" />
        <apm:timeframetooltip runat="server" id="tftOffDays" IconUrl="/Orion/APM/images/baselineNight.png" TimeFrameName="APM_NightAndWeekend" />
        <span id="HelpLinkUrl" class="hidden"><%= HelpLocator.CreateHelpUrl("OrionSAMAGBaselineDetails") %></span>
        <div class="leftColumnWrapper">
            <div class="leftColumn">
                <div>
                    <p class="sectionHeader">
                        <%= Resources.APMWebContent.APMWEBDATA_PS0_62 %><%--Latest Baseline Graphs--%> <span class="thresholdName"></span>
                    </p> 
                    <div id="Tabs">
                        <ul>
                            <li>
                                <img alt="icon_ocurrences" src="/Orion/APM/images/baselineOccurrences.png" >
                                <a href="#tabOccurrences"><%= Resources.APMWebContent.APMWEBDATA_PS0_63 %><%--Occurrences--%></a>
                            </li>    
                            <li>
                                <img alt="icon_metricOverTime" src="/Orion/APM/images/baselineMetricOverTime.png" >
                                <a href="#tabMetricOverTime"><%= Resources.APMWebContent.APMWEBDATA_PS0_64 %><%--Metric over Time--%></a>
                            </li>                                                                                                                        
                        </ul>                                
                        <div id="tabOccurrences" class="chartContainer">                        
                            <apm:CustomChart runat="server" id="ccArea" LoadOnDemand="true" />
                           <%-- <input type="checkbox" id="AreaChartLoaded" name="AreaChartLoaded" class="hidden" />--%>
                        </div>
                        <div id="tabMetricOverTime" class="chartContainer">
                            <apm:CustomChart runat="server" id="ccMinMax" LoadOnDemand="true" />                        
                           <%-- <input type="checkbox" id="MinMaxChartLoaded" name="MinMaxChartLoaded" class="hidden" />--%>                            
                        </div>                    
                        <input type="text" id="ChartsWidth" name="chartWidth" class="hidden" />
                    </div>
                </div>
                <div class="statistics">
                    <p class="sectionHeader">
                        <%= Resources.APMWebContent.APMWEBDATA_PS0_65 %><%--Latest Baseline Statistics--%> <span class="thresholdName"></span>
                    </p> 
                    <table id="tblStatistics">
                        <thead>
                            <tr>
                                <th><%= Resources.APMWebContent.APMWEBDATA_PS0_66 %><%--TIME PERIOD--%>
                                    <span id="StastisticsTooltip">
                                        <span class="text">
                                            <%= Resources.APMWebContent.APMWEBDATA_PS0_67 %><%--A value of--%>&nbsp;<span class="value" id="StastisticsTooltipValue"></span>
                                            <br/><br/>
                                            <div id="lblRecomendedWarning" class="recommended">
                                                <%= Resources.APMWebContent.APMWEBDATA_PS0_68 %><%--Recommended <span class="warning">Warning</span> Threshold--%>
                                            </div>
                                            <div id="lblRecomendedCritical" class="recommended">
                                                <%= Resources.APMWebContent.APMWEBDATA_PS0_69 %><%--Recommended <span class="critical">Critical</span> Threshold--%>
                                            </div>                                                                                
                                            <a id="lnkSetWarning">
                                                » <%= Resources.APMWebContent.APMWEBDATA_PS0_70 %><%--Set as <span class="warning">Warning</span> Threshold--%>
                                            </a>                                        
                                            <br/>
                                            <a id="lnkSetCritical">
                                                » <%= Resources.APMWebContent.APMWEBDATA_PS0_71 %><%--Set as <span class="critical">Critical</span> Threshold--%>
                                            </a>
                                        </span>
                                    </span>                                   
                                </th>
                                <th><%= Resources.APMWebContent.APMWEBDATA_PS0_72 %><%--MIN--%></th>
                                <th><%= Resources.APMWebContent.APMWEBDATA_PS0_73 %><%--MAX--%></th>
                                <th><%= Resources.APMWebContent.APMWEBDATA_PS0_74 %><%--STD DEV--%> (σ)</th>
                                <th>-3σ</th>
                                <th>-2σ</th>
                                <th>-1σ</th>
                                <th><%= Resources.APMWebContent.APMWEBDATA_PS0_75 %><%--MEAN--%></th>
                                <th>1σ</th>
                                <th>2σ</th>
                                <th>3σ</th>
                            </tr>
                        </thead>
                        <tbody>                            
                        </tbody>
                    </table>           
                    <div class="legendContainer">
                        <div class="legend">
                            <div class="warning"></div>&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_76 %><%--Warning Threshold--%>
                        </div>                    
                        <div class="legend">
                            <div class="critical"></div>&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_77 %><%--Critical Threshold--%>
                        </div>    
                    </div>
                    <div class="resetContainer">
                        <a id="ResetToRecommendedThresholds">
                            <%= Resources.APMWebContent.APMWEBDATA_PS0_78 %><%--Reset to Recommended Thresholds--%>
                        </a>
                    </div>
                </div>
            </div>      
        </div>            
        <div class="rightColumn">
            <div id="BaselineDetails" class="section">
                <p class="sectionHeader">                       
                    <%= Resources.APMWebContent.APMWEBDATA_PS0_79 %><%--LATEST BASELINE DETAILS--%>
                </p>
                <p>
                    <span class="bold">
                        <%= Resources.APMWebContent.APMWEBDATA_PS0_80 %><%--Calculated On:--%>
                    </span>
                    <br>
                    <span class="calculatedOn"></span>
                </p> 
                <p>
                    <span class="bold">
                        <%= Resources.APMWebContent.APMWEBDATA_PS0_81 %><%--Baseline Data Used:--%>
                    </span>
                    <br>
                    <span  class="dataUsed"></span>
                </p>                                 
                <apm:MessageBox runat="server" id="mbDetailsHint" Type="Hint" Margin="0px 10px" />                                                                                          
            </div>
            <div id="DataForCurrentThresholds" class="section">
                <p class="sectionHeader">                       
                    <%= Resources.APMWebContent.APMWEBDATA_PS0_82 %><%--BASELINE DATA FOR CURRENT THRESHOLDS--%>
                </p> 
                <p>
                    <span class="bold">
                        <%= Resources.APMWebContent.APMWEBDATA_PS0_80 %><%--Calculated On:--%>
                    </span>
                    <br>
                    <span class="calculatedOn"></span>
                </p> 
                <p>
                    <span class="bold">
                        <%= Resources.APMWebContent.APMWEBDATA_PS0_81 %><%--Baseline Data Used:--%>
                    </span>
                    <br>
                    <span class="dataUsed" style="font-size:11px"></span>
                </p> 
            </div>                
            <div id="CurrentThresholdSettings" class="section">
                <p class="sectionHeader">                       
                    <%= Resources.APMWebContent.APMWEBDATA_PS0_83 %><%--CURRENT THRESHOLD SETTINGS--%>
                </p> 
                <div> 
                    <table>
				        <tr class="label">
					        <td colspan="3">
						        <img src="/Orion/APM/images/StatusIcons/Small-Up-Warn.gif" alt="" />&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_16 %><%--Warning:--%>
					        </td>
                        </tr>
                        <tr>
                            <td class="operator">
				            	<select id="Operator" name="Operator" class="thresholdDialogOperator">
							        <% foreach (var kvp in Options)
		                            { %>
							            <option value="<%= (int) kvp.Key %>"><%= kvp.Value %></option>
							        <% } %>
						        </select>
					        </td>
					        <td class="value">
						        <input id="WarningLevel" name="WarningLevel" type="text" size="200" />   
                                <input id="WarningBaselineValue" name="WarningBaselineValue" type="hidden" />                                             
					        </td>	
                            <td class="baselineIcon" rowspan="3">
                                <img class="baseline" alt="baseline_applied" title="<%= Resources.APMWebContent.APMWEBDATA_PS0_18 %><%--Thresholds calculated from baseline data are being used.--%>" src="/Orion/APM/images/baselineApplied_large.png" />
                            </td>
				        </tr>
				        <tr class="label">
					        <td>
						        <img src="/Orion/APM/images/StatusIcons/Small-Up-Critical.gif" alt="" />&nbsp;<%= Resources.APMWebContent.APMWEBDATA_PS0_17 %><%--Critical:--%>
					        </td>
                        </tr>
				        <tr>
                            <td class="operator">
						        <span id="CriticalOp" class="thresholdOpStatic"></span>
					        </td>
					        <td class="value">
						        <input id="CriticalLevel" name="CriticalLevel" type="text" size="200" />  
                                <input id="CriticalBaselineValue" name="CriticalBaselineValue" type="hidden" />                                                                                                                                           
					        </td>	
				        </tr>
                        <tr class="button">
                            <td colspan="4" class="button">
                                <button type="button" id="UseRecommendedThresholds">
                                    <%= Resources.APMWebContent.APMWEBDATA_PS0_84 %><%--Use Recommended Thresholds--%>
                                </button>
                            </td>
                        </tr>
			        </table>
                    <input type="checkbox" id="ApplyThresholds" name="ApplyThresholds" class="hidden" />
                </div>
            </div>                
        </div>            
        <div class="sw-btn-bar" >                
            <orion:LocalizableButtonLink ID="btnDialogApply" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: APMWebContent, APMWEBDATA_PS0_85 %>"
                 onclick="$('#LatestBaselineDetails #ApplyThresholds').attr('checked', true); $('#LatestBaselineDetails').dialog('close');"/>
            <orion:LocalizableButtonLink ID="btnDialogClose" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources: APMWebContent, APMWEBDATA_PS0_86 %>"
                 onclick="$('#LatestBaselineDetails #ApplyThresholds').attr('checked', false); $('#LatestBaselineDetails').dialog('close');" />
        </div>    
    </div>
</div>