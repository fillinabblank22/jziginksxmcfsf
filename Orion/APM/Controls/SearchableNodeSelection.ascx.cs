using System;
using System.Collections.Generic;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Controls_SearchableNodeSelection : System.Web.UI.UserControl
{
    public string FilterString { get; set; }
    public string EntityName { get; set; }
    public string Entities { get; set; }

    public string AfterNodeSelectionChangeScript
    {
        get{ return this.addRemoveObjectsControl.AfterNodeSelectionChangeScript; }
        set { this.addRemoveObjectsControl.AfterNodeSelectionChangeScript = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("Orion", "Admin/Containers/Containers.css");

        if (!IsPostBack)
        {
            this.EntityName = "Orion.Nodes";
            this.Entities = "Nodes";

            this.addRemoveObjectsControl.LeftPanelTitle = String.Format(Resources.APMWebContent.APMWEBCODE_AK1_24, this.Entities);
            this.addRemoveObjectsControl.RightPanelTitle = String.Format(Resources.APMWebContent.APMWEBCODE_AK1_25, this.Entities);
            this.addRemoveObjectsControl.EntityName = this.EntityName;
            this.addRemoveObjectsControl.Filter = FilterString;
        }
    }

    public void StoreSelectedNodes(List<KeyValuePair<int, string>> nodes)
    {
        List<SelectNodeTreeItem> converted = nodes.ConvertAll<SelectNodeTreeItem>(o => new SelectNodeTreeItem(o.Key, o.Value));
        StoreSelectedNodes(converted);
    }

    public void StoreSelectedNodes(List<SelectNodeTreeItem> nodes)
    {
        if (nodes == null || nodes.Count == 0)
        {
            this.addRemoveObjectsControl.SelectedItems = String.Empty;
        }
        else
        {
			this.addRemoveObjectsControl.SelectedItems = JsHelper.Serialize(nodes.ToArray());
        }
    }

    public List<SelectNodeTreeItem> GetSelectedNodes()
    {
        var nodes = new List<SelectNodeTreeItem>();

        if (!String.IsNullOrEmpty(this.addRemoveObjectsControl.SelectedItems))
        {
            SelectNodeTreeItem[] selectedItems = JsHelper.Deserialize<SelectNodeTreeItem[]>(this.addRemoveObjectsControl.SelectedItems);

            nodes.AddRange(selectedItems);
        }

        return nodes;
    }

	public List<int> GetSelectedNodeIds()
	{
	    List<int> nodes = GetSelectedNodes().ConvertAll(o => o.Id);
	    return nodes;
	}
}