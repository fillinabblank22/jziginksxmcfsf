using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;

public enum ProgressIndicatorSize
{
    OneLine,
    ThreeLine
}

public partial class Orion_APM_Admin_AppFinder_ProgressIndicator : UserControl, ITextListControl
{
    private readonly List<string> _steps = new List<string>();
    private string _selectedStep;
    private ProgressIndicatorSize _indicatorSize = ProgressIndicatorSize.ThreeLine;

    
    public List<string> Steps
    {
        get { return _steps; }
    }

    public string SelectedStep
    {
        get { return _selectedStep; }
        set { _selectedStep = value; }
    }

    public ProgressIndicatorSize IndicatorSize
    {
        get { return _indicatorSize; }
        set { _indicatorSize = value; }
    }

    #region ITextListControl Members

    public IList<string> ListItems
    {
        get { return Steps; }
    }

    public string CurrentItem
    {
        get { return this.SelectedStep; }
        set { this.SelectedStep = value; }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
    }

	protected override void OnDataBinding(EventArgs e)
	{
	    base.OnDataBinding(e);
        BuildSteps();
	}

    private void BuildSteps()
    {
        ProgressIndicator.Controls.Clear();

        int selectedIndex = this.SelectedIndex;
        string imageSize = String.Empty;

        if (this.IndicatorSize == ProgressIndicatorSize.OneLine)
        {
            imageSize = "_sm";
        }
        else
        {
            ProgressIndicator.Attributes["class"] += " ProgressIndicatorThreeLine";
        }

        for (int i = 0; i < _steps.Count; ++i)
        {
            string thisItemsState = (selectedIndex == i) ? "on" : "off";
            string nextItemsState = GetNextItemsState(i, selectedIndex);

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "PI_" + thisItemsState;
            div.InnerHtml = (i == 0) ? "&nbsp;" + _steps[i] : _steps[i];

            HtmlImage img = new HtmlImage();
            img.Src = String.Format("/Orion/Images/ProgressIndicator/PI_sep_{0}_{1}{2}.gif", 
                                    thisItemsState, nextItemsState, imageSize);

            ProgressIndicator.Controls.Add(div);
            ProgressIndicator.Controls.Add(img);
        }
    }

    private string GetNextItemsState(int currentIndex, int selectedIndex)
    {
        int nextIndex = currentIndex + 1;
        if (nextIndex == _steps.Count)
            return "off";

        return (selectedIndex == nextIndex) ? "on" : "off";
    }

    private int SelectedIndex
    {
        get
        {
            return Steps.FindIndex(delegate (string s)
            {
                return String.Equals(s, _selectedStep, StringComparison.OrdinalIgnoreCase);
            });
        }
    }

}