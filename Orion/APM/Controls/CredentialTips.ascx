﻿<%@ Control Language="C#" ClassName="CredentialTips" %>

<script runat="server">	    
	public string BackgroundColor
	{
		get; set;
	}
    
	public string BorderColor
	{
		get; set;
	}

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        
        if (!string.IsNullOrEmpty(this.BackgroundColor) || !string.IsNullOrEmpty(this.BorderColor))
            this.credentialsContainer.Attributes.Add("style", string.Format("background-color:{0}; border-color:{1};", this.BackgroundColor, this.BorderColor));
    }
</script>

<orion:Include runat="server" File="APM/APM.css" />

<div class="CredentialTipsInstructions" runat="server" id="credentialsContainer">
    <h2><%= Resources.APMWebContent.APMWEBDATA_AK1_71 %></h2>
    <ul>
        <li><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_72, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %></li> 
        <li><%= Resources.APMWebContent.APMWEBDATA_AK1_73 %></li>
        <li><%= String.Format(Resources.APMWebContent.APMWEBDATA_PV0_3, Resources.APMWebContent.APMWEBCODE_AK1_98) %></li>
        <li><%= Resources.APMWebContent.APMWEBDATA_AK1_74 %></li>
        <li><%= Resources.APMWebContent.APMWEBDATA_AK1_75 %></li>
        <li><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_76, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %></li>
        <li><%= Resources.APMWebContent.APMWEBDATA_AK1_77 %></li>
		<li><a style="text-decoration:underline;color:#369;" href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("OrionAPMPHConfigSettingsAddNewApplicationMonitors") %>" target="_blank">&#0187; <%= Resources.APMWebContent.APMWEBDATA_AK1_78 %></a></li>
    </ul>
</div>