<%@ Control Language="C#" ClassName="ApmIconHelpButton" %>

<script runat="server">
	private string _helpUrlFragment;
    private string _imageSource = "/Orion/APM/images/Icon.Help.gif";

    public string HelpUrlFragment
    {
        get { return _helpUrlFragment; }
        set { _helpUrlFragment = value; }
    }

    public string ImageSource
    {
        get { return _imageSource; }
        set { _imageSource = value; }
    }

    protected string HelpURL
    {
        get
        {
            return SolarWinds.APM.Web.HelpLocator.CreateHelpUrl(this.HelpUrlFragment);
        }
    }
    
   
</script>

<span class="apm_IconHelpButton">
<a class="apm_HelpIcon" href="<%= this.HelpURL %>" target="_blank"><img alt="" src="<%= this.ImageSource %>" /></a>
<a class="apm_HelpText" href="<%= this.HelpURL %>" target="_blank"><%= Resources.APMWebContent.APMWEBDATA_VB1_32 %></a>
</span>