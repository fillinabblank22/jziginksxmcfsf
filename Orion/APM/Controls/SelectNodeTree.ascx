<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectNodeTree.ascx.cs"
    Inherits="Orion_APM_Controls_SelectNodeTree" %>

<orion:Include runat="server" Module="APM" File="js/jquery.cookie.js"/>
<orion:Include runat="server" Module="APM" File="js/GroupTree.js"/>
<orion:Include runat="server" File="APM/APM.css"/>

<script type="text/javascript">
    $().ready(function () {
        SW.APM.SelectNodeTree(
            '#<%= this.GroupByDropDown.ClientID %>',
            '#<%= this.selectedNodes.ClientID %>',
            <%= this.AllowMultipleSelections.ToString().ToLowerInvariant() %>,
            '<%= this.AfterNodeSelectionChangeScript %>',
            <%= this.CookieInfo %>,
            <%= this.ViewNodeFilter %>
        );
    });
</script>

<%= Resources.APMWebContent.APMWEBDATA_VB1_113 %>
<br />
<asp:DropDownList ID="GroupByDropDown" runat="server" AutoPostBack="false" AppendDataBoundItems="true" OnInit="GroupByDropDown_OnInit"/>
<br /><br />
<div id="apm_selectNodeTree" class="<%=this.TreeCssClass %>" style="position: relative; overflow:auto; height:<%=this.TreeHeight %>;width:<%=this.TreeWidth %>;border:<%=this.TreeBorder%>;"></div>
<input type="text" id="selectedNodes" runat="server" style="width:800px;display:none;" value=''/>
