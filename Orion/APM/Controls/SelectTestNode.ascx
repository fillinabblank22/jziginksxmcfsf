﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectTestNode.ascx.cs" Inherits="Orion_APM_Controls_SelectTestNode" %>
<%@ Register Src="~/Orion/APM/Controls/SelectNodeTree.ascx" TagPrefix="apm" TagName="SelectNodeTree" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="atk" %>

<div runat="server" id="SelectNodeDiv"
     style="background-color: #FFF; border: solid 1px #333;z-index:110000; width:500px; height:400px; padding:20px;display:none;">
    <asp:UpdatePanel ID="popupUpdatePanel" runat="server" UpdateMode="conditional">
        <ContentTemplate>
            <apm:SelectNodeTree ID="SelectNodeTree" runat="server" Visible="true"
                                TreeHeight="300px" TreeWidth="100%"
                                AllowMultipleSelections="false"
                                AfterNodeSelectionChangeScript="OnAfterNodeSelectionChange"/>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="float:right;padding: 15px 0px;">
        <asp:ImageButton ID="SelectNodePopUpOk" runat="server" ImageUrl="~/Orion/APM/Images/Admin/WizardButtons/Button.Select.PNG" Enabled="false" CausesValidation="false" />
        <asp:ImageButton ID="SelectNodePopUpCancel" runat="server" ImageUrl="~/Orion/APM/Images/Admin/WizardButtons/Button.Cancel.PNG" CausesValidation="false" />
		<script type="text/javascript">
			APMjs.SelectNodeOkBtnId = "<%=SelectNodePopUpOk.ClientID%>";
		</script>
	</div>
</div>
<atk:ModalPopupExtender ID="SelectNodeModalPopupExtender" runat="server"
    PopupControlID="SelectNodeDiv"
    BehaviorID="SelectNodeModalPopupDialog"
    TargetControlID="SelectNodeDiv"
    CancelControlID="SelectNodePopUpCancel"
    OnCancelScript="APMjs.SelectNode.nodeSelectionCancelClick()"
    OkControlID="SelectNodePopUpOk"
    OnOkScript="APMjs.SelectNode.nodeSelectionOkClick()"
    BackgroundCssClass="apm_modalBackground">
</atk:ModalPopupExtender>

<script type="text/javascript">
function OnAfterNodeSelectionChange(nodeId, nodeCaption, nodeIpAddress, nodeStatus) {
	APMjs.SelectNode.SelectedNodeInfo = { nodeId: nodeId, text: nodeCaption, ipAddress: nodeIpAddress, status: nodeStatus };
    if (nodeIpAddress.length > 0) {
        var okButton = $get("<%=SelectNodePopUpOk.ClientID%>");
        if (okButton) {
            okButton.disabled = false;
        }
    }
}
</script>