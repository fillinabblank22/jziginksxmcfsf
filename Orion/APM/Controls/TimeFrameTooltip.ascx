﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimeFrameTooltip.ascx.cs" Inherits="Orion_APM_Controls_TimeFrameTooltip" %>

<div id="divTimeFrameTooltip" runat="server" class="tooltipContainer">
    <asp:Label ID="lblTitle" runat="server" class="title"/>
    <asp:Image ID="imgIcon" runat="server" class="icon" />
    <br />
    <asp:Label ID="litTimeFrames" runat="server" class="timeFrames" />
</div>
