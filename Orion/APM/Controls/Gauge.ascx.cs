using System;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;


public partial class Orion_APM_Controls_Gauge : System.Web.UI.UserControl
{
    private long gaugeValue;
    private string label;
    private string chartName;
    private string netObjectId;

    protected override void OnInit(EventArgs e)
    {
        // Always hidden by default (unitl we have a value)
        Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        gauge.GaugeStyle = this.GaugeStyle;
        gauge.Value = (double)this.Value;
        gauge.Scale = this.Scale;
        gauge.MinValue = this.MinValue;
        gauge.MaxValue = this.MaxValue;
        gauge.WarningThreshold = (float) this.WarningThreshold;
        gauge.ErrorThreshold = (float) this.CriticalThreshold;
        gauge.ValueFormatString = String.Format("{0} {1}", this.Value, this.Suffix);
        gauge.BottomTitle = this.GaugeLabel;
    }

    public long Value
    {
        get { return gaugeValue; }
        set
        {
            gaugeValue = value;
            Visible = (gaugeValue != long.MinValue);
        }
    }

    public string GaugeLabel
    {
        get { return label; }
        set { label = value; }
    }
    
    public string ChartName
    {
        get { return chartName; }
        set { chartName = value; }
    }

    public string NetObjectId
    {
        get { return netObjectId; }
        set { netObjectId = value; }
    }

    protected string ChartUrl
    {
        get
        {
            SolarWinds.APM.Web.UrlBuilder builder = new SolarWinds.APM.Web.UrlBuilder("/Orion/Apm/CustomChart.aspx");

            builder["ChartName"] = this.ChartName;
            builder["NetObjectId"] = this.NetObjectId;

            return builder.Url;
        }
    }


    #region Constants

    private const string DEFAULT_STYLE = "Simple Circle";
    private const int DEFAULT_SCALE = 90;
    private const double DEFAULT_MIN_VALUE = 0;
    private const double DEFAULT_MAX_VALUE = 100;
    private const double DEFAULT_TICKMARK_INTERVAL = 20;
    private const bool DEFAULT_INVERTED = false;
    private const string DEFAULT_SUFFIX = "";

    #endregion

    #region Public Properties

    public string GaugeStyle
    {
        get { return gaugeStyle; }
        set { gaugeStyle = value; }
    }

    public double MinValue
    {
        get { return minValue; }
        set { minValue = value; }
    }

    public double MaxValue
    {
        get { return maxValue; }
        set { maxValue = value; }
    }

    public double TickmarkInterval
    {
        get { return tickmarkInterval; }
        set { tickmarkInterval = value; }
    }

    public string Suffix
    {
        get { return suffix; }
        set { suffix = value; }
    }

    public int Scale
    {
        get { return scale; }
        set { scale = value; }
    }

    public bool Inverted
    {
        get { return inverted; }
        set { inverted = value; }
    }

    public double WarningThreshold
    {
        get { return warningLevel; }
        set { warningLevel = value; }
    }

    public double CriticalThreshold
    {
        get { return criticalLevel; }
        set { criticalLevel = value; }
    }

    #endregion
    #region Fields

    private string gaugeStyle = DEFAULT_STYLE;
    private string suffix = DEFAULT_SUFFIX;
    private double minValue = DEFAULT_MIN_VALUE;
    private double maxValue = DEFAULT_MAX_VALUE;
    private double tickmarkInterval = DEFAULT_TICKMARK_INTERVAL;
    private int scale = DEFAULT_SCALE;
    private bool inverted = DEFAULT_INVERTED;
    private double warningLevel = double.MaxValue;
    private double criticalLevel = double.MaxValue;

    #endregion

}
