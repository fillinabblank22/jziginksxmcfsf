﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChartImageLink.ascx.cs" Inherits="Orion_APM_Controls_ChartImageLink" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<a id="link" runat="server" target="_blank" tooltip="processed">
			<img id="chartImage" alt="" src="" runat="server"/>
		</a>
	</Content>
</orion:resourceWrapper>
