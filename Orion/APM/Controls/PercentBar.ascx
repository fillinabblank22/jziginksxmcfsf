<%@ Import namespace="System.Globalization"%>
<%@ Control Language="C#" ClassName="PercentBar" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>

<script runat="server">
    private ApmStatistic _value;
    private string _additionalClassName;

    public ApmStatistic Value
    {
        get { return _value; }
        set { _value = value; }
    }

    public string AdditionalClassName
    {
        get { return _additionalClassName; }
        set { _additionalClassName = value; }
    }    
    
    public string Percentage
    {
        get
        {
            if (!_value.IsValid)
                return "0";

            return _value.Percentage.Value.ToString(CultureInfo.InvariantCulture);
        }
    }

    protected string ClassName
    {
        get
        {
			return String.Format("{0:class}", Value);
        }
    }

</script>

<td class="apm_InlineBar <%=ClassName%> <%=AdditionalClassName%>"><h3><div style="width:<%=Percentage%>%; ; height: 10px; font-size: 7px;">&nbsp;</div></h3></td>
