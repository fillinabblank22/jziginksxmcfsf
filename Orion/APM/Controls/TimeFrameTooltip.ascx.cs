﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.APM.Common.Models;

public partial class Orion_APM_Controls_TimeFrameTooltip : System.Web.UI.UserControl
{
    public string Title { get; set; }
    public string IconUrl { get; set; }
    public string TimeFrameName { get; set; }
    public TimeFrame TimeFrame { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            this.lblTitle.Text = this.Title;

            this.imgIcon.ImageUrl = this.IconUrl;
            this.imgIcon.ImageAlign = ImageAlign.Right;

            this.divTimeFrameTooltip.Attributes["class"] += string.Format(" {0}", this.TimeFrameName);

            this.litTimeFrames.Text = this.GetTimeFramesHtml();            
        }
    }

    private string GetTimeFramesHtml()
    {
        StringBuilder html = new StringBuilder();

        if (this.TimeFrame != null)
        {
            if (!this.TimeFrame.StartTime.Equals(DateTime.MinValue) && !this.TimeFrame.EndTime.Equals(DateTime.MinValue))
            {
                // convert to current date, because daylight saving offset isn't correct when using 1900/1/1
                DateTime startDate = DateTime.SpecifyKind(new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, this.TimeFrame.StartTime.Hour, this.TimeFrame.StartTime.Minute, this.TimeFrame.StartTime.Second), DateTimeKind.Utc);
                DateTime endDate = DateTime.SpecifyKind(new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, this.TimeFrame.EndTime.Hour, this.TimeFrame.EndTime.Minute, this.TimeFrame.EndTime.Second), DateTimeKind.Utc);
                html.Append(startDate.ToLocalTime().ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append(" - ");
                html.AppendLine(endDate.ToLocalTime().ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append("<br/>");
                html.Append(this.GetDaysString(this.TimeFrame.GetTimeFrameDays(false)));
            }

            if (this.TimeFrame.HasWholeDays())
            {
                if (!this.TimeFrame.StartTime.Equals(DateTime.MinValue) && !this.TimeFrame.EndTime.Equals(DateTime.MinValue))
                {
                    html.Append("<br/><br/>");
                }
                html.AppendLine(new DateTime(1900, 1, 1, 0, 0, 0).ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append(" - ");
                html.AppendLine(new DateTime(1900, 1, 1, 23, 59, 59).ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append("<br/>");
                html.Append(this.GetDaysString(this.TimeFrame.GetTimeFrameDays(true)));
            }
        }

        return html.ToString();
    }

    private string GetDaysString(List<DayNames> dayNames)
    {        
        // just one day
        if (dayNames.Count == 1)
        {
            return dayNames[0].ToString();
        }
                
        // common weekend
        if (dayNames.Count == 2)
        {
            return string.Format("{0} and {1}", dayNames[0].Equals(DayNames.Sunday) ? dayNames[1].ToString() : dayNames[0].ToString(), dayNames[0].Equals(DayNames.Sunday) ? dayNames[0].ToString() : dayNames[1].ToString());
        }

        // common weekdays
        if (dayNames.Count == 5 && dayNames.Contains(DayNames.Monday) && dayNames.Contains(DayNames.Tuesday) && dayNames.Contains(DayNames.Wednesday) && dayNames.Contains(DayNames.Thursday) && dayNames.Contains(DayNames.Friday))
        {
            return string.Format("{0} - {1}", DayNames.Monday.ToString(), DayNames.Friday.ToString());
        }

        // general
        return string.Join(", ", dayNames);
    }
}