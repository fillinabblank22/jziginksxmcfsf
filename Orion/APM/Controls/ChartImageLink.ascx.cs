﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;

public partial class Orion_APM_Controls_ChartImageLink : System.Web.UI.UserControl, IChartLinkControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region IChartLinkControl Members

    public void SetLinkUrl(string url)
    {
        link.HRef = url;
    }

    public System.Web.UI.HtmlControls.HtmlImage GetImageControl()
    {
        return chartImage;
    }

    #endregion
}
