﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignGaugesToComponent.ascx.cs" Inherits="Orion_APM_Controls_AssignGaugesToComponent" %>

<script type="text/javascript">
	$().ready(function () {
       	function setDisabled() {
       		$("input:text[id$=ctrMaxScale],input:text[id$=ctrUnitsValue],input:text[id$=ctrUnitsLabel]").attr("disabled", this.value != "SD" && this.value != "RT");
       	}
       	
		var el = $("select[id$=ctrGaugeTypes]");
       	el.load(setDisabled);
       	el.change(setDisabled);
       	el.change(function () {
       	    var text = $(":selected", this).text();
       	    $("h1#editGaugeTitle").text(String.format("<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_AK1_1) %>", text));
       	    $("input:text[id$=resourceTitleEditor_txtTitle]").val(String.format("<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_AK1_2) %>", text));
       	});
       	el.load();
	})
</script>

<orion:Include runat="server" File="APM/APM.css"/>

<asp:CustomValidator ID="ctrCV" ErrorMessage="*" Display="None" OnServerValidate="On_ServerValidate" runat="server"/>
<b><%= Resources.APMWebContent.APMWEBDATA_AK1_203 %></b>
<asp:CheckBoxList ID="ctrComponents" DataTextField="Name" DataValueField="Id" runat="server"/><br/>
<b><%= Resources.APMWebContent.APMWEBDATA_AK1_204 %></b><br/>
<asp:DropDownList ID="ctrGaugeTypes" DataTextField="Value" DataValueField="Key" runat="server"/>
