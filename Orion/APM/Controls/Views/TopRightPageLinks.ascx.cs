﻿using System;

using SolarWinds.APM.Web;

public partial class Orion_APM_Controls_Views_TopRightPageLinks : System.Web.UI.UserControl
{
    public string CustomizeViewHref { get; set; }

    public string EditNetObjectHref { get; set; }

    public string EditNetObjectText { get; set; }

    public string HelpUrlFragment { get; set; }

    public bool EditNetObjectVisible { get; set; }

    public Orion_APM_Controls_Views_TopRightPageLinks()
    {
        EditNetObjectVisible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // customize
        if (Profile.AllowCustomize && !string.IsNullOrWhiteSpace(CustomizeViewHref))
        {
            lnkCustomize.NavigateUrl = CustomizeViewHref;
            lnkCustomize.Text = Resources.APMWebContent.APMWEBDATA_TM0_1;
        }
        else
        {
            lnkCustomize.Visible = false;
        }

        // edit
        if (ApmRoleAccessor.AllowAdmin && !string.IsNullOrWhiteSpace(EditNetObjectHref) && EditNetObjectVisible)
        {
            lnkEditNetObject.NavigateUrl = EditNetObjectHref;
            lnkEditNetObject.Text = string.IsNullOrWhiteSpace(EditNetObjectText) ? "Edit" : EditNetObjectText; //TODO localization
        }
        else
        {
            lnkEditNetObject.Visible = false;
        }
        
        // datetime
        this.lblDateTime.Text = DateTime.Now.ToString("F");

        // help
        this.btnHelp.HelpUrlFragment = HelpUrlFragment;
    }

}