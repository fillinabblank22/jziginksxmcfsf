﻿using System;
using System.Globalization;

public partial class Orion_APM_Controls_Views_ViewTitle : System.Web.UI.UserControl
{
    public string ViewTitle { get; set; }

    public string ViewSubTitle { get; set; }

    public StatusProviderInfo StatusIconInfo { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        // title
        this.lblViewTitle.Text = this.ViewTitle;
        this.lblSubTitle.Text = this.ViewSubTitle;

        // status icon
        //TODO: cannot we use StatusIconProvider?
        if (this.StatusIconInfo != null)
            this.imgStatus.ImageUrl = string.Format(CultureInfo.InvariantCulture, "/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small", this.StatusIconInfo.FullSwisEntityName, this.StatusIconInfo.StatusId);
        else
            this.imgStatus.Visible = false;
    }

    public class StatusProviderInfo
    {
        public StatusProviderInfo(string fullSwisEntityName, int statusId)
        {
            FullSwisEntityName = fullSwisEntityName;
            StatusId = statusId;
        }

        public string FullSwisEntityName { get; private set; }

        public int StatusId { get; private set; }
    }

}