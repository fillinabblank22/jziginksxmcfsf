﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopRightPageLinks.ascx.cs" Inherits="Orion_APM_Controls_Views_TopRightPageLinks" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>
<div class="topRightPageLinks">
    <style>
        .custPageLink {
            background-image: url('/Orion/APM/Images/Page.Icon.CustomPg.gif') !important;
        }
    </style>
    <asp:HyperLink runat="server" ID="lnkCustomize" CssClass="custPageLink"></asp:HyperLink>
    <asp:HyperLink runat="server" ID="lnkEditNetObject" CssClass="editObjectLink"></asp:HyperLink>
	<orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment=""/>
	<br />
    <div class="datetime">
	    <asp:Label ID="lblDateTime" runat="server" />
    </div>
</div>
