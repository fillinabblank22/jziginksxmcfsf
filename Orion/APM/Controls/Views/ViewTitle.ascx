﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewTitle.ascx.cs" Inherits="Orion_APM_Controls_Views_ViewTitle" %>
<div class="title">
    <asp:Label ID="lblViewTitle" runat="server" />
    &nbsp;-&nbsp;
    <asp:Image ID="imgStatus" runat="server" alt="status_icon" />
    <asp:Label ID="lblSubTitle" runat="server" />
</div>           
