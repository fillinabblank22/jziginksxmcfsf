﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignComponentToResource.ascx.cs" Inherits="Orion_APM_Controls_AssignComponentToResource" %>

<asp:Panel ID="pnlSelectComponent" runat="server">
    <%= Resources.APMWebContent.APMWEBCODE_VB1_130 %>
    <asp:DropDownList ID="ctrComponents" DataTextField="Name" DataValueField="Id" AutoPostBack="true" OnSelectedIndexChanged="CtrlComponents_OnSelectedIndexChanged" runat="server"/>
    <br /><br />
</asp:Panel>
<asp:Label ID="lblNoComponent" runat="server" CssClass="apm_ErrorMessage" Text="<%$ Resources: APMWebContent, APMWEBDATA_PS0_8 %>"></asp:Label>