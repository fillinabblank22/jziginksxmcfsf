﻿using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.DisplayStrategies;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_APM_Controls_Management_RealTimeTasksPlugin : UserControl, IManagementResourcePlugin
{
    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private const string LogMessageTemplate = "Creating management task '{0}' in section '{1}' for NetObject {2}";

    private const string ImgSrcTemplate = "<img src='{0}' class='apmNodeManagementTask' />&nbsp;{1}";

    public IEnumerable<ManagementTaskItem> GetManagementTasks(ApmBaseResource resource, NetObject netObject, string returnUrl)
    {
        var appProvider = resource.GetInterfaceInstance<IApplicationProviderBase>();
        var app = (appProvider != null) ? appProvider.ApmApplication : null;
        var node = (app != null) ? app.Node : null;
        if (node == null)
        {
            yield break;
        }

        var sectionName = Resources.CoreWebContent.WEBDATA_AK0_171;

        var dispStrategy = CreateDisplayStrategy(node);

        if (ApmRoleAccessor.AllowRealTimeProcessExplorer && dispStrategy.IsProcessExplorerSupported())
        {
            log.DebugFormat(LogMessageTemplate, "RTPE", sectionName, netObject.NetObjectID);
            const string JsOnClickFormat = "return SW.APM.NodeManagement.openRealTimeProcessWindow({0});";
            yield return new ManagementTaskItem
            {
                Section = sectionName,
                LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, "/Orion/APM/Images/RT_process_explorer.png", Resources.APMWebContent.APMWEBDATA_VB1_298),
                OnClick = InvariantString.Format(JsOnClickFormat, node.Id)
            };
        }

        if (ApmRoleAccessor.AllowRealTimeServiceExplorer && dispStrategy.IsServiceManagerSupported())
        {
            log.DebugFormat(LogMessageTemplate, "SCM", sectionName, netObject.NetObjectID);
            const string JsOnClickFormat = "return SW.APM.NodeManagement.openRealTimeServicesWindow({0},{1});";
            yield return new ManagementTaskItem
            {
                Section = sectionName,
                LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, "/Orion/APM/Images/RT_service_manager.png", Resources.APMWebContent.APMWEBDATA_RM0_7),
                OnClick = InvariantString.Format(JsOnClickFormat, node.Id, dispStrategy.GetWorkingWindowsCredentialId())
            };
        }

        if (ApmRoleAccessor.AllowRealTimeEventExplorer && dispStrategy.IsEventExplorerSupported())
        {
            log.DebugFormat(LogMessageTemplate, "RTEV", sectionName, netObject.NetObjectID);
            const string JsOnClickFormat = "return SW.APM.NodeManagement.openRealTimeEventExplorerWindow({0},{1});";
            yield return new ManagementTaskItem
            {
                Section = sectionName,
                LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, "/Orion/APM/Images/RT_event_viewer.png", Resources.APMWebContent.APMWEBDATA_VB1_343),
                OnClick = InvariantString.Format(JsOnClickFormat, node.Id, dispStrategy.GetWorkingWindowsCredentialId())
            };
        }
    }

    private INodeManagementTasksDisplayStrategy CreateDisplayStrategy(Node node)
    {
        var tasksDisplayStrategyFactory = ServiceLocatorForWeb.GetServiceForWeb<ITasksDisplayStrategyFactory>();
        return tasksDisplayStrategyFactory.CreateTasksDisplayStrategy(node);
    }
}
