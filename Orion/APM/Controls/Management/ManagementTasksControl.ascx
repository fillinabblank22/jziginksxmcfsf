﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagementTasksControl.ascx.cs" Inherits="Orion_APM_Controls_Management_ManagementTasksControl" %>
<%@ Register TagPrefix="orion" TagName="ManagementTasksRenderer" Src="~/Orion/NetPerfMon/Controls/ManagementTasksControlRenderer.ascx" %>

<orion:Include Module="APM" File="ManagementTasksDropDown.css" runat="server"/>

<orion:ManagementTasksRenderer ID="ManagementTasksRenderer" runat="server" />

<asp:PlaceHolder runat="server" ID="ResourcePlaceHolder" />
