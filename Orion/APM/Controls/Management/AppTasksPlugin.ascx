﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppTasksPlugin.ascx.cs" Inherits="Orion_APM_Controls_Management_AppManagementTasksPlugin" %>
<%@ Register TagPrefix="orion" TagName="ProgressDialog" Src="~/Orion/Controls/ProgressDialog.ascx"%>
<%@ Register TagPrefix="orion" TagName="MaintenanceSchedulerDialog" Src="~/Orion/Controls/MaintenanceSchedulerDialog.ascx"%>
<%@ Register TagPrefix="orion" TagName="DateTimePicker" Src="~/Orion/Controls/DateTimePicker.ascx" %>


<orion:ProgressDialog runat="server" />
<orion:Include runat="server" File="NodeMNG.css"  />
<orion:Include runat="server" File="MaintenanceMode.js" Module="APM" />
<orion:MaintenanceSchedulerDialog ID="maintenanceSchedulerDialog" runat="server" />