﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_Management_ManagementTasksControl : System.Web.UI.UserControl
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private readonly List<IManagementResourcePlugin> plugins = new List<IManagementResourcePlugin>();

    private object ManagementTaskControls { get; set; }

    private IEnumerable<string> availableSections = new List<string>();
    public IEnumerable<string> AvailableSections
    {
        get { return availableSections; }
        set { availableSections = value; }
    }

    private IEnumerable<string> disabledSections = new List<string>();
    public IEnumerable<string> DisabledSections
    {
        get { return disabledSections; }
        set { disabledSections = value; }
    }

    public bool CombineSections { get; set; }

    public string ManagementTaskControlsSerialized
    {
        get
        {
            var jsSerializer = new JavaScriptSerializer();
            return jsSerializer.Serialize(this.ManagementTaskControls);
        }
    }

    public NetObject NetObject { get; set; }

    public ApmBaseResource Resource { get; set; }

    public void AddManagementTasksControls()
    {
        if (Resource == null)
        {
            throw new ApplicationException("Resource is not assigned");
        }

        if (NetObject == null)
        {
            throw new ApplicationException("NetObject is not assigned");
        }

        LoadPlugins(NetObject);

        var taskItems = this.plugins.SelectMany(plugin => plugin.GetManagementTasks(Resource, NetObject, "").Select(item =>
        {
            item.Section = item.Section.ToUpper();
            return item;
        })).ToList();

        this.AvailableSections = taskItems.Select(i => i.Section).Distinct().ToList();

        taskItems = taskItems.Where(i => !this.DisabledSections.Contains(i.Section)).ToList();
        if (CombineSections)
        {
            taskItems.ForEach(i => i.Section = Resources.APMWebContent.Management_Combined.ToUpper());
        }

        this.Visible = this.Visible && this.AvailableSections.Any();

        this.ManagementTaskControls = taskItems.GroupBy(task => task.Section)
            .Select(g => new { Title = g.Key, Tasks = g.ToList() }).ToList();
    }

    private void LoadPlugins(NetObject netObject)
    {
        var prefix = NetObjectHelper.GetPrefix(netObject.NetObjectID);
        foreach (var controlPath in WebPluginManager.Instance.SupportedPlugins.SelectMany(p => p.GetManagementPluginPaths(prefix)).Distinct())
        {
            try
            {
                var ctrl = LoadControl(controlPath);
                var plugin = ctrl as IManagementResourcePlugin;
                if (plugin != null)
                {
                    ResourcePlaceHolder.Controls.Add(ctrl);
                    this.plugins.Add(plugin);
                    log.DebugFormat("Plugin '{0}' was loaded.", controlPath);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error when loading plugin '{0}'. Exception: {1}", controlPath, ex);
            }
        }
    }
}