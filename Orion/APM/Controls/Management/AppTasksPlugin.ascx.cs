﻿using System.Collections.Generic;
using System;
using System.Web;
using System.Web.UI;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Models.Alerts;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_APM_Controls_Management_AppManagementTasksPlugin : UserControl, IManagementResourcePlugin
{
    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private const string LogMessageTemplate = "Creating management task '{0}' in section '{1}' for NetObject {2}";

    private const string ImgSrcTemplate = "<img src='{0}' />&nbsp;{1}";
    private const string Img16x16SrcTemplate = "<img src='{0}' height=\'16\' width=\'16\' />&nbsp;{1}";

    private readonly string maintenanceModeGroupLabel = InvariantString.Format("<img src='/Orion/Nodes/images/icons/maintenance_mode_icon16.png' alt='' />&nbsp;{0}", Resources.CoreWebContent.WEBDATA_MH0_01);

    private static readonly IList<EntityAlertSuppressionMode> ShowSuppressAlertsNowLinkModes = new List<EntityAlertSuppressionMode>
    {
        EntityAlertSuppressionMode.NotSuppressed,
        EntityAlertSuppressionMode.SuppressionScheduledForItself,
        EntityAlertSuppressionMode.SuppressionScheduledForParent
    };

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ForceMaintenanceSchedulerDateTimePickerMarkupClientScriptBlockUsage();
    }

    /// <summary>
    /// When ResourceLoadingMode is set to RenderControl, the ClientScript is not inserted to the page and ForceMarkupClientScriptBlockUsage
    /// must be set to overcome the issue
    /// </summary>
    private void ForceMaintenanceSchedulerDateTimePickerMarkupClientScriptBlockUsage()
    {
        var datePickerFrom = this.maintenanceSchedulerDialog.FindControl("periodFrom") as Orion_Controls_DateTimePicker;
        if (datePickerFrom != null)
        {
            datePickerFrom.ForceMarkupClientScriptBlockUsage = true;
        }

        var datePickerUntil = this.maintenanceSchedulerDialog.FindControl("periodUntil") as Orion_Controls_DateTimePicker;
        if (datePickerUntil != null)
        {
            datePickerUntil.ForceMarkupClientScriptBlockUsage = true;
        }
    }

    public IEnumerable<ManagementTaskItem> GetManagementTasks(ApmBaseResource resource, NetObject netObject, string returnUrl)
    {
        var applicationProviderBase = resource.GetInterfaceInstance<IApplicationProviderBase>();
        ApmApplicationBase app = applicationProviderBase != null ? applicationProviderBase.ApmApplication : null;
        if (app == null || OrionConfiguration.IsDemoServer)
        {
            yield break;
        }

        string sectionName = Resources.APMWebContent.Management_Application;

        bool isApmAdmin = ApmRoleAccessor.AllowAdmin;

        if (isApmAdmin)
        {
            yield return CreateEditTaskItem(sectionName, app);
        }

        if (Profile.AllowUnmanage)
        {
            if (!app.NPMNode.UnManaged)
            {
                yield return CreateUnmanageTaskItem(app, sectionName);
            }

            if (app.AlertSuppressionState.SuppressionMode == EntityAlertSuppressionMode.SuppressedByItself)
            {
                yield return CreateResumeAlertsTaskItem(sectionName, app);
            }

            if (ShowSuppressAlertsNowLinkModes.Contains(app.AlertSuppressionState.SuppressionMode))
            {
                yield return CreateMuteAlertsTaskItem(sectionName, app);
            }

            yield return CreateScheduleMaintananceTaskItem(sectionName, app);
        }

        if (isApmAdmin && app.Status.Value != Status.Unmanaged)
        {
            yield return CreatePollNowTaskItem(sectionName, app);
        }

        yield return CreatePerfstackTaskItem(sectionName, app);
    }

    private ManagementTaskItem CreateScheduleMaintananceTaskItem(string sectionName, ApmApplicationBase app)
    {
        return new ManagementTaskItem
        {
            Section = sectionName,
            Group = maintenanceModeGroupLabel,
            ClientID = "scheduleMaintenanceLnk",
            LinkInnerHtml = Resources.CoreWebContent.WEBDATA_LH0_17,
            OnClick = OrionConfiguration.IsDemoServer ? GetDemoActionCallback("Core_Alerting_Schedule") : InvariantString.Format("SW.APM.MaintenanceMode.MaintenanceScheduler.ShowDialogForSingleEntity('{0}', '{1}', '{2}'); return false;", ControlHelper.EncodeJsString(app.SwisUri), app.Id, ControlHelper.EncodeJsString(HttpUtility.HtmlEncode(app.Name)))
        };
    }

    private ManagementTaskItem CreateMuteAlertsTaskItem(string sectionName, ApmApplicationBase app)
    {
        return new ManagementTaskItem
        {
            Section = sectionName,
            Group = maintenanceModeGroupLabel,
            MouseHoverTooltip = Resources.CoreWebContent.WEBDATA_MH0_02,
            ClientID = "suppressAlertsNowLink",
            LinkInnerHtml = Resources.CoreWebContent.WEBDATA_LH0_16,
            OnClick = OrionConfiguration.IsDemoServer ? GetDemoActionCallback("Core_Alerting_Suppress") : InvariantString.Format("return SW.Core.MaintenanceMode.SuppressAlertsNowSingleEntity('{0}')", ControlHelper.EncodeJsString(app.SwisUri))
        };
    }

    private ManagementTaskItem CreateEditTaskItem(string sectionName, ApmApplicationBase app)
    {
        log.DebugFormat(LogMessageTemplate, "Edit", sectionName, app.NetObjectID);
        return new ManagementTaskItem
        {
            Section = sectionName,
            LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, "/Orion/Nodes/images/icons/icon_edit.gif", Resources.APMWebContent.APMWEBDATA_VB1_293),
            LinkUrl = ApmMasterPage.GetEditApplicationPageUrl(app.Id)
        };
    }

    private ManagementTaskItem CreateUnmanageTaskItem(ApmApplicationBase app, string sectionName)
    {
        log.DebugFormat(LogMessageTemplate, "Manage", sectionName, app.NetObjectID);
        string toggleMethod, toggleText, toggleId;
        if (app.Status.Value == Status.Unmanaged)
        {
            toggleMethod = "SW.APM.MaintenanceMode.ManageAgainSingleEntity";
            toggleText = Resources.APMWebContent.MaintenanceMode_ManageAgain;
            toggleId = "remanageLink";
        }
        else
        {
            toggleMethod = "SW.APM.MaintenanceMode.UnmanageNowSingleEntity";
            toggleText = Resources.APMWebContent.MaintenanceMode_UnmanageNow;
            toggleId = "unmanageNowLink";
        }

        return new ManagementTaskItem
        {
            Section = sectionName,
            Group = maintenanceModeGroupLabel,
            LinkInnerHtml = toggleText,
            OnClick = InvariantString.Format("return {0}({1});", toggleMethod, app.Id),
            ClientID = toggleId
        };
    }

    private ManagementTaskItem CreateResumeAlertsTaskItem(string sectionName, ApmApplicationBase app)
    {
        return new ManagementTaskItem
        {
            Section = sectionName,
            Group = maintenanceModeGroupLabel,
            ClientID = "resumeAlertsLink",
            LinkInnerHtml = Resources.CoreWebContent.WEBDATA_LH0_15,
            OnClick = OrionConfiguration.IsDemoServer ? GetDemoActionCallback("Core_Alerting_Resume") : InvariantString.Format("return SW.Core.MaintenanceMode.ResumeAlertsSingleEntity('{0}')", ControlHelper.EncodeJsString(app.SwisUri))
        };
    }

    private ManagementTaskItem CreatePollNowTaskItem(string sectionName, ApmApplicationBase app)
    {
        log.DebugFormat(LogMessageTemplate, "Reload", sectionName, app.NetObjectID);
        const string jsOnClickFormat =
            "return SW.APM.PollApp.pollNow({{ selItems: [{{ id: '{0}', name: '{1}' }}], onComplete: function () {{ window.location.reload(true); }}}});";
        return new ManagementTaskItem
        {
            Section = sectionName,
            LinkInnerHtml = InvariantString.Format(ImgSrcTemplate, "/Orion/images/pollnow_16x16.gif", Resources.APMWebContent.APMWEBDATA_VB1_296),
            OnClick = InvariantString.Format(jsOnClickFormat, app.Id, HttpUtility.HtmlEncode(app.Name))
        };
    }

    private static ManagementTaskItem CreatePerfstackTaskItem(string sectionName, ApmApplicationBase app)
    {
        return new ManagementTaskItem
        {
            Section = sectionName,
            LinkInnerHtml = InvariantString.Format(Img16x16SrcTemplate, "/Orion/PerfStack/images/icons/perfstack_launch.svg", Resources.APMWebContent.AppTasksPlugin_PerformanceAnalyzer),
            LinkUrl = app.GetPerformanceAnalyzerUrl()
        };
    }

    protected string GetDemoActionCallback(string identifier)
    {
        return InvariantString.Format("demoAction('{0}');return false;", identifier);
    }
}
