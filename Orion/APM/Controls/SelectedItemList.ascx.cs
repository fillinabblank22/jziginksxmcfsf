﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_APM_Controls_SelectedItemList : System.Web.UI.UserControl
{
    private IEnumerable<string> _items;
    private const int ItemsToShow = 5;

    protected Orion_APM_Controls_SelectedItemList()
    {
        this.ViewAllFormatText = "View all {0} nodes";
        this.HeaderFormatText = "The following {0} node{1} were selected: ";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int count = _items.Count();
            HeaderLabel.Text = String.Format(this.HeaderFormatText, count, (count == 1) ? "" : "s");
            ViewAll.InnerText = String.Format(this.ViewAllFormatText, count);
            ViewAll.Attributes["onclick"] = String.Format("return SW.APM.ShowSelectedItemList('{0}');", AllItemsAsString);

            foreach (var item in _items.Take(ItemsToShow))
            {
                ItemList.Items.Add(item);
            }

            if (count <= ItemsToShow)
            {
                // We are showing all the items already
                ViewAllRegion.Visible = false;
            }
        }
    }

    public string HeaderFormatText { get; set; }
    public string ViewAllFormatText { get; set; }

    public IEnumerable<string> Items
    {
        get { return _items; }
        set { _items = value; }
    }

    protected string AllItemsAsString => string.Join(",", _items.ToArray()).Replace("'", "\\'");
}
