﻿<%@ Control Language="C#" ClassName="ApmStatusBar" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>

<orion:InlineCss ID="i1" runat="server">
.apm-status-bar {
	height: 12px; 
	display: inline-block; 
	border: none; 
	padding: 0px;
	margin-top: 2px;
	margin-left: 10px;
	background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%); 
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eeeeee), color-stop(100%,#cccccc)); 
	background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%); 
	background: -o-linear-gradient(top, #eeeeee 0%,#cccccc 100%); 
	background: -ms-linear-gradient(top, #eeeeee 0%,#cccccc 100%); 
	background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); 
}
.apm-status-bar .status-bar-value {
	height: 12px; 
}
.apm-status-bar .status-bar-value.Available {
	background: -moz-linear-gradient(top, #77BD2D 0%, #77BD2D 100%); 
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#77BD2D), color-stop(100%,#77BD2D)); 
	background: -webkit-linear-gradient(top, #77BD2D 0%,#77BD2D 100%); 
	background: -o-linear-gradient(top, #77BD2D 0%,#77BD2D 100%); 
	background: -ms-linear-gradient(top, #77BD2D 0%,#77BD2D 100%); 
	background: linear-gradient(to bottom, #77BD2D 0%,#77BD2D 100%); 
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#77BD2D', endColorstr='#77BD2D',GradientType=0 ); 
}
.apm-status-bar .status-bar-value.Warning {
	background: -moz-linear-gradient(top, #FCD928 0%, #FCD928 100%); 
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FCD928), color-stop(100%,#FCD928)); 
	background: -webkit-linear-gradient(top, #FCD928 0%,#FCD928 100%); 
	background: -o-linear-gradient(top, #FCD928 0%,#FCD928 100%); 
	background: -ms-linear-gradient(top, #FCD928 0%,#FCD928 100%); 
	background: linear-gradient(to bottom, #FCD928 0%,#FCD928 100%); 
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FCD928', endColorstr='#FCD928',GradientType=0 ); 
}
.apm-status-bar .status-bar-value.Critical {
	background: -moz-linear-gradient(top, #F93333 0%, #F93333 100%); 
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F93333), color-stop(100%,#F93333)); 
	background: -webkit-linear-gradient(top, #F93333 0%,#F93333 100%); 
	background: -o-linear-gradient(top, #F93333 0%,#F93333 100%); 
	background: -ms-linear-gradient(top, #F93333 0%,#F93333 100%); 
	background: linear-gradient(to bottom, #F93333 0%,#F93333 100%); 
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#F93333', endColorstr='#F93333',GradientType=0 ); 
}
</orion:InlineCss>

<script runat="server">

	public Status? BarStatus { get; set; }

	public int? BarWidth { get; set; }
	
	public int? BarValueWidth { get; set; }
	
</script>

<div class="apm-status-bar" style="width: <%= BarWidth ?? 50 %>%;">
	<div class="status-bar-value <%= BarStatus ?? Status.Undefined %>" style="width: <%= BarValueWidth ?? 0 %>%;">
	</div>
</div>
