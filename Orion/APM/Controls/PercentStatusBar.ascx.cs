﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_APM_Controls_PercentStatusBar : System.Web.UI.UserControl
{
    #region Properties

    private bool reverse;
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Reverse
    {
        get { return reverse; }
        set { reverse = value; }
    }

    private double usedSpacePercentage;
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public double DbFileUsagePercentage
    {
        get { return this.usedSpacePercentage; }
        set { this.usedSpacePercentage = RangeCheck(value); }
    }

    private double _avalableSpacePercentage;
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public double AvailableSpacePercentage
    {
        get { return this._avalableSpacePercentage; }
        set { this._avalableSpacePercentage = RangeCheck(value); }
    }

    private double _usedSpaceWarningLevel;
    [PersistenceMode(PersistenceMode.Attribute)]
    public double UsedSpaceWarningLevel
    {
        get { return _usedSpaceWarningLevel; }
        set { _usedSpaceWarningLevel = RangeCheck(value); }
    }

    private double _usedSpaceErrorLevel;
    [PersistenceMode(PersistenceMode.Attribute)]
    public double UsedSpaceErrorLevel
    {
        get { return _usedSpaceErrorLevel; }
        set { _usedSpaceErrorLevel = RangeCheck(value); }
    }

    private double _availableSpaceWarningLevel;
    [PersistenceMode(PersistenceMode.Attribute)]
    public double AvailableSpaceWarningLevel
    {
        get { return _availableSpaceWarningLevel; }
        set { _availableSpaceWarningLevel = RangeCheck(value); }
    }

    private double _availableSpaceErrorLevel;
    [PersistenceMode(PersistenceMode.Attribute)]
    public double AvailableSpaceErrorLevel
    {
        get { return _availableSpaceErrorLevel; }
        set { _availableSpaceErrorLevel = RangeCheck(value); }
    }

    // show = true (visible normaly), show = false (&nbsp; instead of control)
    private bool show = true;
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Show
    {
        get { return show; }
        set { show = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public Unit Width { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public Unit Height { get; set; }

    #endregion

    public double RangeCheck(double value)
    {
        double result = value;
        if (result < 0) result = 0;
        if (result > 100) result = 100;
        return result;
    }

    private Unit Subst(Unit u, int px)
    {
        return Unit.Pixel((int)(u.Value) - px);
    }

    protected override void OnInit(EventArgs e)
    {
        if ((this.Width == Unit.Empty)) this.Width = Unit.Percentage(100);
        if ((this.Height == Unit.Empty)) this.Height = Unit.Percentage(100);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var size = Math.Round(this.DbFileUsagePercentage + this.AvailableSpacePercentage);
        var availableSpace = Math.Round(size > 0 ? 100 * this.AvailableSpacePercentage / (size) : 0);
        if (Reverse)
        {
            if (size < UsedSpaceErrorLevel || availableSpace < AvailableSpaceErrorLevel)
                this.UsedSpacePanel.CssClass = "RedBar";
            else if (size < UsedSpaceWarningLevel || availableSpace < AvailableSpaceWarningLevel)
                this.UsedSpacePanel.CssClass = "YellowBar";
            else
                this.UsedSpacePanel.CssClass = "GreenBar";
        }
        else
        {
            if (size > UsedSpaceErrorLevel || availableSpace > AvailableSpaceErrorLevel)
                this.UsedSpacePanel.CssClass = "RedBar";
            else if (size > UsedSpaceWarningLevel || availableSpace > AvailableSpaceWarningLevel)
                this.UsedSpacePanel.CssClass = "YellowBar";
            else
                this.UsedSpacePanel.CssClass = "GreenBar";
        }
        this.AvailableSpacePanel.CssClass = "GreyBar";

        this.OuterBorder.Width = Width;
        this.UsedSpacePanel.Width = Unit.Percentage(Convert.ToInt32(Math.Round(this.DbFileUsagePercentage * (Width.Value / 100))));
        this.AvailableSpacePanel.Width = Convert.ToInt32(Math.Round(size*(Width.Value/100))) < Width.Value
            ? Unit.Percentage(Convert.ToInt32(Math.Round(this.AvailableSpacePercentage*(Width.Value/100))))
            : Unit.Percentage(Convert.ToInt32(this.Width.Value - UsedSpacePanel.Width.Value));

        this.OuterBorder.Height = this.UsedSpacePanel.Height = this.AvailableSpacePanel.Height = Height;
        
        if (this.DbFileUsagePercentage == 0) UsedSpacePanel.BackColor = Color.Transparent;
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.Show)
        {
            this.OuterBorder.Visible = false;
            this.Controls.Add(new LiteralControl("&nbsp;"));
        }
        base.OnPreRender(e);
    }
}