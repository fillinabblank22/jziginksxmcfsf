﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Web;

public partial class Orion_APM_Controls_ExtJsDropDown : System.Web.UI.UserControl, IResourceInfo
{
    [PersistenceModeAttribute(PersistenceMode.Attribute)]
    public int ResourceID { get; set; }

    [PersistenceModeAttribute(PersistenceMode.Attribute)]
    public string Period { get; set; }

    [PersistenceModeAttribute(PersistenceMode.Attribute)]
    public string QueryStringExtension { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        if (String.IsNullOrEmpty(this.Period))
            this.Period = "Today";
    }
}