﻿using System;
using SolarWinds.APM.Common.Models.Charting;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Controls_DataSourceTypeSupport : System.Web.UI.UserControl
{
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get
        {
            if (_resource == null)
            {
                Int32 id;
                if (Int32.TryParse(Request.QueryStringOrForm("ResourceID"), out id))
                {
                    _resource = ResourceManager.GetResourceByID(id);
                }
            }
            return _resource;
        }
    }

    private Int32 AppID
    {
        get
        {
            Int32 id;
            if (Int32.TryParse(Request.QueryStringOrForm("AppID"), out id))
            {
                return id;
            }
            var netObj = Request.QueryStringOrForm("NetObject");
            
            if (Resource != null && NetObjectManager.IsCustomObjectResource(Resource))
            {
                netObj = Resource.Properties["netobjectid"];
            }

            if (NetObjectHelper.IsApplication(netObj))
            {
                return (Int32)NetObjectHelper.GetId(netObj);
            }
            return Int32.MinValue;
        }
    }

    public event EventHandler OnSelectedDataSourceTypeChanged;

    protected void CtrlComponents_OnSelectedDataSourceTypeChanged(object sender, EventArgs e)
    {
        if (this.OnSelectedDataSourceTypeChanged != null)
        {
            OnSelectedDataSourceTypeChanged(this, EventArgs.Empty);
        }
    }

    public string SelectedDataSourceType
    {
        get { return DataSourceTypeList.SelectedValue; }
    }

    public void Initialize()
    {
        Initialize(AppID);
    }

    public void Initialize(int appID)
    {
        DataSourceTypeList.DataSource = DataSourceTypes.GenerateSelectionList(appID);
        DataSourceTypeList.DataBind();
        HtmlHelper.SetListSelectedValue(DataSourceTypeList, GetDataSourceType(), ChartInfo.DefaultDataSourceType);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private string GetDataSourceType()
    {
        var dataSourceType = string.Empty;

        if (Resource != null)
        {
            var chartName = Resource.Properties["ChartName"];

            if (string.IsNullOrEmpty(chartName))
                dataSourceType = string.Empty;
            else if (chartName.Equals(ChartNames.AvailabilityCustomBarChart) ||
                chartName.Equals(ChartNames.AvailabilityCustomLineChart) ||
                chartName.Equals(ChartNames.AvailabilityCustomAreaChart))
            {
                dataSourceType = DataSourceTypes.Availability;
            }
            else if (chartName.Equals(ChartNames.ResponseTimeCustomBarChart) ||
                chartName.Equals(ChartNames.ResponseTimeCustomLineChart) ||
                chartName.Equals(ChartNames.ResponseTimeCustomAreaChart))
            {
                dataSourceType = DataSourceTypes.ResponseTime;
            } 
            else if (chartName.Equals(ChartNames.StatisticDataCustomBarChart) ||
                chartName.Equals(ChartNames.StatisticDataCustomLineChart) ||
                chartName.Equals(ChartNames.StatisticDataCustomAreaChart))
            {
                dataSourceType = DataSourceTypes.StatisticData;
            }
        }
        return dataSourceType;
    }
}