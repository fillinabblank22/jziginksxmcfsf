﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Controls_AssignGaugesToComponent : System.Web.UI.UserControl
{
	#region Fields 
	
	private static readonly List<DictionaryEntry> _dsTypes = new List<DictionaryEntry> { 
		new DictionaryEntry("", Resources.APMWebContent.APMWEBCODE_AK1_153), 
		new DictionaryEntry("SD", Resources.APMWebContent.APMWEBCODE_AK1_154), 
		new DictionaryEntry("RT", Resources.APMWebContent.APMWEBCODE_AK1_155),
		new DictionaryEntry("CPU", Resources.APMWebContent.APMWEBCODE_AK1_156), 
		new DictionaryEntry("PM", Resources.APMWebContent.APMWEBCODE_AK1_157), 
		new DictionaryEntry("VM", Resources.APMWebContent.APMWEBCODE_AK1_158),
		new DictionaryEntry("AV_Today", Resources.APMWebContent.APMWEBCODE_AK1_159),
		new DictionaryEntry("AV_Yesterday", Resources.APMWebContent.APMWEBCODE_AK1_160),
		new DictionaryEntry("AV_Last 7 Days", Resources.APMWebContent.APMWEBCODE_AK1_161),
		new DictionaryEntry("AV_Last 30 Days", Resources.APMWebContent.APMWEBCODE_AK1_162),
		new DictionaryEntry("AV_This Month", Resources.APMWebContent.APMWEBCODE_AK1_163),
		new DictionaryEntry("AV_Last Month", Resources.APMWebContent.APMWEBCODE_AK1_164),
		new DictionaryEntry("AV_This Year", Resources.APMWebContent.APMWEBCODE_AK1_165)
	};

	#endregion 

	#region Properties

	private Int32 AppID
	{
		get
		{
			var appID = 0;
			Int32.TryParse(Request.QueryStringOrForm("AppID"), out appID);
			return appID;
		}
	}

	#endregion

	#region Init/Update Data Members

	public void PopulateControl(ResourceInfo res)
	{
		var info = default(AssignedToResourceInfo);
		AssignedToResourceInfo.TryCreate(Request, res, out info);
		if (info.ComponentIDs != null)
		{
			foreach (var id in info.ComponentIDs)
			{
				var item = ctrComponents.Items.FindByValue(id.ToString());
				if (item != null) 
				{
					item.Selected = true;
				}
			}
		}
		if (info[AssignedToResourceInfo.GaugeDataSourceType] != null)
		{
			HtmlHelper.SetListSelectedValue(ctrGaugeTypes, info[AssignedToResourceInfo.GaugeDataSourceType].ToString(), null);
		}
		else 
		{
			HtmlHelper.SetListSelectedValue(ctrGaugeTypes, res.Properties["GaugeDSTypeComponentNames"], null);
		}

		if (ctrComponents.SelectedItem == null) 
		{
			var names = HtmlHelper.SplitNames(res.Properties["GaugeComponentNames"]);
			if (names != null)
			{
				foreach (var name in names)
				{
					HtmlHelper.SetListSelectedText(ctrComponents, name);
				}
			}
		}
	}

	public void PopulateResource(ResourceInfo res)
	{
		var names = new List<String>();
		var ids = new List<Int64>();
		foreach (ListItem item in ctrComponents.Items)
		{
			if (item.Selected)
			{
				names.Add(item.Text);
				ids.Add(Int64.Parse(item.Value));
			}
		}
		var pairs = new[] { new DictionaryEntry(AssignedToResourceInfo.CmpIDs, JsHelper.Serialize(ids)), new DictionaryEntry(AssignedToResourceInfo.GaugeDataSourceType, ctrGaugeTypes.SelectedValue) };
		AssignedToResourceInfo.Update(Request, res, pairs);

		res.Properties["GaugeComponentNames"] = HtmlHelper.JoinNames(names.ToArray());
		res.Properties["GaugeDSTypeComponentNames"] = ctrGaugeTypes.SelectedValue;
	}

	#endregion

	#region Event Handlers

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		if (!IsPostBack && (Visible &= AppID > 0))
		{
			using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				var app = bl.GetApplication(AppID);
				ctrComponents.DataSource = app.Components;
				ctrComponents.DataBind();
			}
			ctrGaugeTypes.DataSource = _dsTypes;
			ctrGaugeTypes.DataBind();
		}
	}

	protected void On_ServerValidate(Object source, ServerValidateEventArgs args)
	{
		ctrCV.ErrorMessage = String.Empty;
		if (Visible)
		{
			if (ctrComponents.SelectedItem == null)
			{
                ctrCV.ErrorMessage = Resources.APMWebContent.APMWEBCODE_AK1_166;
				if (ctrGaugeTypes.SelectedValue.IsNotValid())
				{
                    ctrCV.ErrorMessage = Resources.APMWebContent.APMWEBCODE_AK1_167;
				}
			}
			else if (ctrGaugeTypes.SelectedValue.IsNotValid())
			{
                ctrCV.ErrorMessage = Resources.APMWebContent.APMWEBCODE_AK1_168;
			}
			args.IsValid = ctrCV.ErrorMessage.IsNotValid();
		}
	}

	#endregion
}