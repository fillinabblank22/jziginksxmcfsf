﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Common;

public partial class Orion_APM_Controls_Management_AlertSuppressionStatus : UserControl
{
    public string TargetElementId { get; set; }

    public string EntityUri { get; set; }

    protected bool IsDemoMode
    {
        get { return OrionConfiguration.IsDemoServer; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (string.IsNullOrEmpty(TargetElementId))
        {
            throw new ApplicationException("TargetElementId is not assigned");
        }

        if (string.IsNullOrEmpty(EntityUri))
        {
            throw new ApplicationException("EntityUri is not assigned");
        }
    }
}