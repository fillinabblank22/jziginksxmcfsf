﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Common.Models;

public partial class Orion_APM_Controls_Resources_MonitorThresholdsRepeater : System.Web.UI.UserControl
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    public Func<Threshold, bool> IsThresholdDisplayed { get; set; }

    public List<Threshold> Thresholds { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Thresholds == null)
        {
            log.Debug("Not initialized Thresholds list");
            return;
        }
        Repeater.DataSource = Thresholds.Where(t => IsThresholdDisplayed(t)).ToList();
        Repeater.DataBind();
    }
}