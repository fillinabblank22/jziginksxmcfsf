﻿using System.Web.UI.HtmlControls;
using SolarWinds.APM.Web;

public partial class Orion_APM_Controls_Resources_ApplicationDetails_Management : System.Web.UI.UserControl
{
    public ApmBaseResource Resource { get; set; }

    public ApmApplicationBase ApmApplication { get; set; }

    public HtmlGenericControl ItemsPlaceholder { get; set; }

    public void InitManagementTasks()
    {
        ManagementTasksControl.Resource = Resource;
        ManagementTasksControl.NetObject = ApmApplication;

        ManagementTasksControl.AddManagementTasksControls();
    }
}