﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDetails_Management.ascx.cs" Inherits="Orion_APM_Controls_Resources_ApplicationDetails_Management" %>
<%@ Register TagPrefix="apm" TagName="ManagementTasksControl" Src="~/Orion/APM/Controls/Management/ManagementTasksControl.ascx" %>

<script type="text/javascript" src="/Orion/APM/Services/Applications.asmx/js"></script>

<apm:ManagementTasksControl ID="ManagementTasksControl" runat="server" />

<%-- UnderscoreJS template for management section table rows --%>
<script id="managementTasksTableRowsTemplate-<%= Resource.Resource.ID %>" type="text/x-template">
    {# _.each(Tasks, function(currentSection, index) { #}
        <tr>
            <td class="PropertyHeader" style=" white-space: nowrap;">
                {{ currentSection.Title }}
            </td>
            <td>&nbsp;</td>
            <td class="Property NodeManagementIcons">
                <div id="{{ currentSection.PlaceHolderID }}">
                </div>
            </td>
        </tr>
    {# }); #}
</script>

<script type="text/javascript">
    $(function () {
        var renderManagementItems = function() {
            var sections = <%= ManagementTasksControl.ManagementTaskControlsSerialized %>;
            var resourceId = <%= Resource.Resource.ID %>;

            var templateSource = $("#managementTasksTableRowsTemplate-<%= Resource.Resource.ID %>").html();

            // Assign unique placeholder id to each section and clear section titles
            _.each(sections,
                function(section, index) {
                    section.PlaceHolderID = "managementTasksPlaceHolder-" + resourceId + "-Section" + index;
                    section.Title = "&nbsp";
                });

            // Only first section should have title (predefined)
            if (sections.length > 0) {
                sections[0].Title = "<%= Resources.APMWebContent.APMWEBDATA_VB1_292 %>";
            }
            
            // Render table row for each section with placeholder for management items
            var newHtml = _.template(templateSource,
            {
                Tasks: sections
            });
            var container = $("#<%= ItemsPlaceholder.ClientID %>");
            container.prepend(newHtml);

            // Render each section without section title into place holder
            _.each(sections,
                function(section) {
                    section.Title = "";

                    SW.Core.ManagementTasks.RenderManagementTaskItems([section], section.PlaceHolderID, resourceId);
                });
        };

        renderManagementItems();
    });
</script>