﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorThresholdsRepeater.ascx.cs" Inherits="Orion_APM_Controls_Resources_MonitorThresholdsRepeater" %>
<%@ Register TagPrefix="apm" Namespace="SolarWinds.APM.Web.Controls" Assembly="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.Utility" %>

<apm:MonitorThresholdsRepeater ID="Repeater" runat="server">
    <ItemTemplate>
        <tr>
            <td class="PropertyHeader"><%# ApmThresholdHelper.LookupThresholdName(Container.TypedItem.Name,true)%>
                <%#String.IsNullOrEmpty(ApmThresholdHelper.LookupThresholdName(Container.TypedItem.Name,true)) ? string.Empty : Resources.APMWebContent.Punctuation_colon%></td>
            <td class="Property"><%# ApmThresholdHelper.FormatThresholdValue(Container.TypedItem.CriticalLevel)%></td>                    
        </tr>
        <tr>
            <td class="PropertyHeader"><%# ApmThresholdHelper.LookupThresholdName(Container.TypedItem.Name,false)%>
                <%#String.IsNullOrEmpty(ApmThresholdHelper.LookupThresholdName(Container.TypedItem.Name,false)) ? string.Empty : Resources.APMWebContent.Punctuation_colon%></td>
            <td class="Property"><%# ApmThresholdHelper.FormatThresholdValue(Container.TypedItem.WarnLevel)%></td>                    
        </tr>
    </ItemTemplate>
</apm:MonitorThresholdsRepeater>