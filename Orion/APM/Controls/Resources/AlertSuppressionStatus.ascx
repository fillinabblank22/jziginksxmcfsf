﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertSuppressionStatus.ascx.cs" Inherits="Orion_APM_Controls_Management_AlertSuppressionStatus" %>
<%@ Register TagPrefix="orion" TagName="AlertSuppressionStatusDescription" Src="~/Orion/Controls/AlertSuppressionStatusDescription.ascx" %>

<script type="text/javascript" src="/Orion/Services/Information.asmx/js"></script>

<orion:AlertSuppressionStatusDescription runat="server" />

<script>
    $(function() {
        
        var targetElementId = '<%=TargetElementId %>';
        var entityUri = '<%=EntityUri %>';
        var isDemoMode = <%=IsDemoMode.ToString().ToLower() %>;
        var hasRights = <%=Profile.AllowUnmanage.ToString().ToLower() %>;

        var refreshStatus = function() {
            var statusPromise = SW.Core.MaintenanceMode.GetStatusDescription(
                entityUri,
                isDemoMode,
                hasRights);

            $.when(statusPromise).done(function(result) {
                $('#' + targetElementId).html(result);
            });
        }

        var refreshIfMaintenanceModeIsDefined = function() {
            if (!SW.Core.MaintenanceMode || !SW.Core.MaintenanceMode.GetStatusDescription || !$('#' + targetElementId).length) {
                setTimeout(refreshIfMaintenanceModeIsDefined, 100);
            } else {
                refreshStatus();
            }
        }

        refreshIfMaintenanceModeIsDefined();
    });
</script>


