﻿using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_Controls_AssignResourceToComponent : System.Web.UI.UserControl
{
    private bool showApplicationItemsOnly = true;
	#region Properties

	private string NetObject { get; set; }

	private Int32 applicationId = Int32.MinValue;
	private Int32 AppID
	{
		get
		{
			if (applicationId == Int32.MinValue)
			{
				if (!string.IsNullOrEmpty(NetObject))
				{
					if (NetObjectHelper.IsApplicationBase(NetObject))
					{
					    showApplicationItemsOnly = false;
					}
                    NetObjectHelper.TryGetApplicationID(NetObject, out applicationId);
				}
				else if (!Int32.TryParse(Request.QueryStringOrForm("AppID"), out applicationId))
				{
					var value = Request.QueryStringOrForm("NetObject");
					if (NetObjectHelper.IsApplicationBase(value))
					{
						applicationId = (Int32)NetObjectHelper.GetId(value);
                        showApplicationItemsOnly = false;
					}
				}
			}
			return applicationId;
		}
	}

	ListItem selItem;
	public ListItem SelectedComponent
	{
		get
		{
			if (ctrComponents.SelectedItem != null)
			{
				return ctrComponents.SelectedItem;
			}
			if (selItem == null)
			{
				var id = Request.QueryStringOrForm(SolarWinds.APM.Web.ChartingCoreBased.ModuleConstants.QueryStringParameters.ComponentId);
				if (id.IsValid())
				{
					using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
					{
						var cmp = bl.GetComponent(Int64.Parse(id));

						selItem = new ListItem(cmp.Name, cmp.Id.ToString());
					}
				}
			}
			return selItem;
		}
	}

	#endregion

	#region Event Handlers

	public event EventHandler OnSelectedNameChanged;

	protected void CtrlComponents_OnSelectedIndexChanged(object sender, EventArgs e)
	{
		if (this.OnSelectedNameChanged != null)
		{
			OnSelectedNameChanged(this, EventArgs.Empty);
		}
	}

	#endregion

	#region Init/Update Data Members

	private void TrySetNetObject(ResourceInfo resource, string netObject = null)
	{
		if (netObject.IsNotValid())
		{
			if (NetObjectManager.IsCustomObjectResource(resource))
			{
				netObject = resource.Properties["NetObjectID"];
			}
			else
			{
				netObject = Request.QueryStringOrForm("NetObject");
			}
		}
		NetObject = netObject;
	}

	public void PopulateControl(ResourceInfo res, string netObject = null)
	{
		TrySetNetObject(res, netObject);

		SetVisible(res);
        //we should filter components for database and make this list visible for sql bb databases
		if (Visible)
		{
			var props = res.Properties;

			var cmps = default(List<Component>);
			using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
                var app = bl.GetApplication(AppID);

				cmps = app.Components;
			    var appTemplate = bl.GetApplicationTemplate(app.TemplateId);

				if (Request.QueryStringOrForm("EvidenceFilter").IsValid())
				{
					var filter = Request.QueryStringOrForm("EvidenceFilter")
						.Split(new[] { ' ', '|' }, StringSplitOptions.RemoveEmptyEntries);
					cmps = cmps
						.Where(item => filter.Contains(item.EvidenceType.ToString(), StringComparer.OrdinalIgnoreCase))
						.ToList();
				}

                //show not hidden components and assign not application items specific components to DB object
			    cmps = cmps.Select(c =>
			        {
			            var componentTemplate = appTemplate.ComponentTemplates.FirstOrDefault(t => t.Id == c.TemplateId);
			            if ((componentTemplate == null) || (componentTemplate.VisibilityMode != ComponentVisibilityMode.Hidden &&
			                (componentTemplate.IsApplicationItemSpecific == showApplicationItemsOnly)))
			                return c;
			            return new Component();

			        }).Where(c => c.Application != null).OrderBy(c => c.Name).ToList();
			}
			ctrComponents.DataSource = cmps;
			ctrComponents.DataBind();

			Visible = cmps != null && cmps.Count > 0;
			if (Visible)
			{
				var info = default(AssignedToResourceInfo);
				AssignedToResourceInfo.TryCreate(res.Properties[ApmMonitor.PropAssignedToResourceInfo], NetObject, out info);

				if (info[AssignedToResourceInfo.CmpID] != null && cmps.Exists(item => item.Id.ToString(CultureInfo.InvariantCulture).Equals(info[AssignedToResourceInfo.CmpID])))
				{
					HtmlHelper.SetListSelectedValue(ctrComponents, info[AssignedToResourceInfo.CmpID].ToString(), null);
				}
				else
				{
					HtmlHelper.SetListSelectedText(ctrComponents, props[ApmMonitor.PropComponentName]);
				}
			}
		}
	}

	public void PopulateResource(ResourceInfo res)
	{
		TrySetNetObject(res);

		SetVisible(res);
		if (Visible)
		{
			String name = null, id = null;
			if (ctrComponents.SelectedItem != null)
			{
				id = ctrComponents.SelectedItem.Value;
				name = ctrComponents.SelectedItem.Text;
			}
			AssignedToResourceInfo.Update(Request, res, new[] { new DictionaryEntry(AssignedToResourceInfo.CmpID, id) });
			res.Properties[ApmMonitor.PropComponentName] = name;
		}
	}

	public void SelectComponent(string selectedValue)
	{
		HtmlHelper.SetListSelectedText(ctrComponents, selectedValue);
	}

	private void SetVisible(ResourceInfo res)
	{
		var value = NetObject;
		if (NetObjectManager.IsCustomObjectResource(res))
		{
			var resPath = res.Properties["embeddedobjectresource"];
			if (resPath.IsValid() && resPath.Contains("ApplicationAvailabilityGraph.ascx", StringComparison.InvariantCultureIgnoreCase))
			{
				Visible = false;
			}
			else 
			{
				value = UriHelper.GetNetObjectId(res.Properties["objecturi"]);
			}
		}
		else
		{
			Visible = AppID > 0;
		}

		if (Visible)
		{
            //drop down list also should be visible for DB objects
		    Visible = value.IsValid() &&
		              (NetObjectHelper.IsApplicationBase(value) || NetObjectHelper.IsApplicationItem(value));
		}
	}

	#endregion
}