﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web.Helpers;

using ApmResourceProperties = SolarWinds.APM.Web.UI.Resource.ApmMultiChartBaseResource.ApmResourceProperties;

public partial class Orion_APM_Controls_AssignComponentsToResource : System.Web.UI.UserControl
{    
    #region Properties

    private string NetObject { get; set; }

    public int AppId
    {
        get;
        set;
    }

    public List<ListItem> GetSelectedComponents()
    {
        return this.chblComponents.Items.Cast<ListItem>().Where(li => li.Selected).ToList();
    }

    public List<ListItem> GetSupportedComponents()
    {
        return this.chblComponents.Items.Cast<ListItem>().ToList();
    }

    #endregion

    #region Init/Update Data Members

    private static Component GetComponent(Application app, ComponentTemplate ct, int applicationItemId)
    {
        return (applicationItemId != -1)
            ? app.Components.SingleOrDefault(c => (c.TemplateId == ct.Id) && (c.ApplicationItemID == applicationItemId))
            : app.Components.SingleOrDefault(c => (c.TemplateId == ct.Id));
    }

    private static bool IsComponentEnabled(Application app, ComponentTemplate ct, int applicationItemId)
    {
        var component = GetComponent(app, ct, applicationItemId);
        return (component != null) && (component.Status != Status.IsDisabled);
    }

    private static bool IsTemplateSupportedBase(ComponentTemplate ct, bool showApplicationItemsOnly)
    {
        return (ct.VisibilityMode != ComponentVisibilityMode.Hidden)
               && (ct.IsApplicationItemSpecific == showApplicationItemsOnly);
    }

    private static bool IsTemplateSupportedStatistics(ComponentTemplate ct)
    {
        return (ct.IsPortBased || ct.Type == ComponentType.CustomPerfCounter) && ct.HasStatisticData;
    }

    private static bool IsTemplateSupportedResponseTime(ComponentTemplate ct)
    {
        return ct.HasResponseTimeSupport;
    }

    public class ComponentTemplateWrapper
    {
        public ComponentTemplate Template { get; private set; }
        public ComponentGuidInfo.EvidenceSubTypeEnum SubTypeValue { get; private set; }

        public ComponentTemplateWrapper(ComponentTemplate ct, ComponentGuidInfo.EvidenceSubTypeEnum subtype = ComponentGuidInfo.EvidenceSubTypeEnum.Statistic)
        {
            this.Template = ct;
            this.SubTypeValue = subtype;
        }

        public string UniqueId
        {
            get
            {
                return
                    InvariantString.Format(
                        (this.SubTypeValue == ComponentGuidInfo.EvidenceSubTypeEnum.Statistic) ? "{0}" : "{0}:{1}",
                        this.Template.UniqueId, (int) this.SubTypeValue);
            }
        }

        public string Name
        {
            get
            {
                return
                    InvariantString.Format(
                        (this.SubTypeValue == ComponentGuidInfo.EvidenceSubTypeEnum.Statistic)
                            ? Resources.APMWebContent.TemplateNameWithStatistic
                            : Resources.APMWebContent.TemplateNameWithResponseTime,
                        this.Template.Name);
            }
        }
    }

    public void PopulateControl(ResourceInfo resourceInfo, int applicationId, int applicationItemId, string netobjectID, bool showApplicationItemsOnly)
    {
        NetObject = netobjectID;
        this.AppId = applicationId != -1 ? applicationId : this.GetAppID(resourceInfo);
        this.SetVisibility(resourceInfo);

        if (!this.Visible)
        {
            return;
        }

        List<ComponentTemplate> templates;

        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            var app = bl.GetApplication(this.AppId);
            var appTemplate = bl.GetApplicationTemplate(app.TemplateId);
            templates = appTemplate.ComponentTemplates
                .Where(ct => IsTemplateSupportedBase(ct, showApplicationItemsOnly))
                .Where(ct => IsComponentEnabled(app, ct, applicationItemId))
                .ToList();
        }

        var tempsStatistic = templates
            .Where(IsTemplateSupportedStatistics)
            .Select(ct => new ComponentTemplateWrapper(ct))
            .ToList();

        var tempsResponse = templates
            .Where(IsTemplateSupportedResponseTime)
            .Select(ct => new ComponentTemplateWrapper(ct, ComponentGuidInfo.EvidenceSubTypeEnum.ResponseTime))
            .ToList();

        var componentTemplates = tempsStatistic.Union(tempsResponse)
            .OrderBy(ctw => ctw.Template.Name);

        if (componentTemplates.Any())
        {
            this.chblComponents.DataSource = componentTemplates;
            this.chblComponents.DataBind();

            AssignedToResourceInfo info;
            AssignedToResourceInfo.TryCreate(resourceInfo.Properties[ApmMonitor.PropAssignedToResourceInfo], NetObject, out info);

            var textIds = (info[AssignedToResourceInfo.CmpIDs] as string) ?? resourceInfo.Properties.GetValueOrDefault(ApmResourceProperties.CompIds, "");
            if (!string.IsNullOrWhiteSpace(textIds))
            {
                textIds
                    .Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
                    .ToList()
                    .ForEach(id => HtmlHelper.SetListSelectedValue(this.chblComponents, id, null));
            }

            this.lblNoComponent.Visible = false;
            this.pnlSelectComponent.Visible = true;
        }
        else
        {
            this.lblNoComponent.Visible = true;
            this.pnlSelectComponent.Visible = false;
        }
    }

    #endregion

    #region Private methods

    // TODO revise this
    private void SetVisibility(ResourceInfo resourceInfo)
    {
        var netObj = NetObject;

        if (NetObjectManager.IsCustomObjectResource(resourceInfo))
        {
            netObj = resourceInfo.Properties["embeddedobjectresource"];
            if (netObj.IsValid() && netObj.Contains("ApplicationAvailabilityGraph.ascx", StringComparison.InvariantCultureIgnoreCase))
            {
                this.Visible = false;
            }
            if (Visible)
            {
                netObj = UriHelper.GetNetObjectId(resourceInfo.Properties["objecturi"]);
            }
        }
        else
        {
            this.Visible &= AppId > 0;
        }

        if (this.Visible && netObj.IsNotValid())
        {
            this.Visible = false;            
        }
    }

    // TODO revise this
    private int GetAppID(ResourceInfo resourceInfo)
    {
        int id;

        if (int.TryParse(Request.QueryStringOrForm("AppID"), out id))
        {            
            return id;
        }

        var netObj = Request.QueryStringOrForm("NetObject");

        if (resourceInfo != null && NetObjectManager.IsCustomObjectResource(resourceInfo))
        {
            netObj = resourceInfo.Properties["netobjectid"];
        }

        if (NetObjectHelper.IsApplication(netObj))
        {
            return (int)NetObjectHelper.GetId(netObj);
        }

        return int.MinValue;
    }

    #endregion
}
