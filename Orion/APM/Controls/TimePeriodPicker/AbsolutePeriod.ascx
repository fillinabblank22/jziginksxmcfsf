<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AbsolutePeriod.ascx.cs" Inherits="Orion_APM_Controls_TimePeriodPicker_AbsolutePeriod" %>

<script type="text/javascript" src="/Orion/js/jquery/jquery.timePicker.js"></script>
<asp:Label runat="server" ID="twoLinesFromDiv" style="width:35px;display:inline-block" Text="<%$ Resources: APMWebContent, AbsolutePeriod_TwoLinesFromDiv_Text %>" />
<asp:TextBox ID="txtStartDate" runat="server"  Width="75px" CssClass="periodSelector" />  
<asp:TextBox ID="txtStartTime" runat="server"  Width="60px" />
<span id="endDateTimePart" runat="server">
<span id="singleLineToDiv" runat="server"></span>

<span id="twoLinesBrDiv" runat="server"><br/></span>
<asp:Label runat="server" ID="twoLinesToDiv" style="width:35px;display:inline-block" Text="<%$ Resources: APMWebContent, AbsolutePeriod_TwoLinesToDiv_Text %>" />
<asp:TextBox ID="txtEndDate" runat="server" Width="75px" />
<asp:TextBox ID="txtEndTime" runat="server" Width="60px" />
</span>

<asp:CustomValidator ID="valPeriodCustom" runat="server"
    OnServerValidate="valPeriodCustom_ServerValidate"
    Display="Dynamic"
    Enabled="true">
        <a onclick="return false;" id="valIcon" runat="server" href="#" class="apmValIcon" title="<%$ Resources: APMWebContent, AbsolutePeriod_ValCustomValidatorIcon_Title %>">&nbsp;</a>
</asp:CustomValidator>

