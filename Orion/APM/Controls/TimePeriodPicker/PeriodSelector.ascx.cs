using SolarWinds.Orion.Web;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.APM.Web.Utility;
using SolarWinds.APM.Common;


public partial class Orion_APM_Controls_TimePeriodPicker_PeriodSelector : UserControl
{

    /// <summary>
    /// TimePeriodIUFilter reference
    /// </summary>
    [PersistenceMode(PersistenceMode.Attribute)]
    public TimePeriodUiFilter PeriodFilter
    {
        get
        {
            TimePeriodUiFilter result = new TimePeriodUiFilter();
            if (this.radNamed.Checked)
            {
                result.SetNamedPeriod((NamedPeriodTypesEnum)Enum.Parse(typeof(NamedPeriodTypesEnum), this.lbxNamedPeriods.SelectedValue));
            }
            else if (this.radRelative.Checked)
            {
                return this.relativePeriod.PeriodFilter;
            }
            else if (this.radAbsolute.Checked)
            {
                return this.absolutePeriod.PeriodFilter;
            }
            return result;
        }
        set
        {
            if (value != null && value.TimePeriodType == TimePeriodTypeEnum.NamedPeriod)
            {
                this.lbxNamedPeriods.SelectedValue = value.NamedPeriodType.ToString();
            }
            this.relativePeriod.PeriodFilter = value;
            this.absolutePeriod.PeriodFilter = value;
            SetRadioButtons(value);
        }
    }


    /// <summary>
    /// If set to true it will force initialization on postback as if there was no postback
    /// </summary>
    public bool ForceInitialization { get; set; }

    /// <summary>
    /// Validation enabled flag
    /// </summary>
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ValidationEnabled { get; set; }

    /// <summary>
    /// Validation group
    /// </summary>
    public string ValidationGroup { get; set; }

	public NamedPeriodTypesEnum MinNamedPeriodValue { get; set; }

    /// <summary>
    /// Header label for TimePicker
    /// </summary>
    /// <returns></returns>
    public string GetHeaderLabel()
    {
        if (radNamed.Checked)
        {
            if (!String.IsNullOrEmpty(this.lbxNamedPeriods.SelectedValue))
            {
                NamedPeriodTypesEnum namedPeriodType = (NamedPeriodTypesEnum)Enum.Parse(typeof(NamedPeriodTypesEnum), this.lbxNamedPeriods.SelectedValue);
                var namedPeriodTypeLocalizedString = EnumHelper.GetLocalizedDescription(namedPeriodType);
                return string.Format(@"{0}", namedPeriodTypeLocalizedString);
            }
        }
        if (radRelative.Checked)
        {
            return this.relativePeriod.GetHeaderLabel();
        }
        else if (radAbsolute.Checked)
        {
            return this.absolutePeriod.GetHeaderLabel();
        }
        return String.Empty;
    }

    /// <summary>
    /// Page OnInit
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            FillNamedPeriodsList();
        }

        ValidationGroup = "timePeriodValGroup_" + ClientID;
        this.relativePeriod.ForceInitialization = ForceInitialization;

        this.radNamed.Attributes.Add("onselect", this.ActuatorScriptName + "(); return true;");
        this.radNamed.Attributes.Add("onclick", this.radNamed.Attributes["onselect"]);
        this.radRelative.Attributes.Add("onselect", this.ActuatorScriptName + "(); return true;");
        this.radRelative.Attributes.Add("onclick", this.radRelative.Attributes["onselect"]);
        this.radAbsolute.Attributes.Add("onselect", this.ActuatorScriptName + "(); return true;");
        this.radAbsolute.Attributes.Add("onclick", this.radAbsolute.Attributes["onselect"]);
    }

    /// <summary>
    /// Page OnLoad
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        ScriptManager sm = ScriptManager.GetCurrent(this.Page);

        if (sm == null)
        {
            sm = new ScriptManager();
            this.Controls.AddAt(0, sm);
        }

        if (ForceInitialization)
        {
            FillNamedPeriodsList();
        }

        absolutePeriod.ValidationGroup = this.ValidationGroup;
        relativePeriod.ValidationGroup = this.ValidationGroup;
    }

    /// <summary>
    /// Page OnPrerender
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Setup Client Scripts for handling controls
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), this.ActuatorScriptName, this.BuildActuatorScript(), true);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "timePeriodValidateScript_" + this.ClientID, timePeriodValidateScript(), true);
        string script = String.Format("Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded({0}) \n", this.ActuatorScriptName);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ActuatorScriptName + "_Startup", script, true);

        SetRadioButtons(PeriodFilter);
    }

    /// <summary>
    /// Sets the radio buttons according to TimePeriod
    /// </summary>
    private void SetRadioButtons(TimePeriodUiFilter periodFilter)
    {
        if (periodFilter != null)
        {
            switch (periodFilter.TimePeriodType)
            {
                case TimePeriodTypeEnum.NamedPeriod:
                    {
                        radNamed.Checked = true;
                        divNamedPeriod.Style["display"] = "block";
                        divRelativePeriod.Style["display"] = "none";
                        divAbsolutePeriod.Style["display"] = "none";
                        break;
                    }
                case TimePeriodTypeEnum.RelativePeriod:
                    {
                        radRelative.Checked = true;
                        divNamedPeriod.Style["display"] = "none";
                        divRelativePeriod.Style["display"] = "block";
                        divAbsolutePeriod.Style["display"] = "none";
                        break;
                    }
                case TimePeriodTypeEnum.AbsolutePeriod:
                    {
                        radAbsolute.Checked = true;
                        divNamedPeriod.Style["display"] = "none";
                        divRelativePeriod.Style["display"] = "none";
                        divAbsolutePeriod.Style["display"] = "block";
                        break;
                    }
            }
        }
    }

    /// <summary>
    /// Fill dropdown with enumarator items
    /// </summary>
    protected void FillNamedPeriodsList()
    {
        if (this.lbxNamedPeriods.Items.Count == 0)
        {
            Array values = Enum.GetValues(typeof(NamedPeriodTypesEnum));
            foreach (NamedPeriodTypesEnum val in values)
            {
				if (val >= MinNamedPeriodValue) 
				{
					var name = EnumHelper.GetLocalizedDescription(val);
					this.lbxNamedPeriods.Items.Add(new ListItem(name, val.ToString()));
				}
            }
        }
    }

    /// <summary>
    /// Radio buttons change template
    /// </summary>
    private const string RADIO_CHANGE_TEMPLATE = @"
        function {0}()
        {{
            var namedDropDown = document.getElementById('{1}');
            var radNamed = document.getElementById('{6}');
            var radRelative = document.getElementById('{7}');
            var radAbsolute = document.getElementById('{8}');

            var divNamedPeriod = document.getElementById('{10}');
            var divRelativePeriod = document.getElementById('{11}');
            var divAbsolutePeriod = document.getElementById('{12}');
    
            if(radNamed.checked == true)
            {{
                namedDropDown.disabled = false;
                divNamedPeriod.style.display = 'block';
            }}
            else
            {{
                namedDropDown.disabled = true;
                divNamedPeriod.style.display = 'none';
            }}

            if(radRelative.checked == true)
            {{
                divRelativePeriod.style.display = 'block';
                {2}
            }}
            else
            {{
                divRelativePeriod.style.display = 'none';
                {3}
            }}

            if(radAbsolute.checked == true)
            {{
                divAbsolutePeriod.style.display = 'block';
                {4}
            }}
            else
            {{
                divAbsolutePeriod.style.display = 'none';
                {5}
            }}
        }}
    ";

    /// <summary>
    /// String format for Radio button's events
    /// </summary>
    /// <returns></returns>
    private string BuildActuatorScript()
    {
        return string.Format(
            RADIO_CHANGE_TEMPLATE,
            this.ActuatorScriptName, // 0
            this.lbxNamedPeriods.ClientID, //1
            this.relativePeriod.GetClientEnableScript(),//2
            this.relativePeriod.GetClientDisableScript(),//3
            this.absolutePeriod.GetClientEnableScript(),//4
            this.absolutePeriod.GetClientDisableScript(),//5
            radNamed.ClientID,//6
            radRelative.ClientID,//7
            radAbsolute.ClientID,//8
            this.ClientID,//9
            this.divNamedPeriod.ClientID,//10
            this.divRelativePeriod.ClientID,//11
            this.divAbsolutePeriod.ClientID//12
            );
    }

    /// <summary>
    /// Script for confirming client side validation of user control
    /// </summary>
    /// <returns></returns>
    private string timePeriodValidateScript()
    {
        String s = @"function timePeriodValidate_{0}() {{
            return Page_ClientValidate('{1}');
        }}";
        return String.Format(s, this.ClientID, this.ValidationGroup);
    }

    private string ActuatorScriptName
    {
        get { return this.ClientID + "_Actuate"; }
    }

}
