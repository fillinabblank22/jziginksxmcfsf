
using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.APM.Web.Utility;
using System.Web;
using System.Linq;
using System.Collections.Specialized;


public partial class Orion_APM_Controls_TimePeriodPicker_TimePeriodPicker : System.Web.UI.UserControl
{

    private TimePeriodUiFilter periodFilter;
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    /// <summary>
    /// TimePeriodUiFilter reference
    /// </summary>
    public TimePeriodUiFilter PeriodFilter
    {
        // get { return periodFilter; }
        get { return this.periodSelector.PeriodFilter; }
        set
        {
            this.periodFilter = value;
            this.periodSelector.PeriodFilter = value;
        }
    }


    /// <summary>
    /// Flag for not to submit page by submit button
    /// </summary>
    public bool DontPostbackWhenSubmit { get; set; }
    
    /// <summary>
    /// Control style adjustment
    /// </summary>
    public bool NoLeftMargin { get; set; }

    /// <summary>
    /// Control style adjustment
    /// </summary>
    public bool NoBold { get; set; }


    /// <summary>
    /// Normally is PeriodMenu aligned to the left edge of Period Title (dropdown button).
    /// If this property is set then PeriodMenu is aligned to the right edge of the dropdown button.
    /// </summary>
    public bool AlignRight { get; set; }

    /// <summary>
    /// Submit behavior mode
    /// </summary>
    public TimePeriodHelper.TimePeriodControlSubmitBehaviorEnum TimePeriodControlSubmitBehaviorMode { get; set; }

	public NamedPeriodTypesEnum MinNamedPeriodValue 
	{
		get { return periodSelector.MinNamedPeriodValue; }
		set { periodSelector.MinNamedPeriodValue = value; }
	}

    /// <summary>
    /// Page OnInit
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NoLeftMargin)
        {
            lblPeriodTitle.Attributes.CssStyle.Add("margin-left", "-10px");
            lblPeriodTitle.Attributes.CssStyle.Add("font-size", "small");
        }

        if (NoBold)
        {
            lblPeriodTitle.Attributes.CssStyle.Add("font-weight", "normal");
        }

        // prepare javascript controls                
        this.PeriodDropDown.Attributes.Add("onclick", string.Format("ShowMenu_{0}('{1}','{2}','{3}'); return false;", periodSelector.ClientID, PeriodMenu.ClientID, lblPeriodTitle.ClientID, AlignRight.ToString().ToLower()));

        this.PeriodDropDown.HRef = this.Page.Request.Url.PathAndQuery + "#";
        this.btnCancel.OnClientClick = string.Format("HideMenu_{0}('{1}'); return false;", periodSelector.ClientID, PeriodMenu.ClientID);

        //// Include script for TimePeriod menu
        string script = this.GetTimePeriodMenuJS(this.periodSelector.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), string.Format("TimePeriodPeriodMenu{0}", this.ClientID), script, true);

        if (DontPostbackWhenSubmit)
        {
            btnSubmit.OnClientClick = string.Format("if (timePeriodValidate_{0}()) {{ ChangePeriodLabel_{0}('{1}','{2}'); }}; return false;",
                periodSelector.ClientID, PeriodMenu.ClientID, lblPeriodTitle.ClientID);
        }
        else
        {
            btnSubmit.OnClientClick = string.Format("return timePeriodValidate_{0}();", this.periodSelector.ClientID);
        }

        if (!this.IsPostBack)
        {
            periodFilter = TimePeriodUiFilter.ParseFromQueryString(Page.Request);
            if (periodFilter == null)
            {
                PeriodFilter = TimePeriodUiFilter.DefaultNamedPeriodFilter();
            }
            else
            {
                PeriodFilter = periodFilter;
            }
        }

    }

    /// <summary>
    /// Page OnPrerender
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        lblPeriodTitle.Text = this.periodSelector.GetHeaderLabel();
    }

    /// <summary>
    /// Submit button event handler; after submit, the user is redirected to the same page, but with adjusted URL parameters
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Submit_Click(object sender, EventArgs e)
    {
        if (TimePeriodControlSubmitBehaviorMode == TimePeriodHelper.TimePeriodControlSubmitBehaviorEnum.Redirect)
        {
            String urlForRedirection = TimePeriodUiFilter.AdjustQueryString(HttpContext.Current.Request.RawUrl, this.periodSelector.PeriodFilter);
            Response.Redirect(urlForRedirection);
        }
    }


    /// <summary>
    /// JavaScript for TimePeriod menu.
    /// </summary>
    /// <param name="clientId"></param>
    /// <returns></returns>
    private string GetTimePeriodMenuJS(string clientId)
    {
        return string.Format(@"
        var activeMenu = """";

        function HideMenu_{0}(periodMenuId) {{
            $(""#"" + periodMenuId).hide(); 
            activeMenu = """";
        }}

        // for flow direction selector
        function HideMenu(periodMenuId)
        {{
            HideMenu_{0}(periodMenuId);
        }}

        // for flow direction selector
        function ShowMenu(periodMenuId, periodTitle)
        {{
            ShowMenu_{0}(periodMenuId, periodTitle, 'false');
        }}

        // PeriodDropDown's onClick
        function ShowMenu_{0}(periodMenuId, periodTitle, pAlignRight) {{

            var $_periodMenu = $(""#"" + periodMenuId);
            var $_periodTitle = $(""#"" + periodTitle);
       
            if (activeMenu != """") {{
                HideMenu_{0}(activeMenu);
            }}

            activeMenu = periodMenuId;

            if (pAlignRight === 'true') {{
                $_periodMenu.css(""left"", $_periodTitle.position().left + $_periodTitle.width() - $_periodMenu.width());
            }}
            else {{
                $_periodMenu.css(""left"", $_periodTitle.position().left);
            }}

            $_periodMenu.css(""top"", $_periodTitle.position().top+25);
            $_periodMenu.show();

            return false;
        }}

        function ChangePeriodLabel_{0}(periodMenuId, periodTitle) {{
            // TODO - maybe in future version
            HideMenu_{0}(periodMenuId)
            return false;
        }}", clientId);
    }

}
