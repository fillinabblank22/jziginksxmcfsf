<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimePeriodPicker.ascx.cs" Inherits="Orion_APM_Controls_TimePeriodPicker_TimePeriodPicker" %>
<%@ Register TagPrefix="apm" TagName="PeriodSelector" Src="~/Orion/APM/Controls/TimePeriodPicker/PeriodSelector.ascx" %>

<div style="font-weight:normal; white-space:nowrap; font-size:12pt; display:inline; margin-left:10px; vertical-align:top;">
    <asp:Label runat="server" ID="lblPeriodTitle" Text="<%$ Resources: APMWebContent, Please_Select_a_Period %>" meta:resourcekey="lblPeriodTitle" />
    <a runat="server" id="PeriodDropDown" class="periodIcon" href="#"></a>    
</div>

<div id="PeriodMenu" runat="server" class="periodPicker" >
   
    <apm:PeriodSelector runat="server" ID="periodSelector" ShowAbsoluteOnTwoLines="true" />
    
    <div class="periodPickerButtons">
        <orion:LocalizableButton runat="server" ID="btnSubmit" DisplayType="Primary" LocalizedText="Submit" OnClick="Submit_Click" CssClass="PeriodSubmitButton" />
        <orion:LocalizableButton runat="server" ID="btnCancel" DisplayType="Secondary" LocalizedText="Cancel" CssClass="PeriodCancelButton" />
    </div>
    
</div>

