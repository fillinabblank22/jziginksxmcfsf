using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.APM.Web.Utility;
using SolarWinds.APM.Common;

public partial class Orion_APM_Controls_TimePeriodPicker_RelativePeriod : UserControl
{

    // default MIN / MAX values for textbox
    protected const int MIN_VALUE = 1;
    protected const int MAX_VALUE = 1000000000;


    /// <summary>
    /// TimePeriodUiFilter reference
    /// </summary>
    public TimePeriodUiFilter PeriodFilter
    {
        get
        {
            if (!ValidateQuantityCustomValue(this.txtQuantity.Text))
            {
                throw new FormatException();
            }

            TimePeriodUiFilter result = new TimePeriodUiFilter();
            result.SetRelativePeriod((RelativePeriodTypesEnum)Enum.Parse(typeof(RelativePeriodTypesEnum), this.lbxTimeType.SelectedValue), Convert.ToInt32(this.txtQuantity.Text));
            return result;
        }
        set
        {
            SelectTimePeriod(value);
        }
    }

    /// <summary>
    /// Validation group
    /// </summary>
    public string ValidationGroup { get; set; }

    /// <summary>
    /// Used for forcing call to FillTimeType() regardless of whether this is postback or not
    /// </summary>
    public bool ForceInitialization { get; set; }


    /// <summary>
    /// Header label for TimePicker
    /// </summary>
    /// <returns></returns>
    public string GetHeaderLabel()
    {
        if (ValidateQuantityCustomValue(this.txtQuantity.Text))
        {
            RelativePeriodTypesEnum relativePeriodType = (RelativePeriodTypesEnum)Enum.Parse(typeof(RelativePeriodTypesEnum), this.lbxTimeType.SelectedValue);
            string relativePeriodTypeLocalizedString = EnumHelper.GetLocalizedDescription(relativePeriodType); ;
            return string.Format(APMWebContent.RelativeTimePeriod_Label_Format, this.txtQuantity.Text, relativePeriodTypeLocalizedString);
        }
        return String.Empty;
    }


    /// <summary>
    /// Control input child controls enable / disable script template
    /// </summary>
    private const string CLIENT_DISABLE_TEMPLATE = @"
        document.getElementById('{2}').disabled = {0};
        document.getElementById('{3}').disabled = {0};
        ValidatorEnable(document.getElementById('{4}'), {1});
        ValidatorEnable(document.getElementById('{5}'), {1});";

    /// <summary>
    /// Control input child controls disable script
    /// </summary>
    /// <returns></returns>
    public string GetClientDisableScript()
    {
        return string.Format(
            CLIENT_DISABLE_TEMPLATE, 
            "true",
            "false",
            this.txtQuantity.ClientID, 
            this.lbxTimeType.ClientID, 
            this.valQuantityRequired.ClientID,
            this.valQuantityCustom.ClientID);
    }

    /// <summary>
    /// Control input child controls enable script
    /// </summary>
    /// <returns></returns>
    public string GetClientEnableScript()
    {
        return string.Format(
            CLIENT_DISABLE_TEMPLATE,
            "false",
            "true",
            this.txtQuantity.ClientID,
            this.lbxTimeType.ClientID,
            this.valQuantityRequired.ClientID,
            this.valQuantityCustom.ClientID);
    }

    /// <summary>
    /// Fill dropdown with enumarator items
    /// </summary>
    protected void FillTimeType()
    {
        if (this.lbxTimeType.Items.Count == 0)
        {
            Array values = Enum.GetValues(typeof(RelativePeriodTypesEnum));
            foreach (RelativePeriodTypesEnum val in values)
            {
                var name = EnumHelper.GetLocalizedDescription(val);
                this.lbxTimeType.Items.Add(new ListItem(name, val.ToString()));
            }
        }
    }

    /// <summary>
    /// Pre-fill child controls from parameter
    /// </summary>
    /// <param name="periodFilter"></param>
    protected void SelectTimePeriod(TimePeriodUiFilter periodFilter)
    {
        if (periodFilter == null)
        {
            return;
        }

        if (periodFilter.TimePeriodType != TimePeriodTypeEnum.RelativePeriod)
        {
            txtQuantity.Text = "1";
            lbxTimeType.SelectedValue = RelativePeriodTypesEnum.Hours.ToString();
        }
        else
        {
            txtQuantity.Text = periodFilter.RelativePeriodValue.ToString();
            lbxTimeType.SelectedValue = periodFilter.RelativePeriodType.ToString();
        }
    }

    /// <summary>
    /// Page OnInit
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            FillTimeType();
        }
    }

    /// <summary>
    /// Page OnLoad
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (ForceInitialization)
        {
            FillTimeType();
        }

        //if there is more than one range control, we register only once client validation function
        if (!Page.ClientScript.IsClientScriptBlockRegistered("ClientValidateRelativePeriodRange"))
        {
            const string scriptTemplate = @"
            function ClientValidateRelativePeriodRange(source, clientside_arguments)
            {{
              if (isNaN(clientside_arguments.Value))
              {{
                 clientside_arguments.IsValid = false;
              }}
              else if (parseInt(clientside_arguments.Value) < {0} || parseInt(clientside_arguments.Value) > {1})
              {{
                  clientside_arguments.IsValid = false;
              }}
              else
              {{
                  clientside_arguments.IsValid = true;
              }}
            }}";
            string script = string.Format(scriptTemplate, MIN_VALUE.ToString(), MAX_VALUE.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClientValidateRelativePeriodRange", script, true);
        }

        if (!String.IsNullOrEmpty(ValidationGroup))
        {
            this.valQuantityRequired.ValidationGroup = this.ValidationGroup;
            this.valQuantityCustom.ValidationGroup = this.ValidationGroup;
        }
    }
    
    /// <summary>
    /// Control's server validation event handler
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void valQuantityCustom_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = ValidateQuantityCustomValue(args.Value);
    }

    /// <summary>
    /// Control's server validation function
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    protected bool ValidateQuantityCustomValue(string value)
    {
        int n;
        if (!Int32.TryParse(value, out n))
        {
            return false;
        }
        else
        {
            return (n >= MIN_VALUE && n <= MAX_VALUE);
        }
    }
}
