<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PeriodSelector.ascx.cs" Inherits="Orion_APM_Controls_TimePeriodPicker_PeriodSelector" %>
<%@ Register TagPrefix="apm" TagName="RelativePeriod" Src="~/Orion/APM/Controls/TimePeriodPicker/RelativePeriod.ascx" %>
<%@ Register TagPrefix="apm" TagName="AbsolutePeriod" Src="~/Orion/APM/Controls/TimePeriodPicker/AbsolutePeriod.ascx" %>

<div class="periodSelector" runat="server" id="divPeriodSelector">
    <div class="title">
        <asp:Label runat="server" ID="lblTimePeriodHeader" Text="<%$ Resources: APMWebContent, Time_Period %>" />
    </div>

    <!-- named period -->
    <asp:RadioButton runat="server" ID="radNamed" GroupName="PeriodType" Text="<%$ Resources: APMWebContent,Named_Time_Period %>" /><br />
    <div class="radioButtonDivContent" runat="server" id="divNamedPeriod" style="display: none;">
        <asp:ListBox runat="server" ID="lbxNamedPeriods" Rows="1"/>
    </div>
    
    <!-- relative period -->
    <asp:RadioButton runat="server" ID="radRelative" GroupName="PeriodType" Text="<%$ Resources: APMWebContent, Relative_Time_Period %>" /><br />
    <div class="radioButtonDivContent" runat="server" id="divRelativePeriod" style="display: none;">
        <apm:RelativePeriod runat="server" ID="relativePeriod"/>
    </div>

    <!-- absolute period -->
    <asp:RadioButton runat="server" ID="radAbsolute" GroupName="PeriodType" Text="<%$ Resources: APMWebContent, Absolute_Time_Period %>" /><br />
    <div class="radioButtonDivContent" runat="server" id="divAbsolutePeriod" style="display: none;">
        <apm:AbsolutePeriod ID="absolutePeriod" runat="server"/>
    </div>

    <asp:HiddenField runat="server" ID="periodNameHiddenField" />
</div>
