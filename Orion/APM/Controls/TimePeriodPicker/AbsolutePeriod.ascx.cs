
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Web.Utility;



public partial class Orion_APM_Controls_TimePeriodPicker_AbsolutePeriod : UserControl
{
    #region properties

    /// <summary>
    /// Validation group
    /// </summary>
    public string ValidationGroup { get; set; }

    /// <summary>
    /// Child control's client ID
    /// </summary>
    public string CIDabsStartDate { get { return this.txtStartDate.ClientID; } }

    /// <summary>
    /// Child control's client ID
    /// </summary>
    public string CIDabsStartTime { get { return this.txtStartTime.ClientID; } }
    
    /// <summary>
    /// Child control's client ID
    /// </summary>
    public string CIDabsEndDate { get { return this.txtEndDate.ClientID; } }

    /// <summary>
    /// Child control's client ID
    /// </summary>
    public string CIDabsEndTime { get { return this.txtEndTime.ClientID; } }

    /// <summary>
    /// TimePeriodUiFilter reference
    /// </summary>
    public TimePeriodUiFilter PeriodFilter
    {
        get
        {
            TimePeriodUiFilter result = new TimePeriodUiFilter();
            result.SetAbsolutePeriod(DateTime.Parse(this.DateTimeStart), DateTime.Parse(this.DateTimeEnd));
            return result;
        }
        set
        {
            SelectTimePeriod(value);
        }
    }

    public bool ShowOnlyStartTimeDate { get; set; }


    #endregion

    /// <summary>
    /// Concated start date and start time
    /// </summary>
    public string DateTimeStart
    {
        get { return this.txtStartDate.Text + " " + this.txtStartTime.Text; }
    }

    /// <summary>
    /// Concated end date and end time
    /// </summary>
    public string DateTimeEnd
    {
        get { return this.txtEndDate.Text + " " + this.txtEndTime.Text; }
    }

    /// <summary>
    /// Regional settings serialized, for date picker javascript object
    /// </summary>
    public string DatePickerRegionalSettings
    {
        get
        {
            // TODO - should be read from some global settings
            return "{\"dateFormat\":\"m\\/d\\/yy\",\"dayNames\":[\"Sunday\",\"Monday\",\"Tuesday\",\"Wednesday\",\"Thursday\",\"Friday\",\"Saturday\"],\"dayNamesMin\":[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"],\"dayNamesShort\":[\"Sun\",\"Mon\",\"Tue\",\"Wed\",\"Thu\",\"Fri\",\"Sat\"],\"firstDay\":0,\"monthNames\":[\"January\",\"February\",\"March\",\"April\",\"May\",\"June\",\"July\",\"August\",\"September\",\"October\",\"November\",\"December\",\"\"],\"monthNamesShort\":[\"Jan\",\"Feb\",\"Mar\",\"Apr\",\"May\",\"Jun\",\"Jul\",\"Aug\",\"Sep\",\"Oct\",\"Nov\",\"Dec\",\"\"],\"show24Hours\":false,\"timeSeparator\":\":\"}";
        }
    }

    /// <summary>
    /// Header label for TimePicker
    /// </summary>
    /// <returns></returns>
    public string GetHeaderLabel()
    {
        if (DateTimeStart.Length > 1 && DateTimeEnd.Length > 1)
        {
            return string.Format(Resources.APMWebContent.AbsolutePeriod_AbsoluteTimePeriod_LabelFormat, DateTimeStart, DateTimeEnd);
        }
        return string.Empty;
    }

    /// <summary>
    /// Custom validation function name
    /// </summary>
    protected string CustomValidateFunctionName
    {
        get { return "validateAbsoluteTimePeriod_" + ClientID; }
    }


    /// <summary>
    /// Page OnInit
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        endDateTimePart.Visible = !this.ShowOnlyStartTimeDate;

        ScriptManager.GetCurrent(this.Page).Services.Add(new ServiceReference("~/Orion/APM/Services/TimePeriodValidator.asmx"));
    }

    /// <summary>
    /// Page OnLoad
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // for Search Endpoints resource - javascript can't be registered if control is disabled
        if (this.Visible == false)
        {
            return;
        }

		if (string.IsNullOrWhiteSpace(this.txtStartDate.Text))
			this.txtStartDate.Text = DateTime.UtcNow.AddDays(-7).ToShortDateString();
		if (string.IsNullOrWhiteSpace(this.txtStartTime.Text))
			this.txtStartTime.Text = DateTime.UtcNow.ToShortTimeString();

		if (string.IsNullOrWhiteSpace(this.txtEndDate.Text))
			this.txtEndDate.Text = DateTime.UtcNow.ToShortDateString();
		if (string.IsNullOrWhiteSpace(this.txtEndTime.Text))
			this.txtEndTime.Text = DateTime.UtcNow.ToShortTimeString();

		ScriptManager sm = ScriptManager.GetCurrent(this.Page);

        if (sm == null)
        {
            sm = new ScriptManager();
            this.Controls.AddAt(0, sm);
        }

        RegisterScript();

        if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), CustomValidateFunctionName))
        {
            string validateScript =
                string.Format(
                    @"function {0}(source, clientside_arguments) {{
	                    var validationResult = false;
                        $.ajax({{ type: ""POST"", url: ""/Orion/APM/Services/TimePeriodValidator.asmx/ValidateTime"",
		                    data: ""{{'startDateString':'"" + $(""#{1}"").val() +
                            ""', 'startTimeString':'"" + $(""#{2}"").val() +
                            ""', 'endDateString':'"" + $(""#{3}"").val() +
                            ""', 'endTimeString':'"" + $(""#{4}"").val() + ""'}}"",
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            async: false,
                            success: function (result) {{ 
                                validationResult = result.d.IsValid;
                            }},
                            error: function(error) {{ validationResult = false; }}
                            }});
	
	                    clientside_arguments.IsValid = validationResult;
                    }}",
                    CustomValidateFunctionName, this.txtStartDate.ClientID, this.txtStartTime.ClientID, this.txtEndDate.ClientID, this.txtEndTime.ClientID);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), CustomValidateFunctionName, validateScript, true);
        }

        valPeriodCustom.ClientValidationFunction = CustomValidateFunctionName;
        SetupValidator(valPeriodCustom);
    }

    /// <summary>
    /// Page OnPrerender
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        ScriptManager sm1 = ScriptManager.GetCurrent(this.Page);

        if (sm1 == null)
        {
            sm1 = new ScriptManager();
            this.Controls.AddAt(0, sm1);
        }

        base.OnPreRender(e);

        ScriptManager sm2 = ScriptManager.GetCurrent(this.Page);

        if (sm2 == null)
        {
            sm2 = new ScriptManager();
            this.Controls.AddAt(0, sm2);
        }
    }

    /// <summary>
    /// Page Render override
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
        //important !!! neccessary to render div tags 
        //if not present client side api will not be able work with the control

        writer.AddAttribute("id", this.ClientID);
        writer.RenderBeginTag(HtmlTextWriterTag.Div);

        base.Render(writer);

        writer.RenderEndTag();
    }


    /// <summary>
    /// Period server validation event
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void valPeriodCustom_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = TimePeriodHelper.DoValidateTime(this.txtStartDate.Text, this.txtStartTime.Text, this.txtEndDate.Text, this.txtEndTime.Text).IsValid;
    }

    /// <summary>
    /// Validation group setup
    /// </summary>
    /// <param name="validator"></param>
    private void SetupValidator(BaseValidator validator)
    {
        if (!string.IsNullOrEmpty(this.ValidationGroup))
        {
            validator.ValidationGroup = this.ValidationGroup;
        }
    }

    /// <summary>
    /// Pre-fill child controls from parameter
    /// </summary>
    /// <param name="periodFilter"></param>
    private void SelectTimePeriod(TimePeriodUiFilter periodFilter)
    {
        if (periodFilter == null || periodFilter.TimePeriodType != TimePeriodTypeEnum.AbsolutePeriod)
        {
            return;
        }

        this.txtStartDate.Text = periodFilter.AbsolutePeriodStart.ToShortDateString();
        this.txtStartTime.Text = periodFilter.AbsolutePeriodStart.ToShortTimeString();

        this.txtEndDate.Text = periodFilter.AbsolutePeriodEnd.ToShortDateString();
        this.txtEndTime.Text = periodFilter.AbsolutePeriodEnd.ToShortTimeString();
    }

    /// <summary>
    /// Script manager instance
    /// </summary>
    /// <returns></returns>
    protected ScriptManager GetScriptManager()
    {
        ScriptManager sm = ScriptManager.GetCurrent(this.Page);

        if (sm == null)
        {
            sm = new ScriptManager();
        }


        ScriptManager ret = ScriptManager.GetCurrent(Page);

        if (ret == null)
        {
            throw new HttpException("A ScriptManager control must exist on the current page.");
        }

        return ret;
    }

    /// <summary>
    /// Control input child controls enable / disable script template
    /// </summary>
    const string clientEnableTemplate =
            @"
                var absStartDate = document.getElementById('{0}');
                var absStartTime = document.getElementById('{1}');
                var absEndDate = document.getElementById('{2}');
                var absEndTime = document.getElementById('{3}');

                absStartDate.disabled = {4};
                absStartTime.disabled = {4};
                absEndDate.disabled = {4};
                absEndTime.disabled = {4};
                //$('.ui-datepicker-trigger').css('visibility', 'visible');
                $(absStartDate).datepicker('{5}');
                  $(absEndDate).datepicker('{5}');
                ValidatorEnable(document.getElementById('{6}'), {7});
                ";


    /// <summary>
    /// Control input child controls enable script
    /// </summary>
    /// <returns></returns>
    public string GetClientEnableScript()
    {
        return string.Format(clientEnableTemplate, CIDabsStartDate, CIDabsStartTime, CIDabsEndDate, CIDabsEndTime, "false", "enable", this.valPeriodCustom.ClientID, "true");
    }

    /// <summary>
    /// Control input child controls disable script
    /// </summary>
    /// <returns></returns>
    public string GetClientDisableScript()
    {
        return string.Format(clientEnableTemplate, CIDabsStartDate, CIDabsStartTime, CIDabsEndDate, CIDabsEndTime, "true", "disable", this.valPeriodCustom.ClientID, "false");
    }

    /// <summary>
    /// Javascript datepicker initialization script
    /// </summary>
    /// <param name="pStartDate"></param>
    /// <param name="pEndDate"></param>
    /// <param name="pStartTime"></param>
    /// <param name="pEndTime"></param>
    /// <returns></returns>
    private string SCRIPT(string pStartDate, string pEndDate, string pStartTime, string pEndTime)
    {
        string ret = @" 

            $(document).ready(function() {
          
                var regionalSettings =  " + DatePickerRegionalSettings + @";
                $.dpStart = $('#" + pStartDate + @"');
                $.dpEnd = $('#" + pEndDate + @"');
                
                jQuery.each([$.dpStart, $.dpEnd], function() {

                this.datepicker({
                    setDefaults: regionalSettings,
                    showOn: 'both',
                    duration: 'fast',
                    buttonImage: '/Orion/js/extjs/resources/images/default/shared/calendar.gif',
                    buttonImageOnly: true,
                    closeAtTop: false, 
                    mandatory: true,
                    //dateFormat: 'yy-mm-dd',
                    dateFormat: regionalSettings.dateFormat,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true
                    });

                });
               
                //alert(regionalSettings.dateFormat);

                $.tpStart = $('#" + pStartTime + @"');
                $.tpEnd = $('#" + pEndTime + @"');

                jQuery.each([$.tpStart, $.tpEnd], function() {

                this.timePicker({   
                    separator:regionalSettings.timeSeparator, 
                    show24Hours:regionalSettings.show24Hours,
                    step:15,
                    endTime:new Date(0, 0, 0, 23, 45, 0)
                    });
                this.attr('onchange','');

                });
            
            //
            if ($(""input[id$='radAbsolute']"").attr('checked'))
            {
                $.dpStart.datepicker('enable');
                $.dpEnd.datepicker('enable');
            }
            else
            {
               $.dpStart.datepicker('disable');
               $.dpEnd.datepicker('disable');
            }

            });
            ";

        return ret;
    }

    /// <summary>
    /// Client side scripts registering
    /// </summary>
    private void RegisterScript()
    {
        // OrionInclude.ModuleFile("APM", "../js/jquery/ui.datepicker.css", OrionInclude.Section.Unspecified); -- no need to include, the styles for datepicker are defined in jquery-ui.css too
        OrionInclude.ModuleFile("APM", "../js/jquery/timePicker.css", OrionInclude.Section.Unspecified);
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "datePickerDefaults" + this.ClientID, SCRIPT(txtStartDate.ClientID, txtEndDate.ClientID, txtStartTime.ClientID, txtEndTime.ClientID), true);
    }

}
