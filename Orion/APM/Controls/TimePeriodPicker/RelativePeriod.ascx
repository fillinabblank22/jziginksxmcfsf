<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RelativePeriod.ascx.cs" Inherits="Orion_APM_Controls_TimePeriodPicker_RelativePeriod" %>

<asp:Label runat="server"  Text="Last" CssClass="PeriodSelector" ></asp:Label>
<asp:TextBox runat="server" Columns="5" ID="txtQuantity" MaxLength="6" />
<asp:ListBox runat="server" ID="lbxTimeType" Rows="1" SelectionMode="single" />

<asp:RequiredFieldValidator runat="server" ID="valQuantityRequired" ControlToValidate="txtQuantity" Display="Dynamic" ErrorMessage="$resources:APMWebContent,RelativeTimePeriod_PleaseEnterValue">
    <a href="#" onclick="return false;" class="apmValIcon" runat="server" title="$resources:APMWebContent,RelativeTimePeriod_valCustomValidatorIcon_Title">&nbsp;</a>
</asp:RequiredFieldValidator>

<asp:CustomValidator runat="server" ID="valQuantityCustom" 
    ClientValidationFunction="ClientValidateRelativePeriodRange" 
    onservervalidate="valQuantityCustom_ServerValidate"
    ControlToValidate="txtQuantity" 
    Display="Dynamic">
    <a onclick="return false;" id="valIcon" runat="server" href="#" class="apmValIcon" title="$resources:APMWebContent,RelativeTimePeriod_PleaseEnterValue">&nbsp;</a>
</asp:CustomValidator>
<br />