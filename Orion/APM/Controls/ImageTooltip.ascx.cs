﻿using System.ComponentModel;

public partial class Orion_APM_Controls_ImageTooltip : System.Web.UI.UserControl
{
    private const string TooltipJavascriptTemplate = @"<script type=""text/javascript"">
    Ext.onReady(function () {
        new Ext.ToolTip({
            target: ""ClientID"",
            title: ""ToolTipMessage"",
            showDelay: 500,
            autoHide: false,
            trackMouse: false,
            closable: true,
            anchor: 'left'
        }).addClass('tooltipYellow');
    });
</script>
";

    [
    Localizable(true), 
    Bindable(false), 
    Category("Appearance"), 
    DefaultValue("")
    ]
    public string Text { get; set; }

    protected override void OnPreRender(System.EventArgs e)
    {
        var script = TooltipJavascriptTemplate
            .Replace("ClientID", portNumberTooltip.ClientID)
            .Replace("ToolTipMessage", Text ?? Resources.APMWebContent.APMWEBDATA_NotSpecified);
        this.Page.ClientScript.RegisterClientScriptBlock(typeof(Orion_APM_Controls_ImageTooltip), ClientID + "Tooltip", script);
    }
}