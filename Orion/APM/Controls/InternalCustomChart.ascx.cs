using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;

using KEYS = SolarWinds.APM.Web.Charting.ChartInfo.KEYS;

public partial class Orion_APM_Controls_InternalCustomChart : UserControl
{
	#region Fields

	private const string CustomText = "Custom";
	private bool enableEditing = true;
	private DataSet chartData;
	private ChartInfo chartInfo;
	#endregion

	#region Properties

	public bool EnableEditing
	{
		get { return enableEditing; }
		set { enableEditing = value; }
	}

	public DataSet ChartData
	{
		get
		{
			if (chartData == null)
			{
			    chartData = ChartInfo.LoadData();
			}
			return chartData;
		}
	}

	public ChartInfo ChartInfo
	{
		get { return chartInfo; }
		set { chartInfo = value; }
	}

	public string ChartName
	{
		get { return Request.Params[KEYS.ChartName]; }
	}

	public string NetObjectId
	{
        get { return Request.Params[KEYS.NetObject] ?? Request.Params["NetObjectID"]; }
	}

    private string StatisticValueName
    {
        get { return Request.Params[KEYS.StatisticValueName]; }
    }

    private string ShowYAxisFromZero
    {
        get { return Request.Params[KEYS.ShowYAxisFromZero]; }
    }
	#endregion

	#region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!Page.IsPostBack)
		{
			// Filling dropdown list with periods
			foreach (string period in Periods.GenerateSelectionList())
			{
                TimePeriodsList.Items.Add(new ListItem(GetLocalizedProperty("Period", period), period));
			}
			TimePeriodsList.Items.Add(new ListItem(Resources.APMWebContent.APMWEBCODE_VB1_26, CustomText));

			SampleSizeList.DataSource = SampleSizes.GenerateSelectionList();
			SampleSizeList.DataTextField = "Name";
			SampleSizeList.DataValueField = "SizeName";
			SampleSizeList.DataBind();

			editControlsContainer.Visible = enableEditing;

			//// Create the chart info with the default values.
			this.ChartInfo = GetDefaultChartInfo();

			//// Load the chart info with value from the URL
			UpdateInfoFromRequest(this.ChartInfo);

			SampleSizeList.SelectedValue = this.ChartInfo.SampleSize;
			Width.Text = this.ChartInfo.Width.ToString();
			Height.Text = this.ChartInfo.Height.ToString();

			this.chartImage.Alt = this.ChartInfo.SubTitle;
			this.chartImage.Src = this.BuildUrl(ChartInfo.SourceURL);

			// Set the Export Url correctly.
			RawDataLinkTop.NavigateUrl = BuildUrl(RawDataLinkTop.NavigateUrl);
			RawDataLinkBottom.NavigateUrl = BuildUrl(RawDataLinkBottom.NavigateUrl);
            this.AddTopRightLinks();
		}
	}

	protected void OnRefresh(object sender, EventArgs e)
	{
		Response.Redirect(BuildUrl(Request.Url.AbsolutePath));
	}

	protected void OnPrintableVersion(object sender, EventArgs e)
	{
		Response.Redirect(BuildUrl("/Orion/APM/ViewChart.aspx"));
	}

	#endregion

	#region Helper Members

    private void AddTopRightLinks()
    {
        if (this.Page.Master == null) return;

        try
        {
            var ctrl = ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinks");
            if (ctrl == null) return;

            ctrl.Controls.Add(new HyperLink
            {
                ID = "Img2",
                NavigateUrl = BuildUrl("/Orion/APM/ViewChart.aspx"),
                CssClass = "printablePageLink",
                Text = Resources.APMWebContent.APMWEBDATA_VB1_67,    
                EnableViewState = false
            });

            ctrl.Controls.Add(new ASP.IconHelpButton
            {
                ID = "helpButton",
                HelpUrlFragment = "OrionAPMPHCustomCharts",
                EnableViewState = false
            });
        }

        catch (System.Web.HttpException)
        {
            // -- shouldn't occur --
            // the page may be using this control and incompatible - having control blocks in a content section.
            // if so, this exception will occur. we'd rather default to not having the links show up than see an error page.
            // -- shouldn't occur --
        }
    }

	private ChartInfo GetDefaultChartInfo()
	{
		ChartInfo info = ChartInfoFactory.Create(this.ChartName);
		info.NetObject = NetObjectFactory.Create(this.NetObjectId);
		
		info.SampleSize = ChartInfo.DefaultSampleSize;
		info.Period = ChartInfo.DefaultPeriod;

		info.Title = info.NetObject.Name;
		info.SubTitle = info.DisplayName;
		
		info.Width = ChartInfo.DefaultChartWidth;

		return info;
	}

	private void UpdateInfoFromRequest(ChartInfo info)
	{
		this.SetPeriodValue(info);
		if (!String.IsNullOrEmpty(Request.Params[KEYS.SampleSize]))
		{
			info.SampleSize = Request.Params[KEYS.SampleSize];
		}

		if (!String.IsNullOrEmpty(Request.Params[KEYS.Title]))
		{
			info.Title = Request.Params[KEYS.Title];
		}
		ChartTitle.Text = info.Title;

		if (!String.IsNullOrEmpty(Request.Params[KEYS.SubTitle]))
		{
			info.SubTitle = Request.Params[KEYS.SubTitle];
		}
		SubTitle.Text = info.SubTitle;

		if (!String.IsNullOrEmpty(Request.Params[KEYS.SubTitle2]))
		{
			info.SubTitle2 = Request.Params[KEYS.SubTitle2];
		}
		else 
		{
			info.SubTitle2 = ChartInfo.BuildSubTitle2(info.Period, info.SampleSize);
		}
		SubTitle2.Text = info.SubTitle2;

		int intValue;
		if (int.TryParse(Request.Params[KEYS.Width], out intValue))
		{
			info.Width = intValue;
		}
		Width.Text = info.Width.ToString();
		
		if (int.TryParse(Request.Params[KEYS.Height], out intValue))
		{
			info.Height = intValue;
		}
		Height.Text = info.Height.ToString();
		
		if (int.TryParse(Request.Params[KEYS.FontSize], out intValue))
		{
			info.FontSize = intValue;
		}

	    info.DataColumnName = Request.Params[KEYS.StatisticValueName];

	    bool showYAxisFromZero = true;
	    if(bool.TryParse(Request.Params[KEYS.ShowYAxisFromZero], out showYAxisFromZero ))
	    {
	        info.ShowYAxisFromZero = showYAxisFromZero;
	    }
	}

	private void SetPeriodValue(ChartInfo info)
	{
		String period = Request.Params[KEYS.Period];
		if (String.IsNullOrEmpty(period))
		{
			period = ChartInfo.DefaultPeriod;
		}
		else
		{
			if (period == CustomText)
			{
				DateTime periodBegin, periodEnd;
				PeriodBeginError.Visible = !DateTime.TryParse(Request.Params[KEYS.PeriodBegin], out periodBegin);
				if (!PeriodBeginError.Visible)
				{
					info.StartTime = periodBegin;
					PeriodBegin.Text = periodBegin.ToString();
				}
				PeriodEndError.Visible = !DateTime.TryParse(Request.Params[KEYS.PeriodEnd], out periodEnd);
				if (!PeriodEndError.Visible)
				{
					info.EndTime = periodEnd;
					PeriodEnd.Text = periodEnd.ToString();
				}
			}
		}
		info.Period = period;
		TimePeriodsList.SelectedValue = period;
		if (period.Equals("TODAY", StringComparison.InvariantCultureIgnoreCase)) 
		{
			foreach (ListItem item in TimePeriodsList.Items)
			{
                if (item.Value.Equals("TODAY", StringComparison.InvariantCultureIgnoreCase))
				{
					item.Selected = true;
					break;
				}
			}
		}
	}

	private string BuildUrl(string basePage)
	{
		var builder = new SolarWinds.APM.Web.UrlBuilder(basePage);

		builder[KEYS.ChartName] = this.ChartName;
		builder[KEYS.NetObject] = this.NetObjectId;

		if (!String.IsNullOrEmpty(ChartTitle.Text))
			builder[KEYS.Title] = ChartTitle.Text;

		if (!String.IsNullOrEmpty(SubTitle.Text))
			builder[KEYS.SubTitle] = SubTitle.Text;

		if (!String.IsNullOrEmpty(SubTitle2.Text))
			builder[KEYS.SubTitle2] = SubTitle2.Text;

		bool isCustom = TimePeriodsList.SelectedValue == CustomText;
		builder[KEYS.Period] = TimePeriodsList.SelectedValue;

		if (isCustom)
		{
			if (!String.IsNullOrEmpty(PeriodBegin.Text))
				builder[KEYS.PeriodBegin] = PeriodBegin.Text;

			if (!String.IsNullOrEmpty(PeriodEnd.Text))
				builder[KEYS.PeriodEnd] = PeriodEnd.Text;
		}

		if (!String.IsNullOrEmpty(SampleSizeList.SelectedValue))
			builder[KEYS.SampleSize] = SampleSizeList.SelectedValue;

		if (!String.IsNullOrEmpty(Width.Text))
			builder[KEYS.Width] = Width.Text;

		if (!String.IsNullOrEmpty(Height.Text))
			builder[KEYS.Height] = Height.Text;

        if (StatisticValueName != null && (this.ChartName.Equals(ChartInfo.TYPES.MonMMADynamicStatistic) || this.ChartName.Equals(ChartInfo.TYPES.MonStatisticDataAreaChart) || this.ChartName.Equals(ChartInfo.TYPES.MonStatisticDataLineChart)))
        {
            builder[KEYS.StatisticValueName] = StatisticValueName;
        }

	    builder[KEYS.ShowYAxisFromZero] = ShowYAxisFromZero;

	    return builder.Url;
	}

	protected static string DateTimeSyntaxExamples()
	{
		//<b>1/8/2008</b> or <b>08-Jan-08</b> or <b>11:58 AM</b> or <b>1/8/2008 11:58:10 AM</b> or <b>Jan 08,2008 11:58</b>
		DateTime dt = DateTime.Now;

        return String.Format(Resources.APMWebContent.APMWEBCODE_VB1_25,
							dt.ToShortDateString(),
							dt.ToShortTimeString(),
							dt.ToLocalTime());
	}

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

	#endregion
}
