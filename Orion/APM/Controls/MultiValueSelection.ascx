﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiValueSelection.ascx.cs" Inherits="Orion_APM_Controls_MultiValueSelection" %>

<orion:Include runat="server" File="APM/APM.css"/>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div>
            <asp:RadioButtonList runat="server" ID="SelectionType" OnSelectedIndexChanged="SelectionType_OnSelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_334 %>" Value="All" Selected="True"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources : APMWebContent, APMWEBDATA_VB1_335 %>" Value="SelectedOnly"></asp:ListItem>
            </asp:RadioButtonList>

            <asp:Panel runat="server" ID="SelectItemsPanel">
        
            <div class="apm_indentedList apm_itemSelection">
                <asp:CheckBoxList runat="server" ID="chblSelectedValues" DataTextField="Label" DataValueField="Name" DataTextFormatString="<%$ Resources : APMWebContent , APMWEBCODE_VB1_145 %>">
                </asp:CheckBoxList>
                <asp:CustomValidator runat="server" ID="Validator"
                 ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_336 %>"
                 OnServerValidate="Validator_OnServerValidate"></asp:CustomValidator>
             </div>
             </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>