﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_Charts_EditCustomChart : BaseResourceEditControl, IChartEditorSettings
{
    protected const string selectStatisticData = "Select Statistics:";

    private ResourceInfo Resource
    {
        get
        {
            if (base.Resource == null && !string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
            {
                base.Resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));
            }
            return base.Resource;
        }
    }

    private BaseResourceControl resourceControl;

    private BaseResourceControl ResourceControl
    {
        get
        {
            if (resourceControl == null)
            {
                resourceControl = (BaseResourceControl) LoadControl(Resource.File);
                resourceControl.Resource = Resource;
            }
            return resourceControl;
        }
    }

    private NetObjectManager netObjectManager;

    private NetObjectManager NetObjectManager
    {
        get { return this.netObjectManager ?? (this.netObjectManager = new NetObjectManager(this.ResourceControl)); }
    }

    private new string NetObjectID
    {
        get
        {
            if (base.NetObjectID == null)
            {
                var netObject = NetObjectManager.TryGetNetObject();
                if (netObject != null)
                {
                    base.NetObjectID = netObject.NetObjectID;
                }
            }
            return base.NetObjectID;
        }
    }

    private Component component;

    private Component Cmp
    {
        get
        {
            if (component == null)
            {
                long id = 0;

                if (NetObjectHelper.IsApplication(NetObjectID))
                {
                    if (ctrAssigner.SelectedComponent != null)
                    {
                        Int64.TryParse(ctrAssigner.SelectedComponent.Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out id);
                    }
                }
                else
                {
                    id = (Int32) NetObjectHelper.GetId(NetObjectID);
                }

                if (id > 0)
                {
                    using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                    {
                        component = bl.GetComponent(id);
                    }
                }
            }
            return component;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        ctrAssigner.OnSelectedNameChanged += new EventHandler(CtrAssigner_OnSelectedNameChanged);
        DataSource.OnSelectedDataSourceTypeChanged += new EventHandler(DataSource_OnSelectedDataSourceTypeChanged);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        InistializeDataSource(netObjectId);

        ctrAssigner.dataSourceType = DataSource.SelectedDataSourceType;
        ctrAssigner.PopulateControl(Resource);

        DynamicStatisticType.Visible = false;
        PopulateDynamicStatisticTypeDropDown();

        var info = default(AssignedToResourceInfo);
        AssignedToResourceInfo.TryCreate(Resource.Properties[ApmMonitor.PropAssignedToResourceInfo], NetObjectID, out info);

        if (
            (info[AssignedToResourceInfo.ColumnNames] != null)
            && (Cmp != null)
            && Cmp.DynamicColumnSettings.Any(item => item.Name.Equals(info[AssignedToResourceInfo.ColumnNames]))
            )
        {
            HtmlHelper.SetListSelectedValue(DynamicStatisticTypeDropDown, info[AssignedToResourceInfo.ColumnNames].ToString(), null);
        }
        else if (Resource.Properties.ContainsKey("StatisticName"))
        {
            HtmlHelper.SetListSelectedValue(DynamicStatisticTypeDropDown, Resource.Properties["StatisticName"], null);
        }
    }

    private void InistializeDataSource(string netObjectId)
    {
        if (NetObjectHelper.IsComponent(netObjectId))
        {
            var appId = new ComponentDal().GetComponent(NetObjectHelper.GetId(netObjectId)).ApplicationID;
            DataSource.Initialize(appId);
        }
        else
        {
            DataSource.Initialize();
        }
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        string name = string.Empty;
        string id = string.Empty;
        Dictionary<string, object> EditProperties = new Dictionary<string, object>();

        if (ctrAssigner.SelectedComponent != null)
        {
            name = ctrAssigner.SelectedComponent.Text;
            id = ctrAssigner.SelectedComponent.Value;
        }

        if (NetObjectHelper.IsApplication(NetObjectID))
        {
            EditProperties[ApmMonitor.PropComponentName] = name;
        }
        else
        {
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var cmp = bl.GetComponent(NetObjectHelper.GetId(NetObjectID));

                EditProperties[ApmMonitor.PropComponentName] = cmp.Name;
                id = cmp.Id.ToString(CultureInfo.InvariantCulture);
            }
        }

        if (!string.IsNullOrEmpty(DataSource.SelectedDataSourceType) && Resource.Properties.ContainsKey("netobjectprefix"))
        {
            var chartName = GetProperChartName();
            EditProperties["ChartName"] = chartName;

            AssignedToResourceInfo.Update(NetObjectID, Resource, new[] {new DictionaryEntry(AssignedToResourceInfo.CustomChartName, chartName)});
        }

        AssignedToResourceInfo.Update(NetObjectID, Resource, new[] {new DictionaryEntry(AssignedToResourceInfo.CmpID, id)});

        if (Cmp.IsDynamicBased)
        {
            EditProperties["StatisticName"] = DynamicStatisticTypeDropDown.SelectedValue;

            AssignedToResourceInfo.Update(
                NetObjectID ?? AssignedToResourceInfo.GetNetObject(Request,Resource), 
                Resource, 
                new[] {new DictionaryEntry(AssignedToResourceInfo.ColumnNames, DynamicStatisticTypeDropDown.SelectedValue)});
        }

        if (EditProperties.Any())
        {
            foreach (var editProperty in EditProperties)
            {
                if (properties.ContainsKey(editProperty.Key))
                {
                    properties[editProperty.Key] = editProperty.Value;
                }
                else
                {
                    properties.Add(editProperty.Key, editProperty.Value);
                }
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get { return new Dictionary<string, object>(); }
    }

    private void CtrAssigner_OnSelectedNameChanged(object sender, EventArgs e)
    {
        component = null;
        DynamicStatisticType.Visible = false;

        if (Cmp.IsDynamicBased && DataSource.SelectedDataSourceType.Equals(DataSourceTypes.StatisticData))
        {
            PopulateDynamicStatisticTypeDropDown();
            DynamicStatisticType.Visible = true;
        }
    }

    private void DataSource_OnSelectedDataSourceTypeChanged(object sender, EventArgs e)
    {
        if (ctrAssigner.Visible)
        {
            ctrAssigner.dataSourceType = DataSource.SelectedDataSourceType;
            ctrAssigner.PopulateControl(Resource, ctrAssigner.SelectedComponent == null ? string.Empty : ctrAssigner.SelectedComponent.Text);
        }

        DynamicStatisticType.Visible = false;
        PopulateDynamicStatisticTypeDropDown();
    }

    private void PopulateDynamicStatisticTypeDropDown()
    {
        if (Cmp != null
            && Cmp.IsDynamicBased
            && DataSource.SelectedDataSourceType.Equals(DataSourceTypes.StatisticData))
        {
            DynamicStatisticTypeDropDown.Items.Clear();
            foreach (var item in from columnSetting in Cmp.DynamicColumnSettings
                where columnSetting.Type == DynamicEvidenceColumnDataType.Numeric
                      && !columnSetting.Disabled
                select new ListItem(columnSetting.Label, columnSetting.Name))
            {
                DynamicStatisticTypeDropDown.Items.Add(item);
            }
            DynamicStatisticType.Visible = true;
        }
    }

    private string GetProperChartName()
    {
        if (DataSource.SelectedDataSourceType.Equals(DataSourceTypes.Availability))
        {
            return GetAvailabilityChartName();
        }
        if (DataSource.SelectedDataSourceType.Equals(DataSourceTypes.ResponseTime))
        {
            return GetResponseTimeChartName();
        }
        if (DataSource.SelectedDataSourceType.Equals(DataSourceTypes.StatisticData))
        {
            return GetStatisticDataChartName();
        }

        return string.Empty;
    }

    private string GetStatisticDataChartName()
    {
        if (Resource.Properties["netobjectprefix"].Equals("AACB"))
            return "APMCustomBarChartStatisticData";
        if (Resource.Properties["netobjectprefix"].Equals("AACL"))
            return "APMCustomLineChartStatisticData";
        if (Resource.Properties["netobjectprefix"].Equals("AACA"))
            return "APMCustomAreaChartStatisticData";

        return string.Empty;
    }

    private string GetResponseTimeChartName()
    {
        if (Resource.Properties["netobjectprefix"].Equals("AACB"))
            return "APMCustomBarChartResponseTime";
        if (Resource.Properties["netobjectprefix"].Equals("AACL"))
            return "APMCustomLineChartResponseTime";
        if (Resource.Properties["netobjectprefix"].Equals("AACA"))
            return "APMCustomAreaChartResponseTime";

        return string.Empty;
    }

    private string GetAvailabilityChartName()
    {
        if (Resource.Properties["netobjectprefix"].Equals("AACB"))
            return "APMCustomBarChartAvailability";
        if (Resource.Properties["netobjectprefix"].Equals("AACL"))
            return "APMCustomLineChartAvailability";
        if (Resource.Properties["netobjectprefix"].Equals("AACA"))
            return "APMCustomAreaChartAvailability";

        return string.Empty;
    }
}
