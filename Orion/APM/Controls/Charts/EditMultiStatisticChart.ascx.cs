﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;

public partial class Orion_APM_Controls_Charts_EditMultiStatisticChart : BaseResourceEditControl, IChartEditorSettings
{
	protected const string selectStatisticData = "Select Statistics:";
	private NetObjectManager netObjectManager;
	private NetObjectManager NetObjectManager
	{
		get { return this.netObjectManager ?? (this.netObjectManager = new NetObjectManager(this.ResourceControl)); }
	}

	private BaseResourceControl resourceControl;
	private BaseResourceControl ResourceControl
	{
		get
		{
			if (resourceControl == null)
			{
				resourceControl = (BaseResourceControl)LoadControl(Resource.File);
				resourceControl.Resource = Resource;
			}
			return resourceControl;
		}
	}

	private string NetObjectID
	{
		get
		{
			if (base.NetObjectID == null)
			{
				var netObject = NetObjectManager.TryGetNetObject();
				if (netObject != null)
				{
					base.NetObjectID = netObject.NetObjectID;
				}
			}
			return base.NetObjectID;
		}
	}

	protected ApmMonitor monitor;
	protected ApmMonitor Monitor
	{
		get
		{
			if (monitor == null)
			{
				if (ResourceControl.Parent != null)
				{
					monitor = NetObjectManager.Monitor;
				}
				else if (NetObjectHelper.IsApplication(NetObjectID) && (ctrAssigner.SelectedComponent != null))
				{
					monitor = new ApmMonitor(string.Format(CultureInfo.InvariantCulture, "AM:{0}", ctrAssigner.SelectedComponent.Value));
				}
				else if (!string.IsNullOrEmpty(NetObjectID))
				{
					monitor = new ApmMonitor(NetObjectID);
				}
			}
			return monitor;
		}
	}

	private ResourceInfo Resource
	{
		get
		{
			if (base.Resource == null && !string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
			{
				base.Resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));
			}
			return base.Resource;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!Page.IsPostBack)
		{
			InitMultiValueSelection();
		}
		ctrAssigner.OnSelectedNameChanged += ctrAssigner_OnSelectedNameChanged;
	}

	public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
	{
		ctrAssigner.PopulateControl(resourceInfo);
		InitMultiValueSelection();
	}

	void ctrAssigner_OnSelectedNameChanged(object sender, EventArgs e)
	{
		monitor = null;
		InitMultiValueSelection();
	}

	protected void InitMultiValueSelection()
	{
		MultiValueSelection.Visible = Monitor.BaseComponent.IsScript;
		MultiValueSelection.LoadData(Monitor.Name, Monitor.Id.ToString(CultureInfo.InvariantCulture));
		MultiValueSelection.InitializeSettings(this.Resource);
	}

	public void SaveProperties(Dictionary<string, object> properties)
	{
		var valuesToDisplay = MultiValueSelection.GetSettingsForUpdate();

		var values = new List<DictionaryEntry>();
		values.Add(new DictionaryEntry(AssignedToResourceInfo.ColumnNames, valuesToDisplay));
	    if (ctrAssigner.SelectedComponent != null)
	    {
			values.Add(new DictionaryEntry(AssignedToResourceInfo.CmpID, ctrAssigner.SelectedComponent.Value));
	    }
		AssignedToResourceInfo.Update(NetObjectID, Resource, values.ToArray());
		properties[ResourceSettings.ValuesToDisplay] = valuesToDisplay;
	}

	public override Dictionary<string, object> Properties
	{
		get { return new Dictionary<string, object>(); }
	}
}