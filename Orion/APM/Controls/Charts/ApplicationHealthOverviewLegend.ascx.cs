﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_APM_Controls_Charts_ApplicationHealthOverviewLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "apm_OverviewLegendInitializer__" + legend.ClientID; } }
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}