﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_Charts_EditApplicationChart : BaseResourceEditControl, IChartEditorSettings
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private string TryGetNetObject()
    {
        if (string.IsNullOrEmpty(NetObjectID) && NetObjectManager.IsCustomObjectResource(Resource))
        {
            NetObjectID = Resource.Properties["NetObjectID"];
        }

        return NetObjectID;
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        ctrAssigner.PopulateControl(resourceInfo, netObjectId);
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        string id = string.Empty;

        if (ctrAssigner.SelectedComponent != null)
        {
            id = ctrAssigner.SelectedComponent.Value;
            AssignedToResourceInfo.Update(TryGetNetObject(), Resource, new[] { new DictionaryEntry(AssignedToResourceInfo.CmpID, id) });
        }

        if (Resource.Properties[ApmMonitor.PropAssignedToResourceInfo] != null)
            properties[ApmMonitor.PropAssignedToResourceInfo] = Resource.Properties[ApmMonitor.PropAssignedToResourceInfo];
    }

    public override Dictionary<string, object> Properties
    {
        get { return new Dictionary<string, object>(); }
    }
}