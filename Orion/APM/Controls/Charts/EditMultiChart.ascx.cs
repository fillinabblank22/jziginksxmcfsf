﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

using ApmResourceProperties = SolarWinds.APM.Web.UI.Resource.ApmMultiChartBaseResource.ApmResourceProperties;

public partial class Orion_APM_Controls_Charts_EditMultiChart : BaseResourceEditControl, IChartEditorSettings
{
    protected string thresholdCheckboxTitle = "Show Thresholds";

    public class ParamName
    {
        public const string ResourceId = "ResourceID";
        public const string CompIds = "componentsUIDs";
        public const string AllowThres = "allowthresholds";
    }

    public class BoolText
    {
        public static string True = "True";
        public static string False = "False";
    }

    private ResourceInfo Resource
    {
        get
        {
            var resId = Request.QueryString[ParamName.ResourceId];
            if (base.Resource == null && !string.IsNullOrEmpty(resId))
            {
                base.Resource = ResourceManager.GetResourceByID(Convert.ToInt32(resId));
            }
            return base.Resource;
        }
    }

    private BaseResourceControl resourceControl;
    private BaseResourceControl ResourceControl
    {
        get
        {
            if(resourceControl == null)
            {
                resourceControl = (BaseResourceControl)LoadControl(Resource.File);
                resourceControl.Resource = Resource;
            }
            return resourceControl;
        }
    }

    private NetObjectManager netObjectManager;
    private NetObjectManager NetObjectManager
    {
        get { return this.netObjectManager ?? (this.netObjectManager = new NetObjectManager(this.ResourceControl)); }
    }

    private NetObject netObject;
    public NetObject NetObject
    {
        get
        {
            if (this.netObject == null)
            {
                this.netObject = NetObjectID == null ? this.NetObjectManager.TryGetNetObject() : NetObjectFactory.Create(NetObjectID);
            }

            return this.netObject;
        }
    }
    
    /// <summary>
    /// Entry point, initialization of multi chart control
    /// </summary>
    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        IApplicationNavigationInfo appInfo;
        IApplicationItemNavigationInfo appItemInfo;

        // Reading resource information and its properties
        int appId = -1;
        int appItemId = -1;
        bool isApplicationItem = false;
        if ((appItemInfo = this.NetObject as IApplicationItemNavigationInfo) != null)
        {
            isApplicationItem = true;
            appId = appItemInfo.ApplicationID;
            appItemId = appItemInfo.ApplicationItemID;
        }
        else if ((appInfo = this.NetObject as IApplicationNavigationInfo) != null)
        {
            appId = appInfo.ApplicationID;
        }

        // Filling of checkbox list by available components
        this.ComponentsAssigner.PopulateControl(resourceInfo, appId, appItemId, this.NetObject.NetObjectID, isApplicationItem);

        // Set tresholds checkbox based on resource properties
        this.ThresholdCheckbox.Checked = BoolText.True.EqualsInvariant(resourceInfo.Properties.GetValueOrDefault(ApmResourceProperties.AllowThres, BoolText.True));
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        var allowThresholds = this.ThresholdCheckbox.Checked ? BoolText.True : BoolText.False;

        var oldSelectedText = this.Resource.Properties.GetValueOrDefault(ApmResourceProperties.CompIds) ?? string.Empty;
        var oldSelectedComponentsList = oldSelectedText.Split(';').WhereNot(string.IsNullOrWhiteSpace).ToList();

        var selectedComponentsList = this.ComponentsAssigner.GetSelectedComponents().Select(s => s.Value).ToList();
        var supportedComponentsList = this.ComponentsAssigner.GetSupportedComponents().Select(s => s.Value).ToList();

        selectedComponentsList = selectedComponentsList
            .Union(oldSelectedComponentsList.Except(supportedComponentsList, StringComparer.OrdinalIgnoreCase))
            .Distinct(StringComparer.OrdinalIgnoreCase)
            .ToList();

        properties[ApmResourceProperties.CompIds] = string.Join(";", selectedComponentsList);
        properties[ApmResourceProperties.AllowThres] = allowThresholds;

        AssignedToResourceInfo.Update(this.NetObject.NetObjectID, this.Resource, new[] { new DictionaryEntry(AssignedToResourceInfo.CmpIDs, properties[ApmResourceProperties.CompIds]) });
    }

    public override Dictionary<string, object> Properties
    {
        get { return new Dictionary<string, object>(); }
    }
}
