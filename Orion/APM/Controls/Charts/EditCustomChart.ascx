﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomChart.ascx.cs" Inherits="Orion_APM_Controls_Charts_EditCustomChart" %>
<%@ Register TagPrefix="apm" TagName="Assigners" Src="~/Orion/APM/Controls/AssignComponentToResource.ascx" %>
<%@ Register TagPrefix="apm" TagName="DataSource" Src="~/Orion/APM/Controls/DataSourceTypeSupport.ascx" %>

<p>
    <apm:DataSource ID="DataSource" runat="server" />
    <apm:Assigners ID="ctrAssigner" runat="server" />
    <div class="sw-res-editor-row" runat="server" ID="DynamicStatisticType">
            <%= selectStatisticData%>
            <asp:DropDownList runat="server" ID="DynamicStatisticTypeDropDown">
            </asp:DropDownList>
    </div>
</p>