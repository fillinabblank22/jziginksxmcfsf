﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvailabilityChartLegend.ascx.cs" Inherits="Orion_APM_Controls_Charts_AvailabilityChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.apm_availabilityChartLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
        SW.APM.Charts.AvailabilityChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };


</script>

<table runat="server" id="legend" class="chartLegend sw-APM-availability-legend"></table>
<div style="float:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
