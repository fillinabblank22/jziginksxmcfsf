﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMultiChart.ascx.cs" Inherits="Orion_APM_Controls_Charts_EditMultiChart" %>
<%@ Register TagPrefix="apm" TagName="Assigner" Src="~/Orion/APM/Controls/AssignComponentsToResource.ascx" %>

<apm:Assigner ID="ComponentsAssigner" runat="server" />   
<br /><br />
<div class="sw-res-editor-row" runat="server">
    <input type="checkbox" runat="server" ID="ThresholdCheckbox"/><span><%= thresholdCheckboxTitle%></span>
</div>