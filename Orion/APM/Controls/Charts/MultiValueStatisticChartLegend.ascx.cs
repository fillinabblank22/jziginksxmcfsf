﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_APM_Controls_Charts_MultiValueStatisticChartLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "apm_multiValueStatisticChartLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.MultiValueStatisticChartLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "ChartsStyles.css", OrionInclude.Section.Middle);
    }
}