﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MinMaxAvgChartLegend.ascx.cs" Inherits="Orion_APM_Controls_Charts_MinMaxAvgChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.apm_minMaxAvgLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.APM.Charts.ChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
        
        
</script>

<table runat="server" id="legend" class="chartLegend APM_MinMaxAvgChartLegend"></table>
<div style="float:right;">
    <img src="/orion/images/SolarWinds.Logo.Footer.gif"/>
</div>
