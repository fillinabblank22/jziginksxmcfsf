﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMinMaxAvgChart.ascx.cs" Inherits="Orion_APM_Controls_Charts_EditMinMaxAvgChart" %>
<%@ Register TagPrefix="apm" TagName="Assigner" Src="~/Orion/APM/Controls/AssignComponentToResource.ascx" %>

<p>
    <apm:Assigner ID="ctrAssigner" runat="server" />
    <div class="sw-res-editor-row" runat="server" ID="DynamicStatisticType">
            <%= selectStatisticData%>
            <asp:DropDownList runat="server" ID="DynamicStatisticTypeDropDown">
            </asp:DropDownList>
    </div>
    <div class="sw-res-editor-row" runat="server">
        <input type="checkbox" runat="server" ID="ThresholdCheckbox"/><span><%= thresholdCheckboxTitle%></span>
    </div>
</p>