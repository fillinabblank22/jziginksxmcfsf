﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_APM_Controls_Charts_MinMaxAvgChartLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "apm_minMaxAvgLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}