﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_Charts_EditMinMaxAvgChart : BaseResourceEditControl, IChartEditorSettings
{
//    private static readonly Log _log = new Log();

    protected const string selectStatisticData = "Select Statistics:";
    protected string thresholdCheckboxTitle = "Show Thresholds";

    private ResourceInfo Resource
    {
        get
        {
            if (base.Resource == null && !string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
            {
                base.Resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));
            }
            return base.Resource;
        }
    }

    private BaseResourceControl resourceControl;
    private BaseResourceControl ResourceControl
    {
        get
        {
            if(resourceControl == null)
            {
                resourceControl = (BaseResourceControl)LoadControl(Resource.File);
                resourceControl.Resource = Resource;
            }
            return resourceControl;
        }
    }

    private NetObjectManager netObjectManager;
    private NetObjectManager NetObjectManager
    {
        get { return this.netObjectManager ?? (this.netObjectManager = new NetObjectManager(this.ResourceControl)); }
    }

    private string NetObjectID
    {
        get
        {
            if (base.NetObjectID == null)
            {
                var netObject = NetObjectManager.TryGetNetObject();
                if (netObject != null)
                {
                    base.NetObjectID = netObject.NetObjectID;
                }
            }
            return base.NetObjectID;
        }
    }

    private Component component;
    private Component Cmp
    {
        get
        {
            if (component == null)
            {
                long id = 0;

                if (NetObjectHelper.IsApplication(NetObjectID))
                {
                    if (ctrAssigner.SelectedComponent != null)
                    {
                        Int64.TryParse(ctrAssigner.SelectedComponent.Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out id);
                    }
                }
                else
                {
                    id = (Int32)NetObjectHelper.GetId(NetObjectID);
                }

                if (id > 0)
                {
                    using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                    {
                        component = bl.GetComponent(id);
                    }
                }
            }
            return component;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ctrAssigner.OnSelectedNameChanged += new EventHandler(CtrAssigner_OnSelectedNameChanged);
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        DynamicStatisticType.Visible = false;

        ctrAssigner.PopulateControl(resourceInfo);

        PopulateDynamicStatisticTypeDropDown();

        var info = default(AssignedToResourceInfo);
        AssignedToResourceInfo.TryCreate(Resource.Properties[ApmMonitor.PropAssignedToResourceInfo], NetObjectID, out info);

        if (info[AssignedToResourceInfo.ColumnNames] != null && Cmp != null && Cmp.DynamicColumnSettings.Contains(item => item.Name.Equals(info[AssignedToResourceInfo.ColumnNames])))
        {
            HtmlHelper.SetListSelectedValue(DynamicStatisticTypeDropDown, info[AssignedToResourceInfo.ColumnNames].ToString(), null);
        }
        else if (resourceInfo.Properties.ContainsKey("StatisticName"))
        {
            HtmlHelper.SetListSelectedValue(DynamicStatisticTypeDropDown, resourceInfo.Properties["StatisticName"], null);
        }

        ThresholdCheckbox.Checked = !resourceInfo.Properties.ContainsKey("allowthresholds") || string.Compare((string)resourceInfo.Properties["AllowThresholds"], "True", true, CultureInfo.InvariantCulture) == 0;
    }

    private void PopulateDynamicStatisticTypeDropDown()
    {
        if (Cmp != null && Cmp.IsDynamicBased)
        {
            DynamicStatisticTypeDropDown.Items.Clear();

            foreach (var columnSetting in Cmp.DynamicColumnSettings)
            {
                if (columnSetting.Type == DynamicEvidenceColumnDataType.Numeric)
                {
                    ListItem item = new ListItem(columnSetting.Label, columnSetting.Name);

                    DynamicStatisticTypeDropDown.Items.Add(item);
                }
            }

            DynamicStatisticType.Visible = true;
        }
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        if (Cmp != null)
        {
            string name = string.Empty;
            string id = string.Empty;

            if (ctrAssigner.SelectedComponent != null)
            {
                name = ctrAssigner.SelectedComponent.Text;
                id = ctrAssigner.SelectedComponent.Value;
            }

            if (NetObjectHelper.IsApplication(NetObjectID))
            {
                properties[ApmMonitor.PropComponentName] = name;
            }
            else
            {
                using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                {
                    var cmp = bl.GetComponent(NetObjectHelper.GetId(NetObjectID));

                    properties[ApmMonitor.PropComponentName] = cmp.Name;
                    id = cmp.Id.ToString(CultureInfo.InvariantCulture);
                }
            }

            AssignedToResourceInfo.Update(NetObjectID, Resource, new[] { new DictionaryEntry(AssignedToResourceInfo.CmpID, id) });

            if (Cmp.IsDynamicBased)
            {
                properties["StatisticName"] = DynamicStatisticTypeDropDown.SelectedValue;
                AssignedToResourceInfo.Update(NetObjectID, Resource, new[] { new DictionaryEntry(AssignedToResourceInfo.ColumnNames, DynamicStatisticTypeDropDown.SelectedValue) });
            }

            properties["AllowThresholds"] = ThresholdCheckbox.Checked ? "True" : "False";
        }
    }

    protected void CtrAssigner_OnSelectedNameChanged(object sender, EventArgs e)
    {
        component = null;
        DynamicStatisticType.Visible = false;

        if(Cmp.IsDynamicBased)
        {
            PopulateDynamicStatisticTypeDropDown();
            DynamicStatisticType.Visible = true;
        }
    }

    public override Dictionary<string, object> Properties
    {
        get { return new Dictionary<string, object>(); }
    }
}