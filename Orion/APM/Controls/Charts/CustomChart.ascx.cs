﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_APM_Controls_Charts_CustomChart : UserControl
{
    #region Properties

    private Dictionary<string, object> customChartSettings;

    private string customLegendInitializer;

    public bool LoadOnDemand { get; set; }

    public string Height { get; set; }

    public string Width { get; set; }

    public string DataUrl { get; set; }

    public Dictionary<string, object> CustomChartSettings
    {
        get { return this.customChartSettings ?? (this.customChartSettings = new Dictionary<string, object>()); }
        set { this.customChartSettings = value; }
    }

    public string CustomChartOptions { get; set; }

    public bool UseCoreLegend { get; set; }

    public bool IsCustomLegendInitializer
    {
        get { return this.customLegendInitializer != null; }
    }

    public string CustomLegendInitializer
    {
        get { return this.customLegendInitializer ?? "apm_minMaxAvgLegendInitializer__{0}"; }
        set { this.customLegendInitializer = value; }
    }

    protected string LegendInitializerTemplate
    {
        get { return this.UseCoreLegend ? "core_standardLegendInitializer__{0}" : this.CustomLegendInitializer; }
    }

    public string LegendTitle { get; set; }

    public Control LegendTable { get { return this.tblLegend; } }

    #endregion

    #region Protected methods

    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("Charts/Charts.css");
        OrionInclude.CoreFile("Charts/js/AllCharts.js");

        OrionInclude.ModuleFile("APM", "APM.js");

        if (!this.UseCoreLegend)
        {
            OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ChartLegend.js", OrionInclude.Section.Middle);
        }

        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);

        // SAM formatters are not used currently
        //OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);

        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.CustomChart.js");

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(this.Height))
            {
                this.pnlCustomChart.Attributes.CssStyle.Add("height", this.Height.Trim());
            }

            if (!string.IsNullOrEmpty(this.Width))
            {
                this.pnlCustomChart.Attributes.CssStyle.Add("width", this.Width.Trim());
            }

            if (string.IsNullOrEmpty(this.LegendTitle))
            {
                this.spanChartLegendTitle.InnerText = "no title";
                this.spanChartLegendTitle.Attributes.Add("style", "visibility:hidden;");
            }
            else
            {
                this.spanChartLegendTitle.InnerText = this.LegendTitle;
            }
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.UniqueID + "1", this.GetChartInitializerScript(), true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.UniqueID + "2", this.GetChartLegendScript(), true);

        base.OnPreRender(e);
    }

    #endregion

    #region Private methods

    private string GetChartInitializerScript()
    {
        const string Template = @"
(function () {{
    'use strict';

    var settings, options, SW = APMjs.assertGlobal('SW'), BLD = APMjs.tryGlobal('SW.APM.BaselineDetails');

    settings = {1};
    options = {2};
    

    function getChartSettings(parameters) {{
        return SW.APM.Charts.CustomChart.transformSettings('{0}', parameters, settings);
    }}

    function getChartOptions(parameters) {{
        return SW.APM.Charts.CustomChart.transformOptions('{0}', parameters, options);
    }}

    function load(parameters) {{
        function refresh() {{
            SW.Core.Charts.initializeStandardChart(getChartSettings(parameters), getChartOptions(parameters));
        }}

        if (SW && SW.Core && SW.Core.View && SW.Core.View.AddOnRefresh) {{
            SW.Core.View.AddOnRefresh(refresh, '{3}');
        }}

        refresh();
    }}

    if (BLD) {{
        BLD.registerLoadFn('{0}', load);
    }}

    {4}
}}());
";

        var js = string.Format(
            Template,
            this.ID,
            this.GetChartSettings(),
            this.GetChartOptions(),
            this.divChart.ClientID,
            this.LoadOnDemand ? string.Empty : "load();"
            );

        return js;
    }

    private string GetInitializer()
    {
        return InvariantString.Format(this.LegendInitializerTemplate, tblLegend.ClientID);
    }

    private string GetChartLegendScript()
    {
        if (!this.UseCoreLegend && this.IsCustomLegendInitializer)
        {
            return string.Empty;
        }

        const string Template = @"
(function () {{
    SW.Core.Charts.Legend['{0}'] = function (chart, dataUrlParameters) {{
        $('#{1}').empty();
        {2}
    }};
}}());
";

        var initializer = this.GetInitializer();

        var initcall = this.UseCoreLegend
            ? InvariantString.Format("SW.Core.Charts.Legend.createStandardLegend(chart, dataUrlParameters, '{0}', false);", tblLegend.ClientID)
            : (this.IsCustomLegendInitializer 
                ? string.Empty
                : InvariantString.Format("SW.APM.Charts.ChartLegend.createStandardLegend(chart, '{0}');", tblLegend.ClientID));

        var js = InvariantString.Format(Template, initializer, tblLegend.ClientID, initcall);

        return js;
    }

    private string GetChartOptions()
    {
        return string.IsNullOrWhiteSpace(this.CustomChartOptions) ? "{}" : this.CustomChartOptions;
    }

    private string GetChartSettings()
    {
        var details = this.CustomChartSettings;

        details["renderTo"] = this.divChart.ClientID;
        details["legendInitializer"] = this.GetInitializer();

        details.SetIfNoKey("showTitle", false);
        details.SetIfNoKey("title", string.Empty);
        details.SetIfNoKey("subtitle", string.Empty);
        details.SetIfNoKey("loadingMode", LoadingModeEnum.StandardLoading);
        details.SetIfNoKey("sampleSizeInMinutes", 30);
        details.SetIfNoKey("netObjectIds", new string[0]);
        details.SetIfNoKey("ResourceProperties", new Dictionary<string, object>());

        var text = new JavaScriptSerializer().Serialize(details);
        return string.IsNullOrWhiteSpace(text) ? "{}" : text;
    }

    #endregion
}
