﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMultiStatisticChart.ascx.cs" Inherits="Orion_APM_Controls_Charts_EditMultiStatisticChart" %>
<%@ Register TagPrefix="apm" TagName="MultiValueSelection" Src="~/Orion/APM/Controls/ComponentMultiValueSelection.ascx" %>
<%@ Register TagPrefix="apm" TagName="Assigner" Src="~/Orion/APM/Controls/AssignComponentToResource.ascx" %>
<p>
	<apm:Assigner ID="ctrAssigner" runat="server" />
   <apm:MultiValueSelection runat="server" ID="MultiValueSelection" />
</p>
