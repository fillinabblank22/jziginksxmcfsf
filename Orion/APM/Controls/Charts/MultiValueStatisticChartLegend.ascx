﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiValueStatisticChartLegend.ascx.cs" Inherits="Orion_APM_Controls_Charts_MultiValueStatisticChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.apm_multiValueStatisticChartLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.APM.Charts.MultiValueStatisticChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };


</script>

<table runat="server" ID="legend" class="DataGrid">
    <tr>
        <td class="ReportHeader APM_ReportHeader" colspan="2"><%= Resources.APMWebContent.MultiValueStatisticChartLegend_StatisticLabel%></td>
        <td class="ReportHeader APM_ReportHeader"><%= Resources.APMWebContent.MultiValueStatisticChartLegend_ValueLabel%></td>
        <td class="ReportHeader APM_ReportHeader"><%= Resources.APMWebContent.MultiValueStatisticChartLegend_MessageLabel%></td>
    </tr>

</table>

<div class="chartLegendLogo" style="float:right;">
    <img src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
