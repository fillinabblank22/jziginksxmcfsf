﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationHealthOverviewLegend.ascx.cs" Inherits="Orion_APM_Controls_Charts_ApplicationHealthOverviewLegend" %>
<script type="text/C#" runat="server">
    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("/Charts/js/allcharts.js").AddJsInit(
            string.Format(@"SW.Core.Charts.Legend.apm_OverviewLegendInitializer__{0} = function (chart) {{ SW.APM.Charts.ApplicationHealthOverviewLegend.createStandardLegend(chart, '{0}'); }};", legend.ClientID));
    }
</script>
<div id="ApplicationHealthLegend" class="apm_HealthLegend" runat="server">
    <div class="apm_HealthLegendInner">
        <table runat="server" id="legend" class="chartLegend apm_HealthStatus" ></table>
    </div>
</div>