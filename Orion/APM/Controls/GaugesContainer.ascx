﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GaugesContainer.ascx.cs" Inherits="Orion_APM_Controls_GaugesContainer" %>

<orion:Include runat="server" Module="APM" File="GaugesLayout.js" Section="Top"/>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {
		var containerId = '<%=GaugeControlId%>';
		var gaugeWidth = <%=GaugeWidth%>;
		SW.APM.GaugesLayout.startCheckResize(containerId, gaugeWidth);
    });
    //]]>
</script>

<div id="<%= GaugeControlId %>" name="GaugesContainer">
	<asp:Repeater ID="ctrCmpRep" OnItemDataBound="OnCmpRep_ItemDataBound" runat="server">
		<ItemTemplate>
			<div id="ctrContainer" style="float:left; font-weight:bold; text-align:center;" runat="server">
				<div id="ctrHeader" style="text-align:center; overflow:inherit;" runat="server">
					<%#Eval("Name")%>
				</div>
				<asp:HyperLink ID="ctrHref" Target="_blank" runat="server">
					<asp:PlaceHolder ID="ctrGauge" runat="server"/>
				</asp:HyperLink>
			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>