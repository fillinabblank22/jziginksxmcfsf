﻿using System;
using System.ComponentModel;

using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web;

using Pair = System.Collections.Generic.KeyValuePair<System.String, System.String>;

public partial class Orion_APM_Controls_BreadcrumbBar : System.Web.UI.UserControl
{
	#region Properties

	public string Provider
	{
		get { return ctrSiteMapPath.Provider; }
		set { ctrSiteMapPath.Provider = value; }
	}

    public string SiteMapFilePath
    {
        get { return ctrSiteMapPath.SiteMapFilePath; }
        set { ctrSiteMapPath.SiteMapFilePath = value; }
    }

	#endregion 

	#region Event Handlers

	protected void OnSiteMapPath_OnInit(Object sender, EventArgs e)
	{
		this.Visible = !CommonWebHelper.IsBreadcrumbsDisabled || ctrSiteMapPath.Provider.IsValid();

		if (this.Visible)
		{
            ISiteMapRenderer renderer;

            switch (this.Provider)
            {
                case "SqlBlackBoxSitemapProvider": 
                    renderer = new SolarWinds.APM.BlackBox.Sql.Web.UI.SqlBlackBoxSiteMapRenderer();
                    break;
                case "ExchangeSiteMapProvider":
                    renderer = new SolarWinds.APM.BlackBox.Exchg.Web.UI.ExchangeSiteMapRenderer();
                    break;
                case "IisSiteMapProvider":
                    renderer = new SolarWinds.APM.BlackBox.IIS.Web.UI.IisSiteMapRenderer();
                    break;
                case "ActiveDirectorySiteMapProvider":
                    renderer = new SolarWinds.APM.BlackBox.ActiveDirectory.Web.UI.ActiveDirectorySiteMapRenderer();
                    break;
                default:
                    renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
                    break;
            }

			renderer.SetUpData(
				new Pair("ViewID", Request.QueryStringOrForm("ViewID").IsValid() ? Request.QueryStringOrForm("ViewID") : null),
                new Pair("NetObject", Request.QueryStringOrForm("NetObject").IsValid() ? Request.QueryStringOrForm("NetObject") : null),
				new Pair("ReturnTo", Request.QueryStringOrForm("ReturnTo").IsValid() ? Request.QueryStringOrForm("ReturnTo") : null),
				new Pair("AccountID", Request.QueryStringOrForm("AccountID").IsValid() ? Request.QueryStringOrForm("AccountID") : null));

			ctrSiteMapPath.SetUpRenderer(renderer);
		}
	}

	#endregion
}
