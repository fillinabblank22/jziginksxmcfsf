﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_APM_Controls_ApmCustomQueryTable : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public int UniqueClientID { get; set; }

    public string SWQL { get; set; }

    public string SearchSWQL { get; set; }

    public override string ClientID
    {
        get { return "Grid-" + UniqueClientID; }
    }
}