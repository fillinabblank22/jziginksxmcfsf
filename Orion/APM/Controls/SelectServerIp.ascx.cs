﻿using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_APM_Controls_SelectServerIp : UserControl
{
    private Node selectedNode = null;

    public bool ValidateVmware { get; set; }

    public Node SelectedNode
    {
        get
        {
            if (this.targetServer.Text.Equals(this.selectedNodeAddress.Value, StringComparison.OrdinalIgnoreCase))
            {
                int id;
                int.TryParse(this.selectedNodeId.Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out id);

                if (this.selectedNode == null || (id > 0 && this.selectedNode.Id != id))
                {
                    this.RestoreSelectedNodeById(id);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(targetServer.Text))
                {
                    ChangeSelectedNode(null);
                }
                else
                {
                    try
                    {
                        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                        {
                            ChangeSelectedNode(bl.GetNodeByIPAddress(targetServer.Text));
                        }
                    }
                    catch (ApmBusinessLayerException)
                    {
                        this.selectedNode = null;
                        this.selectedNodeId.Value = "0";
                        // keep address entered by user
                    }
                }
            }
            return this.selectedNode;
        }
        set { this.ChangeSelectedNode(value); }
    }

    private void ChangeSelectedNode(Node value)
    {
        this.selectedNode = value;
        if (value == null)
        {
            this.targetServer.Text = string.Empty;
            this.selectedNodeAddress.Value = string.Empty;
            this.selectedNodeId.Value = "0";
        }
        else
        {
            this.targetServer.Text = value.IpAddress;
            this.selectedNodeAddress.Value = value.IpAddress;
            this.selectedNodeId.Value = value.Id.ToString(CultureInfo.InvariantCulture);
        }
    }

    public void RestoreSelectedNodeById(int nodeId)
    {
        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            ChangeSelectedNode(bl.GetNode(nodeId));
        }
    }

    public string NodeFilter
    {
        set
        {
            SelectNodeTree.ViewNodeFilter = value;
        }
    }

    public virtual string CssClass { get; set; }

    public event EventHandler<VmwareValidatingEventArgs> VmwareValidating;

    protected void Page_Load(object sender, EventArgs e)
    {
        targetServer.CssClass = CssClass;
    }

    protected void ValidateTargetServer(Object source, ServerValidateEventArgs args)
    {
        var node = this.SelectedNode;
        args.IsValid = node != null
                       && node.IpAddress.Equals(this.targetServer.Text, StringComparison.OrdinalIgnoreCase);
    }

    protected void ValidateVmwareEntity(Object source, ServerValidateEventArgs args)
    {
        if (!ValidateVmware)
        {
            args.IsValid = true;
            return;
        }

        string type = this.GetMachineType();

        if (type == null)
        {
            // If the type is null then the node doesn't exist and we don't want to display 
            // the VMware error message. This should be handled by another validator.
            args.IsValid = true;
        }
        else
        {
            args.IsValid =
                type.Equals(AppFinderCurrentSettings.VMWARE_VCENTER_MACHINE_TYPE, StringComparison.InvariantCultureIgnoreCase) ||
                type.Equals(AppFinderCurrentSettings.VMWARE_ESX_MACHINE_TYPE, StringComparison.InvariantCultureIgnoreCase);

            this.OnVmwareValidating(type);
        }
    }

    protected virtual void OnVmwareValidating(string type)
    {
        if (this.VmwareValidating != null)
            this.VmwareValidating(this, new VmwareValidatingEventArgs(type));
    }

    public string GetMachineType()
    {
        var node = this.SelectedNode;
        if (node == null)
            return null;

        var data = SwisDAL.GetNodesByProperty("NodeId", node.Id.ToString(CultureInfo.InvariantCulture));
        if (data != null && data.Rows.Count > 0)
        {
            return (data.Rows[0]["MachineType"] ?? String.Empty).ToString().Trim();
        }
        return null;
    }
}

public class VmwareValidatingEventArgs : EventArgs
{
    public string MachineType { get; private set; }

    public VmwareValidatingEventArgs(string machineType)
    {
        this.MachineType = machineType;
    }
}