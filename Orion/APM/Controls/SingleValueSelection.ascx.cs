﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using System.Collections;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_Controls_SingleValueSelection : System.Web.UI.UserControl
{
    private static readonly Log log = new Log();
    private bool? optionEnabled = null;
	private string componentName = string.Empty;
	private long componentId;

    bool OptionEnabled
    {
        get 
        { 
            if ( optionEnabled == null )
            {
                optionEnabled = false;

                if (Request.QueryStringOrForm(ModuleConstants.QueryStringParameters.MinMaxAvgMultiValueSelection) != null)
                {
                    var dal = new ComponentDal();
                    DataTable table = null;

                    int applicationId;

					if (componentId == 0) 
					{
						long.TryParse(Request.QueryStringOrForm(ModuleConstants.QueryStringParameters.ComponentId), NumberStyles.Integer, CultureInfo.InvariantCulture, out componentId);
					}
					if (componentId > 0)
                    {
                        table = dal.GetComponentByID(componentId);
                    }
                    else if (int.TryParse(Request.QueryStringOrForm(ModuleConstants.QueryStringParameters.ApplicationId), NumberStyles.Integer, CultureInfo.InvariantCulture, out applicationId))
                    {
						table = dal.GetComponentByApplicationID(applicationId, componentName);
                    }
                    else
                    {
                        log.Warn("ComponentId or ApplicationId not present in query string or invalid");
                    }

					if (table != null)
                    {
                        string[] componentTypes = new[]{ ((int)ComponentType.WindowsPowerShell).ToString(), ((int)ComponentType.LinuxScript).ToString(), 
                                                         ((int)ComponentType.WindowsScript).ToString(), ((int)ComponentType.NagiosScript).ToString() };

                        string expresion = "ComponentType in (" + string.Join(", ", componentTypes) + ")";

                        DataRow[] foundedRows = table.Select(expresion);

                        if (foundedRows != null && foundedRows.Length > 0)
                        {
                            optionEnabled = true;
                        }
                    }
                }
            }
            return optionEnabled.Value; 
        }
    }

	public void LoadData(ListItem selectedComponent)
	{
		if (selectedComponent != null) 
		{
			componentName = selectedComponent.Text;
			componentId = long.Parse(selectedComponent.Value);
		}
		optionEnabled = null;
		if (OptionEnabled)
		{
			DataTable table = new DynamicEvidenceDal().GetDynamicEvidenceColumns(componentId, DynamicEvidenceColumnDataType.Numeric);
			if (table != null)
			{
				itemSelection.DataSource = table;
				itemSelection.DataBind();
			}
		}
	}

    public void InitializeSettings(ResourceInfo resource)
    {
        // Visibility is set here because during page_Load is selectedComponentName still empty
        SetVisibility();

        if (OptionEnabled)
        {
			var info = default(AssignedToResourceInfo);
			AssignedToResourceInfo.TryCreate(Request, resource, out info);

			string valueToDisplay = null;
			if (info[AssignedToResourceInfo.ColumnNames] == null)
			{
				valueToDisplay = resource.Properties[ResourceSettings.ValuesToDisplay];
			}
			else
			{
				valueToDisplay = info[AssignedToResourceInfo.ColumnNames].ToString();
			}

            bool isNotSelected = true;

            if (String.IsNullOrEmpty(valueToDisplay) || itemSelection.Items.FindByValue(valueToDisplay) == null)
            {
                isNotSelected = false;
            }

			if (itemSelection.Items.Count > 0) 
			{
				if (isNotSelected)
				{
					itemSelection.Items.FindByValue(valueToDisplay).Selected = true;
				}
				else
				{
					itemSelection.Items[0].Selected = true;
				}
			}
        }
    }

    public void UpdateSettings(ResourceInfo resource)
    {
		if (Visible) 
		{
			var valueToDisplay = itemSelection.SelectedValue;
			resource.Properties[ResourceSettings.ValueToDisplay] = valueToDisplay;
			AssignedToResourceInfo.Update(Request, resource, new[] { new DictionaryEntry(AssignedToResourceInfo.ColumnNames, valueToDisplay) });
		}
    }

	public void SetVisibility()
	{
		this.Visible = this.OptionEnabled;
	}
}