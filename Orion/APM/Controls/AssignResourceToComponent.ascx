﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignResourceToComponent.ascx.cs" Inherits="Orion_APM_Controls_AssignResourceToComponent" %>

<b><%= Resources.APMWebContent.APMWEBCODE_VB1_130 %></b><br />
<asp:DropDownList ID="ctrComponents" DataTextField="Name" DataValueField="Id" AutoPostBack="true" OnSelectedIndexChanged="CtrlComponents_OnSelectedIndexChanged" runat="server"/>
