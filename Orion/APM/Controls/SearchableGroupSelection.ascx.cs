using System;
using System.Collections.Generic;

using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Controls_SearchableGroupSelection : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("Orion", "Admin/Containers/Containers.css");

        if (!IsPostBack)
        {
            AddRemoveObjectsControl.EntityName = "Orion.Groups";
        }
        
        AddRemoveObjectsControl.LeftPanelTitle = String.Format(Resources.APMWebContent.APMWEBCODE_AK1_24, Resources.APMWebContent.APMWEBDATA_AK1_110);
        AddRemoveObjectsControl.RightPanelTitle = String.Format(Resources.APMWebContent.APMWEBCODE_AK1_25, Resources.APMWebContent.APMWEBDATA_AK1_110);
    }

    public void StoreSelectedGroups(List<SelectNodeTreeItem> groups)
    {
        if (groups == null || groups.Count == 0)
        {
            AddRemoveObjectsControl.SelectedItems = String.Empty;
        }
        else
        {
            AddRemoveObjectsControl.SelectedItems = JsHelper.Serialize(groups.ToArray());
        }
    }

    public List<SelectNodeTreeItem> GetSelectedGroups()
    {
        var groups = new List<SelectNodeTreeItem>();

        if (String.IsNullOrEmpty(AddRemoveObjectsControl.SelectedItems))
        {
            return groups;
        }

        var selectedItems = JsHelper.Deserialize<SelectNodeTreeItem[]>(AddRemoveObjectsControl.SelectedItems);

        groups.AddRange(selectedItems);

        return groups;
    }
}