﻿using System;
using System.ComponentModel;
using System.Web.UI;

public partial class Orion_APM_Controls_TestButtonSummary : System.Web.UI.UserControl
{
    [
    CssClassProperty,
    Category("Appearance"),
    DefaultValue("")
    ]
    public string CssClass { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

}