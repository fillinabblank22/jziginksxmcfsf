﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;

public partial class Orion_APM_Controls_FilterExamples : System.Web.UI.UserControl
{
    private DataSet tableColumns = new DataSet();
    private bool showApplicationProperties = true;
    private bool showComponentProperties = true;

    public bool ShowApplicationProperties
    {
        get { return showApplicationProperties; }
        set { showApplicationProperties = value; }
    }

    public bool ShowComponentProperties
    {
        get { return showComponentProperties; }
        set { showComponentProperties = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IList<string> tableNames = new List<string>();
            tableNames.Add("Nodes");
            tableNames.Add("APM_Application");
            tableNames.Add("APM_Component");
            tableNames.Add("APM_CurrentApplicationStatus");
            tableNames.Add("APM_CurrentComponentStatus");

            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                tableColumns = businessLayer.GetTopXXFilterProperties(tableNames);
            }
        }
    }

    protected void NodeGridLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            nodeGrid.DataSource = getColumnNames(tableColumns.Tables["Nodes"]);
            nodeGrid.DataBind();
        }
    }

    protected void ApplicationGridLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            applicationGrid.DataSource = getColumnNames(tableColumns.Tables["APM_Application"]);
            applicationGrid.DataBind();
        }
    }


    protected void MonitorGridLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            monitorGrid.DataSource = getColumnNames(tableColumns.Tables["APM_Component"]);
            monitorGrid.DataBind();
        }
    }

    protected void MonitorStatusGridLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            monitorStatusGrid.DataSource = getColumnNames(tableColumns.Tables["APM_CurrentComponentStatus"]);
            monitorStatusGrid.DataBind();
        }
    }

    protected void ApplicationStatusGridLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            applicationStatusGrid.DataSource = getColumnNames(tableColumns.Tables["APM_CurrentApplicationStatus"]);
            applicationStatusGrid.DataBind();
        }
    }

    private static DataTable getColumnNames(DataTable table)
    {
        DataTable result = new DataTable();
        result.Columns.Add("Name");
        result.Columns.Add("DataType");
        foreach (DataColumn dc in table.Columns)
        {
            DataRow row = result.NewRow();
            object[] rowArray = new object[2];
            rowArray[0] = dc.ColumnName;
            rowArray[1] = dc.DataType.ToString().Remove(0, 7);
            row.ItemArray = rowArray;
            result.Rows.Add(row);
        }
        return result;
    }
}
