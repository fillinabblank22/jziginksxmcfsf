<%@ Control Language="C#" ClassName="ApmStatusIcon" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>

<script runat="server">
    public ApmStatus StatusValue
    {
        get
        {
            if (null == ViewState["StatusValue"])
            {
                return new ApmStatus();
            }
            return (ApmStatus)ViewState["StatusValue"];
        }
        set { ViewState["StatusValue"] = value; }
    }

    public ApmMonitorError ErrorValue
    {
        get
        {
            if (ViewState["ErrorValue"] == null)
                return new ApmMonitorError();
            
            return (ApmMonitorError)ViewState["ErrorValue"];
        }
        set { ViewState["ErrorValue"] = value; }
    }

    protected string ImagePath
    {
        get { return StatusValue.ToString("imgpath", CustomApplicationType); }
    }

    public string CustomApplicationType
    {
        get
        {
            if (null == ViewState["CustomApplicationType"])
            {
                return null;
            }

            return (string)ViewState["CustomApplicationType"];
        }

        set
        {
            ViewState["CustomApplicationType"] = value;
        }
    }
    
    protected string ErrorDescription
    {
        get { return ErrorValue.ToString(ApmMonitorError.formatErrorCode, null); }
    }
</script>
<img class="apm_StatusIcon"  src="<%= ImagePath %>" alt="<%= ErrorDescription %>" title="<%= ErrorDescription %>"/>
