﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataSourceTypeSupport.ascx.cs" Inherits="Orion_APM_Controls_DataSourceTypeSupport" %>
<p id="DataSourceTypeEditPanel" runat="server">
	<b><%= Resources.APMWebContent.APMWEBDATA_VB1_332 %></b><br />
	<asp:DropDownList ID="DataSourceTypeList" DataTextField="DisplayName" DataValueField="Name" AutoPostBack="true" OnSelectedIndexChanged="CtrlComponents_OnSelectedDataSourceTypeChanged" Width="230px" runat="server"/>
</p>
