﻿using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Controls_BaselineDetails : System.Web.UI.UserControl
{
    private int? baselineCollectionDuration;
    private List<TimeFrame> timeFrames;

    protected List<KeyValuePair<ThresholdOperator, string>> Options { get; set; }

    #region Protected methods

    protected override void OnInit(EventArgs e)
    {
        if (this.Options == null)
        {
            this.Options = Threshold.GetOperators();
        }

        OrionInclude.ModuleFile("APM", "APM.js");
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.CustomChart.js");
        OrionInclude.ModuleFile("APM", "BaselineDetails.js");

        base.OnInit(e);
    }
   
    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {            
            this.LoadAPMTimeFrames();
            this.tftWorkDays.TimeFrame = (timeFrames != null) ? timeFrames.Find(t => t.Name.Equals(this.tftWorkDays.TimeFrameName)) : null;
            this.tftWorkDays.Title = Resources.APMWebContent.APMWEBCODE_PS0_2; // Work Days
            this.tftOffDays.TimeFrame = (timeFrames != null) ? timeFrames.Find(t => t.Name.Equals(this.tftOffDays.TimeFrameName)) : null;
            this.tftOffDays.Title = Resources.APMWebContent.APMWEBCODE_PS0_3; // Evenings and Weekends

            this.LoadBaselineCollectionDuration();
            this.mbDetailsHint.MessageHtml = string.Format(Resources.APMWebContent.APMWEBCODE_PS0_4, (baselineCollectionDuration != null) && (baselineCollectionDuration != -1) ? baselineCollectionDuration.ToString() : " "); // The amount of data to be collected in the baseline is currently set to <span class=""bold"">{0} days</span>.
            this.mbDetailsHint.MessageHtml += string.Format(@"&nbsp;<a href=""/Orion/APM/Admin/Settings.aspx"" target=""_blank"">&gt;&gt; {0}</a>", Resources.APMWebContent.APMWEBCODE_PS0_5); // Change this setting                           

            this.InitAreaChart();
            this.InitMinMaxChart();

            this.ccArea.LegendTitle = Resources.APMWebContent.APMWEBCODE_PS0_6; // Show Statistics in Graph
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.ID + "1", this.GetAreaChartScripts(), true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.ID + "2", this.GetMinMaxChartScripts(), true);

        base.OnPreRender(e);
    }

    #endregion

    #region Private methods

    private void InitAreaChart()
    {
        this.ccArea.CustomChartOptions = GetAreaChartOptions();
        this.ccArea.CustomChartSettings = this.GetAreaChartSettings();
    }

    private void InitMinMaxChart()
    {
        this.ccMinMax.CustomChartOptions = GetMinMaxChartOptions();
        this.ccMinMax.CustomChartSettings = this.GetMinMaxChartSettings();
    }

    private void LoadBaselineCollectionDuration()
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            var config = (Config) businessLayer.GetConfig(new Config());
            baselineCollectionDuration = (int) config.BaselineDataCollectionDuration.TotalDays;
        }
    }

    private void LoadAPMTimeFrames()
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            timeFrames = businessLayer.GetAPMTimeFrames();
        }
    }

    #region Area chart

    private static string GetAreaChartOptions()
    {
        const string TemplateOptions = @"{{
    highchartsChartType: 'chart',
    chart: {{
        width: {0},
        height: {1},
        marginTop: 10,
        events: {{
            load: APMjs.noop
        }}
    }},
    rangeSelector: {{
        enabled: false
    }},
    scrollbar: {{
        enabled: false
    }},
    tooltip: {{
            shared: true,
            useHTML: true,
            headerFormat: '<div id=""highchartTooltip"">A value of {{point.key}} <br />',
            pointFormat: '<span style=""color:{{series.color}}"">{{series.name}}</span>: <b>{{point.y}}</b><br />',
            footerFormat: '</div>'
    }},
    navigator: {{
        enabled: false
    }},  
    plotOptions: {{
        area: {{
        }}
    }},
    xAxis: {{
        events: {{
            afterSetExtremes: APMjs.noop
        }}
    }},
    yAxis: [{{
        title: {{
            margin: 5,
            text: 'Number of Occurrences'
        }},
        unit: 'x',
        minRange: 0
    }}], 
    seriesTemplates: {{
        Days: {{
            name: 'Work Days',
            type : 'area',
            color: '#4F81BD',
            zIndex: 2
        }},
        NightsWeekends: {{
            name: 'Evenings and Weekends',
            type: 'area',
            color: '#B43633',
            zIndex: 3
        }},
        AllHours: {{
            name: 'All hours',
            type: 'area',
            color: '#9BBB59',
            zIndex: 1
        }}
    }}
}}";

        return string.Format(TemplateOptions, 450, 235);
    }

    private Dictionary<string, object> GetAreaChartSettings()
    {
        var details = new Dictionary<string, object>();

        details["dataUrl"] = "/Orion/APM/Services/ChartData.asmx/GetBaselineAreaChartsData";
        details["DaysOfDataToLoad"] = this.baselineCollectionDuration ?? -1;
        details["StatisticColumn"] = string.Empty;
        details["intervalCount"] = 50;
        details["columnSchemaId"] = string.Empty;

        return details;
    }

    private string GetAreaChartScripts()
    {
        const string Template = @"
APMjs.onSysLoadOnce(function () {{
    SW.APM.BaselineDetails.transformCustomChart('{0}');
}});
";

        return InvariantString.Format(Template, this.ccArea.ID);
    }

    #endregion

    #region MinMax chart

    private static string GetMinMaxChartOptions()
    {
        const string TemplateOptions = @"{{
    chart: {{
        width: {0},
        height: {1},
        marginTop: 10,
        events: {{
            load: function() {{ SW.APM.EditApp.redrawPlotBandsInCharts(); }}
        }}
    }},
    rangeSelector: {{
        enabled: false
    }},
    plotOptions: {{
        candlestick: {{
            groupPadding: 0,
            dataGrouping: {{
                dateTimeLabelFormats: {{
                    millisecond: ['%A, %b %e, %Y %l:%M:%S.%L %p', '%A, %b %e, %Y %l:%M%S.%L %p', '-%l:%M%S.%L %p'],
                    second: ['%A, %b %e, %Y %l:%M:%S %p', '%A, %b %e, %Y %l:%M:%S %p', '-%l:%M:%S %p'],
                    minute: ['%A, %b %e, %Y %l:%M %p', '%A, %b %e, %Y %l:%M %p', '-%l:%M %p'],
                    hour: ['%A, %b %e, %Y %l:%M %p', '%A, %b %e, %Y %l:%M %p', '-%l:%M %p'],
                    day: ['%A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
                    week: ['Week from %A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
                    month: ['%B %Y', '%B', '-%B %Y'],
                    year: ['%Y', '%Y', '-%Y']
                }}
            }}
        }},
        line : {{
            dataGrouping: {{
                dateTimeLabelFormats: {{
                    millisecond: ['%A, %b %e, %Y %l:%M:%S.%L %p', '%A, %b %e, %Y %l:%M%S.%L %p', '-%l:%M%S.%L %p'],
                    second: ['%A, %b %e, %Y %l:%M:%S %p', '%A, %b %e, %Y %l:%M:%S %p', '-%l:%M:%S %p'],
                    minute: ['%A, %b %e, %Y %l:%M %p', '%A, %b %e, %Y %l:%M %p', '-%l:%M %p'],
                    hour: ['%A, %b %e, %Y %l:%M %p', '%A, %b %e, %Y %l:%M %p', '-%l:%M %p'],
                    day: ['%A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
                    week: ['Week from %A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
                    month: ['%B %Y', '%B', '-%B %Y'],
                    year: ['%Y', '%Y', '-%Y']
                }}
            }}
        }}
    }},
    xAxis: {{
        events: {{
            afterSetExtremes: function() {{ SW.APM.EditApp.redrawPlotBandsInCharts(); }}
        }}    
    }},
    yAxis: [{{
        title: {{
            margin: 5,
            text: ''
        }},
        unit: '',
        labels: {{
            align: 'right',
            x: -5
        }},
        minRange: 0
    }}],
    seriesTemplates: {{
        Avg: {{
            name: 'Average',
            type: 'line',
            zIndex: 2
        }},
        MinMax: {{
            name: 'Min/Max',
            type: 'candlestick',
            lineWidth: 0,
            zIndex: 1
        }}
    }}
}}";
        return InvariantString.Format(TemplateOptions, 450, 255);
    }

    private Dictionary<string, object> GetMinMaxChartSettings()
    {
        var details = new Dictionary<string, object>();

        details["dataUrl"] = "/Orion/APM/Services/ChartData.asmx/GetBaselineMinMaxChartsData";
        details["DaysOfDataToLoad"] = this.baselineCollectionDuration ?? -1;
        details["StatisticColumn"] = string.Empty;

        return details;
    }

    private string GetMinMaxChartScripts()
    {
        const string Template = @"
APMjs.onSysLoadOnce(function () {{
    SW.APM.BaselineDetails.transformCustomChart('{0}');
}});
";

        return InvariantString.Format(Template, this.ccMinMax.ID);
    }

    #endregion

    #endregion
}
