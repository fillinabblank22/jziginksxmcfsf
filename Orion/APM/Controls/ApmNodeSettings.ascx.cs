﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Core.Common.Swis;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using WinRmNodeSettings = SolarWinds.APM.Common.Models.WinRmNodeSettings;

public partial class Orion_APM_Controls_ApmNodeSettings : UserControl, INodePropertyPlugin
{
    private NodePropertyPluginExecutionMode pluginExecutionMode;
    private IList<Node> _nodes;
    private IReadOnlyCollection<NodeApmSettings> nodesApmSettings;
    private readonly ISwisConnectionProxyFactory swisContextFactory = new SwisConnectionProxyFactory();

    #region INodePropertyPlugin members

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        pluginExecutionMode = mode;
        _nodes = nodes;
        nodesApmSettings = LoadApmSettings(_nodes);
        if (!IsPostBack)
        {
            ApmSettings_WinRMEnabledCheckbox.Enabled = pluginExecutionMode == NodePropertyPluginExecutionMode.EditProperies;
            ApmSettings_WinRMEnabledCheckbox.Checked = nodesApmSettings.Any(setting => !setting.WinRMPollingEnabled.HasValue || setting.WinRMPollingEnabled.Value);

            ApmSettings_WinRMUseHttpsCheckbox.Enabled = pluginExecutionMode == NodePropertyPluginExecutionMode.EditProperies;
            ApmSettings_WinRMUseHttpsCheckbox.Checked = nodesApmSettings.Any(setting => setting.WinRMUseHttps.HasValue && setting.WinRMUseHttps.Value);

            var winRmPort = nodesApmSettings.FirstOrDefault(setting => setting.WinRMPort != null);
            if (winRmPort != null)
            {
                ApmSettings_WinRMPort.Text = winRmPort.WinRMPort.ToString();
            }

            var winRmUrlPrefix = nodesApmSettings.FirstOrDefault(setting => setting.WinRMUriPrefix != null);
            if(winRmUrlPrefix != null)
            {
                ApmSettings_WinRMUrlPrefix.Text = winRmUrlPrefix.WinRMUriPrefix;
            }
        }
    }

    public bool Update()
    {
       switch (pluginExecutionMode)
        {
             case NodePropertyPluginExecutionMode.EditProperies:
                 using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                 {
  
                     foreach (NodeApmSettings nodeApmSetting in nodesApmSettings)
                     {
                             businessLayer.InsertOrUpdateNodeWinRmSetting(nodeApmSetting.NodeId, new WinRmNodeSettings
                             {
                                 IsWinRmDisabled = !ApmSettings_WinRMEnabledCheckbox.Checked,
                                 UrlPrefix  = ApmSettings_WinRMUrlPrefix.Text,
                                 Port  = int.Parse(ApmSettings_WinRMPort.Text),
                                 UseHttps =  ApmSettings_WinRMUseHttpsCheckbox.Checked
                                });
                     }

                     return true;
                 }
             default:
                 return true;
        }
    }

    
    private IReadOnlyCollection<NodeApmSettings> LoadApmSettings(IEnumerable<Node> nodes)
    {
        if (!nodes.Any())
        {
            return Enumerable.Empty<NodeApmSettings>().ToList();
        }

        string nodesIDs = string.Join(",", nodes.Select(n => n.ID));
        using (var proxy = swisContextFactory.CreateConnection())
        {
            var winRmSetting = SolarWinds.APM.Common.Management.WinRm.Constants.NodeWinRmSettingName;

            var nodesWithWinRmPollingEnablePropertyQuery = string.Format(@"SELECT ns_winRmPolling.SettingValue as WinRMPollingEnabled, 
                    n.NodeID, ns_useHttps.SettingValue as WinRMUseHttps, ns_uriPrefix.SettingValue as UrlPrefix , ns_port.SettingValue as WinRmPort
                    FROM Orion.Nodes n
                    LEFT JOIN Orion.NodeSettings ns_winRmPolling ON ns_winRmPolling.SettingName = @WinRMPollingEnabled AND n.NodeID = ns_winRmPolling.NodeID
                    LEFT JOIN Orion.NodeSettings ns_useHttps ON ns_useHttps.SettingName = @WinRMUseHttps AND n.NodeID = ns_useHttps.NodeID
                    LEFT JOIN Orion.NodeSettings ns_uriPrefix ON ns_uriPrefix.SettingName = @WinRMUrlPrefix AND n.NodeID = ns_uriPrefix.NodeID
                    LEFT JOIN Orion.NodeSettings ns_port ON ns_port.SettingName = @WinRMPort AND n.NodeID = ns_port.NodeID
                    WHERE n.NodeID IN({1})",
                winRmSetting, nodesIDs);
            
            var queryResult = proxy.Query(nodesWithWinRmPollingEnablePropertyQuery, new Dictionary<string, object>
            {
                { "WinRMPollingEnabled", WinRmNodeSettings.IsWinRmEnabledPropertyName },
                { "WinRMUseHttps", WinRmNodeSettings.UseHttpsPropertyName },
                { "WinRMUrlPrefix", WinRmNodeSettings.UrlPrefixPropertyName },
                { "WinRMPort", WinRmNodeSettings.PortPropertyName },
            });
            
            var result = new List<NodeApmSettings>();
            foreach (DataRow row in queryResult.Rows)
            {
                result.Add(new NodeApmSettings
                {
                    NodeId = (int)row["NodeID"],
                    WinRMPollingEnabled = (row["WinRMPollingEnabled"] == DBNull.Value) || row["WinRMPollingEnabled"].ToString().ToLowerInvariant() == "true",
                    WinRMUseHttps = (row["WinRMUseHttps"] != DBNull.Value) && row["WinRMUseHttps"].ToString().ToLowerInvariant() == "true",
                    WinRMPort = (row["WinRmPort"] != DBNull.Value) ? int.Parse(row["WinRmPort"].ToString()) : (int?)null,
                    WinRMUriPrefix = (row["UrlPrefix"] != DBNull.Value) ? row["UrlPrefix"].ToString() : null
                });
            }

            return result;
        }
    }

    public bool Validate()
    {
        return true;
    }
    #endregion

    public override void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
        base.Dispose();
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (swisContextFactory != null)
            {
                swisContextFactory.Dispose();
            }
        }
    }

    private class NodeApmSettings
    {
        public int NodeId { get; set; }
        public bool? WinRMPollingEnabled { get; set; }
        public bool? WinRMUseHttps { get; set; }
        public string WinRMUriPrefix { get; set; }
        public int? WinRMPort { get; set; }
    }
}