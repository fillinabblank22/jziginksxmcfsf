﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApmNodeSettings.ascx.cs" Inherits="Orion_APM_Controls_ApmNodeSettings" %>
<style>

    .row {
        padding-top: 15px;
        padding-bottom: 15px;
    }
    .rightColumn {
        padding-left: 20px;
    }
    .helpText {
        padding-left: 20px;
        color: Gray;
        font-size: 8pt;
        padding-bottom: 20px;
    }

</style>

<div id="ApmNodeSettingsContent" runat="server" class="contentBlock">
    <table>
        <tbody>
        <tr>
            <td class="contentBlockModuleHeader">
                <asp:Literal runat="server" Text="<%$Resources: ApmWebContent,WinRMSettingsTitleText %>"></asp:Literal>                    
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <table>
                    <tr class="row"><td><%= Resources.APMWebContent.WinRMEnabledCheckboxLabel %></td>
                        <td class="rightColumn">
                            <asp:CheckBox runat="server" ID="ApmSettings_WinRMEnabledCheckbox" />
                        </td></tr>
                    <tr>
                        <td></td>
                        <td class="helpText">
                            <%= Resources.APMWebContent.WinRMEnabledHelpText %>
                            <label><a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("OrionSAMWinRMPolling") %>" target="_blank"><%= Resources.APMWebContent.ConvertValueEditor_HelpHtml_LearnMore_LinkText%></a></label>
                        </td>
                    </tr>
                    <tr class="row"><td><%= Resources.APMWebContent.WinRMUseHttpsCheckboxLabel %></td><td class="rightColumn"><asp:CheckBox runat="server" ID="ApmSettings_WinRMUseHttpsCheckbox" /></td></tr>
                    <tr>
                        <td></td>
                        <td class="helpText">
                            <%= Resources.APMWebContent.WinRMUseHttpsHelpText %>
                        </td>
                    </tr>
                    <tr class="row"><td><%= Resources.APMWebContent.WinRMUrlPrefixTextBoxLabel %></td><td class="rightColumn"><asp:TextBox runat="server" ID="ApmSettings_WinRMUrlPrefix" Text="wsman"/></td></tr>
                    <tr>
                        <td></td>
                        <td class="helpText">
                            <%= Resources.APMWebContent.WinRMUrlPrefixHelpText %>
                        </td>
                    </tr>
                    <tr class="row"><td><%= Resources.APMWebContent.WinRMPortTextBoxLabel %></td><td class="rightColumn"><asp:TextBox runat="server" ID="ApmSettings_WinRMPort" Text="5985" />
					<asp:RegularExpressionValidator ID="winRmPortValidator" runat="server" ValidationExpression="^([1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$" ControlToValidate="ApmSettings_WinRMPort">(1-65535)</asp:RegularExpressionValidator>
					</td></tr>
                    <tr>
                        <td></td>
                        <td class="helpText">
                            <%= Resources.APMWebContent.WinRMPortHelpText %>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
