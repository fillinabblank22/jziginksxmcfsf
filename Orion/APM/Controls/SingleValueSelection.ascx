﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SingleValueSelection.ascx.cs" Inherits="Orion_APM_Controls_SingleValueSelection" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div>
            <label><b><%= Resources.APMWebContent.APMWEBDATA_VB1_333%></b></label>
            <asp:RadioButtonList runat="server" ID="itemSelection" DataTextField="Label" DataValueField="Name" DataTextFormatString="<%$ Resources : APMWebContent , APMWEBCODE_VB1_145 %>">
            </asp:RadioButtonList>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>