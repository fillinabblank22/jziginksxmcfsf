﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;

using CT = SolarWinds.APM.Web.Charting.ChartInfo.TYPES;

public partial class Orion_APM_Controls_GaugesContainer : System.Web.UI.UserControl
{
    private const int RadialWidth = 160;

    #region Properties

    public String GaugeType { get; set; }

    public SolarWinds.Orion.Web.ResourceInfo Resource { get; set; }

    public List<Component> Components { get; set; }

    protected int GaugeWidth
    {
        get
        {
            return GaugeType == "Linear" 
                ? ApmGaugeResource.GetLinearGaugeWidth(Resource.Properties["Style"])
                : Get("Scale", 100) * RadialWidth / 100;
        }
    }

    protected string GaugeControlId
    {
        get
        {
            return "GaugesContainer" + Resource.JavaScriptFriendlyID;
        }
    }

    public delegate void GaugeCreator(
		Control owner, Int32 scale, String style, String title, String chartLink,
		String netObject, String gaugeName, String gaugeType, String period,
		Int64 min, Int64 max, Int64 tickCount, Int64 units, String unitsLable, Boolean reverseThreshold
	);

    public delegate void DynamicGaugeCreator(
        Control owner, Int32 scale, String style, String title, String chartLink,
        String netObject, String gaugeName, String gaugeType,
        Int64 min, Int64 max, Int64 tickCount, Int64 units, String unitsLable, String statisticValueName
    );

	public GaugeCreator CreateGauge;

    public DynamicGaugeCreator DynamicCreateGauge;

	private String dsType;
	
	#endregion

	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack && Visible)
		{
			var cmps = default(IEnumerable<Component>);

			var info = default(AssignedToResourceInfo);
			AssignedToResourceInfo.TryCreate(Request, Resource, out info);
			if (info.ComponentIDs != null)
			{
				cmps = Components
					.Where(item => info.ComponentIDs.Contains(item.Id));
			}
			if (info[AssignedToResourceInfo.GaugeDataSourceType] != null)
			{
				dsType = info[AssignedToResourceInfo.GaugeDataSourceType].ToString();
			}

			if (cmps == null) 
			{
				var names = SolarWinds.APM.Web.HtmlHelper.SplitNames(Resource.Properties["GaugeComponentNames"]);
				if (names != null) 
				{
					cmps = Components
						.Where(cmp => names.Contains(cmp.Name, StringComparer.InvariantCultureIgnoreCase));
				}
			}
			if (cmps != null)
			{
				ctrCmpRep.DataSource = cmps
					.Except(Components.Where(cmp => (cmp.IsDynamicBased && cmp.DynamicColumnSettings.Where(col => !col.Disabled).Count() == 0)))
					.ToArray();
				ctrCmpRep.DataBind();
			}
		}
	}

	protected void OnCmpRep_ItemDataBound(Object sender, RepeaterItemEventArgs e)
	{
		var item = e.Item;
		if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
		{
			var cmp = item.DataItem as Component;
			
			var ctrHref = item.FindControl("ctrHref");
			var ctrGauge = item.FindControl("ctrGauge");

			var scale = Get("Scale", 100);
			var style = Get("Style", GaugeType == "Radial" ? "Simple Circle" : "Tan Beveled Flood");
			if (dsType.IsNotValid())
			{
				dsType = Get("GaugeDSTypeComponentNames", "SD");
			}
			if (dsType.IsNotValid()) 
			{
				return;
			}
			switch (dsType)
			{
				case "SD":
					{
						if (cmp.HasStatisticData)
						{
							(ctrHref as HyperLink).NavigateUrl = GetHref(CT.MonMMAStatistic, cmp);
							CreateGauge(
								ctrGauge, scale, style, Get("StatisticDataCustomLabel", String.Empty), GetHref(CT.MonMMAStatistic, cmp),
								GetID(cmp), "StatisticData", GaugeType, null, 0, Get<Int64>("GaugeMaxUnitsScale", 2500), 500, Get<Int64>("GaugeUnitsValue", 1), Get("GaugeUnitsLabel", String.Empty), false
							);
						}
						else if (cmp.IsDynamicBased)
						{
							var column = cmp.DynamicColumnSettings
								.FirstOrDefault(x => (x.Type == DynamicEvidenceColumnDataType.Numeric) && !x.Disabled);
							if (column != null)
							{
								(ctrHref as HyperLink).NavigateUrl = GetHref(CT.MonMMAStatistic, cmp);
								DynamicCreateGauge(
									ctrGauge, scale, style, column.Label, GetHref(CT.MonMMAStatistic, cmp),
									GetID(cmp), "DynamicStatisticDataAvg", GaugeType, 0, Get<Int64>("GaugeMaxUnitsScale", 2500), 500, Get<Int64>("GaugeUnitsValue", 1), Get("GaugeUnitsLabel", String.Empty), column.Name);
							}
						}
					}
					break;
				case "RT":
					{
						if (cmp.HasResponseTimeSupport)
						{
							(ctrHref as HyperLink).NavigateUrl = GetHref(CT.MonMMAResponse, cmp);
							CreateGauge(
								ctrGauge, scale, style, String.Empty, GetHref(CT.MonMMAResponse, cmp),
								GetID(cmp), "ResponseTime", GaugeType, null, 0, Get<Int64>("GaugeMaxUnitsScale", 2500), 500, Get<Int64>("GaugeUnitsValue", 1), Get("GaugeUnitsLabel", String.Empty), false
							);
						}
					}
					break;
				case "CPU":
					{
						if (cmp.IsProcessBased)
						{
							(ctrHref as HyperLink).NavigateUrl = GetHref(CT.MonMMACpu, cmp);
							CreateGauge(
								ctrGauge, scale, style, String.Empty, GetHref(CT.MonMMACpu, cmp),
								GetID(cmp), "PercentCpu", GaugeType, null, 0, 100, 10, 0, "%", false
							);
						}
					}
					break;
				case "PM":
					{
						if (cmp.IsProcessBased)
						{
							(ctrHref as HyperLink).NavigateUrl = GetHref(CT.MonMMAPMem, cmp);
							CreateGauge(
								ctrGauge, scale, style, String.Empty, GetHref(CT.MonMMAPMem, cmp),
								GetID(cmp), "PercentMemory", GaugeType, null, 0, 100, 10, 0, "%", false
							);
						}
					}
					break;
				case "VM":
					{
						if (cmp.HasVirtualMemorySupport)
						{
							(ctrHref as HyperLink).NavigateUrl = GetHref(CT.MonMMAVMem, cmp);
							CreateGauge(
								ctrGauge, scale, style, String.Empty, GetHref(CT.MonMMAVMem, cmp),
								GetID(cmp), "PercentVirtual", GaugeType, null, 0, 100, 10, 0, "%", false
							);
						}
					}
					break;
				default:
					{
						if (dsType.StartsWith("AV"))
						{
							var parts = dsType.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
							if (parts.Length == 2)
							{
								(ctrHref as HyperLink).NavigateUrl = GetHref(CT.MonAvailBarChart, cmp);
								CreateGauge(
									ctrGauge, scale, style, String.Empty, GetHref(CT.MonAvailBarChart, cmp),
									GetID(cmp), "Availability", GaugeType, parts[1], 0, 100, 10, 0, "%", true
								);
							}
						}
					}
					break;
			}
			ChangeLayout(item, dsType);
		}
	}

	#endregion

	#region Helper Members

	private T Get<T>(String key, T defValue)
	{
		var val = Resource.Properties[key];
		if (val.IsValid())
		{
			try
			{
				return (T)Convert.ChangeType(val, typeof(T));
			}
			catch { }
		}
		return defValue;
	}

	private String GetID(Component cmp)
	{
		return String.Format("AM:{0}", cmp.Id);
	}

	private String GetHref(Object type, Component cmp)
	{
		return String.Format("/Orion/APM/CustomChart.aspx?ChartName={0}&NetObjectID={1}&Period=Today", type, GetID(cmp));
	}

	private void ChangeLayout(RepeaterItem repItem, String dsType)
	{
		var cmp = repItem.DataItem as Component;
		var cmps = (ctrCmpRep.DataSource as IEnumerable<Component>).Where(item =>
            (dsType == "SD" && (item.HasStatisticData || item.IsDynamicBased)) ||
			(dsType == "RT" && item.HasResponseTimeSupport) ||
			(dsType == "CPU" && item.IsProcessBased) ||
			(dsType == "PM" && item.IsProcessBased) ||
			(dsType == "VM" && item.HasVirtualMemorySupport) ||
			(dsType.StartsWith("AV"))
		).ToList();

		HtmlControl ctrCntr = repItem.FindControl("ctrContainer") as HtmlControl, ctrHdr = repItem.FindControl("ctrHeader") as HtmlControl;
		ctrCntr.Visible = cmps.IndexOf(cmp) > -1;
		if (ctrCntr.Visible)
		{
			if (GaugeType != "Linear")
			{
				ctrHdr.Style[HtmlTextWriterStyle.Height] = "35px";
			}
		}
	}

	#endregion
}
