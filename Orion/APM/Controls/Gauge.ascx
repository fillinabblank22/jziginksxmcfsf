<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Gauge.ascx.cs" Inherits="Orion_APM_Controls_Gauge" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>

<orion:Include runat="server" File="styles/GaugeStyle.css" />

<a href="<%= ChartUrl %>">
    <orion:Gauge ID="gauge" runat="server" />
</a>
<br />

