using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.Web.Utility;

[Obsolete("This is only used for 'select single node' for everything else we are moving to SearchableNodeSelection.ascx")]
public partial class Orion_APM_Controls_SelectNodeTree : System.Web.UI.UserControl
{
	#region Fields

	private string treeHeight = "300px";
	private string treeWidth = "600px";
	private string treeBorder = "1px solid #c3daf9";
	private string treeClass;
	private bool allowMultipleSelections = true;
	private string afterNodeSelectionChangeScript;

	private string viewNodeFilter;

	#endregion

	#region Fields

	public string TreeHeight
	{
		get { return treeHeight; }
		set { treeHeight = value; }
	}

	public string TreeWidth
	{
		get { return treeWidth; }
		set { treeWidth = value; }
	}

	public string TreeBorder
	{
		get { return treeBorder; }
		set { treeBorder = value; }
	}

	public string TreeCssClass
	{
		get { return treeClass; }
		set { treeClass = value; }
	}

	public bool AllowMultipleSelections
	{
		get { return allowMultipleSelections; }
		set { allowMultipleSelections = value; }
	}

	public string NodeToSelectByDefault
	{
		get { return selectedNodes.Value; }
		set { selectedNodes.Value = value; }
	}

	public string ViewNodeFilter
	{
		protected get
		{
			if (viewNodeFilter.IsNotValid())
			{
				return "null";
			}
			return new JavaScriptSerializer().Serialize(viewNodeFilter);
		}
		set { viewNodeFilter = value; }
	}

	public string AfterNodeSelectionChangeScript
	{
		get { return afterNodeSelectionChangeScript; }
		set { afterNodeSelectionChangeScript = value; }
	}

	protected string CookieInfo
	{
		get { return string.Format("{{\"path\":\"/sgn{0}{1}/\",\"domain\":\"{2}\"}}", this.Page.GetType().GetHashCode(), Profile.UserName.GetHashCode(), this.Request.Url.Host); }
	}

	#endregion

	#region Event Handlers

	protected void GroupByDropDown_OnInit(object sender, EventArgs e)
	{
		IList<string> customProperties = SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetPropNamesForTable("Nodes", false);
		List<string> usableCustomProperties = new List<string>(customProperties.Count);
		foreach (string prop in customProperties)
		{
			Type fieldType = SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetTypeForProp("Nodes", prop);
			if (fieldType != typeof(System.Text.StringBuilder))
				usableCustomProperties.Add(prop);
		}

		GroupByDropDown.Items.Clear();
        GroupByDropDown.Items.Add(new ListItem(Resources.APMWebContent.APMWEBCODE_VB1_41, "Vendor"));
        GroupByDropDown.Items.Add(new ListItem(Resources.APMWebContent.APMWEBCODE_VB1_42, "MachineType"));
        GroupByDropDown.Items.Add(new ListItem(Resources.APMWebContent.APMWEBCODE_VB1_43, "SNMPVersion"));

		//Add CustomProperties. to retrieve custom properties using SWIS DAL
		foreach (string customProperty in usableCustomProperties)
		{
			GroupByDropDown.Items.Add(new ListItem(customProperty,
				string.Format("CustomProperties.{0}", customProperty)));
		}
	}

	#endregion

	#region Helper Members

	public List<int> GetSelectedNodeIds()
	{
		return GetSelectedNodeInfo().ConvertAll(item => item.Id);
	}

	public List<SelectNodeTreeItem> GetSelectedNodeInfo()
	{
		var result = new List<SelectNodeTreeItem>();
		if (selectedNodes.Value.IsNotValid()) 
		{
			return result;
		}
		var selectedNodesLookup = JsHelper.Deserialize<Dictionary<string, string>>(selectedNodes.Value);
		foreach (KeyValuePair<string, string> item in selectedNodesLookup)
		{
			int id;
			if (Int32.TryParse(item.Key, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out id))
			{
				result.Add(new SelectNodeTreeItem(id, item.Value));
			}
		}
		return result;
	}

	#endregion
}