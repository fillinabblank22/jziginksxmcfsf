<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InternalCustomChart.ascx.cs" 
    Inherits="Orion_APM_Controls_InternalCustomChart" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<orion:Include runat="server" File="APM/Charts.css" />

<img id="chartImage" alt="" src="" runat="server"/>
<br />

<div runat="server" id="editControlsContainer">
	<table width="640" style=""
	       border="0" cellspacing="0" cellpadding="2">
	
	<tr class="PageHeader DefaultShading" style="height: 2em;">
	    <td style="width: 5px;">&nbsp;</td>
		<td align="left" colspan="2" style="width: 50% !important"><%= Resources.APMWebContent.APMWEBDATA_VB1_66%></td>
	    <td style="width: 5px;">&nbsp;</td>		
	</tr>
	<tr class="Property apm_DarkShading" style="height: 25px;">
	    <td>&nbsp;</td>	
	    <td colspan="2">
	        <orion:LocalizableButton runat="server" ID="LocalizableButton1" OnClick="OnRefresh" DisplayType="Primary" LocalizedText="Refresh"/>
        </td>
		<td align="right">
		    <orion:LocalizableButtonLink runat="server" ID="RawDataLinkTop" DisplayType="Small" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_85  %>" ToolTip="<%$ Resources: APMWebContent, APMWEBDATA_VB1_84 %>" NavigateUrl="/Orion/APM/ChartData.aspx"/>
		</td>
	    <td>&nbsp;</td>	
	</tr>

    <tr>
	    <td>&nbsp;</td>
    </tr>
    
	<tr>
	    <td>&nbsp;</td>
		<td colspan="3"><b><%= Resources.APMWebContent.APMWEBDATA_VB1_68 %></b></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>
	    <td style="width: 10px;">&nbsp;</td>
	    <td><%= Resources.APMWebContent.APMWEBDATA_VB1_69 %></td>
	    <td><asp:TextBox runat="server" ID="ChartTitle" Width="225px"></asp:TextBox></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><%= Resources.APMWebContent.APMWEBDATA_VB1_70 %></td>
	    <td><asp:TextBox runat="server" ID="SubTitle" Width="225px"></asp:TextBox></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><%= Resources.APMWebContent.APMWEBDATA_VB1_71 %></td>
	    <td><asp:TextBox runat="server" ID="SubTitle2" Width="225px"></asp:TextBox></td>
	</tr>

	<tr style="height: 20px;">
	    <td colspan="5"><hr class="Property" /></td>
    </tr>
	
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= Resources.APMWebContent.APMWEBDATA_VB1_72 %></b></td>
		<td>
            <asp:DropDownList ID="TimePeriodsList" runat="server" Width="230px"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><%= Resources.APMWebContent.APMWEBDATA_VB1_73 %></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= Resources.APMWebContent.APMWEBDATA_VB1_74 %></b></td>		
	</tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
        <td><%= Resources.APMWebContent.APMWEBDATA_VB1_75 %></td>	
        <td>
            <asp:TextBox runat="server" ID="PeriodBegin"  Width="225px"></asp:TextBox>
            <span style="color: Red;" runat="server" ID="PeriodBeginError" visible="false"><%# Resources.APMWebContent.APMWEBDATA_VB1_76 %></span>
        </td>
    </tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
        <td><%= Resources.APMWebContent.APMWEBDATA_VB1_77 %></td>	
        <td>
            <asp:TextBox runat="server" ID="PeriodEnd"  Width="225px"></asp:TextBox>
            <span style="color: Red;" runat="server" ID="PeriodEndError" visible="false"><%# Resources.APMWebContent.APMWEBDATA_VB1_76 %></span>
        </td>
    </tr>
    <tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
        <td style="font-size: 8pt;">
            <%= Resources.APMWebContent.APMWEBDATA_VB1_83 %><%= DateTimeSyntaxExamples() %>
        </td>
    </tr>
	
	<tr style="height: 20px;">
		<td colspan="5"><hr class="Property" /></td>		
	</tr>
	
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= Resources.APMWebContent.APMWEBDATA_VB1_78 %></b></td>
		<td>
            <asp:DropDownList ID="SampleSizeList" runat="server" Width = "230px"></asp:DropDownList>		    
		</td>
		
	</tr>

	<tr style="height: 20px;">
		<td colspan="5"><hr class="Property" /></td>		
	</tr>
	
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= Resources.APMWebContent.APMWEBDATA_VB1_79 %></b></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td><%= Resources.APMWebContent.APMWEBDATA_VB1_80 %></td>	
        <td><asp:TextBox ID="Width" Columns="10" MaxLength="4" runat="server"></asp:TextBox></td>
    </tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td><%= Resources.APMWebContent.APMWEBDATA_VB1_81 %></td>	
        <td><asp:TextBox ID="Height" Columns="10" MaxLength="4" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td style="font-size: 8pt;"><%= Resources.APMWebContent.APMWEBDATA_VB1_82 %></td>
    </tr>
	
	<tr class="Property apm_DarkShading" style="height: 25px;">
	    <td>&nbsp;</td>	
	    <td colspan="2">
	        <orion:LocalizableButton runat="server" ID="LocalizableButton2" OnClick="OnRefresh" DisplayType="Primary" LocalizedText="Refresh"/>
        </td>
		<td align="right">
		    <orion:LocalizableButtonLink runat="server" ID="RawDataLinkBottom" DisplayType="Small" Text="<%$ Resources: APMWebContent, APMWEBDATA_VB1_85  %>" ToolTip="<%$ Resources: APMWebContent, APMWEBDATA_VB1_84 %>" NavigateUrl="/Orion/APM/ChartData.aspx"/>
		</td>
	    <td>&nbsp;</td>	
	</tr>
	
	
	</table>    
</div>