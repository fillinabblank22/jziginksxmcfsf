﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_MultiValueSelection : System.Web.UI.UserControl
{
	private static readonly Log log = new Log();

	bool DisplayAll
	{
		get { return SelectionType.SelectedValue == "All"; }
		set { SelectionType.SelectedValue = value ? "All" : "SelectedOnly"; }
	}

	bool OptionEnabled
	{
		get { return Request.QueryStringOrForm(ModuleConstants.QueryStringParameters.MultiValueSelection) != null; }
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		if (this.IsPostBack)
			return;
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		SetVisibility();
	}

	protected void SelectionType_OnSelectedIndexChanged(object sender, EventArgs e)
	{
		SetVisibility();
	}

	protected void Validator_OnServerValidate(object sender, ServerValidateEventArgs e)
	{
		if (this.OptionEnabled)
		{
			e.IsValid = DisplayAll || GetSelectedItems().Count != 0;
		}
	}

	public void LoadData(ListItem selectedComponent)
	{
		if (OptionEnabled)
		{
			var dal = new DynamicEvidenceDal();
			DataTable table = null;

			string selectedComponentName = null;
			int applicationId;
			long componentId;
			if (selectedComponent != null) 
			{
				selectedComponentName = selectedComponent.Text;
				if (long.TryParse(selectedComponent.Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out componentId))
				{
					table = dal.GetDynamicEvidenceColumns(componentId, DynamicEvidenceColumnDataType.Numeric);
				}
			}
			if (table == null)
			{
				if (long.TryParse(Request.QueryStringOrForm(ModuleConstants.QueryStringParameters.ComponentId), NumberStyles.Integer, CultureInfo.InvariantCulture, out componentId))
				{
					table = dal.GetDynamicEvidenceColumns(componentId, DynamicEvidenceColumnDataType.Numeric);
				}
				else if (selectedComponentName.IsValid() && int.TryParse(Request.QueryStringOrForm(ModuleConstants.QueryStringParameters.ApplicationId), NumberStyles.Integer, CultureInfo.InvariantCulture, out applicationId))
				{
					table = dal.GetDynamicEvidenceColumns(applicationId, selectedComponentName, DynamicEvidenceColumnDataType.Numeric);
				}
				else
				{
					log.Warn("ComponentId or ApplicationId not present in query string or invalid");
				}
			}

			Visible = table != null;
			if (table != null)
			{
				chblSelectedValues.DataSource = table;
				chblSelectedValues.DataBind();
			}
		}
	}

	public void InitializeSettings(ResourceInfo resource)
	{
		string valuesToDisplay = null;

		var info = default(AssignedToResourceInfo);
        AssignedToResourceInfo.TryCreate(resource.Properties[ApmMonitor.PropAssignedToResourceInfo], AssignedToResourceInfo.GetNetObject(Request, resource), out info);
		if (info[AssignedToResourceInfo.ColumnNames] == null)
		{
			valuesToDisplay = resource.Properties[ResourceSettings.ValuesToDisplay];
		}
		else 
		{
			valuesToDisplay = info[AssignedToResourceInfo.ColumnNames].ToString();
		}

		bool showAll = String.IsNullOrEmpty(valuesToDisplay);

		foreach (ListItem item in chblSelectedValues.Items)
		{
			item.Selected = false;
		}

		DisplayAll = showAll;

		if (!showAll)
		{
			string[] values = valuesToDisplay.Split(new string[] { ResourceSettings.ValueSeparator }, StringSplitOptions.RemoveEmptyEntries);
			List<string> selectedValues = new List<string>(values);
			bool somethingSelected = false;

			foreach (ListItem item in chblSelectedValues.Items)
			{
				if (selectedValues.Exists(o => String.Compare(o, item.Value, StringComparison.InvariantCultureIgnoreCase) == 0))
				{
					item.Selected = true;
					somethingSelected = true;
				}
			}

			if (!somethingSelected)
			{
				DisplayAll = true;
			}
		}
		SetVisibility();
	}

	public void UpdateSettings(ResourceInfo resource)
	{
		if (Visible) 
		{
			var valuesToDisplay = String.Empty;
			if (!DisplayAll)
			{
				valuesToDisplay = String.Join(ResourceSettings.ValueSeparator, GetSelectedItems().Select(o => o.Value).ToArray());
			}
			AssignedToResourceInfo.Update(Request, resource, new[] { new DictionaryEntry(AssignedToResourceInfo.ColumnNames, valuesToDisplay) });
			resource.Properties[ResourceSettings.ValuesToDisplay] = valuesToDisplay;
		}
	}

	void SetVisibility()
	{
		this.Visible &= this.OptionEnabled;

		bool displayAllValues = DisplayAll;

		SelectItemsPanel.Visible = !displayAllValues;
	}

	List<ListItem> GetSelectedItems()
	{
		List<ListItem> selectedValues = new List<ListItem>();
		foreach (ListItem item in chblSelectedValues.Items)
		{
			if (item.Selected)
				selectedValues.Add(item);
		}
		return selectedValues;
	}
}