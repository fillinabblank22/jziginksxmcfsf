﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;

using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Models;
using SolarWinds.Orion.Web.UI;

namespace Orion.APM.Controls
{
    public partial class Orion_APM_Controls_ComponentErrorStatusControl : System.Web.UI.UserControl
    {
        private readonly ComponentDal componentDal = new ComponentDal();

        public List<ComponentErrorStatus> Monitors { get; private set; }

        public bool HasComponentHyperlink(long componentId)
        {
            using (DataTable dt = componentDal.GetComponentByID(componentId))
            {
                return dt.Rows.Count > 0;
            }
        }

        public void Initialize(List<ComponentErrorStatus> monitors)
        {
            this.monitorRepeater.DataSource = monitors;
            this.monitorRepeater.DataBind();
            this.phStatuses.Visible = (monitors.Count > 0);
            this.Monitors = monitors;
        }

        public string GetViewLinkHtml(long componentId, string name)
        {
            if (!HasComponentHyperlink(componentId))
            {
                return string.Empty;
            }
            string link = BaseResourceControl.GetViewLink("AM:" + componentId.ToString(CultureInfo.InvariantCulture));
            return string.Format(
                "<a href=\"{0}\">{1}</a>",
                link,
                Server.HtmlEncode(name)
                );
        }
    }
}
