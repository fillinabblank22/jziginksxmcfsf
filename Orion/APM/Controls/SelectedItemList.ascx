﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectedItemList.ascx.cs" 
    Inherits="Orion_APM_Controls_SelectedItemList" %>

<div class="apm_SelectItemList">
    <asp:Label ID="HeaderLabel" runat="server"></asp:Label>
    <asp:BulletedList ID="ItemList" runat="server" BulletStyle="Square"></asp:BulletedList>
    <span id="ViewAllRegion" runat="server">
        <a id="ViewAll" runat="server" href="#"><%= Resources.APMWebContent.Controls_ViewAll%></a> &#0187;
    </span>    
</div>