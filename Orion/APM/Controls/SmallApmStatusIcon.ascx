<%@ Control Language="C#" ClassName="SmallApmStatusIcon" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>

<script runat="server">
    public ApmStatus StatusValue
    {
        get
        {
            if (null == ViewState["StatusValue"])
            {
                return new ApmStatus();
            }

            return (ApmStatus)ViewState["StatusValue"];
        }

        set
        {
            ViewState["StatusValue"] = value;
        }
    }

    public ApmMonitorError ErrorValue
    {
        get
        {
            if (ViewState["ErrorValue"] == null)
                return new ApmMonitorError();

            return (ApmMonitorError)ViewState["ErrorValue"];
        }
        set
        {
            ViewState["ErrorValue"] = value;
        }
    }

    public string CustomApplicationType
    {
        get
        {
            if (null == ViewState["CustomApplicationType"])
            {
                return null;
            }

            return (string)ViewState["CustomApplicationType"];
        }

        set
        {
            ViewState["CustomApplicationType"] = value;
        }
    }


    protected string ImagePath
    {
        get
        {
            return this.StatusValue.ToString("smallimgpath", CustomApplicationType);
        }
    }

    protected string ErrorDescription
    {
        get
        {
            return this.ErrorValue.ToString(ApmMonitorError.formatFull, null);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        smallStatusIcon.DataBind();
    }
</script>

<asp:Image ID="smallStatusIcon" runat="server" CssClass="apm_StatusIcon" AlternateText="<%# StatusValue.ToString() %>"  ImageUrl="<%# ImagePath %>" ToolTip="<%# ErrorDescription %>" />