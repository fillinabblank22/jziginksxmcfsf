﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DisplayTypes;

public partial class Orion_APM_Controls_BlackBox_PopupComponentList : System.Web.UI.UserControl
{
    private Func<Component, string> getStatusImagePath { get; set; }

    private int remainingComponentCount;

    public string Title { get; set; }

    protected string ComponentListTitle
    {
        get { return string.IsNullOrEmpty(this.Title) ? APMWebContent.APMWEBDATA_TM0_54 : this.Title; }
    }

    protected string MoreComponentsMessage
    {
        get
        {
            string template = (this.remainingComponentCount == 1)
                ? Resources.APMWebContent.APMWEBCODE_TM0_10
                : Resources.APMWebContent.APMWEBCODE_TM0_11;
            return string.Format(template, this.remainingComponentCount);
        }
    }

    public void BindComponents(IEnumerable<Component> components, int topComponentsCount, Func<Component, string> fnStatusImage)
    {
        this.getStatusImagePath = fnStatusImage;

        var allComponents = components.ToList();
        var topComponents = allComponents.Take(topComponentsCount).ToList();
        this.remainingComponentCount = allComponents.Count - topComponents.Count;

        phMoreComp.Visible = (this.remainingComponentCount > 0);
        phNoComps.Visible = (topComponents.Count == 0);

        repComponents.DataSource = topComponents;
        repComponents.DataBind();
    }

    public void BindComponents(IEnumerable<Component> components, int topComponentsCount, string appCustomType, string appItemType = null)
    {
        Func<Component, string> fn = (component) => (new ApmStatus(component.Status)).ToString("smallimgpath", appCustomType, appItemType);
        BindComponents(components, topComponentsCount, fn);
    }

    protected string GetComponentStatusImagePath(object component)
    {
        return this.getStatusImagePath((Component)component);
    }

    protected string GetComponentName(object component)
    {
        return ((Component)component).Name;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
