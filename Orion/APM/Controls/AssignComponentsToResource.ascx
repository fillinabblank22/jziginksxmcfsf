﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignComponentsToResource.ascx.cs" Inherits="Orion_APM_Controls_AssignComponentsToResource" %>

<asp:Panel ID="pnlSelectComponent" runat="server">
    <%= Resources.APMWebContent.APMWEBDATA_PS0_14 %>
    <asp:CheckBoxList ID="chblComponents" runat="server" DataValueField="UniqueId" DataTextField="Name" />    
</asp:Panel>
<asp:Label ID="lblNoComponent" runat="server" CssClass="apm_ErrorMessage" Text="<%$ Resources: APMWebContent, APMWEBDATA_PS0_8 %>"></asp:Label>