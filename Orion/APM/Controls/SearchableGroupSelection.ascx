<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchableGroupSelection.ascx.cs" 
    Inherits="Orion_APM_Controls_SearchableGroupSelection" %>

<%@ Register Src="~/Orion/APM/Controls/AddRemoveObjects/AddRemoveObjectsControl.ascx" TagName="AddRemoveObjectsControl" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:IncludeExtJs ID="IncludeExtJs" runat="server" debug="false" Version="3.4"/>
<orion:Include runat="server" File="js/OrionCore.js"/>

<div>
    <orion:AddRemoveObjectsControl ID="AddRemoveObjectsControl" runat="server" />
</div>