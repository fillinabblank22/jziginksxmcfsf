﻿using System;
using System.Web.UI;

using SolarWinds.APM.Web;
using SolarWinds.APM.Common;

public partial class Orion_APM_Controls_RequiresComponentName : System.Web.UI.UserControl, IConfigurationRequiredControl
{
	#region Filds

	protected String _resourceName;

	#endregion

	#region IConfigurationRequiredControl Members

	public void SetMonitorData(ApmBaseResource res)
	{
		var tplID = -1;

		var netObject = res.TryGetNetObject();
		if (netObject is ApmMonitor)
		{
			tplID = (netObject as ApmMonitor).Application.TemplateId;
		}
		else if (netObject is ApmApplication)
		{
			tplID = (netObject as ApmApplication).TemplateId;
		}

		var isValidTemplate = tplID > -1;
		if (isValidTemplate)
		{
            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
			{
				isValidTemplate = !bl.GetApplicationTemplate(tplID).IsMockTemplate;
			}
		}

		if (isValidTemplate)
		{
			ctrEditURL.NavigateUrl = res.EditURL;
			if (res.Resource.Name.StartsWith("Multi Component Statistics"))
			{
                message.Text = Resources.APMWebContent.APMWEBCODE_VB1_139;
			}
			else
			{
                message.Text = Resources.APMWebContent.APMWEBCODE_VB1_140;
			}
			ctrEditResource.Visible = true;
		}
		else
		{
			_resourceName = res.Resource.Name;

			var wrapper = default(Orion_ResourceWrapper);
			this.FindResourceWrapper(this, out wrapper);
			if (wrapper != null)
			{
				wrapper.ShowEditButton = false;
			}
			ctrInvalidTpl.Visible = true;
		}
	}

	#endregion

	#region Helper Members

	private void FindResourceWrapper(Control parent, out Orion_ResourceWrapper wrapper)
	{
		wrapper = default(Orion_ResourceWrapper);
		foreach (Control ctr in parent.Controls)
		{
			if ((wrapper = ctr as Orion_ResourceWrapper) != null)
			{
				return;
			}
			if (ctr.Controls.Count > 0)
			{
				this.FindResourceWrapper(ctr.Parent, out wrapper);
			}
		}
	}

	#endregion
}
