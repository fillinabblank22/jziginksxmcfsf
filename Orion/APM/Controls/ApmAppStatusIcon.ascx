<%@ Control Language="C#" ClassName="ApmAppStatusIcon" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>

<script runat="server">
    public ApmStatus StatusValue
    {
        get
        {
            if (null == ViewState["StatusValue"])
            {
                return new ApmStatus();
            }

            return (ApmStatus)ViewState["StatusValue"];
        }

        set
        {
            ViewState["StatusValue"] = value;
        }
    }

    protected string ImagePath
    {
        get
        {
            return this.StatusValue.ToString("appimgpath", null);
        }
    }
</script>

<img class="apm_StatusIcon" alt="<%= StatusValue.ToString() %>" src="<%= ImagePath %>" />