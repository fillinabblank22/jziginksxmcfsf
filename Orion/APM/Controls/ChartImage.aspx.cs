﻿using System;
using System.IO;
using System.Drawing.Imaging;

using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_Controls_ChartImage : System.Web.UI.Page
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		ChartInfo info = ChartInfoFactory.Create(this.Request.QueryStringOrForm(ChartInfo.KEYS.ChartName));
		info.Populate(this.Request.QueryString);

		ChartBase chart = info.CreateChart();
		
		String errorMessage;
		if (info.AreSettingsValid(out errorMessage))
		{
			var data = info.LoadData();
			chart.DataBind(data);
		}
		else
		{
			chart.NoDataText = errorMessage;
			chart.DataBind();
		}

		using (MemoryStream stream = new MemoryStream()) 
		{
			chart.GetAsImage(stream, ImageFormat.Png);
			stream.WriteTo(this.Response.OutputStream);
		}
		this.Response.ContentType = "image/png";
		this.Response.End();
	}

	#endregion
}