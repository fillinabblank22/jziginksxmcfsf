﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditWindowsEventDetails.ascx.cs" Inherits="Orion_APM_Controls_EditResourceControls_EditWindowsEventDetails" %>

<p>
    <b><%=Resources.APMWebContent.APMWEBCODE_PV0_2%></b><br/>
    <asp:TextBox runat="server" size="30" ID="pageSize" />
    <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ Resources: APMWebContent,APMWEBCODE_PV0_3 %>"
                Display="Dynamic" ControlToValidate="pageSize" MinimumValue="1" MaximumValue="100"
                Type="Integer" />
</p>
