﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Management.ascx.cs" Inherits="Orion_APM_Controls_EditResourceControls_Management" %>

<p>
    <b><%= Resources.APMWebContent.Management_Edit_SelectSections %></b>
    <asp:CheckBoxList ID="availableList" runat="server" OnInit="checkList_OnInit"/>
    <b/>
    <b/>
    <asp:CheckBox ID="combine" Text="<%# Resources.APMWebContent.Management_Edit_Combined %>" runat="server"/>
</p>