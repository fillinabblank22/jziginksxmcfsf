﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_EditResourceControls_Management : BaseResourceEditControl
{
    public override Dictionary<string, object> Properties
    {
        get
        {
            var disabled = availableList.Items.Cast<ListItem>().Where(i => !i.Selected).Select(i => i.Text).OrderBy(s => s);

            var props = new Dictionary<string, object>
            {
                {"AvailableSections", Resource.Properties["AvailableSections"]},
                {"DisabledSections", String.Join(",", disabled)},
                {"Combined", combine.Checked}
            };
            return props;
        }
    }

    protected void checkList_OnInit(object sender, EventArgs e)
    {
        var availableSections = Resource.Properties.ContainsKey("AvailableSections")
            ? Resource.Properties["AvailableSections"].Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).ToList()
            : new List<string>();

        var disabledSections = Resource.Properties.ContainsKey("DisabledSections")
            ? Resource.Properties["DisabledSections"].Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).ToList()
            : new List<string>();

        availableList.DataSource = availableSections;
        availableList.DataBind();

        foreach (ListItem item in availableList.Items)
        {
            item.Selected = !disabledSections.Contains(item.Text);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool result;
        bool.TryParse(this.Resource.Properties["Combined"], out result);
        combine.Checked = result;
        combine.DataBind();
    }
}