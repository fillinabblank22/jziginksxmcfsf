﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationCustomProperties.ascx.cs" Inherits="Orion_APM_Controls_EditResourceControls_EditApplicationCustomProperties" %>
<div>
    <div runat="server" id="propertiesList">
        <b><%= Resources.CoreWebContent.WEBDATA_AK0_294 %></b>
        <br />
        <asp:CheckBoxList runat="server" ID="selectedProperties" OnDataBound="SelectedProperties_DataBound" />
    </div>
    <div runat="server" id="noPropertiesMessage" style="color: Red; font-weight: bold;"
        visible="false">
        <%= Resources.CoreWebContent.WEBDATA_AK0_295 %></div>
    <br />
</div>
