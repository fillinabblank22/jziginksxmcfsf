﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using System.Globalization;

public partial class Orion_APM_Controls_EditResourceControls_EditAllApplications : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

		var props = Resource.Properties;

		SwqlFilter.Text = props["Filter"];

        bool allowUnsortableProperties;
        bool.TryParse(Request.QueryString["AllowUnsortableProperties"] ?? "false", out allowUnsortableProperties);

        //ReferrerRedirector.Initialize(ViewState);

        foreach (string propName in Node.GetCustomPropertyNames(allowUnsortableProperties))
        {
            string valueName = "Nodes.CustomProperties." + propName;
            string displayedName = string.Format(CultureInfo.CurrentCulture,"{0} {1}", Resources.APMWebContent.ApmWeb_CP_NodePrefix, propName);

            lbxGroup1.Items.Add(new ListItem(displayedName, valueName));
            lbxGroup2.Items.Add(new ListItem(displayedName, valueName));
            lbxGroup3.Items.Add(new ListItem(displayedName, valueName));
        }
        foreach (var propName in SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetPropNamesForTable("APM_ApplicationCustomProperties", false))
        {
            string valueName = string.Format(CultureInfo.CurrentCulture, "Application.CustomProperties.{0}", propName);
            string displayedName = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Resources.APMWebContent.ApmWeb_CP_ApplicationPrefix, propName);

            lbxGroup1.Items.Add(new ListItem(displayedName, valueName));
            lbxGroup2.Items.Add(new ListItem(displayedName, valueName));
            lbxGroup3.Items.Add(new ListItem(displayedName, valueName));
        }

		lbxGroup1.SelectedValue = props["Grouping1"];
		lbxGroup2.SelectedValue = props["Grouping2"];
		lbxGroup3.SelectedValue = props["Grouping3"];

		GroupNulls.SelectedValue = props["GroupNodesWithNullPropertiesAsUnknown"] ?? "true";

		lbxAppOrder.SelectedValue = props["AppCmpOrder"] ?? "1";
		lbxCmpOrder.SelectedValue = props["CmpOrder"] ?? "1";

		RememberCollapseState.Checked = Boolean.Parse(props["RememberCollapseState"] ?? "true");

    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var props = new Dictionary<string, object>();

            FixupBlankGroupings();

			props["Grouping1"] = this.lbxGroup1.SelectedValue;
			props["Grouping2"] = this.lbxGroup2.SelectedValue;
			props["Grouping3"] = this.lbxGroup3.SelectedValue;

			props["GroupNodesWithNullPropertiesAsUnknown"] = this.GroupNulls.SelectedValue;

			props["AppCmpOrder"] = lbxAppOrder.SelectedValue;
			props["CmpOrder"] = lbxCmpOrder.SelectedValue;

			props["Filter"] = SwqlFilter.Text;

			props["RememberCollapseState"] = this.RememberCollapseState.Checked.ToString();
    
            // We changed stuff so clear out the expanded tree node information
            if (!this.RememberCollapseState.Checked)
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

			return props;
        }
    }

    private void FixupBlankGroupings()
    {
        List<string> groups = new List<string>();
        groups.Add(lbxGroup1.SelectedValue);
        groups.Add(lbxGroup2.SelectedValue);
        groups.Add(lbxGroup3.SelectedValue);

        groups.RemoveAll(string.IsNullOrEmpty);

        for (int i = 0; i < 3; ++i) groups.Add(string.Empty);

        lbxGroup1.SelectedValue = groups[0];
        lbxGroup2.SelectedValue = groups[1];
        lbxGroup3.SelectedValue = groups[2];
    }
}
