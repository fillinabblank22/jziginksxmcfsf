﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web;
using System.Globalization;

public partial class Orion_APM_Controls_EditResourceControls_EditWindowsEventDetails : BaseResourceEditControl
{
    private const string _propertyPageSize = "pageSize";

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            if (Resource.Properties.ContainsKey(_propertyPageSize))
            {
                pageSize.Text = Resource.Properties[_propertyPageSize];
            }
            else
            {
                pageSize.Text = ComponentConstants.WindowsEventLog.DefaultPageSize.ToString(CultureInfo.CurrentCulture);
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                {_propertyPageSize, pageSize.Text}
            };

            return properties;
        }
    }
}