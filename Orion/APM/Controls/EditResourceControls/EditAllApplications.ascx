﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllApplications.ascx.cs"
    Inherits="Orion_APM_Controls_EditResourceControls_EditAllApplications" %>

<p>
    <b><%= Resources.APMWebContent.Controls_EditAllApplications_GroupingApplications%></b><br />
    <%= Resources.APMWebContent.Controls_EditAllApplications_GroupingApplications_SubTitle%>
</p>

<p>
    <%= Resources.APMWebContent.Controls_EditAllApplications_Level1%><br />
    <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_None %>" Value="" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_ApplicationTemplate %>" Value="ApplicationTemplate.Name" Selected="True" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeName %>" Value="Nodes.Caption" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeStatus %>" Value="Nodes.Status" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeVendor %>" Value="Nodes.Vendor" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeMachineType %>" Value="Nodes.MachineType" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeContact %>" Value="Nodes.Contact" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeLocation %>" Value="Nodes.Location" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeOSVersion %>" Value="Nodes.IOSVersion" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeObjectSubType %>" Value="Nodes.ObjectSubType" />
    </asp:ListBox>
</p>

<p>
    <%= Resources.APMWebContent.Controls_EditAllApplications_Level2%><br />
    <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_None %>" Value="" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_ApplicationTemplate %>" Value="ApplicationTemplate.Name" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeName %>" Value="Nodes.Caption" Selected="True" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeStatus %>" Value="Nodes.Status" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeVendor %>" Value="Nodes.Vendor" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeMachineType %>" Value="Nodes.MachineType" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeContact %>" Value="Nodes.Contact" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeLocation %>" Value="Nodes.Location" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeOSVersion %>" Value="Nodes.IOSVersion" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeObjectSubType %>" Value="Nodes.ObjectSubType" />
    </asp:ListBox>
</p>

<p>
    <%= Resources.APMWebContent.Controls_EditAllApplications_Level3%><br />
    <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_None %>" Value="" Selected="True" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_ApplicationTemplate %>" Value="ApplicationTemplate.Name"/>
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeName %>" Value="Nodes.Caption"/>
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeStatus %>" Value="Nodes.Status" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeVendor %>" Value="Nodes.Vendor" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeMachineType %>" Value="Nodes.MachineType" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeContact %>" Value="Nodes.Contact" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeLocation %>" Value="Nodes.Location" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeOSVersion %>" Value="Nodes.IOSVersion" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_NodeObjectSubType %>" Value="Nodes.ObjectSubType" />
    </asp:ListBox>
</p>

<b><%= Resources.APMWebContent.Controls_EditAllApplications_PutNodesWithNullValues%></b>
<asp:RadioButtonList runat="server" ID="GroupNulls">
	<asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_InTheUnknownGroup %>" Value="true" />
    <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_AtTheBottomOfTheList %>" Value="false" />
</asp:RadioButtonList>

<p>
    <b><%= Resources.APMWebContent.Controls_EditAllApplications_NodeApplicationOrder%></b><br />
    <asp:ListBox ID="lbxAppOrder" SelectionMode="single" Rows="1" runat="server">
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_None %>" Value="1" Selected="True" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_NameAsc %>" Value="Name ASC"/>
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_NameDesc %>" Value="Name DESC"/>
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_StatusBest %>" Value="Ranking DESC" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_StatusWorst %>" Value="Ranking ASC" />
    </asp:ListBox>
</p>

<p>
    <b><%= Resources.APMWebContent.Controls_EditAllApplications_ComponentOrder%></b><br />
    <asp:ListBox ID="lbxCmpOrder" SelectionMode="single" Rows="1" runat="server">
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_None %>" Value="1" Selected="True" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_NameAsc %>" Value="Name ASC"/>
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_NameDesc %>" Value="Name DESC"/>
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_StatusBest %>" Value="Ranking DESC" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_StatusWorst %>" Value="Ranking ASC" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_OrderAsc %>" Value="Order ASC"/>
        <asp:ListItem Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_Order_OrderDesc %>" Value="Order DESC"/>
    </asp:ListBox>
</p>

<p>
    <asp:CheckBox runat="server" ID="RememberCollapseState" Text="<%$ Resources: APMWebContent, Controls_EditAllApplications_RememberExpandedGroups %>" />
</p>

<p>
    <b><%= Resources.APMWebContent.Controls_EditAllApplications_FilterApplicationsSWQL%></b><br />
    <asp:TextBox runat="server" ID="SwqlFilter" Width="330"></asp:TextBox>
</p>

<p><%= Resources.APMWebContent.Controls_EditAllApplications_FiltersAreOptional%><br/>
<%= Resources.APMWebContent.Controls_EditAllApplications_FiltersAreOptional_SubTitle%></p>

<div style="font-size:8pt">
    <%= Resources.APMWebContent.Controls_EditAllApplications_FilterExamples%>
    <ul>
        <li><%= Resources.APMWebContent.Controls_EditAllApplications_FilterExamples_ApplicationNameExchange%></li>
        <li><%= Resources.APMWebContent.Controls_EditAllApplications_FilterExamples_ApplicationTemplateNameExchange%></li>            
        <li><%= Resources.APMWebContent.Controls_EditAllApplications_FilterExamples_NodesCaptionLikeExchange%></li>
        <li><%= Resources.APMWebContent.Controls_EditAllApplications_FilterExamples_NodesCustomPropertiesCityNewYork%></li>
    </ul>
</div>
