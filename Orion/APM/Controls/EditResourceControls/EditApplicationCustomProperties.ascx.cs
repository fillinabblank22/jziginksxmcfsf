﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_EditResourceControls_EditApplicationCustomProperties : BaseResourceEditControl
{
    private Dictionary<string, object> properties = new Dictionary<string, object>();
    private const char separator = ',';
    private const string PropertyListKey = "PropertyList";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        properties.Add(PropertyListKey, string.Empty);

        var customProperties = from property in CustomPropertyMgr.GetCustomPropertiesForTable("APM_ApplicationCustomProperties")
                               orderby property.PropertyName
                               select property;

        if (customProperties.Count() > 0)
        {
            this.selectedProperties.DataSource = customProperties;
            this.selectedProperties.DataTextField = "PropertyName";
            this.selectedProperties.DataValueField = "PropertyName";
            this.selectedProperties.DataBind();
        }
        else
        {
            this.noPropertiesMessage.Visible = true;
            this.propertiesList.Visible = false;
        }
    }

    protected void SelectedProperties_DataBound(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey(PropertyListKey))
        {
            // by default all properties should be selected
            foreach (ListItem item in this.selectedProperties.Items)
                item.Selected = true;
        }
        else
        {
            string properties = Resource.Properties[PropertyListKey];
            if (!String.IsNullOrEmpty(properties))
                foreach (string property in (properties.Split(separator)))
                {
                    ListItem item = this.selectedProperties.Items.FindByValue(property.Trim());
                    if (item != null)
                        item.Selected = true;
                }
        }

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            StringBuilder value = new StringBuilder();
            var plus = "";
            for (var i = 0; i < this.selectedProperties.Items.Count; i++)
            {
                value.AppendFormat("{1}(this.rows[{0}].children[0].children[0].checked ? this.rows[{0}].children[0].children[1].innerHTML + ',' : '')", i, plus);
                plus = "+";
            }
            selectedProperties.Attributes.Add("onclick", "javascript:SaveData('PropertyList', " + value + ");");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            List<string> selected = new List<string>();
            foreach (ListItem item in this.selectedProperties.Items)
            {
                if (item.Selected)
                {
                    selected.Add(item.Value);
                }
            }
            properties[PropertyListKey] = String.Join(separator.ToString(), selected.ToArray());

            return properties;
        }
    }

}