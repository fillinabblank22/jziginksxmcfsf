﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Common.Models;
using System.Text;

public partial class Orion_APM_Controls_EditResourceControls_EditApplicationsByCurrentStatus : BaseResourceEditControl
{

    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
	
        this.tbFilter.Text = Resource.Properties["Filter"];
    
        statesRepeater.DataSource = GetStateFilters();
        statesRepeater.DataBind();
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("SelectedStatus", GetSelectedStatus());
            properties.Add("Filter", this.tbFilter.Text);

            return properties;
        }
    }

    /// <summary>
    /// Returns the dictionary with description of all application statuses and int value
    /// (from SolarWinds.APM.Common.Models.Status enumeration).
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, int> GetAllStatuses()
    {
        Dictionary<string, int> result = new Dictionary<string, int>();

        Type dataType = Enum.GetUnderlyingType(typeof(SolarWinds.APM.Common.Models.Status));
        foreach (System.Reflection.FieldInfo field in
            typeof(SolarWinds.APM.Common.Models.Status).GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.Public))
        {
            var attributes = (System.ComponentModel.DescriptionAttribute[])field.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
            var value = field.GetValue(null);

            if (attributes.Length > 0 && value != null)
                result.Add(GetLocalizedProperty("StatusDescription", attributes[0].Description), (int)value);
        }

        return result;
    }

    private string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    protected string GetSelectedStatus()
    {
        StringBuilder selectedStatus = new StringBuilder();

        Dictionary<string, int> allStatuses = GetAllStatuses();

        foreach (RepeaterItem item in statesRepeater.Items)
        {
            CheckBox chItem = (item.FindControl("stateCheckbox") as CheckBox);
            if (null != chItem && chItem.Checked)
            {
                if (allStatuses.ContainsKey(chItem.Text))
                {
                    selectedStatus.Append(allStatuses[chItem.Text].ToString());
                    selectedStatus.Append(';');
                }
            }
        }

        return selectedStatus.ToString();
    }

    // TODO: move to BaseResourceEditControl
    private string GetProperty(string propertyName, string defaultValue)
    {
        if (this.Resource.Properties.ContainsKey(propertyName))
            return this.Resource.Properties[propertyName];

        return defaultValue;
    }

    private DataTable GetStateFilters()
    {
        DataTable table = new DataTable();
        table.Columns.Add("Name");
        table.Columns.Add("Value");

        string[] selectedStatuses = GetProperty("SelectedStatus", string.Empty).Split(';');
        if (selectedStatuses != null && selectedStatuses.Length > 0)
        {
            Dictionary<string, int> allStatuses = GetAllStatuses();
            foreach (var pair in allStatuses)
            {
                table.Rows.Add(pair.Key, selectedStatuses.Contains(pair.Value.ToString())); 
            }
        }

        return table;
    }

}
