﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationsByCurrentStatus.ascx.cs"
    Inherits="Orion_APM_Controls_EditResourceControls_EditApplicationsByCurrentStatus" %>

<table border="0">
        <tr>
            <td>
                <b><%= Resources.APMWebContent.APMWEBDATA_AK1_147 %></b>
                <br />
                <asp:Repeater runat="server" ID="statesRepeater">
                  <ItemTemplate>
                    <asp:CheckBox Font-Size="10pt" runat="server" id="stateCheckbox" Text='<%# Eval("Name")%>' 
                        Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Value")) %>'  />  
                        <br />    
                  </ItemTemplate>
                </asp:Repeater>           
            </td>
        </tr>
        <tr>
            <td>
                <br/>
                <b><%= Resources.APMWebContent.APMWEBDATA_AK1_183 %></b>
                <br/>
                <asp:TextBox runat="server" ID="tbFilter" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                <%= Resources.APMWebContent.APMWEBDATA_AK1_184 %>
            </td>
        </tr>
         <tr>
            <td style="font-size:8pt">
                <%= Resources.APMWebContent.APMWEBDATA_AK1_186 %>
                <ul>
                    <li><%= Resources.APMWebContent.APMWEBDATA_AK1_187 %></li>
                    <li><%= Resources.APMWebContent.APMWEBDATA_AK1_188 %></li>
                    <li><%= Resources.APMWebContent.APMWEBDATA_AK1_189 %></li>
                </ul>
            </td>
        </tr>
</table> 
