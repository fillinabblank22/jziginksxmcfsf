﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExtJsDropDown.ascx.cs"
    Inherits="Orion_APM_Controls_ExtJsDropDown" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<orion:IncludeExtJs ID="IncludeExtJs1" runat="server" Debug="false" Version="3.4" />
<orion:Include runat="server" File="APM/Styles/ExtJsFix.css"/>
<orion:Include runat="server" File="js/OrionCore.js"/>

<div id="tabMenu" runat="server">
</div>

<script type="text/javascript">
    CreateTabMenu<%= this.ResourceID %> = function () {
        var controlId = '<%= this.tabMenu.ClientID %>';
        var resourceId = <%= this.ResourceID %>;
        var periodString = '<%= ControlHelper.EncodeJsString(this.Period) %>';
        var queryStringExtension = '<%= ControlHelper.EncodeJsString(this.QueryStringExtension) %>';

        redirectByMenuItem = function (item) {
            ORION.callWebService("/Orion/APM/Controls/ExtJsDropDown.asmx", "GetPageUrl", { actionName: item.id, resourceId: resourceId },
            function (result) {
                window.location = result + queryStringExtension;
            });
        }

        var excelMenu = new Ext.menu.Menu({
            id: 'excelMenu' + ';' + resourceId,
            items: [
                { id: 'data' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_25)%>', handler: redirectByMenuItem },
                { id: 'excel' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.APMWEB_JS_CODE_VB1_26)%>', handler: redirectByMenuItem }
            ]
        });

        var customChartMenu = new Ext.menu.Menu({
            id: 'customChartMenu' + resourceId,
            items: [
                { id: 'Last_Hour' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_hour)%>', handler: redirectByMenuItem, stateId: 'Past Hour' },
                { id: 'Last_2_Hours' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_2_hours)%>', handler: redirectByMenuItem, stateId: 'Last 2 Hours' },
                { id: 'Last_24_Hours' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_24_hours)%>', handler: redirectByMenuItem, stateId: 'Last 24 Hours' },
                { id: 'Today' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_today)%>', handler: redirectByMenuItem, stateId: 'Today' },
                { id: 'Yesterday' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_yesterday)%>', handler: redirectByMenuItem, stateId: 'Yesterday' },
                { id: 'Last_7_Days' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_7_days)%>', handler: redirectByMenuItem, stateId: 'Last 7 Days' },
                { id: 'This_Month' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_this_months)%>', handler: redirectByMenuItem, stateId: 'This Month' },
                { id: 'Last_Month' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_month)%>', handler: redirectByMenuItem, stateId: 'Last Month' },
                { id: 'Last_30_Days' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_30_days)%>', handler: redirectByMenuItem, stateId: 'Last 30 Days' },
                { id: 'Last_3_Months' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_3_months)%>', handler: redirectByMenuItem, stateId: 'Last 3 Months' },
                { id: 'This_Year' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_this_year)%>', handler: redirectByMenuItem, stateId: 'This Year' },
                { id: 'Last_12_Months' + ';' + resourceId, text: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.TimePeriod_last_12_months)%>', handler: redirectByMenuItem, stateId: 'Last 12 Months' }
            ]
        });

        customChartMenu.items.each(function (item) {
            if (item.stateId == periodString) {
                item.icon = '/Orion/APM/Images/ExtJsDropDown/ItemChecked.png';
            }
        });

        var toolbar = new Ext.Toolbar({
            id: 'toolbar' + resourceId,
            renderTo: controlId,
            cls: 'customToolBar',
            height: 24,
            width: 78,
            items:
                [{
                    id: 'excelButton' + resourceId,
                    xtype: 'tbbutton',
                    cls: 'x-btn-icon',
                    icon: '/Orion/APM/Images/ExtJsDropDown/ExportToExcel.png',
                    menu: excelMenu
                },
                {
                    id: 'spacer' + resourceId,
                    xtype: 'tbspacer',
                    width: 10
                },
                {
                    id: 'periodsButton' + resourceId,
                    xtype: 'tbbutton',
                    cls: 'x-btn-icon',
                    icon: '/Orion/APM/Images/ExtJsDropDown/TimePeriods.png',
                    menu: customChartMenu
                }]
        });

        toolbar.doLayout();
    }

    if (typeof(RegisterStartup) == 'undefined') {
       var startupFunctions = [];

       RegisterStartup = function(functionName) {
           startupFunctions.push(functionName);
       }

       StartUp = function() {
           for (var i = 0; i < startupFunctions.length; i++) {
               eval(startupFunctions[i]);
           }
           startupFunctions = [];
       }
    }

    RegisterStartup("CreateTabMenu<%= this.ResourceID %>()");

    // I had problems with multiple listeners
    Ext.onReady(function() { StartUp(); });

</script>
