﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignComponentsFinished.ascx.cs" Inherits="Orion_APM_Controls_AssignComponentsFinished" %>
<style type="text/css">
#finishMsg 
{    
	margin-top: 20px;
	height:30px;
}
#finishResults
{
	margin-left:35px;
}
#finishResults .resultRow
{
	height:25px;
}	
#finishResults .resultRow span#appName
{
	margin-left:7px;
}
#finishResults .resultRow a
{
	color: #4779C4;
	font-size: 12px;
}
</style>
<h2><%= this.StepTitle %></h2>
<div id="finishMsg">
    <asp:Image ID="imgFewMinutes" runat="server" ImageUrl="~/Orion/images/Icon.Info.gif" style="margin-left:8px" />
    <span style="margin-left:7px"><%= Resources.APMWebContent.APMWEBDATA_VB1_26 %></span>
</div>
<div id="finishResults">
    <asp:Repeater ID="rptAppResults" runat="server" OnDataBinding="AppResults_DataBinding" OnItemDataBound="AppResults_ItemDataBound">
        <ItemTemplate>
            <div class="resultRow">
                <asp:Image ID="imgStatus" runat="server" />
                <span ID="appName"><asp:Literal ID="labAppName" runat="server" /></span>
                <asp:HyperLink ID="lnkView" runat="server"><%= Resources.APMWebContent.APMWEBDATA_VB1_27 %></asp:HyperLink>
                <span runat="server" ID="editContainer">|
                    <asp:HyperLink ID="lnkEdit" runat="server"><%= Resources.APMWebContent.APMWEBDATA_VB1_28 %></asp:HyperLink>
                </span>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
