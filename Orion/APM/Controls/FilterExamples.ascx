﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterExamples.ascx.cs"
    Inherits="Orion_APM_Controls_FilterExamples" %>
    
    
<script language="javascript" type="text/javascript">
    function CollapsePanel_Click(buttonID, panelID) {
	    var imgTag = $("#" + buttonID);
	    var collapseArea = $("#" + panelID);
	    
	    if (collapseArea.is(':hidden')) {
	        collapseArea.slideDown("slow");
	        imgTag.attr("src", "/NetPerfMon/images/Icon_Minus.gif"); 
	    }
	    else {
	        collapseArea.slideUp("slow");	    
	        imgTag.attr("src", "/NetPerfMon/images/Icon_Plus.gif");
	    }
	    return false;
    }
</script>
    
<table style="margin-left: 10px">
    <tr>
        <td>
            <a href="#" onclick="return CollapsePanel_Click('filterIcon','filterDesc');" style="font-size: 8pt">
                <img id="filterIcon" src="/NetPerfMon/images/Icon_Plus.gif" alt="Expand" />
                Show filter examples</a>
        </td>
    </tr>
    <tr>
        <td>
            <table id="filterDesc" style="display: none">
                <tr>
                    <td>
                        A few example filters are :<br />
                        <br />
                        Filter the results to only show Nodes that are not UP<br />
                        <b>Nodes.Status&lt;&gt;1</b><br />
                        <br />
                        The valid status levels are :<br />
                    </td>
                </tr>
                <tr>
                    <td>
                        0 = <b>UNKNOWN</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        1 = <b>UP</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        2 = <b>DOWN</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        3 = <b>WARNING</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        Only show Cisco devices<br />
                        <b>Nodes.Vendor = 'Cisco'</b><br />
                        <br />
                        Only show devices in Atlanta. (This assumes you have added a custom property named
                        City)<br />
                        <b>Nodes.City = 'Atlanta'</b><br />
                        <br />
                        Only show devices beginning with "AX3-"<br />
                        <b>Nodes.Caption Like 'AX3-*'</b><br />
                        <br />
                        Only show Nortel devices that are Down<br />
                        <b>Nodes.Vendor Like 'Nortel*' AND Status=2</b><br />
                        <br />
                        Only show devices ending in '-TX'
                        <br />
                        <b>Nodes.Vendor Like '*-TX'</b><br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        Filter the results to only show Monitors that are UP<br />
                        <b>MonitorStatus.Availability=1</b><br />
                        <br />
                        The valid status levels for Application and Monitor are :<br />
                    </td>
                </tr>
                <tr>
                    <td>
                        0 = <b>UNKNOWN</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        1 = <b>UP</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        2 = <b>DOWN</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <a href="#" onclick="return CollapsePanel_Click('nodeIcon', 'nodeProperties');" style="font-size: 8pt">
                            <img id="nodeIcon" src="/NetPerfMon/images/Icon_Plus.gif" alt="Expand" />
                            Show Nodes Properties</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="nodeProperties" style="display: none">
                            <tr>
                                <td>
                                    Some of the properties that can be used for Filtering Nodes are :
                                </td>
                            </tr>
                            <asp:Repeater runat="server" ID="nodeGrid" OnLoad="NodeGridLoad">
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <%# Eval("Name")%>
                                            &nbsp;-&nbsp;<%# Eval("DataType")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
<% if (this.ShowApplicationProperties) { %>                
                <tr>
                    <td>
                        <a href="#" onclick="return CollapsePanel_Click('appIcon', 'applicationProperties');" style="font-size: 8pt">
                            <img id="appIcon" src="/NetPerfMon/images/Icon_Plus.gif" alt="Expand" />
                            Show Application Monitor properties</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="applicationProperties" style="display: none">
                            <tr>
                                <td>
                                    Some of the properties that can be used for filtering Application Monitors are :
                                </td>
                            </tr>
                            <asp:Repeater runat="server" ID="applicationGrid" OnLoad="ApplicationGridLoad">
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <%# Eval("Name")%>
                                            &nbsp;-&nbsp;<%# Eval("DataType")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" onclick="return CollapsePanel_Click('appStatusIcon', 'appStatusProperties');"
                            style="font-size: 8pt">
                            <img id="appStatusIcon" src="/NetPerfMon/images/Icon_Plus.gif" alt="Expand" />
                            Show Application Monitor Status properties</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="appStatusProperties" style="display: none">
                            <tr>
                                <td>
                                    Some of the properties that can be used for filtering Application Monitor status are :
                                </td>
                            </tr>
                            <asp:Repeater runat="server" ID="applicationStatusGrid" OnLoad="ApplicationStatusGridLoad">
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <%# Eval("Name")%>
                                            &nbsp;-&nbsp;<%# Eval("DataType")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
<% } %>                
<% if (this.ShowComponentProperties) { %>                
                <tr>
                    <td>
                        <a href="#" onclick="return CollapsePanel_Click('monitorIcon', 'monitorProperties');" style="font-size: 8pt">
                            <img id="monitorIcon" src="/NetPerfMon/images/Icon_Plus.gif" alt="Expand" />
                            Show Component Monitor properties</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="monitorProperties" style="display: none">
                            <tr>
                                <td>
                                    Some of the properties that can be used for filtering Components Monitors are :
                                </td>
                            </tr>
                            <asp:Repeater runat="server" ID="monitorGrid" OnLoad="MonitorGridLoad">
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <%# Eval("Name")%>
                                            &nbsp;-&nbsp;<%# Eval("DataType")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" onclick="return CollapsePanel_Click('monStatusIcon', 'monStatusProperties');"
                            style="font-size: 8pt">
                            <img id="monStatusIcon" src="/NetPerfMon/images/Icon_Plus.gif" alt="Expand" />
                            Show Component Monitor Status properties</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="monStatusProperties" style="display: none">
                            <tr>
                                <td>
                                    Some of the properties that can be used for filtering Component Monitor status are :
                                </td>
                            </tr>
                            <asp:Repeater runat="server" ID="monitorStatusGrid" OnLoad="MonitorStatusGridLoad">
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <%# Eval("Name")%>
                                            &nbsp;-&nbsp;<%# Eval("DataType")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
<% } %>                

            </table>
        </td>
    </tr>
</table>
