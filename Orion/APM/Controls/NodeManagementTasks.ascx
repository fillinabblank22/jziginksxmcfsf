﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeManagementTasks.ascx.cs" Inherits="Orion_APM_Controls_NodeManagementTasks" %>

<Orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<Orion:Include runat="server" Module="APM" File="js/NodeManagement.js" />

<orion:InlineCss runat="server">
	.apmNodeManagementTask { height: 16px; }
</orion:InlineCss>