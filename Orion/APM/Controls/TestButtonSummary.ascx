﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestButtonSummary.ascx.cs" Inherits="Orion_APM_Controls_TestButtonSummary" %>
<div id="<%= ClientID %>" class="<%= CssClass %>" style="display: none">
    <div class="testSuccessful" style="display: none">
        <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_55 %>" />
    </div>
    <div class="testFailed" style="display: none">
        <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_56 %>" />
    </div>
    <div class="progress" style="display: none">
        <img src="/Orion/images/animated_loading_sm3_whbg.gif" />
        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources: APMWebContent, ApmWeb_TestingProgress%>" />
    </div>
</div>