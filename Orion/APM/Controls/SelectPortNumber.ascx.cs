﻿using System;
using System.ComponentModel;
using System.Web.UI;

using SolarWinds.APM.Common.Extensions.System;

public partial class Orion_APM_Controls_SelectPortNumber : UserControl
{
    [
    CssClassProperty,
    Category("Appearance"),
    DefaultValue("")
    ]
    public string CssClass
    {
        get
        {
            return portNumber.CssClass;
        }
        set
        {
            portNumber.CssClass = value;
        }
    }

    [
    Bindable(true, BindingDirection.TwoWay),
    Category("Appearance"),
    DefaultValue(""),
    ]
    public int? Value
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(portNumber.Text) && PortNumberRangeValidator.IsValid)
                return Int32.Parse(portNumber.Text);
            return null;
        }
        set
        {
            portNumber.Text = value.HasValue ? value.Value.ToInvariantString() : "";
        }
    }

    /// <summary>
    /// Exposed ClientID of contained input control, to be accessible by javascript.
    /// </summary>
    public override string ClientID
    {
        get
        {
            return portNumber.ClientID;
        }
    }
}