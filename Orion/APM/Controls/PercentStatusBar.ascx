﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PercentStatusBar.ascx.cs" Inherits="Orion_APM_Controls_PercentStatusBar" %>
<orion:Include ID="CssStyle" runat="server" File="Resources.css" />
<orion:Include ID="CssStyleControl" Module="APM" runat="server" File="Controls.css" />

<asp:Panel ID="OuterBorder" runat="server" CssClass="BarBackground" style="display: inline-block; overflow: hidden">
    <asp:Panel ID="UsedSpacePanel" runat="server" Font-Size="1px" style="float: left">
        &nbsp;
    </asp:Panel>
    <asp:Panel ID="AvailableSpacePanel" runat="server" Font-Size="1px" CssClass="GreyBar" style="overflow: hidden; background-image: url('/Orion/APM/Images/Bar.gray.gif');background-position: bottom; height: 10px; background-repeat: repeat-x;  background-color: darkgray;">
        &nbsp;
    </asp:Panel>
</asp:Panel>
