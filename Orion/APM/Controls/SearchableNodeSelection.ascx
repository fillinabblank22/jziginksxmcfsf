<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchableNodeSelection.ascx.cs" 
    Inherits="Orion_APM_Controls_SearchableNodeSelection" %>

<%@ Register Src="~/Orion/APM/Controls/AddRemoveObjects/AddRemoveObjectsControl.ascx" TagName="AddRemoveObjectsControl" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
<orion:Include runat="server" File="js/OrionCore.js"/>

<div>
<orion:AddRemoveObjectsControl ID="addRemoveObjectsControl" runat="server" />

</div>