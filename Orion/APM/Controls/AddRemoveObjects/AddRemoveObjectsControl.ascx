﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddRemoveObjectsControl.ascx.cs" 
Inherits="Orion_NPM_Controls_AddRemoveObjectsControl" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	<Scripts>
        <asp:ScriptReference Path="~/Orion/APM/Controls/AddRemoveObjects/AddRemoveObjects.js"/>
	</Scripts>
</asp:ScriptManagerProxy>

<div id="GroupItemsSelector">
	<div class="GroupSection">
	<label for="groupBySelect"><%=Resources.APMWebContent.APMWEBDATA_AK1_68 %></label>                                                   
		<select id="groupBySelect">
  		</select>
	</div>
    <div class="GroupSection">
		<label for="searchBox"><%=Resources.APMWebContent.APMWEBDATA_AK1_69 %></label>                                                  
        <table>
            <tr>
            <td><input type="text" class="searchBox" id="searchBox" name="searchBox" /></td>
            <td><a href="javascript:void(0);" id="searchButton"><img src="/Orion/images/Button.SearchIcon.gif" /></a></td>
            </tr>
        </table>
	</div>
</div>

<div id="ContainerMembersTable" style="width:99%;">
</div>

<div id="AddButtonPanel" style="padding:10px;">
    <a id="AddToGroupButton" href="javascript:void(0);" style="top: 44%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_add_32x32.gif" />
    </a>
    <a id="RemoveFromGroupButton" href="javascript:void(0);" style="top: 46%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_remove_32x32.gif" />
    </a>
</div>

<input type="hidden" id="groupByType" />
<asp:HiddenField ID="gridItems" runat="server" />

<script type="text/javascript">
    // <![CDATA[
    SW.Orion.SelectObjects.SetTitles('<%= ControlHelper.EncodeJsString(LeftPanelTitle) %>', '<%= ControlHelper.EncodeJsString(RightPanelTitle) %>');
    SW.Orion.SelectObjects.SetEntity('<%= ControlHelper.EncodeJsString(EntityName) %>');
    SW.Orion.SelectObjects.SetFilter('<%= ControlHelper.EncodeJsString(Filter) %>');
    SW.Orion.SelectObjects.SetGridItemsFieldClientID('<%= gridItems.ClientID %>');

//TODO
//    SW.Orion.SelectObjects.SetChangeHandler(<%= AfterNodeSelectionChangeScript %>);
//    
    // ]]>
</script>