﻿<%@ WebHandler Language="C#" Class="ContainerObjectsTreeProvider" %>

using System;
using System.Configuration;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

using System.Globalization;

public class ContainerObjectsTreeProvider : IHttpHandler
{
    private readonly int BatchSize = ConfigurationManager.AppSettings["APM-Web-AddRemoveObjectsTreeProviderBatchSize"] != null
                ? Convert.ToInt32(ConfigurationManager.AppSettings["APM-Web-AddRemoveObjectsTreeProviderBatchSize"])
                : 100;

    [Serializable]
    private class TreeNode
    {
        public string Uri { get; set; }
        public string FullName { get; set; }
        public string FullNameWithoutHighlight { get; set; }
        public string Entity { get; set; }
        public int Status { get; set; }
        public int EntityID
        {
            get
            {
                return Convert.ToInt32(Uri.Split('=').Last());
            }
        }


        public override string ToString()
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            str.Append("{");
            str.AppendFormat("id: '{0}',", EntityID);  // Uri.Split('=').Last()
            str.AppendFormat("text: '{0}',", string.IsNullOrEmpty(FullName) ? string.Empty : FullName.Replace("'", @"\'"));
            str.AppendFormat("entity: '{0}',", Entity);
            str.AppendFormat("status: '{0}',", Status);
            str.AppendFormat("fullName: '{0}',", string.IsNullOrEmpty(FullNameWithoutHighlight) ? string.Empty : FullNameWithoutHighlight.Replace("'", @"\'"));
            str.Append("checked: false,");
            str.Append("propagateCheck: true,");
            str.AppendFormat("icon: '/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small',", Entity, Status);
            str.Append("listeners: { 'checkchange': function (node, checked) {");
            str.Append("if (checked) {");
            str.Append("   node.getUI().addClass('x-tree-selected');");
            str.Append("} else {");
            str.Append("  node.getUI().removeClass('x-tree-selected');");
            str.Append("  if ((node.parentNode) && (node.attributes.propagateCheck)) {");
            str.Append("    node.parentNode.attributes.propagateCheck = false;");
            str.Append("    node.parentNode.getUI().removeClass('x-tree-selected');");
            str.Append("    node.parentNode.getUI().toggleCheck(false);");
            str.Append("    node.parentNode.attributes.propagateCheck = true;");
            str.Append("  }");
            str.Append("} } },");
            str.Append("expandable: false,");
            str.Append("leaf: true");
            str.Append("}");
            return str.ToString();
        }
    }

    private string GenerateFullName(IList<string> names)
    {
        if (names == null)
            throw new ArgumentNullException("names");

        return HttpUtility.HtmlEncode(string.Join(" on ", names.ToArray()));
    }

    private string GetWhere(string property, string propertyType, string propertyValue)
    {
        if (String.IsNullOrEmpty(propertyValue))
        {
            var where = new StringBuilder(" ( ");
            if (propertyType.Equals("System.String"))
            {
                where.AppendFormat("e.{0} = '' OR", property);
            }

            where.AppendFormat(" e.{0} IS NULL ) ", property);
            return where.ToString();
        }

        switch (propertyType)
        {
            case "System.String":
            case "System.Boolean":
            case "System.Char":
                return String.Format(" e.{0} = '{1}' ", property, propertyValue.Replace("'", "''"));

            case "System.DateTime":
                DateTime date;
                if (DateTime.TryParse(propertyValue, CultureInfo.CurrentCulture, DateTimeStyles.None, out date) || DateTime.TryParse(propertyValue, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return String.Format(" e.{0} = DateTime('{1:o}') ", property, date);
                }
                return String.Empty;

            case "System.Double":
            case "System.Decimal":
            case "System.Single":
                double d;
                if (Double.TryParse(propertyValue, NumberStyles.Float, CultureInfo.CurrentCulture, out d) || Double.TryParse(propertyValue, NumberStyles.Float, CultureInfo.InvariantCulture, out d))
                {
                    return String.Format(" e.{0} = '{1}' ", property, d.ToString(CultureInfo.InvariantCulture));
                }
                return String.Empty;

            default:
                return String.Format(" e.{0} = {1} ", property, propertyValue);
        }
    }


    private IEnumerable<TreeNode> GetEntities(string entityType, string property, string propertyType, string propertyValue, string searchValue, string excludeDefinitions, string filter, string viewId, bool partialLoading, string startFromString)
    {
        int? viewLimitationId = GetLimitationIdFromViewIdString(viewId);
        var results = new List<TreeNode>();

        var query = new System.Text.StringBuilder();
        query.AppendFormat(@"SELECT e.DisplayName,
                                    e.Uri,
                                    e.Status,
                                    e.AncestorDisplayNames
                            FROM {0} e ", entityType);

        var whereQuery = new List<String>();

        if (!String.IsNullOrEmpty(property) && !String.IsNullOrEmpty(propertyType))
        {
            property = property.Replace(entityType + ".", string.Empty);
            whereQuery.Add(GetWhere(property, propertyType, propertyValue));
        }

        if (!String.IsNullOrEmpty(filter))
        {
            whereQuery.Add(String.Format(" {0} ", filter));
        }

        if (!String.IsNullOrEmpty(searchValue))
        {
            switch (property)
            {
                case "IPAddress":
                    whereQuery.Add(String.Format(" e.IPAddress LIKE '%{0}%' ", searchValue.Replace("'", "''")));
                    break;
                default:
                    whereQuery.Add(String.Format(" e.DisplayName LIKE '%{0}%' ", searchValue.Replace("'", "''")));
                    break;
            }
        }

        if (!String.IsNullOrEmpty(excludeDefinitions))
        {
            whereQuery.Add(String.Format(" e.{0} NOT IN ({1}) ", NetObjectTypeToSWISEntityMapper.GetIdColumn(entityType), excludeDefinitions));
        }

        if (whereQuery.Count > 0)
        {
            query.Append(" WHERE ");
            query.Append(string.Join(" AND ", whereQuery.ToArray()));
        }
           
        query.Append(" ORDER BY e.DisplayName ");
        
        if (partialLoading)
        {
            var rowsFrom = (startFromString != null ? Convert.ToInt32(startFromString) : 0) + 1;
            var rowsTo = rowsFrom + BatchSize;
            query.AppendFormat(" WITH ROWS {0} TO {1} ", rowsFrom, rowsTo);
        }
        
        query.Append(viewLimitationId != null ? String.Format(" WITH LIMITATION {0}", viewLimitationId) : String.Empty);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.Query(query.ToString());

            foreach (DataRow row in table.Rows)
            {
                results.Add(new TreeNode()
                {
                    Entity = entityType,
                    Uri = row["Uri"].ToString(),
					Status = ToInt(row["Status"]),
                    FullName = GenerateFullName((string[])row["AncestorDisplayNames"]),
                    FullNameWithoutHighlight = GenerateFullName((string[])row["AncestorDisplayNames"])
                });
            }
        }
        return results;
    }
    
    private IEnumerable<TreeNode> GetEntitiesPaged(string entityType, string property, string propertyType, string propertyValue, string searchValue, string excludeDefinitions, string filter, string viewId, string startRowIndex, out int endRowIndex, out bool moreData)
    {
        int? viewLimitationId = GetLimitationIdFromViewIdString(viewId);
        var results = new List<TreeNode>();

        var query = new System.Text.StringBuilder();
        query.AppendFormat(@"SELECT e.DisplayName,
                                    e.Uri,
                                    e.Status,
                                    e.AncestorDisplayNames
                            FROM {0} e ", entityType);

        var whereQuery = new List<String>();

        if (!String.IsNullOrEmpty(property) && !String.IsNullOrEmpty(propertyType))
        {
            property = property.Replace(entityType + ".", string.Empty);
            whereQuery.Add(GetWhere(property, propertyType, propertyValue));
        }

        if (!String.IsNullOrEmpty(filter))
        {
            whereQuery.Add(String.Format(" {0} ", filter));
        }

        if (!String.IsNullOrEmpty(searchValue))
        {
            whereQuery.Add(String.Format(" e.DisplayName LIKE '%{0}%' ", searchValue.Replace("'", "''")));
        }

        if (!String.IsNullOrEmpty(excludeDefinitions))
        {
            whereQuery.Add(String.Format(" e.{0} NOT IN ({1}) ", NetObjectTypeToSWISEntityMapper.GetIdColumn(entityType), excludeDefinitions));
        }

        if (whereQuery.Count > 0)
        {
            query.Append(" WHERE ");
            query.Append(string.Join(" AND ", whereQuery.ToArray()));
        }
           
        query.Append(" ORDER BY e.DisplayName ");
        
        var rowsFrom = (startRowIndex != null ? Convert.ToInt32(startRowIndex) : 0);
        endRowIndex = rowsFrom;
        query.AppendFormat(" WITH ROWS {0} TO {1} ", rowsFrom + 1, rowsFrom + BatchSize);
        
        query.Append(viewLimitationId != null ? String.Format(" WITH LIMITATION {0}", viewLimitationId) : String.Empty);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.Query(query.ToString());

            foreach (DataRow row in table.Rows)
            {
				results.Add(new TreeNode()
                {
                    Entity = entityType,
                    Uri = row["Uri"].ToString(),
					Status = ToInt(row["Status"]),
                    FullName = GenerateFullName((string[])row["AncestorDisplayNames"]),
                    FullNameWithoutHighlight = GenerateFullName((string[])row["AncestorDisplayNames"])
                });
            }
            endRowIndex += table.Rows.Count - 1;
            moreData = table.Rows.Count == BatchSize;
        }
        return results;
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        context.Server.ScriptTimeout = 180;

        String entityType = context.Request.Params["entityType"];
        String property = context.Request.Params["groupBy"];
        String propertyType = context.Request.Params["groupByType"];
        String propertyValue = context.Request.Params["value"];
        String searchValue = context.Request.Params["searchValue"];
        String excludeDefinitions = context.Request.Params["excludeDefinitions"];
        String filter = context.Request.Params["filter"];
        String viewId = context.Request.Params["viewId"];
        bool partialLoading = Convert.ToBoolean(context.Request.Params["partialLoading"]);
        bool pagedLoading = Convert.ToBoolean(context.Request.Params["pagedLoading"]);
        String startFromString = context.Request.Params["startFrom"];
        String startRowIndex = context.Request.Params["startRow"];
        int endRowIndex = 0;
        bool moreData = false;

        if (propertyValue == "[Unknown]")
            propertyValue = String.Empty;

        IEnumerable<TreeNode> results;
        if (!pagedLoading)
            results = GetEntities(entityType, property, propertyType, propertyValue, searchValue, excludeDefinitions, filter, viewId, partialLoading, startFromString);
        else
            results = GetEntitiesPaged(entityType, property, propertyType, propertyValue, searchValue, excludeDefinitions, filter, viewId, startRowIndex, out endRowIndex, out moreData);

        foreach (TreeNode entity in results)
        {
            if (!String.IsNullOrEmpty(searchValue))
            {
                // highlight search text
                entity.FullName = Regex.Replace(
                        entity.FullName,
                        String.Format("({0})", Regex.Escape(HttpUtility.HtmlEncode(searchValue))),
                        "<span class=\"searchHighlight\">$1</span>",
                        RegexOptions.IgnoreCase | RegexOptions.Compiled);
            }
        }

        var stringArray = string.Join(",", results.Select(x => x.ToString()));
        if (pagedLoading)
        {
            context.Response.Write(String.Format("{{endRow: {0}, moreData: {1}, nodes: [{2}]}}", endRowIndex,
                                                 moreData.ToString(CultureInfo.InvariantCulture).ToLower(), stringArray));
        }
        else
        {
            context.Response.Write(String.Format("[{0}]", stringArray));
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #region Helpers creepers

	private int ToInt(object rawValue) 
	{
		var value = 0;
		try
		{
			value = Convert.ToInt32(rawValue, CultureInfo.InvariantCulture);
		}
		catch { }
		return value;
	}
	
    private int? GetLimitationIdFromViewIdString(string viewId)
    {
        int? viewLimitationId = null;
        if (!string.IsNullOrEmpty(viewId))
        {
            int viewIdInt;
            if (Int32.TryParse(viewId, out viewIdInt))
            {
                try
                {
                    viewLimitationId = SolarWinds.Orion.Web.ViewManager.GetViewById(viewIdInt).LimitationID;
                }
                catch
                {
                    return null;
                }
            }
        }

        return viewLimitationId;
    }

    #endregion
}
