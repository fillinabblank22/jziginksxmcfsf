﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Models;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Controls_AssignComponentsFinished : System.Web.UI.UserControl
{
    private bool enableEditLink = true;

    public bool EnableEditLink
    {
        get
        {
            return this.enableEditLink;
        }
        set
        {
            this.enableEditLink = value;
        }
    }

    public string NetObjectType { get; set; }

    public List<AppInfo> DataSource { get; set; }

    protected string StepTitle
    {
        get
        {
            var createdApplications = this.DataSource;
            if (createdApplications != null)
            {
                int appCnt = createdApplications.Count;
                return appCnt == 1
                           ? Resources.APMWebContent.APMWEBCODE_VB1_1
                           : string.Format(Resources.APMWebContent.APMWEBCODE_VB1_2, appCnt);
            }
            return string.Format(Resources.APMWebContent.APMWEBCODE_VB1_2, 0);
        }
    }

    public Orion_APM_Controls_AssignComponentsFinished()
    {
        NetObjectType = APMNetObjectType.Application;
    }

    protected void AppResults_DataBinding(object sender, EventArgs e)
    {
        rptAppResults.DataSource = DataSource;
    }

    protected void AppResults_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            var appInfo = (AppInfo)item.DataItem;
            var stat = new ApmStatus(appInfo.Status);
            var imgStatus = (Image)item.FindControl("imgStatus");
            if (imgStatus != null) 
                imgStatus.ImageUrl = stat.ToString("smallappimgpath", null);

            var labAppName = (Literal)item.FindControl("labAppName");
            if (labAppName != null)
                labAppName.Text = InvariantString.Format(Resources.APMWebContent.APMWEBCODE_VB1_3, HttpUtility.HtmlEncode(appInfo.Name), HttpUtility.HtmlEncode(appInfo.NodeName));

            var lnkView = (HyperLink)item.FindControl("lnkView");
            if (lnkView != null)
            {
                lnkView.NavigateUrl = BaseResourceControl.GetViewLink(InvariantString.Format("{0}:{1}", NetObjectType, appInfo.Id));
            }
            

            if (EnableEditLink)
            {
                var lnkEdit = (HyperLink)item.FindControl("lnkEdit");
                if (lnkEdit != null) 
                    lnkEdit.NavigateUrl = ApmMasterPage.GetEditApplicationPageUrl(appInfo.Id);
            }
            else
            {
                var editContainer = item.FindControl("editContainer");
                if (editContainer != null) 
                    editContainer.Visible = false;
            }
        }
    }

}