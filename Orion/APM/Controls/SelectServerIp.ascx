﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectServerIp.ascx.cs" Inherits="Orion_APM_Controls_SelectServerIp" EnableViewState="false" %>
<%@ Register TagPrefix="atk" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50927.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="apm" TagName="SelectNodeTree" Src="~/Orion/APM/Controls/SelectNodeTree.ascx" %>
<asp:TextBox runat="server" ID="targetServer"></asp:TextBox>
<asp:HiddenField runat="server" ID="selectedNodeId" Value="0"/>
<asp:HiddenField runat="server" ID="selectedNodeAddress"/>
<asp:RequiredFieldValidator ID="TargetServerFieldValidator" runat="server" ControlToValidate="targetServer"
ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_121 %>">*</asp:RequiredFieldValidator>
<asp:CustomValidator ID="TargetServerNodeExistsValidator" runat="server" ControlToValidate="targetServer"
ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_122 %>" OnServerValidate="ValidateTargetServer">*</asp:CustomValidator>
<asp:CustomValidator ID="TargetServerVmwareEntityValidator" runat="server" ControlToValidate="targetServer"
ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_123 %>" OnServerValidate="ValidateVmwareEntity">*</asp:CustomValidator>
<orion:LocalizableButton runat="server" ID="browseForServer" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_118 %>" DisplayType="Small" CausesValidation="false" />
<atk:ModalPopupExtender ID="SelectNodeModalPopupExtender" PopupControlID="SelectNodeDiv"
    TargetControlID="browseForServer" runat="server" BackgroundCssClass="apm_modalBackground"
    BehaviorID="SelectNodeModalPopupDialog" 
    CancelControlID="SelectNodePopUpCancel"
    OkControlID="SelectNodePopUpOk" OnOkScript="OnOkPopup()">
</atk:ModalPopupExtender>
<div runat="server" id="SelectNodeDiv" style="background-color: #FFF; border: solid 1px #333;z-index:110000; width:500px; height:400px; padding:20px;display:none;">
    <asp:UpdatePanel ID="_popupUpdatePanel" runat="server" UpdateMode="conditional">
        <ContentTemplate>                 
            <apm:SelectNodeTree ID="SelectNodeTree" runat="server" Visible="true" 
                                TreeHeight="300px" TreeWidth="100%" 
                                AllowMultipleSelections="false" 
                                AfterNodeSelectionChangeScript="OnAfterNodeSelectionChange"/>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="SelectNodePopUpOk" runat="server" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_124 %>" DisplayType="Primary" CausesValidation="false" />
        <orion:LocalizableButton ID="SelectNodePopUpCancel" runat="server" LocalizedText="Cancel" CausesValidation="false" />
    </div>

</div>
<script type="text/javascript">

    var selectTargetNode_SelectedIpAddress = '';
    var selectTargetNode_Id = 0;

function OnOkPopup() {
    var targetServer = $get("<%=targetServer.ClientID%>");
    if (targetServer)
        targetServer.value = selectTargetNode_SelectedIpAddress;

    var selectedNodeAddressField = $get("<%=this.selectedNodeAddress.ClientID%>");
    if (selectedNodeAddressField)
        selectedNodeAddressField.value = selectTargetNode_SelectedIpAddress;

    var selectedNodeIdField = $get("<%=this.selectedNodeId.ClientID%>");
    if (selectedNodeIdField)
        selectedNodeIdField.value = selectTargetNode_Id;
}

function OnAfterNodeSelectionChange(nodeId, nodeCaption, nodeIpAddress) {
    selectTargetNode_SelectedIpAddress = nodeIpAddress;
    selectTargetNode_Id = nodeId;

    if (selectTargetNode_SelectedIpAddress.length > 0) {
        var okButton = $get("<%=SelectNodePopUpOk.ClientID%>");
        if (okButton)
            okButton.disabled = false;
    }
}

</script>