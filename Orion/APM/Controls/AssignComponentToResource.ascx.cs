﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Models.Charting;
using System.Collections;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_APM_Controls_AssignComponentToResource : System.Web.UI.UserControl
{
    #region Properties

    private ResourceInfo resource;

    private Int32 AppID
    {
        get
        {
            Int32 id;
            if (Int32.TryParse(Request.QueryStringOrForm("AppID"), out id))
            {
                return id;
            }
            var netObj = Request.QueryStringOrForm("NetObject");

            if (resource != null && NetObjectManager.IsCustomObjectResource(resource))
            {
                netObj = resource.Properties["netobjectid"];
            }

            if (NetObjectHelper.IsApplication(netObj))
            {
                return (Int32)NetObjectHelper.GetId(netObj);
            }
            return Int32.MinValue;
        }
    }

    ListItem selItem;
    public ListItem SelectedComponent
    {
        get
        {
            if (ctrComponents.SelectedItem != null)
            {
                return ctrComponents.SelectedItem;
            }
            if (selItem == null)
            {
                var id = Request.QueryStringOrForm(SolarWinds.APM.Web.ChartingCoreBased.ModuleConstants.QueryStringParameters.ComponentId);
                if (id.IsValid())
                {
                    using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                    {
                        var cmp = bl.GetComponent(Int64.Parse(id));

                        selItem = new ListItem(cmp.Name, cmp.Id.ToString());
                    }
                }
            }
            return selItem;
        }
    }

    public string dataSourceType;

    #endregion

    #region Event Handlers

    public event EventHandler OnSelectedNameChanged;

    protected void CtrlComponents_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.OnSelectedNameChanged != null)
        {
            OnSelectedNameChanged(this, EventArgs.Empty);
        }
    }

    #endregion

    #region Init/Update Data Members
    public void PopulateControl(ResourceInfo res)
    {
        PopulateControl(res, string.Empty);
    }

    public void PopulateControl(ResourceInfo res, string selectedText)
    {
        resource = res;

        this.SetVisible(res);

        if (Visible)
        {
            var props = res.Properties;
            var cmps = new List<Component>();

            using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                var app = bl.GetApplication(AppID);

                foreach (Component component in app.Components)
                {
                    if (IsComponentValid(res, component))
                    {
                        cmps.Add(component);
                    }
                }                 
            }
            
            if (cmps.Count > 0)
			{
                ctrComponents.DataSource = cmps;
                ctrComponents.DataBind();

			    if (string.IsNullOrEmpty(selectedText))
			    {
			        var info = default(AssignedToResourceInfo);
			        AssignedToResourceInfo.TryCreate(Request, res, out info);

			        if (info[AssignedToResourceInfo.CmpID] != null &&
			            cmps.Exists(item => item.Id.ToString().Equals(info[AssignedToResourceInfo.CmpID])))
			        {
			            HtmlHelper.SetListSelectedValue(ctrComponents, info[AssignedToResourceInfo.CmpID].ToString(), null);
			        }
			        else
			        {
			            HtmlHelper.SetListSelectedText(ctrComponents, props[ApmMonitor.PropComponentName]);
			        }
			    }
			    else
			    {
                    HtmlHelper.SetListSelectedText(ctrComponents, selectedText);
                }

			    this.lblNoComponent.Visible = false;
                this.pnlSelectComponent.Visible = true;
            }
            else
            {
                this.lblNoComponent.Visible = true;
                this.pnlSelectComponent.Visible = false; 
            }                        
        }
    }

    private bool IsComponentValid(ResourceInfo res, Component component)
    {
        var result = false;
        var chartType = ChartType.None;

        if(Enum.TryParse(res.Properties["NetObjectPrefix"], true, out chartType))
        {
            switch (chartType)
            {
                    case ChartType.AAAA:
                    case ChartType.AAAM:
                    {
                        result = true;
                        break;
                    }
                    case ChartType.AASD:
                    {
                        if (component.IsDynamicBased || 
                            (component.IsPortBased && component.HasStatisticData))
                            result = true;

                        break;
                    }
                    case ChartType.AACPU:
                    case ChartType.AAMU:
                    {
                        if (component.IsProcessBased)
                            result = true;

                        break;
                    }
                    case ChartType.AAVMU:
                    {
                        if (component.IsProcessBased && component.HasVirtualMemorySupport)
                            result = true;

                        break;
                    }
                    case ChartType.AART:
                    {
                        if (component.IsPortBased && component.HasResponseTimeSupport)
                            result = true;

                        break;
                    }
                    case ChartType.AACB:
                    case ChartType.AACA:
                    case ChartType.AACL:
                    {
                        if ((component.IsPortBased && component.HasResponseTimeSupport && dataSourceType.Equals(DataSourceTypes.ResponseTime)) ||
                            (dataSourceType.Equals(DataSourceTypes.Availability)) ||
                            ((component.IsDynamicBased || (component.IsPortBased && component.HasStatisticData)) && dataSourceType.Equals(DataSourceTypes.StatisticData)))
                            result = true;

                        break;
                    }
                case ChartType.AAIOROPS:
                case ChartType.AAIOWOPS:
                case ChartType.AAIOTOPS:
                    {
                        if (component.IsProcessBased && component.HasVirtualMemorySupport)
                        {
                            result = true;
                        }
                        break;
                    }
				case ChartType.AAMS:
					{
						if (component.IsDynamicBased)
							result = true;
						break;
					}
            }
        }
        return result;
    }

    public void PopulateResource(ResourceInfo res)
    {
        this.SetVisible(res);

        if (Visible)
        {
            String name = null, id = null;
            if (ctrComponents.SelectedItem != null)
            {
                id = ctrComponents.SelectedItem.Value;
                name = ctrComponents.SelectedItem.Text;
            }
            AssignedToResourceInfo.Update(Request, res, new[] { new DictionaryEntry(AssignedToResourceInfo.CmpID, id) });
            res.Properties[ApmMonitor.PropComponentName] = name;
        }
    }

    public void SelectComponent(string selectedValue)
    {
        HtmlHelper.SetListSelectedText(ctrComponents, selectedValue);
    }

    private void SetVisible(ResourceInfo res)
    {
        var netObj = Request.QueryStringOrForm("NetObject");

        if (NetObjectManager.IsCustomObjectResource(res))
        {
            netObj = res.Properties["embeddedobjectresource"];
            if (netObj.IsValid() && netObj.Contains("ApplicationAvailabilityGraph.ascx", StringComparison.InvariantCultureIgnoreCase))
            {
                Visible = false;
            }
            if (Visible)
            {
                netObj = UriHelper.GetNetObjectId(res.Properties["objecturi"]);
            }
        }
        else
        {
            Visible &= AppID > 0;
        }

        if (Visible)
        {
            if (netObj.IsNotValid() || NetObjectHelper.IsComponent(netObj))
            {
                Visible = false;
            }
        }
    }

    #endregion
}
