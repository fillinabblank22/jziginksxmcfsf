<%@ Control Language="C#" ClassName="SmallApmItemStatusIcon" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>

<script runat="server">
    public ApmStatus StatusValue
    {
        get
        {
            if (null == ViewState["StatusValue"])
            {
                return new ApmStatus();
            }

            return (ApmStatus)ViewState["StatusValue"];
        }

        set
        {
            ViewState["StatusValue"] = value;
        }
    }

    public string CustomApplicationType
    {
        get
        {
            if (null == ViewState["CustomApplicationType"])
            {
                return null;
            }

            return (string)ViewState["CustomApplicationType"];
        }

        set
        {
            ViewState["CustomApplicationType"] = value;
        }
    }

    public string ApplicationItemType
    {
        get
        {
            if (null == ViewState["ApplicationItemType"])
            {
                return null;
            }

            return (string)ViewState["ApplicationItemType"];
        }

        set
        {
            ViewState["ApplicationItemType"] = value;
        }
    }

    protected string ImagePath
    {
        get
        {
            return this.StatusValue.ToString("smallappitemimgpath", CustomApplicationType, ApplicationItemType);
        }
    }
</script>

<img class="apm_StatusIcon" alt="<%= StatusValue.ToLocalizedString() %>" src="<%= ImagePath %>" />