﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ComponentErrorStatusControl.ascx.cs" Inherits="Orion.APM.Controls.Orion_APM_Controls_ComponentErrorStatusControl" %>
<%@ Register TagPrefix="apm" Namespace="SolarWinds.APM.Web.Controls" Assembly="SolarWinds.APM.Web" %>
<%@ Register TagPrefix="apm" TagName="SmallApmStatusIcon" Src="~/Orion/APM/Controls/SmallApmStatusIcon.ascx" %>
<asp:PlaceHolder ID="phStatuses" runat="server">
<apm:ComponentErrorStatusRepeater ID="monitorRepeater" runat="server">
    <HeaderTemplate>
        <table class="DataGrid apm_ComponentList apm_NeedsZebraStripes biggerPadding">
            <thead>
                <tr>
                    <th colspan="2" class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_277%></th>
                    <th class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_215%></th>
                    <th class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_304 %></th>
                </tr>
            </thead>
    </HeaderTemplate>
    <ItemTemplate>
            <tbody class="zebraRow">
                <tr>
                    <td class="icon">
                        <apm:SmallApmStatusIcon ID="MonStatusIcon" runat="server"
                            StatusValue="<%# Container.TypedItem.Status %>"
                            ErrorValue="<%# Container.TypedItem.Error %>" />
                    </td>
                    <td><%# GetViewLinkHtml(Container.TypedItem.ComponentID, Container.TypedItem.Name) %></td>
                    <td><%# Container.TypedItem.Type %></td>
                    <td><%# Container.TypedItem.Status.ToLocalizedString() %></td>
                </tr>
                <tr class="detail" runat="server" Visible="<%# Container.TypedItem.Error.ShowStatusDetails %>">
                    <td colspan="4"><%# Container.TypedItem.Error.GetStatusDetailsInline() %></td>
                </tr>
            </tbody>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</apm:ComponentErrorStatusRepeater>
</asp:PlaceHolder>
