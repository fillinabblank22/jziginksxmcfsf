﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BreadcrumbBar.ascx.cs" Inherits="Orion_APM_Controls_BreadcrumbBar" %>

<div class="breadcrumbs">
	<orion:FileBasedDropDownMapPath ID="ctrSiteMapPath" Provider="AdminSitemapProvider" OnInit="OnSiteMapPath_OnInit" runat="server">
		<RootNodeTemplate>
			<a href="<%# Eval("url") %>"><u> <%# Eval("title") %></u> </a>
		</RootNodeTemplate>
		<CurrentNodeTemplate>
			<a href="<%# Eval("url") %>"> <%# Eval("title") %> </a>
		</CurrentNodeTemplate>
	</orion:FileBasedDropDownMapPath>
</div>