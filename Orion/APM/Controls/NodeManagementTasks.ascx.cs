﻿using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.DisplayStrategies;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

using Node = SolarWinds.Orion.NPM.Web.Node;

public partial class Orion_APM_Controls_NodeManagementTasks : UserControl, IManagementTasksPlugin 
{
    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public const string RebootIconPng = "RebootIcon_16x16.png";
    public const string RtpeIconPng = "RT_process_explorer.png";
    public const string ScmIconPng = "RT_service_manager.png";
    public const string RtevIconPng = "RT_event_viewer.png";

    private const string CreatingManagementTaskLogMessageTemplate = "Creating management task '{0}' in section '{1}' for node {2}";

    private const string NodeManagementTaskImgSrcTemplate = "<img src='/Orion/APM/Images/{0}' class='apmNodeManagementTask' />&nbsp;{1}";

    protected void Page_Load(object sender, EventArgs e)
    {
        ApmBaseResource.EnsureApmScripts();
    }

    public IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
		var node = netObject as Node;
		if (node == null)
		{
			var netobject = netObject.NetObjectID;
			if (NetObjectFactory.ConvertNetObject(ref netobject, new[] { "N" }))
			{
				if (!String.IsNullOrEmpty(netobject))
				{
					node = NetObjectFactory.Create(netobject) as Node;
				}
			}
		}

		if ((node == null) && (netObject is ApmApplicationBase))
			node = (netObject as ApmApplicationBase).NPMNode;

		if (node == null)
			yield break;

        var dispStrategy = CreateDisplayStrategy(node);

        var nodeSectionName = Resources.CoreWebContent.WEBDATA_AK0_171;
        if (ApmRoleAccessor.AllowRealTimeProcessExplorer && dispStrategy.IsProcessExplorerSupported())
        {
            log.DebugFormat(CreatingManagementTaskLogMessageTemplate, "RTPE", nodeSectionName, node.Name);
            var jsOnClick = InvariantString.Format("return SW.APM.NodeManagement.openRealTimeProcessWindow({0});", node.NodeID);
            yield return new ManagementTaskItem
                             {
                                 Section = nodeSectionName,
                                 LinkInnerHtml = InvariantString.Format(NodeManagementTaskImgSrcTemplate, RtpeIconPng, Resources.APMWebContent.APMWEBDATA_VB1_298),
                                 OnClick = jsOnClick
                             };
        }
        if (ApmRoleAccessor.AllowRealTimeServiceExplorer && dispStrategy.IsServiceManagerSupported())
        {
            log.DebugFormat(CreatingManagementTaskLogMessageTemplate, "SCM", nodeSectionName, node.Name);
            var jsOnClick = InvariantString.Format("return SW.APM.NodeManagement.openRealTimeServicesWindow({0},{1});", node.NodeID, dispStrategy.GetWorkingWindowsCredentialId());
            yield return new ManagementTaskItem
                             {
                                 Section = nodeSectionName,
                                 LinkInnerHtml = InvariantString.Format(NodeManagementTaskImgSrcTemplate, ScmIconPng, Resources.APMWebContent.APMWEBDATA_RM0_7),
                                 OnClick = jsOnClick
                             };
        }
        if (ApmRoleAccessor.AllowRealTimeEventExplorer && dispStrategy.IsEventExplorerSupported())
        {
            log.DebugFormat(CreatingManagementTaskLogMessageTemplate, "RTEV", nodeSectionName, node.Name);
            var jsOnClick = InvariantString.Format("return SW.APM.NodeManagement.openRealTimeEventExplorerWindow({0},{1});", node.NodeID, dispStrategy.GetWorkingWindowsCredentialId());
            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                LinkInnerHtml = InvariantString.Format(NodeManagementTaskImgSrcTemplate, RtevIconPng, Resources.APMWebContent.APMWEBDATA_VB1_343),
                OnClick = jsOnClick
            };
        }
        if (ApmRoleAccessor.AllowNodeReboot && dispStrategy.IsRebootSupported())
        {
            IHostHelper hosthelper = new HostHelper(); 
            log.DebugFormat(CreatingManagementTaskLogMessageTemplate, "Reboot", nodeSectionName, node.Name);
            var jsOnClick = InvariantString.Format("return SW.APM.NodeManagement.nodeRebootClick({0}, '{1}', {2}, {3})", 
                node.NodeID, 
                node.Name, 
                SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower(),
                hosthelper.IsLocal(node.IPAddressString).ToString().ToLower());
            yield return new ManagementTaskItem
                             {
                                 Section = nodeSectionName,
                                 LinkInnerHtml = InvariantString.Format(NodeManagementTaskImgSrcTemplate, RebootIconPng, Resources.APMWebContent.Management_Reboot),
                                 OnClick = jsOnClick
                             };
        }   
    }

    private INodeManagementTasksDisplayStrategy CreateDisplayStrategy(Node node)
    {
        var tasksDisplayStrategyFactory = ServiceLocatorForWeb.GetServiceForWeb<ITasksDisplayStrategyFactory>();
        return tasksDisplayStrategyFactory.CreateTasksDisplayStrategy(node);
    }
}
