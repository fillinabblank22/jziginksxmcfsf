﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectPortNumber.ascx.cs" Inherits="Orion_APM_Controls_SelectPortNumber" %>
<asp:TextBox runat="server" ID="portNumber"></asp:TextBox>
<asp:RangeValidator runat="server" ID="PortNumberRangeValidator" ControlToValidate="portNumber"
    ErrorMessage="<%$ Resources: APMWebContent, ApmWeb_JEL_PortNumberRange %>"
    Type="Integer" MinimumValue="0" MaximumValue="65536">*</asp:RangeValidator>
