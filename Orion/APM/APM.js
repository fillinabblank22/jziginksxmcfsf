/*jslint browser: true, continue: true, plusplus: true, unparam: true, indent: 4, */
/** @namespace APMjs */
(function init_ApmJs_Global(global) {
    'use strict';

    var gdoc, gwin, gcon, gnames;

    gnames = {
        jquery: 'jQuery',
        extjs: 'Ext',
        sys: 'Sys'
    };

    // APM internal "globals"

    function noop() {
        return undefined;
    }

    /**
     * @description Determine if value is defined
     * @param v The value to test.
     * @returns {boolean}
     */
    function isDef(v) {
        return (v !== undefined);
    }

    function isSet(v) {
        return (v !== undefined) && (v !== null);
    }

    function isStr(v) {
        return typeof v === 'string';
    }

    function isNum(v) {
        return typeof v === 'number';
    }

    function isBool(v) {
        return typeof v === 'boolean';
    }

    function isObj(o) {
        return isSet(o) && (typeof o === 'object' || typeof o === 'function');
    }

    function isFn(v) {
        return typeof v === 'function';
    }

    function isArr(v) {
        return v instanceof Array;
    }

    function isLikeArr(v) {
        return isArr(v) || (isObj(v) && isNum(v.length));
    }


    function assertObj(o) {
        if (!isObj(o)) {
            throw new TypeError('not an object');
        }
    }

    function assertFn(o) {
        if (!isFn(o)) {
            throw new TypeError('not a function');
        }
    }


    function getType(v) {
        if (v === undefined) {
            return 'undefined';
        }
        if (v === null) {
            return 'null';
        }
        var t = typeof v;
        return (t === 'object') ? v.constructor.name : t;
    }


    function constFn(func) {
        assertFn(func);
        if (Object.isFrozen(func)) {
            return func;
        }
        func.prototype = null;
        return Object.freeze(func);
    }

    function seal(o) {
        assertObj(o);
        Object.keys(o).forEach(function (key) {
            Object.defineProperty(o, key, {
                writable: false,
                configurable: false
            });
        });
        return o;
    }


    function hasProp(o, name) {
        return isSet(o) && Object.prototype.hasOwnProperty.call(o, name);
    }

    function getProp(o, name) {
        return hasProp(o, name) && o[name];
    }

    function extend(target, source) {
        target = target || {};
        Object.keys(source).forEach(function (key) {
            target[key] = source[key];
        });
        return target;
    }


    // safer eval

    function execute(body) {
        if (!isStr(body) || !body.length) {
            return;
        }
        if (!/^\s*["']use strict["'];/.test(body)) {
            body = '"use strict"; ' + body;
        }
        var fnExecute = new Function(body);
        return fnExecute();
    }


    function launch(fn, delay) {
        if (!isSet(fn)) {
            return;
        }
        if (!isFn(fn)) {
            gcon.error('argument error: fn is not a function');
            return;
        }
        global.setTimeout(fn, isNum(delay) ? delay : 0);
    }


    // TODO: not really proud of these 'launch' dependency hacks, but we don't use any AMD (like RequireJS) for dependencies yet
    // could be solved by ensuring correct order in backend
    function launchRetry(opts) {
        if (!isObj(opts)) {
            throw 'TypeError: opts';
        }
        if (!isFn(opts.test)) {
            throw 'TypeError: opts.test';
        }
        opts = {
            retries: opts.retries || 3,
            delay: opts.delay || 100,
            test: opts.test,
            action: opts.action,
            error: opts.error
        };
        launch(function retryAction() {
            var msg;
            if (opts.test()) {
                if (isFn(opts.action)) {
                    opts.action();
                }
                return;
            }
            if (opts.retries) {
                launch(retryAction, opts.delay);
                opts.retries = opts.retries - 1;
                opts.delay = opts.delay * 2;
                return;
            }
            if (isFn(opts.error)) {
                opts.error();
                return;
            }
            msg = 'launchRetry failed\n\n[test:]\n' + String(opts.test) + '\n\n[action:]\n' + String(opts.action) + '\n\n[error:]\n' + String(opts.error);
            gcon.error(msg);
        });
    }


    // enumeration & tests

    function linqEach(value, callback) {
        var key;

        function breakCallback(key) {
            if (!Object.prototype.hasOwnProperty.call(value, key)) {
                return false;
            }
            return false === callback(value[key], key);
        }

        if (!isFn(callback)) {
            return;
        }

        if (isLikeArr(value)) {
            for (key = 0; key < value.length; key++) {
                if (breakCallback(key)) {
                    break;
                }
            }
        } else if (isObj(value)) {
            for (key in value) {
                if (breakCallback(key)) {
                    break;
                }
            }
        }
    }

    function linqEachAsync(value, callbackItem, callbackFinal) {
        var array;

        function action() {
            var item;
            if (array.length === 0) {
                if (isFn(callbackFinal)) {
                    callbackFinal();
                }
                return;
            }
            item = array.shift();
            if (callbackItem(item) !== false) {
                setTimeout(action);
            }
        }

        if (!isLikeArr(value)) {
            throw 'linqEachAsync works only on array-like objects';
        }
        if (!isFn(callbackItem)) {
            return;
        }

        array = Array.prototype.slice.call(value);
        setTimeout(action);
    }

    function linqAny(list, callback) {
        var result = false;
        if (isFn(callback)) {
            linqEach(list, function myArrAny_test(value, key) {
                if (callback(value, key) === true) {
                    result = true;
                    return false;
                }
            });
        }
        return result;
    }

    function linqAll(list, callback) {
        return !linqAny(list, function (value, key) {
            return !callback(value, key);
        });
    }

    function linqFirst(list, test, emptyValue) {
        var result = emptyValue;
        linqEach(list, function (item) {
            if (test(item) === true) {
                result = item;
                return false;
            }
        });
        return result;
    }

    function linqSelect(list, callback, skipValue) {
        var result = [];
        linqEach(list, function (item, key) {
            item = callback(item, key);
            if (item !== skipValue) {
                result.push(item);
            }
        });
        return result;
    }

    function linqSelectUnique(list, callback, skipValue) {
        var result = [];
        linqEach(list, function (item, key) {
            item = callback(item, key);
            if (item !== skipValue) {
                if (result.indexOf(item) === -1) {
                    result.push(item);
                }
            }
        });
        return result;
    }

    function chainFn(functions, combiner) {
        function chained() {
            var that = this, args = arguments, results;
            results = linqSelect(functions, function (f) {
                var result = f.apply(that, args);
                if (result) {
                    result = String(result);
                    if (result.length) {
                        return result;
                    }
                }
            });
            if (isFn(combiner)) {
                results = combiner(results);
            }
            return results;
        }
        return chained;
    }


    // get global object property
    // fails when not found, so we know something's wrong
    function assertGlobal(path, msg) {
        var obj = global, subname = '';
        linqEach(path.split('.'), function processName(name) {
            // not using hasProp due to IE host object limitations
            subname = (subname === '') ? name : subname + '.' + name;
            obj = obj[name];
            if (!isObj(obj)) {
                throw new ReferenceError(subname);
            }
        });
        return obj;
    }

    function tryObject(obj, path) {
        obj = isObj(obj) ? obj : {};
        linqEach(path.split('.'), function processName(name) {
            // not using hasProp due to IE host object limitations
            obj = isObj(obj) && obj[name];
        });
        return obj;
    }

    // get global object property
    function tryGlobal(name) {
        return tryObject(global, name);
    }


    // external libraries references (strict compliant avoiding globals)

    function tryGetExt() {
        return tryGlobal(gnames.extjs);
    }

    function assertExt() {
        return assertGlobal(gnames.extjs, 'missing Ext.js reference!');
    }

    function tryGetQuery() {
        return tryGlobal(gnames.jquery);
    }

    function assertQuery() {
        return assertGlobal(gnames.jquery, 'missing jquery reference!');
    }

    function assertSys() {
        return assertGlobal(gnames.sys, 'missing Sys reference!');
    }


    // load-order proofing hacks
    // TODO: preferred way would be to force load order through some modular framework like require.js
    //   or by ensuring correct order in backend (it is somewhat scattered on some pages now)

    function launchWhenJQuery(fn, msg) {
        if (!isFn(fn)) {
            throw 'TypeError: fn';
        }
        launchRetry({
            test: function () {
                return isFn(tryGetQuery());
            },
            action: fn,
            error: function () {
                gcon.error(msg || 'missing jquery to run action');
            }
        });
    }

    function launchWhenExt(fn, msg) {
        if (!isFn(fn)) {
            throw 'TypeError: fn';
        }
        launchRetry({
            test: function () {
                return isFn(tryGetExt());
            },
            action: fn,
            error: function () {
                if (msg !== '') {
                    gcon.error(msg || 'missing Ext to run action');
                }
            }
        });
    }

    function onSysLoad(fn) {
        if (!isFn(fn)) {
            throw 'TypeError: fn';
        }
        var sys = assertSys();
        sys.Application.add_load(fn);
    }

    function onSysLoadRemove(fn) {
        if (!isFn(fn)) {
            throw 'TypeError: fn';
        }
        var sys = assertSys();
        sys.Application.remove_load(fn);
    }

    function onSysLoadOnce(fn) {
        function wrapper() {
            fn();
            onSysLoadRemove(wrapper);
        }
        onSysLoad(wrapper);
    }

    // helper functions

    function stringFormat(format) {
        var args = arguments;
        return format.replace(/\{(\d+)\}/g, function (m, i) {
            return (i < 0) ? '' : args[Number(i) + 1];
        });
    }

    function stringTrimBuiltIn(text) {
        return String.prototype.trim.call(String(text));
    }

    function stringTrimCustom(text) {
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        return (String(text)).replace(rtrim, '');
    }


    // DOM access

    function $id(id) {
        if (isStr(id)) {
            return gdoc.getElementById(id);
        }
        if (id && id.style) {
            return id;
        }
        return undefined;
    }

    function $show(id, state) {
        var item = $id(id);
        if (item) {
            item.style.display = (state === false) ? 'none' : '';
        }
    }

    function $hide(id) {
        $show(id, false);
    }

    // global objects used in modules

    gdoc = assertGlobal('document');
    gwin = assertGlobal('window');


    // fake console so we don't have to check

    gcon = (function ensureConsole() {
        var con, methods, k, method;
        methods = [
            'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
            'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
            'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
            'timeStamp', 'trace', 'warn'
        ];
        gwin.console = gwin.console || {};
        con = gwin.console;
        for (k = 0; k < methods.length; k++) {
            method = methods[k];
            if (!con[method]) {
                con[method] = noop;
            }
        }
        return con;
    }());


    // basic global APM methods
    (function init_APMjs() {
        var dataDateTimeFormat, moduleAPM, initModules = {};

        // APM helpers

        function apmToggleVisibility(id, displayStyle) {
            var item = $id(id);
            if (item.style.display === 'none') {
                item.style.display = displayStyle;
            } else {
                item.style.display = 'none';
            }
        }

        function myBoolValue(ctl, value) {
            ctl = $id(ctl);
            if (isSet(ctl)) {
                if (isSet(value)) {
                    ctl.value = value ? 'True' : 'False';
                    return ctl.value;
                }
                return ctl && (String(ctl.value).toLowerCase() === 'true');
            }
        }

        function apmToggleEditSetting(editControl, viewControl, stateControl, requiredControl, validateStateControl, theLink) {
            var allowEdit, stateItem, required;

            apmToggleVisibility(editControl, '');
            apmToggleVisibility(viewControl, 'block');

            stateItem = $id(stateControl);
            allowEdit = myBoolValue(stateItem);

            if (requiredControl && validateStateControl) {
                required = myBoolValue(requiredControl);
                myBoolValue(validateStateControl, ($id(editControl).style.display === '') && required);
            }

            myBoolValue(stateItem, !allowEdit);
            theLink.innerHTML = allowEdit ? global.Orion.APM.EditApp.OverrideTemplateButtonText : global.Orion.APM.EditApp.InheritFromTemplateButtonText;

            global.$(theLink)
                .parent()
                .parent()
                .find('span[id$="errorMessage"]')
                .css('display', 'none');

            return false;
        }

        function apmToggleEditSettingByThreshold(pSender, pSubID, pCtrState) {
            var selector, state;

            selector = '#${ID}_warningeditSpan,#${ID}_warningviewSpan,#${ID}_criticaleditSpan,#${ID}_criticalviewSpan'.replace('${ID}', String(pSubID));
            global.$(selector).each(function toggler(i, el) {
                apmToggleVisibility(el, 'block');
            });

            state = pCtrState.val().toLowerCase() === String(true);

            pSender.innerHTML = state ? 'Override Template' : 'Inherit from Template';
            pCtrState.val(String(state));
            return false;
        }

        function apmGetExtJsDateTimeFormat(format) {
            return dataDateTimeFormat || format;
        }

        // check if a template has been exported and if so it does some changes in ExportTemplate.aspx page
        function apmCheckTemplateExporting() {
            global.SW.Core.Services.callWebService(
                '/Orion/APM/Services/Templates.asmx',
                'IsTemplateExported',
                {},
                function (result) {
                    var $ = assertQuery();
                    if (result === true) {
                        $('div.apm_ThwackExport').css('visibility', 'visible');
                        $('div.apm_ThwackExportMessage').toggle();
                    }
                }
            );
        }

        function apmGetCookieValue(key) {
            var cookieKeyValues = gdoc.cookie.split(';'), value;
            linqEach(cookieKeyValues, function (cvalue) {
                var index = cvalue.indexOf(key + '=', 0);
                if (index !== -1) {
                    value = cvalue.substr(index + 1 + key.length);
                    return false;
                }
            });
            return value;
        }

        // prepend elements in selector to resource host table (e.g. on application detail page)
        // each of the selected elements on separate row
        // reacts to column number page customization
        function apmPrependResources(selector) {
            var $ = assertQuery();
            $(function () {
                var $mainrow, cols;
                $mainrow = $('.subNavWrapperMain').closest('tr');
                cols = $mainrow.children('td').length;
                $(selector).each(function () {
                    var $warn = $(this), $warncell, $warnrow;
                    $warncell = $('<td>').css({ 'max-width': '100px' }).attr('colspan', cols - 1).append($warn);
                    $warnrow = $('<tr>').append($('<td>')).append($warncell);
                    $warnrow.insertBefore($mainrow);
                });
            });
        }


        function assertObjectPath(path) {
            if (!isStr(path)) {
                throw new TypeError('path: not a string');
            }
            if (/[.]{2,}/.test(path)) {
                throw new RangeError('path: invalid format');
            }
        }

        function ensureObject(obj, path) {
            assertObjectPath(path);
            obj = isObj(obj) ? obj : {};
            linqEach(path.split('.'), function processName(name) {
                // not using hasProp due to IE host object limitations
                if (!isObj(obj[name])) {
                    obj[name] = {};
                }
                obj = obj[name];
            });
            return obj;
        }

        function ensureObjectNew(obj, path) {
            assertObjectPath(path);
            var result = {
                object: isObj(obj) ? obj : {}
            };
            linqEach(path.split('.'), function processName(name) {
                result.name = name;
                result.parent = result.object;
                if (!isObj(result.parent[name])) {
                    result.parent[name] = {};
                }
                result.object = result.parent[name];
            });
            return result;
        }

        // Creates namespaces to be used for scoping variables and classes so that they are not global.
        // Usage:
        // <pre><code>
        //   MakeNamespace('Company.data');
        //   Company.Widget = function() { ... }
        //   Company.data.CustomStore = function(config) { ... }
        // </code></pre>
        function makeNamespace() {
            linqEach(arguments, function processNamespace(nstext) {
                ensureObject(global, nstext);
            });
        }

        function unsetGlobal(name) {
            var result = ensureObjectNew(global, name);
            delete initModules[name];
            delete result.parent[result.name];
        }

        function isInitialized(name) {
            assertObjectPath(name);
            return initModules[name] === true;
        }

        function initGlobal(name, initializer) {
            if (isSet(initializer) && !isFn(initializer)) {
                throw new TypeError('initializer: not a function');
            }
            var result = ensureObjectNew(global, name);
            if (!initModules[name] && isSet(initializer)) {
                initializer(result.object, moduleAPM, global);
                initModules[name] = true;
            }
            return result.object;
        }

        function initGlobalClass(name, constructor, initializer) {
            if (!isFn(constructor)) {
                throw new TypeError('constructor: not a function');
            }
            if (isSet(initializer) && !isFn(initializer)) {
                throw new TypeError('initializer: not a function');
            }
            var result = ensureObjectNew(global, name);
            if (!initModules[name]) {
                result.parent[result.name] = constructor;
                result.object = constructor;
                if (isSet(initializer)) {
                    initializer(constructor, moduleAPM, global);
                }
                initModules[name] = true;
            }
            return result.object;
        }

        function withGlobal(name, callback) {
            if (!isFn(callback)) {
                throw new TypeError('callback: not a function');
            }
            var mod = ensureObject(global, name);
            callback(mod, moduleAPM, global);
        }

        function withGlobalReady(name, callback) {
            if (!isStr(name)) {
                throw 'name: not a string';
            }
            if (!isFn(callback)) {
                throw 'callback: not a function';
            }
            var Ext = assertExt();
            Ext.onReady(function () {
                var mod = ensureObject(global, name);
                callback(mod, moduleAPM, global);
            });
        }

        initGlobal('SW.APM.Admin');

        // SW.APM functions

        withGlobal('SW.APM', function (APM) {
            APM.calculatePercents = function calculatePercents(part, full) {
                return Math.round((part * 100 / full) * 100) / 100;
            };
        });

        function parseXhrResponse(xhr) {
            // return eval('(' + xhr.responseText + ')');
            if (xhr && isSet(xhr.responseText)) {
                return global.JSON.parse(xhr.responseText);
            }
            return undefined;
        }


        // *** visibility helpers

        function setVisibility(elementId, visible, visibleStyle, hiddenStyle) {
            var element;
            if (isStr(elementId)) {
                element = document.getElementById(elementId);
            } else if (isObj(elementId)) {
                if (elementId.length || elementId.style) {
                    element = elementId;
                }
            }

            if (element) {
                var display = visible ? (visibleStyle || '') : (hiddenStyle || 'none');

                if (element.length) {
                    $.each(element, function (index, item) {
                        item.style.display = display;
                    });
                } else {
                    element.style.display = display;
                }
            }
        }

        function setBlockVisibility(elementId, visible) {
            setVisibility(elementId, visible, 'block');
        }

        function setInlineVisibility(elementId, visible) {
            setVisibility(elementId, visible, 'inline');
        }

        function setValue(elementId, value) {
            document.getElementById(elementId).value = value;
        }

        function getValue(elementId) {
            return document.getElementById(elementId).value;
        }

        moduleAPM = {
            // basic checking
            isDef: isDef,
            isSet: isSet,
            isBool: isBool,
            isNum: isNum,
            isStr: isStr,
            isObj: isObj,
            isFn: isFn,
            isArr: isArr,
            isLikeArr: isLikeArr,
            assertObj: assertObj,
            assertFn: assertFn,

            constFn: constFn,
            seal: seal,

            hasProp: hasProp,
            getProp: getProp,
            getType: getType,

            execute: execute,
            extend: extend,

            setVisibility: setVisibility,
            setBlockVisibility: setBlockVisibility,
            setInlineVisibility: setInlineVisibility,
            setValue: setValue,
            getValue: getValue,

            //
            noop: noop,
            launch: launch,
            launchRetry: launchRetry,
            console: gcon,

            // basic enumerations
            linqEach: linqEach,
            linqEachAsync: linqEachAsync,
            linqAny: linqAny,
            linqAll: linqAll,
            linqSelect: linqSelect,
            linqSelectUnique: linqSelectUnique,
            linqFirst: linqFirst,

            chainFn: chainFn,

            // more advanced helpers
            $id: $id,
            $show: $show,
            $hide: $hide,
            tryGlobal: tryGlobal,
            assertGlobal: assertGlobal,
            assertExt: assertExt,
            assertQuery: assertQuery,
            assertSys: assertSys,
            ensureObject: ensureObject,
            format: stringFormat,
            makeNamespace: makeNamespace,
            initGlobal: initGlobal,
            initGlobalClass: initGlobalClass,
            withGlobal: withGlobal,
            withGlobalReady: withGlobalReady,
            unsetGlobal: unsetGlobal,
            isInitialized: isInitialized,

            parseXhrResponse: parseXhrResponse,
            onSysLoad: onSysLoad,
            onSysLoadRemove: onSysLoadRemove,
            onSysLoadOnce: onSysLoadOnce,

            // apm specific helpers
            getExtJsDateTimeFormat: apmGetExtJsDateTimeFormat,
            getCookieValue: apmGetCookieValue,
            toggleEditSetting: apmToggleEditSetting,
            toggleEditSettingByThreshold: apmToggleEditSettingByThreshold,
            checkTemplateExporting: apmCheckTemplateExporting,
            prependResources: apmPrependResources
        };

        if (isFn(String.prototype.trim) && (String.prototype.trim.call('\uFEFF\xA0') === '')) {
            moduleAPM.trim = stringTrimBuiltIn;
        } else {
            moduleAPM.trim = stringTrimCustom;
        }

        global.APMjs = moduleAPM;

        // for script backwards compatibility's sake
        // in the future we should refactor everything to access this through APMjs module
        // it is confusing to be using things from global namespace without clear origin
        global.$id = $id;
        global.MakeNamespace = makeNamespace;
        global.SF = String.format || stringFormat;

        dataDateTimeFormat = apmGetCookieValue('ExtJsDateTimeFormat');

        launchWhenJQuery(function () {
            var $ = assertQuery();
            $(function APMjs_onready() {
                dataDateTimeFormat = apmGetCookieValue('ExtJsDateTimeFormat');
            });
        });
    }());

    // EventManager
    (function init_ApmJs_EventManager() {
        var SW = global.SW;

        function getEventName(value) {
            return value && value.toString().toLowerCase();
        }

        SW.APM.EventManager = {

            pool: {},

            on: function (name, handler) {
                var pool = this.pool, eventname, handlers, present;
                if (name && isFn(handler)) {
                    eventname = getEventName(name);
                    if (!hasProp(pool, eventname)) {
                        pool[eventname] = [handler];
                    } else {
                        handlers = pool[eventname];
                        present = linqAny(handlers, function testHandler(h) {
                            return h === handler;
                        });
                        if (!present) {
                            handlers.push(handler);
                        }
                    }
                }
                return this;
            },

            un: function (name, handler) {
                var pool = this.pool, eventname, handlers, i;
                if (name && isFn(handler)) {
                    eventname = getEventName(name);
                    if (hasProp(pool, eventname)) {
                        handlers = pool[eventname];
                        i = 0;
                        while (i < handlers.length) {
                            if (handlers[i] === handler) {
                                handlers.splice(i, 1);
                            } else {
                                i++;
                            }
                        }
                    }
                }
                return this;
            },

            fire: function (name, args) {
                var pool = this.pool, eventname, handlers;
                if (name) {
                    eventname = getEventName(name);
                    handlers = getProp(pool, eventname);
                    linqEach(handlers, function fireHandler(handler) {
                        if (isFn(handler)) {
                            handler(args);
                        }
                    });
                }
                return this;
            }
        };
    }());

    (function modify_Date_prototype() {
        Date.prototype.toUTCArray = function () {
            return [this.getUTCMonth(), this.getUTCDate(), this.getUTCFullYear(), this.getUTCHours(), this.getUTCMinutes()];
        };

        Date.prototype.toUTCFormatedString = function () {
            var temporary, utcArray = this.toUTCArray(), i = 0;
            utcArray[0] += 1;
            // Dont update hour
            while (i < 6) {
                temporary = utcArray[i];
                if (temporary < 10 && i !== 3) {
                    utcArray[i] = '0' + temporary;
                }
                i++;
            }
            return utcArray.splice(0, 3).join('/') + ' ' + utcArray.join(':');
        };
    }());

    (function init_ApmJs_NodeSelection() {
        var
            // global references
            $find = global.$find,
            $get = global.$get,
            APMjs = global.APMjs,
            // local data
            moduleNodSel = APMjs.ensureObject(APMjs, 'SelectNode'),
            data = {};

        moduleNodSel.OkBtnId = '';
        moduleNodSel.OnDialogOkClickHandlers = [];
        moduleNodSel.SelectedNodeInfo = null;

        function afterNodeSelectionChange(okButtonId) {
            var theNode, okButton;
            theNode = moduleNodSel.SelectedNodeInfo;
            if (theNode) {
                okButton = $get(okButtonId);
                if (okButton) {
                    okButton.disabled = false;
                }
            }
        }

        function showNodeSelectionDialog(testAgainstLabel, testButton, changeAllTestNodes, setNodeLnk, nodeHidden) {
            data.change_all_test_nodes = changeAllTestNodes;
            data.target_server = $get(testAgainstLabel);
            data.hid_node = $get(nodeHidden);
            data.btn_test = $get(testButton);
            data.lnk_set_node = setNodeLnk;
            data.popup = $find('SelectNodeModalPopupDialog');
            data.popup.show();
            afterNodeSelectionChange(moduleNodSel.OkBtnId);
        }

        function checkNodeIdsConsistency(testAgainstLabel, testButton, changeAllTestNodes, setNodeLnk, nodeHidden, postBack) {
            var $ = assertQuery();
            if ($('input[id$=hidTestNode][value=""]').size() > 0) {
                showNodeSelectionDialog(testAgainstLabel, testButton, changeAllTestNodes, $get(setNodeLnk), nodeHidden);
                if (postBack) {
                    moduleNodSel.OnDialogOkClickHandlers.push(postBack);
                }
                return false;
            }
            return true;
        }

        function showNodeSelectionDialogAndTest(testAgainstLabel, testButton, changeAllTestNodes, setNodeLnk, nodeHidden, postBack) {
            // xTODO: seems there's no need in additional flag?
            if ($get(nodeHidden).value === '') {
                showNodeSelectionDialog(testAgainstLabel, testButton, changeAllTestNodes, $get(setNodeLnk), nodeHidden);
                if (postBack) {
                    moduleNodSel.OnDialogOkClickHandlers.push(postBack);
                }
                return false;
            }
            if (changeAllTestNodes === 'True') {
                if (!checkNodeIdsConsistency(testAgainstLabel, testButton, changeAllTestNodes, setNodeLnk, nodeHidden, postBack)) {
                    return false;
                }
            }
        }

        function nodeSelectionOkClick() {
            if (global.IsDemoMode()) {
                return global.DemoModeMessage();
            }

            var theNode, node, handler, Ext = assertExt(), $ = assertQuery();

            data.popup.hide();
            theNode = this.SelectedNodeInfo;

            if (theNode) {
                node = { id: theNode.nodeId, name: theNode.text, status: theNode.status };
                if (data.change_all_test_nodes === 'False') {
                    data.target_server.innerHTML = (node.id) ? '@{R=APM.Strings;K=APMWEBJS_WillTestAgainst;E=js}' + ' ' + theNode.text : '@{R=APM.Strings;K=APMWEBJS_TestNodeNotSpecified;E=js}';
                    data.lnk_set_node.innerHTML = (node.id) ? '@{R=APM.Strings;K=APMWEBJS_ChangeTestNode;E=js}' : '@{R=APM.Strings;K=APMWEBJS_SetTestNode;E=js}';
                    data.hid_node.value = Ext.encode(node);
                } else {
                    $('a[id^="href"]').html(node.id ? '@{R=APM.Strings;K=APMWEBJS_ChangeTestNode;E=js}' : '@{R=APM.Strings;K=APMWEBJS_SetTestNode;E=js}');
                    $('span[class=ChangeTestNodeMessage]').html(node.id ? '@{R=APM.Strings;K=APMWEBJS_WillTestAgainst;E=js}' + ' ' + theNode.text : '@{R=APM.Strings;K=APMWEBJS_TestNodeNotSpecified;E=js}');
                    $('input[id$=hidTestNode]').val(Ext.encode(node));
                    $('input[class=TestSingleMonitorButton]').removeAttr('disabled');
                }
                $('input[class=TestAllMonitorsButton]').removeAttr('disabled');
                while (moduleNodSel.OnDialogOkClickHandlers.length) {
                    handler = moduleNodSel.OnDialogOkClickHandlers.pop();
                    if (isStr(handler)) {
                        setTimeout(handler, 0);
                    } else if (isFn(handler)) {
                        handler();
                    }
                }
            }
        }

        function nodeSelectionCancelClick() {
            data.popup.hide();
        }

        moduleNodSel.showNodeSelectionDialog = showNodeSelectionDialog;
        moduleNodSel.showNodeSelectionDialogAndTest = showNodeSelectionDialogAndTest;
        moduleNodSel.nodeSelectionOkClick = nodeSelectionOkClick;
        moduleNodSel.nodeSelectionCancelClick = nodeSelectionCancelClick;
    }());

    (function init_ApmJs_Utility() {
        var
            // global references
            APMjs = global.APMjs,
            // locals
            moduleUtil = APMjs.ensureObject(APMjs, 'Utility');

        function apmUtilityChangeName(labelID, editID) {
            function callback(state, value) {
                if ((state === 'ok') && (APMjs.trim(value) !== '')) {
                    if (global.IsDemoMode()) {
                        return global.DemoModeMessage();
                    }
                    $id(labelID).innerHTML = value;
                    $id(editID).value = value;
                }
            }
            var Ext = assertExt();
            Ext.MessageBox.prompt('@{R=APM.Strings;K=APMWEBJS_ComponentName;E=js}', '@{R=APM.Strings;K=APMWEBJS_PleaseEnterComponentName;E=js}', callback, null, false, $id(editID).value);
        }

        function apmUtilityManageChecker(pGrid) {
            if (!pGrid) {
                return;
            }
            var $ = assertQuery(), storeCount, modelCount, el, className, checked;
            storeCount = pGrid.getStore().getCount();
            modelCount = pGrid.getSelectionModel().getCount();
            el = $('div[class^="x-grid3-hd-inner x-grid3-hd-checker"]', pGrid.body.dom);
            className = 'x-grid3-hd-checker-on';
            checked = el.hasClass(className);
            if (checked && (storeCount === 0 || modelCount === 0)) {
                el.removeClass(className);
            } else if ((checked && storeCount !== modelCount) || (!checked && storeCount > 0 && storeCount === modelCount)) {
                el.toggleClass(className);
            }
        }

        // Web Link Probe Result
        function showWLP(pObj) {
            var items = [], dlg, Ext = assertExt();

            linqEach(pObj.Items, function (item) {
                items.push(item.Data);
            });

            pObj.Items = items;

            dlg = new Ext.Window({
                title: String.format("Probe Result Dialog [Page Url: <a target='_blank' href='{0}'>{0}</a>]", pObj.Url),
                width: 750,
                height: 400,
                border: false,
                region: 'center',
                layout: 'fit',
                modal: true,
                bodyStyle: 'padding:5px;',
                items: [
                    new Ext.grid.GridPanel({
                        viewConfig: { forceFit: true },
                        store: new Ext.data.SimpleStore({
                            data: pObj.Items,
                            sortInfo: { field: 'hs', direction: 'ASC' },
                            fields: [{ name: 'hp' }, { name: 'ho' }, { name: 'he' }, { name: 'hc', type: 'int' }, { name: 'hs' }]
                        }),
                        columns: [
                            {
                                header: 'Combine Href',
                                dataIndex: 'hp',
                                sortable: true,
                                menuDisabled: true,
                                renderer: function (val) {
                                    return String.format('<a target="_blank" href="{0}" title="{0}">{0}</a>', val);
                                }
                            },
                            {
                                header: 'Original Href',
                                dataIndex: 'ho',
                                sortable: true,
                                menuDisabled: true
                            },
                            {
                                header: 'External',
                                dataIndex: 'he',
                                width: 70,
                                fixed: true,
                                sortable: true,
                                menuDisabled: true,
                                align: 'center',
                                renderer: function (val) {
                                    return val ? 'Yes' : 'No';
                                }
                            },
                            {
                                header: 'Count',
                                dataIndex: 'hc',
                                width: 60,
                                fixed: true,
                                sortable: true,
                                menuDisabled: true,
                                align: 'center'
                            },
                            {
                                header: 'Status',
                                dataIndex: 'hs',
                                width: 70,
                                fixed: true,
                                sortable: true,
                                menuDisabled: true,
                                align: 'center',
                                renderer: function (val) {
                                    return String.format('<img src="/Orion/images/StatusIcons/Small-{0}.gif" alt="{1}" title="{1}" target="_blank"/>', val === 'OK' ? 'Up' : 'Down', val);
                                }
                            }
                        ],
                        bbar: new Ext.Toolbar({
                            items: [
                                '->',
                                '-',
                                String.format('<b style="color:#d00;">Down: {0}</b>', pObj.Down),
                                '-',
                                String.format('<b style="color:#090;">Up: {0}</b>', pObj.Up),
                                '-',
                                String.format('<b>Response Time: {0} ms</b>', pObj.ResponseTime),
                                '&nbsp;&nbsp;&nbsp;'
                            ]
                        })
                    })
                ],
                buttons: [{
                    text: "Close",
                    handler: function () {
                        dlg.hide();
                        dlg.destroy();
                        dlg = null;
                    }
                }]
            });
            dlg.show();
        }

        function apmUtilityShowProbeResult(pName, pObj) {
            if (pName === 'WLP') {
                showWLP(pObj);
            }
            return false;
        }

        moduleUtil.changeName = apmUtilityChangeName;
        moduleUtil.manageChecker = apmUtilityManageChecker;
        moduleUtil.showProbeResult = apmUtilityShowProbeResult;
    }());

    (function init_ApmJs_DemoMode() {

        function demoModeMessage() {
            global.alert('@{R=APM.Strings;K=APMWEBJS_VB1_169;E=js}');
            return false;
        }

        function isDemoMode() {
            return $id('isDemoMode');
        }

        function isApmDevMode() {
            return global.APM_WEB_DevMode === true;
        }

        // @method notifies demo framework of some action
        // @param {string} objectOrGroupId (optional) is an object/group identifier on which demo team will do some action
        // @param reference (optional) is the object which is identified by objectOrGroupId
        function demoModeAction(objectOrGroupId, reference) {
            var prefix = 'APM_', actionId;
            if (global.demoAction === undefined) {
                demoModeMessage();
            } else {
                actionId = isSet(objectOrGroupId) ? null : prefix + objectOrGroupId;
                global.demoAction(actionId, reference);
            }
            return false;
        }

        var APMjs = assertGlobal('APMjs');

        APMjs.demoModeMessage = demoModeMessage;
        APMjs.demoModeAction = demoModeAction;
        APMjs.isDemoMode = isDemoMode;
        APMjs.isApmDevMode = isApmDevMode;

        global.DemoModeAction = demoModeAction;
        global.DemoModeMessage = demoModeMessage;
        global.IsDemoMode = isDemoMode;
        global.IsApmDevMode = isApmDevMode;
    }());

    (function init_ApmJs_globalFuncs() {
        var APMjs = global.APMjs, SF = String.format, SW = global.SW;

        // helper function to detach events of dialog buttons before destruction of Ext.Window
        function preDestroyDialog(dialog) {
            var i;
            for (i = dialog.buttons.length - 1; i > -1; i--) {
                dialog.buttons[i].destroy();
            }
        }

        SW.APM.highlightSearchText = function (selector, searchTerm) {

            function quoteForRegExp(str) {
                var i, toQuote = '\\/[](){}?+*|.^$'; // note: backslash must be first
                for (i = 0; i < toQuote.length; ++i) {
                    str = str.replace(toQuote.charAt(i), '\\' + toQuote.charAt(i));
                }
                return str;
            }

            // search term highlighting adapted from http://dossy.org/archives/000338.html
            var
                $ = assertQuery(),
                re = new RegExp().compile('(' + quoteForRegExp(searchTerm) + ')', 'ig');
            $(selector + ' *').each(function () {
                var html, newhtml;
                if ($(this).children().size() > 0) {
                    return;
                }
                html = $(this).html();
                newhtml = html.replace(re, '<span class="apm_searchterm">$1</span>');
                $(this).html(newhtml);
            });

        };

        SW.APM.logInToThwack = function (callback, params, showWarning) {
            var mDlg, Ext = assertExt();

            // helper members
            function val(id) {
                return Ext.getCmp(id).getValue().trim();
            }

            // event handlers
            function onShow(pWnd) {
                var
                    $ = assertQuery(),
                    html = '<td class="x-panel-btn-td" style="width:auto; white-space: nowrap;"><div>' +
                    '<a href="http://www.solarwinds.com/embedded_in_products/productLink.aspx?id=Create_Thwack_User"' +
                    ' style="line-height:1;text-decoration:underline;font-size:11px;" target="_blank">@{R=APM.Strings;K=APMWEBJS_VB1_80;E=js}</a>' +
                    '</div></td>';
                $('#tlDlg div.x-panel-btns table:first tbody tr td:first').before(html);
                $(pWnd.buttons[0].el.dom)
                    .attr('apm_enterhandler', '0');
            }
            function onValidate(ctrIndex, text) {
                mDlg.buttons[0].setDisabled(val('tliUN') === '');
                return true;
            }
            function onCancel() {
                if (mDlg) {
                    mDlg.hide();
                    mDlg.destroy();
                    mDlg = null;
                }
            }
            function onLogIn() {
                onValidate();
                if (mDlg.buttons[0].disabled) {
                    return;
                }
                var ORION = assertGlobal('ORION');
                ORION.callWebService(
                    '/Orion/APM/Services/ThwackAPM.asmx',
                    'LogIn',
                    {
                        userName: val('tliUN'),
                        password: val('tliPSW')
                    },
                    function (result) {
                        global.thwackUserInfo = {
                            name: val('tliUN'),
                            pass: val('tliPSW')
                        };
                        onCancel();
                        if (isFn(callback)) {
                            callback(params);
                        }
                    }
                );
            }

            function getDlgItems() {
                var items = [];
                if (showWarning) {
                    items.push({
                        xtype: 'label',
                        html: '@{R=APM.Strings;K=APMWEBJS_VB1_82;E=js}',
                        style: 'margin-left:20px;color:#666;'
                    });
                    items.push({
                        xtype: 'label',
                        html: '<hr style="width:auto;color:#666;"/>'
                    });
                }
                items.push({
                    id: 'tliUN',
                    fieldLabel: '@{R=APM.Strings;K=APMWEBJS_VB1_30;E=js}',
                    anchor: '100%',
                    selectOnFocus: true,
                    validator: onValidate,
                    value: global.thwackUserInfo.name,
                    style: 'white-space: nowrap;'
                });
                items.push({
                    id: 'tliPSW',
                    inputType: 'password',
                    fieldLabel: '@{R=APM.Strings;K=APMWEBJS_VB1_32;E=js}',
                    anchor: '100%',
                    selectOnFocus: true,
                    validator: onValidate,
                    value: global.thwackUserInfo.pass
                });
                return items;
            }

            mDlg = new Ext.Window({
                id: 'tlDlg',
                title: '@{R=APM.Strings;K=APMWEBJS_VB1_81;E=js}',
                width: 320,
                height: showWarning ? 310 : 140,
                border: false,
                region: 'center',
                layout: 'fit',
                modal: true,
                resizable: false,
                bodyStyle: 'padding:5px;',
                items: [
                    new Ext.form.FormPanel({
                        id: 'tldForm',
                        frame: true,
                        labelWidth: 110,
                        defaultType: 'textfield',
                        labelAlign: 'right',
                        items: getDlgItems()
                    })
                ],
                buttons: [
                    {
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_83;E=js}',
                        disabled: true,
                        handler: onLogIn
                    },
                    {
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}',
                        handler: onCancel
                    }
                ],
                listeners: {
                    show: onShow
                }
            });
            mDlg.show();
        };

        // Unmanage/Remanage Application item functionality
        SW.APM.ManageAppItem = function constructor_ManageAppItem(config) {
            var
                Ext = assertExt(),
                $ = assertQuery(),
                mConfig = config,
                waitMsg = Ext.Msg.wait(mConfig.message);

            // Event Handlers
            function onError(xhr, status, errorThrown) {
                var error = APMjs.parseXhrResponse(xhr);
                Ext.Msg.show({
                    title: "@{R=APM.Strings;K=APMWEBJS_TM0_48;E=js}",
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            }

            // Helper Members
            function call(service, method, params, onSuccess) {
                var paramsStr = Ext.encode(params);
                if (paramsStr.length === 0) {
                    paramsStr = "{}";
                }
                $.ajax({
                    type: "POST",
                    url: (service + "/" + method),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: paramsStr,
                    success: function (msg) { onSuccess(msg.d); },
                    error: function (xhr, status, errorThrown) {
                        onError(xhr, status, errorThrown);
                    }
                });
            }

            function onComplete() {
                waitMsg.hide();
                $("#groupByProperty").change();
                if (mConfig.onComplete) {
                    mConfig.onComplete();
                }
            }

            // Public Members
            this.manage = function () {
                call("/Orion/APM/Services/Applications.asmx", "ManageApplicationItem", mConfig, onComplete);
            };
        };

        SW.APM.ManageAppItem.manage = function (config) {
            return new SW.APM.ManageAppItem(config).manage();
        };

        //note: This piece of code has been extracted from former implementation of Unmanage/Remanage operation
        //This code displayed list of applications which remain unmanaged after Remanage operation, because they inherit it from parent node
        //Current Unmanage/Remanage implementation doesn't display this info dialog
        function __unused__onRemanageComplete(appIds) {
            waitMsg.hide();
            if (appIds.length > 0) {
                var appNames = [];
                linqEach(mConfig.selItems, function (rec) {
                    if (appIds.indexOf(parseInt(rec.id, 10)) > -1) {
                        appNames.push(rec.name);
                    }
                });
                Ext.Msg.show({
                    title: '@{R=APM.Strings;K=APMWEBJS_TM0_45;E=js}',
                    msg: String.format(appNames.length > 1 ? '@{R=APM.Strings;K=APMWEBJS_TM0_46;E=js}' : '@{R=APM.Strings;K=APMWEBJS_TM0_47;E=js}', nms.join(', ')),
                    icon: Ext.Msg.INFO,
                    buttons: Ext.Msg.OK,
                    fn: finish
                });
            } else {
                finish();
            }
        }

        SW.APM.PollApp = function constructor_SW_APM_PollApp(config) {
            var $ = assertQuery(), mDlg = null, mConfig = config, fnCancel, fnError;

            // Helper Members
            function call(service, method, params, onSuccess) {
                var paramsStr = global.JSON.stringify(params) || '{}';
                $.ajax({
                    type: 'POST',
                    url: (service + '/' + method),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: paramsStr,
                    success: function (msg) {
                        onSuccess(msg.d);
                    },
                    error: function (xhr, status, errorThrown) {
                        fnCancel();
                        fnError(xhr, status, errorThrown);
                    }
                });
            }

            function getIDs() {
                var ids = [];
                linqEach(mConfig.selItems, function (rec) {
                    ids.push(rec.id);
                });
                return ids;
            }

            // Event Handlers
            fnError = function onError(xhr, status, errorThrown) {
                var error = APMjs.parseXhrResponse(xhr), Ext = assertExt();
                Ext.Msg.show({
                    title: '@{R=APM.Strings;K=APMWEBJS_VB1_170;E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            };

            fnCancel = function onCancel() {
                if (mDlg) {
                    mDlg.hide();
                    mDlg.destroy();
                    mDlg = null;
                }
            };

            // Public Members
            this.pollNow = function () {
                var Ext = assertExt(), waitMsg = Ext.Msg.wait('@{R=APM.Strings;K=APMWEBJS_VB1_168;E=js}');
                function onComplete(appIDs) {
                    waitMsg.hide();
                    $('#groupByProperty').change();
                    if (isFn(mConfig.onComplete)) {
                        mConfig.onComplete();
                    }
                }
                call(
                    '/Orion/APM/Services/Applications.asmx',
                    'PollApplications',
                    { appIds: getIDs() },
                    onComplete
                );
                return false;
            };
        };

        SW.APM.PollApp.pollNow = function (config) {
            return new SW.APM.PollApp(config).pollNow();
        };

        SW.APM.ShowSelectedItemList = function (selectedItems) {
            var items, list, listHtml, dlg, Ext = assertExt();

            items = selectedItems.split(',');
            list = ['<ul style="list-style-type:square;margin-left:20px;">'];
            linqEach(items, function (item) {
                list.push('<li>' + item + '</li>');
            });
            list.push('</ul>');

            listHtml = list.join('');

            dlg = new Ext.Window({
                title: String.format('All Selected Nodes ({0})', items.length),
                width: 500,
                height: 350,
                border: false,
                region: 'center',
                layout: 'fit',
                modal: true,
                resizable: false,
                plain: true,
                items: new Ext.Panel({
                    frame: true,
                    autoScroll: true,
                    defaultType: 'label',
                    html: listHtml
                }),
                buttons: [{
                    text: 'Close',
                    handler: function () {
                        dlg.hide();
                        dlg.destroy();
                        dlg = null;
                    }
                }]
            });

            dlg.show();
            return false;
        };

        // var settEditUtility = {
        //     onChange: function (sender, params) {
        //         var sel = $(sender), parent = sel.parent().parent(), table = parent.children("table.apm_SubItemContainer");
        //         var spanEdit = parent.children("span[id$='editContainer']"), spanView = parent.children("span[id$='viewContainer']");
        //         var isAct = ((spanEdit.css("display") != "none" && sel.val() == params) || (spanView.css("display") != "none" && $.trim(spanView.text()) == params));
        //         table.css("display", isAct ? "block" : "none");
        //     },
        //     update: function (info) {
        //         for (var i = 0; i < info.length; i++) {
        //             settEditUtility.onChange($id(info[i].id), info[i].cmd)
        //         }
        //     }
        // };

        SW.APM.ShowPopupWindow = function (pId, pTitle, pHref, pWidth, pHeight) {
            var Ext = assertExt(), apExtWindow;
            apExtWindow = new Ext.Window({
                id: 'window-' + pId,
                renderTo: Ext.getBody(),
                title: pTitle,
                modal: true,
                layout: 'fit',
                width: pWidth,
                height: pHeight,
                maximizable: true,
                autoScroll: false,
                resizable: true,
                closable: true,
                closeAction: 'close',
                plain: true,
                html: '<iframe id="iframe-' + pId + '"src="' + pHref + '" style="overflow:auto;width:100%;height:100%;" frameborder="0"></iframe>'
            });
            apExtWindow.show();
        };

        /*Application Dashboards Functionality*/
        SW.APM.Dashboard = {};

        SW.APM.Dashboard.CustomizeDialog = function (tplID, tplName, customizeUrlTmpl) {
            var mDlg = null, mWait = null, fnSuccess, fnOk, fnCancel;

            function show() {
                var Ext = assertExt(), config;
                config = {
                    title: '@{R=APM.Strings;K=APMWEBJS_AK1_68;E=js}',
                    width: 650,
                    height: 170,
                    resizable: false,
                    border: false,
                    region: 'center',
                    layout: 'fit',
                    modal: true,
                    frame: true,
                    items: [{
                        xtype: 'fieldset',
                        style: 'padding-top:0px;',
                        bodyStyle: 'padding-top:7px;',
                        items: [{
                            id: 'dcdForm',
                            xtype: 'form',
                            frame: true,
                            labelWidth: 1,
                            items: [{
                                xtype: 'label',
                                text: '@{R=APM.Strings;K=APMWEBJS_AK1_69;E=js}',
                                style: 'font-size:10pt;'
                            }, {
                                xtype: 'radiogroup',
                                columns: 1,
                                items: [{
                                    id: 'rbCreate',
                                    xtype: 'radio',
                                    boxLabel: String.format('@{R=APM.Strings;K=APMWEBJS_AK1_70;E=js}', tplName),
                                    name: 'rbCVMode',
                                    inputValue: 1,
                                    checked: true
                                }, {
                                    id: 'rbAssign',
                                    xtype: 'radio',
                                    boxLabel: '@{R=APM.Strings;K=APMWEBJS_AK1_71;E=js}',
                                    name: 'rbCVMode',
                                    inputValue: 2
                                }]
                            }]
                        }]
                    }],
                    buttons: [{
                        text: '@{R=APM.Strings;K=APMWEBJS_TM0_43;E=js}',
                        handler: fnOk
                    }, {
                        text: '@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}',
                        handler: fnCancel
                    }]
                };
                mDlg = new Ext.Window(config);
                mDlg.show();
                return false;
            }

            function hide() {
                if (mDlg) {
                    mDlg.hide();
                    preDestroyDialog(mDlg);
                    mDlg.destroy();
                    mDlg = null;
                }
                return false;
            }

            // Event Handlers

            fnSuccess = function onSuccess(result) {
                if (mWait) {
                    mWait.hide();
                }
                if (result > 0) {
                    gwin.location = String.format(customizeUrlTmpl, result);
                }
            };

            fnOk = function onOk() {
                var Ext = assertExt(), ORION = assertGlobal('ORION'), crtNew = Ext.getCmp('rbCreate').getValue();
                hide();
                mWait = Ext.Msg.wait('@{R=APM.Strings;K=APMWEBJS_AK1_67;E=js}');
                ORION.callWebService(
                    '/Orion/APM/Services/Templates.asmx',
                    'AssociateTemplateWithView',
                    {
                        'tplID': tplID,
                        'createNew': crtNew
                    },
                    fnSuccess
                );
                return false;
            };

            fnCancel = function onCancel() {
                hide();
                return false;
            };

            // public members
            this.show = show;
            this.hide = hide;
        };

        // enter handler for admin pages
        function initEnterAdminHandler() {
            var $ = global.$;
            $(function loadEnterAdminHandler() {
                $(gdoc).keypress(function (evt) {
                    var i, jEl, handler, HANDLERS_ON_PAGE = global.HANDLERS_ON_PAGE;
                    if (evt.keyCode !== 13 || $(evt.target).is('input[type=text],input[type=password],textarea')) {
                        return true;
                    }
                    for (i = 0, HANDLERS_ON_PAGE = 3; i < HANDLERS_ON_PAGE; i++) {
                        jEl = $(SF('*[apm_enterhandler={0}]:visible:not(:disabled):first', i));
                        if ((jEl.length > 0) && (jEl.parents(':visible:not(:disabled)').length === 0)) {
                            handler = jEl.attr('onclick');
                            if (handler) {
                                if (isFn(handler)) {
                                    if (handler() === false) {
                                        return;
                                    }
                                } else if (isStr(handler)) {
                                    handler = handler.trim();
                                    if (execute(handler) === false) {
                                        return;
                                    }
                                }
                            }
                            handler = jEl.attr('href');
                            if (isStr(handler) && !/javascript\:/.test(handler)) {
                                execute(handler);
                                return;
                            }
                            if ($.browser.msie && parseInt($.browser.version, 10) > 8) {
                                jEl.focus();
                            }
                            jEl.click();
                            return;
                        }
                    }
                });
            });
        }

        if (gwin.location.href.toLowerCase().indexOf('/apm/admin/') > 0) {
            launchWhenJQuery(initEnterAdminHandler);
        }

        function getDefaultRevealTrimmedHandler(opts) {
            function handler() {
                var Ext = tryGetExt(),
                    message = assertQuery()(this).data('message');
                if (Ext && Ext.Msg && Ext.Msg.show) {
                    Ext.Msg.show({
                        title: (opts && opts.title) || '',
                        msg: message,
                        icon: (opts && opts.icon) || Ext.Msg.INFO,
                        buttons: (opts && opts.buttons) || Ext.Msg.OK
                    });
                } else {
                    window.alert(opts.title + '\n\n' + message);
                }
                return false;
            }
            return handler;
        }

        function trimElementText(opts) {
            // nonbreaking fail if element not given
            if (!opts || !opts.$element) {
                console.warn('trimElementText: opts and opts.$element must be set');
                return;
            }
            if (!opts.$element.length) {
                return;
            }
            // postpone if element not yeat visible
            if (!(opts.$element.is(':visible'))) {
                setTimeout(function () {
                    trimElementText(opts);
                });
                return;
            }
            var message, height,
                $ = assertQuery(),
                sublen = 2,
                $element = opts.$element,
                handler = opts.handler || getDefaultRevealTrimmedHandler(),
                htmlMore = opts.htmlMore || '...&nbsp;<a href="#">&raquo; @{R=APM.Strings;K=APMWEBJS_VB1_166;E=js}</a>';
            // get content (used in case of rewrapping after page load)
            message = $element.data('message');
            if (!message) {
                message = $element.html();
                $element.data('message', message);
            } else {
                $element.html(message);
            }
            // make this work for table cells
            if ($element.is('td,th')) {
                $element = $('<span>')
                    .html(message)
                    .appendTo($element.empty());
            }
            height = $element.outerHeight();
            $element.html(message.substr(0, sublen));
            // difference in height too small to matter
            if (5 > Math.abs(height - $element.outerHeight())) {
                $element.html(message);
                return true;
            }
            height = $element.outerHeight() + 3;
            // add text until it wraps
            while ((sublen < message.length) && (height >= $element.outerHeight())) {
                sublen += 5;
                $element.html(message.substr(0, sublen) + htmlMore);
            }
            // no wrap happened?
            if (height >= $element.outerHeight()) {
                $element.html(message);
                return true;
            }
            // trace back until it does not wrap
            while (height < $element.outerHeight()) {
                sublen -= 1;
                $element.html(message.substr(0, sublen) + htmlMore);
            }
            // add click listener to "more" link
            $element
                .find('a')
                .data('message', message)
                .click(handler);
        }

        // trim status details text to a single line
        function trimStatusDetails() {
            var $ = assertQuery(),
                block = true;

            function trimStatusDetailsInner(trimCheck) {
                $('div.status-details-container').each(function () {
                    var jLabel = $(this).find('label');
                    if (trimCheck) {
                        if (jLabel.data('trimmed')) {
                            return true;
                        }
                        jLabel.data('trimmed', true);
                    }
                    trimElementText({
                        $element: jLabel,
                        handler: getDefaultRevealTrimmedHandler({
                            title: '@{R=APM.Strings;K=APMWEBJS_VB1_167;E=js}'
                        })
                    });
                });
            }

            function secondRun() {
                if (block) {
                    setTimeout(secondRun, 100);
                } else {
                    trimStatusDetailsInner(true);
                }
            }

            // rewrap after load complete - icon size can change
            // (actually noticed this behavior when text wrapped onto second line bc trimming happened while icon was not yet loaded)
            $(window).load(secondRun);
            trimStatusDetailsInner();
            block = false;
        }

        SW.APM.getDefaultRevealTrimmedHandler = getDefaultRevealTrimmedHandler;
        SW.APM.trimElementText = trimElementText;
        SW.APM.TrimStatusDetails = trimStatusDetails;


        function initOtherJQueryRelated() {
            var $ = assertQuery();

            launchWhenExt(SW.APM.TrimStatusDetails, '');

            //fix for ie 9
            if ($.browser.msie) {
                if (isDef(global.Range) && !isFn(global.Range.prototype.createContextualFragment)) {
                    global.Range.prototype.createContextualFragment = function (html) {
                        var frag, div;
                        frag = gdoc.createDocumentFragment();
                        div = gdoc.createElement('div');
                        frag.appendChild(div);
                        div.outerHTML = html;
                        return frag;
                    };
                }
            }

            //FB114487 (elem is undefined; var id = elem[expando];)
            if ($.validator !== undefined) {
                $.extend($.fn, {
                    validateDelegate: function (delegate, type, handler) {
                        return this.bind(type, function (event) {
                            var target = $(event.target);
                            if (target.length && target[0].form && target.is(delegate)) {
                                return handler.apply(target, arguments);
                            }
                        });
                    }
                });
            }
        }

        launchWhenJQuery(initOtherJQueryRelated);
    }());
}(window));
