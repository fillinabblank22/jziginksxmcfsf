<%@ Page Language="C#" MasterPageFile="ApmView.master" AutoEventWireup="true" CodeFile="MonitorDetails.aspx.cs" Inherits="Orion_APM_MonitorDetails" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import  Namespace="SolarWinds.APM.Web" %>
<%@ Import  Namespace="SolarWinds.APM.Web.UI" %>
<%@ Register TagPrefix="apm" Assembly="SolarWinds.APM.Web" Namespace="SolarWinds.APM.Web.UI" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="apm" TagName="ApmStatusIcon" Src="~/Orion/APM/Controls/ApmStatusIcon.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="apm" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <style>
        .custPageLink {
            background-image: url('/Orion/APM/Images/Page.Icon.CustomPg.gif') !important;
        }
    </style>
    <%if (Profile.AllowCustomize) {%>
		<a href="<%=CustomizeViewHref%>" class="custPageLink"><%= Resources.APMWebContent.APMWEBDATA_TM0_1 %></a>
	<%}%>
	<%if (ApmRoleAccessor.AllowAdmin)
   { %>
		<a href="<%=ApmMasterPage.GetEditApplicationPageUrl(ApmApplication.Id,string.Format(CultureInfo.InvariantCulture, "selected={0}", Monitor.Id))%>"  
           class="custPageLink" 
           style="background-image: url('/Orion/APM/Images/Button.EditApplication.gif');">  
           <%= Resources.APMWebContent.APMWEBDATA_TM0_2 %>
		</a>
	<%}%>
	<orion:IconHelpButton HelpUrlFragment="OrionAPMPHMonitorDetails" ID="btnHelp" runat="server" />
	<br />
    <div style="padding-top: 5px; font-size: 8pt; text-align: right;">
	<%=DateTime.Now.ToString("F")%>
    </div>
</asp:Content>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="ApmPageTitle" Runat="Server">
   
   <h1><%=ViewInfo.ViewTitle %></h1>
	
    <table cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding-left:15px; padding-right:5px;">
			    <apm:ApmStatusIcon ID="ApmStatusIcon" runat="server" CustomApplicationType="<%# this.ApmApplication.CustomType %>" />
			</td>
			<td style="font-weight:bold;">
            	    <%= SolarWinds.APM.Common.Utility.InvariantString.Format(Resources.APMWebContent.APMWEBDATA_AK1_59, GetMonitorInnerHtml(), GetApplicationInnerHtml(), GetNodeInnerHtml()) %>
			</td>
		</tr>
	</table>
    <br />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
	<apm:ApmMonitorResourceHost ID="resHost" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</apm:ApmMonitorResourceHost>
</asp:Content>
