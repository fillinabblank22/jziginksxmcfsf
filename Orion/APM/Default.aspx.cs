using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;
using System.Collections.Generic;

public partial class Orion_APM_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string pagePath = "~/Orion";

        List<OrionModule> modules = new List<OrionModule>(OrionModuleManager.GetModules());

        foreach (OrionModule mod in modules)
        {
            if (mod.Name.Equals("APM", StringComparison.InvariantCultureIgnoreCase))
            {
                pagePath = mod.HomePage;
                break;
            }
        }

        Response.Redirect(ResolveUrl(pagePath));
    }
}
