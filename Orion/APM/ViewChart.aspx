<%@ Page Language="C#" %>
<%@ Register TagPrefix="apm" TagName="CustomChartControl" Src="~/Orion/APM/Controls/InternalCustomChart.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>View Chart</title>
	<orion:Include runat="server" File="APM/APM.css"/>    
</head>

<body>
    <form id="form1" runat="server">
    <div>
        <apm:CustomChartControl runat="server" ID="CustomChart" EnableEditing="false"/>    
    </div>
    </form>
</body>
</html>
