<%@ Page Language="C#" MasterPageFile="~/Orion/APM/ApmView.master" AutoEventWireup="true" 
    CodeFile="CustomChart.aspx.cs" Inherits="Orion_APM_CustomChart" Title="<%$ Resources:APMWebContent,APMWEBDATA_VB1_65 %>" %>
<%@ Register TagPrefix="apm" TagName="CustomChartControl" Src="~/Orion/APM/Controls/InternalCustomChart.ascx" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ApmPageTitle" Runat="Server">
    <h1 runat="server" id="PageHeader"><%# Resources.APMWebContent.APMWEBDATA_VB1_65 %></h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ApmMainContentPlaceHolder" Runat="Server">

    <apm:CustomChartControl runat="server" ID="CustomChart" EnableEditing="true"/>
    


</asp:Content>

