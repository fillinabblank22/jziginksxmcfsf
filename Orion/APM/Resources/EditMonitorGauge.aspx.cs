﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using EditConfig = SolarWinds.APM.Web.ApmGaugeResource.EditConfig;

public partial class Orion_APM_Resources_EditMonitorGauge : SolarWinds.Orion.NPM.Web.UI.GaugeEditBasePage
{
    // The order of styles could be changed (styles are loaded from Core assembly), so we should have one style as default.
    protected const string DefaultRadialGaugeStyle = "Simple Circle";
    protected const string DefaultLinearGaugeStyle = "SolarWinds";

	#region Properties
    
    protected String HeadTitle 
	{
		get 
		{
            return String.Format(Resources.APMWebContent.APMWEBCODE_AK1_149, Resource.Title); 
		}
	}

    public GaugeType GaugeType
    {
        get { return "Linear".Equals(Request.QueryStringOrForm("GaugeType"), StringComparison.InvariantCultureIgnoreCase) ? GaugeType.Linear : GaugeType.Radial; }
    }

    public string SelectedStyle { get; set; }

	#endregion 

	#region Event Handlers

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		resourceTitleEditor.ResourceTitle = Resource.Title;
		resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;
        ctrGaugeAssigner.OnSelectedNameChanged += new EventHandler(CtrAssigner_OnSelectedNameChanged);

		if (!IsPostBack)
		{
			var config = EditConfig.Default;
			try
			{
				config = (EditConfig)Enum.Parse(typeof(EditConfig), Request.QueryStringOrForm("EditConfig"));
			}
			catch { }

			ctrGaugeAssigner.Visible &= (config & EditConfig.ShowGaugeAssigner) > 0;
			if (ctrGaugeAssigner.Visible)
			{
				ctrGaugeAssigner.PopulateControl(Resource);
			}
			ctrGaugesAssigner.Visible &= (config & EditConfig.ShowGaugesAssigner) > 0;
			if (ctrGaugesAssigner.Visible)
			{
				ctrGaugesAssigner.PopulateControl(Resource);
			}

			//Statistic data setting
			ctrSDContainer.Visible = (config & EditConfig.ShowStatisticLabel) > 0;
			if (ctrSDContainer.Visible)
			{
				var statisticDataCustomLabel = Resource.Properties["StatisticDataCustomLabel"];
				ctrCustomLabelSD.Text = !String.IsNullOrEmpty(statisticDataCustomLabel) ? statisticDataCustomLabel : "";
			}

			InitMultiValueSelection();
		}

		ctrMaxScale.Text = Resource.Properties["GaugeMaxUnitsScale"];
		if (ctrMaxScale.Text.IsNotValid())
		{
			ctrMaxScale.Text = "2500";
		}
		ctrUnitsValue.Text = Resource.Properties["GaugeUnitsValue"];
		if (ctrUnitsValue.Text.IsNotValid())
		{
			ctrUnitsValue.Text = "1";
		}
		ctrUnitsLabel.Text = Resource.Properties["GaugeUnitsLabel"];

		scaleInput.Text = Resource.Properties["Scale"];
		if (scaleInput.Text.IsNotValid())
		{
			scaleInput.Text = "100";
		}

        var allGaugesStyles = GaugeHelper.GetAllGaugeStyles(GaugeType);

	    foreach (var gaugeStyle in allGaugesStyles)
	    {
            stylesList.Items.Add(new ListItem(GetStyleText(gaugeStyle), gaugeStyle));
	    }

		HtmlHelper.SetListSelectedValue(stylesList, Resource.Properties["Style"], this.GaugeType == GaugeType.Linear ? DefaultLinearGaugeStyle : DefaultRadialGaugeStyle);

        SelectedStyle = stylesList.SelectedValue;

        int scaleNum;
        if (!Int32.TryParse(scaleInput.Text, out scaleNum)) { scaleNum = 100; }

		foreach (ListItem gaugeStyle in stylesList.Items)
		{
            if (GaugeType == GaugeType.Linear)
		    {
                var webImage = new Image
				{
					ImageUrl = String.Format("/Orion/NetPerfMon/Gauge.aspx?Style={0}&Property={2}&GaugeType=Linear&Units=&Min=0&Max=100&Scale={1}&WarningThreshold=60&ErrorThreshold=80&Value=35&", gaugeStyle.Value, scaleNum, gaugeStyle.Text),
					CssClass = "gauge"
				};
                webImage.Attributes.Add("stylename", gaugeStyle.Value);
                webImage.Attributes.Add("onclick", "LinearGaugeSelectMe(this); return false;");

                var panel = new Panel { Width = 500, Height = Convert.ToInt32(scaleNum*1.2)};
                panel.Controls.Add(webImage);
                
                GaugeStylesPanel.Controls.Add(panel);
			}
			else
			{
                BaseGauge gauge = new Gauge { GaugeStyle = gaugeStyle.Value, IsSample = true, BottomTitle = gaugeStyle.Text};
			    GaugeStylesPanel.Controls.Add(gauge);
			}
		}
	}

    protected void CtrAssigner_OnSelectedNameChanged(object sender, EventArgs e)
    {
        InitMultiValueSelection();
    }

	protected void OnUnits_ServerValidate(Object source, ServerValidateEventArgs args)
	{
		long units, maxUnits;

		args.IsValid = long.TryParse(ctrMaxScale.Text, out maxUnits);
		if (!args.IsValid)
		{
            ctrUnitsValidator.ErrorMessage = Resources.APMWebContent.APMWEBCODE_AK1_150;
			return;
		}
		args.IsValid = maxUnits >= 10 && maxUnits <= 10000;
		if (!args.IsValid)
		{
            ctrUnitsValidator.ErrorMessage = Resources.APMWebContent.APMWEBCODE_AK1_151;
			return;
		}
		args.IsValid = long.TryParse(args.Value, out units) && units > 0;
		if (!args.IsValid)
		{
            ctrUnitsValidator.ErrorMessage = Resources.APMWebContent.APMWEBCODE_AK1_152;
			return;
		}
	}

	protected void SubmitClick(Object sender, EventArgs e)
	{
		if (Page.IsValid)
		{
			var prors = Resource.Properties;
			if (ctrGaugeAssigner.Visible)
			{
				ctrGaugeAssigner.PopulateResource(Resource);
			}
			if (ctrGaugesAssigner.Visible)
			{
				ctrGaugesAssigner.PopulateResource(Resource);
			}
			Resource.Title = resourceTitleEditor.ResourceTitle;
			Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;

			if (ctrSDContainer.Visible)
			{
				Resource.Properties["StatisticDataCustomLabel"] = ctrCustomLabelSD.Text;
			}
			prors["GaugeMaxUnitsScale"] = ctrMaxScale.Text;
			prors["GaugeUnitsValue"] = ctrUnitsValue.Text;
			prors["GaugeUnitsLabel"] = ctrUnitsLabel.Text;

			prors["Scale"] = scaleInput.Text;
			prors["Style"] = stylesList.SelectedValue;

            MultiValueSelection.UpdateSettings(this.Resource);

			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

            // After submit we return to original view (the same behavior as Orion Core edit resource pages)
            // Note: ReturnUrl can be different than URL of view (e.g. when editing Custom Object Resource)
            string originalReturnUrl = SolarWinds.Orion.Web.Helpers.UrlHelper.FromSafeUrlParameter(Request.QueryStringOrForm("ReturnUrl"));
            string url = GetViewUrl(this.Resource.View.ViewID, GetOriginalNetObject(originalReturnUrl));
			Response.Redirect(url);
		}
	}

    #endregion

	#region Helper Members

	protected void InitMultiValueSelection()
	{
		MultiValueSelection.LoadData(ctrGaugeAssigner.SelectedComponent);
		MultiValueSelection.InitializeSettings(this.Resource);
	}
    
    private string GetStyleText(string style)
    {
        return Resources.APMWebContent.ResourceManager.GetString(ResourceManagerRegistrar.Instance.CleanResxKey("GaugeStyle", style)) ?? style;
    }

    private string GetOriginalNetObject(string returnUrl)
    {
        var uri = new Uri(Page.Request.Url, returnUrl);
        var collection = System.Web.HttpUtility.ParseQueryString(uri.Query);
        return collection["NetObject"];
    }

    private string GetViewUrl(int viewId, string netObjectId)
    {
        string url = string.Format("/Orion/View.aspx?ViewID={0}", viewId);
        if (!string.IsNullOrEmpty(netObjectId))
            url = string.Format("{0}&NetObject={1}", url, netObjectId);
        return url;
    }

	#endregion
}