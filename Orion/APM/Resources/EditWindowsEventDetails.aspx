﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" EnableEventValidation="false" CodeFile="EditWindowsEventDetails.aspx.cs" Inherits="Orion_APM_Resources_EditWindowsEventDetails" MasterPageFile="~/Orion/ResourceEdit.master"%>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="apm" TagName="EditWindowsEventDetails" Src="~/Orion/APM/Controls/EditResourceControls/EditWindowsEventDetails.ascx" %>

<asp:Content ID="ctrBody" runat="server" ContentPlaceHolderID="MainContent">
	
	<h1><%= string.Format(Resources.APMWebContent.APMWEBCODE_VB1_129, this.Resource.Title)%>
	</h1>
	<div>
		<orion:EditResourceTitle ID="ctrTitleEditor"  ShowSubTitle="true" ShowSubTitleHintMessage="false" runat="server" />
        
		<apm:EditWindowsEventDetails ID="eventLogEditControl" runat="server" />

		<div>
               <orion:LocalizableButton ID="ctrSubmit" OnClick="OnSubmit_Click" runat="server" LocalizedText="Submit" DisplayType="Primary" />
		</div>
	</div>
</asp:Content>

