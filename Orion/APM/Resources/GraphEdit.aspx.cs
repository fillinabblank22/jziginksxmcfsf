using System;
using System.Web.UI.WebControls;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.APM.Web.Utility;
using System.Collections.Generic;
using System.Collections;
using SolarWinds.APM.Web.Extensions;

public partial class Resources_GraphEdit : System.Web.UI.Page
{
	#region Properties

	private ResourceInfo _resource;
	protected ResourceInfo Resource
	{
		get
		{
			if (_resource == null)
			{
				Int32 id;
				if (Int32.TryParse(Request.QueryStringOrForm("ResourceID"), out id))
				{
					_resource = ResourceManager.GetResourceByID(id);
				}
			}
			return _resource;
		}
	}

    private bool IsDataSourceTypeSupported
    {
        get
        {
            var dataSourceSupportString = Request.Params["DataSourceSupported"];
            return !String.IsNullOrEmpty(dataSourceSupportString) && Boolean.Parse(dataSourceSupportString);
        }
    }

    private bool IsShortYAxisSupported
    {
        get
        {
            var isShortYAxisSupported = Request.Params["ShowYAxisFromZero"];
            return !String.IsNullOrEmpty(isShortYAxisSupported) && Boolean.Parse(isShortYAxisSupported);
        }
    }

    #endregion

	#region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
	{
        ctrAssigner.OnSelectedNameChanged += new EventHandler(CtrAssigner_OnSelectedNameChanged);

		if (!Page.IsPostBack)
		{
			ResourceTitleEditor.ResourceTitle = Resource.Title;

			ctrAssigner.PopulateControl(Resource);

            foreach (var period in Periods.GenerateSelectionList())
		    {
                TimePeriodsList.Items.Add(new ListItem(GetLocalizedProperty("Period", period), period));
		    }

			SampleSizeList.DataSource = SampleSizes.GenerateSelectionList();
			SampleSizeList.DataBind();

			HtmlHelper.SetListSelectedValue(TimePeriodsList, _resource.Properties[ChartInfo.KEYS.Period], ChartInfo.DefaultPeriod);
			HtmlHelper.SetListSelectedValue(SampleSizeList, _resource.Properties[ChartInfo.KEYS.SampleSize], ChartInfo.DefaultSampleSize);

            if(IsShortYAxisSupported)
            {
                YAxisConfigurationCheckBox.Visible = true;
                bool isChecked = true;
                if (bool.TryParse(Resource.Properties[ChartInfo.KEYS.ShowYAxisFromZero], out isChecked))
                {
                    ShowYAxisFromZero.Checked = isChecked;
                }
            }

		    if (IsDataSourceTypeSupported)
			{
				DataSourceTypeEditPanel.Visible = true;

				DataSourceTypeList.DataSource = DataSourceTypes.GenerateSelectionList();
				DataSourceTypeList.DataBind();
				HtmlHelper.SetListSelectedValue(DataSourceTypeList, _resource.Properties[ChartInfo.KEYS.DataSourceType], ChartInfo.DefaultDataSourceType);
			}

		    InitMultiValueSelection();
            InitSingleValueSelection();
		}
	}

    protected void InitMultiValueSelection()
    {
		MultiValueSelection.LoadData(ctrAssigner.SelectedComponent);
		MultiValueSelection.InitializeSettings(this.Resource);
    }

    protected void InitSingleValueSelection()
    {
		SingleValueSelection.LoadData(ctrAssigner.SelectedComponent);
		SingleValueSelection.InitializeSettings(this.Resource);
    }

	protected void SubmitButton_Click(object sender, EventArgs e)
	{
		if (Page.IsValid) 
		{
			if (ctrAssigner.Visible)
			{
				ctrAssigner.PopulateResource(Resource);
			}

			Resource.Title = ResourceTitleEditor.ResourceTitle;

			Resource.Properties[ChartInfo.KEYS.SampleSize] = SampleSizeList.SelectedValue;
			Resource.Properties[ChartInfo.KEYS.Period] = TimePeriodsList.SelectedValue;

            if(IsShortYAxisSupported)
            {
                Resource.Properties[ChartInfo.KEYS.ShowYAxisFromZero] = ShowYAxisFromZero.Checked.ToString();
            }

			if (IsDataSourceTypeSupported)
			{
				Resource.Properties[ChartInfo.KEYS.DataSourceType] = DataSourceTypeList.SelectedValue;

				AssignedToResourceInfo.Update(Request, Resource, new[] { new DictionaryEntry(AssignedToResourceInfo.GraphDataSourceType, DataSourceTypeList.SelectedValue) });
			}

            MultiValueSelection.UpdateSettings(this.Resource);
            SingleValueSelection.UpdateSettings(this.Resource);

			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

            string originalReturnUrl = SolarWinds.Orion.Web.Helpers.UrlHelper.FromSafeUrlParameter(Request.QueryStringOrForm("ReturnUrl"));
            string url = GetViewUrl(this.Resource.View.ViewID, GetOriginalNetObject(originalReturnUrl));
            Response.Redirect(url);
        }
	}

	protected void OnPeriodValidator_ServerValidate(Object source, ServerValidateEventArgs args)
	{
		var size = SampleSizes.FindSampleSizeBySizeName(SampleSizeList.SelectedValue);
		if (args.IsValid = (size != null))
		{
			String name = TimePeriodsList.SelectedValue;
			DateTime begin = DateTime.Now, end = DateTime.Now;

			Periods.Parse(ref name, ref begin, ref end);
			args.IsValid = ((end - begin).TotalMinutes - size.Minutes > 0);
		}
		ctrPeriodValidator.ErrorMessage = String.Empty;
		if (!args.IsValid) 
		{
            ctrPeriodValidator.ErrorMessage = Resources.APMWebContent.APMWEBCODE_VB1_149;
		}
	}

    protected void CtrAssigner_OnSelectedNameChanged(object sender, EventArgs e)
    {
        InitMultiValueSelection();
        InitSingleValueSelection();
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    private string GetOriginalNetObject(string returnUrl)
    {
        var uri = new Uri(Page.Request.Url, returnUrl);
        var collection = System.Web.HttpUtility.ParseQueryString(uri.Query);
        return collection["NetObject"];
    }

    private string GetViewUrl(int viewId, string netObjectId)
    {
        string url = string.Format("/Orion/View.aspx?ViewID={0}", viewId);
        if (!string.IsNullOrEmpty(netObjectId))
            url = string.Format("{0}&NetObject={1}", url, netObjectId);
        return url;
    }

	#endregion
}
