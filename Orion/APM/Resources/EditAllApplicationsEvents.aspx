<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
    CodeFile="EditAllApplicationsEvents.aspx.cs" Inherits="EditAllApplicationsEvents" %>

<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        <%= Resources.APMWebContent.APMWEBDATA_AK1_168 %>
    </h1>
    
    <br />
    <b><%= Resources.APMWebContent.APMWEBDATA_AK1_169 %></b><br /><br />
    <asp:DropDownList runat="server" ID="timePeriod" Width = "170px" >
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_past_hour %>" Value="0" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_last_2_hours %>" Value="1" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_last_24_hours %>" Value="2" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_today %>" Value="3" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_yesterday %>" Value="4" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_last_7_days %>" Value="5" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_this_months %>" Value="6" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_last_30_days %>" Value="7" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_last_month %>" Value="8" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_last_2_months %>" Value="9" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_this_year %>" Value="10" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_last_12_months %>" Value="11" />
        <asp:ListItem Text="<%$ Resources: APMWebContent, TimePeriod_all_time %>" Value="12"  />
    </asp:DropDownList><br />
    <br />
    <orion:LocalizableButton runat="server" ID="btnSubmit" CausesValidation="true" OnClick="SubmitClick"  LocalizedText="Submit" DisplayType="Primary" />
</asp:Content>
