﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiTabResource.ascx.cs" Inherits="Orion_APM_Resources_MultiTabResource" %>

<orion:Include runat="server" Module="APM" File="Resources.css" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="update" ChildrenAsTriggers="True">
            <ContentTemplate>
                <div class="tabHeaders">
                    <asp:PlaceHolder runat="server" ID="phTabHeaders"></asp:PlaceHolder>
                </div>
                <div class="tabContents">
                    <asp:PlaceHolder runat="server" ID="phTabContents"></asp:PlaceHolder>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
