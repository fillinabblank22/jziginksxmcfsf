﻿using System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Groups_WebUserControl : ContainerBaseResource
{
    private const string SwqlQuery = @"
SELECT apt.Name
FROM Orion.APM.ApplicationTemplate apt
    JOIN Orion.APM.TemplateGroupAssignment tga
        ON apt.ApplicationTemplateID = tga.TemplateID
WHERE tga.GroupID = {0}";

    private const string SwqlCheckQuery = @"
SELECT COUNT(TemplateID) AS TemplatesCount
FROM Orion.APM.TemplateGroupAssignment
WHERE GroupID = {0}";

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.ApplicationTemplateAssignment_ResourceDefaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAssignTemptoGroupRes"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var queryResult = swis.Query(InvariantString.Format(SwqlCheckQuery, Container.Id));
            if ((int)queryResult.Rows[0]["TemplatesCount"] == 0)
            {
                Wrapper.Visible = false;
                return;
            }
        }

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = InvariantString.Format(SwqlQuery, Container.Id);
    }
}