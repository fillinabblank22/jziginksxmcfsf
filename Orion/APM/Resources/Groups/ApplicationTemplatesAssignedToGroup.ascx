﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationTemplatesAssignedToGroup.ascx.cs" Inherits="Orion_APM_Resources_Groups_WebUserControl" %>
<%@Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="CssResources" runat="server" Module="APM" File="Resources.css"/>
        <orion:Include ID="JsResources" runat="server" Module="APM" File="APM.js" />
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: "zebraGrid",
                        rowsPerPage: 300,
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        customHeaderGenerator: function() {
                            return null;
                        }
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>

         <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var templateName = [Columns[0]];
            #}   
            <tr>
                <td class="iconRow"><img src="/Orion/APM/images/Admin/icon_application_template.gif"></td>
                <td><span>{{ templateName }}</span></td>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>