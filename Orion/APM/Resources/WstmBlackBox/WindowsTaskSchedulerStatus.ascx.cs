﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.APM.BlackBox.Wstm.Common;
using SolarWinds.APM.BlackBox.Wstm.Web.DAL;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.TaskScheduler.Contract.Enums;
using i18nExternalized = Resources.APM_WstmBBContent;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_WstmBlackBox_WindowsTaskSchedulerStatus : BaseResourceControl
{
    #region Constants

    private const string SwqlBaseQuery = @"
SELECT
    Name,
    State,
    ToLocal(LastRunTime) as [LastRunTime],
    LastRunResult AS [_lastRunResult],
    '{0}' AS [_IconForStatus],
    ID AS [_Id],
    Author AS [_Autor],
    ToLocal(DateOfCreation) AS [_DateOfCreation],
    LastRunResult AS [lastRunResultIcon]
FROM Orion.APM.Wstm.Task
WHERE NodeID={1}";

    private const string SwqlBaseQueryTrailer =
        @" AND Name LIKE '%${SEARCH_STRING}%'";

    private const string SearchControlPath = @"~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx";

    protected const string ImageLinkFormatString = @"/Orion/StatusIcon.ashx?entity={0}&status={1}";

    protected static readonly string TaskStateMapJson = GetLocalizedEnumJson<TaskState>("TaskState");
    protected static readonly string TaskResultCodeMapJson = GetLocalizedEnumJson<TaskResultCode>("TaskResultCode");

    private static string GetLocalizedEnumJson<TEnum>(string prefix)
    {
        var enumValues = Enum.GetValues(typeof (TEnum)).OfType<TEnum>();

        return string.Format(
            CultureInfo.InvariantCulture,
            "{{{0}}}",
            string.Join(
                ", ",
                enumValues.Select(v => GetEnumLabelAsJson(prefix, v))
            )
        );
    }

    private static string GetEnumLabelAsJson<TEnum>(string prefix, TEnum value)
    {
        return string.Format(
            CultureInfo.InvariantCulture,
            "\"{0}\": \"{1}\"",
            ConvertEnumToInt(value),
            GetLocalizedLabel(prefix, value.ToString())
        );
    }

    private static int ConvertEnumToInt<TEnum>(TEnum value)
    {
        return value.GetHashCode();
    }

    protected static string GetLocalizedLabel(string prefix, string value)
    {
        return i18nExternalized.ResourceManager.GetString(InvariantString.Format("{0}_{1}", prefix, value)) ?? value;
    }

    #endregion

    #region BaseResourceControl properties

    protected override string DefaultTitle
    {
        get { return Resources.APM_WstmBBContent.WinTaskSchedulerStatus_MainResourceTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppWSTMDescription"; }
    }

    protected string UnknownCodeLinkFragment
    {
        get { return "SAMAGWTS"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    #endregion

    #region Fields and Properties

    protected ResourceSearchControl SearchControl { get; private set; }

    protected int NodeID { get; private set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        ApmBaseResource.EnsureApmScripts();

        NodeID = GetInterfaceInstance<INodeProvider>().Node.NodeID;

        if (!new WindowsTaskSchedulerStatusDAL().IsWSTMAssigned(NodeID))
        {
            Wrapper.Visible = false;
            return;
        }

        CustomTable.UniqueClientID = Resource.ID;

        SearchControl = (ResourceSearchControl)LoadControl(SearchControlPath);
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        CustomTable.SWQL = InvariantString.Format(
            SwqlBaseQuery,
            InvariantString.Format(
                ImageLinkFormatString,
                SwisEntities.Task,
                "' + ToString(State) + '"),
            NodeID);

        CustomTable.SearchSWQL = CustomTable.SWQL + SwqlBaseQueryTrailer;
    }
}
