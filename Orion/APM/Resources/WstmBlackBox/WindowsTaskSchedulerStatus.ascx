﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WindowsTaskSchedulerStatus.ascx.cs" Inherits="Orion_APM_Resources_WstmBlackBox_WindowsTaskSchedulerStatus" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/APMCustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<%@ Import Namespace="SolarWinds.APM.BlackBox.Wstm.Common" %>
<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.TaskScheduler.Contract.Enums" %>
<%@ Import Namespace="Resources" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="StyleInclude" runat="server" File="APM/WstmBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSInclude" runat="server" File="APM/WstmBlackBox/js/Resources.js"/>
        <orion:Include ID="Include1" runat="server" File="APM/js/Charts/Charts.APM.Chart.Formatters.js"/>
        <div class="wstmApplicationError" id="wstmApplicationErrorID_<%= Resource.ID %>"></div>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
<script type="text/javascript">
// common page functionality
APMjs.initGlobal(
    'SW.APM.ControlsInline.WindowsTaskSchedulerStatus',
    function (WTSS) {
        var taskStateMap, taskResultCodeMap, Resources,
            SF = APMjs.format;

        taskStateMap = JSON.parse('<%= TaskStateMapJson %>');
        taskResultCodeMap = JSON.parse('<%= TaskResultCodeMapJson %>');

        Resources = {
            AppErrorInline: '<div class="status-details-container" status="Undefined"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><label>{0}</label></div>',
            PollingCredErrMsg: '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.APMWEBCODE_YP0_2) %>',
            FooterMsg: '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.APMWEBCODE_YP0_5) %>',
            SearchTextWatermark: '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.APMWEBCODE_YP0_6) %>',
            LoadErrMsg: '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.APMWEBCODE_ResourceLoadErrMsg) %>',
            PollInProgressErrMsg: '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.APMWEBCODE_PollInProgressErrMsg) %>',
            NoDataMsg: '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.APMWEBCODE_NoDataMsg) %>'
        };

        function updateResourceData(resourceId) {
            $.ajax({
                type: 'POST',
                url: '/Orion/APM/WstmBlackBox/Services/WindowsTaskSchedulerStatusSvc.asmx/GetApplicationPollingData',
                data: '{ nodeId: <%= NodeID %> }',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                async: false,
                success: function (result) {
                    var err, $footer,
                        data = result.d;

                    function isNullOrEmpty(value) {
                        return typeof value !== 'string' || value === '';
                    }

                    function formatNonEmptyAsLocalTime(value) {
                        if (isNullOrEmpty(value)) {
                            return '';
                        }
                        return SW.Core.Charts.dataFormatters.getFriendlyDateString(
                            SW.APM.WSTMBB.Resources.Helpers.utcStrToLocalTime(value)
                        );
                    }

                    err = data.ApplicationError || (data.ComponentErrors && data.ComponentErrors[0]);
                    if (!isNullOrEmpty(err)) {
                        err = SF(Resources.AppErrorInline, SF(Resources.PollingCredErrMsg, err));
                        $(SF('#wstmApplicationErrorID_{0}', resourceId))
                            .html(err);
                    }

                    $footer = $(SF('#wstmResourceFooterID_{0}', resourceId));

                    $footer
                        .find('.wstmErrorMsg')
                        .empty();

                    $footer
                        .find('.wstmLastPolled')
                        .html(SF('<%= APM_WstmBBContent.APMWEBCODE_YP0_3 %>', formatNonEmptyAsLocalTime(data.LastSuccessfulPollStr)));

                    $footer
                        .find('.wstmNextPoll')
                        .html(SF('<%= APM_WstmBBContent.APMWEBCODE_YP0_4 %>', formatNonEmptyAsLocalTime(data.NextPollStr)));

                    $footer
                        .find('.wstmEditPollingSettingsLink')
                        .html(
                            SF(
                                '<a href="{0}">{1}</a>',
                                data.EditPollingSettingsLink,
                                Resources.FooterMsg
                            )
                        );
                },
                error: function() {
                    var footerId = SF('#wstmResourceFooterID_{0}', resourceId);

                    $(SF('{0} > span', footerId))
                        .empty();

                    $(SF('{0} .wstmErrorMsg', footerId))
                        .html(
                            SF(
                                Resources.AppErrorInline,
                                Resources.LoadErrMsg
                            )
                        );
                }
            });
        }

        function isEmptyTag(selector) {
            return $(selector).is(':empty');
        }

        function isErrorMode(resourceId) {
            return !isEmptyTag(SF('#wstmApplicationErrorID_{0}', resourceId));
        }

        function isInitialPollInProgress(resourceId) {
            return isEmptyTag(SF('#wstmResourceFooterID_{0} .wstmLastPolled', resourceId))
                && isEmptyTag(SF('#wstmResourceFooterID_{0} .wstmErrorMsg' , resourceId))
                && !isErrorMode(resourceId);
        }

        function getWinTaskResultStyle(resultCode, lastRunTime){
            var resultStyle;
            $.ajax({
                type: 'POST',
                url: '/Orion/APM/WstmBlackBox/Services/WindowsTaskSchedulerStatusSvc.asmx/GetWinTaskResultStyle',
                data: '{ resultCode: ' + resultCode + ', hasLastRunTime: ' + !lastRunTime + ' }',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                async: false,
                cache: true,
                success: function (result) {
                    resultStyle = result.d;
                },
                error: function () {
                    resultStyle = '';
                }
            });
            return resultStyle;
        }

        function getWinTaskResultIcon(resultCode, lastRunTime) {
            var resultIcon;
            $.ajax({
                type: 'POST',
                url: '/Orion/APM/WstmBlackBox/Services/WindowsTaskSchedulerStatusSvc.asmx/GetWinTaskResultIcon',
                data: '{ resultCode: ' + resultCode + ', hasLastRunTime: ' + !lastRunTime + ' }',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                async: false,
                cache: true,
                success: function (result) {
                    resultIcon = result.d;
                },
                error: function () {
                    resultIcon = 'result_unknown.png';
                }
            });
            return SF(
                '/Orion/APM/WstmBlackBox/Images/LastRunResult/{0}',
                resultIcon
            );
        }

        function getWinTaskResultMsg(resultCode) {
            var lastRunResult, template;
            lastRunResult = taskResultCodeMap[resultCode] || '';
            template = lastRunResult
                ? '({0}) {1}'
                : '<a href="<%= HelpHelper.GetHelpUrl(UnknownCodeLinkFragment) %>" target="_blank">({0})</a>';
            return SF(
                template,
                SW.APM.WSTMBB.Resources.Helpers.decimalToHex(resultCode),
                lastRunResult
            );
        }

        function getWinTaskStateMsg(state) {
            var taskState = taskStateMap[state] || '';
            return taskState;
        }

        // publish methods for template

        WTSS.getWinTaskResultIcon = getWinTaskResultIcon;
        WTSS.getWinTaskResultMsg = getWinTaskResultMsg;
        WTSS.getWinTaskResultStyle = getWinTaskResultStyle;
        WTSS.getWinTaskStateMsg = getWinTaskStateMsg;
        WTSS.isErrorMode = isErrorMode;
        WTSS.isInitialPollInProgress = isInitialPollInProgress;
        WTSS.Resources = Resources;
        WTSS.taskStateMap = taskStateMap;
        WTSS.updateResourceData = updateResourceData;
    }
);

// initialization needed per resource instance
$(function () {
    var customTableId = '<%= CustomTable.ClientID %>',
        resourceId = <%= Resource.ID %>,
        searchTextBoxId = '<%= SearchControl.SearchBoxClientID %>',
        searchButtonId = '<%= SearchControl.SearchButtonClientID %>',
        searchBoxClientId = '<%= SearchControl.SearchBoxClientID %>',
        WTSS = SW.APM.ControlsInline.WindowsTaskSchedulerStatus,
        SF = APMjs.format;

    // initialize CustomQueryTable
    $(function () {

        function refresh() {
            SW.APM.Resources.CustomQuery.refresh(resourceId);
        }

        SW.APM.Resources.CustomQuery.initialize({
            uniqueId: <%= Resource.ID %>,
            initialPage: 0,
            rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
            allowSort: true,
            allowPaging: true,
            allowSearch: true,
            hideShowAll: false,
            cls: 'Wstm',
            searchTextBoxId: searchTextBoxId,
            searchButtonId: searchButtonId,
            explicitZebra: true,
            headerTitles: [
                '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.WinTaskSchedulerState_TaskName) %>',
                '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.WinTaskSchedulerState_State) %>',
                '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.WinTaskSchedulerState_LastRunTime) %>',
                '<%= ControlHelper.EncodeJsString(APM_WstmBBContent.WinTaskSchedulerState_LastRunResult) %>'
            ],
            underscoreTemplateId: SF('#contentTemplate-{0}', resourceId),
            onbeforeload: function () {
                WTSS.updateResourceData(resourceId);
            },
            onloaded: function ($grid) {
                var htmlPollInprogress, htmlRow, htmlNoData;
                if ($grid.find('tr').length <= 2) {
                    if (WTSS.isInitialPollInProgress(resourceId)) {
                        if (!$grid.find('.wstmInitialPollMsg').length) {
                            htmlPollInprogress = SF(
                                Resources.AppErrorInline,
                                Resources.PollInProgressErrMsg
                            );
                            htmlRow = SF(
                                '<tr><td colspan="5">{0}</td></tr>',
                                htmlPollInprogress
                            );
                            $(htmlRow)
                                .addClass('wstmInitialPollMsg')
                                .appendTo($grid);
                        }
                    } else {
                        if(!$grid.find('.wstmNoDataMsg').length) {
                            htmlNoData = SF(
                                '<tr><td colspan="5"><div>{0}</div></td></tr>',
                                WTSS.Resources.NoDataMsg
                            );
                            $(htmlNoData)
                                .addClass('wstmNoDataMsg')
                                .appendTo($grid);
                        }
                    }
                } else {
                    $grid.find('.wstmInitialPollMsg').remove();
                    $grid.find('.wstmNoDataMsg').remove();
                }
            }
        });

        $(SF('#{0}', searchBoxClientId))
            .attr('placeholder', WTSS.Resources.SearchTextWatermark);

        SW.Core.View.AddOnRefresh(refresh, customTableId);

        refresh();
    });

    // Tooltip event handler assignment

    $(SF('#Grid-' + resourceId)).on(
        'hover',
        'td.wstmName span',
        function (e) {
            if (e.type === 'mouseenter') {
                SW.APM.WSTMBB.Resources.Tooltip.showTooltip(resourceId, this);
            } else {
                SW.APM.WSTMBB.Resources.Tooltip.hideTooltip();
            }
        }
    );
    SW.APM.TrimStatusDetails();
});
</script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var stateIcon, state, lastRunTime, lastRunResult,
                    resourceId = <%= Resource.ID %>,
                    WTSS = SW.APM.ControlsInline.WindowsTaskSchedulerStatus;

                if (WTSS.isErrorMode(resourceId)) {
                    stateIcon = '<%= ControlHelper.EncodeJsString(InvariantString.Format(ImageLinkFormatString, SwisEntities.Task, (int) TaskState.Unknown)) %>';
                    state = WTSS.getWinTaskStateMsg('<%= (int) TaskState.Unknown %>');
                } else {
                    stateIcon = Columns[4];
                    state = WTSS.getWinTaskStateMsg(Columns[1]);
                }

                lastRunTime = SW.Core.Charts.dataFormatters.getFriendlyDateString(Columns[2]);
                lastRunResult = WTSS.getWinTaskResultMsg(Columns[3]);
                taskResultStyle = WTSS.getWinTaskResultStyle(Columns[3], Columns[2]);
            #}
            <tbody class="zebraRow">
                <tr class="wstmTaskInfo_{{Columns[5]}} {{taskResultStyle}}">
                    <td class="wstmName">
                        <span>{{ SW.APM.WSTMBB.Resources.Helpers.getShortTaskName(resourceId, [Columns[0]]) }}</span>
                    </td>
                    <td class="wstmState">
                        <img class="wstmStateImg" src="{{ stateIcon }}"/>
                        <span class="wstmStateText">{{ state }}</span>
                    </td>
                    <td class="wstmLastRunTime"><span>{{ lastRunTime }}</span></td>
                    <td class="wstmLastRunResultIcon"><img src="{{ WTSS.getWinTaskResultIcon(Columns[3], Columns[2]) }}"/></td>
                    <td class="wstmAuthor" style="display:none;"><span>{{ !Columns[6] ? "" : Columns[6] }}</span></td>
                    <td class="wstmCreationTime" style="display:none;"><span>{{ Columns[7] == null ? "" : Columns[7].toLocaleString() }}</span></td>
                </tr>
                <tr class="wstmTaskLastResult {{ taskResultStyle }}">
                    <td class="wstmLastRunResult" colspan="4"><span><%= InvariantString.Format(APM_WstmBBContent.APMWEBCODE_YP0_1, "{{ lastRunResult }}") %></span></td>
                </tr>
            </tbody>
        </script>
        <div class="wstmResourceFooter" id="wstmResourceFooterID_<%= Resource.ID %>">
            <span class="wstmErrorMsg"></span>
            <span class="wstmLastPolled"></span>
            <span class="wstmNextPoll"></span>
<% if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin) { %>
            <asp:PlaceHolder ID="phPollingSettingsLink" runat="Server">
                <span class="wstmEditPollingSettingsLink"></span>
            </asp:PlaceHolder>
<% } %>   
        </div>
        <div class="wstmTaskTooltip" id="wstmTaskTooltipID_<%= Resource.ID %>">
            <div id="wstmTooltipHeader"></div>
            <table>
                <tr>
                    <td><b><%= APM_WstmBBContent.APMWEBCODE_YP0_7 %></b>&nbsp;</td>
                    <td id="wstmTooltipState"></td>
                </tr>
                <tr>
                    <td><b><%= APM_WstmBBContent.APMWEBCODE_YP0_9 %></b>&nbsp;</td>
                    <td id="wstmTooltipAuthor"></td>
                </tr>
                <tr>
                    <td><b><%= APM_WstmBBContent.APMWEBCODE_YP0_10 %></b>&nbsp;</td>
                    <td id="wstmTooltipCreationTime"></td>
                </tr>
            </table>
            <div class="wstmTooltipArrow"></div>
        </div>
    </Content>
</orion:resourceWrapper>
