using System;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_MockupResourceContainer : UserControl
{
    private const string AllowAccessProfileProperty = "AllowAdmin";

    private ApplicationTemplate applicationTemplate;
    public ApplicationTemplate ApplicationTemplate
    {
        get
        {
            if (applicationTemplate == null)
            {
                if (!String.IsNullOrEmpty(Request.QueryStringOrForm("FromThwackPreview")) && Boolean.Parse(Request.QueryStringOrForm("FromThwackPreview")))
                {
                    object propertyValue = Context.Profile.GetPropertyValue(AllowAccessProfileProperty);

                    bool allowAccess;
                    Boolean.TryParse(propertyValue.ToString(), out allowAccess);

                    if (!allowAccess)
                    {
                        throw new UnauthorizedAccessException(APMWebContent.Web_NotAuthorizedMessage);
                    }

                    var cred = Session["ThwackCredential"] as NetworkCredential;
                    if (cred == null)
                    {
                        throw new UnauthorizedAccessException(APMWebContent.Web_ThwackAuthError);
                    }

                    try
                    {
                        applicationTemplate = ThwackHelper.LoadTemplateFromThwack(ThwackHelper.ThwackDownloadUrl, int.Parse(Request.QueryStringOrForm("AppTemplateID")), cred);
                    }
                    catch (WebException ex)
                    {
                        var response = ex.Response as HttpWebResponse;
                        if (response != null)
                        {
                            if (response.StatusCode == HttpStatusCode.InternalServerError)
                            {
                                throw new UnauthorizedAccessException(APMWebContent.Web_ThwackAuthFailed);
                            }
                        }
                        throw;
                    }
                }
                else
                {
					var tplId = Request.QueryStringOrForm("AppTemplateID");
					if (!string.IsNullOrWhiteSpace(tplId)) 
					{
						using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
						{
							applicationTemplate = bl.GetApplicationTemplate(int.Parse(tplId));
						}
					}
                }
            }
            return applicationTemplate;
        }
    }

    private ViewAndResources viewInfos;
    public ViewAndResources ViewInfos
    {
        get
        {
            if (viewInfos == null)
            {
				var tpl = ApplicationTemplate;

                var useDefaultViewId = Request.QueryStringOrForm("UseDefaultViewID") != null && Convert.ToBoolean(Request.QueryStringOrForm("UseDefaultViewID"));
				if (useDefaultViewId || tpl == null)
                {
                    var viewInfo = ViewManager.GetViewById(ApmApplication.DefaultViewID);
                    if (viewInfo != null)
                    {
                        var resources = ViewManager.GetResourcesForView(viewInfo);
                        viewInfos = new ViewAndResources(viewInfo, resources);
                    }
                }
                if (viewInfos == null)
                {
					if (tpl != null && tpl.HasImportedView)
					{
						viewInfos = ApmViewManager.GetBaseViewFromApplicationTemplate(tpl);                
					}
					else 
					{
                        if (tpl != null && tpl.ViewId > 0)
                        {
                            var viewInfo = ViewManager.GetViewById(tpl.ViewId);
                            if (viewInfo != null)
                            {
                                var resources = ViewManager.GetResourcesForView(viewInfo);
                                viewInfos = new ViewAndResources(viewInfo, resources);
                            }

                        }
                        else
                        {
                            throw new Exception(APMWebContent.Web_UnknownViewType);  
                        }		
					}
                }
            }
            return viewInfos;
        }
    }

	public override void DataBind()
	{
        var viewInfo = ViewInfos.ViewInfo;
	    var resources = ViewInfos.Resources;

        rptColumn1.DataSource = resources.GetResourcesForColumn(1);

		if (viewInfo.ColumnCount > 1)
		{
			rptColumn2.DataSource = resources.GetResourcesForColumn(2);

			if (viewInfo.ColumnCount > 2)
			{
				rptColumn3.DataSource = resources.GetResourcesForColumn(3);
			}
			else
			{
				tdColumn3.Visible = false;
			}
		}
		else
		{
			tdColumn2.Visible = false;
		}

		base.DataBind();
	}

	protected void ResourcePlaceHolder_DataBind(object sender, EventArgs e)
	{
		var placeholder = (PlaceHolder)sender;
		var rInfo = (ResourceInfo)DataBinder.Eval(placeholder.NamingContainer, "DataItem");

		if (rInfo.IsAspNetControl)
		{
		    var control = (BaseResourceControl)LoadControl("/Orion/APM/Resources/MockupResource.ascx");
            control.Resource = rInfo;

		    try
            {
                placeholder.Controls.Add(control);
            }
            catch (Exception ex)
            {
                var exceptionHandler = control.CreateExceptionHandlerControl(ex);
                exceptionHandler.Resource = rInfo;

                placeholder.Controls.Add(exceptionHandler); 
            }
		}
		else
		{
            placeholder.Controls.Add(new Label { Text = string.Format(APMWebContent.Web_UnsupportedLegacyResource, rInfo.File) });
		}
	}
}
