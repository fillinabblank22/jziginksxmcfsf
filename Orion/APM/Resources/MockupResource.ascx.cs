﻿using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_MockupResource : BaseResourceControl
{
    protected string ResourceTitle
	{
		get
		{
		    return Resource.Title;
		}
	}

    protected string ResourceWidth
    {
        get { return "" + Resource.Width; }
    }

    protected override string DefaultTitle
    {
        get { return ""; }
    }
}
