﻿using System;

using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using System.Globalization;
using SolarWinds.APM.Web.Extensions;

public partial class Orion_APM_Resources_EditWindowsEventDetails : System.Web.UI.Page
{
    #region Properties

    private ResourceInfo _resource;

    protected ResourceInfo Resource
    {
        get
        {
            if (_resource == null)
            {
                Int32 id;
                if (Int32.TryParse(Request.QueryString["ResourceID"], out id))
                {
                    _resource = ResourceManager.GetResourceByID(id);
                }
            }
            return _resource;
        }
    }

    #endregion

    #region Event Handlers

    protected void Page_Init(Object sender, EventArgs e)
    {
        eventLogEditControl.Resource = this.Resource;
    }

    protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			ctrTitleEditor.ResourceTitle = Resource.Title;
			ctrTitleEditor.ResourceSubTitle = Resource.SubTitle;
		}
	}

	protected void OnSubmit_Click(Object sender, EventArgs e)
	{
	    foreach( var pair in eventLogEditControl.Properties)
	    {
	        Resource.Properties[pair.Key] = Convert.ToString(pair.Value, CultureInfo.InvariantCulture);
	    }

        Resource.Title = ctrTitleEditor.ResourceTitle;
		Resource.SubTitle = ctrTitleEditor.ResourceSubTitle;
		
		ResourcesDAL.Update(Resource);
        string originalReturnUrl = SolarWinds.Orion.Web.Helpers.UrlHelper.FromSafeUrlParameter(Request.QueryStringOrForm("ReturnUrl"));
        string url = GetViewUrl(this.Resource.View.ViewID, GetOriginalNetObject(originalReturnUrl));
        Response.Redirect(url);
    }

    private string GetOriginalNetObject(string returnUrl)
    {
        var uri = new Uri(Page.Request.Url, returnUrl);
        var collection = System.Web.HttpUtility.ParseQueryString(uri.Query);
        return collection["NetObject"];
    }

    private string GetViewUrl(int viewId, string netObjectId)
    {
        string url = string.Format("/Orion/View.aspx?ViewID={0}", viewId);
        if (!string.IsNullOrEmpty(netObjectId))
            url = string.Format("{0}&NetObject={1}", url, netObjectId);
        return url;
    }
	#endregion
}
