﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" EnableEventValidation="false" CodeFile="EditMonitor.aspx.cs" Inherits="Orion_APM_Resources_EditMonitor" MasterPageFile="~/Orion/ResourceEdit.master"%>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="apm" TagName="Assigner" Src="~/Orion/APM/Controls/AssignResourceToComponent.ascx" %>

<asp:Content ID="ctrBody" runat="server" ContentPlaceHolderID="MainContent">
	
	<h1><%= string.Format(Resources.APMWebContent.APMWEBCODE_VB1_129, this.Resource.Title)%>
	</h1>
	<div>
		<apm:Assigner ID="ctrAssigner" runat="server" />
		<orion:EditResourceTitle ID="ctrTitleEditor"  ShowSubTitle="true" ShowSubTitleHintMessage="false" runat="server" />
		<div>
               <orion:LocalizableButton ID="ctrSubmit" OnClick="OnSubmit_Click" runat="server" LocalizedText="Submit" DisplayType="Primary" />
		</div>
	</div>
</asp:Content>

