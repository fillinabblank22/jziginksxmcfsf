<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeTopXXByVirtualMemory.ascx.cs" Inherits="Orion_APM_Resources_Node_TopXXByVirtualMemory" %>
<%@ Register Src="../Summary/InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="gridInit" 
            Statistic="VirtualMemory" ColumnOrder="VirtualUsed;PercentVirtual" 
            ResourceType="Process"
            DBFieldTitles="Process Name;Application Name;Network Node" 
            DBFieldColumnNames="ProcessName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>