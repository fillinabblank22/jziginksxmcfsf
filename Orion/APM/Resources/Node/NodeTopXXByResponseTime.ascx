<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeTopXXByResponseTime.ascx.cs" Inherits="Orion_APM_Resources_Node_TopXXByResponseTime" %>
<%@ Register Src="../Summary/InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="grid_Init" 
            ColumnOrder="ResponseTime;PercentResponseTime" 
            ResourceType="Component"
            DBFieldTitles="Component Name;Application Name;Network Node" 
            DBFieldColumnNames="ComponentName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>