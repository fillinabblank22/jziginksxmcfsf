﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using System.Data;
using NPMStatus = SolarWinds.Orion.Web.DisplayTypes.Status;
using NPMNode = SolarWinds.Orion.NPM.Web.Node;
using SolarWinds.APM.Web.UI.Resource;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_APM_Resources_Node_NodeTopXXByStatisticData : ApmBaseResource
{
    private int maxCount;
    private NPMNode node;

    protected NPMNode NPMNode
    {
        get
        {
            if (node == null)
                node = GetInterfaceInstance<INodeProvider>().Node;

            return node;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionAPMPHNodeDetailsTopStatData";
        }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    protected void grid_Init(object sender, EventArgs e)
    {
        InternalTopXXByStatistic1.DBFieldTitles = string.Format("{0};{1};{2}", Resources.APMWebContent.APMWEBDATA_VB1_62,
                                                                               Resources.APMWebContent.APMWEBDATA_VB1_63,
                                                                               Resources.APMWebContent.APMWEBDATA_VB1_64);
        maxCount = GetIntProperty("MaxCount", 10);
        string filter = (GetStringValue("Filter", string.Empty) == string.Empty) ? string.Format("Nodes.NodeID = {0} ", NPMNode.NodeID) : string.Format("{0} AND Nodes.NodeID = {1} ", GetStringValue("Filter", string.Empty), NPMNode.NodeID);
        
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            DataTable table = businessLayer.GetTopComponentsByStatisticData(
                maxCount,
                LimitationHelper.GetCurrentListOfLimitationIDs(),
                filter);
            InternalTopXXByStatistic1.DataSource = TopXXResource.PrepareComponentNamesForMultiValueMonitors(table);
            InternalTopXXByStatistic1.DataBind();
            Visible = table.Rows.Count > 0;
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.APMWebContent.APMWEBCODE_AK1_131;
        }
    }

    public override string DisplayTitle
    {
        get
        {
            return this.Title.Replace("XX", maxCount.ToString());
        }
    }

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/APM/Resources/EditTopXX.aspx?ResourceID={0}", Resource.ID);
            if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
            if (Page is OrionView)
                url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
            return url;
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }
}
