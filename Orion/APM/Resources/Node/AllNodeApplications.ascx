<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllNodeApplications.ascx.cs" Inherits="Orion_APM_Resources_Node_AllApplications" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater ID="appRepeater" runat="server" OnInit="appRepeater_Init">
            <HeaderTemplate>
                <table class="DataGrid">
                    <thead>
                        <tr>
                            <td class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_AK1_146 %></td>
                            <td class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_AK1_147 %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                    <apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server" StatusValue='<%#new ApmStatus((Status)Eval("Status")) %>' CustomApplicationType='<%#Eval("CustomType") %>' />
                        <a href="<%# this.GetApplicationViewLink(Container.DataItem) %>">                    
                        <%# HttpUtility.HtmlEncode(Eval("Name")) %></a>
                    </td>
                    <td>
                        <%# GetLocalizedStatus("StatusDescription", new ApmStatus((Status)Eval("Status")).ToString())%>
                    </td>    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>