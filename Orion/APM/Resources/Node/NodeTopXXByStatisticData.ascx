﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeTopXXByStatisticData.ascx.cs" Inherits="Orion_APM_Resources_Node_NodeTopXXByStatisticData" %>
<%@ Register Src="../Summary/InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="grid_Init" 
            ColumnOrder="StatisticData;StatisticData"
            ResourceType="Component"
            DBFieldTitles="Component Name;Application Name;Network Node" 
            DBFieldColumnNames="ComponentName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>