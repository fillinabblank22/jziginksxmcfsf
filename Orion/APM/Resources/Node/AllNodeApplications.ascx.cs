using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Models;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common;
using System.Collections.Generic;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.NPM.Web;
using NPMNode = SolarWinds.Orion.NPM.Web.Node;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Node_AllApplications : ApmBaseResource
{
    private NPMNode node;
    private List<AppInfo> nodeApplications;



    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHNodeDetailsApplications"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected void appRepeater_Init(object sender, EventArgs e)
    {
        appRepeater.DataSource = NodeApplications;
        if (((List<AppInfo>) appRepeater.DataSource).Count < 1)
        {
            this.Visible = false;
        }
        else
        {
            appRepeater.DataBind();
        }
    }

    protected NPMNode NPMNode
    {
        get
        {
            if (node == null)
                node = GetInterfaceInstance<INodeProvider>().Node;

            return node;
        }
    }

    public List<AppInfo> NodeApplications
    {
        get
        {
            if (nodeApplications == null)
                nodeApplications = GetApplications();
            return nodeApplications;
        }

    }

    protected string GetLocalizedStatus(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    private List<AppInfo> GetApplications()
    {
        var nodeApplications = ApplicationDAL.GetBasicAppInfosByNodeID(NPMNode.NodeID);
        nodeApplications.Sort((a1, a2) => string.Compare(a1.Name, a2.Name, StringComparison.CurrentCultureIgnoreCase));

        return nodeApplications;
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] {typeof (INodeProvider)}; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_127; }
    }

    protected string GetApplicationViewLink(Object item)
    {
        var app = (AppInfo) item;
        return GetViewLink(GetNetObjectId(app.Id, app.CustomType));
    }

    protected string GetNetObjectId(int id, string customApplicationType)
    {
        return ApmApplicationBase.GetNetObjectId(id, customApplicationType);
    }
}
