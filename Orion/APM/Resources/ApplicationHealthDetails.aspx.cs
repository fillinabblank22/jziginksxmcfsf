﻿using System;
using System.Collections.Generic;
using System.Data;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_ApplicationHealthDetails : System.Web.UI.Page
{
	#region Event Handlers

	protected void OnAppViewGrid_Init(object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			String selStatus = String.Empty;
            if (this.ViewAppStatus == "Other")
			{
				List<Status> availableStatus = new List<Status> { 
					Status.Available, Status.Critical, Status.NotAvailable, Status.Undefined, Status.Warning 
				};
				foreach (Object status in Enum.GetValues(typeof(Status)))
				{
					if (!availableStatus.Contains((Status)status))
					{
						selStatus = String.Format("{0};{1}", (Int32)status, selStatus);
					}
				}
			}
			else
			{
				selStatus = ((Int32)this.AppStatus.Value).ToString();
			}

			int viewID;
			if (int.TryParse(Request.QueryString["ViewID"], out viewID) && viewID > 0)
			{
				var view = SolarWinds.Orion.Web.ViewManager.GetViewById(viewID);
				if (view != null)
				{
					System.Web.HttpContext.Current.Items[view.GetType().Name] = view;
				}
			}
			var filterSql = Request.QueryString["Filter"];
			if (filterSql.IsValid())
			{
				filterSql = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(filterSql));
			}
			this.appViewGrid.DataSource = SwisDAL.GetApplicationsByCurrentStatus(filterSql, selStatus);
			this.appViewGrid.DataBind();
		}
	}

	#endregion

	#region Property

	protected ApmStatus AppStatus
	{
		get
		{
			switch (this.ViewAppStatus)
			{
				case "Up":
					return new ApmStatus(Status.Available);
				case "Critical":
					return new ApmStatus(Status.Critical);
				case "Unknown":
					return new ApmStatus(Status.Undefined);
				case "Warning":
					return new ApmStatus(Status.Warning);
				case "Down":
					return new ApmStatus(Status.NotAvailable);
				case "Other":
                    return new ApmStatus(Status.Undefined);
			}
			return null;
		}
	}

    protected string DisplayAppStatus
    {
        get { return Request.QueryString["SelectedStatus"]; }
    }

	protected string ViewAppStatus
	{
		get 
		{ 
			return Request.QueryString["SelectedStatus"];
		}
	}

    protected int LimitationID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["Limit"], out id);
            
            return id;
        }
    }

	#endregion

	#region Helper Members

	protected String GetRowItem(Object item)
	{
        if (item == null)
            throw new ArgumentNullException("item");

		DataRowView row = item as DataRowView;
		if (row != null)
		{
            var customApplicationType = (row["CustomApplicationType"] is DBNull ? (string)null : (string)row["CustomApplicationType"]);
		    var applicationId = (int) row["ApplicationID"];
            var link = BaseResourceControl.GetViewLink(ApmApplication.GetNetObjectId(applicationId, customApplicationType));
            var html = String.Format(Resources.APMWebContent.APMWEBCODE_AK1_120, "<a href='{0}'>{1}</a>", "<img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&id={2}&status={4}' alt='statusIcon' />&nbsp;<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{2}'>{3}</a>");
			return String.Format(html, link, row["Name"], row["NodeID"], row["Caption"], row["NodeStatus"]);
		}
		return Resources.APMWebContent.APMWEBCODE_AK1_23;
	}

    protected String GetCustomApplicationType(Object item)
    {
        if (item == null)
            throw new ArgumentNullException("item");

        DataRowView row = item as DataRowView;
        if (row != null)
        {
            return (row["CustomApplicationType"] is DBNull ? (string)null : (string)row["CustomApplicationType"]);
        }
        return null;
    }

    protected ApmStatus GetApmStatus(Object item)
    {
        if (item == null)
            throw new ArgumentNullException("item");

        DataRowView row = item as DataRowView;

        switch (this.ViewAppStatus)
        {
            case "Up":
                return new ApmStatus(Status.Available);
            case "Critical":
                return new ApmStatus(Status.Critical);
            case "Unknown":
                return new ApmStatus(Status.Undefined);
            case "Warning":
                return new ApmStatus(Status.Warning);
            case "Down":
                return new ApmStatus(Status.NotAvailable);
            case "Other":
                if (row != null)
                {
                    return
                        new ApmStatus(row["Availability"] is DBNull
                            ? Status.Undefined
                            : (Status) (int) row["Availability"]);
                }
                return new ApmStatus(Status.Undefined);
        }
        return null;
    }

    #endregion
}
