﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_Monitor_MonitorDynamicGaugeLinear : SolarWinds.APM.Web.ApmDynamicGaugeResource
{
    #region Properties

    public override SolarWinds.NPM.Web.Gauge.V1.GaugeType GaugeType
    {
        get { return SolarWinds.NPM.Web.Gauge.V1.GaugeType.Linear; }
    }

    protected override String DefaultTitle
    {   //Multiple Value Component Statistics - Linear Gauges
        get { return Resources.APMWebContent.APMWEBCODE_VB1_144; }
    }

    public override String HelpLinkFragment
    {
        get { return "OrionAPMPHMonitorDetailsMultiLinearStats"; }
    }

    protected string GaugeControlId
    {
        get
        {
            return "GaugesContainer" + Resource.JavaScriptFriendlyID;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
    #endregion

    #region Event Handlers

    protected void GaugeDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item != null && e.Item.DataItem != null && e.Item.DataItem is DynamicEvidenceColumnSchema)
        {
            var data = e.Item.DataItem as DynamicEvidenceColumnSchema;
            var itemAvg = e.Item.FindControl("ctrGaugeStatisticAvg") as PlaceHolder;

            var style = GetStringValue("Style", "Solarwinds");
            var scale = GetIntProperty("Scale", 90);
            var chartLink = string.Format(@"/Orion/APM/CustomChart.aspx?ChartName=MonMMADynamicStatistic&NetObjectID={0}&Period=Today&{1}={2}", ApmMonitor.NetObjectID, ChartInfo.KEYS.StatisticValueName, data.Name);

            if (itemAvg != null)
            {
                CreateGauge(
                        itemAvg, scale, style, data.Label, chartLink,
                        ApmMonitor.NetObjectID, "DynamicStatisticDataAvg", "Linear", 0, Int64.Parse(GetStringValue("GaugeMaxUnitsScale", "2500")), 500, Int64.Parse(GetStringValue("GaugeUnitsValue", "1")), GetStringValue("GaugeUnitsLabel", ""), data.Name
                    );
            }

            ChangeLayout(e.Item);
        }
    }

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!IsPostBack && Visible)
        {
            if (DisplayColumns.Count > 0)
            {
                gaugeRepeater.DataSource = DisplayColumns;
                gaugeRepeater.DataBind();
            }
        }
    }

	protected int GaugeWidth
    {
        get
        {
			return SolarWinds.APM.Web.ApmGaugeResource.GetLinearGaugeWidth(Resource.Properties["Style"]);
        }
    }
	
    private void ChangeLayout(RepeaterItem repItem)
    {
        var cmp = repItem.DataItem as DynamicEvidenceColumnSchema;
        var cmps = (gaugeRepeater.DataSource as List<DynamicEvidenceColumnSchema>).ToList();
        var ctrCntr = repItem.FindControl("ctrContainer") as HtmlControl;

        if (ctrCntr != null)
        {
            ctrCntr.Visible = cmps.IndexOf(cmp) > -1;
        }
    }

    #endregion
}