using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Helpers;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Enums;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_Monitor_MonitorGauges : ApmGaugeResource
{
    protected class ResourcePropertyName
    {
        public const string Scale = "Scale";
        public const string Style = "Style";
        public const string GaugeMaxUnitsScale = "GaugeMaxUnitsScale";
        public const string GaugeUnitsValue = "GaugeUnitsValue";
        public const string GaugeUnitsLabel = "GaugeUnitsLabel";
        public const string GaugeType = "GaugeType";
    }

    protected class QueryParamName
    {
        public const string NetObjectID = "NetObjectID";
        public const string Period = "Period";
        public const string EvidenceFilter = "EvidenceFilter";
    }

    protected class DefaultValue
    {
        public const string GaugeType = "Radial";
        public const string GaugeStyle = "Simple Circle";
        public const int GaugeScale = 90;
        public const string Period = "Today";
        public const string PercentUnitLabel = "%";
        public const int PercentMin = 0;
        public const int PercentMax = 100;
        public const int PercentTicks = 10;
        public const int PercentUnitDivisor = 0;
        public const int StatisticMin = 0;
        public const int StatisticTicks = 500;
        public const long StatisticMax = 2500;
        public const long StatisticUnitDivisor = 1;
    }


    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_132; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHMonitorDetailsRadialStats"; }
    }

    protected override Dictionary<string, object> EditUrlParams
    {
        get
        {
            var pairs = base.EditUrlParams;
            pairs[QueryParamName.EvidenceFilter] = String.Format("{0}|{1}", ComponentEvidenceType.ProcessEvidence, ComponentEvidenceType.PortEvidence);
            return pairs;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected int GaugeScale
    {
        get { return this.GetIntProperty(ResourcePropertyName.Scale, DefaultValue.GaugeScale); }
    }

    protected string GaugeStyle
    {
        get { return this.GetStringValue(ResourcePropertyName.Style, DefaultValue.GaugeStyle); }
    }

    protected long GaugeMaxUnitsScale
    {
        get { return ParseHelper.TryParse(this.GetStringValue(ResourcePropertyName.GaugeMaxUnitsScale, null), DefaultValue.StatisticMax); }
    }

    protected long GaugeUnitDivisor
    {
        get { return ParseHelper.TryParse(this.GetStringValue(ResourcePropertyName.GaugeUnitsValue, null), DefaultValue.StatisticUnitDivisor); }
    }

    protected string GaugeUnitsLabel
    {
        get { return this.GetStringValue(ResourcePropertyName.GaugeUnitsLabel, ""); }
    }

    public string GaugeTypeText
    {
        get { return this.GetStringValue(ResourcePropertyName.GaugeType, DefaultValue.GaugeType); }
    }

    private void UpdateLinks(params HyperLink[] controls)
    {
        foreach (var ctl in controls)
        {
            var url = ctl.Attributes["href"];
            if (!string.IsNullOrEmpty(url))
            {
                var uri = new Uri(this.Page.Request.Url, url);
                var query = HttpUtility.ParseQueryString(uri.Query);
                query[QueryParamName.NetObjectID] = this.ApmMonitor.NetObjectID;
                query[QueryParamName.Period] = DefaultValue.Period;
                var newUrl = InvariantString.Format("{0}?{1}", uri.AbsolutePath, query.ToString());
                ctl.Attributes["href"] = newUrl;
            }
        }
    }

    protected string GetCustomLabel(string defaultLabel, Component cmp, string gaugeType)
    {
        var label = this.GetStringValue(gaugeType + "CustomLabel", null);
        if (!string.IsNullOrEmpty(label))
        {
            return label;
        }
        label = WebPluginManager.Instance
            .GetPlugins<IGaugePlugin>()
            .Select(p => p.GetCustomGaugeLabel(cmp.Type, gaugeType))
            .FirstOrDefault(l => l != null);
        return label ?? defaultLabel;
    }

    protected string GetCustomUnit(string defaultUnit, Component cmp, string gaugeType)
    {
        var unit = this.GetStringValue(gaugeType + "CustomUnit", null);
        if (!string.IsNullOrEmpty(unit))
        {
            return unit;
        }
        unit = WebPluginManager.Instance
            .GetPlugins<IGaugePlugin>()
            .Select(p => p.GetCustomGaugeUnit(cmp.Type, gaugeType))
            .FirstOrDefault(l => l != null);
        return unit ?? defaultUnit;
    }

    protected void CreateGauge(Component cmp, PlaceHolder container, string defaultLabel, HyperLink link, string gaugeName, long min, long max, long tickCount, long unitDivisor, string unitsLabel)
    {
        var url = link.Attributes["href"];
        var label = this.GetCustomLabel(defaultLabel, cmp, gaugeName);
        var unit = this.GetCustomUnit(unitsLabel, cmp, gaugeName);
        this.CreateGauge(container, this.GaugeScale, this.GaugeStyle, label, url, this.ApmMonitor.NetObjectID, gaugeName, this.GaugeTypeText, min, max, tickCount, unitDivisor, unit);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Visible)
        {
            return;
        }

        if (this.ApmMonitor == null)
        {
            this.Visible = false;
            return;
        }

        // Current resource not support dynamic evidence components
        if (this.ApmMonitor.Application.Components.All(item => item.IsDynamicBased))
        {
            this.Visible = false;
            return;
        }

        if (!this.ApmMonitor.IsConfigured)
        {
            return;
        }

        var cmp = this.ApmMonitor.BaseComponent;
        this.Visible = cmp.IsProcessBased || cmp.HasVirtualMemorySupport || cmp.HasResponseTimeSupport || cmp.HasStatisticData;

        if (!this.Visible)
        {
            return;
        }

        this.UpdateLinks(this.lnkStatisticData, this.lnkNonMmaCpu, this.lnkNonMmaPmem, this.lnkNonMmaVmem, this.lnkNonMmaResponse);

        if (cmp.IsProcessBased)
        {
            this.CreateGauge(
                cmp, this.GaugeCpu, Resources.APMWebContent.APMWEBCODE_VB1_133, this.lnkNonMmaCpu,
                GaugeName.PercentCpu, DefaultValue.PercentMin, DefaultValue.PercentMax, DefaultValue.PercentTicks, DefaultValue.PercentUnitDivisor, DefaultValue.PercentUnitLabel);
            this.CreateGauge(
                cmp, this.GaugeMemory, Resources.APMWebContent.APMWEBCODE_VB1_134, this.lnkNonMmaPmem,
                GaugeName.PercentMemory, DefaultValue.PercentMin, DefaultValue.PercentMax, DefaultValue.PercentTicks, DefaultValue.PercentUnitDivisor, DefaultValue.PercentUnitLabel);
        }

        if (cmp.HasVirtualMemorySupport)
        {
            this.CreateGauge(
                cmp, this.GaugeVirtual, Resources.APMWebContent.APMWEBCODE_VB1_135, this.lnkNonMmaVmem,
                GaugeName.PercentVirtual, DefaultValue.PercentMin, DefaultValue.PercentMax, DefaultValue.PercentTicks, DefaultValue.PercentUnitDivisor, DefaultValue.PercentUnitLabel);
        }

        if (cmp.HasStatisticData)
        {
            this.CreateGauge(
                cmp, this.GaugeStatisticData, Resources.APMWebContent.APMWEBCODE_VB1_136, this.lnkStatisticData,
                GaugeName.StatisticData, DefaultValue.StatisticMin, this.GaugeMaxUnitsScale, DefaultValue.StatisticTicks, this.GaugeUnitDivisor, this.GaugeUnitsLabel);
        }

        if (cmp.HasResponseTimeSupport)
        {
            this.CreateGauge(
                cmp, this.GaugeResponseTime, Resources.APMWebContent.APMWEBCODE_VB1_137, this.lnkNonMmaResponse,
                GaugeName.ResponseTime, DefaultValue.StatisticMin, this.GaugeMaxUnitsScale, DefaultValue.StatisticTicks, this.GaugeUnitDivisor, this.GaugeUnitsLabel);
        }
    }
}
