using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_Monitor_MonitorGaugesLinear : ApmGaugeResource
{
	public override SolarWinds.NPM.Web.Gauge.V1.GaugeType GaugeType
	{
		get { return SolarWinds.NPM.Web.Gauge.V1.GaugeType.Linear; }
	}

	protected override string DefaultTitle
	{
        get { return Resources.APMWebContent.APMWEBCODE_VB1_138; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionAPMPHMonitorDetailsLinearStats"; }
	}

	protected override Dictionary<String, Object> EditUrlParams
	{
		get
		{
			var pairs = base.EditUrlParams;
			pairs["EvidenceFilter"] = String.Format("{0}|{1}", ComponentEvidenceType.ProcessEvidence, ComponentEvidenceType.PortEvidence);
			return pairs;
		}
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    protected void Page_Load(object sender, EventArgs e)
    {
		Visible &= ApmMonitor != null;
		if (Visible)
		{
			//Current resource not support dynamic evidence components
			var app = ApmMonitor.Application;
			if (app.Components.Count == app.Components.Count(item => item.IsDynamicBased))
			{
				Visible = false;
			}
		}

        if (Visible && ApmMonitor.IsConfigured)
        {
            var cmp = ApmMonitor.BaseComponent;

			Visible = cmp.IsProcessBased || cmp.HasVirtualMemorySupport || cmp.HasResponseTimeSupport || cmp.HasStatisticData;
            if (Visible)
            {
                var gaugeStyle = GetStringValue("Style", "Solarwinds");
				var gaugeScale = GetIntProperty("Scale", 90);

				if (cmp.IsProcessBased)
                {
					CreateGauge(
                        GaugeCpu, gaugeScale, gaugeStyle, Resources.APMWebContent.APMWEBCODE_VB1_133,
						"/Orion/APM/CustomChart.aspx?chartName=MonMMACpu&NetObjectID=" + ApmMonitor.NetObjectID + "&Period=Today",
						ApmMonitor.NetObjectID, "PercentCpu", "Linear", 0, 100, 10, 0, "%"
					);

					CreateGauge(
                        GaugeMemory, gaugeScale, gaugeStyle, Resources.APMWebContent.APMWEBCODE_VB1_134,
						"/Orion/APM/CustomChart.aspx?chartName=MonMMAPMem&NetObjectID=" + ApmMonitor.NetObjectID + "&Period=Today",
						ApmMonitor.NetObjectID, "PercentMemory", "Linear", 0, 100, 10, 0, "%"
					);
                }

				if (cmp.HasVirtualMemorySupport)
                {
					CreateGauge(
                        GaugeVirtual, gaugeScale, gaugeStyle, Resources.APMWebContent.APMWEBCODE_VB1_135,
						"/Orion/APM/CustomChart.aspx?chartName=MonMMAVMem&NetObjectID=" + ApmMonitor.NetObjectID + "&Period=Today",
						ApmMonitor.NetObjectID, "PercentVirtual", "Linear", 0, 100, 10, 0, "%"
					);
                }

				if (cmp.HasStatisticData)
                {
					CreateGauge(
                        GaugeStatisticData, gaugeScale, gaugeStyle, GetStringValue("StatisticDataCustomLabel", Resources.APMWebContent.APMWEBCODE_VB1_136),
						"/Orion/APM/CustomChart.aspx?chartName=MonMMAStatisticData&NetObjectID=" + ApmMonitor.NetObjectID + "&Period=Today",
						ApmMonitor.NetObjectID, "StatisticData", "Linear", 0, Int64.Parse(GetStringValue("GaugeMaxUnitsScale", "2500")), 500, Int64.Parse(GetStringValue("GaugeUnitsValue", "1")), GetStringValue("GaugeUnitsLabel", "")
					);
                }

				if (cmp.HasResponseTimeSupport)
                {
					CreateGauge(
                        GaugeResponseTime, gaugeScale, gaugeStyle, Resources.APMWebContent.APMWEBCODE_VB1_137,
						"/Orion/APM/CustomChart.aspx?chartName=MonMMAResponse&NetObjectID=" + ApmMonitor.NetObjectID + "&Period=Today",
						ApmMonitor.NetObjectID, "ResponseTime", "Linear", 0, Int64.Parse(GetStringValue("GaugeMaxUnitsScale", "2500")), 500, Int64.Parse(GetStringValue("GaugeUnitsValue", "1")), GetStringValue("GaugeUnitsLabel", "")
					);
                }
            }
        }
    }
}