﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WindowsEventDetails.ascx.cs"
    Inherits="Orion_APM_Resources_Monitor_WindowsEventDetails" %>

<orion:Include ID="Include1" Framework="Ext" FrameworkVersion="4.0" runat="server" />
<orion:Include ID="Include3" Module="APM" File="js/Ext4AjaxProxy.js" runat="server" />
<orion:Include ID="Include4" Module="APM" File="Resources/Monitor/WindowsEventDetails.js" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <% if (this.Visible)
           { %>
        <div id="WindowsEventGrid-<%= Resource.ID %>-toolbar" style="width:100%;">
            <span style="float: left;">
                <div id="WindowsEventGrid-<%= Resource.ID %>-toolbar-left"></div>
            </span>
            <span style="float: right;">
                <div id="WindowsEventGrid-<%= Resource.ID %>-toolbar-right"></div>
            </span>
        </div>
        <div style="clear: both;"></div>
        <div id="WindowsEventGrid-<%= Resource.ID %>">
        </div>
        <style type="text/css">
            div#WindowsEventGrid-<%= Resource.ID %> .x4-grid-header-ct { width:100% !important; }
            div#WindowsEventGrid-<%= Resource.ID %> .x4-grid-row .x4-grid-cell-inner a > img { height: 16px !important; }
        </style>
        <script language="javascript" type="text/javascript">
            $(document).ready(function() {
                SW.APM.WindowsEventGrid.createGrid(<%= Resource.ID %>, <%= ApmMonitor.Id %>, <%= GridPageSize %>);
                var refresh = function () {
                    SW.APM.WindowsEventGrid.refreshGrid(<%= Resource.ID %>);
                };
                SW.Core.View.AddOnRefresh(refresh, 'WindowsEventGrid-<%= Resource.ID %>');
            });
        </script>
        <% } %>
    </Content>
</orion:resourceWrapper>
