<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorSettings.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorSettings" %>
<%@ Register tagPrefix="apm" tagName="MonitorThresholdsRepeater" Src="~/Orion/APM/Controls/Resources/MonitorThresholdsRepeater.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
		<orion:Include ID="I2" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="I3" File="APM/Styles/Resources.css" runat="server" />	

        <table cellspacing="0" class="NeedsZebraStripes biggerPadding">
            <asp:Repeater ID="settingsRepeater" runat="server" OnInit="settingsRepeater_Init">
                <ItemTemplate>
                    <tr>
                        <td class="PropertyHeader"><%# Eval("SettingLocalizedName")%></td>
                        <td class="Property"><%# Eval("SettingValue")%></td>                    
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <apm:MonitorThresholdsRepeater runat="server" ID="ThresholdsRepeater" OnInit="ThresholdsRepeater_OnInit"/>
        </table>
    </Content>
</orion:resourceWrapper>
