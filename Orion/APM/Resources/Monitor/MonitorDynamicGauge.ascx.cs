﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_Monitor_MonitorDynamicGauge : SolarWinds.APM.Web.ApmDynamicGaugeResource
{
    #region Fields

    private const Int32 RadialWidth = 160;

    #endregion 

    #region Properties

    protected override String DefaultTitle
    {   //Multiple Value Component Statistics - Radial Gauges
        get { return Resources.APMWebContent.APMWEBCODE_VB1_143; }
    }

    public override String HelpLinkFragment
    {
        get { return "OrionAPMPHMonitorDetailsMultiRadialStats"; }
    }

    protected string GaugeControlId
    {
        get
        {
            return "GaugesContainer" + Resource.JavaScriptFriendlyID;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
    #endregion

    #region Event Handlers

    protected void GaugeDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item != null && e.Item.DataItem != null && e.Item.DataItem is DynamicEvidenceColumnSchema)
        {
            var data = e.Item.DataItem as DynamicEvidenceColumnSchema;
            var itemAvg = e.Item.FindControl("ctrGaugeStatisticAvg") as PlaceHolder;

            var style = GetStringValue("Style", "Simple Circle");
            var scale = GetIntProperty("Scale", 90);
            var chartLink = string.Format(@"/Orion/APM/CustomChart.aspx?ChartName=MonMMADynamicStatistic&NetObjectID={0}&Period=Today&{1}={2}", ApmMonitor.NetObjectID, ChartInfo.KEYS.StatisticValueName, data.Name);

            if (itemAvg != null)
            {
                CreateGauge(itemAvg,
                    scale, style, data.Label, chartLink,
                    ApmMonitor.NetObjectID, "DynamicStatisticDataAvg", "Radial", 0, Int64.Parse(GetStringValue("GaugeMaxUnitsScale", "2500")), 500,
                    Int64.Parse(GetStringValue("GaugeUnitsValue", "1")), GetStringValue("GaugeUnitsLabel", ""), data.Name
                );
            }

            ChangeLayout(e.Item);
        }
    }

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!IsPostBack && Visible)
        {
            if (DisplayColumns.Count > 0)
            {
                gaugeRepeater.DataSource = DisplayColumns;
                gaugeRepeater.DataBind();
            }
        }
    }

	protected int GaugeWidth
    {
        get
        {
			var scale = 0;
			if (!int.TryParse(Resource.Properties["Scale"], out scale))
			{
				scale = 100;
			}
			return scale * RadialWidth / 100;
        }
    }
	
    private void ChangeLayout(RepeaterItem repItem)
    {
        var ctrCntr = repItem.FindControl("ctrContainer") as HtmlControl;
        if (ctrCntr != null)
        {
			var cmp = repItem.DataItem as DynamicEvidenceColumnSchema;
			var cmps = (gaugeRepeater.DataSource as List<DynamicEvidenceColumnSchema>).ToList();
			ctrCntr.Visible = cmps.IndexOf(cmp) > -1;
        }
    }

    #endregion
}