﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorGaugesMulti.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorGaugesMulti" %>
<%@ Register TagPrefix="apm" TagName="Gauges" Src="~/Orion/APM/Controls/GaugesContainer.ascx" %>

<orion:resourceWrapper ID="ctrRW" runat="server">
	<Content>
		<apm:Gauges ID="ctrGauge" GaugeType="Radial" runat="server" />
	</Content>
</orion:resourceWrapper>