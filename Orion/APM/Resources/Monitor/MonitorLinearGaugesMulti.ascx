﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorLinearGaugesMulti.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorLinearGaugesMulti" %>
<%@ Register TagPrefix="apm" TagName="Gauges" Src="~/Orion/APM/Controls/GaugesContainer.ascx" %>

<orion:resourceWrapper ID="ctrRW" runat="server">
	<Content>
		<apm:Gauges ID="ctrGauge" GaugeType="Linear" runat="server" />
	</Content>
</orion:resourceWrapper>