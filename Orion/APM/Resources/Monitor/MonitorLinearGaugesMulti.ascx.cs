﻿using System;
using System.Linq;

using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_Monitor_MonitorLinearGaugesMulti : SolarWinds.APM.Web.ApmGaugeResource
{
	#region Properties

	public override SolarWinds.NPM.Web.Gauge.V1.GaugeType GaugeType
	{
		get { return SolarWinds.NPM.Web.Gauge.V1.GaugeType.Linear; }
	}

	protected override String DefaultTitle
	{
        get { return Resources.APMWebContent.APMWEBCODE_VB1_142; }
	}

	public override String HelpLinkFragment
	{
        get { return "OrionAPMPHMonitorDetailsMultiComponentStatisticsLinear"; }
	}

	public override EditConfig EditResourceConfig
	{
		get
		{
			var config = EditConfig.ShowGaugesAssigner;
			if (ApmMonitor != null && ApmMonitor.Application.Components.Exists(cmp => cmp.HasStatisticData))
			{
				config |= EditConfig.ShowStatisticLabel;
			}
			return config;
		}
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
	#endregion

	#region Event Handlers

	protected override void OnInit(EventArgs e)
	{
		if (!IsPostBack)
		{
			var isAssigned = false;

			var ids = GetAssignedInfo().ComponentIDs;
			if (isAssigned = ids != null)
			{
				isAssigned = ApmMonitor.Application.Components.Exists(item => ids.Contains(item.Id));
			}
			if (!isAssigned)
			{
				var names = HtmlHelper.SplitNames(Resource.Properties["GaugeComponentNames"]);
				if (isAssigned = names != null)
				{
					isAssigned = ApmMonitor.Application.Components.Exists(item => names.Contains(item.Name));
				}
			}

			if (isAssigned)
			{
				ctrGauge.Resource = Resource;
				ctrGauge.Components = ApmMonitor.Application.Components;
				ctrGauge.CreateGauge = CreateGauge;
				ctrGauge.DynamicCreateGauge = CreateGauge;
			}
			else
			{
				Controls.Clear();

				var ctr = Page.LoadControl("~/Orion/APM/Controls/RequiresComponentName.ascx");
				Controls.Add(ctr);

				(ctr as IConfigurationRequiredControl).SetMonitorData(this);
			}
		}
	}

	#endregion
}