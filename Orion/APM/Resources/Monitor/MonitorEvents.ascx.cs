using System;

using SolarWinds.APM.Web;
using SolarWinds.APM.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_APM_Resources_Monitor_MonitorEvents : ApmMonitorBaseResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    protected void grid_Init(object sender, EventArgs e)
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            orionEventList.DataSource = businessLayer.GetEventsForComponent(ApmMonitor.Id);
            orionEventList.DataBind();
        }
    }
            
    protected override string DefaultTitle
    {
        get
        {
            //Last 25 Component Events
            return Resources.APMWebContent.APMWEBCODE_VB1_131;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHMonitorDetailsMonEvents"; }
    }
}
