using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Monitor_MonitorSettings : ApmMonitorBaseResource
{
	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Static; } }

	private List<DisplayedSetting> displayedSettings;

	protected override string DefaultTitle
	{
		get { return Resources.APMWebContent.APMWEBCODE_TB0_1; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHComponentSettings"; }
	}

	protected void settingsRepeater_Init(object sender, EventArgs e)
	{
	    displayedSettings = WebPluginManager.Instance.GetComponentDisplayedSettings(ApmMonitor).ToList();

		settingsRepeater.DataSource = displayedSettings;
		settingsRepeater.DataBind();
	}

    protected void ThresholdsRepeater_OnInit(object sender, EventArgs e)
    {
        ThresholdsRepeater.Thresholds = ApmMonitor.BaseComponent.Thresholds.Values.ToList();
        ThresholdsRepeater.IsThresholdDisplayed = t => true;
    }
}