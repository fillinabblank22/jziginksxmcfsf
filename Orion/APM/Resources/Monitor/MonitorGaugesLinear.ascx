<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorGaugesLinear.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorGaugesLinear" %>

<orion:Include runat="server" File="styles/GaugeStyle.css" />

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <table class="apm_Gauge">
            <tr>
                <td>
   					<a href="/Orion/APM/CustomChart.aspx?chartName=MonMMAStatisticData&NetObjectID=<%=ApmMonitor.NetObjectID%>&Period=Today" target="_blank">
						<asp:PlaceHolder runat="server" ID="GaugeStatisticData" />
					</a>
                </td>
            </tr>
            <tr>
                <td>
   					<a href="/Orion/APM/CustomChart.aspx?chartName=MonMMACpu&NetObjectID=<%=ApmMonitor.NetObjectID%>&Period=Today" target="_blank">
						<asp:PlaceHolder runat="server" ID="GaugeCpu" />
					</a>
                </td>
            </tr>
            <tr>           
                <td>
   					<a href="/Orion/APM/CustomChart.aspx?chartName=MonMMAPMem&NetObjectID=<%=ApmMonitor.NetObjectID%>&Period=Today" target="_blank">
						<asp:PlaceHolder runat="server" ID="GaugeMemory" />
					</a>
                </td>
            </tr>
            <tr>
                <td>
   					<a href="/Orion/APM/CustomChart.aspx?chartName=MonMMAVMem&NetObjectID=<%=ApmMonitor.NetObjectID%>&Period=Today" target="_blank">
						<asp:PlaceHolder runat="server" ID="GaugeVirtual" />
					</a>
                </td>
            </tr>
            <tr>
                <td>
   					<a href="/Orion/APM/CustomChart.aspx?chartName=MonMMAResponse&NetObjectID=<%=ApmMonitor.NetObjectID%>&Period=Today" target="_blank">
						<asp:PlaceHolder runat="server" ID="GaugeResponseTime" />
					</a>
                </td>
            </tr>
        </table>        
    </Content>
</orion:resourceWrapper>