<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorGauges.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorGauges" %>

<orion:Include runat="server" File="styles/GaugeStyle.css" />

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <table class="apm_Gauge">
            <tr>
                <td>
                    <asp:HyperLink runat="server" id="lnkStatisticData" href="/Orion/APM/CustomChart.aspx?chartName=MonMMAStatisticData" target="_blank">
                        <asp:PlaceHolder runat="server" ID="GaugeStatisticData" />
                    </asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink runat="server" id="lnkNonMmaCpu" href="/Orion/APM/CustomChart.aspx?chartName=MonMMACpu" target="_blank">
                        <asp:PlaceHolder runat="server" ID="GaugeCpu" />
                    </asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink runat="server" id="lnkNonMmaPmem" href="/Orion/APM/CustomChart.aspx?chartName=MonMMAPMem" target="_blank">
                        <asp:PlaceHolder runat="server" ID="GaugeMemory" />
                    </asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink runat="server" id="lnkNonMmaVmem" href="/Orion/APM/CustomChart.aspx?chartName=MonMMAVMem" target="_blank">
                        <asp:PlaceHolder runat="server" ID="GaugeVirtual" />
                    </asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink runat="server" id="lnkNonMmaResponse" href="/Orion/APM/CustomChart.aspx?chartName=MonMMAResponse" target="_blank">
                        <asp:PlaceHolder runat="server" ID="GaugeResponseTime" />
                    </asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>