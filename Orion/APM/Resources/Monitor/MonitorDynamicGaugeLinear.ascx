﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorDynamicGaugeLinear.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorDynamicGaugeLinear" %>

<orion:Include runat="server" File="styles/GaugeStyle.css" />
<orion:Include runat="server" Module="APM" File="GaugesLayout.js" Section="Top"/>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {
		var containerId = '<%=GaugeControlId%>';
		var gaugeWidth = <%=GaugeWidth%>;
		SW.APM.GaugesLayout.startCheckResize(containerId, gaugeWidth);
    });
    //]]>
</script>

<orion:resourceWrapper ID="ctrResWrapper" runat="server">
    <Content>
		<div id="<%=GaugeControlId%>" name="GaugesContainer">
			<asp:Repeater ID="gaugeRepeater" runat="server" OnItemDataBound="GaugeDataBound">
				<ItemTemplate>
					<div id="ctrContainer" style="float: left; font-weight: bold; text-align: center;"
						runat="server">
						<asp:PlaceHolder ID="ctrGaugeStatisticAvg" runat="server" />
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</div>
    </Content>
</orion:resourceWrapper>
