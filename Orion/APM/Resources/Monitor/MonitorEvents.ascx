<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorEvents.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorEvents" %>
<%@ Register TagPrefix="orion" TagName="EventList" Src="~/Orion/Controls/EventList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	<orion:EventList runat="server" ID="orionEventList"  OnInit = "grid_Init">
	</orion:EventList>
	</Content>
</orion:resourceWrapper>