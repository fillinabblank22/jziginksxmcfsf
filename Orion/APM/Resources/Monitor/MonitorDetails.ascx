<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorDetails.ascx.cs" Inherits="Orion_APM_Resources_Monitor_MonitorDetails" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models"%>
<%@ Import Namespace="SolarWinds.APM.Web"%>
<%@ Import Namespace="System.Web"%>
<%@ Register TagPrefix="apm" TagName="SmallApmStatusIcon" Src="~/Orion/APM/Controls/SmallApmStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register tagPrefix="apm" tagName="MonitorThresholdsRepeater" Src="~/Orion/APM/Controls/Resources/MonitorThresholdsRepeater.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>
 
<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
		<orion:Include ID="i1" Framework="Ext" FrameworkVersion="3.4" runat="server" /> 
		<orion:Include ID="i3" File="APM/js/NodeManagement.js" runat="server" /> 
		<orion:Include runat="server" File="APM/Styles/Resources.css"/>	
        <% if (ApmMonitor.Type.Value == ComponentType.WindowsService){ %>
          <orion:Include ID="Include4" Module="APM" File="Resources/Monitor/ServiceMonitorDetails.js" runat="server" />         
        <% }%>

        <script type="text/javascript">
            $().ready(function(){
                SW.APM.TrimStatusDetails();

                <% if (ApmMonitor.Type.Value == ComponentType.WindowsService) {
                      int credentialSetId;
                      var credentialSet = ApmApplication.GetAppInsightWindowsCredentials();
                      if (credentialSet == null)
                      {
                          string set = this.ApmMonitor.ComponentSettings["__CredentialSetId"].Value.Value;
                          int.TryParse(set, out credentialSetId);
                      }
                      else
                      {
                          credentialSetId = credentialSet.Id;
                      }
                %>
                SW.APM.ServicesMonitorDetailsHandlers.IsDemo = '<%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer%>'.toLowerCase();
                SW.APM.ServicesMonitorDetailsHandlers.GetServicesInfo(<%=ApmApplication.Node.Id%>, <%=credentialSetId%>,'<%=ApmMonitor.ComponentSettings["ServiceName"].Value.Value%>');
            <% }%>
            });
        </script>

        <table cellspacing="0" class="NeedsZebraStripes biggerPadding">
            <tr>
                <td colspan="2" align="center">
                    <%= string.Format(Resources.APMWebContent.APMWEBDATA_AK1_59, string.Format("<a href=\"{0}\">{1}</a>", GetViewLink(ApmMonitor.GetNetObjectId(ApmMonitor.Id, ApmApplication.CustomType)), HttpUtility.HtmlEncode(ApmMonitor.Name)), string.Format("<a href=\"{0}\">{1}</a>", GetViewLink(ApmApplicationBase.GetNetObjectId(ApmApplication.Id, ApmApplication.CustomType)), HttpUtility.HtmlEncode(ApmMonitor.Application.Name)), string.Format("<a href=\"{0}\">{1}</a>", GetViewLink((string)ApmMonitor.Application.NPMNode.NetObjectID), HttpUtility.HtmlEncode(ApmMonitor.Application.NodeName)))%>
                </td>
            </tr>
            <% if (AllowProcessRealTime || AllowServiceRealTime || AllowEventRealTime) { %>
			<tr>
                <td valign="top" class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_292%></td>
				<td class="Property NodeManagementIcons">

					<%if (ApmRoleAccessor.AllowRealTimeProcessExplorer) { %>
					<a href="#" onclick="return SW.APM.NodeManagement.openRealTimeProcessWindow(<%=ApmMonitor.Application.Node.Id%>, null, <%=ApmMonitor.Id%>);">
						<img alt="" src="/Orion/APM/Images/RT_process_explorer.png" /> <%= Resources.APMWebContent.APMWEBDATA_VB1_298%>
					</a>
					<%}%>

                    <%if (ApmRoleAccessor.AllowRealTimeServiceExplorer && ShowServiceManagerLink){ %>
						<span>
							<br />
							<a href="#" onclick="return SW.APM.NodeManagement.openRealTimeServicesWindow(<%=ApmApplication.Node.Id%>,<%=GetRtMonitorCredId()%>);">
								<img alt="" src="/Orion/APM/Images/RT_service_manager.png" /> <%= Resources.APMWebContent.APMWEBDATA_RM0_7%>
							</a>
						</span>
					<%}%>
                    
                    <% if (AllowServiceRealTime) { %>
						<%if (ApmRoleAccessor.AllowRealTimeServiceManage && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer == false){ %>
                            <div style="padding-left: 16px;">
                                <span id="startStopRestartServiceContainer"></span>
                            </div>
						<%}%>
					<%}%>

					<%if (ApmRoleAccessor.AllowRealTimeEventExplorer && ShowEventExplorerLink) { %>
                        <div>
					        <a href="#" onclick="return SW.APM.NodeManagement.openRealTimeEventExplorerWindow(<%=ApmApplication.Node.Id%>,<%=GetRtMonitorCredId()%>);">
						        <img alt="" src="/Orion/APM/Images/RT_Event_Viewer.png" /> <%= Resources.APMWebContent.APMWEBDATA_VB1_343%>
					        </a>
                        </div>
					<%}%>
                </td>
            </tr>
            <% }%>
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_300%></td>
                <td class="Property">
                    <apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server" /> 
                    <a href="<%=GetViewLink(ApmMonitor.Application.NetObjectID)%>">
                        <%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_301, ApmMonitor.Application.Status.ToLocalizedString())%>
                    </a> 
                    <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>                   
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_311%></td>
                <td class="Property">
                <apm:SmallApmStatusIcon ID="MonitorStatusIcon" runat="server" />
                <%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_312, ApmMonitor.Status.ToLocalizedString())%>
                </td>
            </tr>
			<% if (ApmMonitor.CurrentError.ShowStatusDetails)
            { %>
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_313%></td>
                <td class="Property"><%= ApmMonitor.CurrentError.GetStatusDetailsInline()%></td>
            </tr>
			<% } else %>
            <% if (ApmMonitor.Type.Value == ComponentType.NagiosScript)
            { %>
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_313%></td>
                <td class="Property"><%=ApmMonitor.CurrentError%></td>
            </tr>
			<% } %>
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_314 %></td>
                <td class="Property"><%= ApmMonitor.Type %></td>
            </tr>    
             <% if (ApmMonitor.Type.Value == ComponentType.WindowsService)
               { %>
             <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_ServiceStatus %></td>
                <td  class="Property"><span id="serviceStatusContainer"></span></td>
            </tr>    
               <% } else{ %>
             <tr>
                <td class="PropertyHeader"><br /></td>
                <td class="Property"></td>
            </tr> 
               <% } %>
                          
            <asp:Repeater ID="statisticsRepeater" runat="server" OnInit="StatisticsRepeaterInit">
                <ItemTemplate>
                    <tr>
                        <td class="PropertyHeader"><%# HttpUtility.HtmlEncode(Eval("DisplayName"))%><%#String.IsNullOrEmpty(Eval("DisplayName").ToString()) ? "<br>" : Resources.APMWebContent.Punctuation_colon%></td>
                        <td class="Property apm_WrapLines"><%# HttpUtility.HtmlEncode( Eval("DisplayValue"))%></td>                    
                    </tr>
                </ItemTemplate>
            </asp:Repeater>            
            
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_315 %></td>
                <td class="Property"><%= ApmMonitor.LastTimeUp.ToString("f", System.Globalization.CultureInfo.CurrentUICulture)%></td>
            </tr>    
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_316%></td>
                <td class="Property"><%= ApmMonitor.LastTimeUpElasped %>&nbsp;</td>
            </tr>    
            <tr>
                <td class="PropertyHeader">&nbsp;</td>
                <td class="Property">&nbsp;</td>
            </tr>    
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_317 %></td>
                <td class="Property"><%= ApmMonitor.NextPollTime.ToString("f", System.Globalization.CultureInfo.CurrentUICulture)%></td>
            </tr>    
            
            <% if (ApmMonitor.IsThresholdDisplayed)
               {%>
            <tr>
                <td class="PropertyHeader" colspan="2"><br/></td>
            </tr>
            <% }%>
            
            
            <apm:MonitorThresholdsRepeater runat="server" ID="ThresholdsRepeater" OnInit="ThresholdsRepeater_OnInit"/>
        </table>
    </Content>
</orion:resourceWrapper>
