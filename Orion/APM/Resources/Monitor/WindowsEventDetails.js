/// <reference path="/orion/js/extjs/4.0.7/debug/ext-all-sandbox-debug.js" />
/// <reference path="/Orion/APM/js/Ext4AjaxProxy.js" />
Ext4.apply(Ext4, {
    isIE: true,
    isIE11: true,
    ieVersion: 11
});

if (typeof (SW) == "undefined") {window.SW = {};}
if (typeof (SW.APM) == "undefined") {SW.APM = {};}
if (typeof (SW.APM.WindowsEventGrid) == "undefined") {SW.APM.WindowsEventGrid = {};}

(function (weg) {

    var renderLevelImage = function(value) {
        var img, title;
        switch (value) {
        case 1:
            img = 'Error';
            title = '@{R=APM.Strings;K=APMWEBJS_PV0_12;E=js}';
            break;
        case 2:
            img = 'Warning';
            title = '@{R=APM.Strings;K=APMWEBJS_PV0_13;E=js}';
            break;
        case 3:
            img = 'Information';
            title = '@{R=APM.Strings;K=APMWEBJS_PV0_14;E=js}';
            break;
        case 4:
            img = 'SuccessAudit';
            title = '@{R=APM.Strings;K=APMWEBJS_PV0_15;E=js}';
            break;
        case 5:
            img = 'FailureAudit';
            title = '@{R=APM.Strings;K=APMWEBJS_PV0_16;E=js}';
            break;
        default:
            img = 'Unknown';
            title = '@{R=APM.Strings;K=APMWEBJS_PV0_18;E=js}';
            break;
        }
        return String.format('<img src="/Orion/APM/Images/EventLog/{0}.png" title="{1}"/>', img, title);
    };

    var renderOptionalString = function(value) {
        if (value == undefined || value == null || value == '') return '-';
        return htmlEncode(value);
    };

    var surroundWithDetailLink = function(value, rowIndex, store) {
        return String.format('<a href="javascript:SW.APM.WindowsEventGrid.showDetails(\'{0}\', {1});">{2}</a>', store.storeId, rowIndex, value);
    };

    var renderLevelColumn = function(value, metaData, record, rowIndex, colIndex, store) {
        return surroundWithDetailLink(renderLevelImage(value), rowIndex, store);
    };

    var renderEventIdColumn = function(value, metaData, record, rowIndex, colIndex, store) {
        return surroundWithDetailLink(value, rowIndex, store);
    };

    var renderMessageColumn = function(value, metaData, record, rowIndex, colIndex, store) {
        return surroundWithDetailLink(htmlEncode(value), rowIndex, store);
    };

    var renderTimeColumn = function(value, metaData, record, rowIndex, colIndex, store) {
        return surroundWithDetailLink(value, rowIndex, store);
    };

    var htmlEncode = function(value) {
        return Ext4.util.Format.htmlEncode(value);
    };

    weg.showDetails = function(storeId, rowIndex) {

        var store = Ext4.data.StoreManager.lookup(storeId);
        var record = store.getAt(rowIndex);
        var data = record.data;

        var html = '<table width="100%">';
        html += String.format('<tr style="background-color:#FAFAFA"><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>',
            '@{R=APM.Strings;K=APMWEBJS_PV0_1;E=js}', //LEVEL
            '@{R=APM.Strings;K=APMWEBJS_PV0_2;E=js}', //EVENT ID
            '@{R=APM.Strings;K=APMWEBJS_PV0_3;E=js}', //DATE & TIME
            '@{R=APM.Strings;K=APMWEBJS_PV0_5;E=js}', //Log Name
            '@{R=APM.Strings;K=APMWEBJS_PV0_6;E=js}', //Source
            '@{R=APM.Strings;K=APMWEBJS_PV0_7;E=js}', //Computer
            '@{R=APM.Strings;K=APMWEBJS_PV0_8;E=js}'); //User
        html += String.format('<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>',
            renderLevelImage(data['EventType']), data['EventCode'], data['TimeGenerated'], htmlEncode(data['LogFile']), htmlEncode(data['SourceName']), renderOptionalString(data['ComputerName']), renderOptionalString(data['User']));
        html += '</table>';

        var window = new Ext4.create('Ext.window.Window', {
            title: '@{R=APM.Strings;K=APMWEBJS_PV0_11;E=js}',
            height: 360,
            width: 800,
            bodyPadding: 10,
            border: 0,
            bodyStyle: 'background: none',
            modal: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                { xtype: 'label', html: html },
                { xtype: 'tbspacer', height: 10 },
                {
                    xtype: 'textarea',
                    readOnly: true,
                    value: record.data['Message'],
                    width: '100%',
                    height: '100%',
                    flex: 1
                }
            ],
            buttons: [{ text: '@{R=APM.Strings;K=JS_OK_Button_Text;E=js}', handler: function() { window.close(); } }],
            buttonAlign: 'center'
        });

        window.show();
    };

    weg.createGrid = function (resourceId, componentId, gridPageSize) {

        var proxy = Ext4.create('AspWebAjaxProxy', {
            url: '/Orion/APM/Services/WindowsEvent.asmx/GetEventsPaged',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            actionMethods: {
                read: 'POST'
            },
            extraParams: {
                componentId: componentId,
                timezoneOffset: new Date().getTimezoneOffset(),
                eventType: 0,
                searchText: ''
            },
            reader: {
                type: 'json',
                root: 'd.DataTable.Rows',
                totalProperty: 'd.TotalRows'
            }
        });

        var dataStore = new Ext4.data.Store(
            {
                id: 'gridStore' + resourceId,
                fields: [
                    { name: 'LogFile', mapping: 0 },
                    { name: 'EventType', mapping: 1 },
                    { name: 'EventCode', mapping: 2 },
                    { name: 'ComputerName', mapping: 3 },
                    { name: 'SourceName', mapping: 4 },
                    { name: 'User', mapping: 5 },
                    { name: 'Message', mapping: 6 },
                    { name: 'TimeGenerated', mapping: 7 }
                ],
                sorters: [{ property: 'TimeGenerated', direction: 'DESC' }],
                pageSize: gridPageSize,
                remoteSort: true,
                autoLoad: true,
                proxy: proxy
            }
        );

        var pagingToolbar = Ext4.create('Ext.toolbar.Paging', {
            store: dataStore,
            dock: 'bottom',
            width: '100%',
            displayInfo: true,
            displayMsg: "@{R=APM.Strings;K=APMWEBJS_PV0_9;E=js}",
            emptyMsg: "@{R=APM.Strings;K=APMWEBJS_PV0_10;E=js}",
            beforePageText: "@{R=APM.Strings;K=APMWEBJS_TM0_34;E=js}",
            afterPageText: "@{R=APM.Strings;K=APMWEBJS_TM0_35;E=js}"
        });

        var sep = Ext4.create('Ext.toolbar.Separator');

        if (resourceId > 0) {
            var label = Ext4.create('Ext.form.Label', {
                html: String.format('<a href="/Orion/APM/WindowsEventDetails.aspx?ComponentId={0}">{1}</a>',
                    componentId,'@{R=APM.Strings;K=APMWEBJS_PV0_17;E=js}')
            });
            pagingToolbar.add([sep, label]);
        } else {
            var labelPS = Ext4.create('Ext.form.Label', {
                text: '@{R=APM.Strings;K=APMWEBJS_TM0_21;E=js}',
                style: 'margin-left: 5px; margin-right: 5px'
            });
            
         //FB157509 
        Ext4.override(Ext4.form.field.ComboBox, {
		     onTriggerClick: function () {
		         var val = this.getRawValue();
		         this.clearValue();
		         if (!this.readOnly && !this.disabled) {
		            if (this.isExpanded) this.collapse();
		                else this.expand();

		             this.inputEl.focus();
		             this.setRawValue(val);
		          }
		      },
		     onKeyUp: function (e, t) {
		         var key = e.getKey(),
		            rv = this.getRawValue();
		         if (!this.readOnly && !this.disabled && this.editable) {
		             this.lastKey = key;
		                 if (!e.isSpecialKey() || key == e.BACKSPACE || key == e.DELETE) {
		                    this.clearValue();
		                    this.setRawValue(key_val);
		                    this.doQueryTask.delay(this.queryDelay);
		                  }
		           }
		           if (this.enableKeyEvents) {
		               this.callParent(arguments);
		            }
		       }
		  });
            
            var comboPS = Ext4.create('Ext.form.ComboBox', {
                regex: /^\d*$/ ,
                store: new Ext4.create('Ext.data.SimpleStore', {
                    fields: ['pageSize'],
                    data: [[10], [20], [30], [40], [50], [100]]
                }),
                displayField: 'pageSize',
                mode: 'local',
                triggerAction: 'all',
                selectOnFocus: true,
                width: 60,
                editable: false,
                value: gridPageSize,
                listeners: {
                    'select': function(c, record) {
                        dataStore.pageSize = record[0].data.pageSize;
                        dataStore.currentPage = 1;
                        pagingToolbar.cursor = 0;
                        //ORION.Prefs.save('PageSize', grid.bottomToolbar.pageSize);
                        pagingToolbar.doRefresh();
                    }
                }
            });
            pagingToolbar.add([sep, labelPS, comboPS]);
        }

        var grid = Ext4.create('Ext.grid.Panel', {
            id: 'grid' + resourceId,
            store: dataStore,
            columns: [
                { header: '@{R=APM.Strings;K=APMWEBJS_PV0_1;E=js}', dataIndex: 'EventType', width: 45, renderer: renderLevelColumn },
                { header: '@{R=APM.Strings;K=APMWEBJS_PV0_2;E=js}', dataIndex: 'EventCode', width: 65, renderer: renderEventIdColumn },
                { header: '@{R=APM.Strings;K=APMWEBJS_PV0_3;E=js}', dataIndex: 'TimeGenerated', width: 160, renderer: renderTimeColumn },
                { header: '@{R=APM.Strings;K=APMWEBJS_PV0_4;E=js}', dataIndex: 'Message', sortable: false, flex: 1, renderer: renderMessageColumn }
            ],
            stripeRows: true
        });

        /* full window version (requires empty page)
        if (resourceId == 0) {
            pagingToolbar.dock = 'bottom';
            grid.addDocked(pagingToolbar);
            grid.region = 'center';
            var viewPort = Ext4.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [grid]
            });
            viewPort.render(Ext4.getBody());
        } else {*/
            pagingToolbar.dock = 'bottom';
            grid.addDocked(pagingToolbar);
            grid.render('WindowsEventGrid-' + resourceId);
        //}

        var filterBox = new Ext4.Container({
            layout: {
                type: 'hbox'
            },
            width: 300,
            items: [{
                xtype: 'label',
                text: '@{R=APM.Strings;K=ApmWeb_Common_Filter;E=js}' + ':',
                padding: '2px 0 0'
            },{
                xtype: 'combo',
                store: new Ext4.data.SimpleStore({
                    fields: ['value', 'icon', 'label'],
                    data: [['0', '/Orion/APM/Images/EventLog/All.png', '@{R=APM.Strings;K=ApmWeb_Common_All;E=js}'],
                            ['1', '/Orion/APM/Images/EventLog/Error.png', '@{R=APM.Strings;K=APMWEBJS_PV0_12;E=js}'],
                            ['2', '/Orion/APM/Images/EventLog/Warning.png', '@{R=APM.Strings;K=APMWEBJS_PV0_13;E=js}'],
                            ['3', '/Orion/APM/Images/EventLog/Information.png', '@{R=APM.Strings;K=APMWEBJS_PV0_14;E=js}'],
                            ['4', '/Orion/APM/Images/EventLog/SuccessAudit.png', '@{R=APM.Strings;K=APMWEBJS_PV0_15;E=js}'],
                            ['5', '/Orion/APM/Images/EventLog/FailureAudit.png', '@{R=APM.Strings;K=APMWEBJS_PV0_16;E=js}']]
                }),
                id: 'filterCombo' + resourceId,
                width: 200,
                value: '0',
                queryMode: 'local',
                displayField: 'label',
                valueField: 'value',
                allowBlank: true,
                forceSelection: true,
                editable: false,
                listConfig: {
                    itemTpl: Ext4.create('Ext.XTemplate', '<div class="x-combo-list-item"><img src="{icon}" align="left" width="16" />&nbsp&nbsp{label}</div>')
                },
                listeners: {
                    select: function (combo, records) {
                        SW.APM.WindowsEventGrid.refreshGrid(resourceId, function (store) {
                            store.proxy.extraParams.eventType = combo.getValue();
                            store.currentPage = 1;
                        });
                    }
                }
            }]
        });
        filterBox.render('WindowsEventGrid-' + resourceId + '-toolbar-left');

        var searchBox = new Ext4.Container({
            layout: {
                type: 'hbox',
                pack: 'end'
            },
            width: 200,
            items: [{
                xtype: 'textfield',
                id: 'searchInput' + resourceId,
                width: 130,
                enableKeyEvents: true,
                listeners: {
                    keyup: function (cmp, e) {
                        if (e.getKey() == 13) {
                            SW.APM.WindowsEventGrid.refreshGrid(resourceId, function (store) {
                                store.proxy.extraParams.searchText = cmp.getValue();
                                store.currentPage = 1;
                            });
                        };
                    }
                }
            }, {
                xtype: 'button',
                id: 'searchButton' + resourceId,
                iconAlign: 'left',
                icon: '/Orion/APM/Images/icon_search.png',
                border: 0,
                handler: function () {
                    var searchInput = Ext4.getCmp('searchInput' + resourceId);
                    SW.APM.WindowsEventGrid.refreshGrid(resourceId, function (store) {
                        store.proxy.extraParams.searchText = searchInput.getValue();
                        store.currentPage = 1;
                    });
                }
            }]
        });
        searchBox.render('WindowsEventGrid-' + resourceId + '-toolbar-right');

        //another option would be toolbar like this
        //
        //var toolbar = Ext4.create('Ext.toolbar.Toolbar', {
        //    width: '100%',
        //    border: 0,
        //    color: 'none',
        //    items: [
        //        filterBox,
        //        '->',
        //        searchBox
        //    ]
        //});
        //toolbar.dock = 'top';
        //grid.addDocked(toolbar);

    };

    weg.refreshGrid = function (resourceId, callback) {
        var grid = Ext4.getCmp('grid' + resourceId);
        var store = grid.getStore();
        if (callback) {
            callback(store);
        };
        store.load();
    };
})(SW.APM.WindowsEventGrid);
