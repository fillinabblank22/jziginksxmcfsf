using System;
using System.Linq;

using SolarWinds.APM.Common.Helpers;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.DisplayStrategies;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

using Threshold = SolarWinds.APM.Common.Models.Threshold;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Monitor_MonitorDetails : ApmMonitorBaseResource
{
    protected override string DefaultTitle
	{
		get { return Resources.APMWebContent.APMWEBCODE_VB1_127; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHMonitorDetailsMonDetails"; }
	}

    protected bool AllowProcessRealTime
	{
		get { return ApmRoleAccessor.AllowRealTimeProcessExplorer; }
	}

	protected bool AllowServiceRealTime
	{
		get { return ApmMonitor.Type.Value == ComponentType.WindowsService && (ApmRoleAccessor.AllowRealTimeServiceExplorer || ApmRoleAccessor.AllowRealTimeServiceManage); }
	}

	protected bool AllowEventRealTime
	{
		get { return ApmMonitor.Type.Value == ComponentType.WindowsEventLog && ApmRoleAccessor.AllowRealTimeEventExplorer; }
	}

    private INodeManagementTasksDisplayStrategy nodeManagementTasksDisplayStrategy;
    protected INodeManagementTasksDisplayStrategy NodeManagementTasksDisplayStrategy
    {
        get
        {
            return nodeManagementTasksDisplayStrategy ?? (nodeManagementTasksDisplayStrategy =
                ServiceLocatorForWeb.GetServiceForWeb<ITasksDisplayStrategyFactory>().CreateTasksDisplayStrategy(ApmApplication.Node));
        }
    }

    protected bool ShowServiceManagerLink
    {
        get { return NodeManagementTasksDisplayStrategy.IsServiceManagerSupported(); }
    }

    protected bool ShowEventExplorerLink
    {
        get { return NodeManagementTasksDisplayStrategy.IsEventExplorerSupported(); }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    protected int GetRtMonitorCredId()
    {
        return NodeManagementTasksDisplayStrategy.GetWorkingWindowsCredentialId();
    }

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		AppStatusIcon.StatusValue = ApmMonitor.Application.Status;
		MonitorStatusIcon.StatusValue = ApmMonitor.Status;
		MonitorStatusIcon.ErrorValue = ApmMonitor.CurrentError;

	    alertSuppressionStatus.EntityUri = ApmMonitor.Application.SwisUri;
        alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;
    }

    protected void StatisticsRepeaterInit(object sender, EventArgs e)
	{
		statisticsRepeater.DataSource = ApmMonitor.Statistics;
		statisticsRepeater.DataBind();
	}

	protected bool IsThresholdDisplayed(Threshold threshold)
	{
		if (ApmMonitor.IsThresholdDisplayed)
		{
            var enumValue = ParseHelper.ParseEnum<ThresholdNames>(threshold.Name);
            switch (enumValue)
            {
                case ThresholdNames.CPU:
                case ThresholdNames.PMem:
                    return ApmMonitor.BaseComponent.IsProcessBased;
                case ThresholdNames.VMem:
                case ThresholdNames.IOReadOperationsPerSec:
                case ThresholdNames.IOWriteOperationsPerSec:
                case ThresholdNames.IOTotalOperationsPerSec:
                    return ApmMonitor.BaseComponent.HasVirtualMemorySupport;
                case ThresholdNames.Response:
                    return ApmMonitor.BaseComponent.HasResponseTimeSupport;
                case ThresholdNames.StatisticData:
                    return ApmMonitor.BaseComponent.HasStatisticData;
            }
		}
		return false;
	}

    protected void ThresholdsRepeater_OnInit(object sender, EventArgs e)
    {
        ThresholdsRepeater.IsThresholdDisplayed = this.IsThresholdDisplayed;
        ThresholdsRepeater.Thresholds = ApmMonitor.BaseComponent.Thresholds.Values.ToList();
    }
}
