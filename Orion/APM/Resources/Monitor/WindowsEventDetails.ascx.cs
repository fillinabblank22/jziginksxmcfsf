﻿using System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.MonitorProviders;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Monitor_WindowsEventDetails : ApmMonitorBaseResource
{
    private const string _propertyPageSize = "pageSize";

    private ApmMonitor eventLogMonitor;
    private Boolean eventLogMonitorInitialize = false;

    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IApplicationProviderBase) };
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ApmMonitor.Type.Value != ComponentType.WindowsEventLog)
        {
            this.Visible = false;
        }
    }

    public override ApmMonitor ApmMonitor
    {
        get
        {
            if (!eventLogMonitorInitialize)
            {
                eventLogMonitorInitialize = true;
                eventLogMonitor = GetEventLogMonitor();
            }
            return eventLogMonitor != null ? eventLogMonitor : base.ApmMonitor;
        }
    }

    private SolarWinds.APM.Web.ApmMonitor GetEventLogMonitor()
    {
        var monitorProvider = GetInterfaceInstance<IEventLogMonitorProvider>(this);
        if (monitorProvider != null)
        {
            try
            {
                return monitorProvider.EventLogMonitor;
            }
            catch (Exception)
            {
                return null;
            }
        }
        return null;
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_PV0_1; }
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment("OrionAPMPHMonitorDetailsWinEventDetails") ; }
    }

    protected override String EditPageUrl
    {
        get { return "/Orion/APM/Resources/EditWindowsEventDetails.aspx"; }
    }

    public int GridPageSize
    {
        get
        {
            int result;
            if (!int.TryParse(Resource.Properties[_propertyPageSize],out result))
            {
                result = ComponentConstants.WindowsEventLog.DefaultPageSize;
            }
            return result;
        }
    }
}