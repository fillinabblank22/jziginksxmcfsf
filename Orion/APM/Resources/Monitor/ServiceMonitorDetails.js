﻿Ext.namespace('SW.APM');

SW.APM.ServicesMonitorDetailsHandlers = (function () {
    var appDetails = {};
    var service;
    var getServiceByName = function (name, services) {
    	service = null;
    	var lowerName = name.toLowerCase();
        Ext.each(services, function () {
        	if (this.Name.toLowerCase() == lowerName || this.DisplayName.toLowerCase() == lowerName) {
                service = this;
                return service;
            }
        });
        return service;
    };

    var showErrorMessage = function (message) {
        Ext.Msg.show({ title: "@{R=APM.Strings;K=JS_ServiceErrorDetails_Message;E=js}", msg: message, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK });
    };
    
    var showErrorDialog = function (error) {
        Ext.Msg.show({ title: '@{R=APM.Strings;K=JS_Error_Message;E=js}', msg: error, buttons: { ok: '@{R=APM.Strings;K=JS_OK_Button_Text;E=js}' }, width: 400, height: 150, icon: Ext.MessageBox.ERROR });
    };
    
    var dependencyDialog = function (action, msg, successAction, nodeId, credentialId, serviceName) {
        Ext.Msg.show({
            buttons: Ext.Msg.YESNO,
            title: action + ' ' + '@{R=APM.Strings;K=JS_Related_Services_Message;E=js}',
            msg: msg,
            icon: Ext.MessageBox.WARNING,
            width: 420,
            fn: function (btn) {
                if (btn == 'yes') {
                    successAction(nodeId, credentialId, serviceName);
                }
            }
        });
    };

    var getServiceDependenciesHtml = function (dependencies) {
        var depMsg = '';
        Ext.each(dependencies, function () {
            if (this.AcceptStop == true && this.State == "Running") {
                depMsg += '<li> -  ' + this.DisplayName + ' (' + this.Name + ')' + '</li>';
            }
        });
        if (depMsg != '') {
            return '<div style=\"max-height: 250px; overflow:auto; overflow-x:hidden; padding-top:5px\" ><ul>' + depMsg + '</ul></div>';
        } else {
            return depMsg;
        }
    };

    var renderService = function (service, serviceActionMessage, serviceErrorMessage) {
        var startStopRestartServiceContainer = $('#startStopRestartServiceContainer');

        if (startStopRestartServiceContainer[0]) {
            if (service.State == 'Stopped') {
                var startClick = 'SW.APM.ServicesMonitorDetailsHandlers.StartService (' + appDetails.NodeId + ',' + appDetails.CredentialId + ',\'' + appDetails.ServiceName.toString() + '\');';
                startStopRestartServiceContainer[0].innerHTML = '<a href="javascript:void(0)" onclick="return ' + startClick + '"><img alt="" src="/Orion/APM/Images/ProcessMonitor/icon_play.png" />' + ' ' + '@{R=APM.Strings; K=APMWEBJS_StartThisService; E=js}' + '</a>';
            }
            if (service.State == 'Running') {
                var stopClick = 'SW.APM.ServicesMonitorDetailsHandlers.StopService (' + appDetails.NodeId + ',' + appDetails.CredentialId + ',\'' + appDetails.ServiceName.toString() + '\');',
                    restartClick = 'SW.APM.ServicesMonitorDetailsHandlers.RestartService (' + appDetails.NodeId + ',' + appDetails.CredentialId + ',\'' + appDetails.ServiceName.toString() + '\');';

                var stopLink = '<a href="javascript:void(0)" onclick="return ' + stopClick + '"><img alt="" src="/Orion/APM/Images/stop_service.png" />' + ' ' + '@{R=APM.Strings;K=APMWEBJS_Stop_this_service;E=js}' + '</a>',
                    restartLink = '<a href="javascript:void(0)" onclick="return ' + restartClick + '"><img alt="" src="/Orion/APM/Images/restart_service.png" />' + ' ' + '@{R=APM.Strings;K=APMWEBJS_Restart_this_service;E=js}' + '</a>';

                startStopRestartServiceContainer[0].innerHTML = restartLink + '<br />' + stopLink;
            }
        }
        renderServiceStatus(service.State, serviceActionMessage, serviceErrorMessage);
    };

    var renderServiceStatus = function (status, serviceActionMessage, serviceErrorMessage) {
        var res;
        var processingAction = $('#serviceStatusContainer');
        if (!Ext.isDefined(processingAction[0])) return;
        
        switch (status) {
            case 'Running':
                res = '<img class="row-img" src="/Orion/Images/icon_run.gif"/>&nbsp;&nbsp;' + status;
                break;
            case 'Stopped':
                res = '<img class="row-img" src="/Orion/Images/Cmd-Stopped.png"/>&nbsp;&nbsp;' + status;
                break;
            default:
                res = status;
                break;
        }

        if (serviceActionMessage != null || serviceErrorMessage != null) {
            var msg = serviceActionMessage ? serviceActionMessage : serviceErrorMessage,
                errorBoxHtml = '<div class="status-details-container" status="NotAvailable"><span>{0}</span><label>{1}</label></div>',
                spacing = Array(6).join('&nbsp;');

            if (res != '') res += '</br>';

            res += String.format(errorBoxHtml, spacing, msg);

            processingAction[0].innerHTML = res;

            SW.APM.TrimStatusDetails();
        } else {
            processingAction[0].innerHTML = res;
        }
    };

    var stopService = function (nodeId, credentialId, serviceName) {
        var processingAction = $('#serviceStatusContainer');
        if (!Ext.isDefined(processingAction[0])) return;
     
        if (service && service.Unmanaged == true) {
            showErrorDialog('The <b>' + service.DisplayName + '</b>' + ' ' +  '@{R=APM.Strings;K=JS_ServiceCannotBeStopped_Message;E=js}');
            return;
        }
        
        var action = "@{R=APM.Strings;K=JS_Stopping_Action;E=js}";
        processingAction[0].innerHTML = "<img src='/Orion/APM/Images/loading_gen_16x16.gif'></img>" + action;
        var parameters = "{nodeId:" + nodeId + ",'credentialId':'" + credentialId + "','serviceName':'" + serviceName + "'}";
        $.ajax({
            type: 'Post',
            data: parameters,
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            dataType: "json",
            url: '/Orion/APM/Services/Services.asmx/StopService',
            success: function (data) {
                processingAction[0].innerHTML = "";
                service = getServiceByName(serviceName, data.d.Services);
                var serviceActionMessage = null, serviceErrorMessage = null;
                if (service && service.ActionExitCode != 0 && service.ActionMessage) { serviceActionMessage = service.ActionMessage;}

                if (data.d.ErrorMessage != null && data.d.PollStatus != 0) { serviceErrorMessage = data.d.ErrorMessage;}

                if (service) { renderService(service, serviceActionMessage, serviceErrorMessage); }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                processingAction[0].innerHTML = "";
                renderServiceStatus("",thrownError, null);
            }
        });
    };

    var restartService = function (nodeId, credentialId, serviceName) {       
        var processingAction = $('#serviceStatusContainer');
        if (!Ext.isDefined(processingAction[0])) return;

        if (service && service.Unmanaged == true) {
            showErrorDialog('@{R=APM.Strings;K=JS_ServiceCannotBeRestarted_Message;E=js}'.format(service.DisplayName));
            return;
        }
        
        var action = "@{R=APM.Strings;K=JS_Restarting_Action;E=js}"; 
        processingAction[0].innerHTML = "<img src='/Orion/APM/Images/loading_gen_16x16.gif'></img>" + action;
        var parameters = "{nodeId:" + nodeId + ",'credentialId':'" + credentialId + "','serviceName':'" + serviceName + "'}";
        $.ajax({
            type: 'Post',
            data: parameters,
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            dataType: "json",
            url: '/Orion/APM/Services/Services.asmx/RestartService',
            success: function (data) {
                processingAction[0].innerHTML = "";
                service = getServiceByName(serviceName, data.d.Services);
                var serviceActionMessage = null, serviceErrorMessage = null;

                if (service && service.ActionExitCode != 0 && service.ActionMessage) { serviceActionMessage = service.ActionMessage; }

                if (data.d.ErrorMessage != null && data.d.PollStatus != 0) { serviceErrorMessage = data.d.ErrorMessage; }

                if (service) { renderService(service, serviceActionMessage, serviceErrorMessage); }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                processingAction[0].innerHTML = "";
                renderServiceStatus("", thrownError, null);
            }
        });
    };

    return {
        init: function () {
        },
        GetServicesInfo: function (nodeId, credentialId, serviceName) {
            appDetails.NodeId = nodeId;
            appDetails.CredentialId = credentialId;
            appDetails.ServiceName = serviceName;
            var processingAction = $('#serviceStatusContainer');
            if (!Ext.isDefined(processingAction[0])) return;
            
            var action = " Loading...";
            processingAction[0].innerHTML = "<img src='/Orion/APM/Images/loading_gen_16x16.gif'></img>" + action;
            var parameters = "{nodeId:" + nodeId + ",'credentialId':'" + credentialId + "','serviceName':'" + serviceName + "'}";
            $.ajax({
                type: 'Post',
                data: parameters,
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                dataType: "json",
                url: '/Orion/APM/Services/Services.asmx/GetServiceInfoSync',
                success: function (data) {
                    processingAction[0].innerHTML = "";
                    service = getServiceByName(serviceName, data.d.Services);
                    if (service && service.ActionExitCode != 0 && service.ActionMessage) { renderServiceStatus("", service.ActionMessage, null); }
                    
                    if (data.d.ErrorMessage != null && data.d.PollStatus != 0) { renderServiceStatus("", data.d.ErrorMessage, null); }

                    if (service) { renderService(service); }  
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    processingAction[0].innerHTML = "";
                    renderServiceStatus("", thrownError, null);
                }
            });
        },

        StartService: function (nodeId, credentialId, serviceName) {
            
            if (SW.APM.ServicesMonitorDetailsHandlers.IsDemo == 'true') {
                DemoModeAction("Service_Component_Details_Start_Service");
                return;
            }
            
            var processingAction = $('#serviceStatusContainer');
            if (!Ext.isDefined(processingAction[0])) return;
            
            var action = "@{R=APM.Strings;K=JS_Starting_Action;E=js}";
            processingAction[0].innerHTML = "<img src='/Orion/APM/Images/loading_gen_16x16.gif'></img>" + action;

            var parameters = "{nodeId:" + nodeId + ",'credentialId':'" + credentialId + "','serviceName':'" + serviceName + "'}";
            $.ajax({
                type: 'Post',
                data: parameters,
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                dataType: "json",
                url: '/Orion/APM/Services/Services.asmx/StartService',
                success: function (data) {
                    processingAction[0].innerHTML = "";
                    service = getServiceByName(serviceName, data.d.Services);
                    var serviceActionMessage = null,  serviceErrorMessage = null;
                    if (service && service.ActionExitCode != 0 && service.ActionMessage) { serviceActionMessage = service.ActionMessage;}

                    if (data.d.ErrorMessage != null && data.d.PollStatus != 0) { serviceErrorMessage = data.d.ErrorMessage;}
                 
                    if (service) { renderService(service, serviceActionMessage, serviceErrorMessage); }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    processingAction[0].innerHTML = "";
                    renderServiceStatus("", thrownError, null);
                }
            });
        },

        RestartService: function (nodeId, credentialId, serviceName) {
            
            if (SW.APM.ServicesMonitorDetailsHandlers.IsDemo == 'true') {
                DemoModeAction("Service_Component_Details_Restart_Service");
                return;
            }
            
            if (service && service.Dependencies && service.Dependencies.length > 0) {
                var msg = '<b>' + '@{R=APM.Strings;K=JS_RestartServices_Message;E=js}'.format(service.DisplayName) + ' </b><br/><br/> <div>' + '@{R=APM.Strings;K=JS_ServicesWillBeRestarted_Message;E=js}' + '<br/>';
                var depHtml = getServiceDependenciesHtml(service.Dependencies);
                
                if (depHtml != '') {
                    var resMsg = msg + depHtml + '</div>';
                    dependencyDialog('@{R=APM.Strings;K=JS_Restart_Action;E=js}', resMsg, restartService, nodeId, credentialId, serviceName);
                } 
                else { restartService(nodeId, credentialId, serviceName); }
            }
            else { restartService(nodeId, credentialId, serviceName); }
        },

        StopService: function (nodeId, credentialId, serviceName) {

            if (SW.APM.ServicesMonitorDetailsHandlers.IsDemo == 'true') {
                DemoModeAction("Service_Component_Details_Stop_Service");
                return;
            }

            if (service && service.Dependencies && service.Dependencies.length > 0) {
                var msg = '<b>' + '@{R=APM.Strings;K=JS_StopServices_Message;E=js}'.format(service.DisplayName) + '</b><br/><br/> <div>' + '@{R=APM.Strings;K=JS_ServicesWillBeStopped_Message;E=js}' + '<br/>';
                var depHtml = getServiceDependenciesHtml(service.Dependencies);

                if (depHtml != '') {
                    var resMsg = msg + depHtml + '</div>';
                    dependencyDialog('@{R=APM.Strings;K=JS_Stop_Action;E=js}', resMsg, stopService, nodeId, credentialId, serviceName);
                }
                else { stopService(nodeId, credentialId, serviceName); }
            }
            else { stopService(nodeId, credentialId, serviceName); }
        }
    };
})();