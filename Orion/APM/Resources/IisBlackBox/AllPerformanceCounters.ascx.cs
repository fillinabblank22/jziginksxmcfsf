﻿using System;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_APM_Resources_IisBlackBox_AllPerformanceCounters : IisStatisticResourceBase
{
    protected override string DefaultTitle
    {
        get { return APMWebContent.Web_Performance_Counters; }
    }
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected string CustomApplicationType
    {
        get { return IisStatistic.Application.CustomType; }
    }

    protected int ApplicationId
    {
        get { return IisStatistic.IisApplication.Id; }
    }

    protected int ApplicationItemId
    {
        get { return IisStatistic.BaseComponent.ApplicationItemID ?? -1; }
    }

    protected bool IsApplicationItemSpecific
    {
        get { return IisStatistic.IsApplicationItemSpecific; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        bool compatible = (this.IisStatistic != null);
        this.incompatibleComponentPlaceHolder.Visible = !compatible;
        this.contentPlaceHolder.Visible = compatible;
    }
}
