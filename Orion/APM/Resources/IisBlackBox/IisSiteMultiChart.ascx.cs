﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_IisSiteMultiChart : IisMultiChartBaseResource, IResourceIsInternal
{
	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(ISiteProvider) }; }
	}
	
	public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		InitChartLocation(pnlLeft, pnlRight, pnlMiddle);
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		InitChartVisible(WrapperContents, rptMonitors);
	}
}
