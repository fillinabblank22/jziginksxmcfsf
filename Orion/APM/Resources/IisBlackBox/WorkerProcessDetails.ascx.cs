﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.BlackBox.IIS.Web.DAL;
using SolarWinds.APM.BlackBox.IIS.Web.Models;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Tables;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_WorkerProcessDetails : ApmBaseResource
{
	protected override string DefaultTitle
	{
		get { return "Worker Process Details"; }
	}

    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override string HelpLinkFragment
	{
        get { return "SAMAGAppInforIISWorkProDetRes"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.RenderControl; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(IApplicationPoolProvider) }; }
	}

	public ApplicationPool Pool
	{
		get { return GetInterfaceInstance<IApplicationPoolProvider>().ApplicationPool; }
	}

	public WorkerProcesses Processes
	{
		get { return Pool.Processes; }
	}

    public long WorkerProcessComponentId { get; set; }

    protected bool HasData
	{
		get { return Pool != null && Pool.Processes.HasData; } 
	}

	protected void Page_Load(object sender, EventArgs e)
    {
        var applicationPoolDal = new ApplicationPoolDAL();

		componentStatusIcon.StatusValue = new ApmStatus(Status.NotRunning);
		componentStatus.InnerText = EnumHelper.GetDescription(Status.NotRunning);
        if (HasData)
        {
		    using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		    {
		        var statDetailsCmp = businessLayer.GetComponent(Processes.ComponentID);

		        WorkerProcessComponentId = Processes.ComponentID;
                componentStatusIcon.StatusValue = new ApmStatus(statDetailsCmp.Status);
                componentStatus.InnerText = EnumHelper.GetDescription(statDetailsCmp.Status);

		        cpuLoadStatusBar.BarStatus = Pool.ApplicationPoolModel.TotalWpCPUStatus.GetValueOrDefault().OrionApmStatus();
		        cpuLoadStatusBar.BarWidth = 100;
		        cpuLoadStatusBar.BarValueWidth = (int) Math.Round(Processes.TotalPercentCpu);

                physicalMemoryStatusBar.BarStatus = Pool.ApplicationPoolModel.TotalWpPMemStatus.GetValueOrDefault().OrionApmStatus();
		        physicalMemoryStatusBar.BarWidth = 100;
		        physicalMemoryStatusBar.BarValueWidth = (int) Math.Round(Processes.TotalPercentPm);

                virtualMemoryStatusBar.BarStatus = Pool.ApplicationPoolModel.TotalWpWMemStatus.GetValueOrDefault().OrionApmStatus();
		        virtualMemoryStatusBar.BarWidth = 100;
		        virtualMemoryStatusBar.BarValueWidth = (int) Math.Round(Processes.TotalPercentVm);
		    }
		}
		else
		{
             var workerProcessComponentInfo = applicationPoolDal.GetApplicationPoolsWorkerProcessIdAndStatus(Pool.Id);
		    WorkerProcessComponentId = workerProcessComponentInfo.Item1;
            componentStatusIcon.StatusValue = new ApmStatus(workerProcessComponentInfo.Item2);
            componentStatus.InnerText = EnumHelper.GetDescription(workerProcessComponentInfo.Item2);

		}
    }

	protected string GetValue(object value)
	{
	    return HasData ? value.ToString() : Resources.APM_IisBBContent.Sites_Value_NA;
	}

    protected string GetRunningCopiesValue(object value)
    {
        return HasData ? value.ToString() : "0";
    }

    protected string GetProcessDisplayNamesValue(object value)
    {
        return HasData ? value.ToString() : "w3wp.exe";
    }
}