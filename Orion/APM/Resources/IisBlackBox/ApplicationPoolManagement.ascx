﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationPoolManagement.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_ApplicationPoolManagement" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>

<orion:Include ID="i2" File="APM/IisBlackBox/Js/AppItemManagement.js" Section="Bottom" runat="server" />
<orion:Include ID="Include2" runat="server" File="APM/IisBlackBox/Styles/IisManagement.css" />
<orion:Include ID="Include3" runat="server" File="APM/Styles/Resources.css" />

<script type="text/javascript">
function manageApplicationItem(managed) {

    var isDemo = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? 1 : 0 %>;
    if (isDemo)
    {
        DemoModeAction("ApplicationPoolManagement_Manage");
        return;
    }

    var name = '<%= ControlHelper.EncodeJsString(this.ApplicationPool.Name) %>';
    var message = managed ? '<%= ControlHelper.EncodeJsString(APM_IisBBContent.ApplicationPoolManagement_AppPoolRemanageConfirmDialog) %>' 
        : '<%= ControlHelper.EncodeJsString(APM_IisBBContent.ApplicationPoolManagement_AppPoolUnmanageConfirmDialog) %>';
    message = SF(message, name);
    var minimalMessageWindowWidth = 400;
    Ext.Msg.show({
        msg: message,
        buttons: Ext.Msg.YESNO,
        minWidth: minimalMessageWindowWidth,
        fn: function (btn) {
            if (btn === 'yes') {
                var config = {
                    applicationCustomType: '<%= SolarWinds.APM.BlackBox.IIS.Common.Constants.ApplicationTemplateCustomType%>',
                    applicationId: <%= ApplicationPool.IisApplication.Id%>,
                    itemId: <%= ApplicationPool.Id%>,
                    managed: managed,
                    message: message,
                    onComplete: function() { window.location.reload(true); }
                };
                SW.APM.ManageAppItem.manage(config);
            }
        }
    });
		
    return false;
}
</script>

<orion:resourceWrapper ID="resourceContent" runat="server">
    <Content>
        <div class="ReportHeader" style="margin-bottom: 10px">APPLICATION POOL</div>
        <div>
            <span id="restart-container-<%= Resource.ID %>" class="management-button" style="display: none;">
                <a id="restart-<%= Resource.ID %>" href="javascript:void(0)">
                    <img class="NodeManagementIcons" src="/Orion/APM/IisBlackBox/Images/restart.png" alt="">
                    Recycle</a>
            </span>
            <span id="stop-container-<%= Resource.ID %>" class="management-button" style="display: none;">
                <a id="stop-<%= Resource.ID %>" href="javascript:void(0)">
                    <img class="NodeManagementIcons" src="/Orion/APM/IisBlackBox/Images/stop.png" alt="">
                    Stop</a>
            </span>
            <span id="start-container-<%= Resource.ID %>" class="management-button" style="display: none;">
                <a id="start-<%= Resource.ID %>" href="javascript:void(0)">
                    <img class="NodeManagementIcons" src="/Orion/APM/IisBlackBox/Images/play.png" alt="">
                    Start</a>
            </span>
<%
    if (ApplicationPool.IisApplication.Status.Value != Status.Unmanaged && Profile.AllowUnmanage)
{
    if (ApplicationPool.ApmStatus.Value != Status.Unmanaged)
    {
%>
            <span id="unmanage-container-<%= Resource.ID %>" class="management-button">
                <a id="unmanage-<%= Resource.ID %>" onclick="return manageApplicationItem(false);" href="#">
                    <img class="NodeManagementIcons" src="/Orion/Nodes/images/icons/icon_manage.gif" alt="">
                    <%= APM_IisBBContent.ApplicationPoolManagement_AppPoolUnmanageBtn %></a>
            </span>
<%
    }
    else
    {
%>
            <span id="remanage-container-<%= Resource.ID %>" class="management-button">
                <a id="remanage-<%= Resource.ID %>" onclick="return manageApplicationItem(true);" href="#">
                    <img class="NodeManagementIcons" src="/Orion/Nodes/images/icons/icon_remanage.gif" alt="">
                    <%= APM_IisBBContent.ApplicationPoolManagement_AppPoolRemanageBtn %></a>
            </span>
<%
    }
}
%>
        </div>
        <div id="inprogress-container-<%= Resource.ID %>" style="display: none; padding-top: 10px;">
            <img src="/Orion/APM/Images/loading_gen_16x16.gif" />
            <label id="message-<%= Resource.ID %>"></label>
        </div>
        <div style="padding-top: 10px;"></div>

        <span id="managementError-<%= Resource.ID %>" class="validationMsg sw-suggestion sw-suggestion-fail" style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <span>
                <label id="manage-action-error-<%= Resource.ID %>"></label>
            </span>
        </span>
        <span id="managementSucess-<%= Resource.ID %>" class="validationMsg sw-suggestion sw-suggestion-pass " style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <span class="">
                <label id="manage-action-success-<%= Resource.ID %>"></label>
            </span>
        </span>
        <script type="text/javascript">
$(function () {
    var data = {
        poolName: '<%= ApplicationPool.Name %>',
        nodeId: '<%= ApplicationPool.IisApplication.Node.Id %>',
        poolId: '<%= ApplicationPool.Id %>',
        resourceId: '<%= Resource.ID %>',
        applicationId: '<%= ApplicationPool.IisApplication.Id%>',
        templateId: '<%= ApplicationPool.IisApplication.TemplateId%>',
        show: !(<%= this.HideManagementControls ? 1 : 0 %>),
        poolState: '<%= ApplicationPool.State %>',
        isDemo: <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? 1 : 0 %> 
    };
    new SW.APM.IisBB.ApplicationPoolManagement(data);
});
        </script>

        <div style="display: none;">
            <div id="iisConfirmDialog-<%= Resource.ID %>" title="" class="dialog-border">
                <table>
                    <tr>
                        <td>
                            <img src="/Orion/images/NotificationImages/notification_warning.gif" style="padding-right: 15px" /></td>
                        <td>
                            <label id="actionMessage-<%= Resource.ID %>"></label>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <div align="right">
                    <orion:LocalizableButton ID="btnYes" Text="Yes" DisplayType="Primary" runat="server" CssClass="btn-yes" />
                    &nbsp;&nbsp;
				<orion:LocalizableButton ID="btnNo" Text="no" DisplayType="Secondary" runat="server" CssClass="btn-no" />
                </div>
            </div>
        </div>
    </Content>
</orion:resourceWrapper>
