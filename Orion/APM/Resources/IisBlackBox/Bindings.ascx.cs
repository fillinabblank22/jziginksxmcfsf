﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_Bindings : ApmBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var siteProvider = this.GetInterfaceInstance<ISiteProvider>();

        string swqlQuery =
            InvariantString.Format(@"SELECT 
	b.Protocol as [Protocol], 
    b.BindingInformation as [BindingInformation],
	b.HostName as [_HostName], 
	b.Port as [_Port], 
	b.IpAddress as [_IpAddress], 
	b.Site.Application.Node.IP_Address as [_NodeIPAddress]
FROM Orion.APM.IIS.SiteBinding b WHERE b.SiteID = {0}", siteProvider.IisSite.SiteModel.Id);

        string checkQuery = InvariantString.Format(@"SELECT SiteID FROM Orion.APM.IIS.SiteBinding WHERE SiteID = {0}",
            siteProvider.IisSite.SiteModel.Id);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var queryResult = swis.Query(checkQuery);
            if (queryResult.Rows.Count == 0)
            {
                Wrapper.Visible = false;
                return;
            }
        }

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = swqlQuery;
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_IisBBContent.Binding_DefaultTitile; }
    }

    public override string HelpLinkFragment
    {
        get { return "samagappinforiisbindings"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ISiteProvider) }; }
    }
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();
}