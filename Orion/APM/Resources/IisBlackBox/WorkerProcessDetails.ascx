﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WorkerProcessDetails.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_WorkerProcessDetails" %>
<%@ Register TagPrefix="apm" TagName="StatusBar" Src="~/Orion/APM/Controls/ApmStatusBar.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmStatusIcon" Src="~/Orion/APM/Controls/SmallApmStatusIcon.ascx" %>

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
		<orion:Include ID="i1" runat="server" Module="APM" File="/IisBlackBox/Styles/Resources.css"/>

		<table cellspacing="0" class="apm-iis-wpd NeedsZebraStripes">
            <%-- Monitored Process --%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_MonitoredProcess %>
                </td>
                <td class="Property value">
                    <%= GetProcessDisplayNamesValue(Processes.DisplayNames) %>
                </td>
                <td colspan="2" class="Property value">
					<% if (WorkerProcessComponentId != 0)
                 { %>
					<a href="/Orion/APM/MonitorDetails.aspx?NetObject=AM:<%= WorkerProcessComponentId %>" class="coloredLink">
						>> View Details
					</a>
					<% } %>
                </td>
            </tr>
			<%-- Status --%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_Status %>
                </td>
                <td colspan="3" class="Property value">
                    <apm:SmallApmStatusIcon ID="componentStatusIcon" runat="server" />
                    <label id="componentStatus" runat="server"></label>
                </td>
            </tr>
			<%-- Running Copies --%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_RunningCopies %>
                </td>
                <td colspan="3" class="Property value">
					<%= GetRunningCopiesValue(Processes.Count) %>
                </td>
            </tr>
			<%-- Maximum Worker Processes --%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_MaximumWorkerProcesses %>
                </td>
                <td colspan="3" class="Property value">
					<%= Pool.ApplicationPoolModel.MaxProcesses %>
                </td>
            </tr>
			<%-- Process ID(s) --%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_ProcessIDs %>
                </td>
                <td colspan="3" class="Property value">
					<%= GetValue(Processes.DisplayPids) %>
                </td>
            </tr>
			<%--CPU Load--%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_CpuLoad %>
                </td>
                <td class="Property value">
					<%= GetValue(Processes.DisplayTotalPercentCpu) %>
                </td>
				<td class="Property status">
				    <apm:StatusBar ID="cpuLoadStatusBar" BarStatus="NotRunning" BarWidth="100" BarValueWidth="0" runat="server" />
				</td>
                <td>&nbsp;</td>
            </tr>
			<%-- Physical Memory Load--%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_PmLoad %>
                </td>
                <td class="Property value">
					<%= GetValue(Processes.DisplayTotalPercentPm) %>
                </td>
                <td class="Property status">
  				    <apm:StatusBar ID="physicalMemoryStatusBar" BarStatus="NotRunning" BarWidth="100" BarValueWidth="0" runat="server" />
                </td>
                <td class="Property value" style="padding-left: 10px;">
					<%= GetValue(Processes.DisplayTotalPmUsed) %>
                </td>
            </tr>
			<%-- Virtual Memory Load --%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_VmLoad %>
                </td>
                <td class="Property value">
					<%= GetValue(Processes.DisplayTotalVmPercent) %>
                </td>
                <td class="Property status">
                    <apm:StatusBar ID="virtualMemoryStatusBar" BarStatus="NotRunning" BarWidth="100" BarValueWidth="0" runat="server" />
                </td>
                <td class="Property value" style="padding-left: 10px;">
					<%= GetValue(Processes.DisplayTotalVmUsed) %>
                </td>
            </tr>
			<%-- IOPS --%>
            <tr>
                <td class="PropertyHeader label">
                    <%= Resources.APM_IisBBContent.WorkerProcessDetails_Iops %>
                </td>
                <td colspan="3" class="Property value">
                    <%= GetValue(Processes.DisplayTotalIops) %>
                </td>
            </tr>
		</table>
	</Content>
</orion:resourceWrapper>