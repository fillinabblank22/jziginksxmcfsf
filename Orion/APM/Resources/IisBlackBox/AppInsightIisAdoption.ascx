﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppInsightIisAdoption.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_AppInsightIisAdoption" %>

<orion:Include ID="Include1" runat="server" File="APM/Styles/Resources.css" />

<orion:resourcewrapper runat="server" id="Wrapper" showheaderbar="true" CssClass="AppInsightIisAdoption">
    <Content>
        <div class="Content">
            <div class="Lightbulb">
                <img src="/Orion/APM/Images/Admin/icon_lightbulb.gif" />
            </div>
            <div class="ContentText">
                <p><%= Resources.APM_IisBBContent.Adoption_Message %></p>
                <orion:LocalizableButton runat="server" ID="btnSetupIIS" LocalizedText="CustomText" Text="<%$ resources:APM_IisBBContent,Adoption_Button_SetupIis %>" DisplayType="Secondary" OnClick="OnSetupIisButtonClicked" />
                <orion:LocalizableButton runat="server" ID="btnRemoveResource" CssClass="Hidden" DisplayType="Secondary" OnClick="OnRemoveResourceButtonClicked" />
                <div class="Right"><%= HelpLink("SAMAGAppInsIISTop", "» Learn more") %></div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </Content>
</orion:resourcewrapper>
