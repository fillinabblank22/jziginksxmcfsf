﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Common.Models;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_IisBlackBox_SiteManagement : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ISiteProvider) };

    public override string HelpLinkFragment => "SAMAGAppInforIISSiteMgmtRes";

    protected override string DefaultTitle => APM_IisBBContent.Web_SiteManagement_Resource_Title;

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public Site ManagedSite => GetInterfaceInstance<ISiteProvider>().IisSite;

    public bool HideManagementControls => ApmRoleAccessor.AllowIisActionRights == false || ManagedSite.ApmStatus.Value == Status.Unmanaged;

    protected void Page_Init(object sender, EventArgs e)
    {
        this.Visible = ApmRoleAccessor.AllowAdmin || ApmRoleAccessor.AllowIisActionRights;        
    }
}