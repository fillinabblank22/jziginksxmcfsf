﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteDetails.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_SiteDetails" %>
<%@ Register TagPrefix="apm" TagName="componenterrorstatuscontrol" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.IIS.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>

<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server" />
        <orion:Include ID="I3" File="APM/APM.js" runat="server" />
        <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="I6" File="APM/js/NodeManagement.js" runat="server" />
        <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />
        <orion:Include ID="I7" File="APM/IisBlackBox/Styles/Resources.css" runat="server" />
        <script type="text/javascript">
            $().ready(function () {
                SW.APM.TrimStatusDetails();
            });
        </script>

        <table class="biggerPadding noWrap NeedsZebraStripes" cellspacing="0">
            <tr>
                <td class="PropertyHeader siteDetailsHeader">
                    <%= APM_IisBBContent.SiteDetails_SiteName %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                 <img  src="/Orion/StatusIcon.ashx?entity=Orion.APM.IIS.Site&size=small&status=<%=Site.SiteModel.Status.Value%>"   /> &nbsp; <%= Site.Name%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_IisBBContent.SiteDetails_SiteState %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <label<%= Site.SiteModel.State == EntityState.Stopped ? "class=\"redAndBold\"" : string.Empty %>><%= Site.SiteModel.State %></label>
                    <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>
                </td>
            </tr>
            <% if (ShowError())
               { %>
            <tr>
                <td colspan="3">
                    <%= GetError().GetStatusDetailsInline()%>
                </td>
            </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader"><%= APM_IisBBContent.AppPoolDetails_StartAutomatically %></td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <%= AutoStart%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader siteDetailsAuth">
                    <%= APM_IisBBContent.SiteDetails_URL %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <label><%= SiteUrl %></label>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_IisBBContent.SiteDetails_ApplicationPool %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <%if (Site.SiteModel.ApplicationPoolId != null)
                      { %>
                        <a <%=this.ApplicationPoolViewLink %>>
                        <img  src="/Orion/StatusIcon.ashx?entity=Orion.APM.IIS.ApplicationPool&size=small&status=<%=SiteAppPoolStatus%>"   />
                        &nbsp;
                        <%= SiteAppPoolName %></a>
                   <% }
                      else
                      { %>
                        <%= SiteAppPoolName %>
                   <% }%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_IisBBContent.SiteDetails_PhysicalPath %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <label><%= SitePhysicalPath %></label>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_IisBBContent.SiteDetails_LogFilePath %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <% if (Site.SiteModel.LoggingEnabled.HasValue && Site.SiteModel.LoggingEnabled.Value)
                       { %>
                    <label><%= Site.SiteModel.Directory %></label>
                    <% }
                      else
                      { %>
                    <label style="font-style: italic"><%= APM_IisBBContent.SiteDetails_LoggingDisabled %></label>
                    <% }%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_IisBBContent.SiteDetails_Limits %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText"></td>
            </tr>
            <tr>
                <td class="PropertyHeader siteDetailsPadding">
                    <%= APM_IisBBContent.SiteDetails_LimitBandwidthUsage %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <label><%= GetValue(Site.SiteModel.MaxBandwidth, uint.MinValue, uint.MaxValue, APMWebContent.APMWEBDATA_VB1_344) %></label>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader siteDetailsPadding">
                    <%= APM_IisBBContent.SiteDetails_ConnectionTimeout %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <label><%= GetValue(Site.SiteModel.ConnectionTimeoutSec, uint.MinValue, uint.MaxValue, APMWebContent.APMWEBDATA_VB1_95) %></label>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader siteDetailsPadding">
                    <%= APM_IisBBContent.SiteDetails_LimitNumberOfConnections %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <label><%= GetValue(Site.SiteModel.MaxConnections, uint.MinValue, uint.MaxValue) %></label>
                </td>
            </tr>
             <% if (this.componentErrorStatusControl.Monitors != null && this.componentErrorStatusControl.Monitors.Count != 0)
                { %>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_IisBBContent.SiteDetails_CurrentProblems %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText"></td>
            </tr>
            <tr>
                <apm:componenterrorstatuscontrol
                    ID="componentErrorStatusControl"
                    runat="server"
                    OnInit="ComponentErrorStatusControlInit"></apm:componenterrorstatuscontrol>
            </tr>
            <% } %>
        </table>
    </Content>
</orion:resourceWrapper>
