﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDetails.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_ApplicationDetails" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Register TagPrefix="apm" TagName="ComponentErrorStatusControl" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>
 
<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server" />
        <orion:Include ID="I3" File="APM/APM.js" runat="server" />
        <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="I6" File="APM/js/NodeManagement.js" runat="server" />
        <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />
        <orion:Include ID="I7" File="APM/IisBlackBox/Styles/Resources.css" runat="server" />
        <script type="text/javascript">
            $().ready(function () {
                SW.APM.TrimStatusDetails();
            });
        </script>

        <table cellspacing="0" class="biggerPadding NeedsZebraStripes">
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap;">
                    <%= APM_IisBBContent.AppDetails_ApplicationName %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server"
                        CustomApplicationType="<%# App.CustomType %>" StatusValue="<%# App.Status %>"/>
                    <a href="<%= GetViewLink(App.NetObjectID) %>"><%= App.Name %></a>
                    on
                    <orion:SmallNodeStatus ID="ServerStatusIcon" runat="server"
                        StatusValue="<%# App.NPMNode.Status %>" />
                    <a href="<%= GetViewLink(App.NPMNode.NetObjectID) %>"><%= App.NodeName %></a>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap;">
                    <%= APM_IisBBContent.ApplicationDetails_LastSuccessfulPoll %>
                </td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%= App.LastSuccessfulPoll.ToString("f", System.Globalization.CultureInfo.CurrentCulture) %>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap;">
                    <%= APM_IisBBContent.AppDetails_Status %>
               </td>
                <td>&nbsp;</td>
                <td class="Property">
                    <span <%= App.Status.Value == Status.Critical?"class=\"boldAndRed\"" : App.Status.Value == Status.Warning?"class=\"red\"":string.Empty %>><%= App.Status %></span>
                    <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>
                </td>
            </tr>
            <% if (ShowApplicationError) { %>
            <tr>
                <td colspan="3">
                    <%= ApplicationError.GetStatusDetailsInline()%>
                </td>
            </tr>
            <% } %>
             <tr>
                <td class="PropertyHeader" style="white-space: nowrap;">
                    <%= APM_IisBBContent.AppDetails_IisVersion %>
                </td>
                <td>&nbsp;</td>
                <td class="Property">   
                    <% if (string.IsNullOrEmpty(App.IisVersion)){ %> 
                          <label class="italicText"> N/A</label>
                    <% } else {%>
                           <label > <%= App.IisVersion %></label>                     
                    <% }%>
                </td>
            </tr>
        </table>
            <apm:ComponentErrorStatusControl
                ID="componentErrorStatusControl" 
                runat="server" 
                OnInit="componentErrorStatusControl_Init">
            </apm:ComponentErrorStatusControl>
    </Content>
</orion:resourceWrapper>
