﻿using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Models;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]

public partial class Orion_APM_Resources_IisBlackBox_ApplicationDetails : ApmBaseResource
{
    private readonly IisApplicationComponentErrors componentErrors = new IisApplicationComponentErrors();

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IIisApplicationProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInforIISAppDetRes"; }
    }

    protected override string DefaultTitle
    {
        get { return APMWebContent.APMWEBCODE_VB1_124; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public IisApplication App
    {
        get { return GetInterfaceInstance<IIisApplicationProvider>().IisApplication; }
    }

    protected bool ShowApplicationError
    {
        get { return ApplicationError.Status != Status.Available; }
    }

    protected bool InitialPollInProgress
    {
        get { return ApplicationError.Status == Status.Undefined && (int)ApplicationError.ErrorCode == int.MinValue; }
    }

    private ApmMonitorError applicationError;

    protected ApmMonitorError ApplicationError
    {
        get
        {
            if (applicationError == null)
                applicationError = componentErrors.ToApmError(App.Id);
            
            return applicationError;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        AppStatusIcon.DataBind();
        ServerStatusIcon.DataBind();

        alertSuppressionStatus.EntityUri = App.SwisUri;
        alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;
    }

    protected void componentErrorStatusControl_Init(object sender, EventArgs e)
    {
        List<ComponentErrorStatus> components = componentErrors.LoadIISComponentsWithError(App.Id);
        componentErrorStatusControl.Initialize(components);
    }
}