﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteFileSize.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_SiteFileSize" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/IisBlackBox/Js/Resources.js"/>

        <apm:CustomQueryTable runat="server" ID="CustomTable"/>

        <script type="text/javascript">
            $(function () {

                var resourceId = <%= Resource.ID %>;

                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(resourceId);
                }

                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: resourceId,
                    cls: 'siteFileGrid',
                    allowSort: true,
                    showAllLimit: 50,
                    underscoreTemplateId: APMjs.format('#contentTemplate-{0}', resourceId),
                    explicitZebra: true,
                    headerTitles: [
                        '<%= Resources.APM_IisBBContent.Web_FolderPath_Header%>',
                        '<%= Resources.APM_IisBBContent.Web_NumberOfFiles_Header%>',
                        '<%= Resources.APM_IisBBContent.Web_TotalFileSize_Header%>',
                        '<%= Resources.APM_IisBBContent.Web_VolumeUsage_Header%>'
                    ],
                    customHeaderGenerator: function(columns) {
                        return $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0],
                                columns[1],
                                columns[2],
                                columns[3].attr('colspan', 2)
                            );
                    }
                });

                SW.Core.View.AddOnRefresh(refresh, "<%= CustomTable.ClientID %>");
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var IisBbRes = SW.APM.IisBB.Resources,
                    path = Columns[0],
                    count = Columns[1],
                    size = Columns[2],
                    volumeLink = Columns[3],
                    volumeUsage = Columns[4],
                    volumeStatus = Columns[5],
                    countStatus = Columns[6],
                    sizeStatus = Columns[7],
                    volumeUsageBar = '',
                    bar, countStyle, sizeStyle;

                size = SW.Core.Charts.dataFormatters.byte(size, 0);

                volumeUsage = (volumeUsage == 0)
                    ? '0.0 %'
                    : SW.Core.Charts.dataFormatters.percent(volumeUsage, undefined, 1);

                bar = new SW.APM.Controls.PercentStatusBar();
                bar.BuildByStatus = true;
                bar.Status = volumeStatus;
                bar.UsedSpacePercentage = volumeUsage;

                volumeUsageBar = IisBbRes.getVolumeLink(volumeLink, bar.BuildPercentStatusBar())

                countStyle = IisBbRes.getTextStatusCss(countStatus);
                sizeStyle = IisBbRes.getTextStatusCss(sizeStatus);
            #}
            <tbody class="zebraRow">
                <tr class="topRow">
                    <td colspan="5" style="border-bottom: 1px dashed #E0E0E0 !important;">
                        <a href="<%= CustomChartUrl %>">{{ path }}</a>
                    </td>
                </tr>
                <tr class="bottomRow">
                    <td>&nbsp;</td>
                    <td class="{{ countStyle }}">{{ count }} </td>
                    <td class="{{ sizeStyle }}">{{ size }}</td>
                    <td style="text-align: center;">{{ volumeUsage }}</td>
                    <td> {{ volumeUsageBar }}</td>
                </tr>
            </tbody>
        </script>
    </Content>
</orion:resourceWrapper>
