﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationPoolDetails.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_ApplicationPoolDetails" %>
<%@ Register TagPrefix="apm" TagName="componenterrorstatuscontrol" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>

<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.IIS.Common.Models" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server" />
        <orion:Include ID="I3" File="APM/APM.js" runat="server" />
        <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="I6" File="APM/js/NodeManagement.js" runat="server" />
        <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />
        <orion:Include ID="I7" File="APM/IisBlackBox/Styles/Resources.css" runat="server" />
        <script type="text/javascript">
            $().ready(function () {
                SW.APM.TrimStatusDetails();
            });
        </script>

        <table cellspacing="0" class="biggerPadding NeedsZebraStripes">
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px; width: 25%;">
                    <%= APM_IisBBContent.AppPoolDetails_ApplicationPoolName%>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <img src="<%=StatusUrl %>" alt="Status"/>
                    <%= ApplicationPool.Name %>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px;"><%= APM_IisBBContent.AppPoolDetails_ApplicationPoolState %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <span<%= ApplicationPool.State == EntityState.Stopped ? " class=\"redAndBold\"" : string.Empty %>><%= ApplicationPool.State.ToString() %></span>
                    <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>
                </td>
            </tr>
            <% if (ShowError()) 
               { %>
            <tr>
                <td colspan="3">
                    <%= GetError().GetStatusDetailsInline()%>
                </td>
            </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px;"><%= APM_IisBBContent.AppPoolDetails_WorkerProcessesStatus %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <label id="WorkerProcessComponentStatus" runat="server"></label>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px"><%= APM_IisBBContent.AppPoolDetails_StartAutomatically %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%= AutoStart%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px"><%= APM_IisBBContent.AppPoolDetails_StartMode %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%= StartMode%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px"><%= APM_IisBBContent.AppPoolDetails_NetVersion %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%= ManagedRuntimeVersion%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px"><%= APM_IisBBContent.AppPoolDetails_ManagedPipelineMode %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%= ManagedPipelineMode%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px"><%= APM_IisBBContent.AppPoolDetails_Identity %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%= Identity%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; padding: 5px"><%= APM_IisBBContent.AppPoolDetails_Applications %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%= ApplicationsCount%>
                </td>
            </tr>
            <% if (this.componentErrorStatusControl.Monitors != null && this.componentErrorStatusControl.Monitors.Count != 0)
               { %>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_IisBBContent.SiteDetails_CurrentProblems %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText"></td>
            </tr>
            <tr>
                <apm:componenterrorstatuscontrol
                    ID="componentErrorStatusControl"
                    runat="server"
                    OnInit="ComponentErrorStatusControlInit"></apm:componenterrorstatuscontrol>
            </tr>
            <% } %>
        </table>
    </Content>
</orion:resourceWrapper>