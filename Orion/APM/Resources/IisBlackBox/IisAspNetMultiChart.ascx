﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IisAspNetMultiChart.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_IisAspNetMultiChart" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.Plugins" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>

<orion:Include ID="Include1" runat="server" Module="APM" File="APM.css"/>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <style>
            .mcHeader {
                overflow-y: visible !important;
            }
        </style>     
        <div class="mcContainer" style="height: <%= Height %>px;">
            <asp:Panel ID="pnlLeft" runat="server" CssClass="mcLeftColumn mcHeader">
                <span class="title">
                    <%= Resources.APMWebContent.Chart_Statistic_Name %>
                </span>
            </asp:Panel>
            <asp:Panel ID="pnlMiddle" runat="server" CssClass="mcMiddleColumn mcHeader">
                <!-- This element is used to access ResourceWrapper element for this resource so that 
                we can add extra CSS class to it to fix chart tooltips clipping. (FB109023) -->
                <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
            </asp:Panel>
            <asp:Panel ID="pnlRight" runat="server" CssClass="mcRightColumn mcHeader">
                <span class="title">
                    <%= Resources.APMWebContent.Chart_Value_From_Last_Poll %>
                </span>
            </asp:Panel>
            <asp:Repeater ID="rptMonitors" runat="server">
                <ItemTemplate>
                    <div class="mcCounter" style="height: <%= CounterDivHeight %>px;">
                        <table>
                            <tr>
                                <td class="icon" style="width: <%= IconColumnWidth %>px">
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# WebPluginManager.Instance.GetSmallIconUrl(ViewType.Monitor, GetInterfaceInstance<IApplicationProviderBase>().ApmApplication.CustomType, (int)((ApmStatus)Eval("Status")).Value) %>'
                                               ToolTip='<%# ((ApmStatus)Eval("Status")).ToString() %>' />
                                </td>
                                <td class="name" style="width: <%= NameColumnWidth %>px">
                                    <a href="<%# GetViewLink((long)Eval("Id")) %>"> <%# AddLineBreaksToString(Eval("Name").ToString()) %> </a>                                    
                                </td>
                                <td class="chart"></td>
                                <td class="lastValue" style="width: <%= LastValueColumnWidth %>px">
                                    <span class="lastValue"></span>
                                </td>
                            </tr>    
                        </table>  
                        <hr />              
                    </div>                      
                </ItemTemplate>  
            </asp:Repeater>
        </div>         
    </Content>
</orion:resourceWrapper> 