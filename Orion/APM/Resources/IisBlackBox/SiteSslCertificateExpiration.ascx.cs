﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_SiteSslCertificateExpiration : SolarWinds.APM.Web.ApmBaseResource
{
	private DataTable dataSource;
	private Threshold threshold;

	protected override string DefaultTitle => APM_IisBBContent.Web_SSLCertificateExpiration_Resource_Title;

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override string HelpLinkFragment => "SAMAGAppInforIISCertSSLExpRes";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ISiteProvider) };

    public Site Site => GetInterfaceInstance<ISiteProvider>().IisSite;

    private DataTable DataSource
	{
		get
		{
			if (dataSource == null)
			{
				////TEMPORARY (FAKE DATA FOR QA)
				//var now = DateTime.UtcNow;

				//dataSource = new DataTable();
				//dataSource.Columns.Add("Port");
				//dataSource.Columns.Add("SslSubject");
				//dataSource.Columns.Add("SslValidDate");
				//dataSource.Columns.Add("SslExpiryDate");

				//dataSource.Rows.Add(new object[] { 1, "Ssl Subject 1", now, now });
				//dataSource.Rows.Add(new object[] { 2, "Ssl Subject 2", now.AddYears(-2), now.AddYears(-1) });
				//dataSource.Rows.Add(new object[] { 3, "Ssl Subject 3", now.AddYears(1), now.AddYears(2) });
				//dataSource.Rows.Add(new object[] { 4, "Ssl Subject 4", now.AddYears(-1), now.AddDays(10) });
				//dataSource.Rows.Add(new object[] { 5, "Ssl Subject 5", now.AddYears(-1), now.AddMonths(6) });
				//dataSource.Rows.Add(new object[] { 6, "Ssl Subject 6", now.AddYears(-1), now.AddYears(15) });

				//dataSource.AcceptChanges();
				//return dataSource;

				const string Query = @"
SELECT DISTINCT
	sb.Port, 
	sb.SslSubject,
	sb.SslValidDate,
	sb.SslExpiryDate,
    sb.SslDaysRemaining
FROM Orion.APM.IIS.SiteBinding sb 
WHERE sb.Protocol = 'https' AND sb.SslSubject IS NOT NULL AND sb.SiteID = @SiteID
ORDER BY sb.Port";
				using (var swis = InformationServiceProxy.CreateV3())
				{
					dataSource = swis.Query(Query, new Dictionary<string, object> { { "SiteID", Site.SiteModel.Id } });
				}
			}
			return dataSource;
		}
	}

	private Threshold Threshold
	{
		get
		{
			if (threshold == null)
			{
				var component = Site.IisApplication.Components
					.FirstOrDefault(item => item.ShortName == "SSL Certificate Monitor" && item.ApplicationItemID == Site.SiteModel.Id);
				if (component != null)
				{
					const string Query = @"
SELECT 
    ThresholdName AS Name, 
    Warning AS WarnLevel, 
    Critical AS CriticalLevel, 
    ThresholdOperator
FROM Orion.APM.ThresholdsByComponent
WHERE ComponentId = @ComponentId";
					var data = default(DataTable);
					using (var swis = InformationServiceProxy.CreateV3())
					{
						data = swis.Query(Query, new Dictionary<string, object> { { "ComponentId", component.Id } });
					}

					if (data.Rows.Count > 0)
					{
						threshold = new SolarWinds.APM.Common.Helpers.AdHocMapper()
							.ToObject<Threshold>(data.Rows[0], true, true);
					}
				}
				threshold = threshold ?? new Threshold();
			}
			return threshold;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Wrapper.Visible = DataSource.Rows.Count > 0)
		{
			ctrCerts.DataSource = DataSource;
			ctrCerts.DataBind();
		}
	}

	protected void OnCerts_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
		{
			return;
		}

		var row = e.Item.DataItem as DataRowView;

		var fromDate = DateTime.UtcNow;
		var toDate = DateTime.UtcNow;
		if (IsExpaired(row["SslValidDate"], row["SslExpiryDate"], ref fromDate, ref toDate))
		{
			ShowContent(e.Item, "ctrAlreadyNotValid", "expired");
			return;
		}
		if (fromDate > DateTime.UtcNow)
		{
			ShowContent(e.Item, "ctrYetNotValid", "yet-not-valid", fromDate, toDate);
			return;
		}
		ShowContent(e.Item, "ctrDaysRemaining", GetStatus(fromDate, toDate).ToString().ToLowerInvariant(), fromDate, toDate);
	}

	protected bool IsExpaired(object rawFromDate, object rawToDate, ref DateTime fromDate, ref DateTime toDate)
	{
		if (ParseDate(rawFromDate, ref fromDate) && ParseDate(rawToDate, ref toDate))
		{
			var nowDate = DateTime.UtcNow;
			if (nowDate > toDate)
			{
				return true;
			}
			return (toDate - fromDate).TotalDays < 0;
		}
		return true;
	}

	private Status GetStatus(DateTime fromDate, DateTime toDate)
	{
		return Threshold.CalculateStatus(GetDaysRemaining(fromDate, toDate));
	}

	private int GetDaysRemainingPercent(DateTime fromDate, DateTime toDate)
	{
		var nowDate = DateTime.UtcNow;
		if (fromDate > nowDate)
		{
			return 100;
		}
		if (nowDate > toDate)
		{
			return 0;
		}
		var totalDays = Math.Truncate(Math.Min((toDate - fromDate).TotalDays, 365));
		var usedDays = Math.Truncate(Math.Min((toDate - nowDate).TotalDays, 365));

		return Convert.ToInt32(usedDays / totalDays * 100);
	}

	protected double GetDaysRemaining(DateTime fromDate, DateTime toDate)
	{
		var nowDate = DateTime.UtcNow;
		if (nowDate > fromDate) 
		{
			fromDate = nowDate;
		}
		return Math.Truncate(Math.Max((toDate - fromDate).TotalDays, 0));
	}

	protected object FormatString(object rawString)
	{
		if (rawString == null || rawString.ToString().IsNotValid())
		{
			return Resources.APMWebContent.APMWEBCODE_AK1_23;
		}
		return rawString.ToString();
	}

	protected object FormatDate(object rawDate)
	{
		var date = DateTime.UtcNow;
		if (ParseDate(rawDate, ref date))
		{
			return new ApmDateTime(date).ToString("d", CultureInfo.CurrentUICulture);
		}
		return Resources.APMWebContent.APMWEBCODE_AK1_23;
	}

	private bool ParseDate(object rawDate, ref DateTime date)
	{
		return rawDate != null && DateTime.TryParse(rawDate.ToString(), out date);
	}

	private void ShowContent(RepeaterItem item, string controlId, string status, DateTime fromDate = default(DateTime), DateTime toDate = default(DateTime))
	{
		item.FindControl(controlId).Visible = true;
		foreach (var control in item.Controls)
		{
			var tr = control as HtmlTableRow;
			if (tr != null)
			{
				tr.Attributes["class"] = status;
			}
		}
		if (status != "expired")
		{
			var ctr = item.FindControl("ctrStatusBar") as ASP.ApmStatusBar;
			ctr.BarWidth = 40;
			ctr.BarStatus = GetStatus(fromDate, toDate);
			ctr.BarValueWidth = GetDaysRemainingPercent(fromDate, toDate);
		}
	}
}