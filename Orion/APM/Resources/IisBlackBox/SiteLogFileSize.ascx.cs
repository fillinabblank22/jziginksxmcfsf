﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualBasic;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_SiteLogFileSize : ApmBaseResource
{
    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ISiteProvider) };

    public override string HelpLinkFragment => "samagappinforiislogsizebyfileres";

    protected override string DefaultTitle => APM_IisBBContent.Web_LogSizByFile_Resource_Title;

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public Site WebSite => GetInterfaceInstance<ISiteProvider>().IisSite;

    public string CustomChartUrl => CustomChartUriBuilder.Create("BBIIS_SiteLogDirSize", Constants.SitePrefix, WebSite.SiteModel.Id.ToString(), DefaultTitle, false);

    private const string SwqlQueryTemplate = @"
SELECT
    f.Directory,
    f.FilesCount,
    f.FilesSize,
    f.VolumeID as [_volumeId],
    '{1}:' + ToString(v.VolumeId) as [_volumeLink],
    CASE WHEN (v.Size = 0) THEN 0 ELSE Round(100 * f.FilesSize / v.Size, 1) END as [VolumeUsage],
    v.Status as [_status],
    f.[FilesCountStatus] as [_fileCountStatus],
    f.[FilesSizeStatus] as [_fileSizeStatus]
FROM Orion.APM.IIS.SiteLogDirectory as f
LEFT JOIN Orion.Volumes v
    ON v.VolumeID = f.VolumeID
WHERE f.SiteID = {0} AND f.Directory IS NOT NULL";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (WebSite.SiteModel.LoggingEnabled.HasValue && !WebSite.SiteModel.LoggingEnabled.Value)
        {
            wrapper.Visible = false;
            return;
        }

        var query = InvariantString.Format(SwqlQueryTemplate, WebSite.SiteModel.Id, GetViewLink("V"));
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            using (var result = proxy.Query(query))
            {
                wrapper.Visible = result.Rows.Count > 0;
                if (wrapper.Visible)
                {
                    var filesCount = result.Rows[0].Field<int>(1);
                    wrapper.Visible = filesCount > 0;
                }
            }
        }

        if (wrapper.Visible)
        {
            CustomTable.UniqueClientID = Resource.ID;
            CustomTable.SWQL = query;
        }
    }
}
