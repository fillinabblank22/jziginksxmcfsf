﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_SiteDirectorySizeChart : APMChartBaseResource, IResourceIsInternal
{
	private string netObjectID;

	protected override string DefaultTitle => APM_IisBBContent.Web_Site_Directory_SizeResource_Title;

    public string NetObjectID
	{
		get
		{

			if (netObjectID == null)
			{
				var provider = GetProvider();
				if (provider == null)
				{
					netObjectID = GetNetObjectIDFromCustomChart();
				}
				else
				{
					netObjectID = provider.IisSite.NetObjectID;
				}
			}
			return netObjectID;
		}
	}

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	protected override string NetObjectPrefix
	{
		get { return Constants.SitePrefix; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(ISiteProvider) }; }
	}

	public bool IsInternal
	{
		get { return true; }
	}

	private ISiteProvider GetProvider()
	{
		return GetInterfaceInstance<ISiteProvider>();
	}
	
	private string GetNetObjectIDFromCustomChart()
	{
		return NetObjectHelper.GetNetObjectId(Resource.Properties["netobjectprefix"], int.Parse(Resource.Properties["elementlist"]));
	}

	protected override IEnumerable<string> GetElementIdsForChart()
	{
		var elementsList = new string[0];
		if (NetObjectID != null) 
		{
			elementsList = new[] { NetObjectID };
		}
		if (!elementsList.Any()) 
		{
			ForceDisplayOfNeedsEditPlaceholder = true;
		}
		return elementsList;
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		if (!Resource.Properties.ContainsKey("ChartName")) 
		{
			Resource.Properties.Add("ChartName", "BBIIS_SiteDirSize");
		}

		try
		{
			HandleInit(WrapperContents);
		}
		catch { }

		ApmBaseResource.EnsureApmScripts();
        EnsureDateFormatScripts();
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "IisBlackBox/js/Charts.js", OrionInclude.Section.Middle);
	}
}