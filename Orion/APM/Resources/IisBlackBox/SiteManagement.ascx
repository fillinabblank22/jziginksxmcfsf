﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteManagement.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_SiteManagement" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>

<orion:Include ID="Include3" runat="server" File="APM/Styles/Resources.css" />
<orion:Include ID="i2" File="APM/IisBlackBox/Js/AppItemManagement.js" Section="Bottom" runat="server" />
<orion:Include ID="Include2" runat="server" File="APM/IisBlackBox/Styles/IisManagement.css" />

<script type="text/javascript">
function manageApplicationItem(managed, message) {

    var isDemo = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? 1 : 0 %>;
    if (isDemo)
    {
        DemoModeAction("SiteManagement_Manage");
        return;
    }

    var siteName = '<%= ManagedSite.Name%>';
    var message = managed ? '<%= APM_IisBBContent.SiteManagement_SiteRemanageDialog %>' : '<%= APM_IisBBContent.SiteManagement_SiteUnmanageDialog %>';
    message = SF(message, siteName);
    Ext.Msg.show({
        msg: message,
        buttons: Ext.Msg.YESNO,
        fn: function (btn) {
            if (btn === 'yes') {
                var config = {
                    applicationCustomType: 'ABIA',
                    applicationId: <%= ManagedSite.IisApplication.Id%>,
                    itemId: <%= ManagedSite.SiteModel.Id%>,
                    managed: managed,
                    message: message,
                    onComplete: function() { window.location.reload(true); }
                };
                SW.APM.ManageAppItem.manage(config);
            }
        }
    });
		
    return false;
}
$(function () {
    var data = {
        siteName: '<%= ManagedSite.Name %>',
        nodeId: '<%= ManagedSite.IisApplication.Node.Id %>',
        siteId: '<%= ManagedSite.SiteModel.Id %>',
        resourceId: '<%= Resource.ID %>',
        applicationId: '<%= ManagedSite.IisApplication.Id %>',
        templateId: '<%= ManagedSite.IisApplication.TemplateId %>',
        show: !(<%= this.HideManagementControls ? 1 : 0 %>),
        poolState: '<%= ManagedSite.SiteModel.State %>',
        isDemo: <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? 1 : 0 %> 
    };
    new SW.APM.IisBB.SiteManagement(data);
});
</script>

<orion:resourceWrapper ID="resourceContent" runat="server">
    <Content>
        <div class="ReportHeader" style="margin-bottom: 10px;">SITE</div>
        <div>
            <span id="restart-container-<%= Resource.ID %>" class="management-button" style="display: none;">
                <a id="restart-<%= Resource.ID %>" href="javascript:void(0)">
                    <img class="NodeManagementIcons" src="/Orion/APM/IisBlackBox/Images/restart.png" alt="">
                    <%= APM_IisBBContent.SiteManagement_RestartBtn %></a>
            </span>
            <span id="stop-container-<%= Resource.ID %>" class="management-button" style="display: none;">
                <a id="stop-<%= Resource.ID %>" href="javascript:void(0)">
                    <img class="NodeManagementIcons" src="/Orion/APM/IisBlackBox/Images/stop.png" alt="">
                    <%= APM_IisBBContent.SiteManagement_StopBtn %></a>
            </span>
            <span id="start-container-<%= Resource.ID %>" class="management-button" style="display: none;">
                <a id="start-<%= Resource.ID %>" href="javascript:void(0)">
                    <img class="NodeManagementIcons" src="/Orion/APM/IisBlackBox/Images/play.png" alt="">
                    <%= APM_IisBBContent.SiteManagement_StartBtn %></a>
            </span>
<%
    if (ManagedSite.IisApplication.Status.Value != Status.Unmanaged && Profile.AllowUnmanage)
{
    if (ManagedSite.IsUnmanaged)
    {
%>
            <span id="remanage-container-<%= Resource.ID %>" class="management-button">
                <a id="remanage-<%= Resource.ID %>" onclick="return manageApplicationItem(true);" href="#">
                    <img class="NodeManagementIcons" src="/Orion/Nodes/images/icons/icon_remanage.gif" alt="">
                    <%= APM_IisBBContent.SiteManagement_SiteRemanageBtn %></a>
            </span>
<%
    }
    else
    {
%>
            <span id="unmanage-container-<%= Resource.ID %>" class="management-button">
                <a id="unmanage-<%= Resource.ID %>" onclick="return manageApplicationItem(false);" href="#">
                    <img class="NodeManagementIcons" id="manage_toggle_image" src="/Orion/Nodes/images/icons/icon_manage.gif" alt="">
                    <%= APM_IisBBContent.SiteManagement_SiteUnmanageBtn %></a>
            </span>
<%
    }
}
%>
        </div>
        <div id="inprogress-container-<%= Resource.ID %>" style="display: none; padding-top: 10px;">
            <img src="/Orion/APM/Images/loading_gen_16x16.gif" />
            <label id="message-<%= Resource.ID %>"></label>
        </div>
        <div style="padding-top: 10px;"></div>

        <span id="managementError-<%= Resource.ID %>" class="validationMsg sw-suggestion sw-suggestion-fail" style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <span>
                <label id="manage-action-error-<%= Resource.ID %>"></label>
            </span>
        </span>
        <span id="managementSucess-<%= Resource.ID %>" class="validationMsg sw-suggestion sw-suggestion-pass " style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <span class="">
                <label id="manage-action-success-<%= Resource.ID %>"></label>
            </span>
        </span>
        <div style="display: none;">
            <div id="iisConfirmDialog-<%= Resource.ID %>" title="" class="dialog-border">
                <table>
                    <tr>
                        <td>
                            <img src="/Orion/images/NotificationImages/notification_warning.gif" style="padding-right: 15px" /></td>
                        <td>
                            <label id="actionMessage-<%= Resource.ID %>"></label>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <div align="right">
                    <orion:LocalizableButton ID="btnYes" Text="Yes" DisplayType="Primary" runat="server" CssClass="btn-yes" />
                    &nbsp;&nbsp;
				<orion:LocalizableButton ID="btnNo" Text="no" DisplayType="Secondary" runat="server" CssClass="btn-no" />
                </div>
            </div>
        </div>
    </Content>
</orion:resourceWrapper>
