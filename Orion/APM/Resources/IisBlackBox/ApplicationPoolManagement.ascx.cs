﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Common.Models;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_IisBlackBox_ApplicationPoolManagement : ApmBaseResource
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IApplicationPoolProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsIISAppPoolMgtRes"; }
    }

    protected override string DefaultTitle
    {
		get { return APM_IisBBContent.Web_ApplicationPoolManagement_Resource_Title; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public ApplicationPool ApplicationPool
    {
        get { return GetInterfaceInstance<IApplicationPoolProvider>().ApplicationPool; }
    }

    public bool HideManagementControls
    {
        get { return ApmRoleAccessor.AllowIisActionRights == false || ApplicationPool.ApmStatus.Value == Status.Unmanaged; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.Visible = ApmRoleAccessor.AllowAdmin || ApmRoleAccessor.AllowIisActionRights;        
    }
}
