﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.AppInsight)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Iis)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_IISAverageNetworkTraffic : StandardChartResource
{
    private string netObjectID;

    protected override string DefaultTitle => APM_IisBBContent.IISAvgNetworTraffic_Resource_Title;

    public string NetObjectID
    {
        get
        {
            if (netObjectID == null)
            {
                var provider = GetProvider();
                if (provider == null)
                {
                    netObjectID = GetNetObjectIDFromCustomChart();
                }
                else
                {
                    netObjectID = provider.ApmApplication.NetObjectID;
                }
            }
            return netObjectID;
        }
    }

    protected override bool AllowCustomization
    {
        get
        {
            return Profile.AllowCustomize;
        }
    }

    protected override string NetObjectPrefix
    {
        get
        {
            return Constants.ApplicationPrefix;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IIisApplicationProvider) };
        }
    }

    private IIisApplicationProvider GetProvider()
    {
        return GetInterfaceInstance<IIisApplicationProvider>();
    }

    private string GetNetObjectIDFromCustomChart()
    {
        return NetObjectHelper.GetNetObjectId(
            Resource.Properties["netobjectprefix"],
            int.Parse(Resource.Properties["elementlist"]));
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var elementsList = new string[0];
        if (NetObjectID != null)
        {
            elementsList = new[] { NetObjectID };
        }
        if (!elementsList.Any())
        {
            ForceDisplayOfNeedsEditPlaceholder = true;
        }
        return elementsList;
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var title = string.IsNullOrEmpty(Request.QueryString["ChartTitle"])
            ? ChartResourceSettings.Title
            : Request.QueryString["ChartTitle"];
        var subtitle = string.IsNullOrEmpty(Request.QueryString["ChartSubTitle"])
            ? ChartResourceSettings.SubTitle
            : Request.QueryString["ChartSubTitle"];
        this.ChartTitle = string.IsNullOrEmpty(title) ? GetProvider().Node.Name : title;
        this.ChartSubTitle = string.IsNullOrEmpty(subtitle) ? "${ZoomRange}" : subtitle;
        var result = base.GenerateDisplayDetails();
        return result;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", "BBIIS_IISAverageNetworkTraffic");
        }

        try
        {
            HandleInit(WrapperContents);
        }
        catch (Exception ex)
        {
            new SolarWinds.Logging.Log().Error("Cannot initialize IISAverageNetworkTraficChart resource", ex);
        }

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "IisBlackBox/js/Charts.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "IisBlackBox/Styles/Charts/IisTrafficChart.css", OrionInclude.Section.Top);
    }
}