﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_SiteAvailabilityChart : ApmCustomAvailabilityChartResource, IResourceIsInternal
{
    protected void Page_Init(object sender, EventArgs e)
    {
        this.HandleInit(this.WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return this.Profile.AllowCustomize; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(SolarWinds.APM.BlackBox.IIS.Web.ISiteProvider) }; }
    }

    bool IResourceIsInternal.IsInternal
    {
        get
        {
            return true;
        }
    }
}
