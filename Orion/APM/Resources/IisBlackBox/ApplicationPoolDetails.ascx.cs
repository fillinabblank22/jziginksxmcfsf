﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.BlackBox.IIS.Web.DAL;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

using ApplicationDAL = SolarWinds.APM.Web.DAL.ApplicationDAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_ApplicationPoolDetails : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (IApplicationPoolProvider)}; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsIISAPAPDetRes"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_IisBBContent.Web_ApplicationPoolDetails_Resource_Title; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public IisApplication App
    {
        get { return GetInterfaceInstance<IApplicationPoolProvider>().IisApplication; }
    }

    public ApplicationPool ApplicationPool
    {
        get { return GetInterfaceInstance<IApplicationPoolProvider>().ApplicationPool; }
    }

    public string StatusUrl
    {
        get
        {
            return string.Format("/Orion/StatusIcon.ashx?entity={0}&size=small&status={1}",
                                 SwisEntities.IISApplicationPool, (int)ApplicationPool.ApmStatus.Value);
        }
    }

    public string AutoStart
    {
        get { return ApplicationPool.AutoStart ? "Yes" : "No"; }
    }

	public string StartMode
    {
        get
        {
			if (ApplicationPool.StartMode.HasValue) 
			{
				return EnumHelper.GetDescription(ApplicationPool.StartMode.Value);
			}
			return Resources.APM_IisBBContent.NotSupported;
		}
    }

    public string ManagedPipelineMode
    {
        get { return ApplicationPool.ManagedPipelineMode == 1 ? "Classic" : "Integrated "; }
    }

    public string Identity
    {
        get
        {
            switch (ApplicationPool.IdentityType)
            {
                case 0:
                    return "LocalSystem";
                case 1:
                    return "LocalService";
                case 2:
                    return "NetworkService";
                case 3:
                    return ApplicationPool.UserName;
                default:
                    return "ApplicationPoolIdentity";
            }
        }
    }

    public int ApplicationsCount
    {
        get
        {
            if (ApplicationPool.ApplicationsCount == null)
            {
                return 0;
            }
            return (int) ApplicationPool.ApplicationsCount;
        }
    }

    public string ManagedRuntimeVersion
    {
        get
        {
            return String.IsNullOrEmpty(ApplicationPool.ManagedRuntimeVersion) ? "N/A" : ApplicationPool.ManagedRuntimeVersion;
        }
    }

    protected bool ShowError()
    {
        return ApplicationPool.HasError;
    }

    protected ApmMonitorError GetError()
    {
        return ApplicationPool.ToError();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        alertSuppressionStatus.EntityUri = ApmApplication.SwisUri;
        alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ApplicationPool.Processes.HasData)
        {
            var app = ApplicationDAL.GetBoldApplicationContainingComponent(ApplicationPool.Processes.ComponentID, ApplicationPool.IisApplication.CustomType);
            ApplicationDAL.LoadApplicationProps(app);
            var statDetailsCmp = ComponentDal.GetLazyComponentsForApplication(app,
                        new Dictionary<string, object> { { "ComponentID", ApplicationPool.Processes.ComponentID } }, null, null, null).First();
            WorkerProcessComponentStatus.InnerText = new ApmStatus(statDetailsCmp.Status).ToLocalizedString();
        }
        else
        {
            var workerProcessComponentInfo = new ApplicationPoolDAL().GetApplicationPoolsWorkerProcessIdAndStatus(ApplicationPool.Id);
            WorkerProcessComponentStatus.InnerText = new ApmStatus(workerProcessComponentInfo.Item2).ToLocalizedString();
        }
    }

    protected void ComponentErrorStatusControlInit(object sender, EventArgs e)
    {
        if (!this.ApplicationPool.ApplicationPoolModel.Status.HasValue
             || this.ApplicationPool.ApplicationPoolModel.Status.Value == (int)Status.Unmanaged)
        {
            return;
        }
        this.componentErrorStatusControl.Initialize(
            ComponentDal.GetComponentsWithErrorByApplicationItemID(this.App.Id, this.ApplicationPool.ApplicationPoolModel.ID).ToList());
    }
}