﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_Sites : ApmBaseResource
{
    private const string SearchControlPath = @"~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx";

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IIisApplicationProvider) };

    public IisApplication App
    {
        get
        {
            var provider = GetInterfaceInstance<IIisApplicationProvider>();
            if (provider == null)
            {
                return null;
            }
            return provider.IisApplication;
        }
    }

    public override string HelpLinkFragment => "SAMAGAppInforIISSiteRes";

    protected override string DefaultTitle => APM_IisBBContent.Web_Sites_Resource_Title;

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    protected ResourceSearchControl SearchControl { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.App == null)
        {
            return;
        }

        var query = string.Format(
            @"
SELECT
    s.[Name],
    s.[IISSiteID] as [_iisSiteId],
    s.[State],
    s.[ServerAutoStart],
    s.DetailsUrl as [_siteDetailsUrl],
    '/Orion/StatusIcon.ashx?entity={0}&size=small&status=' + ToString(s.[Status]) AS [_statusIcon],
    cmp.[Connections],
    cmp.[ConnectionsStatus] as [_connectionsStatus],
    s.[AverageResponseTime],
    s.[AverageResponseTimeStatus] as [_avgResponseTimeStatus]
FROM Orion.APM.IIS.Site s
LEFT JOIN
(
    SELECT
        IsNull(cs.ComponentStatisticData,0) as [Connections],
        cs.ComponentAvailability as [ConnectionsStatus],
        c.ApplicationItemID
    FROM Orion.APM.Component c
    LEFT JOIN Orion.APM.ComponentTemplate ct
        ON ct.ID = c.TemplateID
    LEFT JOIN Orion.APM.CurrentStatistics cs
        ON c.ComponentID = cs.ComponentID
    WHERE c.ApplicationID = {1} AND ct.UniqueId='{2}'
) as cmp ON s.ItemID = cmp.ApplicationItemID
WHERE s.ApplicationID = {1}",
            SwisEntities.IISSite,
            this.App.Id,
            ComponentIdentifiers.IDSiteCurrentConnections);

        this.SearchControl = this.LoadControl(SearchControlPath) as ResourceSearchControl;
        this.Wrapper.HeaderButtons.Controls.Add(this.SearchControl);

        this.CustomTable.UniqueClientID = Resource.ID;
        this.CustomTable.SWQL = query;
        this.CustomTable.SearchSWQL = string.Format("{0} AND {1}", query, "s.Name LIKE '%${SEARCH_STRING}%'");
    }
}
