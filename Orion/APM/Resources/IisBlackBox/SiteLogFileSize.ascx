﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteLogFileSize.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_SiteLogFileSize" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/IisBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssResources" runat="server" Module="APM" File="/IisBlackBox/Styles/Resources.css"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                var resourceId = <%= Resource.ID %>;

                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(resourceId);
                }

                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: resourceId,
                    cls: 'logFileGrid',
                    allowSort: true,
                    showAllLimit: 50,
                    underscoreTemplateId: APMjs.format('#contentTemplate-{0}', resourceId),
                    explicitZebra: true,
                    headerTitles: [
                        '<%= Resources.APM_IisBBContent.Web_FolderPath_Header%>',
                        '<%= Resources.APM_IisBBContent.Web_NumberOfFiles_Header%>',
                        '<%= Resources.APM_IisBBContent.Web_TotalFileSize_Header%>',
                        '<%= Resources.APM_IisBBContent.Web_VolumeUsage_Header%>'
                    ],
                    customHeaderGenerator: function (columns) {
                        return $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0],
                                columns[1],
                                columns[2],
                                columns[3].attr('colspan', '2')
                            );
                    },
                    defaultOrderBy: '[FilesSize]'
                });

                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var IisBbRes = SW.APM.IisBB.Resources,
                    folderPath, filesCount, filesSize, volumeUsage,
                    filesCountStatus, filesSizeStatus, volumeUsageBar;

                folderPath = Columns[0];
                filesCount = Columns[1] || 0;
                filesSize = SW.Core.Charts.dataFormatters.byte(Columns[2], 0);

                volumeUsageBar = IisBbRes.getVolumeLink(Columns[4], IisBbRes.getVolumeStatusBar(Columns[5], Columns[6]));
                volumeUsage = SW.Core.Charts.dataFormatters.percent(Columns[5], undefined, 1);

                filesCountStatus = IisBbRes.getTextStatusCss(Columns[7]);
                filesSizeStatus = IisBbRes.getTextStatusCss(Columns[8]);
            #}
            <tbody class="zebraRow">
                <tr class="topRow">
                    <td colspan='7' style="border-bottom: 1px dashed #E0E0E0 !important;"><a href="<%= CustomChartUrl %>">{{ folderPath }}</a></td>
                </tr>
                <tr class="bottomRow">
                    <td>&nbsp;</td>
                    <td class="{{ filesCountStatus }}">{{ filesCount }} </td>
                    <td class="{{ filesSizeStatus }}">{{ filesSize }}</td>
                    <td>{{ volumeUsage }}</td>
                    <td>{{ volumeUsageBar }}</td>
                </tr>
            </tbody>
        </script>
    </Content>
</orion:resourceWrapper>
