﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationPools.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_ApplicationPools" %>

<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:Include ID="JSResources" runat="server" Module="APM" File="/IisBlackBox/Js/Resources.js"/>
<orion:Include ID="CssResources" runat="server" Module="APM" File="/IisBlackBox/Styles/Resources.css"/>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <apm:CustomQueryTable ID="CustomTable" runat="server" />
        <orion:Include ID="JSFormatters" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js" runat="server" />

        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>);
                }

                var config = {
                    uniqueId: '<%= Resource.ID %>',
                    cls: 'appPoolGrid NeedsZebraStripes',
                    initialPage: 0, rowsPerPage: "10",
                    allowSort: true,
                    allowSearch: true,
                    showAllLimit: 50,
                    searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                    searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                    headerTitles: [
                        '<%= Resources.APM_IisBBContent.Site_ColumnName %>', 
                        '<%= Resources.APM_IisBBContent.Site_ColumnState %>', 
                        '<%= Resources.APM_IisBBContent.Site_ColumnWorkerProcesses %>',
                        '<%= Resources.APM_IisBBContent.Site_ColumnCPU %>',
                        '<%= Resources.APM_IisBBContent.Site_ColumnPhysicalMemory %>',
                        '<%= Resources.APM_IisBBContent.Site_ColumnVirtualMemory %>'
                    ],
                    customHeaderGenerator: function (columns) {
                        return $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0].attr('colspan', 2),
                                columns[1],
                                columns[2],
                                columns[3],
                                columns[4],
                                columns[5]
                            );
                    },
                    underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
                    defaultOrderBy: '[Name] ASC'
                };

                SW.APM.Resources.CustomQuery.initialize(config);

                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>

        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var name, state, icon, url, count, cpu, pm, vm,
                    toPercent = SW.Core.Charts.dataFormatters.percent,
                    stateMap = {
                        0: 'Starting',
                        1: 'Started',
                        2: 'Stopping',
                        3: 'Stopped'
                    };

                function valueOrNA(value) {
                    return (value == null)
                        ? 'N/A'
                        : value;
                }

                function toPercentOrNA(value) {
                    return (value == null)
                        ? 'N/A'
                        : toPercent(value, { minData: 0, maxData: 100 }, 2);
                }

                name = Columns[0];
                state = stateMap[Number(Columns[1])] || 'Undefined';
                icon = Columns[5];
                url = Columns[8];
                count = valueOrNA(Columns[9]);
                cpu = toPercentOrNA(Columns[10]);
                pm = toPercentOrNA(Columns[11]);
                vm = toPercentOrNA(Columns[12]);
            #}
            <tr class="topRow iisAppItemRow">
                <td><a href="{{ url }}"><img src="{{ icon }}" /></a></td>
                <td><a href="{{ url }}">{{ name }}</a></td>
                <td>{{ state }}</td>
                <td>{{ count }}</td>
                <td>{{ cpu }}</td>
                <td>{{ pm }}</td>
                <td>{{ vm }}</td>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>
