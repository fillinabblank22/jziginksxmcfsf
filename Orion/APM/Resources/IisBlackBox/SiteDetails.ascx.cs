﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Models;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_SiteDetails : ApmBaseResource
{
    private const string LinkFormatString = @"<a href=""{0}"" {1}>{2}</a>";

    private const string AlertFormat = @"onclick=""alert('{0}'); return false;""";

    private const string Target = @"Target=""_blank""";

    private const string EmptyLink = "#";

    private const string HrefFormatString = @"href=""{0}""";

    private const string FormatString = @"{0} {1}";

	protected override string DefaultTitle
	{
		get { return "Site Details"; }
	}

    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override string HelpLinkFragment
	{
        get { return "SAMAGAppInforIISSiteDetailsRes"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.RenderControl; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(ISiteProvider) }; }
	}

	public Site Site
	{
		get { return GetInterfaceInstance<ISiteProvider>().IisSite; }
	}

	private Node Node
	{
		get
		{
			return GetInterfaceInstance<ISiteProvider>().ApmApplication.Node;
		}
	}

	public string SiteUrl
	{
		get
		{
			const string query = @"
SELECT Protocol, ISNULL(IpAddress, '') AS IpAddress, Port
FROM Orion.APM.IIS.SiteBinding
WHERE SiteID = @ID";
			using (var swis = InformationServiceProxy.CreateV3())
			{
				var data = swis.Query(query, new Dictionary<string, object> { { "ID", Site.SiteModel.Id } }).Select();
                this.EnsureIpAdress(data);
                var hosts = ConvertBindingsToUrls(data);
				if (hosts.Length > 0)
				{
					return string.Join("<br/>", hosts);
				}
				return Resources.APMWebContent.APMWEBCODE_AK1_23;
			}
		}
	}

    private void EnsureIpAdress(IEnumerable<DataRow> bindings)
    {
        foreach (var rec in bindings)
        {
            var ip = rec["IpAddress"].ToString();
            if (ip.IsNotValid() || ip == "*")
            {
                rec["IpAddress"] = this.Node.IpAddress;
            }
        }
    }

    private static string[] ConvertBindingsToUrls(IEnumerable<DataRow> bindings)
    {
        return bindings
            .Where(item => IsSupportedUrlProtocol((string)item["Protocol"]))
            .Select(item => string.Format("{0}://{1}:{2}", item["Protocol"], item["IpAddress"], item["Port"]))
            .Select(ConvertUrlToHtml)
            .ToArray();
    }

    private static string ConvertUrlToHtml(string url)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            // if demo mode is on - href is set to '#' and we are adding AlertFormat message on onclick event
            return string.Format(LinkFormatString, EmptyLink, string.Format(AlertFormat, Resources.APM_IisBBContent.Bindings_EvalMessage), url);
        }
        else
        {
            return string.Format(LinkFormatString, url, Target, url);
        }
    }

    private static bool IsSupportedUrlProtocol(string protocol)
    {
        var lowerCaseProtocol = protocol.ToLowerInvariant();

        if (lowerCaseProtocol == "http" || lowerCaseProtocol == "https")
        {
            return true;
        }

        return false;
    }

    public string ApplicationPoolViewLink
    {
        get
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                // returning href and onclick attributes
                return string.Format(FormatString, string.Format(HrefFormatString, EmptyLink), string.Format(AlertFormat, Resources.APM_IisBBContent.Bindings_EvalMessage));
            }
            // only href attribute is returned here
            return string.Format(HrefFormatString, GetViewLink(string.Format("{0}:{1}", Constants.ApplicationPoolPrefix, Site.SiteModel.ApplicationPoolId)));
        }
    }

    protected string SiteAppPoolName { get; set; }

	protected object SiteAppPoolStatus { get; set; }

	protected string SitePhysicalPath
	{
		get { return Site.SiteModel.PhysicalPath; }
	}

	public Status Status
	{
		get { return (Status)(Site.SiteModel.Status ?? (int)Status.Undefined); }
	}

	public IisApplication App
	{
		get { return GetInterfaceInstance<IIisApplicationProvider>().IisApplication; }
	}

    public string AutoStart
    {
        get { return Site.SiteModel.ServerAutoStart ? Resources.APM_IisBBContent.AutoStart_Yes : Resources.APM_IisBBContent.AutoStart_No; }
    }

    protected bool ShowError()
    {
        return Site.HasError;
    }

    protected ApmMonitorError GetError()
    {
        return Site.ToError();
    }

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		SiteAppPoolName = Resources.APMWebContent.APMWEBCODE_AK1_23;

		using (var swis = InformationServiceProxy.CreateV3())
		{
			const string poolQuery = @"SELECT ap.Name, ap.Status
        FROM Orion.APM.IIS.Site s
        JOIN Orion.APM.IIS.ApplicationPool ap ON ap.ItemID = s.ApplicationPoolID
        WHERE s.ItemID = @ID";

			var data = swis.Query(poolQuery, new Dictionary<string, object> { { "ID", Site.SiteModel.Id } });
			if (data.Rows.Count > 0)
			{
				SiteAppPoolName = data.Rows[0]["Name"].ToString();
				SiteAppPoolStatus = data.Rows[0]["Status"];
			}
		}

        alertSuppressionStatus.EntityUri = ApmApplication.SwisUri;
        alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;
    }

	protected void ComponentErrorStatusControlInit(object sender, EventArgs e)
	{
		var site = Site.SiteModel;
		if (!site.Status.HasValue || site.Status.Value == (int)Status.Unmanaged)
	    {
	        return;
	    }

        var errors = ComponentDal.GetComponentsWithErrorByApplicationItemID(App.Id, site.Id).ToList();
        this.componentErrorStatusControl.Initialize(errors);                     
	}	

    protected object GetValue<T>(T? rawValue, T minValue, T maxValue, string units = null)
		where T : struct, IFormattable, IComparable
	{
		if (rawValue.HasValue)
		{
			var value = rawValue.Value;
			if (minValue.CompareTo(value) <= 0) 
			{
				if (maxValue.CompareTo(value) <= 0)
				{
					return Resources.APM_IisBBContent.Unlimited;
				}
				return string.Format("{0:N0} {1}", value, units);
			}
		}
		return Resources.APMWebContent.APMWEBCODE_AK1_23;
	}
}