﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteSslCertificateExpiration.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_SiteSslCertificateExpiration" %>
<%@ Register TagPrefix="apm" TagName="StatusBar" Src="~/Orion/APM/Controls/ApmStatusBar.ascx" %>

<orion:Include ID="i0" File="APM/IisBlackBox/Styles/Resources.css" runat="server" />

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
		<asp:Repeater ID="ctrCerts" OnItemDataBound="OnCerts_ItemDataBound" runat="server">
			<HeaderTemplate>
				<table cellspacing="0" cellpadding="0" class="apm-iis-ssl-cert apm_NeedsZebraStripes">
			</HeaderTemplate>
			<ItemTemplate>
				<tbody class="zebraRow">
				<tr runat="server">
					<td colspan="4" class="PropertyHeader" style="font-size: 10pt; padding: 3px 0px 0px 3px;"><%# FormatString(Eval("SslSubject")) %></td>
				</tr>
				<tr id="ctrDaysRemaining" visible="false" runat="server">
					<td>&nbsp;&nbsp;</td>
					<td class="property-header">
						<%= Resources.APM_IisBBContent.SiteSslCertificateExpiration_DaysRemaining %>
					</td>
					<td class="days-remaining"> 
						<label>
							<%# FormatString(Eval("SslDaysRemaining"))%>
						</label>
					</td>
					<td>
						<apm:StatusBar ID="ctrStatusBar" runat="server" />
					</td>
				</tr>
				<tr id="ctrAlreadyNotValid" visible="false" runat="server">
					<td>&nbsp;&nbsp;</td>
					<td class="property-header">
						<%= Resources.APM_IisBBContent.SiteSslCertificateExpiration_DaysRemaining %>
					</td>
					<td class="days-remaining"> 
						<label>
							<%# FormatString(Eval("SslDaysRemaining"))%>
						</label>
					</td>
					<td>
						<label class="property-info">
							<%= Resources.APM_IisBBContent.SiteSslCertificateExpiration_CertificateExpired %>
						</label>
					</td>
				</tr>
				<tr id="ctrYetNotValid" visible="false" runat="server">
					<td>&nbsp;&nbsp;</td>
					<td class="property-header">
						<%= Resources.APM_IisBBContent.SiteSslCertificateExpiration_DaysRemaining %>
					</td>
					<td class="days-remaining"> 
						<label>
							<%# FormatString(Eval("SslDaysRemaining")) %>
						</label>
					</td>
					<td>
						<label class="property-info">
							<%= Resources.APM_IisBBContent.SiteSslCertificateExpiration_CertificateNotValidYet %>
						</label>
					</td>
				</tr>
				<tr id="Tr2" runat="server">
					<td>&nbsp;&nbsp;</td>
					<td class="property-header">
						<%= Resources.APM_IisBBContent.SiteSslCertificateExpiration_ValidBetween %>
					</td>
					<td colspan="2"> <%# FormatDate(Eval("SslValidDate")) %> - <%# FormatDate(Eval("SslExpiryDate")) %> </td>
				</tr>
				<tr id="Tr3" runat="server">
					<td>&nbsp;&nbsp;</td>
					<td class="property-header">
						<%= Resources.APM_IisBBContent.SiteSslCertificateExpiration_Port %>
					</td>
					<td colspan="2"> <%# FormatString(Eval("Port")) %> </td>
				</tr>
				<tr>
					<td colspan="4" style="font-size: 6px;">&nbsp;&nbsp;</td>
				</tr>
				</tbody>
			</ItemTemplate>
			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
    </Content>
</orion:resourceWrapper>
