﻿using System;
using System.Collections.Generic;
using System.Globalization;

using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_APM_Resources_IisBlackBox_TopPageRequestsByAverageServerExecutionTime : ApmBaseResource
{
    private Site site;
    private IisApplication iisApplication;

    private const string swqlWhereApplication = @"s.ApplicationID = {0}";

    private const string swqlWhereSite = @"r.SiteID = {0}";

    private const string swqlQuery = @"
SELECT TOP {0} 
    s.Name,
    r.URLStem, 
    r.AverageElapsedTime,
    r.Status,
    r.ID,
    queryResult.Warning,
    queryResult.Critical,
    s.DetailsUrl,
    sb.Protocol as [_protocol],
    s.Application.Node.IP_Address as [_nodeIPAddress],
    sb.IpAddress as [_ipAddress],
    sb.hostName as [_hostName], 
    sb.Port as [_port]
FROM Orion.APM.IIS.Site s
JOIN Orion.APM.IIS.Request r ON s.ItemID = r.SiteID
LEFT JOIN (
    SELECT 
        ce.ApplicationItemID, 
        tc.Warning, 
        tc.Critical
    FROM Orion.APM.ComponentExt ce
    JOIN Orion.APM.ThresholdsByComponent tc ON ce.ComponentID = tc.ComponentID AND tc.ThresholdName = '{1}' AND ce.ApplicationID = {3}
) queryResult ON queryResult.ApplicationItemID = r.SiteID
LEFT JOIN (
    SELECT TOP 1 
    b.SiteID,
    b.Protocol, 
    b.IpAddress, 
    b.hostName, 
    b.Port
    FROM Orion.APM.IIS.SiteBinding b
    WHERE b.Protocol Like 'http%'
) sb ON sb.SiteID = r.SiteID
WHERE {2}
ORDER BY AverageElapsedTime DESC";

    private const string checkQuery = @"
SELECT s.Name FROM Orion.APM.IIS.Site s
JOIN Orion.APM.IIS.Request r ON s.ItemID = r.SiteID
JOIN Orion.APM.IIS.SiteLogDirectory as f ON f.SiteID = s.ItemID AND f.Enabled = 1
WHERE {0}";

    protected bool IsSiteDetails
    {
        get { return site != null; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_IisBBContent.TopXXPageRequestsByAvrServerExecutionTimeDefaultTitle; }
    }

    protected int ApplicationID
    {
        get { return IsSiteDetails ? site.ApplicationId : iisApplication.Id; }
    }

    public override string DisplayTitle
    {
        get
        {
            return Title.Replace("XX", RowCountLimit.ToString(CultureInfo.InvariantCulture));
        }
    }
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IIisApplicationProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInforIISTopPageReqs"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/APM/Controls/EditTopXX.ascx";
        }
    }

    protected int RowCountLimit
    {
        get { return GetIntProperty("RowsPerPage", 20); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Initialize();

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var query = string.Format(checkQuery, IsSiteDetails
                ? string.Format(swqlWhereSite, site.SiteModel.Id)
                : string.Format(swqlWhereApplication, iisApplication.Id));

            var queryResult = swis.Query(query);

            if (queryResult.Rows.Count == 0)
            {
                Wrapper.Visible = false;
                return;
            }
        }

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = string.Format(swqlQuery, RowCountLimit, Constants.PageRequestsThresholdName, IsSiteDetails
            ? string.Format(swqlWhereSite, site.SiteModel.Id)
            : string.Format(swqlWhereApplication, iisApplication.Id), ApplicationID);
    }

    protected void Initialize()
    {
        var appProvider = GetInterfaceInstance<IIisApplicationProvider>();
        if (appProvider == null)
        {
            throw new InvalidOperationException("Cannot get provider.");
        }

        var siteProvider = GetInterfaceInstance<ISiteProvider>();
        if (siteProvider != null)
        {
            site = siteProvider.IisSite;
            if (site == null)
            {
                throw new InvalidOperationException("Cannot get site"); 
            }
        }

        if (!IsSiteDetails)
        {
            iisApplication = appProvider.IisApplication;
            if (iisApplication == null)
            {
                throw new InvalidOperationException("Cannot get iis application");
            }
        }
    }
}
