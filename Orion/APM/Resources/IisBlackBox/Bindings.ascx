﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Bindings.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_Bindings" %>
<%@Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="CssResources" runat="server" Module="APM" File="/IisBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/IisBlackBox/Js/Resources.js"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function() {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: "iisGrid",
                        initialPage: 0,
                        rowsPerPage: 10,
                        allowSort: true,
                        allowPaging: true,
                        allowSearch: false,
                        showAllLimit: 50,
                        defaultOrderBy: '[BindingInformation] ASC',
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        headerTitles: ["<%= Resources.APM_IisBBContent.Bindings_Type%>",
                            "<%= Resources.APM_IisBBContent.Bindings_BindingInformation%>"],
                        customHeaderGenerator: function(columns) {
                            var headerRow = $('<tr/>')
                                .addClass('HeaderRow');

                            columns[0].appendTo(headerRow);
                            columns[1].appendTo(headerRow);
                            columns[1].attr('colspan', '2');
                            return headerRow;
                        }
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>

         <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var protocol = Columns[0];
                var bindingInformation = Columns[1];
                var hostName  = Columns[2];
                var port = Columns[3];
                var siteIp = Columns[4];
                var nodeIp = Columns[5];
                var link = SW.APM.IisBB.Resources.getWebsiteLink(protocol, nodeIp, siteIp, hostName, port);
            #}   
            <tr>
                <td class="rowText, apm-iis-bindings-protocol"><span>{{ [protocol] }}</span></td>
                <td class="rowText, apm-iis-bindings-information"><span>{{ [bindingInformation] }}</span></td>
                <td class="rowText, apm-iis-bindings-link">
                {# if(protocol && (protocol.toLowerCase() == 'http' || protocol.toLowerCase() == 'https')){ #}
                <% if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                   { %>
                    <img src="/Orion/APM/IisBlackBox/Images/browse.png" />&nbsp;<a class="bindings_link" href="{{ [link] }}" target="_blank"><%= Resources.APM_IisBBContent.Bindings_BrowseLink %></a>
                <% }
                   else
                   { %>
                    <img src="/Orion/APM/IisBlackBox/Images/browse.png" />&nbsp;<a class="bindings_link" href="#" onclick="alert('<%= Resources.APM_IisBBContent.Bindings_EvalMessage %>'); return false;" ><%= Resources.APM_IisBBContent.Bindings_BrowseLink %></a>
                <% } %>
                {# } #}
                </td>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>
