﻿using System;
using System.Text;
using System.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_IisBlackBox_AppInsightIisAdoption : ApmAppBaseResource, IResourceIsInternal
{
    protected override void OnInit(EventArgs e)
    {
        Wrapper.ManageButtonText = Resources.APMWebContent.APMWEBDATA_AK1_191;
        Wrapper.ManageButtonTarget = this.Page.ClientScript.GetPostBackClientHyperlink(this.btnRemoveResource, String.Empty);
        Wrapper.ShowManageButton = true;
        base.OnInit(e);
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_IisBBContent.Adoption_Title; }
    }

    public override string EditURL
    {
        get { return String.Empty; }
    }

    protected static string HelpLink(string fragment, string title)
    {
        return InvariantString.Format(
            "<a href=\"{0}\" target=\"_blank\">{1}</a>",
            HelpLocator.CreateHelpUrl(fragment),
            HttpContext.Current.Server.HtmlEncode(title));
    }

    protected void OnSetupIisButtonClicked(object sender, EventArgs e)
    {
        string redirectUri;
        if (this.ApmApplication.Node.NodeSubType == NodeSubType.WMI)
        {
            var returnUri = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("/Orion/Apm/Summary.aspx"));
            redirectUri = String.Format("/Orion/Nodes/ListResources.aspx?Nodes={0}&ReturnTo={1}&SubType=WMI", this.ApmApplication.Node.Id, returnUri);
        }
        else
        {
            redirectUri = String.Format(
                "/Orion/APM/IisBlackBox/Admin/AssignWizard/AssignSmartIisApplication.aspx?Node={0}&Address={1}",
                this.ApmApplication.Node.Id,
                this.ApmApplication.Node.IpAddress);
        }
        Response.Redirect(redirectUri);
    }

    protected void OnRemoveResourceButtonClicked(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }

    public bool IsInternal
    {
        get { return true; }
    }
}