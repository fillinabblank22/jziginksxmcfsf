﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_PerfCounterExpertKnowledge : IisStatisticResourceBase
{
	private bool _dataInitialized;
	private DataRow _data;
    private Regex _linkRegex = new Regex(@"(?<Protocol>\w+):\/\/(?<Domain>[\w@][\w.:@]+)\/?[\w\.?=%&=\-@/$,()]*");

	protected override string DefaultTitle
	{
		get { return Resources.APM_IisBBContent.PerformanceCounterDetails_ExpertKnowledge; }
	}

	private DataRow Data
	{
		get
		{
			if (!_dataInitialized)
			{
				var queryFormat = @"
SELECT c.UserDescription AS ExpertKnowledge
FROM Orion.APM.Component AS c
WHERE c.ComponentID = {0}";
				var query = InvariantString.Format(queryFormat, IisStatistic.Id);
				using (var swis = InformationServiceProxy.CreateV3())
				{
					var data = swis.Query(query);
					if (data.Rows.Count > 0)
					{
						_data = data.Rows[0];
					}
				}

				_dataInitialized = true;
			}
			return _data;
		}
	}

    protected string ExpertKnowledgeText
    {
        get
        {
            var text = Data.GetValue<string>("ExpertKnowledge");
            foreach (var match in _linkRegex.Matches(text))
            {
                text = text.Replace(match.ToString(), String.Format("<a href=\"{0}\" target=\"_blank\">{0}</a>", match));
            }

            return text;
        }
    }

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		if (IisStatistic == null)
		{
			ctrStatisticPlaceHolder.Visible = false;
			ctrInvalidCmpPlaceHolder.Visible = true;
			return;
		}
	}
}