﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Sites.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_Sites" %>

<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:Include ID="JSResources" runat="server" Module="APM" File="/IisBlackBox/Js/Resources.js"/>
<orion:Include ID="CssResources" runat="server" Module="APM" File="/IisBlackBox/Styles/Resources.css"/>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <apm:CustomQueryTable ID="CustomTable" runat="server" />
        <orion:Include ID="JSFormatters" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js" runat="server" />

<script type="text/javascript">
$(function() {
    function refresh() {
        SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>);
    }
    var config = {
        uniqueId: '<%= Resource.ID %>',
        cls: 'iis-site-grid NeedsZebraStripes',
        initialPage: 0,
        rowsPerPage: 10,
        showAllLimit: 50,
        allowSort: true,
        allowSearch: true,
        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
        headerTitles: [
            "<%= Resources.APM_IisBBContent.Site_ColumnName %>", // [Name]
            "<%= Resources.APM_IisBBContent.Site_ColumnState %>", // [State]
            "<%= Resources.APM_IisBBContent.Site_ColumnConnections %>", // [Connections]
            "<%= Resources.APM_IisBBContent.Site_ColumnResponseTime %>", // [AverageResponseTime]
            "<%= Resources.APM_IisBBContent.Site_ColumnStartMode %>" // [ServerAutoStart]
        ],
        customHeaderGenerator: function(columns) {
            return $('<tr/>')
                .addClass('HeaderRow')
                .append(
                    $('<td class="ReportHeader" style="display:none;">IconSrc</td>'),
                    columns[0].attr('colspan', 2),
                    columns[1],
                    columns[2],
                    columns[3],
                    columns[4]
                );
        },
        underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
        defaultOrderBy: '[Name] ASC'
    };

    SW.APM.Resources.CustomQuery.initialize(config);
    SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
    refresh();
});
</script>

        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var name = Columns[0];
                var state = SW.APM.IisBB.Resources.getSiteStateText(Columns[2]);
            
                var start = (Columns[3] === false) 
                    ? "<%= Resources.APM_IisBBContent.Site_StartMode_Manually %>" 
                    : "<%= Resources.APM_IisBBContent.Site_StartMode_Automatic %>";

                var href = Columns[4];
                var icon = Columns[5];

                var connections = SW.APM.IisBB.Resources.getValueText(Columns[6]);
                var connectionsStyle = SW.APM.IisBB.Resources.getTextStateCss(Columns[7]);

                var response = SW.APM.IisBB.Resources.getValueText(Columns[8]);
                var responseStyle = SW.APM.IisBB.Resources.getTextStatusCss(Columns[9]);
            
                if (!isNaN (response))
                    response = SW.Core.Charts.dataFormatters.msec (response, 0, response < 1000 ? 0 : 2);
            #}
            <tr class="topRow iisAppItemRow">
                <td>
                    <a href="{{ href }}"><img src="{{ icon }}" /></a>
                </td>
                <td>
                    <a href="{{ href }}">{{ name }}</a>
                </td>
                <td>{{ state }}</td>
                <td><span class={{ connectionsStyle }}>{{ connections }}</span></td>
                <td><span class={{ responseStyle }}>{{ response }}</span></td>
                <td>{{ start }}</td>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>
