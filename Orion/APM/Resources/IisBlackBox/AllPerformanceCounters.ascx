﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllPerformanceCounters.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_AllPerformanceCounters" %>

<orion:Include Module="APM" File="BlackBox/Js/Resources.PerfCounters.js" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="contentPlaceHolder" runat="server">
            <div id="BBPerfCountersTreeDiv-<%= Resource.ID %>" ></div>
            <script type="text/javascript">
                $(function () {
                    var data = {
                        isItemSpecific: <%= this.IsApplicationItemSpecific.ToString().ToLower() %>,
                        resourceId: '<%= this.Resource.ID %>',
                        appType: '<%= this.CustomApplicationType %>',
                        appId: '<%= this.ApplicationId %>',
                        appItemId: '<%= this.ApplicationItemId %>'
                    };

                    function refresh() {
                        SW.APM.BB.Resources.PerfCounters.LoadDataForAppOrItem(data);
                    }

                    SW.Core.View.AddOnRefresh(refresh, 'BBPerfCountersTreeDiv-<%= this.Resource.ID %>');
                    refresh();
                });
            </script>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="incompatibleComponentPlaceHolder" runat="server">
            <div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%=CustomApplicationType%></div>
        </asp:PlaceHolder>
    </Content>
</orion:resourceWrapper>
