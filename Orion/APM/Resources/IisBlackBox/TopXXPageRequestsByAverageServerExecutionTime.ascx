﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXPageRequestsByAverageServerExecutionTime.ascx.cs" Inherits="Orion_APM_Resources_IisBlackBox_TopPageRequestsByAverageServerExecutionTime" %>
<%@Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/IisBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssResources" runat="server" Module="APM" File="/IisBlackBox/Styles/Resources.css"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <orion:Include ID="JSGeneral" runat="server" Module="APM" File="Resources.js"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: "iisGrid",
                        initialPage: 0,
                        rowsPerPage: 5,                        
                        allowSort: true,
                        allowPaging: true,
                        allowSearch: false,
                        maxRowsPerPage: 25,
                        showSpanLoader: true,
                        defaultOrderBy: '[AverageElapsedTime] DESC',
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        headerTitles: [
                            <% if (!IsSiteDetails)
                               { %>
                                "<%= Resources.APM_IisBBContent.TopXXPageRequestsByAvrServerExecutionTimeSite%>",
                            <% } %>
                            "<%= Resources.APM_IisBBContent.TopXXPageRequestsByAvrServerExecutionTimeURLStem%>",
                            "<%= Resources.APM_IisBBContent.TopXXPageRequestsByAvrServerExecutionTimeAvrExecutionTime%>"],
                        customHeaderGenerator: function(columns) {
                            var headerRow = $('<tr/>')
                                .addClass('HeaderRow');

                            columns[0].attr('colspan', '2');
                            columns[0].appendTo(headerRow);
                            columns[1].appendTo(headerRow);
                            <% if (!IsSiteDetails)
                               { %>
                            columns[2].appendTo(headerRow);
                            <% } %>
                            return headerRow;
                        },
                        onloaded: function(grid) {
                            $(grid).find("td[id^='Expander-<%= Resource.ID %>']").on("click", function(obj) {
                                var getText = function(elem) { return elem.innerText || elem.textContent; };
                                var url = getText($(obj.currentTarget).find(".urlStem")[0]);
                                var urlStemLink = getText($(obj.currentTarget).find(".urlStemLink")[0]);
                                var urlToDisplay = SW.APM.IisBB.Resources.urlToDisplay(url);
                                var urlAllowedWidth = SW.APM.IisBB.Resources.getUrlColumnWidth(<%= Resource.Width%>);

                                var urlSelector;
                             <% if (!IsSiteDetails) { %>
                                urlSelector = ".topXXPageReq_URL span";
                             <% } else { %>
                                urlSelector = ".topXXPageReq_URL_SiteDetails span";
                             <% } %>
                                
                                var rowIndex = obj.currentTarget.id.split('_')[1];
                                var innerTable = $("#InnerTable-<%= Resource.ID %>_" + rowIndex)[0];
                                
                                var expanderDiv = $(obj.currentTarget).find("div")[0];
                                if (expanderDiv.className == "closed") {
                                    expanderDiv.className = "opened";
                                    innerTable.className = "visible";
                                    
                                    if (innerTable.innerHTML == "") {
                                        var requestID = getText($(obj.currentTarget).find(".requestID")[0]);
                                        $.ajax({
                                            type: "POST",
                                            url: "/Orion/APM/IisBlackBox/Services/TopXXPageRequestsByAverageServerExecutionTimeService.asmx/LoadInnerTable",
                                            data: "{uniqueClientID: " + "<%= Resource.ID %>" + rowIndex + ", requestID: " + requestID + "}",
                                            dataType: "json",
                                            contentType: "application/json; charset=utf-8",
                                            async: false,
                                            cache: true,
                                            success: function(result) {
                                                $("#InnerTable-<%= Resource.ID %>_" + rowIndex).html(result.d);
                                            }
                                        });
                                    }

                                    var wrappedUrl = SW.APM.IisBB.Resources.wrapUrl(urlToDisplay, urlAllowedWidth);
                                    var openedLinkedUrl = SW.APM.IisBB.Resources.getLinkWithTitle(urlStemLink, urlToDisplay, wrappedUrl, true);
                                    $($(obj.currentTarget).closest("tr").find(urlSelector)[0]).html(openedLinkedUrl);
                                } else {
                                    var cuttedUrl = SW.APM.IisBB.Resources.cutUrl(urlToDisplay, urlAllowedWidth, 4);
                                    var closedLinkedUrl = SW.APM.IisBB.Resources.getLinkWithTitle(urlStemLink, urlToDisplay, cuttedUrl, true);
                                    $($(obj.currentTarget).closest("tr").find(urlSelector)[0]).html(closedLinkedUrl);

                                    expanderDiv.className = "closed";
                                    innerTable.className = "hidden";
                                }
                            });
                        }
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>

        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var urlCss;
                var urlAllowedWidth;
                <% if (!IsSiteDetails)
                   { %>
                    urlCss = "topXXPageReq_URL";
                    urlAllowedWidth = SW.APM.IisBB.Resources.getUrlColumnWidth(<%= Resource.Width%>);
             <% }
                   else
                   { %>
                    urlCss = "topXXPageReq_URL_SiteDetails";
                    urlAllowedWidth = SW.APM.IisBB.Resources.getUrlColumnWidthForSiteDetails(<%= Resource.Width%>);
             <% } %>
                var requestID              = Columns[4];
                var siteName               = Columns[0];
                var urlStem                = SW.APM.IisBB.Resources.urlToDisplay(Columns[1]);
                var cuttedUrl              = SW.APM.IisBB.Resources.cutUrl(urlStem, urlAllowedWidth, 4);
                var urlStemLink            = SW.APM.IisBB.Resources.getWebsiteLink(Columns[8],Columns[9],Columns[10],Columns[11],Columns[12]) + urlStem;
                var cuttedUrlWithTitle     = SW.APM.IisBB.Resources.getLinkWithTitle(urlStemLink, urlStem, cuttedUrl, true);
                var avrServerExecution     = SW.APM.IisBB.Resources.msConverter(Columns[2]);
                var avrStatus              = SW.APM.IisBB.Resources.getTextStatusCss(Columns[3]);
                var href                   = Columns[7];
            #}   
            <tr>
                <td id="Expander-<%= Resource.ID %>_{{Index}}">
                    <div class="closed"></div>
                    <div class="hidden">
                        <div class="requestID">{{requestID}}</div>
                        <div class="urlStem">{{ [urlStem] }}</div>
                        <div class="urlStemLink">{{ [urlStemLink] }}</div>
                    </div>
                </td>
            <% if (!IsSiteDetails) { %>
                <td class="topXXPageReq_SiteName"><a href="{{ href }}"><span>{{ siteName }}</span></a></td>
            <% } %>
                <td class="{{ urlCss }}"><span>{{ [cuttedUrlWithTitle] }}</span></td>
                <td class="topXXPageReq_AvgExec"><span class="{{ avrStatus }} linearGaugeAvgTimeText">{{ avrServerExecution }}</span>{{SW.APM.IisBB.Resources.getAvgExecTimePercentLinearGauge(Columns[2], Columns[3], Columns[5], Columns[6])}}</td>
            </tr>
            <tr></tr>
            <tr>
                <td id="InnerTable-<%= Resource.ID %>_{{Index}}" class="hidden" colspan="5"></td>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>
