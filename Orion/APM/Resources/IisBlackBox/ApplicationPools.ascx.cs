﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_ApplicationPools : ApmBaseResource
{
    private const string SearchControlPath = @"~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx";

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IIisApplicationProvider) }; }
    }

    public IisApplication App
    {
        get { return GetInterfaceInstance<IIisApplicationProvider>().IisApplication; }
    }

    public override string HelpLinkFragment
    {
        get { return "samagappinforiisapppoolsres"; }
    }

    protected override string DefaultTitle
    {
        get { return APM_IisBBContent.Web_ApplicationPools_Resource_Title; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        const string Query = @"
SELECT
    a.[Name],
    a.[State],
    a.[AutoStart] as [_autoStart],
    a.[ManagedPipelineMode] as [_managedPipelineMode],
    a.[ManagedRuntimeVersion] as [_managedRuntimeVersion],
    '/Orion/StatusIcon.ashx?entity={0}&size=small&status=' + ToString(a.[Status]) as [_statusIcon],
    a.[IdentityType] as [_identityType],
    a.[ApplicationsCount] as [_applicationCount],
    a.[DetailsUrl] as [_applicationPoolUrl],
    swp.NumberOfProcesses,
    swp.PercentCPU,
    swp.PercentMemory,
    swp.PercentVirtualMemory
FROM Orion.APM.IIS.ApplicationPool a
LEFT JOIN (
    SELECT
        wp.ApplicationPoolID,
        COUNT(wp.ApplicationPoolID) AS NumberOfProcesses,
        SUM(wp.PercentCPU) AS PercentCPU,
        SUM(wp.PercentMemory) AS PercentMemory,
        SUM(wp.PercentVirtualMemory) AS PercentVirtualMemory
    FROM Orion.APM.IIS.WorkerProcess wp
    GROUP BY wp.ApplicationPoolID
) swp ON swp.ApplicationPoolID = a.ItemID
WHERE a.ApplicationID = {1}";

        var query = string.Format(
            Query,
            SwisEntities.IISApplicationPool,
            App.Id);

        this.SearchControl = LoadControl(SearchControlPath) as ResourceSearchControl;
        this.Wrapper.HeaderButtons.Controls.Add(this.SearchControl);

        this.CustomTable.UniqueClientID = this.Resource.ID;
        this.CustomTable.SWQL = query;
        this.CustomTable.SearchSWQL = string.Format("{0} AND {1}", query, "a.Name LIKE '%${SEARCH_STRING}%'");
    }
}
