﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualBasic;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.AppInsight)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Iis)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_SiteFileSize : ApmBaseResource
{
	protected override string DefaultTitle => APM_IisBBContent.Web_SiteSizeByDirectory_Resource_Title;

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override string HelpLinkFragment => "samagappinforiissitesizebyfileres";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ISiteProvider) };

    public Site WebSite => GetInterfaceInstance<ISiteProvider>().IisSite;

    public string WebSiteId
	{
		get 
		{
			var siteId = 0;
			try
			{
				siteId = WebSite.SiteModel.Id;
			}
			catch { }
			return siteId.ToString();
		}
	}

	public string CustomChartUrl => CustomChartUriBuilder.Create("BBIIS_SiteDirSize", Constants.SitePrefix, WebSiteId, DefaultTitle, false);

    protected void Page_Load(object sender, EventArgs e)
	{
		var query = @"
SELECT 
     sd.PhysicalPath, 
     ISNULL(sd.FilesCount, 0) AS FilesCount,
     ISNULL(sd.FilesSize, 0) AS FilesSize,
     '{0}:' + ToString(sd.VolumeID) AS [VolumeLink],
    CASE WHEN (v.Size = 0) THEN 0 ELSE Round(100 * sd.FilesSize / v.Size, 1) END AS [VolumeUsage],
    v.Status,
    sd.FilesCountStatus,
    sd.FilesSizeStatus
FROM Orion.APM.IIS.SiteDirectory as sd
LEFT JOIN Orion.Volumes v ON v.VolumeID = sd.VolumeID 
WHERE 
	sd.SiteID = {1} AND 
	sd.PhysicalPath IS NOT NULL";

		query = InvariantString.Format(query, GetViewLink("V"), WebSiteId);
		using (var proxy = InformationServiceProxy.CreateV3())
		{
			using (var result = proxy.Query(query))
			{
                // hide this resource if there is no record or number of files wasn't counted correctly
                int filesCount = result.Rows[0].Field<int>(1);
                wrapper.Visible = result.Rows.Count > 0 && filesCount > 0;
			}
		}
		if (wrapper.Visible)
		{
			CustomTable.UniqueClientID = Resource.ID;
			CustomTable.SWQL = query;
		}
	}
}
