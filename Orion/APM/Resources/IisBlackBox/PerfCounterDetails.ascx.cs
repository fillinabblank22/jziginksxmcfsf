﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Resources;
using SolarWinds.APM.BlackBox.IIS.Common;
using SolarWinds.APM.BlackBox.IIS.Web.UI;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web.Charting;
using System.Globalization;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_IisBlackBox_PerfCounterDetails : IisStatisticResourceBase
{
	private bool _dataInitialized;
	private DataRow _data;

	protected override string DefaultTitle
	{
		get { return APM_IisBBContent.PerformanceCounterDetails_Title; }
	}

    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.RenderControl; }
	}

	private DataRow Data
	{
		get
		{
			if (!_dataInitialized)
			{
				var queryFormat = @"
SELECT 
	c.ShortName AS ComponentName,
	c.ComponentDefinition.DisplayName AS ComponentTypeDisplayName, 
	c.StatusDescription AS ComponentStatusDescription,
	cc.DisplayName AS CategoryDisplayName,
	pe.AvgStatisticData AS CurrentValue,
	cts.Value AS CounterUnits,
	tbc.Warning AS ThresholdWarningValue,
	tbc.Critical AS ThresholdCriticalValue,
	c.ApplicationItemID,
	isi.Name AS SiteName,
	iap.Name AS PoolName
FROM Orion.APM.Component AS c
LEFT JOIN Orion.APM.ComponentTemplate AS ct ON c.TemplateID = ct.ID
LEFT JOIN Orion.APM.ComponentCategory AS cc ON ct.ComponentCategoryID = cc.CategoryID
LEFT JOIN Orion.APM.PortEvidence pe ON c.CurrentStatus.ComponentStatusID = pe.ComponentStatusID
LEFT JOIN Orion.APM.ComponentTemplateSetting cts ON c.TemplateID = cts.ComponentTemplateID AND cts.Key = 'CounterUnit'
LEFT JOIN Orion.APM.ThresholdsByComponent tbc ON c.ComponentID = tbc.ComponentID
LEFT JOIN Orion.APM.IIS.Site isi ON c.ApplicationItemID = isi.ItemID
LEFT JOIN Orion.APM.IIS.ApplicationPool iap ON c.ApplicationItemID = iap.ItemID
WHERE ComponentID = {0}";

				var query = InvariantString.Format(queryFormat, IisStatistic.Id);
				using (var swis = InformationServiceProxy.CreateV3())
				{
					var data = swis.Query(query);
					if (data.Rows.Count > 0)
					{
						_data = data.Rows[0];
					}
				}

				_dataInitialized = true;
			}
			return _data;
		}
	}

	protected string CurrentValue
	{
		get
		{
			var value = Data.GetValue<double?>("CurrentValue");
		    return value.HasValue ? value.Value.ToString(CultureInfo.InvariantCulture) : APM_IisBBContent.PerformanceCounterDetails_Value_NA;
		}
	}

	protected string CurrentValueUnit
	{
		get { return MultiChartDataHelper.ChartFormatterToFormatterJS(Data.GetValue<string>("CounterUnits")); }
	}

	protected string ComponentStatusDescription
	{
		get { return Data.GetValue<string>("ComponentStatusDescription"); }
	}

	protected string ComponentName
	{
		get { return Data.GetValue<string>("ComponentName"); }
	}

	protected string CategoryDisplayName
	{
		get { return Data.GetValue<string>("CategoryDisplayName"); }
	}

	protected string ComponentTypeDisplayName
	{
		get { return Data.GetValue<string>("ComponentTypeDisplayName"); }
	}

	protected string[] ValidThresholdValues
	{
		get
		{
			var result = new List<string>(2);
			foreach (var value in new[] { Data.GetValue<double?>("ThresholdWarningValue"), Data.GetValue<double?>("ThresholdCriticalValue") }) 
			{
				if(Threshold.IsValidValue(value))
				{
					result.Add(value.Value.ToString(CultureInfo.InvariantCulture));
				}
			}
			return result.ToArray();
		}
	}

    protected string PerformanceCounterPath
    {
        get
        {
            var category = IisStatistic.ComponentSettings["Category"].Value.Value;
            var counter = IisStatistic.ComponentSettings["Counter"].Value.Value;
            var instance = IisStatistic.ComponentSettings["Instance"].Value.Value;

            if (!String.IsNullOrEmpty(instance))
            {
                if (instance.IndexOf(Constants.SiteNameMacro, StringComparison.InvariantCultureIgnoreCase) > -1)
                {
                    instance = instance.Replace(Constants.SiteNameMacro, Data.GetValue<string>("SiteName"));
                }
                else if (instance.IndexOf(Constants.ApplicationPoolNameMacro, StringComparison.InvariantCultureIgnoreCase) > -1)
                {
                    instance = instance.Replace(Constants.ApplicationPoolNameMacro, Data.GetValue<string>("PoolName"));
                }
                return InvariantString.Format(@"\{0}({1})\{2}", category, instance, counter);
            }

            return String.Format(@"\{0}\{1}", category, counter);
        }
    }

    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (IisStatistic == null)
		{
			ctrStatisticPlaceHolder.Visible = false;
			ctrInvalidCmpPlaceHolder.Visible = true;
			return;
		}

		ctrCmpImgStatus.ImageUrl = ComponentImageUrl;

		ctrCmpName.Text = ComponentName;
		ctrCmpStatus.Text = ComponentStatusDescription;
		ctrCmpCounterGroup.Text = CategoryDisplayName;
		ctrCmpCounterType.Text = ComponentTypeDisplayName;
        ctrCmpExchangePerformanceCounter.Text = PerformanceCounterPath;
	}

	protected bool ShowCounterError()
	{
		return IisStatistic != null && IisStatistic.CurrentError.ErrorCode > 0;
	}

	protected ApmMonitorError GetCounterError()
	{
		return IisStatistic != null ? IisStatistic.CurrentError : null;
	}
}
