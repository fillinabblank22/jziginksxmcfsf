﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Castle.Core.Internal;
using SolarWinds.APM.BlackBox.IIS.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.i18n.Registrar;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_MultiTabResource : BaseResourceControl, IResourceIsInternal
{
    private readonly Log log = new Log();

    protected class PropName
    {
        public const string TabOrder = "taborder";
        public const string EditUrl = "editurl";
    }

    protected class QueryParamName
    {
        public const string NetObject = "NetObject";
        public const string ViewID = "ViewID";
        public const string ReturnTo = "ReturnTo";
        public const string ResourceID = "ResourceID";
    }

    protected class CssClasses
    {
        public const string Item = "item";
        public const string Active = "active";
    }

    protected class TabSettings
    {
        public string TabId { get; set; }
        public string TabName { get; set; }
        public string TabName_i18nKey { get; set; }
        public string ResourcePath { get; set; }
        public Dictionary<string, string> Properties { get; set; }
    }

    protected TabSettings[] Tabs { get; private set; }

    private string ActiveTabId
    {
        get { return this.ViewState.GetStrictOrDefault("ActiveTabId", (this.Tabs.Any() ? this.Tabs.First().TabId : string.Empty)); }
        set { this.ViewState["ActiveTabId"] = value; }
    }

    private TabSettings ActiveTab
    {
        get { return this.Tabs.FirstOrDefault(t => t.TabId == this.ActiveTabId); }
    }

    protected string CustomEditUrl
    {
        get { return this.Resource.Properties[PropName.EditUrl]; }
    }

    override protected string DefaultTitle
    {
        get { return string.Empty; }
    }

    public override string HelpLinkFragment
    {
        get { return "samagappinsiisappdetiisavgcpures"; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        this.ParseTabSettings();
        this.RenderTabHeaders();
        this.ParseActiveTab();
        this.RenderTabContent();
    }

    protected override void OnPreRender(EventArgs e)
    {
        this.RenderActiveTab();

        base.OnPreRender(e);
    }

    private void ParseActiveTab()
    {
        if (!this.IsPostBack)
        {
            return;
        }
        var target = this.Request.Params["__EVENTTARGET"];
        var tabIndex = this.phTabHeaders.Controls.Cast<Control>().IndexOf(th => th.UniqueID == target);
        if (tabIndex == -1)
        {
            return;
        }
        var tab = this.Tabs.GetValueOrDefault(tabIndex);
        this.ActiveTabId = (tab == null) ? string.Empty : tab.TabId;
    }

    private static readonly Regex reTabProperty = new Regex(@"^tab\((?'id'.+)\)(\.(?'type'properties))?\.(?'key'.+)$");

    private enum PropType
    {
        Inner = 0,
        Properties
    }

    private static PropType GetPropType(string value)
    {
        switch (value)
        {
            case "properties": return PropType.Properties;
            default: return PropType.Inner;
        }
    }

    private static void ExtendProperties(Dictionary<string, string> target, Dictionary<string, string> source)
    {
        foreach (var prop in source)
        {
            if (!target.ContainsKey(prop.Key))
            {
                target[prop.Key] = prop.Value;
            }
        }
    }

    private void ParseTabSettings()
    {
        var sourceProps = this.Resource.Properties;

        // read tab order from resource property into array of trimmed tab ids
        var tabOrder = Convert.ToString(sourceProps[PropName.TabOrder] ?? string.Empty)
            .ToLower()
            .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
            .Select(s => s.Trim())
            .ToArray();
        
        // parse all tab related resource settings and group them by tab id
        var tabData = sourceProps.Keys.Cast<string>()
            .Select(k => new { Key = k, Match = reTabProperty.Match(k) })
            .Where(t => t.Match.Success)
            .Select(t => new
            {
                t.Key,
                Value = sourceProps[t.Key],
                TabId = t.Match.Groups["id"].Value,
                PropType = GetPropType(t.Match.Groups["type"].Value),
                Name = t.Match.Groups["key"].Value
            })
            .GroupBy(t => t.TabId)
            .OrderBy(t => Array.IndexOf(tabOrder, t.Key))
            .ToList();
        
        
        // process all tab related settings
        var tabs = tabData
            .Select(td =>
            {
                var dataInternal = td.Where(d => d.PropType == PropType.Inner).ToDictionary(d => d.Name, d => d.Value);
                var dataPropsProp = td.Where(d => d.PropType == PropType.Properties).ToDictionary(d => d.Name, d => d.Value);
                return new TabSettings
                {
                    TabId = td.Key,
                    TabName = dataInternal.GetValueOrDefault("name"),
                    TabName_i18nKey = dataInternal.GetValueOrDefault("name_i18n_key"),
                    ResourcePath = dataInternal.GetValueOrDefault("path"),
                    Properties = dataPropsProp
                };

            })
            .ToDictionary(ts => ts.TabId, ts => ts);

        var tabUniversal = tabs.GetValueOrDefault("*");

        if (tabUniversal != null)
        {
            tabs.Remove("*");
            tabs.Values.ForEach(ts => ExtendProperties(ts.Properties, tabUniversal.Properties));
        }

        this.Tabs = tabs.Values.ToArray();
    }

    void AddTabHeader(string tabId, string name)
    {
        var ctlTabHeader = new LinkButton
        {
            ID = tabId,
            Text = name,
            CssClass = CssClasses.Item
        };
        this.phTabHeaders.Controls.Add(ctlTabHeader);
    }

    void RenderTabHeaders()
    {
        this.phTabHeaders.Controls.Clear();

        foreach (var tab in this.Tabs)
        {
            string tabDescription = !string.IsNullOrEmpty(tab.TabName_i18nKey) 
                ? TryGetLocalizedAPMString(tab.TabName_i18nKey)
                : tab.TabName;
            this.AddTabHeader(tab.TabId, tabDescription);
        }
    }

    void RenderActiveTab()
    {
        var tabs = this.phTabHeaders.Controls.OfType<LinkButton>();
        foreach (var tab in tabs)
        {
            tab.CssClass = CssClasses.Item;
            if (tab.ID == this.ActiveTabId)
            {
                tab.CssClass += " " + CssClasses.Active;
            }
        }
    }


    #region tab content rendering

    Control LoadResource(string resourcePath)
    {
        try
        {
            return this.Page.LoadControl(resourcePath);
        }
        catch (Exception xcp)
        {
            var msg = InvariantString.Format("failed to load resource '{0}'", resourcePath);
            log.Error(msg, xcp);
            return new HtmlGenericControl("div")
            {
                InnerText = msg
            };
        }
    }

    private const string ResourceReferencePrefix = "[resource].";

    string GetPropValue(string value)
    {
        if (value == null)
        {
            return null;
        }
        if (value.StartsWith(ResourceReferencePrefix))
        {
            var name = value.Substring(ResourceReferencePrefix.Length);
            return this.Resource.Properties.GetValueOrDefault(name.ToLowerInvariant());
        }
        return value;
    }

    void SetProperties(IPropertyProvider resource, Dictionary<string, string> props)
    {
        foreach (var prop in props)
        {
            resource.Properties[prop.Key] = Convert.ToString(GetPropValue(prop.Value));
        }
    }

    void AddTabContent(TabSettings tab)
    {
        var ctlInner = LoadResource(tab.ResourcePath);
        var provider = ctlInner as IPropertyProvider;
        if (provider != null)
        {
            this.SetProperties(provider, tab.Properties);
        }
        this.phTabContents.Controls.Add(ctlInner);
    }

    void RenderTabContent()
    {
        if (this.ActiveTab != null)
        {
            this.AddTabContent(this.ActiveTab);
        }
    }

    #endregion


    #region customize page related

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IIisApplicationProvider) }; }
    }

    public bool IsInternal { get { return true; } }

    #endregion

    private string FormatEditUrl(string path)
    {
        var ub = new UriBuilder(this.Request.Url);
        ub.Path = path;
        var query = HttpUtility.ParseQueryString(ub.Query);
        query[QueryParamName.ResourceID] = this.Resource.ID.ToInvariantString();
        // add standard orion parameters, when available
        var netobj = this.Request.QueryString[QueryParamName.NetObject];
        if (!string.IsNullOrEmpty(netobj))
        {
            query[QueryParamName.NetObject] = netobj;
        }
        var oview = this.Page as OrionView;
        if (oview != null)
        {
            query[QueryParamName.ViewID] = oview.ViewInfo.ViewID.ToString();
        }
        query[QueryParamName.ReturnTo] = ReferrerRedirectorBase.GetReturnUrl();
        ub.Query = query.ToString();
        return ub.ToString();
    }

    public override string EditURL
    {
        get {
            if (string.IsNullOrEmpty(this.CustomEditUrl))
            {
                return base.EditURL;
            }
            return this.FormatEditUrl(this.CustomEditUrl);
        }
    }

    private string TryGetLocalizedAPMString(string i18nKey)
    {
        const string managerid = "APM.Strings";

        ResourceManagerRegistrar registrar = ResourceManagerRegistrar.Instance;
        return registrar.SearchAll(i18nKey, new[] { managerid }) ?? i18nKey;
    }
}
