<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GraphEdit.aspx.cs" EnableEventValidation="false" Inherits="Resources_GraphEdit" MasterPageFile="~/Orion/ResourceEdit.master" Title="<%$ Resources : APMWebContent , APMWEBDATA_VB1_337%>" ValidateRequest="false" %>
<%@ Register TagPrefix="apm" TagName="Assigner" Src="~/Orion/APM/Controls/AssignResourceToComponent.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="apm" TagName="MultiValueSelection" Src="~/Orion/APM/Controls/MultiValueSelection.ascx" %>
<%@ Register TagPrefix="apm" TagName="SingleValueSelection" Src="~/Orion/APM/Controls/SingleValueSelection.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
	<h1><%= string.Format(Resources.APMWebContent.APMWEBCODE_VB1_129, this.Resource.Title)%></h1>
	<div>

    <asp:ScriptManager runat="server"></asp:ScriptManager>

		<apm:Assigner ID="ctrAssigner" runat="server" />
		<orion:EditResourceTitle runat="server" ID="ResourceTitleEditor" ShowSubTitle="false" />
		<p>
			<b><%= Resources.APMWebContent.APMWEBDATA_VB1_330 %></b><br />
			<asp:DropDownList ID="TimePeriodsList" runat="server"/>
		</p>
		<p>
			<b><%= Resources.APMWebContent.APMWEBDATA_VB1_331 %></b><br />
			<asp:DropDownList ID="SampleSizeList" DataTextField="Name" DataValueField="SizeName" Width="230px" runat="server"/>
		</p>
		<asp:CustomValidator ID="ctrPeriodValidator" OnServerValidate="OnPeriodValidator_ServerValidate" runat="server"/>

        <p id="YAxisConfigurationCheckBox" visible="false" runat="server">
            <asp:CheckBox ID="ShowYAxisFromZero" Checked="true" Text="<%$ Resources : APMWebContent , APMWEBDATA_VB1_338 %>" runat="server" />
        </p>

		<p id="DataSourceTypeEditPanel" visible="false" runat="server">
			<b><%= Resources.APMWebContent.APMWEBDATA_VB1_332 %></b><br />
			<asp:DropDownList ID="DataSourceTypeList" DataTextField="DisplayName" DataValueField="Name" Width="230px" runat="server"/>
		</p>
        <p>
            <apm:MultiValueSelection runat="server" ID="MultiValueSelection" />
        </p>
        <p>
            <apm:SingleValueSelection runat="server" ID="SingleValueSelection" />
        </p>
        <br />
		<div>
             <orion:LocalizableButton runat="server" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitButton_Click" />
		</div>

	</div>
</asp:Content>
