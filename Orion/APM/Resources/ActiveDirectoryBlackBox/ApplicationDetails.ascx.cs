﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Resources;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web.Dal;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web.Models;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_APM_Resources_ActiveDirectoryBlackBox_ApplicationDetails : ApmBaseResource
{
    private ApplicationDto _application;
    private bool _domainInitialized;
    private ApmMonitorError applicationError;

    public ApplicationDto ApplicationDto
    {
        get
        {
            if (!_domainInitialized)
            {
                _application = ServiceLocator.GetService<IActiveDirectoryApplicationDal>().FindByApplicationId(App.Id) ?? new ApplicationDto();
                _domainInitialized = true;
            }

            return _application;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof(IActiveDirectoryApplicationProvider)}; }
    }

    protected override string DefaultTitle
    {
        get { return APM_ActiveDirectoryBlackBoxContent.ApplicationDetails_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAADApplicationDetails"; }
    }

    public ActiveDirectoryApplication App
    {
        get { return GetInterfaceInstance<IActiveDirectoryApplicationProvider>().ActiveDirectoryApplication; }
    }

    protected ApmMonitorError ApplicationError
    {
        get
        {
            if (applicationError == null)
            {
                var errorMessage = LoadApplicationError();
                const int statusCode = 0;
                if (errorMessage == null)
                {
                    applicationError = new ApmMonitorError
                    {
                        Status = Status.Undefined //will be displayed as 'Initial poll in progress'
                    };
                }
                else if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    applicationError = new ApmMonitorError(ApmErrorCode.Ok, statusCode, StatusCodeType.None, errorMessage)
                    {
                        Status = Status.Undefined
                    };
                }
                else
                {
                    applicationError = new ApmMonitorError(ApmErrorCode.Ok, statusCode, StatusCodeType.None, string.Empty)
                    {
                        Status = Status.Available
                    };
                }
            }
            return applicationError;
        }
    }

    protected bool ShowApplicationError
    {
        get { return ApplicationError.Status != Status.Available; }
    }

    protected string GetNameOrUnknown(string domainName)
    {
        return domainName ?? APMWebContent.UnknownValue;
    }

    protected string GetShortDateTimeOrNA(ApmDateTime dateTime)
    {
        if(dateTime != null)
        {
            return dateTime.ToString("f", CultureInfo.CurrentCulture);
        }
        else
        {
            return APMWebContent.APMWEBCODE_AK1_23;
        }
    }

    protected string GetApplicationStatus(string applicationStatus)
    {
        if(string.IsNullOrWhiteSpace(applicationStatus))
        {
            return APMWebContent.APMWEBCODE_VB1_77;
        }
        else
        {
            return applicationStatus;
        }
    }

    private string LoadApplicationError()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string Query = @"SELECT ErrorMessage FROM Orion.APM.CurrentApplicationStatus WHERE ApplicationID = @ApplicationID";
            var status = swis.Query(Query, new Dictionary<string, object> { { "ApplicationID", App.Id } });
            return status.Rows.Count > 0 ? status.Rows[0].Field<string>(0) : null;
        }
    }
}