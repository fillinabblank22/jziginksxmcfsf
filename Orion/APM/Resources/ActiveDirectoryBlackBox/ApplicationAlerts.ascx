<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationAlerts.ascx.cs" Inherits="Orion_APM_Resources_Application_ApplicationAlerts" %>
<%@ Register TagPrefix="orion" TagName="AlertSuppressionInfoMessage" Src="~/Orion/NetPerfMon/Controls/AlertSuppressionInfoMessage.ascx" %>
<%@ Register TagPrefix="orion" TagName="AlertsTable" Src="~/Orion/NetPerfMon/Controls/AlertsOnThisEntity.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:AlertSuppressionInfoMessage ID="AlertSuppressionInfoMessage" runat="server"/>
        <orion:AlertsTable runat="server" ID="AlertsTable"/>
    </Content>
</orion:resourceWrapper>