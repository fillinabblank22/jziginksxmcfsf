using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Alarms)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warns)]
public partial class Orion_APM_Resources_Application_ApplicationAlerts : ApmAppBaseResource
{
    private bool showAcknowledgedAlerts;
    
    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForActiveDirectoryActiveAlerts"; }
    }

	protected override string DefaultTitle
	{
		get { return Resources.APMWebContent.APMWEBCODE_VB1_118; }
	}

    public override string DrawerIcon => ResourceDrawerIconName.Table.ToString();

    public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
				return Resource.SubTitle;

			if (this.showAcknowledgedAlerts)
				return Resources.APMWebContent.APMWEBCODE_VB1_119;
				
			return Resources.APMWebContent.APMWEBCODE_VB1_120;
		}
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IApplicationProviderBase) }; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var node = GetInterfaceInstance<INodeProvider>().Node;
        var application = GetInterfaceInstance<IApplicationProviderBase>().ApmApplication;
        if (application == null || node == null)
        {
            //hide resource because otherwise it will display incorrect data
            this.Wrapper.Visible = false;
            return;
        }
		
		string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
		AlertsTable.ShowAcknowledgedAlerts = showAcknowledgedAlerts = !String.IsNullOrEmpty(showAck) && showAck.Equals("true", StringComparison.OrdinalIgnoreCase);
		AlertsTable.ResourceID = Resource.ID;
		AlertsTable.RelatedNodeId = node.NodeID;
		AlertsTable.RelatedNodeEntityUri = node.SwisUri;
		AlertsTable.TriggeringObjectEntityUris.Add(application.SwisUri);

        AlertsTable.TriggeringObjectEntityUris.AddRange(
            WebPluginManager.Instance.SupportedPlugins.Where(
                plugin => plugin.SupportedCustomApplicationTypes.Contains(application.CustomType, StringComparer.InvariantCultureIgnoreCase))
                    .OfType<IAlertingEntitiesUriProvider>().SelectMany(x => x.GetEntitiesUriForAlerting(application.Id)));

		int rowsPerPage;
		if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
			rowsPerPage = 5;

		AlertsTable.InitialRowsPerPage = rowsPerPage;
		
		AlertSuppressionInfoMessage.SetSuppressAlertsMessageVisibility(application.SwisUri);
	}
}