﻿<%@ Control Language="C#" ClassName="ApplicationDetails" AutoEventWireup="true" CodeFile="ApplicationDetails.ascx.cs" Inherits="Orion_APM_Resources_ActiveDirectoryBlackBox_ApplicationDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table cellspacing="0" class="biggerPadding NeedsZebraStripes">
            
            <tr>
                <td class="PropertyHeader" style="white-space: nowrap;">
                    <%= APM_ActiveDirectoryBlackBoxContent.ApplicationDetails_DisplayName %>
                </td>
                <td>&nbsp;</td>
                <td class="Property statusAndText">
                    <apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server"
                        CustomApplicationType="<%# App.CustomType %>" StatusValue="<%# App.Status %>"/>
                    <a href="<%: GetViewLink(App.NetObjectID) %>"><%: App.Name %></a>
                    <span> on</span>
                    <orion:SmallNodeStatus ID="ServerStatusIcon" runat="server"
                        StatusValue="<%# App.NPMNode.Status %>" />
                    <a href="<%: GetViewLink(App.NPMNode.NetObjectID) %>"><%: App.NodeName %></a>
                </td>
            </tr>
            <% if (ShowApplicationError) { %>
                <tr>
                    <td colspan="2">
                        <%= ApplicationError.GetStatusDetailsInline() %>
                    </td>
                </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader">
                    <%: APM_ActiveDirectoryBlackBoxContent.ApplicationDetails_LastSuccessfulPoll %>
                </td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%: GetShortDateTimeOrNA(ApplicationDto.LastSuccessfulPoll) %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader">
                    <%: APM_ActiveDirectoryBlackBoxContent.ApplicationDetails_RootDomainDisplayName %>
                </td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%: GetNameOrUnknown(ApplicationDto.RootDomainDisplayName) %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader">
                    <%: APM_ActiveDirectoryBlackBoxContent.ApplicationDetails_DomainDisplayName %>
                </td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%: GetNameOrUnknown(ApplicationDto.DomainDisplayName) %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader">
                    <%: APM_ActiveDirectoryBlackBoxContent.ApplicationDetails_WhenCreated %>
                </td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%: GetShortDateTimeOrNA(ApplicationDto.WhenCreated) %>
                </td>
            </tr>
            
            <tr>
                <td class="PropertyHeader">
                    <%: APM_ActiveDirectoryBlackBoxContent.ApplicationDetails_WhenChanged %>
                </td>
                <td>&nbsp;</td>
                <td class="Property">
                    <%: GetShortDateTimeOrNA(ApplicationDto.WhenChanged) %>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
