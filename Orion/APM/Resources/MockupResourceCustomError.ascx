﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MockupResourceCustomError.ascx.cs" Inherits="Orion_APM_Resources_MockupResourceCustomError" %>

<div id="container">
	<div id="content">
        <div style="font-size: large; margin:0px 0px 20px 20px;"><%= Resources.APMWebContent.APMWEBDATA_AK1_129 %></div>
        <table style="background-color: White; border: solid 1px #cccccc; margin: 0px 20px 20px 20px">
            <tr>
                <td style="padding: 10px; " valign="top"><img alt="Website Error" src="/Orion/images/stop_32x32.gif" /></td>
                <td>
                    <p><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_130, SolarWinds.APM.Common.ApmConstants.ModuleShortName) %></p>
                    <ol>
                        <li><%= Resources.APMWebContent.APMWEBDATA_AK1_131 %></li>
                        <li><%= Resources.APMWebContent.APMWEBDATA_AK1_132 %></li>
                    </ol>
                    <p><b><%= Resources.APMWebContent.APMWEBDATA_AK1_133 %></b></p>
                    <pre><asp:PlaceHolder runat="server" ID="ErrorMessagePlaceHolder" /></pre>
                </td>
            </tr>
        </table>
    </div>
</div>
