﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MockupResource.ascx.cs" Inherits="Orion_APM_Resources_MockupResource" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<div style='width: <%= ResourceWidth %>px; padding: 0px' class="ResourceWrapper">
	<div class="HeaderBar" style="padding: 14px">
        <div class="HelpButton">
            <orion:LocalizableButton Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_144 %>" ID="helpButton" DisplayType="Resource" runat="server" />
        </div>
        <h1><%= ResourceTitle %></h1>
    </div>
</div>
