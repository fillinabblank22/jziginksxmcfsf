using System;
using System.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_APM_Resources_EditThwackUploads : Page
{
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    private string _netObjectID;

    private void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ReferrerRedirector.Initialize(ViewState);
        }
    } 

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.APMWebContent.APMWEBCODE_AK1_134, this.Resource.Title);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
            _netObjectID = Request.QueryString["NetObject"];

        SortByList.SelectedValue = this.Resource.Properties["SortBy"];

        string showType = this.Resource.Properties["ShowType"];
        if (String.Equals(showType, "all", StringComparison.OrdinalIgnoreCase) || String.IsNullOrEmpty(showType))
        {
            ShowAll.Checked = true;
        }
        ShowLast.Checked = !ShowAll.Checked;

        int days;
        NumberOfDays.Text = Int32.TryParse(this.Resource.Properties["ShowLast"], out days) ? days.ToString() : "30";
        int records;
        NumberOfRecords.Text = Int32.TryParse(this.Resource.Properties["NumberOfRecords"], out records) ? records.ToString() : "15";
        bool limit;
        LimitCount.Checked = bool.TryParse(this.Resource.Properties["LimitCount"], out limit) ? limit : true;
    }


    protected void SubmitClick(object sender, EventArgs e)
	{
        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

	    this.Resource.Properties["SortBy"] = SortByList.SelectedValue;
        this.Resource.Properties["ShowType"] = ShowAll.Checked ? "all" : "lastX";
        this.Resource.Properties["ShowLast"] = NumberOfDays.Text;
        this.Resource.Properties["NumberOfRecords"] = NumberOfRecords.Text;
        this.Resource.Properties["LimitCount"] = LimitCount.Checked ? true.ToString() : false.ToString();


        ReferrerRedirector.Redirect(ViewState, @"/Orion/APM/Summary.aspx");
    }
}
