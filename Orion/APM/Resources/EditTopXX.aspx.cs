using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models;
using System.Globalization;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class EditTopXX : System.Web.UI.Page
{
    private string maxCount = "10";
    private string filter = string.Empty;

    private ResourceInfo _resource;

    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
        this._resource = ResourceManager.GetResourceByID(resourceID);

        if (!IsPostBack)
        {
            maxCount = GetProperty("MaxCount", maxCount);
            filter = GetProperty("Filter", "");

            tbMaxRows.Text = maxCount;
            tbEditTitle.Text = this.Resource.Title;
            tbEditSubTitle.Text = this.Resource.SubTitle;
            tbFilter.Text = filter;

            ReferrerRedirector.Initialize(ViewState);
        }
        btnSubmit.Focus();
    }

    private string GetProperty(string propertyName, string defaultValue)
    {
        if (this.Resource.Properties.ContainsKey(propertyName))
            return this.Resource.Properties[propertyName];

        return defaultValue;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        this.Resource.Title = tbEditTitle.Text;
        this.Resource.SubTitle = tbEditSubTitle.Text;
        this.Resource.Properties["MaxCount"] = tbMaxRows.Text;
        this.Resource.Properties["Filter"] = tbFilter.Text;

        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(this.Resource);

        ReferrerRedirector.Redirect(ViewState, @"/Orion/APM/Summary.aspx");
    }
}

