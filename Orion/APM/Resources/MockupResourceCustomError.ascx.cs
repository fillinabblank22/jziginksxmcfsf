﻿using System;
using System.Web;
using System.Web.UI;

public partial class Orion_APM_Resources_MockupResourceCustomError : UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Exception ex = Server.GetLastError();
        
        if (null != ex)
        {
            if (ex is HttpUnhandledException)
            {
                ex = ex.InnerException;
            }

            ErrorMessagePlaceHolder.Controls.Add(new LiteralControl(ex.Message));
            
            Server.ClearError();
        }
        else
        {
            ErrorMessagePlaceHolder.Controls.Add(new LiteralControl((string)Session["MockupResourceErrorMessage"]));
        }
    }
}
