<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
    CodeFile="EditTopXX.aspx.cs" Inherits="EditTopXX" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
    
<%@ Register TagPrefix="apm" TagName="FilterExamples" Src="~/Orion/APM/Controls/FilterExamples.ascx" %>
       

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <h1><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_148, Resource.Title) %></h1>
    <table style="margin-left: 10px">
        <tr>
            <td>
                <b><%= Resources.APMWebContent.APMWEBDATA_VB1_69 %></b><br>
                <asp:TextBox runat="server" ID="tbEditTitle" MaxLength="60" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                <b><%= Resources.APMWebContent.APMWEBDATA_VB1_70 %></b><br>
                <asp:TextBox runat="server" ID="tbEditSubTitle" MaxLength="60" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                <b><%= Resources.APMWebContent.APMWEBDATA_AK1_149 %></b><br />
                <asp:TextBox runat="server" ID="tbMaxRows" Text="10" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbMaxRows"
                    SetFocusOnError="true">*</asp:RequiredFieldValidator>
                <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="tbMaxRows"
                    Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="1" ErrorMessage="Invalid entry"
                    SetFocusOnError="true"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td><br />
                <b><%= Resources.APMWebContent.APMWEBDATA_AK1_150 %></b><br>
                <asp:TextBox runat="server" ID="tbFilter" MaxLength="200" Width="300px" />
            </td>
        </tr>
        <tr>
            <td style="font-size:8pt">
                <%= Resources.APMWebContent.APMWEBDATA_AK1_151 %><br/>
                <a href="<%= HelpHelper.GetHelpUrl("OrionAPMAGSQLSyntax") %>" id="filterHelpLink" style="text-decoration: underline;" target="_blank"><%= Resources.APMWebContent.APMWEBDATA_AK1_152 %></a>.
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit"
                    OnClick="SubmitClick" CausesValidation="true" />
            </td>
        </tr>
    </table>
</asp:Content>
