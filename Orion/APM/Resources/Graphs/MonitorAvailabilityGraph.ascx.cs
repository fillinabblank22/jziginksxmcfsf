using System.Web.UI.HtmlControls;
using SolarWinds.APM.Web;

public partial class Graphs_MonitorAvailabilityGraph : ApmMonitorGraphResource
{
	public override string ChartName
	{
		get { return SolarWinds.APM.Web.Charting.ChartInfo.TYPES.MonAvail; }
	}
	
    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_150; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHMonitorDetailsMonAvail"; }
    }
}
