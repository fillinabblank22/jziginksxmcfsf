using System.Web.UI.HtmlControls;
using SolarWinds.APM.Web;

public partial class Graphs_ApplicationAvailabilityGraph : ApmAppGraphResource
{
	public override HtmlImage ChartImage
	{
		get { return this.chartImage; }
	}

	public override string ChartName
	{
		get { return SolarWinds.APM.Web.Charting.ChartInfo.TYPES.AppAvail; }
	}
    
	protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_146; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppDetailsAppAvail"; }
    }
}
