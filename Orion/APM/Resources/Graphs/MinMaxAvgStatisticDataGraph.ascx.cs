using System;
using System.Data;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Charting;

public partial class Graphs_MinMaxAvgStatisticDataGraph : ApmMonitorGraphResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (ApmMonitor.IsConfigured)
        {
            Visible = ApmMonitor.BaseComponent.IsScript || ApmMonitor.BaseComponent.HasStatisticData;
        }
    }

	public override string ChartName
	{
		get 
        {
            return ApmMonitor.BaseComponent.IsScript ? ChartInfo.TYPES.MonMMADynamicStatistic : ChartInfo.TYPES.MonMMAStatistic; 
        }
	}
	
	protected override string DefaultTitle
	{
        get { return Resources.APMWebContent.APMWEBCODE_VB1_147; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHMonitorDetailsMinMaxStatistics"; }
	}

	protected override System.Collections.Generic.Dictionary<String, Object> EditUrlParams
	{
		get
		{
			var pairs = base.EditUrlParams;
			pairs[ModuleConstants.QueryStringParameters.MinMaxAvgMultiValueSelection] = true;
			if (ApmMonitor != null) 
			{
				if (ApmMonitor.ComponentFromResource)
				{
					if (!pairs.ContainsKey(ModuleConstants.QueryStringParameters.ApplicationId))
					{
						pairs[ModuleConstants.QueryStringParameters.ApplicationId] = ApmMonitor.Application.Id;
					}
				}
				else if (ApmMonitor.BaseComponent.IsScript)
				{
					if (!pairs.ContainsKey(ModuleConstants.QueryStringParameters.ComponentId))
					{
						pairs[ModuleConstants.QueryStringParameters.ComponentId] = ApmMonitor.Id;
					}
				}
			}
            pairs[ChartInfo.KEYS.ShowYAxisFromZero] = true;
			return pairs;
		}
	}

    protected override void PopulateChartUrl(ref UrlBuilder url)
    {
        base.PopulateChartUrl(ref url);

        if (ApmMonitor.BaseComponent.IsScript)
        {
            string valueToDisplay = this.Resource.Properties[ResourceSettings.ValueToDisplay];
            string subTitleStatisticValueLabel = string.Empty;

            var dynamicEvidenceDal = new DynamicEvidenceDal();
            DataTable table = dynamicEvidenceDal.GetDynamicEvidenceColumns(ApmMonitor.Id, DynamicEvidenceColumnDataType.Numeric);

            if (table != null && table.Rows.Count > 0)
            {
                // I need check if value stored in Resouce is defined for current component monitor
                string expresion = string.Format("Name = '{0}'", this.Resource.Properties[ResourceSettings.ValueToDisplay]);
                DataRow[] foundRows = table.Select(expresion);

                if (this.Resource.Properties[ResourceSettings.ValueToDisplay] == null || foundRows.Length < 1)
                {
                    valueToDisplay = (string)table.Rows[0]["Name"];
                    subTitleStatisticValueLabel = (string)table.Rows[0]["Label"];
                }
                else
                {
                    valueToDisplay = this.Resource.Properties[ResourceSettings.ValueToDisplay];
                    subTitleStatisticValueLabel = (string)foundRows[0]["Label"];
                }
                url[ChartInfo.KEYS.SubTitle] = string.Format(Resources.APMWebContent.APMWEBCODE_VB1_148, GetLocalizedProperty("ColumnLabel", subTitleStatisticValueLabel));
            }

            if (String.IsNullOrEmpty(url[ChartInfo.KEYS.StatisticValueName])) 
			{
				url[ChartInfo.KEYS.StatisticValueName] = valueToDisplay;
			}
        }

        if(String.IsNullOrEmpty(url[ChartInfo.KEYS.ShowYAxisFromZero]))
        {
            url[ChartInfo.KEYS.ShowYAxisFromZero] = this.Resource.Properties[ChartInfo.KEYS.ShowYAxisFromZero];
        }
    }


}
