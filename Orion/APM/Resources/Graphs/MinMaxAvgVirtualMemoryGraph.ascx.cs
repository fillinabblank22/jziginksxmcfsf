using System;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;

public partial class Graphs_MinMaxAvgVirtualMemoryGraph : ApmMonitorGraphResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (ApmMonitor.IsConfigured)
        {
            Visible = ApmMonitor.BaseComponent.HasVirtualMemorySupport;
        }
    }

	public override string ChartName
	{
		get { return ChartInfo.TYPES.MonMMAVMem; }
	}
	
	protected override string DefaultTitle
    {
        get { return APMWebContent.Web_LegacyAverageVirtualMemory_Resource_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHMonitorDetailsMinMaxVirtMem"; }
    }

    protected override System.Collections.Generic.Dictionary<string, object> EditUrlParams
    {
        get
        {
            var pairs = base.EditUrlParams;
            pairs[ChartInfo.KEYS.ShowYAxisFromZero] = true;
            return pairs;
        }
    }

    protected override void PopulateChartUrl(ref UrlBuilder url)
    {
        base.PopulateChartUrl(ref url);

        if (String.IsNullOrEmpty(url[ChartInfo.KEYS.ShowYAxisFromZero]))
        {
            url[ChartInfo.KEYS.ShowYAxisFromZero] = this.Resource.Properties[ChartInfo.KEYS.ShowYAxisFromZero];
        }
    }
}
