using System.Collections.Generic;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.UI.Resource;

public partial class Graphs_MonitorBarGraph : ApmMonitorGraphResource
{
	public override string ChartName
	{
		get
		{
			var dsType = Resource.Properties[ChartInfo.KEYS.DataSourceType];

			var info = GetAssignedInfo();
			if (info[AssignedToResourceInfo.GraphDataSourceType] != null)
			{
				dsType = info[AssignedToResourceInfo.GraphDataSourceType].ToString();
			}
			switch (dsType)
		    {
                case "ResponseTime":
                    return ChartInfo.TYPES.MonResponseTimeBarChart;
                case "StatisticData":
					return ChartInfo.TYPES.MonStatisticDataBarChart;
                default:
					return ChartInfo.TYPES.MonAvailBarChart;
		    }
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHAppDetailsCustomBarGraph"; }
	}

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_152; }
    }

	protected override Dictionary<string, object> EditUrlParams
	{
		get
		{
			var pairs = base.EditUrlParams;
			pairs["DataSourceSupported"] = true;
			if (ApmMonitor != null) 
			{
				if (ApmMonitor.Application.Components.Exists(item => item.IsDynamicBased))
				{
					pairs[ModuleConstants.QueryStringParameters.MinMaxAvgMultiValueSelection] = true;
				}
			}
			return pairs;
		}
	}
}