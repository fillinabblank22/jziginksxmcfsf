using System.Collections.Generic;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.UI.Resource;

public partial class Graphs_MonitorAreaGraph : ApmMonitorGraphResource
{
	public override string ChartName
	{
		get
		{
			var dsType = Resource.Properties[ChartInfo.KEYS.DataSourceType];

			var info = GetAssignedInfo();
			if (info[AssignedToResourceInfo.GraphDataSourceType] != null)
			{
				dsType = info[AssignedToResourceInfo.GraphDataSourceType].ToString();
			}
			switch (dsType)
			{
				case "ResponseTime":
					return ChartInfo.TYPES.MonResponseTimeAreaChart;
				case "StatisticData":
					return ChartInfo.TYPES.MonStatisticDataAreaChart;
				default:
					return ChartInfo.TYPES.MonAvailAreaChart;
			}
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHAppDetailsCustomAreaGraph"; }
	}

	protected override string DefaultTitle
	{
        get { return Resources.APMWebContent.APMWEBCODE_VB1_151; }
	}

	protected override Dictionary<string, object> EditUrlParams
	{
		get
		{
			var pairs = base.EditUrlParams;
			pairs["DataSourceSupported"] = true;
			if (ApmMonitor != null) 
			{
				if (ApmMonitor.Application.Components.Exists(item => item.IsDynamicBased))
				{
					pairs[ModuleConstants.QueryStringParameters.MinMaxAvgMultiValueSelection] = true;
				}
			}
			return pairs;
		}
	}
}