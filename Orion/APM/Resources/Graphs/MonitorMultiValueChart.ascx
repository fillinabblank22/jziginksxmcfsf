﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorMultiValueChart.ascx.cs"
    Inherits="Orion_APM_Resources_Graphs_MonitorMultiValueChart" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
    <Content>
		<orion:Include ID="I2" File="APM/Js/Resources.js" runat="server" />
        <div>
            <asp:Repeater runat="server" ID="DetailsRepeater" OnItemDataBound="DetailsRepeater_ItemDataBound">
                <HeaderTemplate>
                    <table class="DataGrid NeedsZebraStripes">
                    <thead>
                        <tr>
                            <td class="apm_stats" colspan="2" width="25%">
                                <%= Resources.APMWebContent.APMWEBDATA_VB1_339 %>
                            </td>
                            <td class="apm_stats" width="70px;">
                                <%= Resources.APMWebContent.APMWEBDATA_VB1_340 %>
                            </td>
                            <td class="apm_stats">
                                <%= Resources.APMWebContent.APMWEBDATA_VB1_341 %>
                            </td>
                        </tr>
                     </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="apm_stats" width="15px;">
                            <div style="background-color: <%# Eval("Color")%>; width: 15px; height: 15px; border: 1px solid #000;">
                            </div>
                        </td>
                        <td class="apm_stats" align="left">
                            <%# Eval("ColumnLabel") %>
                        </td>
                        <td class="apm_stats" nowrap="nowrap">
                            <asp:Label ID="StatisticValueLabel" runat="server"></asp:Label>
                        </td>
                        <td class="apm_stats" >
                            <%# Eval("StringData")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </Content>
</orion:resourceWrapper>
