<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationAvailabilityGraph.ascx.cs" Inherits="Graphs_ApplicationAvailabilityGraph" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<a href="<%= this.GetChartLink().Url %>" target="_blank" tooltip="processed">
			<img id="chartImage" alt="" src="" runat="server"/>
		</a>
	</Content>
</orion:resourceWrapper>
