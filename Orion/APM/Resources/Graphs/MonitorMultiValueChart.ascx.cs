﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.APM.Web.Utility;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_Resources_Graphs_MonitorMultiValueChart : ApmMultiLineMonitorGraphResource
{
    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_154; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHMonitorDetailsMultiStatChart"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    public override string EditURL
    {
        get
        {
			var url = new StringBuilder()
				.AppendFormat("/Orion/Apm/Resources/GraphEdit.aspx?")
				.AppendFormat("ResourceID={0}&", Resource.ID)
				.AppendFormat("{0}=True&", ModuleConstants.QueryStringParameters.MultiValueSelection);
			if (Monitor != null) 
			{
				url.AppendFormat("{0}={1}&", ModuleConstants.QueryStringParameters.ApplicationId, Monitor.Application.Id);
				if (!Monitor.ComponentFromResource)
				{
					url.AppendFormat("{0}={1}&", ModuleConstants.QueryStringParameters.ComponentId, Monitor.Id);
				}
			}
			if (!string.IsNullOrEmpty(Request.QueryStringOrForm("NetObject")))
			{
				url.AppendFormat("NetObject={0}&", Request.QueryStringOrForm("NetObject"));
			}

			var returnUrl = UrlHelper.ToSafeUrlParameter(HtmlHelper.RemoveQueryStringParameter(this.Page, SolarWinds.APM.Web.Charting.ChartInfo.KEYS.Period));
			url.AppendFormat("ReturnURL={0}", returnUrl);
			
			return url.ToString();
        }
    }
    
    protected void Page_Load(object caller, EventArgs ea)
    {
        if (!this.Monitor.BaseComponent.IsScript)
        {
            this.Visible = false;
            return;
        }

		SolarWinds.Orion.Web.ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);

		Parameters[ModuleConstants.ElementList] = GetAssignedColumnName();
        Parameters[ModuleConstants.LegendColorStr] = ModuleConstants.LegendColorList;

        //HACK to fix FB66418.
        this.Parameters["ShowSum"] = SolarWinds.Orion.Web.Charting.ChartAggregation.NoSum.ToString();

        CreateMultiLineChartWithLegendColors(null, this.Monitor.NetObjectID, ModuleConstants.MultiValueStatisticChart, chartPlaceHolder);

        Panel dropDownPanel = new Panel() { CssClass = "EditResourceButton" };
        Control menu = LoadControl("~/Orion/APM/Controls/ExtJsDropDown.ascx");

        IResourceInfo info = ((IResourceInfo)menu);

        if (info != null)
        {
            info.ResourceID = this.Resource.ID;
            info.Period = this.Resource.Properties["Period"];

            StringBuilder sb = new StringBuilder();

            foreach (var p in this.Parameters)
            {
                sb.AppendFormat("&{0}={1}", p.Key, p.Value);
            }

            //HACK to fix FB66418. If this parameter is not present in query string, then labels for CSV columns don't have proper labels
            sb.AppendFormat("&ShowSum={0}", SolarWinds.Orion.Web.Charting.ChartAggregation.NoSum);

            sb.AppendFormat("&ChartName={0}&NetObject={1}", ModuleConstants.MultiValueStatisticChart, this.Monitor.NetObjectID);

            info.QueryStringExtension = sb.ToString();
        }

        dropDownPanel.Controls.Add(menu);

        Wrapper.HeaderButtons.Controls.Add(dropDownPanel);

        LoadLegend();
    }

    protected void CreateMultiLineChartWithLegendColors(CustomPoller customPoller, string netObjectID, string chartName, Control parentControl)
    {
        CreateChart(customPoller, netObjectID, chartName, this.Parameters, parentControl);
    }

    void RemoveRedundantRow(DataTable table)
    {
		List<string> statisticValues = new List<string>();
		string valuesToDisplay = GetAssignedColumnName();
		if (!String.IsNullOrEmpty(valuesToDisplay))
		{
			string[] parts = valuesToDisplay.Split(new string[] { ResourceSettings.ValueSeparator },
												   StringSplitOptions.RemoveEmptyEntries);
			statisticValues.AddRange(parts);

			int x = 0;
			while (x < table.Rows.Count)
			{
				DataRow row = table.Rows[x];
				string statisticName = (string)row["Name"];
				if (!statisticValues.Exists(o => String.Compare(o, statisticName, StringComparison.InvariantCultureIgnoreCase) == 0))
				{
					table.Rows.RemoveAt(x);
				}
				else
					x++;
			}
		}
    }

    void LoadLegend()
    {
        var dal = new ChartingDal();
        //Loading everything from SQL to make sure we always load something (to prevent situations
        //when user select that he wants to display only a single value and then removes it from script
        DataTable table = dal.GetMultiValueStatisticLegendData(this.Monitor.Id);

        //TODO: Consider post-processing to repair invalid settings (users wants to display only non-existing data)
        RemoveRedundantRow(table);

        table.Columns.Add("Color", typeof(string));

        string[] stringSeperator = new string[] { "," };
        string[] colors = ModuleConstants.LegendColorList.Split(stringSeperator, StringSplitOptions.RemoveEmptyEntries);

        var dataView = table.DefaultView;
        dataView.Sort = "ColumnLabel ASC";

        var cc = new ColorConverter();

        for (int i = 0; i < dataView.Count; i++)
        {
            DataRowView drv = dataView[i];
            Color color = (Color)cc.ConvertFromString(colors[i]);;

            drv["Color"] = ColorTranslator.ToHtml(color);
        }

        DetailsRepeater.DataSource = table;
        DetailsRepeater.DataBind();
    }

    protected void PrintStatisticData(object item, Label statisticLabel)
    {
        DataRowView row = (DataRowView)item;

        if (row["NumericData"] != DBNull.Value)
        {
            double value = (double)row["NumericData"];
            Threshold threshold;

            if ( row["ThresholdOperator"] != DBNull.Value)
            {
                double warnLevel = ApmThresholdHelper.GetThresholdValue(row["ThresholdWarning"]);
                double criticalLevel = ApmThresholdHelper.GetThresholdValue(row["ThresholdCritical"]);

                ThresholdOperator thresholdOperator = (ThresholdOperator)(Int16)row["ThresholdOperator"];
                threshold = new Threshold(warnLevel, criticalLevel, thresholdOperator);
            }
            else
            {
                threshold = new Threshold();
            }

            Status componentStatus = threshold.CalculateStatus(value);

            if (componentStatus == Status.Warning)
            {
                statisticLabel.CssClass = "apm_StatisticThresholdWarning";
            }
            else if (componentStatus == Status.Critical)
            {
                statisticLabel.CssClass = "apm_StatisticThresholdCritical";
            }
            ApmStatisticData statistic = new ApmStatisticData(value);

            statisticLabel.Text = statistic.ToString();
        }
    }

    protected void DetailsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var label = e.Item.FindControl("StatisticValueLabel") as Label;
        if ( label == null )
            return;

        PrintStatisticData(e.Item.DataItem, label);
    }

	private String GetAssignedColumnName()
	{
		var info = default(AssignedToResourceInfo);
		AssignedToResourceInfo.TryCreate(Request, Resource, out info);
		if (info[AssignedToResourceInfo.ColumnNames] == null)
		{
			return null;
		}
		return info[AssignedToResourceInfo.ColumnNames].ToString();
	}
}