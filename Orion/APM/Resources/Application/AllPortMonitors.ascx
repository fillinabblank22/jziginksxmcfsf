<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllPortMonitors.ascx.cs"
    Inherits="Orion_APM_Resources_Application_AllPortMonitors" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmStatusIcon" Src="~/Orion/APM/Controls/SmallApmStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="PercentBar" Src="~/Orion/APM/Controls/PercentBar.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I2" File="APM/Js/Resources.js" runat="server" />

        <asp:Repeater ID="monitorRepeater" runat="server" OnInit="monitorRepeater_Init" OnItemDataBound="monitorRepeater_ItemDataBound">
            <HeaderTemplate>
                <table class="DataGrid NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader" style="width: 35%;"><%= Resources.APMWebContent.APMWEBDATA_AK1_1 %></td>
                        <td class="ReportHeader" style="width: 15%;"><%= Resources.APMWebContent.APMWEBDATA_AK1_2 %></td>
                        <td class="ReportHeader" style="width: 20%;"><%= Resources.APMWebContent.APMWEBDATA_AK1_3 %></td>
                        <td class="ReportHeader" style="width: 17%;" colspan="2"><%= Resources.APMWebContent.APMWEBDATA_AK1_4 %></td>
                        <td class="ReportHeader" style="width: 5%;"></td>                               
                        <td class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_AK1_5 %></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td id="tdComponentName" runat="server">
                        <apm:SmallApmStatusIcon ID="MonStatusIcon" runat="server" StatusValue='<%# GetComponentStatus(Container.DataItem)%>' ErrorValue='<%# GetMonitorError(Container.DataItem)%>' />
                        <a href="<%#GetViewLink((string)Eval("ComponentID", "AM:{0}"))%>">
                            <%# HttpUtility.HtmlEncode(Eval(GetKeyName(Container.DataItem))) %>
                    </td>
                    <td id="tdStatisticData" runat="server">
                        <%# GetStatisticData(Container.DataItem, "statisticdata") %>
                    </td>
                    <td id="tdMessage" runat="server">
                        <%# HttpUtility.HtmlEncode(GetStatisticMessage(Container.DataItem, "statisticmessage")) %>
                    </td>
                    <td id="tdResponceTime" runat="server">
                        <%# GetResponseTime(Container.DataItem, "responsetime")%>
                    </td>
                        <apm:PercentBar ID="PercentBar1" width="5px" runat="server" Value='<%# GetResponseTime(Container.DataItem, "responsetime") %>' />
                    <td id="tdSpacer" runat="server">
                    </td>
                    <td  id="tdPort" runat="server">
                        <%# GetColumnValue(Container.DataItem, "port") %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
