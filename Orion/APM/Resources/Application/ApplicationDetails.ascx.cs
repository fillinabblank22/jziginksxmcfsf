using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Models;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Extensions.System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Application_ApplicationDetails : ApmAppBaseResource
{
    protected string WarningMessage = "";

    protected override string DefaultTitle => Resources.APMWebContent.APMWEBCODE_VB1_124;

    public override string HelpLinkFragment => "OrionAPMPHAppDetailsAppDetails";

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        managementControl.Resource = this;
        managementControl.ApmApplication = this.ApmApplication;
        managementControl.ItemsPlaceholder = appDetailsSection;

        managementControl.InitManagementTasks();

        alertSuppressionStatus.EntityUri = this.ApmApplication.SwisUri;
        alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;

        this.AppStatusIcon.StatusValue = this.ApmApplication.Status;
        this.ServerStatusIcon.StatusValue = this.ApmApplication.NPMNode.Status;
        this.WarningMessage = (this.ApmApplication.RootComponents.Count < 1) ? Resources.APMWebContent.APMWEBCODE_VB1_123 : string.Empty;

        this.InitComponentErrorStatus();
    }

    private static ApmMonitorError GetMonitorError(Component c, Dictionary<long, List<Evidence>> componentEvidence)
    {
        List<Evidence> evidenceList = componentEvidence.GetValueOrDefault(c.Id);
        var error = (evidenceList != null && evidenceList.Any())
            ? new ApmMonitorError(evidenceList.First())
            : new ApmMonitorError();
        error.Status = c.Status;
        return error;
    }

    private void InitComponentErrorStatus()
    {
        List<long> componentIds = this.ApmApplication.RootComponents.ConvertAll(c => c.Id);
        Dictionary<long, List<Evidence>> componentEvidence;

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            componentEvidence = businessLayer.GetLatestEvidence(componentIds);
        }

        List<ComponentErrorStatus> components = this.ApmApplication.RootComponents
            .OrderBy(c => c.ComponentOrder)
            .ThenBy(c => c.TemplateId)
            .Select(c => new ComponentErrorStatus
            {
                ComponentID = c.Id,
                Error = GetMonitorError(c, componentEvidence),
                Name = c.Name,
                Status = new ApmStatus(c.Status),
                Type = new ApmComponentType(c.Type)
            })
            .ToList();

        this.componentErrorStatusControl.Initialize(components);
    }
}
