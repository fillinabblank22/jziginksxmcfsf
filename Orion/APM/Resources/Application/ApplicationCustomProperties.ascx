﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationCustomProperties.ascx.cs" Inherits="Orion_APM_Resources_Application_ApplicationCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="APM_ApplicationCustomProperties"/>
    </Content>
</orion:resourceWrapper>