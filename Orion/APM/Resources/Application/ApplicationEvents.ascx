<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationEvents.ascx.cs" Inherits="Orion_APM_Resources_Application_ApplicationHistory" %>
<%@ Register TagPrefix="orion" TagName="EventList" Src="~/Orion/Controls/EventList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:EventList runat="server" ID="orionEventList" OnInit="grid_Init">
        </orion:EventList>
    </Content>
</orion:resourceWrapper>