using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.APM.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_Summary_ApplicationContainerMembership : ApmAppBaseResource
{
    #region Private methods
    private IEnumerable<Container> GetContainersByApplicationId(int appId)
    {
        return SwisDAL.GetContainersByApplication(appId);
    }
    #endregion

    protected void grid_Init(object sender, EventArgs e)
    {
        grid.DataSource = GetContainersByApplicationId(ApmApplication.Id);
        grid.DataBind();
    }

    #region Properties
    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_121; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceApplicationContainerMembership"; }//should be fixed: no available page about Group Membership
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IApplicationProviderBase) }; }
    }
    #endregion
}
