using System;
using System.Data;
using System.Linq;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Helpers;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using AdHocMapper = SolarWinds.APM.Web.DAL.AdHocMapper;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Application_AllProcessAndServiceMonitors : ApmAppBaseResource
{
    private Dictionary<long, Component> componentsById;
    private List<string> componentsUIDs;

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override string HelpLinkFragment
    {
          get { return this.ResourceHelpLinkFragment("OrionAPMPHAppDetailsProcServMons"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IApplicationProviderBase) }; }
    }

    protected void monitorRepeater_Init(object sender, EventArgs e)
    {
        using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            var data = businessLayer.GetProcessEvidenceComponentsStatisticsForApplication(ApmApplication.Id, ComponentUIDs);
            this.monitorRepeater.DataSource = data;
            if (data.Rows.Count < 1)
            {
                this.Visible = false;
            }
            else
            {
                this.monitorRepeater.DataBind();
            }
        }
    }

    private static T Format<T>(object value, Func<object, T> formatter, Func<T> defaultGetter = null)
    {
        if (value != DBNull.Value)
        {
            return (formatter == null)
                ? (T) value
                : formatter(value);
        }
        return (defaultGetter == null)
            ? default(T)
            : defaultGetter();
    }

    private static T Format<T>(object value, Func<object, T> formatter, T defaultValue)
    {
        return Format(value, formatter, () => defaultValue);
    }

    protected ApmStatus GetComponentStatus(object item)
    {
        var id = (long) ((DataRowView) item)["ComponentID"];
        var c = ComponentsById[id];
        return new ApmStatus(c.Status);
    }

    protected ApmMonitorError GetMonitorError(object item)
    {
        var row = (DataRowView) item;
        var errorCode = Format(row["ComponentErrorCode"], null, (ApmErrorCode) int.MinValue);
        var errorDescription = Format(row["ComponentErrorDescription"], null, string.Empty);
        var statusType = Format(row["ComponentStatusCodeType"], null, StatusCodeType.None);
        var statusValue = Format(row["ComponentStatusCode"], null, int.MinValue);
        var statusDescription = Format(row["ComponentStatusDescription"], null, string.Empty);
        var errorMessage = Format(row["ComponentErrorMessage"], null, string.Empty);
        return new ApmMonitorError(errorCode, errorDescription, statusValue, statusDescription, statusType, errorMessage);
    }

    protected ApmComponentType GetComponentType(object item)
    {
        var id = (long) ((DataRowView) item)["ComponentID"];
        var c = ComponentsById[id];
        return new ApmComponentType(c.Type);
    }

    protected ApmDateTime GetNextPollTime(object item)
    {
        var row = (DataRowView) item;
        var id = (long) row["ComponentID"];
        var c = ComponentsById[id];
        return Format(
            row["TimeStamp"],
            ts => new ApmDateTime((DateTime) ts + c.Application.Frequency),
            () => new ApmDateTime(DateTime.MinValue));
    }

    protected string GetProcessOrServiceName(object item)
    {
        var row = (DataRowView) item;
        return ((ComponentType) row["ComponentType"] == ComponentType.WindowsService)
            ? GetServiceName(row)
            : Format(row["ProcessName"], null, () => GetProcessName(row));
    }

    protected ApmTimeSpan GetElapsedTime(object item)
    {
        var row = (DataRowView) item;
        return Format(
            row["LastTimeUp"],
            lastUp => new ApmTimeSpan(DateTime.UtcNow - (DateTime) lastUp),
            () => new ApmTimeSpan(TimeSpan.Zero)
        );
    }

    private static double GetThresholdValue(object columnValue)
    {
        return ParseHelper.TryParse(InvariantString.ToString(columnValue), double.MaxValue);
    }

    protected enum ColumnKey
    {
        CPU,
        PMem,
        VMem,
        PMemUsed,
        VMemUsed,
        PID,
        LastTimeUp,
        IOPS
    }

    protected ApmPercentage GetPercentValue(object item, ColumnKey key)
    {
        var row = (DataRowView) item;
        var percentage = new ApmPercentage();

        switch (key)
        {
            case ColumnKey.CPU:
                if (row["PercentCPU"] != DBNull.Value)
                {
                    percentage.Value = Convert.ToDouble(row["PercentCPU"]);
                    percentage.Threshold.WarnLevel = GetThresholdValue(row["CpuWarnThreshold"]);
                    percentage.Threshold.CriticalLevel = GetThresholdValue(row["CpuCriticalThreshold"]);
                }
                break;
            case ColumnKey.PMem:
                if (row["PercentMemory"] != DBNull.Value)
                {
                    percentage.Value = Convert.ToDouble(row["PercentMemory"]);
                    percentage.Threshold.WarnLevel = GetThresholdValue(row["PMemWarnThreshold"]);
                    percentage.Threshold.CriticalLevel = GetThresholdValue(row["PMemCriticalThreshold"]);
                }
                break;
            case ColumnKey.VMem:
                if (row["PercentVirtualMemory"] != DBNull.Value)
                {
                    percentage.Value = Convert.ToDouble(row["PercentVirtualMemory"]);
                    percentage.Threshold.WarnLevel = GetThresholdValue(row["VMemWarnThreshold"]);
                    percentage.Threshold.CriticalLevel = GetThresholdValue(row["VMemCriticalThreshold"]);
                }
                break;
        }

        return percentage;
    }

    protected object GetColumnValue(object item, ColumnKey key)
    {

        var row = (DataRowView) item;

        switch (key)
        {
            case ColumnKey.PMemUsed:
                return Format<object>(
                    row["MemoryUsed"],
                    val => new ApmBytes((long)val, ApmBytesBase.Unit.GB, 2),
                    string.Empty
                );
            case ColumnKey.VMemUsed:
                return Format<object>(
                    row["VirtualMemoryUsed"],
                    val => new ApmBytes((long)val, ApmBytesBase.Unit.GB, 2),
                    string.Empty
                );
            case ColumnKey.PID:
                return Format(
                    row["PID"],
                    val => InvariantString.Format("({0})", val),
                    string.Empty
                );
            case ColumnKey.LastTimeUp:
                return Format<object>(
                    row["LastTimeUp"],
                    val => new ApmDateTime((DateTime) val, "Never"),
                    string.Empty
                );
            case ColumnKey.IOPS:
                return Format(
                    row["IOTotalOperationsPerSec"],
                    val => InvariantString.Format(APMWebContent.APMWEBDATA_PAS_IOTOTAL_VALUE_FORMAT, val),
                    () => ApmStatistic.NA
                );
        }
        return string.Empty;
    }

    private string GetServiceName(DataRowView row)
    {
        var componentId = (long) row["ComponentID"];
        var c = ComponentsById.GetValueOrDefault(componentId);
        if (c != null)
        {
            var value = c.Settings.GetValueOrDefault("ServiceName");
            if (value != null)
            {
                return value.Value;
            }
        }
        return string.Empty;
    }

    private string GetProcessName(DataRowView row)
    {
        var componentId = (long) row["ComponentID"];
        var c = ComponentsById.GetValueOrDefault(componentId);
        if (c != null)
        {
            var value = c.Settings.GetValueOrDefault("ProcessName");
            if (value != null)
            {
                return value.Value;
            }
        }
        return string.Empty;
    }

    private static readonly AdHocMapper mapper = new AdHocMapper();

    private static readonly string[] UsedSettings = new string[] { "ServiceName", "ProcessName" };

    private static void LoadComponentSettings(ApmApplicationBase app, IEnumerable<string> keys)
    {
        var ids = app.Components
            .Where(item => item.TemplateId > 0)
            .Where(item => item.Type != ComponentType.SqlConnection && item.Type != ComponentType.SqlQA && item.Type != ComponentType.SqlTable)
            .Select(item => item.TemplateId)
            .Distinct()
            .ToArray();

        if (ids.Length == 0)
        {
            return;
        }

        const string QueryTemplate = @"
SELECT
    cts.ComponentTemplateID AS ID,
    cts.Key,
    cts.Value,
    cts.ValueType,
    cts.Required,
    {0} AS SettingLevel
FROM Orion.APM.ComponentTemplateSetting AS cts
WHERE cts.ComponentTemplateID IN ({1})
    AND cts.Key IN ({2})
ORDER BY cts.ComponentTemplateID";

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var listIds = string.Join(",", ids);
            var listKeys = string.Join(",", keys.Select(k => InvariantString.Format("'{0}'", k.Replace("'", "''"))).ToArray());
            var query = InvariantString.Format(QueryTemplate, (int) SettingLevel.Temporary, listIds, listKeys);
            var data = swis.Query(query);

            if (data.Rows.Count == 0)
            {
                return;
            }

            foreach (var row in data.Select(null, "ID ASC"))
            {
                var key = InvariantString.ToString(row["Key"]);

                if (key.StartsWith(InternalSettings.SystemPrefix, StringComparison.InvariantCultureIgnoreCase))
                    continue;

                var component = app.Components.First(item => object.Equals(item.TemplateId, row["ID"]));
                component.Settings[key] = mapper.ToObject<SettingValue>(row, true);
            }
        }
    }

    protected Dictionary<long, Component> ComponentsById
    {
        get
        {
            if (this.componentsById == null)
            {
                this.componentsById = this.ApmApplication.Components.ToDictionary(c => c.Id);
                LoadComponentSettings(this.ApmApplication, UsedSettings);
            }
            return this.componentsById;
        }
    }

    protected List<string> ComponentUIDs
    {
        get
        {
            if (Resource.Properties.ContainsKey("ComponentUIDs"))
            {
                componentsUIDs = Resource.Properties["ComponentUIDs"]
                    .Trim()
                    .Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToList();
            }
            return componentsUIDs;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_VB1_117; }
    }
}
