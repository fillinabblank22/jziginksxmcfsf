﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDetails.ascx.cs" Inherits="Orion_APM_Resources_Application_ApplicationDetails" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="ApplicationDetails_Management" Src="~/Orion/APM/Controls/Resources/ApplicationDetails_Management.ascx" %>
<%@ Register TagPrefix="apm" TagName="ComponentErrorStatusControl" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>

<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server"/>
        <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="I6" File="APM/js/NodeManagement.js" runat="server" />
        <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />

        <script type="text/javascript">
            $(document).ready(function(){
                SW.APM.TrimStatusDetails();
            });
        </script>

        <div id="popupPanel" style="position: absolute; display: none; z-index: 10; width: 200px;">
            <div style="color: White; background-color: #f99d1c; padding-left: 7px">
                <%= Resources.APMWebContent.APMWEBDATA_VB1_291 %>
            </div>
            <table bgcolor="white" width="200" cellpadding="6" cellspacing="0" border="1" class="biggerPadding">
                <tr>
                    <td id="statusDesc">
                    </td>
                </tr>
            </table>
        </div>
        
        <apm:ApplicationDetails_Management ID="managementControl" runat="server" />

        <table cellspacing="0" class="biggerPadding NeedsZebraStripes">
            <tbody ID="appDetailsSection" runat="server">
                <tr>
                    <td class="PropertyHeader" style=" white-space: nowrap;">
                        <%= Resources.APMWebContent.APMWEBDATA_VB1_299 %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property">
                        <%= string.Format(
                                Resources.APMWebContent.APMWEBCODE_VB1_3,
                                string.Format(
                                    "<a href=\"{0}\">{1}</a>",
                                    GetViewLink(ApmApplication.NetObjectID),
                                    HttpUtility.HtmlEncode(ApmApplication.Name)
                                ),
                                string.Format(
                                    "<a href=\"{0}\">{1}</a>",
                                    GetViewLink(ApmApplication.NPMNode.NetObjectID),
                                    HttpUtility.HtmlEncode(ApmApplication.NodeName)
                                )
                            )
                        %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader" style=" white-space: nowrap;">
                        <%= Resources.APMWebContent.APMWEBDATA_VB1_300 %>
                    </td>
                    <td class="StatusColumn">
                        <apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server" />
                    </td>
                    <td class="Property">
                        <%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_301, ApmApplication.Status.ToLocalizedString()) %>
                        <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader" style=" white-space: nowrap;">
                        <%= Resources.APMWebContent.APMWEBDATA_VB1_302 %>
                    </td>
                    <td class="StatusColumn">
                        <orion:SmallNodeStatus ID="ServerStatusIcon" runat="server" />
                    </td>
                    <td class="Property">
                        <a href="<%=GetViewLink(ApmApplication.NPMNode.NetObjectID) %>">
                            <%= string.Format(Resources.APMWebContent.APMWEBDATA_VB1_303, ApmApplication.NPMNode.Status.ParentStatus.ToLocalizedString()) %>
                        </a>
                    </td>
                </tr>
            </tbody>
            
        </table>
        <apm:ComponentErrorStatusControl ID="componentErrorStatusControl" runat="server" />
        <asp:Label runat="server" ID="lbWarning" ForeColor="red" Font-Size="8"><%= WarningMessage %></asp:Label>
    </Content>
</orion:resourceWrapper>
