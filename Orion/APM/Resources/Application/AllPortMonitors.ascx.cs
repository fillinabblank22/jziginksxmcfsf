using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common;
using System.Collections.Generic;
using SolarWinds.APM.Web.Utility;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Application_AllPortMonitors : ApmAppBaseResource
{
	private Dictionary<long, Component> componentsById;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
	}

	public override string EditURL 
	{ 
		get { return base.EditURL; } 
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHAppDetailsPortMons"; }
	}

	protected void monitorRepeater_Init(object sender, EventArgs e)
	{
		DataTable data;
		using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
			data = bl.GetPortEvidenceComponentsStatisticsForApplication(ApmApplication.Id);
		}
		PrepareDataBeforePostingOnWeb(data);

		Visible = data.Rows.Count > 0;
		if (Visible)
		{
			monitorRepeater.DataSource = data;
			monitorRepeater.DataBind();
		}
	}

	protected void monitorRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			DataRowView row = (DataRowView)e.Item.DataItem;
			if (row["ColumnLabel"] != DBNull.Value)
			{
				// Multivalue row
				HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdMessage");
				td.ColSpan = 5;

				td = (HtmlTableCell)e.Item.FindControl("tdComponentName");
				td.Style.Add(HtmlTextWriterStyle.PaddingLeft, "40px");
				td.InnerHtml = (string)row["ColumnLabel"];

				e.Item.FindControl("tdResponceTime").Visible = false;
				e.Item.FindControl("PercentBar1").Visible = false;
                e.Item.FindControl("tdSpacer").Visible = false;
                e.Item.FindControl("tdPort").Visible = false;
			}

			if (row["StatisticData"] != DBNull.Value)
			{
				HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdStatisticData");

				FormatStatisticDataOutputByTresholds(e, row, td);
			}
		}
	}

	private void FormatStatisticDataOutputByTresholds(RepeaterItemEventArgs e, DataRowView row, HtmlTableCell td)
	{
		if (row["ThresholdOperator"] != DBNull.Value)
		{
			double value = (double)row["StatisticData"];
			double warnLevel = ApmThresholdHelper.GetThresholdValue(row["StatisticWarnThreshold"]);
			double criticalLevel = ApmThresholdHelper.GetThresholdValue(row["StatisticCriticalThreshold"]);

			ThresholdOperator thresholdOperator = (ThresholdOperator)row["ThresholdOperator"];

			Threshold threshold = new Threshold(warnLevel, criticalLevel, thresholdOperator);

			SolarWinds.APM.Common.Models.Status componentStatus = threshold.CalculateStatus(value);

			if (componentStatus == SolarWinds.APM.Common.Models.Status.Warning)
			{
				FormatWarningLevelOutput(e, td);
			}
			else if (componentStatus == SolarWinds.APM.Common.Models.Status.Critical)
			{
				FormatCriticalLevelOutput(e, td);
			}
		}
	}

	private string FormatCriticalLevelOutput(RepeaterItemEventArgs e, HtmlTableCell td)
	{
		return td.InnerHtml = "<font color=\"#FF0000\"><b>" + GetStatisticData(e.Item.DataItem, "statisticdata") + "</b></font>";
	}

	private void FormatWarningLevelOutput(RepeaterItemEventArgs e, HtmlTableCell td)
	{
		td.InnerHtml = "<font color=\"#FF0000\">" + GetStatisticData(e.Item.DataItem, "statisticdata") + "</font>";
	}

	protected void PrepareDataBeforePostingOnWeb(DataTable dataTable)
	{
        foreach (var row in dataTable.Select("ComponentStatus NOT IN (1, 5, 6) AND ComponentErrorCode <> 0")) 
		{
			if (row["StatisticMessage"] != DBNull.Value) 
			{
				row["StatisticMessage"] = DBNull.Value;
			}
		}

		string[] componentTypes = {
			((int)ComponentType.WindowsScript).ToString(), ((int)ComponentType.LinuxScript).ToString(), 
			((int)ComponentType.WindowsPowerShell).ToString(), ((int)ComponentType.NagiosScript).ToString() 
		};

		string expresion = "ComponentType IN (" + string.Join(", ", componentTypes) + ")";

		var foundRows = dataTable.Select(expresion).ToList<DataRow>();
		if (foundRows.Count > 0) 
		{
            dataTable.DefaultView.Sort = "ComponentID ASC, ColumnLabel ASC";
			while (foundRows.Count > 0)
			{
				if (foundRows[0]["ColumnLabel"] != DBNull.Value)
				{
					// Manufacturing of header row for multivalue component monitors
					DataRow headerRow = dataTable.NewRow();
					headerRow.ItemArray = foundRows[0].ItemArray;
					headerRow["ColumnLabel"] = DBNull.Value;
					headerRow["StatisticData"] = DBNull.Value;
					headerRow["StatisticMessage"] = DBNull.Value;

					int index = dataTable.Rows.IndexOf(foundRows[0]);
					dataTable.Rows.InsertAt(headerRow, index);
				}

				// Removing component already done from the list.
				long componentID = (long)foundRows[0]["ComponentID"];
				List<DataRow> subSelect = foundRows.FindAll(item => (long)item["ComponentID"] == componentID);

				for (int i = 0; i < subSelect.Count; i++)
				{
					if (subSelect[i]["IsDisabled"] != DBNull.Value && (bool)subSelect[i]["IsDisabled"])
					{
						dataTable.Rows.Remove(subSelect[i]);
					}

					foundRows.Remove(subSelect[i]);
				}
			}
			dataTable.AcceptChanges();
		}
	}

	protected ApmStatus GetComponentStatus(object item)
	{
		long id = (long)((DataRowView)item)["ComponentID"];
		Component c = ComponentsById[id];
		return new ApmStatus(c.Status);
	}


	protected ApmComponentType GetComponentType(object item)
	{
		long id = (long)((DataRowView)item)["ComponentID"];
		Component c = ComponentsById[id];
		return new ApmComponentType(c.Type);
	}

	protected ApmMonitorError GetMonitorError(object item)
	{
		DataRowView row = (DataRowView)item;
		ApmErrorCode errorCode = (ApmErrorCode)int.MinValue;
		string errorDescription = string.Empty;
		StatusCodeType statusType = StatusCodeType.None;
		int statusValue = int.MinValue;
		string statusDescription = string.Empty;
		string errorMessage = string.Empty;
		if (row["ComponentErrorCode"] != DBNull.Value)
		{
			errorCode = (ApmErrorCode)row["ComponentErrorCode"];
		}
		if (row["ComponentErrorDescription"] != DBNull.Value)
		{
			errorDescription = row["ComponentErrorDescription"] as string;
		}
		if (row["ComponentStatusCodeType"] != DBNull.Value)
		{
			statusType = (StatusCodeType)row["ComponentStatusCodeType"];
		}
		if (row["ComponentStatusCode"] != DBNull.Value)
		{
			statusValue = (int)row["ComponentStatusCode"];
		}
		if (row["ComponentStatusDescription"] != DBNull.Value)
		{
			statusDescription = row["ComponentStatusDescription"] as string;
		}
		if (row["ComponentErrorMessage"] != DBNull.Value)
		{
			errorMessage = (string)row["ComponentErrorMessage"];
		}
		return new ApmMonitorError(errorCode, errorDescription, statusValue, statusDescription, statusType, errorMessage);
	}

	protected ApmDateTime GetNextPollTime(object item)
	{
		DataRowView row = (DataRowView)item;
		long id = (long)row["ComponentID"];
		Component c = ComponentsById[id];
		DateTime timeStamp = new DateTime();

		if (row["TimeStamp"] != DBNull.Value)
		{
			timeStamp = (DateTime)row["TimeStamp"];
			return new ApmDateTime(timeStamp + c.Application.Frequency);
		}
		return new ApmDateTime(DateTime.MinValue);
	}

	protected ApmTimeSpan GetElapsedTime(object item)
	{
		DateTime lastUp;
		DataRowView row = (DataRowView)item;
		if (row["LastTimeUp"] != DBNull.Value)
		{
			lastUp = (DateTime)row["LastTimeUp"];
			return new ApmTimeSpan(DateTime.UtcNow - lastUp);
		}
		else
		{
			return new ApmTimeSpan(TimeSpan.Zero);
		}
	}


	protected ApmResponseTime GetResponseTime(object item, string columnKey)
	{
		if (string.IsNullOrEmpty(columnKey))
			return ApmResponseTime.Empty;

		DataRowView row = (DataRowView)item;

		ApmResponseTime responseTime = ApmResponseTime.Empty;

		if (columnKey.ToLowerInvariant() == "responsetime" && row["ResponseTime"] != DBNull.Value)
		{
			responseTime.Value = (long)row["ResponseTime"];
			responseTime.Threshold.WarnLevel = ApmThresholdHelper.GetThresholdValue(row["ResponseTimeWarnThreshold"]);
			responseTime.Threshold.CriticalLevel = ApmThresholdHelper.GetThresholdValue(row["ResponseTimeCriticalThreshold"]);
		}
		return responseTime;
	}

	protected string GetKeyName(object item)
	{
		DataRowView row = (DataRowView)item;

		return row["ColumnLabel"] == DBNull.Value ? "ComponentName" : "ColumnLabel";
	}
	protected ApmStatisticData GetStatisticData(object item, string columnKey)
	{
		if (string.IsNullOrEmpty(columnKey))
			return ApmStatisticData.Empty;

		DataRowView row = (DataRowView)item;

		ApmStatisticData statistic;

		statistic = row["ColumnLabel"] != DBNull.Value ? ApmStatisticData.Empty : null;

		if (columnKey.ToLowerInvariant() == "statisticdata" && row["StatisticData"] != DBNull.Value)
		{
			statistic = ApmStatisticData.Empty;
			statistic.Value = (double)row["StatisticData"];
			statistic.Threshold.WarnLevel = ApmThresholdHelper.GetThresholdValue(row["StatisticWarnThreshold"]);
			statistic.Threshold.CriticalLevel = ApmThresholdHelper.GetThresholdValue(row["StatisticCriticalThreshold"]);
		}
		return statistic;
	}

	protected string GetStatisticMessage(object item, string columnKey)
	{
		if (string.IsNullOrEmpty(columnKey))
			return string.Empty;

		DataRowView row = (DataRowView)item;

		string statisticMessage = " ";

		if (columnKey.ToLowerInvariant() == "statisticmessage" && row["StatisticMessage"] != DBNull.Value)
		{
			statisticMessage = row["StatisticMessage"].ToString();
		}
		return statisticMessage;
	}

	protected object GetColumnValue(object item, string columnKey)
	{
		if (string.IsNullOrEmpty(columnKey))
			return "";

		DataRowView row = (DataRowView)item;

		switch (columnKey.ToLowerInvariant())
		{
			case "port":
				if ((row["Port"] != DBNull.Value) && ((int)row["Port"] != 0))
					return (int)row["Port"];
				break;
			case "lasttimeup":
				if (row["LastTimeUp"] != DBNull.Value)
					return new ApmDateTime((DateTime)row["LastTimeUp"], "Never");
				break;
		}
		return ApmStatistic.NA;
	}

	protected Dictionary<long, Component> ComponentsById
	{
		get
		{
			if (componentsById == null)
			{
				componentsById = new Dictionary<long, Component>();
				foreach (Component c in ApmApplication.Components)
				{
					componentsById.Add(c.Id, c);
				}
			}
			return componentsById;
		}
	}

	protected override string DefaultTitle
	{
        get { return Resources.APMWebContent.APMWEBCODE_AK1_1; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
