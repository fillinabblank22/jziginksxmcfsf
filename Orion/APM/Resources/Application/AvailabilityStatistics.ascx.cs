﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Application_AvailabilityStatistics : BaseResourceControl
{
	#region Event Handlers
	
	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		ApmApplication = GetInterfaceInstance<IApplicationProviderBase>().ApmApplication;
        if (ApmApplication == null)
		{
			return;
		}

        ApmBaseResource.EnsureApmScripts();

        Dictionary<string, string> periods = new Dictionary<string, string>() { {"Today", Resources.APMWebContent.TimePeriod_today}, {"Yesterday", Resources.APMWebContent.TimePeriod_yesterday}, {"Last 7 Days", Resources.APMWebContent.TimePeriod_last_7_days}, {"Last 30 Days", Resources.APMWebContent.TimePeriod_last_30_days}, {"This Month", Resources.APMWebContent.TimePeriod_this_months}, {"Last Month", Resources.APMWebContent.TimePeriod_last_month}, {"This Year", Resources.APMWebContent.TimePeriod_this_year} };

		DataTable statTable = new DataTable();
        statTable.Columns.Add(new DataColumn("DisplayPeriod", typeof(string)));
		statTable.Columns.Add(new DataColumn("Period", typeof(string)));
		statTable.Columns.Add(new DataColumn("SampleSize", typeof(string)));
		statTable.Columns.Add(new DataColumn("Availability", typeof(string)));

        foreach (KeyValuePair<string, string> period in periods)
		{
			DateTime periodStart = new DateTime(), periodEnd = new DateTime();
			string periodName = period.Key, sampleName = "1H";
			int sampleSize = 60;
			bool isCustom = false;
			Periods.Parse(ref periodName, ref periodStart, ref periodEnd, ref sampleSize, ref sampleName, ref isCustom);

			DataTable periodStat = SwisDAL.GetApplicationAvailability(periodStart, periodEnd, ApmApplication.Id);

			if (periodStat.Rows.Count > 0)
			{
				statTable.Rows.Add(period.Value, period.Key, sampleName, String.Format("{0:#0.000} %", periodStat.Rows[0]["PercentAvailability"]));
			}
		}

		this.AvailabilityTable.DataSource = statTable;
		this.AvailabilityTable.DataBind();
	}

	#endregion 

	#region Properties

	protected ApmApplicationBase ApmApplication { get; private set; }

	protected override string DefaultTitle => Resources.APMWebContent.APMWEBCODE_VB1_126;

    public override string HelpLinkFragment => "OrionAPMPHAppDetailsAvailabilityStatistics";

    public override string DetachURL
	{
		get
		{
			if (this.ApmApplication == null) 
			{
				return "#";
			}
			return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&NetObject=AA:{1}", this.Resource.ID, this.ApmApplication.Id);
		}
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces => new Type[] { typeof(IApplicationProviderBase) };

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    #endregion

	#region Internal Helper Members

	protected String GetCustomChartUrl(Object data)
	{
		var app = this.ApmApplication;
		if (app == null)
		{
			return "#";
		}

		var url = new UrlBuilder("/Orion/APM/CustomChart.aspx");

		url[ChartInfo.KEYS.NetObject] = app.NetObjectID;

		url[ChartInfo.KEYS.ChartName] = ChartInfo.TYPES.AppAvail;
		url[ChartInfo.KEYS.Title] = app.Name;
		url[ChartInfo.KEYS.SubTitle] = this.DisplayTitle;

		url[ChartInfo.KEYS.Width] = ChartInfo.DefaultChartWidth.ToString();
		url[ChartInfo.KEYS.Height] = "0";
		url[ChartInfo.KEYS.FontSize] = "1";

		DataRowView row = data as DataRowView;
		url[ChartInfo.KEYS.Period] = row["Period"].ToString();
		url[ChartInfo.KEYS.SampleSize] = row["SampleSize"].ToString();

		return url.Url;
	}

	#endregion
}
