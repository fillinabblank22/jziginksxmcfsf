﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Application_ApplicationDependencies : ApmAppBaseResource
{
    protected string ApplicationNetObjectId => ApmApplication.NetObjectID;

    protected bool AutoHide
    {
        get
        {
            if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
                return false;
            return Resource.Properties["AutoHide"].Equals("1");
        }
    }

    protected string Filter
    {
        get
        {
            if (string.IsNullOrEmpty(Resource.Properties["Filter"]))
                return string.Empty;
            return Resource.Properties["Filter"];
        }
    }

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IApplicationProviderBase) };

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    protected override void OnInit(EventArgs e)
    {
        Wrapper.ManageButtonImage = "/Orion/Images/Button.Manage.Dependencies.gif";
        Wrapper.ManageButtonTarget = "/Orion/Admin/DependenciesView.aspx";
        Wrapper.ShowManageButton = ApmRoleAccessor.AllowAdmin || OrionConfiguration.IsDemoServer;

        DependencyCache.UpdateDependencyCache(ApplicationNetObjectId,
            string.IsNullOrEmpty(Resource.Properties["ParentFilter"]) ? string.Empty : Resource.Properties["ParentFilter"],
            string.IsNullOrEmpty(Resource.Properties["ChildFilter"]) ? string.Empty : Resource.Properties["ChildFilter"],
            string.IsNullOrEmpty(Resource.Properties["DependencyFilter"]) ? string.Empty : Resource.Properties["DependencyFilter"]);

        base.OnInit(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        // hide resource if there aren't any data
        if (string.IsNullOrEmpty(Filter) && AutoHide)
        {
            bool allowInterfaces = new RemoteFeatureManager().AllowInterfaces;
            using (var dal = new DependenciesDAL())
            {
                if (dal.GetDependenciesCount(ApmApplication.Id, "Orion.APM.Application", allowInterfaces, Filter) == 0) Wrapper.Visible = false;
            }
        }

        base.OnPreRender(e);
    }

    protected override string DefaultTitle => APMWebContent.APMWEBCODE_VB1_122;

    public override string HelpLinkFragment => "OrionAPMPHAppDetailsApplicationDependencies";

    public override string EditControlLocation => "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx";
}
