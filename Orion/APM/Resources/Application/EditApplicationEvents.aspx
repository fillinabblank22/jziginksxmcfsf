<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
    CodeFile="EditApplicationEvents.aspx.cs" Inherits="ApplicationTemplates" %>

<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        <%= Resources.APMWebContent.APMWEBDATA_VB1_305%>
    </h1>
    <b><%= Resources.APMWebContent.APMWEBDATA_VB1_306 %></b><br/><br/>
    <asp:TextBox runat="server" ID="tbMaxEvents" Text="10"/>
       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbMaxEvents" SetFocusOnError="true">*</asp:RequiredFieldValidator>
       <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="tbMaxEvents"
                                Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="1"
                                ErrorMessage="<%$ Resources : APMWebContent , APMWEBDATA_VB1_307 %>"
                                SetFocusOnError="true"></asp:RangeValidator>
    <br />
    
    <br />
    <b><%= Resources.APMWebContent.APMWEBDATA_VB1_308 %></b><br /><br/>
    <asp:DropDownList runat="server" ID="timePeriod" style="width: auto" >
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_past_hour %>" Value="0" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_last_2_hours %>" Value="1" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_last_24_hours %>" Value="2" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_today %>" Value="3" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_yesterday %>" Value="4" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_last_7_days %>" Value="5" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_this_months %>" Value="6" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_last_month %>" Value="7" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_last_30_days %>" Value="8" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_last_2_months %>" Value="9" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_this_year %>" Value="10" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_last_12_months %>" Value="11" />
        <asp:ListItem Text="<%$ Resources : APMWebContent , TimePeriod_all_time %>" Value="12"  />
    </asp:DropDownList><br />
    <br />
    <orion:LocalizableButton runat="server" ID="btnSubmit" CausesValidation="true" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
</asp:Content>
