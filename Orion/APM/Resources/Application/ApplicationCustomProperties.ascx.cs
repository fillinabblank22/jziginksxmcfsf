﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Application_ApplicationCustomProperties : ApmBaseResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        this.customPropertyList.Resource = this.Resource;
        var application = GetInterfaceInstance<IApplicationProviderBase>().ApmApplication;
        if (application != null)
        {
            customPropertyList.NetObjectId = application.Id;
        }

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            var allProperties = new SolarWinds.APM.Web.DAL.CustomPropertiesDAL().GetCustomProperties("Application", netObjectId);
            IEnumerable<CustomPropertyInfo> requiredProperties = allProperties;
            if (displayProperties.Count > 0)
            {
                requiredProperties = from dp in displayProperties
                                     join ap in allProperties on dp equals ap.PropertyName
                                     select ap;
            }
            return requiredProperties.ToDictionary(p => p.PropertyName, p => GetPropertyValue(p));
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/APM/Admin/Edit/EditApplication.aspx?id={0}&ReturnTo={1}", netObjectId, redirectUrl);
        };

    }

    private object GetPropertyValue(CustomPropertyInfo property)
    {
        object value = string.Empty;
        switch (property.PropertyType)
        {
            case "System.Single":
                float floatVal;
                if (Single.TryParse(property.PropertyValue, out floatVal))
                    value = floatVal;
                break;
            case "System.DateTime":
                DateTime dateVal;
                if (DateTime.TryParse(property.PropertyValue, out dateVal))
                    value = dateVal;
                break;
            case "System.Boolean":
                bool boolVal;
                if (bool.TryParse(property.PropertyValue, out boolVal))
                    value = boolVal;
                break;
            default:
                value = property.PropertyValue;
                break;
        }
        return value;
    }

    protected override string DefaultTitle => APMWebContent.APM_Web_Aplication_Custom_Properties_Title;

    public override string EditControlLocation => "/Orion/APM/Controls/EditResourceControls/EditApplicationCustomProperties.ascx";

    public override string HelpLinkFragment => "OrionAPMAGCustomProperties";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public override IEnumerable<Type> RequiredInterfaces => new Type[] { typeof(IApplicationProviderBase) };
}
