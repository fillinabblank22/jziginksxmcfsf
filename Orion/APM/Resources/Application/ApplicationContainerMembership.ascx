<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationContainerMembership.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_ApplicationContainerMembership" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Containers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater ID="grid" runat="server" OnInit="grid_Init">
            <HeaderTemplate>
                <table class="DataGrid NeedsZebraStripes">
                    <thead>
                        <tr>
                            <td><%= Resources.APMWebContent.APMWEBDATA_VB1_290 %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr <%#((Container.ItemIndex%2==0) ? "" : "class=\"ZebraStripe\"")%>>
                    <td>
                        <orion:EntityStatusIcon runat="server" Entity="<%#((Container)Container.DataItem).Entity%>" Status="<%#((Container)Container.DataItem).Status.StatusId%>" />
                        <a href="<%#((Container)Container.DataItem).DetailsUrl%>">
                            <%#UIHelper.Escape(((Container)Container.DataItem).Name)%></a>                         
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>