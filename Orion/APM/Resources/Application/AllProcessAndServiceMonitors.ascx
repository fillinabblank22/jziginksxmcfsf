<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllProcessAndServiceMonitors.ascx.cs" Inherits="Orion_APM_Resources_Application_AllProcessAndServiceMonitors" %>
<%@ Register TagPrefix="apm" TagName="SmallApmStatusIcon" Src="~/Orion/APM/Controls/SmallApmStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="PercentBar" Src="~/Orion/APM/Controls/PercentBar.ascx" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I2" File="js/jquery/jquery.tablesorter.min.js" runat="server" />
        <style type="text/css">
            .ReportHeader.center {
                text-align: center;
            }
            table.tablesorter thead tr th.header {
                padding: 2px;
                width: auto;
                cursor: pointer;
                text-align: left;
            }
            table.tablesorter thead tr th.header img {
                padding-left: 5px;
            }
            table.tablesorter tr td.apm_InlineBar {
                width: 10%;
                min-width: 35px;
                padding-left: 2px;
            }
            table.tablesorter tr td.apm_cmpNameColumn {
                width: 20%;
            }
            table.tablesorter thead tr th.processNameCol,
            table.tablesorter tr td.processNameCol {
                word-break: break-all;
                width: 50%;
                min-width: 50px;
            }
            table.tablesorter tbody td:first-child {
                padding-left: 0;
            }
            table.tablesorter tbody td {
                padding-left: 5px;
            }
            table.tablesorter tbody td.apm_PercentColumn {
                white-space: nowrap;
            }
            table.tablesorter tbody td.iops {
                text-align: right;
            }
        </style>
<script type="text/javascript">
$(document).ready(function () {

    var $tab, $img,
        iconUrl = {
            down: '/Orion/images/Arrows/Arrow_Ascending.png',
            up: '/Orion/images/Arrows/Arrow_Descending.png'
        };

    function extract(el) {
        var $el, val, text;

        $el = $(el);
        val = $el.attr('apm-ts');

        if (!val) {
            $el = $el.next('td');
            if ($el && $el.length) {
                return this.textExtraction($el);
            }
            return '';
        }

        if (val === 'a') {
            return $.trim($el.find(val).text());
        }

        text = $.trim($el.text());
        if (val === 'num' || val === 'num-per') {
            text = text.replace(',', '.');
        }
        if (val === 'num-per') {
            if (text.indexOf('/') > 0) {
                text = $.trim(text.split('/')[0]);
            }
        }
        return text;
    }

    $tab = $('#apmProcSvcRes<%= Resource.ID %>');
    $img = $('<img>');

    $tab.tablesorter({
        sortList: [[0, 0]],
        textExtraction: extract
    });

    $tab.bind('sortEnd', function () {

        function tryUpdateImg(selector, url) {
            var $hdr = $tab.find(selector);
            if ($hdr.length) {
                $img.appendTo($hdr);
                $img.attr('src', url);
                return true;
            }
        }

        return tryUpdateImg('.headerSortDown', iconUrl.down)
            || tryUpdateImg('.headerSortUp', iconUrl.up)
            || $img.detach();
    });
});
</script>
        <asp:Repeater ID="monitorRepeater" runat="server" OnLoad="monitorRepeater_Init" >
            <HeaderTemplate>
                <table id="apmProcSvcRes<%= Resource.ID %>" class="tablesorter DataGrid apm_NeedsZebraStripes">
                    <thead>
                        <tr>
                            <td class="ReportHeader" colspan="5">&nbsp;</td>
                            <td class="ReportHeader center" colspan="4"><%= Resources.APMWebContent.APMWEBDATA_VB1_281 %></td>
                            <td class="ReportHeader" colspan="1">&nbsp;</td>
                        </tr>
                        <tr>
                            <th class="ReportHeader" colspan="2"><%= Resources.APMWebContent.APMWEBDATA_VB1_282 %></th>
                            <th class="ReportHeader processNameCol" colspan="1"><%= Resources.APMWebContent.APMWEBDATA_VB1_283 %></th>
                            <th class="ReportHeader" colspan="2"><%= Resources.APMWebContent.APMWEBDATA_VB1_284 %></th>
                            <th class="ReportHeader" colspan="2"><%= Resources.APMWebContent.APMWEBDATA_VB1_285 %></th>
                            <th class="ReportHeader" colspan="2"><%= Resources.APMWebContent.APMWEBDATA_VB1_286 %></th>
                            <th class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_PAS_IOTOTAL_HEADER %></th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="zebraRow">
                    <td>
                        <apm:SmallApmStatusIcon ID="MonStatusIcon" runat="server"
                            StatusValue="<%# GetComponentStatus(Container.DataItem)%>"
                            ErrorValue="<%# GetMonitorError(Container.DataItem)%>" />
                    </td>
                    <td apm-ts="a" class="apm_cmpNameColumn">
                        <a href="<%# GetViewLink(Eval("ComponentID", "AM:{0}")) %>">
                            <%# Eval("ComponentName") %>
                        </a>
                    </td>
                    <td apm-ts="*" class="processNameCol">
                        <%# GetProcessOrServiceName(Container.DataItem) %>&nbsp;<%# GetColumnValue(Container.DataItem, ColumnKey.PID) %>
                    </td>
                    <td apm-ts="num" class="apm_PercentColumn">
                        <%# GetPercentValue(Container.DataItem, ColumnKey.CPU) %>
                    </td>
                    <apm:PercentBar ID="PercentBar1" runat="server" Value="<%# GetPercentValue(Container.DataItem, ColumnKey.CPU) %>" />
                    <td apm-ts="num" class="apm_PercentColumn">
                        <%# GetPercentValue(Container.DataItem, ColumnKey.PMem) %>
                    </td>
                    <apm:PercentBar ID="PercentBar2" runat="server" Value="<%# GetPercentValue(Container.DataItem, ColumnKey.PMem) %>" />
                    <td apm-ts="num" class="apm_PercentColumn">
                        <%# GetPercentValue(Container.DataItem, ColumnKey.VMem) %>
                    </td>
                    <apm:PercentBar ID="PercentBar3" runat="server" Value="<%# GetPercentValue(Container.DataItem, ColumnKey.VMem) %>" />
                    <td apm-ts="num-per" class="iops">
                        <%# GetColumnValue(Container.DataItem, ColumnKey.IOPS) %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
