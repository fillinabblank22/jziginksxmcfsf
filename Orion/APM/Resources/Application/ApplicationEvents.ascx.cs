using System;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_APM_Resources_Application_ApplicationHistory : ApmAppBaseResource
{
    private string subTitle = Resources.APMWebContent.TimePeriod_today;
    private int maxEventCount = 25;

    public override string DisplayTitle => Title.Replace("XX", maxEventCount.ToString());

    protected override string DefaultTitle => Resources.APMWebContent.APMWEBCODE_VB1_125;

    public override string HelpLinkFragment => "OrionAPMPHAppDetailsAppEvents";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public override string SubTitle => subTitle;

    public override string EditURL
	{
		get
		{
			if (ApmApplication != null)
			{
				return String.Format(
					"/Orion/APM/Resources/Application/EditApplicationEvents.aspx?ResourceID={0}&NetObject={1}&ReturnURL={2}",
					Resource.ID, ApmApplication.NetObjectID, SolarWinds.Orion.Web.Helpers.UrlHelper.ToSafeUrlParameter(Page.Request.Url.PathAndQuery)
				);
			}
			return base.EditURL;
		}
	}

    protected void grid_Init(object sender, EventArgs e)
    {
        DateTime startDate;
        DateTime endDate;
        ParsePeriodLimitaion((TimePeriod)GetIntProperty("Period", 3), out startDate, out endDate);

        maxEventCount = GetIntProperty("MaxEvents", maxEventCount);
        string topXX = (maxEventCount >= 0 ? string.Format(" TOP {0} ", maxEventCount) : string.Empty);

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            orionEventList.DataSource = businessLayer.GetEventsForApplication(ApmApplication.Id, topXX, startDate, endDate);
            orionEventList.DataBind();
        }
    }

    private void ParsePeriodLimitaion(TimePeriod timePeriod, out DateTime start, out DateTime end)
    {
        switch (timePeriod)
        {
            case TimePeriod.PastHour:
                start = DateTime.Now.AddHours(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_past_hour;
                break;

            case TimePeriod.LastMonth:
                start = DateTime.Now.AddMonths(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_month;
                break;

            case TimePeriod.Last12Months:
                start = DateTime.Now.AddYears(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_12_months;
                break;

            case TimePeriod.Last24Hours:
                start = DateTime.Now.AddDays(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_24_hours;
                break;

            case TimePeriod.Last2Hours:
                start = DateTime.Now.AddHours(-2);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_2_hours;
                break;

            case TimePeriod.Last2Months:
                start = DateTime.Now.AddMonths(-2);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_2_months;
                break;

            case TimePeriod.Last30Days:
                start = DateTime.Now.AddDays(-30);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_30_days;
                break;

            case TimePeriod.Last7Days:
                start = DateTime.Now.AddDays(-7);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_7_days;
                break;

            case TimePeriod.ThisMonth:
                end = DateTime.Now;
                start = new DateTime(end.Year, end.Month, 1);
                subTitle = Resources.APMWebContent.TimePeriod_this_months;
                break;

            case TimePeriod.ThisYear:
                end = DateTime.Now;
                start = new DateTime(end.Year, 1, 1);
                subTitle = Resources.APMWebContent.TimePeriod_this_year;
                break;

            case TimePeriod.Today:
                end = DateTime.Now;
                start = new DateTime(end.Year, end.Month, end.Day);
                subTitle = Resources.APMWebContent.TimePeriod_today;
                break;

            case TimePeriod.Yesterday:
                start = DateTime.Now.Date;
                end = new DateTime(start.Year, start.Month, start.Day);
                start = DateTime.Now.Date.AddDays(-1);
                subTitle = Resources.APMWebContent.TimePeriod_yesterday;
                break;
            case TimePeriod.Custom:
                end = DateTime.Now;
                start = new DateTime(1900, 1, 1);
                subTitle = Resources.APMWebContent.TimePeriod_all_time;
                break;

            default:
                end = DateTime.Now;
                start = new DateTime(end.Year, end.Month, end.Day);
                break;
        }
    }
}





