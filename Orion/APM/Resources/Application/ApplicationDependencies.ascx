﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDependencies.ascx.cs" Inherits="Orion_APM_Resources_Application_ApplicationDependencies" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<asp:ScriptManagerProxy id="DepTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/Services/DependenciesTree.asmx" />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="/Orion/NetPerfMon/js/Dependencies.js" />
	</Scripts>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
        	//<![CDATA[
        	$(function () {
        		var refresh = function () {
        			ORIONDependencies.DependenciesTree.LoadDependencies($('#<%=DepTree.ClientID%>'), '<%=Resource.ID %>', '<%= ApplicationNetObjectId %>');
	    		};
	    		SW.Core.View.AddOnRefresh(refresh, '<%= DepTree.ClientID %>');
	    		refresh();
	    	});
	    	//]]>
	    </script>
    
        <div class="Tree" ID="DepTree" runat="server">
            <asp:Literal runat="server" ID="TreeLiteral" />
        </div>
    </Content>
</orion:resourceWrapper>