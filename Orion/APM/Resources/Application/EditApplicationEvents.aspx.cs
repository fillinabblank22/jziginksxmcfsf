using System;
using System.Data;
using System.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;

public partial class ApplicationTemplates : System.Web.UI.Page
{
    private string resID;

    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get
        {
            if (_resource == null)
            {
                Int32 id;
                if (Int32.TryParse(Request.QueryString["ResourceID"], out id))
                {
                    _resource = ResourceManager.GetResourceByID(id);
                }
            }
            return _resource;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            resID = Request.QueryString["ResourceID"];
            
            DataTable dt = new DataTable();
            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                dt = businessLayer.GetResourceProperties(int.Parse(resID));
            }

            int period = 3; //Today
            string maxEvents = "25";
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
					if ("Period".Equals(dr["PropertyName"]))
                    {
                        period = int.Parse(Convert.ToString(dr["PropertyValue"]));
                    }
					else if ("MaxEvents".Equals(dr["PropertyName"]))
                    {
                        maxEvents = HttpUtility.HtmlEncode(Convert.ToString(dr["PropertyValue"]));
                    }
                }
            }

            timePeriod.SelectedIndex = period;
            tbMaxEvents.Text = maxEvents;
        }
        btnSubmit.Focus();
    }
    
    protected void SubmitClick(object sender, EventArgs e)
    {
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            businessLayer.UpdateEventsResourceProperties(int.Parse(Request.QueryString["ResourceID"]), timePeriod.SelectedIndex.ToString(), tbMaxEvents.Text);
        }

        string originalReturnUrl = SolarWinds.Orion.Web.Helpers.UrlHelper.FromSafeUrlParameter(Request.QueryString["ReturnUrl"]);
        string url = GetViewUrl(this.Resource.View.ViewID, GetOriginalNetObject(originalReturnUrl));
        Response.Redirect(url);
    }

    private string GetOriginalNetObject(string returnUrl)
    {
        var uri = new Uri(Page.Request.Url, returnUrl);
        var collection = System.Web.HttpUtility.ParseQueryString(uri.Query);
        return collection["NetObject"];
    }

    private string GetViewUrl(int viewId, string netObjectId)
    {
        string url = string.Format("/Orion/View.aspx?ViewID={0}", viewId);
        if (!string.IsNullOrEmpty(netObjectId))
            url = string.Format("{0}&NetObject={1}", url, netObjectId);
        return url;
    }
}
