﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvailabilityStatistics.ascx.cs" Inherits="Orion_APM_Resources_Application_AvailabilityStatistics" %>

<orion:resourceWrapper runat="server">
	<Content>
		<asp:Repeater runat="server" ID="AvailabilityTable">
			<HeaderTemplate>
				<table border="0" cellpadding="2" cellspacing="0" width="100%">
					<tr>
						<td class="ReportHeader">
							<%= Resources.APMWebContent.APMWEBDATA_VB1_309%>
						</td>
						<td class="ReportHeader">
							<%= Resources.APMWebContent.APMWEBDATA_VB1_310%>
						</td>
					</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td class="Property">
						<a href="<%#this.GetCustomChartUrl(Container.DataItem)%>" target="_blank">
							<%# Eval("DisplayPeriod")%>
						</a>&nbsp;
					</td>
					<td class="Property">
						<a href="<%#this.GetCustomChartUrl(Container.DataItem)%>" target="_blank">
							<%# Eval("Availability") %>
						</a>&nbsp;
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</Content>
</orion:resourceWrapper>