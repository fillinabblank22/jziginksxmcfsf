﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditMonitorGauge.aspx.cs" Inherits="Orion_APM_Resources_EditMonitorGauge" %>

<%@ Import Namespace="SolarWinds.NPM.Web.Gauge.V1" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="apm" TagName="GaugeAssigner" Src="~/Orion/APM/Controls/AssignResourceToComponent.ascx" %>
<%@ Register TagPrefix="apm" TagName="GaugesAssigner" Src="~/Orion/APM/Controls/AssignGaugesToComponent.ascx" %>
<%@ Register TagPrefix="apm" TagName="MultiValueSelection" Src="~/Orion/APM/Controls/MultiValueSelection.ascx" %>

<asp:Content ID="ctrHead" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">
    <orion:Include runat="server" File="styles/EditGauge.css" />
    <orion:Include runat="server" File="APM/Styles/Resources.css" />
	<orion:Include runat="server" File="../js/ig_webGauge.js"/>
    <orion:Include runat="server" File="../js/ig_shared.js"/>
    <script language="javascript" type="text/javascript">
		var titleTemplate;
		$().ready(
			function () {
				$("input[id$='resourceTitleEditor_txtTitle']").css("width", "400px");
				var gaugeTypesSelect = $("select[id$='ctrGaugeTypes']");
				if (gaugeTypesSelect && gaugeTypesSelect.size() > 0) {
					gaugeTypesSelect.change(function () {
						var title = '<%=this.Resource.Title%>';
						var titleInput = $("input[id$='txtTitle']");
						if (titleInput.val() != title) {
							titleInput.val(title);
						}
					});
					gaugeTypesSelect.change();
				}
			}
		);

		var selectedStyle;
		function SelectGauge(styleName) {
			var oldSelectedStyle = document.getElementById(selectedStyle);
			if (oldSelectedStyle != null) {
				oldSelectedStyle.className = "DeSelectedGauge";
			}
			var gaugesListSpan = document.getElementById("GaugesList");
			var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
			var gaugeSelect = gaugesSelect[0];
			for (var i = 0; i < gaugeSelect.options.length; i++) {
				if (gaugeSelect.options[i].value == styleName) {
				    gaugeSelect.selectedIndex = i;
				    LinearGaugeSelectStyle(styleName);
					break;
				}
			}
			var gaugeImageSpan = document.getElementById(styleName);
			gaugeImageSpan.className = "SelectedGauge";
			selectedStyle = gaugeImageSpan.id;
		}

		function SelectCurrentGauge() {
			var gaugesListSpan = document.getElementById("GaugesList");
			var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
			var gaugeSelect = gaugesSelect[0];
			var value = gaugeSelect.options[gaugeSelect.selectedIndex].value;
			SelectGauge(value);
		}

		function LinearGaugeSelectMe(element) {
			LinearGaugeSelectStyle(element.getAttribute('stylename'));
		}

		function LinearGaugeSelectCurrent(element) {
			LinearGaugeSelectStyle(element.value)
		}

		function LinearGaugeSelectStyle(styleName) {
			$(".gauge").css("border", "0px solid red");
			$('[stylename=\'' + styleName + '\']').css("border", "2px dashed red");

			var gaugesListSpan = document.getElementById("GaugesList");
			var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
			var gaugeSelect = gaugesSelect[0];
			for (var i = 0; i < gaugeSelect.options.length; i++) {
				if (gaugeSelect.options[i].value == styleName) {
					gaugeSelect.selectedIndex = i;
					break;
				}
			}
		}
	</script>
</asp:Content>

<asp:Content ID="ctrBody" ContentPlaceHolderID="MainContent" runat="Server">
	<asp:ScriptManager runat="server"/>

	<h1 id="editGaugeTitle" style="padding-left: 0px;"><%=this.HeadTitle%></h1>
	
	<asp:Panel ID="ctrEditPanel" DefaultButton="btnSubmit" runat="server">
		<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
		<orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowTitle="true" ShowSubTitle="true" />
		<div id="ctrSDContainer" runat="server">
			<b><%= Resources.APMWebContent.APMWEBDATA_AK1_193 %></b><br />
			<span>
				<asp:TextBox ID="ctrCustomLabelSD" MaxLength="19" Columns="30" runat="server" />
				<asp:CompareValidator ID="ctrCustomLabelSDRangeValidator" ControlToValidate="ctrCustomLabelSD" Type="String" Operator="DataTypeCheck" Display="Dynamic" ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_207 %>" runat="server" />
			</span>
			<br />
			<br />
		</div>
		<apm:GaugeAssigner ID="ctrGaugeAssigner" runat="server" />
		<apm:GaugesAssigner ID="ctrGaugesAssigner" runat="server" />
		<% if (ctrGaugeAssigner.Visible || ctrGaugesAssigner.Visible) {%> <br /> <br /> <%}%>
		<div>
			<i><%= Resources.APMWebContent.APMWEBDATA_AK1_194 %></i>
		</div>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th align="left"><%= Resources.APMWebContent.APMWEBDATA_AK1_195 %></th>
				<th align="left"><%= Resources.APMWebContent.APMWEBDATA_AK1_196 %></th>
				<th align="left"><%= Resources.APMWebContent.APMWEBDATA_AK1_197 %></th>
			</tr>
			<tr>
				<td>
					<asp:TextBox ID="ctrMaxScale" MaxLength="10" Columns="20" runat="server" />&nbsp;
				</td>
				<td>
					<asp:TextBox ID="ctrUnitsValue" MaxLength="18" Columns="20" runat="server" />&nbsp;
				</td>
				<td>
					<asp:TextBox ID="ctrUnitsLabel" MaxLength="10" Columns="20" runat="server" />&nbsp;
					<asp:CustomValidator ID="ctrUnitsValidator" ControlToValidate="ctrUnitsValue" ErrorMessage="*" Display="Dynamic" OnServerValidate="OnUnits_ServerValidate" ValidateEmptyText="true" runat="server" />
				</td>
			</tr>
		</table>
		<br />
		<b><%= Resources.APMWebContent.APMWEBDATA_AK1_198 %></b><br />
		<span id="GaugesList">
			<asp:DropDownList ID="stylesList" runat="server" onchange="SelectCurrentGauge()" />
		</span>
		<br /> <br />
		<b><%= Resources.APMWebContent.APMWEBDATA_AK1_199 %></b><br />
		<asp:TextBox ID="scaleInput" runat="server" MaxLength="3"></asp:TextBox>
		<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="scaleInput" ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_200 %>" MinimumValue="30" MaximumValue="250" Type="Integer">*</asp:RangeValidator>
		<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="scaleInput" ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_201 %>">*</asp:RequiredFieldValidator>
		<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="scaleInput" ErrorMessage="<%$ Resources: APMWebContent, APMWEBDATA_AK1_202 %>" Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
		<br /><br />
		<apm:MultiValueSelection runat="server" ID="MultiValueSelection" />
		<orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources: APMWebContent, APMWEBDATA_AK1_206 %>" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
		<br /><br />
        <div id="GaugeStylesPanel" runat="server" style="background-color: White; border-style: ridge; width: 840px">
		</div>
		<% if (GaugeType == GaugeType.Linear) { %>
		<script language="javascript" type="text/javascript">
			LinearGaugeSelectStyle('<%= SelectedStyle %>');
		</script>
		<% } else { %>
		<script language="javascript" type="text/javascript">
			SelectCurrentGauge();
		</script>
		<% } %>
	</asp:Panel>
</asp:Content>