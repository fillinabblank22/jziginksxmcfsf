﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_TopXXIndexes_Base : ApmBaseResource, IResourceIsInternal
{
	public override IEnumerable<Type> RequiredInterfaces
	{
		get
		{
			return new[] { typeof(ISqlBlackBoxDatabaseProvider) };
		}
	}

	protected override string DefaultTitle
	{
		get { return null; }
	}

    public override string DisplayTitle
    {
        get
        {
            return Title.Replace("XX", GetStringValue("maxCount", "10"));
        }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    public override string EditControlLocation
    {
        get { return "/Orion/APM/SqlBlackBox/Controls/Top10QueriesSettings.ascx"; }
    }

    public override string EditURL
    {
        get
        {
            var urlBuilder = new UrlBuilder(base.EditURL);
            urlBuilder["ShowFilter"] = bool.TrueString;
            urlBuilder["ShowMaxCount"] = bool.TrueString;

            return urlBuilder.Url;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
	    var filter = GetStringValue("filter", String.Empty);
        var rowCountLimit = GetStringValue("maxCount", "10");
        
		CustomTable.UniqueClientID = Resource.ID;
		CustomTable.SWQL = string.Format("SELECT TOP {4}" +
										 " i.TableName, " +
										 " i.Name, " +
                                         " i.Size, " +
                                         " i.Fragmentation, " +
                                         " i.Status as _status " +
										 "FROM {0} AS i " +
										 "WHERE i.IndexType = {1} AND i.SqlDatabaseID = {2}{3}",
										 SwisEntities.Index,
										 Resource.Properties["IndexType"],
										 GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>().SqlBlackBoxDatabase.Id,
                                         String.IsNullOrWhiteSpace(filter) ? String.Empty : " AND " + filter,
                                         rowCountLimit);
	}

	public bool IsInternal
	{
		get { return true; }
	}
}