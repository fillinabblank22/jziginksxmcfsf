﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_TopXXTablesBySize : ApmBaseResource
{
	public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ISqlBlackBoxDatabaseProvider) };

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    public override string EditControlLocation => "/Orion/APM/SqlBlackBox/Controls/Top10QueriesSettings.ascx";

    public override string EditURL
    {
        get
        {
            var urlBuilder = new UrlBuilder(base.EditURL);
            urlBuilder["ShowFilter"] = bool.TrueString;
            urlBuilder["ShowMaxCount"] = bool.TrueString;

            return urlBuilder.Url;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = GetStringValue("filter", String.Empty);
        var rowCountLimit = GetStringValue("maxCount", "10");

		CustomTable.UniqueClientID = Resource.ID;
		CustomTable.SWQL = string.Format("SELECT TOP {3} Name, " +
		                                 "TotalSpace * 1024 As TotalSpace, " +
                                         "PercentUsageByIndex, " +
		                                 "RowCount, " +
                                         "Status as _status " +
		                                 "FROM {1} " +
		                                 "WHERE SqlDatabaseID = {0}{2}",
								GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>().SqlBlackBoxDatabase.Id,
								SwisEntities.Table,
                                String.IsNullOrWhiteSpace(filter) ? String.Empty : " AND " + filter,
                                rowCountLimit);
    }

	protected override string DefaultTitle => APM_SQLBBContent.Web_TopXXTablesBySize_Resource_Title;

    public override string DisplayTitle => Title.Replace("XX", GetStringValue("maxCount", "10"));
}