﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentMonitoring.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_AgentMonitoring" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSResources"  runat="server" File="APM/SqlBlackBox/Js/Resources.js"/>
        <orion:Include ID="JSConverters" runat="server" File="APM/js/Converters.js"/>
        <orion:Include ID="CssResources" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>

        <% if (MonitoringEnabled || Resource.IsInReport){ %>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                var expanded_<%= Resource.JavaScriptFriendlyID %> = [];

                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.JavaScriptFriendlyID %>,
                        cls: "sqlAgentMonitoring NeedsZebraStripes",
                        initialPage: 0,
                        rowsPerPage: 100,
                        allowSort: true,
                        allowSearch: false,
                        allowPaging: false,
                        underscoreTemplateId: "#contentTemplate-<%= Resource.JavaScriptFriendlyID %>",
                        headerTitles: [
                            "<%= Resources.APM_SQLBBContent.AgentMonitoring_JobName%>",
                            "<%= Resources.APM_SQLBBContent.AgentMonitoring_LastRun%>",
                            "<%= Resources.APM_SQLBBContent.AgentMonitoring_Duration%>",
                            "<%= Resources.APM_SQLBBContent.AgentMonitoring_Outcome%>"],
                        defaultOrderBy: "[LastRunDate] DESC"
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.JavaScriptFriendlyID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });

            var renderDuration = function (duration) {
                if (duration === null) {
                    return '';
                }
                if (duration < 1) {
                    return '<1s';
                }
                return SW.Core.Charts.dataFormatters.secondNoLimit(duration);
            };

            var renderDateTime = function (date) {
                if (date === null)
                    return '';

                date = new Date(date);

                var newDate = new Date(date.getTime());
                var offset = date.getTimezoneOffset() / 60;
                var hours = date.getHours();

                newDate.setHours(hours - offset);

                return newDate.toLocaleString();
            };
        </script>
        <script id="contentTemplate-<%= Resource.JavaScriptFriendlyID %>" type="text/template">
            {#
                var outcome = Columns[3];
                var outcome_status = "undefined";
                if (outcome == 0)
                    outcome_status = "critical";
                else if (outcome == 1)
                    outcome_status = "up";
                else if (outcome == 2 || outcome == 3)
                    outcome_status = "warning";

                var status = SW.APM.SQLBB.Resources.getSimpleStatus(Columns[6]);
                if (outcome != 1 && outcome != 4)
                { // Duration thresholds are important only for successful jobs AND in progress
                    status = "up";
                }
            #}
            <tr>
                <td class="column_JobName">{{ [Columns[0]] }}</td>
                <td class="column_LastRun">{{ renderDateTime(Columns[1]) }}</td>
                <td class="column_Duration status_{{ status }}">{{ renderDuration(Columns[2]) }}</td>
                <td class="column_Outcome status_{{ outcome_status }}"><img src="{{[Columns[5]]}}"/>{{ [Columns[4]] }}</td>
            </tr>
        </script>
        <% } else { %>
        <div class="sqlAgentMonitoringInfo">
            <span><%= Resources.APM_SQLBBContent.AgentMonitoring_HintText%></span>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButtonLink ID="btLearnMore" runat="server" Text="<%$ Resources : APM_SQLBBContent, AgentMonitoring_LearnMore %>" DisplayType="Primary" />
        </div>
        <% } %>
    </Content>
</orion:resourceWrapper>
