﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_AgentMonitoring : ApmBaseResource
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(ISqlBlackBoxApplicationProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btLearnMore.NavigateUrl = HelpLocator.CreateHelpUrl("OrionSAMAGSQLAppAgentMonitoring");
        
        CustomTable.UniqueClientID = Resource.JavaScriptFriendlyID;
        CustomTable.SWQL = InvariantString.Format(@"SELECT
    Name,
    LastRunDate,
    LastRunDuration,
    LastRunStatus as _LastRunStatusId,
    CASE LastRunStatus
        WHEN 0 THEN '{1}'
        WHEN 1 THEN '{2}'
        WHEN 2 THEN '{3}'
        WHEN 3 THEN '{4}'
        WHEN 4 THEN '{5}'
        ELSE ''
    END as LastRunStatus,
    CASE ji.LastRunStatus
        WHEN 0 THEN '/Orion/APM/SqlBlackBox/Images/StatusIcons/job_failed.png'
        WHEN 1 THEN '/Orion/APM/SqlBlackBox/Images/StatusIcons/job_succeeded.png'
        WHEN 2 THEN '/Orion/APM/SqlBlackBox/Images/StatusIcons/job_retry.png'
        WHEN 3 THEN '/Orion/APM/SqlBlackBox/Images/StatusIcons/job_cancelled.png'
        WHEN 4 THEN '/Orion/APM/SqlBlackBox/Images/StatusIcons/job_inprogress.png'
        ELSE '/Orion/images/StatusIcons/Small-EmptyIcon.gif'
    END as _IconFor_LastRunStatus,
    Status AS _Status
FROM Orion.APM.SqlJobInfo ji
WHERE ApplicationID={0}",
            GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().SqlBlackBoxApplication.Id,
            Resources.APM_SQLBBContent.AgentMonitoring_Outcome_Failed,
            Resources.APM_SQLBBContent.AgentMonitoring_Outcome_Succeeded,
            Resources.APM_SQLBBContent.AgentMonitoring_Outcome_Retry,
            Resources.APM_SQLBBContent.AgentMonitoring_Outcome_Canceled,
            Resources.APM_SQLBBContent.AgentMonitoring_Outcome_InProgress);
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.APM_SQLBBContent.AgentMonitoring_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionSAMAGSQLAppAgentMonitoring";
        }
    }

    public bool MonitoringEnabled
    {
        get
        {
            var application = GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().SqlBlackBoxApplication;
            long componentId = new ComponentDal().GetComponentIdByTemplateUid(application.Id, SqlBlackBoxConstants.IDAgentJobInfo);
            if (componentId >= 0)
            {
                return !ApmMonitor.GetComponent(componentId).IsDisabled;
            }
            return false;
        }
    }
}