﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MostExpensiveQueries.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_MostExpensiveQueries" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Include ID="Include1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
		<orion:Include ID="Include3" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>

	    <asp:PlaceHolder runat="server" ID="dpaoPlaceHolder" />

		<table class="mostExpensiveQueries_FilterTable">
			<tr>
				<td rowspan="2" ><%= Resources.APM_SQLBBContent.MostExpensiveQueries_Filter_Title %></td>
				<td><%= Resources.APM_SQLBBContent.MostExpensiveQueries_Filter_Time %></td>
				<td><%= Resources.APM_SQLBBContent.MostExpensiveQueries_Filter_Host %></td>
				<td><%= Resources.APM_SQLBBContent.MostExpensiveQueries_Filter_Database %></td>
				<td><%= Resources.APM_SQLBBContent.MostExpensiveQueries_Filter_Login %></td>
			</tr>
			<tr>
				<td><select id="time_<%= Resource.JavaScriptFriendlyID %>">
					    <option value="_null_"><%= Resources.APM_SQLBBContent.MostExpensiveQueries_FilterValue_All %></option>
					    <option value="5"><%= string.Format(Resources.APM_SQLBBContent.MostExpensiveQueries_FilterValue_LastXXMin, 5) %></option>
						<option value="15"><%= string.Format(Resources.APM_SQLBBContent.MostExpensiveQueries_FilterValue_LastXXMin, 15) %></option>
						<option value="30"><%= string.Format(Resources.APM_SQLBBContent.MostExpensiveQueries_FilterValue_LastXXMin, 30) %></option>
						<option value="60"><%= Resources.APM_SQLBBContent.MostExpensiveQueries_FilterValue_LastHour %></option>
				    </select></td>
				<td><select id="hosts_<%= Resource.JavaScriptFriendlyID %>"></select></td>
				<td><select id="databases_<%= Resource.JavaScriptFriendlyID %>"></select></td>
				<td><select id="logins_<%= Resource.JavaScriptFriendlyID %>"></select></td>
			</tr>
		</table>
		<apm:CustomQueryTable runat="server" ID="customTable"/>
		
		<script type="text/javascript">
			$(function () {
				var expanded_<%= Resource.JavaScriptFriendlyID %> = [];
				var filterOptions_Hosts_<%= Resource.JavaScriptFriendlyID %> = [];
				var filterOptions_Databases_<%= Resource.JavaScriptFriendlyID %> = [];
				var filterOptions_Logins_<%= Resource.JavaScriptFriendlyID %> = [];

				var customQueryConfig_<%= Resource.JavaScriptFriendlyID %> = {
                    	uniqueId: <%= Resource.JavaScriptFriendlyID %>,
				    cls: "mostExpensiveQueries NeedsZebraStripes",
                    	initialPage: 0,
                    	rowsPerPage: 100,                        
                    	allowSort: true,
                    	allowSearch: false,
                    	defaultOrderBy: "[CpuTime] DESC",
                    	headerTitles: [
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_Query %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_Spid %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_PlanCount %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_Executions %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_CPUTime %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_PhysicalReads %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_LogicalReads %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_LogicalWrites %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_AverageDuration %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_Login %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_Host %>",
								"<%= Resources.APM_SQLBBContent.MostExpensiveQueries_Database %>"],
                    	customHeaderGenerator: function(columns) {
                    		var headerRow = $('<tr/>')
							.addClass('HeaderRow');

                    		//td for +/- button
                    		var cell = $('<td/>')
										.addClass('ReportHeader')
						
                    		cell.appendTo(headerRow);
                    		
                    		for (var columnIndex = 0; columnIndex < columns.length; columnIndex++){
                    			columns[columnIndex].appendTo(headerRow);
                    		}

                    		return headerRow;
                    	},
                    	customRowGenerator: function(columns) {
                    		var result = $('<tr/>');
                    		
                    		var cell = $("<td/>").appendTo(result);
                    		<% if (IsDpaoPatchLoaded) { %>
                    	        cell.attr("rowspan", "2");
                            <% } %>
                    		cell.css("vertical-align", "text-top");
                    		var content = $("<a/>");
                    		content.attr("href", "javascript:void(0);");
                    		content.click(function () {
                    			var row = $(".tQuery_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);
                    			if (row.hasClass("topQueries_Collapsed")) {
                    				expanded_<%= Resource.JavaScriptFriendlyID %>.push(columns[0]);
                    				row.removeClass("topQueries_Collapsed");
                    				row.addClass("topQueries_Expanded");
                    				$(".tQuery_Button_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]).attr("src", "/Orion/images/Button.Collapse.gif");
                    			}
                    			else {
                    				expanded_<%= Resource.JavaScriptFriendlyID %>.pop(columns[0]);
                    				
                    				row.removeClass("topQueries_Expanded");
                    				row.addClass("topQueries_Collapsed");
                    				var x = $(".tQuery_Button_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);
                    			    $(".tQuery_Button_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]).attr("src", "/Orion/images/Button.Expand.gif");
                    			}
                    		});

                    	    var img = $("<img alt='.' />");
                    	    img.addClass("tQuery_Button_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);
                    	    img.addClass("mostExpensiveQueries_ExpanderImg");
                    		
                    		img.appendTo(content);
                    		content.appendTo(cell);

                    		var expander = content;

                    		cell = $("<td/>").appendTo(result);
                    		cell.css("vertical-align", "top");
                    		<% if (IsDpaoPatchLoaded) { %>
                    	        cell.attr("rowspan", "2");
                            <% } %>

                    		content = $("<span/>").appendTo(cell);
                    		content.attr("title", columns[1]);
                    		content.addClass("queryColumn");
                    		content.addClass("tQuery_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);
                    		if ($.inArray(columns[0], expanded_<%= Resource.JavaScriptFriendlyID %>) > -1) {
                    			content.addClass("topQueries_Expanded");
                    			img.attr("src", "/Orion/images/Button.Collapse.gif");
		                    }
                    		else {
                    			content.addClass("topQueries_Collapsed");
                    			img.attr("src", "/Orion/images/Button.Expand.gif");
                    		}
                    	    content.text(columns[1]);

                    	    var isExpanded = content.hasClass("topQueries_Expanded");

                    	    for (var columnIndex = 2; columnIndex < columns.length - 3; columnIndex++){
                    			cell = $("<td/>").appendTo(result);
                    			cell.css({
                                    "vertical-align": "top",
		                            "text-align": "left",
		                            "white-space": "nowrap",
		                            "padding-left": "5px",
                                    "padding-right": "5px"
		                        });

                    		    <% if (IsDpaoPatchLoaded) { %>
                    		        cell.css({
		                                "height": "0px",
                    		            "border-bottom": "0px"
		                            });
                                <% } %>
                    			
                    			content = $("<span/>").appendTo(cell);

                    			if (columnIndex == 5)	//CPU Time
                    				content.text(columns[columnIndex] + " ms");
                    			else if (columnIndex == 9)	//Average Duration
                    				content.text(columns[columnIndex] + " ms");
                    			else
                    				content.text(columns[columnIndex]);


                    			//Get all possible options for filter fields
                    			var insertIfNotExists = function(array, value) {
                    				if ($.inArray(value, array) == -1)
                    					array.push(value);
                    			};
                    			if (columnIndex == 10)	//Login
                    				insertIfNotExists(filterOptions_Logins_<%= Resource.JavaScriptFriendlyID %>, columns[columnIndex]);
                    			else if (columnIndex == 11) // Host
                    				insertIfNotExists(filterOptions_Hosts_<%= Resource.JavaScriptFriendlyID %>, columns[columnIndex]);
                    			else if (columnIndex == 12) //Database
                    				insertIfNotExists(filterOptions_Databases_<%= Resource.JavaScriptFriendlyID %>, columns[columnIndex]);
								
                    			if (columnIndex == columns.length - 4) { // Last column is image for DB
                    				if (columns[columnIndex] != null) {
                    					var img = $("<img alt='.' />");
                    					img.attr("src", columns[columnIndex+1]);
                    					img.prependTo(cell);

                    					var a = $("<a />");
                    					a.attr("href", columns[columnIndex+2]);
                    					cell.children().appendTo(a);
                    					a.appendTo(cell);
                    				}
                    				break;
                    			}
                    		}

                    	    <% if (IsDpaoPatchLoaded) { %>
                    	        var dpaLinksRow = <%= this.IntegrationCallbackFn %>_createDpaLinksRow("<%= Resource.JavaScriptFriendlyID %>_" + columns[0], expander, columns[15], isExpanded);
                    	        return $.makeArray(dpaLinksRow, result);
                            <% } else {%>
                    	        return result;
                            <% } %>
                    	},
					onloaded: function() {
						var configureComboboxes = function(htmlSelect, dataArray) {
							dataArray = dataArray.sort(function (a, b){
								var _a = a;
								var _b = b;
								if (_a != null) _a = _a.toLowerCase();
								if (_b != null) _b = _b.toLowerCase();
								return (_a > _b); 
							});
							var select = $(htmlSelect);
							var currentlySelectedVal = select.val();

							var currentlySelectedValFound = false;
							var hasEmptyValue = false;

							
							var appendOption = function(value, stringValue) {
								select.append("<option value='" + value + "'>"+ stringValue + "</option>");
							}

							select.empty();
							appendOption("_null_", "<%= Resources.APM_SQLBBContent.MostExpensiveQueries_FilterValue_All %>");
							$(dataArray).each(function(index, value){
								if (value == currentlySelectedVal)
									currentlySelectedValFound = true;
								if (value == null){
									hasEmptyValue = true;
									if (currentlySelectedVal == "_empty_")
										currentlySelectedValFound = true;
								}
								else
									appendOption(value, value);
							});

							if (hasEmptyValue)
								appendOption("_empty_", "<%= Resources.APM_SQLBBContent.MostExpensiveQueries_FilterValue_None %>");

							if (!currentlySelectedValFound)
								currentlySelectedVal = "_null_";

							select.val(currentlySelectedVal);
						}
						
						configureComboboxes("#hosts_<%= Resource.JavaScriptFriendlyID %>", filterOptions_Hosts_<%= Resource.JavaScriptFriendlyID %>);
						configureComboboxes("#databases_<%= Resource.JavaScriptFriendlyID %>", filterOptions_Databases_<%= Resource.JavaScriptFriendlyID %>);
						configureComboboxes("#logins_<%= Resource.JavaScriptFriendlyID %>", filterOptions_Logins_<%= Resource.JavaScriptFriendlyID %>);

					}
                };
				SW.APM.Resources.CustomQuery.initialize(customQueryConfig_<%= Resource.JavaScriptFriendlyID %>); 
				var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.JavaScriptFriendlyID %>); };

				var onFilterChange = function() {
					var timeValue = $("#time_<%= Resource.JavaScriptFriendlyID %>").val();
					var hostValue = $("#hosts_<%= Resource.JavaScriptFriendlyID %>").val();
					var databaseValue = $("#databases_<%= Resource.JavaScriptFriendlyID %>").val();
					var loginValue = $("#logins_<%= Resource.JavaScriptFriendlyID %>").val();

					var appendCondition = function(conditions, value, swqlColumnName){
						if (value != "_null_") {
							if (value != "_empty_")
								conditions.push(swqlColumnName + " = '" + value + "'");
							else
								conditions.push(swqlColumnName + " IS NULL");
						}
					};

					var conditions = [];
					appendCondition(conditions, hostValue, "Host");
					appendCondition(conditions, databaseValue, "db.Name");
					appendCondition(conditions, loginValue, "Login");
					//Time condition
					if (timeValue != "_null_")
						conditions.push("MinuteDiff(LastExecutionTime,GetUtcDate()) < " + timeValue);

					var whereClause = null;
					if (conditions.length != 0) {
						whereClause = conditions.join(" AND ");
					} else {	
						//We don´t have any filter -> refresh all possible options
						filterOptions_Hosts_<%= Resource.JavaScriptFriendlyID %> = [];
						filterOptions_Databases_<%= Resource.JavaScriptFriendlyID %> = [];
						filterOptions_Logins_<%= Resource.JavaScriptFriendlyID %> = [];
					}
					
					SW.APM.Resources.CustomQuery.UpdateConfig({
						uniqueId: <%= Resource.JavaScriptFriendlyID %>,
						whereClause: whereClause
					});
					refresh();
				};
				
				$("#time_<%= Resource.JavaScriptFriendlyID %>").change(onFilterChange);
				$("#hosts_<%= Resource.JavaScriptFriendlyID %>").change(onFilterChange);
				$("#databases_<%= Resource.JavaScriptFriendlyID %>").change(onFilterChange);
				$("#logins_<%= Resource.JavaScriptFriendlyID %>").change(onFilterChange);
				
				SW.Core.View.AddOnRefresh(refresh, '<%= customTable.ClientID %>');
        		refresh();
        	});	    
        </script>
	</Content>
</orion:resourceWrapper>