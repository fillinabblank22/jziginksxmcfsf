﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseDetails.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_DatabaseDetails" %>
<%@ Import Namespace="SolarWinds.Internationalization.Extensions" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Web.UI" %>
<%@ Register TagPrefix="apm" TagName="ComponentErrorStatusControl" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>

<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
            function pollApp() {
                var config = {
                    selItems: [{ id: '<%=Database.ApplicationId%>' }],
                    onComplete: function() { window.location.reload(true); }
                };
                return SW.APM.PollApp.pollNow(config);
            }

            function manageApplicationItem(managed, message) {
                var dbName = '<%=Database.Database.Name%>';
                var message = managed ? "<%=Resources.APM_SQLBBContent.ManageDbRemanageQuestion%>" : "<%=Resources.APM_SQLBBContent.ManageDbUnmanageQuestion%>";
                message = SF(message, dbName);
                Ext.Msg.show({
                    msg: message,
                    buttons: Ext.Msg.YESNO,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            var config = {
                                applicationCustomType: "ABSA",
                                applicationId: <%=Database.ApplicationId%>,
                                        itemId: <%=Database.Id%>,
                                managed: managed,
                                message: message,
                                onComplete: function() { window.location.reload(true); }
                            };
                            SW.APM.ManageAppItem.manage(config);
                        }
                    }
                });
                return false;
            }

            function showApplicationUnmanagedMessage() {
                Ext.Msg.show({
                    title: "<%=Resources.APM_SQLBBContent.ManageDbDialogCaption%>",
                    msg: "<%=Resources.APM_SQLBBContent.ManageDbUnmanagedApplication%>",
                    icon: Ext.Msg.INFO, buttons: Ext.Msg.OK
                });
                return false;
            }
        </script>
		<orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
		<orion:Include runat="server" File="APM/js/NodeManagement.js" />
		<orion:Include runat="server" File="APM/Styles/Resources.css" />	

        <table cellspacing="0" class="NeedsZebraStripes biggerPadding">
            <% if ((ApmRoleAccessor.AllowAdmin || Profile.AllowUnmanage) && ApmApplication != null && ApmApplication.Status != null)
			{ %>
            <tr>
                <td class="PropertyHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_292 %></td>
                <td class="Property NodeManagementIcons">
					<% if (ApmRoleAccessor.AllowAdmin) { %>
					    <a class="inline" href="<%=ApmMasterPage.GetEditApplicationItemPageUrl(ApmApplication.Id, Database.Id)%>">
					        <img alt="" src="/Orion/Nodes/images/icons/icon_edit.gif"/> <%= Resources.APMWebContent.APMWEBDATA_AK1_98 %>  
					    </a>
					<% } %>
                    <% if (Profile.AllowUnmanage) { %>
					        <% if (Status == SolarWinds.APM.Common.Models.Status.Unmanaged) { %>
						          <a class="inline" onclick="return <%= (Database.Database.ApplicationStatus == CoreStatusUnmanaged) ? "showApplicationUnmanagedMessage()" : "manageApplicationItem(true,'" + Resources.APM_SQLBBContent.ManageDbManageProgress + "')" %>;" href="#">
							         <img id="manage_toggle_image" alt="" src="/Orion/Nodes/images/icons/icon_remanage.gif"/> <%= Resources.APMWebContent.APMWEBDATA_VB1_294 %> 
						           </a>
					        <% } else { %>
						        <a class="inline" onclick="return manageApplicationItem(false,'<%=Resources.APM_SQLBBContent.ManageDbUnmanageProgress%>');" href="#">
							        <img id="Img1" alt="" src="/Orion/Nodes/images/icons/icon_manage.gif"/> <%= Resources.APMWebContent.APMWEBDATA_VB1_295 %> 
						        </a>
					        <% } %>
                    <% } %>
					<% if (ApmRoleAccessor.AllowAdmin && Status != SolarWinds.APM.Common.Models.Status.Unmanaged) { %>
					    <a class="inline" onclick="return pollApp();" href="#">
					        <img alt="" src="/Orion/images/pollnow_16x16.gif" /> <%= Resources.APMWebContent.APMWEBDATA_VB1_296 %>
					    </a>
					<% } %>
                </td>
            </tr>
            <% } %>

            <% if (ApmRoleAccessor.AllowRealTimeProcessExplorer || ApmRoleAccessor.AllowRealTimeServiceExplorer || ApmRoleAccessor.AllowRealTimeEventExplorer)
			   { %>
			<tr>
				<td class="PropertyHeader">&nbsp;</td>
				<td class="Property NodeManagementIcons">
				     <% if (ApmRoleAccessor.AllowRealTimeProcessExplorer){ %>
                	    <a href="#" onclick="return SW.APM.NodeManagement.openRealTimeProcessWindow(<%=Database.SqlBlackBoxApplication.Node.Id%>);">
					    	<img alt="" src="/Orion/APM/Images/RT_process_explorer.png" /> <%= Resources.APMWebContent.APMWEBDATA_VB1_298 %>
					    </a>
                    <% } %>
                     <% if (ApmRoleAccessor.AllowRealTimeServiceExplorer && ShowServiceManagerLink) { %>
                        <span><a href="#" onclick="return SW.APM.NodeManagement.openRealTimeServicesWindow(<%=Database.SqlBlackBoxApplication.Node.Id%>,<%=GetRtMonitorCredId()%>);">
						    <img alt="" src="/Orion/APM/Images/RT_service_manager.png" /> <%= Resources.APMWebContent.APMWEBDATA_RM0_7%>
					    </a></span>
                     <% } %>
                     <% if (ApmRoleAccessor.AllowRealTimeEventExplorer && ShowEventExplorerLink) { %>
                        <div>
                            <a href="#" onclick="return SW.APM.NodeManagement.openRealTimeEventExplorerWindow(<%=Database.SqlBlackBoxApplication.Node.Id%>,<%=GetRtMonitorCredId()%>);">
						        <img alt="" src="/Orion/APM/Images/RT_Event_Viewer.png" /> <%= Resources.APMWebContent.APMWEBDATA_VB1_343%>
					        </a>
                        </div>
                     <% } %>
                 </td>
			</tr>
            <% }%>


            <tr>
                <td class="PropertyHeader" style="min-width: 140px;"><%= Resources.APM_SQLBBContent.DatabaseDetails_DbName %></td>
                <td class="Property"><img src="<%= StatusIcon %>" />&nbsp;<%= Database.Database.Name %></td>
            </tr>

            <tr>
                <td class="PropertyHeader" style="vertical-align: baseline"><%= Resources.APM_SQLBBContent.DatabaseDetails_PerfStatus %></td>
                <td class="Property"><b><%=ApmStatus.ToLocalizedString() %></b><br /><%= StatusHelper.GetStatusDescription(Status) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader" style="vertical-align: baseline"><%= Resources.APM_SQLBBContent.DatabaseDetails_OpStatus %></td>
                <td class="Property">
                    <b><%= DbState.LocalizedLabel() %></b><br /><%= StatusHelper.GetStatusDescription(DbState) %>
                    <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader" style="vertical-align: baseline"><%= Resources.APM_SQLBBContent.DatabaseDetails_RecoveryModel %></td>
                <td class="Property"><b><%= DbRecoveryModel.LocalizedLabel() %></b><br /><%= StatusHelper.GetStatusDescription(DbRecoveryModel) %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.DatabaseDetails_Collation %></td>
                <td class="Property"><%= Database.Database.Collation %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.DatabaseDetails_CompLevel %></td>
                <td class="Property"><%= Database.Database.CompatibilityLevel %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.DatabaseDetails_LastBackup %></td>
                <td class="Property">
                    <%= Database.Database.LastBackup.HasValue 
                            ? string.Format("{0} ({1})", LastBackupTimeElapsed, Database.Database.LastBackup.Value.ToLocalTime().ToString("f", System.Globalization.CultureInfo.CurrentUICulture)) 
                            : Resources.CoreWebContent.WEBCODE_VB0_242 %>

                </td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.DatabaseDetails_HighAvalSetup %></td>
                <td class="Property"><%= MirroringInfo %></td>
            </tr>
            
            <% if (MirroringFound)
               { %>
            <tr>
                <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.DatabaseDetails_MirroringRole %></td>
                <td class="Property"><%= MirroringRole %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.APM_SQLBBContent.DatabaseDetails_MirroringState %></td>
                <td class="Property"><%= MirroringState %></td>
            </tr>

            <% } %>

            </table>
			
			<% if (Database.AutoCloseOn)
               { %>
				<div class="sw-suggestion sw-suggestion-warn">
					<span class="sw-suggestion-icon"></span>
					<%= Resources.APM_SQLBBContent.DatabaseDetails_AutoCloseOnWarning %>
				</div>
			<% } %>
			
            <apm:ComponentErrorStatusControl
                ID="componentErrorStatusControl" 
                runat="server" 
                OnInit="componentErrorStatusControl_Init">
            </apm:ComponentErrorStatusControl>        
    </Content>
</orion:resourceWrapper>
