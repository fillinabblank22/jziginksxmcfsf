﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_DatabaseDiskIo : ApmBaseResource, IResourceIsInternal
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString(); 

    protected override string DefaultTitle
    {
        get
        {
            return string.Empty;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(ISqlBlackBoxDatabaseProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = SWQL;
    }

    public int FileType
    {
        get
        {
            int fileType;
            if (Resource.Properties.ContainsKey("FileType") && int.TryParse(Resource.Properties["FileType"], out fileType))
                return fileType;
            else
                return 0;
        }
    }

    public string SWQL
    {
        get
        {
            var dbProvider = this.GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>();
            if (dbProvider == null)
                throw new InvalidOperationException("Cannot get database ID for filtering files by database ID");

            return @"SELECT
	f.DatabaseFileID as _fileID, f.PhysicalName as FilePath,
	v.VolumeId as _volumeID, v.Status as _volumeStatus,
    ISNULL(v.Caption, '{3}') as Volume,
    v.DiskQueueLength as QueueLength, v.TotalDiskIOPS as TotalIOPS, 
	v.DiskTransfer as Latency,
    '{2}:' + ToString(v.VolumeId) as _volumeLink
FROM 
	Orion.APM.SqlDatabaseFile f
	LEFT JOIN Orion.Volumes v ON v.VolumeId=f.VolumeID
WHERE f.SqlDatabaseId={0} AND f.FileType={1}".FormatInvariant(dbProvider.SqlBlackBoxDatabase.Id, FileType, GetViewLink("V"), Resources.APM_SQLBBContent.DatabaseDiskIo_Volume_NA);
        }
    }

    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment("SAMAGBBDatabaseDiskIO"); }
    }
}