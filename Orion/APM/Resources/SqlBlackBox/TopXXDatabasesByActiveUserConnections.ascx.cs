﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_TopXXDatabasesByActiveUserConnections : StandardChartResource, IResourceIsInternal
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", "DatabasesByActiveUserConnections");
        }

        HandleInit(WrapperContents);

		Wrapper.ShowManageButton = false;

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "SqlBlackBox/js/Charts.APM.SqlBBTopXxDatabasesByActiveUserConnectionsLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "ChartsStyles.css", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "SqlBlackBox/Styles/Charts.css", OrionInclude.Section.Middle);
    }

    protected override string DefaultTitle
    {
        get { return string.Empty; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var provider = GetInterfaceInstance<ISqlBlackBoxApplicationProvider>();

        if (provider != null && provider.ApmApplication != null)
        {
            return new string[]{provider.SqlBlackBoxApplication.Id.ToString(CultureInfo.InvariantCulture)};
        }

        return new string[0];
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ISqlBlackBoxApplicationProvider) }; }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "ADAUC"; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();

        details["ResourceID"] = Resource.ID;
        details["NetObject"] = Request.QueryString["NetObject"];

        return details;
    }
}
