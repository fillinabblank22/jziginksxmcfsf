﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_DatabaseFileSize : StandardChartResource, IResourceIsInternal
{
    private readonly Log log = new Log();
    private string netObjectID;

    #region Properties

    protected override string DefaultTitle => APM_SQLBBContent.Web_DatabaseFileSize_Resource_Title;

    public string NetObjectID
    {
        get
        {

            if (netObjectID == null)
            {
                var dbFileProvider = GetSqlBlackBoxDatabaseFileProvider();
                if (dbFileProvider != null)
                    netObjectID = dbFileProvider.SqlBlackBoxDatabaseFile.NetObjectID;
                else
                    netObjectID = this.GetNetObjectIDFromCustomChart();
            }
            return netObjectID;
        }
    }

    protected override bool AllowCustomization => Profile.AllowCustomize;

    protected override string NetObjectPrefix => SqlBlackBoxConstants.SqlBlackBoxDatabaseFilePrefix;

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ISqlBlackBoxDatabaseFileProvider) };

    private ISqlBlackBoxDatabaseFileProvider GetSqlBlackBoxDatabaseFileProvider()
    {
        return this.GetInterfaceInstance<ISqlBlackBoxDatabaseFileProvider>();
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
            Resource.Properties.Add("ChartName", SqlBlackBoxConstants.DatabaseFileSizeChartName);

        try
        {
            HandleInit(WrapperContents);
        }
        catch (Exception exc)
        {
            log.Error("Cannot initialize StandardChartResource", exc);
        }

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "SqlBlackBox/js/Charts.js", OrionInclude.Section.Middle);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.CheckAccountLimitation();
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var elementsList = new string[0];
        if (this.NetObjectID != null)
            elementsList = new[] { this.NetObjectID };

        // if the chart is not set for particular component for this application then force the customer to choose one
        if (!elementsList.Any())
            this.ForceDisplayOfNeedsEditPlaceholder = true;

        return elementsList;
    }

    private string GetNetObjectIDFromCustomChart()
    {
        return NetObjectHelper.GetNetObjectId(Resource.Properties["netobjectprefix"], int.Parse(Resource.Properties["elementlist"]));
    }

    private void CheckAccountLimitation()
    {
        if (this.NetObjectID != null)
        {
            //TODO: some better way how to check account limitation required
            try
            {
                NetObjectFactory.Create(this.NetObjectID);
            }
            catch (AccountLimitationException)
            {
                this.TransferPageAccountLimitationError();
            }
        }
    }

    private void TransferPageAccountLimitationError()
    {
        var url = "/Orion/AccountLimitationError.aspx?NetObject={0}".FormatInvariant(this.netObjectID);
        Page.Server.Transfer(url);
    }

    #region IResourceIsInternal Implementation

    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    #endregion
}
