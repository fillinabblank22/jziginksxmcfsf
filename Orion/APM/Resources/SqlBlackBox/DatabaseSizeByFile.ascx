﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseSizeByFile.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_DatabaseSizeByFile" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="Include4" runat="server" File="APM/js/Converters.js"/>
        <orion:Include ID="Include1" runat="server" File="APM/js/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/js/Resources.js"/>
        <orion:Include ID="Include3" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: 'dbSizeByFile doubleRows',
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                    	allowSort: true,
                    	headerTitles: ["<%= Resources.APM_SQLBBContent.DatabaseBySizeFile_FilePath %>", "<%= Resources.APM_SQLBBContent.DatabaseBySizeFile_AutoGrowth %>", "<%= Resources.APM_SQLBBContent.DatabaseBySizeFile_FileSize %>","<%= Resources.APM_SQLBBContent.DatabaseBySizeFile_DatabaseUsage %>", "<%= Resources.APM_SQLBBContent.DatabaseBySizeFile_VolumeUsage %>"],
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>"
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {# 
                var status=SW.APM.SQLBB.Resources.getSimpleStatus(Columns[6]);
                var fileSize = Columns[2];
                var usedSpace = Columns[3];
                if (Columns[1] == 0 /* 'Disabled' */) {
                    var fileUsed = SW.APM.calculatePercents(usedSpace, fileSize);
                } else if (Columns[1] == 1 /* 'Restricted' */ || Columns[1] == 2 /* 'Unrestricted' */) {
                    var availAutoGrow = Columns[8];
                    if (availAutoGrow == null)
                        availAutoGrow = 0; // if available volume space is not polled
                    var spaceReallyUsed = SW.APM.calculatePercents(usedSpace, fileSize + availAutoGrow); // used space in available autogrow space 
                    var spaceUsedByFile = SW.APM.calculatePercents(fileSize, fileSize + availAutoGrow); // file size in available autogrow space 
                }
                var url='<%= CustomChartUriTemplate %>'.replace(/{Name}/g, encodeURIComponent(Columns[0])).replace(/{DatabaseFileID}/g, Columns[9]);
            #}
            <tr>
                <td class='columnFull' colspan='5'><a href="{{ url }}">{{ Columns[0] }}</a></td>
            </tr>
            <tr>
                <td class='column0'> </td>
                <td class='column1'>{{ SW.APM.SQLBB.Resources.autoGrowthValues[Columns[1]] }}</td>
                <td class='column2'><span class='status_{{ status }}'>{{ SW.Core.Charts.dataFormatters.byte(Columns[2]) }}</span></td>
                <td class='column3'>{# if (Columns[1] == 0 /* 'Disabled' */) { #}
                        <span class='percentageBar' value='{{ fileUsed }}' value2='100' status='{{ status }}'><span class="percentageValue">{{ SW.Core.Charts.dataFormatters.percent(fileUsed, undefined, 2) }}</span></span>
                    {# } else if (Columns[1] == 1 /* 'Restricted' */ || Columns[1] == 2 /* 'Unrestricted' */) { #}
                        <span class='percentageBar' value='{{ spaceReallyUsed }}' value2='{{ spaceUsedByFile }}' status='{{ status }}'><span class="percentageValue">{{ SW.Core.Charts.dataFormatters.percent(spaceReallyUsed, undefined, 2) }}</span></span>
                    {# }; #}</td>
                <td class='column4'>{# if (Columns[5] == null) { #}
                        <span><%= Resources.APM_SQLBBContent.DatabaseBySizeFile_VolumeUsageNotAvailable %></span>
                    {# } else { 
                        var volumeStatus=SW.APM.SQLBB.Resources.getSimpleStatus(SW.APM.Converters.getApmStatusFromCore(Columns[7])); #}
                        <span class='percentageBar' value='{{ Columns[5] }}' status='{{ volumeStatus }}'><span class="percentageValue">{{ SW.Core.Charts.dataFormatters.percent(Columns[5], undefined, 2) }}</span></span>
                    {# }; #}</td>
            </tr>
        
        </script>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
    </Content>
</orion:resourceWrapper>