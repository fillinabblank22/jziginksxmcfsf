﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseDiskIo.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_DatabaseDiskIo" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
		<orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: 'doubleRows',
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: true,
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        headerTitles: ["<%= Resources.APM_SQLBBContent.DatabaseDiskIo_FilePath%>", "<%= Resources.APM_SQLBBContent.DatabaseDiskIo_Volume%>", "<%= Resources.APM_SQLBBContent.DatabaseDiskIo_QueueLength%>", "<%= Resources.APM_SQLBBContent.DatabaseDiskIo_TotalIOPS%>", "<%= Resources.APM_SQLBBContent.DatabaseDiskIo_Latency%>"],
                        defaultOrderBy: "FilePath ASC"
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
            
            var getIcon = function(volumeId, status){
                if(volumeId != null)
                    return '<img src="/Orion/StatusIcon.ashx?entity=Orion.Volumes&id='+volumeId+'&status='+status+'" />';
                else
                    return '';
            }
            
            var getLink = function(volumeLink, caption){
                if(volumeLink != null)
                    return '<a href="'+volumeLink+'">'+caption+'</a>';
                else
                    return caption;
            }
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
		{#
			function round(value) {
				return (value === null)
					? ''
					: SW.Core.Charts.dataFormatters.roundValue(value, 2).toLocaleString();
			}
			
			function appendUnit(value, unit) {
				var unbreakableSpace = String.fromCharCode(160);
				return (value === null || value === '')
					? ''
					: value + unbreakableSpace + unit;
			}
			
			function isNumber(n) {
			  return !isNaN(parseFloat(n)) && isFinite(n);
			}
			
			diskQueueLength = round(Columns[5]);
			totalIops = round(Columns[6]);
			if(Columns[7] != null && isNumber(Columns[7])) {
				latency =  round(Columns[7] * 1000);
				latency = appendUnit(latency, 'ms');
			}
			else {
				latency = '';
			}
			
		#}
            <tr>
                <td class='dbFileName' colspan='5'>{{ Columns[1] }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class='statusAndText'>
                    {{getIcon(Columns[2], Columns[3])}}{{getLink(Columns[8], Columns[4])}}
                </td>
                <td>{{ [ diskQueueLength] }}</td>
                <td>{{ [ totalIops ] }}</td>
                <td>{{ [ latency ] }}</td>
            </tr>
        
        </script>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
    </Content>
</orion:resourceWrapper>