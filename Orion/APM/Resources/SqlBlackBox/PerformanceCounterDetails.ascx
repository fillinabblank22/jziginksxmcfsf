﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerformanceCounterDetails.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_PerformanceCounterDetails"  %>
<%@ Register TagPrefix="apm" TagName="ApplicationDetails_Management" Src="~/Orion/APM/Controls/Resources/ApplicationDetails_Management.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <asp:PlaceHolder ID="statisticPlaceHolder" runat="server">
            <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
	        <orion:Include ID="I2" File="js/OrionCore.js" runat="server"/>        
		    <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
		    <orion:Include ID="I6" File="APM/js/NodeManagement.js" runat="server" />

		    <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />
            <orion:Include ID="Include1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
            <orion:Include ID="Include3" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>

            <script type="text/javascript">
                $(document).ready(function() {
                    SW.APM.TrimStatusDetails();

                    var thresholds = [<%= String.Join(", ", ValidThresholdValues) %>];
                    var currentValue = <%= CurrentValue %>;
                    var formatOptions = { unit: '<%= CurrentValueUnit %>' };

                    $("#currentValue").html(SW.Core.Charts.formatValue(currentValue, formatOptions, thresholds));
                });
            </script>
            
            <apm:ApplicationDetails_Management runat="server" ID="managementControl" />

            <table cellspacing="0" class="counterDetails biggerPadding NeedsZebraStripes">
			    <tbody ID="appDetailsSection" runat="server">
			        <tr>
				        <td class="PropertyHeader">
					        <%= Resources.APM_SQLBBContent.PerformanceCounterDetails_Name %>
				        </td>
				        <td>&nbsp;</td>
				        <td class="Property">
				            <asp:Image ID="imgStatus" runat="server" alt="status_icon" />
				            <asp:Literal ID="componentName" runat="server"/>
				        </td>
			        </tr>
			        <tr>
				        <td class="PropertyHeader">
					        <%= Resources.APM_SQLBBContent.PerformanceCounterDetails_Status %>
				        </td>
				        <td>&nbsp;</td>
			            <td class="Property"><asp:Literal ID="componentStatus" runat="server" /></td>
			        </tr>
                    <% if (ShowCounterError()) { %>
			        <tr>
				        <td colspan="3">
					        <%= GetCounterError().GetStatusDetailsInline()%>
				        </td>
			        </tr>
                    <% } %>
   			        <tr>
				        <td class="PropertyHeader">
					        <%= Resources.APM_SQLBBContent.PerformanceCounterDetails_CurrentValue %>
				        </td>
				        <td>&nbsp;</td>
			            <td class="PropertyCurrentValue<%= ComponentStatusDescription %>" id="currentValue"></td>
			        </tr>
			        <tr>
				        <td class="PropertyHeader">
					        <%= Resources.APM_SQLBBContent.PerformanceCounterDetails_Group %>
				        </td>
				        <td>&nbsp;</td>
				        <td class="Property"><asp:Literal ID="componentGroup" runat="server"/></td>
			        </tr>
			        <tr>
				        <td class="PropertyHeader">
					        <%= Resources.APM_SQLBBContent.PerformanceCounterDetails_ComponentType %>
				        </td>
				        <td>&nbsp;</td>
				        <td class="Property"><asp:Literal ID="componentType" runat="server"/></td>
			        </tr>
			        <tr>
				        <td class="PropertyHeader">
					        <%= Resources.APM_SQLBBContent.PerformanceCounterDetails_ExpertKnowledge %>
				        </td>
				        <td>&nbsp;</td>
				        <td class="PropertyExpertKnowledge"><asp:Literal ID="expertKnowledge" runat="server"/></td>
			        </tr>
			    </tbody>
		    </table>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="incompatibleComponentPlaceHolder" runat="server" Visible="False">
            <div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= Resources.APM_SQLBBContent.PerformanceCounterDetails_InvalidComponent %></div>
        </asp:PlaceHolder>
	</Content>
</orion:resourceWrapper>