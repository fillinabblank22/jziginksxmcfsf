﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_DatabaseSizeByFile : ApmBaseResource, IResourceIsInternal
{
    readonly Log log = new Log();

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString(); 

    protected override string DefaultTitle
    {
        get
        {
            return Resources.APM_SQLBBContent.DatabaseBySizeFile_Title;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(ISqlBlackBoxDatabaseProvider) };
        }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    private ISqlBlackBoxDatabaseProvider SqlBlackBoxDatabaseProvider
    {
        get
        {
            var dbProvider = this.GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>();
            if (dbProvider == null)
            {
                throw new InvalidOperationException("Cannot get database ID for filtering files by database ID");
            }
            return dbProvider;
        }
    }

    public string CustomChartUriTemplate
    {
        get
        {
            return CustomChartUriBuilder.Create(SqlBlackBoxConstants.DatabaseFileSizeChartName, SqlBlackBoxConstants.SqlBlackBoxDatabaseFilePrefix, "{DatabaseFileID}", InvariantString.Format(Resources.APM_SQLBBContent.CustomDatabaseSize_Title, "{Name}"), false);
        }
    }

    private int GetFileTypeFilter()
    {
        var filter = 0;
        if (this.Resource.Properties["FileType"] != null
            && !int.TryParse(this.Resource.Properties["FileType"], out filter))
        {
            this.log.Debug("Cannot read property FileType, using default 0");
        }
        return filter;
    }

    private string CreateSwql()
    {
        return @"select PhysicalName, 
                    AutoGrowth, 
                    (dbfile.Size*1024) as [fileSize], 
                    (UsedSpace*1024) as [usedSpace], 
                    (MaxSize*1024) as [_MaxSize], 
					CASE VolumeSize
						WHEN 0 THEN NULL
						ELSE round((dbfile.Size*1024*100)/VolumeSize, 2,1) 
					END as [volumeUsage], 
                    dbfile.Status as _Status, 
                    vol.Status as _VolumeStatus, 
                    (AvailableAutoGrowSpace*1024) as [_AvailableAutoGrow],
                    DatabaseFileID as [_DatabaseFileID]
from {1} as dbfile
left join Orion.Volumes as vol on vol.VolumeID = dbfile.VolumeID
where dbfile.SqlDatabaseID = {0} and FileType='{2}'
".FormatInvariant(this.SqlBlackBoxDatabaseProvider.SqlBlackBoxDatabase.Id,
					SwisEntities.DatabaseFile,
                    this.GetFileTypeFilter());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = this.CreateSwql();
    }

}
