﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_SQLErrors : ApmBaseResource
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(ISqlBlackBoxApplicationProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = Resource.JavaScriptFriendlyID;
        CustomTable.SWQL = string.Format("SELECT toLocal(TimeStamp) AS TimeStamp, Message FROM Orion.APM.SqlServerErrorLog WHERE ApplicationID={0}", 
            GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().SqlBlackBoxApplication.Id);
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.APM_SQLBBContent.SQLErrors_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionSAMAGSQLErrors";
        }
    }
}