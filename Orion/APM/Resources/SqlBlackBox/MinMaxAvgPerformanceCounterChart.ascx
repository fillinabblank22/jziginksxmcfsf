﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MinMaxAvgPerformanceCounterChart.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_MinMaxAvgPerformanceCounterChart" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="contentPlaceHolder" runat="server">
            <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="incompatibleComponentPlaceHolder" runat="server" Visible="False">
            <div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= Resources.APM_SQLBBContent.PerformanceCounterDetails_InvalidComponent %></div>
        </asp:PlaceHolder>
    </Content>
</orion:resourceWrapper>
