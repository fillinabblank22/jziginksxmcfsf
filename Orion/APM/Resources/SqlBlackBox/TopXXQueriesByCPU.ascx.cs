﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_TopXXQueriesByCPU : ApmBaseResource
{
	public override IEnumerable<Type> RequiredInterfaces
	{
		get
		{
			return new[] { typeof(ISqlBlackBoxApplicationProvider) };
		}
	}

	public override string EditControlLocation
	{
		get { return "/Orion/APM/SqlBlackBox/Controls/Top10QueriesSettings.ascx"; }
	}

    public override string EditURL
    {
        get
        {
            var urlBuilder = new UrlBuilder(base.EditURL);
            urlBuilder["ShowOrderBy"] = bool.TrueString;
            urlBuilder["ShowMaxCount"] = bool.TrueString;

            return urlBuilder.Url;
        }
    }

	protected override string DefaultTitle
	{
		get { return Resources.APM_SQLBBContent.Top10Queries_Title; }
	}

	public override string DisplayTitle
	{
		get
		{
            return Title.Replace("{metric}", TitleMetric).Replace("XX", GetStringValue("maxCount", "10"));
		}
	}

	private string TitleMetric;
	protected string Column2Alias { get; private set; }
	protected string Column2 { get; private set; }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected void Page_Load(object sender, EventArgs e)
	{
        var rowCountLimit = GetStringValue("maxCount", "10");

		switch (Resource.Properties["orderBy"])
		{
			case "reads":
				Column2 = "LogicalReadCount";
				Column2Alias = Resources.APM_SQLBBContent.Top10Queries_LogicalReads;
				TitleMetric = Resources.APM_SQLBBContent.Top10Queries_TitleMetric_LogicalReads;
				break;
			case "writes":
				Column2 = "LogicalWriteCount";
				Column2Alias = Resources.APM_SQLBBContent.Top10Queries_LogicalWrites;
				TitleMetric = Resources.APM_SQLBBContent.Top10Queries_TitleMetric_LogicalWrites;
				break;
			case "cpu":
			default:
				Column2 = "CpuTime";
				Column2Alias = Resources.APM_SQLBBContent.Top10Queries_CpuTime;
				TitleMetric = Resources.APM_SQLBBContent.Top10Queries_TitleMetric_CPUTime;
				break;
		}
		

		string where = string.Format("WHERE ApplicationID = {0}", GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().ApmApplication.Id);

		string innerQuery;
		string mainSelect;
		var dbProvider = GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>();
		if (dbProvider != null)	//Resource is on application page
		{
			// Resource is on DB Page
			where += string.Format(" AND SqlDatabaseID = {0}", dbProvider.SqlBlackBoxDatabase.Database.Id);
			innerQuery = string.Format(
				"SELECT TOP {3} QueryID, QueryText, {1} FROM {0} {2} ORDER BY {1} DESC",
				SwisEntities.Query,
				Column2,
				where,
                rowCountLimit);
			mainSelect = string.Format("QueryID AS [_queryId], QueryText, {0}", Column2);
		}
		else					
		{
			// Resource is on DB Page
			innerQuery = string.Format(
                "SELECT TOP {5} QueryID, QueryText, {1}, SqlQuery.Database.Name, '/Orion/StatusIcon.ashx?entity={3}&id=' + ToString(SqlQuery.Database.DatabaseID) + '&status=' + ToString(SqlQuery.Database.Status) as icon, '{4}:' +  ToString(SqlQuery.Database.DatabaseID) as [link] FROM {0} {2} ORDER BY {1} DESC",
				SwisEntities.Query,
				Column2,
				where,
				SwisEntities.Database,
                GetViewLink(SqlBlackBoxConstants.SqlBlackBoxDatabasePrefix),
                rowCountLimit);
			mainSelect = string.Format(	"QueryID AS [_queryId], " +
										"QueryText, " +
										"{0}, " +
										"Name As [name], " +
                                        "icon AS [_IconFor_name], " +
                                        "link AS [_LinkFor_name]",
										Column2);
		}

		CustomTable.UniqueClientID = Resource.JavaScriptFriendlyID;
		CustomTable.SWQL = string.Format("SELECT {0} FROM ({1})",
										 mainSelect,
										 innerQuery);
	}
}