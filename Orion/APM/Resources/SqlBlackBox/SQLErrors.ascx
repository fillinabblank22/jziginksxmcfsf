﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SQLErrors.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_SQLErrors" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Include ID="Include3" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>
		<apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                var expanded_<%= Resource.JavaScriptFriendlyID %> = [];

        	    SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.JavaScriptFriendlyID %>,
                        cls: "sqlErrorLog NeedsZebraStripes",
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: true,
                        allowSearch: false,
                        allowPaging: true,
                        hideShowAll: true,
                        underscoreTemplateId: "#contentTemplate-<%= Resource.JavaScriptFriendlyID %>",
                        headerTitles: ["<%= Resources.APM_SQLBBContent.SQLErrors_DateTime%>", "<%= Resources.APM_SQLBBContent.SQLErrors_Message%>"],
                        defaultOrderBy: "[TimeStamp] DESC"
                    });
                
        	    var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.JavaScriptFriendlyID %>); };
        	    SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
        	    refresh();
        	});
        </script>
        <script id="contentTemplate-<%= Resource.JavaScriptFriendlyID %>" type="text/template">
            <tr>
                <td class="column_DateTime">{{ [Columns[0]].toLocaleString() }}</td>
                <td class="column_Message">{{ [Columns[1]] }}</td>
            </tr>
        </script>
	</Content>
</orion:resourceWrapper>
