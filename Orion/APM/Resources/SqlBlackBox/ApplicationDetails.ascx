﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDetails.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_ApplicationDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="apm" TagName="ApplicationDetails_Management" Src="~/Orion/APM/Controls/Resources/ApplicationDetails_Management.ascx" %>
<%@ Register TagPrefix="apm" TagName="ComponentErrorStatusControl" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>

<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server" />
        <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="I6" File="APM/js/NodeManagement.js" runat="server" />
        <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />

        <script type="text/javascript">
            $().ready(function () {
                SW.APM.TrimStatusDetails();
            });
        </script>
        
        <apm:ApplicationDetails_Management runat="server" ID="managementControl" />

        <table cellspacing="0" class="biggerPadding NeedsZebraStripes">
            <tbody ID="appDetailsSection" runat="server">
                <tr>
                    <td class="PropertyHeader" style="white-space: nowrap;">
                        <%= Resources.APM_SQLBBContent.AppDetails_InstanceName %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= SqlBbApplication.InstanceName %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader" style="white-space: nowrap;">
                        <%= APM_SQLBBContent.ApplicationDetails_LastSuccessfulPoll %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property">
                        <%= SqlBbApplication.LastSuccessfulPoll.ToString("f", System.Globalization.CultureInfo.CurrentCulture) %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader" style="white-space: nowrap;">
                        <%= Resources.APM_SQLBBContent.AppDetails_Status %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property">
                        <%= SqlBbApplication.Status %>
                        <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>
                    </td>
                </tr>
                <% if (ShowApplicationError())
                   { %>
                <tr>
                    <td colspan="3">
                        <%= GetApplicationError().GetStatusDetailsInline()%>
                    </td>
                </tr>
                <% } %>
                <tr>
                    <td class="PropertyHeader" style="white-space: nowrap;">
                        <%= Resources.APM_SQLBBContent.AppDetails_SqlVersion %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= SqlBbApplication.SqlServerVersion %>
                        <% if (!SqlBbApplication.IsSupportedSqlServerVersion) { %>
                        &nbsp;&nbsp;<div class="sw-suggestion sw-suggestion-fail whatsNew"><span class="sw-suggestion-icon"></span>
                                    <%= string.Format(Resources.APM_SQLBBContent.AppDetails_ServerVersionNotSupported,
                                                     SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("OrionSAMAGAppInsightforSQLPermissions"))
                                                     %>
                                   </div>
                        <% } %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader" style="white-space: nowrap;">
                        <%= Resources.APM_SQLBBContent.AppDetails_SqlProductLevel %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= SqlBbApplication.SqlServerLevel %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader" style="white-space: nowrap;">
                        <%= Resources.APM_SQLBBContent.AppDetails_SqlEdition %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= SqlBbApplication.SqlServerEdition %></td>
                </tr>
                <% if (ClusterInfoAvailable)
                   { %>
                <tr>
                    <td class="PropertyHeader" style="white-space: nowrap;">
                        <%= Resources.APM_SQLBBContent.AppDetails_ClusteredWith %>
                    </td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= ClusterInfo %></td>
                </tr>
                <% } %>
            </tbody>
        </table>
            <apm:ComponentErrorStatusControl
                ID="componentErrorStatusControl" 
                runat="server" 
                OnInit="componentErrorStatusControl_Init">
            </apm:ComponentErrorStatusControl>        
    </Content>
</orion:resourceWrapper>