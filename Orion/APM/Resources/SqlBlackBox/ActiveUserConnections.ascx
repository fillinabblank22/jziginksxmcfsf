﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveUserConnections.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_ActiveUserConnections" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<%@ Register TagPrefix="apm" TagName="ConnectionsTable" Src="~/Orion/APM/SqlBlackBox/Controls/ActiveUserConnectionsTable.ascx" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Include ID="Include1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
        <orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>

        <apm:ConnectionsTable runat="server" ID="ConnectionsTable" />
    </Content>
</orion:resourceWrapper>
