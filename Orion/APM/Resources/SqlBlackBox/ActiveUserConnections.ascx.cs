﻿using System.Globalization;

using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_ActiveUserConnections : ApmBaseResource
{
	protected override string DefaultTitle
	{
		get { return Resources.APM_SQLBBContent.ActiveUserConnections_Title; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get
		{
            return new[] { typeof(ISqlBlackBoxDatabaseProvider) };
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment("SAMAGBBActiveUserConnections"); }
    }

    protected void Page_Load(object sender, EventArgs e)
	{
        var provider = GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>();

        ConnectionsTable.ApplicationId = provider.SqlBlackBoxApplication.Id;
        ConnectionsTable.DatabaseId = provider.SqlBlackBoxDatabase.Database.Id;
        ConnectionsTable.ParentNetObject = provider.SqlBlackBoxApplication.NetObjectID;
        ConnectionsTable.UniqueId = Resource.JavaScriptFriendlyID;
	}
}