﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXTablesBySize.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_TopXXTablesBySize" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Common.Models" %>
<%@ Import Namespace="SolarWinds.Internationalization.Extensions" %>
<%@ Import Namespace="Resources" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/js/Resources.js"/>
		<orion:Include ID="I1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
        <orion:Include ID="Include4" runat="server" File="APM/js/Converters.js"/>
		<orion:CustomQueryTable runat="server" ID="CustomTable" />

		<script type="text/javascript">
			$(function () {


				SW.APM.Resources.CustomQuery.initialize(
					{
					    uniqueId: <%= Resource.ID %>,
					    cls: 'tblsBySize',
						initialPage: 0,
						rowsPerPage: 100,                        
						allowSort: true,
						allowSearch: false,
						headerTitles: ["<%= Resources.APM_SQLBBContent.Top10Tables_Name%>", "<%= Resources.APM_SQLBBContent.Top10Tables_Size%>", "<%= Resources.APM_SQLBBContent.Top10Tables_PercentUsageByIndex %>", "<%= Resources.APM_SQLBBContent.Top10Tables_TotalRowCount%>"],
					    defaultOrderBy: "[TotalSpace] DESC",
					    underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>"
					}); 
		        		var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
						SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
		        		refresh();
		        	});	    
		</script>
        
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {# 
                var status=SW.APM.SQLBB.Resources.getSimpleStatus(Columns[4]);
            #}
            <tr>
                <td class='column0'>{{ Columns[0] }}</td>
                <td class='column1'><span class='status_{{ status }}'>{{ SW.Core.Charts.dataFormatters.byte(Columns[1]) }}</span></td>
                <td class='column2'><span class='percentageBar status_{{ status }}' value='{{ Columns[2] }}' status='{{SW.APM.SQLBB.Resources.getSimpleStatus(Columns[4])}}'>{{ Columns[2].toFixed(1) }} %</span></td>
                <td class='column3'><span class='status_{{ status }}'>{{ SW.Core.Charts.dataFormatters.statistic(Columns[3]) }}</span></td>
            </tr>
        
        </script>
	</Content>
</orion:resourceWrapper>
