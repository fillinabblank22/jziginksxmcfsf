﻿using System.Globalization;
using System.IO;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Web.UI;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_MostExpensiveQueries : ApmBaseResource
{
    private Log _logger = new Log();
    private const string ApplicationIdPropertyName = "ApplicationId";
    private const string ControlPatchWebSetting = "Orion.APM.MostExpensiveQueries.IntegrationPlugin";
    private const string IntegrationCallbackFnKey = "IntegrationCallbackFn";

    private Control _dpaoMostExpensiveQueriesControl;

    protected bool IsDpaoPatchLoaded
    {
        get
        {
            return _dpaoMostExpensiveQueriesControl != null && _dpaoMostExpensiveQueriesControl.Visible;
        }
    }

    protected string DpaoMostExpensiveQueriesControlLocation { get; set; }

    protected int ApplicationId { get; set; }

	protected override string DefaultTitle
	{
		get { return Resources.APM_SQLBBContent.MostExpensiveQueries_Title; }
	}

    protected string IntegrationCallbackFn { get; set; }
    
	public override IEnumerable<Type> RequiredInterfaces
	{
		get
		{
			return new[] { typeof(ISqlBlackBoxApplicationProvider) };
		}
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

	protected void Page_Load(object sender, EventArgs e)
	{
		customTable.UniqueClientID = Resource.JavaScriptFriendlyID;
		customTable.SWQL = string.Format(@"
SELECT 	
	QueryID as [_queryID],
	QueryText, 
	Spid, 
	PlanCount,
	ExecutionCount,
	CpuTime,
	PhysicalReadCount,
	LogicalReadCount,
	LogicalWriteCount,
	AvgDuration,
	Login,
	Host,
	db.Name,
	'/Orion/StatusIcon.ashx?entity=Orion.APM.SqlDatabase&id=' + ToString(db.[DatabaseID]) + '&status=' + ToString(db.[Status]) as [_IconFor_Name],
	'/Orion/APM/SqlBlackBox/SqlBlackBoxDatabaseDetails.aspx?NetObject=ABSD:' + ToString(db.[DatabaseID]) as [_LinkFor_Name],
    ServerQueryHandle as [_serverQueryHandle]
FROM {0} 
LEFT JOIN {1} AS db ON SqlDatabaseID = db.DatabaseID WHERE SqlQuery.ApplicationID = {2}",
		SwisEntities.Query,
		SwisEntities.Database,
		GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().ApmApplication.Id);
	}

    protected override void OnLoad(EventArgs e)
    {
        ApplicationId = GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().ApmApplication.Id;

        string controlPatchPath = WebSettingsDAL.GetValue(ControlPatchWebSetting, string.Empty);

        if (!String.IsNullOrEmpty(controlPatchPath))
        {
            var moduleName = controlPatchPath.Split('|')[0];

            if (ModulesCollector.IsModuleInstalled(moduleName))
            {
                _dpaoMostExpensiveQueriesControl = LoadResource(controlPatchPath.Split('|')[1]);

                IPropertyProvider integrationControlProperties = _dpaoMostExpensiveQueriesControl as IPropertyProvider;

                if (integrationControlProperties != null)
                {
                    // set all neccesary properties into integration control
                    integrationControlProperties.Properties.Add(ApplicationIdPropertyName, ApplicationId);

                    // read integration properties from control
                    IntegrationCallbackFn = integrationControlProperties.Properties[IntegrationCallbackFnKey].ToString();
                }

                dpaoPlaceHolder.Controls.Add(_dpaoMostExpensiveQueriesControl);
            }
        }

        base.OnLoad(e);
    }
    
    private Control LoadResource(string resourcePath)
    {
        try
        {
            return  Page.LoadControl(resourcePath);
        }
        catch (Exception ex)
        {
            var msg = string.Format(CultureInfo.InvariantCulture, "failed to load resource '{0}'", resourcePath);
            _logger.Error(msg, ex);
            return new HtmlGenericControl("div")
            {
                InnerText = msg
            };
        }
    }

}