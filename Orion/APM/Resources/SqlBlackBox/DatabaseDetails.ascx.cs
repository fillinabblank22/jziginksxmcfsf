﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.BlackBox.Sql.Web.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.BlackBox.Sql.Common.Models;
using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.UI.DisplayStrategies;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_DatabaseDetails : ApmBaseResource
{
    protected const int CoreStatusUnmanaged = 9;

    protected override string DefaultTitle
    {
        get
        {
            return Resources.APM_SQLBBContent.DatabaseDetails_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment("OrionSAMAGSQLBBDBDetails"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(ISqlBlackBoxDatabaseProvider) };
        }
    }

    private SqlBlackBoxDatabase database;
    private bool databaseInitialized;

    public SqlBlackBoxDatabase Database
    {
        get
        {
            if (!databaseInitialized)
            {
                var provider = GetInterfaceInstance<ISqlBlackBoxDatabaseProvider>();
                if (provider != null)
                {
                    database = provider.SqlBlackBoxDatabase;
                }
                if (database == null)
                {
                    database = new SqlBlackBoxDatabase(new Database()
                    {
                    });
                }
                databaseInitialized = true;
            }
            return database;
        }
    }

    public DatabaseState DbState
    {
        get
        {
            return (DatabaseState)Database.Database.OperationalState;
        }
    }

    public DatabaseRecoveryModel DbRecoveryModel
    {
        get
        {
            return (DatabaseRecoveryModel)Database.Database.RecoveryModel;
        }
    }

    public string StatusIcon
    {
        get
        {
            return string.Format("/Orion/StatusIcon.ashx?entity={0}&id={1}&status={2}", SwisEntities.Database, Database.Id.ToString(), Database.StatusId.ToString());
        }
    }

	public Status Status
	{
		get
		{
			return Database.StatusId.OrionApmStatus();
		}
	}

    public ApmStatus ApmStatus
    {
        get
        {
            return new ApmStatus(Status);
        }
    }

    public string DatabaseLink
    {
        get
        {
            return string.Format("{0}:{1}", GetViewLink(SqlBlackBoxConstants.SqlBlackBoxDatabasePrefix), Database.Id);
        }
    }

    public bool MirroringFound
    {
        get
        {
            if (mirroringInfo == null)
                this.LoadMirroringInfo();

            return mirroringInfo != string.Empty;
        }
    }

    private string mirroringInfo;
    public string MirroringInfo
    {
        get { return MirroringFound ? mirroringInfo : Resources.APM_SQLBBContent.DatabaseDetails_NoMirroring; }
    }

    private string mirroringRole;
    public string MirroringRole
    {
        get { return MirroringFound ? mirroringRole: Resources.APM_SQLBBContent.DatabaseDetails_NoMirroring; }
    }

    private string mirroringState;
    public string MirroringState
    {
        get { return MirroringFound ? mirroringState: Resources.APM_SQLBBContent.DatabaseDetails_NoMirroring; }
    }

    private void LoadMirroringInfo()
    {
        var table =
            SwisProxy.ExecuteQuery(
                @"SELECT this.Role, this.State, this.PartnerInstance, partner.DatabaseID, partner.SqlDatabase.Status, partner.SqlDatabase.Name,
partner.SqlDatabase.SqlServer.Node.Status as NodeStatus, partner.SqlDatabase.SqlServer.Node.NodeID, partner.SqlDatabase.SqlServer.Node.Caption
FROM Orion.APM.SqlDatabaseMirroring this
LEFT JOIN Orion.APM.SqlDatabaseMirroring partner ON this.MirroringID = partner.MirroringID AND this.DatabaseID <> partner.DatabaseID
WHERE this.DatabaseID = @DatabaseID",
                new Dictionary<string, object> { { "DatabaseID", this.Database.Id } });

        if (table.Rows.Count > 0)
        {
            var row = table.Rows[0];

            mirroringRole = ((DatabaseMirroringRole)(byte)row["Role"]).LocalizedLabel();
            mirroringState = ((DatabaseMirroringState)(byte)row["State"]).LocalizedLabel();

            string databaseInfo;
            string nodeInfo;

            if (row.IsNull("DatabaseID"))
            {
                databaseInfo = string.Format(
                    "<img src='/Orion/StatusIcon.ashx?entity={0}&id={1}&status=100' />&nbsp;{2}",
                    SwisEntities.Database,
                    this.Database.Id,
                    this.Database.Name);

                nodeInfo = row["PartnerInstance"].ToString();
            }
            else
            {
                databaseInfo = string.Format(
                    "<img src='/Orion/StatusIcon.ashx?entity={0}&id={1}&status={2}' />&nbsp;<a href='{3}:{1}'>{4}</a>",
                    SwisEntities.Database,
                    row["DatabaseID"],
                    row["Status"],
                    GetViewLink(SqlBlackBoxConstants.SqlBlackBoxDatabasePrefix),
                    row["Name"]);
                nodeInfo = string.Format(
                    "<img src='/Orion/StatusIcon.ashx?entity={0}&id={1}&status={2}' />&nbsp;<a href='{3}:{1}'>{4}</a>",
                    "Orion.Nodes",
                    row["NodeID"],
                    row["NodeStatus"],
                    GetViewLink("N"),
                    row["Caption"]);
            }
            mirroringInfo = string.Format(
                Resources.APM_SQLBBContent.DatabaseDetails_MirroredMsg, databaseInfo, nodeInfo);
        }
        else
        {
            mirroringInfo = string.Empty;
        }
    }

    public string LastBackupTimeElapsed
    {
        get
        {
            if (Database.Database.LastBackup.HasValue && Database.Database.LastBackup.Value > DateTime.MinValue)
                return string.Format(Resources.APM_SQLBBContent.DatabaseDetails_ElapsedTime, new ApmTimeSpan(DateTime.UtcNow - Database.Database.LastBackup.Value));
            else
                return string.Empty;
        }
    }

    private INodeManagementTasksDisplayStrategy nodeManagementTasksDisplayStrategy;
    protected INodeManagementTasksDisplayStrategy NodeManagementTasksDisplayStrategy
    {
        get
        {
            return nodeManagementTasksDisplayStrategy 
                ?? (nodeManagementTasksDisplayStrategy = ServiceLocatorForWeb.GetServiceForWeb<ITasksDisplayStrategyFactory>().CreateTasksDisplayStrategy(this.GetNode()));
        }
    }

    private Node GetNode()
    {
        FillNodeSubTypeIfNotSet(Database.SqlBlackBoxApplication);
        return Database.SqlBlackBoxApplication.Node;
    }

    protected int GetRtMonitorCredId()
    {
        return NodeManagementTasksDisplayStrategy.GetWorkingWindowsCredentialId();
    }

    protected bool ShowServiceManagerLink
    {
        get
        {
            return NodeManagementTasksDisplayStrategy.IsServiceManagerSupported();
        }
    }

    protected bool ShowEventExplorerLink
    {
        get
        {
            return NodeManagementTasksDisplayStrategy.IsEventExplorerSupported();
        }
    }
    
    protected void componentErrorStatusControl_Init(object sender, EventArgs e)
    {
        componentErrorStatusControl.Initialize(
            ComponentDal.GetComponentsWithErrorByApplicationItemID(Database.ApplicationId, Database.Id).ToList());
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        alertSuppressionStatus.EntityUri = ApmApplication.SwisUri;
        alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;
    }
}
