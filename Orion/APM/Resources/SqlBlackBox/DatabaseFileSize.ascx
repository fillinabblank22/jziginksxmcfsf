﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseFileSize.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_DatabaseFileSize" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <!-- This element is used to access ResourceWrapper element for this resource so that 
        we can add extra CSS class to it to fix chart tooltips clipping. (FB109023) -->
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper> 