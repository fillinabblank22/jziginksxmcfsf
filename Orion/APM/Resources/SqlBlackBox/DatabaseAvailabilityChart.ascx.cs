﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_DatabaseAvailabilityChart : ApmCustomAvailabilityChartResource, IResourceIsInternal
{
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (ISqlBlackBoxDatabaseProvider)}; }
    }

    bool IResourceIsInternal.IsInternal
    {
        get { return true; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }
}