﻿using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.Models;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_ApplicationDetails :  ApmBaseResource
{
	public override IEnumerable<Type> RequiredInterfaces
	{
		get
		{
			return new[] { typeof(ISqlBlackBoxApplicationProvider) };
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.APM_SQLBBContent.AppDetails_Title; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

	public SqlBlackBoxApplication SqlBbApplication
	{
		get
		{
			return base.GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().SqlBlackBoxApplication;
		}
	}

    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        FillNodeSubTypeIfNotSet(ApmApplication);    // after setting proper node's NodeSubType managementControl works correctly

	    managementControl.Resource = this;
        managementControl.ApmApplication = ApmApplication;
        managementControl.ItemsPlaceholder = appDetailsSection;

        managementControl.InitManagementTasks();

	    alertSuppressionStatus.EntityUri = SqlBbApplication.SwisUri;
	    alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;
	}

    private string clusterInfo = null;
    public string ClusterInfo
    {
        get
        {
            if (clusterInfo == null)
            {
                var table = SwisProxy.ExecuteQuery(
                    @"SELECT cn.NodeName, n.NodeID, n.Status, n.Caption
FROM Orion.APM.SqlClusterNode cn
LEFT JOIN Orion.Nodes n ON (n.DNS = cn.NodeName OR n.DNS LIKE cn.NodeName + '.%')
WHERE cn.ApplicationID = @ApplicationID",
                    new Dictionary<string, object>
                        {
                            { "ApplicationID", this.SqlBbApplication.Id }
                        });
                var sb = new StringBuilder();
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (i > 0)
                        sb.Append(", ");
                    var row = table.Rows[i];
                    if (row.IsNull("NodeID"))
                    {
                        sb.Append(row["NodeName"]);
                    }
                    else
                    {
                        sb.AppendFormat(
                            "<img src='/Orion/StatusIcon.ashx?entity={0}&id={1}&status={2}' />&nbsp;<a href='{3}:{1}'>{4}</a>",
                            "Orion.Nodes",
                            row["NodeID"],
                            row["Status"],
                            GetViewLink("N"),
                            row["Caption"]);
                    }
                }
                clusterInfo = sb.ToString();
            }
            return clusterInfo;
        }
    }

    public bool ClusterInfoAvailable
    {
        get { return !string.IsNullOrEmpty(this.ClusterInfo); }
    }
    
    protected void componentErrorStatusControl_Init(object sender, EventArgs e)
    {
        bool includeUndefinedComponents = GetApplicationError().Status == Status.Available;
        var componentDal = new ComponentDal();
        List<ComponentErrorStatus> components = componentDal.GetComponentsWithErrorByApplicationID(SqlBbApplication.Id, includeUndefinedComponents).ToList();
        componentErrorStatusControl.Initialize(components);        
    }

    private string LoadApplicationError()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            string query = @"SELECT ErrorMessage FROM Orion.APM.CurrentApplicationStatus WHERE ApplicationID = @ApplicationID";
            var status = swis.Query(query, new Dictionary<string, object> { { "ApplicationID", SqlBbApplication.Id } });
            if (status.Rows.Count > 0)
            {
                if (status.Rows[0][0] != DBNull.Value)
                    return (string)status.Rows[0][0];
                else
                    return null;
            }
            else
            {
                return null;
            }
        }
    }

    protected bool ShowApplicationError()
    {
        return GetApplicationError().Status != Status.Available;
    }

    private ApmMonitorError applicationError;

    protected ApmMonitorError GetApplicationError()
    {
        if (applicationError == null)
        {
            string errorMessage = LoadApplicationError();
            if (errorMessage == null)
            {
                applicationError = new ApmMonitorError()
                {
                    Status = Status.Undefined //will be displayed as 'Initial poll in progress'
                };
            }
            else if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                applicationError = new ApmMonitorError(ApmErrorCode.Ok, 0, StatusCodeType.None, errorMessage)
                {
                    Status = Status.Undefined
                };
            }
            else
            {
                applicationError = new ApmMonitorError(ApmErrorCode.Ok, 0, StatusCodeType.None, string.Empty)
                {
                    Status = Status.Available
                };
            }
        }
        return applicationError;
    }
}
