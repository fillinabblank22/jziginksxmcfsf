﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXQueriesByCPU.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_TopXXQueriesByCPU" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Common" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Web" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <orion:Include ID="Include3" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>
        <script type="text/javascript">
            $(function() {
                var expanded_<%= Resource.JavaScriptFriendlyID %> = [];

                SW.APM.Resources.CustomQuery.initialize(
                {
                    uniqueId: <%= Resource.JavaScriptFriendlyID %>,
                    cls: "topXXQueries NeedsZebraStripes",
                    initialPage: 0,
                    rowsPerPage: 100,
                    allowSort: true,
                    allowSearch: false,
                    allowPaging: false,
                    headerTitles: ["<%= APM_SQLBBContent.Top10Queries_QueryText %>", "<%= Column2Alias %>", "<%= APM_SQLBBContent.Top10Queries_DbName %>"],
                    defaultOrderBy: "[<%= Column2 %>] DESC",
                    customHeaderGenerator: function(columns) {
                        var headerRow = $("<tr/>")
                            .addClass("HeaderRow");

                        var cell = $("<td/>")
                            .addClass("ReportHeader");
                        cell.appendTo(headerRow);
                        columns[0].appendTo(headerRow);

                        var cpuTimeCol = columns[1].appendTo(headerRow);
                        cpuTimeCol.addClass("counterColumn");

                        if (columns.length > 2) //On App view display also DB name
                        {
                            columns[2].appendTo(headerRow);
                        }

                        return headerRow;
                    },
                    customRowGenerator: function(columns) {
                        var result = $("<tr/>");

                        var cell = $("<td/>").appendTo(result);
                        cell.css("vertical-align", "text-top");
                        var content = $("<a/>");
                        content.attr("href", "javascript:void(0);");
                        content.click(function() {
                            var row = $(".tQuery_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);
                            if (row.hasClass("topQueries_Collapsed")) {
                                expanded_<%= Resource.JavaScriptFriendlyID %>.push(columns[0]);
                                row.removeClass("topQueries_Collapsed");
                                row.addClass("topQueries_Expanded");
                                $(".tQuery_Button_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]).attr("src", "/Orion/images/Button.Collapse.gif");
                            } else {
                                expanded_<%= Resource.JavaScriptFriendlyID %>.pop(columns[0]);
                                row.removeClass("topQueries_Expanded");
                                row.addClass("topQueries_Collapsed");
                                var button = $(".tQuery_Button_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);
                                button.attr("src", "/Orion/images/Button.Expand.gif");
                            }
                        });

                        var img = $("<img alt='.' />");
                        img.addClass("tQuery_Button_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);


                        img.appendTo(content);
                        content.appendTo(cell);


                        cell = $("<td/>").appendTo(result);
                        var subViewId = <%= ViewManager.GetViewByViewKey(SqlBlackBoxConstants.ViewKeyExpensiveQueries) ==null ? "null" : ViewManager.GetViewByViewKey(SqlBlackBoxConstants.ViewKeyExpensiveQueries).ViewID.ToString() %>;
                        if (subViewId != null) {
                            content = $("<a/>");
                            content.attr("href", "/Orion/APM/SqlBlackBox/SqlBlackBoxApplicationDetails.aspx?NetObject=ABSA:<%= GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().ApmApplication.Id %>&ViewID="+subViewId);
                        } else {
                            content = $("<span/>");
                        }
                        content.appendTo(cell);
                        content.addClass("tQuery_<%= Resource.JavaScriptFriendlyID %>_" + columns[0]);
                        if ($.inArray(columns[0], expanded_<%= Resource.JavaScriptFriendlyID %>) > -1) {
                            content.addClass("topQueries_Expanded");
                            img.attr("src", "/Orion/images/Button.Collapse.gif");
                        } else {
                            content.addClass("topQueries_Collapsed");
                            img.attr("src", "/Orion/images/Button.Expand.gif");
                        }

                        content.text(columns[1]);

                        cell = $("<td/>").appendTo(result);
                        cell.css({
                            "vertical-align": "text-top",
                            "text-align": "center"
                        });
                        content = $("<span />");
                        content.appendTo(cell);
                        content.text(columns[2]);

                        if (columns.length > 3) //On App view display also DB name
                        {
                            cell = $("<td/>").appendTo(result);
                            cell.css({
                                "vertical-align": "text-top",
                                "text-align": "left",
                                "white-space": "nowrap"
                            });
                            var img = $("<img alt='.' />");
                            img.attr("src", columns[4]);
                            img.appendTo(cell);

                            content = $("<a />").appendTo(cell);
                            content.attr("href", columns[5]);
                            content.addClass("NoTip");
                            content.text(columns[3]);
                        }


                        return result;
                    }
                });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.JavaScriptFriendlyID %>); };
                SW.Core.View.AddOnRefresh(refresh, "<%= CustomTable.ClientID %>");
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>