﻿using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Specialized;

[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_MinMaxAvgPerformanceCounterChart : APMStandardChartBaseResource, IResourceIsInternal
{
    private SqlBlackBoxNetObjectManager netObjectManager;

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected string ChartName
    {
        get { return Resource.Properties.ContainsKey("ChartName") ? Resource.Properties["ChartName"] : "MinMaxAvgPerformanceCounter"; }
    }
    
    protected override string NetObjectPrefix
    {
        get { return "ABMMAPC"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(ISqlBlackBoxStatisticProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    protected SqlBlackBoxStatistic SqlBlackBoxStatistic
    {
        get
        {
            return netObjectManager.SqlBlackBoxStatistic;
        }
    }

    protected override ApmMonitor Monitor
    {
        get
        {
            return SqlBlackBoxStatistic;
        }
    }

    protected override string StatisticColumnName
    {
        get
        {
            var properties = GetResourcePropertiesAsDictionary();
            if (EvidenceType == ComponentEvidenceType.DynamicEvidence)
            {
                AssignedToResourceInfo info;
                AssignedToResourceInfo.TryCreate(Request, Resource, out info);

                if (info[AssignedToResourceInfo.ColumnNames] != null)
                {
                    return info[AssignedToResourceInfo.ColumnNames].ToString();
                }

                if (properties.ContainsKey("StatisticName"))
                {
                    return properties["StatisticName"];
                }
            }
            
            return Resource.Properties.ContainsKey("StatisticColumnName") ? Resource.Properties["StatisticColumnName"] : base.StatisticColumnName;
        }
    }

    public override void ShowHeaderBar(bool show)
    {
        Wrapper.ShowHeaderBar = show;
    }

    protected override ComponentEvidenceType EvidenceType
    {
        get
        {
            return Monitor != null ? Monitor.BaseComponent.EvidenceType : ComponentEvidenceType.None;
        }
    }

    public override string DisplayTitle
    {
        get
        {
            return Monitor != null ? Monitor.Name : string.Empty;
        }
    }

    protected override NameValueCollection GetCustomChartParameters()
    {
        return new NameValueCollection();
    }


    protected override System.Collections.Generic.Dictionary<string, object> GenerateDisplayDetails()
    {
        if (SqlBlackBoxStatistic == null)
        {
            return new Dictionary<string, object>();
        }
        // js settings
        Resource.Properties["OptionsSetting_1"] = String.Format("options.seriesTemplates.Avg_Statistic_Data.name = '{0}'", AvgDataSeriesName);
        Resource.Properties["OptionsSetting_2"] = String.Format("options.seriesTemplates.MinMax_Statistic_Data.name = '{0}'", MinMaxDataSeriesName);
        Resource.Properties["OptionsSetting_3"] = String.Format("options.yAxis[0].title.text = '{0}'", AxisTitle);
        Resource.Properties["OptionsSetting_4"] = String.Format("options.yAxis[0].unit = '{0}'", Units);

        var details = base.GenerateDisplayDetails();

        details["title"] = String.Empty;    // no title in chart

        return details;
    }

    private string AvgDataSeriesName 
    {
        get
        {
            return String.Format(Resources.APM_SQLBBContent.MinMaxAvgPerformCounterChart_AvgDataSeriesName, Monitor.Name);
        }
    }

    private string MinMaxDataSeriesName
    {
        get
        {
            return String.Format(Resources.APM_SQLBBContent.MinMaxAvgPerformCounterChart_MinMaxDataSeriesName, Monitor.Name);
        }
    }

    private string AxisTitle
    {
        get
        {
            return Monitor.Name;
        }
    }

    private string Units 
    {
        get
        {
			ApmSettingValue settingValue;
            if (Monitor.ComponentSettings.TryGetValue("CounterUnit", out settingValue))
            {
                return MultiChartDataHelper.ChartFormatterToFormatterJS(settingValue.Value.Value);
            }

            return MultiChartDataHelper.ChartFormatterToFormatterJS(SolarWinds.APM.Web.UI.Formatters.ChartFormatter.Statistic.ToString());
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        netObjectManager = new SqlBlackBoxNetObjectManager(this);
        if (SqlBlackBoxStatistic == null)
        {
            incompatibleComponentPlaceHolder.Visible = true;
            contentPlaceHolder.Visible = false;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ChartLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);

        HandleInit(WrapperContents);
    }

    public bool IsInternal
    {
        get { return true; }
    }
}
