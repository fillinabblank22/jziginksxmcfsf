﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllDatabases.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_AllDatabases" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Common.Models" %>
<%@ Import Namespace="SolarWinds.Internationalization.Extensions" %>
<%@ Import Namespace="Resources" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Include ID="I1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
		<apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        initialPage: 0,
                        rowsPerPage: 100,                        
                        allowSort: true,
                        allowSearch: false,
                        headerTitles: ["<%= Resources.APM_SQLBBContent.AllDbs_DbName %>", "<%= Resources.APM_SQLBBContent.AllDbs_DbStatus %>", "<%= Resources.APM_SQLBBContent.AllDbs_DbSize %>", "<%= Resources.APM_SQLBBContent.AllDbs_DbLogSize %>"],
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        cls: "NeedsZebraStripes"
                    }); 
        	    var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
        	    SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
        	    refresh();
        	});	    
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var name = " " + Columns[0];
                var status;
                <% foreach (DatabaseState ds in Enum.GetValues(typeof(DatabaseState))) 
				    { %>if (Columns[1] == <%= (int)ds %> ) status = "<%= ds.LocalizedLabel() %>";
				<% } %>
                var dbSize;
                if (Columns[4] == "") {
                    dbSize = "-";
                } else {
                    dbSize = SW.Core.Charts.dataFormatters.byte(Columns[4], 0);
                }
                var logSize;
                if (Columns[5] == "") {
                    logSize = "-";
                } else {
                    logSize = SW.Core.Charts.dataFormatters.byte(Columns[5], 0);
                }
            #}
            <tr>
                <td class="column0">
                    <img src="{{ Columns[2] }}"/>
                    <a href="{{ Columns[3] }}">{{ name }}</a></td>
                <td class="column1"><span>{{ status }}</span></td>
                <td class="column4"><span>{{ dbSize }}</span></td>
                <td class="column5"><span>{{ logSize }}</span></td>
            </tr>
        </script>
	</Content>
</orion:resourceWrapper>
