﻿using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_AllPerformanceCounters : SqlBlackBoxStatisticResourceBase
{
	protected override string DefaultTitle
	{
		get { return Resources.APM_SQLBBContent.AllPerformanceCounters_Title; }
	}

	public override SolarWinds.Orion.Web.UI.ResourceLoadingMode ResourceLoadingMode
	{
		get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax; }
	}

	protected bool IsApplicationItemSpecific
	{
		get { return SqlBlackBoxStatistic.IsDatabaseSpecific; }
	}

	protected string CustomApplicationType
	{
        get { return SqlBlackBoxStatistic.Application.CustomType; }
	}

	protected int ApplicationId
	{
        get { return SqlBlackBoxStatistic.Application.Id; }
	}

	protected int ApplicationItemId
	{
		get { return SqlBlackBoxStatistic.ApplicationItemId ?? -1; }
	}

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        bool compatible = (this.SqlBlackBoxStatistic != null);
        this.incompatibleComponentPlaceHolder.Visible = !compatible;
        this.contentPlaceHolder.Visible = compatible;
    }
}
