﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using SolarWinds.APM.Web.DisplayTypes;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_PerformanceCounterDetails : SqlBlackBoxStatisticResourceBase
{
	protected override string DefaultTitle
	{
        get { return Resources.APM_SQLBBContent.PerformanceCounterDetails_Title; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    private DataRow ResultDataRow { get; set; }

    protected string CurrentValue
    {
        get
        {
            var currentValue = GetResultValue<double?>("CurrentValue");
            var retval = currentValue.HasValue ? currentValue.Value.ToString(CultureInfo.InvariantCulture) : Resources.APM_SQLBBContent.PerformanceCounterDetails_Value_NA;

            return retval;
        }
    }

    protected string CurrentValueUnit 
    {
        get { return MultiChartDataHelper.ChartFormatterToFormatterJS(GetResultValue<string>("CounterUnits")); }
    }

    protected string ComponentStatusDescription
    {
        get { return GetResultValue<string>("ComponentStatusDescription"); }
    }

    protected string ComponentName 
    {
        get { return GetResultValue<string>("ComponentName"); }
    }

    protected string CategoryDisplayName 
    {
        get { return GetResultValue<string>("CategoryDisplayName"); }
    }

    protected string ComponentTypeDisplayName
    {
        get { return GetResultValue<string>("ComponentTypeDisplayName"); }
    }

    protected string ExpertKnowledge
    {
        get { return GetResultValue<string>("ExpertKnowledge"); }
    }

    protected double? ThresholdWarningValue 
    {
        get { return GetThresholdResultValue("ThresholdWarningValue"); }
    }

    protected double? ThresholdCriticalValue 
    {
        get { return GetThresholdResultValue("ThresholdCriticalValue"); }
    }

    protected string[] ValidThresholdValues
    {
        get
        {
            var retval = new List<double?>() { ThresholdWarningValue, ThresholdCriticalValue }
                            .Where(x => x.HasValue)
                            .Select(x => x.Value.ToString(CultureInfo.InvariantCulture))
                            .ToArray();
            
            return retval;
        }
    }

    protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);
        if (SqlBlackBoxStatistic == null)
        {
            incompatibleComponentPlaceHolder.Visible = true;
            statisticPlaceHolder.Visible = false;
        }
        else
        {
            managementControl.Resource = this;
            managementControl.ApmApplication = ApmApplication;
            managementControl.ItemsPlaceholder = appDetailsSection;

            managementControl.InitManagementTasks();

            imgStatus.ImageUrl = ComponentImageUrl;

            ResultDataRow = this.GetQueryResult(SqlBlackBoxStatistic.Id);

            componentName.Text = ComponentName;
            componentStatus.Text = ComponentStatusDescription;
            componentGroup.Text = CategoryDisplayName;
            componentType.Text = ComponentTypeDisplayName;
            expertKnowledge.Text = ExpertKnowledge;
        }
        
	}

    private DataRow GetQueryResult(long componentId)
    {
        DataRow retval = null;

        var queryFormat = @"SELECT 
                            c.ShortName AS ComponentName,
                            c.ComponentDefinition.DisplayName AS ComponentTypeDisplayName, 
                            c.StatusDescription AS ComponentStatusDescription,
                            cc.DisplayName AS CategoryDisplayName,
                            pe.AvgStatisticData AS CurrentValue,
                            cts.Value AS CounterUnits,
                            c.UserDescription AS ExpertKnowledge,
                            tbc.Warning AS ThresholdWarningValue,
                            tbc.Critical AS ThresholdCriticalValue
                            FROM Orion.APM.Component AS c
                            LEFT JOIN Orion.APM.ComponentTemplate AS ct ON c.TemplateID = ct.ID
                            LEFT JOIN Orion.APM.ComponentCategory AS cc ON ct.ComponentCategoryID = cc.CategoryID
                            LEFT JOIN Orion.APM.PortEvidence pe ON c.CurrentStatus.ComponentStatusID = pe.ComponentStatusID
                            LEFT JOIN Orion.APM.ComponentTemplateSetting cts ON c.TemplateID = cts.ComponentTemplateID AND cts.Key = 'CounterUnit'
                            LEFT JOIN Orion.APM.ThresholdsByComponent tbc ON c.ComponentID = tbc.ComponentID
                            WHERE ComponentID = {0}";

        var query = String.Format(queryFormat, componentId);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var rows = swis.Query(query).Rows;
            if (rows.Count > 0)
            {
                retval = rows[0];
            }
        }

        return retval;
    }

    private double? GetThresholdResultValue(string columnName)
    {
        var retval = GetResultValue<double?>(columnName);

        if (retval.HasValue && (retval.Value.Equals(double.MinValue) || retval.Value.Equals(double.MaxValue)))
        {
            retval = null;
        }

        return retval;
    }

    private T GetResultValue<T>(string columnName, T defaultValue = default(T))
    {
        var retval = defaultValue;

        if (ResultDataRow != null && !ResultDataRow.IsNull(columnName))
        {
            retval = (T)ResultDataRow[columnName];
        }

        return retval;
    }

    protected bool ShowCounterError()
    {
        return SqlBlackBoxStatistic != null && SqlBlackBoxStatistic.CurrentError.ErrorCode > 0;
    }

    protected ApmMonitorError GetCounterError()
    {
        return SqlBlackBoxStatistic != null ? SqlBlackBoxStatistic.CurrentError : null;
    }
}
