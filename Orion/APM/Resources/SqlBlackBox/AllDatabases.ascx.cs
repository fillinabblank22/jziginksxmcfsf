﻿using SolarWinds.APM.BlackBox.Sql.Common;
using SolarWinds.APM.BlackBox.Sql.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Sql.Web;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, MetadataSearchTags.Sql)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_SqlBlackBox_AllDatabases : ApmBaseResource
{
	protected override string DefaultTitle => APM_SQLBBContent.Web_AllDatabases_Resource_Title;

    public override IEnumerable<Type> RequiredInterfaces => new []{typeof(ISqlBlackBoxApplicationProvider)};

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    protected void Page_Load(object sender, EventArgs e)
	{
		CustomTable.UniqueClientID = Resource.ID;
		CustomTable.SWQL = string.Format("SELECT " +
								"Name as [name]," +
								"OperationalState AS [state], " +
								"'/Orion/StatusIcon.ashx?entity={0}&id=' + ToString(DatabaseID) + '&status=' + ToString(db.[Status]) AS [_IconFor_name], " +
								"'{1}:' +  ToString(DatabaseID) as [_LinkFor_name], " +
								//Size in KB
								"(SELECT SUM(Size) * 1024 AS Size FROM {4} AS dbF WHERE dbF.SqlDatabaseID = db.DatabaseID AND dbF.FileType != 1) AS [dbSize], " +	//Type 0 - ROW, Type 2 - FILESTREAM
								"(SELECT SUM(Size) * 1024 AS Size FROM {4} AS dbF WHERE dbF.SqlDatabaseID = db.DatabaseID AND dbF.FileType = 1) AS [logSize] " +		//Type 1 - LOG
								"FROM {3} AS db " +
								"WHERE ApplicationID = {2}",
								SwisEntities.Database,
								GetViewLink(SqlBlackBoxConstants.SqlBlackBoxDatabasePrefix),
								GetInterfaceInstance<ISqlBlackBoxApplicationProvider>().ApmApplication.Id,
								SwisEntities.Database,
								SwisEntities.DatabaseFile);
	}
}