﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXIndexes_Base.ascx.cs" Inherits="Orion_APM_Resources_SqlBlackBox_TopXXIndexes_Base" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<%@ Import Namespace="SolarWinds.APM.BlackBox.Sql.Common.Models" %>
<%@ Import Namespace="SolarWinds.Internationalization.Extensions" %>
<%@ Import Namespace="Resources" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Include ID="I1" File="/APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
        <orion:Include ID="Include1" runat="server" File="APM/js/Converters.js"/>
        <orion:Include ID="Include2" runat="server" File="APM/SqlBlackBox/js/Resources.js"/>
		<orion:Include ID="Include3" runat="server" File="APM/SqlBlackBox/Styles/Resources.css"/>
		<apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
        	$(function () {
        	    var tableNameHeader, indexNameHeader, indexSizeHeader, fragmentationHeader;
        		SW.APM.Resources.CustomQuery.initialize(
                    {
                    	uniqueId: <%= Resource.ID %>,
                        cls: "topIndexes NeedsZebraStripes",
                    	initialPage: 0,
                    	rowsPerPage: 100,                        
                    	allowSort: true,
                    	allowSearch: false,
                    	headerTitles: ["<%= Resources.APM_SQLBBContent.Top10Indexes_TableName %>", "<%= Resources.APM_SQLBBContent.Top10Indexes_IndexName %>", "<%= Resources.APM_SQLBBContent.Top10Indexes_IndexSize %>", "<%= Resources.APM_SQLBBContent.Top10Indexes_Fragmentation %>"],
                    	defaultOrderBy: "i.Fragmentation DESC",
                    	customHeaderGenerator: function(columns) {
                    		var headerRow = $('<tr/>')
								.addClass('HeaderRow');

                    		tableNameHeader = columns[0];
                    		indexNameHeader = columns[1];
	                        indexSizeHeader = columns[2];
	                        fragmentationHeader = columns[3];
	                        fragmentationHeader.wrapInner("<span style='padding-left: 12px;'></span>"); // aligns column header to start with the data in cells


                    		tableNameHeader.appendTo(headerRow).css({ "max-width": "30%" });
                    		indexNameHeader.appendTo(headerRow).css({ "max-width": "30%" });
                    		indexSizeHeader.appendTo(headerRow).css({ "max-width": "15%", "min-width": "52px" });
                    		fragmentationHeader.appendTo(headerRow).css({ "width": "150px" });

                    		return headerRow;
                    	},
                    	customRowGenerator: function(dataRowColumns) {
                    	    var tr = $('<tr/>');

	                        var addCssWidthToElement = function(header, domElementInsideCell) {
	                            var w = header.width();
	                            if (w > 0) {
	                                domElementInsideCell.css({ "width": w + "px" });
	                            }
	                        };

	                        var createTableNameCell = function() {
	                            var tdEl = $("<td/>").appendTo(tr);
	                            var pEl = $("<p/>").appendTo(tdEl).addClass("cell");
	                            addCssWidthToElement(tableNameHeader, pEl);

	                            var tableName = dataRowColumns[0];
	                            pEl.text(tableName);
	                        }

                            var createIndexNameCell = function() {
                                var tdEl = $("<td/>").appendTo(tr);
                                var pEl = $("<p/>").appendTo(tdEl).addClass("cell").addClass("indexColumn");
                                addCssWidthToElement(indexNameHeader, pEl);

                                var indexName = dataRowColumns[1];
                                pEl.attr("title", indexName);
                                pEl.text(indexName);
                            }

                            var createIndexSizeCell = function() {
                                var tdEl = $("<td/>").appendTo(tr);
                                var pEl = $("<p/>").appendTo(tdEl).addClass("cell").addClass("indexColumn");
                                addCssWidthToElement(indexSizeHeader, pEl);

                                var indexSize = dataRowColumns[2];
                                var indexSizeInBytes = indexSize * 1024;
                                pEl.html(SW.Core.Charts.dataFormatters.byte(indexSizeInBytes, { baseConversionValue: 1024 }));
                            }

                            var createFragmentationCell = function() {
                                var fragmentation = dataRowColumns[3].toFixed(1);
                                var status = dataRowColumns[4];
                                var humanfriendlyStatus = SW.APM.SQLBB.Resources.getSimpleStatus(status);

                                var tdEl = $("<td/>").addClass("fragmentationColumn").appendTo(tr);
                                addCssWidthToElement(fragmentationHeader, tdEl);
                                tdEl.html("<span class='percentageBar' value='" + fragmentation + "' value2='x' status='" + humanfriendlyStatus + "'>" + fragmentation + "%</span>");
                            };

	                        createTableNameCell();
	                        createIndexNameCell();
	                        createIndexSizeCell();
	                        createFragmentationCell();
                    		
                    		return tr;
                    	}
                    }); 
        		var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
        		SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
        		refresh();
        	});	    
        </script>
	</Content>
</orion:resourceWrapper>