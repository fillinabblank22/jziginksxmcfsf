﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXDatabasesByActiveUserConnections.ascx.cs" Inherits="Orion_APM_Resources_Charting_TopXXDatabasesByActiveUserConnections" %>
<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="False">
    <Content>
        <!-- This element is used to access ResourceWrapper element for this resource so that 
        we can add extra CSS class to it to fix chart tooltips clipping. (FB109023) -->
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper> 