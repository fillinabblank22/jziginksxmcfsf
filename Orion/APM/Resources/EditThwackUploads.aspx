<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" 
    CodeFile="EditThwackUploads.aspx.cs" Inherits="Orion_APM_Resources_EditThwackUploads" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= Page.Title %></h1>
    
    <br />
    
    <div style="margin-left: 10px">
        <b><%= Resources.APMWebContent.APMWEBDATA_AK1_155 %></b><br />
        <div>
            <asp:CheckBox ID="LimitCount" runat="server" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_165 %>" />
            <asp:TextBox ID="NumberOfRecords" runat="server"  Columns="5" /><br /><br />
            <asp:RadioButton ID="ShowAll" runat="server" GroupName="Show" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_157 %>" /><br />
            <asp:RadioButton ID="ShowLast" runat="server" GroupName="Show" Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_158 %>" />
            <asp:TextBox ID="NumberOfDays" runat="server"  Columns="5"/>
        </div>
    
        <br />

        <b><%= Resources.APMWebContent.APMWEBDATA_AK1_160 %></b><br />
        <div>
            <asp:DropDownList ID="SortByList" runat="server" >
            <asp:ListItem Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_161 %>" Value="Date"/>
            <asp:ListItem Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_162 %>" Value="Title"/>
            <asp:ListItem Text="<%$ Resources: APMWebContent, APMWEBDATA_AK1_163 %>" Value="Views" />
            </asp:DropDownList>
        </div>

        <br /> 
          
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
    </div>       
</asp:Content>

