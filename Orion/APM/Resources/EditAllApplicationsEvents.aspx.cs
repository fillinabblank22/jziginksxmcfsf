using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Common.Models;
using System.Globalization;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web.UI;

public partial class EditAllApplicationsEvents : System.Web.UI.Page
{
    private string resID;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            resID = Request.QueryStringOrForm("ResourceID");

            DataTable dt = new DataTable();
            using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
            {
                dt = businessLayer.GetResourceProperties(int.Parse(resID));
            }

            int period = 12; //All time events
            //string maxEvents = "25";

            if ((dt != null) && (dt.Rows.Count > 0))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToString(dr["PropertyName"]) == "Period")
                    {
                        period = int.Parse(Convert.ToString(dr["PropertyValue"]));
                    }
                }
            }

            timePeriod.SelectedIndex = period;
            // tbMaxEvents.Text = maxEvents;
            ReferrerRedirector.Initialize(ViewState);
        }
        btnSubmit.Focus();
    }

    protected void SubmitClick(object sender, EventArgs e)
    {

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            businessLayer.UpdateEventsResourceProperties(int.Parse(Request.QueryStringOrForm("ResourceID")), timePeriod.SelectedIndex.ToString(), int.MaxValue.ToString());
        }

        ReferrerRedirector.Redirect(ViewState, @"/Orion/APM/Summary.aspx");
    }
}
