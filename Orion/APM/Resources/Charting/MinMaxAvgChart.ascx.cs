﻿using SolarWinds.APM.Common;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Globalization;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_MinMaxAvgChart : APMStandardChartBaseResource, IResourceIsInternal
{
	protected override bool AllowCustomization => Profile.AllowCustomize;

    protected string ChartName => this.GetResourceProperty("ChartName", "APMMinMaxAvgChart");

    protected override string NetObjectPrefix => this.GetResourceProperty("NetObjectPrefix", "AA");

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    protected override string StatisticColumnName
	{
		get
		{
			if (NetObjectPrefix.Equals("AASD"))
			{
				var properties = GetResourcePropertiesAsDictionary();
				if (EvidenceType == ComponentEvidenceType.DynamicEvidence)
				{
					var info = default(AssignedToResourceInfo);
					AssignedToResourceInfo.TryCreate(Request, Resource, out info);

					if (info[AssignedToResourceInfo.ColumnNames] != null)
					{
						return info[AssignedToResourceInfo.ColumnNames].ToString();
					}

					if (properties.ContainsKey("StatisticName"))
					{
						return properties["StatisticName"];
					}
				}
			}
			return Resource.Properties.ContainsKey("StatisticColumnName") ? Resource.Properties["StatisticColumnName"] : "APM Standard Base Chart";
		}
	}

	public override void ShowHeaderBar(bool show)
	{
		Wrapper.ShowHeaderBar = show;
	}

	protected override string GetChartSubTitle()
	{
        if (NetObjectPrefix.Equals("AASD") && EvidenceType == ComponentEvidenceType.DynamicEvidence)
		{
			return "MIN/MAX Average {0} Statistic Data";
		}

		return string.Empty;
	}

	protected override ComponentEvidenceType EvidenceType
	{
		get
		{
			return Monitor != null ? Monitor.BaseComponent.EvidenceType : ComponentEvidenceType.None;
		}
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		// initialize properties for this chart resource if needed
		if (!Resource.Properties.ContainsKey("ChartName"))
		{
			Resource.Properties.Add("ChartName", ChartName);
		}

		HandleInit(WrapperContents);

		// Visibility is combination of wo factors (it counts only for Component detail page).
		// 1 - if the chart is eligible for current component
		// 2 - if the statistic value is defined (used ) in this monitor. (e.g. Virtual memory for Process Evidence type of monitor ... )
		this.Visible = this.Visible && SetVisibility() && this.SetVisibiltyByParameters();
		if (Visible) 
		{
			Wrapper.ManageButtonTarget = ApmBaseResource.GetTitleForCustomChartPage(Monitor, null, Wrapper.ManageButtonTarget, Title);
		}

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ChartLegend.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);
	}

    private bool SetVisibiltyByParameters()
    {
        bool hideWhenNoIopsSupport = bool.Parse(this.GetResourceProperty("HideWhenNoIopsSupport", "false"));
        if (hideWhenNoIopsSupport && (this.Monitor != null && !Monitor.BaseComponent.HasIopsSupport))
        {
            return false;
        }
        bool hideWhenNoVirtMemSupport = bool.Parse(this.GetResourceProperty("HideWhenNoVirtualMemorySupport", "false"));
        if (hideWhenNoVirtMemSupport && (this.Monitor != null && !Monitor.BaseComponent.HasVirtualMemorySupport))
        {
            return false;
        }
        return true;
    }

    #region IResourceIsInternal members

    public bool IsInternal
	{
		get { return true; }
	}

	#endregion
}