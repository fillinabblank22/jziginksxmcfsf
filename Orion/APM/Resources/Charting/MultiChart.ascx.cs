﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Helpers;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI.Formatters;
using SolarWinds.APM.Web.Plugins;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_MultiChart : ApmMultiChartBaseResource, IResourceIsInternal
{
    protected static class PropertyName
    {
        public const string HideIfNoMonitors = "hideResourceIfNoMonitors";
        public const string HideIfAllDisabled = "hideResourceIfAllDisabled";
        public const string YAxisHeight = "yAxisHeight";
        public const string YUnits = "yUnits";
    }

	protected int CounterDivHeight
	{
		get
		{
			if (Resource.Properties.ContainsKey(PropertyName.YAxisHeight))
			{
                return int.Parse(Resource.Properties[PropertyName.YAxisHeight]) + 10; // yAxisHeight + margin (same in APMMultiChartHelper.cs)
			}

			return 40;
		}
	}
	protected int IconColumnWidth = 29;
	protected int NameColumnWidth = 174;
	protected int LastValueColumnWidth = 80;

	protected string GetViewLink(long monitorId)
	{
        var component = ComponentDal.GetComponentNavigationInfo((int)monitorId);
        
        return WebPluginManager.Instance.GetComponentViewLink(component);
	}

    protected bool HideIfNoMonitors
    {
        get
        {
            return ParseHelper.TryParse(this.Resource.Properties.GetValueOrDefault(PropertyName.HideIfNoMonitors), false);
        }
    }

    protected bool HideIfAllDisabled
    {
        get
        {
            return ParseHelper.TryParse(this.Resource.Properties.GetValueOrDefault(PropertyName.HideIfAllDisabled), false);
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        this.Resource.Properties[PropertyName.YUnits] = string.Join(";", this.Monitors.Select(m => this.CounterUnits.GetValueOrDefault(m, ChartFormatter.Statistic)));
		
		// chart width and heigth
		this.Width = this.Resource.Width - IconColumnWidth - NameColumnWidth - LastValueColumnWidth - 4 + 1; // resource width - icon cell - name cell - last value cell - padding + chart left margin
		this.Height = this.ForceDisplayOfNeedsEditPlaceholder ? 125 : 5 + 26 + 35; // chart top margin + range selector + scrollbar

	    var tpp = this.Page as ITimePeriodProvider;
        if (tpp != null && tpp.TimePeriodIsSetByUser() && tpp.TimePeriodStartDate.HasValue && tpp.TimePeriodEndDate.HasValue)
        {
            var dateStart = tpp.TimePeriodStartDate.Value;
            var dateEnd = tpp.TimePeriodEndDate.Value;
            Resource.SubTitle = string.Format(this.GetClientCultureInfo(), "{0:G} - {1:G}", dateStart, dateEnd);
        }
        else
        {
            int dateSpan;
            if (int.TryParse(Resource.Properties["chartdatespan"], out dateSpan))
            {
                Resource.SubTitle = string.Format(this.GetClientCultureInfo(), "{0:G} - {1:G}", DateTime.Now.AddDays(-dateSpan), DateTime.Now);
            }
        }

		if (this.Monitors != null)
		{
			this.Height += this.Monitors.Count * CounterDivHeight;
		}

		this.pnlLeft.Visible = !this.ForceDisplayOfNeedsEditPlaceholder;
		this.pnlRight.Visible = !this.ForceDisplayOfNeedsEditPlaceholder;

		if (!this.ForceDisplayOfNeedsEditPlaceholder)
		{
			pnlMiddle.Style.Add("width", this.Width + "px");
		}
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		HandleInit(WrapperContents);
		
		if (this.Monitors != null)
		{
			this.rptMonitors.DataSource = this.Monitors;
			this.rptMonitors.DataBind();

			if (this.Monitors.Count > 0)
			{
				this.ForceDisplayOfNeedsEditPlaceholder = false;
			}

            if (this.HideIfNoMonitors && !this.Monitors.Any())
            {
                this.Visible = false;
            }

		    if (this.HideIfAllDisabled && this.Monitors.All(m => m.Status == null || m.Status.Value == Status.IsDisabled))
		    {
		        this.Visible = false;
		    }
		}

		// TODO revise Visibility
		//this.Visible &= SetVisibility();

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);
	}

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var result = base.GenerateDisplayDetails();
        var tpp = this.Page as ITimePeriodProvider;

        if (tpp != null && tpp.TimePeriodIsSetByUser() && tpp.TimePeriodStartDateUTC.HasValue && tpp.TimePeriodEndDateUTC.HasValue)
        {
            if (tpp.GetRelativeTimePeriodMinutesUTC() > 0)
            {
                result["MinutesOfDataToLoad"] = tpp.GetRelativeTimePeriodMinutesUTC();
            }
            else
            {
                result["DateFromUtc"] = SolarWinds.Orion.Common.DatabaseFunctions.ToISO860DateTimeString(tpp.TimePeriodStartDateUTC.GetValueOrDefault());
                result["DateToUtc"] = SolarWinds.Orion.Common.DatabaseFunctions.ToISO860DateTimeString(tpp.TimePeriodEndDateUTC.Value);
            }
        }

        // get ComponentIDs and Guids Dictionary
        if (this.MonitorsGuids != null && this.MonitorsGuids.Any())
        {
            result.SetMonitorsGuids(this.MonitorsGuids);
            result.SetApplicationCustomType(GetInterfaceInstance<IApplicationProviderBase>().ApmApplication.CustomType);
        }

        return result;
    }

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get
		{
			return ResourceLoadingMode.Ajax;
		}
	}

	internal static string AddLineBreaksToString(string text)
	{
		if (text != null && text.Length > 15)
		{
            return text.Replace("/", "/<span style=\"font-size: 1px\"> </span>");
        }
    	return text;
	}

    public override string HelpLinkFragment 
    {
        get { return this.ResourceHelpLinkFragment(base.HelpLinkFragment); }
    }

    #region IResourceIsInternal members

	public bool IsInternal
	{
		get { return true; }
	}

	#endregion
}
