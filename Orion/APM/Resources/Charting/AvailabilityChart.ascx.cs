﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_AvailabilityChart : APMChartBaseResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    internal const String MissingNetObjectErrorMessage = "NetObject is missing or not valid from the query string.";

    private String netObjectID = null;
    public virtual String NetObjectID
    {
        get
        {
            if (netObjectID == null)
            {
                netObjectID = String.Empty;

                var netObject = TryGetNetObject();
                if (netObject != null)
                {
                    netObjectID = netObject.NetObjectID;
                }
            }
            return netObjectID;
        }
    }

    private ApmApplicationBase apmApplication;
    private ApmApplicationBase ApmApplication
    {
        get
        {
            if (apmApplication == null)
            {
                var provider = GetInterfaceInstance<IApplicationProviderBase>();
                if (provider != null)
                    apmApplication = provider.ApmApplication;
            }
            return apmApplication;
        }
    }

    private ApmMonitor monitor;
    private ApmMonitor Monitor
    {
        get
        {
            if (monitor == null)
            {
                var provider = GetInterfaceInstance<IMonitorProvider>();
                if (provider != null)
                {
                    monitor = provider.Monitor;
                }
                else
                {
                    var info = default(AssignedToResourceInfo);
                    AssignedToResourceInfo.TryCreate(Resource.Properties[ApmMonitor.PropAssignedToResourceInfo], NetObjectID, out info);

                    if (info[AssignedToResourceInfo.CmpID] != null)
                    {
                        monitor = new ApmMonitor(string.Format(CultureInfo.InvariantCulture, "AM:{0}", info[AssignedToResourceInfo.CmpID].ToString()));
                    }
                    else
                    {
                        if (Resource.Properties.ContainsKey("componentname") && (ApmApplication != null))
                        {
                            var cmpName = Resource.Properties["componentname"];

                            var cmp = new ApmApplication(NetObjectID)
                                .Components
                                .FirstOrDefault(item => string.Equals(item.Name, cmpName, StringComparison.InvariantCultureIgnoreCase));
                            if (cmp != null)
                            {
                                monitor = new ApmMonitor(cmp);
                            }
                        }
                        else if (NetObject is ApmMonitor)
                        {
                            return NetObject as ApmMonitor;
                        }
                    }
                }
            }
            return monitor;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ApmBaseResource.EnsureApmScripts();
        EnsureDateFormatScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ChartLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);

        HandleInit(WrapperContents);

		Wrapper.ManageButtonTarget = ApmBaseResource.GetTitleForCustomChartPage(Monitor, ApmApplication, Wrapper.ManageButtonTarget, Title);
    }

    /// <summary>
    /// Gets a value indicating whether [allow customization].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [allow customization]; otherwise, <c>false</c>.
    /// </value>
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    /// <summary>
    /// Gets the net object prefix.
    /// </summary>
    protected override string NetObjectPrefix
    {
        get { return Resource.Properties.ContainsKey("NetObjectPrefix") ? Resource.Properties["NetObjectPrefix"] : "AA"; }
    }

    /// <summary>
    /// Gets the list of uniqueidentifier elements for which we will need to retrieve data points for charts
    /// </summary>
    /// <returns>List of uniqueidentifiers of elements for which we via webservice will want to retrieved data.</returns>
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var provider = GetInterfaceInstance<IApplicationProviderBase>();
        var elementsList = new string[0];
        if (provider != null)
        {
			if (NetObjectPrefix.Equals("AAAA"))
			{
                if (ApmApplication != null)
                {
                    elementsList = new string[1] { ApmApplication.Id.ToString(CultureInfo.InvariantCulture) };
                }
                else
                {
                    this.ForceDisplayOfNeedsEditPlaceholder = true;
                }
			}
			else
			{
			    if (Monitor != null)
			    {
			        elementsList = new string[1] {Monitor.Id.ToString(CultureInfo.InvariantCulture)};
			    }
			    else
			    {
                    // if the chart is not set for particular componen for this application then force the customer to choose one
                    this.ForceDisplayOfNeedsEditPlaceholder = true;
			    }
			}
        }

        return elementsList;
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get{ return new Type[] { typeof(IApplicationProviderBase) }; }
    }

    /// <summary>
    /// Gets the default title for resource which will be displayed in the case that template will not override this.
    /// </summary>
    protected override string DefaultTitle
    {
        get { return string.Empty; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    public NetObject NetObject { get; protected set; }

    public virtual NetObject TryGetNetObject()
    {
        if (NetObject == null)
        {
            var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();

            if (netObjectProvider != null)
            {
                NetObject = netObjectProvider.NetObject;
            }
        }
        return NetObject;
    }

    public static Boolean IsCustomObjectResource(ResourceInfo resource)
    {
        return NetObjectManager.IsCustomObjectResource(resource);
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        string subTitle = string.IsNullOrEmpty(Request.QueryString["ChartSubTitle"]) ? ChartResourceSettings.SubTitle : Request.QueryString["ChartSubTitle"];

        this.ChartTitle = GetTitle();
        this.ChartSubTitle = string.IsNullOrEmpty(Request.QueryString["ChartSubTitle"]) ? ChartResourceSettings.SubTitle : Request.QueryString["ChartSubTitle"];

        var objProp = GetMonitorObjectProperties();
        // Parse macros in the title/subtitle (if they are there)
        if ((this.ChartTitle.Contains("${") || this.ChartSubTitle.Contains("${")) && (objProp != null))
        {
            this.ChartTitle = Macros.ParseDataMacros(this.ChartTitle, objProp, true, false);
            this.ChartSubTitle = Macros.ParseDataMacros(this.ChartSubTitle, objProp, true, false);
        }

        var result = base.GenerateDisplayDetails();
        if (this.Page is ITimePeriodProvider && ((ITimePeriodProvider)this.Page).TimePeriodIsSetByUser() && ((ITimePeriodProvider)this.Page).TimePeriodStartDateUTC.HasValue && ((ITimePeriodProvider)this.Page).TimePeriodEndDateUTC.HasValue)
        {
            if (((ITimePeriodProvider)this.Page).GetRelativeTimePeriodMinutesUTC() > 0)
            {
                result["MinutesOfDataToLoad"] = ((ITimePeriodProvider)this.Page).GetRelativeTimePeriodMinutesUTC();
            }
            else
            {
                result["DateFromUtc"] = SolarWinds.Orion.Common.DatabaseFunctions.ToISO860DateTimeString(((ITimePeriodProvider)this.Page).TimePeriodStartDateUTC.Value);
                result["DateToUtc"] = SolarWinds.Orion.Common.DatabaseFunctions.ToISO860DateTimeString(((ITimePeriodProvider)this.Page).TimePeriodEndDateUTC.Value);
            }
        }
        return result;
    }

    private Dictionary<string, object> GetMonitorObjectProperties()
    {
        Dictionary<string, object> objProp = new Dictionary<string, object>();
        if (NetObjectPrefix.Equals("AAAA"))
        {
            objProp = ApmApplication.ObjectProperties;
        }
        else
        {
            if (Monitor != null)
            {
                objProp = Monitor.ObjectProperties;
            }
        }
        return objProp;
    }

    private string GetTitle()
    {
        string title = string.IsNullOrEmpty(Request.QueryString["ChartTitle"]) ? ChartResourceSettings.Title : Request.QueryString["ChartTitle"];
        
        if (string.IsNullOrEmpty(title))
        {
            if (NetObjectPrefix.Equals("AAAA"))
            {
                title = ApmApplication.Name;
            }
            else
            {
                if (Monitor != null)
                {
                    title = Monitor.Name;
                }
            }
        }
        return title;
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment(ApmHelpHelper.FirstNonWhiteSpace(base.HelpLinkFragment, "OrionAPMAGAppAvailabilityChart")); }
    }
}
