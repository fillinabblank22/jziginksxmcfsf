﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.ChartingCoreBased;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_ComponentMultiValueChart : APMChartBaseResource, IResourceIsInternal
{
	private static readonly Log _log = new Log();

	private bool _monitorInitialize;
	private ApmMonitor _monitor;

	private AssignedToResourceInfo _assignedInfo;

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	protected override string NetObjectPrefix
	{
		get { return Resource.Properties.ContainsKey("NetObjectPrefix") ? Resource.Properties["NetObjectPrefix"] : "AA"; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IMonitorProvider) }; }
	}

	protected override string DefaultTitle
	{
		get { return string.Empty; }
	}

	public bool IsInternal
	{
		get { return true; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

	public NetObject NetObject { get; protected set; }

	private ApmMonitor Monitor
	{
		get
		{
			if (!_monitorInitialize)
			{
				_monitorInitialize = true;

				var obj = TryGetNetObject();
				if (obj != null)
				{
					if (NetObjectHelper.IsComponent(obj.NetObjectID))
					{
						_monitor = obj as ApmMonitor;
					}

					if (_monitor == null)
					{
						var info = GetAssignedInfo();
						if (info != null && info[AssignedToResourceInfo.CmpID] != null)
						{
							_monitor = new ApmMonitor(InvariantString.Format("{0}:{1}", ApmMonitor.Prefix, info[AssignedToResourceInfo.CmpID].ToString()));
						}
					}

					if (_monitor == null) 
					{
						if (NetObjectHelper.IsApplication(obj.NetObjectID))
						{
							var app = obj as ApmApplication;
							if (app != null) 
							{
								var cmp = app.Components
									.FirstOrDefault(item => item.IsDynamicBased);
								if (cmp != null) 
								{
									_monitor = new ApmMonitor(cmp);
								}
							}
						}
					}
				}
			}
			return _monitor;
		}
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		if (Monitor == null)
		{
			ForceDisplayOfNeedsEditPlaceholder = true;
		}

        ApmBaseResource.EnsureApmScripts();
        EnsureDateFormatScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ChartLegend.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);

		HandleInit(WrapperContents);

		Visibility();
		if (Visible && Monitor != null)
		{
			Wrapper.ManageButtonTarget = ApmBaseResource.GetTitleForCustomChartPage(Monitor, null, Wrapper.ManageButtonTarget, Title);
		}
	}

	private void Visibility()
	{
		Visible = (Monitor != null && Monitor.BaseComponent.IsDynamicBased) || NetObjectHelper.IsApplication(NetObject.NetObjectID);
	}

	protected override IEnumerable<string> GetElementIdsForChart()
	{
		var componentIds = new string[1];
		if (Monitor != null)
		{
			componentIds[0] = Monitor.Id.ToString(CultureInfo.InvariantCulture);
			return componentIds;
		}

		return componentIds;
	}

	public virtual NetObject TryGetNetObject()
	{
		if (NetObject == null)
		{
			String netObjectID = null;
			if (NetObjectManager.IsCustomObjectResource(Resource))
			{
				netObjectID = Resource.Properties["netobjectid"];
				if (netObjectID.IsNotValid())
				{
					return null;
				}
			}

			if (netObjectID.IsNotValid())
			{
				netObjectID = Request.QueryString["NetObject"];
				if (netObjectID.IsNotValid())
				{
					if (RequiredInterfaces.Any())
					{
						var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
						if (netObjectProvider == null)
						{
							return null;
						}
						NetObject = netObjectProvider.NetObject;
						NetObject = NetObjectFactory.FindNetObjectByNetObjectPrefix(NetObject, this.NetObjectPrefix);
					}
				}
			}

			try
			{
				if (NetObject == null)
				{
					NetObject = NetObjectFactory.Create(netObjectID);
				}
			}
			catch (AccountLimitationException)
			{
				Page.Server.Transfer(InvariantString.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", netObjectID));
			}
		}
		return NetObject;
	}

	protected override Dictionary<string, object> GenerateDisplayDetails()
	{
		ChartTitle = GetTitle();
		ChartSubTitle = Request.QueryString["ChartSubTitle"];
		if (string.IsNullOrWhiteSpace(ChartSubTitle))
		{
			ChartSubTitle = ChartResourceSettings.SubTitle;
		}

		var objProp = GetMonitorObjectProperties();
		if (objProp != null) 
		{
			if (ChartTitle.Contains("${") || ChartSubTitle.Contains("${"))
			{
				ChartTitle = Macros.ParseDataMacros(ChartTitle, objProp, true, false);
				ChartSubTitle = Macros.ParseDataMacros(ChartSubTitle, objProp, true, false);
			}
		}

		var details = base.GenerateDisplayDetails();

		var info = GetAssignedInfo();
		if (info[AssignedToResourceInfo.ColumnNames] == null)
		{
			details["ElementList"] = Resource.Properties[ResourceSettings.ValuesToDisplay] ?? string.Empty;
		}
		else
		{
			details["ElementList"] = info[AssignedToResourceInfo.ColumnNames].ToString();
		}
		return details;
	}

	private AssignedToResourceInfo GetAssignedInfo()
	{
		if (_assignedInfo == null)
		{
			AssignedToResourceInfo.TryCreate(Request, Resource, out _assignedInfo);
		}
		return _assignedInfo;
	}

	private Dictionary<string, object> GetMonitorObjectProperties()
	{
		var objProp = new Dictionary<string, object>();
		if (Monitor != null)
		{
			objProp = Monitor.ObjectProperties;
		}
		return objProp;
	}

	private string GetTitle()
	{
		string title = Request.QueryString["ChartTitle"];
		if (string.IsNullOrWhiteSpace(title))
		{
			title = ChartResourceSettings.Title;
		}
		if (string.IsNullOrWhiteSpace(title))
		{
			if (Monitor != null)
			{
				title = Monitor.Name;
				
				var netObject = Request.QueryStringOrForm("NetObject");
				if (!NetObjectHelper.IsComponent(Request.QueryStringOrForm("NetObject")))
				{
					title = InvariantString.Format("{0} - {1}", Monitor.LongName, Monitor.Type);
				}
			}
		}
		return title;
	}
}
