﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiChart.ascx.cs" Inherits="Orion_APM_Resources_Charting_MultiChart" %>
<%@ Import Namespace="SolarWinds.APM.Web" %>
<%@ Import Namespace="SolarWinds.APM.Web.Plugins" %>
<%@ Import Namespace="SolarWinds.APM.Web.DisplayTypes" %>
<%@ Register TagPrefix="apm" TagName="SmallApmStatusIcon" Src="~/Orion/APM/Controls/SmallApmStatusIcon.ascx" %>

<orion:Include runat="server" Module="APM" File="APM.css"/>
<orion:Include runat="server" Module="APM" File="/js/ResourcesColumnZIndex.js" Section="Bottom"/>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <style>
            .mcHeader {
                overflow-y: visible !important;
            }
            .mcContainer {
                overflow-y: visible;
                position: relative;
            }
            .mcContainer .chartDataNotAvailableArea {
                background-position-y: -15px;
                max-height: 100%;
                padding-top: 0px;
                margin-bottom: 0px;
                left: 185px;
                position: absolute;
                top: 0px;
                width: calc(100% - 185px - 85px);
                display: flex;
                flex-direction: column;
                justify-content: center;
            }
            .mcRow {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: flex-start;
                min-height: <%= this.CounterDivHeight %>px;
            }
            
        </style>    
        <div class="mcContainer" style="height: <%= this.Height %>px;">
            <asp:Panel ID="pnlLeft" runat="server" CssClass="mcLeftColumn mcHeader">
                <span class="title">
                    <%= Resources.APMWebContent.Chart_Statistic_Name %>
                </span>
            </asp:Panel>
			<asp:Panel ID="pnlMiddle" runat="server" CssClass="mcMiddleColumn mcHeader">
                <!-- This element is used to access ResourceWrapper element for this resource so that 
                we can add extra CSS class to it to fix chart tooltips clipping. (FB109023) -->
                <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
            </asp:Panel>
			<asp:Panel ID="pnlRight" runat="server" CssClass="mcRightColumn mcHeader">
                <span class="title">
                    <%= Resources.APMWebContent.Chart_Value_From_Last_Poll %>
                </span>
            </asp:Panel>
            <asp:Repeater ID="rptMonitors" runat="server">
                <ItemTemplate>
                    <div class="mcCounter" style="height: <%= this.CounterDivHeight %>px;">
                        <table>
                            <tr class="mcRow">
                                 <td class="icon" style="width: <%= this.IconColumnWidth %>px">
	                                 <asp:Image runat="server" 
										 ImageUrl='<%# WebPluginManager.Instance.GetSmallIconUrl(
												ViewType.Monitor, 
												GetInterfaceInstance<IApplicationProviderBase>().ApmApplication.CustomType, 
												(int)((ApmStatus)Eval("Status")).Value) %>'
										 ToolTip='<%# ((ApmStatus)Eval("Status")).ToString() %>'
										   />
                                 </td>
                                 <td class="name" style="width: <%= this.NameColumnWidth %>px; max-height: 40px; overflow: hidden; min-width: 160px;">
                                    <a href="<%# GetViewLink((long)Eval("Id")) %>">
                                       <%# AddLineBreaksToString(Eval("Name").ToString()) %> 
                                    </a>                                    
                                 </td>
                                 <td class="chart"></td>
                                 <td class="lastValue" style="width: 100%">
                                    <span class="lastValue"></span>
                                 </td>
                             </tr>    
                        </table>  
                        <hr />              
                    </div>                      
                </ItemTemplate>  
            </asp:Repeater>
        </div>         
    </Content>
</orion:resourceWrapper> 
