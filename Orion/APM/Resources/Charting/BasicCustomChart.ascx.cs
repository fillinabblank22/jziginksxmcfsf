﻿using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.Models.Charting;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_BasicCustomChart : APMChartBaseResource, IResourceIsInternal
{
	private string _chartName = null, _netObjectID = null;
	private string _statisticColumnName, _statisticColumnLabel;
	private ApmMonitor monitor;

	public virtual String NetObjectID
	{
		get
		{
			if (_netObjectID == null)
			{
				_netObjectID = String.Empty;

				var netObject = TryGetNetObject();
				if (netObject != null)
				{
					_netObjectID = netObject.NetObjectID;
				}
			}
			return _netObjectID;
		}
	}

	public NetObject NetObject { get; protected set; }

	private ApmMonitor Monitor
	{
		get
		{
			if (monitor == null)
			{
				if (NetObjectHelper.IsComponent(NetObjectID))
				{
					monitor = new ApmMonitor(NetObjectID);
				}
				else
				{
					var info = default(AssignedToResourceInfo);
					AssignedToResourceInfo.TryCreate(Request, Resource, out info);

					if (info[AssignedToResourceInfo.CmpID] != null)
					{
						monitor = new ApmMonitor(string.Format(CultureInfo.InvariantCulture, "AM:{0}", info[AssignedToResourceInfo.CmpID].ToString()));
					}
				}

				if (monitor == null)
				{
					if (Resource.Properties.ContainsKey("componentname"))
					{
						if (NetObjectHelper.IsApplication(NetObjectID))
						{
							var cmpName = Resource.Properties["componentname"];

							var cmp = new ApmApplication(NetObjectID)
								.Components
								.FirstOrDefault(item => string.Equals(item.Name, cmpName, StringComparison.InvariantCultureIgnoreCase));
							if (cmp != null)
							{
								monitor = new ApmMonitor(cmp);
							}
						}
					}
				}
			}
			return monitor;
		}
	}

	protected override string DefaultTitle
	{
		get { return string.Empty; }
	}

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	private string AssignedChartName
	{
		get
		{
			if (_chartName == null)
			{
				var info = default(AssignedToResourceInfo);
				AssignedToResourceInfo.TryCreate(Resource, NetObjectID, out info);

				if (info[AssignedToResourceInfo.CustomChartName] != null)
				{
					var chartName = info[AssignedToResourceInfo.CustomChartName];
					_chartName = chartName.ToString();
				}
				if (_chartName.IsNotValid())
				{
					_chartName = Resource.Properties["ChartName"];
				}
				if (_chartName.IsNotValid())
				{
					_chartName = string.Empty;
				}
			}
			return _chartName;
		}
	}

	protected override string NetObjectPrefix
	{
		get { return Resource.Properties["NetObjectPrefix"] ?? "AACA"; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IMonitorProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

	public bool IsInternal
	{
		get { return true; }
	}

	private bool IsStatisticDataSource
	{
		get
		{
			var name = Resource.Properties["ChartName"];
			if (string.IsNullOrWhiteSpace(name))
			{
				return false;
			}
			return name.EqualsInvariant(ChartNames.StatisticDataCustomAreaChart) ||
				name.Equals(ChartNames.StatisticDataCustomBarChart) ||
				name.Equals(ChartNames.StatisticDataCustomLineChart);
		}
	}

	protected string StatisticColumnName
	{
		get
		{
			if (_statisticColumnName == null)
			{
				InitStatisticColumnNameLabel(ref _statisticColumnName, ref _statisticColumnLabel);
			}
			return _statisticColumnName;
		}
	}

	protected string StatisticColumnLabel
	{
		get 
		{
			if (_statisticColumnLabel == null) 
			{
				InitStatisticColumnNameLabel(ref _statisticColumnName, ref _statisticColumnLabel);
			}
			return _statisticColumnLabel; 
		}
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		Resource.Properties["ChartName"] = AssignedChartName;
		
		HandleInit(WrapperContents);

        ApmBaseResource.EnsureApmScripts();
        EnsureDateFormatScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);

		Wrapper.ManageButtonTarget = ApmBaseResource.GetTitleForCustomChartPage(Monitor, null, Wrapper.ManageButtonTarget, Title);
	}

	protected override IEnumerable<string> GetElementIdsForChart()
	{
		var elementsList = new string[0];
		if (Monitor != null)
		{
			elementsList = new[] { Monitor.Id.ToString(CultureInfo.InvariantCulture) };
		}

		// if the chart is not set for particular componen for this application then force the customer to choose one
		if (!elementsList.Any())
			this.ForceDisplayOfNeedsEditPlaceholder = true;

		return elementsList;
	}

	public virtual NetObject TryGetNetObject()
	{
		if (NetObject == null)
		{
			var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
			if (netObjectProvider != null)
			{
				NetObject = netObjectProvider.NetObject;
			}
		}
		return NetObject;
	}

	protected override Dictionary<string, object> GenerateDisplayDetails()
	{
		var details = base.GenerateDisplayDetails();
		
		this.ChartTitle = GetTitle();
		this.ChartSubTitle = GetSubTitle();

		// Parse macros in the title/subtitle (if they are there)
		if (Monitor != null)
		{
			if (ChartTitle.Contains("${"))
			{
				ChartTitle = Macros.ParseDataMacros(this.ChartTitle, Monitor.ObjectProperties, true, false);
			}
			if (ChartSubTitle.Contains("${"))
			{
				ChartSubTitle = Macros.ParseDataMacros(this.ChartSubTitle, Monitor.ObjectProperties, true, false);
			}

			if (Monitor.BaseComponent.IsDynamicBased)
			{
				var columnName = StatisticColumnName;
				if (columnName.IsValid())
				{
					details["StatisticColumn"] = StatisticColumnName;
					details["StatisticColumnLabel"] = StatisticColumnLabel;
					details["HasStatisticData"] = true;
				}
			}

			if (Monitor.BaseComponent.HasStatisticData)
				details["HasStatisticData"] = "true";

			if (Monitor.BaseComponent.HasResponseTimeSupport)
				details["HasResponseTimeSupport"] = "true";
		}

		details["ResourceID"] = Resource.ID;
		details["title"] = this.ChartTitle;
		details["subtitle"] = this.ChartSubTitle;

		return details;
	}

	private string GetTitle()
	{
		string result = string.IsNullOrEmpty(Request.QueryString["ChartTitle"]) ? ChartResourceSettings.Title : Request.QueryString["ChartTitle"];
		if (Monitor == null)
		{
			return result;
		}

		if (string.IsNullOrEmpty(result))
		{
			result = Monitor.Name;
		}

		if (IsStatisticDataSource && Monitor.BaseComponent.IsDynamicBased)
		{
			result = string.Format(CultureInfo.InvariantCulture, "{0} - {1}", result, string.IsNullOrEmpty(StatisticColumnLabel) ? StatisticColumnName : StatisticColumnLabel);
		}
		return result;
	}

	private string GetSubTitle()
	{
		string result = string.IsNullOrEmpty(Request.QueryString["ChartSubTitle"]) ? ChartResourceSettings.SubTitle : Request.QueryString["ChartSubTitle"];
		if (string.IsNullOrEmpty(result))
		{
			result = string.Format(CultureInfo.InvariantCulture, GetChartSubTitle(), string.IsNullOrEmpty(StatisticColumnLabel) ? StatisticColumnName : StatisticColumnLabel);
		}

		return result;
	}

	protected string GetChartSubTitle()
	{
		if (IsStatisticDataSource && Monitor.BaseComponent.IsDynamicBased)
		{
			return "Custom Chart - {0} Statistic Data";
		}
		return string.Empty;
	}

	private void InitStatisticColumnNameLabel(ref string columnName, ref string columnLabel) 
	{
		if (columnName != null)
		{
			return;
		}

		columnName = columnLabel = string.Empty;
		if (Monitor == null)
		{
			return;
		}

		if (IsStatisticDataSource && Monitor.BaseComponent.IsDynamicBased)
		{
			var info = default(AssignedToResourceInfo);
			AssignedToResourceInfo.TryCreate(Request, Resource, out info);

			if (info[AssignedToResourceInfo.ColumnNames] != null)
			{
				columnName = info[AssignedToResourceInfo.ColumnNames].ToString();
			}
			else
			{
				columnName = Resource.Properties["StatisticName"];
			}
			ValidateColumnName(ref columnName, ref columnLabel);
		}
	}

	private void ValidateColumnName(ref string columnName, ref string columnLabel)
	{
		if (Monitor == null)
		{
			return;
		}

		if (IsStatisticDataSource && Monitor.BaseComponent.IsDynamicBased)
		{
			// Check the schema of the script monitor
			var columnSchema = new DynamicEvidenceDal()
				.GetDynamicEvidenceColumns(Monitor.Id, DynamicEvidenceColumnDataType.Numeric);
			if (columnSchema.Rows.Count > 0)
			{
				DataRow row = null;
				if (columnName.IsValid())
				{
					row = columnSchema
						.Select(string.Format(CultureInfo.InvariantCulture, "Name = '{0}'", columnName))
						.FirstOrDefault();
				}
				if (row == null)
				{
					row = columnSchema.Rows[0];
				}
				columnName = (string)row["Name"];
				columnLabel = (string)row["Label"];
			}
		}
	}
}