﻿using System;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Charting_CustomAvailabilityChart : ApmCustomAvailabilityChartResource, IResourceIsInternal
{
    protected void Page_Init(object sender, EventArgs e)
    {
        this.HandleInit(this.WrapperContents);
    }

    protected override bool AllowCustomization => this.Profile.AllowCustomize;

    bool IResourceIsInternal.IsInternal => true;
}
