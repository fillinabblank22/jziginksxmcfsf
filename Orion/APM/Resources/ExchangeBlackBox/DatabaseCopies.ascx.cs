﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using I18nExternalized = Resources.APM_ExBBContent;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_DatabaseCopies : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    protected override string DefaultTitle
    {
        get { return I18nExternalized.DatabaseCopies_Title; }
    }

    public override string SubTitle
    {
        get { return InvariantString.Format(I18nExternalized.DatabaseCopies_SubTitle, Db.Name); }
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment("SAMAGAppInsightForExchangeDatabaseCopies"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IDatabaseProvider) }; }
    }

    private Database db;
    private bool databaseInitialized;

    private Database Db
    {
        get
        {
            if (databaseInitialized)
            {
                return this.db;
            }
            var provider = this.GetInterfaceInstance<IDatabaseProvider>();
            if (provider != null)
            {
                this.db = provider.ExchangeDatabase;
            }
            if (this.db == null)
            {
                this.db = new Database(new DatabaseModel());
            }
            this.databaseInitialized = true;
            return db;
        }
    }



    private const string SwqlQueryTemplate = @"
SELECT
    '{1}:' + ToString(cpy.ApplicationID) as [_appLink],
    '/Orion/StatusIcon.ashx?entity={2}&size=small&status=' + ToString(app.CurrentStatus.Availability) as [_appIconLink],
    app.Name as [_appName],
    '{3}:' + ToString(node.NodeID) as [_nodeLink],
    '/Orion/images/StatusIcons/Small-' + ToString(node.StatusIcon) as [_nodeIconLink],
    node.Caption as [nodeCaption],
    '{4}:' + ToString(cpy.ID) as [_databaseLink],
    '/Orion/StatusIcon.ashx?entity={5}&size=small&status=' + ToString(cpy.Status) + '&hint=' + ToString(cpy.IsActive) as [_databaseIconLink],
    cpy.CopyState,
    cpy.CopyQueueLength,
    cpy.ReplayQueueLength,
    cpy.LastInspectedLogTime,
    cpy.ContentIndexState,
    cpy.ActivationPreference,
    cpy.ApplicationID as [_appId],
    cpy.ID as [_dbCopyId],
    cpy.StatusCopyQueueLength as [_statusCopyQueueLength],
    cpy.StatusReplayQueueLength as [_statusReplayQueueLength]
FROM Orion.APM.Exchange.DatabaseCopy cpy
INNER JOIN Orion.APM.Exchange.Application app
    ON app.ApplicationID = cpy.ApplicationID
INNER JOIN Orion.Nodes node
    ON node.NodeID = app.NodeID
WHERE cpy.DatabaseID = {0}";

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = InvariantString.Format(
            SwqlQueryTemplate,
            Db.Id,
            InvariantString.Format(
                "/Orion/APM/ExchangeBlackBox/ExchangeApplicationDetails.aspx?NetObject={0}",
                ExchangeConstants.ApplicationPrefix),
            SwisEntities.Application,
            GetViewLink("N"),
            InvariantString.Format(
                "/Orion/View.aspx?NetObject={0}",
                ExchangeConstants.DatabaseCopyPrefix),
            SwisEntities.DatabaseCopy);
    }
}
