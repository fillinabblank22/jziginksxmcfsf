﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_ExchangeBlackBox_MailboxDatabaseStatus : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    private IExchangeApplicationProvider applicationProvider;

    protected ExchangeApplication ExchangeApplication { get; set; }

    protected string SWQL
    {
        get
        {
            var additionalFilter = string.Empty;

            int selectedIndex;
            if (!String.IsNullOrEmpty(Resource.Properties["ShowDatabases"]) &&
                Int32.TryParse(Resource.Properties["ShowDatabases"], out selectedIndex))
            {
                switch (selectedIndex)
                {
                    case 1:
                        additionalFilter = "AND dc.IsActive = 1";
                        break;
                    case 2:
                        additionalFilter = "AND dc.IsActive = 0";
                        break;
                }
            }

            return InvariantString.Format(@"SELECT dc.DatabaseName as [DatabaseName],
	dc.CopyState as [CopyState], 
	dc.CopyQueueLength as [CopyQueueLength],
	dc.ReplayQueueLength as [ReplayQueueLength],
	dc.LastInspectedLogTime as [LastInspectedLogTime],
	dc.ContentIndexState as [ContentIndexState],
    dc.ActivationPreference as [ActivationPreference],
    '{2}:' + ToString(dc.ID) as [_dbLink],
    '/Orion/StatusIcon.ashx?entity={3}&size=small&status=' + ToString(dc.Status) + '&hint=' + ToString(dc.IsActive) as [_databaseIconLink],
    dc.IsActive as [_isActive],
    d.MasterType,
    (SELECT ID
    FROM Orion.APM.Exchange.Application
    WHERE NodeID = d.ServerNodeID) as MountedServerId,
    (SELECT ID
    FROM Orion.APM.Exchange.Application
    WHERE NodeID = d.PreferredNodeId) as PreferredServerId,
    d.ServerName,
    d.PreferredServer,
    dc.ID,
    dc.StatusCopyQueueLength as [StatusCopyQueueLength],
	dc.StatusReplayQueueLength as [StatusReplayQueueLength]
FROM Orion.APM.Exchange.DatabaseCopy dc
JOIN Orion.APM.Exchange.Database d ON dc.DatabaseID = d.ID
WHERE dc.ApplicationID = {0} {1}", ExchangeApplication.Id, additionalFilter,
                "/Orion/View.aspx?NetObject={0}".FormatInvariant(
                    ExchangeConstants.DatabaseCopyPrefix), SwisEntities.DatabaseCopy);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        applicationProvider = this.GetInterfaceInstance<IExchangeApplicationProvider>();
        if (applicationProvider == null)
            throw new InvalidOperationException("Cannot get application ID for filtering databases by application ID");

        ExchangeApplication = applicationProvider.ExchangeApplication;

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = SWQL;
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_ExBBContent.MailboxDatabaseStatus_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForExchangeMailboxDatabaseStatus"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IExchangeApplicationProvider) }; }
    }

    public override string SubTitle
    {
        get
        {
            string subTitlePart;

            int selectedIndex;
            if (!String.IsNullOrEmpty(Resource.Properties["ShowDatabases"]) &&
                Int32.TryParse(Resource.Properties["ShowDatabases"], out selectedIndex))
            {
                switch (selectedIndex)
                {
                    case 1:
                        subTitlePart = Resources.APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_ActiveDatabases;
                        break;
                    case 2:
                        subTitlePart = Resources.APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_PassiveDatabases;
                        break;
                    default:
                        subTitlePart = Resources.APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_AllDatabases;
                        break;
                }
            }
            else
            {
                subTitlePart = Resources.APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_AllDatabases;
            }

            return Resources.APM_ExBBContent.MailboxDatabaseResource_SubTitle.FormatInvariant(subTitlePart);
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/APM/ExchangeBlackBox/Controls/EditDatabaseResourceControl.ascx";
        }
    }
}
