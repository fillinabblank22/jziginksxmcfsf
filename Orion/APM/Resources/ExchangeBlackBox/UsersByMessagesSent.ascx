﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsersByMessagesSent.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_UsersByMessagesSent" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: <%= Resource.ID %>,
                    cls: 'exchangeGrid',
                    initialPage: 0,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                    searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                    searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                    allowSort: true,
                    allowSearch: true,
                    showAllLimit: 50,
                    headerTitles:[
                        "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_UserName %>",
                        "<%= APM_ExBBContent.SentYesterday %>",
                        "<%= APM_ExBBContent.SentLast7Days %>",
                        "<%= APM_ExBBContent.SentLast30Days %>",
                        "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_LastAccessed %>",
                        "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_Database %>"
                    ],
                    customHeaderGenerator: function(columns) {
                        var headerRow = $('<tr/>').addClass('HeaderRow');
                        columns[0].attr('colspan', '2');
                        columns[0].appendTo(headerRow);
                        columns[1].appendTo(headerRow);
                        columns[2].appendTo(headerRow);
                        columns[3].appendTo(headerRow);
                        columns[4].appendTo(headerRow);
<% if (!isDbDetails) { %>
                        columns[5].attr('colspan', '2');
                        columns[5].appendTo(headerRow);
<% } %>
                        return headerRow;
                    },
                    underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
                    defaultOrderBy:  "[MessagesSent] DESC"
                });
            $('#' + '<%= SearchControl.SearchBoxClientID %>').attr("placeholder", "<%= APM_ExBBContent.SearchUsers%>");
            var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
            SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
            refresh();
            });
        </script>
        
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
        {#
            var sentDay = SW.APM.EXBB.Resources.formatValue(Columns[3]);
            var sentWeek = SW.APM.EXBB.Resources.formatValue(Columns[4]);
            var sentMonth = SW.APM.EXBB.Resources.formatValue(Columns[5]);
            var itemCount = SW.APM.EXBB.Resources.formatValue(Columns[8]);
            if (!isFinite(String(itemCount))) {
                sentDay    = '<%= APM_ExBBContent.NotAvailableData %>';
                sentWeek   = '<%= APM_ExBBContent.NotAvailableData %>';
                sentMonth  = '<%= APM_ExBBContent.NotAvailableData %>';
            }
            var regionalSettings = <%= DatePickerRegionalSettings.GetDatePickerRegionalSettings() %>;
            var date = (Columns[6] === null) ? '' : SW.APM.EXBB.Resources.DB.formatDateWithRegoinalSettings(SW.APM.EXBB.Resources.DB.utcToLocalTime(Columns[6]), SW.APM.EXBB.Resources.Constants.SHORT_DATE_FORMAT, regionalSettings);
        #}
            <tr class="topRow">
                <td style="padding-right:0px!important;">{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[0], Columns[1])}}</td>
                <td class="UsersFixedColumnWidth" ><div class="UsersFixedColumnEllipsis" >{{SW.APM.EXBB.Resources.DB.getLink(Columns[0], Columns[2])}}</div></td>
                <td class="fixedColumn">{{sentDay}}</td>
                <td class="fixedColumn">{{sentWeek}}</td>
                <td class="fixedColumn">{{sentMonth}}</td>
                <td>{{ date }}</td>
<% if (!isDbDetails) { %>
                <td style="padding-right:0px!important;">{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[9], Columns[10])}}</td>
                <td class="UsersFixedColumnWidth" ><div class="UsersFixedColumnEllipsis" >{{SW.APM.EXBB.Resources.DB.getLink(Columns[9], Columns[11])}}</div></td>
<% } %>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>
