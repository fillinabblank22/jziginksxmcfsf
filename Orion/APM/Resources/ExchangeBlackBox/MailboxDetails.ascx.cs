﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Common.Models;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.Models;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using APMModels = SolarWinds.APM.Common.Models;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_ExchangeBlackBox_MailboxDetails : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public ExchangeApplication App
	{
		get { return this.Mailbox.MailboxDatabase.MountedServer; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected override string DefaultTitle
	{
		get { return APM_ExBBContent.MailboxDetails_Title; }
	}

	public override string HelpLinkFragment
	{
        get { return "SAMAGAppInsightForExchangeMailboxDetails"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.RenderControl; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(IMailboxProvider) }; }
	}

	private Mailbox mailbox;
	private bool mailboxInitialized;

	public Mailbox Mailbox
	{
		get
		{
			if (!mailboxInitialized)
			{
				var provider = GetInterfaceInstance<IMailboxProvider>();
				if (provider != null)
				{
					mailbox = provider.ExchangeMailbox;
				}
				if (mailbox == null)
				{
					mailbox = new Mailbox(new MailboxModel());
				}
				mailboxInitialized = true;
			}
			return mailbox;
		}
	}

	public ApmStatus GetMailboxStatus(int mailboxStatus)
	{
		return ApmStatus.FromInt(mailboxStatus);
	}

	public string GetIconUrl()
	{
		return InvariantString.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small", this.Mailbox.SwisEntity, this.Mailbox.MailboxModel.Status.GetValueOrDefault());
	}

	public string GetDatabaseIconUrl()
	{
         return InvariantString.Format("/Orion/StatusIcon.ashx?entity={0}&size=small&status={1}", SwisEntities.Database, Mailbox.MailboxDatabase.StatusId);
	}

	protected object ToMb(long? value)
	{
		if (value.HasValue)
		{
			return new Byte1024FormatInfo().Format("0:0.0#", value.Value, CultureInfo.InvariantCulture);
		}
		return APM_ExBBContent.NotAvailableData;
	}

    protected object ToMb(ulong? value)
    {
        if (value.HasValue)
        {
            return new Byte1024FormatInfo().Format("0:0.0#", value.Value, CultureInfo.InvariantCulture);
        }
        return APM_ExBBContent.NotAvailableData;
    }

    protected object CheckForCountLimit(long? value, bool returnWithValue)
    {
        if (value.HasValue)
        {
            if (returnWithValue)
            {
                return value == 10000 ? ">10000" : value.ToString();
            }

            return value == 10000 ? ">" : string.Empty;            
        }
        return returnWithValue ? APM_ExBBContent.NotAvailableData : string.Empty;
    }     

	protected object ToSize(long? value)
	{
		if (value.HasValue)
		{
			return value.Value;
		}
		return APM_ExBBContent.NotAvailableData;
	}

    protected string CustomLimitsUsed()
    {
        if (this.Mailbox.MailboxModel.UseDatabaseQuotaDefaults)
        {
            return APM_ExBBContent.MailboxDetails_DatabaseLimitUsed;
        }

        return APM_ExBBContent.MailboxDetails_CustomLimitUsed;
    }

    protected string GetSimpleStatus()
    {
        switch (mailbox.MailboxModel.Status.GetValueOrDefault())
        {
            case(14):
                return "critical";
            case (3):
                return "warning";
            default:
                return "up";
        }
    }

	protected string GetLastSuccessfulMailboxPoll()
	{
		var date = Mailbox.MailboxDatabase.LastSuccessfulMailboxPoll;
		if (date.HasValue) 
		{
			return date.Value
				.ToLocalTime()
				.ToString("f", CultureInfo.CurrentUICulture);
		}
		return CoreWebContent.WEBCODE_VB0_242;
	}

    protected string GetLastLogonInfo()
    {
        MailboxModel model = this.Mailbox.MailboxModel;
        if (!model.LastLogonTime.HasValue)
        {
            return model.Status.GetValueOrDefault() == 10 //offline
                ? APM_ExBBContent.NotAvailableData
                : APM_ExBBContent.MailboxDetails_NeverLoggedOn;
        }

        DateTime lastTime = model.LastLogonTime.Value;
        string lastLogonTime = lastTime.ToLocalTime().ToString("f", CultureInfo.CurrentUICulture);
        string lastLogonUser = model.LastLogonUserAccount;

        return string.IsNullOrEmpty(lastLogonUser)
            ? lastLogonTime
            : InvariantString.Format(APM_ExBBContent.MailboxDetails_LastLoggedOnFormat, lastLogonTime, lastLogonUser);
    }

	protected string GetLastModified() 
	{ 
		return Mailbox.MailboxModel.LastModified
			.ToLocalTime()
			.ToString("f", CultureInfo.CurrentUICulture);
	}

    protected string GetTypeLabel()
    {
        RecipientTypeDetails type = this.Mailbox.MailboxModel.Type;
        IEnumerable<RecipientTypeDetails> typeFlags = GetEnumValues().Where(item => type.HasFlag(item));
        IEnumerable<string> localizedFlags = typeFlags.Select(flag => flag.LocalizedLabel());
        return string.Join(",", localizedFlags);
    }

    private static IEnumerable<RecipientTypeDetails> GetEnumValues()
    {
        return Enum.GetValues(typeof(RecipientTypeDetails))
                   .OfType<RecipientTypeDetails>()
                   .Except(new List<RecipientTypeDetails> { RecipientTypeDetails.None });
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ApplicationStatusIcon.DataBind();
        ServerStatusIcon.DataBind();
    }
}
