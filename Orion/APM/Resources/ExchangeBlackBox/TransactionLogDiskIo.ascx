﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionLogDiskIo.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_TransactionLogDiskIo" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <script type="text/javascript">
            $(function () {
                var resourceId = <%= Resource.ID %>;

                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(resourceId);
                }

                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: resourceId,
                    cls: 'exchangeGrid',
                    allowSort: true,
                    showAllLimit: 50,
                    underscoreTemplateId: APMjs.format('#contentTemplate-{0}', resourceId),
                    explicitZebra: true,
                    headerTitles: [
                        '<%= ControlHelper.EncodeJsString(APM_ExBBContent.FolderPath) %>',
                        '<%= ControlHelper.EncodeJsString(APM_ExBBContent.TransactionLogDiskId_GridColHeader_Volume) %>',
                        '<%= ControlHelper.EncodeJsString(APM_ExBBContent.TransactionLogDiskId_GridColHeader_DiskQueueLength) %>',
                        '<%= ControlHelper.EncodeJsString(APM_ExBBContent.TransactionLogDiskId_GridColHeader_TotalIOPS) %>',
                        '<%= ControlHelper.EncodeJsString(APM_ExBBContent.TransactionLogDiskId_GridColHeader_Latency) %>',
                        '<%= ControlHelper.EncodeJsString(APM_ExBBContent.TransactionLogDiskId_GridColHeader_ExchangeServer) %>'
                    ],
                    defaultOrderBy: '[_activationPreference]'
                });
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var folderPath, folderPathIcon, volume, diskQueueLength, totalIops, latency, server,
                    ExBbRes = SW.APM.EXBB.Resources;

                function round(value) {
                    return (value === null)
                        ? ''
                        : SW.Core.Charts.dataFormatters.roundValue(value, 2).toLocaleString();
                }
				
				function appendUnit(value, unit) {
					var unbreakableSpace = String.fromCharCode(160);
                    return (value === null || value === '')
                        ? ''
                        : value + unbreakableSpace + unit;
                }
				
				function isNumber(n) {
				  return !isNaN(parseFloat(n)) && isFinite(n);
				}

                folderPathIcon = ExBbRes.DB.getLinkedIcon(Columns[0], Columns[1]);
                folderPath = Columns[14] || '<span class="italicText"><%= ControlHelper.EncodeJsString(APM_ExBBContent.UnknownValue) %></span>';

                volume = ExBbRes.DB.getLinkedIconCaption(Columns[2], Columns[3], Columns[4]);
                diskQueueLength = round(Columns[5]);
                totalIops = round(Columns[6]);
				if(Columns[7] != null && isNumber(Columns[7])) {
					latency =  round(Columns[7] * 1000);
					latency = appendUnit(latency, 'ms');
				}
				else {
					latency = '';
				}
                
                server = ExBbRes.DB.getAppOnNodeCaption(Columns[8], Columns[9], Columns[10], Columns[11], Columns[12], Columns[13]);
            #}
            <tbody class="zebraRow">
                <tr class="topRow">
                    <td colspan='6' style="border-bottom: 1px dashed #E0E0E0 !important;">{{ folderPathIcon }}{{ folderPath }}</td>
                </tr>
                <tr class="bottomRow">
                    <td>&nbsp;</td>
                    <td>{{ volume }}</td>
                    <td class="centeredColumn">{{ diskQueueLength }}</td>
                    <td class="centeredColumn">{{ totalIops }}</td>
                    <td class="centeredColumn">{{ latency }}</td>
                    <td>{{ server }}</td>
                </tr>
            </tbody>
        </script>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
    </Content>
</orion:resourceWrapper>
