﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

    [ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
    [ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
    public partial class Orion_APM_Resources_ExchangeBlackBox_UsersByMailboxSize : ApmBaseResource, IResourceIsInternal
    {
        #region BaseResourceControl properties

        protected override string DefaultTitle
        {
            get
            {
                return APM_ExBBContent.UserByMailboxSize_GridColHeader_UserName;
            }
        }

        public override string HelpLinkFragment
        {
            get
            {
                return this.ResourceHelpLinkFragment("SAMAGAppInsightForExchangeUsersByMailboxSize");
            }
        }

        public override IEnumerable<Type> RequiredInterfaces
        {
            get
            {
                return new[] { typeof(IExchangeApplicationProvider) };
            }
        }

        public override ResourceLoadingMode ResourceLoadingMode
        {
            get
            {
                return ResourceLoadingMode.Ajax;
            }
        }

        public override string SubTitle
        {
            get
            {
                if (!IsDbDetails)
                {
                    return string.Empty;
                }

                return database.LastSuccessfulMailboxPoll.HasValue
                           ? InvariantString.Format(
                               APM_ExBBContent.UserByMailboxSize_SubTitle,
                               database.LastSuccessfulMailboxPoll.Value.ToLocalTime().ToString(CultureInfo.CurrentUICulture)): string.Empty;
            }
        }

        #endregion

        #region Constants

        private static class SwqlParts
        {
            public const string SearchControlPath = @"~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx";

            public const string QueryBase = @"
SELECT{0}
    '{1}:' + ToString(mb.ID) as [_LinkFor_DisplayName],
	'/Orion/StatusIcon.ashx?entity={2}&size=small&status=' + ToString(mb.Status) as [_IconFor_DisplayName],
    mb.DisplayName,
    {3},
    {4},
    mb.AttachmentsSize,
    mb.AttachmentsCount,
    mb.LastLogonTime,
    dc.DatabaseName,
    mb.ID as [_mailboxID],
    {5} AS Limited,
    mb.ItemCount";

            public const string QueryFrom = @"
FROM Orion.APM.Exchange.Mailbox mb
JOIN Orion.APM.Exchange.DatabaseCopy dc
    ON dc.DatabaseID = mb.DatabaseID AND dc.IsActive = 1";

            public const string AppSelect = @",
    '{0}:' + ToString(dc.ID) as [_LinkFor_DatabaseName],
	'/Orion/StatusIcon.ashx?entity={1}&size=small&status=' + ToString(mb.Database.Status) as [_IconFor_DatabaseName],
    mb.Status,
    dc.ID";

            public const string WhereApp = @"
WHERE dc.ApplicationID = {0}";

            public const string WhereDb = @"
WHERE mb.DatabaseID = {0}";

            public const string SearchTrailer = @" AND mb.DisplayName LIKE '%${SEARCH_STRING}%'";

            public const string ColQuota = "mb.PercentageOfUsedQuota";

            public const string ColSize = "mb.TotalItemSize";

            //public const string Limitation = @"CASE WHEN mb.PercentageOfUsedQuota IS NULL THEN 0 ELSE 1 END";
            public const string Limitation = @"CASE
    WHEN (mb.UseDatabaseQuotaDefaults = True AND mb.Database.ProhibitSendQuota IS NULL AND mb.Database.ProhibitSendReceiveQuota IS NULL AND mb.Database.IssueWarningQuota IS NULL)
        OR (mb.UseDatabaseQuotaDefaults = False AND mb.ProhibitSendQuota IS NULL AND mb.ProhibitSendReceiveQuota IS NULL AND mb.IssueWarningQuota IS NULL)
    THEN 0 ELSE 1 END";            
            public const string LimitationCondition = @"mb.PercentageOfUsedQuota IS NOT NULL";

            public const string TopOne = " TOP 1";
        }
        #endregion

        #region Properties and Fields

        private ExchangeApplication application;

        private Database database;

        protected bool IsDbDetails
        {
            get { return this.database != null; }
        }

        protected bool BySize
        {
            get
            {
                var bySize = true;
                if (Resource.Properties.ContainsKey("bysize"))
                {
                    bool.TryParse(Resource.Properties["bysize"], out bySize);
                }
                return bySize;
            }
        }

        protected ResourceSearchControl SearchControl { get; private set; }

        #endregion

        private static string GetSWQL_App(ExchangeApplication app, bool isBySize, bool getFirst = false)
        {
            var sb = new StringBuilder();

            sb.AppendInvariantFormat(
                SwqlParts.QueryBase,getFirst?SwqlParts.TopOne:string.Empty,
                GetViewLink(ExchangeConstants.MailboxPrefix),
                SwisEntities.Mailbox,
                isBySize ? SwqlParts.ColSize : SwqlParts.ColQuota,
                isBySize ? SwqlParts.ColQuota : SwqlParts.ColSize,
                SwqlParts.Limitation);

            sb.AppendInvariantFormat(
                SwqlParts.AppSelect,
                GetViewLink(ExchangeConstants.DatabaseCopyPrefix),
                SwisEntities.Database);

            sb.Append(SwqlParts.QueryFrom);

            sb.AppendInvariantFormat(SwqlParts.WhereApp, app.Id);
        
            if (!isBySize)
            {
                sb.AppendInvariantFormat(" AND {0} = 1 AND {1}", SwqlParts.Limitation, SwqlParts.LimitationCondition);
            }
        
            return sb.ToString();
        }

        private static string GetSWQL_DB(Database db, bool isBySize, bool getFirst = false)
        {
            var sb = new StringBuilder();

            sb.AppendInvariantFormat(
                SwqlParts.QueryBase,getFirst?SwqlParts.TopOne:string.Empty,
                GetViewLink(ExchangeConstants.MailboxPrefix),
                SwisEntities.Mailbox,
                isBySize ? SwqlParts.ColSize : SwqlParts.ColQuota,
                isBySize ? SwqlParts.ColQuota : SwqlParts.ColSize,
                SwqlParts.Limitation);

            sb.Append(",mb.Status");
            sb.Append(SwqlParts.QueryFrom);

            sb.AppendInvariantFormat(SwqlParts.WhereDb, db.Id);
        
            if (!isBySize)
            {
                sb.AppendInvariantFormat(" AND {0} = 1 AND {1}", SwqlParts.Limitation, SwqlParts.LimitationCondition);
            }
        
            return sb.ToString();
        }

    protected string CustomChartUriTemplate
    {
        get
        {
            return CustomChartUriBuilder.Create(ExchangeConstants.MailboxQuotaUsedChartName,
                ExchangeConstants.MailboxPrefix, "{MailboxID}",
                InvariantString.Format(APM_ExBBContent.CustomDatabaseSize_Title, "{UserLogonName}"), false);
        }
    }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Initialize();

            if (!BySize)
            {
                using (var proxy = InformationServiceProxy.CreateV3())
                {
                    using (var result = proxy.Query(this.IsDbDetails ? GetSWQL_DB(this.database, this.BySize, true) : GetSWQL_App(this.application, this.BySize, true)))
                    {
                        if (result.Rows.Count != 1)
                        {
                            this.wrapper.Visible = false;
                            return;
                        }
                    }
                }
            }

            this.SearchControl = (ResourceSearchControl) LoadControl(SwqlParts.SearchControlPath);
            this.wrapper.HeaderButtons.Controls.Add(this.SearchControl);
            this.CustomTable.UniqueClientID = this.Resource.ID;
            this.CustomTable.SWQL = this.IsDbDetails ? GetSWQL_DB(this.database, this.BySize) : GetSWQL_App(this.application, this.BySize);
            this.CustomTable.SearchSWQL = this.CustomTable.SWQL + SwqlParts.SearchTrailer;
        }

        protected void Initialize()
        {
            var appProvider = this.GetInterfaceInstance<IExchangeApplicationProvider>();
            if (appProvider == null)
            {
                throw new InvalidOperationException("Cannot get provider.");
            }

            var dbProvider = appProvider as IDatabaseProvider;
            if (dbProvider != null)
            {
                this.database = dbProvider.ExchangeDatabase;
                if (this.database == null)
                {
                    throw new InvalidOperationException("Cannot get database.");
                }
            }

            this.application = appProvider.ExchangeApplication;
            if (this.application == null)
            {
                throw new InvalidOperationException("Cannot get database.");
            }
        }

        #region IResourceIsInternal Implementation

        public bool IsInternal
        {
            get
            {
                return true;
            }
        }

        #endregion
    }

