﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReplicationStatusChecks.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_ReplicationStatusChecks" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<%@ Register TagPrefix="apm" TagName="TopRightLinks" Src="~/Orion/APM/Controls/Views/TopRightPageLinks.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include runat="server" ID="Include2" File="APM/ExchangeBlackBox/Styles/ReplicationStatus.css"/>
        <orion:Include runat="server" ID="Include3" File="APM/ExchangeBlackBox/js/Resources.js" />

        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {
                var resourceId = <%= Resource.ID %>;

                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(resourceId);
                }

                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: resourceId,
                    cls: 'table-replication-status-checks',
                    initialPage: 0,
                    rowsPerPage: 100,
                    allowSort: false,
                    allowSearch: false,
                    explicitZebra: true,
                    headerTitles: [
                        '<%= APM_ExBBContent.ReplicationStatusChecks_Name %>', // [CheckName]
                        '<%= APM_ExBBContent.ReplicationStatusChecks_Status %>' // [Status]
                    ],
                    underscoreTemplateId: '#contentTemplate-' + resourceId,
                    customHeaderGenerator: function (columns) {
                        return $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0].attr('colspan', 2),
                                columns[1],
                                columns[0].clone(),
                                columns[1].clone()
                            );
                    },
                    customResultProcessor: function (result) {
                        result.Rows = SW.APM.Resources.CustomQuery.stackRows(result.Rows, 2);
                    },
                    onloaded: function ($grid) {
                        SW.APM.EXBB.Resources.trimReplicationStatusErrors($grid.find('td.col-error'));
                    }
                });

                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>

        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                function safeText(name) {
                    return (typeof name === 'string') ? name : '';
                }

                function formatImage(path) {
                    return (typeof path === 'string' && path) ? '<img src="' + path + '" />' : '';
                }

                function formatRow1Cells(name, status) {
                    var statusClass = SW.APM.EXBB.Resources.getReplicationStatusClass(status);
                    var statusName = SW.APM.EXBB.Resources.getReplicationStatusName(status);
                    var iconPath = SW.APM.EXBB.Resources.getReplicationStatusIcon(status);
                    return APMjs.format(
                        '<td class="col-name {1}">{0}</td><td class="col-icon {1}">{2}</td><td class="col-status {1}">{3}</td>',
                        safeText(name),
                        safeText(statusClass),
                        formatImage(iconPath),
                        safeText(statusName)
                    );
                }

                function formatErrorCell(error) {
                    return APMjs.format(
                        '<td class="col-error" colspan="3">{0}</td>',
                        error
                    );
                }

                function formatRow2(error1, error2) {
                    if (error1 === '' && error2 === '') {
                        return '';
                    }
                    return APMjs.format(
                        '<tr>{0}{1}</tr>',
                        formatErrorCell(error1),
                        formatErrorCell(error2)
                    );
                }
                var row1 = formatRow1Cells(Columns[0], Columns[1])
                    + formatRow1Cells(Columns[3], Columns[4]);

                var row2 = formatRow2(safeText(Columns[2]), safeText(Columns[5]));
            #}
            <tbody class="zebraRow">
                <tr>
                    {{ row1 }}
                </tr>
                {{ row2 }}
            </tbody>
        </script>
    </Content>
</orion:resourceWrapper>
