﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_MailboxDatabaseSizeAndSpaceUse : ApmBaseResource
{
    #region BaseResourceControl properties

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    protected override string DefaultTitle
    {
        get
        {
            return APM_ExBBContent.MailboxDatabaseSizeAndSpaceUse_Title;
        }
    }

    protected ExchangeApplication ExchangeApplication { get; set; }

    public override string SubTitle
    {
        get
        {
            string subTitlePart;

            int selectedIndex;
            if (!String.IsNullOrEmpty(Resource.Properties["ShowDatabases"]) &&
                Int32.TryParse(Resource.Properties["ShowDatabases"], out selectedIndex))
            {
                switch (selectedIndex)
                {
                    case 1:
                        subTitlePart = APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_ActiveDatabases;
                        break;
                    case 2:
                        subTitlePart = APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_PassiveDatabases;
                        break;
                    default:
                        subTitlePart = APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_AllDatabases;
                        break;
                }
            }
            else
            {
                subTitlePart = APM_ExBBContent.MailboxDatabaseResource_SubTitlePart_AllDatabases;
            }

            return APM_ExBBContent.MailboxDatabaseResource_SubTitle.FormatInvariant(subTitlePart);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForExchangeMailboxDatabaseSize"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/APM/ExchangeBlackBox/Controls/EditDatabaseResourceControl.ascx";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IExchangeApplicationProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    #endregion

    #region Properties and Fields

    private string SWQL
    {
        get
        {
            var additionalFilter = string.Empty;

            int selectedIndex;
            if (!String.IsNullOrEmpty(Resource.Properties["ShowDatabases"]) &&
                Int32.TryParse(Resource.Properties["ShowDatabases"], out selectedIndex))
            {
                switch (selectedIndex)
                {
                    case 1:
                        additionalFilter = "AND dc.IsActive = 1";
                        break;
                    case 2:
                        additionalFilter = "AND dc.IsActive = 0";
                        break;
                }
            }

            return @"
SELECT 	 
	'{2}:' + ToString(dc.ID) as [_databaseLink],
	'/Orion/StatusIcon.ashx?entity={3}&size=small&status=' + ToString(dc.Status) + '&hint=' + ToString(dc.IsActive) as [_databaseIconLink],
	dc.DatabaseName,
	df.Size, 
	df.AvailableWhiteSpace,
	CASE WHEN (df.Size = 0) THEN 0 
                ELSE Round(100.0 * (df.Size - ISNULL(df.AvailableWhiteSpace,0)) / (df.Size + df.AvailableSpace), 1) 
    END AS [DBUsedSpaceOnVolume],
    d.TotalMailboxes,
    d.AvgMailBoxSize,
    CASE WHEN (df.Size = 0) THEN 0 
                ELSE Round(100.0 * ISNULL(df.AvailableWhiteSpace,0) / (df.Size + df.AvailableSpace), 1) 
    END AS [_DBAvailableSpaceOnVolume],
    df.DatabaseFileID  as [_databaseFileID],
    df.PhysicalName,
    dc.ID,
    df.StatusDbUsage,
    df.StatusDbWhitespaceSize
FROM Orion.APM.Exchange.DatabaseCopy dc
JOIN Orion.APM.Exchange.DatabaseFile df ON df.DatabaseCopyID = dc.ID
JOIN Orion.APM.Exchange.Database d ON d.ID = dc.DatabaseID
WHERE dc.ApplicationID = {0} {1}"
                .FormatInvariant(ExchangeApplication.Id, 
                    additionalFilter,
                    "/Orion/View.aspx?NetObject={0}".FormatInvariant(ExchangeConstants.DatabaseCopyPrefix),
                    SwisEntities.DatabaseCopy, 
                    "/Orion/View.aspx?NetObject={0}".FormatInvariant(ExchangeConstants.DatabaseFilePrefix)
                );
        }
    }

    protected string CustomChartUriTemplate
    {
        get
        {
            return CustomChartUriBuilder.Create(ExchangeConstants.DatabaseFileSizeChartName,
                ExchangeConstants.DatabaseFilePrefix, "{DatabaseFileID}",
                InvariantString.Format(APM_ExBBContent.CustomDatabaseSize_Title, "{Name}"), false);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeApplication();

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = SWQL;
    }

    private void InitializeApplication()
    {
        var dbProvider = GetInterfaceInstance<IExchangeApplicationProvider>();
        if (dbProvider == null)
            throw new InvalidOperationException("Cannot get a database.");

        ExchangeApplication = dbProvider.ExchangeApplication;
    }
}
