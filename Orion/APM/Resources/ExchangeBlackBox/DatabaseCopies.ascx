﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseCopies.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_DatabaseCopies" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                function refresh () {
                    SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>);
                }

                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: <%= Resource.ID %>,
                    cls: 'exchangeGrid',
                    initialPage: 0,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                    allowSort: true,
                    allowSearch: false,
                    showAllLimit: 50,
                    headerTitles: [
                        '<%= Resources.APM_ExBBContent.DatabaseFileSize_ExchangeServer %>', //'[nodeCaption]',
                        '<%= Resources.APM_ExBBContent.DatabaseDetails_CopyStatus %>', // '[CopyState]'
                        '<%= Resources.APM_ExBBContent.DatabaseCopies_CopyQueueLength %>', // '[CopyQueueLength]'
                        '<%= Resources.APM_ExBBContent.DatabaseCopies_ReplayQueueLength %>', // '[ReplayQueueLength]'
                        '<%= Resources.APM_ExBBContent.DatabaseCopies_LastInspectedLogTime %>', // '[LastInspectedLogTime]'
                        '<%= Resources.APM_ExBBContent.DatabaseCopies_ContentIndexState %>', // '[ContentIndexState]'
                        '<%= Resources.APM_ExBBContent.DatabaseCopies_ActivationPreference %>' // '[ActivationPreference]'
                    ],
                    customHeaderGenerator: function (columns) {
                        return $headerRow = $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0].attr('colspan', 2),
                                columns[1],
                                columns[2],
                                columns[3],
                                columns[4],
                                columns[5],
                                columns[6]
                            );
                    },
                    underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
                    defaultOrderBy: '[ActivationPreference]'
                });
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
             {#
                var appOnNode, linkedIcon, copyStatus, copyQueue, replayQueue,
                    lastInspected, contentIndexState, activationPreference,
                    copyQueueClass, replayQueueClass, regionalSettings,
                    ExBbRes = SW.APM.EXBB.Resources;

                function getClass(value) {
                    value = Number(value);
                    return (value === 14)
                        ? 'redAndBold'
                        : (value === 3)
                            ? 'red'
                            : '';
                }

                function formatDate(value) {
                    return (value === null)
                        ? ''
                        : ExBbRes.DB.formatDateWithRegoinalSettings(
                            ExBbRes.DB.utcToLocalTime(value),
                            ExBbRes.Constants.SHORT_DATE_FORMAT,
                            regionalSettings
                        );
                }

                appOnNode = ExBbRes.DB.getAppOnNodeCaption(Columns[0], Columns[1], Columns[2], Columns[3], Columns[4], Columns[5]);
                linkedIcon = ExBbRes.DB.getLinkedIcon(Columns[6], Columns[7]);
                copyStatus = ExBbRes.getCopyStatus(Columns[8]).name;
                copyQueue = Columns[8];
                replayQueue = Columns[9];
                regionalSettings = <%= DatePickerRegionalSettings.GetDatePickerRegionalSettings() %>;
                lastInspected = formatDate(Columns[11]);
                contentIndexState = ExBbRes.getContentIndexStatusType(Columns[12]).name;
                activationPreference = Columns[13];
                copyQueueClass = getClass(Columns[16]);
                replayQueueClass = getClass(Columns[17]);
            #}

            <tr class="topRow">
                <td>{{ appOnNode }}</td>
                <td class="fixedColumn">{{ linkedIcon }}</td>
                <td>{{ copyStatus }}</td>
                <td style="text-align: left" class="{{ copyQueueClass }}">{{ copyQueue }}</td>
                <td style="text-align: left" class="{{ replayQueueClass }}">{{ replayQueue }}</td>
                <td>{{ lastInspected }}</td>
                <td class="fixedColumn">{{ contentIndexState }}</td>
                <td class="fixedColumn">{{ activationPreference }}</td>
            </tr>

        </script>
    </Content>
</orion:resourceWrapper>
