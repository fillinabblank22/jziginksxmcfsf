﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_MailboxQuotaUsedChart : StandardChartResource, IResourceIsInternal
{
    private readonly Log log = new Log();
    private string netObjectID;

    #region Properties

    protected override string DefaultTitle
    {
        get { return APM_ExBBContent.MailboxQuotaUsedChart_Title; }
    }

    public string NetObjectID
    {
        get
        {
            if (netObjectID != null) return netObjectID;

            var mailboxProvider = GetInterfaceInstance<IMailboxProvider>();

            netObjectID = mailboxProvider != null
                ? mailboxProvider.ExchangeMailbox.NetObjectID
                : GetNetObjectIDFromCustomChart();

            return netObjectID;
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return ExchangeConstants.MailboxPrefix; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IMailboxProvider) };
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", ExchangeConstants.MailboxQuotaUsedChartName);
        }

        try
        {
            HandleInit(WrapperContents);
        }
        catch (Exception exc)
        {
            log.Error("Cannot initialize StandardChartResource", exc);
        }

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "ExchangeBlackBox/js/Charts.js", OrionInclude.Section.Middle);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckAccountLimitation();
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var elementsList = new string[0];
        if (NetObjectID != null)
        {
            elementsList = new[] {NetObjectID};
        }

        if (!elementsList.Any())
        {
            ForceDisplayOfNeedsEditPlaceholder = true;
        }

        return elementsList;
    }

    private string GetNetObjectIDFromCustomChart()
    {
        return NetObjectHelper.GetNetObjectId(Resource.Properties["netobjectprefix"], int.Parse(Resource.Properties["elementlist"]));
    }

    private void CheckAccountLimitation()
    {
        if (NetObjectID == null) return;

        try
        {
            NetObjectFactory.Create(NetObjectID);
        }
        catch (AccountLimitationException)
        {
            TransferPageAccountLimitationError();
        }
    }

    private void TransferPageAccountLimitationError()
    {
        var url = "/Orion/AccountLimitationError.aspx?NetObject={0}".FormatInvariant(netObjectID);
        Page.Server.Transfer(url);
    }

    #region IResourceIsInternal Implementation

    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    #endregion
}
