﻿using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_APM_Resources_ExchangeBlackBox_ReplicationStatusChecks : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IExchangeApplicationProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string EditURL
    {
        get
        {
            var urlBuilder = new UrlBuilder(base.EditURL);
            urlBuilder["ShowFilter"] = bool.FalseString;
            urlBuilder["ShowMaxCount"] = bool.FalseString;

            return urlBuilder.Url;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return this.ResourceHelpLinkFragment("SAMAGAppInsightForExchangeReplicationStatusChecks");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        const string SwqlQuery = @"
SELECT
    [CheckName],
    [Status],
    [Error] as [_error]
FROM {0}
WHERE [ApplicationID] = {1}";

        var application = GetInterfaceInstance<IExchangeApplicationProvider>().ExchangeApplication;

        this.CustomTable.UniqueClientID = this.Resource.ID;
        this.CustomTable.SWQL = InvariantString.Format(SwqlQuery, SwisEntities.ReplicationStatus, this.ApmApplication.Id);

        var isReplicationStatusDisabled = ComponentDal.GetComponentStateByTemplateUid(application.Id, ComponentIdentifiers.IDReplicationStatus);
        this.Wrapper.Visible = (application.DAG != null) && !isReplicationStatusDisabled;
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_ExBBContent.ReplicationStatus_Title; }
    }
}
