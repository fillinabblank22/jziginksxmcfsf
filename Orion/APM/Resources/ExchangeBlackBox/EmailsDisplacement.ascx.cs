﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_EmailsDisplacement : StandardChartResource, IResourceIsInternal
{
    protected override string DefaultTitle => APM_ExBBContent.Web_EmailsDisplacement_Resource_Title;

    protected override bool AllowCustomization => Profile.AllowCustomize;

    protected override string NetObjectPrefix => ExchangeConstants.MailboxPrefix;

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IMailboxProvider) };

    public bool IsInternal => true;

    protected void Page_Init(object sender, EventArgs e)
	{
		HandleInit(WrapperContents);

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Common.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "ExchangeBlackBox/js/Charts.js", OrionInclude.Section.Middle);

	    var server = GetInterfaceInstance<IMailboxProvider>().ExchangeMailbox.MailboxDatabase.MountedServer;
        var isMailboxAccountsDisabled = ComponentDal.GetComponentStateByTemplateUid(server.Id, ComponentIdentifiers.IDMailboxAccountDetails);
        if (isMailboxAccountsDisabled)
        {
            Wrapper.Visible = false;
        }

	}

	protected override Dictionary<string, object> GenerateDisplayDetails()
	{
		var props = Resource.Properties;
		if (!props.ContainsKey("OptionsSetting_1"))
		{
			if (props["chartname"] == "BBEXReceivedMail")
			{
				props["OptionsSetting_1"] = "options.yAxis[0].title.text = 'Messages Received'";
			}
			else
			{
				props["OptionsSetting_1"] = "options.yAxis[0].title.text = 'Messages Sent'";
			}
		}

		var details = base.GenerateDisplayDetails();
		details["title"] = String.Empty;

		return details;
	}


	protected override IEnumerable<string> GetElementIdsForChart()
	{
		return new[] { GetInterfaceInstance<IMailboxProvider>().ExchangeMailbox.NetObjectID };
	}

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment(string.Empty); }
    }
}