﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionLogFileSize.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_TransactionLogFileSize" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>);
                }
                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: <%= Resource.ID %>,
                    cls: 'exchangeGrid',
                    allowSort: true,
                    showAllLimit: 50,
                    underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
                    explicitZebra: true,
                    headerTitles: [
                        '<%= APM_ExBBContent.FolderPath %>',
                        '<%= APM_ExBBContent.TransactionLogFileSize_GridColHeader_NumberOfFiles %>',
                        '<%= APM_ExBBContent.TransactionLogFileSize_GridColHeader_TotalFileSize %>',
                        '<%= APM_ExBBContent.TransactionLogFileSize_GridColHeader_VolumeUsage %>',
                        '<%= APM_ExBBContent.TransactionLogFileSize_GridColHeader_ExchangeServer %>'
                    ],
                    customHeaderGenerator: function(columns) {
                        var headerRow = $('<tr/>').addClass('HeaderRow');
                        columns[0].appendTo(headerRow);
                        columns[1].appendTo(headerRow);
                        columns[2].appendTo(headerRow);
                        columns[3].attr('colspan', '2');
                        columns[3].appendTo(headerRow);
                        columns[4].appendTo(headerRow);
                        return headerRow;
                    },
                    defaultOrderBy: '[_activationPreference]'
                });
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            <tbody class="zebraRow">
                <tr class="topRow">
                    <td colspan='7' style="border-bottom: 1px dashed #E0E0E0 !important;">{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[0], Columns[1])}}{{ Columns[12]===null?'<span class="italicText"><%= APM_ExBBContent.UnknownValue %></span>':Columns[12] }}</td>
                </tr>
                <tr class="bottomRow">
                    <td>&nbsp;</td>
                    <td>{{ [Columns[2]] }}</td>
                    <td>{{SW.Core.Charts.dataFormatters.byte(Columns[3],0)}}</td>
                    <td>{{SW.APM.EXBB.Resources.DB.getLink(Columns[4], SW.APM.EXBB.Resources.DB.getPercentageLabel(SW.Core.Charts.dataFormatters.roundValue(Columns[5],1)))}}</td>
                    <td>{{SW.APM.EXBB.Resources.DB.getLink(Columns[4], SW.APM.EXBB.Resources.DB.getVolumePercentStatusBar(Columns[5], Columns[14]))}}</td>
                    <td>{{SW.APM.EXBB.Resources.DB.getAppOnNodeCaption(Columns[6], Columns[7], Columns[8], Columns[9], Columns[10], Columns[11])}}</td>
                </tr>
            </tbody>
        </script>
    </Content>
</orion:resourceWrapper>
