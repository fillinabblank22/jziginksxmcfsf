﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MailboxDatabaseSizeAndSpaceUse.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_MailboxDatabaseSizeAndSpaceUse" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: "exchangeGrid",
                        allowSort: true,
                        showAllLimit: 50,
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        headerTitles: ["<%= APM_ExBBContent.MailboxDatabaseSizeAndSpaceUse_GridColHeader_DatabaseName %>",
                            "<%= APM_ExBBContent.MailboxDatabaseSizeAndSpaceUse_GridColHeader_DatabaseSize %>",
                            "<%= APM_ExBBContent.MailboxDatabaseSizeAndSpaceUse_GridColHeader_WhiteSpace %>",
                            "<%= APM_ExBBContent.MailboxDatabaseSizeAndSpaceUse_GridColHeader_SpaceUsage %>",
                            "<%= APM_ExBBContent.TotalMailboxes %>",
                            "<%= APM_ExBBContent.DatabaseDetails_MailboxSize %>"],
                        customHeaderGenerator: function(columns) {
                            var headerRow = $('<tr/>').addClass('HeaderRow');
                            columns[0].attr('colspan', '2');
                            columns[0].appendTo(headerRow);
                            columns[1].appendTo(headerRow);
                            columns[2].appendTo(headerRow);
                            columns[3].appendTo(headerRow);
                            columns[4].appendTo(headerRow);
                            columns[5].appendTo(headerRow);
                            return headerRow;
                        },
                        defaultOrderBy: "DatabaseName ASC"
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#                
                var statusDbUsage = Columns[12];
                var statusDbWhitespaceSize = Columns[13];               
                var combinedStatus = statusDbUsage == 14 || statusDbWhitespaceSize == 14 ? 14 : statusDbUsage  == 3 || statusDbWhitespaceSize == 3 ? 3 : 1;
                
                var dbUsageClass = statusDbUsage == 14 ? 'redAndBold' : statusDbUsage  == 3 ? 'red' : '';
                var dbWhitespaceClass = statusDbWhitespaceSize == 14 ? 'redAndBold' : statusDbWhitespaceSize  == 3 ? 'red' : '';
                                
                var customChartUrl='<%= CustomChartUriTemplate %>'.replace(/{Name}/g, encodeURIComponent(Columns[10])).replace(/{DatabaseFileID}/g, Columns[9]);
                
                var tms = Columns[6];
                var totalMailboxes  = parseInt(tms) ? tms: '<%= APMWebContent.UnknownValue %>';   

                var ams = Columns[7];
                var avgMailboxSize = parseInt(ams) ? SW.Core.Charts.dataFormatters.byte(ams,0) : '<%= APMWebContent.UnknownValue %>';                   
            #}
            <tr class="topRow">
                <td class="iconColumn">{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[0], Columns[1])}}</td>
                <td>{{SW.APM.EXBB.Resources.DB.getLink(Columns[0], Columns[2])}}</td>
                <td style="text-align: left" class="{{ dbUsageClass }}">{{SW.Core.Charts.dataFormatters.byte(Columns[3],0)}}</td>
                <td style="text-align: left" class="{{ dbWhitespaceClass }}">{{SW.Core.Charts.dataFormatters.byte(Columns[4],0)}}</td>
                <td>{{SW.APM.EXBB.Resources.DB.getLink(customChartUrl,SW.APM.EXBB.Resources.DB.getDatabasePercentStatusBar(Columns[5], Columns[8], combinedStatus))}}</td>
                <td>{{totalMailboxes}}</td>
                <td>{{avgMailboxSize}}</td>
            </tr> 
        
        </script>
    </Content>
</orion:resourceWrapper>
