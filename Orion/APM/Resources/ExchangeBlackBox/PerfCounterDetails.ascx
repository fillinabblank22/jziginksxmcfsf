﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerfCounterDetails.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_PerfCounterDetails" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <asp:PlaceHolder ID="ctrStatisticPlaceHolder" runat="server">
            <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
	        <orion:Include ID="I2" File="js/OrionCore.js" runat="server"/>        
		    <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
		    <orion:Include ID="I5" File="APM/js/NodeManagement.js" runat="server" />

		    <orion:Include ID="I6" File="APM/Styles/Resources.css" runat="server" />
            <orion:Include ID="I7" File="APM/js/Charts/Charts.APM.Chart.Formatters.js" runat="server" />
            <orion:Include ID="I8" File="APM/ExchangeBlackBox/Styles/Resources.css" runat="server" />

            <script type="text/javascript">
                $(document).ready(function () {
            		SW.APM.TrimStatusDetails();
            		var html = SW.Core.Charts
						.formatValue(<%= CurrentValue == "NA" ? "\"" + CurrentValue + "\"" : CurrentValue %>, { unit: "<%= CurrentValueUnit %>" }, [<%= String.Join(", ", ValidThresholdValues) %>])
            		$("#currentValue").html(html);
            	});
            </script>

            <table cellspacing="0" class="counterDetails biggerPadding NeedsZebraStripes">
			    <tr>
				    <td class="PropertyHeader">Name</td>
				    <td>&nbsp;</td>
				    <td class="Property">
				        <asp:Image ID="ctrCmpImgStatus" alt="status_icon" runat="server" /> &nbsp;
				        <asp:Literal ID="ctrCmpName" runat="server"/>
				    </td>
			    </tr>
			    <tr>
				    <td class="PropertyHeader">Status</td>
				    <td>&nbsp;</td>
			        <td class="Property"><asp:Literal ID="ctrCmpStatus" runat="server" /></td>
			    </tr>
                <% if (ShowCounterError()) { %>
			    <tr>
				    <td colspan="3">
					    <%= GetCounterError().GetStatusDetailsInline()%>
				    </td>
			    </tr>
                <% } %>
   			    <tr>
				    <td class="PropertyHeader">Current Value</td>
				    <td>&nbsp;</td>
			        <td id="currentValue" class="PropertyCurrentValue<%= ComponentStatusDescription %>"></td>
			    </tr>
			    <tr>
				    <td class="PropertyHeader">Group</td>
				    <td>&nbsp;</td>
				    <td class="Property"><asp:Literal ID="ctrCmpCounterGroup" runat="server"/></td>
			    </tr>
			    <tr>
				    <td class="PropertyHeader">
					    <%= Resources.APM_SQLBBContent.PerformanceCounterDetails_ComponentType %>
				    </td>
				    <td>&nbsp;</td>
				    <td class="Property"><asp:Literal ID="ctrCmpCounterType" runat="server"/></td>
			    </tr>
                 <tr>
				    <td class="PropertyHeader">Exchange Performance Counter</td>
				    <td>&nbsp;</td>
				    <td class="Property">
                        <asp:Literal ID="ctrCmpExchangePerformanceCounter" runat="server"/>
				    </td>
			    </tr>
		    </table>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="ctrInvalidCmpPlaceHolder" Visible="False" runat="server">
            <div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span>Selected component is not compatible with this resource.</div>
        </asp:PlaceHolder>
	</Content>
</orion:resourceWrapper>