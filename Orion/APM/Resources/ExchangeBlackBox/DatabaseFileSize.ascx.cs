﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_DatabaseFileSize : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    protected IDatabaseProvider DbProvider { get; set; }

    protected string CustomChartUriTemplate
    {
        get
        {
            return CustomChartUriBuilder.Create(ExchangeConstants.DatabaseFileSizeChartName,
                ExchangeConstants.DatabaseFilePrefix, "{DatabaseFileID}",
                InvariantString.Format(Resources.APM_ExBBContent.CustomDatabaseSize_Title, "{Name}"), false);
        }
    }

    private const string SwqlQueryTemplate = @"
SELECT
    dc.Status,
    df.Size,
    CASE WHEN (df.Size = 0) THEN 0 ELSE Round(100 * df.Size * 1.0 / (df.Size + df.AvailableSpace) * 1.0, 1) END AS DBUsageOnVolume,
    CASE WHEN (df.Size = 0) THEN 0 ELSE Round(100 * (df.Size - ISNULL(df.AvailableWhiteSpace,0)* 1.0 ) / (df.Size + df.AvailableSpace)* 1.0, 1) END AS [_DBUsedSpaceOnVolume],
    CASE WHEN (df.Size = 0) THEN 0 ELSE Round(100 * ISNULL(df.AvailableWhiteSpace,0)* 1.0  / (df.Size + df.AvailableSpace)* 1.0 , 1) END AS [_DBAvailableSpaceOnVolume],
    CASE WHEN (v.Size = 0) THEN 0 ELSE Round(100 * df.Size / v.Size, 1) END AS [VolumeUsage],
    df.PhysicalName AS [_FilePath],
    a.Name AS [_AppName],
    n.Caption AS [NodeCaption],
    '/Orion/StatusIcon.ashx?entity={1}&size=small&status=' + ToString(dc.Status) + '&hint=' + ToString(dc.IsActive) as [_databaseIconLink],
    '{2}:' + ToString(dc.ID) as [_dbLink],
    '{3}:' + ToString(a.ApplicationID) as [_appLink],
    '/Orion/StatusIcon.ashx?entity={4}&size=small&status=' + ToString(a.CurrentStatus.Availability) as [_appIconLink],
    '{5}:' + ToString(n.NodeID) as [_nodeLink],
    '/Orion/images/StatusIcons/Small-' + ToString(n.StatusIcon) as [_nodeIconLink],
    '{6}:' + ToString(v.VolumeId) as [_volumeLink],
    df.DatabaseFileID  as [_databaseFileID],
    dc.ID,
    df.StatusDbUsage,
    df.StatusDbWhitespaceSize,
    dc.ActivationPreference as [_activationPreference]
FROM Orion.APM.Exchange.DatabaseFile df
JOIN Orion.APM.Exchange.DatabaseCopy dc
    ON df.DatabaseCopyID = dc.ID
JOIN Orion.APM.Exchange.Application a
    ON dc.ApplicationID = a.ApplicationID
JOIN Orion.Nodes n
    ON n.NodeID = a.NodeID
LEFT JOIN Orion.Volumes v
    ON v.VolumeID = df.VolumeID
WHERE dc.DatabaseID = {0}";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DbProvider = this.GetInterfaceInstance<IDatabaseProvider>();
        if (this.DbProvider == null)
        {
            throw new InvalidOperationException("Cannot get database ID for filtering files by database ID");
        }

        this.CustomTable.UniqueClientID = this.Resource.ID;
        this.CustomTable.SWQL = InvariantString.Format(
            SwqlQueryTemplate,
            this.DbProvider.ExchangeDatabase.Id, SwisEntities.DatabaseCopy,
            InvariantString.Format(
                "/Orion/View.aspx?NetObject={0}",
                ExchangeConstants.DatabaseCopyPrefix
            ),
            GetViewLink(ExchangeConstants.ApplicationPrefix),
            SwisEntities.Application,
            GetViewLink("N"),
            GetViewLink("V"),
            InvariantString.Format(
                "/Orion/View.aspx?NetObject={0}",
                ExchangeConstants.DatabaseFilePrefix
            )
        );
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_ExBBContent.DatabaseFileSize_DefaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForExchangeDatabaseFileSize"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IDatabaseProvider) }; }
    }

    public override string SubTitle
    {
        get
        {
            return InvariantString.Format(
                Resources.APM_ExBBContent.DatabaseFileSize_SubTitle,
                this.DbProvider.ExchangeDatabase.Name
                );
        }
    }
}
