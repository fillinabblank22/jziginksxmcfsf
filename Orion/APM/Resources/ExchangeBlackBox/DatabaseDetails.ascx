﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseDetails.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_DatabaseDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Register TagPrefix="apm" TagName="ComponentErrorStatusControl" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="Include2" File="APM/Styles/Resources.css" runat="server"/>
        <orion:Include ID="Include1" File="APM/APM.css" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server" />
        <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />
        <orion:Include ID="I7" File="APM/ExchangeBlackBox/Styles/Resources.css" runat="server" />
        
        <script type="text/javascript">
            $().ready(function () {
                SW.APM.TrimStatusDetails();
            });
        </script>

        <table cellspacing="0" class="NeedsZebraStripes biggerPadding">
            <tr>
                <td class="PropertyHeader" style="min-width: 140px;"><%= APM_ExBBContent.DatabaseDetails_DbName %></td>
                <td class="Property statusAndText">
                    <asp:Image ID="DbStatusIcon" CssClass="apm_StatusIcon" runat="server" />&nbsp;<%= Db.Name %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.DatabaseDetails_PerfStatus %></td>
                <td class="Property <%= Db.ApmStatus.Value == Status.Critical ? "boldAndRed" : Db.ApmStatus.Value == Status.Warning ? "red" : string.Empty %>">
                    <%= Db.ApmStatus.ToLocalizedString() %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.DatabaseDetails_ActiveCopyStatus %></td>
                <% if (DbCopyStatus == CopyStatus.Mounted) { %>
                <td class="Property statusAndText">
                    <%= GetCopyStatus() %>&nbsp;
                    <% if (Db.MountedServer != null) { %>
                    <apm:SmallApmAppStatusIcon ID="AppCopyStatusIcon" runat="server" />
                    <a href="<%= GetViewLink(Db.MountedServer.NetObjectID) %>"><%= Db.MountedServer.Name %></a>
                    on
                    <orion:SmallNodeStatus ID="ServerCopyStatusIcon" runat="server" />
                    <a href="<%= GetViewLink(Db.MountedServer.NPMNode.NetObjectID) %>"><%= Db.MountedServer.NodeName %></a>
                    <% } else { %>
                    <%= MountedServerName + APM_ExBBContent.ServerNotMonitored %>
                    <% } %>
                </td>
                <% } else { %>
                <td class="Property"><%= GetCopyStatus() %></td>
                <% } %>
            </tr>

            <% if (Db.MasterType == MasterType.DatabaseAvailabilityGroup) { %>
            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.DatabaseDetails_PreferredServer %></td>
                <td class="Property statusAndText">
                    <% if (Db.PreferredServer != null) { %>
                    <orion:SmallNodeStatus ID="PreferredServerStatusIcon" runat="server" />
                    <a href="<%= GetViewLink(Db.PreferredServer.NPMNode.NetObjectID) %>"><%= Db.PreferredServer.NodeName %></a>
                    <% } else { %>
                    <%= PreferredServerName + APM_ExBBContent.ServerNotMonitored %>
                    <% } %>
                </td>
            </tr>
            <% } %>

            <% if (IsOnDifferentServer) { %>
            <tr>
                <td colspan="2">
                    <div class="warning-container">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><label><%= APM_ExBBContent.DatabaseDetails_NotOnPreferredServer %></label>
                    </div>
                </td>
            </tr>
            <% } %>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.DatabaseDetails_Master %></td>
                <td class="Property statusAndText"><%= GetMaster() %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.DatabaseDetails_MailboxNumber %></td>
                <td class="Property">
                <% if (MailboxNumber.HasValue)
                   {
                       if (MailboxNumber == 1)
                       { %>
                    <%= String.Format(APM_ExBBContent.DatabaseDetails_MailboxNumber_Format_Singular, MailboxNumber) %>
                    <% } else { %>
                    <%= String.Format(APM_ExBBContent.DatabaseDetails_MailboxNumber_Format_Plural, MailboxNumber) %>
                    <% }
                   }else{ %>
                    <%= APMWebContent.UnknownValue %>
                    <% } %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.DatabaseDetails_MailboxSize %></td>
                <td class="Property"><%= GetSize(MailboxSize) %></td>
            </tr>

            <tr>
                <td class="PropertyHeader" style="vertical-align: baseline"><%= APM_ExBBContent.DatabaseDetails_FullBackup %></td>
                <td class="Property"><%= GetTimeElapsed(Db.DatabaseModel.LastFullBackup) %></td>
            </tr>

            <tr>
                <td class="PropertyHeader" style="vertical-align: baseline"><%= APM_ExBBContent.DatabaseDetails_IncrementalBackup %></td>
                <td class="Property"><%= GetTimeElapsed(Db.DatabaseModel.LastIncrementalBackup) %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.DatabaseDetails_CircularLogging %></td>
                <td class="Property"><%= Db.DatabaseModel.CircularLoggingEnabled.GetValueOrDefault()
                                     ? APM_ExBBContent.DatabaseDetails_Enabled
                                     : APM_ExBBContent.DatabaseDetails_Disabled %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader" style="white-space: nowrap; border: hidden;" colspan="2">
                    <%= APM_ExBBContent.DatabaseDetails_StorageQuotaDefaults %>
                </td>
            </tr>

            <tr>
                <td class="Property" style="white-space: nowrap; padding-left: 4em; border: hidden;">
                    <%= APM_ExBBContent.DatabaseDetails_StorageQuotaDefaults_IssueWarning %>
                </td>
                <td class="Property" style="border: hidden;"><%= GetSize(Db.DatabaseModel.IssueWarningQuota) %></td>
            </tr>

            <tr>
                <td class="Property" style="white-space: nowrap; padding-left: 4em; border: hidden;">
                    <%= APM_ExBBContent.DatabaseDetails_StorageQuotaDefaults_ProhibitSend %>
                </td>
                <td class="Property" style="border: hidden;"><%= GetSize(Db.DatabaseModel.ProhibitSendQuota) %></td>
            </tr>

            <tr>
                <td class="Property" style="white-space: nowrap; padding-left: 4em; border: hidden;">
                    <%= APM_ExBBContent.DatabaseDetails_StorageQuotaDefaults_ProhibitSendReceive %>
                </td>
                <td class="Property" style="border: hidden;"><%= GetSize(Db.DatabaseModel.ProhibitSendReceiveQuota) %></td>
            </tr>

            </table>
            <apm:ComponentErrorStatusControl
                ID="componentErrorStatusControl" 
                runat="server" 
                OnInit="ComponentErrorStatusControl_Init">
            </apm:ComponentErrorStatusControl>        
    </Content>
</orion:resourceWrapper>
