﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseDiskIo.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_DatabaseDiskIo" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssResources" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: 'exchangeGrid',
                        initialPage: 0,
                        allowSort: true,
                        showAllLimit: 50,
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        headerTitles: ["<%= APM_ExBBContent.FilePath %>", "<%= APM_ExBBContent.DatabaseDiskIo_Volume%>",
                            "<%= APM_ExBBContent.DatabaseDiskIo_DiskQueue%>",
                            "<%= APM_ExBBContent.DatabaseDiskIo_TotalIops%>",
                            "<%= APM_ExBBContent.DatabaseDiskIo_Latency%>",
                            "<%= APM_ExBBContent.DatabaseFileSize_ExchangeServer%>"
                        ],
                        defaultOrderBy: "[_activationPreference]"
                    });
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
		        {#
             
                function round(value) {
                    return (value === null)
                        ? ''
                        : SW.Core.Charts.dataFormatters.roundValue(value, 2).toLocaleString();
                }
				
				function appendUnit(value, unit) {
					var unbreakableSpace = String.fromCharCode(160);
                    return (value === null || value === '')
                        ? ''
                        : value + unbreakableSpace + unit;
                }
				
				function isNumber(n) {
				  return !isNaN(parseFloat(n)) && isFinite(n);
				}
				
                volume = SW.APM.EXBB.Resources.DB.getLinkedIconCaption(Columns[14], Columns[15], Columns[1]);
                diskQueueLength = round(Columns[2]);
                totalIops = round(Columns[3]);
				if(Columns[4] != null && isNumber(Columns[4])) {
					latency =  round(Columns[4] * 1000);
					latency = appendUnit(latency, 'ms');
				}
				else {
					latency = '';
				}
                
                server = SW.APM.EXBB.Resources.DB.getAppOnNodeCaption(Columns[10], Columns[11], Columns[5], Columns[12], Columns[13], Columns[6]);
            #}
            <tr class="topRow" >
                <td colspan="6" style="border-bottom: 1px dashed #E0E0E0 !important;">{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[9], Columns[8])}}{{Columns[7] === null?'<span class="italicText"><%= APM_ExBBContent.UnknownValue %></span>':Columns[7]}}</td>
            </tr>  
            <tr>
            </tr>
            <tr class="bottomRow">
                <td>&nbsp;</td>
                <td>{{ volume }}</td>
                <td>{{ diskQueueLength }}</td>
                <td>{{ totalIops }}</td>
                <td>{{ latency }}</td>
                <td>{{ server }}</td>
            </tr>
        </script>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
    </Content>
</orion:resourceWrapper>
