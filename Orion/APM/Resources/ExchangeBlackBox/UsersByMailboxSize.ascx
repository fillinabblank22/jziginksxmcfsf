﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsersByMailboxSize.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_UsersByMailboxSize" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= Resource.ID %>,
                        cls: "exchangeGrid",
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                        allowSort: true,
                        allowPaging: true,
                        allowSearch: true,
                        showAllLimit: 50,
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        underscoreTemplateId: "#contentTemplate-<%= Resource.ID %>",
                        headerTitles: ["<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_UserName %>",
<% if (this.BySize) { %>
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_MailboxSize %>", 
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_QuotaUsed %>",
<% } else { %>
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_QuotaUsed %>",
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_MailboxSize %>", 
<% } %>
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_AttachmentSize %>",
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_AttachmentCount %>",
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_LastAccessed %>",
<% if (!this.IsDbDetails) { %>
                            "<%= APM_ExBBContent.UserByMailboxSize_GridColHeader_Database %>"
<% } %>
                        ],
                        customHeaderGenerator: function(columns) {
                            var headerRow = $('<tr/>').addClass('HeaderRow');
                            columns[0].attr('colspan', '2');
                            columns[0].appendTo(headerRow);
                            columns[1].appendTo(headerRow);
                            columns[2].appendTo(headerRow);                        
<% if (this.BySize) { %>
                            columns[2].attr('colspan', '2');
<% } else { %>
                            columns[1].attr('colspan', '2');
<% } %>
                            columns[3].appendTo(headerRow);
                            columns[4].appendTo(headerRow);
                            columns[5].appendTo(headerRow);
<% if (!this.IsDbDetails) { %>                            
                            columns[6].attr('colspan', '2');
                            columns[6].appendTo(headerRow);
<% } %>
                            return headerRow;
                        },
                        defaultOrderBy: '<%= BySize?"[TotalItemSize] DESC":"[PercentageOfUsedQuota] DESC"%>'
                    });
                $('#' + '<%= SearchControl.SearchBoxClientID %>').attr("placeholder", "<%= APM_ExBBContent.SearchUsers %>");
                var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var quotaColNumber = <%= this.BySize ? 4 : 3 %>;
                var sizeColNumber = <%= this.BySize ? 3 : 4 %>;
                var stateColNumber = <%= this.IsDbDetails ? 12 : 14 %>;
                var quota = Columns[quotaColNumber];
                var status = Columns[stateColNumber];
                var limited = Columns[10];
                var hasData = isFinite(String(Columns[11]));
                var quotaUsed = '';
                var quotaBar = '<%= APM_ExBBContent.NoLimit %>';
                if (limited)
                {
                    quotaUsed = hasData ? SW.APM.EXBB.Resources.DB.getPercentageLabel(SW.Core.Charts.dataFormatters.roundValue(quota,0),true):'<%= APM_ExBBContent.NotAvailableData %>';
                    quotaBar = SW.APM.EXBB.Resources.DB.getMailboxPercentStatusBar(quota,status);
                }
                var quotaUsedClass = (status == 14) ? 'redAndBold': status  == 3 ? 'red' : '';
                var quotaBarClass = limited ? '' : 'italicText';
                var customChartUrl = '<%= CustomChartUriTemplate %>'.replace(/{UserLogonName}/g, encodeURIComponent(Columns[2])).replace(/{MailboxID}/g, Columns[9]);
            
                var size = Columns[sizeColNumber];
                var totalSize  = hasData ? SW.Core.Charts.dataFormatters.byte(size,0): '<%= APM_ExBBContent.NotAvailableData %>';                   
                var attachments = Columns[5];
                var attachmentsCount = Columns[6] == 10000 ? '>10000' : Columns[6];
                var attachmentsSize  = hasData ? Columns[6] == 10000 ? '>' + SW.Core.Charts.dataFormatters.byte(attachments,0) : SW.Core.Charts.dataFormatters.byte(attachments,0) : '<%= APM_ExBBContent.NotAvailableData %>';                   
                var regionalSettings = <%= DatePickerRegionalSettings.GetDatePickerRegionalSettings() %>;
                var date = Columns[7] == null ? '' : SW.APM.EXBB.Resources.DB.formatDateWithRegoinalSettings(SW.APM.EXBB.Resources.DB.utcToLocalTime(Columns[7]),SW.APM.EXBB.Resources.Constants.SHORT_DATE_FORMAT,regionalSettings);
            #}
            <tr class="topRow">
                <td style="padding-right:0px !important;">{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[0], Columns[1])}}</td>
                <td class="UsersFixedColumnWidth" ><div class="UsersFixedColumnEllipsis" >{{SW.APM.EXBB.Resources.DB.getLink(Columns[0], Columns[2])}}</div></td>
<% if (BySize) { %>
                <td>{{totalSize}}</td>
                <td class="{{quotaUsedClass}}">{{quotaUsed}}</td>
                <td class="{{quotaBarClass}}">{{SW.APM.EXBB.Resources.DB.getLink(customChartUrl,quotaBar)}}</td>
<% } else { %>
                <td class="{{quotaUsedClass}}">{{quotaUsed}}</td>
                <td class="{{quotaBarClass}}">{{SW.APM.EXBB.Resources.DB.getLink(customChartUrl,quotaBar)}}</td>
                <td>{{totalSize}}</td>
<% } %>
                <td class="fixedColumn">{{attachmentsSize}}</td>
                <td class="fixedColumn">{{attachmentsCount}}</td>
                <td>{{ date }}</td>
<% if (!this.IsDbDetails) { %>  
                <td style="padding-right:0px !important;">{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[12], Columns[13])}}</td>
                <td class="UsersFixedColumnWidth" ><div class="UsersFixedColumnEllipsis" >{{SW.APM.EXBB.Resources.DB.getLink(Columns[12], Columns[8])}}</div></td>
<% } %>
            </tr> 
        </script>
    </Content>
</orion:resourceWrapper>
