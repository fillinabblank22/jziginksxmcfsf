﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MailboxDatabaseCopies.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_MailboxDatabaseCopies" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>);
                }

                function decorateGrid(grid) {
                    var $firstCell, prevHtml = '';
                    $(grid).find('tr').next().each(function () {
                        var $value = $(this);
                        $firstCell = $value.find('td:first-child');
                        if ($firstCell.find('a').html() === prevHtml) {
                            $firstCell.empty();
                        }
                        $value.addClass('zebraRow');
                        prevHtml = $firstCell.find('a').html();
                    });
                }

                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: <%= Resource.ID %>,
                    cls: 'exchangeGrid',
                    allowSort: true,
                    showAllLimit: 50,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                    underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
                    headerTitles: [
                        '<%= APM_ExBBContent.MailboxDatabaseSizeAndSpaceUse_GridColHeader_DatabaseName %>', // [DatabaseName]
                        '<%= APM_ExBBContent.DatabaseDetails_CopyStatus %>', // [CopyState]
                        '<%= APM_ExBBContent.DatabaseCopies_ContentIndexState %>', // [ContentIndexState]
                        '<%= APM_ExBBContent.DatabaseFileSize_ExchangeServer %>' // [NodeCaption]
                    ],
                    customHeaderGenerator: function (columns) {
                        return $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0].attr('colspan', 2),
                                columns[1],
                                columns[2],
                                columns[3]
                            );
                    },
                    defaultOrderBy: '[Status]',
                    baseOrderBy: '[DatabaseName]',
                    onloaded: decorateGrid
                });
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var dbLink, dbIcon, copyStatus, indexState, nodeCaption,
                    indexStateClass,
                    ExBbRes = SW.APM.EXBB.Resources;

                dbLink = ExBbRes.DB.getLink(Columns[1], Columns[0]);
                dbIcon = ExBbRes.DB.getLinkedIcon(Columns[1], Columns[2]);
                copyStatus = ExBbRes.getCopyStatus(Columns[3]).name;
                indexState = ExBbRes.getContentIndexStatusType(Columns[4]).name;
                indexStateClass = ExBbRes.DB.getIndexStateCss(Columns[4], Columns[5]);
                nodeCaption = ExBbRes.DB.getAppOnNodeCaption(Columns[6], Columns[7], Columns[8], Columns[9], Columns[10], Columns[11]);
            #}
            <tr class="topRow">
                <td class="noWrap">{{ dbLink }}</td>
                <td>{{ dbIcon }}</td>
                <td>{{ copyStatus }}</td>
                <td class="{{ indexStateClass }}">{{ indexState }}</td>
                <td>{{ nodeCaption }}</td>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>
