﻿using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_MailboxSizeChart : StandardChartResource
{
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected string ChartName
    {
        get { return Resource.Properties.ContainsKey("ChartName") ? Resource.Properties["ChartName"] : "BBEXMailboxSize"; }
    }

    protected override string NetObjectPrefix
    {
        get { return "ABXMBS"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IMailboxProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_ExBBContent.Web_TotalMailboxAndAttachmentsSizes_Resource_Title; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected Mailbox Mailbox
    {
        get { return this.GetInterfaceInstance<IMailboxProvider>().ExchangeMailbox; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ChartLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);

        Resource.Properties["ChartName"] = ChartName;
        HandleInit(WrapperContents);
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new[] {Mailbox.Id.ToInvariantString()};
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        this.ChartTitle = string.IsNullOrEmpty(Request.QueryString["ChartTitle"]) ? ChartResourceSettings.Title : Request.QueryString["ChartTitle"];
        this.ChartSubTitle = string.IsNullOrEmpty(Request.QueryString["ChartSubTitle"]) ? ChartResourceSettings.SubTitle : Request.QueryString["ChartSubTitle"];

        var details = base.GenerateDisplayDetails();

        bool result;
        bool.TryParse(details["showThreshold"].ToString(), out result);

        var warning = Mailbox.IssueWarningQuota.HasValue ? Mailbox.IssueWarningQuota.GetValueOrDefault().ToInvariantString() : "null";
        var prohibitSend = Mailbox.ProhibitSendQuota.HasValue ? Mailbox.ProhibitSendQuota.GetValueOrDefault().ToInvariantString() : "null";

        var options = this.ChartResourceSettings.ChartSettings.ChartOptions;
        options = options.Replace("${ShowThreshold}", result ? "true" : "false");
        options = options.Replace("${warningThresholdValue}", warning);
        options = options.Replace("${criticalThresholdValue}", prohibitSend);
        options = options.Replace("${warningThresholdLabel}", "'Issue Warning'");
        options = options.Replace("${criticalThresholdLabel}", "'Prohibit Send'");
        this.ChartResourceSettings.ChartSettings.ChartOptions = options;

        return details;
    }
}