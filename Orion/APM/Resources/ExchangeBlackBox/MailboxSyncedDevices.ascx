﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MailboxSyncedDevices.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_MailboxSyncedDevices" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="CssResource" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css" />
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js" />
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: <%= Resource.ID %>,
                          cls: 'exchangeGrid',
                          initialPage: 0,
                          rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                    allowSort: true,
                    allowSearch: false,
                    showAllLimit: 50,
                    headerTitles:[
                        "<%= Resources.APM_ExBBContent.SyncedDevices_DeviceType %>",
                        "<%= Resources.APM_ExBBContent.SyncedDevices_DeviceUserAgent %>",
                        "<%= Resources.APM_ExBBContent.SyncedDevices_LastSuccesfulSync %>"
                    ],
                          underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>'
                      }); 
                      var refresh = function() { SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                      SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                      refresh();
                  });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            <tr class="topRow">
                <td class="fixedColumn">{{[Columns[0]]}}</td>
                <td class="fixedColumn">{{[Columns[1]]}}</td>
                <td class="fixedColumn">{{Columns[2] == null ? '' : SW.APM.EXBB.Resources.DB.utcToLocalTime(Columns[2]).toLocaleString()}}</td>
            </tr>
        </script>
    </Content>
</orion:resourceWrapper>
