﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_TransactionLogDiskIo : ApmBaseResource
{
    #region BaseResourceControl properties

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    protected override string DefaultTitle
    {
        get { return APM_ExBBContent.TransactionLogDiskIo_Title; }
    }

    public override string SubTitle
    {
        get { return InvariantString.Format(APM_ExBBContent.DatabaseResource_SubTitle, database.Name); }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForExchangeTransactionLogIO"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IDatabaseProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    #endregion

    #region Properties and Fields

    private Database database;

    private const string SwqlQuery = @"
SELECT 
    '{1}:' + ToString(dc.ID) as [_databaseLink],
	'/Orion/StatusIcon.ashx?entity={2}&size=small&status=' + ToString(dc.Status) + '&hint=' + ToString(dc.IsActive) as [databaseIconLink],
    '{3}:' + ToString(v.VolumeId) as [_volumeLink],
	'/Orion/StatusIcon.ashx?entity={4}&size=small&id=' + ToString(v.VolumeID) + '&status=' + ToString(v.Status) as [_volumeIconLink],
	ISNULL(v.Caption,'{5}') as [volumeCaption],
	Round(v.DiskQueueLength,2) as [diskQueueLength],
	Round(v.TotalDiskIOPS, 2) as [totalDiskIOPS],
	v.DiskTransfer as [VolumeLatency],
	'{6}:' + ToString(a.ApplicationID) as [_appLink],
	'/Orion/StatusIcon.ashx?entity={7}&size=small&status=' + ToString(a.CurrentStatus.Availability) as [_appIconLink],
	a.Name as [_appName],
	'{8}:' + ToString(n.NodeID) as [_nodeLink],
	'/Orion/images/StatusIcons/Small-' + ToString(n.StatusIcon) as [_nodeIconLink],
	n.Caption as [nodeCaption],
	tld.PhysicalName as [_physicalName],
    dc.ActivationPreference as [_activationPreference]
FROM Orion.APM.Exchange.TransactionLogDir tld
JOIN Orion.APM.Exchange.DatabaseCopy dc
    ON tld.DatabaseCopyID = dc.ID
JOIN Orion.APM.Exchange.Application a
    ON a.ApplicationID = dc.ApplicationID
JOIN Orion.Nodes n
    ON n.NodeID = a.NodeID
LEFT JOIN Orion.Volumes v
    ON v.VolumeID = tld.VolumeID
WHERE dc.DatabaseID = {0}";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeDatabase();
        
        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = InvariantString.Format(
            SwqlQuery,
            database.Id,
            InvariantString.Format(
                "/Orion/View.aspx?NetObject={0}",
                ExchangeConstants.DatabaseCopyPrefix),
            SwisEntities.DatabaseCopy,
            GetViewLink("V"),
            SolarWinds.Orion.Core.Common.NetObjectHelper.VolumeEntity,
            APM_ExBBContent.NotAvailableData,
            GetViewLink(ExchangeConstants.ApplicationPrefix),
            SwisEntities.Application,
            GetViewLink("N"));
    }

    private void InitializeDatabase()
    {
        var dbProvider = GetInterfaceInstance<IDatabaseProvider>();
        if (dbProvider == null)
        {
            throw new InvalidOperationException("Cannot get a database.");
        }

        database = dbProvider.ExchangeDatabase;
    }
}
