﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDetails.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_ApplicationDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Web" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>
<%@ Register TagPrefix="apm" TagName="ComponentErrorStatusControl" Src="~/Orion/APM/Controls/ComponentErrorStatusControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="AlertSuppressionStatus" Src="~/Orion/APM/Controls/Resources/AlertSuppressionStatus.ascx" %>
 
<apm:AlertSuppressionStatus ID="alertSuppressionStatus" runat="server" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server" />
        <orion:Include ID="I4" Module="APM" File="js/Resources.js" runat="server" />
        <orion:Include ID="I6" Module="APM" File="js/NodeManagement.js" runat="server" />
        <orion:Include ID="I5" Module="APM" File="Styles/Resources.css" runat="server" />
        <orion:Include ID="I7" Module="APM" File="ExchangeBlackBox/Styles/Resources.css" runat="server" />

        <script type="text/javascript">
            $(document).ready(function () {
                SW.APM.TrimStatusDetails();
                $('table.ExBbAppDetail tr.DagSpacer')
                    .slice(1)
                    .append($('<td colspan="2"><div></div></td>'));
            });
        </script>

        <table cellspacing="0" class="biggerPadding NeedsZebraStripes ExBbAppDetail">
            <tr>
                <td class="PropertyHeader">
                    <%= APM_ExBBContent.AppDetails_ApplicationName %>
                </td>

                <td class="Property statusAndText">
                    <apm:SmallApmAppStatusIcon ID="AppStatusIcon" runat="server"/>
                    <a href="<%= GetViewLink(App.NetObjectID) %>"><%= App.Name %></a>
                    on
                    <orion:SmallNodeStatus ID="ServerStatusIcon" runat="server"/>
                    <a href="<%= GetViewLink(App.NPMNode.NetObjectID) %>"><%= App.NodeName %></a>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_ExBBContent.AppDetails_Status %>
                </td>

                <td class="Property">
                    <span <%= App.Status.Value == Status.Critical?"class=\"boldAndRed\"" : App.Status.Value == Status.Warning?"class=\"red\"":string.Empty %>><%= App.Status %></span>
                    <div ID="alertSuppressionStatusDescription" class="alertSuppressionStatus" runat="server"></div>
                </td>
            </tr>
<% if (ShowApplicationError) { %>
            <tr>
                <td colspan="2">
                    <!-- ApplicationError -->
                    <%= ApplicationError.GetStatusDetailsInline(ExchangeConstants.ApplicationTemplateCustomType) %>
                </td>
            </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_ExBBContent.AppDetails_ServerVersion %>
                </td>

                <td class="Property"><%= App.ExchangeVersion %></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_ExBBContent.AppDetails_Build %>
                </td>

                <td class="Property"><%= App.BuildVersion %></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= APM_ExBBContent.AppDetails_Domain %>
                </td>

                <td class="Property"><%= (App.Domain != null)? App.Domain.Name : (InitialPollInProgress)? String.Empty : APM_ExBBContent.AppDetails_NoDomain %></td>
            </tr>
            <tr>
                <td class="PropertyHeader" colspan="2">
                    <!-- APM_ExBBContent.AppDetails_DAG_Title -->
                    <%= APM_ExBBContent.AppDetails_DAG_Title %>
                </td>
            </tr>
            <tr class="DagSpacer"></tr>
<% if (App.DAG == null && !InitialPollInProgress) { %>
            <tr class="DagRow">
                <td class="Property" colspan="2"><%= APM_ExBBContent.AppDetails_NoDAG %></td>
            </tr>
<% } else { %>
            <tr class="DagRow">
                <td class="Property">
                   <%= APM_ExBBContent.AppDetails_DAG_Name %>:
                </td>
                <td class="Property">
                   <%= (InitialPollInProgress) ? String.Empty : App.DAG.Name %>
                </td>
            </tr>
            <tr class="DagSpacer"></tr>
            <tr class="DagRow">
                <td class="Property">
                   <%= APM_ExBBContent.AppDetails_DAG_OtherServer %>:
                </td>
                <td class="Property statusAndText">
    <% if (OtherServers.Count > 0) { %>
                    <apm:SmallApmAppStatusIcon ID="OtherAppStatusIcon" runat="server"/>
                    <a href="<%= GetViewLink(OtherServers[0].NetObjectID) %>"> <%= OtherServers[0].Name %> </a> on
                    <orion:SmallNodeStatus ID="OtherNodeStatusIcon" runat="server"/>
                    <a href="<%= GetViewLink(OtherServers[0].NPMNode.NetObjectID) %>"> <%= OtherServers[0].NodeName %> </a>

                    <asp:Repeater ID="serverRepeater" runat="server" OnInit="serverRepeater_Init">
                        <ItemTemplate>
                            <br/>
                            <span style="padding-top: 5px; display: inline-block;">
                                <apm:SmallApmAppStatusIcon runat="server" StatusValue="<%# ((ExchangeApplication)Container.DataItem).Status %>" CustomApplicationType="<%# ((ExchangeApplication)Container.DataItem).CustomType %>" />
                                <a href="<%# GetViewLink(((ExchangeApplication)Container.DataItem).NetObjectID) %>"> <%# ((ExchangeApplication)Container.DataItem).Name %> </a> on
                                <orion:SmallNodeStatus runat="server" StatusValue="<%# ((ExchangeApplication)Container.DataItem).NPMNode.Status %>" />
                                <a href="<%# GetViewLink(((ExchangeApplication)Container.DataItem).NPMNode.NetObjectID) %>"><%# ((ExchangeApplication)Container.DataItem).NodeName %></a>
                            </span>
                         </ItemTemplate>
                    </asp:Repeater>
    <% } else { %>
                    <label class="DagGroupLabel"><%= (InitialPollInProgress)? String.Empty : APM_ExBBContent.AppDetails_DAG_NoOtherServer %></label>
    <% } %>
                </td>
            </tr>
            <tr class="DagSpacer"></tr>
            <tr class="DagRow joined">
    <% if (WitnessServer == null && (App.DAG == null || App.DAG.WitnessServerIdentity == null)) { %>
                <td class="Property">
                    <%= APM_ExBBContent.AppDetails_DAG_Witness %>:
                </td>
    <% } else { %>
                <td class="Property">
                    <%= APM_ExBBContent.AppDetails_DAG_Witness %>:
                </td>
    <% } %>
                <td class="Property statusAndText">
    <% if (WitnessServer != null) { %>
                    <orion:SmallNodeStatus ID="WitnessNodeStatus" runat="server" />
                    <a href="<%= GetViewLink(WitnessServer.NPMNode.NetObjectID) %>"><%= WitnessServer.NodeName %></a>
                </td>
            </tr>
            <tr></tr>
            <tr class="DagRow">
                <td class="Property">
                   <%= APM_ExBBContent.AppDetails_DAG_Path %>:
                </td>
                <td class="Property statusAndText">
                   <%= WitnessServerDirectory %>
    <% } else { %>
        <% if (App.DAG != null && App.DAG.WitnessServerIdentity != null && App.DAG.WitnessDirectory != null) { %>
            <% if (WitnessServerId != 0 && WitnessServerStatusIcon != null) { %>
                    <img src="/Orion/Images/StatusIcons/Small-<%= WitnessServerStatusIcon%>"/>
                    <a  href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%= WitnessServerId %>"> <%= App.DAG.WitnessServerIdentity %> </a>
            <% } else { %>
                    <label><%= App.DAG.WitnessServerIdentity %></label>
            <% } %>
                </td>
            </tr>
            <tr></tr>
            <tr class="DagRow">
                <td class="Property">
                    <%= APM_ExBBContent.AppDetails_DAG_Path %>:
                </td>
                <td class="Property statusAndText">
                    <%= App.DAG.WitnessDirectory %>
        <% } else { %>
                    <label class="DagGroupLabel"> <%= (InitialPollInProgress) ? String.Empty : APM_ExBBContent.NotAvailableData %></label>
        <% } %>
    <% } %>
                </td>
            </tr>
            <tr class="DagSpacer"></tr>
            <tr class="DagRow">
    <% if (AlternateWitnessServer == null && (App.DAG == null || App.DAG.AlternativeWitnessServerIdentity == null)) { %>
                <td class="Property">
                    <%= APM_ExBBContent.AppDetails_DAG_AlternateWitness %>:
                </td>
    <% } else { %>
                <td class="Property">
                    <%= APM_ExBBContent.AppDetails_DAG_AlternateWitness %>:
                </td>
    <% } %>
                <td class="Property statusAndText">
    <% if (AlternateWitnessServer != null) { %>
                    <orion:SmallNodeStatus ID="AlternateWitnessNodeStatus" runat="server"/>
                    <a href="<%= GetViewLink(AlternateWitnessServer.NPMNode.NetObjectID) %>"><%= AlternateWitnessServer.NodeName %></a>
                </td>
            </tr>
            <tr>
                <td class="Property">
                   <%= APM_ExBBContent.AppDetails_DAG_Path %>:
                </td>
                <td class="Property statusAndText">
                    <%= WitnessServerDirectory %>
    <% } else { %>
        <% if (App.DAG != null && App.DAG.AlternativeWitnessServerIdentity != null && App.DAG.AlternativeWitnessDirectory != null) { %>
            <% if (AlternateWitnessServerId != 0 && AlternateWitnessServerStatusIcon != null) { %>
                            <img src="/Orion/Images/StatusIcons/Small-<%= AlternateWitnessServerStatusIcon%>"/>
                            <a  href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%= AlternateWitnessServerId %>"> <%= App.DAG.AlternativeWitnessServerIdentity %> </a>
            <% } else { %>
                            <label><%= App.DAG.AlternativeWitnessServerIdentity %></label>
            <% } %>
                </td>
            </tr>
            <tr class="DagRow">
                <td class="Property">
                    <%= APM_ExBBContent.AppDetails_DAG_Path %>:
                </td>
                <td class="Property statusAndText">
                    <%= App.DAG.AlternativeWitnessDirectory %>
        <% } else { %>
                    <label class="DagGroupLabel"><%= (InitialPollInProgress) ? String.Empty : APM_ExBBContent.NotAvailableData %></label>
        <% } %>
    <% } %>
                </td>
            </tr>
            <tr class="DagSpacer"></tr>
            <tr class="DagRow">
                <td class="Property"><%= APM_ExBBContent.AppDetails_DAG_WitnessShareInUse %></td>
                <td class="Property statusAndText">
    <% if (WitnessShareStatus != null) { %>
                    <%= WitnessShareStatus %>
    <% } else { %>
                    <label class="DagGroupLabel"><%= (InitialPollInProgress)? String.Empty : APM_ExBBContent.AppDetails_DAG_WitnessNotInUse %></label>
    <% } %>
                </td>
            </tr>
            <tr class="DagSpacer"></tr>
<% } %>
        </table>
            <apm:ComponentErrorStatusControl
                ID="componentErrorStatusControl"
                runat="server"
                OnInit="componentErrorStatusControl_Init">
            </apm:ComponentErrorStatusControl>
    </Content>
</orion:resourceWrapper>
