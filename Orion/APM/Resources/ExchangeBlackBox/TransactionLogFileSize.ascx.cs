﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_TransactionLogFileSize : ApmBaseResource
{
    #region BaseResourceControl properties

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    protected override string DefaultTitle
    {
        get
        {
            return APM_ExBBContent.TransactionLogFileSize_Title;
        }
    }

    protected Database Database { get; set; }

    protected ExchangeApplication ExchangeApplication { get; set; }

    public override string SubTitle
    {
        get
        {
            return APM_ExBBContent.DatabaseResource_SubTitle.FormatInvariant(Database.Name);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForExchangeTransactionLogSize"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IDatabaseProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    #endregion

    private const string SwqlQueryTemplate = @"
SELECT 
    '{1}:' + ToString(dc.ID) as [_databaseLink],
	'/Orion/StatusIcon.ashx?entity={2}&size=small&status=' + ToString(dc.Status) + '&hint=' + ToString(dc.IsActive) as [databaseIconLink],
    tld.FilesCount as [numbersOfFiles],
    tld.Size as [totalFileSize],
    '{3}:' + ToString(v.VolumeId) as [_volumeLink],
    CASE WHEN (v.Size = 0) THEN 0 ELSE Round(100 * tld.Size / v.Size, 1) END as [volumeUsage],
	'{4}:' + ToString(a.ApplicationID) as [_appLink],
	'/Orion/StatusIcon.ashx?entity={5}&size=small&status=' + ToString(a.CurrentStatus.Availability) as [_appIconLink],
	a.Name as [_appName],
	'{6}:' + ToString(n.NodeID) as [_nodeLink],
	'/Orion/images/StatusIcons/Small-' + ToString(n.StatusIcon) as [_nodeIconLink],
	n.Caption as [nodeCaption],
	tld.PhysicalName as [_physicalName],
    dc.ID,
    df.StatusDbUsage,
    dc.ActivationPreference as [_activationPreference]
FROM Orion.APM.Exchange.TransactionLogDir tld
JOIN Orion.APM.Exchange.DatabaseCopy dc
    ON tld.DatabaseCopyID = dc.ID
JOIN Orion.APM.Exchange.DatabaseFile df
    ON df.DatabaseCopyID = dc.ID
JOIN Orion.APM.Exchange.Application a
    ON a.ApplicationID = dc.ApplicationID
JOIN Orion.Nodes n
    ON n.NodeID = a.NodeID
LEFT JOIN Orion.Volumes v
    ON v.VolumeID = tld.VolumeID 
WHERE dc.DatabaseID = {0}";

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeDatabase();

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = InvariantString.Format(
            SwqlQueryTemplate,
            Database.Id,
            InvariantString.Format(
                "/Orion/View.aspx?NetObject={0}", 
                ExchangeConstants.DatabaseCopyPrefix),
            SwisEntities.DatabaseCopy,
            GetViewLink("V"),
            GetViewLink(ExchangeConstants.ApplicationPrefix),
            SwisEntities.Application,
            GetViewLink("N"));
    }

    private void InitializeDatabase()
    {
        var dbProvider = GetInterfaceInstance<IDatabaseProvider>();
        if (dbProvider == null)
        {
            throw new InvalidOperationException("Cannot get a database.");
        }

        Database = dbProvider.ExchangeDatabase;

        ExchangeApplication = dbProvider.ExchangeApplication;
    }
}
