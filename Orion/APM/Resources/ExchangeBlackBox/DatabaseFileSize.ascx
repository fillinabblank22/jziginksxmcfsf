﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseFileSize.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_DatabaseFileSize" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSControls" runat="server" Module="APM" File="Controls.js"/>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssControls" runat="server" Module="APM" File="Controls.css"/>
        <orion:Include ID="CssResources" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <style type="text/css">
            table.exchangeGrid tbody tr.filePathRow td,
            table.exchangeGrid thead tr.filePathRow td {
                border-bottom: 1px dashed #e0e0e0 !important;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>);
                }

                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: <%= Resource.ID %>,
                    cls: 'exchangeGrid',
                    initialPage: 0,
                    allowSort: true,
                    explicitZebra: true,
                    showAllLimit: 50,
                    underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
                    headerTitles: [
                        '<%= APM_ExBBContent.FilePath %>',
                        '<%= APM_ExBBContent.DatabaseFileSize_TotalFileSize %>',
                        '<%= APM_ExBBContent.DatabaseFileSize_DatabaseUsage %>',
                        '<%= APM_ExBBContent.DatabaseFileSize_VolumeUsage %>',
                        '<%= APM_ExBBContent.DatabaseFileSize_ExchangeServer %>'
                    ],
                    defaultOrderBy: '[_activationPreference]',
                    customHeaderGenerator: function (columns) {
                        return $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0],
                                columns[1],
                                columns[2].attr('colspan', '2'),
                                columns[3].attr('colspan', '2'),
                                columns[4]
                            );
                    }
                });

                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            {#
                var filePath, fileIcon, fileSize, dbUsage, dbUsageBar, volUsage, volUsageBar, server,
                    statusDbUsage, combinedStatus, customChartUrl,
                    roundValue = SW.Core.Charts.dataFormatters.roundValue,
                    ExBbRes = SW.APM.EXBB.Resources;

                customChartUrl = '<%= CustomChartUriTemplate %>'
                    .replace(/\{Name\}/g, encodeURIComponent(Columns[6]))
                    .replace(/\{DatabaseFileID\}/g, Columns[16]);

                statusDbUsage = Columns[18];
                combinedStatus = ExBbRes.DB.combineStatus(statusDbUsage, Columns[19]);

                fileIcon = ExBbRes.DB.getLinkedIcon(Columns[10], Columns[9]);
                filePath = Columns[6] || '<span class="italicText"><%= APM_ExBBContent.UnknownValue %></span>';
                fileSize = SW.Core.Charts.dataFormatters.byte(Columns[1], 0);
                dbUsage = ExBbRes.DB.getLink(customChartUrl, ExBbRes.DB.getPercentageLabel(roundValue(Columns[2], 1)));
                dbUsageBar = ExBbRes.DB.getLink(customChartUrl, ExBbRes.DB.getDatabasePercentStatusBar(Columns[3], Columns[4], combinedStatus));
                volUsage = ExBbRes.DB.getLink(Columns[15], ExBbRes.DB.getPercentageLabel(roundValue(Columns[5], 1)));
                volUsageBar = ExBbRes.DB.getLink(Columns[15], ExBbRes.DB.getVolumePercentStatusBar(Columns[5], statusDbUsage));
                server = ExBbRes.DB.getAppOnNodeCaption(Columns[11], Columns[12], Columns[7], Columns[13], Columns[14], Columns[8]);
            #}
            <tbody class="zebraRow">
                <tr class="topRow filePathRow">
                    <td colspan="7">{{ fileIcon }}{{ filePath }}</td>
                </tr>
                <tr class="bottomRow">
                    <td></td>
                    <td>{{ fileSize }}</td>
                    <td>{{ dbUsage }}</td>
                    <td>{{ dbUsageBar }}</td>
                    <td>{{ volUsage }}</td>
                    <td>{{ volUsageBar }}</td>
                    <td>{{ server }}</td>
                </tr>
            </tbody>
        </script>
        <apm:CustomQueryTable runat="server" ID="CustomTable" />
    </Content>
</orion:resourceWrapper>
