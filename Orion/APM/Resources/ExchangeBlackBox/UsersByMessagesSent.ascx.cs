﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_UsersByMessagesSent : ApmBaseResource
{
	private const string searchControlPath = @"~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx";

	private const string searchSWQLTrailer = @" AND mb.DisplayName LIKE '%${SEARCH_STRING}%'";

	private const string swql = @"
SELECT
    '{1}:' + ToString(mb.ID) as [_mailboxLink],
	'/Orion/StatusIcon.ashx?entity={2}&size=small&status=' + ToString(mb.Status) as [_mailboxIconLink],
    mb.DisplayName,
    mb.MessagesSent,
    mb.MessagesSentLastSevenDays,
    mb.MessagesSentLastThirtyDays,
    mb.LastLogonTime,
    mb.DatabaseName,
    mb.ItemCount
    {3}
FROM Orion.APM.Exchange.Mailbox mb
{0}";

	private const string appDetailsQueryPart = @",
    '{0}:' + ToString(dc.ID) as [_databaseLink],
	'/Orion/StatusIcon.ashx?entity={1}&size=small&status=' + ToString(mb.Database.Status) as [_databaseIconLink],
    mb.Database.Identity";

    private const string appDetailsQueryEnd = @"JOIN Orion.APM.Exchange.DatabaseCopy dc ON dc.IsActive = 1 AND dc.DatabaseID = mb.DatabaseID
WHERE dc.ApplicationID = {0}";

	private const string dbDetailsQueryEnd = @"WHERE mb.DatabaseID = {0}";

	private ExchangeApplication application;

	private Database database;

	protected bool isDbDetails;

	protected override string DefaultTitle
	{
		get
		{
			return Resources.APM_ExBBContent.UsersByMessagesSent;
		}
	}

	public override string HelpLinkFragment
	{
		get
		{
			return this.ResourceHelpLinkFragment("SAMAGAppInsightForExchangeUsersByMessagesSent");
		}
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get
		{
			return new[] { typeof(IExchangeApplicationProvider) };
		}
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get
		{
			return ResourceLoadingMode.Ajax;
		}
	}

	protected ResourceSearchControl SearchControl { get; private set; }

	protected void Page_Load(object sender, EventArgs e)
	{
		Initialize();
        isDbDetails = database != null;

	    SearchControl = (ResourceSearchControl)LoadControl(searchControlPath);
		Wrapper.HeaderButtons.Controls.Add(SearchControl);
		CustomTable.UniqueClientID = Resource.ID;

	    CustomTable.SWQL = GetQuery(isDbDetails);

		CustomTable.SearchSWQL = InvariantString.Format("{0}{1}", CustomTable.SWQL, searchSWQLTrailer);

	    var isMailboxAccountsDisabled = ComponentDal.GetComponentStateByTemplateUid(application.Id, ComponentIdentifiers.IDMailboxAccountDetails);
        if (isMailboxAccountsDisabled)
        {
            Wrapper.Visible = false;
        }
	}
    
    private string GetQuery(bool forDb)
    {
        string query;
        if (forDb)
        {
            query = swql.FormatInvariant(dbDetailsQueryEnd.FormatInvariant(database.Id),
                GetViewLink(ExchangeConstants.MailboxPrefix),
                SwisEntities.Mailbox, string.Empty);
        }
        else
        {
            query = swql.FormatInvariant(appDetailsQueryEnd.FormatInvariant(application.Id),
                GetViewLink(ExchangeConstants.MailboxPrefix),
                SwisEntities.Mailbox,
                appDetailsQueryPart.FormatInvariant(GetViewLink(ExchangeConstants.DatabaseCopyPrefix), SwisEntities.Database));
        }
        return query;
    }

    protected void Initialize()
    {
        var appProvider = this.GetInterfaceInstance<IExchangeApplicationProvider>();
        if (appProvider == null)
        {
            throw new InvalidOperationException("Cannot get provider.");
        }

        var dbProvider = appProvider as IDatabaseProvider;
        if (dbProvider != null)
        {
            this.database = dbProvider.ExchangeDatabase;
            if (this.database == null)
            {
                throw new InvalidOperationException("Cannot get database.");
            }
        }

        this.application = appProvider.ExchangeApplication;
        if (this.application == null)
        {
            throw new InvalidOperationException("Cannot get database.");
        }
    }
}
