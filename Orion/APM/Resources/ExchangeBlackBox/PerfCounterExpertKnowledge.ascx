﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerfCounterExpertKnowledge.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_PerfCounterExpertKnowledge" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <orion:Include ID="CssResources" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <orion:Include ID="CommonCssResources" runat="server" Module="APM" File="/BlackBox/Styles/Resources.css"/>
        <asp:PlaceHolder ID="ctrStatisticPlaceHolder" runat="server">
            <table cellspacing="0" class="counterDetails biggerPadding expert-knowledge-table">
			    <tr>
				    <td><%= ExpertKnowledgeText %></td>
			    </tr>
		    </table>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="ctrInvalidCmpPlaceHolder" Visible="False" runat="server">
            <div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span>Selected component is not compatible with this resource.</div>
        </asp:PlaceHolder>
	</Content>
</orion:resourceWrapper>