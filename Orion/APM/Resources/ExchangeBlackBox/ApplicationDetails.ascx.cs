﻿using Resources;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Models;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_ApplicationDetails :  ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IExchangeApplicationProvider) };

    public override string HelpLinkFragment => "SAMAGAppInsightForExchangeAppDetailsResource";

    public List<ExchangeApplication> OtherServers
    {
        get
        {
            if (App.DAG == null)
            {
                return new List<ExchangeApplication>();
            }
            return App.DAG.ExchangeServersInfo.Where(s => s.NetObjectID != this.App.NetObjectID).ToList();
        }
    }

    protected override string DefaultTitle => APM_ExBBContent.AppDetails_Title;

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public ExchangeApplication App => GetInterfaceInstance<IExchangeApplicationProvider>().ExchangeApplication;

    public ExchangeApplication WitnessServer { get; set; }
    public string WitnessServerDirectory { get; set; }
    protected string WitnessServerStatusIcon;
    protected int WitnessServerId;

    public ExchangeApplication AlternateWitnessServer { get; set; }
    public string AlternateWitnessServerDirectory { get; set; }
    protected string AlternateWitnessServerStatusIcon;
    protected int AlternateWitnessServerId;

    protected bool ShowApplicationError
    {
        get { return ApplicationError.Status != Status.Available; }
    }
    
    protected bool InitialPollInProgress
    {
        get { return ApplicationError.Status == Status.Undefined && (int) ApplicationError.ErrorCode == int.MinValue; }
    }

    protected string WitnessShareStatus
    {
        get
        {
            if (App.DAG != null)
            {
                switch (App.DAG.WitnessShareInUse)
                {
                    case "Primary":
                        return APM_ExBBContent.AppDetails_DAG_Primary;
                    case "Alternate":
                        return APM_ExBBContent.AppDetails_DAG_Alternate;
                    case "InvalidConfiguration":
                        return APM_ExBBContent.AppDetails_DAG_InvalidConfig;
                }
            }
            return null;
        }
    }

    private ApmMonitorError applicationError;

    protected ApmMonitorError ApplicationError
    {
        get
        {
            if (applicationError == null)
            {
                var errorMessage = LoadApplicationError();
                if (errorMessage == null)
                {
                    applicationError = new ApmMonitorError
                    {
                        Status = Status.Undefined //will be displayed as 'Initial poll in progress'
                    };
                }
                else if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    applicationError = new ApmMonitorError(ApmErrorCode.Ok, 0, StatusCodeType.None, errorMessage)
                    {
                        Status = Status.Undefined
                    };
                }
                else
                {
                    applicationError = new ApmMonitorError(ApmErrorCode.Ok, 0, StatusCodeType.None, string.Empty)
                    {
                        Status = Status.Available
                    };
                }
            }
            return applicationError;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        AppStatusIcon.StatusValue = App.Status;
        AppStatusIcon.CustomApplicationType = App.CustomType;
        ServerStatusIcon.StatusValue = App.NPMNode.Status;

        if (App.DAG != null)
        {
            if (OtherServers.Count > 0)
            {
                OtherAppStatusIcon.StatusValue = OtherServers[0].Status;
                OtherAppStatusIcon.CustomApplicationType = OtherServers[0].CustomType;
                OtherNodeStatusIcon.StatusValue = OtherServers[0].NPMNode.Status;
            }

            if (App.DAG.WitnessServerInfo != null)
            {
                this.WitnessServer = App.DAG.WitnessServerInfo;
                this.WitnessServerDirectory = App.DAG.WitnessDirectory;
                WitnessNodeStatus.StatusValue = WitnessServer.NPMNode.Status;
            }
            else if (App.DAG.WitnessServerIdentity != null)
            {
                PopulateWitnessServerData(App.DAG.WitnessServerIdentity, out WitnessServerId, out WitnessServerStatusIcon);
            }
            if (App.DAG.AlternativeWitnessServerInfo != null)
            {
                this.AlternateWitnessServer = App.DAG.AlternativeWitnessServerInfo;
                this.AlternateWitnessServerDirectory = App.DAG.AlternativeWitnessDirectory;
                AlternateWitnessNodeStatus.StatusValue = AlternateWitnessServer.NPMNode.Status;
            }
            else if (App.DAG.AlternativeWitnessServerIdentity != null)
            {
                PopulateWitnessServerData(App.DAG.AlternativeWitnessServerIdentity, out AlternateWitnessServerId, out AlternateWitnessServerStatusIcon);
            }
        }

        alertSuppressionStatus.EntityUri = App.SwisUri;
        alertSuppressionStatus.TargetElementId = alertSuppressionStatusDescription.ClientID;
    }

    private string LoadApplicationError()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string Query = @"SELECT ErrorMessage FROM Orion.APM.CurrentApplicationStatus WHERE ApplicationID = @ApplicationID";
            var status = swis.Query(Query, new Dictionary<string, object> { { "ApplicationID", App.Id } });
            return status.Rows.Count > 0 ? status.Rows[0].Field<string>(0) : null;
        }
    }

    private void PopulateWitnessServerData(string witnessServerName, out int witnessServerId, out string witnessServerStatusIcon)
    {
        var dotIndex = witnessServerName.IndexOf('.');
        var host = (dotIndex < 0) ? witnessServerName : witnessServerName.Substring(0, dotIndex);
        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string query = @"SELECT NodeID, StatusIcon FROM Orion.Nodes WHERE DNS = @WitnessServerName or DNS = @Host";
            var res = swis.Query(query, new Dictionary<string, object> { { "WitnessServerName", witnessServerName }, { "Host", host } });
            witnessServerId = (res.Rows.Count > 0) ? res.Rows[0].Field<int>(0) : 0;
            witnessServerStatusIcon = (res.Rows.Count > 0) ? res.Rows[0].Field<string>(1) : null;
        }
    }

    protected void componentErrorStatusControl_Init(object sender, EventArgs e)
    {
        bool includeUndefinedComponents = ApplicationError.Status == Status.Available;
        var componentDal = new ComponentDal();
        List<ComponentErrorStatus> components = componentDal.GetComponentsWithErrorByApplicationID(App.Id, includeUndefinedComponents).ToList();
        componentErrorStatusControl.Initialize(components);
    }

    protected void serverRepeater_Init(object sender, EventArgs e)
    {
        serverRepeater.DataSource = OtherServers.Skip(1);
        serverRepeater.DataBind();
    }
}
