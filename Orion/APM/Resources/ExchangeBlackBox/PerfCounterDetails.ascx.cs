﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web.Charting;
using System.Globalization;
using Resources;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Common.Models;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_PerfCounterDetails : SolarWinds.APM.BlackBox.Exchg.Web.UI.ExchangeStatisticResourceBase
{
	private bool _dataInitialized;
	private DataRow _data;

    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    protected override string DefaultTitle
	{
		get { return APM_ExBBContent.Web_ExchangePerformanceCounterDetails_Resource_Title; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.RenderControl; }
	}

	private DataRow Data
	{
		get
		{
			if (!_dataInitialized)
			{
				_dataInitialized = true;

				var queryFormat = @"
SELECT 
	c.ShortName AS ComponentName,
	c.ComponentDefinition.DisplayName AS ComponentTypeDisplayName, 
	c.StatusDescription AS ComponentStatusDescription,
	cc.DisplayName AS CategoryDisplayName,
	pe.AvgStatisticData AS CurrentValue,
	cts.Value AS CounterUnits,
	tbc.Warning AS ThresholdWarningValue,
	tbc.Critical AS ThresholdCriticalValue,
	c.ApplicationItemID,
	edc.DatabaseName,
	ees.AvgStatisticData AS EntityStatisticsCurrentValue
FROM Orion.APM.Component AS c
LEFT JOIN Orion.APM.ComponentTemplate AS ct ON c.TemplateID = ct.ID
LEFT JOIN Orion.APM.ComponentCategory AS cc ON ct.ComponentCategoryID = cc.CategoryID
LEFT JOIN Orion.APM.PortEvidence pe ON c.CurrentStatus.ComponentStatusID = pe.ComponentStatusID
LEFT JOIN Orion.APM.ComponentTemplateSetting cts ON c.TemplateID = cts.ComponentTemplateID AND cts.Key = 'CounterUnit'
LEFT JOIN Orion.APM.ThresholdsByComponent tbc ON c.ComponentID = tbc.ComponentID
LEFT JOIN Orion.APM.Exchange.DatabaseCopy edc ON c.ApplicationItemID = edc.ID
LEFT JOIN Orion.APM.CurrentApplicationStatus cas ON c.ApplicationID = cas.ApplicationID
LEFT JOIN Orion.APM.Exchange.EntityStatistics ees ON edc.DatabaseID = ees.ParentEntityID AND ees.ComponentTemplateID = c.TemplateID AND ees.TimeStamp = cas.LastSuccessfulPoll
WHERE ComponentID = {0}";

				var query = InvariantString.Format(queryFormat, ExchangeStatistic.Id);
				using (var swis = InformationServiceProxy.CreateV3())
				{
					var data = swis.Query(query);
					if (data.Rows.Count > 0)
					{
						_data = data.Rows[0];
					}
				}
			}
			return _data;
		}
	}

	protected string CurrentValue
	{
		get
		{
			var value = Data.GetValue<double?>("CurrentValue");
            
            if (!value.HasValue)
            {
                value = Data.GetValue<double?>("EntityStatisticsCurrentValue");
            }

			return value.HasValue ? value.Value.ToString(CultureInfo.InvariantCulture) : "NA";
		}
	}

	protected string CurrentValueUnit
	{
		get { return MultiChartDataHelper.ChartFormatterToFormatterJS(Data.GetValue<string>("CounterUnits")); }
	}

	protected string ComponentStatusDescription
	{
		get { return Data.GetValue<string>("ComponentStatusDescription"); }
	}

	protected string ComponentName
	{
		get { return Data.GetValue<string>("ComponentName"); }
	}

	protected string CategoryDisplayName
	{
		get { return Data.GetValue<string>("CategoryDisplayName"); }
	}

	protected string ComponentTypeDisplayName
	{
		get { return Data.GetValue<string>("ComponentTypeDisplayName"); }
	}

	protected string[] ValidThresholdValues
	{
		get
		{
			var result = new List<string>(2);
			foreach (var value in new[] { Data.GetValue<double?>("ThresholdWarningValue"), Data.GetValue<double?>("ThresholdCriticalValue") }) 
			{
				if(Threshold.IsValidValue(value))
				{
					result.Add(value.Value.ToString(CultureInfo.InvariantCulture));
				}
			}
			return result.ToArray();
		}
	}

    protected string PerformanceCounterPath
    {
        get
        {
            var category = ExchangeStatistic.ComponentSettings["Category"].Value.Value;
            var counter = ExchangeStatistic.ComponentSettings["Counter"].Value.Value;
            var instance = ExchangeStatistic.ComponentSettings["Instance"].Value.Value;

            if (!String.IsNullOrEmpty(instance))
            {
                if (instance.IndexOf(ExchangeConstants.DbNameMacro, StringComparison.InvariantCultureIgnoreCase) > -1)
                {
                    instance = instance.Replace(ExchangeConstants.DbNameMacro, Data.GetValue<string>("DatabaseName"));
                }
                var inst =
                    instance.Split(new[] { ExchangeConstants.InstanceSeparator }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(
                                s => InvariantString.Format(
                                    @"\{0}({1})\{2}",
                                    category,
                                    s,
                                    counter))
                            .ToArray();

                return string.Join(Resources.APM_ExBBContent.ORDelimiter, inst);
            }

            return String.Format(@"\{0}\{1}", category, counter);
        }
    }

    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (ExchangeStatistic == null)
		{
			ctrStatisticPlaceHolder.Visible = false;
			ctrInvalidCmpPlaceHolder.Visible = true;
			return;
		}

		ctrCmpImgStatus.ImageUrl = ComponentImageUrl;

		ctrCmpName.Text = ComponentName;
		ctrCmpStatus.Text = ComponentStatusDescription;
		ctrCmpCounterGroup.Text = CategoryDisplayName;
		ctrCmpCounterType.Text = ComponentTypeDisplayName;
        ctrCmpExchangePerformanceCounter.Text = PerformanceCounterPath;
	}

	protected bool ShowCounterError()
	{
		return ExchangeStatistic != null && ExchangeStatistic.CurrentError.ErrorCode > 0;
	}

	protected ApmMonitorError GetCounterError()
	{
		return ExchangeStatistic != null ? ExchangeStatistic.CurrentError : null;
	}
}
