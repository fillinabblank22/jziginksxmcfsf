﻿using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Common.Models;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_DatabaseDetails : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    protected CopyStatus DbCopyStatus
    {
        get { return this.Db.DatabaseCopy.CopyStatus; }
    }

    public ExchangeApplication App
    {
        get { return this.Db.MountedServer; }
    }

    public string MountedServerName
    {
        get
        {
            var name = this.Db.MountedServer != null ? this.Db.MountedServer.Name : this.Db.DatabaseModel.ServerName;

            if (String.IsNullOrWhiteSpace(name) && this.Db.MasterType == MasterType.Server && this.Db.MasterServer != null)
            {
                name = this.Db.MasterServer.Name;
            }

            return String.IsNullOrWhiteSpace(name) ? APMWebContent.UnknownValue : name;
        }
    }

    public string PreferredServerName
    {
        get
        {
            var name = this.Db.PreferredServer != null ? this.Db.PreferredServer.Name : this.Db.DatabaseModel.PreferredServer;
            return String.IsNullOrWhiteSpace(name) ? APMWebContent.UnknownValue : name;
        }
    }

    protected bool IsOnDifferentServer
    {
        get
        {
            switch (this.Db.MasterType)
            {
                case MasterType.DatabaseAvailabilityGroup:
                    if (this.Db.MountedServerId == this.Db.PreferredServerId)
                    {
                        if (this.Db.MountedServerId != null)
                        {
                            return false;
                        }
                        return this.Db.DatabaseModel.ServerName != this.Db.DatabaseModel.PreferredServer;
                    }
                    return true;
                case MasterType.Server:
                    return false;
                default:
                    return false;
            }
        }
    }

    protected int? MailboxNumber { get; set; }
    protected long? MailboxSize { get; set; }

    protected override string DefaultTitle
    {
        get { return APM_ExBBContent.DatabaseDetails_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment("SAMAGAppInsightForExchangeActiveDBDetailsView"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (IDatabaseProvider)}; }
    }

    private Database db;
    private bool databaseInitialized;

    public Database Db
    {
        get
        {
            if (!databaseInitialized)
            {
                var provider = GetInterfaceInstance<IDatabaseProvider>();
                if (provider != null)
                {
                    db = provider.ExchangeDatabase;
                }
                if (db == null)
                {
                    db = new Database(new SolarWinds.APM.BlackBox.Exchg.Web.Models.DatabaseModel());
                }
                databaseInitialized = true;
            }
            return db;
        }
    }

    protected string GetTimeElapsed(DateTime? dateTime)
    {
        if (!dateTime.HasValue || dateTime.Value == DateTime.MinValue)
        {
            //TODO: never or unknown?
            return CoreWebContent.WEBCODE_VB0_242;
        }

        var builder = new StringBuilder();
        builder.AppendFormat(APM_ExBBContent.DatabaseDetails_ElapsedTime, new ApmTimeSpan(DateTime.UtcNow - dateTime.Value));
        builder.Append("<br/>");
        builder.AppendFormat(dateTime.Value.ToLocalTime().ToString("f", CultureInfo.CurrentUICulture));
        return builder.ToString();
    }

    protected string GetSize(long? bytes)
    {
        return !bytes.HasValue ? APMWebContent.UnknownValue : new ApmBytes(bytes.Value, ApmBytesBase.Unit.GB, 2);
    }

    protected string GetSize(ulong? bytes)
    {
        return !bytes.HasValue ? APMWebContent.NoLimitValule : new ApmBytes(bytes.Value, ApmBytesBase.Unit.GB, 2);
    }

    protected string GetCopyStatus()
    {
        switch (DbCopyStatus)
        {
            case CopyStatus.Mounted:
                return APM_ExBBContent.DatabaseDetails_ActiveCopyStatus_Mounted;
            case CopyStatus.Unknown:
            case CopyStatus.Failed:
            case CopyStatus.Seeding:
            case CopyStatus.Suspended:
            case CopyStatus.Healthy:
            case CopyStatus.ServiceDown:
            case CopyStatus.Initializing:
            case CopyStatus.Resynchronizing:
            case CopyStatus.Dismounted:
            case CopyStatus.Mounting:
            case CopyStatus.Dismounting:
            case CopyStatus.DisconnectedAndHealthy:
            case CopyStatus.FailedAndSuspended:
            case CopyStatus.DisconnectedAndResynchronizing:
            case CopyStatus.NonExchangeReplication:
            case CopyStatus.SeedingSource:
            case CopyStatus.Misconfigured:
                return DbCopyStatus.ToString();
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        DbStatusIcon.AlternateText = Db.ApmStatus.ToLocalizedString();
        DbStatusIcon.ImageUrl = WebPluginManager.Instance.GetIconUrl(SwisEntities.Database, ExchangeConstants.ApplicationTemplateCustomType, Db.StatusId, "small");

        if (this.Db.MountedServer != null)
        {
            AppCopyStatusIcon.CustomApplicationType = this.Db.MountedServer.CustomType;
            AppCopyStatusIcon.StatusValue = this.Db.MountedServer.Status;
            ServerCopyStatusIcon.StatusValue = this.Db.MountedServer.NPMNode.Status;
        }

        if (this.Db.PreferredServer != null)
        {
            PreferredServerStatusIcon.StatusValue = this.Db.PreferredServer.NPMNode.Status;
        }

        MailboxNumber = Db.DatabaseModel.TotalMailboxes;
        MailboxSize = Db.DatabaseModel.AvgMailBoxSize;
    }

    protected string GetMaster()
    {
        var typeString = (string)EnumHelper.GetLocalizedName(this.Db.MasterType, APM_ExBBContent.ResourceManager);
        string nameString = null;

        if (this.Db.MasterType == MasterType.Server && this.Db.MasterServer != null)
        {
            nameString = Db.MasterServer.ServerIdentity;
        }
        else if (this.Db.MasterType == MasterType.DatabaseAvailabilityGroup && this.Db.MasterDag != null)
        {
            nameString = Db.MasterDag.DagIdentity;
        }

        if (String.IsNullOrWhiteSpace(nameString))
        {
            nameString = APMWebContent.UnknownValue;
        }

        return this.Db.MasterType == MasterType.Unknown
                   ? nameString
                   : String.Format(CultureInfo.CurrentUICulture, APM_ExBBContent.DatabaseDetails_Master_Format, nameString, typeString);
    }

    protected void ComponentErrorStatusControl_Init(object sender, EventArgs e)
    {
        componentErrorStatusControl.Initialize(
            ComponentDal.GetComponentsWithErrorByApplicationItemID(Db.MountedServer.Id, Db.DatabaseCopy.Id).ToList());
    }
}
