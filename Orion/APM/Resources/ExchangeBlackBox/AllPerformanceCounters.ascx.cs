﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_APM_Resources_ExchangeBlackBox_AllPerformanceCounters : SolarWinds.APM.BlackBox.Exchg.Web.UI.ExchangeStatisticResourceBase
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    protected override string DefaultTitle => APM_ExBBContent.Web_PerformanceCounters_Resource_Title;

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    protected string CustomApplicationType => ExchangeStatistic.Application.CustomType;

    protected int ApplicationId => ExchangeStatistic.ExchangeApplication.Id;

    protected int ApplicationItemId => ExchangeStatistic.BaseComponent.ApplicationItemID ?? -1;

    protected bool IsApplicationItemSpecific => ExchangeStatistic.IsApplicationItemSpecific;

    protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);
        bool compatible = (this.ExchangeStatistic != null);
        this.incompatibleComponentPlaceHolder.Visible = !compatible;
        this.contentPlaceHolder.Visible = compatible;
    }
}