﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using APMModels = SolarWinds.APM.Common.Models;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_ExchangeBlackBox_MailboxSyncedDevices : ApmBaseResource
{
	public ExchangeApplication App
	{
		get { return this.Mailbox.MailboxDatabase.MountedServer; }
	}

	protected override string DefaultTitle
	{
		get { return APM_ExBBContent.SyncedDeviceTitle; }
	}

	public override string HelpLinkFragment
	{
	    get
	    {
            return this.ResourceHelpLinkFragment("SAMAGAppInsightForExchangeAppDetailsMailboxSynced");
	    }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(IMailboxProvider) }; }
	}

	private Mailbox mailbox;
	private bool mailboxInitialized;

	public Mailbox Mailbox
	{
		get
		{
			if (!mailboxInitialized)
			{
				var provider = GetInterfaceInstance<IMailboxProvider>();
				if (provider != null)
				{
					mailbox = provider.ExchangeMailbox;
				}
				if (mailbox == null)
				{
					mailbox = new Mailbox(new SolarWinds.APM.BlackBox.Exchg.Web.Models.MailboxModel());
				}
				mailboxInitialized = true;
			}
			return mailbox;
		}
	}

    private const string SwqlQueryTemplate = @"SELECT DeviceType, DeviceUserAgent, LastSuccessfulSync FROM Orion.APM.Exchange.SyncedDevices
                                               WHERE MailboxID = {0}";

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = InvariantString.Format(SwqlQueryTemplate, Mailbox.Id);
    }
}