﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using SolarWinds.APM.BlackBox.Exchg.Common.ResourceMetadata;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.BlackBox.Exchg.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.UI.Formatters;
using SolarWinds.APM.Web.UI.Resource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_PerfCounterMinMaxAvgChart : APMStandardChartBaseResource, IResourceIsInternal
{
	private ExchangeNetObjectManager netObjectManager;

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	protected string ChartName
	{
		get { return Resource.Properties.ContainsKey("ChartName") ? Resource.Properties["ChartName"] : "MinMaxAvgPerformanceCounter"; }
	}

	protected override string NetObjectPrefix
	{
		get { return "ABEMMAPC"; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new[] { typeof(IExchangeStatisticProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

	protected ExchangeStatistic ExchangeStatistic
	{
		get { return netObjectManager.ExchangeStatistic; }
	}

	protected override ApmMonitor Monitor
	{
		get { return ExchangeStatistic; }
	}

	protected override string StatisticColumnName
	{
		get
		{
			var properties = GetResourcePropertiesAsDictionary();
			if (EvidenceType == ComponentEvidenceType.DynamicEvidence)
			{
				AssignedToResourceInfo info;
				AssignedToResourceInfo.TryCreate(Request, Resource, out info);
				if (info[AssignedToResourceInfo.ColumnNames] != null)
				{
					return info[AssignedToResourceInfo.ColumnNames].ToString();
				}
				if (properties.ContainsKey("StatisticName"))
				{
					return properties["StatisticName"];
				}
			}
			if (Resource.Properties.ContainsKey("StatisticColumnName")) 
			{
				return Resource.Properties["StatisticColumnName"];
			}
			return base.StatisticColumnName;
		}
	}

	public override void ShowHeaderBar(bool show)
	{
		Wrapper.ShowHeaderBar = show;
	}

	protected override ComponentEvidenceType EvidenceType
	{
		get { return Monitor == null ? ComponentEvidenceType.None : Monitor.BaseComponent.EvidenceType; }
	}

	public override string DisplayTitle
	{
		get { return Monitor == null ? string.Empty : Monitor.Name; }
	}

	protected override NameValueCollection GetCustomChartParameters()
	{
		return new NameValueCollection();
	}

	private string AvgDataSeriesName
	{
		get { return string.Format("Average {0}", Monitor.Name); }
	}

	private string MinMaxDataSeriesName
	{
		get { return string.Format("Min/Max {0}", Monitor.Name); }
	}

	private string AxisTitle
	{
		get { return Monitor.Name; }
	}

	private string Units
	{
		get
		{
			ApmSettingValue settingValue;
			if (Monitor.ComponentSettings.TryGetValue("CounterUnit", out settingValue))
			{
				return MultiChartDataHelper.ChartFormatterToFormatterJS(settingValue.Value.Value);
			}
			return MultiChartDataHelper.ChartFormatterToFormatterJS(ChartFormatter.Statistic.ToString());
		}
	}

	public bool IsInternal
	{
		get { return true; }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		netObjectManager = new ExchangeNetObjectManager(this);
		if (ExchangeStatistic == null)
		{
			incompatibleComponentPlaceHolder.Visible = true;
			contentPlaceHolder.Visible = false;
		}
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ChartLegend.js", OrionInclude.Section.Middle);
		OrionInclude.ModuleFile("APM", "Charts/Charts.APM.Chart.Formatters.js", OrionInclude.Section.Middle);

		HandleInit(WrapperContents);
	}

	protected override Dictionary<string, object> GenerateDisplayDetails()
	{
		if (ExchangeStatistic == null)
		{
			return new Dictionary<string, object>();
		}
		Resource.Properties["OptionsSetting_1"] = string.Format("options.seriesTemplates.Avg_Statistic_Data.name = '{0}'", AvgDataSeriesName);
		Resource.Properties["OptionsSetting_2"] = string.Format("options.seriesTemplates.MinMax_Statistic_Data.name = '{0}'", MinMaxDataSeriesName);
		Resource.Properties["OptionsSetting_3"] = string.Format("options.yAxis[0].title.text = '{0}'", AxisTitle);
		Resource.Properties["OptionsSetting_4"] = string.Format("options.yAxis[0].unit = '{0}'", Units);

        Resource.Properties["applicationcustomtype"] = GetInterfaceInstance<IApplicationProviderBase>().ApmApplication.CustomType;
	    Resource.Properties["componentuniqueid"] = Monitor.BaseComponent.TemplateUniqueId.ToString();
        
		var details = base.GenerateDisplayDetails();
		details["title"] = String.Empty;

		return details;
	}
}