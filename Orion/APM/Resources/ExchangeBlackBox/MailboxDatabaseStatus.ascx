﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MailboxDatabaseStatus.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_MailboxDatabaseStatus" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>
<%@ Register TagPrefix="apm" TagName="CustomQueryTable" Src="~/Orion/APM/Controls/ApmCustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:Include ID="JSFormatters" runat="server" Module="APM" File="/Charts/Charts.APM.Chart.Formatters.js"/>
        <orion:Include ID="JSResources" runat="server" Module="APM" File="/ExchangeBlackBox/Js/Resources.js"/>
        <orion:Include ID="CssResources" runat="server" Module="APM" File="/ExchangeBlackBox/Styles/Resources.css"/>
        <style type="text/css">
        </style>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.APM.Resources.CustomQuery.refresh(<%= Resource.ID %>);
                };
                SW.APM.Resources.CustomQuery.initialize({
                    uniqueId: <%= Resource.ID %>,
                    cls: 'exchangeGrid',
                    initialPage: 0,
                    allowSort: true,
                    rowsPerPage:10,
                    showAllLimit: 50,
                    underscoreTemplateId: '#contentTemplate-<%= Resource.ID %>',
                    explicitZebra: true,
                    headerTitles: [
                        "<%= Resources.APM_ExBBContent.MailboxDatabaseStatus_DatabaseName %>",
                        "<%= Resources.APM_ExBBContent.MailboxDatabaseStatus_CopyStatus %>",
                        "<%= Resources.APM_ExBBContent.MailboxDatabaseStatus_CopyQueueLength %>",
                        "<%= Resources.APM_ExBBContent.MailboxDatabaseStatus_ReplayQueueLength %>",
                        "<%= Resources.APM_ExBBContent.MailboxDatabaseStatus_LastInspectedLogTime %>",
                        "<%= Resources.APM_ExBBContent.MailboxDatabaseStatus_ContentIndexState %>",
                        "<%= Resources.APM_ExBBContent.MailboxDatabaseStatus_ActivationPreference%>"
                    ],
                    defaultOrderBy: '',
                    customHeaderGenerator: function(columns) {
                        return $('<tr/>')
                            .addClass('HeaderRow')
                            .append(
                                columns[0].attr('colspan', '2'),
                                columns[1],
                                columns[2],
                                columns[3],
                                columns[4],
                                columns[5],
                                columns[6]
                            );
                    }
                });
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <script id="contentTemplate-<%= Resource.ID %>" type="text/template">
            <tbody class="zebraRow">
                {#
                    var statusCopyQueueLength = Columns[16];
                    var statusReplayQueueLength = Columns[17];
                    var copyQueueLengthClass = statusCopyQueueLength == 14 ? 'redAndBold' : statusCopyQueueLength  == 3 ? 'red' : '';
                    var replayQueueLengthClass = statusReplayQueueLength == 14 ? 'redAndBold' : statusReplayQueueLength  == 3 ? 'red' : '';
                    var regionalSettings = <%= DatePickerRegionalSettings.GetDatePickerRegionalSettings() %>;
                    var date = Columns[4] == null ? '' : SW.APM.EXBB.Resources.DB.formatDateWithRegoinalSettings(SW.APM.EXBB.Resources.DB.utcToLocalTime(Columns[4]),SW.APM.EXBB.Resources.Constants.SHORT_DATE_FORMAT,regionalSettings);
                #}
                <tr class="topRow">
                    <td>{{SW.APM.EXBB.Resources.DB.getLinkedIcon(Columns[7], Columns[8])}}</td>
                    <td>{{SW.APM.EXBB.Resources.DB.getLink(Columns[7],[Columns[0]])}}</td>
                    <td>{{ SW.APM.EXBB.Resources.getCopyStatus(Columns[1]).name }}</td>
                    <td style="text-align: left" class="{{ copyQueueLengthClass }}">{{ [Columns[2]] }}</td>
                    <td style="text-align: left" class="{{ replayQueueLengthClass }}">{{ [Columns[3]] }}</td>
                    <td>{{ date }}</td>
                    <td class="{{SW.APM.EXBB.Resources.DB.getIndexStateCss(Columns[5], Columns[9])}}">{{SW.APM.EXBB.Resources.getContentIndexStatusType(Columns[5]).name }}</td>
                    <td style="text-align: right"> <label style = "margin-right: 25px;">{{ [Columns[6]] }}</label></td>
                </tr>
                {# if (SW.APM.EXBB.Resources.DB.isDbOnDifferentServer(Columns[10], Columns[11], Columns[12], Columns[13], Columns[14])) { #}
                <tr class="bottomRow">
                    <td colspan="8">
                        <div class="warning-container">
                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><label><%= Resources.APM_ExBBContent.DatabaseDetails_NotOnPreferredServer %></label>
                        </div>
                    </td>
                </tr>
                {# } #}
            </tbody>
        </script>
        <apm:CustomQueryTable runat="server" ID="CustomTable"/>
    </Content>
</orion:resourceWrapper>
