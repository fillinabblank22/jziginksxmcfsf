﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MailboxDetails.ascx.cs" Inherits="Orion_APM_Resources_ExchangeBlackBox_MailboxDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Exchg.Common" %>
<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="Include2" File="APM/Styles/Resources.css" runat="server"/>
        <orion:Include ID="Include1" File="APM/APM.css" runat="server" />
        <orion:Include ID="I2" File="js/OrionCore.js" runat="server" />
        <orion:Include ID="I4" File="APM/Js/Resources.js" runat="server" />
        <orion:Include ID="Include3" File="APM/Js/PercentageBarRenderers.js" runat="server" />
        <orion:Include ID="I5" File="APM/Styles/Resources.css" runat="server" />
        <orion:Include ID="I7" File="APM/ExchangeBlackBox/Styles/Resources.css" runat="server" />

        <script type="text/javascript">
            $().ready(function () {
                SW.APM.TrimStatusDetails();
                SW.APM.Renderers.PercentageBarRenderer('#MailboxSizePercentage');
            });
        </script>

        <table cellspacing="0" class="biggerPadding mailboxDetails NeedsZebraStripes">
            <tr>
                <td class="PropertyHeader" style="min-width: 140px;"><%= APM_ExBBContent.MailboxDetails_MailboxName %></td>
                <td class="Property statusAndText">
                    <img ID="MailboxStatusIcon" src="<%= GetIconUrl() %>" class="apm_StatusIcon" alt="status"/>&nbsp;<%= Mailbox.Name %>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_DisplayName %></td>
                <td class="Property"><%= Mailbox.MailboxModel.DisplayName  %></td>
            </tr>
            
            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_DatabaseStatus %></td>
                <td class="Property statusAndText">
                    <img ID="DatabaseIcon" src="<%= GetDatabaseIconUrl() %>" class="apm_StatusIcon" alt="status"/>
                    <a href="<%= InvariantString.Format("{0}:{1}", GetViewLink(ExchangeConstants.DatabaseCopyPrefix),  Mailbox.MailboxDatabase.DatabaseCopy.Id) %>"><%= Mailbox.MailboxDatabase.Name %></a>
                    on
                    <apm:SmallApmAppStatusIcon ID="ApplicationStatusIcon" runat="server"
                        CustomApplicationType="<%# App.CustomType %>" StatusValue="<%# App.Status %>"/>
                    <a href="<%= GetViewLink(App.NetObjectID) %>"><%= App.Name %></a>
                    on
                    <orion:SmallNodeStatus ID="ServerStatusIcon" runat="server"
                        StatusValue="<%# App.NPMNode.Status %>" />
                    <a href="<%= GetViewLink(App.NPMNode.NetObjectID) %>"><%= App.NodeName %></a>
                </td>
            </tr>
            
            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_Type %></td>
                <td class="Property"><%= GetTypeLabel()  %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_OrganizationalUnit %></td>
                <td class="Property"><%= Mailbox.MailboxModel.OrganizationalUnit  %></td>
            </tr>
            
            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_PrimarySmtpAddress %></td>
                <td class="Property">
                    <a href="mailto:<%= this.Mailbox.MailboxModel.PrimarySmtpAddress %>">
                        <img ID="Img1" src="/Orion/APM/ExchangeBlackBox/Images/MailTo.png" class="apm_StatusIcon" alt="<%= APM_ExBBContent.MailboxDetails_MailToAlt %>"/>
                        <%= Mailbox.MailboxModel.PrimarySmtpAddress  %>
                    </a>
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_MailboxSize %></td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td class="indentDetailsPadding"><%= APM_ExBBContent.MailboxDetails_TotalSize%></td>
                <td><span style="float:left;width: 100px;"><%= ToMb(Mailbox.MailboxModel.TotalItemSize) %></span><span id="MailboxSizePercentage" class="percentageBar" value="<%= Mailbox.MailboxModel.PercentageOfUsedQuota ?? 0 %>" status="<%= GetSimpleStatus() %>"><span><%= Mailbox.MailboxModel.PercentageOfUsedQuota ?? 0 %> %</span></span></td>
            </tr>

            <tr>
                <td class="indentDetailsPadding"><%= APM_ExBBContent.MailboxDetails_TotalItems %></td>
                <td><%= ToSize(Mailbox.MailboxModel.ItemCount) %></td>
            </tr>

            <tr>
                <td class="indentDetailsPadding"><%= APM_ExBBContent.MailboxTooltip_AttachmentSize %></td>
                <td><%= CheckForCountLimit(Mailbox.MailboxModel.AttachmentsCount, false).ToString() + ToMb(Mailbox.MailboxModel.AttachmentsSize).ToString() %></td>
            </tr>

            <tr>
                <td class="indentDetailsPadding"><%= APM_ExBBContent.MailboxDetails_AttachmentsCount %></td>
                <td><%= CheckForCountLimit(Mailbox.MailboxModel.AttachmentsCount, true) %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_LastPolled %></td>
                <td class="Property"><%= GetLastSuccessfulMailboxPoll() %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_LastLoggedOn %></td>
                <td class="Property"><%= GetLastLogonInfo()  %></td>
            </tr>
            
            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_LastModified %></td>
                <td class="Property"><%= GetLastModified() %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_Alias %></td>
                <td class="Property"><%= Mailbox.MailboxModel.UserLogonName %></td>
            </tr>

            <tr>
                <td class="PropertyHeader"><%= APM_ExBBContent.MailboxDetails_MailboxLimits %></td>
                <td><%= CustomLimitsUsed() %></td>
            </tr>

            <tr>
                <td class="indentDetailsPadding"><%= APM_ExBBContent.MailboxDetails_IssueWarning%></td>
                <% if (Mailbox.IssueWarningQuota.HasValue) { %>
                <td><%= ToMb(Mailbox.IssueWarningQuota) %></td>
<% } else { %>
                <td><%= APM_ExBBContent.MailboxDetails_NoLimit %></td>
<% } %>
            </tr>

            <tr>
                <td class="indentDetailsPadding"><%= APM_ExBBContent.MailboxDetails_ProhibitSend %></td>
<% if (Mailbox.ProhibitSendQuota.HasValue) { %>
                            <td><%= ToMb(Mailbox.ProhibitSendQuota) %></td>
<% } else { %>
                            <td><%= APM_ExBBContent.MailboxDetails_NoLimit %></td>
<% } %>
            </tr>

            <tr>
                <td class="indentDetailsPadding"><%= APM_ExBBContent.MailboxDetails_ProhibitSendAndReceive %></td>
<% if (Mailbox.ProhibitSendReceiveQuota.HasValue) { %>
                <td><%= ToMb(Mailbox.ProhibitSendReceiveQuota) %></td>
<% } else { %>
                <td><%= APM_ExBBContent.MailboxDetails_NoLimit %></td>
<% } %>
            </tr>
        </table>

    </Content>
</orion:resourceWrapper>