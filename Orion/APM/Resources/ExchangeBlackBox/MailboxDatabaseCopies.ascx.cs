﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Extensions.System;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_MailboxDatabaseCopies : ApmBaseResource
{
    #region BaseResourceControl properties

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    protected override string DefaultTitle
    {
        get
        {
            return APM_ExBBContent.MailboxDatabaseCopies_Title;
        }
    }

    public override string SubTitle
    {
        get
        {
            return APM_ExBBContent.MailboxDatabaseCopies_SubTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForExchangeMailboxDatabaseCopies"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IExchangeApplicationProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    #endregion

    #region Properties and Fields

    private ExchangeApplication application;

    private string SWQL
    {
        get
        {
            return @"
SELECT
    dc.DatabaseName,
    '{1}:' + ToString(dc.ID) as [_databaseLink],
    '/Orion/StatusIcon.ashx?entity={2}&size=small&status=' + ToString(dc.Status) + '&hint=' + ToString(dc.IsActive) as [_databaseIconLink],
    dc.CopyState,
    dc.ContentIndexState,
    dc.IsActive as [_isActive],
    '{3}:' + ToString(dc.ApplicationID) as [_appLink],
    '/Orion/StatusIcon.ashx?entity={4}&size=small&status=' + ToString(a.CurrentStatus.Availability) as [_appIconLink],
     a.Name as [_appName],
    '{5}:' + ToString(n.NodeID) as [_nodeLink],
    '/Orion/images/StatusIcons/Small-' + ToString(n.StatusIcon) as [_nodeIconLink],
    n.Caption as [NodeCaption]
FROM Orion.APM.Exchange.DatabaseCopy dc
JOIN Orion.APM.Exchange.Application a ON a.ApplicationID = dc.ApplicationID
JOIN Orion.Nodes n ON n.NodeID = a.NodeID
WHERE dc.DatabaseID IN (SELECT DatabaseID FROM Orion.APM.Exchange.DatabaseCopy WHERE ApplicationID = {0})"
                .FormatInvariant(application.Id,
                    "/Orion/View.aspx?NetObject={0}".FormatInvariant(ExchangeConstants.DatabaseCopyPrefix),
                    SwisEntities.DatabaseCopy,
                    GetViewLink(ExchangeConstants.ApplicationPrefix),
                    SwisEntities.Application,
                    GetViewLink("N"));
        }
    }



    protected int ErrorLevel { get; set; }

    protected int WarningLevel { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeApplication();

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = SWQL;
    }

    private void InitializeApplication()
    {
        var dbProvider = GetInterfaceInstance<IExchangeApplicationProvider>();
        if (dbProvider == null)
        {
            throw new InvalidOperationException("Cannot get a database.");
        }

        application = dbProvider.ExchangeApplication;
    }
}
