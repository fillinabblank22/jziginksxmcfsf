﻿using System;
using System.Collections.Generic;
using SolarWinds.APM.BlackBox.Exchg.Common;
using SolarWinds.APM.BlackBox.Exchg.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_ExchangeBlackBox_DatabaseDiskIo : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    private IDatabaseProvider dbProvider;

    private const string SwqlQuery = @"
SELECT
    dc.Status,
    ISNULL(v.Caption,'{8}') as volumeCaption,
    Round(v.DiskQueueLength, 2) as QueueLength,
    Round(v.TotalDiskIOPS, 2) as TotalIOPS,
    v.DiskTransfer as VolumeLatency,
    a.Name AS [_AppName],
    n.Caption AS [NodeCaption],
    df.PhysicalName AS [_FilePath],
    '/Orion/StatusIcon.ashx?entity={1}&size=small&status=' + ToString(dc.Status) + '&hint=' + ToString(dc.IsActive) as [_databaseIconLink],
    '{2}:' + ToString(dc.ID) as [_dbLink],
    '{3}:' + ToString(a.ApplicationID) as [_appLink],
    '/Orion/StatusIcon.ashx?entity={4}&size=small&status=' + ToString(a.CurrentStatus.Availability) as [_appIconLink],
    '{5}:' + ToString(n.NodeID) as [_nodeLink],
    '/Orion/images/StatusIcons/Small-' + ToString(n.StatusIcon) as [_nodeIconLink],
    '{6}:' + ToString(v.VolumeId) as [_volumeLink],
    '/Orion/StatusIcon.ashx?entity={7}&size=small&id=' + ToString(v.VolumeID) + '&status=' + ToString(v.Status) as [_volumeIconLink],
    dc.ActivationPreference as [_activationPreference]
FROM Orion.APM.Exchange.DatabaseFile df
JOIN Orion.APM.Exchange.DatabaseCopy dc
    ON df.DatabaseCopyID = dc.ID
JOIN Orion.APM.Exchange.Application a
    ON dc.ApplicationID = a.ApplicationID
JOIN Orion.Nodes n
    ON n.NodeID = a.NodeID
LEFT JOIN Orion.Volumes v
    ON v.VolumeID = df.VolumeID
WHERE dc.DatabaseID = {0}";

    protected void Page_Load(object sender, EventArgs e)
    {
        dbProvider = this.GetInterfaceInstance<IDatabaseProvider>();
        if (dbProvider == null)
        {
            throw new InvalidOperationException("Cannot get database ID for filtering files by database ID");
        }

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = InvariantString.Format(
            SwqlQuery,
            dbProvider.ExchangeDatabase.Id,
            SwisEntities.DatabaseCopy,
            InvariantString.Format(
                "/Orion/View.aspx?NetObject={0}",
                ExchangeConstants.DatabaseCopyPrefix),
            GetViewLink(ExchangeConstants.ApplicationPrefix),
            SwisEntities.Application,
            GetViewLink("N"),
            GetViewLink("V"),
            SolarWinds.Orion.Core.Common.NetObjectHelper.VolumeEntity,
            Resources.APM_ExBBContent.NotAvailableData);
    }

    protected override string DefaultTitle
    {
        get { return Resources.APM_ExBBContent.DatabaseDiskIo_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SAMAGAppInsightForExchangeDatabaseDiskIO"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] {typeof (IDatabaseProvider)}; }
    }

    public override string SubTitle
    {
        get { return InvariantString.Format(Resources.APM_ExBBContent.DatabaseFileSize_SubTitle, dbProvider.ExchangeDatabase.Name); }
    }
}
