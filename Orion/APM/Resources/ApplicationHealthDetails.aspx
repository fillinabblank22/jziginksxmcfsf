﻿<%@ Page Title="<%$ Resources: APMWebContent, APMWEBDATA_AK1_127 %>" Language="C#" MasterPageFile="~/Orion/APM/ApmView.master" AutoEventWireup="true" CodeFile="ApplicationHealthDetails.aspx.cs" Inherits="Orion_APM_Resources_ApplicationHealthDetails" %>
<%@ Register TagPrefix="apm" TagName="StatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="ApmMainContentPlaceHolder" runat="server">
    <orion:Include File="js/AsyncView.js" runat="server"/>
	<h1><%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_128, this.DisplayAppStatus)%></h1>
	<div class="ResourceWrapper" style="width:500px; margin-left:15px;">
		<asp:Repeater ID="appViewGrid" OnInit="OnAppViewGrid_Init" runat="server">
			<HeaderTemplate>
				<table class="DataGrid" style="width:500px;">
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td>
						<apm:StatusIcon ID="appIcon" StatusValue='<%# GetApmStatus(Container.DataItem) %>' runat="server" CustomApplicationType="<%# this.GetCustomApplicationType(Container.DataItem) %>" />
						<asp:Label ID="appName" Text="<%# this.GetRowItem(Container.DataItem) %>" runat="server" />
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</div>
</asp:Content>