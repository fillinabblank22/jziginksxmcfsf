<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MockupResourceContainer.ascx.cs" Inherits="Orion_APM_Resources_MockupResourceContainer" %>

<h1><%#ViewInfos.ViewInfo.ViewShortTitle%></h1>
<table class="ResourceContainer" id="ResourceContainer" runat="server">
    <tr>
        <td runat="server" id="tdColumn1" class="ResourceColumn">
            <asp:Repeater runat="server" ID="rptColumn1">
                <ItemTemplate>
                    <asp:PlaceHolder runat="server" OnDataBinding="ResourcePlaceHolder_DataBind" />
                </ItemTemplate>
            </asp:Repeater>
        </td>
        <td runat="server" id="tdColumn2" class="ResourceColumn">
            <asp:Repeater runat="server" ID="rptColumn2">
                <ItemTemplate>
                    <asp:PlaceHolder runat="server" OnDataBinding="ResourcePlaceHolder_DataBind" />
                </ItemTemplate>
            </asp:Repeater>
        </td>
        <td runat="server" id="tdColumn3" class="ResourceColumn">
            <asp:Repeater runat="server" ID="rptColumn3">
                <ItemTemplate>
                    <asp:PlaceHolder runat="server" OnDataBinding="ResourcePlaceHolder_DataBind" />
                </ItemTemplate>
            </asp:Repeater>
        </td>
    </tr>
</table>