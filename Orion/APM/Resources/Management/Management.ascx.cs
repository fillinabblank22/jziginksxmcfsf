﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Management_Management : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IApplicationProviderBase) }; }
    }

    public override string HelpLinkFragment
    {
        get { return this.ResourceHelpLinkFragment("SAMAGAppInsightForExchangeManagement"); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.Management_Title; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/APM/Controls/EditResourceControls/Management.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.ManagementTasksControl.Resource = this;

        var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
        var netObject = (netObjectProvider != null) ? netObjectProvider.NetObject : null;
        if (netObject != null)
        {
            this.ManagementTasksControl.NetObject = netObject;

            this.ManagementTasksControl.DisabledSections = GetStringsFromProperty("DisabledSections");
            this.ManagementTasksControl.CombineSections = GetBooleanValue("Combined", false);

            this.ManagementTasksControl.AddManagementTasksControls();

            RefreshSectionsInDb();
        }
    }

    private IEnumerable<string> GetStringsFromProperty(string propertyName)
    {
        return GetStringValue(propertyName, string.Empty).ToUpper().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
    }

    private void RefreshSectionsInDb()
    {
        var availableSectionsProp = GetStringsFromProperty("AvailableSections");

        var availableSections = availableSectionsProp.OrderBy(s => s);
        var disabledSections = this.ManagementTasksControl.DisabledSections.OrderBy(s => s);

        var availableSectionsInMemory = this.ManagementTasksControl.AvailableSections.OrderBy(s => s);
        var disabledSectionsInMemory = disabledSections.Where(availableSectionsInMemory.Contains).ToList();

        if (!availableSections.SequenceEqual(availableSectionsInMemory) || !disabledSections.SequenceEqual(disabledSectionsInMemory))
        {
            Resource.Properties["AvailableSections"] = string.Join(",", availableSectionsInMemory);
            Resource.Properties["DisabledSections"] = string.Join(",", disabledSectionsInMemory);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = this.Visible && this.ManagementTasksControl.Visible;
    }
}