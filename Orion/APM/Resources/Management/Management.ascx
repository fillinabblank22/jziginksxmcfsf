﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Management.ascx.cs" Inherits="Orion_APM_Resources_Management_Management" %>
<%@ Register TagPrefix="apm" TagName="ManagementTasksControl" Src="~/Orion/APM/Controls/Management/ManagementTasksControl.ascx" %>
<script type="text/javascript" src="/Orion/APM/Services/Applications.asmx/js"></script>
<orion:resourceWrapper ID="resourceContent" runat="server">
    <Content>
        
        <orion:Include ID="I1" Framework="Ext" FrameworkVersion="3.4" runat="server" />
        
        <apm:ManagementTasksControl ID="ManagementTasksControl" runat="server" />
        
        <script type="text/javascript">
            $(function () {
                var refresh = function() {
                    var managementTaskItems = <%= ManagementTasksControl.ManagementTaskControlsSerialized %>;

                    SW.Core.ManagementTasks.RenderManagementTaskItems(managementTaskItems, '<%= managementTasksPlaceHolder.ClientID %>', <%= Resource.ID %>);
                };
                SW.Core.View.AddOnRefresh(refresh, '<%= managementTasksPlaceHolder.ClientID %>');
                refresh();
            });
        </script>

        <div id="managementTasksPlaceHolder" runat="server">
        </div>
    </Content>
</orion:resourceWrapper>