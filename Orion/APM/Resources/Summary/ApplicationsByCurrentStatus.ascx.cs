using System;
using System.Data;
using System.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Common.Models;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Summary_ApplicationsByCurrentStatus : ApmBaseResource
{
    protected ApmMonitorError lastMonitorError = new ApmMonitorError(ApmErrorCode.Ok, int.MinValue, StatusCodeType.None, string.Empty);

    protected void grid_Init(object sender, EventArgs e)
    {
        string filter = this.GetStringValue("Filter", "");
        
        /* Ensure to create default settings for resource with no settings.
           e.g. when added to app detail page or anywhere else, than config wizzard adds  */ 
        if (!Resource.Properties.ContainsKey("SelectedStatus"))
            Resource.Properties["SelectedStatus"] = "2;0;5;6;";
        
        /* when you add defult value here, it will work, but will not show checked checkboxes
           in resource edit */
        string selectedStatus = this.GetStringValue("SelectedStatus", string.Empty);

        grid.DataSource = SwisDAL.GetApplicationsByCurrentStatus(filter, selectedStatus);
        grid.DataBind();
    }

    protected ApmStatus GetStatus(object item)
    {
        DataRowView row = (DataRowView)item;
        return Convert.IsDBNull(row["Availability"]) ? new ApmStatus() : new ApmStatus((Status)row["Availability"]); 
    }

    protected string GetApplicationNetObjectID(object item, string columnName, string customAppTypeColumnName)
    {
        DataRowView row = (DataRowView)item;

        string customAppType = row[customAppTypeColumnName] is DBNull ? null : (string)row[customAppTypeColumnName];
        return ApmApplicationBase.GetNetObjectId((int)row[columnName], customAppType);
    }

    protected string GetNetObjectID(object item, string columnName, string prefix)
    {
        DataRowView row = (DataRowView)item;
        return string.Format("{0}:{1}", prefix, row[columnName]);
    }

    protected string GetAppNodeNameLinks(object item, string name)
    {
        string appNameLink = String.Format("<a href=\"{0}\">{1}</a>", GetViewLink(GetApplicationNetObjectID(item, "ApplicationId", "CustomApplicationType")), name);
        
        DataRowView row = (DataRowView)item;
        string nodeNameLink = String.Format("<a href=\"{0}\">{1}</a>", GetViewLink(GetNetObjectID(item, "NodeId", "N")), HttpUtility.HtmlEncode(row["Caption"]));

        return String.Format(Resources.APMWebContent.APMWEBDATA_TM0_58, appNameLink, nodeNameLink);
    }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBDATA_AK1_182; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppSummaryApplicationStatus"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/APM/Controls/EditResourceControls/EditApplicationsByCurrentStatus.ascx"; }
    }


	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
