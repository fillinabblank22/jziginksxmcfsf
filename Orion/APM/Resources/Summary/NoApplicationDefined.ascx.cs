using System;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Summary_NoApplicationDefined : ApmBaseResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Wrapper.ManageButtonImage = "~/Orion/APM/images/Button.AppBuilder.gif";
        Wrapper.ManageButtonTarget = "";
		Wrapper.ShowManageButton = SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin;
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Static;}}

    protected override string DefaultTitle
    {
        get { return InvariantString.Format(Resources.APMWebContent.APMWEBCODE_AK1_144, ApmConstants.ModuleShortName); }
    }
    
    public override string EditURL
    {
        get { return ""; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppSummaryNoApps"; }
    }

    protected void OnRemoveResourceButtonClicked(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }

}
