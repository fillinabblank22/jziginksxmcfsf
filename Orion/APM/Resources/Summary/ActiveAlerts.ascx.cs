using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL.Alerting;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_APM_Resources_Summary_ActiveAlerts : ApmBaseResource
{
    private bool showAcknowledgedAlerts;
    private IEntityNameDal entitynameDal;

    public Orion_APM_Resources_Summary_ActiveAlerts(IEntityNameDal entitynameDal)
    {
        this.entitynameDal = entitynameDal;
    }

    public Orion_APM_Resources_Summary_ActiveAlerts() : this(new EntityNameDal())
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
        AlertsTable.ShowAcknowledgedAlerts = showAcknowledgedAlerts = !String.IsNullOrEmpty(showAck) && showAck.Equals("true", StringComparison.OrdinalIgnoreCase);
        AlertsTable.ResourceID = Resource.ID;

        List<string> triggeringObjectEntityNames = entitynameDal.AllAlertsEntities();
        triggeringObjectEntityNames.AddRange(WebPluginManager.Instance.SupportedPlugins.OfType<IAlertingEntitiesUriProvider>().SelectMany(x => x.GetEntityNamesForAlerting()));

        AlertsTable.TriggeringObjectEntityNames.AddRange(triggeringObjectEntityNames.Distinct());

        int rowsPerPage;
        if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
            rowsPerPage = 5;

        AlertsTable.InitialRowsPerPage = rowsPerPage;
    }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_139; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

            if (this.showAcknowledgedAlerts)
                return Resources.APMWebContent.APMWEBCODE_AK1_140;
            else
                return Resources.APMWebContent.APMWEBCODE_AK1_141;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppSummaryAppAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}

