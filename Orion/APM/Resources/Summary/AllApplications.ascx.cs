using System;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

using NPMNode = SolarWinds.Orion.NPM.Web.Node;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Summary_AllApplications : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.Tree.ToString();

    protected override void OnInit(EventArgs e)
	{
		Wrapper.ManageButtonText = Resources.APMWebContent.APMWEBCODE_AK1_126;
		Wrapper.ManageButtonTarget = "~/Orion/APM/Admin/Applications/Default.aspx";
		Wrapper.ShowManageButton = SolarWinds.APM.Web.ApmRoleAccessor.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

		SetGroupingDefaults();

        foreach (var pluginUrl in WebPluginManager.Instance.GetAllApplicationsPluginUrls())
	    {
	        placeHolderPlugins.Controls.Add(Page.LoadControl(pluginUrl));
	    }
	    

		base.OnInit(e);
	}

	private void SetGroupingDefaults()
	{
		string group1 = Resource.Properties["Grouping1"];
		bool needToSetDefaults = (group1 == null);

		if (needToSetDefaults == false)
		{
			// If the grouping display name returns the same text, then
			// the is not a recognized grouping.  Set the defaults.
			needToSetDefaults = (group1 == GetGroupingDisplayName(group1));
		}

		if (needToSetDefaults)
		{
			// No groups defined.  Just set the defaults
			Resource.Properties["Grouping1"] = "ApplicationTemplate.Name";
			Resource.Properties["Grouping2"] = "Nodes.Caption";
			Resource.Properties["Grouping3"] = "";
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.APMWebContent.APMWEBCODE_AK1_2; }
	}

	protected string Grouping1
	{
		get { return GetGrouping("Grouping1"); }
	}

	protected string Grouping2
	{
		get { return GetGrouping("Grouping2"); }
	}

	protected string Grouping3
	{
		get { return GetGrouping("Grouping3"); }
	}

	private string GetGrouping(string name)
	{
		string thisGroup = Resource.Properties[name];
		if (thisGroup == null || thisGroup.Equals("none", StringComparison.InvariantCultureIgnoreCase))
			return String.Empty;

		return thisGroup;
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHAppSummaryAllApps"; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/APM/Controls/EditResourceControls/EditAllApplications.ascx"; }
	}

	public override string SubTitle
	{
		get { return (base.SubTitle == null || base.SubTitle.Trim() == "" ? BuildSubTitle() : base.SubTitle); }
	}

	private string BuildSubTitle()
	{
		string[] groupNames = new string[] { Grouping1, Grouping2, Grouping3 };
		string[] groupDisplayNames = new string[groupNames.Length];

		int index = 0;
		foreach (string groupName in groupNames)
		{
			if (!String.IsNullOrEmpty(groupName))
				groupDisplayNames[index++] = GetGroupingDisplayName(groupName);
		}

		if (index == 0)
			return Resources.APMWebContent.APMWEBCODE_AK1_6;

		return string.Format(Resources.APMWebContent.APMWEBCODE_AK1_7, String.Join(Resources.APMWebContent.APMWEBCODE_AK1_8, groupDisplayNames, 0, index));
	}

	private static string GetGroupingDisplayName(string grouping)
	{
		switch (grouping.ToLowerInvariant())
		{
			case "applicationtemplate.name":
				return Resources.APMWebContent.APMWEBCODE_AK1_9;
			case "nodes.status":
				return Resources.APMWebContent.APMWEBCODE_AK1_10;
			case "nodes.vendor":
				return Resources.APMWebContent.APMWEBCODE_AK1_11;
			case "nodes.machinetype":
				return Resources.APMWebContent.APMWEBCODE_AK1_12;
			case "nodes.contact":
				return Resources.APMWebContent.APMWEBCODE_AK1_13;
			case "nodes.location":
				return Resources.APMWebContent.APMWEBCODE_AK1_14;
			case "nodes.caption":
				return Resources.APMWebContent.APMWEBCODE_AK1_15;
			case "nodes.iosversion":
				return Resources.APMWebContent.APMWEBCODE_AK1_16;
			case "nodes.objectsubtype":
				return Resources.APMWebContent.APMWEBCODE_AK1_17;
		}

		const string CustomPropertyPrefix = "Nodes.CustomProperties.";
		if (grouping.StartsWith(CustomPropertyPrefix, StringComparison.OrdinalIgnoreCase))
		{
			return grouping.Substring(CustomPropertyPrefix.Length);
		}

		const string appCustomPropertyPrefix = "Application.CustomProperties.";
		if (grouping.StartsWith(appCustomPropertyPrefix, StringComparison.OrdinalIgnoreCase))
		{
			return grouping.Substring(appCustomPropertyPrefix.Length);
		}

		return grouping;
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}