<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsByCurrentStatus.ascx.cs"
 Inherits="Orion_APM_Resources_Summary_ApplicationsByCurrentStatus" %>
 
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
		<orion:Include ID="I2" File="APM/Js/Resources.js" runat="server" />
        
        <asp:Repeater ID="grid" runat="server" OnInit="grid_Init">
            <HeaderTemplate>
                <table class="DataGrid NeedsZebraStripes">
                    <thead>
                        <tr>
                            <td class="ReportHeader"><%= Resources.APMWebContent.APMWEBDATA_VB1_63 %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <apm:SmallApmAppStatusIcon runat="server" StatusValue='<%#GetStatus(Container.DataItem) %>' ErrorValue='<%# lastMonitorError %>' />
                        <%# GetAppNodeNameLinks(Container.DataItem, HttpUtility.HtmlEncode(Eval("Name").ToString())) %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
