<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXByVirtualMemory.ascx.cs" Inherits="Orion_APM_Resources_Summary_TopXXByVirtualMemory" %>
<%@ Register Src="InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="gridInit" 
            Statistic="VirtualMemory" ColumnOrder="VirtualUsed;PercentVirtual" 
            ResourceType="Process"
            DBFieldTitles="Process Name;Application Name;Network Node" 
            DBFieldColumnNames="ProcessName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>