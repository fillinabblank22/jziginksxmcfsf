﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNew.ascx.cs" Inherits="Orion_APM_Resources_Summary_WhatsNew" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI" %>

<orion:resourcewrapper runat="server" id="Wrapper" showheaderbar="true">
    <Content>
        <orion:Include ID="Include1" runat="server" File="APM/Styles/Resources.css" />
        <table class="whatsNew">
            <tr>
                <td style="width: 55px;">
                    <img src="<%= LocalizationUrlHelper.GetUrlFromTemplate("/Orion/APM/Images/WhatsNewResource/[lang]/pic_newfeature.gif") %>" alt="new feature" style="border: 0px;" />
                </td>
                <td>
                    <div>
                        <ul>
                            <li><%= MakeBoldText(Resources.APMWebContent.WhatsNew_ApiPollerImprovements) %></li>
                            <li style="list-style-type: none;">
                                <ul>
                                    <li><%= Resources.APMWebContent.WhatsNew_ApiPollerImprovements_EnhanceAlerts %></li>
                                    <li><%= Resources.APMWebContent.WhatsNew_ApiPollerImprovements_ChainAPIrequests %></li>
                                    <li><%= Resources.APMWebContent.WhatsNew_ApiPollerImprovements_MonitorAPIResponseHeaders %></li>
                                    <li><%= Resources.APMWebContent.WhatsNew_ApiPollerImprovements_UseNewManagePage %></li>
                                </ul>
                            </li>
                            <li><%= MakeBoldText(Resources.APMWebContent.WhatsNew_NewAPIPollerTemplatesForMicrosoft365) %></li>
                            <li><%= MakeBoldText(Resources.APMWebContent.WhatsNew_OrionRemoteCollector) %>
                                <a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("OrionSAMOrionRemoteCollector") %>" target="_blank">» <%= Resources.APMWebContent.WhatsNew_OrionRemoteCollector_Details_LearnMore %><%--Learn more--%>
                                </a>
                            </li>
                            <li style="list-style-type: none;">
                                <ul>
                                    <li><%= Resources.APMWebContent.WhatsNew_OrionRemoteCollector_MonitorBranchOffices %></li>
                                    <li><%= Resources.APMWebContent.WhatsNew_OrionRemoteCollector_AutomaticDataStorage %></li>
                                </ul>
                            </li>
                            <li><%= MakeBoldText(Resources.APMWebContent.WhatsNew_EnhancedSAMScalability) %>
                                <a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("OrionSAMAppInsightADScalability") %>" target="_blank">» <%= Resources.APMWebContent.WhatsNew_OrionRemoteCollector_Details_LearnMore %><%--Learn more--%>
                                </a>
                            </li>
                        </ul>
                    </div>
                 </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-right: 0; padding-top: 10px; text-align: right; padding-bottom: 0;">
                    <orion:LocalizableButtonLink runat="server" ID="imgNewFeature" Target="_blank" Text="<%$ Resources:APMWebContent, WhatsNew_LearnMoreButtonText %>" DisplayType="Primary">
                    </orion:LocalizableButtonLink>&nbsp;
                    <%if (!Request.Url.ToString().Contains(DetachURL) && Profile.AllowAdmin)
                      { %>
                        <orion:LocalizableButton runat="server" ID="btnRemoveResource" Text="<%$ Resources:APMWebContent, WhatsNew_RemoveResourceButtonText %>" DisplayType="Secondary" OnClick="OnRemoveResourceButtonClicked"/>
                    <% } %>		
                </td>
            </tr>
        </table>
    </Content>
</orion:resourcewrapper>
