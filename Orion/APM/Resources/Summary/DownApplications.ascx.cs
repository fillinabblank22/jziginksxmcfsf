using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

using NPMNode = SolarWinds.Orion.NPM.Web.Node;

using SolarWinds.APM.Common.Models;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Summary_DownApplications : ApmBaseResource
{
    protected string GetAppNodeNameLinks(object item)
    {
        DataRowView row = (DataRowView)item;

        string appNameLink = String.Format("<a href=\"{0}\">{1}</a>", GetViewLink("AA:" + row["ApplicationID"]), row["Name"]);
        string nodeNameLink = String.Format("<a href=\"{0}\">{1}</a>", GetViewLink("N:" + row["NodeID"]), row["Caption"]);

        return String.Format(Resources.APMWebContent.APMWEBDATA_TM0_58, appNameLink, nodeNameLink);
    }

    protected void grid_Init(object sender, EventArgs e)
    {
        string filter = this.GetStringValue("Filter", "");
        string selectedStatus = ((int) Status.NotAvailable).ToString();
        grid.DataSource = SwisDAL.GetApplicationsByCurrentStatus(filter, selectedStatus);
        grid.DataBind();
    }

    protected object GetColumnValue(object item, string columnKey)
    {
        if (string.IsNullOrEmpty(columnKey))
            return "";

        DataRowView row = (DataRowView)item;
        if (columnKey.ToLowerInvariant() == "updownmonitors")
        {
            return BuildStatusString(row);
        }
        return "";
    }

    protected static string GetDownTime(object item)
    {
        DataRowView row = (DataRowView)item;
        object downSinceValue = row["LastTimeUp"];
        string label = String.Empty;

        if ((downSinceValue == null) || (downSinceValue == Convert.DBNull))
            label = Resources.APMWebContent.APMWEBCODE_AK1_116;
        else
        {
            try
            {
                DateTime lastUpTime = Convert.ToDateTime(downSinceValue);
                ApmTimeSpan apmTimeSpan = new ApmTimeSpan(DateTime.UtcNow - lastUpTime);
                label = apmTimeSpan.ToString();
            }
            catch (Exception)
            {
                // do nothing.  We don't care if we can't convert the date
            }
        }

        return label;
    }

    private static string BuildStatusString(DataRowView row)
    {
        int UpMonitorsCount = (int) row["UpComponentCount"];
        int DownMonitorsCount = (int)row["DownComponentCount"];
        int UnknownMonitorsCount = (int)row["UnknownComponentCount"];

        var str = new List<string>();

        if (UpMonitorsCount > 0)
            str.Add(String.Format(Resources.APMWebContent.APMWEBDATA_TM0_64, UpMonitorsCount));
        if (DownMonitorsCount > 0)
            str.Add(String.Format(Resources.APMWebContent.APMWEBCODE_AK1_147, DownMonitorsCount));
        if (UnknownMonitorsCount > 0)
            str.Add(String.Format(Resources.APMWebContent.APMWEBCODE_AK1_148, UnknownMonitorsCount));

        return String.Join(Resources.APMWebContent.APMWEBCODE_AK1_8, str.ToArray());
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_146; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppSummaryCriticalApps"; }
    }

    protected static ApmStatus GetApmStatus(object item)
    {
        DataRowView row = (DataRowView)item;
        return ApmStatus.FromStringInt(row["Availability"]);
    }
}
