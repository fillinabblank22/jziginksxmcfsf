﻿using System;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_APM_Resources_Summary_WhatsNew : ApmBaseResource
{
    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }

    public override sealed string DisplayTitle
    {
        get { return this.Title + " 2020.2.1"; }
    }

    protected override string DefaultTitle
    {
        get { return InvariantString.Format("What's New in {0}", ApmConstants.ModuleShortName); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMNewFeatures202021"; }
    }

    protected static string MakeBoldText(string text)
    {
        return string.Format(text, "<strong>", "</strong>");
    }

    protected void Page_Load()
    {
        imgNewFeature.NavigateUrl = HelpHelper.GetHelpUrl(HelpLinkFragment);
    }

    public override string EditURL
    {
        get { return string.Empty; }
    }

    protected void OnRemoveResourceButtonClicked(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(this.Resource.ID);
        var whereToRedirect = (this.Request.Url.ToString().Contains("DetachResource.aspx")) ? this.Request.UrlReferrer : this.Request.Url;
        this.Response.Redirect(whereToRedirect.ToString());
    }
}
