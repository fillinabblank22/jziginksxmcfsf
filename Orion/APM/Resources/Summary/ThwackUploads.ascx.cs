using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_APM_Resources_Summary_ThwackUploads : ApmBaseResource
{
    private const int DefaultNumberOfPosts = 15;

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        OrionInclude.ModuleFile("Orion", "styles/Thwack.css");
    }

    protected int Count
    {
        get
        {
            bool limit;
            int records;
            if (bool.TryParse(this.Resource.Properties["LimitCount"], out limit) && limit &&
                int.TryParse(this.Resource.Properties["NumberOfRecords"], out records))
            {
                return records;
            }
            return DefaultNumberOfPosts;
        }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Static; } }
    
    protected DateTime OldestAllowedRssItem
    {
        get
        {
            string showType = this.Resource.Properties["ShowType"];
            if (String.IsNullOrEmpty(showType) || String.Equals(showType, "all", StringComparison.OrdinalIgnoreCase))
            {
                return DateTime.MinValue;
            }


            int days;
            days = Int32.TryParse(this.Resource.Properties["ShowLast"], out days) ? days: 30;

            return DateTime.Today.AddDays(-1*days);
        }
    }

    protected string SortBy
    {
        get
        {
            return this.Resource.Properties["SortBy"];
        }
    }
    
    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_133; }
    }

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/APM/Resources/EditThwackUploads.aspx?ResourceID={0}", this.Resource.ID);
            if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
            if (Page is OrionView)
                url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
            return url;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppSummaryThwack"; }
    }

}
