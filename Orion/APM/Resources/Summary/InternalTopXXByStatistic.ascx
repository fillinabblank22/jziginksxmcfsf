<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InternalTopXXByStatistic.ascx.cs" 
    Inherits="Orion_APM_Resources_Summary_InternalTopXXByStatistic" %>
<%@ Import Namespace="SolarWinds.APM.Web"%>
    
<%@ Register TagPrefix="apm" TagName="SmallApmStatusIcon" Src="~/Orion/APM/Controls/SmallApmStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>
<%@ Register TagPrefix="apm" TagName="PercentBar" Src="~/Orion/APM/Controls/PercentBar.ascx" %>

<orion:Include ID="I2" File="APM/Js/Resources.js" runat="server" />
<orion:Include ID="I3" File="APM/js/NodeManagement.js" runat="server" />

<asp:Repeater ID="grid" runat="server" >
    <HeaderTemplate>
        <table module-tag="apm-res-InternalTopXXByStatistic" id="topXXRes" class="DataGrid NeedsZebraStripes">
            <thead>                                 
                <tr>
                    <td class="apm_stats ReportHeader" colspan="2" style="white-space: nowrap">
                        <%= GetDBFieldTitle(0) %>
                    </td>
                    <td class="apm_stats ReportHeader" colspan="2" style="white-space: nowrap">
                        <%= GetDBFieldTitle(1) %>
                    </td>
                    <td class="apm_stats ReportHeader" colspan="2" style="white-space: nowrap">
                        <%= GetDBFieldTitle(2) %>
                    </td>
                    <td class="ReportHeader" style="text-align: center; padding-right: 20px; width: auto; white-space: nowrap" 
                        colspan="5"><%= GetColumnTitle(0)%>
                    </td>
                </tr>
            </thead>
    </HeaderTemplate>

    <ItemTemplate>
        <tr>
            <td style="width: 5%">
                <apm:SmallApmStatusIcon ID="SmallApmStatus2" runat="server" StatusValue='<%#GetComponentStatus(Container.DataItem, "ComponentStatus") %>' ErrorValue='<%# GetMonitorError(Container.DataItem) %>' CustomApplicationType='<%# GetCustomApplicationType(Eval("CustomApplicationType"))%>' />
            </td>
            <td class="apm_stats" style="word-break: break-all; width: 20%;">
                <a href="<%#GetComponentViewLink(Container.DataItem)%>">
                    <%# HttpUtility.HtmlEncode(Eval(GetDBFieldColumnNames(0)))%>
                </a>
            </td>
            <td style="width: 5%">
                <apm:SmallApmAppStatusIcon ID="SmallApmStatusIcon" runat="server" StatusValue='<%#GetApplicationStatus(Container.DataItem, "ApplicationStatus") %>' />
            </td>
            <td class="apm_stats" style="word-break: break-all; width: 15%;">
                <a href="<%#GetApplicationViewLink((int)Eval("ApplicationID"), GetCustomApplicationType(Eval("CustomApplicationType")))%>">
                    <%# HttpUtility.HtmlEncode(Eval(GetDBFieldColumnNames(1))) %>
                </a>
            </td>
            <td style="width: 5%">
                <img src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("nodeid"), null, Eval("nodestatus"), Eval("childstatus") ) %>" class="StatusIcon"/>
            </td>
            <td class="apm_stats" style="word-break: break-all;">
                <a href="<%#ApmBaseResource.GetViewLink((string)Eval("NodeId", "N:{0}"))%>">
                    <%# HttpUtility.HtmlEncode(Eval(GetDBFieldColumnNames(2)))%>
                </a>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td <%# GetCellStyleOverride(Container.DataItem, 0)%> ><%# GetColumnValue(Container.DataItem, 0)%></td>
            <td><%# ShowPercentValueAndPercentBar(Container.DataItem, 1) ? GetPercentValue(Container.DataItem, 1).ToString() : "" %></td>
            <apm:PercentBar ID="PercentBar1" runat="server" Value='<%#GetPercentValue(Container.DataItem, 1)%>' Visible='<%# ShowPercentValueAndPercentBar(Container.DataItem, 1)%>' />
            <td colspan="<%# GetNumberOfColspan(Container.DataItem, 1)%>">
                &nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
    <% if ((ProcessMonitorNodeId > 0) && (ApmRoleAccessor.AllowRealTimeServiceExplorer || ApmRoleAccessor.AllowRealTimeProcessExplorer || ApmRoleAccessor.AllowRealTimeEventExplorer))
       {%>
       <tr style="background-color: #E0E0E0;">
         <td colspan="11">
           <div align="right" style="padding-top:4px;padding-right:4px;">
                <% if (ApmRoleAccessor.AllowRealTimeEventExplorer && ShowEventExplorerLink)
                  { %>
                    <a class="sw-btn sw-btn-small" href="#" onclick="return SW.APM.NodeManagement.openRealTimeEventExplorerWindow(<%=ProcessMonitorNodeId %>,<%=GetRtMonitorCredId()%>)"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.APMWebContent.APMWEBDATA_VB1_343%></span></span></a>
                <% } %>
               <% if (ApmRoleAccessor.AllowRealTimeServiceExplorer && ShowServiceManagerLink)
                  { %>
                    <a class="sw-btn sw-btn-small" href="#" onclick="return SW.APM.NodeManagement.openRealTimeServicesWindow(<%=ProcessMonitorNodeId %>,<%=GetRtMonitorCredId()%>)"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.APMWebContent.APMWEBDATA_RM0_7%></span></span></a>
                <% } %>
               <% if (ApmRoleAccessor.AllowRealTimeProcessExplorer){ %>
                    <a class="sw-btn sw-btn-small" href="#" onclick="return SW.APM.NodeManagement.openRealTimeProcessWindow(<%=ProcessMonitorNodeId %>, '<%=ProcessMonitorSort%>')"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.APMWebContent.APMWEBDATA_VB1_61 %></span></span></a>
               <% } %>
            </div>
         </td>
       </tr>    
    <%}%>
        </table>
    </FooterTemplate>
        
</asp:Repeater> 
