using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Helpers;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_APM_Resources_Summary_TopXXByResponseTime : ApmBaseResource
{
	private int maxCount;

	protected override string DefaultTitle
	{
		get { return Resources.APMWebContent.APMWEBCODE_AK1_130; }
	}

	public override string DisplayTitle
	{
		get { return Title.Replace("XX", maxCount.ToString()); }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMPHAppSummaryTopResTime"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get
		{
			return ResourceLoadingMode.RenderControl;
		}
	}

	public override string EditURL
	{
		get
		{
			string url = string.Format("/Orion/APM/Resources/EditTopXX.aspx?ResourceID={0}", this.Resource.ID);
			if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
			{
				url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
			}
			if (Page is OrionView)
			{
				url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
			}
			return url;
		}
	}

	protected void grid_Init(object sender, EventArgs e)
	{
		InternalTopXXByStatistic1.DBFieldTitles = string.Format("{0};{1};{2}",
			Resources.APMWebContent.APMWEBDATA_VB1_62, Resources.APMWebContent.APMWEBDATA_VB1_63, Resources.APMWebContent.APMWEBDATA_VB1_64);

		maxCount = GetIntProperty("MaxCount", 10);
		//string filter = GetStringValue("Filter", string.Empty);
        string filter = (string.IsNullOrEmpty(GetStringValue("Filter", string.Empty)))
            ? string.Format("Monitor.ComponentType not in ( {0} ) ", ComponentHelper.GetJoinedListOfNotSupportResponseTime())
            : string.Format("{0} AND Monitor.ComponentType not in ( {1} ) ", GetStringValue("Filter", string.Empty), ComponentHelper.GetJoinedListOfNotSupportResponseTime());
		
		using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
		{
            var componentsInfo = businessLayer.GetTopComponentsByResponseTime(
                maxCount, 
                LimitationHelper.GetCurrentListOfLimitationIDs(), 
                filter);
            //var rows = (from DataRow row in componentsInfo.Rows let monitor = new ComponentTemplate {Type = (ComponentType) row["ComponentType"]} where monitor.HasResponseTimeSupport == false select row).ToList();

            //foreach (var dataRow in rows)
            //{
            //    componentsInfo.Rows.Remove(dataRow);
            //}

			InternalTopXXByStatistic1.DataSource = componentsInfo;
			InternalTopXXByStatistic1.DataBind();
		}
	}
}