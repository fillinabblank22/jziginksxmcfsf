using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Web.UI.HtmlControls;

using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Common.ResourceMetadata;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_APM_Resources_Summary_ApplicationHealth : StandardChartResource
{
    private const float ChartToResourceWidthRatio = 0.52f;

    protected override void OnInit(EventArgs e)
    {
        if ("NodeDetails".Equals(Resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase) && GetAppCount() == 0)
        {
            Visible = false;
        }
        else
        {
            if (!IsValidData)
            {
                WrapperContents.Visible = false;
                SampleImage.Visible = true;
            }
        }

        base.OnInit(e);

        this.ChartWidth = (int)(Resource.Width * ChartToResourceWidthRatio);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", "ApplicationHealthOverview");
        }

        HandleInit(WrapperContents);

        ApmBaseResource.EnsureApmScripts();
        OrionInclude.ModuleFile("APM", "Charts/Charts.APM.ApplicationOverviewChartLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("APM", "ChartsStyles.css", OrionInclude.Section.Middle);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return "AAOW"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_145; }
    }

    protected Int32 GetAppCount()
    {
        return IsValidData ? ApplicationCount : 0;
    }

    private int applicationCount = int.MinValue;
    private int ApplicationCount
    {
        get
        {
            if (applicationCount == int.MinValue)
            {
                applicationCount = SwisDAL.GetApplicationCount(GetFilter());
            }
            return applicationCount;
        }
    }

    protected Boolean IsValidData
    {
        get
        {
            return (ApplicationCount > 0);
        }
    }

    protected override void SetAdditionalStyles(HtmlGenericControl ChartPlaceHolder)
    {
        ChartPlaceHolder.Style["float"] = "left";
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();

        details["ResourceID"] = Resource.ID;

		var np = this.GetInterfaceInstance<SolarWinds.Orion.NPM.Web.INodeProvider>();
		if (np != null && np.Node != null)
		{
            details["NodeId"] = np.Node.NodeID;
		}

        return details;
    }
    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        // There is no need for export button
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected String GetFilter()
    {
        var filter = String.Empty;
        if ("NodeDetails".Equals(Resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase))
        {
		    var np = this.GetInterfaceInstance<SolarWinds.Orion.NPM.Web.INodeProvider>();
            if (np != null && np.Node != null)
            {
                filter = String.Format("Application.NodeID = {0}", np.Node.NodeID);
            }
        }
        return filter;
    }
}