<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveAlerts.ascx.cs" Inherits="Orion_APM_Resources_Summary_ActiveAlerts" %>
<%@ Register TagPrefix="orion" TagName="AlertsTable" Src="~/Orion/NetPerfMon/Controls/AlertsOnThisEntity.ascx" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:AlertsTable runat="server" ID="AlertsTable" />
    </Content>
</orion:resourceWrapper>
