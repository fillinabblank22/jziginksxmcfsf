﻿if (typeof (SW) == "undefined") {
	window.SW = {};
}
if (typeof (SW.APM) == "undefined") {
	SW.APM = {};
}
SW.APM.AppTree = {

    Succeeded: function (result, context) {
        var contentsDiv = $(context);
        contentsDiv.html(result);
        contentsDiv.slideDown('fast');
        SW.APM.AppTree.AutoClickLinks(contentsDiv);
        if (!result.length) {
        	var noDataContent=$(context).parents('div')[0];
        	$($(noDataContent).find("[id*='-NoDataContent']")).show();
        	$($(noDataContent).find(".apm_apptree")).hide();
    	} 
    },

    Failed: function (error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = String.format("@{R=APM.Strings;K=APMWEBJS_AK1_1;E=js}", error.get_message()); 
    },

    AutoClickLinks: function (contentsDiv) {
        // auto-click the "get 100 more Apps" links
        $("[id*='-startFrom-'] a", contentsDiv).click();

        // expand any nodes that are marked to be expanded	    
        $("[expand]", contentsDiv).removeAttr("expand").click();
    },


    ClickGroup: function (rootId, resourceId, keys) {
    	return SW.APM.AppTree.HandleClick(rootId, function (contentDiv) {
            ApplicationTree.GetTreeSection(resourceId, rootId, keys, 0, SW.APM.AppTree.Succeeded, SW.APM.AppTree.Failed, contentDiv);
        }, function () {
            ApplicationTree.CollapseTreeSection(resourceId, keys);
        });
    },

    ClickApplication: function (rootId, resourceId, applicationId) {
        return SW.APM.AppTree.HandleClick(rootId, function (contentDiv) {
            ApplicationTree.GetApplication(resourceId, rootId, applicationId, SW.APM.AppTree.Succeeded, SW.APM.AppTree.Failed, contentDiv);
        }, function () {
            ApplicationTree.CollapseApplication(resourceId, applicationId);
        });
    },

    ClickApplicationItem: function (rootId, resourceId, applicationId, appItemId, applicationItemType) {
        return SW.APM.AppTree.HandleClick(rootId, function (contentDiv) {
            ApplicationTree.GetComponentsByApplicationItem(resourceId, rootId, applicationId, appItemId, applicationItemType, SW.APM.AppTree.Succeeded, SW.APM.AppTree.Failed, contentDiv);
        }, function () {
            ApplicationTree.CollapseApplicationItem(resourceId, applicationId, appItemId);
        });
    },

    ClickAppItemCreator: function (rootId, resourceId, applicationId, componentId, applicationItemType) {
    	return SW.APM.AppTree.HandleClick(rootId, function (contentDiv) {
    		ApplicationTree.GetApplicationItems(resourceId, rootId, applicationId, componentId, applicationItemType, SW.APM.AppTree.Succeeded, SW.APM.AppTree.Failed, contentDiv);
    	}, function () {
    	    ApplicationTree.CollapseApplicationItems(resourceId, applicationId, componentId);
    	});
    },

    ClickCategory: function (rootId, resourceId, applicationId, categoryId, appItemId) {
        return SW.APM.AppTree.HandleClick(rootId, function (contentDiv) {
            ApplicationTree.GetComponentsByCategory(resourceId, rootId, applicationId, categoryId, typeof appItemId == 'undefined' ? null : appItemId, SW.APM.AppTree.Succeeded, SW.APM.AppTree.Failed, contentDiv);
        }, function () {
            ApplicationTree.CollapseCategory(resourceId, applicationId, categoryId, typeof appItemId == 'undefined' ? null : appItemId);
        });
    },

    LoadRoot: function (treeDiv, resourceId) {
        treeDiv.html("<div>@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}</div>");
        ApplicationTree.GetTreeSection(resourceId, "AppTree_r" + resourceId, [], 0, SW.APM.AppTree.Succeeded, SW.APM.AppTree.Failed, treeDiv.get(0));
    },

    HandleClick: function (rootId, expandCallback, collapseCallback) {
        var toggleImg = $('#' + rootId + '-toggle');
        var contentsDiv = $('#' + rootId + '-contents');

        if (!contentsDiv.is(':hidden')) {
            contentsDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
        } else {
            contentsDiv.html("@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}");
            contentsDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            expandCallback(contentsDiv.get(0));
        }

        return false;
    },

    GetMore: function (linkId, resourceId, keys, startFrom) {
        var contentsDiv = $get(linkId);
        contentsDiv.innerHTML = "@{R=APM.Strings;K=APMWEBJS_TM0_1;E=js}";
        ApplicationTree.GetTreeSection(resourceId, linkId, keys, startFrom, SW.APM.AppTree.Succeeded, SW.APM.AppTree.Failed, contentsDiv);
        return false;
    }
};