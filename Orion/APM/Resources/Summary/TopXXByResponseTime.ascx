<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXByResponseTime.ascx.cs" Inherits="Orion_APM_Resources_Summary_TopXXByResponseTime" %>    
<%@ Register Src="InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="grid_Init" 
            ColumnOrder="ResponseTime;PercentResponseTime" 
            ResourceType="Component"
            DBFieldTitles="Component Name;Application Name;Network Node" 
            DBFieldColumnNames="ComponentName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>