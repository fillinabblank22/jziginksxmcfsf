<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXByIOReadOpsPS.ascx.cs" 
    Inherits="Orion_APM_Resources_Summary_TopXXByIOReadOpsPS" %>
<%@ Register Src="InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="gridInit" 
            Statistic="IOReadOperationsPerSec" ColumnOrder="IOReadOperationsPerSec;IOReadOperationsPerSec" 
            ResourceType="Process"
            DBFieldTitles="" 
            DBFieldColumnNames="ProcessName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>