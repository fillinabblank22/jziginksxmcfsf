using System;
using System.Collections.Generic;
using SolarWinds.APM.Common;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_APM_Resources_Summary_TopXXByPhysicalMemory : ApmBaseResource
{
    private int maxCount;

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_129; }
    }

    public override string DisplayTitle
    {
        get
        {
            return this.Title.Replace("XX", maxCount.ToString());
        }
    }
    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppSummaryTopPhysMem"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get
		{
			return ResourceLoadingMode.RenderControl;
		}
	}

    protected void gridInit(object sender, EventArgs e)
    {
        InternalTopXXByStatistic1.DBFieldTitles = string.Format("{0};{1};{2}", Resources.APMWebContent.APMWEBDATA_VB1_62,
                                                                               Resources.APMWebContent.APMWEBDATA_VB1_63,
                                                                               Resources.APMWebContent.APMWEBDATA_VB1_64);
        maxCount = GetIntProperty("MaxCount", 10);
        string filter = GetStringValue("Filter", string.Empty);

        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            InternalTopXXByStatistic1.DataSource =
                businessLayer.GetTopComponentsByProcessStatistics(maxCount,
                    InternalTopXXByStatistic1.Statistic,
                    LimitationHelper.GetCurrentListOfLimitationIDs(),
                    filter);
            InternalTopXXByStatistic1.DataBind();
        }
    }

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/APM/Resources/EditTopXX.aspx?ResourceID={0}", this.Resource.ID);
            if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
            if (Page is OrionView)
                url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
            return url;
        }
    }
}
