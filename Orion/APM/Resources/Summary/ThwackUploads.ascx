<%@ Import Namespace="SolarWinds.APM.Web"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThwackUploads.ascx.cs" Inherits="Orion_APM_Resources_Summary_ThwackUploads" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>   
    
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Services>
			<asp:ServiceReference path="/Orion/APM/Services/ThwackAPM.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="APM/Styles/Resources.css" />

<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowHeaderBar="false">
    <Content>
	    <div style="border: 1px solid #d5d5d5;">
		    <table cellpadding="0" cellspacing="0" border="0">
			    <tr>
				    <td style="border: 0;border-bottom: 0; background-image: url(/Orion/APM/Images/Thwack/New_ThwackHeader_Background.png);">
					    <table cellpadding="0px" cellspacing="0px" border="0" class="ThwackHeader" >
						    <tr>
							    <td style="border: none; width: 100px; vertical-align: middle; padding-top: 2px; ">
								    <img class="ThwackHeaderImage" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("APM", "Thwack/New_ThwackLogo.png") %>" />
							    </td>
                                <td style="border: none;  padding-top: 2px; color: white; font-size: 14px; font-weight: bold; text-decoration: none; ">
								    <%= Resources.APMWebContent.Web_Thwack_LatestAppTemplates %>
							    </td>
							    <td style="border: none; color: white;">
								    <asp:PlaceHolder ID="phEditButton" runat="server">
								    <orion:LocalizableButtonLink  runat="server" href="<%# this.EditURL %>" DisplayType="Resource" CssClass="EditResourceButton" LocalizedText="Edit" />
								    </asp:PlaceHolder></td>
							    <td style="border: none; width:10px; padding-right: 10px; color: white;">
								    <asp:PlaceHolder ID="phHelpButton" runat="server">
                                    <orion:HelpButton runat="server" ID="HelpButton" HelpUrlFragment="<%# this.HelpLinkFragment %>" />                       
								    </asp:PlaceHolder>
							    </td>
						    </tr>
					    </table>
				    </td>
			    </tr>
		    </table>        
            <div runat="server" id="ThwackRecentlyPostedContentList" class="ThwackContent" />
            <table cellpadding="0px" cellspacing="0px" style="background-color: #eaeaea; height: 22px;">
                <tr>                
                    <td style="border: none; text-align: right; padding-right: 10px;">
                        <a href="http://thwack.com/r.ashx?7" ><%= Resources.APMWebContent.APMWEBDATA_AK1_153 %> &#0187;</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="http://thwack.com/user/CreateUser.aspx" ><%= Resources.APMWebContent.APMWEBDATA_AK1_154 %> &#0187;</a>
                    </td>            
                </tr>
            </table>
        </div>
    
        <script type="text/javascript">
            //<![CDATA[
            Sys.Application.add_init(function(){
                ThwackAPM.GetRecentlyPostedContent("apm", <%= this.Count %>, '<%= this.OldestAllowedRssItem %>','<%= this.SortBy %>', 
                    function(result) { 
                        var thwackRecentContentList = $get('<%=this.ThwackRecentlyPostedContentList.ClientID %>');
                        thwackRecentContentList.innerHTML = result; 
                    },
                    function() { 
                        var thwackRecentContentList = $get('<%=this.ThwackRecentlyPostedContentList.ClientID %>');
                        thwackRecentContentList.innerHTML = 'Unable to contact thwack server.'; 
                    }); 
                });
            //]]>
        </script>
    </Content>
</orion:resourceWrapper>    
    