<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NoApplicationDefined.ascx.cs" Inherits="Orion_APM_Resources_Summary_NoApplicationDefined" %>
    
<orion:Include runat="server" File="APM/Styles/Resources.css" />

<orion:resourceWrapper runat="server" ID="Wrapper" ShowHeaderBar="false">
	<Content>
	    <div class="NoApps">
			<table border="0" cellpadding="0" cellspacing="0" style="border-bottom:0;">
				<tr>
					<td style="width:115px; border-bottom:0; padding-top: 20px;">
						<img src="/Orion/APM/Images/FirstTimeUse/icon_ftu_quickstart.gif" />
					</td>
					<td style="padding: 5px; border-bottom:0;">
						<div style="font-weight: bold; font-size: 15px">
						    <%= Resources.APMWebContent.APMWEBDATA_AK1_192 %>
						</div>
						<div class="Box">
							<span style="font-weight: bold;"><%= Resources.APMWebContent.APMWEBDATA_AK1_170 %></span><br />
							<%= Resources.APMWebContent.APMWEBDATA_AK1_171 %>
						</div>
						<div class="sw-btn-bar">
                            <orion:LocalizableButtonLink runat="server" ID="AssignApplicationMonitors" NavigateUrl="/Orion/APM/Admin/QuickStart/GettingStarted.aspx" Text="<%$ Resources:APMWebContent,APMWEBDATA_AK1_190 %>" DisplayType="Primary" />
                            <orion:LocalizableButton runat="server" ID="RemoveResourceButton" OnClick="OnRemoveResourceButtonClicked" Text="<%$ Resources:APMWebContent,APMWEBDATA_AK1_191 %>" DisplayType="Secondary"/>
						</div>
					</td>
				</tr>
			</table>
        </div>
	</Content>
</orion:resourceWrapper>    
