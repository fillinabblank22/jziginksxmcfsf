<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXByCpuLoad.ascx.cs" Inherits="Orion_APM_Resources_Summary_TopXXByCpuLoad" %>
<%@ Register Src="InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="gridInit" 
            Statistic="CPU" ColumnOrder="PercentCPU;PercentCPU" 
            ResourceType="Process"
            DBFieldTitles="" 
            DBFieldColumnNames="ProcessName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>