<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationHealth.ascx.cs" Inherits="Orion_APM_Resources_Summary_ApplicationHealth" %>
<%@ Import Namespace="SolarWinds.APM.Web.UI" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="false">
    <Content>
       <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
       <div id="SampleImage" runat="server" visible="false">
			<img src="<%= LocalizationUrlHelper.GetUrlFromTemplate("/Orion/APM/Images/[lang]/AppHelathOverviewSample.gif") %>" />
		</div>
    </Content>
</orion:resourceWrapper> 
