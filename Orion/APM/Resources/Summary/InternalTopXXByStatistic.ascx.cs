using System;
using System.Data;
using System.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.DAL;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.UI.DisplayStrategies;
using System.Globalization;

using SolarWinds.Orion.Web.UI;

public enum ResourceType
{
	Component,
	Process
}

public partial class Orion_APM_Resources_Summary_InternalTopXXByStatistic : UserControl
{
    private readonly AdHocMapper mapper = new AdHocMapper();
	private string[] columnOrder;
	private ProcessStatistics statistic;
	private PortStatistics statisticData;
	private object dataSource;
	private string[] dbFieldTitles;
	private string[] dbFieldColumnNames;
	private ResourceType resourceType;

	public ProcessStatistics Statistic
	{
		get { return statistic; }
		set { statistic = value; }
	}

	public PortStatistics StatisticData
	{
		get { return statisticData; }
		set { statisticData = value; }
	}

	public string ColumnOrder
	{
		set { columnOrder = value.Split(new char[] { ';' }); }
	}

	public int ColumnCount
	{
		get { return columnOrder.Length; }
	}

	public ResourceType ResourceType
	{
		get { return resourceType; }
		set { resourceType = value; }
	}

	public object DataSource
	{
		get { return dataSource; }
		set { dataSource = value; }
	}

	public string DBFieldTitles
	{
		set { dbFieldTitles = value.Split(new char[] { ';' }); }
	}

	public string DBFieldColumnNames
	{
		set { dbFieldColumnNames = value.Split(new char[] { ';' }); }
	}

	protected virtual bool ShowPercentValueAndPercentBar(object item, int columnIndex)
	{
		return (String.IsNullOrEmpty(GetPercentValue(item, columnIndex).ToString()) ||
			GetPercentValue(item, columnIndex).ToString().Equals("N/A", StringComparison.OrdinalIgnoreCase))
			? false
			: true;
	}

	protected int GetNumberOfColspan(object item, int columnIndex)
	{
		if (ShowPercentValueAndPercentBar(item, columnIndex))
		{
			return 1;
		}
		return 2;
	}

	protected override void OnDataBinding(EventArgs e)
	{
		base.OnDataBinding(e);

		grid.DataSource = DataSource;
		grid.DataBind();
	}

	protected ApmResponseTime GetResponseTime(object item, string columnName)
	{
		DataRowView row = (DataRowView)item;

		Threshold theThreshold = new Threshold();
		theThreshold.WarnLevel = GetThresholdValue(row["WarnThreshold"]);
		theThreshold.CriticalLevel = GetThresholdValue(row["CriticalThreshold"]);

		long responseTime = (long)row[columnName];
		return new ApmResponseTime(responseTime, theThreshold.WarnLevel, theThreshold.CriticalLevel);
	}

	protected ApmStatisticData GetStatisticData(object item, string columnName)
	{
		if (string.IsNullOrEmpty(columnName))
			return new ApmStatisticData();

		DataRowView row = (DataRowView)item;

		ApmStatisticData statistic = new ApmStatisticData();

		switch (columnName.ToLowerInvariant())
		{
			case "statisticdata":
				if (row["StatisticData"] != DBNull.Value)
				{
					statistic.Value = (double)row["StatisticData"];
					statistic.Threshold.WarnLevel = GetThresholdValue(row["StatisticWarnThreshold"]);
					statistic.Threshold.CriticalLevel = GetThresholdValue(row["StatisticCriticalThreshold"]);
				}
				break;
		}
		return statistic;
	}

	protected ApmPercentage GetResponseTimePercentage(object item, string columnName)
	{
		return new ApmPercentage(GetResponseTime(item, columnName).Percentage);
	}

	protected ApmPercentage GetStatisticDataPercentage(object item, string columnName)
	{
		return new ApmPercentage(GetStatisticData(item, columnName).Percentage);
	}

	protected string GetDBFieldTitle(int columnIndex)
	{
		if (columnIndex >= dbFieldTitles.Length)
			return "";

		string columnKey = dbFieldTitles[columnIndex];
		return columnKey;
	}

	protected string GetDBFieldColumnNames(int columnIndex)
	{
		if (columnIndex >= dbFieldColumnNames.Length)
			return "";

		string columnKey = dbFieldColumnNames[columnIndex];
		return columnKey;
	}

	protected string GetColumnTitle(int columnIndex)
	{
		if (columnIndex >= columnOrder.Length)
			return "";

		string columnKey = columnOrder[columnIndex];

		switch (columnKey.ToLowerInvariant())
		{
			case "percentcpu":
				return Resources.APMWebContent.APMWEBCODE_VB1_18;
			case "percentmemory":
			case "memoryused":
				return Resources.APMWebContent.APMWEBCODE_VB1_19;
			case "percentvirtual":
			case "virtualused":
				return Resources.APMWebContent.APMWEBCODE_VB1_20;
			case "responsetime":
			case "percentresponsetime":
				return Resources.APMWebContent.APMWEBCODE_VB1_21;
			case "statisticdata":
				return Resources.APMWebContent.APMWEBCODE_VB1_22;
			case "ioreadoperationspersec":
				return Resources.APMWebContent.APMWEBCODE_TOPXX_IOREADOPS_HEADER;
			case "iowriteoperationspersec":
				return Resources.APMWebContent.APMWEBCODE_TOPXX_IOWRITEOPS_HEADER;
			case "iototaloperationspersec": 
				return Resources.APMWebContent.APMWEBCODE_TOPXX_IOTOTALOPS_HEADER;
		}

		return columnKey;
	}

	private double GetThresholdValue(object columnValue)
	{
		double value = Double.MaxValue;
		if (columnValue != DBNull.Value)
		{
			if (!Double.TryParse(columnValue.ToString(), out value))
				value = Double.MaxValue;
		}
		return value;
	}

	protected string GetCellStyleOverride(object item, int columnIndex)
	{
		if (columnIndex >= columnOrder.Length)
			return null;

		string columnKey = columnOrder[columnIndex];

		DataRowView row = (DataRowView)item;

		switch (columnKey.ToLowerInvariant())
		{
			case "ioreadoperationspersec":
			case "iowriteoperationspersec":
			case "iototaloperationspersec":
				return "style=\"text-align:right\"";
			default:
				return null;
		}
	}

	protected object GetColumnValue(object item, int columnIndex)
	{
		if (columnIndex >= columnOrder.Length)
			return "";

		string columnKey = columnOrder[columnIndex];

		DataRowView row = (DataRowView)item;

		switch (columnKey.ToLowerInvariant())
		{
			case "memoryused":
				if (row["MemoryUsed"] != DBNull.Value)
					return new ApmBytes((long)row["MemoryUsed"], ApmBytes.Unit.GB, 2);
				break;
			case "virtualused":
				if (row["VirtualMemoryUsed"] != DBNull.Value)
					return new ApmBytes((long)row["VirtualMemoryUsed"], ApmBytes.Unit.GB, 2);
				break;
			case "responsetime":
				if (row["ResponseTime"] != DBNull.Value)
					return GetResponseTime(item, columnKey);
				break;
			case "statisticdata":
				if (row["StatisticData"] != DBNull.Value)
					return GetStatisticData(item, columnKey);
				break;
			case "ioreadoperationspersec":
				if (row["IOReadOperationsPerSec"] != DBNull.Value)
					return string.Format(CultureInfo.CurrentCulture, Resources.APMWebContent.APMWEBCODE_TOPXX_IOOPS_VALUE_FORMAT, (double)row["IOReadOperationsPerSec"]);
				break;
			case "iowriteoperationspersec":
				if (row["IOWriteOperationsPerSec"] != DBNull.Value)
					return string.Format(CultureInfo.CurrentCulture, Resources.APMWebContent.APMWEBCODE_TOPXX_IOOPS_VALUE_FORMAT, (double) row["IOWriteOperationsPerSec"]);
				break;
			case "iototaloperationspersec":
				if (row["IOTotalOperationsPerSec"] != DBNull.Value)
					return string.Format(CultureInfo.CurrentCulture, Resources.APMWebContent.APMWEBCODE_TOPXX_IOOPS_VALUE_FORMAT, (double)row["IOTotalOperationsPerSec"]);
				break;

		}

		return "";
	}
	protected object GetColumnValue(object item, string columnName)
	{
		DataRowView row = (DataRowView)item;

		switch (columnName)
		{
			case "nodestatus":
				if (row["NodeStatus"] != DBNull.Value)
					return Int32.Parse(row["NodeStatus"].ToString().Trim());
				break;
			case "nodeid":
				if (row["NodeId"] != DBNull.Value)
					return row["NodeId"];
				break;
		}

		return "";
	}

	protected ApmPercentage GetPercentValue(object item, int columnIndex)
	{
		if (columnIndex >= columnOrder.Length)
			return new ApmPercentage(0);

		string columnKey = columnOrder[columnIndex];

		DataRowView row = (DataRowView)item;

		Threshold theThreshold = new Threshold();
		theThreshold.WarnLevel = GetThresholdValue(row["WarnThreshold"]);
		theThreshold.CriticalLevel = GetThresholdValue(row["CriticalThreshold"]);

		ApmPercentage stat = new ApmPercentage();

		switch (columnKey.ToLowerInvariant())
		{
			case "percentcpu":
				if (row["PercentCPU"] != DBNull.Value)
					stat.Value = Convert.ToDouble(row["PercentCPU"]);
				break;
			case "percentmemory":
				if (row["PercentMemory"] != DBNull.Value)
					stat.Value = Convert.ToDouble(row["PercentMemory"]);
				break;
			case "percentvirtual":
				if (row["PercentVirtualMemory"] != DBNull.Value)
					stat.Value = Convert.ToDouble(row["PercentVirtualMemory"]);
				break;
			case "percentresponsetime":
				if (row["ResponseTime"] != DBNull.Value)
					stat.Value = GetResponseTimePercentage(item, "ResponseTime").Value;
				break;
			case "percentstatisticdata":
				if (row["StatisticData"] != DBNull.Value)
					stat.Value = GetStatisticDataPercentage(item, "StatisticData").Value;
				break;
		}

		if (theThreshold.WarnLevel != Double.MaxValue)
			stat.Threshold.WarnLevel = theThreshold.WarnLevel;

		if (theThreshold.CriticalLevel != Double.MaxValue)
			stat.Threshold.CriticalLevel = theThreshold.CriticalLevel;

		return stat;
	}

	protected ApmStatus GetComponentStatus(object item, string columnName)
	{
		DataRowView row = (DataRowView)item;
		return new ApmStatus((SolarWinds.APM.Common.Models.Status)row[columnName]);
	}

	protected ApmStatus GetApplicationStatus(object item, string columnName)
	{
		DataRowView row = (DataRowView)item;
		return new ApmStatus((SolarWinds.APM.Common.Models.Status)row[columnName]);
	}

	protected ApmMonitorError GetMonitorError(object item)
	{
		switch (ResourceType)
		{
			case ResourceType.Component:
				return new ApmMonitorError(ApmErrorCode.Ok, int.MinValue, StatusCodeType.None, string.Empty);
			case ResourceType.Process:
				{
					DataRowView row = (DataRowView)item;
					ApmErrorCode errorCode = (ApmErrorCode)int.MinValue;
					StatusCodeType statusType = StatusCodeType.None;
					int statusValue = int.MinValue;
					string errorMessage = string.Empty;
					if (row["ComponentErrorCode"] != DBNull.Value)
					{
						errorCode = (ApmErrorCode)row["ComponentErrorCode"];
					}
					if (row["ComponentStatusCodeType"] != DBNull.Value)
					{
						statusType = (StatusCodeType)row["ComponentStatusCodeType"];
					}
					if (row["ComponentStatusCode"] != DBNull.Value)
					{
						statusValue = (int)row["ComponentStatusCode"];
					}
					if (row["ComponentErrorMessage"] != DBNull.Value)
					{
						errorMessage = (string)row["ComponentErrorMessage"];
					}
					return new ApmMonitorError(errorCode, statusValue, statusType, errorMessage);
				}
			default:
				return null;
		}
	}

	public int ProcessMonitorNodeId { get; set; }

	public string ProcessMonitorSort { get; set; }

    public bool ItSupportsNodeManagementTasksDisplayStrategy 
    { 
        get { return ProcessMonitorNodeId > 0; } 
    }

    private INodeManagementTasksDisplayStrategy nodeManagementTasksDisplayStrategy;
    protected INodeManagementTasksDisplayStrategy NodeManagementTasksDisplayStrategy
    {
        get
        {
            if (nodeManagementTasksDisplayStrategy == null && ItSupportsNodeManagementTasksDisplayStrategy)
            {
                using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                {
                    var node = businessLayer.GetNode(ProcessMonitorNodeId);
                    nodeManagementTasksDisplayStrategy = ServiceLocatorForWeb.GetServiceForWeb<ITasksDisplayStrategyFactory>().CreateTasksDisplayStrategy(node);
                }
            }
            
            return nodeManagementTasksDisplayStrategy;
        }
    }

	protected bool ShowServiceManagerLink
	{
		get
		{
		    return ItSupportsNodeManagementTasksDisplayStrategy && NodeManagementTasksDisplayStrategy.IsServiceManagerSupported();
		}
	}

    protected bool ShowEventExplorerLink
    {
        get
        {
            return ItSupportsNodeManagementTasksDisplayStrategy && NodeManagementTasksDisplayStrategy.IsEventExplorerSupported();
        }
    }

    protected int GetRtMonitorCredId()
    {
        return ItSupportsNodeManagementTasksDisplayStrategy ? NodeManagementTasksDisplayStrategy.GetWorkingWindowsCredentialId() : ComponentBase.NoCredentialSetId;
    }
    
    protected string GetCustomApplicationType (object value)
    {
        return value is DBNull ? null : (string)value;
    }

    protected string GetApplicationViewLink(int applicationId, string customAppType)
    {
        return BaseResourceControl.GetViewLink(ApmApplication.GetNetObjectId(applicationId, customAppType));
    }

    protected string GetComponentViewLink(object rowViewObject)
    {
        var rowView = (DataRowView) rowViewObject;

        var componentNavigationInfo = mapper.ToObject<ComponentNavigationInfo>(rowView.Row, true);
        return WebPluginManager.Instance.GetComponentViewLink(componentNavigationInfo);
    }

}
