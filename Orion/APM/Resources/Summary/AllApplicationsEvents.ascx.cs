using System;
using System.Collections.Generic;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_APM_Resources_Summary_AllAppplicationsEvents : ApmBaseResource
{
    private ApmApplicationBase application;
    private string subTitle = Resources.APMWebContent.TimePeriod_all_time; 
    protected override void OnInit(EventArgs e)
    {
        
        base.OnInit(e);
    }

    protected void grid_Init(object sender, EventArgs e)
    {
        DateTime startDate;
        DateTime endDate;
        ParsePeriodLimitaion((TimePeriod)this.GetIntProperty("Period", 3), out startDate, out endDate);
        
        using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            orionEventList.DataSource = businessLayer.GetEventsForAllApplications(
                startDate, 
                endDate, 
                LimitationHelper.GetCurrentListOfLimitationIDs());
            orionEventList.DataBind();
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.APMWebContent.APMWEBCODE_AK1_143; }
    }

    public override string DisplayTitle
    {
        get { return this.DefaultTitle; }
    }

    public override string SubTitle
    {
        get
        {
            return subTitle;
        }
    }

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/APM/Resources/EditAllApplicationsEvents.aspx?ResourceID={0}", this.Resource.ID);
            if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
            if (Page is OrionView)
                url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
            return url;
        }
    }

    protected ApmApplicationBase ApmApplication
    {
        get
        {
            if (application == null)
                application = GetInterfaceInstance<IApplicationProviderBase>().ApmApplication;

            return application;
        }
    }

    private void ParsePeriodLimitaion(TimePeriod timePeriod, out DateTime start, out DateTime end)
    {

        switch (timePeriod)
        {
            case TimePeriod.PastHour:
                start = DateTime.Now.AddHours(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_past_hour;
                break;

            case TimePeriod.LastMonth:
                start = DateTime.Now.AddMonths(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_month;
                break;

            case TimePeriod.Last12Months:
                start = DateTime.Now.AddYears(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_12_months;
                break;

            case TimePeriod.Last24Hours:
                start = DateTime.Now.AddDays(-1);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_24_hours;
                break;

            case TimePeriod.Last2Hours:
                start = DateTime.Now.AddHours(-2);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_2_hours;
                break;

            case TimePeriod.Last2Months:
                start = DateTime.Now.AddMonths(-2);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_2_months;
                break;

            case TimePeriod.Last30Days:
                start = DateTime.Now.AddDays(-30);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_30_days;
                break;

            case TimePeriod.Last7Days:
                start = DateTime.Now.AddDays(-7);
                end = DateTime.Now;
                subTitle = Resources.APMWebContent.TimePeriod_last_7_days;
                break;

            case TimePeriod.ThisMonth:
                end = DateTime.Now;
                start = new DateTime(end.Year, end.Month, 1);
                subTitle = Resources.APMWebContent.TimePeriod_this_months;
                break;

            case TimePeriod.ThisYear:
                end = DateTime.Now;
                start = new DateTime(end.Year, 1, 1);
                subTitle = Resources.APMWebContent.TimePeriod_this_year;
                break;

            case TimePeriod.Today:
                end = DateTime.Now;
                start = new DateTime(end.Year, end.Month, end.Day);
                subTitle = Resources.APMWebContent.TimePeriod_today;
                break;

            case TimePeriod.Yesterday:
                start = DateTime.Now.Date;
                end = new DateTime(start.Year, start.Month, start.Day);
                start = DateTime.Now.Date.AddDays(-1);
                subTitle = Resources.APMWebContent.TimePeriod_yesterday;
                break;
            case TimePeriod.Custom:
                end = DateTime.Now;
                start = new DateTime(1900, 1, 1);
                subTitle = Resources.APMWebContent.TimePeriod_all_time;
                break;

            default:
                end = DateTime.Now;
                start = new DateTime(end.Year, end.Month, end.Day);
                break;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMPHAppSummaryAppEvents"; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}





