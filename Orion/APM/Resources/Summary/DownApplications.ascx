<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownApplications.ascx.cs" Inherits="Orion_APM_Resources_Summary_DownApplications" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="apm" TagName="SmallApmAppStatusIcon" Src="~/Orion/APM/Controls/SmallApmAppStatusIcon.ascx" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Include ID="I2" File="APM/Js/Resources.js" runat="server" />
	
        <asp:Repeater ID="grid" runat="server" OnInit="grid_Init">
            <HeaderTemplate>
                <table class="DataGrid NeedsZebraStripes">
                    <thead>
                        <tr>
                            <td colspan="2"><%= Resources.APMWebContent.APMWEBDATA_AK1_146 %></td>
                            <td><%= Resources.APMWebContent.APMWEBDATA_AK1_179 %></td>
                            <td nowrap="nowrap"><%= Resources.APMWebContent.APMWEBDATA_AK1_180 %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <apm:SmallApmAppStatusIcon ID="SmallApmAppStatusIcon1" runat="server" StatusValue='<%# GetApmStatus(Container.DataItem)%>' />
                    </td>
                    <td>
                        <%# GetAppNodeNameLinks(Container.DataItem) %>
                    </td>
                    <td>
                        <a href="<%#GetViewLink("AA:" + Eval("ApplicationID"))%>">                    
                         <%# GetColumnValue(Container.DataItem, "UpDownMonitors")%></a>
                    </td>
	                <td nowrap="nowrap">
                        <a href="<%#GetViewLink("AA:" + Eval("ApplicationID"))%>">                    
	                    <%# GetDownTime(Container.DataItem)%></a>
	                </td>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>
                    
        </asp:Repeater>
	</Content>
</orion:resourceWrapper>