<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXByStatisticData.ascx.cs" Inherits="Orion_APM_Resources_Summary_TopXXByStatisticData" %>
<%@ Register Src="InternalTopXXByStatistic.ascx" TagName="InternalTopXXByStatistic" TagPrefix="apm" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <apm:InternalTopXXByStatistic ID="InternalTopXXByStatistic1" runat="server"  OnInit="grid_Init" 
            ColumnOrder="StatisticData;StatisticData"
            ResourceType="Component"
            DBFieldTitles="Component Name;Application Name;Network Node" 
            DBFieldColumnNames="ComponentName;ApplicationName;NodeName" />
    </Content>
</orion:resourceWrapper>