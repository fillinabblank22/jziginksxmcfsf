<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllApplications.ascx.cs" Inherits="Orion_APM_Resources_Summary_AllApplications" %>

<asp:ScriptManagerProxy id="ApmTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/APM/Services/ApplicationTree.asmx" />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="AllApplications.js" />
	</Scripts>
</asp:ScriptManagerProxy>


<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <asp:PlaceHolder id="placeHolderPlugins" runat="server"></asp:PlaceHolder>
        
        <script type="text/javascript">
            //<![CDATA[
            $(function () {
                var refresh = function () {
                    SW.APM.AppTree.LoadRoot($('#<%=this.apptree.ClientID%>'), '<%=this.Resource.ID %>');
	            };
	            SW.Core.View.AddOnRefresh(refresh, '<%= this.apptree.ClientID %>');
	            refresh();
	        });
	        //]]>
        </script>

		<div class="apm_apptree" ID="apptree" runat="server">
			<asp:Literal runat="server" ID="TreeLiteral" />
		</div>
		 <!-- Following show me when no nodes exists in DB -->
        <div id="SAMAllApp-<%= this.Resource.ID %>-NoDataContent" style ="display: none;">
           <div class="SAM_GettingStartedBox">
              <div class="SAM_First">
                 <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                 <h3><b><%= Resources.CoreWebContent.WEBDATA_PS0_1 %></b> <%= Resources.APMWebContent.APMWEBDATA_RM0_4%></h3>
              </div>
			  <div> <%= Resources.APMWebContent.APMWEBDATA_RM0_5%></div>
			  <div><%= Resources.APMWebContent.APMWEBDATA_RM0_6%> </div>
			  <div class="SAM_GettingStartedButtons">
                  <orion:LocalizableButton runat="server" ID="discoverNetworkButton" PostBackUrl="~/Orion/APM/Admin/AutoAssign/SelectNodes.aspx" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ resources:APMWebContent,APMSITEMAPDATA_VB1_10 %>" />
              </div>
           </div>
        </div>
	</Content>	
</orion:resourceWrapper>
