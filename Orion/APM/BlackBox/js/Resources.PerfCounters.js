﻿/*jslint browser: true*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.BB.Resources.PerfCounters', function (PerfCounters) {
    'use strict';

    var $ = APMjs.assertQuery(),
        resources = {
            buttonCollapse: '/Orion/images/Button.Collapse.gif',
            buttonExpand: '/Orion/images/Button.Expand.gif',
            timing: 400
        };

    function setExpandImage(imageControl, expanded) {
        imageControl.attr('src', expanded ? resources.buttonCollapse : resources.buttonExpand);
    }

    function toggleCollapse($container, $img) {
        if ($container.is(':visible')) {
            setExpandImage($img, false);
            $container.hide(resources.timing);
        } else {
            setExpandImage($img, true);
            $container.show(resources.timing);
        }
    }

    function makeCollapsible($container, $lnk) {
        var $img;

        $img = $('<img />');
        $img.prependTo($lnk);
        setExpandImage($img, true);

        $lnk.click(function () {
            toggleCollapse($container, $img);
        });
    }

    function initComponentsContainer($div, $lnk) {
        var $container;

        $container = $('<div/>');
        $container.css('margin-left', '35px');

        makeCollapsible($container, $lnk);

        $div.addClass('zebraRow');
        $container.appendTo($div);

        return $container;
    }

    function initCategory($target, data) {
        var $div, $lnk, $container;

        $div = $('<div/>');

        $lnk = $('<a/>')
            .attr('href', data.components ? '#' : data.link)
            .text(data.name)
            .appendTo($div);

        $('<img/>')
            .attr('src', data.img)
            .prependTo($lnk);

        if (data.components) {
            $container = initComponentsContainer($div, $lnk);
            APMjs.linqEach(data.components, function (component) {
                initCategory($container, {
                    img: component.Image,
                    name: component.Name,
                    link: component.Link
                });
            });
        }

        $div.appendTo($target);
    }

    function onLoaded(treeData, resourceID) {
        var $target = $('#BBPerfCountersTreeDiv-' + resourceID);
        $target.empty();
        $target.addClass('apm_NeedsZebraStripes');
        APMjs.linqEach(treeData, function (category) {
            initCategory($target, {
                img: category.Image,
                name: category.Name,
                components: category.Components
            });
        });
    }

    function loadData(resourceID, data) {
        $.ajax({
            type: 'POST',
            url: '/Orion/APM/BlackBox/Services/PerformanceCounters.asmx/GetPerformanceCountersTree',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(data),
            success: function (xData) {
                onLoaded(xData.d, resourceID);
            }
        });
    }

    function loadDataForAppOrItem(data) {
        var wsdata = {
            customApplicationType: data.appType,
            applicationId: data.appId,
            applicationItemId: data.isItemSpecific ? data.appItemId : null
        };
        loadData(data.resourceId, wsdata);
    }

    // publish methods
    PerfCounters.LoadDataForAppOrItem = loadDataForAppOrItem;
});
