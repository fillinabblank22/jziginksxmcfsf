﻿<%@ WebService Language="C#" Class="Orion.APM.BlackBox.Services.PerformanceCounters" %>

using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.DisplayTypes;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

namespace Orion.APM.BlackBox.Services
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class PerformanceCounters : WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetPerformanceCountersTree(string customApplicationType, int applicationId, int? applicationItemId)
        {
            var whereConditions = new List<string>(2) { string.Format(CultureInfo.InvariantCulture, "c.ApplicationID = {0}", applicationId) };
            if (applicationItemId.HasValue)
            {
                whereConditions.Add("c.ApplicationItemId = " + applicationItemId.Value);
                whereConditions.Add("c.ComponentType IN (59 ,37, 55)");
            }
            else
            {
                whereConditions.Add("ct.IsApplicationItemSpecific = 0");
                whereConditions.Add("c.ComponentType IN (37, 55)");
            }

            var netObjectPrefix = WebPluginManager.Instance.GetNetObjectPrefix(ViewType.Monitor, customApplicationType);
            using (var swis = InformationServiceProxy.CreateV3())
            {
                var query = string.Format(CultureInfo.InvariantCulture, @"SELECT ComponentID, c.ShortName, cc.DisplayName, ccs.Availability
FROM Orion.APM.Component as c
LEFT JOIN Orion.APM.ComponentTemplate AS ct ON c.TemplateID = ct.ID
LEFT JOIN Orion.APM.ComponentCategory AS cc ON ct.ComponentCategoryID = cc.CategoryID
LEFT JOIN Orion.APM.CurrentComponentStatus AS ccs ON c.ComponentID = ccs.ComponentID
WHERE ct.VisibilityMode <> {0} AND ccs.Availability <> 7 AND {1}", (int)ComponentVisibilityMode.Hidden, string.Join(" AND ", whereConditions));

                var plugin = WebPluginManager.Instance;

                return swis.Query(query).Rows
                    .Cast<DataRow>()
                    .Select(item =>
                        new
                        {
                            ComponentID = item.Field<long>("ComponentID"),
                            Name = item["ShortName"].ToString(),
                            Category = item["DisplayName"].ToString(),
                            Status = ApmStatus.FromInt(item.Field<int?>("Availability") ?? 0)
                        })
                    .GroupBy(item => item.Category)
                    .Select(item =>
                        new
                        {
                            Name = item.Key,
                            Image = plugin.GetSmallIconUrl(ViewType.Monitor, customApplicationType, (int)ApmStatus.Summarize(item.Select(item1 => item1.Status), true).Value),
                            Components = item.Select(x =>
                                new
                                {
                                    Name = x.Name,
                                    Link = BaseResourceControl.GetViewLink(netObjectPrefix + ":" + x.ComponentID),
                                    Image = plugin.GetSmallIconUrl(ViewType.Monitor, customApplicationType, (int)x.Status.Value)
                                })
                        });
            }
        }
    }
}
