<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MockupApplicationDetails.aspx.cs" Inherits="Orion_APM_MockupApplicationDetails"%>
<%@ Import Namespace="SolarWinds.APM.Web"%>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/APM/Controls/MockupResourceContainerStub.ascx" %>
<%@ Register TagPrefix="orion" TagName="GlobalStylesheets" Src="~/Orion/Controls/GlobalStylesheets.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/resources/common/globalStyles.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Resources.css" />
    
    <link rel="stylesheet" type="text/css" href="/SolarWinds.css" />
    <link rel="stylesheet" type="text/css" href="/Events.css" />

    <orion:Include runat="server" File="APM/APM.css"/>
    <orion:Include runat="server" File="styles/Events.css"/>
		
    <title>        
    </title>
    
    <orion:GlobalStylesheets ID="GlobalStylesheets1" runat="server" />
    
    <style type="text/css">
        a, a:active, a:visited {
            color:black;
            text-decoration:none;
        }
        
        a:hover {
            color:#F99D1C;
            text-decoration:none;
        }
        
        table.ResourceContainer {
            width:80%;
        }
        
        body.GradientBackground {
            background-image: url(/Orion/images/bg_content_gradient.gif);
            background-repeat: repeat-x;
        }
    </style>
</head>
<body class="GradientBackground">
    <form id="form1" style="background-image: none;" runat="server">
       	<div id="container">
	    	<div id="content">
                <orion:ResourceContainer runat="server" ID="resContainer" />
            </div>
        </div>
    </form>
</body>
</html>