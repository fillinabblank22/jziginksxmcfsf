using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Models;
using SolarWinds.APM.Common;
using SolarWinds.Orion.Web;
using SolarWinds.APM.Web.Utility;
using SolarWinds.APM.Web.DAL;

public partial class Orion_APM_ApplicationDetails : ApmViewPage, INodeProvider, IApplicationProvider
{
    private static readonly Log log = new Log();

    private bool IsPreview
    {
        get
        {
            return (Request.QueryString["ViewId"] != null);
        }
    }

    ApplicationTemplateInfo template;
    private ApplicationTemplateInfo Template
	{
		get 
		{
            if (template == null)
            {
                var dal = new ApplicationTemplateDAL();
                template = dal.GetTemplateInfo(ApmApplication.TemplateId);
                // #FB113137 - duplicating view fix
                if (!IsPreview && template.HasIndividualView && ApmViewManager.IsOrphanedTemplate(template, this))
                {
                    template.ViewId = 0;

                    using (IAPMBusinessLayer businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
                    {
                        businessLayer.UpdateApplicationTemplateViewId(template.Id, template.ViewId);
                    }
                }
            }
			return template;
		}
	}

    protected override void OnInit(EventArgs e)
    {
        this.resHost.ApmApplication = (ApmApplication)NetObject;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        ApmAppStatusIcon.StatusValue = ApmApplication.Status;
        
        //incorrect url - hack
        if (!string.IsNullOrEmpty(ApmApplication.CustomType))
        {
            var url = WebPluginManager.Instance.GetViewLink(SolarWinds.APM.Web.Plugins.ViewType.Application, ApmApplication.CustomType, ApmApplication.Id);
            log.Warn(InvariantString.Format("Unexpected custom application type, redirecting to correct page: [url:{0}]", url));
            Response.Redirect(url);
        }

        ApmBaseResource.EnsureApmScripts();

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        this.Page.Title = this.ViewInfo.ViewTitle;
        base.OnLoad(e);
    }

	protected override string CustomizeViewHref 
	{
		get
		{
			if (Template.ViewId > 0 || Template.IsMockTemplate)
			{
                return HtmlHelper.GetDefaultCustomizeViewHref(ViewInfo.ViewID);
			}
			return "#";
		}
	}

	protected string CustomizeViewClientHandler
	{
		get
		{
			if (Template.ViewId > 0 || Template.IsMockTemplate) 
			{
				return "return true;";
			}
            return string.Format(
                    "return new SW.APM.Dashboard.CustomizeDialog({0}, {1}, \"{2}\").show();",
                    Template.Id,
                    JsHelper.Serialize(Server.HtmlEncode(Template.Name)),
                    HtmlHelper.GetDefaultCustomizeViewHref("{0}")
                );

            // I did change the ' for " to be able use JsHelper on template name. 
            // This is necessery to eliminate dangerous characters which can be present there. And serializer return value already 
		}
	}

    public override string NetObjectIDForASP
    {
        get
        {
            return this.resHost.ApmApplication.NPMNode.NetObjectID;
        }
    }

    public override string ViewType
    {
        get { return "APM Application Details"; }
    }

    public override void SelectView()
    {
        if (!VerifyNetObject() || !SelectViewByTemplate())
            base.SelectView();

        if (IsPreview)
        {
            Int32 viewId = Convert.ToInt32(Request.QueryString["ViewId"]);
            if ((ApmApplication.ViewId != 0) && (ApmApplication.ViewId != viewId))
            {
                // FB#4117 - wrong application and view were selected by Core code for specified VievID
                var data =
                    ApplicationDAL.GetAllApplicationsAsLazy(new Dictionary<string, object> { { "ViewId", viewId } });
                SolarWinds.APM.Common.Models.Application app = data.Count == 0 ? null : data[0];
                if (app == null)
                {
                    // no application for current view - leave application as is and just change view
                    ViewInfo vi = ViewManager.GetViewById(viewId);
                    if (vi != null)
                    {
                        this.ViewInfo = vi;
                    }
                    return;
                }
                ApmApplication application = new ApmApplication(app);

                NetObject = application;
                template = new ApplicationTemplateDAL().GetTemplateInfo(ApmApplication.TemplateId);

                SelectViewByTemplate();
            }
        }
    }

	private bool SelectViewByTemplate()
	{
		var tpl = Template;
        if (tpl.HasIndividualView)
        {
            ViewInfo vi = ViewManager.GetViewById(tpl.ViewId);
            if (vi != null)
            {
                this.ViewInfo = vi;
                return true;
            }
        }
        if (tpl.HasImportedView && tpl.HasIndividualView)
        {
            ApmViewManager.CreateTemplateViews(tpl, true);
            this.ViewInfo = ViewManager.GetViewById(tpl.ViewId);
            return true;
        }
	    return false;
	}

    protected override ViewInfo GetDefaultViewForViewType()
    {

        var v = ViewManager.GetViewById(ApmApplicationBase.DefaultViewID);
        return v;
    }

    public string GetApplicationInnerHtml()
    {
        return string.Format("<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(ApmApplication.NetObjectID), HttpUtility.HtmlEncode(ApmApplication.Name));
    }

    public string GetNodeInnerHtml()
    {
        if (ApmApplication.NPMNode == null)
        {
            return string.Empty;
        }

        return string.Format("<img src='{2}' alt='statusIcon' />&nbsp;<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(Node.NetObjectID), HttpUtility.HtmlEncode(Node.Name), ApmApplication.NPMNode.Status.ToString("smallimgpath", null));
    }

    #region INodeProvider Members

    public Node Node
    {
        get { return this.ApmApplication.NPMNode; }
    }

    #endregion

    #region IApplicationProvider Members

    public new ApmApplicationBase ApmApplication
    {
        get { return (ApmApplication)this.NetObject; }
    }

    #endregion

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}
