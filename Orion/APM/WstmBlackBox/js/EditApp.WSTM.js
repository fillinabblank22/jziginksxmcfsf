﻿
(function(appModel){
	'use strict'
	function postLoad() {
		$('#detailsViewRow').hide();    
		SW.APM.EditApp.Grid.setHiddenColumn('CategoryDisplayName', true, false);
	}

	appModel.loadToForm = function (){
		this.loadAppSettingsToForm();
		postLoad();
	};
}(Ext.namespace('SW.APM.EditApp.AppModel')));
