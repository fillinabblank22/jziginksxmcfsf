/*jslint browser: true, unparam: true*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.templates', function (templates, APMjs) {
    'use strict';

    // callback(value, meta, record)
    function renderName(value) {
        return APMjs.format('<img src="/Orion/APM/Images/BlackBox/scheduled_task.png" />&nbsp;<span class="searchable">{0}</span>', value);
    }

    // callback(value, meta, record)
    function renderCustomView() {
        return '';
    }

    templates.registerHandlerAssignWizard('ABTA', '../WstmBlackBox/Admin/AssignWizard/AssignSmartWstmApplication.aspx');
    templates.registerCustomTypeRenderers('ABTA', {
        'RenderName': renderName,
        'RenderCustomView': renderCustomView
    });
});
