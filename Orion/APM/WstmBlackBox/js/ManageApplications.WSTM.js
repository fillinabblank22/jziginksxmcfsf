/*jslint browser: true, indent: 4, nomen: true*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.apps', function (apps) {
    'use strict';

    /*jslint unparam: true*/
    // callback(value, meta, record)
    function rendererCondition(unusedValue, unusedMeta, record) {
        return record.data.CustomApplicationType === 'ABTA';
    }
    /*jslint unparam: false*/

    // callback(value, meta, record)
    function rendererAction() {
        return '';
    }

    function wstmAppPreRender() {
        apps.registerRenderer('RenderCustomView', {
            Condition: rendererCondition,
            Action: rendererAction
        });
    }

    apps.onPreRender = wstmAppPreRender;
});
