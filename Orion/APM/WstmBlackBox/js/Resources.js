﻿SW.APM = SW.APM || {};
SW.APM.WSTMBB = SW.APM.WSTMBB || {};
SW.APM.WSTMBB.Resources = SW.APM.WSTMBB.Resources || {};

SW.APM.WSTMBB.Resources.Tooltip = SW.APM.WSTMBB.Resources.Tooltip || {};
SW.APM.WSTMBB.Resources.Helpers = SW.APM.WSTMBB.Resources.Helpers || {};

/* TaskTooltip logic */

(function (tooltip) { 
    tooltip.showTooltip = function(resourceId, obj) {
        var position = $(obj).position();
        var wstmTaskTooltipId = String.format("#wstmTaskTooltipID_{0}", resourceId); 
        $(wstmTaskTooltipId).css({
            top: position.top + 14,
            left: position.left + $(obj).width() + 24,
            display: "block"
        });
        fillTooltipData(obj);
        $(wstmTaskTooltipId).delay(2000).animate({ opacity: 1.0 }, 0);
    };

    tooltip.hideTooltip = function() {
        $(".wstmTaskTooltip").css({ display: "none", opacity: 0.0 });
    };

    var fillTooltipData = function(obj) {
        var sourceClass = $(obj).closest(".[class^=wstmTaskInfo]").attr('class').split(' ')[0];
        var sourceSelector = String.format("tr.{0}", sourceClass) + " td{0}";
        var targetSelector = ".wstmTaskTooltip td{0}";

        var taskNameHtml = $(String.format(sourceSelector, ".wstmName")).attr("innerhtml");
        $(".wstmTaskTooltip div#wstmTooltipHeader").html(taskNameHtml);

        var taskStateHtml = $(String.format(sourceSelector, ".wstmState")).html();
        $(String.format(targetSelector, "#wstmTooltipState")).html(taskStateHtml);

        var taskAuthorHtml = $(String.format(sourceSelector, ".wstmAuthor")).html();
        $(String.format(targetSelector, "#wstmTooltipAuthor")).html(taskAuthorHtml);

        var creationTimeHtml = $(String.format(sourceSelector, ".wstmCreationTime")).html();
        $(String.format(targetSelector, "#wstmTooltipCreationTime")).html(creationTimeHtml);
    };
}(SW.APM.WSTMBB.Resources.Tooltip));

/* Auxiliary methods */

(function(helper) {
    helper.decimalToHex = function(d) {
        var hex = Number(d).toString(16).toUpperCase();
        hex = "0x" + hex;
        return hex;
    };

    helper.utcStrToLocalTime = function(dateStr) {
        var date = new Date(dateStr);
        if (isNaN(date)) return null;

        return helper.utcToLocalTime(date);
    };

    helper.utcToLocalTime = function(date) {
        date = new Date(date);

        var newDate = new Date(date.getTime());

        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();

        newDate.setHours(hours - offset);

        return newDate;
    };

    helper.getShortTaskName = function(resourceId, name) {
        var table = $(String.format("#Grid-{0}", resourceId));

        var width = $(table).width();
        var allowedWidth = (width - 40) * 0.55;
        
        return fitStringToSize(name, allowedWidth, "wstmName");
    };

    var fitStringToSize = function(str, len, className) {
        var result;
        str = str.toString();
        
        var span = document.createElement("span");
        span.className = className;
        span.style.visibility = 'hidden';
        span.style.padding = '0px';

        document.body.appendChild(span);

        var dots = "&#8230";
        span.innerHTML = dots;
        var dotsWidth = span.offsetWidth;

        span.innerHTML = str;
        if (span.offsetWidth > len) {
            span.innerHTML = str.substring(0, 10);
            for (var i = 10; i < str.length; i++) {
                span.innerHTML += str.charAt(i);
                if (span.offsetWidth + dotsWidth >= len) {
                    span.innerHTML = span.innerHTML.substring(0, i - 1);
                    break;
                }
            }
            result = span.innerHTML.trim() + dots;
        } else {
            result = str;
        }

        document.body.removeChild(span);

        return result;
    };
}(SW.APM.WSTMBB.Resources.Helpers));