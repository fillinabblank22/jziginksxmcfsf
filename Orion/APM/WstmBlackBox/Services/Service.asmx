﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Wstm.Web.Service" %>
using System.Web.Script.Services;
using System.Web.Services;

namespace SolarWinds.APM.BlackBox.Wstm.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class Service : Services.WstmBlackBoxWebService
    {
    }
}