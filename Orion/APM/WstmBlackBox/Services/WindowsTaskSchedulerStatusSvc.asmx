﻿<%@ WebService Language="C#" Class="SolarWinds.APM.BlackBox.Wstm.Web.WindowsTaskSchedulerStatusSvc" %>
using System;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.APM.BlackBox.Wstm.Web.DAL;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;

namespace SolarWinds.APM.BlackBox.Wstm.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class WindowsTaskSchedulerStatusSvc : WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetApplicationPollingData(string nodeId)
        {
            return new WindowsTaskSchedulerStatusDAL().GetApplicationPollingData(Int32.Parse(nodeId));
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetWinTaskResultStyle(string resultCode, bool hasLastRunTime)
        {
            var status = ScheduledTasksHelper.GetTaskStatus(UInt32.Parse(resultCode));
            if (UInt32.Parse(resultCode) == 1 && hasLastRunTime)
                return String.Empty;
            switch (status)
            {
                case(Status.NotAvailable):
                    return "wstmFiledLastRunResult";
                default:
                    return String.Empty;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
        public object GetWinTaskResultIcon(string resultCode, bool hasLastRunTime)
        {
            var status = ScheduledTasksHelper.GetTaskStatus(UInt32.Parse(resultCode));
            if (UInt32.Parse(resultCode) == 1 && hasLastRunTime)
                return "result_succeeded.png";
            switch (status)
            {
                case (Status.NotAvailable):
                    return "result_failed.png";
                case (Status.Available):
                    return "result_succeeded.png";
                default:
                    return "result_unknown.png";
            }
        }
    }
}