﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationSettings.ascx.cs" Inherits="Orion_APM_WstmBlackBox_Controls_EditApplicationSettings" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.Wstm.Common" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="TestButtonSummary" Src="~/Orion/APM/Controls/TestButtonSummary.ascx" %>

<script type="text/javascript">
    (function () {

        function getTestSettingsFromUi(testingNodeId) {
            var result = {
                NodeId: testingNodeId,
                AppType: '<%= Constants.ApplicationTemplateCustomType%>',
                CredentialSet: {
                    Id: $('#appEditWstmCredentials table[class="selectCreds"] select[id$="CredentialSetList"]').val(),
                    Login: $('.selectCreds input[id$=selectWstmCredentials_UserName]').val(),
                    Password: $('.selectCreds input[id$=selectWstmCredentials_Password]').val()
                },
                CustomSettings: {}
            };

            SW.APM.EditApp.updateCommonTestSettings(result);

            return { settings: result };
        }

        Ext.namespace('SW.APM.BB.WSTM');

        SW.APM.BB.WSTM.CredentialsEditor = function () {
            var settingLevelInstance = 2; // SettingLevel.Instance enumeration
            var credSetIdElSelector = "#appEditWstmCredentials select[id$=CredentialSetList]";

            function findSetting(settingPropertyName, settingsCollection) {
                var foundSetting = null;
                Ext.each(settingsCollection, function (sett) {
                    if (sett.Key == settingPropertyName) {
                        foundSetting = sett;
                        return false;
                    }
                });
                return foundSetting;
            }

            function createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError) {
                var jqSelCredTable = $('#appEditWstmCredentials table[class="selectCreds"]');
                var newCredentials = {
                    name: jqSelCredTable.find("input[id$=selectWstmCredentials_CredentialName]").val(),
                    userName: jqSelCredTable.find("input[id$=selectWstmCredentials_UserName]").val(),
                    psw: jqSelCredTable.find("input[id$=selectWstmCredentials_Password]").val()
                };
                var onCredentialsCreated = function (credentialSetId) {
                    credentialSetIdSetting.Value = credentialSetId;
                    setTimeout(function () {
                        SW.APM.SelectCredentials.ReloadCredentialSets(credSetIdElSelector);
                    }, 500);
                    onSuccess();
                };
                SW.Core.Services.callWebService("/Orion/APM/Services/Credentials.asmx", "CreateCredential", newCredentials, onCredentialsCreated, onError);
            }

            this.initUiState = function (item) {
                // show the dialog 
                $('#appEditWstmCredentials').show();

                // and bind data

                var foundSetting = findSetting("<%= Constants.CredentialSetIdKey%>", item.Settings);
                if (foundSetting) {
                    $(credSetIdElSelector)
                        .val(foundSetting.Value)
                        .trigger('change');

                    // remember initial value
                    $('#appEditWstmCredentials').data('initialCredSetId', foundSetting.Value);
                }
                
                var testButtonHandler = new SW.APM.BB.WSTM.TestButtonHandler(item.NodeId, '<%= testSummary.ClientID %>');
                $('#<%= testButton.ClientID %>')
                    .unbind('click')
                    .click(testButtonHandler.onClick);
            };

            this.updateModelFromUi = function (item) {
                var foundSetting = findSetting("<%=  Constants.CredentialSetIdKey%>", item.Settings);
                if (foundSetting) {
                    // override template level to instance
                    foundSetting.SettingLevel = settingLevelInstance;
                    foundSetting.Value = $(credSetIdElSelector).val();
                }
            };

            this.asyncPostProcessing = function (app, template, onSuccess, onError) {
                var connectionTestSucceeded = false;
                var credentialSetIdSetting = findSetting("<%= Constants.CredentialSetIdKey %>", app.Settings);

                if (credentialSetIdSetting) {

                    var testSett = getTestSettingsFromUi(app.NodeId);
                    var isNewCredAssigned = testSett.settings.CredentialSet.Id == SW.APM.SelectCredentials.NewCredentialId;

                    // test connection for new credinitialCredSetIds 
                    if (isNewCredAssigned || $('#appEditWstmCredentials').data('initialCredSetId') != credentialSetIdSetting.Value) {
                        SW.Core.Services.callWebServiceSync("/Orion/APM/WstmBlackBox/Services/Service.asmx", "TestWstmBlackBoxConnection",
                            testSett,
                            function (result) {
                                
                                connectionTestSucceeded = result.TestSuccessfull && result.Result.IsValidCredential;
                            },
                            function () {
                                // test failed
                                connectionTestSucceeded = false;
                            }
                        );
                    }

                    // display question whether to save failing creds
                    if (!connectionTestSucceeded && (isNewCredAssigned || $('#appEditWstmCredentials').data('initialCredSetId') != credentialSetIdSetting.Value)) {

                        Ext.Msg.show({
                            title: '<%= Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Title %>',
                            msg: '<%= Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Msg %>',
                            buttons: Ext.Msg.OKCANCEL,
                            fn: function (btn) {
                                if (btn == 'ok') {
                                    if (isNewCredAssigned) {
                                        createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                                    } else {
                                        onSuccess();
                                    }
                                } else {
                                    onError();
                                }
                            },
                            animEl: 'elId',
                            icon: Ext.MessageBox.WARNING
                        });

                        return true;
                    }

                    // create new creds /*credentialSetIdSetting.Value == SW.APM.SelectCredentials.NewCredentialId*/
                    if (isNewCredAssigned) {
                        createNewCredentialsAsync(credentialSetIdSetting, onSuccess, onError);
                        return true;
                    }
                }
                return false;
            };
        };

        Ext.ComponentMgr.registerPlugin('SW.APM.BB.WSTM.CredentialsEditor', SW.APM.BB.WSTM.CredentialsEditor);
        
        //TODO: retest in IE (which versions?)
        SW.APM.BB.WSTM.TestButtonHandler = function (nodeId, summaryId) {
            var customMessageCls = 'customMsg';
            var jqSummary = $('#' + summaryId);

            function callTestBlackBoxConnection(settings, onSuccess, onError) {
                SW.Core.Services.callWebService("/Orion/APM/WstmBlackBox/Services/Service.asmx", "TestWstmBlackBoxConnection", settings, onSuccess, onError);
            }

            function displayTestingSummary(testSuccessfull, msg) {
                //TODO: move close to TestButtonSummary.ascx to allow reuse
                var jqCustomMessageSpan = null;
                jqSummary.find('.' + customMessageCls).remove();
                if (msg != null && msg != "")
                    jqCustomMessageSpan = $('<span />').addClass(customMessageCls).html(msg);
                if (testSuccessfull) {
                    jqSummary.find('.testSuccessful').show().append(jqCustomMessageSpan);
                    jqSummary.find('.testFailed').hide();
                    jqSummary.find('.progress').hide();
                } else {
                    jqSummary.find('.testSuccessful').hide();
                    jqSummary.find('.testFailed').show().append(jqCustomMessageSpan);
                    jqSummary.find('.progress').hide();
                }
            }

            function displayProgress() {
                jqSummary.find('.testSuccessful').hide();
                jqSummary.find('.testFailed').hide();
                if (!jqSummary.is(':visible'))
                    jqSummary.slideDown();
                jqSummary.find('.progress').show();
            }

            this.onClick = function () {
                var testSett = getTestSettingsFromUi(nodeId);
                if ($('#appEditWstmCredentials').closest("form").valid()) {
                    displayProgress(jqSummary);
                    callTestBlackBoxConnection(
                        testSett,
                        function (result) {
                            var testSuccessfull = result.TestSuccessfull && result.Result.IsValidCredential;
                            displayTestingSummary(testSuccessfull, '');
                        },
                        function () {
                            displayTestingSummary(false, "@{R=APM.Strings;K=APMWEBJS_ErrorWhileTestingConnection;E=js}");
                        }
                    );
                    }
                return false;
            };
        };
    }());
</script>

<style>
    #appEditWstmCredentials .testButtonRow, #appEditWstmCredentials .testRow {
        text-align: center;
    }

    #appEditWstmCredentials .testRow div {
        height: 20px;
        margin-bottom: 5px;
        padding-top: 4px;
    }

    #appEditWstmCredentials .testRow div img {
        height: 16px;
        vertical-align: bottom;
        margin-right: 5px;
    }

    #appEditWstmCredentials .testRow div.testSuccessful {
        background-color: #D6F6C6;
    }

    #appEditWstmCredentials .testRow div.testFailed {
        background-color: #FEEE90;
        color: Red;
    }
</style>

<table id="appEditWstmCredentials" class="appProperties" style="display: none" cellspacing="0">
    <tr>
        <td class="label"><asp:Literal ID="wstmCredentialsLabel" runat="server" /></td>
        <td>
            <table class="selectCreds" style="width: 430px">
                <apm:SelectCredentials runat="server" ID="selectWstmCredentials" 
                    ValidationGroupName="SelectCredentialsValidationGroup" 
                    AllowNodeWmiCredential="True" />
                <tr>
                    <td colspan="2" class="testRow">
                        <apm:TestButtonSummary runat="server" ID="testSummary" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="testButtonRow">
                        <orion:LocalizableButton runat="server" ID="testButton" LocalizedText="Test" DisplayType="Small" />
                    </td>
                </tr>
                <tr><td colspan="2" >
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SelectCredentialsValidationGroup"/>
                </td></tr>
            </table>
        </td>
    </tr>
</table>

