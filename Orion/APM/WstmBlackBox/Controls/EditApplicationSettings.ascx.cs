﻿using System;
using System.Web.UI;

public partial class Orion_APM_WstmBlackBox_Controls_EditApplicationSettings : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        wstmCredentialsLabel.Text = Resources.APM_WstmBBContent.EditApplicationSettings_InstanceCredentials;
    }
}