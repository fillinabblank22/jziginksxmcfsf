﻿using System;
using SolarWinds.APM.BlackBox.Wstm.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;

public partial class Orion_APM_Admin_SmartWstmApplication_Finish : WizardPage<AssignSmartWstmApplicationWorkflow>
{
    protected string TemplateName
    {
        get
        {
            return Workflow.SelectedTemplate.Name;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        finishedMessage.DataSource = Workflow.CreatedApplications;
        finishedMessage.DataBind();

        if (!IsPostBack) 
            imgbDone.AddEnterHandler(0);
    }

    protected void OnDone(object sender, EventArgs e)
    {
        ResetSession();
        Response.Redirect("~/Orion/Apm/Summary.aspx", true);
    }

}