﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.APM.BlackBox.Wstm.Common;
using SolarWinds.APM.BlackBox.Wstm.Common.Models;
using SolarWinds.APM.BlackBox.Wstm.Web.DAL;
using SolarWinds.APM.BlackBox.Wstm.Web.UI;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;

public partial class Orion_APM_Admin_SmartWstmApplication_AssignSmartWstmApplication : WizardPage<AssignSmartWstmApplicationWorkflow>, IPostBackEventHandler
{
    #region Fields and Properties

    private static readonly Log log = new Log();
    
    private SelectNodeTreeItem selectedNode;
    private SelectNodeTreeItem SelectedNode
    {
        get
        {
            if (selectedNode == null)
            {
                selectedNode = new SelectNodeTreeItem(serverIP.SelectedNode);
                var selectedCredSetId = selectCredentials.SelectedCredentialSetId;
                if (selectedCredSetId != ComponentBase.NewCredentialsId)
                {
                    selectedNode.CredentialId = selectedCredSetId;
                }
            }

            return selectedNode;
        }
    }

    #endregion

    #region Events handlers

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Workflow.SelectedNodesInfo != null && Workflow.SelectedNodesInfo.Count > 0)
            {
                if (Workflow.SelectedNodesInfo.Count > 1)
                {
                    log.Debug("Found more than one nodes in workflow state, using just first, because the wizard can assign only one at a time, ignoring the rest.");
                }
                var nodeInfo  = Workflow.SelectedNodesInfo[0];
                serverIP.RestoreSelectedNodeById(nodeInfo.Id);
            }
            selectCredentials.ReloadCredentialSets();
        }
        selectCredentials.ValidationSummaryParentClientID = CredentialsValidationSummary.ClientID;

        if (!IsPostBack)
        {
            string postData = Server.UrlDecode(Request.Form["postData"]);
            if (postData != null)
            {
                Workflow.HasStarted = true;

                var serializer = new JavaScriptSerializer();
                var templateId = serializer.Deserialize<int>(postData);
                Workflow.SelectedTemplate = new TemplateInfo(templateId);
            }
			else 
			{
				Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
			}
			imgbNext.AddEnterHandler(0);
        }
    }

    /// <summary>
    /// Triggered when user presses Test button.
    /// </summary>
    protected void OnTest(object sender, EventArgs e)
    {
        bool? testSucceeded = null;

        Validate(PageValidationSummary.ValidationGroup);
        if (!Page.IsValid)
        {
            MoveInvalidValidators(PageValidationSummary.ValidationGroup, CredentialsValidationSummary.ValidationGroup);
        }
        else
        {
            Validate(selectCredentials.ValidationGroupName);
            if (selectCredentials.IsValid)
            {
                testSucceeded = TestBlackBoxWstmConnection();
            }
        }

        SetUiTestedCredentialControls(testSucceeded);
    }

    #endregion

    #region Protected methods

    /// <summary>
    /// Processes entered data from current wizard page and switches to the next page.
    /// </summary>
    protected void OnNext()
    {
        Validate(PageValidationSummary.ValidationGroup);
        Validate(selectCredentials.ValidationGroupName);

        if (Page.IsValid && selectCredentials.IsValid)
        {
            WstmTestConnectionResult testResult;
            if (TestBlackBoxWstmConnection(out testResult))
            {
                CreateApplication(testResult);
                GotoNextPage();
            }
            else
            {
                AddErrorMessage(Resources.APM_WstmBBContent.AssignWizard_InvalidCredentialsMessage,
                                selectCredentials.ValidationGroupName);
                SetUiTestedCredentialControls(null);
            }
        }
    }

    /// <summary>
    /// Triggered when user presses Cancel button.
    /// </summary>
    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    /// <summary>
    /// Cancels the wizard.
    /// </summary>
    protected override void CancelWizard()
    {
        base.CancelWizard();
        Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx", true);
    }

    #endregion

    #region Privete methods

    /// <summary>
    /// Tests Wstm connection using filled values and returns true when the test was successful.
    /// </summary>
    private bool TestBlackBoxWstmConnection()
    {
        WstmTestConnectionResult testResult;
        return TestBlackBoxWstmConnection(out testResult);
    }

    /// <summary>
    /// Tests Wstm connection using filled values and returns true when the test was successful.
    /// </summary>
    /// <param name="testResult">Out parameter contains details about credentials connection test</param>
    private bool TestBlackBoxWstmConnection(out WstmTestConnectionResult testResult)
    {
        var retval = false;
        testResult = null;
        
        using (var bl = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            var appSettings = new Settings(credentialSetId: SelectedNode.CredentialId);

            BusinessLayerTaskInfo<string> result = null;
            try
            {
                result = bl.TestBlackBoxConnection(
                    SelectedNode.Id,
                    selectCredentials.SelectedCredentialSet,
                    ApplicationSettings.DefaultForBlackBoxAssignTemplateWizards,
                    Constants.ApplicationTemplateCustomType,
                    appSettings.ToXmlString());
            }
            catch (Exception)
            {
                AddErrorMessage(APM_WstmBBContent.Orion_APM_Admin_SmartWstmApplication_AssignSmartWstmApplication_TestBlackBoxWstmConnection_Error_while_testing_Wstm_BB_connection, selectCredentials.ValidationGroupName);
            }

            if (result != null)
            {
                if (result.Outcome == Status.Available && result.Result != null)
                {
                    testResult = WstmTestConnectionResult.FromXmlString(result.Result);
                    retval = testResult != null && testResult.IsValidCredential;
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        AddErrorMessage(error.Message, selectCredentials.ValidationGroupName);
                    }
                }
            }
        }

        return retval;
    }

    /// <summary>
    /// Adds error message within validator messages
    /// </summary>
    private void AddErrorMessage(string message, string validationGroupName)
    {
        var err = new CustomValidator
        {
            ValidationGroup = validationGroupName,
            IsValid = false,
            ErrorMessage = message
        };
        Page.Validators.Add(err);
    }

    /// <summary>
    /// Creates Wstm BB application after the connection test using filled values.
    /// </summary>
    private void CreateApplication(WstmTestConnectionResult testResult)
    {
        Workflow.SelectedNodesInfo = new List<SelectNodeTreeItem> { SelectedNode };
        Workflow.TestConnectionResult = testResult;

        if (selectCredentials.SelectedCredentialSetId == ComponentBase.NewCredentialsId)
        {
            Workflow.UseCredentials(selectCredentials.SelectedCredentialSet);
        }
        else if (selectCredentials.SelectedCredentialSetId == ComponentBase.NodeWmiCredentialsId)
        {
            Workflow.InheritCredentials();
        }
        else
        {
            Workflow.UseCredentials(new List<SelectNodeTreeItem> { SelectedNode });
        }

        Workflow.CreateApplications();
    }

    /// <summary>
    /// Moves the invalid validators from one group to another.
    /// </summary>
    private void MoveInvalidValidators(string sourceValidationGroup, string targetValidationGroup)
    {
        var invalidValidators = GetValidators(sourceValidationGroup).OfType<IValidator>().Where(v => !v.IsValid);
        foreach (var val in invalidValidators)
        {
            Page.Validators.Remove(val);
            AddErrorMessage(val.ErrorMessage, targetValidationGroup);
        }
    }

    /// <summary>
    /// Sets credential controls according to connection test result.
    /// </summary>
    private void SetUiTestedCredentialControls(bool? testSucceeded)
    {
        if (testSucceeded.HasValue)
        {
            testSuccessful.Visible = testSucceeded.Value;
            testFailed.Visible = !testSucceeded.Value;
        }
        else
        {
            testSuccessful.Visible = false;
            testFailed.Visible = false;
        }

        selectCredentials.PreFillPasswords();
    }

    #endregion

    #region IPostBackEventHandler implementation

    /// <summary>
    /// Enables a server control to process an event raised when a form is posted to the server.
    /// </summary>
    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Next":
                OnNext();
                break;
        }
    }

    #endregion

    #region Web methods

    /// <summary>
    /// Check whether Task Scheduler application monitor already assigned to specified node.
    /// </summary>
    [WebMethod(EnableSession = true)]
    public static bool InstanceExists(int nodeId, string ipAddress)
    {
        Node node = GetNode(nodeId, ipAddress);

        if (node == null)
            return false;

        return ServiceLocatorForWeb.GetServiceForWeb<IWstmApplicationDAL>().ApplicationExists(node.Id);
    }

    #endregion
}
