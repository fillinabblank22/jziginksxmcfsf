﻿<%@ Page Title="Assign Application Monitor" Language="C#" MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="AssignSmartWstmApplication.aspx.cs" Inherits="Orion_APM_Admin_SmartWstmApplication_AssignSmartWstmApplication" %>
<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="apm" TagName="CredsTip" Src="~/Orion/APM/Controls/CredentialTips.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectServerIp" Src="~/Orion/APM/Controls/SelectServerIp.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include ID="Include1" runat="server" File="Nodes/js/TimeoutHandling.js" />
    <orion:Include ID="Include2" runat="server" File="APM/WstmBlackBox/Styles/AssignWizard.css"/>
    
    <script type="text/javascript">
         function HideTestResults() {
             $(".testSuccessful").hide();
             $(".testFailed").hide();
         }
         
         function TestDuplicateInstance() {
             $.ajax({
                 type: "POST",
                 url: "AssignSmartWstmApplication.aspx/InstanceExists",
                 data: Ext.encode({ nodeId: window.$get('<%= serverIP.ClientID %>' + '_selectedNodeId').value, ipAddress: window.$get('<%= serverIP.ClientID %>' + '_targetServer').value }),
                 contentType: 'application/json; charset=utf-8',
                 dataType: "json",
                 success: function (dataResponse) {
                     if (!dataResponse.d) {
                         Ext.MessageBox.hide();
                         <%= Page.ClientScript.GetPostBackEventReference(this, "Next") %>;
                     } else {
                         Ext.Msg.show({
                             msg: "<%=Resources.APM_WstmBBContent.AssignWizard_DuplicateFound%>",
                             buttons: Ext.Msg.OK,
                             icon: Ext.Msg.INFO
                         });
                     }
                 },
                 error: function (response) {
                     Ext.Msg.alert("<%=Resources.APM_WstmBBContent.AssignWizard_DuplicateCheckError%>", response.statusText);
                 }
             });
         }

         function NextClick() {
             TestDuplicateInstance();
             return false;
         }
    </script>

    <h2><%=  InvariantString.Format(Resources.APM_WstmBBContent.AssignWizard_MonitorTitle, Workflow.SelectedTemplate.Name)%></h2>
    <p><%= InvariantString.Format(Resources.APM_WstmBBContent.AssignWizard_MonitorDescription, Workflow.SelectedTemplate.Name)%></p>
    <br />
    <p class="note">
        <%= Resources.APMWebContent.APMWEBDATA_VB1_242 %>
        <a id="addLink" href="/Orion/Nodes/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_243 %></a>
    </p>
    <div id="selectInstanceStep">
        <ul id="selectInstanceForm">
            <li>
                <asp:ValidationSummary ID="PageValidationSummary" runat="server" />
            </li>
            <li>
                <label><%=  Resources.APMWebContent.APMWEBDATA_AK1_117 %></label>
                    <apm:SelectServerIp ID="serverIP" runat="server" ValidateVmware="false" CssClass="apm_Target" />
            </li>
            <li>
                <label><%=  Resources.APM_WstmBBContent.EditApplicationSettings_InstanceCredentials %></label>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional" Class="credentialsContainer ie7addLayout">
                    <ContentTemplate>            
                    <table class="selectCreds">
                        <apm:SelectCredentials runat="server" ID="selectCredentials" 
                            ValidationGroupName="SelectCredentialsValidationGroup" 
                            AllowNodeWmiCredential="True" />
                        <tr>
                            <td colspan="2" class="testRow">
                                <div runat="server" id="testSuccessful" class="testSuccessful" visible="false">
                                    <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_55 %>" />
                                </div>
                                <div runat="server" id="testFailed" class="testFailed" visible="false">
                                    <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_56 %>" />
                                </div>
                                <asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="true" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources: APMWebContent, ApmWeb_TestingProgress %>" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="testButtonRow">
                                <orion:LocalizableButton runat="server" ID="TestButton" LocalizedText="Test" DisplayType="Small" OnClick="OnTest" OnClientClick="HideTestResults();" />
                            </td>
                        </tr>
                        <tr><td colspan="2">
                            <asp:ValidationSummary ID="CredentialsValidationSummary" runat="server" ValidationGroup="SelectCredentialsValidationGroup"/>
                        </td></tr>
                    </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </li>
        </ul>
        <div class="credentialTips">
            <apm:CredsTip ID="ctrCredsTip" runat="server"/>  
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APM_WstmBBContent, AssignWizard_ApplicationMonitorButton %>" DisplayType="Primary" OnClientClick="NextClick(); return false;" />
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false" />
    </div>   
</asp:Content>

