using System;
using System.Web.UI;
using SolarWinds.APM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_CustomChart : Page
{

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PageHeader.InnerHtml = GeneratePageTitle();
        }
    }

    private string GeneratePageTitle()
    {
        string title = Resources.APMWebContent.APMWEBDATA_VB1_65;

        NetObject netObject = CustomChart.ChartInfo.NetObject;

        if (netObject is ApmApplication)
        {
            ApmApplication app = (ApmApplication)netObject;

            title += String.Format(Resources.APMWebContent.APMWEBCODE_VB1_23,
                            NetObjectLink(app),
                            NetObjectLink(app.NPMNode));
        }
        else if (netObject is ApmMonitor)
        {
            ApmMonitor monitor = (ApmMonitor) netObject;

            title += String.Format(Resources.APMWebContent.APMWEBCODE_VB1_24, 
                            NetObjectLink(monitor), 
                            NetObjectLink(monitor.Application), 
                            NetObjectLink(monitor.Application.NPMNode) );
        }

        return title;
    }

    private static string NetObjectLink(NetObject netObject)
    {
        return String.Format("<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(netObject.NetObjectID), netObject.Name);
    }
}

