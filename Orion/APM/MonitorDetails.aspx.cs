using System;
using System.Linq;
using System.Web;
using SolarWinds.APM.Common.Utility;
using SolarWinds.APM.Web.Plugins;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.APM.Web;

public partial class Orion_APM_MonitorDetails : ApmViewPage, IMonitorProvider
{
    private static readonly Log log = new Log();

    private string monitorUrl;

    protected override void OnInit(EventArgs e)
    {
        this.resHost.Monitor = Monitor;

        var navigationInfo = new ComponentNavigationInfo
        {
            ApplicationID = Monitor.Application.Id,
            ApplicationItemID = Monitor.BaseComponent.ApplicationItemID,
            ComponentID = Monitor.BaseComponent.Id,
            ComponentType = Monitor.BaseComponent.Type,
            CustomApplicationType = Monitor.Application.CustomType
        };
        this.monitorUrl = WebPluginManager.Instance.GetComponentViewLink(navigationInfo);

        if (!string.IsNullOrEmpty(Monitor.Application.CustomType) && WebPluginManager.Instance.HasCustomComponentView(navigationInfo) && !string.IsNullOrEmpty(this.monitorUrl))
        {
            log.Warn(InvariantString.Format("Invalid url detected, redirecting [newUrl:{0}]", this.monitorUrl));
            Response.Redirect(this.monitorUrl);
        }

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        ApmStatusIcon.StatusValue = Monitor.Status;
        ApmStatusIcon.ErrorValue = Monitor.CurrentError;

        base.OnInit(e);
    }

    public string GetApplicationInnerHtml()
    {
        return string.Format("<img src='{2}' alt='statusIcon' />&nbsp;<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(ApmApplicationBase.GetNetObjectId(ApmApplication.Id, ApmApplication.CustomType)), HttpUtility.HtmlEncode(ApmApplication.Name), ApmApplication.Status.ToString("smallappimgpath", ApmApplication.CustomType));
    }

    public string GetNodeInnerHtml()
    {
        if (ApmApplication.NPMNode == null)
        {
            return string.Empty;
        }

        return string.Format("<img src='{2}' alt='statusIcon' />&nbsp;<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(Node.NetObjectID), HttpUtility.HtmlEncode(Node.Name), ApmApplication.NPMNode.Status.ToString("smallimgpath", null));
    }

    public string GetMonitorInnerHtml()
    {
        return string.Format("<a href='{0}'>{1}</a>", this.monitorUrl, HttpUtility.HtmlEncode(Monitor.Name));
    }

    public override string ViewType
    {
        get { return "APM Monitor Details"; }
    }

    protected override ViewInfo GetDefaultViewForViewType()
    {
        ViewInfoCollection views = ViewManager.GetViewsByType(this.ViewType);
        return views.FirstOrDefault(view => view.IsSystem);
    }

    #region IMonitorProvider Members

    public ApmMonitor Monitor
    {
        get { return (ApmMonitor)this.NetObject; }
    }

    #endregion

    #region IApplicationProvider Members

    public new ApmApplicationBase ApmApplication
    {
        get { return this.Monitor.Application; }
    }

    #endregion

    #region INodeProvider Members

    public SolarWinds.Orion.NPM.Web.Node Node
    {
        get { return this.ApmApplication.NPMNode; }
    }

    #endregion

	#region IDynamicInfoProvider
	public object GetValue(string key)
	{
		return this.GetDynamicInfoValue(key);
	}
	#endregion
}
