﻿/* 
 * This file is more or less copy&paste of Orion\Core\Src\Web\Orion\js\MaintenanceMode\MaintenanceScheduler.js
 */
(function(MaintenanceMode) {

    var maintenanceScheduler = (function() {
        var onScheduleAlertSuppression = function(entityUri,
            schedule,
            onFinishedCallback,
            scheduleSuccessCallback,
            scheduleFailCallback) {
            SW.Core.MaintenanceMode.SuppressAlerts(
                [entityUri],
                schedule.from,
                schedule.until,
                function() {
                    scheduleSuccessCallback();
                    onFinishedCallback();
                },
                function() {
                    scheduleFailCallback("@{R=Core.Strings.2;K=MaintenanceScheduler_FailedToScheduleMaintenance;E=js}");
                });
        };

        var onScheduleMultipleAlertSuppressions = function(entityUris, schedule, onFinishedCallback) {
            SW.Core.MaintenanceMode.ScheduleAlertSuppression(
                entityUris,
                schedule.from,
                schedule.until,
                onFinishedCallback);
        };

        var onScheduleUnmanage = function(netObjectIds,
            schedule,
            onFinishedCallback,
            scheduleSuccessCallback,
            scheduleFailCallback) {
            SW.APM.MaintenanceMode.UnmanageNetObjects(
                netObjectIds,
                schedule.from,
                schedule.until,
                function() {
                    scheduleSuccessCallback();
                    onFinishedCallback();
                },
                function() {
                    scheduleFailCallback("@{R=Core.Strings.2;K=MaintenanceScheduler_FailedToScheduleMaintenance;E=js}");
                });
        };

        var onScheduleUnmanageMultipleEntities = function(netObjectIds, schedule, onFinishedCallback) {
            SW.APM.MaintenanceMode.ScheduleUnmanage(
                netObjectIds,
                schedule.from,
                schedule.until,
                onFinishedCallback);
        };

        var showDialogForSingleEntity = function(entityUri, netObjectId, entityCaption, onFinishedCallback) {

            var promiseSchedules = SW.Core.MaintenanceMode.MaintenanceScheduler.GetScheduleForEntity(entityUri);

            if (!onFinishedCallback) {
                onFinishedCallback = function() {
                    window.location.reload(true);
                }
            }

            var config = {
                title: entityCaption,
                schedules: promiseSchedules,
                onSchedule: function(schedule, scheduleSuccessCallback, scheduleFailCallback) {

                    if (schedule.key === "alertSuppression") {
                        onScheduleAlertSuppression(entityUri,
                            schedule,
                            onFinishedCallback,
                            scheduleSuccessCallback,
                            scheduleFailCallback);

                    } else if (schedule.key === "unmanage") {
                        var netObjectIds = [netObjectId];
                        onScheduleUnmanage(netObjectIds,
                            schedule,
                            onFinishedCallback,
                            scheduleSuccessCallback,
                            scheduleFailCallback);
                    };
                }
            };

            SW.Core.MaintenanceMode.MaintenanceSchedulerDialog.show(config);
        };

        var getDialogTitle = function(netObjects) {
            var length = netObjects.Ids.length;
            return String.format(length === 1
                ? "@{R=APM.Strings;K=MaintenanceMode_ScheduleTitle_Single;E=js}"
                : "@{R=APM.Strings;K=MaintenanceMode_ScheduleTitle_Multiple;E=js}",
                length);
        };

        var showDialogForMultipleEntities = function(netObjects, onFinishedCallback) {

            var config = {
                title: getDialogTitle(netObjects),
                schedules: [],
                onSchedule: function(schedule, scheduleSuccessCallback) {

                    if (schedule.key === "alertSuppression") {
                        // Close the scheduler dialog
                        scheduleSuccessCallback();

                        // Show progress dialog and apply changes
                        onScheduleMultipleAlertSuppressions(netObjects.Uris,
                            schedule,
                            onFinishedCallback);

                    } else if (schedule.key === "unmanage") {
                        // Close the scheduler dialog
                        scheduleSuccessCallback();

                        // Show progress dialog and apply changes
                        onScheduleUnmanageMultipleEntities(netObjects.Ids,
                            schedule,
                            onFinishedCallback);
                    };
                }
            };

            SW.Core.MaintenanceMode.MaintenanceSchedulerDialog.show(config);
        };

        // Export
        return {
            ShowDialogForSingleEntity: showDialogForSingleEntity,
            ShowDialogForMultipleEntities: showDialogForMultipleEntities
        }
    }());

    MaintenanceMode.MaintenanceScheduler = maintenanceScheduler;

}(SW.Core.namespace("SW.APM.MaintenanceMode")));