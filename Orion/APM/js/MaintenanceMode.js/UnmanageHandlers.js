﻿/* 
 * This file is more or less copy&paste of Orion\Core\Src\Web\Orion\js\MaintenanceMode\UnmanageHandlers.js
 */
(function(MaintenanceMode) {
    MaintenanceMode.UnmanageNowSingleEntity = function(netObjectId) {
        SW.APM.MaintenanceMode.ScheduleUnmanage([netObjectId],
            null,
            null,
            function(numSucceeded) {
                if (numSucceeded > 0) {
                    window.location.reload(true);
                }
            });
    };

    MaintenanceMode.ManageAgainSingleEntity = function(netObjectId) {
        SW.APM.MaintenanceMode.ManageAgain([netObjectId],
            function(numSucceeded) {
                if (numSucceeded > 0) {
                    window.location.reload(true);
                }
            });
    };

    /* Apply unmanage on netObjects and show progress in dialog */
    MaintenanceMode.ScheduleUnmanage = function(netObjectIds, from, until, onFinishedCallback) {
        var bulkSize = SW.Core.MaintenanceMode.BulkSize;
        var bulks = SW.Core.MaintenanceMode.GetBulksFromArray(netObjectIds, bulkSize);

        var unmanageFunction = function(netObjectIds, successCallback, failureCallback) {
            SW.APM.MaintenanceMode.UnmanageNetObjects(netObjectIds,
                from,
                until,
                successCallback,
                failureCallback);
        }
        SW.Core.MessageBox.ProgressDialog({
            title: "@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow;E=js}",
            items: bulks,
            bulkSize: bulkSize,
            totalItems: netObjectIds.length,
            serverMethod: unmanageFunction,
            getArg: function(id) { return id },
            sucessMessage: "@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow_Success;E=js}",
            failMessage: "@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow_Failed;E=js}",
            showOnlySummary: bulks.length > 1,
            afterFinished: onFinishedCallback
        });
        return false;
    };

    /* Manage netObjects again and show progress in dialog */
    MaintenanceMode.ManageAgain = function(netObjectIds, onFinishedCallback, serverMethodSuccessCallback) {
        var bulkSize = SW.Core.MaintenanceMode.BulkSize;
        var bulks = SW.Core.MaintenanceMode.GetBulksFromArray(netObjectIds, bulkSize);

        var remanageFunction = function(netObjectIds, successCallback, failureCallback) {
            SW.APM.MaintenanceMode.RemanageNetObjects(netObjectIds,
                function(result) {
                    if (serverMethodSuccessCallback) {
                        serverMethodSuccessCallback(result);
                    }
                    successCallback(result);
                },
                failureCallback);
        }
        SW.Core.MessageBox.ProgressDialog({
            title: "@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain;E=js}",
            items: bulks,
            bulkSize: bulkSize,
            totalItems: netObjectIds.length,
            serverMethod: remanageFunction,
            getArg: function(id) { return id },
            sucessMessage: "@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain_Success;E=js}",
            failMessage: "@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain_Failed;E=js}",
            showOnlySummary: bulks.length > 1,
            afterFinished: onFinishedCallback
        });
        return false;
    };

    MaintenanceMode.UnmanageNetObjects = function(netObjectIds,
        startDate,
        endDate,
        successCallback,
        failureCallback) {
	    Applications.ManageApplications(netObjectIds, false, startDate, endDate, successCallback, failureCallback);
    };


    MaintenanceMode.RemanageNetObjects = function(netObjectIds, successCallback, failureCallback) {
	    Applications.ManageApplications(netObjectIds, true, null, null, successCallback, failureCallback);
    };

}(SW.Core.namespace("SW.APM.MaintenanceMode")));