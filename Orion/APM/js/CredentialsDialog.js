﻿MakeNamespace("SW.APM");
/*
Required ExtJs framework version 3.4 or 4.0.7

delegate 
	pConfig.onSuccess({ id: {Credential ID}, name: {Credential Name}, user: {User Name}, isNew: {Is New Credential} });
	pConfig.addNoneCred
		-- default [false]
		-- added empty credential [0, "[None]", null].
	pConfig.addInheritedWindows
		-- default [false]
		-- added node credential [-3, "[Inherit credentials from Windows]", null].
*/
SW.APM.CredentialsDialog = function (pConfig) {
	var mExt = null;

	var mConfig = { addNoneCred: false, addInheritedWindows: false, onSuccess: null };
	var mDlg = null, mPanel = null, mLoading = null, mErrors = [];

    /*helper members*/
	function getCtr(pID) { return mPanel.getComponent(pID); }
    function getVal(pID) { var val = getCtr(pID).getValue(); if (mExt.isString(val)) { return val.trim(); } return val; }

    /*event handlers*/
    function onLoaded(pData) {
        var items = [[-1, "@{R=APM.Strings;K=APMWEBJS_AK1_57;E=js}", null, true]];
        if (pConfig.addInheritedWindows) {
            items.push([-3, "@{R=APM.Strings;K=APMWEBJS_AK1_58;E=js}", null, true]);
        }
        if (mConfig.addNoneCred) {
            items.push([0, "@{R=APM.Strings;K=APMWEBJS_AK1_59;E=js}", null, true]);
        }
        mExt.each(pData, function (item) {
            items.push([item.Id, item.Name, item.UserName, item.IsUserNamePasswordCredential]);
        });
        getCtr("cdCreds").getStore().loadData(items);
        var index = getCtr("cdCreds").getStore().find("Id", pConfig.credId);
        if (!isNaN(pConfig.credId) && index != -1) {
            getCtr("cdCreds").setValue(pConfig.credId);
            onSelect(this, getCtr("cdCreds").getStore().getAt(index));
        }
        mLoading.hide();
    }

    function onLoadError(error) {
        mLoading.hide();
        if (mExt.isDefined(error)) {
        	mExt.Msg.show({ title: "@{R=APM.Strings;K=APMWEBJS_VB1_1;E=js}", msg: error, buttons: mExt.Msg.OK, icon: mExt.Msg.ERROR, fn: function (dr) { onCancelClick(); } });
        }
        else {
            onCancelClick();
        }
    }

    function onSelect(pOwner, pRecs) {
    	var rec = pRecs;
    	if (mExt.isArray(pRecs)) {
    		rec = pRecs[0];
    	}

    	var id = rec.get("Id"), isNew = id == -1, isNone = id == 0, name = "", userName = "", psw = "", pwdVisible = true;
        if (!isNew && !isNone) {
        	name = rec.get("Name"); userName = rec.get("UserName");
        	if (pwdVisible = rec.get("IsUserNamePasswordCredential")) {
        		psw = "********";
        	}
        }

        var layoutChanges = (getCtr("cdPsw1").isVisible() !== pwdVisible);
        getCtr("cdPsw1").setVisible(pwdVisible);
        getCtr("cdPsw2").setVisible(pwdVisible);
        getCtr("label").setVisible(pwdVisible);

        getCtr("cdName").setValue(name); getCtr("cdUser").setValue(userName); getCtr("cdPsw1").setValue(psw); getCtr("cdPsw2").setValue(psw);
        getCtr("cdName").setDisabled(!isNew); getCtr("cdUser").setDisabled(!isNew); getCtr("cdPsw1").setDisabled(!isNew); getCtr("cdPsw2").setDisabled(!isNew);

        mExt.getCmp("btnOk").setDisabled(isNew);

        if (mConfig.isRemote) {
            getCtr("lWarn").setVisible(isNew);
            layoutChanges = true;
        }
        if (layoutChanges) {
            mDlg.setHeight(mDlg.getHeight());
            mDlg.doLayout(true);
        }
    }
    function onValidate(pName) {
        var retVal = true, cb = getCtr("cdCreds");
        if (cb.getValue() == "-1" || cb.getValue() == "@{R=APM.Strings;K=APMWEBJS_AK1_57;E=js}") {
            getCtr("cdPsw1").clearInvalid(); getCtr("cdPsw2").clearInvalid();

            mErrors["N1"] = getVal("cdName") == "";
            if (pName == "N" && mErrors["N1"]) { retVal = "@{R=APM.Strings;K=APMWEBJS_VB1_24;E=js}"; }

            mErrors["N2"] = cb.getStore().findBy(function (pRec) { return pRec.get("Name").toLowerCase() == getVal("cdName").toLowerCase(); }) > -1;
            if (pName == "N" && mExt.isBoolean(retVal) && mErrors["N2"]) { retVal = "@{R=APM.Strings;K=APMWEBJS_VB1_25;E=js}"; }

            mErrors["U"] = getVal("cdUser") == "";
            if (pName == "U" && mExt.isBoolean(retVal) && mErrors["U"]) { retVal = "@{R=APM.Strings;K=APMWEBJS_VB1_26;E=js}"; }

            mErrors["P"] = getVal("cdPsw1") != getVal("cdPsw2");
            if (pName == "P" && mExt.isBoolean(retVal) && mErrors["P"]) { retVal = "@{R=APM.Strings;K=APMWEBJS_VB1_27;E=js}"; }
        }

        mExt.getCmp("btnOk").setDisabled(mErrors["N1"] || mErrors["N2"] || mErrors["U"] || mErrors["P"]);

        return retVal;
    }

    function onCredentialCreated(pCredID) {
        if (mConfig.onSuccess) {
        	SW.Core.Services.callWebService("/Orion/APM/Services/Credentials.asmx", "GetCredentials", { credentialIDs: [pCredID] }, onCredentialLoaded);
        }
    }
    function onCredentialLoaded(pData) {
        mLoading.hide();
        onCancelClick();
        if ((pData || []).length > 0) {
            mConfig.onSuccess({ id: pData[0].Id, name: pData[0].Name, user: pData[0].UserName, isNew: true });
        }
    }

    function onOkClick() {
        var id = getVal("cdCreds");
        if (id > -1 || id == -3) {
            if (mConfig.onSuccess) {
                var rec = getCtr("cdCreds").findRecord("Id", id);
                onCancelClick();

                mConfig.onSuccess({ id: id, name: rec.get("Name"), user: rec.get("UserName") });
                return false;
            }
            onCancelClick();
        } else {
            mLoading.msg = "@{R=APM.Strings;K=APMWEBJS_AK1_60;E=js}";
            mLoading.show();
            SW.Core.Services.callWebService("/Orion/APM/Services/Credentials.asmx", "CreateCredential", { name: getVal("cdName"), userName: getVal("cdUser"), psw: getVal("cdPsw1") }, onCredentialCreated, onLoadError);
        }
    }
    function onCancelClick() { if (mDlg) { mDlg.close(); mDlg.destroy(); mDlg = null; } }

    /*create dialog*/
    function ctor() {
    	var isExt4 = (typeof (Ext4) != "undefined");
    	if (isExt4) {
    		mExt = Ext4;
    	} else {
    		mExt = Ext;
    	}

    	mExt.QuickTips.init();
    	mExt.apply(mConfig, pConfig, mConfig);

    	var panelItems = [
			{
				id: "cdCreds", xtype: "combo", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_28;E=js}", typeAhead: false, triggerAction: "all", anchor: "95%", editable: false, selectOnFocus: false,
				queryMode: "local", mode: "local", valueField: "Id", displayField: "Name", value: "@{R=APM.Strings;K=APMWEBJS_AK1_57;E=js}", 
				store: { xtype: "arraystore", fields: ["Id", "Name", "UserName", "IsUserNamePasswordCredential"] },
				listeners: { select: onSelect }
			},
			{ id: "cdName", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_29;E=js}", anchor: "95%", selectOnFocus: true, validator: function () { return onValidate("N"); }, msgTarget: "side" },
			{ id: "cdUser", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_30;E=js}", anchor: "95%", selectOnFocus: true, validator: function () { return onValidate("U"); }, msgTarget: "side" },
			{ id: "label", xtype: "label", html: "@{R=APM.Strings;K=APMWEBJS_VB1_31;E=js}<br/><br/>", style: "margin-left:185px;color:#999;" },
			{ id: "cdPsw1", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_32;E=js}", inputType: "password", anchor: "95%", selectOnFocus: true, validator: function () { return onValidate("P"); }, msgTarget: "side" },
			{ id: "cdPsw2", fieldLabel: "@{R=APM.Strings;K=APMWEBJS_VB1_33;E=js}", inputType: "password", anchor: "95%", selectOnFocus: true, validator: function () { return onValidate("P"); }, msgTarget: "side" }
		];

        if (mConfig.isRemote) {
            var warnHtml;
            if (location.protocol.toLowerCase().indexOf("https") > -1) {
                warnHtml = SF("@{R=APM.Strings;K=APMWEBJS_AK1_63;E=js}", "<a href='/Orion/APM/Admin/EditCredentialSet.aspx?mode=Insert' target='_blank'>", "</a>");
            }
            else {
                warnHtml = "@{R=APM.Strings;K=APMWEBJS_AK1_64;E=js}";
            }
            panelItems.push({ id: "lWarn", xtype: "label", html: warnHtml, style: "color:#f00;" });
        }

        var panelConfig = {
        	frame: true, autoHeight: true, defaultType: "textfield", items: panelItems
        };
        if (isExt4) {
        	mExt.apply(panelConfig, {
        		layout: { type: "anchor", align: "stretch", padding: 5 }, defaults: { anchor: "100%", labelWidth: 180, labelAlign: "right" }
        	});
        	mPanel = mExt.create("Ext.panel.Panel", panelConfig);
        } else {
        	mExt.apply(panelConfig, {
        		labelWidth: 180, labelAlign: "right"
        	});
        	mPanel = new mExt.form.FormPanel(panelConfig);
        }

        var dlgConfig = {
        	title: "@{R=APM.Strings;K=APMWEBJS_AK1_61;E=js}", width: 600, autoHeight: true,
        	border: false, region: "center", layout: "fit", modal: true, resizable: false, bodyStyle: "padding:5px;", items: [mPanel],
        	buttons: [{ id: "btnOk", xtype: "button", text: "@{R=APM.Strings;K=APMWEBJS_TM0_43;E=js}", disabled: true, handler: onOkClick }, { text: "@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}", handler: onCancelClick }]
        };
        if (isExt4) {
        	mDlg = mExt.create("Ext.window.Window", dlgConfig);
        }
        else {
        	mDlg = new mExt.Window(dlgConfig);
        }
        mDlg.show();

        mLoading = new mExt.LoadMask(mDlg.getEl(), { msg: "@{R=APM.Strings;K=APMWEBJS_AK1_62;E=js}" });
        mLoading.show();

        SW.Core.Services.callWebService("/Orion/APM/Services/Credentials.asmx", "GetCredentials", { credentialIDs: [] }, onLoaded, onLoadError);
        return false;
    } ctor();
}
/*show credentials dialog*/
SW.APM.CredentialsDialog.show = function (pConfig) {
	new SW.APM.CredentialsDialog(pConfig); 
}