﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/
APMjs.withGlobal('SW.APM', function (APM, APMjs) {

    'use strict';

    var Ext = APMjs.assertExt(),
        $ = APMjs.assertQuery(),
        SWTreeNodeUI;

    // We need to subclass the NodeUI class to hide the icon.  When the icon is set directly
    // (as opposed to using a css class) the "loading" spinner icon shows up behind the icon.
    // We will simply hide our icon while we wait.
    SWTreeNodeUI = Ext.extend(Ext.tree.TreeNodeUI, {
        beforeLoad: function () {
            var icon = $(this.getIconEl());
            this.iconSrc = icon.attr('src');
            icon.attr('src', this.emptyIcon);
            SWTreeNodeUI.superclass.beforeLoad.call(this);
        },

        afterLoad: function () {
            SWTreeNodeUI.superclass.afterLoad.call(this);
            $(this.getIconEl()).attr('src', this.iconSrc);
        },

        onTextChange: function (node, text) {
            if (this.rendered) {
                this.textNode.innerHTML = node.isLeaf() ? text : this.generateGroupText(node, text);
            }
        },

        refreshText: function (node) {
            this.onTextChange(node, node.text, node.text);
        },

        renderElements: function (n, a, targetNode, bulkRender) {
            SWTreeNodeUI.superclass.renderElements.call(this, n, a, targetNode, bulkRender);

            if (!n.isLeaf() && n.attributes.groupTreeSettings.allowMultipleSelections) {
                $(this.getAnchor()).after(this.selectAllSpan(n));
            }
            this.onTextChange(n, n.text, n.text);
        },

        selectAllSpan: function (node) {

            function clickFunction(nodeId, isSelectAll, linkText) {
                var s = APMjs.format(
                    '<a onclick="{3}(\'{0}\', {1}); return false;">{2}</a>',
                    nodeId,
                    isSelectAll,
                    linkText,
                    node.attributes.groupTreeSettings.selectAllCallbackFunctionName
                );
                return s;
            }

            var selectAllNone = APMjs.format(
                '<br><span class="apm_selectAll">@{R=APM.Strings;K=APMWEBJS_VB1_84;E=js}</span>',
                clickFunction(node.id, true, '@{R=APM.Strings;K=APMWEBJS_VB1_85;E=js}'),
                clickFunction(node.id, false, '@{R=APM.Strings;K=APMWEBJS_VB1_86;E=js}')
            );

            return selectAllNone;
        },

        generateGroupText: function (node, text) {
            var gTS, countInfo, childCountText;

            gTS = node.attributes.groupTreeSettings;
            if (!gTS.allowMultipleSelections) {
                return text;
            }

            countInfo = gTS.getInitialNodeCount(node);
            if (node.isLoaded()) {
                countInfo.selCount = node.getOwnerTree().getChecked('text', node).length;
            }

            childCountText = gTS.getSelectedChildCountText(countInfo.nodeCount, countInfo.selCount);

            return APMjs.format(
                '{0} {1}',
                text,
                childCountText
                //this._selectAllSpan(node)
            );
        }

    });


    // GroupTree class
    function GroupTree(settings) {

        var groupByDropDown, tree;

        function onLeafCheckChange(node, isChecked) {
            settings.onCheckStateChange([node], isChecked);
            var parent = node.parentNode;
            parent.ui.refreshText(parent);
        }

        function checkNodeWithNoEvent(node, checkState) {
            node.suspendEvents();
            node.getUI().toggleCheck(checkState);
            node.resumeEvents();
        }

        function onCheckAllInGroup(groupNode, isChecked) {
            if (!groupNode.isLoaded()) {
                // Force the nodes to load...
                groupNode.reload(function () {
                    groupNode.collapse(false, false);
                    // We are loaded now, now do the check all (or none)
                    onCheckAllInGroup(groupNode, isChecked);
                });
                return;
            }

            groupNode.eachChild(function (node) {
                checkNodeWithNoEvent(node, isChecked);
            });

            settings.onCheckStateChange(groupNode.childNodes, isChecked);
            groupNode.ui.refreshText(groupNode);
        }

        settings.onCheckStateChange = settings.onCheckStateChange || Ext.emptyFn;
        settings.isDefaultSelectedItem = settings.isDefaultSelectedItem || function () { return false; };
        groupByDropDown = $(settings.groupByDropDown);

        tree = new Ext.tree.TreePanel({
            el: settings.renderTo,
            animate: true,
            rootVisible: false,
            lines: false,
            trackMouseOver: false,
            height: settings.height,
            dataUrl: settings.dataUrl,
            root: {
                nodeType: 'async',
                text: 'HiddenRoot',
                draggable: false,
                id: '/'
            }
        });

        groupByDropDown.change(function () {
            tree.getRootNode().reload();
        });

        tree.getLoader().on('beforeload', function (treeLoader, node) {

            // Pass the groupby setting to the webservice when we load nodes
            treeLoader.baseParams.groupby = groupByDropDown.val();
            treeLoader.baseParams.nodeFilter = settings.nodeFilter;

            node.on('beforechildrenrendered', function (parentNode) {
                parentNode.eachChild(function (node) {
                    //FB #24335 If node name contains double quotes, it can't be selected in assign application wizard and test dialog
                    node.id = node.id.replace(/"/gi, '&#34;');
                    if (node.isLeaf() && settings.allowMultipleSelections) {
                        node.attributes.checked = settings.isDefaultSelectedItem(node);
                        node.on('checkchange', onLeafCheckChange);
                    }
                    node.attributes.groupTreeSettings = settings;
                });
                parentNode.ui.onTextChange(parentNode, parentNode.text, parentNode.text);
            });
        });

        /*jslint unparam: true*/
        tree.getLoader().on('load', function (treeLoader, node) {
            if (!node.parentNode) {
                // We just loaded the root.  Add Zebra stripes
                $('.x-tree-root-node li').addClass('zebraRow');
            }
        });
        /*jslint unparam: false*/

        /*jslint unparam: true*/
        tree.getSelectionModel().on('beforeselect', function (selectionModel, newNode, oldNode) {
            if (settings.allowMultipleSelections === false) {
                // Only change the selection if the node is a leaf
                return newNode.isLeaf();
            }
            // Don't show the selection
            return false;
        });
        /*jslint unparam: false*/

        if ((settings.allowMultipleSelections === false) && (settings.selectionChangeCallback)) {
            /*jslint unparam: true*/
            tree.getSelectionModel().on('selectionchange', function (selectionModel, newNode) {
                var fn = eval(settings.selectionChangeCallback);
                if (typeof fn === 'function') {
                    fn(newNode.attributes.nodeId, newNode.attributes.text, newNode.attributes.ipAddress, newNode.attributes.status);
                }
            });
            /*jslint unparam: false*/
        }

        tree.render();

        return {
            selectAllInGroup: function (groupNodeId, isChecked) {
                var groupNode = tree.getNodeById(groupNodeId);
                onCheckAllInGroup(groupNode, isChecked);
            }
        };
    }

    function HashTable(domStore) {

        var that, saveElement, store;

        function saveState() {
            saveElement.val(Ext.encode(store));
        }

        function loadState() {
            var v = saveElement.val();
            return (v.length === 0) ? {} : Ext.decode(v);
        }

        that = this;
        saveElement = $(domStore);
        store = loadState();


        this.add = function (key) {
            store[key.attributes.nodeId] = key.attributes.text;
        };

        this.addRange = function (keys) {
            Ext.each(keys, function (n) {
                that.add(n);
            });
            saveState();
        };

        this.remove = function (key) {
            delete store[key.attributes.nodeId];
        };

        this.removeRange = function (keys) {
            Ext.each(keys, function (n) {
                that.remove(n);
            });
            saveState();
        };

        this.contains = function (key) {
            return (store[key.attributes.nodeId] !== undefined);
        };

        this.getKeys = function () {
            var keys = [];
            $.each(store, function (k) {
                keys.push(k);
            });
            return keys;
        };
    }

    function GroupCountCache(url) {

        var store = null,
            dataUrl = url;

        this.reload = function (groupby, initialSelections) {

            var data = { groupby: groupby, items: Ext.encode(initialSelections) };

            $.ajax({
                async: false,
                type: 'POST',
                url: dataUrl,
                data: data,
                dataType: 'json',
                success: function (result) {
                    store = result;
                }
            });
        };

        this.getCount = function (groupName) {
            var i;
            if (store && store[groupName]) {
                i = store[groupName];
                return {
                    nodeCount: Ext.num(i[0], 0),
                    selCount: Ext.num(i[1], 0)
                };
            }
            return {
                nodeCount: 0,
                selCount: 0
            };
        };
    }

    function SelectNodeTree(groupByDropDownClientId, selectedNodesClientId, allowMultipleSelections, selectionChangeCallback, cookieInfo, nodeFilter) {

        var dataUrl, checkedNodes, groupCountCache, groupByDefault, nodeTree;

        nodeFilter = nodeFilter || '';
        dataUrl = '/Orion/APM/Services/NodeTree.aspx';
        checkedNodes = new HashTable(selectedNodesClientId);
        groupCountCache = new GroupCountCache(dataUrl + '?GroupCounts=1&nodeFilter=' + nodeFilter);

        groupByDefault = $.cookie('groupBy');
        if (groupByDefault === null) {
            groupByDefault = 'Vendor';
            $.cookie(
                'groupBy',
                groupByDefault,
                {
                    expires: 7,
                    path: cookieInfo.path,
                    domain: cookieInfo.domain
                }
            );
        }

        $(groupByDropDownClientId).val(groupByDefault);

        $(groupByDropDownClientId).change(function () {
            var groupBy = $(groupByDropDownClientId).val();
            $.cookie('groupBy', groupBy);
            groupCountCache.reload(groupBy, checkedNodes.getKeys());
        }).change();

        nodeTree = new GroupTree({
            groupByDropDown: groupByDropDownClientId,
            allowMultipleSelections: allowMultipleSelections,
            selectAllCallbackFunctionName: 'SW.APM.NodeTreeSelectAllCallback',
            dataUrl: dataUrl,
            renderTo: 'apm_selectNodeTree',
            height: $('#apm_selectNodeTree').css('height'),
            selectionChangeCallback: (selectionChangeCallback === '') ? null : selectionChangeCallback,
            nodeFilter: nodeFilter,
            onCheckStateChange: function (nodes, isChecked) {
                if (isChecked) {
                    checkedNodes.addRange(nodes);
                } else {
                    checkedNodes.removeRange(nodes);
                }

                if (APM.EventManager) {
                    nodes = checkedNodes.getKeys();
                    APM.EventManager.fire(
                        'onCheckStateChange',
                        {
                            nodeCount: nodes.length,
                            nodes: nodes
                        }
                    );
                }
            },

            isDefaultSelectedItem: function (node) {
                return checkedNodes.contains(node);
            },

            getInitialNodeCount: function (node) {
                // we need to determine how many children are initially selected for
                // this node even though this node hasn't beed loaded yet.
                return groupCountCache.getCount(node.text);
            },

            getSelectedChildCountText: function (totalCount, checkedCount) {
                if (checkedCount > 0) {
                    return APMjs.format(
                        '<span class="nodeGroupInfo">@{R=APM.Strings;K=APMWEBJS_VB1_87;E=js}</span>',
                        checkedCount,
                        totalCount
                    );
                }
                return '';
            }

        });

        // override this function to be connected with current instance
        APM.NodeTreeSelectAllCallback = function (treeNodeId, isChecked) {
            nodeTree.selectAllInGroup(treeNodeId, isChecked);
        };
    }

    // publish classes
    APM.GroupTree = GroupTree;
    APM.SelectNodeTree = SelectNodeTree;
    APM.SWTreeNodeUI = SWTreeNodeUI;
});
