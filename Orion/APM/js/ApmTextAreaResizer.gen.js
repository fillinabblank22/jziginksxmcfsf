/// <reference path="../../typescripts/typings/jquery.d.ts"/>
var SW;
(function (SW) {
    var APM;
    (function (APM) {
        var TextAreaResizer;
        (function (TextAreaResizer) {
            var constants = {
                minHeight: 20
            };
            // singleton state for resizing action - there will always be just one at a time
            var state = {
                $target: null,
                offset: null,
                position: null,
                scrollTop: null
            };
            function getScrollTop() {
                return document.documentElement.scrollTop || document.body.scrollTop;
            }
            function updateHeight(state) {
                var height = Math.max(constants.minHeight, state.offset + state.position);
                state.$target.height(height);
            }
            function dragStart(e) {
                var $target = $(e.data.target);
                $target.blur();
                $target.css('opacity', 0.25);
                state.position = e.pageY || state.position;
                state.$target = $target;
                state.offset = $target.height() - state.position;
                $(document)
                    .mousemove(dragUpdate)
                    .mouseup(dragFinish);
                $(window)
                    .scroll(dragUpdateScroll);
                return false;
            }
            function dragUpdate(e) {
                state.scrollTop = getScrollTop();
                state.position = e.pageY;
                updateHeight(state);
                return false;
            }
            function dragUpdateScroll(e) {
                var scrollTop = getScrollTop();
                var relative = scrollTop - state.scrollTop;
                state.scrollTop = scrollTop;
                state.position += relative;
                updateHeight(state);
                return false;
            }
            function dragFinish(e) {
                $(document)
                    .unbind('mousemove', dragUpdate)
                    .unbind('mouseup', dragFinish);
                $(window)
                    .unbind('scroll', dragUpdateScroll);
                state.$target
                    .css('opacity', 1)
                    .focus();
                state.$target = null;
                state.offset = null;
                state.position = null;
            }
            function rebindElement(target) {
                $(target)
                    .closest('.resizable')
                    .find('.grippie')
                    .unbind('mousedown')
                    .bind('mousedown', { target: target }, dragStart);
            }
            // published methods
            function rebind(target) {
                $(target).each(function () {
                    rebindElement(this);
                });
            }
            TextAreaResizer.rebind = rebind;
            function makeResizable(target) {
                $(target).each(function () {
                    var $element = $(this);
                    if ($element.is('.processed')) {
                        return;
                    }
                    $element
                        .wrap('<div class="resizable"><span></span></div>')
                        .parent()
                        .append($('<div class="grippie"></div>'));
                    rebindElement($element[0]);
                    $element.addClass('processed');
                });
            }
            TextAreaResizer.makeResizable = makeResizable;
            function makeTextAreasResizable(parent) {
                if (parent === void 0) { parent = null; }
                var jparent = $(parent || document);
                makeResizable(jparent.find('textarea.apm_Resizable:not(.processed)'));
            }
            TextAreaResizer.makeTextAreasResizable = makeTextAreasResizable;
            // default page initializer
            $(document).ready(function () {
                makeTextAreasResizable();
            });
        })(TextAreaResizer = APM.TextAreaResizer || (APM.TextAreaResizer = {}));
    })(APM = SW.APM || (SW.APM = {}));
})(SW || (SW = {}));
//# sourceMappingURL=ApmTextAreaResizer.gen.js.map