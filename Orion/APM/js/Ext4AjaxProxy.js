/// <reference path="/Orion/js/extjs/4.0.7/debug/ext-all-sandbox-debug.js" />

Ext4.define('AspWebAjaxProxy', {
    extend: 'Ext.data.proxy.Ajax',
    require: 'Ext.data',

    buildRequest: function (operation) {
        var params = Ext4.applyIf(operation.params || {}, this.extraParams || {}),
            request;

        params = Ext4.applyIf(params, this.getParams(operation));

        if (operation.id && !params.id) {
            params.id = operation.id;
        }

        params = Ext4.JSON.encode(params);

        request = Ext4.create('Ext.data.Request', {
            params: params,
            action: operation.action,
            records: operation.records,
            operation: operation,
            url: operation.url
        });
        request.url = this.buildUrl(request);
        operation.request = request;
        return request;
    }
});
