﻿Ext.ns("SW.APM");
/*
pConfig = { 
	Callback =>  function(Array selectedDefinitions )
		params 
			selectedDefinitions => [{ID : definition id, Name: definition name, Count: definition count }]
}
*/
SW.APM.AddComponentDefinition = function (pConfig) {
	var SF = String.format, KEY_GPN = "ME.AddDef.Name", KEY_GPV = "ME.AddDef.Value", KEY_GPS = "ME.AddDef.Size";
	var mCmpUrl = "/Orion/APM/Services/Components.asmx/";
	var mWindow = null, mPropValList = null, mCmpGrid = null;
	var mSelItems = [];

	/*helper members*/
	function gc(pKey) { return getCookie(pKey) || ""; }
	function sc(pKey, pVal) { setCookie(pKey, pVal || "", "months", 1); }

	/*helper members*/
	function getSelInfo(pRec) { return { ID: pRec.get("ID"), Name: pRec.get("Name"), Count: pRec.get("Count") }; }
	function getSelIndex(pRec) { for (var i = 0; i < mSelItems.length; i++) { if (mSelItems[i].ID == pRec.get("ID")) { return i; } } return -1; }
	function setTitle() {
        var title = "@{R=APM.Strings;K=APMWEBJS_NoComponentsSelected;E=js}";
		switch (mSelItems.length) {
			case 0:
        		title = "@{R=APM.Strings;K=APMWEBJS_NoComponentsSelected;E=js}";
				break; 
			case 1:
				title = "@{R=APM.Strings;K=APMWEBJS_ComponentSelected;E=js}";
				break;
			default:
				title = SF("@{R=APM.Strings;K=APMWEBJS_ComponentsSelected;E=js}", mSelItems.length);
				break;
		}
		mWindow.setTitle(title);
	}

	/*combo box event handlers*/
	function onPropSelect(pSender, pRec, pIndex) {
		var gpn = Ext.getCmp("ctrPropCb").getValue();
		if (gpn != gc(KEY_GPN)) { sc(KEY_GPN, gpn); sc(KEY_GPV, ""); }

		var store = mPropValList.getStore();
		if (gpn == "") {
			store.removeAll();
			onPropValLoad(store, [], store.lastOptions);
		} else {
			store.proxy.conn.jsonData = { property: gpn };
			store.reload();
		}
	}
	/*list event handlers*/
	function onPropValLoad(pStore, pRecs, pOpt) {
		var store = mPropValList.getStore();
		store.removeAll();

		if (pRecs.length > 0) {
			for (var i = 0, count = pRecs.length; i < count; i++) {
				pRecs[i].data.theValue = pRecs[i].json[0];
				pRecs[i].data.theCount = pRecs[i].json[1];
				store.add(pRecs[i]);
			}
		}
		onPropValSelect(mPropValList, Math.max(store.find("theValue", gc(KEY_GPV)), 0))
	}
	function onPropValSelect(pSender, pIndex, pNode, pEvt) {
		var rec = mPropValList.getStore().getAt(pIndex);
		if (rec) {
			mPropValList.select(rec);
		}
		sc(KEY_GPV, rec ? rec.get("theValue") : "");

		onClearSearch();
	}

	/*grid event handlers*/
	function onCompBeforeLoad(pStore, pOpt) {
		var property = gc(KEY_GPN), value = gc(KEY_GPV), filterText = Ext.getCmp("ctrSearchText").getValue(), filterRule = Ext.getCmp("ctrSearchRule").getValue();
		var start = pOpt.params.start, limit = pOpt.params.limit, direction = pOpt.params.dir;
		if (!start || !limit) {
			start = 0; limit = mCmpGrid.getBottomToolbar().pageSize;
		}
		pStore.proxy.conn.jsonData = { "property": property, "value": value, "filterText": filterText, "filterRule": filterRule, "start": start, "limit": limit, "direction": direction };
	}
	function onCompLoad(pStore, pRecs, pOpt) {
		var store = mCmpGrid.getStore();
		store.removeAll();
		if (pRecs.length > 0) {
			for (var i = 0, count = pRecs.length; i < count; i++) {
				pRecs[i].data.ID = pRecs[i].json[0];
				pRecs[i].data.Name = pRecs[i].json[1];
				pRecs[i].data.Count = pRecs[i].json[2];

				store.add(pRecs[i]);
			}

			var selRecs = [];
			for (var i = 0, rec = store.getAt(i); i < store.getCount(); rec = store.getAt(++i)) {
				if (getSelIndex(rec) > -1) {
					selRecs.push(rec);
				}
			}
			mCmpGrid.getSelectionModel().selectRecords(selRecs);
		}
		APMjs.Utility.manageChecker(mCmpGrid);
	}
	function onCompSelect(pSender, pIndex, pRec) { var index = getSelIndex(pRec); if (index != -1) { return false; } mSelItems.push(getSelInfo(pRec)); return true; }
	function onCompDeselect(pSender, pIndex, pRec) { var index = getSelIndex(pRec); if (index == -1) { return false; } mSelItems.splice(index, 1); return true; }
	function onCompSelectionChange() { setTitle(); APMjs.Utility.manageChecker(mCmpGrid); mWindow.buttons[0].setDisabled(mSelItems.length == 0); return true; }
	function onCompAfterCellEdit(pEvt) { pEvt.record.commit(); onCompDeselect(null, pEvt.row, pEvt.record); onCompSelect(null, pEvt.row, pEvt.record); }
	function onCompRenderName(pValue) {
		var text = Ext.getCmp("ctrSearchText").getValue(), rule = Ext.getCmp("ctrSearchRule").getValue(), pref = "", suff = "";
		if (rule == "@{R=APM.Strings;K=APMWEBJS_ComponentFiltertingName_StartsWith;E=js}") { pref = "^", suff = ""; }
		if (rule == "@{R=APM.Strings;K=APMWEBJS_ComponentFiltertingName_EndsWith;E=js}") { pref = "", suff = "$"; }
		return pValue.replace(eval(SF("/{0}{1}{2}/gi", pref, text.replace("/","\\/"), suff)), SF("<b style='background-color:#ff0;'>{0}</b>", text));
	}

	/*search members*/
	function onDoSearch(pSender, pEvt) { if (pEvt.getKey() == pEvt.ENTER) { doSearch(); } return true; }
	function onClearSearch() { Ext.getCmp("ctrSearchText").setValue(""); doSearch(); return true; }
	function doSearch() {
		mCmpGrid.getBottomToolbar().cursor = 0;
		
		var btb = mCmpGrid.getBottomToolbar();
		btb.cursor = 0;
		btb.doRefresh();
	}

	/*dialog event handlers*/
	function onBtnAddClick() {
		if (mSelItems.length == 0) {
            Ext.Msg.show({ title: "Error", msg: "@{R=APM.Strings;K=APMWEBJS_SelectComponentsToAdd;E=js}", icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });
			return true;
		}
		if (pConfig.Callback) {
			pConfig.Callback(mSelItems);
		}
		onBtnCancelClick();
		return true;
	}
	function onBtnCancelClick() { if (mWindow) { mWindow.close(); mWindow = null; } return true; }

	/*constructor*/
	function ctor() {
		Ext.QuickTips.init();

		var wWidth = parseInt(Ext.getBody().getWidth() * 0.7), wHeight = parseInt(Ext.getBody().getHeight() * 0.8);

		var gpn = gc(KEY_GPN), gpv = gc(KEY_GPV), gps = parseInt(gc(KEY_GPS) || 20);
		/*init group by combo box*/
		var gConfig = {
            id: "ctrPropCb", store: new Ext.data.ArrayStore({ fields: ["Name", "Value"], data: [["@{R=APM.Strings;K=APMWEBJS_NoGrouping;E=js}", ""], ["@{R=APM.Strings;K=APMWEBJS_CategoryName;E=js}", "Category"]] }),
			valueField: "Value", displayField: "Name", mode: "local", value: gpn, triggerAction: "all", width: 240, editable: false,
			listeners: { select: onPropSelect }
		};

		/*init group by properties list*/
		var gpStore = new Ext.data.JsonStore({
			url: mCmpUrl + "GetDefinitionValuesAndCountForProperty",
			root: "d.Rows", baseParams: { property: gpv }, fields: ["theValue", "theCount"], remoteSort: false,
			listeners: { load: onPropValLoad }
		});
		var gpConfig = {
			store: gpStore, emptyText: "", hideHeaders: true, selectedClass: "x-grid3-row-selected", trackOver: false, singleSelect: true, reserveScrollOffset: true,
			columns: [{ tpl: "<a href='#'><b>{theValue} ({theCount})</b></a>"}],
			listeners: { click: onPropValSelect }
		};

		/*init components grid*/
		var cStore = new Ext.data.Store({
			reader: new Ext.data.JsonReader({ totalProperty: "d.TotalRows", root: "d.DataTable.Rows", fields: ["ID", "Name", "Count"] }),
			url: mCmpUrl + "GetDefinitions", remoteSort: true, sortInfo: { field: "Name", direction: "ASC" },
			listeners: { beforeload: onCompBeforeLoad, load: onCompLoad }
		});
		var sm1 = new Ext.grid.CheckboxSelectionModel({ checkOnly: true, listeners: { rowselect: onCompSelect, rowdeselect: onCompDeselect, selectionchange: onCompSelectionChange} });
		var cConfig = {
			height: wHeight - 70, border: true, autoScroll: true, trackMouseOver: false, disableSelection: true, stripeRows: true, loadMask: true, maskDisabled: false, viewConfig: { forceFit: true, enableRowBody: true },
			store: cStore, clicksToEdit: 1,
			cm: new Ext.grid.ColumnModel([
				sm1,
                { xtype: "numbercolumn", header: "@{R=APM.Strings;K=APMWEBJS_Quantity;E=js}", dataIndex: "Count", align: "center", editor: { xtype: "numberfield", allowBlank: false, minValue: 1, value: 1, maxValue: 10 }, width: 70, format: "0", sortable: false, menuDisabled: true, fixed: true },
				{ header: "@{R=APM.Strings;K=APMWEBJS_Name;E=js}", dataIndex: "Name", sortable: true, menuDisabled: true, renderer: onCompRenderName }
			]),
			sm: sm1,
			listeners: { afteredit: onCompAfterCellEdit },
			tbar: [
				"->", {
					id: "ctrSearchRule", xtype: "combo",
                    store: new Ext.data.ArrayStore({ fields: ["Name"], data: [["@{R=APM.Strings;K=APMWEBJS_ComponentFiltertingName_Contains;E=js}"], ["@{R=APM.Strings;K=APMWEBJS_ComponentFiltertingName_StartsWith;E=js}"], ["@{R=APM.Strings;K=APMWEBJS_ComponentFiltertingName_EndsWith;E=js}"]] }), valueField: "Name", displayField: "Name",
					mode: "local", value: "@{R=APM.Strings;K=APMWEBJS_ComponentFiltertingName_Contains;E=js}", triggerAction: "all", width: 100, editable: false, listeners: { select: doSearch }
				}, " ", {
					id: "ctrSearchText", xtype: "textfield", enableKeyEvents: true, listeners: { specialkey: onDoSearch }
				}, " ", {
					xtype: "button", iconCls: "btn-def-search-do", icon: "/Orion/images/Button.SearchIcon.gif", listeners: { click: doSearch }
				}, " ", {
					xtype: "button", iconCls: "btn-def-search-cancel", icon: "/Orion/images/Button.SearchCancel.gif", listeners: { click: onClearSearch }
				}
			],
			bbar: new Ext.PagingToolbar({
                store: cStore, pageSize: gps, displayInfo: true, displayMsg: "@{R=APM.Strings;K=APMWEBJS_VB1_111;E=js}", emptyMsg: "@{R=APM.Strings;K=APMWEBJS_NoComponentsToDisplay;E=js}",
				items: [
                    new Ext.form.Label({ text: "@{R=APM.Strings;K=APMWEBJS_Show;E=js}", style: "margin-left:5px;margin-right:5px" }),
                    new Ext.form.ComboBox({
                    	store: new Ext.data.SimpleStore({ fields: ["size"], data: [[10], [20], [30], [40], [50]] }),
                    	displayField: "size", mode: "local", triggerAction: "all", selectOnFocus: true, width: 50, editable: false, value: gps, regex: /^\d*$/,
                    	listeners: {
                    		select: function (pStore, pRec) { var btb = mCmpGrid.getBottomToolbar(); btb.pageSize = pRec.get("size"); btb.cursor = 0; sc(KEY_GPS, btb.pageSize); btb.doRefresh(); }
                    	}
                    }),
                    new Ext.form.Label({ text: "@{R=APM.Strings;K=APMWEBJS_ItemsPerPage;E=js}", style: "margin-left:5px;margin-right:5px;" })
				]
			})
		};

		/*populate ui to window*/
		mWindow = new Ext.Window({
            title: "@{R=APM.Strings;K=APMWEBJS_NoComponentsSelected;E=js}", layout: "border", closeAction: "close", plain: true, modal: true, width: wWidth, height: wHeight, resizable: false,
			items: [
				{ region: "west", width: 300, padding: 5, margins: "0 0 0 0", items: [new Ext.form.ComboBox(gConfig), { xtype: "label", html: "<hr/>" }, (mPropValList = new Ext.list.ListView(gpConfig))] },
				{ region: "center", width: wWidth - 320, margins: "0 0 0 5", border: false, items: [(mCmpGrid = new Ext.grid.EditorGridPanel(cConfig))] }
			],
            buttons: [{ text: "@{R=APM.Strings;K=APMWEBJS_Add;E=js}", handler: onBtnAddClick, disabled: true }, { text: "@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}", handler: onBtnCancelClick}]
		});
		mWindow.show();
		onPropSelect();
	}; ctor();
}
SW.APM.AddComponentDefinition.show = function (pConfig) {
	var instance = new SW.APM.AddComponentDefinition(pConfig);
	return false;
}
