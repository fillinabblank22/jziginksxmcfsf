/*jslint browser: true, indent: 4, */
/*globals APMjs: false*/
APMjs.initGlobal('SW.APM.Transformation', function (module, APMjs) {
    'use strict';

    var getValue = APMjs.getValue,
        setValue = APMjs.setValue,
        setVisibility = APMjs.setVisibility,
        setInlineVisibility = APMjs.setInlineVisibility,
        setBlockVisibility = APMjs.setBlockVisibility;

    function setTableRowVisibility(elementId, visible) {
        setVisibility(elementId, visible, '', 'none');
    }

    function dataTransformPostProcessing(successful, statistic, tbResultId) {
        setValue(tbResultId, statistic);
    }

    // *** transformation methods

    function dataTransformPostTestProcessingMultiValue(successful, statistic, tbResultId, sColumnName) {
        var array, value, i;
        // TODO: eval is evil
        array = eval(statistic);
        value = '';
        for (i = 0; i < array.length; i += 1) {
            if (array[i].First === sColumnName) {
                value = array[i].Second;
                break;
            }
        }
        setValue(tbResultId, value);
    }

    function retrieveValue(hfTestSinglePostProcessingId, btnTestButtonId, tbResultId, hfGetCurrentValueProcessId, bIsMultiValue, sColumnName) {
        var sPostProcessingMethod = '';
        setValue(hfGetCurrentValueProcessId, 'true');
        if (bIsMultiValue) {
            sPostProcessingMethod = 'SW.APM.Transformation.DataTransformPostTestProcessingMultiValue("#TestSuccessful", "#TestStatistic", "' + tbResultId + '", "' + sColumnName + '");';
        } else {
            sPostProcessingMethod = 'SW.APM.Transformation.DataTransformPostTestProcessing("#TestSuccessful", "#TestStatistic", "' + tbResultId + '");';
        }
        setValue(hfTestSinglePostProcessingId, getValue(hfTestSinglePostProcessingId) + ' ' + sPostProcessingMethod);
        document.getElementById(btnTestButtonId).click();
    }

    function showCommonFormulasDiv(index, divArray) {
        var i;
        for (i = 0; i < divArray.length; i += 1) {
            setInlineVisibility(divArray[i], false);
        }
        setInlineVisibility(divArray[index], true);
    }

    function expandCollapseTest(image, hfTestExpandedId, elementIds) {
        var i, collapsed = (image.src.indexOf('Expand') >= 0);
        image.src = collapsed ? '/Orion/images/Button.Collapse.gif' : '/Orion/images/Button.Expand.gif';
        for (i = 0; i < elementIds.length; i += 1) {
            setTableRowVisibility(elementIds[i], collapsed);
        }
        setValue(hfTestExpandedId, collapsed);
    }

    function dataTransformEvaluateTest(tbFormulaId, tbInputId, spanOutputId, divErrorId) {

        function populateSpanOutput(message) {
            if ($.browser.msie) {
                document.getElementById(spanOutputId).innerText = message;
            } else {
                document.getElementById(spanOutputId).innerHTML = message;
            }
        }

        function onSuccess(result) {
            if (!result.startsWith('error')) {
                if (result.indexOf('Infinity') >= 0 || result.startsWith('NaN')) {
                    populateSpanOutput('Error: Result is out of range');
                } else {
                    populateSpanOutput(result);
                }
            } else {
                populateSpanOutput('Error');
                setBlockVisibility(divErrorId, true);
            }
        }

        var input, formula;

        setBlockVisibility(divErrorId, false);
        populateSpanOutput('');

        input = getValue(tbInputId);
        formula = getValue(tbFormulaId);

        if (formula === '') {
            window.alert('The Formula is missing.');
            return;
        }

        populateSpanOutput('In progress...');

        APMjs.assertGlobal('ORION').callWebService(
            '/Orion/APM/Services/Components.asmx',
            'DataTransformEvaluateTest',
            {
                'expression': formula,
                'input': input
            },
            onSuccess
        );
    }

    module.DataTransformPostTestProcessing = dataTransformPostProcessing;
    module.DataTransformPostTestProcessingMultiValue = dataTransformPostTestProcessingMultiValue;
    module.RetrieveValue = retrieveValue;
    module.ShowCommonFormulasDiv = showCommonFormulasDiv;
    module.ExpandCollapseTest = expandCollapseTest;
    module.DataTransformEvaluateTest = dataTransformEvaluateTest;
});
