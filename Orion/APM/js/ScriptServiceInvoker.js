﻿/// <reference name="MicrosoftAjax.js"/>
Type.registerNamespace("Orion.APM.js");

Orion.APM.js.ScriptServiceInvoker = function () {
    Orion.APM.js.ScriptServiceInvoker.initializeBase(this);

    //this.Interval = interval;
    //this._enabled = false;
    //this._timer = null;
    this.isCanceled = false;
};

Orion.APM.js.ScriptServiceInvoker.prototype = {

    dispose: function () {
        Orion.APM.js.ScriptServiceInvoker.callBaseMethod(this, 'dispose');
    },

    callService: function (scriptServiceProxyMethod, webMethodParams, consumerData, interval, scope,
        notLastResponseCallback, lastResponseCallback, errorCallback) {
        var callContext = {};
        callContext.scriptServiceProxyMethod = scriptServiceProxyMethod;
        callContext.webMethodParams = webMethodParams;
        callContext.consumerData = consumerData;
        callContext.interval = interval;
        callContext.scope = scope;
        callContext.notLastResponseCallback = notLastResponseCallback;
        callContext.lastResponseCallback = lastResponseCallback;
        callContext.errorCallback = errorCallback;

        this.callServiceMethod(callContext);
    },

    callServiceMethod: function (callContext) {
        if (!this.isCanceled) {
            var invokeParams = [Function.createDelegate(this, this.onSuccess),
            Function.createDelegate(this, this.onError),
            callContext];

            var allParams = (callContext.webMethodParams || []).concat(invokeParams);
            callContext.scriptServiceProxyMethod.apply(this, allParams);
        }
    },

    cancel: function () {
        this.isCanceled = true;
    },

    onSuccess: function (result, context) {
        var resultContainer = {};
        resultContainer.result = result;

        //let's allow consumer to modify webMethodParams and delay interval for next partial request
        resultContainer.webMethodParams = context.webMethodParams;
        resultContainer.interval = context.interval;

        if (result.IsNotFinished) {

            //call custom callback first
            resultContainer.callServiceAgain = true;

            if (context.notLastResponseCallback !== null) {
                context.notLastResponseCallback.call(context.scope, resultContainer, context.consumerData);
            }

            //if user don't want to escape iteration
            if (resultContainer.callServiceAgain) {
                //change method params and interval (could be modified by consumer)
                context.webMethodParams = resultContainer.webMethodParams;
                context.interval = resultContainer.interval;

                var callServiceMethodDelegate = Function.createDelegate(this, this.callServiceMethod);
                setTimeout(function () { callServiceMethodDelegate(context); }, context.interval);
            }
        }
        else {
            //to satisfy an interface
            resultContainer.callServiceAgain = false;

            if (context.lastResponseCallback !== null) {
                context.lastResponseCallback.call(context.scope, resultContainer, context.consumerData);
            }
        }
    },

    onError: function (error, context) {
        if (context.errorCallback !== null) {
            context.errorCallback.call(context.scope, error, context.consumerData);
        }
    }
};

// JSON object that describes all properties, events, and methods of this component that should
// be addressable through the Sys.TypeDescriptor methods, and addressable via xml-script.
Orion.APM.js.ScriptServiceInvoker.descriptor = {
    properties: [],
    events: []
};

Orion.APM.js.ScriptServiceInvoker.registerClass('Orion.APM.js.ScriptServiceInvoker', Sys.Component);

// Since this script is not loaded by System.Web.Handlers.ScriptResourceHandler
// invoke Sys.Application.notifyScriptLoaded to notify ScriptManager 
// that this is the end of the script.
if (typeof (Sys) !== 'undefined') { Sys.Application.notifyScriptLoaded(); }
