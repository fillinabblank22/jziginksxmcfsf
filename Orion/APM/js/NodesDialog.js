﻿/*
	pParams.onSuccess({ id: {Node ID}, name: {Node Name}, ip: {Node IP Address}, status: {Node Status} });
	pParams.filter = { ids: {Array of nodes id} };
*/
SW.APM.NodesDialog = function (pParams) {
	var SF = String.format;
	var mUrl = "/Orion/APM/Services/NodeTree.aspx";
	var mDlg = null, mCB = null, mTree = null, mLoading = null;

	/*helper members*/
	function keyGB() { return "APM.ND|GroupBy"; }
	function keyEC(pNode) { return SF("APM.ND|{0}|{1}", mCB.getValue(), pNode.getPath().split("/").join(" ").trim()); }

	/*event handlers*/
	function onGroupByLoaded(pStore, pRecs, pOptions) {
		pStore.un("load", onGroupByLoaded);

		mCB.mode = "local"; mCB.setValue(getCookie(keyGB()) || "Vendor");
		mCB.fireEvent("select", mCB, mCB.findRecord("Value", mCB.getValue()));
	}
    
	function onTreeBeforeLoad(pLoder, pNode, pCallback) {
		if (mLoading == null) { return false; }
		mLoading.show();

		this.baseParams.uiProvider = "Ext.tree.TreeNodeUI";
		this.baseParams.groupBy = mCB.getValue();
		if (pParams.filter) {
			var val = pParams.filter.ids;
			if (val && val.length > 0) {
				this.baseParams.nodeFilter = SF("n.NodeID IN ({0})", val.join(","));
			}
		}
	}
    
	function onTreeLoadException(pLoder, pNode, pResponse) {
	    mLoading.hide();
	    //window.console && console.log('Failed to load node: ' + pNode.text);
	}
    
	function onTreeLoaded(pLoder, pNode, pResponse) {
		mLoading.hide();
		pNode.eachChild(function (pChild) {
			if (getCookie(keyEC(pChild)) == "expanded") { pChild.expand(); }
		});
	}

	function onGroupBySelect(pOwner, pRec) {
		mDlg.buttons[0].setDisabled(true);
		mTree.getRootNode().reload();
	}

	function onNodeExpand(pNode) { if (pNode.id != "/") { setCookie(keyEC(pNode), "expanded", "months", 1); } }
	function onNodeCollapse(pNode) { if (pNode.id != "/") { setCookie(keyEC(pNode), "NA", "minutes", 1); } }
	function onNodeClick(pNode) { mDlg.buttons[0].setDisabled(!pNode.isLeaf()); }

	function onOkClick() {
		if (pParams.onSuccess) {
			var info = mTree.getSelectionModel().getSelectedNode().attributes;

			onCancelClick();

			pParams.onSuccess({ id: info.nodeId, name: info.text, ip: info.ipAddress, status: info.status });
			return;
		}
		onCancelClick();
	}
	function onCancelClick() { if (mDlg) { mDlg.close(); mDlg.destroy(); mDlg = null; } }

	/*create tree*/
	function ctor() {
		mDlg = new Ext.Window({
			title: "@{R=APM.Strings;K=APMWEBJS_NodeDialogTitle;E=js}", width: 500, height: 400, border: false, region: "center", layout: "fit", modal: true, bodyStyle: "padding:5px;",
			tbar: new Ext.Toolbar({
				items: [
					"@{R=APM.Strings;K=APMWEBJS_VB1_90;E=js}",
					new Ext.form.ComboBox({
						id: "ndCBGroupBy", width: 250, displayField: "Key", valueField: "Value", editable: false, typeAhead: true, forceSelection: true, triggerAction: "all", selectOnFocus: false,
						store: new Ext.data.JsonStore({
							url: mUrl, autoLoad: true, baseParams: { action: "getGroupByProps" }, fields: ["Key", "Value"], listeners: { load: onGroupByLoaded }
						}),
						listeners: { select: onGroupBySelect }
					})
				]
			}),
			items: [
				new Ext.tree.TreePanel({
					id: "ndTreeNodes", xtype: "treepanel", region: "center", margins: "2 2 0 2", autoScroll: true, rootVisible: false, frame: true,
					root: new Ext.tree.AsyncTreeNode({ id: "/" }),
					loader: new Ext.tree.TreeLoader({
					    dataUrl: mUrl, listeners: { beforeload: onTreeBeforeLoad, load: onTreeLoaded, loadexception: onTreeLoadException }
					}),
					listeners: { collapsenode: onNodeCollapse, expandnode: onNodeExpand, click: onNodeClick }
				})
			],
			buttons: [{ text: "@{R=APM.Strings;K=APMWEBJS_TM0_43;E=js}", disabled: true, handler: onOkClick }, { text: "@{R=APM.Strings;K=APMWEBJS_VB1_39;E=js}", handler: onCancelClick}]
		});
		mDlg.show();

		mCB = mDlg.getTopToolbar().getComponent("ndCBGroupBy");
		mTree = mDlg.getComponent("ndTreeNodes");

        mLoading = new Ext.LoadMask(mDlg.getEl(), { msg: "@{R=APM.Strings;K=APMWEBJS_LoadingNodesPleaseWait;E=js}" });
	} ctor();
}

/*show nodes dialog*/
SW.APM.NodesDialog.show = function (pParams) {
	new SW.APM.NodesDialog(pParams)
}

/*change node dialog using in "Select Credentials & Test" wizard's step*/
SW.APM.Admin.ChangeTestNode = function (pNodeCnt) {
	var ids = [];
	pNodeCnt.find("input[type=radio][onclick]").each(function () {
		ids.push(parseInt(this.value));
	});
	if (ids.length > 0) {
		SW.APM.NodesDialog.show({ filter: { "ids": ids }, "onSuccess": function (pNode) { pNodeCnt.find(String.format("input[type=radio][onclick][value={0}]", pNode.id)).click(); } });
	}
	return false;
}