﻿APMjs.initGlobal('SW.APM.Controls', function (controls) {
//This control can be used to display percentage status bar on resources built on UnderscoreJS templates
//Also it can be used inside ApmCustomQueryTable table
//Example of usage
/*
    it will display percent bar with one line(green, yellow or red)
    function GetPercentStatusBar(usedSpace) {
                 var p = new SW.APM.Controls.PercentStatusBar();
                 p.WarningLevel = 80;
                 p.ErrorLevel = 95;
                 p.UsedSpace = usedSpace;
                 return p.BuildPercentStatusBar();
             };
    
    it will display percent bar with two lines: first(green, yellow or red), second dark grey
    function GetPercentStatusBar(usedSpace, availableSpace) {
                 var p = new SW.APM.Controls.PercentStatusBar();
                 p.WarningLevel = 80;
                 p.ErrorLevel = 95;
                 p.UsedSpace = usedSpace;
                 p.AvailableSpace = availableSpace;
                 return p.BuildPercentStatusBar();
             };
*/
    SW.APM.Controls.PercentStatusBar = function() {
        this.Reverse = false;
        this.UsedSpacePercentage = 50;
        this.AvailableSpacePercentage = 0;
        this.UsedSpaceWarningLevel = 100;
        this.UsedSpaceErrorLevel = 100;
        this.AvailableSpaceErrorLevel = 100;
        this.AvailableSpaceWarningLevel = 100;
        this.Width = 100;
        this.Height = 10;
        this.BuildByStatus = false;
        this.Status = 0;

        //public method. Build percent bar according to set properties
        this.BuildPercentStatusBar = function() {

            //validate properties
            this.UsedSpacePercentage = rangeCheck(this.UsedSpacePercentage);
            this.AvailableSpacePercentage = rangeCheck(this.AvailableSpacePercentage);
            this.UsedSpaceWarningLevel = thresholdCheck(this.UsedSpaceWarningLevel);
            this.UsedSpaceErrorLevel = thresholdCheck(this.UsedSpaceErrorLevel);
            this.AvailableSpaceErrorLevel = thresholdCheck(this.AvailableSpaceErrorLevel);
            this.AvailableSpaceWarningLevel = thresholdCheck(this.AvailableSpaceWarningLevel);

            var cssCalss;
            var size = Math.round(this.UsedSpacePercentage + this.AvailableSpacePercentage);
            var availableSpace = Math.round(size > 0 ? 100 * this.AvailableSpacePercentage / (size) : 0);
            if (this.BuildByStatus) {

                if (this.Status == 3) cssCalss = "YellowBar";
                else if (this.Status == 14) cssCalss = "RedBar";
                else cssCalss = "GreenBar";
            } else {
                if (this.Reverse) {
                    if (size < this.UsedSpaceErrorLevel || availableSpace < this.AvailableSpaceErrorLevel)
                        cssCalss = "RedBar";
                    else if (size < this.UsedSpaceWarningLevel || availableSpace < this.AvailableSpaceWarningLevel) {
                        cssCalss = "YellowBar";
                    } else {
                        cssCalss = "GreenBar";
                    }
                } else {
                    if (size > this.UsedSpaceErrorLevel || availableSpace > this.AvailableSpaceErrorLevel)
                        cssCalss = "RedBar";
                    else if (size > this.UsedSpaceWarningLevel || availableSpace > this.AvailableSpaceWarningLevel) {
                        cssCalss = "YellowBar";
                    } else {
                        cssCalss = "GreenBar";
                    }
                }
            }
            //filled value should be built according to Width value
            var internalUsedDivWidth = parseInt(Math.round(this.UsedSpacePercentage * (this.Width / 100)));
            var internalFreeDivWidth = parseInt(Math.round(size * (this.Width / 100))) < this.Width ? parseInt(Math.round(this.AvailableSpacePercentage * (this.Width / 100))) : this.Width - internalUsedDivWidth;

            var div = $('<div/>');
            var internalDiv = $('<div/>').addClass('BarBackground').css({ 'width': this.Width + 'px', 'height': this.Height + 'px', 'display': 'inline-block', 'overflow': 'hidden' }).appendTo(div);
            $('<div/>').addClass(cssCalss).css({ 'width': internalUsedDivWidth + 'px', 'height': this.Height + 'px', 'float': 'left' }).appendTo(internalDiv);
            $('<div/>').addClass('GreyBar').css({ 'width': internalFreeDivWidth + 'px', 'height': this.Height + 'px', 'overflow': 'hidden', 'background-image': "url('/Orion/APM/Images/Bar.gray.gif')", 'background-repeat': 'repeat-x', 'background-color': 'darkgrey' }).appendTo(internalDiv);
            return div.html();
        };

        //private function that validates values
        var rangeCheck = function(value) {
            var result = value;
            if (result < 0) result = 0;
            if (result > 100) result = 100;
            return result;
        };

        var thresholdCheck = function(value) {
            var result = value;
            if (result < 0) result = 0;
            if (result > 100 || result == null || result == '') result = 100;
            return result;
        };
    }

    SW.APM.Controls.LinearGauge = function() {
        this.Width = 150;
        this.Height = 10;
        this.WarningThreshold;
        this.CriticalThreshold;
        this.Value = 0;
        this.Status;
        this.GreenBarWidth = 50;
        this.YellowBarWidth = 25;
        this.RedBarWidth = 25;

        this.BuildLinearGauge = function() {
            var gaugeWidth = this.Width;
            var gaugeHeight = this.Height;
            var div = $('<div/>');

            if (drawLinearGauge(this.Status, this.WarningThreshold, this.CriticalThreshold) && (this.GreenBarWidth + this.YellowBarWidth + this.RedBarWidth) == 100) {
                var percentage = calculatePercentage(this.Value, this.WarningThreshold, this.CriticalThreshold, this.Status);

                var greenBarWidth = (gaugeWidth * this.GreenBarWidth) / 100;
                var yellowBarWidth = (gaugeWidth * this.YellowBarWidth) / 100;
                var redBarWidth = (gaugeWidth * this.RedBarWidth) / 100;
                var threshold = (10000 / (this.RedBarWidth)) - 100;
                var markerPosition;

                var greenBarCssClass = 'transparentBar greenTransparent';
                var yellowBarCssClass = 'transparentBar yellowTransparent';
                var redBarCssClass = 'transparentBar redTransparent';

                if (this.Status == 3) {
                    yellowBarCssClass = 'expandedBar yellowExpanded';
                    percentage = percentageCheck(percentage);
                    markerPosition = calculateMarkerPosition(percentage, yellowBarWidth) + greenBarWidth;
                } else if (this.Status == 14) {
                    redBarCssClass = 'expandedBar redExpanded';

                    var results = calculateRedBarWidth(redBarWidth, percentage);
                    var initialRedBarWidth = redBarWidth;

                    if (percentage >= threshold - 5) {
                        var onePercent = gaugeWidth / 100;
                        greenBarWidth = onePercent;
                        yellowBarWidth = onePercent;
                        if (results[0] < gaugeWidth) {
                            markerPosition = calculateMarkerPosition(percentage, initialRedBarWidth) + greenBarWidth + yellowBarWidth;
                        } else {
                            markerPosition = gaugeWidth;
                        }
                        redBarWidth = gaugeWidth - 2 * onePercent;
                    } else {
                        redBarWidth = results[0];
                        var overflow = results[1];

                        greenBarWidth = resizeBarWidth(greenBarWidth, overflow / 2);
                        yellowBarWidth = resizeBarWidth(yellowBarWidth, overflow / 2);
                        redBarWidth += gaugeWidth - (redBarWidth + yellowBarWidth + greenBarWidth);
                        markerPosition = calculateMarkerPosition(percentage, initialRedBarWidth) + greenBarWidth + yellowBarWidth;
                    }
                } else {
                    greenBarCssClass = 'expandedBar greenExpanded';
                    percentage = percentageCheck(percentage);
                    markerPosition = calculateMarkerPosition(percentage, greenBarWidth);
                }

                var internalDiv = $('<div/>').addClass('linearGaugeContainer').css({ 'width': gaugeWidth + 'px', 'height': gaugeHeight + 'px' }).appendTo(div);
                $('<div/>').addClass(greenBarCssClass).css({ 'width': Math.floor(greenBarWidth) + 'px', 'float': 'left' }).appendTo(internalDiv);
                $('<div/>').addClass(yellowBarCssClass).css({ 'width': Math.floor(yellowBarWidth) + 'px', 'float': 'left' }).appendTo(internalDiv);
                $('<div/>').addClass(redBarCssClass).css({ 'width': Math.floor(redBarWidth) + 'px', 'float': 'left' }).appendTo(internalDiv);
                $('<div/>').addClass('markerImage').css({ 'left': (markerPosition - 5) + 'px' }).appendTo(internalDiv);
                return div.html();
            }

            return div.html();
        };

        var calculateRedBarWidth = function(width, percentage) {
            if (percentage < 100) {
                return [width, 0];
            } else {
                var overflow = percentage - 100;
                return [((width * overflow) / 100) + width, overflow];
            }
        };

        var resizeBarWidth = function(oldWidth, overflow) {
            var diff = (oldWidth * overflow) / 100;
            return oldWidth - diff;
        };

        var calculatePercentage = function(value, warnThreshold, critThreshold, status) {
            if (status == 1) {
                return (value / warnThreshold) * 100;
            } else if (status == 3) {
                return ((value - warnThreshold) / (critThreshold - warnThreshold)) * 100;
            } else if (status == 14) {
                var allGauge = (critThreshold * 100 / 75);
                return ((value - critThreshold) / (allGauge - critThreshold)) * 100;
            }
        };

        var calculateMarkerPosition = function(percentage, width) {
            return (width * percentage) / 100;
        };

        var percentageCheck = function(value) {
            if (value < 0) {
                return 0;
            }
            if (value > 100 || value == null || value == undefined) {
                return 100;
            }
            return value;
        };

        var drawLinearGauge = function(status, warnThreshold, critThreshold) {
            if (((critThreshold == 0) || critThreshold) && ((warnThreshold == 0) || warnThreshold) && (status == 1 || status == 3 || status == 14)) {
                return true;
            } else {
                return false;
            }
        };
    }
});