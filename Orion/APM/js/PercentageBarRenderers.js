﻿SW.APM = SW.APM || {};
SW.APM.Renderers = SW.APM.Renderers || {};

SW.APM.Renderers.PercentageBarRenderer = function (element) {
    var el = $(element);
    var bar = $("<span/>")
        .addClass("BarBackground")
        .css("float", "right") // let's align it to the right, because we want to be on the same line as inner text
        .width("100px");
    // due to float bug in IE 7, we have to use prepend() and not append() method
    el.prepend(bar);

    var status = el.attr('status') || 'up';
    var cssClass = 'GreenBar';
    if (status != 'up')
        cssClass = status == 'critical' ? 'RedBar' : 'YellowBar'; // warning is the last expected value
    var usedPercentPoints = 0;
    var barsToUpdate = [];
    $.each(["value", "value2"], function (index, attribName) {
        var percentPoints = Math.round(el.attr(attribName));
        if (!isNaN(percentPoints)) {
            // let's subtract percent points already displayed
            if (percentPoints > 100) percentPoints = 100;
            percentPoints -= usedPercentPoints;
            usedPercentPoints += percentPoints;
            var percentageBar = $("<span/>")
                .css("float", "left") // let's display all bars from the left
                .addClass(index == 0 ? cssClass : "GrayPercentageBar"); // the class will add color to the bar
            bar.append(percentageBar);
            barsToUpdate.push({
                jqBar: percentageBar,
                percentPoints: percentPoints
            });
        }
    });

    // we don't want to use JQuery animate() function, because we want to draw bars sequentially
    var update = function (barsToUpdate) {
        if (barsToUpdate.length > 0) {
            var config = barsToUpdate.shift();
            config.jqBar.animate({
                width: config.percentPoints
            },
                1000,
                function () {
                    update(barsToUpdate);
                });
        }
    };
    update(barsToUpdate);
};
