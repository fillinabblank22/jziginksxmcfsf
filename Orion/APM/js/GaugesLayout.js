﻿
APMjs.initGlobal('SW.APM.GaugesLayout', function (gaugesLayout) {
    'use strict';

    var adjustColumns = function(containerId, gaugeWidth) {
        var containerWidth = $('#' + containerId).width();
        var columnsCount = Math.max(Math.floor(containerWidth / gaugeWidth), 1);

        var children = $('#' + containerId).children();
        var fullRowsCount = Math.floor(children.length / columnsCount);
        var lastRowFirstIndex = fullRowsCount * columnsCount;
        var lastRowColumnsCount = children.length - lastRowFirstIndex;

        var fullRowColumnWidth = Math.floor(containerWidth / columnsCount);
        var lastRowColumnWidth = 0;
        if (lastRowColumnsCount > 0) {
            lastRowColumnWidth = Math.floor(containerWidth / lastRowColumnsCount);
        }

        children.slice(0, lastRowFirstIndex).width(fullRowColumnWidth);
        children.slice(lastRowFirstIndex, children.length).width(lastRowColumnWidth);
    };

    gaugesLayout.checkResize = function(containerId, gaugeWidth, lastContainerWidth) {
        var currentContainerWidth = $('#' + containerId).width();
        if (currentContainerWidth !== lastContainerWidth) {
            lastContainerWidth = currentContainerWidth;
            adjustColumns(containerId, gaugeWidth);
        }
		
		return lastContainerWidth;
    };
	
	var rescheduleCheckResize = function(containerId, gaugeWidth, lastContainerWidth) {
		setInterval(function(){
				lastContainerWidth = gaugesLayout.checkResize(containerId, gaugeWidth, lastContainerWidth);
			}, 500);
	};
	
	gaugesLayout.startCheckResize = function(containerId, gaugeWidth) {
		rescheduleCheckResize(containerId, gaugeWidth, 0);
	};
});