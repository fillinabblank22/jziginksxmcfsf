﻿/*jslint browser: true*/
/*global APMjs: false*/
APMjs.initGlobal('SW.APM.NodeManagement', function (NodeManagement, APMjs) {
    'use strict';

    var $ = APMjs.assertQuery(),
        CoreServices = APMjs.assertGlobal('SW.Core.Services');

    function getErrorMessage(prefix, message) {
        return (message.indexOf(prefix) === -1)
            ? prefix + ": " + message
            : message;
    }

    function progressDialog(options) {
        // this is slightly modified Copy&Paste from PollRediscoverDialog.ascx, grrr, but I was not allowed to reuse the original function
        var $statusBox, $statusContent, $statusText, $progressBarOuter, $progressBarInner,
            numFinished = 0,
            numSucceeded = 0,
            width = 600;

        function hideStatus() {
            $statusBox
                .fadeOut('slow', function () {
                    $statusBox
                        .dialog('destroy')
                        .remove();
                });
        }

        function onFinished(succeeded) {
            numFinished += 1;
            numSucceeded += 1;
            $progressBarInner.width((numFinished / options.items.length) * 100.0 + '%');
            if (succeeded && numSucceeded >= options.items.length) {
                setTimeout(hideStatus, 1000);
            }
        }

        $statusBox = $('<div>')
            .addClass('StatusDialog')
            .width(width);

        $statusContent = $('<div>')
            .appendTo($statusBox);

        $progressBarOuter = $('<div>')
            .addClass('ProgressBarOuter')
            .appendTo($statusContent);

        $progressBarInner = $('<div>')
            .addClass('ProgressBarInner')
            .appendTo($progressBarOuter);

        $statusText = $('<div>')
            .addClass('NetObjectText')
            .appendTo($statusContent);

        $('<orion:LocalizableButtonLink runat="server" LocalizedText="Cancel" DisplayType="Secondary" style="float:right; margin-right:5px;" />')
            .appendTo($statusText)
            .click(function () {
                $statusBox.dialog('destroy').remove();
            });

        $('body').append($statusBox);

        function getIcon(success, options, error) {
            var $icon = $('<span class="iconLink">');
            if (success) {
                $icon.addClass('success');
                $icon.html(options.sucessMessage);
            } else {
                $icon.addClass('failed');
                $icon.html(getErrorMessage(options.failMessage, error));
            }
            return $icon;
        }

        APMjs.linqEach(options.items, function (item) {
            $statusText.append('&nbsp;&nbsp;');
            options.serverMethod(
                options.getArg(item),
                function (retValue) {
                    getIcon(retValue.InitiatedSuccessfully, options, retValue.ErrorMessage)
                        .appendTo($statusText);
                    if (options.afterSucceeded) {
                        options.afterSucceeded(item);
                    }
                    onFinished(retValue.InitiatedSuccessfully);
                },
                function (error) {
                    getIcon(false, options, error)
                        .appendTo($statusText);
                    onFinished(false);
                }
            );
        });

        $statusBox.dialog({
            width: width,
            height: $statusContent.height() + 100,
            modal: true,
            overlay: {
                'background-color': 'black',
                opacity: 0.4
            },
            title: options.title
        });
    }

    function showReboot(id, isDemo) {
        if (isDemo) {
            APMjs.demoModeAction("NodeManagement_Reboot");
            return;
        }
        progressDialog({
            title: '@{R=APM.Strings;K=ApmWeb_JEL_RebootInitiation;E=js}',
            items: [id],
            serverMethod: function (args, fnSuccess, fnError) {
                CoreServices.callWebService(
                    '/Orion/APM/Services/NodeManagement.asmx',
                    'Reboot',
                    args,
                    fnSuccess,
                    fnError
                );
            },
            getArg: function (id) {
                return { nodeId: id };
            },
            sucessMessage: '@{R=APM.Strings;K=ApmWeb_JEL_RebootCompleted;E=js}',
            failMessage: '@{R=APM.Strings;K=ApmWeb_JEL_RebootFailed;E=js}'
        });
    }

    function rebootWarningClick(nodeId, isDemo) {
        showReboot(nodeId, isDemo);
        return false;
    }

    function showRebootOrionWarning(nodeId, isDemo) {
        APMjs.assertExt().Msg.confirm(
            '@{R=APM.Strings;K=ApmWeb_JEL_RebootInitiation;E=js}',
            '@{R=APM.Strings;K=ApmWeb_JEL_RebootOrionServerWarning;E=js}',
            function (btn) {
                if (btn === 'yes') {
                    rebootWarningClick(nodeId, isDemo);
                }
            }
        );
    }

    function nodeRebootClick(nodeId, nodeName, isDemo, isOrionWebServer) {
        APMjs.assertExt().Msg.confirm(
            '@{R=APM.Strings;K=ApmWeb_JEL_RebootInitiation;E=js}',
            APMjs.format('@{R=APM.Strings;K=ApmWeb_JEL_RebootQuestion;E=js}', nodeName),
            function (btn) {
                if (btn === 'yes') {
                    if (isOrionWebServer) {
                        showRebootOrionWarning(nodeId, isDemo);
                    } else {
                        showReboot(nodeId, isDemo);
                    }
                }
            }
        );
        return false;
    }

    function openRealTimeProcessWindow(nodeid, sort, componentId) {
        var params = 'width=900,height=360,scrollbars=yes,resizable=yes,location=no,toolbar=no,status=no',
            url = '/Orion/APM/Admin/RealtimeProcesses/Default.aspx?NodeId=' + nodeid;
        if (sort) {
            url += '&sort=' + sort;
        }
        if (componentId) {
            url += '&cid=' + componentId;
        }
        window.open(url, 'RealtimeProcessesWindow' + nodeid, params);
        return false;
    }

    function openRealTimeServicesWindow(nodeid, credentialSetId) {
        var width = Math.floor($('body').width() * 0.9),
            height = Math.floor($('body').height() * 0.9),
            params = APMjs.format('width={0},height={1},scrollbars=no,resizable=yes,location=no,toolbar=no,status=no', width, height),
            url = APMjs.format('/Orion/APM/Admin/RealTimeServices/Default.aspx?NodeId={0}&CredentialsId={1}', nodeid, credentialSetId);
        window.open(url, 'RealtimeServicesWindow' + nodeid, params);
        return false;
    }

    function openRealTimeEventExplorerWindow(nodeid, credentialSetId) {
        var width = Math.floor($("body").width() * 0.9),
            height = Math.floor($("body").height() * 0.9),
            params = 'width=' + width + ',height=' + height + ',scrollbars=no,resizable=yes,location=no,toolbar=no,status=no',
            url = '/Orion/APM/Admin/RealTimeEventLogViewer/Default.aspx?NodeId=' + nodeid + '&CredId=' + credentialSetId;
        window.open(url, 'RealtimeEventViewerWindow' + nodeid, params);
        return false;
    }

    // publish methods
    NodeManagement.nodeRebootClick = nodeRebootClick;
    NodeManagement.rebootWarningClick = rebootWarningClick;
    NodeManagement.openRealTimeProcessWindow = openRealTimeProcessWindow;
    NodeManagement.openRealTimeServicesWindow = openRealTimeServicesWindow;
    NodeManagement.openRealTimeEventExplorerWindow = openRealTimeEventExplorerWindow;
});
