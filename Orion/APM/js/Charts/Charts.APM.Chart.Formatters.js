/*jslint browser: true, indent: 4, plusplus: true, continue: true, unparam: true*/
/*global APMjs: false*/
/// <reference path="../../../Charts/js/highstock.js"/>
/// <reference path="../../../Charts/js/Charts.TooltipPositioner.js"/>
/// <reference path="../../../Charts/js/Charts.js"/>
/// <reference path="../../APM.js"/>
APMjs.withGlobal('SW.Core.Charts', function (module, APMjs, global) {
    'use strict';

    var DF, SW;

    SW = APMjs.assertGlobal('SW');

    function formatValue(value, multiplier, numberFormat, unit) {
        var multipliedValue, formattedValue, preciseFormattedValue;
        multipliedValue = value / multiplier;
        formattedValue = multipliedValue.toFixed(numberFormat.decimalPlaces);
        if (APMjs.isSet(numberFormat.customPrecision)) {
            if (numberFormat.customPrecision > numberFormat.decimalPlaces) {
                preciseFormattedValue = (multipliedValue.toFixed(numberFormat.customPrecision)).toString();
                if (preciseFormattedValue.length > formattedValue.length) {
                    formattedValue = preciseFormattedValue;
                }
            }
        }
        // dealing with -0.00
        if (formattedValue === 0) {
            formattedValue = (0).toFixed(numberFormat.decimalPlaces);
        }
        if (APMjs.isStr(unit) && unit.length) {
            formattedValue += '&nbsp;' + unit;
        }
        return formattedValue;
    }

    function getNumberFormat(decimalPlaces, axisOptions) {
        var retval = { decimalPlaces: 2 }; // default precision
        if (APMjs.isNum(decimalPlaces)) {
            retval.decimalPlaces = decimalPlaces;
        }
        if (APMjs.isSet(axisOptions) && APMjs.isSet(axisOptions.customPrecision)) {
            retval.customPrecision = axisOptions.customPrecision;
        }
        return retval;
    }

    function innerFormat(value, orderMultiplier, numberFormat, orderUnits, targetUnit) {
        var currentOrderMultiplier, order, nextOrderMultiplier, unitIndex;
        if (APMjs.isSet(targetUnit)) {
            unitIndex = orderUnits.indexOf(targetUnit);
            if (unitIndex >= 0) {
                return formatValue(value, Math.pow(orderMultiplier, unitIndex), numberFormat, targetUnit);
            }
        }
        if (APMjs.isArr(orderUnits)) {
            currentOrderMultiplier = 1;
            for (order = 0; order < orderUnits.length - 1; order++) {
                nextOrderMultiplier = currentOrderMultiplier * orderMultiplier;
                if (Math.abs(value) < nextOrderMultiplier) {
                    break;
                }
                currentOrderMultiplier = nextOrderMultiplier;
            }
            return formatValue(value, currentOrderMultiplier, numberFormat, orderUnits[order]);
        }
        return value;
    }

    function innerFormatTime(value, noFormatLimit) {
        var limit, tmpValue, result;

        // don't format when value is small
        limit = APMjs.isNum(noFormatLimit) ? noFormatLimit : 300;
        if (value <= limit) {
            return value.toFixed(2) + ' s';
        }

        tmpValue = Math.round(value);
        result = (tmpValue % 60) + 's';
        if (tmpValue >= 60) {
            tmpValue = Math.floor(tmpValue / 60);
            result = (tmpValue % 60) + 'm ' + result;
            if (tmpValue >= 60) {
                result = Math.floor(tmpValue / 60) + 'h ' + result;
            }
        }
        return result;
    }

    module.getUnitFromValue = function (formattedValue) {
        var index = formattedValue.indexOf(' ');
        if (index >= 0) {
            return formattedValue.substr(index + 1);
        }
        return '';
    };

    module.getValidDecimalPlaces = function (formattedValue, requiredPrecision) {
        var precisedValue, matchResult, decimalPlaces, matchNumber;
        if (!APMjs.isNum(requiredPrecision)) {
            requiredPrecision = 2;
        }
        matchResult = formattedValue.match(/\.\d+/g)[0]; // take only decimal point and following digits
        if (APMjs.isSet(matchResult)) {
            precisedValue = parseFloat(matchResult).toPrecision(requiredPrecision);
            decimalPlaces = 0;
            matchNumber = parseFloat(precisedValue).toExponential().match(/\d+(\.\d+)?e-(\d+)/);
            if (matchNumber && matchNumber.length > 2) {
                if (APMjs.isSet(matchNumber[1])) {
                    // do not count decimal point
                    decimalPlaces += matchNumber[1].length - 1;
                }
                if (APMjs.isSet(matchNumber[2])) {
                    decimalPlaces += parseInt(matchNumber[2], 10);
                }
            }
            return Math.max(requiredPrecision, decimalPlaces);
        }
        return 0;
    };

    module.formatValue = function (rawValue, formatOptions, edgeValues) {
        var formattedValue, currentUnit, formattedDiff, fnFormat, customPrecisions;
        formattedValue = SW.Core.Charts.dataFormatters.any(rawValue, formatOptions);

        if (APMjs.isSet(edgeValues)
                && edgeValues.length > 0
                && APMjs.isSet(formatOptions)
                && APMjs.isSet(formatOptions.unit)
                && formatOptions.unit.indexOf('second') !== 0) {

            currentUnit = SW.Core.Charts.getUnitFromValue(formattedValue);
            fnFormat = function format(value, decimalPlaces) {
                return SW.Core.Charts.dataFormatters.any(value, formatOptions, decimalPlaces, currentUnit);
            };

            customPrecisions = [];
            APMjs.linqEach(edgeValues, function (value, index) {
                if (fnFormat(value, null) === formattedValue) {
                    formattedDiff = fnFormat(Math.abs(rawValue - value), 10);
                    customPrecisions.push(SW.Core.Charts.getValidDecimalPlaces(formattedDiff));
                }
            });

            if (customPrecisions.length > 0) {
                formatOptions.customPrecision = Math.max.apply(Math, customPrecisions);
                formattedValue = fnFormat(rawValue, formatOptions.customPrecision);
            }
        }

        return formattedValue;
    };


    // Let modules define their custom formatters before this script. Sometimes it may
    // be difficult to put module script after this script.
    module.dataFormatters = module.dataFormatters || {};
    DF = module.dataFormatters;

    DF.statistic = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1000, getNumberFormat(decimalPlaces, axis), ['', 'K', 'M', 'G', 'T'], targetUnit);
    };

    DF.byte = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1024, getNumberFormat(decimalPlaces, axis), ['bytes', 'KB', 'MB', 'GB', 'TB'], targetUnit);
    };

    DF.kbyte = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1024, getNumberFormat(decimalPlaces, axis), ['KB', 'MB', 'GB', 'TB'], targetUnit);
    };

    DF.percent = function (value, axis, decimalPlaces, targetUnit) {
        var exp, range, origPlaces = APMjs.isNum(decimalPlaces) ? decimalPlaces : 2;
        if (APMjs.isSet(axis)) {
            if (APMjs.isNum(axis.minData) && APMjs.isNum(axis.maxData)) {
                range = Math.abs(axis.maxData - axis.minData);
            }
        }
        if (!APMjs.isNum(range)) {
            range = Math.abs(value) / 10;
        }
        if (range > 0) {
            exp = Math.min(0, Math.floor(Math.log(range) / Math.LN10));
            if (range < 1) {
                decimalPlaces = origPlaces - 1 - exp;
                if (decimalPlaces > 20) {
                    // In further formatting, there is used function toFixed. It accepts only values from range 0 to 20.
                    // Otherwise some browsers might throw exception.
                    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed
                    // But rounding such a small value to 20 places might result in 20 decimals points full of zeros.
                    decimalPlaces = Math.min(origPlaces, decimalPlaces);
                }
            } else {
                decimalPlaces = origPlaces + exp; // ??? exp == 0
            }
        }
        return innerFormat(value, 1, getNumberFormat(decimalPlaces, axis), ['%']);
    };

    DF.msec = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1000, getNumberFormat(decimalPlaces, axis), ['ms', 's'], targetUnit);
    };

    DF.second = function (value, axis, decimalPlaces) {
        return innerFormatTime(value, 300);
    };

    DF.secondNoLimit = function (value, axis, decimalPlaces) {
        return innerFormatTime(value, 0);
    };

    DF.any = function (value, axis, decimalPlaces, targetUnit) {
        var fn;
        if (APMjs.isSet(axis)) {
            if (axis.unit === '') {
                return value.toFixed(2).toLocaleString();
            }
            fn = SW.Core.Charts.dataFormatters[axis.unit];
            if (APMjs.isFn(fn)) {
                return fn(value, axis, decimalPlaces, targetUnit);
            }
        }
        return value;
    };

    DF.getFriendlyDateString = function (date) {
        var dateToCompare, now, dateStrPart, dif;

        if (!APMjs.isSet(date) || isNaN(date)) {
            return '';
        }

        dateToCompare = new Date(date.valueOf());
        now = new Date();
        if (dateToCompare.setHours(0, 0, 0, 0) === now.setHours(0, 0, 0, 0)) {
            dateStrPart = '@{R=APM.BlackBox.Wstm.Strings;K=APMWEBJS_YP0_1;E=js}';
        } else {
            dif = now.setHours(0, 0, 0, 0) - dateToCompare.setHours(0, 0, 0, 0);
            // 1000 days
            if (dif > 0 && dif <= 86400000) {
                dateStrPart = '@{R=APM.BlackBox.Wstm.Strings;K=APMWEBJS_YP0_2;E=js}';
            } else if (dif < 0 && dif >= -86400000) {
                dateStrPart = '@{R=APM.BlackBox.Wstm.Strings;K=APMWEBJS_YP0_3;E=js}';
            }
        }

        return String.format(
            '@{R=APM.BlackBox.Wstm.Strings;K=APMWEBJS_YP0_4;E=js}',
            APMjs.isSet(dateStrPart) ? dateStrPart : date.toLocaleDateString(),
            date.toLocaleTimeString()
        );
    };

    DF.roundValue = function (value, x) {
        return APMjs.isNum(value) ? Number(value.toFixed(x)) : null;
    };

});
