﻿/*jslint browser: true, eqeq: true*/
/*global $: false, SW: true, Highcharts: false*/
SW = SW || {};
SW.APM = SW.APM || {};
SW.APM.Charts = SW.APM.Charts || {};
SW.APM.Charts.ChartLegend = SW.APM.Charts.ChartLegend || {};
(function(legend) {
    legend.toggleSeries = function (series) {
        if (series.visible) {
            series.hide();
        } else {
            series.show();
        }
    };

    legend.addTitle = function(series, container) {
        var label = $('<span/>');

        if (series.name === 'Work Days') {
            label.html("<img src='/Orion/APM/images/baselineDay.png' onmouseover='$(&quot;div.APM_WorkHours&quot;).show().position({ at: &quot;right bottom&quot;, of: $(this), my: &quot;left bottom&quot; });' onmouseout='$(&quot;div.APM_WorkHours&quot;).hide();' alt='sun_icon' style='position: relative; top: 1px;'>");
        } else if (series.name === 'Evenings and Weekends') {
            label.html("<img src='/Orion/APM/images/baselineNight.png' onmouseover='$(&quot;div.APM_NightAndWeekend&quot;).show().position({ at: &quot;right bottom&quot;, of: $(this), my: &quot;left bottom&quot; });' onmouseout='$(&quot;div.APM_NightAndWeekend&quot;).hide();' alt='moon_icon' style='position: relative; top: 1px;'>");
        } else {
            label.text(series.name);
        }

        label.appendTo(container);
    };

    legend.addLegendSymbol = function(series, container) {
        var
            symbolWidth = 16,
            symbolHeight = 12,
            label = $('<span class="legendSybmol" />'),
            renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);

        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
            .attr({
                stroke: series.color,
                fill: series.color
            })
            .add();

        label.appendTo(container);
    };

    legend.addCheckbox = function(series, container) {
        var check = $('<input type="checkbox" />');

        check
            .click(function() {
                legend.toggleSeries(series);
            });

        if (series.visible) {
            check.attr("checked", 'checked');
        }

        check.appendTo(container);
    };

    legend.addThresholdBand = function(chart, thresholdFrom, thresholdTo, thresholdId, color, tresholdLabel) {
        if (thresholdFrom < thresholdTo) {
            chart.yAxis[0].addPlotBand({
                from: thresholdFrom,
                to: thresholdTo,
                color: color,
                id: 'plot-band-' + thresholdId,
                label: {
                    text: tresholdLabel,
                    align: 'left',
                    verticalAlign: 'top',
                    y: 12
                }
            });
        } else {
            chart.yAxis[0].addPlotBand({
                from: thresholdTo,
                to: thresholdFrom,
                color: color,
                id: 'plot-band-' + thresholdId,
                label: {
                    text: tresholdLabel,
                    align: 'left',
                    verticalAlign: 'top',
                    y: 12
                }
            });
        }
    };

    legend.addThresholdLine = function (chart, threshold, thresholdId, color, tresholdLabel) {
        chart.yAxis[0].addPlotLine({
            value: threshold,
            width: 2,
            color: color,
            id: 'plot-line-' + thresholdId,
            label: {
                text: tresholdLabel,
                align: 'left',
                y: 12
            }
        });
    };

    legend.removePlotBandsAndLines = function (chart) {
        chart.yAxis[0].removePlotBand('plot-band-1');
        chart.yAxis[0].removePlotBand('plot-band-2');
        chart.yAxis[0].removePlotLine('plot-line-1');
        chart.yAxis[0].removePlotLine('plot-line-2');
    };

    legend.drawPlotBands = function (chart) {
        legend.removePlotBandsAndLines(chart);

        var warningValue = null,
            warningLabel = null,
            criticalValue = null,
            criticalLabel = null,
            criticalTopValue = null,
            thresholdStyle = null;

        if (chart.options.APM) {
            warningValue = chart.options.APM.warningThreshold;
            warningLabel = (chart.options.APM.warningThresholdLabel != null) ? chart.options.APM.warningThresholdLabel : "Warning Threshold";
            criticalValue = chart.options.APM.criticalThreshold;
            criticalLabel = (chart.options.APM.criticalThresholdLabel != null) ? chart.options.APM.criticalThresholdLabel : "Critical Threshold";
            criticalTopValue = chart.options.APM.criticalTopThreshold;
            thresholdStyle = chart.options.APM.thresholdStyle;
        }

        if (warningValue == null && criticalValue == null) {
            return;
        }

        if (thresholdStyle === 0) {
            if (warningValue != null && criticalValue != null && warningValue > chart.yAxis[0].min && warningValue < chart.yAxis[0].max) {
                legend.addThresholdBand(chart, warningValue, criticalValue, 1, "rgba(252,255,21,0.2)", warningLabel);
            }
            if (criticalValue != null && criticalTopValue != null && criticalValue > chart.yAxis[0].min && criticalValue < chart.yAxis[0].max) {
                legend.addThresholdBand(chart, criticalValue, criticalTopValue, 2, "rgba(255,0,21,0.2)", criticalLabel);
            }
        } else if (thresholdStyle === 1) {
            if (warningValue != null && warningValue > chart.yAxis[0].min && warningValue < chart.yAxis[0].max) {
                legend.addThresholdLine(chart, warningValue, 1, "rgba(252,255,21,0.2)", warningLabel);
            }
            if (criticalValue != null && criticalValue > chart.yAxis[0].min && criticalValue < chart.yAxis[0].max) {
                legend.addThresholdLine(chart, criticalValue, 2, "rgba(255,0,21,0.2)", criticalLabel);
            }
        } else if (thresholdStyle === 2) {
            if (warningValue > criticalValue) {
                legend.addThresholdBand(chart, criticalValue, criticalTopValue, 1, "rgba(252,255,21,0.2)", warningLabel);
                legend.addThresholdLine(chart, warningValue, 1, "white");
            } else if (criticalValue > warningValue) {
                legend.addThresholdBand(chart, warningValue, criticalTopValue, 1, "rgba(255,0,21,0.2)", criticalLabel);
                legend.addThresholdLine(chart, criticalValue, 1, "white");
            }
        }
    };

    legend.createStandardLegend = function (chart, legendContainerId) {

        var table = $('#' + legendContainerId), row, td;
        table.empty();

        $.each(chart.series, function(index, series) {
            if (series.options.showInLegend) {
                row = $('<tr />');

                td = $('<td style="width:15px;" />').appendTo(row);
                legend.addCheckbox(series, td);

                td = $('<td style="width:20px;" />').appendTo(row);
                legend.addLegendSymbol(series, td);

                td = $('<td/>').appendTo(row);
                legend.addTitle(series, td);

                row.appendTo(table);
            }
        });
    };
}(SW.APM.Charts.ChartLegend));
