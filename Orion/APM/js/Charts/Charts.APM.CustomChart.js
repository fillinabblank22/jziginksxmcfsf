/*jslint indent: 4, plusplus: true, vars: true*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.Charts.CustomChart', function (module, APMjs) {
    'use strict';

    var mapTransformSettings = {}, mapTransformOptions = {};

    function setTransform(id, fnSettings, fnOptions) {
        mapTransformSettings[id] = APMjs.isFn(fnSettings) ? fnSettings : null;
        mapTransformOptions[id] = APMjs.isFn(fnOptions) ? fnOptions : null;
    }

    function transformSettings(id, parameters, settings) {
        var fn = mapTransformSettings[id];
        if (fn) {
            try {
                fn(parameters, settings);
            } catch (ignore) {
            }
        }
        return settings;
    }

    function transformOptions(id, parameters, options) {
        var fn = mapTransformOptions[id];
        if (fn) {
            try {
                fn(parameters, options);
            } catch (ignore) {
            }
        }
        return options;
    }

    module.setTransform = setTransform;
    module.transformSettings = transformSettings;
    module.transformOptions = transformOptions;
});
