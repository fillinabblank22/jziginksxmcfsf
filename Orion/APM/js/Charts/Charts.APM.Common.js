/*jslint indent: 4, plusplus: true, vars: true*/
/*global APMjs: false*/
/// <reference path="../../APM.js"/>
APMjs.initGlobal('SW.APM.Charts.Common', function (common, APMjs) {
    'use strict';

    var dataFormatters;

    // publish templates

    common.AvailabilitySeriesTemplates = {
        Down_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Down;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(145,0,0)"],
                    [0.5, "rgb(205,0,16)"],
                    [1, "rgb(230,25,41)"]
                ]
            }
        },
        Up_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Up;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(63,134,0)"],
                    [0.5, "rgb(93,163,19)"],
                    [1, "rgb(119,189,45)"]
                ]
            }
        },
        Warning_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Warning;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(196,163,0)"],
                    [0.5, "rgb(228,193,16)"],
                    [1, "rgb(252,217,40)"]
                ]
            }
        },
        Critical_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Critical;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(187,95,1)"],
                    [0.5, "rgb(224,132,3)"],
                    [1, "rgb(249,157,28)"]
                ]
            }
        },
        Unknown_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Unknown;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,0,0)"],
                    [0.5, "rgb(50,50,50)"],
                    [1, "rgb(100,100,100)"]
                ]
            }
        },
        NotRunning_Count: {
            name: "@{R=APM.Strings;K=APM_Status_NotRunning;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(63,134,136)"],
                    [0.5, "rgb(95,158,160)"],
                    [1, "rgb(119,189,191)"]
                ]
            }
        },
        Other_Count: {
            name: "@{R=APM.Strings;K=APM_Status_Other;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(31,62,164)"],
                    [0.5, "rgb(41,82,184)"],
                    [1, "rgb(51,102,204)"]
                ]
            }
        },
        Availability: {
            name: "@{R=APM.Strings;K=APM_ChartSettings_yAxis_Availability;E=js}",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.CustomBarChartSeriesTemplates = {
        Data: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_Data;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(31,62,164)"],
                    [0.5, "rgb(41,82,184)"],
                    [1, "rgb(51,102,204)"]
                ]
            }
        },
        Data_Navigator: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_Navigator;E=js}",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.CustomBarPercentAvailabilityChartSeriesTemplates = {
        Percent_Availability: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_PercentAvailability;E=js}",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(31,62,164)"],
                    [0.5, "rgb(41,82,184)"],
                    [1, "rgb(51,102,204)"]
                ]
            }
        }
    };

    common.CustomLineChartSeriesTemplates = {
        Data: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_Data;E=js}",
            type: "line",
            zIndex: 2
        },
        Data_Navigator: {
            name: "Navigator",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.CustomLinePercentAvailabilityChartSeriesTemplates = {
        Percent_Availability: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_PercentAvailability;E=js}",
            type: "line",
            zIndex: 2
        }
    };

    common.CustomAreaChartSeriesTemplates = {
        Data: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_Data;E=js}",
            type: "area",
            zIndex: 2
        },
        Data_Navigator: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_Navigator;E=js}",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.CustomAreaPercentAvailabilityChartSeriesTemplates = {
        Percent_Availability: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_PercentAvailability;E=js}",
            type: "area",
            zIndex: 2
        }
    };

    common.StackAreaChartSeriesTemplates = {
        Process: {
            type: "area",
            zIndex: 2
        },
        Data_Navigator: {
            name: "@{R=APM.Strings;K=APM_ChartTemplate_Navigator;E=js}",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.TooltipDateFormat = "%A, %b %e %Y, %H:%M";


    // rendering helper functions

    function formatter(data, fmtNoTab, fmtTab) {
        var items, series, header, isTable, fmt, s;

        items = data.points || APMjs.assertGlobal('splat')(data);
        series = items[0].series;

        // build the header
        header = series.tooltipHeaderFormatter(items[0].key);

        // Galaga changed formatting to use tables so we have to count with it.
        isTable = (header.indexOf('</tr>') !== -1); // if header contains </tr> then it's a table layout
        fmt = isTable ? fmtTab : fmtNoTab;
        s = [header];

        APMjs.linqEach(items, function (item) {
            s.push(fmt(item));
        });

        s.push(series.chart.options.tooltip.footerFormat || '');

        return s.join('');
    }

    function fmtNoTabAvailability(item) {
        return '<b>' + item.series.name + ':</b> ' + item.point.y + 'x (' + item.point.percentage.toFixed(2) + '%)<br />';
    }

    function fmtNoTabStatistic(item) {
        return '<b>' + item.series.name + ':</b> (' + item.point.y.toFixed(2) + '%)<br />';
    }

    function fmtNoTabTopProcesses(item) {
        return '<b>' + item.series.name + ':</b> ' + item.point.y.toFixed(2) + '%<br />';
    }

    function fmtTab(item, value, unit, inBracket) {
        var color = item.series.color;
        if (APMjs.isObj(color) && color.stops && color.stops.length) {
            color = color.stops[color.stops.length - 1][1];
        }
        return '<tr style="line-height: 90%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: ' +
            color + ';">' + item.series.name + ': </td><td style="border: 0px; font-size: 12px"><b> ' +
            (inBracket ? '(' : '') + value + unit + (inBracket ? ')' : '') +
            '</b></td></td></tr>';
    }

    function formatStatistic(value, axis, decimalPlaces) {
        dataFormatters = dataFormatters || APMjs.assertGlobal('SW.Core.Charts.dataFormatters');
        return dataFormatters.statistic(value, axis, decimalPlaces);
    }

    function fmtTabAvailability(item) {
        var value = item.point.percentage.toFixed(2);
        return fmtTab(item, value, '%', true);
    }

    function fmtTabStatistic(item) {
        var value = formatStatistic(item.point.y, item.series.yAxis, 2);
        return fmtTab(item, value, '', true);
    }

    function fmtTabTopProcesses(item) {
        var value = formatStatistic(item.point.y, item.series.yAxis, 2);
        return fmtTab(item, value, '%', false);
    }

    // publish tooltip templates

    common.AvailabilityTooltip = {
        xDateFormat: common.TooltipDateFormat,
        formatter: function () {
            return formatter(this, fmtNoTabAvailability, fmtTabAvailability);
        }
    };

    common.StatisticDataTooltip = {
        xDateFormat: common.TooltipDateFormat,
        formatter: function () {
            return formatter(this, fmtNoTabStatistic, fmtTabStatistic);
        }
    };

    common.TopProcessDataTooltip = {
        xDateFormat: common.TooltipDateFormat,
        formatter: function () {
            return formatter(this, fmtNoTabTopProcesses, fmtTabTopProcesses);
        }
    };
});
