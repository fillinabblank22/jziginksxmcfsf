﻿/*jslint browser: true*/

window.APMjs.makeNamespace('SW.APM.Charts.TooltipHilite');

(function init_ChartsApmTooltipHilites_module(global, module) {
    'use strict';

    var APMjs = global.APMjs, SW = global.SW;

    module.initializeStandardChart = function tooltipHiliteInitializeStandardChart(chartSettings, chartOptions) {
        var chartHilite = {};
        chartOptions = chartOptions || {};
        APMjs.ensureObject(chartOptions, 'chart.events');
        // init series tooltip hilite state
        chartOptions.chart.events.addSeries = function tooltipHiliteAddSeries() {
            this.swState = '';
        };
        APMjs.ensureObject(chartOptions, 'plotOptions.series.events');
        // series tooltip hilite state update
        chartOptions.plotOptions.series.events.mouseOver = function tooltipHiliteMouseOver() {
            var series = this, update = false;
            if (chartHilite.lastActiveSeries) {
                if (chartHilite.lastActiveSeries !== series) {
                    chartHilite.lastActiveSeries.swState = '';
                    update = true;
                }
            } else {
                update = true;
            }
            if (update) {
                chartHilite.lastActiveSeries = series;
                series.swState = 'hover';
                series.chart.redraw();
            }
        };
        chartOptions = addFormattingOptions(chartOptions);
        
        APMjs.ensureObject(chartOptions, 'tooltip');
        chartOptions.tooltip.pointFormat = '<tr style="line-height: 90%;" class="{series.swState}"><td style="border: 0; font-size: 12px; color: {series.color};">{series.name}: </td><td style="border: 0px; font-size: 12px"><b>{point.y}</b></td></td></tr>';
        // call original init function with modified options
        SW.Core.Charts.initializeStandardChart(chartSettings, chartOptions);
    };

    module.initializeChartFormating = function xDateAndTimeLabelFormats(chartSettings, chartOptions) {
        chartOptions = addxDateFormat(chartOptions);
        chartOptions = addFormattingOptions(chartOptions);
        
        // call original init function with modified options
        SW.Core.Charts.initializeStandardChart(chartSettings, chartOptions);
    };
    
    function addFormattingOptions(chartOptions) {
        APMjs.ensureObject(chartOptions, 'plotOptions.series.dataGrouping.dateTimeLabelFormats');
        
        // datetime formats for the header of the tooltip
        // for each of these array definitions: the first item is the point or start value, the second is the start value if we're dealing with range,
        // the third one is the end value if dealing with a range
        chartOptions.plotOptions.series.dataGrouping.dateTimeLabelFormats = {
            millisecond: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMilisecond;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMilisecondBegin;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMilisecondEnd;E=js}'],
            second: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatSecond;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatSecondBegin;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatSecondEnd;E=js}'],
            minute: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMinute;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMinuteBegin;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMinuteEnd;E=js}'],
            hour: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatHour;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatHourBegin;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatHourEnd;E=js}'],
            day: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatDay;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatDayBegin;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatDayEnd;E=js}'],
            week: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatWeek;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatWeekBegin;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatWeekEnd;E=js}'],
            month: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMonth;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMonthBegin;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatMonthEnd;E=js}'],
            year: ['@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatYear;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatYear;E=js}', '@{R=APM.Strings;K=ChartsApmDateTimeLabelFormatYearEnd;E=js}']
        };
        return chartOptions;
    }
    
    function addxDateFormat(chartOptions) {
        APMjs.ensureObject(chartOptions, 'tooltip');

        chartOptions.tooltip.xDateFormat = '@{R=APM.Strings;K=ChartsApmxDateFormat;E=js}';
        return chartOptions;
    }
}(window, window.SW.APM.Charts.TooltipHilite));
