﻿SW = SW || {};
SW.APM = SW.APM || {};
SW.APM.Charts = SW.APM.Charts || {};
SW.APM.Charts.MultiValueStatisticChartLegend = SW.APM.Charts.MultiValueStatisticChartLegend || {};
(function (legend) {
    legend.createStandardLegend = function (chart, legendContainerId) {
        var table = $('#' + legendContainerId);
        var numSeries = chart.series.length;
        table.empty();

        // recreate the header
        var headerRow = $('<tr />').appendTo(table);
        var headerTd = $('<td class="ReportHeader APM_ReportHeader" colspan="2" />').appendTo(headerRow);
        headerTd.html('@{R=APM.Strings;K=APMWEBJS_DO1_02;E=js}');
        headerTd = $('<td class="ReportHeader APM_ReportHeader" />').appendTo(headerRow);
        headerTd.html('@{R=APM.Strings;K=APMWEBJS_DO1_03;E=js}');
        headerTd = $('<td class="ReportHeader APM_ReportHeader" />').appendTo(headerRow);
        headerTd.html('@{R=APM.Strings;K=APMWEBJS_DO1_04;E=js}');

        for (var i = 0; i < numSeries; i++) {
            if (chart.series[i].options.id == 'highcharts-navigator-series')
                continue;

            var row = $('<tr />').appendTo(table);
            var td = $('<td class="Property APM_Property APM_LegendColorIconCell" />').appendTo(row);
            var legendIcon = $('<span class="APM_LegendColorIcon" style="background-color: ' + chart.series[i].color + '" />').appendTo(td);
            td = $('<td class="Property APM_Property APM_LegendItemName" />').appendTo(row);
            td.html(chart.options.transactions[i].name);
            td = $('<td class="Property APM_Property" />').appendTo(row);
            td.html('<span class="' + chart.options.transactions[i].cssClass + '">' + chart.options.transactions[i].value + '</span>');
            td = $('<td class="Property APM_Property" />').appendTo(row);
            td.html(chart.options.transactions[i].message);
        }
    };

}(SW.APM.Charts.MultiValueStatisticChartLegend));