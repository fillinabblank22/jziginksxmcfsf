﻿SW = SW || {};
SW.APM = SW.APM || {};
SW.APM.Charts = SW.APM.Charts || {};
SW.APM.Charts.AvailabilityChartLegend = SW.APM.Charts.AvailabilityChartLegend || {};
(function (legend) {

    legend.toggleSeries = function (series) {
        series.visible ? series.hide() : series.show();
    };

    legend.addCheckbox = function (series, container) {

        var check = $('<input type="checkbox"/>')
                    .click(function () { legend.toggleSeries(series); });

        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };

    legend.addTitle = function (series, container) {
        var label = $('<span/>');
        label.text(series.name);
        label.appendTo(container);
    };

    legend.addLegendSymbol = function (series, container) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        label.html('&nbsp;');

        label.css({
            height: symbolHeight,
            width: symbolWidth,
            'background-color': series.color.stops[series.color.stops.length - 1][1],
            display: 'inline-block'
        });

        label.appendTo(container);
    };

    legend.createStandardLegend = function (chart, legendContainerId) {
        var div = $('#' + legendContainerId);
        var wrapper = $('<tr/>');
        div.empty();

        for (var i = chart.series.length - 1, index = 0; i >= 0; i--) {
            var series = chart.series[i];

            if (!series.options.showInLegend)
                continue;

            //var elem = $('<td style="width:15px;"/>').appendTo(wrapper);
            //legend.addCheckbox(series, elem);

            var elem = $('<td style="width:20px;"/>').appendTo(wrapper);
            legend.addLegendSymbol(series, elem);

            elem = $('<td class="sw-APM-title"/>').appendTo(wrapper);
            legend.addTitle(series, elem);

            if (index % 2 != 0) {
                wrapper.appendTo(div);
                wrapper = $('<tr />');
            }

            if (i == 0 && index % 2 == 0) {
                wrapper.appendTo(div);
            }

            index++;
        }
    };
}(SW.APM.Charts.AvailabilityChartLegend));