﻿SW = SW || {};
SW.APM = SW.APM || {};
SW.APM.Charts = SW.APM.Charts || {};
SW.APM.Charts.ApplicationHealthOverviewLegend = SW.APM.Charts.ApplicationHealthOverviewLegend || {};
(function(legend) {
    legend.createStandardLegend = function(chart, legendContainerId) {
        var table = $('#' + legendContainerId);
        var serie = chart.series[0];
        var row = $('<tr />');

        // we need to clean the table, because of async refresh
        table.empty();

        $('<td colspan = 6 ><b>@{R=APM.Strings;K=APMWEBJS_DO1_01;E=js} ' + serie.data[0].size + '</b></td>').appendTo(row);
        row.appendTo(table);

        row = $('<tr />');
        $('<td colspan = 6 > </td>').appendTo(row);
        row.appendTo(table);

        row = $('<tr />');
        
        for (var i = serie.data.length-1, index = 0; i >= 0; i--) {
            var point = serie.data[index];
            var targetUrl = "/Orion/APM/Resources/ApplicationHealthDetails.aspx?SelectedStatus=" + point.name + "&amp;Limit=" + point.limitation + "&amp;Filter=" + point.filter + "&amp;ViewID=" + point.viewid;

            $('<td class="apm_HealthStatusCount' + point.name + '"><b>' + point.count + '</b></td>').appendTo(row);
            $('<td class="apm_HealthStatusIcon"><a href="' + targetUrl + '" target=""_blank""><img src="/Orion/APM/Images/StatusIcons/' + point.icon + '"/></a></td>').appendTo(row);
            $('<td class="apm_HealthStatusName" style="white-space: nowrap;"><a href="' + targetUrl + '" target=""_blank"">' + point.legendLabel + '</a></td>').appendTo(row);
            
            if(index % 2 != 0) {
                row.appendTo(table);
                row = $('<tr />');
            }

            index++;
        }
    };
}(SW.APM.Charts.ApplicationHealthOverviewLegend));