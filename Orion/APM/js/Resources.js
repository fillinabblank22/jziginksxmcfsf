﻿/// <reference path="..\APM.js"/>
/*jslint browser: true*/
/*global $: false, APMjs: false*/
APMjs.initGlobal('SW.APM.Resources', function (Resources) {
    'use strict';

    // \Orion\APM\Resources\Application\AllProcessAndServiceMonitors.ascx
    Resources.setOrder = function (column) {
        $('#OrderBy').val(column);
        return true;
    };

    // \Orion\APM\IisBlackBox\Controls\TopXXPageRequestsByAverageServerExecutionTimeInnerTable.ascx
    Resources.htmlEscape = function (value) {
        if (value === null || value === undefined) {
            return '';
        }

        value = String(value);

        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return value.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    };

    // \Orion\APM\Resources\Summary\InternalTopXXByStatistic.ascx
    $(document).ready(function () {
        var $el = $('table[module-tag="apm-res-InternalTopXXByStatistic"]');
        if ($el.length) {
            if ($el.parent().width() <= 400) {
                $el.css('table-layout', 'fixed');
            }
        }
    });

});
