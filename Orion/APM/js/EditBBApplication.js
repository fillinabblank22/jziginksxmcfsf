/*jslint indent: 4*/
/*global APMjs: false*/
/// <reference path="../APM.js"/>
APMjs.initGlobal('SW.APM.EditBbApp', function (EditBbApp, APMjs) {
    'use strict';

    var Ext = APMjs.assertExt(),
        //regexConsts
        regexScheme = 'https?://',
        regexAuthoritySymbols = "\\w-.~!$&'()*+,;=",
        regexBasicSymbols = regexAuthoritySymbols + ':',
        regexEncodChar = '%[0-9A-F]{2}',
        regexIpv6 = '\\[([0-9a-f]{1,4}:){7}([0-9a-f]{1,4})\\]',
        regexUrl = '^' + regexScheme + '(' +
            //login name @
            '(([' + regexBasicSymbols + ']|' + regexEncodChar + ')+@)?' + '(' +
            //authority part (host name, domain or IPv4 address or almost anything)
            '([' + regexAuthoritySymbols + ']|' + regexEncodChar + ')+' +
            // IPv6 address
            '|(' + regexIpv6 + ')' + ')' +
            //port number
            '(:(\\d+))?' +
            //path
            '([/]([' + regexBasicSymbols + '@/]|' + regexEncodChar + ')*)?' + ')' +
            //query and fragments
            '(\\?(([' + regexBasicSymbols + '@/?]|' + regexEncodChar + ')*))?' +
            '(#(([' + regexBasicSymbols + '@/?]|' + regexEncodChar + ')*))?' + '$',
        reUrl = new RegExp(regexUrl, 'i'),
        VARS = {
            'IP': '0.0.0.0'
        },
        MSG = {
            urlFormat: '@{R=APM.Strings;K=EditApplicationSettingsValidatorErrorUrlFormat;E=js}'
        };

    function isNonEmptyText(text) {
        return APMjs.isStr(text) && !!APMjs.trim(text).length;
    }

    function showError(text, messages, jError) {
        if (isNonEmptyText(text)) {
            if (messages) {
                messages.push(text);
            }
            if (jError) {
                jError
                    .show()
                    .find('.validationError')
                    .html(text);
            }
        } else {
            if (jError) {
                jError
                    .hide()
                    .find('.validationError')
                    .html('');
            }
        }
    }

    function replaceVars(text) {
        if (!isNonEmptyText(text)) {
            return '';
        }
        APMjs.linqEach(VARS, function (value, key) {
            text = text.replace('${' + key + '}', value);
        });
        return text;
    }

    function validateUrlInternal(value) {
        var text = replaceVars(value);
        return reUrl.test(text);
    }

    function validateUrl(value, messages, jqError) {
        var validationResult = validateUrlInternal(value),
            errorText = null;
        if (!validationResult) {
            errorText = MSG.urlFormat;
        }
        showError(errorText, messages, jqError);
        return validationResult;
    }

    function initToolTip(target, message) {
        new Ext.ToolTip({
            target: target,
            title: message,
            showDelay: 500,
            autoHide: false,
            trackMouse: false,
            closable: true,
            anchor: 'left'
        }).addClass('tooltipYellow');
    }

    function findSetting(settingPropertyName, settingsCollection) {
        var foundSetting = null;
        APMjs.linqAny(settingsCollection, function (sett) {
            if (sett.Key === settingPropertyName) {
                foundSetting = sett;
                return true;
            }
        });
        return foundSetting;
    }

    EditBbApp.isNonEmptyText = isNonEmptyText;
    EditBbApp.validateUrl = validateUrl;
    EditBbApp.initToolTip = initToolTip;
    EditBbApp.findSetting = findSetting;
});
