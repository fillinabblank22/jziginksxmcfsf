﻿Ext.ns("SW.APM.Admin");

SW.APM.Admin.TestResult = function (pConfig) {
    var SF = String.format;
    var mNEW_CRED_ID = -1, mNODE_CRED_ID = -3, mTPL_CRED_ID = -4, mTEST_PING_TIMEOUT = 10000;
    var mUrl = "/Orion/APM/Services/ComponentTest.asmx/";
    var mPanel = null, mGrid = null, mStore = null;

    /*helper members*/
    function setBaseParams(pNodeID, pNodeName, pCredID) {
        var params = { key: pConfig.sessionKey, isAppMode: pConfig.isAppMode, credID: pConfig.credID, cmpIDs: [] };

        if (pNodeID) { params.nodeID = pNodeID; }
        if (pNodeName) { params.nodeName = pNodeName; }
        if (pCredID) { params.credID = pCredID; }
        $.each(pConfig.components, function () { params.cmpIDs.push(this[0]); });

        mStore.baseParams.jsonData = params;
    }
    function setBaseNode(pNodeID, pNodeName) {
        mPanel.getFooterToolbar().get(2).setValue(pNodeID);
        $(SF("#ctrCb{0}", pConfig.renderTo)).val($("<div/>").html(pNodeName).text());
    }
    function errorHandler() {
        var req = null;
        for (var i = 0, arg = arguments[i]; req == null && i < arguments.length; arg = arguments[++i]) {
            if (arg.responseText) {
                if (arg.status == 200) { i = arg.length; } else { req = arg; }
            }
        }
        mStore.removeAll();
        if (req) {
            eval(SF("var obj = {0}", req.responseText));
            mStore.add(new mStore.recordType({ ID: 0, Name: "@{R=APM.Strings;K=APMWEBJS_AK1_24;E=js}", Message: obj.Message, Status: "NotAvailable" }));
        }
        mStore.commitChanges();
    }

    /*render members*/
    function renderName(pVal, pMeta, pRec) {
        return SF('<div class="tr-name">{0}<div>', pRec.get("Name"));
    }
    function renderMessage(pVal, pMeta, pRec) {
        var msg = pRec.get("Message"), status = pRec.get("Status") || "Undefined";
        if (msg) {
            return SF('<div class="tr-message tr-{0}">{1}<div>', status, msg);
        }

        return String.format('<div class="tr-message"><img src="/Orion/APM/Images/Admin/animated_loading_16x16.gif"> {0}</div>',
                          '@{R=APM.Strings;K=APMWEBJS_AK1_16;E=js}');
    }

    /*event handlers*/
    function onLoad(pStore, pRecs, pOpt) {
        mGrid.expand(false);
        $.each(pRecs, function () { if (this.get("Message") == null) { setTimeout(function () { mStore.reload(); }, mTEST_PING_TIMEOUT); return false; } });
    }
    function onStartTest() {
        var params = mStore.baseParams.jsonData;
        if (params.credID != mNODE_CRED_ID && params.credID != mTPL_CRED_ID) {
            var credID = parseInt($(SF("select[id^={0}_ctrCreds]", pConfig.cntID)).val());
            if (credID == mNEW_CRED_ID) {
                $(SF("#{0}_ctrCredValidate", pConfig.cntID)).click(); return;
            }
            params.credID = credID;
        }

        mGrid.expand(false);

        mStore.removeAll();
        mStore.add(new mStore.recordType({ ID: 0, Name: null, Message: null, Status: null }));
        mStore.commitChanges();

        $.ajax({
            type: "POST", url: mUrl + "StartTestResult", contentType: "application/json; charset=utf-8", dataType: "json", data: Ext.encode(params),
            success: function (pResp) { mStore.reload(); }, error: errorHandler
        });
        return false;
    }
    function onNodeSelect(pCombo, pRec, pIndex) {
        setBaseParams(pRec.get("ID"), pRec.get("Name"));

        $(SF("#{0}_ctrNodes input[type=radio][onclick][value={1}]", pConfig.cntID, pRec.get("ID")))
			.click();
        return false;
    }

    /*ctor*/
    function ctor() {
        Ext.QuickTips.init();

        var node = pConfig.nodes[0];

        mStore = new Ext.data.JsonStore({
            url: mUrl + "LoadTestResult", remoteSort: false, root: "d", fields: ["ID", "Name", "Message", "Status"],
            autoLoad: true, autoDestroy: true, listeners: { load: onLoad, loadexception: errorHandler }
        });
        setBaseParams(node[0], node[1]);

        var config = {
            title: "@{R=APM.Strings;K=APMWEBJS_AK1_17;E=js}", collapsible: true, collapsed: true,
            autoHeight: true, border: true, trackMouseOver: false, disableSelection: true, stripeRows: true, loadMask: false, viewConfig: { forceFit: true, enableRowBody: true },
            store: mStore,
            cm: new Ext.grid.ColumnModel([
				{ header: "@{R=APM.Strings;K=APMWEBJS_AK1_18;E=js}", dataIndex: "Name", width: 50, sortable: false, menuDisabled: true, renderer: renderName },
				{ header: "@{R=APM.Strings;K=APMWEBJS_AK1_19;E=js}", dataIndex: "Message", sortable: true, menuDisabled: true, renderer: renderMessage }
			])
        };
        
        mPanel = new Ext.Panel({
            renderTo: pConfig.renderTo, hideHeaders: true, border: false,
            items: [(mGrid = new Ext.grid.GridPanel(config))],
            fbar: [
				{
				    xtype: "button", text: "@{R=APM.Strings;K=APMWEBJS_AK1_20;E=js}", icon: "/Orion/APM/Images/test_credentials_16x16.png", handler: onStartTest
				}, {
				    xtype: "label", html: "@{R=APM.Strings;K=APMWEBJS_AK1_21;E=js}"
				}, {
				    xtype: "combo", id: SF("ctrCb{0}", pConfig.renderTo),
				    store: { xtype: "arraystore", fields: ["ID", "Name", "Status"], data: pConfig.nodes }, valueField: "ID", displayField: "Name", value: node[0],
				    mode: "local", triggerAction: "all", width: 200, editable: false, tpl: ('<tpl for="."><div ext:qtip="' + '@{R=APM.Strings;K=APMWEBJS_AK1_23;E=js}' + ' " class="x-combo-list-item"><img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={Status}"/> {Name}</div></tpl>'),
				    listeners: { select: onNodeSelect }
				}
			]
        });
    } ctor();

    this.update = function (pNodeID, pNodeName, pCredID, pAction) {
        setBaseParams(pNodeID, pNodeName, pCredID);
        setBaseNode(pNodeID, pNodeName);
        if (pAction == "run-test") {
            onStartTest();
        }
    }
}

SW.APM.Admin.TestResult.show = function (pConfig) {
	var key = pConfig.sessionKey;
	if (typeof (window[key]) == "undefined") {
		window[key] = new SW.APM.Admin.TestResult(pConfig);
	}
	return window[key];
}
