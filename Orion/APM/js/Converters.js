﻿SW.APM = SW.APM || {};
SW.APM.Converters = SW.APM.Converters || {};
(function (singleton) {
    singleton.getStatusRanking = function (apmStatus) {
        // ranking is based on SolarWinds.APM.Common.Models.StatusExtension.GetStatusRanking() method
        if (apmStatus == 2) return 110;  // Status.NotAvailable
        if (apmStatus == 12) return 150; // Status.Unreachable
        if (apmStatus == 6) return 210;  // Status.Critical
        if (apmStatus == 5) return 220;  // Status.Warning
        if (apmStatus == 3) return 230;  // Status.PartlyAvailable
        if (apmStatus == 7) return 460;  // Status.IsDisabled
        if (apmStatus == 4) return 490;  // Status.NotLicensed
        if (apmStatus == 0) return 495;  // Status.Undefined
        if (apmStatus == 8) return 499;  // Status.Unmanaged
        if (apmStatus == 1) return 500;  // Status.Available
        return 0;
    };
    singleton.getApmStatusFromCore = function (coreStatus) {
        // returns apm status
        // status is based on SolarWinds.APM.Common.Models.StatusExtension.OrionApmStatus() method
        if (coreStatus == 2) return 2;  // Status.NotAvailable
        if (coreStatus == 12) return 12; // Status.Unreachable
        if (coreStatus == 14) return 6;  // Status.Critical
        if (coreStatus == 3) return 5;  // Status.Warning
        if (coreStatus == 15) return 3;  // Status.PartlyAvailable
        if (coreStatus == 27) return 7;  // Status.IsDisabled
        if (coreStatus == 28) return 4;  // Status.NotLicensed
        if (coreStatus == 0) return 0;  // Status.Undefined
        if (coreStatus == 9) return 8;  // Status.Unmanaged
        if (coreStatus == 1) return 1;  // Status.Available
        return 0;
    };
})(SW.APM.Converters);
