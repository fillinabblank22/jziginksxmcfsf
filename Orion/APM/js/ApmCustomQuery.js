/*jslint browser: true, vars: true*/
/*global APMjs: false */

APMjs.initGlobal('SW.APM.Resources.CustomQuery', function (CustomQuery, APMjs) {
    'use strict';

    var
        $ = APMjs.assertQuery(),
        APM = APMjs.assertGlobal('SW.APM'),
        settings = {},
        re = {
            search: new RegExp('[$][{]SEARCH_STRING[}]', 'g')
        },
        baseSettings = {
            initialPage: 0,
            rowsPerPage: 5,
            searchTextBoxId: '',
            searchButtonId: '',
            allowSort: true,
            showSpanLoader: false
        },
        circular = {},
        renderers = [];


    function getTrimmedText(text) {
        return (typeof text === 'string') ? text.trim() : '';
    }

    function isNonEmptyText(text) {
        return (getTrimmedText(text) !== '');
    }

    function isFunction(func) {
        return typeof func === 'function';
    }

    function toggle($el, show) {
        $el[show ? 'show' : 'hide']();
    }

    function PageManager(pageIndex, pageSize) {
        this.currentPageIndex = pageIndex;
        this.rowsPerPage = pageSize;
        this.totalRowsCount = 0;
    }
    PageManager.prototype = {
        startItem: function () {
            return (this.rowsPerPage * this.currentPageIndex) + 1;
        },
        lastItem: function () {
            // Ask for rowsPerPage + 1 rows so we can detect if this is the last page.
            return (this.rowsPerPage * (this.currentPageIndex + 1)) + 1;
        },
        numberOfPages: function () {
            return Math.ceil(this.totalRowsCount / this.rowsPerPage);
        },
        withRowsClause: function () {
            return ' WITH ROWS ' + this.startItem() + ' TO ' + this.lastItem() + ' WITH TOTALROWS';
        },
        isLastPage: function (rowCount) {
            // We asked for rowsPerPage + 1 rows.  If more than rowsPerPage rows
            // got returned, we know this isn't the last page.
            return rowCount <= this.rowsPerPage;
        }
    };

    function getReferencedColumnIndex(name, prefix, columnIndexLookup) {
        var reColIndex, match;
        reColIndex = new RegExp('^' + prefix + '(.+)$', 'i');
        match = name.match(reColIndex);
        return match ? columnIndexLookup[match[1]] : null;
    }

    function generateColumnInfo(uniqueId, columns) {
        var columnInfo = [],
            indexLookup = {},
            headerTitles = null,
            visibleColumnCounter = 0;

        // Map column text to its index.
        APMjs.linqEach(columns, function (name, index) {
            indexLookup[name] = index;
            columnInfo[index] = {};
        });

        if (settings[uniqueId].headerTitles) {
            headerTitles = settings[uniqueId].headerTitles;
        }

        // Lookup each column's formatting information.
        APMjs.linqEach(columns, function (name, index) {
            var otherColumnIndex;

            if (isNonEmptyText(name) && (name[0] !== '_')) {
                if (headerTitles && (headerTitles.length > visibleColumnCounter)) {
                    columnInfo[index].header = headerTitles[visibleColumnCounter];
                } else {
                    columnInfo[index].header = name;
                }
                columnInfo[index].orderByName = name;
                visibleColumnCounter += 1;
            } else {
                columnInfo[index].header = null;
            }

            otherColumnIndex = getReferencedColumnIndex(name, '_IconFor_', indexLookup);
            if (otherColumnIndex !== null) {
                columnInfo[otherColumnIndex].iconColumn = index;
            }

            otherColumnIndex = getReferencedColumnIndex(name, '_LinkFor_', indexLookup);
            if (otherColumnIndex !== null) {
                columnInfo[otherColumnIndex].linkColumn = index;
            }
        });

        return columnInfo;
    }

    function getCustomFormatter(uniqueId, columnName) {
        var formatters, formatter;
        formatters = settings[uniqueId].customFormatters;
        formatter = formatters && formatters[columnName];
        return isFunction(formatter) ? formatter : null;
    }

    function renderCell(uniqueId, cellValue, rowArray, columnInfo, cellIndex) {
        var $cell, customFormatter, culture;
        culture = APMjs.assertGlobal('Sys.CultureInfo.CurrentCulture');
        $cell = $('<td/>');
        $cell.addClass('column' + cellIndex);

        if (cellValue === null) {
            cellValue = '';
        } else if (Date.isInstanceOfType(cellValue)) {
            cellValue = cellValue.localeFormat(culture.dateTimeFormat.ShortDatePattern) + ' ' + cellValue.localeFormat(culture.dateTimeFormat.LongTimePattern);
        }

        customFormatter = getCustomFormatter(uniqueId, columnInfo.header, cellIndex);
        if (customFormatter) {
            cellValue = customFormatter(cellValue);
        }

        if (columnInfo.iconColumn) {
            $('<img/>')
                .attr('src', rowArray[columnInfo.iconColumn])
                .appendTo($cell);
        }

        if (columnInfo.linkColumn) {
            $('<a/>')
                .attr('href', rowArray[columnInfo.linkColumn])
                .attr('class', 'NoTip')
                .html(cellValue)
                .appendTo($cell);
        } else {
            $('<span/>')
                .html(cellValue)
                .appendTo($cell);
        }

        return $cell;
    }

    function getSearchText(uniqueId) {
        var searchId = getTrimmedText(settings[uniqueId].searchTextBoxId);
        return searchId ? getTrimmedText($('#' + searchId).val()) : '';
    }

    function getPager(uniqueId) {
        return $('#Pager-' + uniqueId);
    }

    function getCurrentPageSize(uniqueId) {
        var pager = getPager(uniqueId),
            pageSize = pager.find('.pageSize').val();
        return (!pageSize || pageSize < 0) ? 1 : pageSize;
    }

    function generateRow(uniqueId, row, columnInfo) {
        var $result = $('<tr/>');
        APMjs.linqEach(row, function (cell, cellIndex) {
            var info = columnInfo[cellIndex];
            if (info.header !== null) {
                renderCell(uniqueId, cell, row, info, cellIndex)
                    .appendTo($result);
            }
        });
        return $result;
    }

    function callIfFunction(fn, that, params) {
        if (isFunction(fn)) {
            return fn.apply(that, params);
        }
    }

    // prevent blinking when not using loader span
    // to match with core CQT behavior
    function initLoader($grid, uniqueId) {
        var $loader, $gridbkp;
        $loader = $('#LoaderSpan-' + uniqueId);
        if (settings[uniqueId].showSpanLoader) {
            $loader.show();
            $grid.hide();
        } else {
            $loader.hide();
            $gridbkp = $grid.clone().insertBefore($grid);
            $grid.hide();
        }
        return {
            $loader: $loader,
            $grid: $grid,
            $gridbkp: $gridbkp
        };
    }

    function freeLoader(data) {
        data.$loader.hide();
        if (data.$gridbkp) {
            data.$gridbkp.remove();
        }
        data.$grid.show();
    }

    function createTableFromQuery(uniqueId, pageIndex, pageSize, orderColumn) {
        var swql, nonSearchSwql, searchSwql, currentOrderBy, whereClause, searchText,
            $errorMsg, $grid, loaderData, gridCls,
            Information = APMjs.assertGlobal('Information'),
            Underscore = APMjs.assertGlobal('_');

        function parseColumnName(fullColumn) {
            var column, colunmParts;
            colunmParts = fullColumn.split(/\s[aA][sS]\s/);
            if (colunmParts.length > 1) {
                column = colunmParts[1];
            } else {
                colunmParts = fullColumn.split('.');
                column = colunmParts[colunmParts.length - 1];
            }
            return column.trim();
        }

        currentOrderBy = $('#OrderBy-' + uniqueId).val();
        nonSearchSwql = $('#SWQL-' + uniqueId).val().trim();
        swql = nonSearchSwql;
        searchSwql = $('#SearchSWQL-' + uniqueId).val().trim();
        $errorMsg = $('#ErrorMsg-' + uniqueId);
        $grid = $('#Grid-' + uniqueId);
        gridCls = getTrimmedText(settings[uniqueId].cls);
        if (gridCls) {
            $grid.addClass(gridCls);
        }
        //Place JavaScript side WHERE clause as AND to current WHERE
        whereClause = getTrimmedText(settings[uniqueId].whereClause);
        if (whereClause) {
            swql = swql + ' AND ' + whereClause;
        }

        // use search SWQL if search text is defined
        searchText = getSearchText(uniqueId);
        if (searchText) {
            swql = searchSwql;
            swql = swql.replace(re.search, searchText);
        }

        if (swql.length === 0) {
            $errorMsg
                .text('@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_NOQUERY;E=js}')
                .show();
            return;
        }

        $errorMsg.hide();

        // 0 means to keep whatever page size is set in page size text box
        if (pageSize === 0) {
            pageSize = getCurrentPageSize(uniqueId);
        }

        var maxRowsPerPage = settings[uniqueId].maxRowsPerPage;

        pageSize = maxRowsPerPage && (pageSize > maxRowsPerPage) ? maxRowsPerPage : pageSize;

        var pageManager = new PageManager(pageIndex, pageSize);
        var swqlUpperCase = swql.toUpperCase();

        var commaIndex = swqlUpperCase.indexOf(',');
        var fromIndex = swqlUpperCase.indexOf('FROM');
        var selectIndex = swqlUpperCase.indexOf('SELECT');
        var endIndex = commaIndex;
        if (endIndex === -1) {
            endIndex = fromIndex;
        }

        var startIndex = selectIndex + 6;
        if (!orderColumn) {
            if (currentOrderBy) {
                orderColumn = currentOrderBy;
            } else if (settings[uniqueId].defaultOrderBy) {
                orderColumn = settings[uniqueId].defaultOrderBy;
            } else if (startIndex < endIndex) {
                orderColumn = parseColumnName(swql.substring(startIndex, endIndex).replace(/TOP\s+[0-9]+\s/, ''));
            } else {
                orderColumn = '1';
            }
        }

        var baseOrderColumn;
        var baseOrderBy = settings[uniqueId].baseOrderBy;
        if (baseOrderBy) {
            if (!$('#BaseOrderBy-' + uniqueId).val()) {
                $('#BaseOrderBy-' + uniqueId).val(baseOrderBy);
            }
            baseOrderBy = $('#BaseOrderBy-' + uniqueId).val();
            var baseOrderColumnBackspaceIndex = orderColumn.indexOf(' ');
            if (baseOrderBy.indexOf(
                    baseOrderColumnBackspaceIndex > -1
                        ? orderColumn.substring(0, baseOrderColumnBackspaceIndex)
                        : orderColumn
                ) > -1) {
                var descIdx = baseOrderBy.indexOf(' DESC');
                if (descIdx > -1) {
                    baseOrderBy = baseOrderBy.substring(0, descIdx);
                } else {
                    var acsIdx = baseOrderBy.indexOf(' ASC');
                    baseOrderBy = acsIdx > -1 ? baseOrderBy.substring(0, acsIdx)  + ' DESC' : baseOrderBy + ' DESC';
                }
                $('#BaseOrderBy-' + uniqueId).val(baseOrderBy);
                baseOrderColumn = baseOrderBy;
            } else {
                baseOrderColumn = baseOrderBy + ', ' + (orderColumn !== '1' ? orderColumn : '');
            }
        }

        if ((swqlUpperCase.indexOf('ORDER BY') === -1) || (swqlUpperCase.substr(-1) === ')')) {
            swql += ' ORDER BY ' + (baseOrderColumn || orderColumn);
        } else {
            var columnsStr = swql.substring(selectIndex + 6, fromIndex);
            var columnsArr = columnsStr.split(',');
            var clearColumnsArr = APMjs.linqSelect(columnsArr, parseColumnName);
            swql = APMjs.format(
                'SELECT {0} FROM ({1}) ORDER BY {2}',
                clearColumnsArr.join(', '),
                swql,
                baseOrderColumn || orderColumn
            );
        }

        if ((settings[uniqueId].allowPaging === undefined) || (settings[uniqueId].allowPaging === true)) {
            swql += pageManager.withRowsClause();
        }

        loaderData = initLoader($grid, uniqueId);

        callIfFunction(settings[uniqueId].onbeforeload);

        Information.Query(swql, function (result) {
            var customResultProcessor, columns, columnInfos;

            customResultProcessor = settings[uniqueId].customResultProcessor;
            if (isFunction(customResultProcessor)) {
                try {
                    customResultProcessor(result);
                } catch (xcp) {
                    $('<div>')
                        .text('customResultProcessor failed!')
                        .prependTo($grid)
                        .append($('<div>').text(xcp).hide());
                }
            }

            pageManager.totalRowsCount = result.TotalRows;
            $grid.empty();

            columns = [];
            columnInfos = generateColumnInfo(uniqueId, result.Columns);

            APMjs.linqEach(columnInfos, function (column) {
                var headerHtml, sortArrow, orderBy, descIndex, $cell2;
                if (column.header) {
                    headerHtml = $('<div/>').text(column.header).text();
                    sortArrow = '';
                    orderBy = '[' + column.orderByName + ']';
                    if (orderBy === orderColumn) {
                        // reverse order on next click
                        descIndex = orderBy.indexOf(' DESC');
                        if (descIndex === -1) {
                            orderBy += ' DESC';
                        } else {
                            orderBy = orderBy.substring(0, descIndex);
                        }
                        sortArrow += '&nbsp;<img class="SortArrow" src="/Orion/images/Arrows/Arrow_Ascending.png" />';
                    } else if (orderColumn === (orderBy + ' DESC')) {
                        sortArrow += '&nbsp;<img class="SortArrow" src="/Orion/images/Arrows/Arrow_Descending.png" />';
                    }

                    $cell2 = $('<td/>')
                        .addClass('ReportHeader')
                        .css('text-transform', 'uppercase');

                    if (settings[uniqueId].allowSort) {
                        $cell2.addClass('Sortable');
                        $cell2.click(function () {
                            $('#OrderBy-' + uniqueId).val(orderBy);
                            createTableFromQuery(uniqueId, pageIndex, pageManager.rowsPerPage, orderBy);
                        });
                        headerHtml += sortArrow;
                    }

                    $cell2.html(headerHtml);
                    columns.push($cell2);
                }
            });

            var $thead = $('<thead>').prependTo($grid);
            var customHeaderGenerator = settings[uniqueId].customHeaderGenerator;

            if (isFunction(customHeaderGenerator)) {
                $thead.append(customHeaderGenerator(columns));
            } else {
                var $headers = $('<tr/>')
                    .addClass('HeaderRow')
                    .appendTo($thead);
                APMjs.linqEach(columns, function ($col) {
                    $headers.append($col);
                });
            }

            var rowsToOutput = result.Rows.slice(0, pageManager.rowsPerPage);

            //Use default rowGenerator if nothing else defined
            var rowGenerator = generateRow;

            //Use underscore if defined
            var underscoreTemplateSelector = getTrimmedText(settings[uniqueId].underscoreTemplateId);
            if (underscoreTemplateSelector) {
                /*jslint unparam: true*/
                rowGenerator = function (uniqueId, row, columnInfos, rowIndex) {
                    var templateHtml, resultHtml;
                    templateHtml = $(underscoreTemplateSelector).html();
                    resultHtml = Underscore.template(templateHtml, {
                        Columns: row,
                        Index: rowIndex
                    });
                    return $(resultHtml);
                };
                /*jslint unparam: false*/
            }

            //Use customRow generator if defined
            var customRowGenerator = settings[uniqueId].customRowGenerator;
            if (isFunction(customRowGenerator)) {
                /*jslint unparam: true*/
                rowGenerator = function (uniqueId, row, columnInfos) {
                    return customRowGenerator(row);
                };
                /*jslint unparam: false*/
            }

            APMjs.linqEach(rowsToOutput, function (row, rowIndex) {
                $grid.append(rowGenerator(uniqueId, row, columnInfos, rowIndex));
            });

            // change classes if explicit striping needed
            if (settings[uniqueId].explicitZebra) {
                $grid
                    .removeClass('NeedsZebraStripes')
                    .addClass('apm_NeedsZebraStripes');
            }

            // TO-DO: move all following renderers to stand-alone classes
            APMjs.linqEach(renderers, function (renderer) {
                $grid
                    .find(renderer.selector)
                    .each(function () {
                        renderer.renderTo(this);
                    });
            });

            callIfFunction(settings[uniqueId].onloaded, null, [$grid]);

            freeLoader(loaderData);

            circular.updatePagerControls(uniqueId, pageManager, result.Rows.length);
        }, function (error) {
            freeLoader(loaderData);
            $errorMsg.text(error.get_message()).show();
        });
    }

    function updatePagerControls(uniqueId, pageManager, rowCount) {
        var pager, pageIndex, startHtml, endHtml, contents,
            html = [],
            showAllText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_SHOWALL;E=js}', //Show all
            displayingObjectsText = '@{R=Core.Strings;K=WEBJS_AK0_54;E=js}', //Displaying objects {0} - {1} of {2}
            pageXofYText = '@{R=Core.Strings;K=WEBJS_JT0_2;E=js}', //Page {0} of {1}
            itemsOnPageText = '@{R=Core.Strings;K=WEBJS_JT0_3;E=js}', // Items on page
            style = 'style="vertical-align:middle"',
            firstImgRoot = '/Orion/images/Arrows/button_white_paging_first',
            previousImgRoot = '/Orion/images/Arrows/button_white_paging_previous',
            nextImgRoot = '/Orion/images/Arrows/button_white_paging_next',
            lastImgRoot = '/Orion/images/Arrows/button_white_paging_last',
            showAll = showAllText,
            haveLinks = false;

        function changePageSize() {
            createTableFromQuery(uniqueId, 0, getCurrentPageSize(uniqueId));
        }

        function changePageNumber() {
            var pageNumber = pager.find('.pageNumber').val();
            if (pageNumber <= 0) {
                pageNumber = 1;
            } else if (pageNumber > pageManager.numberOfPages()) {
                pageNumber = pageManager.numberOfPages();
            }
            createTableFromQuery(uniqueId, pageNumber - 1, pageManager.rowsPerPage);
        }

        pager = getPager(uniqueId);
        pageIndex = pageManager.currentPageIndex;

        if (pageIndex > 0) {
            startHtml = '<a href="#" class="firstPage NoTip">';
            contents = String.format('<img src="{0}.gif" {1}/>', firstImgRoot, style);
            endHtml = '</a>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');

            startHtml = '<a href="#" class="previousPage NoTip">';
            contents = String.format('<img src="{0}.gif" {1}/>', previousImgRoot, style);
            endHtml = '</a>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');

            haveLinks = true;
        } else {
            startHtml = '<span style="color:#646464;">';
            contents = String.format('<img src="{0}_disabled.gif" {1}/>', firstImgRoot, style);
            endHtml = '</span>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');

            startHtml = '<span style="color:#646464;">';
            contents = String.format('<img src="{0}_disabled.gif" {1}/>', previousImgRoot, style);
            endHtml = '</span>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');
        }

        startHtml = APMjs.format(
            pageXofYText,
            '<input type="text" class="pageNumber SmallInput" value="' + (pageManager.currentPageIndex + 1) + '" />',
            pageManager.numberOfPages()
        );

        html.push(startHtml);
        html.push(' | ');

        if (!pageManager.isLastPage(rowCount)) {
            startHtml = '<a href="#" class="nextPage NoTip">';
            contents = APMjs.format('<img src="{0}.gif" {1}/>', nextImgRoot, style);
            endHtml = '</a>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');

            startHtml = '<a href="#" class="lastPage NoTip">';
            contents = APMjs.format('<img src="{0}.gif" {1}/>', lastImgRoot, style);
            endHtml = '</a>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');

            haveLinks = true;
        } else {
            startHtml = '<span style="color:#646464;">';
            contents = APMjs.format('<img src="{0}_disabled.gif" {1}/>', nextImgRoot, style);
            endHtml = '</span>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');

            startHtml = '<span style="color:#646464;">';
            contents = APMjs.format('<img src="{0}_disabled.gif" {1}/>', lastImgRoot, style);
            endHtml = '</span>';

            html.push(startHtml + contents + endHtml);
            html.push(' | ');
        }

        contents = itemsOnPageText;
        endHtml = '<input type="text" class="pageSize SmallInput" value="' + pageManager.rowsPerPage + '" />';

        html.push(contents + endHtml);
        html.push(' | ');

        if (!settings[uniqueId].hideShowAll && pageManager.totalRowsCount <= settings[uniqueId].showAllLimit) {
            html.push('<a href="#" class="showAll NoTip">' + showAll + '</a>');
            html.push(' | ');
        }

        html.push('<div class="ResourcePagerInfo">');
        startHtml = APMjs.format(
            displayingObjectsText,
            pageManager.startItem(),
            Math.min(pageManager.lastItem() - 1, pageManager.totalRowsCount),
            pageManager.totalRowsCount
        );
        html.push(startHtml);
        html.push('</div>');

        pager.empty().append(html.join(' '));
        toggle(pager, haveLinks);

        pager.find('.firstPage').click(function () {
            createTableFromQuery(uniqueId, 0, pageManager.rowsPerPage);
            return false;
        });

        pager.find('.previousPage').click(function () {
            createTableFromQuery(uniqueId, pageIndex - 1, pageManager.rowsPerPage);
            return false;
        });

        pager.find('.nextPage').click(function () {
            createTableFromQuery(uniqueId, pageIndex + 1, pageManager.rowsPerPage);
            return false;
        });

        pager.find('.lastPage').click(function () {
            createTableFromQuery(uniqueId, pageManager.numberOfPages() - 1, pageManager.rowsPerPage);
            return false;
        });

        pager.find('.showAll').click(function () {
            // We don't have a good way to show all.  We'll show 1 million and
            // accept that there's an issue if there are more than that :)
            createTableFromQuery(uniqueId, 0, 1000000);
            return false;
        });

        pager.find('.pageSize').change(function () {
            changePageSize();
        });

        pager.find('.pageSize').keydown(function (e) {
            if (e.keyCode === 13) {
                changePageSize();
                return false;
            }
            return true;
        });

        pager.find('.pageNumber').change(function () {
            changePageNumber();
        });
        pager.find('.pageNumber').keyup(function (e) {
            if (e.keyCode === 13) {
                changePageNumber();
                return false;
            }
            return true;
        });
    }

    circular.updatePagerControls = updatePagerControls;

    function initializeSearch(initialSettings) {
        var $searchTextBox, selector, $searchButton, searchButtonId;

        function triggerSearchFunction() {
            var searchText = getSearchText(initialSettings.uniqueId);
            if ($searchButton) {
                // switch search button functionality to "cancel" if search text exists
                if (searchText) {
                    // cancel search on click
                    $searchButton.attr('src', '/Orion/images/Button.SearchCancel.gif');
                } else {
                    $searchButton.attr('src', '/Orion/images/Button.SearchIcon.gif');
                }
            }
            createTableFromQuery(initialSettings.uniqueId, 0, 0, '', searchText);
        }

        selector = getTrimmedText(initialSettings.searchTextBoxId);
        $searchTextBox = $('#' + selector);
        if (!$searchTextBox.length) {
            // no search box, no search
            return;
        }

        searchButtonId = getTrimmedText(initialSettings.searchButtonId);
        $searchButton = $('#' + searchButtonId);
        if ($searchButton.length) {
            $searchButton.click(function () {
                if ($searchButton.attr('src') === '/Orion/images/Button.SearchCancel.gif') {
                    $searchTextBox.val('');
                }
                triggerSearchFunction();
                return false;
            });
        }

        // search textbox handler
        $searchTextBox.keyup(function (e) {
            if (e.keyCode === 13) {
                triggerSearchFunction();
                return false;
            }
            if (e.keyCode === 27) {
                $searchTextBox.val('');
                triggerSearchFunction();
                return false;
            }
            return true;
        });
    }

    function updateConfig(settingsToUpdate) {
        var oldSettings = settings[settingsToUpdate.uniqueId];
        var mergedSettings = $.extend({}, oldSettings, settingsToUpdate);
        settings[settingsToUpdate.uniqueId] = mergedSettings;
    }

    function initialize(initialSettings) {
        var mergedSettings = $.extend({}, baseSettings, initialSettings);
        settings[initialSettings.uniqueId] = mergedSettings;
        if (isNonEmptyText(mergedSettings.searchTextBoxId)) {
            initializeSearch(initialSettings);
        }
    }

    function refresh(uniqueId) {
        createTableFromQuery(uniqueId, settings[uniqueId].initialPage, settings[uniqueId].rowsPerPage);
    }

    function addRenderer(selector, method) {
        var rendererIndex = -1;

        APMjs.linqEach(renderers, function (el, index) {
            if (el.selector === selector) {
                rendererIndex = index;
            }
        });

        if (!isFunction(method)) {
            if (rendererIndex >= 0) {
                renderers.remove(rendererIndex);
            }
        } else {
            var newRenderer = {
                selector: selector,
                renderTo: method
            };
            if (rendererIndex >= 0) {
                renderers[rendererIndex] = newRenderer;
            } else {
                renderers.push(newRenderer);
            }
        }
    }

    function stackColumns(cols, stackCount) {
        var result = [], i;
        for (i = 0; i < stackCount; i += 1) {
            Array.prototype.push.apply(result, cols);
        }
        return result;
    }

    function stackRows(rows, stackCount) {
        var result = [], stackOffset = 0, row;
        APMjs.linqEach(rows, function (srow) {
            if (!row) {
                row = [];
                result.push(row);
            }
            Array.prototype.push.apply(row, srow);
            stackOffset += 1;
            if (stackOffset === stackCount) {
                stackOffset = 0;
                row = null;
            }
        });
        return result;
    }

    function appendEach($target, listToAppend) {
        APMjs.linqEach(listToAppend, function (el) {
            $target.append(el);
        });
        return $target;
    }


    // publish methods
    CustomQuery.UpdateConfig = updateConfig;
    CustomQuery.initialize = initialize;
    CustomQuery.refresh = refresh;
    CustomQuery.addRenderer = addRenderer;
    CustomQuery.stackColumns = stackColumns;
    CustomQuery.stackRows = stackRows;
    CustomQuery.appendEach = appendEach;


    $.getScript('/Orion/APM/js/PercentageBarRenderers.js', function () {
        addRenderer('.percentageBar', APM.Renderers.PercentageBarRenderer);
    });
});

/*
    // === headerTitles ===
    // - you need to define titles for all columns not starting with an underscore
    // - titles should be in the same order they are returned from query
    //   (even though it's not critical for cell rendering using custom template)
    // - if you need to change order of columns displayed or show just some of them, use customHeaderGenerator
    //
    // this is mostly important when sorting is allowed
    // but I would consider good practice to always stick to that
    // ===

    // === explicitZebra
    // when set to true changes zebra striping only to elements explicitly marked with "zebraRow" class
    // in CQT usually used to stripe whole tbody sections of multiple rows
    // ===

    // === Sample of initial settings
    SW.APM.Resources.CustomQuery.initialize({
        uniqueId: <%= Resource.ID %>,
        initialPage: 0,
        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
        allowSearch: true,
        allowPaging: false,
        hideShowAll: true,
        showAllLimit: 50,
        showSpanLoader: true,
        maxRowsPerPage: 25,
        defaultOrderBy: "[Name] DESC",
        baseOrderBy: "[Name] ASC",
        whereClause: "Column1 = 'abc' AND Column2 > 5",
        headerTitles: [
            "Column1",
            "Column2",
            "Column3"
        ],
        onbeforeload: function(grid) {},
        onloaded: function(grid) {},
        customFormatters: {
            'Name': function (value) { return '##' + value; }
        },
        contentTemplateId: "#underscoreTemplate-<%= Resource.ID %>",
        customRowGenerator: function(columns) {
            var result = $('<tr/>');
            $("<b/>").text("Ahoj").appendTo(result);
            return result;
        },
        customHeaderGenerator: function(columns) {
            return $('<tr/>')
                .addClass('HeaderRow')
                .append(columns[0], columns[1]);
        },
        //Row generators priority
        // 1 - customRowGenerator
        // 2 - contentTemplateId
        // 3 - customFormatters
    });
*/
