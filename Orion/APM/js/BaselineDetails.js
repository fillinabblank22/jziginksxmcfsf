/*jslint indent: 4, plusplus: true, vars: true*/
/*global APMjs: false*/

APMjs.initGlobal('SW.APM.BaselineDetails', function (module, APMjs) {
    'use strict';

    var CONST, SW, mapLoadFn;

    CONST = {
        statisticColumns: {
            'CPU': 'PercentCPU',
            'PMem': 'PercentMemory',
            'VMem': 'PercentVirtualMemory',
            'Response': 'ResponceTime',
            'StatisticData': 'StatisticData',
            'IOReadOperationsPerSec': 'IOReadOperationsPerSec',
            'IOWriteOperationsPerSec': 'IOWriteOperationsPerSec',
            'IOTotalOperationsPerSec': 'IOTotalOperationsPerSec'
        },
        units: {
            'CPU': '%',
            'PMem': '%',
            'VMem': '%',
            'Response': 'ms'
        }
    };

    SW = APMjs.assertGlobal('SW');
    mapLoadFn = {};

    function getStatisticColumn(thresholdName) {
        return CONST.statisticColumns[thresholdName] || '';
    }

    function getUnit(thresholdName) {
        return CONST.units[thresholdName] || '';
    }

    function transformSettings(parameters, settings) {
        settings.netObjectIds[0] = parameters[0];
        if (parameters[1]) {
            settings.StatisticColumn = parameters[1];
        } else {
            settings.StatisticColumn = getStatisticColumn(parameters[2]);
        }
        settings.columnSchemaId = parameters[5];
        return settings;
    }

    function transformOptions(parameters, options) {
        var unit = getUnit(parameters[2]);
        options.chart.events.load = function () {
            SW.APM.EditApp.redrawPlotBandsInCharts();
            SW.APM.EditApp.redrawXAxis(parameters[3], unit);
        };
        options.xAxis.events.afterSetExtremes = function () {
            SW.APM.EditApp.redrawPlotBandsInCharts();
            SW.APM.EditApp.redrawXAxis(parameters[3], unit);
        };
        if (parameters[4]) {
            options.chart.width = parameters[4];
        }
        if (unit) {
            options.tooltip.headerFormat = '<div id=""highchartTooltip"">A value of {point.key} ' + unit + '<br />';
        }
        return options;
    }

    function transformOptionsMinMax(parameters, options) {
        options.yAxis[0].title.text = parameters[3];
        options.yAxis[0].unit = getUnit(parameters[2]);
        options.seriesTemplates.Avg.name = 'Average ' + parameters[3];
        options.seriesTemplates.MinMax.name = 'Min/Max ' + parameters[3];
        if (parameters[4]) {
            options.chart.width = parameters[4];
        }
        return options;
    }

    function transformCustomChart(controlId) {
        SW.APM.Charts.CustomChart.setTransform(controlId, transformSettings, controlId === 'ccMinMax' ? transformOptionsMinMax : transformOptions);
    }

    function registerLoadFn(id, fn) {
        if (!APMjs.isFn(fn)) {
            throw 'TypeError: fn';
        }
        mapLoadFn[id] = fn;
    }

    function callLoadFn(id) {
        var fn = mapLoadFn[id], args;
        if (fn) {
            args = Array.prototype.slice.call(arguments, 1);
            fn.apply(null, args);
        }
    }

    module.transformCustomChart = transformCustomChart;
    module.registerLoadFn = registerLoadFn;
    module.callLoadFn = callLoadFn;
});
