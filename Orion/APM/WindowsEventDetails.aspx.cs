﻿using System;
using System.Globalization;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.Extensions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_APM_WindowsEventDetails : System.Web.UI.Page
{
    private ApmMonitor monitor = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        var netObjectID = "AM:" + Request.QueryStringOrForm("ComponentId");
        try
        {
            monitor = (ApmMonitor)NetObjectFactory.Create(netObjectID);
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(string.Format(CultureInfo.InvariantCulture, "/Orion/AccountLimitationError.aspx?NetObject={0}", netObjectID));
        }
        catch (IndexOutOfRangeException ex)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = ex });
        }
    }

    public ApmMonitor Monitor
    {
        get { return monitor; }
    }
}