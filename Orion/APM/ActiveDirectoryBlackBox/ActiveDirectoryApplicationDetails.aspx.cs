﻿using System;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web.UI;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web;
using SolarWinds.APM.BlackBox.ActiveDirectory.Common.Constants;

public partial class Orion_APM_ActiveDirectoryBlackBox_ActiveDirectoryApplicationDetails: ActiveDirectoryOrionView, IActiveDirectoryApplicationProvider, ITimePeriodProvider
{
    public override string ViewKey => Applications.SummaryViewKey;
    public override string ViewType => Applications.DetailsViewType;
    public override ActiveDirectoryApplication ActiveDirectoryApplication => (ActiveDirectoryApplication)NetObject;

    protected override void OnInit(EventArgs e)
    {
        resHost.ActiveDirectoryApplication = ActiveDirectoryApplication;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        TimePeriodPicker.Visible = ViewInfo.ViewKey == Applications.SummaryViewKey;

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // title
        title.ViewTitle = string.IsNullOrEmpty(ViewInfo.ViewGroupName) ? ViewInfo.ViewTitle : ViewInfo.ViewGroupName;
        title.ViewSubTitle = ActiveDirectoryApplication.Node.Name;

        // status icon
        this.title.StatusIconInfo = new Orion_APM_Controls_Views_ViewTitle.StatusProviderInfo(Swis.Entities.ActiveDirectoryApplication, this.ActiveDirectoryApplication.StatusId);

        // customize
        if (Profile.AllowCustomize)
            topRightLinks.CustomizeViewHref = CustomizeViewHref;

        // edit
        if (ApmRoleAccessor.AllowAdmin)
        {
            topRightLinks.EditNetObjectHref = ApmMasterPage.GetEditApplicationPageUrl(ActiveDirectoryApplication.Id);
            topRightLinks.EditNetObjectText = Resources.APMWebContent.APMWEBDATA_TM0_2;
        }

        // help
        topRightLinks.HelpUrlFragment = "samagappinsaaddetailstop";
    }

    public object GetValue(string key)
    {
        return this.GetDynamicInfoValue(key);
    }

    public DateTime? TimePeriodStartDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDate().Value; }
    }

    public DateTime? TimePeriodEndDate
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDate().GetValueOrDefault(DateTime.Now); }
    }

    public DateTime? TimePeriodStartDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetStartDateUTC().Value; }
    }

    public DateTime? TimePeriodEndDateUTC
    {
        get { return TimePeriodPicker.PeriodFilter.GetEndDateUTC().GetValueOrDefault(DateTime.UtcNow); }
    }

    public int GetRelativeTimePeriodMinutes()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutes();
    }

    public int GetRelativeTimePeriodMinutesUTC()
    {
        return TimePeriodPicker.PeriodFilter.GetRelativeTimePeriodMinutesUTC();
    }

    public Boolean TimePeriodIsSetByUser()
    {
        return TimePeriodPicker.PeriodFilter.IsSetInQueryString(Request);
    }
}
