﻿<%@ Page Title="Assign Application Monitor" Language="C#" MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master" 
    AutoEventWireup="true" CodeFile="AssignSmartActiveDirectoryApplication.aspx.cs" Inherits="Orion_APM_Admin_SmartActiveDirectoryApplication_AssignSmartActiveDirectoryApplication" %>
<%@ Import Namespace="SolarWinds.APM.Common.Utility" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="apm" TagName="CredsTip" Src="~/Orion/APM/Controls/CredentialTips.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectServerIp" Src="~/Orion/APM/Controls/SelectServerIp.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>
<%@ Register TagPrefix="apm" TagName="SelectPortNumber" Src="~/Orion/APM/Controls/SelectPortNumber.ascx" %>
<%@ Register TagPrefix="apm" TagName="ImageTooltip" Src="~/Orion/APM/Controls/ImageTooltip.ascx" %>

<asp:Content ID="WizardContentPlaceholder" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:include runat="server" file="APM/ActiveDirectoryBlackBox/js/ActiveDirectoryWarningHandler.js" />
    <orion:Include ID="IncludeTimeoutHandling" runat="server" File="Nodes/js/TimeoutHandling.js" />
    <orion:Include ID="IncludeAssignWizard" runat="server" File="APM/ActiveDirectoryBlackBox/Styles/AssignWizard.css"/>
    
    <script type="text/javascript">
         function HideTestResults() {
             $(".testSuccessful").hide();
             $(".testFailed").hide();
         }
         
        function TestDuplicateInstance() {
             $.ajax({
                 type: "POST",
                 url: "AssignSmartActiveDirectoryApplication.aspx/InstanceExists",
                 data: Ext.encode({ nodeId: window.$get('<%= serverIP.ClientID %>' + '_selectedNodeId').value, ipAddress: window.$get('<%= serverIP.ClientID %>' + '_targetServer').value }),
                 contentType: 'application/json; charset=utf-8',
                 dataType: "json",
                 success: function (dataResponse) {
                     if (!dataResponse.d) {
                         Ext.MessageBox.hide();
                         <%= Page.ClientScript.GetPostBackEventReference(this, "Next") %>;
                     } else {
                         Ext.Msg.show({
                             msg: "<%=Resources.APM_ActiveDirectoryBlackBoxContent.AssignWizard_DuplicateFound%>",
                             buttons: Ext.Msg.OK,
                             icon: Ext.Msg.INFO
                         });
                     }
                 },
                 error: function (response) {
                     Ext.Msg.alert("<%=Resources.APM_ActiveDirectoryBlackBoxContent.AssignWizard_DuplicateCheckError%>", response.statusText);
                     Ext.Msg.alert("<%=Resources.APM_ActiveDirectoryBlackBoxContent.AssignWizard_DuplicateCheckError%>", response.statusText);
                 }
             });
         }

        function NextClick() {
            TestDuplicateInstance();
            return false;
        }

        function handleSecurityWarning() {
            const warningSelector = "#nonSecureConnWarning";
            const isIgnoreCertificateErrorsSelector = "#<%= ControlHelper.EncodeJsString(this.ignoreCertificateErrors.ClientID) %>";
            const selectedAuthenticationMethodSelector = "#<%= ControlHelper.EncodeJsString(this.authenticationMethod.ClientID) %>";

            SW.APM.BB.ActiveDirectory.SetNonSecureConnWarningVisibility(warningSelector, isIgnoreCertificateErrorsSelector, selectedAuthenticationMethodSelector);
        }
    </script>

    <h2><%=  InvariantString.Format(Resources.APM_ActiveDirectoryBlackBoxContent.AssignWizard_MonitorTitle, Workflow.SelectedTemplate.Name)%></h2>
    <p><%= InvariantString.Format(Resources.APM_ActiveDirectoryBlackBoxContent.AssignWizard_MonitorDescription, Workflow.SelectedTemplate.Name)%></p>
    <br />
    <p class="note">
        <%= Resources.APMWebContent.APMWEBDATA_VB1_242 %>
        <a id="addLink" href="/Orion/Nodes/Default.aspx"><%= Resources.APMWebContent.APMWEBDATA_VB1_243 %></a>
    </p>
    <div id="selectInstanceStep">
        <ul id="selectInstanceForm" class="aad-select-instance-form">
            <li class="aad-list-item">
                <asp:ValidationSummary ID="PageValidationSummary" runat="server" />
            </li>
            <li class="aad-list-item">
                <label class="aad-list-item-label"><%=  Resources.APMWebContent.APMWEBDATA_AK1_117 %></label>
                    <apm:SelectServerIp ID="serverIP" runat="server" ValidateVmware="false" CssClass="apm_Target" />
            </li>
            <li class="aad-list-item">
                <label class="aad-list-item-label"><%=  Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_LdapPortNumber %></label>
                <apm:SelectPortNumber runat="server" ID="domainControllerPortNumber" CssClass="intInput" />
                <asp:RequiredFieldValidator ID="domainControllerPortNumberValidator" ControlToValidate="domainControllerPortNumber:portNumber" ErrorMessage="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_ErrorLdapPortNumber %>" runat="server">*</asp:RequiredFieldValidator>
                <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_PortNumberTooltip %>" />
            </li>
            <li class="aad-list-item">
                <label class="aad-list-item-label"><%=  Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_GcPortNumber %></label>
                <apm:SelectPortNumber runat="server" ID="globalCatalogPortNumber" CssClass="intInput" />
                <asp:RequiredFieldValidator ID="globalCatalogPortNumberValidator" ControlToValidate="globalCatalogPortNumber:portNumber" ErrorMessage="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_ErrorGcPortNumber %>" runat="server">*</asp:RequiredFieldValidator>
                <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_GcPortNumberTooltip %>" />
            </li>
            <li class="aad-list-item">
                <label class="aad-list-item-label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_EncryptionMethodLabel%></label>
                <asp:DropDownList ID="encryptionMethod" runat="server" AutoPostBack="false">
                </asp:DropDownList>
                <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_EncryptionMethodTooltip %>" />
            </li>
            <li class="aad-list-item">
                <label class="aad-list-item-label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_AuthenticationMethodLabel%></label>
                <asp:DropDownList ID="authenticationMethod" runat="server" AutoPostBack="false" onChange="handleSecurityWarning()">
                </asp:DropDownList>
                <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_AuthenticationMethodTooltip %>" />
                <asp:PlaceHolder runat="server" ID="phNonSecureConn">
                    <div id="nonSecureConnWarning" class="non-secure-warning-container">
                        <div class="non-secure-warning"><%= Resources.APMWebContent.NonSecureConnectionHasBeenSelected %></div>
                    </div>
                </asp:PlaceHolder>
            </li>
            <li class="aad-list-item">
                <label class="aad-list-item-label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_IgnoreCertificateErrors%></label>
                <asp:Checkbox ID="ignoreCertificateErrors" runat="server" AutoPostBack="false" onClick="handleSecurityWarning()">
                </asp:Checkbox>
                <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_IgnoreCertificateErrorsTooltip %>" />
            </li>
            <li class="aad-list-item">
                <label class="aad-list-item-label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_EnableDomainComponents%></label>
                <asp:Checkbox ID="enableDomainComponents" runat="server" AutoPostBack="false">
                </asp:Checkbox>
                <apm:ImageTooltip runat="server" Text="<%$ Resources: APM_ActiveDirectoryBlackBoxContent, AssignWizard_EnableDomainComponentsTooltip %>" />
            </li>
            <li class="aad-list-item">
                <label  class="aad-list-item-label"><%=  Resources.APM_ActiveDirectoryBlackBoxContent.EditApplicationSettings_InstanceCredentials %></label>
                <asp:UpdatePanel ID="UpdateCredentialsPanel" runat="server" UpdateMode="conditional" Class="credentialsContainer ie7addLayout">
                    <ContentTemplate>            
                    <table class="selectCreds">
                        <apm:SelectCredentials runat="server" ID="selectCredentials" Class="add-select-instance-step"
                            ValidationGroupName="SelectCredentialsValidationGroup" 
                            AllowNodeWmiCredential="True" />
                        <tr>
                            <td colspan="2" class="add-test-row">
                                <div runat="server" id="testSuccessful" class="add-test-successful-panel add-test-row-panel" visible="false">
                                    <img class="add-test-row-img" src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                                    <asp:Literal ID="testSuccessfulLiteral" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_55 %>" />
                                </div>
                                <div runat="server" id="testFailed" class="add-test-failed-panel add-test-row-panel" visible="false">
                                    <img class="add-test-row-img" src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                    <asp:Literal ID="testFailedLiteral" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_56 %>" />
                                </div>
                                <asp:UpdateProgress runat="server" ID="UpdateProgress" DynamicLayout="true" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <img class="add-test-row-img" src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                        <asp:Literal ID="UpdateProgressLiteral" runat="server" Text="<%$ Resources: APMWebContent, ApmWeb_TestingProgress %>" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="add-test-button-row">
                                <orion:LocalizableButton runat="server" ID="TestButton" LocalizedText="Test" DisplayType="Small" OnClick="OnTest" OnClientClick="HideTestResults();" />
                            </td>
                        </tr>
                        <tr><td colspan="2">
                            <asp:ValidationSummary ID="CredentialsValidationSummary" runat="server" ValidationGroup="SelectCredentialsValidationGroup"/>
                        </td></tr>
                    </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </li>
        </ul>
        <div class="aad-credential-tips">
            <apm:CredsTip ID="ctrCredsTip" runat="server"/>  
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbNext" runat="server" Text="<%$ Resources : APM_ActiveDirectoryBlackBoxContent, AssignWizard_ApplicationMonitorButton %>" DisplayType="Primary" OnClientClick="NextClick(); return false;" />
        <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="OnCancel" CausesValidation="false" />
    </div>   
</asp:Content>

