﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Finish.aspx.cs" Inherits="Orion_APM_Admin_SmartActiveDirectoryApplication_Finish" 
    MasterPageFile="~/Orion/APM/Admin/Templates/Assign/AssignWizard.master" EnableViewState="false" 
    Title="Finish" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/APM/Controls/AssignComponentsFinished.ascx" TagPrefix="apm" TagName="AssignComponentsFinished" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="WizardContentPlaceholder" ContentPlaceHolderID="wizardContentPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include ID="IncludeStyles" runat="server" File="APM/ActiveDirectoryBlackBox/Styles/AssignWizard.css"/>
    
    <apm:AssignComponentsFinished runat="server" ID="finishedMessage" EnableEditLink="false" NetObjectType="<%# SolarWinds.APM.BlackBox.ActiveDirectory.Common.Constants.Applications.TemplateCustomType %>" />
   
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton ID="imgbDone" runat="server" LocalizedText="Done" DisplayType="Primary" OnClick="OnDone"/>
    </div>   
</asp:Content>

