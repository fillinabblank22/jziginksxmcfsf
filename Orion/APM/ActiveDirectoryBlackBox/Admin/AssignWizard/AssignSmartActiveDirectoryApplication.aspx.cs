﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.APM.BlackBox.ActiveDirectory.Common.Constants;
using SolarWinds.APM.BlackBox.ActiveDirectory.Common.Models;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web.UI;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web;
using SolarWinds.APM.Web.UI;
using SolarWinds.Logging;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web.Dal;
using SolarWinds.APM.Common;

public partial class Orion_APM_Admin_SmartActiveDirectoryApplication_AssignSmartActiveDirectoryApplication : WizardPage<AssignSmartActiveDirectoryApplicationWorkflow>, IPostBackEventHandler
{
    private static readonly Log _log = new Log();
    private SelectNodeTreeItem _selectedNode;

    private SelectNodeTreeItem SelectedNode
    {
        get
        {
            if (_selectedNode == null)
            {
                _selectedNode = new SelectNodeTreeItem(serverIP.SelectedNode);
                var selectedCredSetId = selectCredentials.SelectedCredentialSetId;
                if (selectedCredSetId != ComponentBase.NewCredentialsId)
                {
                    _selectedNode.CredentialId = selectedCredSetId;
                }
            }

            return _selectedNode;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindControls();
            RestoreSelectedNode();
            selectCredentials.ReloadCredentialSets();

            var postData = Server.UrlDecode(Request.Form["postData"]);
            if (postData != null)
            {
                SetTemplate(postData);
            }
            else
            {
                Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx");
            }
            imgbNext.AddEnterHandler(0);
        }
        selectCredentials.ValidationSummaryParentClientID = CredentialsValidationSummary.ClientID;
    }

    private void BindControls()
    {
        ActiveDirectoryControlHelpers.BindComboBox<UseLdapEncryptionSettings>(encryptionMethod, UseLdapEncryptionSettings.None.ToString());
        ActiveDirectoryControlHelpers.BindComboBox<UseLdapAuthenticationSettings>(authenticationMethod, UseLdapAuthenticationSettings.Negotiate.ToString());
        domainControllerPortNumber.Value = LdapConstants.DefaultLdapPort;
        globalCatalogPortNumber.Value = LdapConstants.DefaultGcPort;
        ignoreCertificateErrors.Checked = true;
        enableDomainComponents.Checked = true;
    }

    public void RestoreSelectedNode()
    {
         if (Workflow.SelectedNodesInfo != null && Workflow.SelectedNodesInfo.Count > 0)
            {
                if (Workflow.SelectedNodesInfo.Count > 1)
                {
                    _log.Debug("Found more than one nodes in workflow state, using just first, because the wizard can assign only one at a time, ignoring the rest.");
                }
                var nodeInfo = Workflow.SelectedNodesInfo[0];
                serverIP.RestoreSelectedNodeById(nodeInfo.Id);
            }
    }

    public void SetTemplate(string postData)
    {
        Workflow.HasStarted = true;
        var serializer = new JavaScriptSerializer();
        var templateId = serializer.Deserialize<int>(postData);
        Workflow.SelectedTemplate = new TemplateInfo(templateId);
    }

    protected void OnTest(object sender, EventArgs e)
    {       
        Validate(PageValidationSummary.ValidationGroup);
        if (Page.IsValid)
        {
            Validate(selectCredentials.ValidationGroupName);
            if (selectCredentials.IsValid)
            {
                SetUiTestedCredentialControls(TestConnection());
            }
            else
            {
                testSuccessful.Visible = false;
                testFailed.Visible = false;
            }
        }
        else
        {
             MoveInvalidValidators(PageValidationSummary.ValidationGroup, CredentialsValidationSummary.ValidationGroup);
        }
    }

    protected void OnNext(object sender, EventArgs e)
    {
        Validate(PageValidationSummary.ValidationGroup);
        Validate(selectCredentials.ValidationGroupName);

        if (Page.IsValid && selectCredentials.IsValid)
        {
            var testConnectionResult = TestConnection();
            if (IsConnectionTestOK(testConnectionResult))
            {
                CreateApplication();
                GotoNextPage();
            }
            else
            {
                SetUiTestedCredentialControls(testConnectionResult);
            }
        }
    }

    private bool IsConnectionTestOK(TestConnectionResult result) {
        return result.IsValidCredential && result.IsConnected;
    }
    
    protected void OnCancel(object sender, EventArgs e)
    {
        CancelWizard();
    }

    protected override void CancelWizard()
    {
        base.CancelWizard();
        Response.Redirect("~/Orion/APM/Admin/ApplicationTemplates.aspx", true);
    }

    protected TestConnectionResult TestConnection()
    {
        using (var businessLayer = ServiceLocatorForWeb.GetServiceForWeb<IBusinessLayerFactory>().Create())
        {
            try
            {                
                var result = businessLayer.TestBlackBoxConnection(
                    SelectedNode.Id, selectCredentials.SelectedCredentialSet, ApplicationSettings.DefaultForBlackBoxAssignTemplateWizards, Applications.TemplateCustomType, GetSettings().ToXmlString());
                if (result != null && result.Result !=null)
                {
                    return TestConnectionResult.FromXmlString(result.Result);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);   
                AddErrorMessage(Resources.APM_ActiveDirectoryBlackBoxContent.AssignWizard_ServerCredentialsError, selectCredentials.ValidationGroupName); 
            }

            return new TestConnectionResult()
            {
                IsConnected = false,
                IsValidCredential = false
            };
        }
    }

    private Settings GetSettings()
    {
        var settingFromTemplate = new Settings(SelectedNode.CredentialId, domainControllerPortNumber.Value.Value, globalCatalogPortNumber.Value.Value, authenticationMethod.SelectedValue, encryptionMethod.SelectedValue, ignoreCertificateErrors.Checked, SettingLevel.Instance);
        return settingFromTemplate;
    }

    private void AddErrorMessage(string message, string validationGroupName)
    {
        var err = new CustomValidator { ValidationGroup = validationGroupName, IsValid = false, ErrorMessage = message };
        Page.Validators.Add(err);
    }

    private void MoveInvalidValidators(string sourceValidationGroup, string targetValidationGroup)
    {
        var invalidValidators = GetValidators(sourceValidationGroup).OfType<IValidator>().Where(v => !v.IsValid);
        foreach (var val in invalidValidators)
        {
            Page.Validators.Remove(val);
            AddErrorMessage(val.ErrorMessage, targetValidationGroup);
        }
    }

    private void CreateApplication()
    {
        Workflow.SelectedNodesInfo = new List<SelectNodeTreeItem> { SelectedNode };
        Workflow.PortNumber = domainControllerPortNumber.Value.Value;
        Workflow.GlobalCatalogPortNumber = globalCatalogPortNumber.Value.Value;
        Workflow.AuthenticationMethod = authenticationMethod.SelectedValue;
        Workflow.EncryptionMethod = encryptionMethod.SelectedValue;
        Workflow.IgnoreCertificateErrors = ignoreCertificateErrors.Checked;
        Workflow.EnableDomainComponents = enableDomainComponents.Checked;

        if (selectCredentials.SelectedCredentialSetId == ComponentBase.NewCredentialsId)
        {
            Workflow.UseCredentials(selectCredentials.SelectedCredentialSet);
        }
        else if (selectCredentials.SelectedCredentialSetId == ComponentBase.NodeWmiCredentialsId)
        {
            Workflow.InheritCredentials();
        }
        else
        {
            Workflow.UseCredentials(new List<SelectNodeTreeItem> { SelectedNode });
        }

        Workflow.CreateApplications();
    }

    private void SetUiTestedCredentialControls(TestConnectionResult testResult)
    {
        var testSucceeded = IsConnectionTestOK(testResult);
        if(!testSucceeded)
        {
            testFailedLiteral.Text = GetErrorMessages(testResult);
        }
        testSuccessful.Visible = testSucceeded;
        testFailed.Visible = !testSucceeded;
        selectCredentials.PreFillPasswords();
    }

    private string GetErrorMessages(TestConnectionResult testConnectionResult) {
        var messages = new List<string>();
        messages.Add(Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_CredentialsFailed);
        if(!testConnectionResult.IsConnected)
        {
            messages.Add(Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_ConnectionFailed);
        }
        else if(!testConnectionResult.IsValidCredential)
        {
            messages.Add(Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_UserCredentialsFailed);
        }
        if(!String.IsNullOrEmpty(testConnectionResult.ErrorMessage)) 
        {
            messages.Add(testConnectionResult.ErrorMessage);
        }

        return String.Join("<br/>", messages);
    }

    #region [web service methods]

    [WebMethod(EnableSession = true)]
    public static bool InstanceExists(int nodeId, string ipAddress)
    {
        Node node = GetNode(nodeId, ipAddress);

        return (node != null) && ServiceLocatorForWeb.GetServiceForWeb<IActiveDirectoryApplicationDal>().IsAssignedToNode(node.Id);
    }


    #endregion

    #region [Raise Post Back Event]

    protected static class PagePostbackEvents
    {
        public const string Next = "Next";
    }

    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == PagePostbackEvents.Next)
        {
            OnNext(this, new EventArgs());
            return;
        }
        throw new ArgumentNullException("");
    }

    #endregion

}
