﻿<%@ WebService Language="C#"  Class="SolarWinds.APM.BlackBox.ActiveDirectory.Web.ActiveDirectoryConfigurationService" %>
using System;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.Web.Services;
using SolarWinds.APM.BlackBox.ActiveDirectory.Common.Constants;
using TestConnectionResult = SolarWinds.APM.BlackBox.ActiveDirectory.Common.Models.TestConnectionResult;

namespace SolarWinds.APM.BlackBox.ActiveDirectory.Web
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class ActiveDirectoryConfigurationService : BlackBoxTestWebService
    {

        public ActiveDirectoryConfigurationService()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod]
        public TestResultInfo TestConnection(TestBlackBoxSettings settings)
        {
            return TestActiveDirectoryBlackbox(settings);
        }

        private TestResultInfo TestActiveDirectoryBlackbox(TestBlackBoxSettings settings)
        {
            if (settings.NodeId < 1)
            {
                throw new ArgumentException(APM_ActiveDirectoryBlackBoxContent.ActiveDirectoryConfigurationService_TestActiveDirectoryBlackbox_Node_Id_is_required_);
            }
            if (string.IsNullOrWhiteSpace(settings.CredentialSet.Login) &&
                settings.CredentialSet.Id != ComponentBase.NodeWmiCredentialsId)
            {
                throw new ArgumentException(APM_ActiveDirectoryBlackBoxContent.ActiveDirectoryConfigurationService_TestActiveDirectoryBlackbox_Credential_is_required_);
            }
            try
            {
                // Force 64bit. If we are on 32bit system, we will use implicitly 32bit.
                settings.Use64Bit = true;
                var xmlSettings = SerializeAppSettings(settings);
                return TestBlackBoxConnectionInternal(settings, xmlSettings, typeof(TestConnectionResult));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error while serializing app settings: {0}, error: {1}", RequestToString(settings), ex);
                var result = new TestConnectionResult();
                result.AddErrorMessage(Resources.APM_ActiveDirectoryBlackBoxContent.ActiveDirectoryConfigurationService_DetailedErrorInformation, ex.Message);

                return new TestResultInfo(false, result);
            }
        }

        protected override object DeserializeTestResults(string result)
        {
            return TestConnectionResult.FromXmlString(result);
        }

        protected override string SerializeAppSettings(TestBlackBoxSettings settings)
        {
            var credentialSetId = -3;
            if (!string.IsNullOrWhiteSpace(settings.CredentialSet.Login))
            {
                credentialSetId = settings.CredentialSet.Id;
            }
            var settingsToSerialize = new ActiveDirectory.Common.Models.Settings()
            {
                Port = Int32.Parse(settings.CustomSettings[Applications.Settings.PortSettingName]),
                EncryptionMethod = (UseLdapEncryptionSettings)Enum.Parse(typeof(UseLdapEncryptionSettings), settings.CustomSettings[Applications.Settings.EncryptionMethodSettingName]),
                AuthenticationMethod = (UseLdapAuthenticationSettings)Enum.Parse(typeof(UseLdapAuthenticationSettings), settings.CustomSettings[Applications.Settings.AuthenticationMethodSettingName]),
                IgnoreCertificateErrors = Boolean.Parse(settings.CustomSettings[Applications.Settings.IgnoreCertificateErrorsSettingName]),
                PollingTimeout = Int32.Parse(settings.CustomSettings[Applications.Settings.PollingTimeoutSettingName])
            };

            return settingsToSerialize.ToXmlString();
        }
    }
}
