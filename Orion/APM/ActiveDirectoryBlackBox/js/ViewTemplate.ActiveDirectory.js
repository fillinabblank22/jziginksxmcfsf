/*jslint browser: true, unparam: true*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.templates', function (templates, APMjs) {
    'use strict';

    // callback(value, meta, record)
    function renderName(value) {
        return APMjs.format('<img src="/Orion/APM/Images/BlackBox/SmartApp.png" />&nbsp;<span class="searchable">{0}</span>', value);
    }

    // callback(value, meta, record)
    function renderCustomView(value, meta, record) {
        return value;
    }

    templates.registerHandlerAssignWizard('ABAA', '../ActiveDirectoryBlackBox/Admin/AssignWizard/AssignSmartActiveDirectoryApplication.aspx');
    templates.registerCustomTypeRenderers('ABAA', {
        'RenderName': renderName,
        'RenderCustomView': renderCustomView
    });
});
