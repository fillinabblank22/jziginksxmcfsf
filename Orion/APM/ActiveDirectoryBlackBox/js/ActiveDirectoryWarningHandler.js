﻿/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.BB.ActiveDirectory', function (module, APMjs) {
    'use strict';

    function setNonSecureConnWarningVisibility(warningSelector, isIgnoreCertificateErrorsSelector, selectedAuthenticationMethodSelector) {
        const nonSecureConnWarning = $(warningSelector);
        const isIgnoreCertificateErrors = $(isIgnoreCertificateErrorsSelector).prop("checked");
        const selectedAuthenticationMethod = $(selectedAuthenticationMethodSelector).val();

        if (isIgnoreCertificateErrors || selectedAuthenticationMethod === "Anonymous" || selectedAuthenticationMethod === "Simple") {
            nonSecureConnWarning.show();
        } else {
            nonSecureConnWarning.hide();
        }
    }

    module.SetNonSecureConnWarningVisibility = setNonSecureConnWarningVisibility;
});