/*jslint browser: true, indent: 4*/
/*global APMjs: false*/

APMjs.withGlobal('SW.APM.BB.ActiveDirectory', function (ActiveDirectoryBB, APMjs) {
    'use strict';


    var
        // module references
        SW = APMjs.assertGlobal('SW'),
        Ext = APMjs.assertExt(),
        $ = APMjs.assertQuery(),
        EditBbApp = APMjs.assertGlobal('SW.APM.EditBbApp'),
        findSetting = EditBbApp.findSetting,
        isNonEmptyText = EditBbApp.isNonEmptyText,
        applicationCustomType,
        nodeId;

    var settingsDivControlId = '#appEditActiveDirectorySettings',
        testButtonSelector,
        testSummarySelector,
        credentialSetIdKey,
        portNumberKey,
        gcPortNumberKey,
        encryptionMethodKey,
        authenticationMethodKey,
        ignoreCertificateErrorsKey,
        totalCountersDisabledKey,
        domainComponentsDisabledKey,
        settingLevelInstance,
        configurationServiceUrl =
            '/Orion/APM/ActiveDirectoryBlackBox/Services/ActiveDirectoryConfigurationService.asmx',
        credentialsControlIds = {
            definedListId: 'select[id$=aadCredentials_CredentialSetList]',
            credentialName: 'input[id$=aadCredentials_CredentialName]',
            userName: 'input[id$=aadCredentials_UserName]',
            password: 'input[id$=aadCredentials_Password]'
        },
        portNumberSelector,
        gcPortNumberSelector,
        pollingTimeoutSelector,
        encryptionMethodSelector,
        authenticationMethodSelector,
        ignoreCertificateErrorsSelector,
        enableTotalCountersSelector,
        enableDomainComponentSelector,
        initialPortNumber,
        initialGcPortNumber,
        initialEncryptionMethod,
        initialAuthenticationMethod,
        initialIgnoreCertificateErrors,
        isModelUpdated = false,
        areNewCredentials = false,
        saveCredentialsTitle,
        saveCredentialsInvalidCredentialsMsg,
        saveCredentialsCannotConnectMsg,
        testConnectionIncorrectCredentialsMsg,
        testConnectionCannotConnectMsg,
        testConnectionFailedMsg,
        totalCountersUniqueIds,
        domainComponentsUniqueIds;

    function testSuccessfulMessageSelector() {
        return testSummarySelector.find('div.testSuccessful');
    }
    function testFailedMessageSelector() {
        return testSummarySelector.find('div.testFailed');
    }
    function progressSelector() {
        return testSummarySelector.find('div.progress');
    }

    function getSelector(controlId) {
        return $('#' + controlId);
    }

    // SW.APM.BB.ActiveDirectory.EditAppSettings -->
    APMjs.initGlobal('SW.APM.BB.ActiveDirectory.EditAppSettings', function (ActiveDirectoryBbEditApp) {
        function init(data) {
            testButtonSelector = getSelector(data.testButtonClientId);
            testSummarySelector = getSelector(data.testSummaryClientId);
            testButtonSelector.unbind('click').click(onTestButtonClick);
            credentialSetIdKey = data.credentialSetIdKey;
            portNumberKey = data.portNumberKey;
            gcPortNumberKey = data.gcPortNumberKey;
            encryptionMethodKey = data.encryptionMethodKey;
            authenticationMethodKey = data.authenticationMethodKey;
            ignoreCertificateErrorsKey = data.ignoreCertificateErrorsKey;
            totalCountersDisabledKey = data.totalCountersDisabledKey;
            applicationCustomType = data.applicationCustomType;
            settingLevelInstance = data.settingLevelInstance;
            portNumberSelector = getSelector(data.portNumberClientId);
            gcPortNumberSelector = getSelector(data.gcPortNumberClientId);
            pollingTimeoutSelector = getSelector(data.pollingTimeoutClientId);
            encryptionMethodSelector = getSelector(data.encryptionMethodClientId);
            authenticationMethodSelector = getSelector(data.authenticationMethodClientId);
            ignoreCertificateErrorsSelector = getSelector(data.ignoreCertificateErrorsClientId);
            enableTotalCountersSelector = getSelector(data.enableTotalCountersClientId);
            domainComponentsDisabledKey = data.domainComponentsDisabledKey;
            domainComponentsUniqueIds = data.domainComponentsUniqueIds;
            enableDomainComponentSelector = getSelector(data.enableDomainComponentsClientId);
            saveCredentialsTitle = data.saveCredentialsTitle;
            saveCredentialsInvalidCredentialsMsg = data.saveCredentialsInvalidCredentialsMsg;
            saveCredentialsCannotConnectMsg = data.saveCredentialsCannotConnectMsg;
            testConnectionIncorrectCredentialsMsg = data.testConnectionIncorrectCredentialsMsg;
            testConnectionCannotConnectMsg = data.testConnectionCannotConnectMsg;
            testConnectionFailedMsg = data.testConnectionFailedMsg;
            totalCountersUniqueIds = data.totalCountersUniqueIds;
        }

        ActiveDirectoryBbEditApp.init = init;
    });

    function showSuccessMessage() {
        progressSelector().hide();
        testFailedMessageSelector().hide();
        testSuccessfulMessageSelector().show();
    }

    function showFailedMessage(result) {
        var messages = getErrorMessages(result);

        progressSelector().hide();
        testSuccessfulMessageSelector().hide();
        testFailedMessageSelector().show().html(joinErrorMessages(messages));
    }

    function getErrorMessages(result) {
        var messages = [];
        messages.push(testConnectionFailedMsg);
        if (!result.IsConnected) {
            messages.push(testConnectionCannotConnectMsg);
        }
        else if (!result.IsValidCredential) {
            messages.push(testConnectionIncorrectCredentialsMsg);
        }
        if (isNonEmptyText(result.ErrorMessage)) {
            messages.push(result.ErrorMessage);
        }

        return messages;
    }

    function joinErrorMessages(messages) {
        return messages.join('</br>');
    }

    function isConnectionTestOK(result) {
        return result && result.Result && result.Result.IsValidCredential && result.Result.IsConnected;
    }

    function onTestButtonClick() {
        testConnection(onTestButtonSuccess);
        return false;
    };

    function onTestButtonSuccess(result) {
        var connectionTestOK = isConnectionTestOK(result);
        if (!connectionTestOK) {
            showFailedMessage(result.Result);
        } else {
            showSuccessMessage();
        }
    }

    ActiveDirectoryBB.ApplicationEditor = function () {
        this.initUiState = function (item) {
            nodeId = item.NodeId;
            $(settingsDivControlId).show();
            setComboboxValue($(credentialsControlIds.definedListId), credentialSetIdKey, item.Settings, function (value) { });
            setTextboxValue($(portNumberSelector), portNumberKey, item.Settings, function (value) { initialPortNumber = value; });
            setTextboxValue($(gcPortNumberSelector), gcPortNumberKey, item.Settings, function (value) { initialGcPortNumber = value; });
            setComboboxValue($(encryptionMethodSelector), encryptionMethodKey, item.Settings, function (value) { initialEncryptionMethod = value; });
            setComboboxValue($(authenticationMethodSelector), authenticationMethodKey, item.Settings, function (value) { initialAuthenticationMethod = value; });
            setCheckboxValue($(ignoreCertificateErrorsSelector), ignoreCertificateErrorsKey, item.Settings, function (value) { });
            setCheckboxValueInvert($(enableTotalCountersSelector), totalCountersDisabledKey, item.Settings, function (value) { });
            setCheckboxValueInvert($(enableDomainComponentSelector), domainComponentsDisabledKey, item.Settings, function (value) { });
            SW.APM.BB.ActiveDirectory.SetNonSecureConnWarningVisibility("#nonSecureConnWarning", ignoreCertificateErrorsSelector, authenticationMethodSelector);
        };
        this.asyncPostProcessing = asyncPostProcessing;
        this.updateModelFromUi = updateModelFromUI;
    };

    function setComboboxValue(controlSelector, settingId, settings, onSuccess) {
        getValueFromSettings(settingId, settings, function (value) {
            controlSelector
                .val(value)
                .change();
            onSuccess(value);
        });
    }

    function setCheckboxValue(controlSelector, settingId, settings, onSuccess) {
        getValueFromSettings(settingId, settings, function (value) {
            var checkboxState = (value.toLowerCase() === 'true');
            controlSelector.prop('checked', checkboxState);
            onSuccess(value);
        });
    }

    function setCheckboxValueInvert(controlSelector, settingId, settings, onSuccess) {
        getValueFromSettings(settingId, settings, function (value) {
            var checkboxState = (value.toLowerCase() === 'false');
            controlSelector.prop('checked', checkboxState);
            onSuccess(value);
        });
    }

    function setTextboxValue(textboxSelector, settingId, settings, onSuccess) {
        getValueFromSettings(settingId, settings, function (value) {
            textboxSelector.val(value);
            onSuccess(value);
        });
    }

    function getValueFromSettings(settingId, settings, onSuccess) {
        var foundValue = findSetting(settingId, settings);
        if (foundValue && foundValue.Value) {
            onSuccess(foundValue.Value);
        }
    }

    function asyncPostProcessing(app, template, onSuccess, onError, saveToFormDelegate) {
        updateIsDisabledFlagForComponents(app, enableTotalCountersSelector, function (c) { return totalCountersUniqueIds.indexOf(c.TemplateUniqueId) >= 0; });

        updateIsDisabledFlagForComponents(app, enableDomainComponentSelector, function (c) { return domainComponentsUniqueIds.indexOf(c.TemplateUniqueId) >= 0; });

        testConnectionIfModelChanged(app, saveToFormDelegate, onError);
        return true;
    }

    function updateIsDisabledFlagForComponents(app, checkboxSelector, componentFilter) {   
        var isDisabled = !checkboxSelector.is(':checked');

        app.Components.filter(
            componentFilter
        ).forEach(
            function(component) {
            component.IsDisabled.Value = isDisabled;
            component.IsDisabled.SettingLevel = 2;
        });
    }

    function testConnectionIfModelChanged(app, onSuccess, onError) {
        function onTestConnectionSuccess(result) {
            var connectionTestOK = isConnectionTestOK(result);
            progressSelector().hide();
            if (!connectionTestOK) {
                processConnectionResultNotOK(onSuccess, onError, app, result);
            }
            else {
                processConnectionResultOK(onSuccess, onError, app);
            }
        }

        if (!isModelUpdated) {
            onSuccess();
        }
        else {
            testConnection(onTestConnectionSuccess, onError);
        }
    }

    function testConnection(onSuccess, onError) {
        var settings = getConnectionSettingsFromUi();
        if (!validateCredentials() || !validateConfiguration(settings.settings.CustomSettings)) {
            return false;
        }
        showProgress();
        SW.Core.Services.callWebService(
            configurationServiceUrl,
            'TestConnection',
            settings,
            onSuccess,
            onError
        );
    }

    function showProgress() {
        testSummarySelector.show();
        hideMessages();
        progressSelector().show();
    }

    function hideMessages() {
        testFailedMessageSelector().hide();
        testSuccessfulMessageSelector().hide();
    }

    function processConnectionResultNotOK(onSuccess, onError, app, result) {
        var msg = '';
        if (!result.Result.IsConnected) {
            msg = saveCredentialsCannotConnectMsg;
        }
        else {
            msg = saveCredentialsInvalidCredentialsMsg;
        }
        displayWarningMessage(onSuccess, onError, app, msg);
    }

    function displayWarningMessage(onSuccess, onError, app, msg) {
        Ext.Msg.show({
            title: saveCredentialsTitle,
            msg: msg,
            buttons: Ext.Msg.OKCANCEL,
            fn: function (btn) {
                if (btn === 'ok') {
                    if (areNewCredentials) {
                        createNewCredentials(onSuccess, onError, app);
                    }
                    else {
                        onSuccess();
                    }
                }
                else {
                    onError();
                }
            },
            icon: Ext.MessageBox.WARNING
        });
    }

    function processConnectionResultOK(onSuccess, onError, app) {
        if (areNewCredentials) {
            createNewCredentials(onSuccess, onError, app);
        }
        else {
            onSuccess();
        }
    }

    function createNewCredentials(onSuccess, onError, app) {
        function onCreatedCredentials(credentialSetId) {
            if (credentialSetId) {
                var credentialId = findSetting(credentialSetIdKey, app.Settings);
                if (credentialId) {
                    credentialId.Value = credentialSetId;
                }
                onSuccess();
            }
            else {
                onError();
            }
        }

        var newCredentials = readCredentials();
        SW.Core.Services.callWebService(
            '/Orion/APM/Services/Credentials.asmx',
            'CreateCredential',
            newCredentials, onCreatedCredentials, onError);
    }

    function readCredentials() {
        var credentials = {
            selectedCredentials: $(credentialsControlIds.definedListId).val(),
            name: $(credentialsControlIds.credentialName).val(),
            userName: $(credentialsControlIds.userName).val(),
            psw: $(credentialsControlIds.password).val()
        };
        return credentials;
    }

    function getConnectionSettingsFromUi() {
        return {
            settings: {
                NodeId: nodeId,
                AppType: applicationCustomType,
                CredentialSet: {
                    Id: $(credentialsControlIds.definedListId).val(),
                    Login: $(credentialsControlIds.userName).val(),
                    Password: $(credentialsControlIds.password).val()
                },
                CustomSettings: {
                    Port: $(portNumberSelector).val(),
                    GcPort: $(gcPortNumberSelector).val(),
                    EncryptionMethod: $(encryptionMethodSelector).val(),
                    AuthenticationMethod: $(authenticationMethodSelector).val(),
                    IgnoreCertificateErrors: $(ignoreCertificateErrorsSelector).prop('checked'),
                    PollingTimeout: $(pollingTimeoutSelector).val()
                }
            }
        };
    }

    function updateModelFromUI(item) {
        if (!validateCredentials()) {
            throw "Credentials validation exception";
        }
        if (!validateConfiguration(getConnectionSettingsFromUi().settings.CustomSettings)) {
            throw "Invalid configuration exception";
        }
        updateModelCredentialsSetIdFromUI(item);
        updateModelPortNumberFromUI(item);
        updateModelGcPortNumberFromUI(item);
        updateModelEncryptionMethodFromUI(item);
        updateModelAuthenticationMethodFromUI(item);
        updateModelIgnoreCertificateErrorsFromUI(item);
        updateModelTotalCountersDisabledFromUI(item);
        updateModelDomainComponentsDisabledFromUI(item);
    }

    function validateCredentials() {
        return $(settingsDivControlId).closest('form').valid();
    }

    function validateConfiguration(customSettings) {
        var dcPortNumberErrorControl = $("#domainControllerPortNumberError").hide();
        var gcPortNumberErrorControl = $("#globalCatalogPortNumberError").hide();

        var isDcPortValid = validatePortNumber(customSettings.Port);
        if (!isDcPortValid) {
            showErrorMessageForInput(dcPortNumberErrorControl);
        }
        var isGcPortValid = validatePortNumber(customSettings.GcPort);
        if (!isGcPortValid) {
            showErrorMessageForInput(gcPortNumberErrorControl);
        }

        return isDcPortValid && isGcPortValid;
    }

    function validatePortNumber(portNumber) {
        var port = Number(portNumber);
        if (isNaN(port)) {
            return false;
        } else if (port < 1 || port > 65535) {
            return false;
        }
        return true;
    }

    function showErrorMessageForInput(errorControl) {
        errorControl.show();
        errorControl.find('.error').show();
    }

    function updateModelCredentialsSetIdFromUI(app) {
        updateModelSettingFromUI(app, credentialSetIdKey, $(credentialsControlIds.definedListId), function (value) {
            areNewCredentials = parseInt(value) === SW.APM.SelectCredentials.NewCredentialId;
            if (areNewCredentials) {
                isModelUpdated = true;
            }
        });
    }

    function updateModelPortNumberFromUI(app) {
        updateModelSettingFromUI(app, portNumberKey, $(portNumberSelector), function (value) {
            markIfModelUpdated(initialPortNumber, value);
        });
    }

    function updateModelGcPortNumberFromUI(app) {
        updateModelSettingFromUI(app, gcPortNumberKey, $(gcPortNumberSelector), function (value) {
            markIfModelUpdated(initialGcPortNumber, value);
        });
    }

    function updateModelEncryptionMethodFromUI(app) {
        updateModelSettingFromUI(app, encryptionMethodKey, encryptionMethodSelector, function (value) {
            markIfModelUpdated(initialEncryptionMethod, value);
        });
    }

    function updateModelAuthenticationMethodFromUI(app) {
        updateModelSettingFromUI(app, authenticationMethodKey, authenticationMethodSelector, function (value) {
            markIfModelUpdated(initialAuthenticationMethod, value);
        });
    }
    
    function updateModelIgnoreCertificateErrorsFromUI(app) {
        var setting = findSetting(ignoreCertificateErrorsKey, app.Settings);
        if (setting) {
            setting.SettingLevel = settingLevelInstance;
            setting.Value = ignoreCertificateErrorsSelector.is(':checked');
        }
    }

    function updateModelTotalCountersDisabledFromUI(app) {
        var setting = findSetting(totalCountersDisabledKey, app.Settings);
        if (setting) {
            setting.SettingLevel = settingLevelInstance;
            setting.Value = !enableTotalCountersSelector.is(':checked');
        }
    }

    function updateModelDomainComponentsDisabledFromUI(app) {
        var setting = findSetting(domainComponentsDisabledKey, app.Settings);
        if (setting) {
            setting.SettingLevel = settingLevelInstance;
            setting.Value = !enableDomainComponentSelector.is(':checked');
        }
    }

    function updateModelSettingFromUI(app, settingId, controlSelector, onSuccess) {
        var setting = findSetting(settingId, app.Settings);
        if (setting) {
            setting.SettingLevel = settingLevelInstance;
            setting.Value = controlSelector.val();
            onSuccess(setting.Value);
        }
    }

    function markIfModelUpdated(initialValue, UIValue) {
        if (initialValue !== UIValue) {
            isModelUpdated = true;
        }
    }
    // <-- SW.APM.BB.ActiveDirectory.EditAppSettings
    Ext.ComponentMgr.registerPlugin('SW.APM.BB.ActiveDirectory.ApplicationEditor', ActiveDirectoryBB.ApplicationEditor);
});
