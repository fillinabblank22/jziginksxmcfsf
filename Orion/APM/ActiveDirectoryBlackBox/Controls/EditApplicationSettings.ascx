﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditApplicationSettings.ascx.cs" Inherits="Orion_APM_ActiveDirectoryBlackBox_Controls_EditApplicationSettings" %>
<%@ Import Namespace="SolarWinds.APM.BlackBox.ActiveDirectory.Common.Constants" %>
<%@ Import Namespace="SolarWinds.APM.Common.Models" %>
<%@ Register TagPrefix="apm" TagName="SelectCredentials" Src="~/Orion/APM/Admin/MonitorLibrary/Controls/SelectCredentials.ascx" %>
<%@ Register TagPrefix="apm" TagName="TestButtonSummary" Src="~/Orion/APM/Controls/TestButtonSummary.ascx" %>
<%@ Register TagPrefix="apm" TagName="SelectPortNumber" Src="~/Orion/APM/Controls/SelectPortNumber.ascx" %>

<orion:include runat="server" file="APM/js/EditBBApplication.js" />
<orion:include runat="server" file="APM/ActiveDirectoryBlackBox/js/ActiveDirectoryWarningHandler.js" />
<orion:include runat="server" file="APM/ActiveDirectoryBlackBox/js/EditApplicationSettings.js" />
<orion:include runat="server" file="APM/ActiveDirectoryBlackBox/Styles/EditApplicationSettings.css" />
<script type="text/javascript">
    SW.APM.BB.ActiveDirectory.EditAppSettings.init({
        testButtonClientId: '<%= ControlHelper.EncodeJsString(this.testButton.ClientID) %>',
        testSummaryClientId:  '<%= ControlHelper.EncodeJsString(this.aadTestSummary.ClientID) %>',
        credentialSetIdKey: '<%= ControlHelper.EncodeJsString(InternalSettings.CredentialSetIdKey) %>',
        portNumberKey: '<%= ControlHelper.EncodeJsString(Applications.Settings.PortSettingName) %>',
        gcPortNumberKey: '<%= ControlHelper.EncodeJsString(Applications.Settings.GcPortSettingName) %>',
        encryptionMethodKey: '<%= ControlHelper.EncodeJsString(Applications.Settings.EncryptionMethodSettingName) %>',
        authenticationMethodKey: '<%= ControlHelper.EncodeJsString(Applications.Settings.AuthenticationMethodSettingName) %>',
        ignoreCertificateErrorsKey: '<%= ControlHelper.EncodeJsString(Applications.Settings.IgnoreCertificateErrorsSettingName) %>',
        totalCountersDisabledKey: '<%= ControlHelper.EncodeJsString(Applications.Settings.TotalCountersDisabledSettingName) %>',
		domainComponentsDisabledKey: '<%= ControlHelper.EncodeJsString(Applications.Settings.DomainComponentsDisabledSettingName) %>',
        applicationCustomType: '<%= ControlHelper.EncodeJsString(Applications.TemplateCustomType) %>',
        portNumberClientId: '<%= ControlHelper.EncodeJsString(Applications.Settings.DomainControllerPortNumberKey) %>',
        gcPortNumberClientId: '<%= ControlHelper.EncodeJsString(Applications.Settings.GlobalCatalogPortNumberKey) %>',
        pollingTimeoutClientId: '<%= ControlHelper.EncodeJsString(Applications.Settings.AppTimeoutKey) %>',
        settingLevelInstance: '<%= (int) SettingLevel.Instance %>',
        encryptionMethodClientId:  '<%= ControlHelper.EncodeJsString(this.encryptionMethod.ClientID) %>',
        authenticationMethodClientId:  '<%= ControlHelper.EncodeJsString(this.authenticationMethod.ClientID) %>',
        ignoreCertificateErrorsClientId:  '<%= ControlHelper.EncodeJsString(this.ignoreCertificateErrors.ClientID) %>',
        enableTotalCountersClientId: '<%= ControlHelper.EncodeJsString(this.enableTotalCounters.ClientID) %>',
        enableDomainComponentsClientId: '<%= ControlHelper.EncodeJsString(this.enableDomainComponents.ClientID) %>',
        saveCredentialsTitle: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Title) %>',
        saveCredentialsInvalidCredentialsMsg: '<%= ControlHelper.EncodeJsString(Resources.APMWebContent.EditAppSettings_SaveCredentialChanges_Msg) %>',
        saveCredentialsCannotConnectMsg: '<%= ControlHelper.EncodeJsString(Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_SaveCredentialsConnectionFailed) %>',
        testConnectionIncorrectCredentialsMsg: '<%= ControlHelper.EncodeJsString(Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_UserCredentialsFailed) %>',
        testConnectionCannotConnectMsg: '<%= ControlHelper.EncodeJsString(Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_ConnectionFailed) %>',
        testConnectionFailedMsg: '<%= ControlHelper.EncodeJsString(Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_CredentialsFailed) %>',
        totalCountersUniqueIds: '<%= ControlHelper.ToJSON(Applications.Settings.TotalCounterPerformanceComponentUniqueIds) %>',
        domainComponentsUniqueIds: '<%= ControlHelper.ToJSON(Applications.Settings.DomainComponentsUniqueIds) %>'
    });

    function handleSecurityWarning() {
        const warningSelector = "#nonSecureConnWarning";
        const isIgnoreCertificateErrorsSelector = "#<%= ControlHelper.EncodeJsString(this.ignoreCertificateErrors.ClientID) %>";
        const selectedAuthenticationMethodSelector = "#<%= ControlHelper.EncodeJsString(this.authenticationMethod.ClientID) %>";

        SW.APM.BB.ActiveDirectory.SetNonSecureConnWarningVisibility(warningSelector, isIgnoreCertificateErrorsSelector, selectedAuthenticationMethodSelector);
    }
</script>

<table id="appEditActiveDirectorySettings" class="appProperties" cellspacing="0" style="display: none;">
    <tr>
        <td class="label">
            <%= Resources.APM_ActiveDirectoryBlackBoxContent.EditApplicationSettings_CredentialsName%>
        </td>
        <td style="height: 170px">
            <table class="selectCreds" style="width: 410px">
                <apm:selectcredentials runat="server" id="aadCredentials" validationgroupname="SelectCredentialsValidationGroup" allownodewmicredential="True" />
            </table>
        </td>
    </tr>
    <tr id="domainControllerPortNumberRow">
        <td class="label">
            <%=  Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_LdapPortNumber %>
        </td>
        <td>
            <input id="domainControllerPortNumber" name="domainControllerPortNumber" class="number domainControllerPortNumber" maxlength="5" />
            <span id="domainControllerPortNumberError" class="validationMsg sw-suggestion sw-suggestion-fail domainControllerPortNumberError">
                <span class="sw-suggestion-icon"></span>
                <span class="validationError">
                    <label class="error"><%=  Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_InvalidPortNumberError %></label>
                </span>
            </span>
        </td>
        <td></td>
    </tr>
    <tr id="globalCatalogPortNumberRow">
        <td class="label">
            <%=  Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_GcPortNumber %>
        </td>
        <td>
            <input id="globalCatalogPortNumber" name="globalCatalogPortNumber" class="number globalCatalogPortNumber" maxlength="5" />
            <span id="globalCatalogPortNumberError" class="validationMsg sw-suggestion sw-suggestion-fail globalCatalogPortNumberError">
                <span class="sw-suggestion-icon"></span>
                <span class="validationError">
                    <label class="error"><%=  Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_InvalidPortNumberError %></label>
                </span>
            </span>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_EncryptionMethodLabel%>
        </td>
        <td>
            <asp:DropDownList ID="encryptionMethod" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_AuthenticationMethodLabel%>
        </td>
        <td>
            <asp:DropDownList ID="authenticationMethod" runat="server" AutoPostBack="false" onClick="handleSecurityWarning()">
            </asp:DropDownList>
        </td>
        <td></td>
    </tr>
    <asp:PlaceHolder runat="server" ID="phNonSecureConn">
        <tr id="nonSecureConnWarning" class="non-secure-warning-container">
            <td>&nbsp;</td>
            <td><div class="non-secure-warning"><%= Resources.APMWebContent.NonSecureConnectionHasBeenSelected %></div></td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td class="label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_IgnoreCertificateErrors%>
        </td>
        <td>
            <asp:Checkbox ID="ignoreCertificateErrors" runat="server" AutoPostBack="false" onChange="handleSecurityWarning()">
            </asp:Checkbox>
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="testRow" style="width: 430px">
            <apm:testbuttonsummary runat="server" id="aadTestSummary" />
            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="SelectCredentialsValidationGroup" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="testButtonRow">
            <orion:localizablebutton runat="server" id="testButton" text="<%$ Resources: APMWebContent, TEST_CONNECTION %>" displaytype="Secondary" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_EnableDomainComponentsLabel%>
        </td>
        <td>
            <asp:Checkbox ID="enableDomainComponents" runat="server" AutoPostBack="false">
            </asp:checkbox>
        </td>
        <td></td>
    </tr>
     <tr>
         <td><span class="label"><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_EnableTotalCountersLabel%></span>
             <p><%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_EnableTotalCountersWarning%></p>
             <div class="ConfigHelpLink" style="font-size: 11px;">
                 <label><a href="<%= SolarWinds.APM.Web.HelpLocator.CreateHelpUrl("SAMAGAppInsActiveDirectoryEditTemplate") %>" target="_blank">&#0187;<%= Resources.APM_ActiveDirectoryBlackBoxContent.EditPage_EnableTotalCountersLink%></a></label>
             </div>
         </td>
        <td>
            <asp:Checkbox ID="enableTotalCounters" runat="server" AutoPostBack="false">
            </asp:Checkbox>
        </td>
        <td></td>
    </tr>
</table>
