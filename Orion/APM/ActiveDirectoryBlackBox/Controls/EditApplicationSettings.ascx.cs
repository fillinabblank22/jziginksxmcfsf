﻿using System;
using SolarWinds.APM.Common;
using SolarWinds.APM.Common.Models;
using SolarWinds.APM.BlackBox.ActiveDirectory.Common.Constants;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Extensions;
using SolarWinds.APM.BlackBox.ActiveDirectory.Web.UI;

public partial class Orion_APM_ActiveDirectoryBlackBox_Controls_EditApplicationSettings : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            ActiveDirectoryControlHelpers.BindComboBox<UseLdapEncryptionSettings>(encryptionMethod, UseLdapEncryptionSettings.None.ToString());
            ActiveDirectoryControlHelpers.BindComboBox<UseLdapAuthenticationSettings>(authenticationMethod, UseLdapAuthenticationSettings.Negotiate.ToString());
        }
    }
}