<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChartData.aspx.cs" Inherits="Orion_APM_ChartData" %>
<%@ Register TagPrefix="apm" TagName="CustomChartControl" Src="~/Orion/APM/Controls/InternalCustomChart.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= Resources.APMWebContent.ChartData%></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <apm:CustomChartControl runat="server" ID="CustomChart" Visible="false"/>        
    </div>
    </form>
</body>
</html>
