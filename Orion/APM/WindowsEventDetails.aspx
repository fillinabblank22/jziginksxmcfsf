﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true"
    CodeFile="WindowsEventDetails.aspx.cs" Inherits="Orion_APM_WindowsEventDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI" %>
<%@ Import Namespace="SolarWinds.APM.Web.Extensions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <orion:Include ID="Include1" Framework="Ext" FrameworkVersion="4.0" runat="server" />
    <orion:Include ID="Include3" Module="APM" File="js/Ext4AjaxProxy.js" runat="server" />
    <orion:Include ID="Include4" Module="APM" File="Resources/Monitor/WindowsEventDetails.js" runat="server" />
    <div class="titleTable">
        <h1><%=Resources.APMWebContent.APMWEBCODE_PV0_1%></h1>
    </div>
    <div style="font-weight: bold;padding-left: 10px;padding-bottom: 10px">
    <%= String.Format(Resources.APMWebContent.APMWEBDATA_AK1_59,
    String.Format("<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(Monitor.NetObjectID), Monitor.Name),
        String.Format("<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(Monitor.Application.NetObjectID), Monitor.Application.Name),
            String.Format("<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(Monitor.Application.NPMNode.NetObjectID), Monitor.Application.NPMNode.Name))%>
    </div>
    <div style="margin-left: 10px; width:1172px;">
        <div id="WindowsEventGrid-0-toolbar" style="width:100%;">
            <span style="float: left;">
                <div id="WindowsEventGrid-0-toolbar-left"></div>
            </span>
            <span style="float: right;">
                <div id="WindowsEventGrid-0-toolbar-right"></div>
            </span>
        </div>
        <div style="clear: both;"></div>
        <div id="WindowsEventGrid-0" style="width: 100%">
        </div>
    </div>
    <script language="javascript" type="text/javascript">
            $(document).ready(function() {
                SW.APM.WindowsEventGrid.createGrid(0, <%= Request.QueryStringOrForm("ComponentId") %>, 20);
            });
    </script>
</asp:Content>
