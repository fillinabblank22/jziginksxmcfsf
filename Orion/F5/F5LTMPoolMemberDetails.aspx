﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
    CodeFile="F5LTMPoolMemberDetails.aspx.cs" Inherits="Orion_F5_F5LTMPoolMemberDetails" Title="F5 LTM Pool Member Details" %>

<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="f5" Namespace="SolarWinds.F5.Web.UI" Assembly="SolarWinds.F5.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="ObsoleteF5DataBar" Src="~/Orion/F5/Controls/ObsoleteF5DataBar.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
	<h1>
	    <%= F5WebContent.F5WEBDATA_LH0_40 %>:
        <asp:Image ID="LTMPoolMemberStatusIcon" runat="server" style="vertical-align: text-bottom"/>
        <%= PoolMemberName %>
	</h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <orion:ObsoleteF5DataBar runat="server" ID="ObsoleteDataMessageBar" />

	<f5:F5LTMPoolMemberResourceHost runat="server" ID="ltmPoolMemberHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</f5:F5LTMPoolMemberResourceHost>
</asp:Content>
