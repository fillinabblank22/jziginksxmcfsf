﻿using System;
using System.Web;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Shared;

public partial class Orion_F5_F5LTMPoolMemberDetails : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var poolmember = NetObject as F5LTMPoolMember;
        if (poolmember == null)
            throw new IndexOutOfRangeException(string.Format(F5WebContent.F5WEBCODE_PS0_15, NetObject.GetType().Name, NetObject.NetObjectID));



        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.LTMPoolMemberStatusIcon.ImageUrl = "~" + StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.PoolMember", (int)poolmember.OrionStatus, StatusIconSize.Small);
        this.LTMPoolMemberStatusIcon.AlternateText = this.LTMPoolMemberStatusIcon.ToolTip = StatusInfo.GetStatus((int)poolmember.OrionStatus).ShortDescription;

        this.PoolMemberName = HttpUtility.HtmlEncode(poolmember.ShortName);
        this.Title = string.Concat(this.ViewInfo.ViewTitle, " - ", this.PoolMemberName);

        ObsoleteDataMessageBar.NodeID = poolmember.NodeId;
    }

    protected string PoolMemberName { get; private set; }

    public override string ViewType
    {
        get { return "F5LTMPoolMemberDetails"; }
    }
}