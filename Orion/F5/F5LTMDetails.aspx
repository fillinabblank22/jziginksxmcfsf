﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true"
    CodeFile="F5LTMDetails.aspx.cs" Inherits="Orion_F5_F5LTMDetails" Title="F5 LTM Details" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="orion" TagName="ObsoleteF5DataBar" Src="~/Orion/F5/Controls/ObsoleteF5DataBar.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="f5" Namespace="SolarWinds.F5.Web.UI" Assembly="SolarWinds.F5.Web" %>
<%@ Reference Control="~/Orion/View.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" runat="Server">
    <h1>
        <%= F5WebContent.F5WEBCODE_PS0_2_1 %>:
        <asp:Image ID="LTMStatusIcon" runat="server" Style="vertical-align: text-bottom" />
        <%= F5WebContent.F5WEBCODE_PS0_1 %>
        <orion:StatusIconControl ID="statusIconControl" runat="server" EntityFullName="Orion.Nodes" EntityIdName="NodeID" EntityId='<%$ Code: this.NetObject["ID"] %>' />
        <orion:NodeLink runat="server" NodeID='<%$ Code: string.Format("N:{0}", this.NetObject["ID"]) %>' />        
    </h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <orion:ObsoleteF5DataBar runat="server" ID="ObsoleteDataMessageBar" />
    
    <f5:F5LTMResourceHost runat="server" ID="ltmHost">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </f5:F5LTMResourceHost>
</asp:Content>
