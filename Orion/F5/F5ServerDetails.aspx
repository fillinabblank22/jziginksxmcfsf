﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
         CodeFile="F5ServerDetails.aspx.cs" Inherits="Orion_F5_F5ServerDetails" 
         Title="F5 Server Details"%>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="f5" Namespace="SolarWinds.F5.Web.UI" Assembly="SolarWinds.F5.Web" %>
<%@ Register TagPrefix="orion" TagName="ObsoleteF5DataBar" Src="~/Orion/F5/Controls/ObsoleteF5DataBar.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
	<h1>
	    <%= F5WebContent.F5WEBCODE_PS0_18 %>:
        <asp:Image ID="SStatusIcon" runat="server" style="vertical-align: text-bottom" />
        <%= ServerName  %>
	</h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <orion:ObsoleteF5DataBar runat="server" ID="ObsoleteDataMessageBar" />

	<f5:F5ServerResourceHost runat="server" ID="mapID">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</f5:F5ServerResourceHost>
</asp:Content>