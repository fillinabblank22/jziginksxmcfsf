﻿<%@ Page Language="C#" AutoEventWireup="true" 
    MasterPageFile="~/Orion/View.master"
    CodeFile="F5VirtualServerDetails.aspx.cs" Inherits="Orion_F5_F5VirtualServerDetails"
    Title="F5 Virtual Server Details" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="f5" Namespace="SolarWinds.F5.Web.UI" Assembly="SolarWinds.F5.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="ObsoleteF5DataBar" Src="~/Orion/F5/Controls/ObsoleteF5DataBar.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
	<h1>
	    <%= F5WebContent.F5WEBCODE_PS0_17 %>:
        <asp:Image ID="VSStatusIcon" runat="server" style="vertical-align: text-bottom" />
        <%= VirtualServerName  %>
	</h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <orion:ObsoleteF5DataBar runat="server" ID="ObsoleteDataMessageBar" />

	<f5:F5VirtualServerResourceHost runat="server" ID="mapID">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</f5:F5VirtualServerResourceHost>
</asp:Content>
