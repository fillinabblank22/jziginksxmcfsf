﻿using System;
using System.Web;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Shared;

public partial class Orion_F5_F5GTMWideIPDetails : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var gtmWideIp = NetObject as F5GTMWideIP;
        if (gtmWideIp == null)
            throw new IndexOutOfRangeException(string.Format(F5WebContent.F5WEBCODE_PS0_15, NetObject.GetType().Name, NetObject.NetObjectID));

        ((Orion_View)this.Master).HelpFragment = "orionphresourcef5gtmwideipdetails";

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.GTMWideIPStatusIcon.ImageUrl = "~" + StatusIconProvider.Instance.GetImagePath("Orion.F5.GTM.WideIP", (int)gtmWideIp.OrionStatus, StatusIconSize.Small);
        this.GTMWideIPStatusIcon.AlternateText = this.GTMWideIPStatusIcon.ToolTip = StatusInfo.GetStatus((int)gtmWideIp.OrionStatus).ShortDescription;

        this.WideIPName = HttpUtility.HtmlEncode(gtmWideIp.ShortName);
        this.Title = string.Concat(this.ViewInfo.ViewTitle, " - ", this.WideIPName);

        ObsoleteDataMessageBar.NodeID = gtmWideIp.ParentId;
    }

    public string WideIPName { get; private set; }

    public override string ViewType
    {
        get { return "F5GTMWideIPDetails"; }
    }
}