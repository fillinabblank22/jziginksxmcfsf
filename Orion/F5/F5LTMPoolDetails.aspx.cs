﻿using System;
using System.Web;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Shared;

public partial class Orion_F5_F5LTMPoolDetails : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var pool = NetObject as F5LTMPool;
        if (pool == null)
            throw new IndexOutOfRangeException(string.Format(F5WebContent.F5WEBCODE_PS0_15, NetObject.GetType().Name, NetObject.NetObjectID));



        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.LTMPoolStatusIcon.ImageUrl = "~" + StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.Pool", (int)pool.OrionStatus, StatusIconSize.Small);
        this.LTMPoolStatusIcon.AlternateText = this.LTMPoolStatusIcon.ToolTip = StatusInfo.GetStatus((int)pool.OrionStatus).ShortDescription;

        this.PoolName = HttpUtility.HtmlEncode(pool.ShortName);
        this.Title = string.Concat(this.ViewInfo.ViewTitle, " - ", this.PoolName);

        ObsoleteDataMessageBar.NodeID = pool.NodeID;
    }

    protected string PoolName { get; private set; }

    public override string ViewType
    {
        get { return "F5LTMPoolDetails"; }
    }
}