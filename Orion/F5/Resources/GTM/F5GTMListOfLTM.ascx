﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="F5GTMListOfLTM.ascx.cs" Inherits="Orion_F5_Resources_GTM_F5GTMListOfLTM" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<orion:Include ID="Include" runat="server" File="F5/Styles/TabularResource.css" /> 

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="f5-tabular-resource">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
 
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize({
                    uniqueId: <%= CustomTable.UniqueClientID %>,
                    initialPage: 0,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                    allowSort: true,
                    columnSettings: {
                        "LtmName": {
                            header: '<%= Resources.F5WebContent.F5WEBCODE_PS0_10 %>',
                            cellCssClassProvider: function() {
                                return 'f5-field-entity';
                            },
                            isHtml: true
                        },
                        "IpAddress": {
                            header: '<%= Resources.F5WebContent.F5WebCODE_MP0_10 %>'
                        },
                        "TrafficRequests": {
                            header: '<%= Resources.F5WebContent.F5WEBDATA_JP0_22 %>',
							cellCssClassProvider: function() {
								return 'align-right';
							},
							headerCssClass: 'align-right'
                        }
                    }
                });
 
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</orion:resourceWrapper>
