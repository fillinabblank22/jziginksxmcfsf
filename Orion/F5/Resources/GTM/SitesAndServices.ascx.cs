﻿using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;

public partial class Orion_F5_Resources_GTM_SitesAndServices : BaseResourceControl
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            yield return typeof(IF5GTMProvider);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var provider = this.GetInterfaceInstance<IF5GTMProvider>();

        if(provider == null || provider.F5Gtm == null)
        {
            // no provider or missing netobject
            this.Visible = false;
            return;
        }

        int nodeId = provider.Node.NodeID;

        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL.Replace("@NodeID", nodeId.ToString(CultureInfo.InvariantCulture));
        CustomTable.OrderBy = DefaultOrderBy;
    }

    public string SWQL
    {
        get
        {
            return @"
SELECT
  s.WideIPID as _WideIPID,
  s.ShortName,  
  s.LBModeDescription,
  FLOOR(s.RequestsPerSec) AS RequestsPerSec,
  FLOOR(s.ResolutionsPerSec) AS ResolutionsPerSec,
  s.OrionStatus as _OrionStatus,
  s.DetailsUrl as _DetailsUrl
FROM Orion.F5.GTM.WideIP s
WHERE s.F5Device.Node.NodeID = @NodeID";
        }
    }

    public string DefaultOrderBy
    {
        get
        {
            return "ShortName ASC";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcesitesservices";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_LH0_25; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/F5/Controls/EditResourceControls/EditSitesAndServices.ascx";
        }
    }
}