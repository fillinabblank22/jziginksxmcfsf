﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.F5.Web.DAL.SummaryStatus;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.F5.Web.UI;

public partial class Orion_F5_Resources_GTM_SummaryStatus : SummaryStatusResourceControl
{
    private readonly ISummaryStatus dal = new F5GTMSummaryStatus();

    protected override string DefaultTitle
    {
        get { return F5WebContent.F5WEBDATA_JP0_10; }
    }

    public override String HelpLinkFragment
    {
        get { return ("orionphgtmsummary"); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IF5GTMProvider) }; }
    }

    protected override ISummaryStatus Dal
    {
        get { return dal; }
    }

    protected override IEnumerable<string> EntityNames
    {
        get
        {
            yield return "Orion.F5.GTM.WideIP";
            yield return "Orion.F5.System.ModuleLTM";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        statusComponent.NetObject = "F5GTM:" + NodeID;
        AddTileData(statusComponent.Tiles);
    }
}
