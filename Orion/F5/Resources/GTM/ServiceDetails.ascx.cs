﻿using System;
using System.Collections.Generic;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Resources_GTM_ServiceDetails : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var wip = GetInterfaceInstance<IF5GTMWideIPProvider>();
        if (wip == null)
        {
            return;
        }

        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5GTM";
        GenericTable.Action = "GetServiceDetails";
        GenericTable.ElementId = wip.F5GtmWideIP.Id.ToString();
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_PS0_7; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IF5GTMWideIPProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcegtmservicedetails";
        }
    }
}