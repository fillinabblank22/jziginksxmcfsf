﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SummaryStatus.ascx.cs" Inherits="Orion_F5_Resources_GTM_SummaryStatus" %>
<%@ Register TagPrefix="orion" TagName="EnvironmentStatus" Src="~/Orion/F5/Controls/EnvironmentStatus.ascx" %>

<orion:Include File="EnvironmentStatus.css" runat="server" Module="F5" />

<orion:resourceWrapper runat="server">
    <Content>
        <orion:EnvironmentStatus runat="server" ID="statusComponent" />
    </Content>
</orion:resourceWrapper>
