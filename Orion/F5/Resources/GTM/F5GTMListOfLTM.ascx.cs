﻿using System;
using System.Collections.Generic;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_F5_Resources_GTM_F5GTMListOfLTM : BaseResourceControl
{
    private const string SwqlAvailableData = @"
        SELECT
		    TOP 1 gtmvs.Map.LTMVirtualServer.NodeID
	    FROM
		    Orion.F5.GTM.VirtualServer gtmvs
        JOIN
	        Orion.F5.System.ModuleLTM ltm ON ltm.NodeID = NodeID
	    WHERE
		    gtmvs.NodeID = @nodeId
            AND
            gtmvs.Map.LTMVirtualServer.NodeID IS NOT NULL";

    private string SwqlGetData
    {
        get
        {
            const string baseSelectQuery = @"
                SELECT
	                n.NodeName AS LtmName,
	                n.IpAddress,
	                ROUND(ISNULL(ltmPicks.VsPicks,0), 0) AS TrafficRequests,
	                '/Orion/View.aspx?NetObject=F5LTM:'+ ToString(ltmPicks.LtmNodeID) AS _LinkFor_LtmName,
	                '/Orion/StatusIcon.ashx?entity=Orion.F5.System.Module&size=small&hint=LTM&status=' + ToString(ltm.OrionStatus) AS _IconFor_LtmName
                FROM
                (
	                SELECT
		                gtms.VSPicks,
		                gtms.VirtualServer.Map.LtmVirtualServer.NodeId as LtmNodeID
	                FROM
		                Orion.F5.GTM.Server gtms
	                WHERE
		                gtms.NodeID = {0}
	                GROUP BY
		                gtms.ServerName,
		                gtms.VirtualServer.Map.LtmVirtualServer.NodeId,
                        gtms.VSPicks
                ) ltmPicks
                JOIN
	                Orion.Nodes n ON n.NodeID = ltmPicks.LtmNodeID
                JOIN
	                Orion.F5.System.ModuleLTM ltm ON ltm.NodeID = ltmPicks.LtmNodeID";

            return string.Format(baseSelectQuery, F5GTM.Node.NodeID);
        }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    private F5GTM F5GTM
    {
        get
        {
            var provider = GetInterfaceInstance<IF5GTMProvider>();

            return provider != null ? provider.F5Gtm : null;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!AreDataAvailable())
        {
            Visible = false;
            return;
        }
        
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SwqlGetData;
        CustomTable.OrderBy = "n.NodeName";
    }

    private bool AreDataAvailable()
    {
        if (F5GTM == null)
        {
            return false;
        }

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var result = proxy.Query(SwqlAvailableData, new Dictionary<string, object>
            {
                { "nodeId", F5GTM.Node.NodeID }
            });

            return result != null && result.Rows.Count > 0;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_JP0_21; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IF5GTMProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionphresourcef5gtmlocaltrafficmanagers"; }
    }
}
