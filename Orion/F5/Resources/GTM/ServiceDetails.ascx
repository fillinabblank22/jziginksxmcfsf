﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServiceDetails.ascx.cs" Inherits="Orion_F5_Resources_GTM_ServiceDetails" %>
<%@ Register Src="~/Orion/F5/Controls/DetailsTable.ascx" TagPrefix="orion" TagName="GenericTable" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
  <Content>    
    <orion:GenericTable runat="server" ID="GenericTable" />
 </Content>
</orion:ResourceWrapper>
