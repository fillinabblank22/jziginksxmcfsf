﻿using System;
using System.Collections.Generic;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Resources_GTM_F5GTMDetails : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var gtm = GetInterfaceInstance<IF5GTMProvider>();
        if (gtm == null)
        {
            return;
        }

        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5GTM";
        GenericTable.Action = "GetGTMDetails";
        GenericTable.ElementId = gtm.F5Gtm.Id.ToString();
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_JF0_00; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IF5GTMProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcegtmdetails";
        }
    }

}