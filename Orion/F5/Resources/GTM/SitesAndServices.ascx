﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SitesAndServices.ascx.cs" Inherits="Orion_F5_Resources_GTM_SitesAndServices" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:Include ID="Include1" runat="server" Module="F5" File="DetailsTable.css" Section="Top" />
<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {				
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowPaging: true,
                        allowSort: true,
                        columnSettings: {
                            "ShortName": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_26 %>",
                                formatter: function(value, row, cellInfo){
                                    var status = row[5];
                                    var url = row[6];
                                    
                                    return "<a class='nowrap tooltip' href='"+url+"'><img class='icon' src='/Orion/StatusIcon.ashx?entity=Orion.F5.GTM.WideIP&status="+status+"&size=small'/> <span class='name'>" + value + "</span></a>";
                                    
                                },
                                isHtml: true
                            },
                            "LBModeDescription": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_27 %>",
								cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'width-100';
                                }
                            },
                            "RequestsPerSec": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_28 %>",
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'align-right width-100';
                                },
                                headerCssClass: 'align-right'

                            },
                            "ResolutionsPerSec": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_29 %>",
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'align-right width-100';
                                },
                                headerCssClass: 'align-right'
                            }
                        }
                    });

                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
