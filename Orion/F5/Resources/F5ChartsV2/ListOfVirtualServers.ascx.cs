﻿using System;
using System.Linq;
using System.Web.UI;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.Orion.Web.UI;
using SolarWinds.F5.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;

public partial class Orion_F5_Resources_F5ChartsV2_ListOfVirtualServers : SolarWinds.F5.Web.Resources.F5EntityListBase
{
    private readonly string[] enabledStates =
    {
        Resources.F5WebContent.F5WebCODE_MP0_0,
        Resources.F5WebContent.F5WebCODE_MP0_6,
        Resources.F5WebContent.F5WebCODE_MP0_7,
        Resources.F5WebContent.F5WebCODE_MP0_8
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            IF5LTMDALService dalService = new F5LTMDalService(swisProxy);
            bool isF5PollerEnabled = dalService.IsF5PollerEnabled(Node.NodeID);
            if (!isF5PollerEnabled)
            {
                Visible = false;
                return;
            }

            var ltmVirtualServers = dalService.GetVirtualServerResults(Node.NodeID);

            if (ltmVirtualServers == null || !ltmVirtualServers.Any())
            {
                Visible = false;
                return;
            }

            List.DataSource = ltmVirtualServers;
            List.DataBind();
        }
    }


    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_MP0_15; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public string GetEnabledState(object index)
    {
        return GetState(enabledStates, index);
    }

    public string GetAvailabilityState(object index)
    {
        if (index == null || index is DBNull)
        {
            return String.Empty;
        }

        int i = Convert.ToInt32(index);
        return String.Format("<img src=\"{0}\" title=\"{1}\" />",
            WebSecurityHelper.HtmlEncode(StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.VirtualServer", i, StatusIconSize.Small)),
            WebSecurityHelper.HtmlEncode(StatusInfo.GetStatus(i).ShortDescription)
		);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceF5ListOfVirtualServers"; }
    }

	public string GetEscapedProperty(object dataItem, string propertyName) {
		var evaluatedProperty = DataBinder.Eval(dataItem, propertyName);
		return evaluatedProperty != null
			? WebSecurityHelper.HtmlEncode(evaluatedProperty.ToString())
			: string.Empty;
	}
}