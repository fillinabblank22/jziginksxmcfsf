﻿using System;
using SolarWinds.F5.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NetPerfMon_Resources_Inventory_F5DeviceStatus : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var node = GetInterfaceInstance<INodeProvider>();
        if (node== null)
        {
            return;
        }

        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            var f5LtmDal = new F5LTMDalService(swisProxy);
            var isF5PollerEnabled = f5LtmDal.IsF5PollerEnabled(node.Node.NodeID);

            if (!isF5PollerEnabled)
            {
                Visible = false;
                return;
            }
        }

        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5Device";
        GenericTable.Action = "GetF5DeviceDetails";
        GenericTable.ElementId = node.Node.NodeID.ToString();
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBCODE_LF0_13; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceF5DeviceDetails"; }
    }
}
