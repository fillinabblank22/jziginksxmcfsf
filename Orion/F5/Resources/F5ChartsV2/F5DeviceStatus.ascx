﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="F5DeviceStatus.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Inventory_F5DeviceStatus" %>
<%@ Register Src="~/Orion/F5/Controls/DetailsTable.ascx" TagPrefix="orion" TagName="GenericTable" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
  <Content>    
    <orion:GenericTable runat="server" ID="GenericTable" />
 </Content>
</orion:ResourceWrapper>

