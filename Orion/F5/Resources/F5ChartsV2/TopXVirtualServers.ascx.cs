﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_F5_Resources_F5ChartsV2_TopXVirtualServers : StandardChartResource, IResourceIsInternal
{
    const string topXXProperty = "numberofseriestoshow";

    private NetObject supportedNetObject;

    protected void Page_Init(object sender, EventArgs e)
    {
        supportedNetObject = GetSupportedNetObject();

        const string chartTitleKey = "ChartTitle";
        if (!this.Resource.Properties.ContainsKey(chartTitleKey))
        {
            if (supportedNetObject != null)
            {
                this.Resource.Properties.Add(chartTitleKey, "${Caption}");
            }
            else
            {
                this.Resource.Properties.Add(chartTitleKey, string.Empty);
            }
        }

        HandleInit(WrapperContents);
    }

    public override string SubTitle
    {
        get
        {
            if (string.IsNullOrEmpty(base.SubTitle))
            {
                return string.Format(Resources.F5WebContent.F5WebCODE_AO0_7, GetIntProperty(topXXProperty, 5).ToString(CultureInfo.InvariantCulture));
            }

            return base.SubTitle;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBCODE_TM0_1; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] {}; }
    }
    
    public override string EditControlLocation
    {
        get { return "/Orion/F5/Controls/EditResourceControls/EditTopXXVirtualServers.ascx"; }
    }
   
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (supportedNetObject != null)
        {
            return new string[] {supportedNetObject.NetObjectID};
        }
        else
        {
            return new string[] {};
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get
        {
            if (supportedNetObject is F5LTM)
            {
                return F5LTM.NetObjectPrefix;
            }
            else if (supportedNetObject is F5GTMWideIP)
            {
                return F5GTMWideIP.NetObjectPrefix;
            }
            else
            {
                return "NPM_GENERIC";
            }
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	public bool IsInternal
    {
        get { return true; }
    }

    /// <summary>
    /// Tries to find NetObject supported by this resource.
    /// </summary>
    /// <returns>NetObject for LTM or Wide IP. null is none of these found.</returns>
    private NetObject GetSupportedNetObject()
    {
        var ltmProvider = GetInterfaceInstance<IF5LTMProvider>();
        if (ltmProvider != null)
        {
            return ltmProvider.F5Ltm;
        }
        
        var wideIpProvider = GetInterfaceInstance<IF5GTMWideIPProvider>();
        if (wideIpProvider != null)
        {
            return wideIpProvider.F5GtmWideIP;
        }

        return null;
    }
}