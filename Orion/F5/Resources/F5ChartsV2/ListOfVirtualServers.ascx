﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfVirtualServers.ascx.cs" Inherits="Orion_F5_Resources_F5ChartsV2_ListOfVirtualServers" %>
<%@ Import Namespace="SolarWinds.F5.Web.UI" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include ID="Include" runat="server" File="F5/Styles/TabularResource.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div style="max-height: 350px; overflow-y: auto" id="Output" runat="server">
            <asp:Repeater ID="List" runat="server">
                <HeaderTemplate>
                    <table cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                        <thead>
                            <tr class="HeaderRow">
                                <td class="ReportHeader" style="width: 15px;"></td>
                                <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_MP0_9%></td>
                                <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_MP0_10%></td>
                                <td class="ReportHeader f5-header-numeric" style="width: 40px;"><%=Resources.F5WebContent.F5WebCODE_MP0_11%></td>
                                <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_MP0_12%></td>

                                <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_AO0_2%></td>
                                <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_AO0_3%></td>
                                <td class="ReportHeader f5-header-numeric"><%=Resources.F5WebContent.F5WebCODE_AO0_4%></td>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# GetAvailabilityState(DataBinder.Eval(Container.DataItem, "F5LTMVirtualServer.OrionStatus"))%></td>
                        <td class="f5-column-long-text f5-column-entity-name">
                            <a href="<%# GetEscapedProperty(Container.DataItem, "F5LTMVirtualServer.DetailsUrl")%>">
                                <%# GetEscapedProperty(Container.DataItem, "F5LTMVirtualServer.ShortName")%>
                            </a>
                        </td>
                        <td class="f5-column-ip">
                            <%# WebSecurityHelper.HtmlEncode(Formating.FormatIpAddress(DataBinder.Eval(Container.DataItem, "IPAddress").ToString()))%>
                        </td>
                        <td class="f5-column-numeric"><%# GetEscapedProperty(Container.DataItem, "F5LTMVirtualServer.Port")%></td>
                        <td><%# WebSecurityHelper.HtmlEncode(GetEnabledState(DataBinder.Eval(Container.DataItem, "F5LTMVirtualServer.Enabled")))%></td>
                        <td class="f5-column-long-text f5-column-entity-name">
                            <a href="<%# GetEscapedProperty(Container.DataItem, "PoolDetailsUrl")%>">
                                <%# GetEscapedProperty(Container.DataItem, "PoolShortName")%>
                            </a>
                        </td>
                        <td><%# GetEscapedProperty(Container.DataItem, "F5LTMVirtualServer.WildIPMask")%></td>
                        <td class="f5-column-numeric"><%# new Dimensionless(GetEscapedProperty(Container.DataItem, "ConnectionCount"))%></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
            </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </Content>
</orion:ResourceWrapper>