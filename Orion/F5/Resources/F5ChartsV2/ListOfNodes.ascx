﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfNodes.ascx.cs" Inherits="Orion_F5_Resources_F5ChartsV2_ListOfNodes" %>
<%@ Import Namespace="SolarWinds.F5.Web.UI" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include ID="Include" runat="server" File="F5/Styles/TabularResource.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
    <div style="max-height:350px; overflow-y:auto" id="Output" runat="server">	
    <asp:Repeater ID="List" runat="server">
        <HeaderTemplate>
            <table cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                <thead>
                    <tr class="HeaderRow">
                        <td class="ReportHeader" style="width:15px;"></td>
                        <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_MP0_9%></td>
                        <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_MP0_10%></td>                        
                        <td class="ReportHeader"><%=Resources.F5WebContent.F5WebCODE_MP0_12%></td>
                        <td class="ReportHeader"><%=Resources.F5WebContent.F5WEBDATA_LH0_10%></td>
                    </tr>
                 </thead>
                 <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                    <tr>
                        <td><%# GetAvailabilityState( DataBinder.Eval(Container.DataItem, "OrionStatus") ) %></td>
                        <td class="f5-column-long-text f5-column-entity-name">
                            <a href="<%# DataBinder.Eval(Container.DataItem, "DetailsUrl")%>">
                                <%# DataBinder.Eval(Container.DataItem, "ShortName")%>
                            </a>
                        </td>
                        <td><%# Formating.FormatIpAddress(DataBinder.Eval(Container.DataItem, "IPAddress").ToString())%></td>
                        <td><%# GetEnabledState( DataBinder.Eval(Container.DataItem, "Enabled") ) %></td>
                        <td><%# DataBinder.Eval(Container.DataItem, "F5StatusReason")  %></td>
                    </tr>
        </ItemTemplate>
        <FooterTemplate>
                </tbody>
            </table>   
        </FooterTemplate>
    </asp:Repeater>
    </div>
    </Content>
</orion:ResourceWrapper>
