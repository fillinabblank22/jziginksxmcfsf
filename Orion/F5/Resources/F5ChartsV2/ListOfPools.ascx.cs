﻿using System;
using System.Linq;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.Orion.Web.UI;
using SolarWinds.F5.Web.DAL;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;

public partial class Orion_F5_Resources_F5ChartsV2_ListOfPools : SolarWinds.F5.Web.Resources.F5EntityListBase
{
    private readonly string[] enabledStates = 
    {
        Resources.F5WebContent.F5WebCODE_MP0_0,
        Resources.F5WebContent.F5WebCODE_MP0_6,
        Resources.F5WebContent.F5WebCODE_MP0_7,                                     
        Resources.F5WebContent.F5WebCODE_MP0_8
    };

    private readonly string[] modes = 
    {
        Resources.F5WebContent.F5WebCODE_MP0_18,
        Resources.F5WebContent.F5WebCODE_MP0_19,
        Resources.F5WebContent.F5WebCODE_MP0_20,
        Resources.F5WebContent.F5WebCODE_MP0_21,
        Resources.F5WebContent.F5WebCODE_MP0_22,
        Resources.F5WebContent.F5WebCODE_MP0_23,
        Resources.F5WebContent.F5WebCODE_MP0_24,
        Resources.F5WebContent.F5WebCODE_MP0_25,
        Resources.F5WebContent.F5WebCODE_MP0_26,
        Resources.F5WebContent.F5WebCODE_MP0_27,
        Resources.F5WebContent.F5WebCODE_MP0_28,
        Resources.F5WebContent.F5WebCODE_MP0_29,
        Resources.F5WebContent.F5WebCODE_MP0_30,
        Resources.F5WebContent.F5WebCODE_MP0_31,
        Resources.F5WebContent.F5WebCODE_MP0_32,
        Resources.F5WebContent.F5WebCODE_MP0_33,
        Resources.F5WebContent.F5WebCODE_MP0_34,
        Resources.F5WebContent.F5WebCODE_MP0_35
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            IF5LTMDALService dalService = new F5LTMDalService(swisProxy);
            bool isF5PollerEnabled = dalService.IsF5PollerEnabled(Node.NodeID);
            if (!isF5PollerEnabled)
            {
                Visible = false;
                return;
            }

            var ltmPools = dalService.GetPools(Node.NodeID);
            if (ltmPools == null || !ltmPools.Any())
            {
                Visible = false;
                return;
            }

            List.DataSource = ltmPools;
            List.DataBind();
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_MP0_16; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public string GetEnabledState(object index)
    {
        return GetState(enabledStates, index);
    }

    public string GetAvailabilityState(object index)
    {
        if (index == null || index is DBNull)
        {
            return String.Empty;
        }

        int i = Convert.ToInt32(index);
        return String.Format("<img src=\"{0}\" title=\"{1}\" />", 
            StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.Pool", i, StatusIconSize.Small),
            StatusInfo.GetStatus(i).ShortDescription);
    }

    public string GetMode(object index)
    {
        return GetState(modes, index);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceF5ListOfPools"; }
    }
}