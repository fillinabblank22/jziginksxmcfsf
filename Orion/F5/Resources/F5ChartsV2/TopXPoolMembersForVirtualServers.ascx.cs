﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Resources_F5ChartsV2_TopXPoolMembersForVirtualServers : StandardChartResource, IResourceIsInternal
{
    const string topXXProperty = "numberofseriestoshow";

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }
    
    public override string SubTitle
    {
        get
        {
            return string.IsNullOrEmpty(base.SubTitle) 
                ? string.Format(Resources.F5WebContent.F5WebCODE_AO0_8, GetIntProperty(topXXProperty, 5).ToString(CultureInfo.InvariantCulture)) 
                : base.SubTitle;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_AO0_00; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IF5VirtualServerProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/F5/Controls/EditResourceControls/EditTopXXVirtualServers.ascx"; }

    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var virtualServerProvider = GetInterfaceInstance<IF5VirtualServerProvider>();
        if (virtualServerProvider != null)
        {
            yield return virtualServerProvider.F5VirtualServer.Id.ToString(CultureInfo.InvariantCulture);
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "F5VS"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
    	
	public bool IsInternal
    {
        get { return true; }
    }
}