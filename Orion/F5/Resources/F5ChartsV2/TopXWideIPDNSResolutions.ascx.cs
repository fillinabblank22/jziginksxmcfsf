﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_F5_Resources_F5ChartsV2_TopXWideIPDNSResolutions : StandardChartResource, IResourceIsInternal
{

    const string topXXProperty = "numberofseriestoshow";

   protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

   public override string SubTitle
   {
       get
       {
           if (string.IsNullOrEmpty(base.SubTitle))
           {
               return string.Format(Resources.F5WebContent.F5WebCODE_JF0_011, GetIntProperty(topXXProperty, 5).ToString(CultureInfo.InvariantCulture));
           }

           return base.SubTitle;
       }
   }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_JF0_010; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IF5GTMProvider) }; }
    }
    
    public override string EditControlLocation
    {
        get { return "/Orion/F5/Controls/EditResourceControls/EditTopXXVirtualServers.ascx"; }
        
    }
   
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var gtmProvider = GetInterfaceInstance<IF5GTMProvider>();
        if (gtmProvider != null)
    	{
            yield return gtmProvider.F5Gtm.Id.ToString(CultureInfo.InvariantCulture);
    	}
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "F5GTM"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
	
	public bool IsInternal
    {
        get { return true; }
    }
}