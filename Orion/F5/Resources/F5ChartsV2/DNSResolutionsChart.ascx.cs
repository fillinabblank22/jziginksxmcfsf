﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_F5_Resources_F5ChartsV2_DNSResolutionsChart : StandardChartResource, IResourceIsInternal
{
    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_JF0_012; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IF5GTMWideIPProvider) }; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var serviceProvider = GetInterfaceInstance<IF5GTMWideIPProvider>();
        if (serviceProvider != null)
        {
            yield return serviceProvider.F5GtmWideIP.Id.ToString(CultureInfo.InvariantCulture);
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "F5WIP"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}
