﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.DAL;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.Shared;

public partial class Orion_F5_Resources_F5ChartsV2_ListOfNodes : SolarWinds.F5.Web.Resources.F5EntityListBase
{
    private readonly string[] enabledStates =
    {
        Resources.F5WebContent.F5WebCODE_MP0_0,
        Resources.F5WebContent.F5WebCODE_MP0_6,
        Resources.F5WebContent.F5WebCODE_MP0_7,
        Resources.F5WebContent.F5WebCODE_MP0_8
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            IF5LTMDALService dalService = new F5LTMDalService(swisProxy);
            bool isF5PollerEnabled = dalService.IsF5PollerEnabled(Node.NodeID);
            if (!isF5PollerEnabled)
            {
                this.Visible = false;
                return;
            }
        }

        var table = LoadData(Node.NodeID);

        if (table.Rows.Count == 0)
        {
            this.Visible = false;
            return;
        }

        List.DataSource = table;
        List.DataBind();
    }

    private DataTable LoadData(int nodeID)
    {
        var swqlParams = new Dictionary<string, object> { { "NodeID", nodeID } };

        using (var swis = InformationServiceProxy.CreateV3())
        {

            return swis.Query(@"
                SELECT
                    LTMNode.ShortName,
                    LTMNode.Name,
                    LTMNode.IPAddress,
                    LTMNode.OrionStatus,
                    LTMNode.Enabled,
                    LTMNode.F5StatusReason,
                    LTMNode.DetailsUrl
                FROM Orion.F5.LTM.Server (nolock=true) AS LTMNode 
                WHERE LTMNode.NodeID = @NodeID
                ORDER BY ShortName ASC",
            swqlParams);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_MP0_17; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public string GetEnabledState(object index)
    {
        return GetState(enabledStates, index);
    }

    public string GetAvailabilityState(object index)
    {
        if (index == null || index is DBNull)
        {
            return String.Empty;
        }

        int i = Convert.ToInt32(index);
        return String.Format("<img src=\"{0}\" title=\"{1}\" />", 
            StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.Server", i, StatusIconSize.Small), 
            StatusInfo.GetStatus(i).ShortDescription);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceF5ListOfNodes"; }
    }
}