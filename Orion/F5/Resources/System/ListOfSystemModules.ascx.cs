﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.DAL;

public partial class Orion_F5_Resources_ListOfSystemModules : BaseResourceControl, INodeProvider
{
    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPollerEnabled())
        {
            Visible = false;
            return;
        }

        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = string.Format(SWQL, Node.NodeID);
        CustomTable.OrderBy = DefaultOrderBy;

    }

    public string SWQL
    {
        get
        {
            return @"SELECT m.Name FROM Orion.F5.System.Module m WHERE m.Enabled = 1 AND m.NodeID = {0}";
        }
    }

    public string DefaultOrderBy
    {
        get
        {
            return "Name Asc";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            yield return typeof(INodeProvider);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    public Node Node
    {
        get
        {
            var provider = this.GetInterfaceInstance<INodeProvider>();

            if (provider != null)
            {
                return provider.Node;
            }

            return null;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_LH0_2; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceF5SystemModules";
        }
    }

    private bool IsPollerEnabled()
    {
        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            IF5LTMDALService dalService = new F5LTMDalService(swisProxy);
            bool isF5PollerEnabled = dalService.IsF5PollerEnabled(Node.NodeID);
            return isF5PollerEnabled;
        }
    }

}