﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_F5_Resources_System_HighAvailability : BaseResourceControl
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }
    /// <summary>
    /// Query checks if node is part of failover (being in group of more than 1 member), whether ltm and/or gtm module is on
    /// </summary>
    private const string IsFailoverQuery = @"
SELECT d.IsRedundant FROM Orion.F5.System.Device d
WHERE d.Node.NodeID = @NodeID
";

    protected void Page_Load(object sender, EventArgs e)
    {

        var provider = this.GetInterfaceInstance<INodeProvider>();

        if (provider == null || provider.Node == null)
        {
            // netobject was not provided at all - hide resource
            this.Visible = false;
            return;
        }

        int nodeId = provider.Node.NodeID;

        bool isFailover = false;

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string query = IsFailoverQuery;
            var result = proxy.Query(query, new Dictionary<string, object>() { { "NodeID", nodeId } });

            if (result != null && result.Rows.Count > 0)
            {
                isFailover = Convert.ToBoolean(result.Rows[0][0]);
            }
        }

        if (!isFailover)
        {
            this.Visible = false;
            return;
        }

        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL.Replace("@NodeID", nodeId.ToString(CultureInfo.InvariantCulture));
        CustomTable.OrderBy = DefaultOrderBy;
    }

    public string SWQL
    {
        get
        {
            return @"
SELECT
  fo.DeviceShortName,
  ip.IPAddress,
  fo.FailoverStatusDescription,
  d.SyncStatusDescription,
  ISNULL(ip.Status, 0) as _OrionStatus,
  ip.StatusDescription as _OrionStatusDescription,
  ip.DetailsUrl as _DetailsUrl,
  ip.NodeID as _NodeID
FROM Orion.F5.System.Failover fo
INNER JOIN Orion.Nodes n ON fo.NodeID=n.NodeID
LEFT JOIN Orion.Nodes ip ON fo.DeviceShortName=ip.SysName
LEFT JOIN Orion.F5.System.Device d ON d.NodeID = ip.NodeID
WHERE fo.NodeID=@NodeID AND fo.DeviceShortName!=n.SysName";
        }
    }

    public string DefaultOrderBy
    {
        get
        {
            return "DeviceShortName ASC";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            yield return typeof(INodeProvider);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceHALTM";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_LH0_24; }
    }
}