﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfSystemModules.ascx.cs" Inherits="Orion_F5_Resources_ListOfSystemModules" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <!-- Include CustomQueryTable control in your resource -->
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
 
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        // Which page should resource display when loaded
                        initialPage: 0,
                        // How many rows there can be on one page. If there are more rows, paging toolbar is displayed.
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        // True to allow sorting of rows by clicking on column headers
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: '<%# Resources.F5WebContent.F5WEBDATA_LH0_1 %>',
                            }
                        }
                    });
 
                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });    
        </script>
    </Content>
</orion:resourceWrapper>