﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HighAvailability.ascx.cs" Inherits="Orion_F5_Resources_System_HighAvailability" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:Include ID="Include1" runat="server" Module="F5" File="DetailsTable.css" Section="Top" />
<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {				
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        // Which page should resource display when loaded
                        initialPage: 0,
                        allowPaging: false,
                        rowsPerPage: 100,
                        allowSort: true,
                        columnSettings: {
                            "DeviceShortName": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_19 %>",
                                formatter: function(value, row, cellInfo)
                                {
                                    var status = row[4];
                                    var statusDescription = row[5];
                                    var url = row[6];
                                    var nodeId = row[7];

                                    if(statusDescription == null || statusDescription == ''){
                                        statusDescription = "<%=Resources.F5WebContent.F5WEBDATA_LH0_23 %>";
                                    }

                                    if(url == null || url == ''){
                                        return "<span class='nowrap'><img class='icon' title='"+statusDescription+"' alt='"+statusDescription+"' src='/Orion/StatusIcon.ashx?id="+nodeId+"&entity=Orion.Nodes&status="+status+"&size=small'/><span class='name'>" + value + "</span></span>";
                                    } else{
                                        return "<a class='nowrap tooltip' href='"+url+"'><img class='icon' title='"+statusDescription+"' alt='"+statusDescription+"' src='/Orion/StatusIcon.ashx?id="+nodeId+"&entity=Orion.Nodes&status="+status+"&size=small'/><span class='name'>" + value + "</span></a>";
                                    }

                                    
                                },
                                isHtml: true
                            },
                            "IPAddress": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_20 %>",
                                formatter: function(value, row, cellInfo)
                                {
                                    if(value == null || value == ''){
                                        return "<%=Resources.F5WebContent.F5WEBDATA_LH0_23 %>";
                                    }
                                    else{
                                        return value;
                                    }
                                },
                            },
                            "FailoverStatusDescription": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_21 %>",
                            },
                            "SyncStatusDescription": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_22 %>",
                                formatter: function(value, row, cellInfo)
                                {
                                    if(value == null || value == ''){
                                        return "<%=Resources.F5WebContent.F5WEBDATA_LH0_23 %>";
                                    }
                                    else{
                                        return value;
                                    }
                                },
                            }
                        }
                    });

                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
