﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="F5LTMDetails.ascx.cs" Inherits="Orion_F5_Resources_LTM_F5LTMDetails" %>
<%@ Register Src="~/Orion/F5/Controls/DetailsTable.ascx" TagPrefix="orion" TagName="GenericTable" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
  <Content>    
    <orion:GenericTable runat="server" ID="GenericTable" />
 </Content>
</orion:ResourceWrapper>