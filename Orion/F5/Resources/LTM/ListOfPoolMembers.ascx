<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfPoolMembers.ascx.cs" Inherits="Orion_F5_Resources_LTM_ListOfPoolMembers" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register Src="~/Orion/F5/Controls/SearchBox.ascx" TagPrefix="f5" TagName="searchbox" %>
<%@ Register Src="~/Orion/F5/Controls/ChangeRotationPresence.ascx" TagPrefix="f5" TagName="ChangeRotationPresence" %>


<orion:Include ID="Include1" runat="server" File="F5/Styles/TabularResource.css" />

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="ltm-pool-member-list f5-tabular-resource">
    <Content>
        <f5:searchbox runat="server" ID="SearchControl" Placeholder="<%# Resources.F5WebContent.F5WEBDATA_JP0_20%>" />
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <f5:ChangeRotationPresence runat="server" id="ChangeRotationPresence" Visible="False" />

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        searchTextBoxId: '<%= SearchControl.InputClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: true,
                        columnSettings: {
                            "memberName": {
                                header: '<%=MemberNameHeader %>',
                                cellCssClassProvider: function() {
                                    return 'f5-field-entity';
                                }
                            },
                            "Connections": {
                                header: '<%= ConnectionsHeader%>',								
                                cellCssClassProvider: function() {
                                    return 'align-right';
                                },
                                headerCssClass: 'align-right'
                            }
                        }
                    });

                SW.F5.SearchBox.createInstance($("#<%= SearchControl.ClientID%>"));
                // CQT checks only src attribute, so we need to set the initial value
                $("#<%= SearchControl.SearchButtonClientID%>").attr('src', '/Orion/images/Button.SearchIcon.gif');
 
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();

                SW.F5.ChangeRotationPresence.init(<%=Resource.ID%>, {
                    dialogSettings: {
                        title: '<%=Resources.F5WebContent.F5WEDATA_EK0_19%>'
                    },
                    columnSettings: {
                        'Member': {
                            headerFormatter: function() {
                                return '<%=MemberNameHeader %>';
                            }
                        },
                        'DisableReason': {
                            headerFormatter: function() {
                                return '';
                            },
                            cellFormatter: function(cell, row) {
                                if (cell != null && cell !== '') {
                                    var img = '<img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" title="' + cell + '" />';
                                    return img;
                                } else {
                                    return '';
                                }
                            },
                            cellCssClassProvider: function(cell, row) {
                                return 'shrink';
                            }
                        },
                        'Enabled': {
                            headerFormatter: function() {
                                return '';
                            },
                            cellFormatter: function(cell, row) {
                                var cssClass = '';
                                switch (cell) {
                                    case 1: 
                                        cssClass = 'on'; 
                                        break;
                                    case 2:
                                        cssClass = 'off'; 
                                        break;
                                    case 0:
                                    case 3:
                                        cssClass = 'off disabled';
                                        break;
                                }
                                
                                return '<div title="' + row[4] + '" class="change-rotation-presence-status ' + cssClass + '">';
                            },
                            cellCssClassProvider: function(cell, row) {
                                return 'shrink';
                            }
                        }
                    },
                    paginationDisplayingTextFormat: '<%=Resources.F5WebContent.F5WEDATA_EK0_20%>'
                });
            });    
        </script>

    </Content>
</orion:resourceWrapper>
