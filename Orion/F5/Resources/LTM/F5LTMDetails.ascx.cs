﻿using System;
using System.Collections.Generic;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Resources_LTM_F5LTMDetails : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var ltm = GetInterfaceInstance<IF5LTMProvider>();
        if (ltm == null)
        {
            return;
        }

        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5LTM";
        GenericTable.Action = "GetLTMDetails";
        GenericTable.ElementId = ltm.F5Ltm.Id.ToString();
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_AO0_00; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IF5LTMProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourceltmdetails";
        }
    }

}