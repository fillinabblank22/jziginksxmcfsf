﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="F5PoolMemberMonitors.ascx.cs" Inherits="Orion_F5_Resources_LTM_F5PoolMemberMonitors" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="f5" TagName="F5ApiConfigCheck" Src="~/Orion/F5/Controls/F5ApiConfigCheck.ascx" %>

<orion:Include ID="Include2" runat="server" File="F5/Styles/HealthMonitors.css" /> 

<orion:resourceWrapper ID="ResourceWrapper" runat="server" CssClass="ltm-health-monitor-list">
    <Content>
        <f5:F5ApiConfigCheck runat="server" ID="F5ApiConfigCheck" />

        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            <% if (CustomTable.Visible) { %>
            $(function () {			
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowPaging: true,
                        allowSort: true,
                        columnSettings: {
                            "ShortName": {
                                header: "<%=Resources.F5WebContent.F5WEDATA_ZB0_1 %>",
                                formatter: function(value, row, cellInfo) {
                                    var orionStatus = row[4];
                                    var orionStatusName = row[5];
                                    var statusIcon = String.format("<img src='/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.Monitor&status={0}&size=small' title='{1}' />", orionStatus, orionStatusName);
                                    return "<span class=\"field-monitorStatus\">" + statusIcon + "</span> <span class=\"field-monitorName\">" + value + "</span>";
                                },
                                isHtml: true
                            },
                            "Type": {
                                header: "<%=Resources.F5WebContent.F5WEDATA_ZB0_2 %>"
                            },
                            "Port": {
                                header: "<%=Resources.F5WebContent.F5WebCODE_JF0_003 %>"
                            },
                            "F5StatusReason": {
                                header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_10 %>"
                            }
                        }
                    });

                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });
            <% } %>
        </script>
    </Content>
</orion:resourceWrapper>
