﻿using System;
using System.Collections.Generic;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Resources_LTM_F5LTMPoolDetails : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var pool = GetInterfaceInstance<IF5LTMPoolProvider>();
        if (pool == null)
        {
            return;
        }
        
        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5LTM";
        GenericTable.Action = "GetLTMPoolDetails";
        GenericTable.ElementId = pool.Pool.Id.ToString();

    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_PS0_5; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new [] { typeof(IF5LTMPoolProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceF5PoolDetails"; }
    }
}