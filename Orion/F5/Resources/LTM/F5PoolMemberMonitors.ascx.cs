﻿using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;

public partial class Orion_F5_Resources_LTM_F5PoolMemberMonitors : BaseResourceControl
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        CustomTable.Visible = !F5ApiConfigCheck.Visible;
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(IF5LTMPoolMemberProvider); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var provider = this.GetInterfaceInstance<IF5LTMPoolMemberProvider>();

        if (provider == null || provider.PoolMember == null)
        {
            // no provider or missing netobject
            this.Visible = false;
            return;
        }

        F5ApiConfigCheck.NodeId = provider.PoolMember.NodeId;
       
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL.Replace("@EntityID", provider.PoolMember.Id.ToString(CultureInfo.InvariantCulture));
        CustomTable.OrderBy = DefaultOrderBy;
    }

    public string SWQL
    {
        get
        {
            return @"
SELECT 
    m.ShortName, 
    m.Type, 
    m.Port, 
    m.F5StatusReason,
    m.OrionStatus as _OrionStatus,
	m.OrionStatusDescription as _OrionStatusName
FROM Orion.F5.LTM.Monitor m
WHERE m.F5Server.PoolMembers.MemberID = @EntityID AND (m.F5Server.PoolMembers.Port = m.Port OR m.Port = 0)";
        }
    }

    public string DefaultOrderBy
    {
        get { return "ShortName ASC"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionphresourcehealthmonitors"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_JT0_15; }
    }
}