﻿using System;
using System.Collections.Generic;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Resources_LTM_F5PoolMemberDetails : BaseResourceControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        var provider = GetInterfaceInstance<IF5LTMPoolMemberProvider>();
        if (provider == null)
        {
            Visible = false;
            return;
        }

        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5LTM";
        GenericTable.Action = "GetLTMPoolMemberDetails";
        GenericTable.ElementId = provider.PoolMember.Id.ToString();

    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_LH0_41; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IF5LTMPoolMemberProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceF5PoolMemberDetails"; }
    }

}