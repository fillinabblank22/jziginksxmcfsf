﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="F5ServerDetails.ascx.cs" Inherits="Orion_F5_Resources_LTM_F5ServerDetails" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register Src="~/Orion/F5/Controls/DetailsTable.ascx" TagPrefix="orion" TagName="GenericTable" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>
<%@ Register TagPrefix="f5" TagName="ChangeRotationPresence" Src="~/Orion/F5/Controls/ChangeRotationPresence.ascx" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<orion:include id="Include" runat="server" file="F5/Styles/TabularResource.css" />
<orion:include id="Include1" runat="server" file="F5/Styles/ServerDetails.css" />
<orion:include id="Include2" runat="server" file="F5/js/LinkServerWithNode.js" />
<orion:IncludeExtJs ID="ctrExtJs" Version="4.2" Debug="false" runat="server"/>

<orion:resourcewrapper id="Wrapper" runat="server" cssclass="f5-tabular-resource f5-serverDetails">
    <Content>
		<div class="netobjectPickerWrapper" style="display:none" id="NetobjectPickerWrapper" runat="server">
            <orion:NetObjectPicker runat="server" id="nop"/>

            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="DialogLinkButton" CssClass="linkButton" LocalizedText="CustomText" Text="<%$ Resources : F5WebContent, F5WEBDATA_LH0_42 %>" DisplayType="Primary" CausesValidation="false" OnClientClick="return false;" />
                <orion:LocalizableButton runat="server" ID="DialogAddAndLinkButton" LocalizedText="CustomText" Text="<%$ Resources : F5WebContent, F5WEBDATA_MK0_00 %>" DisplayType="Secondary" CausesValidation="false" OnClick="DialogAddAndLinkButton_OnClick" />
                <orion:LocalizableButton runat="server" ID="DialogCancelButton" CssClass="cancelButton" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return false;" />
            </div>
        </div>

        <style>
            .netobjectPickerWrapper .GroupSection {
                padding: 0;
            }
            .sw-report .f5ServerManageButton{
                display: none;
            }

            .nodelink{
                margin-right: 20px;
            }

            .f5ServerManageButton{
                white-space: nowrap;
            }

            .f5ServerManageButton img{
                position: relative;
                top: -1px;
                vertical-align: middle;
            }
        </style>

        <orion:GenericTable runat="server" ID="GenericTable" />
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <f5:ChangeRotationPresence runat="server" id="ChangeRotationPresence" Visible="False" />

        <script type="text/javascript">
            $(function () {
                var escape = Ext42.util.Format.htmlEncode;

                SW.F5.LinkServerWithNode.init(
                    {
                        resourceId: <%=ScriptFriendlyResourceID  %>,
                        serverId: <%=F5ServerId  %>,
                        dialogElementClassName: "netobjectPickerWrapper",
                        isDemo: <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? 1 : 0%>,
                        errorMessage: "<%=Resources.F5WebContent.WEBJS_LH0_02 %>",
                        demoMessage: "<%=Resources.F5WebContent.WEBJS_LH0_03 %>"
                        });

                // netobject picker has troubles with isolation thus removing other instances (added by other instances of this resource) to make it work correctly
                $(".netobjectPickerWrapper:not(:first)").remove();

                SW.Core.Resources.CustomQuery.initialize({
                    uniqueId: <%= CustomTable.UniqueClientID %>,
                    initialPage: 0,
                    rowsPerPage: 100,
                    allowPaging: false,
                    allowSort: true,
                    columnSettings: {
                        "MemberName": {
                            header: "<%=Resources.F5WebContent.F5WEBDATA_JP0_16 %>",
                            headerCssClass: 'f5-serverDetails-poolMember',
                            cellCssClassProvider: function() {
                                return 'f5-field-entity f5-field-entity-truncate';
                            },
                            formatter: escape,
                            isHtml: true
                        },
                        "F5StatusReason": {
                            header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_5 %>",
                            formatter: escape
                        }
                    }
                });

                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();

                SW.F5.ChangeRotationPresence.init(<%=Resource.ID%>, {
                    dialogSettings: {
                        title: '<%=Resources.F5WebContent.F5WEDATA_EK0_19%>'
                    },
                    columnSettings: {
                        'Member': {
                            headerFormatter: function() {
                                return '<%= Resources.F5WebContent.F5WEBDATA_LH0_4 %>';
                            },
                            cellFormatter: escape
                        },
                        'Pool': {
                            headerFormatter: function() {
                                return '<%= Resources.F5WebContent.F5WebCODE_JF0_004 %>';
                            },
                            cellFormatter: escape
                        },
                        'DisableReason': {
                            headerFormatter: function() {
                                return '';
                            },
                            cellFormatter: function(cell, row) {
                                if (cell != null && cell !== '') {
                                    var img = '<img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" title="' + escape(cell) + '" />';
                                    return img;
                                } else {
                                    return '';
                                }
                            },
                            cellCssClassProvider: function(cell, row) {
                                return 'shrink';
                            }
                        },
                        'Enabled': {
                            headerFormatter: function() {
                                return '';
                            },
                            cellFormatter: function(cell, row) {
                                var cssClass = '';
                                switch (cell) {
                                    case 1:
                                        cssClass = 'on';
                                        break;
                                    case 2:
                                        cssClass = 'off';
                                        break;
                                    case 0:
                                    case 3:
                                        cssClass = 'off disabled';
                                        break;
                                }

                                return '<div title="' + escape(row[6]) + '" class="change-rotation-presence-status ' + cssClass + '">';
                            },
                            cellCssClassProvider: function(cell, row) {
                                return 'shrink';
                            }
                        }
                    },
                    paginationDisplayingTextFormat: '<%=Resources.F5WebContent.F5WEDATA_EK0_20%>'
                });
            });
        </script>
    </Content>
</orion:resourcewrapper>
