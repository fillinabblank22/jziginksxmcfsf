﻿using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;

public partial class Orion_F5_Resources_LTM_F5ServerMonitors : BaseResourceControl
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(IF5ServerProvider); }
    }

    protected override void OnPreRender(EventArgs e)
    {        
        base.OnPreRender(e);
        CustomTable.Visible = !F5ApiConfigCheck.Visible;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var provider = this.GetInterfaceInstance<IF5ServerProvider>();

        if (provider == null || provider.F5Server == null)
        {
            // no provider or missing netobject
            this.Visible = false;
            return;
        }

        F5ApiConfigCheck.NodeId = provider.F5Server.NodeId;
       
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL.Replace("@F5ServerID", provider.F5Server.Id.ToString(CultureInfo.InvariantCulture));
        CustomTable.OrderBy = DefaultOrderBy;
    }

    public string SWQL
    {
        get
        {
            return @"
SELECT 
    m.ShortName, 
    m.Type, 
    m.Port, 
    m.F5StatusReason,
    m.OrionStatus as _OrionStatus,
	m.OrionStatusDescription as _OrionStatusName
FROM Orion.F5.LTM.Monitor m
WHERE m.F5Server.F5ServerID = @F5ServerID";
        }
    }

    public string DefaultOrderBy
    {
        get { return "ShortName ASC"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionphresourcehealthmonitors"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEDATA_ZB0_3; }
    }
}