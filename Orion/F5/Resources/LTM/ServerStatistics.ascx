﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServerStatistics.ascx.cs" Inherits="Orion_F5_Resources_LTM_ServerStatisitics" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<orion:Include ID="Include" runat="server" File="F5/Styles/TabularResource.css" />
<orion:Include ID="Include1" runat="server" File="F5/Styles/ServerStatistics.css" />
<orion:Include ID="Include2" runat="server" File="F5/js/Formatters.js" />
<orion:IncludeExtJs ID="ctrExtJs" Version="4.2" Debug="false" runat="server"/>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" CssClass="f5-tabular-resource f5-ltm-serverStatistics">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {
                var escape = Ext42.util.Format.htmlEncode;

                var formatBps = function(value) {
                    var escapedValue = escape(value);
                    return SW.F5.Formatters.formatWithUnit(escapedValue, 1, "bps");
                }

                SW.Core.Resources.CustomQuery.initialize({
                    uniqueId: <%= CustomTable.UniqueClientID %>,
                    initialPage: 0,
					rowsPerPage: 100,
                    allowPaging: false,
                    allowSort: true,
                    columnSettings: {
                        "Pool": {
                            header: "<%=Resources.F5WebContent.F5WEBDATA_LH0_8 %>",
                            headerCssClass: "f5-ltm-serverStatistics-pool",
                            cellCssClassProvider: function() {
                                return "f5-field-entity f5-field-entity-truncate";
                            },
                            formatter: escape,
                            isHtml: true
                        },
                        "PoolMember": {
                            header: "<%=Resources.F5WebContent.F5WEBDATA_JP0_16 %>",
                            headerCssClass: "f5-ltm-serverStatistics-poolMember",
                            cellCssClassProvider: function() {
                                return "f5-field-entity f5-field-entity-truncate";
                            },
                            formatter: escape,
                            isHtml: true
                        },
                        "Port": {
                            header: "<%=Resources.F5WebContent.F5WebCODE_JF0_003 %>",
                            headerCssClass: "f5-ltm-serverStatistics-port",
                            formatter: function(value) {
                                return ":" + escape(value);
                            }
                        },
                        "Connections": {
                            header: "<%=Resources.F5WebContent.F5WebCODE_JF0_005 %>",
                            headerCssClass: "align-right",
                            cellCssClassProvider: function() {
                                return "align-right";
                            },
                            formatter: escape
                        },
                        "RequestsPerSec": {
                            header: "<%=Resources.F5WebContent.F5WebCODE_JF0_006 %>",
                            headerCssClass: "align-right",
                            cellCssClassProvider: function() {
                                return "align-right";
                            },
                            formatter: escape
                        },
                        "ThroughputInPerSec": {
                            header: "<%=Resources.F5WebContent.F5WEBDATA_JP0_24 %>",
                            headerCssClass: "align-right",
                            formatter: formatBps,
                            cellCssClassProvider: function() {
                                return "align-right";
                            },
                            isHtml: true
                        },
                        "ThroughputOutPerSec": {
                            header: "<%=Resources.F5WebContent.F5WEBDATA_JP0_25 %>",
                            headerCssClass: "align-right",
                            formatter: formatBps,
                            cellCssClassProvider: function() {
                                return "align-right";
                            },
                            isHtml: true
                        }
                    }
                });

                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, "<%= CustomTable.ClientID %>");
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>
