﻿using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

public partial class Orion_F5_Resources_LTM_F5LTMVirtualServerDetails : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var vs = GetInterfaceInstance<IF5VirtualServerProvider>();
        if (vs == null)
        {
            return;
        }

        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5LTM";
        GenericTable.Action = "GetVirtualServerDetails";
        GenericTable.ElementId = vs.F5VirtualServer.Id.ToString();
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_LH0_18; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IF5VirtualServerProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcef5virtualserverdetails";
        }
    }
}