﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LTMListOfVirtualServers.ascx.cs" Inherits="Orion_F5_Resources_LTM_LTMListOfVirtualServers" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<orion:Include ID="Include1" runat="server" File="F5/Styles/DetailsTable.css" /> 

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" class="NeedsZebraStripes"/>
        
          <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize({
                    uniqueId: <%= CustomTable.UniqueClientID %>,
                    initialPage: 0,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                    allowSort: true,
                    columnSettings: {
                        "virtualServerName": {
                            header: '<%=Resources.F5WebContent.F5WebCODE_JF0_002 %>',
                            formatter: function(value, row) {
								var shortName = row[1];
                                var orionStatus = row[2];
                                    
                                var statusIcon = String.format("<img src='/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.VirtualServer&status={0}&size=small' />", orionStatus);                                   
                                return 	"<span class=\"field-vserverStatus\" title=\""+ value +"\">" + statusIcon + "</span> "+
										"<span class=\"field-vserverName\" \" title=\""+ value +"\">" + shortName + "</span>";
                            },
                            isHtml: true
                        },
                        "Port": {
                            header: '<%= Resources.F5WebContent.F5WebCODE_MP0_11%>',																
                            formatter: function(value) { 
								return value ? (":" + value) : "";
                            }
                        },
                        "PoolName": {
                            header: '<%=Resources.F5WebContent.F5WebCODE_AO0_9 %>',
                            formatter: function(value, row) {

                                if (!value) return "";
                                var shortName = row[5];
                                var orionStatus = row[6];
                                    
                                var statusIcon = String.format("<img src='/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.Pool&status={0}&size=small' />", orionStatus);                                   
                                return 	"<span class=\"field-poolStatus\" title=\""+ value +"\">" + statusIcon + "</span> "+
										"<span class=\"field-poolName\" \" title=\""+ value +"\">" + shortName + "</span>";
                            },
                            isHtml: true
                        },
                        "BalancingAlgorithm":{
                            header: '<%=Resources.F5WebContent.F5WebCODE_AO0_10 %>',                               
                        },
                        "Connections": {
                            header: '<%=Resources.F5WebContent.F5WebCODE_AO0_4 %>',
                            cellCssClassProvider: function () {
                                return "align-right";
                            },
                            headerCssClass: 'align-right',
                        },

                    }
                });
 
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</orion:ResourceWrapper>

