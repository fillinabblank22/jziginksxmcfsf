﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_F5_Resources_LTM_ListOfPoolMembers : BaseResourceControl
{
    // Resources in reporting have negative ID, does not work in JavaScript
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsDataAvailable())
        {
            Visible = false;
            return;
        }

        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL_ByPool;
        CustomTable.OrderBy = DefaultOrderBy;
        CustomTable.SearchSWQL = SWQL_SearchByPool;

        InsertChangeRotationPresenceLink();
    }

    private bool IsDataAvailable()
    {
        if (!PoolId.HasValue)
            return false;

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var result = proxy.Query(Top1SWQL, new Dictionary<string, object>() { { "id", PoolId } });

            return result != null && result.Rows.Count > 0;
        }
    }

    private void InsertChangeRotationPresenceLink()
    {
        if (this.F5LtmPool == null)
        {
            this.Wrapper.ShowManageButton = false;
            this.ChangeRotationPresence.Visible = false;
            return;
        }

        this.Wrapper.ShowManageButton = true;
        this.Wrapper.ManageButtonText = Resources.F5WebContent.F5WEDATA_EK0_19;
        this.Wrapper.ManageButtonTarget = string.Format("javascript:SW.F5.ChangeRotationPresence.showDialog({0});", Resource.ID);

        this.ChangeRotationPresence.ResourceID = this.Resource.ID;
        this.ChangeRotationPresence.NodeId = this.F5LtmPool.NodeID;
        this.ChangeRotationPresence.Visible = true;
        this.ChangeRotationPresence.SWQL = string.Format(@"SELECT
                        pm.F5Server.ShortName as Member,
                        '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.PoolMember&size=small&status=' + ToString(pm.OrionStatus) as _IconFor_Member,
                        pm.DisableReason as DisableReason,
                        pm.Enabled as Enabled,
                        pm.EnabledDescription as _EnabledDescription,
                        pm.MemberID as _MemberID
                    FROM Orion.F5.LTM.PoolMember pm
                    WHERE pm.Pool.PoolID = {0}
                    ORDER BY pm.Enabled DESC, pm.F5Server.ShortName", F5LtmPool.Id);
    }

    #region swql
    private const string DefaultOrderBy = "pm.F5Server.Name Asc";
    private const string Top1SWQL = @"SELECT TOP 1 pm.OrionStatus FROM Orion.F5.LTM.PoolMember pm WHERE pm.Pool.PoolID = @id";

    private int? PoolId
    {
        get
        {
            if (F5VirtualServer != null)
                return F5VirtualServer.PoolId;
            if (F5LtmPool != null)
                return F5LtmPool.Id;
            return null;
        }
    }

    private string SWQL_ByPool
    {
        get
        {
            return (PoolId.HasValue) ? string.Format(@"SELECT
                        pm.F5Server.ShortName as memberName,
                        pm.Connections as Connections,
                        pm.DetailsUrl as _LinkFor_memberName,
                        '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.PoolMember&size=small&status=' + ToString(pm.OrionStatus) as _IconFor_memberName
                    FROM Orion.F5.LTM.PoolMember pm                    					
                    WHERE pm.Pool.PoolID = {0}", PoolId) : string.Empty;
        }
    }

    private string SWQL_SearchByPool
    {
        get
        {
            return (PoolId.HasValue) ? string.Format(@"SELECT
                        pm.F5Server.ShortName as memberName,
                        pm.Connections as Connections,
                        pm.DetailsUrl as _LinkFor_memberName,
                        '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.PoolMember&size=small&status=' + ToString(pm.OrionStatus) as _IconFor_memberName
                    FROM Orion.F5.LTM.PoolMember pm                    					
                    WHERE pm.Pool.PoolID = {0} and pm.F5Server.Name like '%${{SEARCH_STRING}}%'", PoolId) : string.Empty;
        }
    }
    #endregion

    public string MemberNameHeader
    {
        get { return Resources.F5WebContent.F5WEBDATA_LH0_4; }
    }

    public string ConnectionsHeader
    {
        get { return Resources.F5WebContent.F5WebCODE_JF0_005; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionphresourcef5poolmembers"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_AO0_5; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    private F5VirtualServer F5VirtualServer
    {
        get
        {
            var provider = this.GetInterfaceInstance<IF5VirtualServerProvider>();

            return provider != null ? provider.F5VirtualServer : null;
        }
    }

    private F5LTMPool F5LtmPool
    {
        get
        {
            var provider = this.GetInterfaceInstance<IF5LTMPoolProvider>();

            return provider != null ? provider.Pool : null;
        }
    }

    public override IEnumerable<string> RequiredDynamicInfoKeys
    {
        get { return new[] { "LtmPoolMembersViewLimitation" }; }
    }
}
