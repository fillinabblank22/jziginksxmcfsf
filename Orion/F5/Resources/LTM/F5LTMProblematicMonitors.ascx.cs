﻿using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_F5_Resources_LTM_F5LTMProblematicMonitors : BaseResourceControl
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            yield return typeof(INodeProvider);
            yield return typeof(IF5LTMProvider); 
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        CustomTable.Visible = !F5ApiConfigCheck.Visible;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var provider = this.GetInterfaceInstance<INodeProvider>();

        if (provider == null || provider.Node == null)
        {
            // no provider or missing netobject
            this.Visible = false;
            return;
        }        

        int nodeId = provider.Node.NodeID;

        F5ApiConfigCheck.NodeId = nodeId;

        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL.Replace("@NodeID", nodeId.ToString(CultureInfo.InvariantCulture));
        CustomTable.OrderBy = DefaultOrderBy;
    }

    public string SWQL
    {
        get
        {
            return @"
SELECT 
    m.ShortName as MonitorName,     
    m.F5Server.PoolMembers.ShortName as PoolMemberName,    
    m.F5Server.PoolMembers.Pool.ShortName as PoolName,
    m.Type,    
    m.F5Server.PoolMembers.Pool.VirtualServer.ShortName as VirtualServerName,
    m.F5StatusReason,
    m.OrionStatus as _MonitorStatus,
    m.OrionStatusDescription as _MonitorStatusName,    
    m.F5Server.PoolMembers.DetailsUrl as _LinkFor_PoolMemberName,
    m.F5Server.PoolMembers.OrionStatus as _PoolMemberStatus,
    m.F5Server.PoolMembers.OrionStatusDescription as _PoolMemberStatusName,
    m.F5Server.PoolMembers.Pool.DetailsUrl as _LinkFor_PoolName,    
    m.F5Server.PoolMembers.Pool.OrionStatus as _PoolStatus,
    m.F5Server.PoolMembers.Pool.OrionStatusDescription as _PoolStatusName,
    m.F5Server.PoolMembers.Pool.VirtualServer.DetailsUrl as _LinkFor_VirtualServerName,
    m.F5Server.PoolMembers.Pool.VirtualServer.OrionStatus as _VirtualServerStatus,
    m.F5Server.PoolMembers.Pool.VirtualServer.OrionStatusDescription as _VirtualServerStatusName
FROM Orion.F5.LTM.Monitor m
WHERE m.NodeId = @NodeID AND m.OrionStatus <> 1";
        }
    }

    public string DefaultOrderBy
    {
        get { return "_MonitorStatus DESC, MonitorName ASC"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionphresourceproblematicmonitors"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEDATA_JT0_1; }
    }
}