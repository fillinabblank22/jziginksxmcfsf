﻿using SolarWinds.F5.Web.Resources;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Net;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.Shared;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_F5_Resources_LTM_F5ServerDetails : F5EntityListBase, IF5ServerProvider
{
    private static readonly Log log = new Log();

    public string ServerStatus { get; set; }
    public bool IsInReport { get; set; }

    public string GetOrionStatusIcon(object index)
    {
        if (index == null || index is DBNull)
        {
            return String.Empty;
        }

        int i = Convert.ToInt32(index);
        return String.Format("<img src=\"{0}\" title=\"{1}\" />  {1}",
            StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.Server", i, StatusIconSize.Small),
            StatusInfo.GetStatus(i).ShortDescription);
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public int F5ServerId { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        IsInReport = Resource.IsInReport;

        if (IsInReport)
        {
            Wrapper.CssClass += " sw-report";
        }

        if (OrionConfiguration.IsDemoServer)
        {
            DialogAddAndLinkButton.OnClientClick = string.Format("alert('{0}'); return false;", Resources.F5WebContent.WEBJS_LH0_03);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (F5Server == null)
        {
            this.Visible = false;
            return;
        }

        ServerStatus = GetOrionStatusIcon(F5Server.OrionStatus);
        
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL.Replace("@ServerID", F5Server.Id.ToString());
        CustomTable.OrderBy = DefaultOrderBy;
        
        GenericTable.ID = Math.Abs(Resource.ID).ToString();
        GenericTable.Route = "/api/F5LTM";
        GenericTable.Action = "GetServerDetails";
        GenericTable.ElementId = F5Server.Id.ToString();

        F5ServerId = F5Server.Id;

        InsertChangeRotationPresenceLink();
    }


    private void InsertChangeRotationPresenceLink()
    {
        if (this.F5Server == null)
        {
            this.Wrapper.ShowManageButton = false;
            this.ChangeRotationPresence.Visible = false;
            return;
        }

        this.Wrapper.ShowManageButton = true;
        this.Wrapper.ManageButtonText = Resources.F5WebContent.F5WEDATA_EK0_19;
        this.Wrapper.ManageButtonTarget = string.Format("javascript:SW.F5.ChangeRotationPresence.showDialog({0});", Resource.ID);

        this.ChangeRotationPresence.ResourceID = this.Resource.ID;
        this.ChangeRotationPresence.NodeId = this.F5Server.NodeId;
        this.ChangeRotationPresence.Visible = true;
        this.ChangeRotationPresence.SWQL = string.Format(@"SELECT
                        pm.F5Server.ShortName as Member,
                        '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.PoolMember&size=small&status=' + ToString(pm.OrionStatus) as _IconFor_Member,
                        pm.Pool.ShortName as Pool,
                        '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.PoolMember&size=small&status=' + ToString(pm.OrionStatus) as _IconFor_Pool,
                        pm.DisableReason as DisableReason,
                        pm.Enabled as Enabled,
                        pm.EnabledDescription as _EnabledDescription,
                        pm.MemberID as _MemberID
                    FROM Orion.F5.LTM.PoolMember pm
                    WHERE pm.F5Server.F5ServerID = {0}
                    ORDER BY pm.Enabled DESC, pm.Pool.ShortName", F5Server.Id);
    }

    public string SWQL
    {
        get
        {
            return @"
                SELECT
                    pm.ShortName as MemberName,
                    pm.OrionStatus as _OrionStatus,
                    pm.F5StatusReason,
                    pm.DetailsUrl AS _LinkFor_MemberName,
                    '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.PoolMember&size=small&status=' + ToString(pm.OrionStatus) AS _IconFor_MemberName
                FROM Orion.F5.LTM.Server s
                JOIN Orion.F5.LTM.PoolMember pm ON s.F5ServerIndex = pm.F5ServerIndex AND s.NodeID = pm.NodeID
                JOIN Orion.Nodes n ON n.NodeID = s.NodeID
                WHERE s.F5ServerID = @ServerID";
        }
    }

    public string DefaultOrderBy
    {
        get
        {
            return "MemberName ASC";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            yield return typeof(IF5ServerProvider);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcef5serverdetails";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WEBDATA_LH0_9; }
    }

    public F5Server F5Server
    {
        get
        {
            var provider = this.GetInterfaceInstance<IF5ServerProvider>();

            if (provider != null)
            {
                return provider.F5Server;
            }

            return null;
        }
    }

    protected void DialogAddAndLinkButton_OnClick(object sender, EventArgs e)
    {
        // checks for demo mode and node management rights are also on client side, this is just safety check
        if ((this.F5Server == null) || OrionConfiguration.IsDemoServer || !Profile.AllowNodeManagement)
        {
            return;
        }

        string addNodeLink = "/Orion/Nodes/Add/Default.aspx";

        string ipAddress = IsValidIpAddress(this.F5Server.IpAddress)
            ? this.F5Server.IpAddress
            : (IsValidIpAddress(this.F5Server.ShortName) ? this.F5Server.ShortName : null);
        
        if (ipAddress != null)
        {
            addNodeLink = string.Format("{0}?IPAddress={1}", addNodeLink, ipAddress);
        }

        log.DebugFormat("Redirecting to Add node wizard to link to F5 Server (ID:{0}).", F5Server.Id);

        string returnUrl = string.Format("/Orion/F5/Admin/LinkF5ServerToOrionNode.aspx?F5ServerId={0}", this.F5Server.Id);

        AddNodeReturnPageHelper.Leave(returnUrl);

        Response.Redirect(addNodeLink);
    }

    private bool IsValidIpAddress(string ipAddressString)
    {
        if (string.IsNullOrWhiteSpace(ipAddressString))
            return false;

        bool result;

        try
        {
            IPAddress.Parse(ipAddressString);

            result = true;
        }
        catch (FormatException)
        {
            result = false;
        }

        return result;
    }
}
