﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.DAL;


public partial class Orion_F5_Resources_LTM_LTMListOfVirtualServers : BaseResourceControl
{
    protected Node Node
    {
        get { return GetInterfaceInstance<INodeProvider>().Node; }
    }

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        INodeProvider provider = this.GetInterfaceInstance<INodeProvider>();

        if(provider == null || provider.Node == null || !IsPollerEnabled() || !IsDataAvailable(provider.Node.NodeID))
        {
            Visible = false;
            return;
        }
        
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = string.Format(SWQL, Node.NodeID);
        CustomTable.OrderBy = DefaultOrderBy;
    }

    private bool IsDataAvailable(int nodeID)
    {
        bool isDataAvailable = false;

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string query = "SELECT TOP 1 VServer.NodeID FROM Orion.F5.LTM.VirtualServer (nolock=true) VServer WHERE VServer.F5Device.Node.NodeID = @NodeID";
            var result = proxy.Query(query, new Dictionary<string, object>() { { "NodeID", nodeID } });

            if (result != null && result.Rows.Count > 0)
            {
                isDataAvailable = true;
            }
        }

        return isDataAvailable;
    }

    private bool IsPollerEnabled()
    {
        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            IF5LTMDALService dalService = new F5LTMDalService(swisProxy);
            bool isF5PollerEnabled = dalService.IsF5PollerEnabled(Node.NodeID);
            return isF5PollerEnabled;
        }
    }

    public string DefaultOrderBy
    {
        get { return "VServer.ShortName ASC"; }
    }


    private string SWQL = @"
SELECT VServer.Name as virtualServerName, VServer.ShortName as _virtualServerNameShortName, VServer.OrionStatus as _vserverOrionStatus, Port, 
        VServer.Pool.Name as PoolName,VServer.Pool.ShortName as _PoolShortName, VServer.Pool.OrionStatus as _poolOrionStatus, VServer.Pool.LBModeDescription as BalancingAlgorithm, ROUND(VServer.Connections,0) AS Connections,
        VServer.DetailsUrl as _LinkFor_virtualServerName, VServer.Pool.DetailsUrl as _LinkFor_PoolName
FROM Orion.F5.LTM.VirtualServer (nolock=true) VServer
WHERE VServer.F5Device.Node.NodeID = {0}";


    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_AO0_11; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] {typeof (INodeProvider)}; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionphresourcevirtualservers"; }
    }
}