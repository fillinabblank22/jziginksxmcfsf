﻿using System;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.F5.Web.Resources;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;


public partial class Orion_F5_Resources_LTM_ServerStatisitics : F5EntityListBase, IF5ServerProvider
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public F5Server F5Server
    {
        get
        {
            var provider = this.GetInterfaceInstance<IF5ServerProvider>();

            if (provider != null)
            {
                return provider.F5Server;
            }

            return null;
        }
    }

    private const string SwqlAreDataAvailable = @"
        SELECT
            TOP 1 s.F5ServerID
        FROM
            Orion.F5.LTM.Server s
        WHERE
            s.F5ServerID = @serverId
            AND
            s.PoolMembers.Pool.PoolId IS NOT NULL";

    private const string SwqlGetData = @"
        SELECT
            s.PoolMembers.Pool.ShortName AS Pool,
            s.PoolMembers.ShortName AS PoolMember,
            s.PoolMembers.Port,
            ROUND(ISNULL(s.PoolMembers.Connections, 0), 0) AS Connections,
            ROUND(ISNULL(s.PoolMembers.RequestsPerSec, 0), 0) AS RequestsPerSec,
            ROUND(ISNULL(s.PoolMembers.BPS_In, 0), 1) AS ThroughputInPerSec,
            ROUND(ISNULL(s.PoolMembers.BPS_Out, 0), 1) AS ThroughputOutPerSec,
            s.PoolMembers.Pool.DetailsUrl AS _LinkFor_Pool,
            s.PoolMembers.DetailsUrl AS _LinkFor_PoolMember,
            '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.Pool&size=small&hint=LTM&status=' + ToString(s.PoolMembers.Pool.OrionStatus) AS _IconFor_Pool,
            '/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.PoolMember&size=small&hint=LTM&status=' + ToString(s.PoolMembers.OrionStatus) AS _IconFor_PoolMember
        FROM
            Orion.F5.LTM.Server s
        WHERE
            s.F5ServerID = {0}";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!AreDataAvailable())
        {
            Visible = false;
            return;
        }

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = string.Format(SwqlGetData, F5Server.Id);
        CustomTable.OrderBy = "Pool, PoolMember";
        
    }

    private bool AreDataAvailable()
    {
        if (F5Server == null)
        {
            return false;
        }

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var result = proxy.Query(SwqlAreDataAvailable, new Dictionary<string, object>
            {
                { "serverId", F5Server.Id }
            });

            return result != null && result.Rows.Count > 0;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.F5WebContent.F5WebCODE_JF0_001; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceF5ServerStatistics"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            yield return typeof(IF5ServerProvider);
        }
    }

    public string GetStatus(object index)
    {
        if (index == null || index is DBNull)
        {
            return String.Empty;
        }

        int i = Convert.ToInt32(index);
        return String.Format("<img src=\"{0}\" title=\"{1}\" />  ",
                                StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.PoolMember", i, StatusIconSize.Small),
                                StatusInfo.GetStatus(i).ShortDescription);
    }
}