﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.F5.Web.DAL.SummaryStatus;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.F5.Web.UI;

public partial class Orion_F5_Resources_LTM_SummaryStatus : SummaryStatusResourceControl
{
    private readonly ISummaryStatus dal = new F5LTMSummaryStatus();

    protected override string DefaultTitle
    {
        get { return F5WebContent.F5WEBDATA_JP0_4; }
    }

    public override String HelpLinkFragment
    {
        get { return "orionphltmsummary"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IF5LTMProvider) }; }
    }

    protected override ISummaryStatus Dal
    {
        get { return dal; }
    }

    protected override IEnumerable<string> EntityNames
    {
        get
        {
            yield return "Orion.F5.LTM.VirtualServer";
            yield return "Orion.F5.LTM.Pool";
            yield return "Orion.F5.LTM.PoolMember";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        statusComponent.NetObject = "F5LTM:" + NodeID;
        AddTileData(statusComponent.Tiles);
    }
}
