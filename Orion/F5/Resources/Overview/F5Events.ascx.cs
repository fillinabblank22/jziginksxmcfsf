﻿using SolarWinds.F5.Web.Helpers.SWQLBuilder;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.F5.Web.Models;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

public partial class Orion_F5_Resources_Overview_F5Events : BaseResourceControl
{

    public override IEnumerable<string> RequiredDynamicInfoKeys
    {
        get
        {
            // only to views providing this key is possible to add F5 events resource
            return new[] { "F5DetailsPage" };
        }
    }

    private const string propertyNetobjectTypes = "NetobjectTypes";
    private const string NetobjectTypesSeparator = ";";

    private readonly Dictionary<string, EventType> mapping = new Dictionary<string, EventType>()
            {
                { "F5LHM", EventType.HealthMonitor}, 
                { "F5", EventType.HighAvailability },
                { "F5P", EventType.Pool },
                { "F5PM", EventType.PoolMember},
                { "F5S", EventType.Server },
                { "F5WIP", EventType.Service },
                { "F5LVA", EventType.VirtualIPAddress },
                { "F5VS", EventType.VirtualServer },                
            };

    private readonly Dictionary<string, string> defaultNetobjectTypes = new Dictionary<string, string>()
    {
        { "F5WIP", "F5WIP" },
        { "F5P", "F5P;F5PM;F5S" },
        { "F5VS", "F5LVA;F5VS" },
        { "F5S", "F5P;F5PM;F5S;F5LHM" },
        { "F5GTM", "F5;F5WIP" },
        { "F5LTM", "F5;F5LVA;F5LHM" },
        { "F5PM", "F5PM;F5S;F5P;F5LHM" },
    };

    private readonly Dictionary<Type, Func<object, NetObjectDto>> netobjectSelector = new Dictionary<Type, Func<object, NetObjectDto>>()
        {
            {
                typeof(IF5GTMWideIPProvider),
                o => {
                    var providerInstance = o as IF5GTMWideIPProvider;
                    return (providerInstance != null && providerInstance.F5GtmWideIP != null)? new NetObjectDto("F5WIP", providerInstance.F5GtmWideIP.Id) : null;
                }
            },
            {
                typeof(IF5LTMPoolMemberProvider),
                o => {
                    var providerInstance = o as IF5LTMPoolMemberProvider;
                    return (providerInstance != null && providerInstance.PoolMember!= null)? new NetObjectDto("F5PM", providerInstance.PoolMember.Id) : null;
                }
            },
            {
                typeof(IF5LTMPoolProvider),
                o => {
                    var providerInstance = o as IF5LTMPoolProvider;
                    return (providerInstance != null && providerInstance.Pool!= null)? new NetObjectDto("F5P", providerInstance.Pool.Id) : null;
                }
            },
            {
                typeof(IF5VirtualServerProvider),
                o => {
                    var providerInstance = o as IF5VirtualServerProvider;
                    return (providerInstance != null && providerInstance.F5VirtualServer!= null)? new NetObjectDto("F5VS", providerInstance.F5VirtualServer.Id) : null;
                }
            },
            {
                typeof(IF5ServerProvider),
                o => {
                    var providerInstance = o as IF5ServerProvider;
                    return (providerInstance != null && providerInstance.F5Server!= null)? new NetObjectDto("F5S", providerInstance.F5Server.Id) : null;
                }
            },
            {
                typeof(IF5GTMProvider),
                o => {
                    var providerInstance = o as IF5GTMProvider;
                    return (providerInstance != null && providerInstance.F5Gtm!= null)? new NetObjectDto("F5GTM", providerInstance.F5Gtm.Id) : null;
                }
            },
            {
                typeof(IF5LTMProvider),
                o => {
                    var providerInstance = o as IF5LTMProvider;
                    return (providerInstance != null && providerInstance.F5Ltm!= null)? new NetObjectDto("F5LTM", providerInstance.F5Ltm.Id) : null;
                }
            }
        };

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return Enumerable.Empty<Type>();
        }
    }
    public int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.F5WebContent.F5WEBDATA_LH0_30;
        }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

            // if subtitle is not specified subtitle is a period
            string period = Resource.Properties["Period"];

            if (string.IsNullOrEmpty(period))
            {
                period = defaultTimePeriod;
            }

            return Periods.GetLocalizedPeriod(period);
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/F5/Controls/EditResourceControls/EditF5Events.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphresourcef5events";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    /// <summary>
    /// Lists of netobject types used to filter events.
    /// </summary>
    private IEnumerable<string> netObjectTypes;

    private IEnumerable<NetObjectDto> specificNetobjects;

    private const string defaultTimePeriod = "Last 12 Months";

    private const string defaultOrderBy = "EventTime DESC";

    private const string propertyRowsPerPage = "RowsPerPage";

    private const string defaultRowsPerPage = "10";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.OrderBy = defaultOrderBy;

        DateTime periodBegin = new DateTime();
        DateTime periodEnd = new DateTime();
        // get Period and configure when not set yet
        GetPeriod(ref periodBegin, ref periodEnd);

        // configure RowsPerPage when not set yet
        if (GetIntProperty(propertyRowsPerPage, 0) == 0)
        {
            Resource.Properties.Add(propertyRowsPerPage, defaultRowsPerPage);
        }


        var currentProviderType = GetCurrentProviderType();
        var netobject = GetCurrentNetobject(currentProviderType);

        if (netobject == null)
        {
            this.Visible = false;
            return;
        }

        var relatedNetobjects = GetEventTypes();

        if (relatedNetobjects == 0)
        {
            InsertDefaultResourceConfiguration(netobject);
            relatedNetobjects = GetEventTypes();
        }

        if (relatedNetobjects == 0)
        {
            // we need some dummy qyery in order to satisfy CQR
            CustomTable.SWQL = "select top 0 EventType, Message, EventTime from Orion.Events e";
            return;
        }

        string query = EventQueryBuilder.BuildCommand(netobject.NetObjectType, (EventType)relatedNetobjects);
        query = query.Replace("@ID", netobject.NetObjectID.ToString())
            .Replace("@fromDate", string.Format("'{0}'", periodBegin.ToUniversalTime().ToString(CultureInfo.InvariantCulture)))
            .Replace("@toDate", string.Format("'{0}'", periodEnd.ToUniversalTime().ToString(CultureInfo.InvariantCulture)));

        CustomTable.SWQL = query;
    }

    private void GetPeriod(ref DateTime periodBegin, ref DateTime periodEnd)
    {
        // get period from resources
        string periodName = Resource.Properties["Period"];

        // if there is no valid period set "Last 12 Months" as default
        if (string.IsNullOrEmpty(periodName))
        {
            Resource.Properties.Add("Period", defaultTimePeriod);
            periodName = defaultTimePeriod;
        }

        // parse period to begin and end
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);
    }

    private void InsertDefaultResourceConfiguration(NetObjectDto netobject)
    {
        if (defaultNetobjectTypes.ContainsKey(netobject.NetObjectType))
        {
            string defaultNetobjectTypesForNetObject = defaultNetobjectTypes[netobject.NetObjectType];
            Resource.Properties.Add(propertyNetobjectTypes, defaultNetobjectTypesForNetObject);
        }
    }

    private int GetEventTypes()
    {
        var selectedTypesString = GetStringValue(propertyNetobjectTypes, null);
        int currentTypes = 0;

        if (!string.IsNullOrEmpty(selectedTypesString))
        {
            var selectedTypes = selectedTypesString.Split(new string[1] { NetobjectTypesSeparator }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var selectedType in selectedTypes)
            {
                if (mapping.ContainsKey(selectedType))
                {
                    currentTypes |= (int)mapping[selectedType];
                }
            }
        }

        return currentTypes;
    }

    /// <summary>
    /// Gets the first found supported provider type or null when resource is on not supported view or summary view.
    /// </summary>
    private Type GetCurrentProviderType()
    {
        // the list is prioritized - thus the most general LTM/GTM and Node providers are in the end.
        var providersTypes = new Type[] { typeof(IF5GTMWideIPProvider), typeof(IF5LTMPoolMemberProvider), typeof(IF5LTMPoolProvider), typeof(IF5VirtualServerProvider), typeof(IF5ServerProvider), typeof(IF5GTMProvider), typeof(IF5LTMProvider), typeof(INodeProvider) };

        object provider = null;

        foreach (var providerType in providersTypes)
        {
            provider = GetInterfaceInstance(providerType);
            if (provider != null)
            {
                return providerType;
            }
        }
        return null;
    }

    private NetObjectDto GetCurrentNetobject(Type providerType)
    {
        if (providerType == null)
        {
            return null;
        }

        if (netobjectSelector.ContainsKey(providerType))
        {
            var getNetobject = netobjectSelector[providerType];

            if (getNetobject != null)
            {
                object provider = GetInterfaceInstance(providerType);
                return getNetobject(provider);
            }
        }

        return null;
    }
}