<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BalancingEnvironment.ascx.cs" Inherits="Orion_F5_Resources_Overview_BalancingEnvironment" %>

<%@ Register Src="~/Orion/F5/Controls/BalancingEnvironmentControl.ascx" TagPrefix="f5" TagName="be" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <f5:be runat="server" ID="BalancingEnvironmentControl"/>
    </Content>
</orion:resourceWrapper>