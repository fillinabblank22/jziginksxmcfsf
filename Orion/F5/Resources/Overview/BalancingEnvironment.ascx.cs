﻿using System;
using Resources;
using SolarWinds.F5.Web.Interfaces;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;


public partial class Orion_F5_Resources_Overview_BalancingEnvironment : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BalancingEnvironmentControl.Resource = Resource;
        BalancingEnvironmentControl.ResourceID = Resource.ID;
        BalancingEnvironmentControl.HeaderButtons = Wrapper.HeaderButtons;
        BalancingEnvironmentControl.PreselectedObject = null;
    }

    protected override string DefaultTitle
    {
        get { return F5WebContent.F5WEBDATA_PS0_1; }
    }

    public override String HelpLinkFragment
    {
        get { return ("orionphresourcebalancingenvironment"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/F5/Controls/EditResourceControls/EditBalancingEnvironment.ascx"; }
    }
}