﻿using System;
using Resources;
using SolarWinds.F5.Web.Interfaces.NetObjectProviders;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Resources_Overview_BalancingEnvironmentForEntity : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BalancingEnvironmentControl.Resource = Resource;
        BalancingEnvironmentControl.ResourceID = Resource.ID;
        BalancingEnvironmentControl.HeaderButtons = Wrapper.HeaderButtons;
        BalancingEnvironmentControl.PreselectedObject = GetParentalObject();
    }

    private NetObject GetParentalObject()
    {
        // All of these netobjects *must* have set the ID property to NetObjectID (int) !
        var ltmProvider = GetInterfaceInstance<IF5LTMProvider>();
        if (ltmProvider != null)
        {
            return ltmProvider.F5Ltm;
        }
        var gtmProvider = GetInterfaceInstance<IF5GTMProvider>();
        if (gtmProvider != null)
        {
            return gtmProvider.F5Gtm;
        }
        var poolProvider = GetInterfaceInstance<IF5LTMPoolProvider>();
        if (poolProvider != null)
        {
            return poolProvider.Pool;
        }
        var vsProvider = GetInterfaceInstance<IF5VirtualServerProvider>();
        if (vsProvider != null)
        {
            return vsProvider.F5VirtualServer;
        }
        var wideIpProvider = GetInterfaceInstance<IF5GTMWideIPProvider>();
        if (wideIpProvider != null)
        {
            return wideIpProvider.F5GtmWideIP;
        }
        var serverProvider = GetInterfaceInstance<IF5ServerProvider>();
        if (serverProvider != null)
        {
            return serverProvider.F5Server;
        }
        var poolMemberProvider = GetInterfaceInstance<IF5LTMPoolMemberProvider>();
        if (poolMemberProvider != null)
        {
            return poolMemberProvider.PoolMember;
        }
        return null;
    }

    protected override string DefaultTitle
    {
        get { return F5WebContent.F5WEBDATA_PS0_6; }
    }

    public override String HelpLinkFragment
    {
        get { return ("orionphresourcebalancingenvironment"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/F5/Controls/EditResourceControls/EditBalancingEnvironment.ascx"; }
    }
}