﻿using System;
using System.Web;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Shared;

public partial class Orion_F5_F5ServerDetails : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var s = NetObject as F5Server;
        if (s == null)
            throw new IndexOutOfRangeException(string.Format(F5WebContent.F5WEBCODE_PS0_15, NetObject.GetType().Name, NetObject.NetObjectID));

        ((Orion_View)this.Master).HelpFragment = "orionphviewf5serverdetails";
		
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

		base.OnInit(e);

        this.SStatusIcon.ImageUrl = "~" + StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.Server", (int)s.OrionStatus, StatusIconSize.Small);
        this.SStatusIcon.AlternateText = this.SStatusIcon.ToolTip = StatusInfo.GetStatus((int)s.OrionStatus).ShortDescription;

        this.ServerName = HttpUtility.HtmlEncode(((F5Server)NetObject).ShortName);
        this.Title = string.Concat(this.ViewInfo.ViewTitle, " - ", this.ServerName);

        ObsoleteDataMessageBar.NodeID = s.NodeId;

    }

    protected string ServerName { get; set; }

    /// <summary>
    /// ViewType
    /// </summary>
    public override string ViewType
    {
        get { return "F5ServerDetails"; }
    }
}