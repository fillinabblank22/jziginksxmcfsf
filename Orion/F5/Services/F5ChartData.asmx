﻿<%@ WebService Language="C#" Class="F5ChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.F5.Web;
using SolarWinds.F5.Web.Charts;
using SolarWinds.F5.Web.DAL;
using SolarWinds.F5.Web.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.F5.Web.Interfaces;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class F5ChartData : ChartDataWebService
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private const string _nodeNameLookupSwql = "SELECT NodeID, Caption FROM Orion.Nodes WHERE NodeID in @NodeID";
  

    [WebMethod]
    public ChartDataResults Connections(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 3);
        const string sql = @"SELECT f.DateTime, 
                                f.Avg_Connections as Connections,
                                f.Avg_ConnectionsSSL as ConnectionsSSL,
                                f.Avg_ConnectionsNew as ConnectionsNew
                             From  F5_System_DeviceStats f
                             where f.NodeID = @NetObjectId
                                AND f.DateTime >= @StartTime AND f.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        //var result = SimpleLoadData(request, dateRange, sql, string.Empty, DateTimeKind.Utc, "Connections", "ConnectionsSSL");


        var result = DoSimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Utc,
            new[] { "Connections", "ConnectionsSSL", "ConnectionsNew" },
            SampleMethod.Average, false
            );
        var outputData = new ChartDataResults(ComputeSubseries(request, dateRange, result));
        outputData.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(outputData);

        return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults Throughput(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 3);
        const string sql = @"SELECT f.DateTime, 
                                f.In_AvgClientThroughput + f.In_AvgServerThroughput as [In],
                                f.Out_AvgClientThroughput + f.Out_AvgServerThroughput as [Out],
                                f.In_AvgClientThroughput  + f.Out_AvgClientThroughput + f.In_AvgServerThroughput  + f.Out_AvgServerThroughput as [Total]
                             From  F5_System_DeviceStats f
                             where f.NodeID = @NetObjectId
                                AND f.DateTime >= @StartTime AND f.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        //var result = SimpleLoadData(request, dateRange, sql, string.Empty, DateTimeKind.Utc, "In", "Out", "Total");
        //return result;
        var result = DoSimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Utc,
            new[] { "In", "Out", "Total" },
            SampleMethod.Average, false
            );

        var outputData = new ChartDataResults(ComputeSubseries(request, dateRange, result));
        outputData.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(outputData);

        return CalculateMinMaxForYAxis(dynamicResult);
    }

    protected IEnumerable<DataSeries> ComputeSubseries(ChartDataRequest request, DateRange dateRange, IEnumerable<DataSeries> series, bool ignoreEmpty = true)
    {
        foreach (var serie in series)
        {
            if (!ignoreEmpty || serie.Data.Any(x => !x.IsNullPoint))
            {
                var trends = ComputeTrendsFromSampledData(request, dateRange, serie);
                yield return serie;
                foreach (var trend in trends)
                    yield return trend;
            }
        }
    }

    protected ChartDataResults SimpleLoadMinMaxAvgData_InOut(ChartDataRequest request, DateRange dateRange, string dataSql, string labelNamesSwql, DateTimeKind dateFormatInDatabase,
        string inAverageSeriesTag, string inMinMaxSeriesTag,
        string outAverageSeriesTag, string outMinMaxSeriesTag
        )
    {
        var limitedSql = Limitation.LimitSQL(dataSql);

        var allSeries = new List<DataSeries>();

        Dictionary<string, string> labelNames = null;

        if (request.NetObjectIds.Length > 1)
        {
            labelNames = LoadDisplayNameLookup(request.NetObjectIds, labelNamesSwql);
        }

        foreach (var netObjectId in request.NetObjectIds)
        {
            string label = null;
            if (labelNames != null)
            {
                labelNames.TryGetValue(netObjectId, out label);
            }

            var parameters = new[]
                {
                    new SqlParameter("NetObjectId", netObjectId),
                    new SqlParameter("StartTime", dateRange.StartDate),
                    new SqlParameter("EndTime", dateRange.EndDate)
                };

            var rawSeries = GetData(limitedSql, parameters, dateFormatInDatabase, netObjectId,
                inAverageSeriesTag, "inmin", "inmax",
                outAverageSeriesTag, "outmin", "outmax");

            /* in */
            {
                var rawAverage = rawSeries[0];
                var rawMin = rawSeries[1];
                var rawMax = rawSeries[2];

                rawAverage.Label = label;
                rawMin.Label = label;
                rawMax.Label = label;

                var sampledAverage = rawAverage.CreateSampledSeries(dateRange, request.SampleSizeInMinutes,
                                                                    SampleMethod.Average);

                var sampledMin = rawMin.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Min);
                var sampledMax = rawMax.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Max);

                var sampledMinMax = DataSeries.CombineMinMaxSeries(sampledMin, sampledMax, inMinMaxSeriesTag);

                allSeries.AddRange(new[] { sampledAverage, sampledMinMax });

                if (request.Calculate95thPercentile && rawAverage.HasData)
                    allSeries.Add(rawAverage.CreatePercentileSeries(sampledAverage.Dates));

                if (request.CalculateTrendLine && rawAverage.HasData)
                    allSeries.Add(rawAverage.CreateTrendSeries(dateRange, request.SampleSizeInMinutes));
            }

            /* out */
            {
                var rawAverage = rawSeries[3];
                var rawMin = rawSeries[4];
                var rawMax = rawSeries[5];

                rawAverage.Label = label;
                rawMin.Label = label;
                rawMax.Label = label;

                var sampledAverage = rawAverage.CreateSampledSeries(dateRange, request.SampleSizeInMinutes,
                                                                    SampleMethod.Average);

                var sampledMin = rawMin.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Min);
                var sampledMax = rawMax.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Max);

                var sampledMinMax = DataSeries.CombineMinMaxSeries(sampledMin, sampledMax, outMinMaxSeriesTag);

                allSeries.AddRange(new[] { sampledAverage, sampledMinMax });

                if (request.Calculate95thPercentile && rawAverage.HasData)
                    allSeries.Add(rawAverage.CreatePercentileSeries(sampledAverage.Dates));

                if (request.CalculateTrendLine && rawAverage.HasData)
                    allSeries.Add(rawAverage.CreateTrendSeries(dateRange, request.SampleSizeInMinutes));
            }
        }

        return new ChartDataResults(allSeries);
    }


    protected ChartDataResults SimpleLoadData(ChartDataRequest request, DateRange dateRange, string dataSql,
        string labelNamesSwql, DateTimeKind dateFormatInDatabase,
        string mainSeriesTag, string secondarySeriesTag, string thirdSeriesTag)
    {
        var limitedSql = Limitation.LimitSQL(dataSql);

        var allSeries = new List<DataSeries>();

        Dictionary<string, string> labelNames = null;

        if (request.NetObjectIds.Length > 1)
        {
            labelNames = LoadDisplayNameLookup(request.NetObjectIds, labelNamesSwql);
        }

        foreach (var netObjectId in request.NetObjectIds)
        {
            string label = null;
            if (labelNames != null)
            {
                labelNames.TryGetValue(netObjectId, out label);
            }

            var parameters = new[]
                {
                    new SqlParameter("NetObjectId", netObjectId),
                    new SqlParameter("StartTime", dateRange.StartDate),
                    new SqlParameter("EndTime", dateRange.EndDate)
                };

            List<DataSeries> rawSeries;

            rawSeries = GetData(limitedSql, parameters, dateFormatInDatabase, netObjectId, mainSeriesTag, secondarySeriesTag, thirdSeriesTag);

            var rawMainSeries = rawSeries[0];
            rawMainSeries.Label = label;

            var sampledMainSeries = rawMainSeries.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Average);

            allSeries.Add(sampledMainSeries);

            var rawSecondarySeries = rawSeries[1];
            rawSecondarySeries.Label = label;

            var sampledSecondarySeries = rawSecondarySeries.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Average);

            allSeries.Add(sampledSecondarySeries);

            var rawThirdSeries = rawSeries[2];
            rawThirdSeries.Label = label;

            var sampledThirdSeries = rawThirdSeries.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Average);

            allSeries.Add(sampledThirdSeries);


            if (request.Calculate95thPercentile && rawMainSeries.HasData)
                allSeries.Add(rawMainSeries.CreatePercentileSeries(sampledMainSeries.Dates));

            if (request.CalculateTrendLine && rawMainSeries.HasData)
                allSeries.Add(rawMainSeries.CreateTrendSeries(dateRange, request.SampleSizeInMinutes));
        }

        if (request.CalculateSum)
            allSeries.Add(DataSeries.CreateSumSeries(allSeries.Where(x => x.TagName == mainSeriesTag)));

        return new ChartDataResults(allSeries);
    }

    //Calculate minimal and maximal value for Y-axis. When is minVal higher then 0, we use 0.
    private ChartDataResults CalculateMinMaxForYAxis(ChartDataResults result)
    {
        double minVal;
        double maxVal;
        bool noPoints = CalculateMinMaxValueForDataSeries(result.DataSeries, out minVal, out maxVal);

        minVal = Math.Min(minVal, 0);		//when no points result is double.MaxValue
        if (!noPoints)
            maxVal = 0;

        if (result.ChartOptionsOverride == null)
            result.ChartOptionsOverride = new JsonObject();

        dynamic options = result.ChartOptionsOverride as JsonObject;
        if (options == null)
            _log.Error("ChartOptionsOverride is not a JsonObject");

        options.yAxis = JsonObject.CreateJsonArray(1);
        options.yAxis[0].min = minVal;
        options.yAxis[0].minRange = 10;

        result.ChartOptionsOverride = options;

        return result;
    }



    class DatePoint
    {
        public DateTime Time { get; set; }
        public int Count { get; set; }
    }

}
