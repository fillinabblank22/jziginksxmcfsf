﻿SW = SW || {};
SW.F5 = SW.F5|| {};
SW.F5.Charts= SW.F5.Charts || {};
SW.F5.Charts.HorizontalLegend = SW.F5.Charts.HorizontalLegend || {};

(function(legend) {
        legend.createStandardLegend = function(chart, legendContainerId) {
            var div = $('#' + legendContainerId);
            div.empty();
           
            $.each(chart.series, function (index, current) {

                
                if (current.options.id === 'highcharts-navigator-series') {
                    return;
                }
                var newHtml = $('<span class="horizontalLegendItem" />');
                

                if (current.options.customProperties && current.options.customProperties.NetObjectId) {
                    var netObjectId = current.options.customProperties.NetObjectId;
                    var href = '/Orion/View.aspx?NetObject=' + netObjectId;
                    var link = $('<a class="horizontalLegendItem" href="' + href + '"/>');

                    legend.addCheckbox(current, newHtml);
                    legend.addTextWithImage(current, link);
                    newHtml.append(link);
                } else
                {
                    legend.addCheckbox(current, newHtml);
                    legend.addTextWithImage(current, newHtml);
                }

                div.append(newHtml);
			});
        };


        legend.addTextWithImage = function(current, container) {
            
            //image
            SW.Core.Charts.Legend.addLegendSymbol(current, container);
            //Text
            var legendTextSpan = $('<span class="horizontalLegendText"/>').append(current.name);
            container.append(legendTextSpan);
        }

        legend.toggleSeries = function (series) {
                   series.visible ? series.hide() : series.show();
               };
        legend.addCheckbox = function (series, container) {
            
            var check = $('<input class="horizontalLegendCheckBox" type="checkbox"/>')
                                .click(function () { legend.toggleSeries(series); });
            
                    if (series.visible)
                        check.attr("checked", 'checked');
            
                    check.appendTo(container);
                };
}(SW.F5.Charts.HorizontalLegend)
)