﻿SW.Core.namespace("SW.F5.DetailsTable").SetupTable = function (id, route, action, elementId) {
    var showData = function(data) {
        var template = $('#DetailsTableTemplate-' + id).html();
        var newHtml = _.template(template, { rows: data });
        $('#DetailsTableContent-' + id).html(newHtml);
    };

    var showError = function (error) {
        var template = $('#DetailsTableError-' + id).html();
        var newHtml = _.template(template, { message: error });
        $('#DetailsTableContent-' + id).html(newHtml);
    };

    var refresh = function() {
        SW.Core.Services.callControllerAction(route, action, { ElementId: elementId }, showData, showError);
    }

    SW.Core.View.AddOnRefresh(refresh, "DetailsTableContent-" + id);
    refresh();

    return {
        refresh: refresh
    };
};