﻿(function () {
    var prefixes = {
        0: "",
        3: "k",
        6: "M",
        9: "T",
        12: "P"
    };

    var breakpoints = Object.keys(prefixes);

    var formatWithUnit = function(value, decimals, unit) {
        var magnitude = Math.floor(Math.log(value) / Math.LN10);

        var appliedMagnitude = breakpoints[0];
        for (var i = breakpoints.length; i > 0; i--) {
            if (magnitude >= breakpoints[i]) {
                appliedMagnitude = breakpoints[i];
                break;
            }
        }

        value *= Math.pow(10, -appliedMagnitude);
        return value.toFixed(decimals) + "&nbsp;" + prefixes[appliedMagnitude] + unit;
    };

    SW.Core.namespace("SW.F5").Formatters = {
        formatWithUnit: formatWithUnit
    };
}())
