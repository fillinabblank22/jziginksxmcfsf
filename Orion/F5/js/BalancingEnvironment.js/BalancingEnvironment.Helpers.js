﻿(function (beStatic, beVariables) {
    var sizeConstants = beStatic.sizeConstants;

    var statusIconCache = [];

    var posYFunc = function (lh, i) {
        return i == 0 ? 0 : lh.slice(0, i).reduce(function (a, b) { return a + b; }, 0);
    };

    var statusIcons = function (statusId, settings) {
        if (statusId in statusIconCache) {
            return statusIconCache[statusId];
        }

        var obj = {
            iconPath: String.format(settings.F5StatusIconPath, statusId, '&hint=Medium'),
            smallIconPath: String.format(settings.F5StatusIconPath, statusId, ''),
        };
        statusIconCache[statusId] = obj;

        return obj;
    };

    var onlyVerticalGroup = function (level) {
        return level.Grouping == "vertical";
    };

    var onlyUltimateGroup = function (level) {
        return level.Grouping == "ultimate";
    };

    var getHighest = function (previous, current) {
        if (current.count > previous) {
            return current.count
        }
        return previous;
    };

    var isInteger = function (number) {
        return (typeof number === "number") && (number % 1 === 0);
    };

    var numberToDigits = function (number) {
        return isInteger(number)? number.toString().length : 0;
    };

    var groupLengths = function (group) {
        var count = group.Statuses.reduce(getHighest, 0);
        return numberToDigits(count);
    };

    var toGroupLength = function (array) {
        var id = array.length > 0 ? array[0].levelId : null;

        if (id == null) {
            return;
        }

        return {
            Id: id,
            Groups: array.map(function (i) { return numberToDigits(i.count); })
        };
    };

    var regroupByStatus = function (d) {
        var statuses = d.SummaryStatuses.length ? d.SummaryStatuses : settings.DefaultSummaryStatuses;
        return statuses
            .map(function(v) {
                var objects = d.Objects.filter(function(e) { return e.Status == v; })
                    .map(function(o) {
                        return o.Id;
                    });
                return { status: v, count: objects.length, objects: objects, levelId: d.Id };
            })
            .filter(function(v) {
                return v.count > 0;
            })
            .map(function(v, i) {
                v.columnIndex = i;
                return v;
            });
    };

    var calculateStatusCounts = function (level) {
        for (var i = 0; i < level.Groups.length; i++) {
            var group = level.Groups[i];

            var statuses = level.SummaryStatuses.length ? level.SummaryStatuses : settings.DefaultSummaryStatuses;
            group.Statuses = statuses.map(function (v) {
                var objects = level.Objects.filter(function (e) {
                    return $.inArray(e.Id, group.ObjectIds) > -1 && e.Status == v;
                }).map(function (o) {
                    return o.Id;
                });

                return {
                    status: v,
                    count: objects.length,
                    objects: objects
                };
            }).filter(function (v) { return v.count > 0; });
        }
    };

    var calculateAreaTakenByVerticalGroups = function (level) {

        var w = level.Groups.reduce(function (previous, group) {
            return previous + groupLengths(group) * sizeConstants.digitWidth;
        }, 0);
        return w + level.Groups.length * (sizeConstants.linePadding + sizeConstants.imageSize) - ((level.Groups.lenght > 0) ? sizeConstants.linePadding : 0);
    };

    var setGrouping = function (level, previousLevel, groupByDefault, contentWidth) {
        if (groupByDefault) {
            if (level.Groups.length == 0 && previousLevel == null) {
                // no groups and no previous level so it's a situation when there are too many selected objects on line
                level.Grouping = 'ultimate';
            }
            else if (level.Groups.length == 1) {
                level.Grouping = 'ultimate';
            }
            else if (level.Groups.length > 1) {
                if (previousLevel.Grouping === 'ultimate' || level.Id === 'F5WIP') {
                    // we don't want the vertical grouping on WIP level
                    level.Grouping = 'ultimate';
                } else if (previousLevel.Grouping === 'vertical' && level.Id === 'F5PM') {
                    // on PoolMember level when the Pools are vertical the ultimate grouping should be applied
                    level.Grouping = 'ultimate';
                } else {
                    calculateStatusCounts(level);
                    var totalVerticalGroupWidth = calculateAreaTakenByVerticalGroups(level);

                    if (totalVerticalGroupWidth > contentWidth) {
                        level.Grouping = 'ultimate';
                    }
                    else {
                        level.Grouping = 'vertical';
                    }
                }
            }
        } else if (previousLevel && previousLevel.Grouping === 'ultimate') {
            level.Grouping = 'ultimate';
        }
    };

    // init some properties need for rendering
    var initProperties = function (data, resourceId) {
        var properties = {
            resourceHeight: 0,
            connectionsPresent: data.Connections && data.Connections.length > 0,
        };

        properties.noObjects = !data.IsAnyObjectManaged;
        if (properties.noObjects) {
            return properties;
        }

        var variables = beVariables.get(resourceId);
        var settings = variables.settings;

        var noProblematicObjects = settings.ShowOnlyProblems;
        $.each(data.Levels, function (idx, lvl) {
            if (lvl.Objects.length > 0) {
                noProblematicObjects = false;
                return false;
            }
        });

        properties.noProblematicObjects = noProblematicObjects;
        if (noProblematicObjects) {
            return properties;
        }

        var calculateUltimateGroupPositions = function (level) {
            var previous;
            var areaWidth = settings.SpaceForObjects - sizeConstants.linePadding;
            var firstItemShift = (sizeConstants.imageSize - sizeConstants.statusImageSize) / 2;
            var getFirstItemPosition = function (groupSize) {
                return {
                    x: firstItemShift,
                    y: 0,
                    count: groupSize
                }
            };
            var getCurrentItemPositionBasedOnPrevious = function (groupSize, previous) {
                var x = previous.x + previous.count * sizeConstants.digitWidth + sizeConstants.linePadding + sizeConstants.statusImageSize;
                var y = previous.y;
                var currentItemWidth = groupSize * sizeConstants.digitWidth + sizeConstants.statusImageSize;

                if (x + currentItemWidth >= areaWidth) {
                    x = firstItemShift;
                    y = y + sizeConstants.lineHeight + sizeConstants.multiLinePadding;
                }

                return {
                    x: x,
                    y: y,
                    count: groupSize
                };
            }

            var calculate = function (g, i) {
                var currentItem = (i == 0) ? getFirstItemPosition(g) : getCurrentItemPositionBasedOnPrevious(g, previous);
                previous = currentItem;
                return currentItem;
            };

            return {
                Id: level.Id,
                Groups: level.Groups.map(calculate)
            };
        };

        var itemSize = sizeConstants.imageSize
            + sizeConstants.linePadding
            + (settings.ShowObjectCaptions ? sizeConstants.objectCaptionWidth : 0);

        var contentWidth = settings.ResourceWidth
            - sizeConstants.levelNameColumnWidth
            - 2 * sizeConstants.linePadding
            - (settings.ShowStatusSummaryInternal ? sizeConstants.overallStatusColumnWidth : 0);

        var selectedObjectLevelIndex = -1;
        if (data.SelectedObject) {
            $.each(data.Levels, function (ind, e) {
                if ($.grep(data.ObjectsMarkedSelected, function (el) { return el.startsWith(e.Id + ':'); }).length > 0) {
                    selectedObjectLevelIndex = ind;
                    return false;
                }
                return true;
            });
        }

        if (selectedObjectLevelIndex > -1) {
            var i, level, previousLevel, totalItemWidth;
            for (var j = 0; j < data.Levels.length; j++) {
                data.Levels[j].Grouping = 'none';
            }

            level = data.Levels[selectedObjectLevelIndex];
            previousLevel = null;
            totalItemWidth = level.Objects.length * itemSize;

            setGrouping(level, previousLevel, totalItemWidth > contentWidth, contentWidth);

            for (i = selectedObjectLevelIndex + 1; i < data.Levels.length; i++) {
                level = data.Levels[i];
                previousLevel = data.Levels[i - 1];
                totalItemWidth = level.Objects.length * itemSize;

                setGrouping(level, previousLevel, totalItemWidth > contentWidth, contentWidth);
            }
            for (i = selectedObjectLevelIndex - 1; i >= 0; i--) {
                level = data.Levels[i];
                previousLevel = data.Levels[i + 1];
                totalItemWidth = level.Objects.length * itemSize;

                setGrouping(level, previousLevel, totalItemWidth > contentWidth, contentWidth);
            }
        }

        var ultimateGroups = data.Levels
            .filter(onlyUltimateGroup)
            .map(regroupByStatus)
            .map(toGroupLength)
            .map(calculateUltimateGroupPositions);

        properties.ultimateGroupWidths = ultimateGroups;

        var groupWidths = data.Levels.filter(onlyVerticalGroup).map(function (l) {
            return {
                Id: l.Id,
                Groups: l.Groups.map(groupLengths)
            };
        });

        var lineHeights = data.Levels.map(function (l) {
            // height of space for objects
            var objectsHeight = 0;

            if (!l.Grouping || l.Grouping === 'none') {
                var linesOnLevel = Math.max(Math.ceil(l.Objects.length / settings.ObjectsPerLine), 1);
                objectsHeight = 2 * sizeConstants.linePadding
                    + linesOnLevel * sizeConstants.lineHeight
                    + (linesOnLevel - 1) * sizeConstants.multiLinePadding;
            } else if (l.Grouping === 'ultimate') {
                var level = properties.ultimateGroupWidths.filter(function (level) { return level.Id == l.Id })[0];
                var firstItemShift = (sizeConstants.imageSize - sizeConstants.statusImageSize) / 2;
                var linesOnLevel = level.Groups.reduce(function (previous, current) {
                    return (current.x == firstItemShift) ? previous + 1 : previous;
                }, 0);

                objectsHeight = 2 * sizeConstants.linePadding
                    + linesOnLevel * sizeConstants.lineHeight
                    + (linesOnLevel - 1) * sizeConstants.multiLinePadding;

            } else if (l.Grouping === 'vertical') {
                var maxSummaryStatuses = Math.max.apply(null, l.Groups.map(function (d) { return d.Statuses.length; }));
                objectsHeight = 2 * sizeConstants.linePadding
                    + maxSummaryStatuses * sizeConstants.statusImageSize
                    + (maxSummaryStatuses - 1) * sizeConstants.multiLinePaddingSummary;
            }

            // height of space for summary statuses
            var summaryLinesOnLevel = Math.max(Math.ceil((l.SummaryStatuses.length ? l.SummaryStatuses : settings.DefaultSummaryStatuses).length / sizeConstants.summaryStatusesPerLine), 1);
            var summaryHeight = 2 * sizeConstants.linePadding
                + summaryLinesOnLevel * sizeConstants.statusImageSize
                + (summaryLinesOnLevel - 1) * sizeConstants.multiLinePaddingSummary;

            var height = Math.max(objectsHeight, settings.ShowStatusSummaryInternal ? summaryHeight : 0);

            properties.resourceHeight += height;

            return height;
        });

        variables.lineHeights = lineHeights;

        properties.groupWidths = groupWidths;
        properties.itemSize = itemSize;
        properties.objectsPerLine = settings.ObjectsPerLine;
        properties.showStatusSummary = settings.ShowStatusSummaryInternal;
        properties.resourceMinWidth = settings.ResourceMinWidth;

        return properties;
    };

    var characterWidthMap = {
        map: null,
        averageWidth: null,
        minCharacters: null,
        appendWidth: null,
        get: function (character) {
            return this.map[character] || this.averageWidth;
        },
        getStringWidth: function (string) {
            var width = 0;
            for (var i = 0; i < string.length; i++) {
                width += this.get(string.charAt(i));
            }
            return width;
        }
    };

    var generateCharacterWidthMap = function (resourceId) {
        var settings = beVariables.get(resourceId).settings;

        d3.select("#BalancingEnvironment-" + resourceId)
            .append('text')
            .attr('id', 'sw-be-generateCharacterWidthMap-' + resourceId)
            .attr('class', 'sw-be-objectCaption');

        var containingElement = document.getElementById('sw-be-generateCharacterWidthMap-' + resourceId);
        containingElement.style.visibility = 'hidden';

        var self = d3.select(containingElement);
        var totalWidth = 0;
        var maxWidth = 0;
        var charCodeFrom = settings.AllowedCharacterRange.min;
        var charCodeTo = settings.AllowedCharacterRange.max;
        characterWidthMap.map = {};

        for (var charCode = charCodeFrom; charCode <= charCodeTo; charCode++) {
            var char = String.fromCharCode(charCode);
            self.text(char);
            var width = Math.ceil(self.node().getComputedTextLength());
            characterWidthMap.map[char] = width;
            totalWidth += width;
            if (maxWidth < width) {
                maxWidth = width;
            }
        }

        characterWidthMap.averageWidth = totalWidth / (charCodeTo - charCodeFrom);
        characterWidthMap.appendWidth = characterWidthMap.getStringWidth('...');
        characterWidthMap.minCharacters = Math.floor((sizeConstants.objectCaptionWidth - characterWidthMap.appendWidth) / maxWidth);

        $(containingElement).remove();
    };

    var wrap = function () {
        var self = d3.select(this);
        var text = self.text();

        if (text.length <= characterWidthMap.minCharacters) {
            return;
        }

        var index = characterWidthMap.minCharacters + 1;
        var textLength = characterWidthMap.getStringWidth(text.slice(0, index));
        var lengthLimit = sizeConstants.objectCaptionWidth - characterWidthMap.appendWidth;

        while (textLength < lengthLimit) {
            index++;
            textLength += characterWidthMap.get(text.charAt(index));

            if (index >= text.length) {
                break;
            }
        }

        if (text.length > index) {
            self.text(text.slice(0, index) + '...');
        }
    };

    var calculateItemPosition = function (index, svgProperties) {
        var itemLine = Math.floor(index / svgProperties.objectsPerLine);

        var x = (index % svgProperties.objectsPerLine) * svgProperties.itemSize;

        var y = itemLine * (sizeConstants.imageSize + sizeConstants.multiLinePadding);

        var positionObject = { x: x, y: y };

        return positionObject;
    };

    var calculateBoundingPath = function (points, itemWidth, itemHeight, skipOuterType) {
        if (!points.length) {
            return [];
        }

        // group points per row
        var rows = [[]];
        var previousY = points[0].y;
        points.forEach(function (point) {
            if (previousY != point.y) {
                rows.push([]);
            }

            rows[rows.length - 1].push(point);
            previousY = point.y;
        });

        // special case - two parts that never meet
        if (rows.length == 2 && rows[0][0].x > rows[1][rows[1].length - 1].x + itemWidth) {
            var upper = calculateBoundingPath(rows[0], itemWidth, itemHeight, 'right');
            var lower = calculateBoundingPath(rows[1], itemWidth, itemHeight, 'left');

            lower[0].skip = true; // no connection between parts
            return upper.concat(lower);
        }

        // walk around - top to bottom
        var result = [];
        for (var i = 0; i < rows.length; i++) {
            if (i == 0) {
                var lastIndex = rows[i].length - 1;
                // starting point
                result.push({ x: rows[i][0].x, y: rows[i][0].y, skip: true });
                // full top line
                result.push({ x: rows[i][lastIndex].x + itemWidth, y: rows[i][lastIndex].y });
                // first line down (top right)
                result.push({ x: rows[i][lastIndex].x + itemWidth, y: rows[i][lastIndex].y + itemHeight, skip: skipOuterType === 'right' });
                continue;
            }

            var lastPointInPreviousRow = rows[i - 1][rows[i - 1].length - 1];
            var lastPointInCurrentRow = rows[i][rows[i].length - 1];

            // if current row does not end in the same column - add horizontal line to connect it
            if (lastPointInPreviousRow.x != lastPointInCurrentRow.x) {
                result.push({ x: lastPointInCurrentRow.x + itemWidth, y: lastPointInPreviousRow.y + itemHeight });
            }
            // add horizontal line down for this row
            result.push({ x: lastPointInCurrentRow.x + itemWidth, y: lastPointInCurrentRow.y + itemHeight });
        }

        // walk around - bottom to top
        var lastRowIndex = rows.length - 1;
        for (var i = lastRowIndex; i >= 0; i--) {
            if (i == lastRowIndex) {
                // full bottom line
                result.push({ x: rows[i][0].x, y: rows[i][0].y + itemHeight });
                // first line up (left bottom)
                result.push({ x: rows[i][0].x, y: rows[i][0].y, skip: skipOuterType === 'left' });
                continue;
            }

            var firstPointInPreviousRow = rows[i + 1][0];
            var firstPointInCurrentRow = rows[i][0];

            // if current row does not start in the same column - add horizontal line to connect it
            if (firstPointInPreviousRow.x != firstPointInCurrentRow.x) {
                result.push({ x: firstPointInCurrentRow.x, y: firstPointInPreviousRow.y });
            }
            // add horizontal line up for this row
            result.push({ x: firstPointInCurrentRow.x, y: firstPointInCurrentRow.y });
        }

        return result;
    };

    var pointsToPathDefinition = function (points) {
        var pathString = [];
        points.forEach(function (point) {
            var command = point.skip === true ? "M " : "L ";
            pathString.push(command + point.x + " " + point.y);
        });
        return pathString.join(' ');
    }

    var getContentContainer = function (resourceId) {
        return $('#Content-' + resourceId);
    };

    var calculateObjectsPerLine = function (resourceId) {
        var variables = beVariables.get(resourceId);
        var settings = variables.settings;

        var resourceMinWidthWithoutStatus = sizeConstants.levelNameColumnWidth
            + sizeConstants.minimalObjectsPerLine * (sizeConstants.imageSize + sizeConstants.linePadding)
            + sizeConstants.linePadding;
        var resourceMinWidthWithStatus = resourceMinWidthWithoutStatus + sizeConstants.overallStatusColumnWidth;

        // hide status summary when resource is too narrow
        var showStatusSummary = (settings.ResourceWidth >= resourceMinWidthWithStatus) && settings.ShowStatusSummary;
        var resourceMinWidth = showStatusSummary ? resourceMinWidthWithStatus : resourceMinWidthWithoutStatus;

        var spaceForObjects = Math.max(resourceMinWidth, settings.ResourceWidth)
            - sizeConstants.levelNameColumnWidth
            - (showStatusSummary ? sizeConstants.overallStatusColumnWidth : 0)
            - sizeConstants.linePadding;

        var itemSize = sizeConstants.imageSize
            + sizeConstants.linePadding
            + (settings.ShowObjectCaptions ? sizeConstants.objectCaptionWidth : 0);

        var objectsPerLine = Math.floor(spaceForObjects / itemSize);

        settings.SpaceForObjects = spaceForObjects;
        settings.ResourceMinWidth = resourceMinWidth;
        settings.ShowStatusSummaryInternal = showStatusSummary;
        settings.ObjectsPerLine = objectsPerLine;

        beVariables.update(resourceId, settings);
    };

    var getSummaryGroupObjects = function (resourceId, type, filter) {
        var lastData = beVariables.get(resourceId).lastData;
        if (!lastData) {
            return [];
        }

        var level = lastData.Levels.filter(function (l) {
            return l.Id === type;
        });
        if (!level) {
            return [];
        }

        return level[0].Objects.filter(filter);
    };

    var updateWidth = function (resourceId) {
        var variables = beVariables.get(resourceId);
        var settings = variables.settings;

        var parentDiv = getContentContainer(resourceId);
        var resourceContent = parentDiv.closest('.ResourceContent');
        var resourceWrapper = resourceContent.closest('.ResourceWrapper');
        settings.ResourceWidth = resourceWrapper.width() - parseInt(resourceContent.css('padding-left')) - parseInt(resourceContent.css('padding-right'));

        beVariables.update(resourceId, settings);

        calculateObjectsPerLine(resourceId);
    }

    var prepareSvg = function (resourceId, settings, svgProperties) {
        var svg = d3.select('#BalancingEnvironment-' + resourceId)
            .attr('width', settings.ResourceWidth)
            .attr('height', svgProperties.resourceHeight);

        return svg;
    };

    var getObjectDetailsUrl = function (resourceId, elementId) {
        var settings = beVariables.get(resourceId).settings;
        return String.format(settings.ObjectDetailsUrl, elementId);
    };

    var getAddNodeUrl = function (resourceId, id) {
        var settings = beVariables.get(resourceId).settings;
        var object = getObjectData(resourceId, id);
        var ipAddress = object && object.Metadata.filter(function (item) {
            return item.Key == 'mgmtip';
        });

        return String.format(settings.AddNodeUrl, ipAddress ? ipAddress[0].Value : "");
    };

    var getObjectData = function (resourceId, netObjectId) {
        var prefix = netObjectId.split(':')[0];
        var variables = beVariables.get(resourceId);
        if (!variables.lastData) {
            return null;
        }

        var objects = getSummaryGroupObjects(resourceId, prefix, function (item) {
            return item.Id == netObjectId;
        });
        if (!objects) {
            return null;
        }

        return objects[0];
    };

    var setupManagedObjectTooltip = function (resourceId, element) {
        var id = element.getAttribute('data-id');
        element.href = getObjectDetailsUrl(resourceId, id);
    };

    var setupUnmanagedObjectTooltip = function (resourceId, element) {
        var id = element.getAttribute('data-id');
        var prefix = id.split(':')[0];
        var variables = beVariables.get(resourceId);
        var object = getObjectData(resourceId, id);
        if (object == null) {
            return;
        }

        var ipAddress = object.Metadata.filter(function (item) {
            return item.Key == 'mgmtip';
        });

        if (element.getAttribute('class').indexOf('ForceTip') == -1) {
            element.setAttribute('class', element.getAttribute('class') + ' ForceTip');
        }

        $(element).data('url', String.format(
            variables.settings.NotManagedObjectTooltipUrl,
            encodeURIComponent(object.Caption),
            encodeURIComponent(prefix),
            encodeURIComponent(ipAddress ? ipAddress[0].Value : '')
        ));
    };

    var setupObjectTooltip = function (resourceId, element) {
        if (element.getAttribute('class').indexOf('sw-be-notManagedObject') >= 0) {
            setupUnmanagedObjectTooltip(resourceId, element);
        } else {
            setupManagedObjectTooltip(resourceId, element);
        }
    }

    var loadingStarted = function (resourceId) {
        var parentDiv = getContentContainer(resourceId);
        $('.sw-be-loading', parentDiv).show();
    };

    var loadingFinished = function (resourceId) {
        var parentDiv = getContentContainer(resourceId);
        $('.sw-be-loading', parentDiv).hide();
        $('.sw-be', parentDiv).show();
    };

    var displayErrorMessage = function (resourceId, message, element) {
        if (!element) {
            element = $("#LoadingError-" + resourceId);
            getContentContainer(resourceId).hide();
        }
        var textElement = element.find('div');
        var messageFormat = textElement.data('format');
        if (messageFormat) {
            textElement.html(String.format(messageFormat, message))
        }

        element.show();
    };

    var hideErrorMessage = function (resourceId, element) {
        if (!element) {
            element = $("#LoadingError-" + resourceId);
            getContentContainer(resourceId).show();
        }

        element.hide();
    };

    var getElementTitle = function (resourceId, elementId) {
        if (!elementId) {
            return null;
        }

        var parentDiv = getContentContainer(resourceId);
        var element = $('.sw-be-object[data-id="' + elementId + '"]', parentDiv);
        var text = element.data('title');
        return text ? text : elementId;
    };

    var applyObjectSelection = function (resourceId, settings) {
        updateSelectedObjectBox(resourceId, settings.SelectedObjectCaption);
        updateSearchPlaceholder(resourceId, settings.SelectedObjectCaption);
    };

    var updateSelectedObjectBox = function (resourceId, text) {
        var variables = beVariables.get(resourceId);
        var settings = variables.settings;

        var parentDiv = getContentContainer(resourceId);
        var box = $('.sw-be-selected-object-box', parentDiv);
        if (!text || !text.length) {
            box.hide();
            return;
        }

        if (settings.PreselectedObject) {
            box.find('.sw-be-unselectObject').hide();
        } else {
            box.find('.sw-be-unselectObject').show();
        }

        var innerDiv = box.find('.sw-be-selected-object-box-inner > div');
        innerDiv.toggleClass('sw-be-selected-object-clickable', !settings.PreselectedObject);
        innerDiv.toggleClass('sw-be-selected-object-preselected', settings.PreselectedObject);

        box.find('.sw-be-selectObjectTitle').text(text);
        box.show();
    };

    var updateSearchPlaceholder = function (resourceId, text) {
        var variables = beVariables.get(resourceId);
        var search = variables.searchBox;

        var newPlaceholder = "";
        if (text && text.length) {
            var specificPlaceholder = variables.settings.Strings['placeholderSelectedObject'];
            newPlaceholder = String.format(specificPlaceholder, text);
        } else {
            newPlaceholder = search.getPlaceholder();
        }

        search.setPlaceholder(newPlaceholder);
    };

    var searchObjects = function (resourceId, string, successCallback, errorCallback) {
        if (!string) {
            successCallback(null);
            return;
        }
        var variables = beVariables.get(resourceId);

        var postData = prepareDataForWebservice(variables.settings);
        string = encodeURIComponent(string);
        SW.Core.Services.callControllerAction(variables.route, variables.actions.GetSearchResults + '/?text=' + string, postData, successCallback, errorCallback);
    };

    var clearSearchBox = function (resourceId) {
        var variables = beVariables.get(resourceId);
        if (variables) {
            variables.lastHighlighted = null;
            variables.searchBox.setValue('');
            loadingFinished(resourceId);
        }
    };

    var processDataResponse = function (data, variables) {
        variables.settings.SelectedObjectCaption = data.SelectedObjectCaption;
        variables.settings.ObjectsMarkedSelected = data.ObjectsMarkedSelected;
        variables.lastData = data;
    };

    var refresh = function (resourceId, renderFunc) {
        var variables = beVariables.get(resourceId);

        loadingStarted(resourceId);
        var postData = prepareDataForWebservice(variables.settings);
        SW.Core.Services.callControllerAction(variables.route, variables.actions.GetData, postData, function (data) {
            hideErrorMessage(resourceId);
            processDataResponse(data, variables);

            showHideDropdown(resourceId, !variables.settings.SelectedObject);
            renderFunc(data, resourceId);
            loadingFinished(resourceId);
        }, function (message) {
            displayErrorMessage(resourceId, message);
        });
    };

    var rememberState = function (resourceId) {
        var variables = beVariables.get(resourceId);
        var postData = prepareDataForWebservice(variables.settings);
        SW.Core.Services.callControllerAction(variables.route, variables.actions.RememberState, postData, function () { });

        var sessionKey = "SW.F5.BalancingEnvironment-" + resourceId + ".SelectedObject";
        if (variables.settings.SelectedObject) {
            sessionStorage.setItem(sessionKey, variables.settings.SelectedObject);
        } else {
            sessionStorage.removeItem(sessionKey);
        }
    };

    var getRememberedSelectionState = function (resourceId) {
        return sessionStorage.getItem("SW.F5.BalancingEnvironment-" + resourceId + ".SelectedObject");
    };

    var getInitiallySelectedObject = function(resourceId) {
        var vars = beVariables.get(resourceId);
        if (vars.settings.PreselectedObject) {
            return vars.settings.SelectedObject;
        }

        var rememberedSelectedObject = getRememberedSelectionState(resourceId);
        if (rememberedSelectedObject !== vars.settings.SelectedObject) {
            return rememberedSelectedObject;
        }

        return vars.settings.SelectedObject;
    };

    var showHideDropdown = function (resourceId, show) {
        var dropdownElement = $('#ShowObjects-' + resourceId);
        if (show) {
            dropdownElement.show();
        } else {
            dropdownElement.hide();
        }
    };

    var prepareDataForWebservice = function (settings) {
        return {
            ShowStatusSummary: settings.ShowStatusSummary,
            ShowObjectCaptions: settings.ShowObjectCaptions,
            ShowOnlyProblems: !settings.SelectedObject && settings.ShowOnlyProblems,
            ResourceId: settings.ResourceId,
            ParentId: settings.ParentId,
            SelectedObject: settings.SelectedObject,
            PreselectedObject: settings.PreselectedObject
        };
    };

    SW.Core.namespace("SW.F5.BalancingEnvironment").Helpers = {
        applyObjectSelection: applyObjectSelection,
        calculateBoundingPath: calculateBoundingPath,
        calculateItemPosition: calculateItemPosition,
        clearSearchBox: clearSearchBox,
        displayErrorMessage: displayErrorMessage,
        generateCharacterWidthMap: generateCharacterWidthMap,
        getAddNodeUrl: getAddNodeUrl,
        getContentContainer: getContentContainer,
        getElementTitle: getElementTitle,
        getObjectDetailsUrl: getObjectDetailsUrl,
        getInitiallySelectedObject: getInitiallySelectedObject,
        getSummaryGroupObjects: getSummaryGroupObjects,
        hideErrorMessage: hideErrorMessage,
        initProperties: initProperties,
        pointsToPathDefinition: pointsToPathDefinition,
        posYFunc: posYFunc,
        prepareSvg: prepareSvg,
        refresh: refresh,
        regroupByStatus: regroupByStatus,
        rememberState: rememberState,
        searchObjects: searchObjects,
        setupObjectTooltip: setupObjectTooltip,
        showHideDropdown: showHideDropdown,
        statusIcons: statusIcons,
        updateWidth: updateWidth,
        wrap: wrap,
    };
})(
    SW.F5.BalancingEnvironment.Static,
    SW.F5.BalancingEnvironment.Variables
);
