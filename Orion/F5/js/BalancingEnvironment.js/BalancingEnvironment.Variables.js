﻿(function () {
    var variables = {};

    var createVariables = function() {
        return {
            lineHeights: [],
            objectPositions: {},
            tooltipDiv: undefined,
            searchBox: null,
            settings: {},
            lastData: {},
            lastHighlighted: null,
            rendering: false,
            levelShifts: {}
        };
    };

    var updateSettings = function(resourceId, newSettings) {
        var current = variables[resourceId];

        return $.extend(current.settings, newSettings);
    };
    var create = function(resourceId, settings) {
        var vars = createVariables();
        vars.settings = settings;
        variables[resourceId] = vars;

        return vars;
    };

    SW.Core.namespace("SW.F5.BalancingEnvironment").Variables = {
        create: create,
        get: function(resourceId) {
            return variables[resourceId];
        },
        update: updateSettings
    };
})();