(function (beStatic, beHelpers, beVariables) {
    var sizeConstants = beStatic.sizeConstants;
    var posYFunc = beHelpers.posYFunc;
    var statusIcons = beHelpers.statusIcons;
    var wrap = beHelpers.wrap;
    var initProperties = beHelpers.initProperties;
    var calculateItemPosition = beHelpers.calculateItemPosition;
    var prepareSvg = beHelpers.prepareSvg;

    var addDefinitions = function (svg) {
        if ($('defs', $(svg[0])).length) {
            return;
        }
        var defs = svg.append('defs');
        defs.append('pattern')
            .attr('id', 'leftShadow')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', sizeConstants.shadowWidth)
            .attr('height', 1)
            .attr('patternUnits', 'userSpaceOnUse')
            .append('svg:image')
            .attr('xlink:href', '/Orion/F5/images/BalancingEnvironment/shadow_small.png')
            .attr('width', sizeConstants.shadowWidth)
            .attr('height', 1)
            .attr('x', 0)
            .attr('y', 0);
        defs.append('pattern')
            .attr('id', 'rightShadow')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', sizeConstants.shadowWidth)
            .attr('height', 1)
            .attr('patternUnits', 'userSpaceOnUse')
            .attr('patternTransform', 'rotate(180, 2, 0)')
            .append('svg:image')
            .attr('xlink:href', '/Orion/F5/images/BalancingEnvironment/shadow_small.png')
            .attr('width', sizeConstants.shadowWidth)
            .attr('height', 1)
            .attr('x', 0)
            .attr('y', 0);
        defs.append('pattern')
            .attr('id', 'selectedFill')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', 2)
            .attr('height', 2)
            .attr('patternUnits', 'userSpaceOnUse')
            .append('svg:image')
            .attr('xlink:href', '/Orion/F5/images/BalancingEnvironment/selected.png')
            .attr('width', 2)
            .attr('height', 2)
            .attr('x', 0)
            .attr('y', 0);
    };

    var getObjectWidth = function (settings) {
        return sizeConstants.imageSize + (2 * sizeConstants.selectedRectSpan)
            + (settings.ShowObjectCaptions ? sizeConstants.objectCaptionWidth : 0);
    };

    var getObjectHeight = function (settings) {
        return sizeConstants.imageSize + (2 * sizeConstants.selectedRectSpan);
    };

    var renderZebraStripes = function (svg, data, settings) {
        var vars = beVariables.get(settings.ResourceId);

        var stripeLayer = svg.select('.sw-be-stripe-layer');
        if (stripeLayer.empty()) {
            stripeLayer = svg.append('g')
                .attr('class', 'sw-be-stripe-layer');
        }

        var levels = stripeLayer
            .selectAll('.sw-be-row')
            .data(data.Levels);

        levels.enter().append('rect')
            .attr('x', 0);

        levels.attr('x', 0)
            .attr('y', function (d, i) {

                return posYFunc(vars.lineHeights, i);
            })
            .attr('width', settings.ResourceWidth)
            .attr('height', function (d, i) { return vars.lineHeights[i]; })
            .attr('class', function (d, i) { return 'sw-be-row' + ((i % 2 == 0) ? ' sw-be-row-odd' : ' sw-be-row-even'); });
    };

    var createLineLayer = function (svg, settings) {
        var vars = beVariables.get(settings.ResourceId);

        var lineLayer = svg.select('.sw-be-line-layer');
        if (lineLayer.empty()) {
            lineLayer = svg.append('g')
                .attr('class', 'sw-be-line-layer');
        }

        var linePos = sizeConstants.linePadding + sizeConstants.imageSize / 2;
        var leftLine = lineLayer.select('.sw-be-levelLine');
        if (leftLine.empty()) {
            leftLine = lineLayer.append('line')
                .attr('stroke-dasharray', '2,2')
                .attr('class', 'sw-be-levelLine');
        }

        leftLine.attr('x1', linePos)
            .attr('y1', linePos)
            .attr('x2', linePos)
            .attr('y2', linePos + posYFunc(vars.lineHeights, vars.lineHeights.length - 1));

        return lineLayer;
    };

    var createLevelLayersWithDescription = function (svg, data, settings) {
        var vars = beVariables.get(settings.ResourceId);

        var levelLayer = svg.select('.sw-be-level-layer');
        if (levelLayer.empty()) {
            levelLayer = svg.append('g')
                .attr('class', 'sw-be-level-layer');
        }
        // layer with objects
        var level = levelLayer
            .selectAll('.sw-be-level')
            .data(data.Levels, function (d) { return d.Id; });

        var newLevel = level.enter()
            .append('g')
            .attr('class', 'sw-be-level')
            .attr('data-label', function (d) { return d.Name; })
            .attr('data-level', function (d) { return d.Id; });

        // left splitter
        newLevel.append('rect')
                .attr('fill', 'url(#leftShadow)')
                .attr('x', sizeConstants.levelNameColumnWidth)
                .attr('y', 0)
                .attr('width', 4);

        // level description with image and text
        var levelDesc = newLevel.append('g')
            .attr('class', 'sw-be-level-description')
            .attr('transform', function () { return 'translate(' + sizeConstants.linePadding + ',' + sizeConstants.linePadding + ')'; });

        levelDesc.append('rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', sizeConstants.imageSize)
            .attr('height', sizeConstants.imageSize)
            .attr('class', function (d, i) { return (i % 2 == 0) ? 'sw-be-row-odd' : 'sw-be-row-even'; });

        levelDesc.append('svg:image')
            .attr('class', 'sw-be-level-icon')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', sizeConstants.imageSize)
            .attr('height', sizeConstants.imageSize);

        levelDesc.append('text')
            .attr('x', sizeConstants.imageSize + sizeConstants.imgTextSpan)
            .attr('y', (sizeConstants.imageSize - sizeConstants.fontSize) / 2 + sizeConstants.fontSize)
            .attr('class', 'sw-be-level-caption');

        level.attr('transform', function (d, i) {
            var y = posYFunc(vars.lineHeights, i);
            vars.levelShifts[d.Id] = y;
            return 'translate(0,' + y + ')';
        });

        // update changed data
        var captionTextOffset = sizeConstants.imageSize + sizeConstants.imgTextSpan;
        var captionTextWidth = sizeConstants.levelNameColumnWidth - 2 * sizeConstants.linePadding - captionTextOffset;

        level.select('rect')
            .attr('height', function (d, i) { return vars.lineHeights[i]; });
        level.select('.sw-be-level-icon')
            .attr('xlink:href', function (d) { return d.IconPath; });
        var levelCaption = level
            .select('.sw-be-level-caption')
            .text(function (d) { return d.Name; });
        levelCaption
            .append('tspan')
                .attr('class', 'sw-be-level-itemCount')
                .text(function (d) { return ' (' + d.Objects.length + ')'; });
        levelCaption
            .call(wrapText, captionTextWidth, captionTextOffset);

        return level;
    };

    var wrapText = function (levelDescriptions, maxWidth, x) {
        levelDescriptions.each(function () {
            var levelDescription = d3.select(this);
            var text = levelDescription.text();
            if (levelDescription.node().getComputedTextLength() < maxWidth) {
                // text fits the level descriptions column, no need to add linebreaks
                return;
            }

            // extra string " (123)" after level description needs to be considered
            var itemCountElement = levelDescription.select('.sw-be-level-itemCount');
            var itemCountString = itemCountElement.text();
            var width = maxWidth - itemCountElement.node().getComputedTextLength();
            text = text.substring(0, text.length - itemCountString.length);

            var substringFrom = 0;

            // keep filling current line (~tspan element) with description until it fills the column or ends
            var tspan = levelDescription.text(null).append("tspan");
            for (var i = 0; i < text.length; i++) {
                var line = text.substring(substringFrom, i);

                tspan.text(line + text[i]);
                if (tspan.node().getComputedTextLength() > width) {
                    tspan.text(line);
                    // create new line
                    tspan = levelDescription.append("tspan").attr("x", x).attr("dy", "1.2em");
                    substringFrom = i;
                    i++;
                }
            }

            // element got deleted, re-adding
            levelDescription.append('tspan')
                .attr('class', 'sw-be-level-itemCount')
                .text(function (d) { return ' (' + d.Objects.length + ')'; });
        });
    };

    var addNewObject = function (newObjectGroups, settings) {
        var highlightedBoxWidth = sizeConstants.imageSize
            + (2 * sizeConstants.highlightedRectSpan)
            + (settings.ShowObjectCaptions ? sizeConstants.objectCaptionWidth : 0);

        newObjectGroups
            .append('rect')
            .attr('class', 'sw-be-selected-object')
            .attr('x', -sizeConstants.selectedRectSpan + 0.5)
            .attr('y', -sizeConstants.selectedRectSpan + 0.5)
            .attr('height', sizeConstants.imageSize + (2 * sizeConstants.selectedRectSpan))
            .attr('fill', 'url(#selectedFill)')
            .attr('shape-rendering', 'crispEdges');

        newObjectGroups
            .append('rect')
            .attr('class', 'sw-be-highlighted-object')
            .attr('x', -sizeConstants.highlightedRectSpan + 0.5)
            .attr('y', -sizeConstants.highlightedRectSpan + 0.5)
            .attr('height', sizeConstants.imageSize + (2 * sizeConstants.highlightedRectSpan))
            .attr('width', highlightedBoxWidth)
            .attr('shape-rendering', 'crispEdges');

        newObjectGroups
            .append('svg:image')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', sizeConstants.imageSize)
            .attr('height', sizeConstants.imageSize)
            .attr('class', 'sw-be-objectIcon');

        if (settings.ShowObjectCaptions) {
            newObjectGroups
                .append('text')
                .attr('class', 'sw-be-objectCaption')
                .attr('x', sizeConstants.imageSize + sizeConstants.imgTextSpan)
                .attr('y', (sizeConstants.imageSize - sizeConstants.fontSize) / 2 + sizeConstants.fontSize)
                .text(function (d) { return d.Caption; })
                .each(wrap);
        }
    };

    var setHighlight = function (objectGroups, settings) {
        // remove highlight
        objectGroups
            .select('.sw-be-selected-object-active')
            .attr('width', '0')
            .attr('height', '0')
            .attr('class', 'sw-be-selected-object');
        // highlight the selected object (if any)
        objectGroups
            .filter(function (d) { return settings.ObjectsMarkedSelected.indexOf(d.Id) !== -1; })
            .select('.sw-be-selected-object')
            .attr('height', getObjectHeight(settings))
            .attr('width', getObjectWidth(settings))
            .attr('class', 'sw-be-selected-object sw-be-selected-object-active');
    };

    var updateObjects = function (objectGroups, positionFunc, settings) {
        // set if not managed
        objectGroups.classed('sw-be-notManagedObject', function (d) {
            return d.Metadata.some(function (item) { return item.Key == 'notmanaged' && item.Value === 'True'; });
        });

        // update position & details
        objectGroups
            .attr('data-has-details', function (d) { return d.HasDetails; })
            .attr('transform', function (d, i) {
                var objPos = positionFunc(d, i);
                return 'translate(' + objPos.x + ',' + objPos.y + ')';
            })
            .attr('data-title', function (d) { return d.Caption; });
        // update status icon
        objectGroups.select('.sw-be-objectIcon')
            .attr('xlink:href', function (d) { return statusIcons(d.Status, settings).iconPath; })
            .attr('data-status', function (d) { return d.Status; });
    };

    var haGroupDefinitionToIndexes = function (allLevelObjects, haGroupDefinition, grouping) {
        var result = {
            members: haGroupDefinition.ObjectIds,
            indexes: [],
            display: !grouping || grouping === 'none'
        };

        allLevelObjects.forEach(function (value, index) {
            if (haGroupDefinition.ObjectIds.indexOf(value.Id) !== -1) {
                result.indexes.push(index);
            }
        });

        return result;
    };

    var addHAGroups = function (objectsGroups) {
        var haGroupElements = objectsGroups.selectAll('.sw-be-ha-group')
            .data(function (d) {
                var haGroupDefinitions = d.Groups.filter(function (group) {
                    return !group.ParentId || 0 === group.ParentId.length;
                });

                return haGroupDefinitions.map(function (haGroupDefinition) {
                    return haGroupDefinitionToIndexes(d.Objects, haGroupDefinition, d.Grouping);
                });
            });

        haGroupElements.enter().append('path')
            .attr('data-members', function (d) { return d.members; })
            .attr('stroke-dasharray', '2, 2');

        haGroupElements.exit().remove();

        return haGroupElements;
    };

    var renderHAGroups = function (haGroupElements, settings, svgProperties) {
        var itemWidth = svgProperties.itemSize + sizeConstants.haGroupWidthAdjustment;
        var itemHeight = sizeConstants.imageSize + sizeConstants.multiLinePadding + sizeConstants.haGroupHeightAdjustment;

        haGroupElements
            .attr('d', function (d) {
                var positions = d.indexes.map(function (index) {
                    var position = beHelpers.calculateItemPosition(index, svgProperties);
                    position.x += sizeConstants.haGroupLeftShift;
                    position.y += sizeConstants.haGroupTopShift;
                    return position;
                });

                var pathPoints = beHelpers.calculateBoundingPath(positions, itemWidth, itemHeight);
                return beHelpers.pointsToPathDefinition(pathPoints);
            })
            .attr('class', function (d) { return d.display ? 'sw-be-ha-group' : 'sw-be-ha-group hidden'; });
    };

    var renderObjects = function (level, settings, svgProperties, positionFunc) {
        // objects on the level
        var objectsGroups = level.select('.sw-be-object-group');
        if (objectsGroups.empty()) {
            objectsGroups = level.append('g')
            .attr('class', 'sw-be-object-group')
            .attr('transform', 'translate(' + (sizeConstants.levelNameColumnWidth + sizeConstants.linePadding) + ',' + sizeConstants.linePadding + ')');
        }

        var objectGroups = objectsGroups.selectAll('.sw-be-object')
            .data(function (d) {
                // we want show objects only when there is no grouping
                if (!d.Grouping || d.Grouping === 'none') {
                    return d.Objects;
                }
                return new Array(0);
            }, function (d) { return d.Id; });

        var newObjectGroups = objectGroups.enter().append('g')
            .attr('class', 'sw-be-object')
            .attr('data-id', function (d) { return d.Id; });

        // just hide removed data
        objectGroups.exit()
            .attr('class', 'sw-be-object hidden');

        addNewObject(newObjectGroups, settings);

        // show hidden objects
        objectGroups
            .attr('class', 'sw-be-object');

        setHighlight(objectGroups, settings);

        updateObjects(objectGroups, positionFunc, settings);

        var haGroupElements = addHAGroups(objectsGroups);

        renderHAGroups(haGroupElements, settings, svgProperties);
    };

    var renderUltimateGroup = function (ultimateGroup, settings, svgProperties) {
        var objectIds = new Array();
        var groupWidths = svgProperties.ultimateGroupWidths;

        var positionFunc = function (d, i) {
            var level = groupWidths.filter(function (g) { return g.Id == d.levelId })[0];

            var x = level.Groups[i].x;
            var y = level.Groups[i].y;

            return { x: x, y: y };
        };

        var itemWidth = sizeConstants.statusSummaryShift - sizeConstants.highlightedGroupRectSpanX;

        renderStatusSummaryStatuses(ultimateGroup, function (d) {
            d.Objects.reduce(function (prev, curr) {
                prev.push(curr.Id);
                return prev;
            }, objectIds);
            return beHelpers.regroupByStatus(d);
        }, positionFunc, settings, itemWidth, groupWidths);

        ultimateGroup.attr('data-objects', function (d) { return JSON.stringify(objectIds); });
    };

    var renderVerticalGroups = function (verticalGroups, settings, svgProperties, addObjectPositionCallback) {
        var itemWidth = sizeConstants.imageSize + sizeConstants.verticalGroupBoxTextWidth + 1;
        var groupWidths = svgProperties.groupWidths;

        var groups = verticalGroups.selectAll('g')
            .data(function (d) {
                return d.Groups.filter(function (g) { return (g.ParentId && g.ParentId.length > 0); })
                .map(function (g, i) {
                    g.levelId = d.Id;
					g.columnIndex = i;
                    return g;
                });
            }, function (d) { return d.ParentId; })
            .enter()
            .append('g')
            .attr('transform', function (d, i) {
                var xPosition = 0;
                // calculate x position only for 2nd and other element in array
                if (i > 0) {
                    var level = groupWidths.filter(function(element) { return element.Id == d.levelId; })[0];
                    if (level) {
                        // get number of digits in largest status number
                        var maxDigits = level.Groups.reduce(function (previous, current, currentIndex) {
                            if (currentIndex < i) {
                                return previous + current;
                            }
                            return previous;
                        }, 0);
                        xPosition = maxDigits * sizeConstants.digitWidth + i * (sizeConstants.linePadding + sizeConstants.imageSize);
                    }
                }

                var x = xPosition;
                var y = 0;
                addObjectPositionCallback(d.ObjectIds[0], { x: x, y: y });
                return 'translate(' + x + ',' + y + ')';
            })
            .attr('data-parent', function (d) { return d.ParentId; })
            .attr('data-objects', function (d) { return JSON.stringify(d.ObjectIds); });

        // rectangle
        groups.append('rect')
            .attr('class', 'sw-be-group-vertical-box')
            .attr('x', 0.5)
            .attr('y', 0.5)
            .attr('width', function (d, i) {
                var groupWidth = 0;
                var level = groupWidths.filter(function(element) { return element.Id == d.levelId; })[0];
                if (level) {
                    var maxDigits = level.Groups[i];
                    groupWidth = maxDigits * sizeConstants.digitWidth + sizeConstants.imageSize;
                }
                return groupWidth;
            })
            .attr('height', function (d) {
                return 2 * sizeConstants.verticalGroupBoxPadding
                    + d.Statuses.length * sizeConstants.statusImageSize
                    + (d.Statuses.length - 1) * sizeConstants.multiLinePaddingSummary
                    + 1;
            });

        var dataFunc = function (d) {
            var statusesAndLevelId = d.Statuses.map(function(st) {
                return {
                    levelId: d.levelId,
                    objects: st.objects,
                    count: st.count,
                    status: st.status,
					columnIndex: d.columnIndex
                }
            });
            return statusesAndLevelId;
        };

        var positionFunc = function (d, i) {
            var x = sizeConstants.verticalGroupBoxPadding;
            var y = sizeConstants.verticalGroupBoxPadding + i * (sizeConstants.statusImageSize + sizeConstants.multiLinePaddingSummary);

            return { x: x, y: y };
        };

        renderStatusSummaryStatuses(groups, dataFunc, positionFunc, settings, itemWidth - 1, groupWidths);
    };

    var renderGroups = function (level, settings, svgProperties, addObjectPositionCallback) {
        var objectGroup = level
            .select('.sw-be-object-group');

        // ultimate grouping
        objectGroup.select('.sw-be-group-ultimate').remove();

        var ultimateGroup = objectGroup
            .filter(function (d) { return d.Grouping === 'ultimate'; })
            .append('g')
            .attr('class', 'sw-be-group-ultimate');

        if (!ultimateGroup.empty()) {
            renderUltimateGroup(ultimateGroup, settings, svgProperties);
        }

        //vertical grouping
        objectGroup.select('.sw-be-group-vertical').remove();

        var verticalGroups = objectGroup
            .filter(function (d) { return d.Grouping === 'vertical'; })
            .append('g')
            .attr('class', 'sw-be-group-vertical');

        if (!verticalGroups.empty()) {
            renderVerticalGroups(verticalGroups, settings, svgProperties, addObjectPositionCallback);
        }
    };

    var renderStatusSummaryStatuses = function (parentGroup, summaryDataFunc, positionFunc, settings, itemWidth, groupWidths) {
        // groups for each status
        var summaryStatus = parentGroup
            .selectAll('.sw-be-summary-group')
            .data(function (d) { return summaryDataFunc(d); }, function (d) { return d.status; });

        summaryStatus.exit().remove();

        var newSummaryStatus = summaryStatus
            .enter().append('g')
                .attr('class', 'sw-be-summary-group');

        if (itemWidth) {
            newSummaryStatus
                .append('rect')
                .attr('class', 'sw-be-highlighted-object')
                .attr('shape-rendering', 'crispEdges');
        }

        newSummaryStatus.append('svg:image')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', sizeConstants.statusImageSize)
            .attr('height', sizeConstants.statusImageSize)
            .attr('class', 'sw-be-icon-small');

        newSummaryStatus.append('text')
            .attr('x', sizeConstants.statusImageSize + sizeConstants.imgTextSpan)
            .attr('y', (sizeConstants.statusImageSize - sizeConstants.fontSize) / 2 + sizeConstants.fontSize)
            .attr('class', 'sw-be-summary-text');

        summaryStatus
            .attr('class', function (d) { return 'sw-be-summary-group' + (d.count == 0 ? ' sw-be-summary-empty-group' : ' sw-be-summary-fill-group'); })
            .attr('transform', function (d, i) {
                var pos = positionFunc(d, i);
                return 'translate(' + pos.x + ',' + pos.y + ')';
            })
            .attr('data-status', function (d) { return d.status; })
            .attr('data-members', function (d) { return d.objects; });

        summaryStatus.select('.sw-be-icon-small')
            .attr('xlink:href', function (d) { return statusIcons(d.status, settings).smallIconPath; });
        summaryStatus.select('.sw-be-summary-text')
            .text(function (d) { return d.count; });

        if (itemWidth) {
            var statusItemHeight = sizeConstants.statusImageSize + 2 * sizeConstants.highlightedGroupRectSpanY;
            summaryStatus.select('.sw-be-highlighted-object')
                .attr("x", function () { return -(sizeConstants.highlightedGroupRectSpanX / 2); })
                .attr("y", function () { return -(sizeConstants.highlightedGroupRectSpanY / 2); })
                .attr('width', function (d) {
                    var groupWidth = 1;
                    var level = groupWidths.filter(function(element) { return element.Id == d.levelId; })[0];
                    if (level) {
                        var maxDigits;
						var group = level.Groups[d.columnIndex];
                        if (group.count) {
                            maxDigits = group.count;
                        } else {
                            maxDigits = group;
                        };
                        groupWidth = ((maxDigits * sizeConstants.digitWidth + sizeConstants.imageSize) - sizeConstants.highlightedGroupRectSpanX) - 1;
                    }
                    return groupWidth;
                })
                .attr("height", function () { return statusItemHeight; });
        }
    };

    var renderStatusSummary = function (level, settings) {
        var vars = beVariables.get(settings.ResourceId);

        // summary group with image and count
        var summaryGroup = level.select('.sw-be-summary');
        if (!settings.ShowStatusSummaryInternal) {
            summaryGroup.attr('class', 'sw-be-object hidden');
            return;
        }
        if (summaryGroup.empty()) {
            summaryGroup = level.append('g')
                .attr('class', 'sw-be-summary');
        }
        summaryGroup.attr('transform', 'translate(' + (settings.ResourceWidth - sizeConstants.overallStatusColumnWidth) + ',0)');

        // right splitter
        var rightSplitter = summaryGroup.select('rect');
        if (rightSplitter.empty()) {
            rightSplitter = summaryGroup.append('rect')
                .attr('fill', 'url(#rightShadow)')
                .attr('x', 0)
                .attr('y', 0)
                .attr('width', 4);
        }

        rightSplitter.attr('height', function (d, i) { return vars.lineHeights[i]; });

        var dataFunc = function (d) {
            var statuses = d.SummaryStatuses.length ? d.SummaryStatuses : settings.DefaultSummaryStatuses;
            return statuses.map(function (v) {
                var objects = d.Objects.filter(function (e) { return e.Status == v; });
                return { status: v, count: objects.length };
            });
        };

        var positionFunc = function (d, i) {
            var x = sizeConstants.shadowWidth
                    + sizeConstants.linePadding
                    + (i % sizeConstants.summaryStatusesPerLine) * (sizeConstants.statusSummaryShift);
            var y = sizeConstants.linePadding
                + Math.floor(i / sizeConstants.summaryStatusesPerLine) * (sizeConstants.statusImageSize + sizeConstants.multiLinePaddingSummary);

            return { x: x, y: y };
        };

        renderStatusSummaryStatuses(summaryGroup, dataFunc, positionFunc, settings);
    };

    var renderConnections = function (lineLayer, objectPositions, connections) {
        // generate mesh
        var items = lineLayer.selectAll('.sw-be-connection')
            .data(connections.filter(function (c) { return objectPositions[c.Source] !== undefined && objectPositions[c.Target] !== undefined; }), function (d) { return d.Source + '#' + d.Target; });

        items.enter().append('line')
            .attr('class', 'sw-be-connection');
        items
            .attr('data-source-id', function (d) { return d.Source; })
            .attr('data-target-id', function (d) { return d.Target; })
            .attr('x1', function (d) { return objectPositions[d.Source].x + sizeConstants.imageSize / 2 + 0.5; })
            .attr('y1', function (d) { return objectPositions[d.Source].y + sizeConstants.imageSize / 2; })
            .attr('x2', function (d) { return objectPositions[d.Target].x + sizeConstants.imageSize / 2 + 0.5; })
            .attr('y2', function (d) { return objectPositions[d.Target].y + sizeConstants.imageSize / 2; });
        items.exit().remove();
    };

    var render = function (data, resourceId) {
        var startTime = Date.now();

        var vars = beVariables.get(resourceId);
        vars.rendering = true;
        vars.objectPositions = {};

        var svgProperties = initProperties(data, resourceId);
        var settings = vars.settings;

        var contentDiv = $('#Content-' + resourceId);
        var noObjectsDiv = $('#NoObjects-' + resourceId);
        var noProblematicObjectsDiv = $('#NoProblematicObjects-' + resourceId);

        if (svgProperties.noObjects || svgProperties.noProblematicObjects) {
            noObjectsDiv[svgProperties.noObjects ? 'show' : 'hide']();
            noProblematicObjectsDiv[svgProperties.noProblematicObjects ? 'show' : 'hide']();
            contentDiv.hide();
            return;
        } else {
            noObjectsDiv.hide();
            noProblematicObjectsDiv.hide();
            contentDiv.show();
        }

        settings.ResourceWidth = Math.max(svgProperties.resourceMinWidth, settings.ResourceWidth);
        beVariables.update(resourceId, settings);

        var svg = prepareSvg(resourceId, settings, svgProperties);

        addDefinitions(svg);

        renderZebraStripes(svg, data, settings);

        var lineLayer = createLineLayer(svg, settings);

        var level = createLevelLayersWithDescription(svg, data, settings);

        renderObjects(level, settings, svgProperties, function (d, i) {
            var positionObject = calculateItemPosition(i, svgProperties);

            var shift = vars.levelShifts[d.Id.split(':')[0]] || 0;
            vars.objectPositions[d.Id] = {
                x: positionObject.x + sizeConstants.levelNameColumnWidth + sizeConstants.linePadding,
                y: positionObject.y + sizeConstants.linePadding + shift
            };

            return positionObject;
        });

        renderGroups(level, settings, svgProperties, function (child, pos) {
            var shift = vars.levelShifts[child.split(':')[0]] || 0;
            vars.objectPositions[child] = {
                x: pos.x + sizeConstants.levelNameColumnWidth + sizeConstants.linePadding,
                y: pos.y + sizeConstants.linePadding + shift
            };
        });

        renderConnections(lineLayer, vars.objectPositions, data.Connections);

        renderStatusSummary(level, settings);

        vars.rendering = false;
        startTime = Date.now() - startTime;
        console.log("Rendering took " + startTime + "ms");

        if (vars.lastHighlighted) {
            applyHighlighting(vars.lastHighlighted, resourceId);
        }
    }

    var applyHighlighting = function (highlighted, resourceId) {
        var startTime = Date.now();
        var container = beHelpers.getContentContainer(resourceId);

        var dots = container.find('.sw-be-object:not(.hidden)');
        var verticalGroups = container.find('.sw-be-group-vertical .sw-be-summary-group');
        var ultimateGroups = container.find('.sw-be-group-ultimate .sw-be-summary-group');

        if (highlighted !== null) {
            container.addClass('sw-be-objects-highlighted');
            updateHighlightingObjects(highlighted, dots, 'sw-be-object', 'data-id');
            updateHighlightingGroups(highlighted, verticalGroups);
            updateHighlightingGroups(highlighted, ultimateGroups);
        } else {
            container.removeClass('sw-be-objects-highlighted');
            removeHighlighting(dots);
            removeHighlighting(verticalGroups);
            removeHighlighting(ultimateGroups);
        }

        beVariables.get(resourceId).lastHighlighted = highlighted;
        console.log("Highlighting took " + (Date.now() - startTime) + "ms");
    };

    var updateHighlightingObjects = function (highlighted, collection, baseClass, dataAttribute) {
        var highlightedClass = baseClass + ' sw-be-highlighted';
        var notHighlightedClass = baseClass + ' sw-be-not-highlighted';

        collection.each(function () {
            var self = $(this);
            var id = self.attr(dataAttribute);

            self.attr('class', highlighted.indexOf(id) != -1 ? highlightedClass : notHighlightedClass);
        });
    }

    var updateHighlightingGroups = function (highlighted, collection) {
        var highlightedClass = 'sw-be-highlighted';
        var notHighlightedClass = 'sw-be-not-highlighted';

        collection.each(function () {
            var self = $(this);
            var members = self.attr('data-members')
            if (!members) {
                return;
            }

            members = members.split(',');
            var containsSearchedItem = false;
            for (var i = 0; i < highlighted.length; i++) {
                if (members.indexOf(highlighted[i]) != -1) {
                    containsSearchedItem = true;
                    break;
                }
            }

            var originalClass = self.attr('class').replace(highlightedClass, '').replace(notHighlightedClass, '');
            self.attr('class', originalClass + ' ' + (containsSearchedItem ? highlightedClass : notHighlightedClass));
        });
    }

    var removeHighlighting = function (element) {
        var classes = element.attr('class');
        if (classes) {
            classes = classes.replace(/sw\-be\-highlighted/g, '');
            classes = classes.replace(/sw\-be\-not\-highlighted/g, '');
        }
        element.attr('class', classes);
    };

    SW.Core.namespace("SW.F5.BalancingEnvironment").Renderers = {
        render: render,
        applyHighlighting: applyHighlighting
    };
})(
    SW.F5.BalancingEnvironment.Static,
    SW.F5.BalancingEnvironment.Helpers,
    SW.F5.BalancingEnvironment.Variables
);