﻿(function (beStatic, beVariables) {
    var sizeConstants = beStatic.sizeConstants;
    var tooltipDivs = {};

    var init = function (resourceId, beHandlers) {
        var vars = beVariables.get(resourceId);

        var tooltipDiv = $('#Content-' + resourceId + ' .sw-be-serviceTools').first();        
        tooltipDivs[resourceId] = tooltipDiv;
        tooltipDiv.appendTo($('body'));

        tooltipDiv.find('.sw-be-serviceTools-connections').click(function() {
            var id = tooltipDiv.data('id');
            if (!id)
                return;

            beHandlers.showConnections(resourceId, id);
            tooltipDiv.addClass('hidden');
        });

        tooltipDiv.find('.sw-be-serviceTools-cancel-connections').click(function() {
            beHandlers.showConnections(resourceId, null);
            tooltipDiv.addClass('hidden');
        });

        tooltipDiv.find('.sw-be-serviceTools-details').click(function () {
            tooltipDiv.addClass('hidden');
        });

        tooltipDiv.click(function(e){
            e.stopImmediatePropagation();
        });

        vars.tooltipDiv = tooltipDiv;
    };

    var show = function(resourceId, objectId, title, objectPosition, shownMenuItems) {
        var tooltipDiv = tooltipDivs[resourceId];
        var body = $('body');
        var vars = beVariables.get(resourceId);
        var settings = vars.settings;

        tooltipDiv.find('.sw-be-serviceTools-title')
            .text(title);

        var detailsMenuItem = tooltipDiv.find('.sw-be-serviceTools-details');
        detailsMenuItem[shownMenuItems.detailsPage ? 'removeClass' : 'addClass']('hidden');
        detailsMenuItem.attr('href', shownMenuItems.detailsUrl || "");

        var connectionsMenuItem = tooltipDiv.find('.sw-be-serviceTools-connections');
        connectionsMenuItem[settings.PreselectedObject || !shownMenuItems.showConnections ? 'addClass' : 'removeClass']('hidden');

        var cancelConnectionsMenuItem = tooltipDiv.find('.sw-be-serviceTools-cancel-connections');
        cancelConnectionsMenuItem[settings.PreselectedObject || !shownMenuItems.cancelConnections ? 'addClass' : 'removeClass']('hidden');

        var addAsMonitoredMenuItem = tooltipDiv.find('.sw-be-serviceTools-add-monitored');
        addAsMonitoredMenuItem[!shownMenuItems.addAsMonitored ? 'addClass' : 'removeClass']('hidden');
        addAsMonitoredMenuItem.attr('href', shownMenuItems.addAsMonitoredUrl || "");

        objectId ? tooltipDiv.data('id', objectId) : tooltipDiv.removeData('id');

        setupPosition(objectPosition, tooltipDiv);

        tooltipDiv.removeClass('hidden');

        body.one('click', function(){
            tooltipDiv.addClass('hidden');
        });
    };

    var setupPosition = function(objectPosition, tooltipDiv) {
        var $window = $(window);

        var vertical = objectPosition.top + sizeConstants.imageSize + sizeConstants.tooltipMenuVerticalShift;
        var overflowsBottom = vertical - $window.scrollTop() + tooltipDiv.outerHeight() >= $window.height();
        if (overflowsBottom) {
            vertical = objectPosition.top - tooltipDiv.outerHeight() - sizeConstants.tooltipMenuVerticalShift;
            tooltipDiv.addClass('sw-be-serviceTools-isAbove');
        } else {
            tooltipDiv.removeClass('sw-be-serviceTools-isAbove');
        }

        var horizontal = objectPosition.left + sizeConstants.tooltipMenuHorizontalShift;
        var overflowsRight = horizontal - $window.scrollLeft() + tooltipDiv.outerWidth() >= $window.width();
        if (overflowsRight) {
            horizontal = objectPosition.left + sizeConstants.imageSize - tooltipDiv.outerWidth() - sizeConstants.tooltipMenuHorizontalShift;
            tooltipDiv.addClass('sw-be-serviceTools-isLeft');
        } else {
            tooltipDiv.removeClass('sw-be-serviceTools-isLeft');
        }

        tooltipDiv.css({ top: vertical, left: horizontal });
    };

    SW.Core.namespace("SW.F5.BalancingEnvironment").Tooltip = {
        init: init,
        show: show
};
})(
    SW.F5.BalancingEnvironment.Static,
    SW.F5.BalancingEnvironment.Variables
);