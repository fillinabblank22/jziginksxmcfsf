﻿(function (beStatic, beHelpers, beVariables, beTooltip, beGroupDialog, beRenderers, beHandlers) {
    var defaultSettings = beStatic.defaultSettings;

    var updateWidth = beHelpers.updateWidth;
    var reload = beHandlers.reload;

    var render = beRenderers.render;

    var initMain = beHandlers.initMain;
    var initContent = beHandlers.initContent;
    
    SW.Core.namespace("SW.F5").BalancingEnvironment = {
        Setup: function (route, actions, customSettings, strings) {
            var settings = $.extend({}, defaultSettings);

            if (customSettings) {
                customSettings = JSON.parse(customSettings);
                settings = $.extend(settings, customSettings);
            }

            if (strings) {
                settings.Strings = strings;
            }

            if (settings.ResourceId == 0) {
                return;
            }

            var resId = settings.ResourceId;
            var container = $('#Content-' + resId);

            var vars = beVariables.create(resId, settings);
            vars.route = route;
            vars.actions = actions;
            vars.searchBox = beHandlers.setupSearch(resId);

            beTooltip.init(resId, beHandlers);
            beGroupDialog.init(resId, container);

            initMain(resId);
            updateWidth(resId);

            var renderAndInit = function (data, resourceId) {
                render(data, resourceId);
                initContent(resourceId);
            };

            container.closest('.ResourceWrapper').bind('resized', function () {
                var vars = beVariables.get(resId);
                if (vars.rendering)
                    return;

                updateWidth(resId);
                renderAndInit(vars.lastData, resId);
            });

            SW.Core.View.AddOnRefresh(function() {
                reload(resId);
            }, "Content-" + resId);
            reload(resId);
            beHelpers.clearSearchBox();
        }
    };
})(
    SW.F5.BalancingEnvironment.Static,
    SW.F5.BalancingEnvironment.Helpers,
    SW.F5.BalancingEnvironment.Variables,
    SW.F5.BalancingEnvironment.Tooltip,
    SW.F5.BalancingEnvironment.GroupDialog,
    SW.F5.BalancingEnvironment.Renderers,
    SW.F5.BalancingEnvironment.Handlers
);