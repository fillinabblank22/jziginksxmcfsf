﻿(function () {
    var sizeConstants = {
        linePadding: 15,
        contentPadding: 15,
        multiLinePadding: 7,
        levelNameColumnWidth: 230,
        overallStatusColumnWidth: 184, // 2*linePadding + summaryStatusesPerLine*statusSummaryShift + shadowWidth
        lineHeight: 24,
        fontSize: 10,
        imgTextSpan: 2,
        imageSize: 24,
        statusImageSize: 16,
        statusSummaryShift: 50,
        objectCaptionWidth: 100,
        selectedRectSpan: 6,
        highlightedRectSpan: 3.5,
        highlightedGroupRectSpanX: 2,
        highlightedGroupRectSpanY: 2,
        maxObjectCaptionWidth: 16, // for objectCaptionWidth: 100 !!
        shadowWidth: 4,
        summaryStatusesPerLine: 3,
        multiLinePaddingSummary: 7,
        minimalObjectsPerLine: 5,
        tooltipMenuVerticalShift: -5,
        tooltipMenuHorizontalShift: -10,
        maximalObjectsPerLine: 10,
        verticalGroupBoxPadding: 3,
        verticalGroupBoxTextWidth: 12,
        dialogPageItemCount: 5,
        paginationPagesFromStart: 1,
        paginationPagesFromEnd: 1,
        paginationPagesBeforeAfterCurrent: 2,
        haGroupLeftShift: -3,
        haGroupTopShift: -3,
        haGroupWidthAdjustment: -8,
        haGroupHeightAdjustment: 0,
        digitWidth: 8 // used for calculation of position and width of groups
    };

    var defaultSettings = {
        ParentId: 0,
        ResourceId: 0,
        ResourceWidth: 0,
        ShowStatusSummary: true,
        ShowObjectCaptions: false,
        ShowOnlyProblems: false,
        SearchAsYouType: true,
        SelectedObject: '',
        SelectedObjectCaption: '',
        ObjectsMarkedSelected: [],
        DefaultSummaryStatuses: [2, 3, 9, 1], // down, warning, unmanaged, up
        F5StatusIconPath: '/Orion/StatusIcon.ashx?&entity=Orion.F5&status={0}&size=small{1}',
        ObjectDetailsUrl: '/Orion/View.aspx?NetObject={0}',
        NotManagedObjectTooltipUrl: '/Orion/F5/Controls/Tooltips/F5NotManagedObject.aspx?caption={0}&netObject={1}&ipaddress={2}',
        AddNodeUrl: '/Orion/Nodes/Add/Default.aspx?&restart=false&ipAddress={0}',
        AllowedCharacterRange: { min: 32, max: 127 },
        Strings: {}
    };

    
    SW.Core.namespace("SW.F5.BalancingEnvironment").Static = {
        sizeConstants: sizeConstants,
        defaultSettings: defaultSettings
    };
})();