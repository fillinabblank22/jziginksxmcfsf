(function (beStatic, beVariables, beHelpers, searchBoxFactory) {
    var sizeConstants = beStatic.sizeConstants;
    var groupDialogs = {};
    var paginationDots = -1;

    var init = function (resourceId, container) {
        var vars = beVariables.get(resourceId);

        var groupDialogDiv = container.find('.sw-be-groupDialog');
        groupDialogDiv.appendTo($('body'));

        var dialog = groupDialogDiv.dialog({
            autoOpen: false,
            width: 400,
            minHeight: 180,
            modal: true,
            resizable: false
        });

        groupDialogs[resourceId] = {
            element: groupDialogDiv,
            searchBox: null,
            dialog: dialog,
            settings: vars.settings,
            objects: []
        };
    };

    var getElement = function(resourceId) {
        return groupDialogs[resourceId].element;
    };

    var show = function(resourceId, title, objects, searchBox) {
        var groupDialog = groupDialogs[resourceId];
        var page = 1;

        groupDialog.objects = objects;
        groupDialog.searchBox = searchBox;

        updateContent(resourceId, page, objects);

        groupDialog.element.find('.ui-dialog-title').html(title);
        groupDialog.dialog.dialog('option', 'title', title);
        groupDialog.dialog.dialog('open');
    };

    var filterObjects = function(resourceId, filterCallback) {
        var page = 1;
        var objects = groupDialogs[resourceId].objects;
        updateContent(resourceId, page, objects.filter(filterCallback));
    };

    var updateContent = function(resourceId, page, objects) {
        var groupDialog = groupDialogs[resourceId];
        var content = groupDialog.element.find('ol');

        updateItems(resourceId, content, objects, page, groupDialog.settings);
        updatePagination(resourceId, sizeConstants, page, objects.length, function(page){
            updateContent(resourceId, page, objects);
        });
    };

    var updateItems = function (resourceId, content, objects, page, settings) {
        var from = (page - 1) * sizeConstants.dialogPageItemCount;
        var to = Math.min(page * sizeConstants.dialogPageItemCount, objects.length);
        var remaining = objects.length < sizeConstants.dialogPageItemCount ? 0 : sizeConstants.dialogPageItemCount - (to - from);
        
        var itemContent = "";
        for(var i = from; i < to; i++) {
            itemContent += formatItem(resourceId, objects[i], settings);
        }
        for(var i = 0; i < remaining; i++) {
            itemContent += getEmptyItem();
        }
        content.html(itemContent);
    };

    var formatItem = function(resourceId, object, settings) {
        var statusIcon = beHelpers.statusIcons(object.Status, settings).iconPath;
        var urlLink = beHelpers.getObjectDetailsUrl(resourceId, object.Id);
        var title = object.Caption;

        return String.format(
            '<li class="sw-be-groupDialog-item"><img src="{0}"><a href="{1}">{2}</a></li>',
            statusIcon,
            urlLink,
            title
        );
    };

    var getEmptyItem = function() {
        return '<li class="sw-be-groupDialog-item"></li>';
    };

    var updatePagination = function(resourceId, sizeConstants, page, total, refresh) {
        var pageSize = sizeConstants.dialogPageItemCount;
        var element = groupDialogs[resourceId].element;
        var minItem = (page - 1) * pageSize + 1;
        var maxItem = Math.min(page * pageSize, total);
        var totalPages = Math.ceil(total / pageSize);

        var content = "";
        if (total > pageSize) {
            content += "<a class='arrow page-previous{0}'></a>";
            getClickablePageNumbers(page, totalPages, sizeConstants).forEach(function(item){
                if( item === paginationDots ) {
                    content += "<span class='pagination-dots'>...</span>";
                } else if (item === page) {
                    content += "<span class='page-specific-current'>" + item + "</span>";
                } else {
                    content += "<a href='#' class='page-specific' data-page='" + item + "'>" + item + "</a>";
                }
            });
            content += "<a class='arrow page-next{1}'></a>";
            content = String.format(content, page > 1 ? '' : '-disabled', maxItem < total ? '' : '-disabled');
        }

        element.find('.pagination-content').html(content);
        element.find('.page-specific').click(function(){
            refresh($(this).data('page'));
        });
        element.find('.page-previous').click(function(){
            refresh(page - 1);
        });
        element.find('.page-next').click(function(){
            refresh(page + 1);
        });

        var displayingItemsText = element.find('.pagination-displaying-text');
        displayingItemsText.html(
            String.format(displayingItemsText.data('format'), total ? minItem : 0, maxItem, total)
        );
    };

    var getClickablePageNumbers = function(currentPage, total, sizeConstants) {
        var pages = [];
        for(var i = 0; i < sizeConstants.paginationPagesFromStart; i++) {
            pages.push(i + 1);
        }
        var currentRadiusMin = Math.max(currentPage - sizeConstants.paginationPagesBeforeAfterCurrent, sizeConstants.paginationPagesFromStart + 1);
        var currentRadiusMax = Math.min(currentPage + sizeConstants.paginationPagesBeforeAfterCurrent, total - sizeConstants.paginationPagesFromEnd + 1);
        if( currentRadiusMin > sizeConstants.paginationPagesFromStart + 1 ) {
            pages.push(paginationDots);
        }
        for(var p = currentRadiusMin; p <= currentRadiusMax; p++) {
            pages.push(p);
        }
        var maxValue = pages[pages.length - 1];
        if( currentRadiusMax < total - sizeConstants.paginationPagesFromEnd ) {
            pages.push(paginationDots);
        }

        var endFrom = Math.max(maxValue + 1, total - sizeConstants.paginationPagesFromEnd + 1);
        for(var i = endFrom; i <= total; i++) {
            pages.push(i);
        }
        return pages;
    };

    SW.Core.namespace("SW.F5.BalancingEnvironment").GroupDialog = {
        getElement: getElement,
        filterObjects: filterObjects,
        init: init,
        show: show,
    };
})(
    SW.F5.BalancingEnvironment.Static,
    SW.F5.BalancingEnvironment.Variables,
    SW.F5.BalancingEnvironment.Helpers,
    SW.F5.SearchBox
);