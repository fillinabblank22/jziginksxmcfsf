﻿(function (beHelpers, beRenderers, beVariables, beTooltip, beGroupDialog, searchBoxFactory) {
    var initMain = function (resourceId) {
        var vars = beVariables.get(resourceId);
        
        vars.settings.SelectedObject = beHelpers.getInitiallySelectedObject(resourceId);

        var container = beHelpers.getContentContainer(resourceId);

        beHelpers.generateCharacterWidthMap(resourceId);

        $('.sw-be-unselectObject', container).unbind('click').on('click', function () {
            beHelpers.clearSearchBox(resourceId);
            showConnections(resourceId, null);
        });

        $('#ShowObjects-' + resourceId).off('change').change(function() {
            var selectedOption = $(this).val();
            var showOnlyProblems = selectedOption === 'onlyProblems';

            var newSettings = beVariables.update(resourceId, { ShowOnlyProblems: showOnlyProblems });

            SW.Core.Services.callControllerAction(vars.route, vars.actions.SaveUserSettings, newSettings, function () { });

            reload(resourceId);
        });
    };

    var setupSearch = function(resourceId) {
        var vars = beVariables.get(resourceId);
        var container = beHelpers.getContentContainer(resourceId);
        var element = $('.sw-be-search', container);
        var settings = vars.settings;

        var errorMessageElement = container.find('.sw-be-mainSearchErrorMessage');

        var searchBoxInstance = searchBoxFactory.createInstance(element, {
            activeClass: 'sw-be-highlighting-active',
            submitAutomatically: settings.SearchAsYouType,
            onBeforeSearch: function() {
                if (vars.lastHighlighted) {
                    vars.lastHighlighted = null;
                }
            },
            onSearch: function (searchText, isValidSearch) {
                var dfd = $.Deferred();
                beHelpers.searchObjects(resourceId, searchText, function(results){
                    beHelpers.hideErrorMessage(resourceId, errorMessageElement);
                    if (isValidSearch()) {
                        beRenderers.applyHighlighting(results, resourceId);
                    }
                    dfd.resolve();
                }, function(errorMessage) {
                    beHelpers.displayErrorMessage(resourceId, errorMessage, errorMessageElement);
                    dfd.reject();
                });
                return dfd;
            },
            onStopSearch: function() {
                beRenderers.applyHighlighting(null, resourceId);
                beHelpers.hideErrorMessage(resourceId, errorMessageElement);
            }
        });

        return searchBoxInstance;
    };

    var initContent = function (resourceId) {
        var vars = beVariables.get(resourceId);
        var container = beHelpers.getContentContainer(resourceId);
        var settings = vars.settings;

        beHelpers.applyObjectSelection(resourceId, settings);
                
        $('.sw-be-objectIcon, .sw-be-objectCaption', container).unbind('click').click(function() {
            var self = $(this);
            var parent = self.parent();
            var id = parent.data('id');

            var selectedObject = settings.SelectedObject;
            var isMonitored = parent[0].getAttribute('class').indexOf('sw-be-notManagedObject') == -1;

            var shownMenuItems = {
                detailsPage: isMonitored && parent.data('hasDetails'),
                detailsUrl: isMonitored && beHelpers.getObjectDetailsUrl(resourceId, id),
                showConnections: isMonitored && selectedObject != id,
                cancelConnections: isMonitored && selectedObject == id,
                addAsMonitored: !isMonitored,
                addAsMonitoredUrl: !isMonitored && beHelpers.getAddNodeUrl(resourceId, id),
            };

            var position = parent.find('.sw-be-objectIcon').offset();
            var title = beHelpers.getElementTitle(resourceId, id);
            beTooltip.show(resourceId, id, title, position, shownMenuItems);

            parent.trigger('mouseout');

            return false;
        });

        $('.sw-be-object', container).each(function() {
            beHelpers.setupObjectTooltip(resourceId, this);
            $.swtooltip(this);
        });

        $('.sw-be-summary-fill-group', container).unbind('click').click(function() {
            var self = $(this);
            var status = self.data('status');
            var level = self.closest('.sw-be-level');
            var ids = self.parent().data('objects');
            if( !ids ) {
                return;
            }

            showGroupDialog(resourceId, level, status, ids, vars.searchBox);
        });
    };

    var showGroupDialog = function(resourceId, level, status, ids, searchBox) {
        var settings = beVariables.get(resourceId).settings;
        var levelTitle = level.data('label');

        var objects = beHelpers.getSummaryGroupObjects(resourceId, level.data('level'), function(object){
            return ids.indexOf(object.Id) != -1 && object.Status === status;
        });
        objects.sort(function(a, b){
            return a.Caption > b.Caption ? 1 : -1;
        });

        var searchBoxElement = beGroupDialog.getElement(resourceId).find('.sw-be-groupDialog-search');
        var groupSearchBox = searchBoxFactory.createInstance(searchBoxElement, {
            activeClass: 'sw-be-highlighting-active',
            boundWith: [searchBox],
            submitAutomatically: settings.SearchAsYouType,
            onSearch: function(searchText, isValidSearch) {
                searchGroupDialog(resourceId, searchText, isValidSearch);
            },
            onStopSearch: function() {
                searchGroupDialog(resourceId, '');
            }
        });

        beGroupDialog.show(resourceId, levelTitle, objects, groupSearchBox);
    };

    var searchGroupDialog = function(resourceId, searchText, isValidSearch) {
        beHelpers.searchObjects(resourceId, searchText, function(results){
            var dialogElement = beGroupDialog.getElement(resourceId);
            if (dialogElement) {
                beHelpers.hideErrorMessage(resourceId, dialogElement.find('.sw-be-groupSearchErrorMessage'));
            }

            if (isValidSearch && !isValidSearch()) {
                return;
            }

            beGroupDialog.filterObjects(resourceId, function(object){
                return !results || results.indexOf(object.Id) != -1;
            });
        }, function (errorMessage) {
            var dialogElement = beGroupDialog.getElement(resourceId);
            if (dialogElement) {
                beHelpers.displayErrorMessage(resourceId, errorMessage, dialogElement.find('.sw-be-groupSearchErrorMessage'));
            }
        });
    };

    var reload = function(resourceId) {
        beHelpers.refresh(resourceId, function (data, resId) {
            beRenderers.render(data, resId);
            initContent(resId);
        });
    };

    var showConnections = function (resourceId, elementId) {        
        beVariables.update(resourceId, { SelectedObject: elementId });
        beHelpers.rememberState(resourceId);

        beHelpers.clearSearchBox(resourceId);
        reload(resourceId);
    };

    var displayDetails = function (resourceId, elementId) {
        window.location.href = beHelpers.getObjectDetailsUrl(resourceId, elementId);
    };

    SW.Core.namespace("SW.F5.BalancingEnvironment").Handlers = {
        initMain: initMain,
        initContent: initContent,
        showConnections: showConnections,
        displayDetails: displayDetails,
        reload: reload,
        searchGroupDialog: searchGroupDialog,
        setupSearch: setupSearch
    };
})(
    SW.F5.BalancingEnvironment.Helpers,
    SW.F5.BalancingEnvironment.Renderers,
    SW.F5.BalancingEnvironment.Variables,
    SW.F5.BalancingEnvironment.Tooltip,
    SW.F5.BalancingEnvironment.GroupDialog,
    SW.F5.SearchBox
);