﻿SW.Core.namespace("SW.F5.LinkServerWithNode").init = function (options) {
    var settings = {
        resourceId: 0,
        serverId: null,
        dialogElementClassName: null,
        isDemo: 0,
        errorMessage : null,
        demoMessage : null
    };

    $.extend(settings, options);

    var baseElement = $("*[resourceid=" + settings.resourceId + "]");
    var $selectorDialog = $("." + settings.dialogElementClassName + ":first");
    var $detailsTableSelector = $("#DetailsTableContent-" + settings.resourceId);


    function errorHandler() {
        alert(settings.errorMessage);
    }

    function demoHandler() {
        alert(settings.demoMessage);
    }

    var showDialog = function (e) {
        e.preventDefault();

        // unwire any existing events in netobject picker dialog as it is shared instance
        $($selectorDialog).off('click', ".linkButton");
        $($selectorDialog).off('click', ".cancelButton");
        // wire events in netobject picker dialog
        $($selectorDialog).on('click', ".linkButton", createLink);
        $($selectorDialog).on('click', ".cancelButton", closeDialog);


        SW.Orion.NetObjectPicker.SetFilter('');
        SW.Orion.NetObjectPicker.SetUpEntities(
          [{ MasterEntity: 'Orion.Nodes' }],
          'Orion.Nodes',
          []/*previous selection*/,
          'Single',
          true
        );

        $selectorDialog.dialog({
            position: 'center',
            width: 890,
            height: 'auto',
            modal: true,
            draggable: true,
            title: '@{R=F5.Strings;K=WEBJS_LH0_01;E=js}',
            show: { duration: 0 }
        });
    }

    var closeDialog = function (e) {
        e.preventDefault();
        $selectorDialog.dialog('close');
    }

    var removeLink = function (e) {
        e.preventDefault();

        if (settings.isDemo) {
            demoHandler();
            return;
        }

        SW.Core.Services.callControllerAction("/api/F5LTM", "RemoveLinkF5ServerToNode", { F5ServerId: settings.serverId }, function () {
            var o = $detailsTableSelector.data("sw-f5-genericTable");
            if (o) {
                o.refresh();
            }
        }, errorHandler)
    }

    var createLink = function (e) {
        e.preventDefault();

        if (settings.isDemo) {
            demoHandler();
            return;
        }

        var serverid = settings.serverId;

        var selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntities();
        if (selectedEntities.length > 0) {
            var x = selectedEntities[0];

            var start = x.indexOf('=');
            if ((start >= 0) && (start < x.length)) {
                {
                    var nodeId = x.substring(start + 1, x.length);
                    if (nodeId && serverid) {
                        SW.Core.Services.callControllerAction("/api/F5LTM", "CreateLinkF5ServerToNode", { F5ServerId: serverid, NodeId: nodeId }, function () {
                            $selectorDialog.dialog('close');
                            var o = $detailsTableSelector.data("sw-f5-genericTable");
                            if (o) {
                                o.refresh();
                            }
                        }, errorHandler)
                    }
                }
            }
        }
    }

    //wire events on resource
    $(baseElement).on('click', ".linknode", showDialog);
    $(baseElement).on('click', ".unlinknode", removeLink);
}