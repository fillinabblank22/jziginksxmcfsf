﻿(function () {
    var columnId = '_MemberID';
    var columnEnabled = 'Enabled';
    var columnEnabledDescription = '_EnabledDescription';
    var cache = {};

    var setSubmitButton = function (model, resourceId) {
        var submitButton = SW.F5.ListDialog.getElement(resourceId).find('[id$="btnSubmit"]');
        var submitText = '@{R=F5.Strings;K=WEBDATA_JT0_3;E=js}';
        var changesCount = Object.keys(model.changes).length;
        if (0 < changesCount) {
            submitText = submitText + " (" + changesCount + ")";
        }
        submitButton.text(submitText);
    };

    var showErrorMessage = function (resourceId, message) {
        SW.F5.ListDialog.getElement(resourceId)
            .find('.change-rotation-presence-error')
            .show()
            .find('.sw-pg-hint-text')
            .html(message);
    }

    var hideErrorMessage = function (resourceId) {
        SW.F5.ListDialog.getElement(resourceId)
            .find('.change-rotation-presence-error')
            .hide();
    }

    SW.Core.namespace("SW.F5").ChangeRotationPresence = {
        init: function (resourceId, settings) {
            if (cache[resourceId] !== undefined) {
                return;
            }
            var model = cache[resourceId] = {
                resourceId: resourceId,
                changes: {}
            };
            SW.F5.ListDialog.init(resourceId, settings);
            if (SW.F5.ListDialog.getElement(resourceId).find('[id$="btnSubmit"]').length > 0) {
                SW.F5.ListDialog.getElement(resourceId).on('click', '.change-rotation-presence-status', function () {

                    var enabled = SW.F5.ListDialog.getCellValue(resourceId, $(this), columnEnabled);
                    if (enabled === 3 || enabled === 0) {
                        return;
                    }

                    $(this).toggleClass('on').toggleClass('off');

                    var id = SW.F5.ListDialog.getCellValue(resourceId, $(this), columnId);
                    if (id != undefined) {
                        if (model.changes[id] == undefined) {
                            model.changes[id] = enabled === 1 ? 2 : 1;
                        } else {
                            delete model.changes[id];
                        }
                    }
                    SW.F5.ListDialog.setCellValue(resourceId, $(this), columnEnabled, enabled === 1 ? 2 : 1);

                    setSubmitButton(model, resourceId);
                    hideErrorMessage(resourceId);
                });
            }
        },
        showDialog: function (resourceId) {
            var model = cache[resourceId];
            model.changes = {};
            SW.F5.ListDialog.getElement(resourceId).find('[id$="tbDisableReason"]').val('');
            setSubmitButton(model, resourceId);
            hideErrorMessage(resourceId);
            SW.F5.ListDialog.show(resourceId);
        },
        closeDialog: function (resourceId) {
            SW.F5.ListDialog.close(resourceId);
        },
        submit: function (resourceId) {
            var model = cache[resourceId];

            var changelist = new Array();
            var disableReason = SW.F5.ListDialog.getElement(resourceId).find('[id$="tbDisableReason"]').val();

            for (var id in model.changes) {
                if (model.changes.hasOwnProperty(id)) {
                    changelist.push({ PoolMemberId: id, EnabledState: model.changes[id] });
                }
            }

            var request = { PoolMembers: changelist, DisableReason: disableReason };

            SW.F5.ListDialog.startLoading(resourceId, '@{R=F5.Strings;K=WEBDATA_JT0_4;E=js}');

            if (0 < changelist.length) {
                SW.Core.Services.callControllerAction('/api/F5LTM', 'ChangeEnabledState', request, function (data) {
                    SW.F5.ListDialog.endLoading(resourceId);
                    if (data.IsSuccessful === true) {
                        SW.F5.ListDialog.close(resourceId);
                    } else {
                        showErrorMessage(resourceId, data.ErrorMessage);
                    }
                }, function (message) {
                    SW.F5.ListDialog.endLoading(resourceId);
                    showErrorMessage(resourceId, message);
                });
            } else {
                SW.F5.ListDialog.endLoading(resourceId);
                SW.F5.ListDialog.close(resourceId);
            }
        }
    };
})();