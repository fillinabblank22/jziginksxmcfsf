﻿(function () {
    var defaultSettings = {
        dialogCssClass: '.sw-f5-list-dialog',
        dialogSettings: {
            title: '',
            width: 500,
            minHeight: 180,
            resizable: false
        },
        pageItemCount: 5,
        paginationDisplayingTextFormat: 'Displaying items <b>{0}</b>-<b>{1}</b> of {2}',
        columnSettings: {},
        defaultColumnSettings: {
            headerFormatter: function (column) {
                return column;
            },
            headerCssClassProvider: function(column) {
                return '';
            },
            cellFormatter: function(cell, row) {
                return cell;
            },
            cellCssClassProvider: function (cell, row) {
                return '';
            }
        },
        afterRowContent: function(row) {
            return '<';
        }
    }
    var listDialogs = {};

    var updateData = function (listDialog, currentPageIndex) {
        var settings = listDialog.settings;
        var columns = listDialog.data.Columns;
        var rows = listDialog.data.Rows;
        var table = listDialog.element.find('.sw-f5-list-dialog-table');

        var from = (currentPageIndex - 1) * settings.pageItemCount;

        var content = '';

        content += '<tr class="HeaderRow">';
        var columnIndex, columnValue, columnSettings;
        for (columnIndex = 0; columnIndex < columns.length; columnIndex++) {
            columnValue = columns[columnIndex];
            if (!columnValue.startsWith('_')) {
                columnSettings = settings.columnSettings[columnValue] = $.extend(true, {}, settings.defaultColumnSettings, settings.columnSettings[columnValue]);
                content += '<td class="ReportHeader ' + columnSettings.headerCssClassProvider(columnValue) + '" style="text-transform: uppercase;">';
                content += columnSettings.headerFormatter(columnValue);
                content += '</td>';
            }
        }

        content += '</tr>';

        for (var k = 0; k < settings.pageItemCount; k++) {
            var rowIndex = (from + k);
            var row = (rowIndex < rows.length) ? rows[rowIndex] : undefined;
            content += '<tr data-r="'+ rowIndex + '">';
            for (columnIndex = 0; columnIndex < columns.length; columnIndex++) {
                columnValue = columns[columnIndex];
                if (!columnValue.startsWith('_')) {
                    columnSettings = settings.columnSettings[columnValue];
                    if (row !== undefined) {
                        var cell = row[columnIndex];
                        content += '<td class="' + columnSettings.cellCssClassProvider(cell, row) + '">';
                        var cellContent = columnSettings.cellFormatter(cell, row);
                        var iconForIndex = $.inArray('_IconFor_' + columnValue, columns);
                        if (iconForIndex !== -1) {
                            content += '<img class="icon-for" src="' + row[iconForIndex] + '" />';
                        }
                        var linkForIndex = $.inArray('_LinkFor_' + columnValue, columns);
                        if (linkForIndex !== -1) {
                            content += '<a class="link-for" href="' + row[linkForIndex] + '">' + cellContent + '</a>';
                        } else {
                            content += '<span class="text">' + cellContent + '</span>';
                        }
                    } else {
                        content += '<td class="' + columnSettings.cellCssClassProvider(null, null) + '">';
                        content += '&nbsp;';
                    }

                    content += '</td>';
                }
            }
            content += '</tr>';
            if ($.isFunction(settings.afterRowContent)) {
                content += settings.afterRowContent(row);
            }
        }
        table.html(content);

        // set same height for all items
        var trs = table.find('[data-r]');
        var max = Math.max.apply(Math, trs.map(function () { return $(this).height(); }));
        trs.height(max);
    };

    var getPageNumber = function (currentPageIndex, i) {
        if (i === currentPageIndex) {
            return "<span class='page-specific-current'>" + i + "</span>";
        } else {
            return "<span class='page-specific' data-page='" + i + "'>" + i + "</span>";
        }
    };
    var getPageDots = function() {
        return "<span class='pagination-dots'>...</span>";
    }
    var generatePageNumbers = function(currentPageIndex, start, count) {
        var result = '';
        for (var i = start; i < start + count; i++) {
            result += getPageNumber(currentPageIndex, i);
        }
        return result;
    };
    var updatePagination = function (listDialog, currentPageIndex, total) {
        var element = listDialog.element;
        var pageItemCount = listDialog.settings.pageItemCount;
        var minItem = (currentPageIndex - 1) * pageItemCount + 1;
        var maxItem = Math.min(currentPageIndex * pageItemCount, total);
        var totalPages = Math.ceil(total / pageItemCount);

        var content = "";
        if (total > pageItemCount) {

            content += String.format('<span class="arrow page-previous {0}" data-page="{1}"></span>',
                (currentPageIndex === 1 ? 'page-disabled' : ''),
                currentPageIndex - 1);

            if (totalPages <= 5) {
                content += generatePageNumbers(currentPageIndex, 1, totalPages);
            }
            else if (currentPageIndex <= 3) {
                content += generatePageNumbers(currentPageIndex, 1, 4);
                content += getPageDots();
                content += generatePageNumbers(currentPageIndex, totalPages, 1);
            } else if (currentPageIndex >= totalPages - 2) {
                content += generatePageNumbers(currentPageIndex, 1, 1);
                content += getPageDots();
                content += generatePageNumbers(currentPageIndex, totalPages - 3, 4);
            } else {
                content += generatePageNumbers(currentPageIndex, 1, 1);
                content += getPageDots();
                content += generatePageNumbers(currentPageIndex, currentPageIndex - 1, 3);
                content += getPageDots();
                content += generatePageNumbers(currentPageIndex, totalPages, 1);
            }

            content += String.format('<span class="arrow page-next {0}" data-page="{1}"></a>',
                (currentPageIndex === totalPages ? 'page-disabled' : ''),
                (currentPageIndex + 1));
        }

        element.find('.pagination-content').html(content);
        
        element.find('.pagination-displaying-text').html(String.format(listDialog.settings.paginationDisplayingTextFormat, total ? minItem : 0, maxItem, total));
    };

    var updateContent = function (listDialog, currentPageIndex) {

        updateData(listDialog, currentPageIndex);

        updatePagination(listDialog, currentPageIndex, listDialog.data.Rows.length);
    };

    var showErrorMessage = function (listDialog, msg) {
        listDialog.element.find('.error-message').show().find('.text').text(msg);
    };
    var hideErrorMessage = function (listDialog) {
        listDialog.element.find('.error-message').hide();
    };

    var startLoading = function (listDialog, message) {
        hideErrorMessage(listDialog);
        var label = listDialog.loading.find('span');
        label.text(message);
        listDialog.loading.show();        
    };

    var endLoading = function (listDialog) {
        listDialog.loading.hide();
    };

    var getData = function (resourceId) {
        return listDialogs[resourceId].data;
    };
    var getRowIndex = function(resourceId, el) {
        var rowIndex = el.closest('[data-r]').data('r');
        return rowIndex < getData(resourceId).Rows.length ? rowIndex : undefined;
    };
    var cellValue = function(resourceId, el, columnName, value) {
        var rowIndex = getRowIndex(resourceId, el);
        if (rowIndex == undefined) {
            return undefined;
        }
        var data = getData(resourceId);
        var columnIndex = $.inArray(columnName, data.Columns);
        if (columnIndex === -1) {
            return undefined;
        }
        if (value === undefined) {
            return data.Rows[rowIndex][columnIndex];
        } else {
            data.Rows[rowIndex][columnIndex] = value;
        }
    };

    SW.Core.namespace("SW.F5").ListDialog = {
        init: function (resourceId, settings) {
            if (listDialogs[resourceId] !== undefined) {
                return;
            }
            settings = $.extend(true, {}, defaultSettings, settings);
            
            var listDialogDiv = $('[ResourceId=' + resourceId + ']').find(settings.dialogCssClass);
            listDialogDiv.appendTo($('body'));

            var dialog = listDialogDiv.dialog($.extend({
                autoOpen: false,
                modal: true
            }, settings.dialogSettings));

            var listDialog = listDialogs[resourceId] = {
                resourceId: resourceId,
                element: listDialogDiv,
                loading: listDialogDiv.find('#ListDialog-Loading-' + resourceId),
                submitting: listDialogDiv.find('#ListDialog-Submitting-' + resourceId),
                data: {
                    Columns: [],
                    Rows: []
                },
                settings: settings,
                open: function (options) {
                    dialog.dialog('option', options).dialog('open');
                },
                close: function () {
                    dialog.dialog('close');
                }
            };

            listDialogDiv.find('.pagination-content').on('click', '[data-page]:not(.page-disabled)', function () {
                updateContent(listDialog, $(this).data('page'));
            });
        },
        getElement: function (resourceId) {
            return listDialogs[resourceId].element;
        },
        getData: getData,
        getRowIndex: getRowIndex,
        getCellValue: cellValue,
        setCellValue: cellValue,
        refresh: function (resourceId) {
            var swql = $('#ListDialog-SWQL-' + resourceId).val();
            var listDialog = listDialogs[resourceId];
            var currentPageIndex = 1;

            startLoading(listDialog, '@{R=F5.Strings;K=WEBDATA_JT0_5;E=js}');
            Information.QueryWithPartialErrors(swql, function (result) {
                listDialog.data = result.Data;

                updateContent(listDialog, currentPageIndex);

                endLoading(listDialog);
            }, function (error) {
                showErrorMessage(listDialog, error.get_message());
                endLoading(listDialog);
            });
        },
        show: function (resourceId, settings) {
            var listDialog = listDialogs[resourceId];

            listDialog.settings = $.extend(true, {}, listDialog.settings, settings);

            listDialog.open(listDialog.settings.dialogSettings);

            SW.F5.ListDialog.refresh(resourceId);
        },
        close: function (resourceId) {
            var listDialog = listDialogs[resourceId];
            listDialog.close();
        },
        startLoading: function (resourceId, message) {
            var listDialog = listDialogs[resourceId];                
            startLoading(listDialog, message);
        },
        endLoading: function (resourceId) {
            var listDialog = listDialogs[resourceId];            
            endLoading(listDialog);
        }
    };    
})();