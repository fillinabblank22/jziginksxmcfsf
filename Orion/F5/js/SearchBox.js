(function(){
    var keyEnter = 13;
    var keyEscape = 27;
    var dontPropagate = false;
    var buttonClassActive = 'f5-searchbox-button-active';
    var buttonClassCancel = 'f5-searchbox-button-cancel';
    var buttonClassLoading = 'f5-searchbox-button-loading';

    var defaultSettings = {
        activeClass: 'f5-searchbox-highlighting-active',
        // other instances that will change value and trigger search along with this one
        boundWith: [],
        // executed before submitting search
        onBeforeSearch: null,
        // search handler
        onSearch: null,
        // when status of input changes and no search is done (e.g. is empty)
        onStopSearch: null,
        submitAutomatically: false,
        submitAutomaticallyMinLenght: 2,
        submitAutomaticallyDelay: 200,
        // show loading icon on AJAX request
        showLoading: true
    };

    var createInstance = function (element, settings) {
        var searchId = 0;
        var searchTimeoutInstance;
        var input = element.find('.f5-searchbox-input');
        var button = element.find('.f5-searchbox-button');
        settings = $.extend({}, defaultSettings, settings);

        // binding
        var bindWith = function(searchBox) {
            settings.boundWith.push(searchBox);

            setPlaceholder(searchBox.getPlaceholder(), dontPropagate);
            setValue(searchBox.getValue(), dontPropagate);

            if (!automaticSearchEnabled()) {
                search(searchBox.getValue(), true);
            }
        };

        var unbind = function(searchBox) {
            var index = settings.boundWith.indexOf(searchBox);
            if (index > -1) {
                settings.boundWith.splice(index, 1);
            }
        };

        var propagate = function(shouldPropagate, callback) {
            if( shouldPropagate !== dontPropagate ) {
                settings.boundWith.forEach(callback);
            }
        };


        // placeholders
        var getPlaceholder = function() {
            return input.attr('data-placeholder');
        };

        var setPlaceholder = function(text, shouldPropagate) {
            input.attr('customplaceholder', text);
            input.attr('placeholder', text);

            propagate(shouldPropagate, function(instance){
                instance.setPlaceholder(text, dontPropagate);
            });
        };


        // status
        var getValue = function() {
            return input.val();
        };

        var setValue = function(value, shouldPropagate) {
            input.val(value);

            valueChanged(shouldPropagate);
        };

        var valueChanged = function(shouldPropagate) {
            if (shouldSearchAutomatically()) {
                search(getValue(), false, dontPropagate);
            } else if (automaticSearchEnabled()) {
                stopSearch();
            }

            propagate(shouldPropagate, function(instance){
                instance.setValue(getValue(), dontPropagate);
            });
        };

        var isActive = function() {
            return !!element.data('search-active');
        }

        var setActive = function(isActive) {
            element.data('search-active', isActive);

            if( isActive ) {
                element.addClass(settings.activeClass);
                button.removeClass(buttonClassActive);
                button.addClass(buttonClassCancel);
            } else {
                element.removeClass(settings.activeClass);
                button.addClass(buttonClassActive);
                button.removeClass(buttonClassCancel);
            }
        };

        var clear = function() {
            clearTimeout(searchTimeoutInstance);
            setValue('');
            if (automaticSearchEnabled() === false) {
                search('', true);
            }
        };


        // search
        var isValidSearch = function(id) {
            return !id || id === searchId;
        };

        var search = function(searchText, triggerImmediately, shouldPropagate) {
            setActive(searchText.length > 0);

            if (typeof settings.onSearch === "function") {
                searchId++;
                var currentSearchId = searchId;

                var isValidCallback = function() {
                    return isValidSearch(currentSearchId);
                };
                
                var onSearch = function (value) {
                    if (settings.showLoading) {
                        button.addClass(buttonClassLoading);
                    }

                    var dfd = settings.onSearch(value, isValidCallback);
                    // Expecting jQuery.Deferred object
                    if (dfd == undefined || !$.isFunction(dfd.always)) {
                        dfd = $.Deferred().resolve();
                    }
                    dfd.always(function () {
                        button.removeClass(buttonClassLoading);
                    });
                }

                if (!triggerImmediately && automaticSearchEnabled()) {
                    clearTimeout(searchTimeoutInstance);
                    searchTimeoutInstance = setTimeout(function() {
                        var value = shouldSearchAutomatically() ? getValue() : '';
                        onSearch(value);
                    }, settings.submitAutomaticallyDelay);
                } else {
                    onSearch(searchText);
                }
            }

            propagate(shouldPropagate, function(instance){
                instance.search(searchText, triggerImmediately, dontPropagate);
            });
        };

        var stopSearch = function() {
            setActive(false);

            searchId++;
            if (typeof settings.onStopSearch === "function") {
                settings.onStopSearch();
            }
        };

        var automaticSearchEnabled = function() {
            return settings.submitAutomatically;
        };

        var shouldSearchAutomatically = function() {
            return settings.submitAutomatically && getValue().length >= settings.submitAutomaticallyMinLenght;
        };


        // interaction
        var previousValue;
        input.on('keydown', function (e) {
            previousValue = getValue();

            if (e.which == keyEscape && previousValue.length > 0) {
                e.preventDefault();
                e.stopImmediatePropagation();
                clear();
                return false;
            }

            // prevent submit seems to work only in keydown
            if (e.which === keyEnter) {
                e.preventDefault();
                e.stopPropagation();
            }
        });

        input.on('keyup', function (e) {
            // we have the actual value in keyup available
            var searchText = input.val();

            if (e.which == keyEscape) {
                return;
            }

            if (typeof settings.onBeforeSearch === "function") {
                settings.onBeforeSearch(searchText, input, element);
            }

            if (searchText != previousValue) {
                valueChanged();
            }

            if (e.which === keyEnter) {
                search(searchText, true);
            }

            return e.which != keyEnter;
        });

        button.click(function() {
            if (isActive()) {
                clear();
            } else {
                search(getValue(), true);
            }
        });

        // trigger initial bindings
        var boundWith = settings.boundWith;
        settings.boundWith = [];
        boundWith.forEach(function(otherSearchBox){
            bindWith(otherSearchBox);
        });

        return {
            bindWith: bindWith,
            getPlaceholder: getPlaceholder,
            getValue: getValue,
            search: search,
            setPlaceholder: setPlaceholder,
            setValue: setValue,
            unbind: unbind,
        }
    };

    SW.Core.namespace("SW.F5").SearchBox = {
        createInstance: createInstance
    };
}());
