﻿using System;
using System.Web;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Shared;
using SolarWinds.Orion.Web.InformationService;
using System.Collections.Generic;
using System.Text;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_F5_F5LTMDetails : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var ltm = NetObject as F5LTM;
        if (ltm == null)
            throw new IndexOutOfRangeException(string.Format(F5WebContent.F5WEBCODE_PS0_15, NetObject.GetType().Name, NetObject.NetObjectID));

        ((Orion_View)this.Master).HelpFragment = "orionphresourcef5ltmdetails";

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.LTMStatusIcon.ImageUrl = "~" + StatusIconProvider.Instance.GetImagePath("Orion.F5.System.Module", (int)ltm.LTMStatus, StatusIconSize.Small, "LTM");
        this.LTMStatusIcon.AlternateText = this.LTMStatusIcon.ToolTip = StatusInfo.GetStatus((int)ltm.LTMStatus).ShortDescription;

        ObsoleteDataMessageBar.NodeID = ltm.Node.NodeID;
    }
    
    public override string ViewType
    {
        get { return "F5LTMDetails"; }
    }
}