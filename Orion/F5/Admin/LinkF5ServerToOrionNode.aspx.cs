﻿using System;
using System.Globalization;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_F5_Admin_LinkF5ServerToOrionNode : Page
{
    private static readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        int f5ServerId;

        if (!int.TryParse(Request.QueryString["F5ServerId"], NumberStyles.None, CultureInfo.InvariantCulture, out f5ServerId))
        {
            log.DebugFormat("Attemp to link F5 Server to Orion node with no or invalid F5 Server ID specified ({0}), doing nothing.", Request.QueryString["F5ServerId"] ?? "n/a");

            const string loadBalancingOverview = @"/Orion/SummaryView.aspx?ViewKey=Load Balancing";

            Response.Redirect(loadBalancingOverview);
        }

        string returnUrl = string.Format("/Orion/View.aspx?NetObject=F5S:{0}", f5ServerId);

        string addNodeNetObjectId = AddNodeReturnPageHelper.NetObjectId;
        AddNodeReturnPageHelper.Leave(null);

        int nodeId;

        if (!int.TryParse(addNodeNetObjectId, NumberStyles.None, CultureInfo.InvariantCulture, out nodeId))
        {
            log.DebugFormat("Attempt to link F5 Server (ID:{0}) to invalid Orion Node (ID:{1}), doing nothing.", f5ServerId, addNodeNetObjectId ?? "n/a");

            Response.Redirect(returnUrl);
        }

        if (!Profile.AllowNodeManagement)
        {
            log.DebugFormat("Attempt to link F5 Server with Orion node without Node Management right allowed. Doing nothing.");

            Response.Redirect(returnUrl);
        }

        log.DebugFormat("Linking F5 Server (ID:{0}) to Orion Node (ID:{1}).", f5ServerId, nodeId);

        try
        {
            SWISVerbsHelper.InvokeV3("Orion.F5.LTM.Server", "LinkNode", f5ServerId, nodeId);

            log.DebugFormat("Successfully linked F5 Server (ID:{0}) to Orion Node (ID:{1}).", f5ServerId, nodeId);
        }
        catch (Exception exception)
        {
            log.Error("Error while linking F5 Server to Orion node.", exception);
        }

        Response.Redirect(returnUrl);
    }
}