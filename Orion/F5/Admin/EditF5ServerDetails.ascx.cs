﻿using SolarWinds.F5.Web.Controls;

public partial class Orion_F5_Admin_EditF5ServerDetails : EditF5ViewDetailsBase
{
    protected override string ViewName
    {
        get { return "F5ServerDetails"; }
    }
}