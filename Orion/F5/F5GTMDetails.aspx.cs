﻿using System;
using System.Web;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Shared;

public partial class Orion_F5_F5GTMDetails : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var gtm = NetObject as F5GTM;
        if (gtm == null)
            throw new IndexOutOfRangeException(string.Format(F5WebContent.F5WEBCODE_PS0_15, NetObject.GetType().Name, NetObject.NetObjectID));

        ((Orion_View)this.Master).HelpFragment = "orionphresourcef5gtmdetails";

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.GTMStatusIcon.ImageUrl = "~" + StatusIconProvider.Instance.GetImagePath("Orion.F5.System.Module", (int)gtm.GTMStatus, StatusIconSize.Small, "GTM");
        this.GTMStatusIcon.AlternateText = this.GTMStatusIcon.ToolTip = StatusInfo.GetStatus((int)gtm.GTMStatus).ShortDescription;

        ObsoleteDataMessageBar.NodeID = gtm.Node.NodeID;
    }

    public override string ViewType
    {
        get { return "F5GTMDetails"; }
    }
}