﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
    CodeFile="F5LTMPoolDetails.aspx.cs" Inherits="Orion_F5_F5LTMPoolDetails" Title="F5 LTM Pool Details" %>

<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="f5" Namespace="SolarWinds.F5.Web.UI" Assembly="SolarWinds.F5.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="ObsoleteF5DataBar" Src="~/Orion/F5/Controls/ObsoleteF5DataBar.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
	<h1>
	    <%= F5WebContent.F5WEBCODE_PS0_14 %>:
        <asp:Image ID="LTMPoolStatusIcon" runat="server" style="vertical-align: text-bottom"/>
        <%= PoolName %>
	</h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:ObsoleteF5DataBar runat="server" ID="ObsoleteDataMessageBar" />
    
    <f5:F5LTMPoolResourceHost runat="server" ID="ltmPoolHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</f5:F5LTMPoolResourceHost>
</asp:Content>
