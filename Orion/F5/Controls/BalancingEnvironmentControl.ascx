﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BalancingEnvironmentControl.ascx.cs" Inherits="Orion_F5_Controls_BalancingEnvironmentControl" %>
<%@ Register Src="~/Orion/F5/Controls/SearchBox.ascx" TagPrefix="f5" TagName="searchbox" %>

<orion:Include ID="Include3" runat="server" Module="F5" File="d3.min.js" Section="Top" />
<orion:Include ID="Include1" runat="server" Module="F5" File="BalancingEnvironment.js" Section="Top" />
<orion:Include ID="Include2" runat="server" Module="F5" File="BalancingEnvironment.css" Section="Top" />

<script type="text/javascript">
    $(function () {
        SW.F5.BalancingEnvironment.Setup('<%= Route %>', <%= ActionsSerialized %>, '<%= ConfigSerialized %>', {
            'placeholderSelectedObject': '<%# Resources.F5WebContent.F5WEBDATA_JP0_7%>'
        });
    });
</script>
        
<div id="Content-<%= ResourceID %>" class="sw-be-parent">
    <div class="sw-be-selected-object-box hidden ui-helper-clearfix">
        <div class="sw-be-selected-object-title"><%= Resources.F5WebContent.F5WEBDATA_JP0_5%></div>
        <div class="sw-be-selected-object-box-inner">
            <div class="sw-be-selected-object-clickable">
                <span class="sw-be-selectObjectTitle"></span>
                <span class="sw-be-unselectObject">&#x2715;</span>
            </div>
        </div>
    </div>
            
    <f5:searchbox runat="server" ClassNames="sw-be-search sw-be-search-style" Placeholder="<%# Resources.F5WebContent.F5WEBDATA_JP0_6%>" />

    <div class="sw-be-errorMessage sw-be-mainSearchErrorMessage sw-suggestion sw-suggestion-fail hidden">
        <span class="sw-suggestion-icon"></span>
        <div><%= Resources.F5WebContent.F5WEBDATA_JP0_19 %></div>
    </div>

    <div class="sw-be-loading hidden">
        <div>
            <div>
                <img src="/Orion/images/loading_gen_small.gif" />
                <%= Resources.CoreWebContent.WEBDATA_AK0_51%>
            </div>
        </div>
    </div>

    <svg id="BalancingEnvironment-<%= ResourceID %>" class="sw-be"/>
            
    <div id="BalancingEnvironment-serviceTools-<%= ResourceID %>" class="sw-be-serviceTools hidden">
        <div class="sw-be-serviceTools-inner">
            <div class="sw-be-serviceTools-arrow"></div>
            <table>
                <tr>
                    <th><span class="sw-be-serviceTools-title"><%= Resources.F5WebContent.F5WEBCODE_PS0_3 %></span></th>
                </tr>
                <tr>
                    <td>
                        <div class="sw-be-serviceTools-link">
                            <a href="#" class="sw-be-serviceTools-details NoTip">
                                <img align="top" src="/Orion/F5/images/BalancingEnvironment/Cmd-Details.png">
                                <%= Resources.F5WebContent.F5WEBCODE_PS0_4 %>
                            </a>
                            <div class="sw-be-serviceTools-connections">
                                <img align="top" src="/Orion/F5/images/BalancingEnvironment/Cmd-Connections.png">
                                <%= Resources.F5WebContent.F5WEBCODE_PS0_5 %>
                            </div>
                            <div class="sw-be-serviceTools-cancel-connections">
                                <img align="top" src="/Orion/F5/images/BalancingEnvironment/Cmd-ClearConnections.png">
                                <%= Resources.F5WebContent.F5WEBCODE_PS0_7 %>
                            </div>
                            <a href="#" class="sw-be-serviceTools-add-monitored hidden">
                                <img align="top" src="/Orion/F5/images/BalancingEnvironment/Cmd-Add.png">
                                <%= Resources.F5WebContent.F5WEBCODE_PS0_8 %>
                            </a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="sw-be-groupDialog hidden">
        <f5:searchbox runat="server" ClassNames="sw-be-groupDialog-search sw-be-search-style" />

        <div class="sw-be-errorMessage sw-be-groupSearchErrorMessage sw-suggestion sw-suggestion-fail hidden">
            <span class="sw-suggestion-icon"></span>
            <div><%= Resources.F5WebContent.F5WEBDATA_JP0_19 %></div>
        </div>

        <ol></ol>
        <div class="pagination">
            <span class="pagination-content"></span>
            <span class="pagination-displaying-text" data-format="<%= Resources.F5WebContent.F5WEBDATA_JP0_11%>"></span>
        </div>
    </div>
</div>
        
<div id="NoObjects-<%= ResourceID %>" class="sw-be-fullResourceMessage sw-be-noObjects hidden">
    <img src="/Orion/images/Icon.Info.gif" width="16" height="16" alt="OK"/>
    <span><%= Resources.F5WebContent.F5WEBDATA_JP0_9%></span>
</div>

<div id="NoProblematicObjects-<%= ResourceID %>" class="sw-be-fullResourceMessage sw-be-noObjects hidden">
    <img src="/Orion/images/Check.Green.gif" width="16" height="16" alt="OK"/>
    <span><%= Resources.F5WebContent.F5WEBCODE_PS0_9 %></span>
</div>

<div id="LoadingError-<%= ResourceID %>" class="sw-be-errorMessage sw-suggestion sw-suggestion-fail hidden">
    <span class="sw-suggestion-icon"></span>
    <div data-format="<%= Resources.F5WebContent.F5WEBDATA_JP0_18 %>"></div>
</div>
