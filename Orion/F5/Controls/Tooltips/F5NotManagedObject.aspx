﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="F5NotManagedObject.aspx.cs" Inherits="Orion_F5_Controls_Tooltips_F5NotManagedObject" %>

<div style="min-width: 270px; max-width: 500px">
	<h3 class="StatusUnknown" style="word-break: break-all; padding: 8px 14px;">
		<%= Caption%>
	</h3>
	<div class="NetObjectTipBody">
		<table cellpadding="0" cellspacing="0">
			 <tr>
				<th valign="top" width="80" style="padding: 5px 15px 5px 5px; vertical-align: top">
					<%= Resources.F5WebContent.F5WEBDATA_LH0_3 %>
				</th>
				<td style="padding: 5px 0; vertical-align: top" colspan="2">
					<img src="<%= RoleIcon%>" style="display: inline-block; margin-top: -3px; margin-bottom: -8px" align="top" />
					<%= Role%>
				</td>
			</tr>
            <tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.CoreWebContent.WEBDATA_AK0_47 %>
					</th>
				<td style="padding: 5px 0;">
					<b><%= LocalizedStatusText%></b>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.CoreWebContent.WEBDATA_VB0_13 %> 
				</th>
				<td style="padding: 5px 0;">
					<%= IpAddress %>
				</td>
			</tr>
            <tr>
				<td style="padding: 8px 12px 4px; text-align: center; vertical-align: top; border-top: 1px solid rgb(236,237,238)" colspan="2">
					<%= string.Format(Resources.F5WebContent.F5WEBDATA_JP0_27, Role)%>
				</td>
			</tr>
		</table>
	</div>
</div>
