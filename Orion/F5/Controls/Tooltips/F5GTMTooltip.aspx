﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="F5GTMTooltip.aspx.cs" Inherits="Orion_F5_Controls_Tooltips_F5GTMTooltip" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>

<div style="min-width: 270px; max-width: 500px">
	<h3 class="Status<%=Entity.GTMStatus.ToString()%>" style="word-break: break-all; padding: 8px 14px;">
		<%= Entity.Name%>
	</h3>
	<div class="NetObjectTipBody">
		<table cellpadding="0" cellspacing="0">
			 <tr>
				<th valign="top" width="80" style="padding: 5px 15px 5px 5px; vertical-align: top">
					<%= Resources.F5WebContent.F5WEBDATA_LH0_3 %>
				</th>
				<td style="padding: 5px 0; vertical-align: top" colspan="2">
					<img src="/Orion/F5/images/BalancingEnvironment/GTM.png" style="display: inline-block; margin-top: -3px; margin-bottom: -8px" align="top" />
					<%= Resources.F5WebContent.F5WEBDATA_JP0_15 %>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.CoreWebContent.WEBDATA_AK0_47 %>
					</th>
				<td style="padding: 5px 0; color: <%= Entity.GTMStatus == OrionObjectStatus.Down ? "#DA3838" : "#333" %>">
					<b><%= LocalizedStatusText%></b>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.F5WebContent.F5WEBDATA_LH0_10 %>
				</th>
				<td style="padding: 5px 0;">
					<%= GTMStatusReason %>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.CoreWebContent.WEBDATA_VB0_13 %> 
				</th>
				<td style="padding: 5px 0;">
					<%= Entity.IPAddress %>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<%= Resources.F5WebContent.F5WEBCODE_PS0_11 %>
				</th>
				<td style="padding: 5px 0; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&status=<%= (int)Entity.NodeStatus %>&size=small' align='top' width="16" height="16">
                    <%= Entity.Name %>
				</td>
			</tr>
            <% if (Entity.IsRedundant)  {%>
            <tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<%= Resources.F5WebContent.F5WEBCODE_PS0_13 %>
				</th>
				<td style="padding: 5px 0; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
                    <%= HAStatus %>
				</td>
			</tr>
            <% } %>
            <tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<%= Resources.F5WebContent.F5WEBDATA_JP0_26 %>
				</th>
				<td style="padding: 5px 0; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<%= Entity.RequestsPerSec.ToString("#,0") %>
				</td>
			</tr>
		</table>
	</div>
</div>
