﻿using System;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.F5.Web.TextProviders;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;

public partial class Orion_F5_Controls_Tooltips_F5GTMTooltip : System.Web.UI.Page
{
    protected F5GTM Entity { get; private set; }
    protected string LocalizedStatusText { get; private set; }
    protected string GTMStatusReason { get; private set; }
    protected string HAStatus { get; private set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Entity = NetObjectFactory.Create(Request["NetObject"], true) as F5GTM;
		if (Entity != null)
		{
		    var textFactory = TextFactory.Instance;

		    LocalizedStatusText = StatusInfo.GetStatus((int)Entity.GTMStatus).ShortDescription;
            GTMStatusReason = textFactory.GetText("Orion.F5.System.Module", (int)Entity.GTMStatus, "GTM.StatusReason");
            var failoverText = textFactory.GetText("Orion.F5.System.Module", Entity.FailoverStatus, "HAStatus");
            var syncText = textFactory.GetText("Orion.F5.System.Module", Entity.SyncStatus, "HASyncStatus");

		    HAStatus = string.Format(Resources.F5WebContent.F5WEBCODE_PS0_12, failoverText, syncText);
		}
    }
}