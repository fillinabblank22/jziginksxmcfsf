﻿using System;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;

public partial class Orion_F5_Controls_Tooltips_F5GTMWideIPTooltip : System.Web.UI.Page
{
    protected F5GTMWideIP Entity { get; private set; }
    protected string LocalizedStatusText { get; private set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Entity = NetObjectFactory.Create(Request["NetObject"], true) as F5GTMWideIP;
		if (Entity != null)
		{
		    LocalizedStatusText = StatusInfo.GetStatus((int)Entity.OrionStatus).ShortDescription;
		}
    }
}