﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="F5LTMPoolMemberTooltip.aspx.cs" Inherits="Orion_F5_Controls_Tooltips_F5LTMPoolMemberTooltip" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>

<div style="min-width: 270px; max-width: 500px">
	<h3 class="Status<%=Entity.OrionStatus.ToString()%>" style="word-break: break-all; padding: 8px 14px;">
		<%= Entity.ShortName%>
	</h3>
	<div class="NetObjectTipBody">
		<table cellpadding="0" cellspacing="0">
			 <tr>
				<th valign="top" width="80" style="padding: 5px 15px 5px 5px; vertical-align: top">
					<%= Resources.F5WebContent.F5WEBDATA_LH0_3 %>
				</th>
				<td style="padding: 5px 0; vertical-align: top" colspan="2">
					<img src="/Orion/F5/images/BalancingEnvironment/PoolMember.png" style="display: inline-block; margin-top: -3px; margin-bottom: -8px" align="top" />
					<%= Resources.F5WebContent.F5WEBDATA_JP0_16%>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.CoreWebContent.WEBDATA_AK0_47 %>
					</th>
				<td style="padding: 5px 0; color: <%= Entity.OrionStatus == OrionObjectStatus.Down ? "#DA3838" : "#333"%>">
					<b><%= LocalizedStatusText%></b>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.F5WebContent.F5WEBDATA_LH0_10%>
				</th>
				<td style="padding: 5px 0;">
					<%= StatusReason %>
				</td>
			</tr>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top;">
					<%= Resources.F5WebContent.F5WebCODE_JF0_003%>
				</th>
				<td style="padding: 5px 0;">
					:<%= Entity.Port%>
				</td>
			</tr>
            <% if (Entity.HostNode != null) { %>
            <tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<%= Resources.F5WebContent.F5WEBCODE_PS0_11 %>
				</th>
				<td style="padding: 5px 0; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&status=<%= (int) Entity.HostNode.Status.ParentStatus %>&size=small' align='top' width="16" height="16">
                    <%= Entity.HostNode.Name %>
				</td>
			</tr>
            <% } %>
			<tr>
				<th style="padding: 5px 15px 5px 5px; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<%= Resources.F5WebContent.F5WebCODE_JF0_005%>
				</th>
				<td style="padding: 5px 0; vertical-align: top; border-top: 1px solid rgb(236,237,238)">
					<%= Entity.Connections.ToString("#,0") %>
				</td>
			</tr>
			<tr runat="server" id="trHealthMonitors">
				<th style="padding: 5px 15px 5px 5px; vertical-align: middle;">
					<%= Resources.F5WebContent.F5WEDATA_EK0_18%>
				</th>
				<td style="padding: 5px 0; vertical-align: middle;">
					<%= MonitorsStatus %>
				</td>
			</tr>
		</table>
	</div>
</div>
