﻿using System;
using System.Collections.Generic;
using SolarWinds.F5.Web.IconProviders;
using SolarWinds.Shared;

public partial class Orion_F5_Controls_Tooltips_F5NotManagedObject: System.Web.UI.Page
{
    private static readonly Dictionary<string, string> NetObjectToRole = new Dictionary<string, string>
    {
        { "F5GTM", Resources.F5WebContent.F5WEBDATA_JP0_15 },
        { "F5LTM", Resources.F5WebContent.F5WEBCODE_PS0_10 }
    };

    protected string Caption { get; set; }
    protected string LocalizedStatusText { get; set; }
    protected string Role { get; set; }
    protected string RoleIcon { get; set; }
    protected string IpAddress { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["caption"]))
        {
            Caption = Request["caption"];
        }

        string role = null;
        if (!string.IsNullOrEmpty(Request.QueryString["netObject"]))
        {
            NetObjectToRole.TryGetValue(Request["netObject"], out role);
            RoleIcon = BasicDeviceIcons.GetPathByNetObject(Request["netObject"]);
        }
        Role = role ?? string.Empty;

        if (!string.IsNullOrEmpty(Request.QueryString["ipaddress"]))
        {
            IpAddress = Request["ipaddress"];
        }

        LocalizedStatusText = StatusInfo.GetStatus(0).ShortDescription;
    }
}