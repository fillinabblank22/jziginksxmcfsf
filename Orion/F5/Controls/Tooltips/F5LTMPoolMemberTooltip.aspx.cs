﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Resources;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;

public partial class Orion_F5_Controls_Tooltips_F5LTMPoolMemberTooltip : System.Web.UI.Page
{
    protected F5LTMPoolMember Entity { get; private set; }
    protected string LocalizedStatusText { get; private set; }
    protected string StatusReason { get; private set; }
    protected string MonitorsStatus { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Entity = NetObjectFactory.Create(Request["NetObject"], true) as F5LTMPoolMember;
        if (Entity != null)
        {
            LocalizedStatusText = StatusInfo.GetStatus((int)Entity.OrionStatus).ShortDescription;
            StatusReason = Entity.F5StatusReason;
            if (Entity.F5StatusReason.Length > 160)
            {
                StatusReason = StatusReason.Substring(0, 160) + "...";
            }

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var table = swis.Query(
                    @"SELECT
                        OrionStatus,
                        COUNT(MonitorID) as MonitorCount
                    FROM
                        Orion.F5.LTM.Monitor m
                    WHERE
                        m.F5Server.PoolMembers.MemberID = @PoolMemberId AND (m.F5Server.PoolMembers.Port = m.Port OR m.Port = 0)
                    GROUP BY
                        OrionStatus",
                    new Dictionary<string, object>
                {
                    {"PoolMemberId", Entity.Id}
                });
                if (table.Rows.Count == 0)
                {
                    this.MonitorsStatus = "";
                    trHealthMonitors.Visible = false;
                }
                else
                {
                    var sb = new StringBuilder();
                    foreach (DataRow row in table.Rows)
                    {
                        var orionStatus = row.Field<byte>("OrionStatus");
                        var orionStatusName = StatusInfo.GetStatus(orionStatus).ShortDescription;
                        var monitorCount = row.Field<int>("MonitorCount");
                        sb.AppendFormat("<span><img style='vertical-align: middle;' src='/Orion/StatusIcon.ashx?entity=Orion.F5.LTM.Monitor&status={0}&size=small' title='{1}' />&nbsp;<span>{2}</span></span>", orionStatus, orionStatusName, monitorCount);
                    }
                    MonitorsStatus = sb.ToString();
                    trHealthMonitors.Visible = true;
                }
            }
        }
    }
}