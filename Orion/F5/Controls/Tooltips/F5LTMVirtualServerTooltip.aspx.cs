﻿using System;
using System.Linq;
using System.Collections.Generic;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;

public partial class Orion_F5_Controls_Tooltips_F5LTMVirtualServerTooltip : System.Web.UI.Page
{
    protected F5VirtualServer Entity { get; private set; }
    protected string LocalizedStatusText { get; private set; }
    protected string StatusReason { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Entity = NetObjectFactory.Create(Request["NetObject"], true) as F5VirtualServer;
        if (Entity != null)
        {
            LocalizedStatusText = StatusInfo.GetStatus((int)Entity.OrionStatus).ShortDescription;
            StatusReason = Entity.F5StatusReason;
            if (Entity.F5StatusReason.Length > 160)
            {
                StatusReason = StatusReason.Substring(0, 160) + "...";
            }
        }
    }
}