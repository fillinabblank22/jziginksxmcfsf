﻿using System;
using System.Linq;
using System.Collections.Generic;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;

public partial class Orion_F5_Controls_Tooltips_F5LTMPoolTooltip : System.Web.UI.Page
{
    protected F5LTMPool Entity { get; private set; }
    protected string LocalizedStatusText { get; private set; }
    protected List<string> PoolMemberTexts { get; private set; }
    protected string StatusReason { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Entity = NetObjectFactory.Create(Request["NetObject"], true) as F5LTMPool;
        if (Entity != null)
        {
            LocalizedStatusText = StatusInfo.GetStatus((int)Entity.OrionStatus).ShortDescription;
            PoolMemberTexts = Entity.PoolMembers.Select(
                pm => string.Format("<img src='/Orion/StatusIcon.ashx?entity=Orion.F5&status={0}&size=small' align='top' width='16' height='16'> {1}", (byte)pm.Key, pm.Value)
            ).ToList();
            StatusReason = Entity.F5StatusReason;
            if (Entity.F5StatusReason.Length > 160)
            {
                StatusReason = StatusReason.Substring(0, 160) + "...";
            }
        }
    }
}