﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.F5.Web.ViewModels.SummaryStatus;

public partial class Orion_F5_Controls_EnvironmentStatus : UserControl
{
    public string NetObject { get; set; }

    private readonly IList<Tile> tiles = new List<Tile>();
    public IList<Tile> Tiles
    {
        get { return tiles;}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
