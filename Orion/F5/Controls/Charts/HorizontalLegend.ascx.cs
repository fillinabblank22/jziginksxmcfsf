﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_F5_Controls_Charts_HorizontalLegend : UserControl, IChartLegendControl
{
  
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string LegendInitializer
    { get { return "HorizontalLegendChartLegendInitializer__" + legend.ClientID; } }
    
}