﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HorizontalLegend.ascx.cs" Inherits="Orion_F5_Controls_Charts_HorizontalLegend" %>
<orion:Include ID="Include1" runat="server" Module="F5" File="HorizontalLegend.js" Section="Bottom"/>
<orion:Include ID="Include2" runat="server" Module="F5" File="HorizontalLegend.css" Section="Top"/>

<script type="text/javascript">
    SW.Core.Charts.Legend.HorizontalLegendChartLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.F5.Charts.HorizontalLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
</script>

<div runat="server" id="legend" class="chartLegend" ></div>
<div class="SWChartLogo">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>