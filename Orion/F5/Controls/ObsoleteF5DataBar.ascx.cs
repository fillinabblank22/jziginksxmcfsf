﻿using SolarWinds.F5.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_F5_Controls_ObsoleteF5DataBar : System.Web.UI.UserControl
{
    public int NodeID { get; set; }

    private bool IsPollerEnabled(int nodeID)
    {
        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            var f5Dal = new F5LTMDalService(swisProxy);
            return f5Dal.IsF5PollerEnabled(nodeID);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool isPollerEnabled = IsPollerEnabled(NodeID);

        if (isPollerEnabled)
        {
            Visible = false;
            return;
        }

        PolledDisabledLearnMoreLink.HRef = HelpHelper.GetHelpUrl("OrionPHEnableF5Poller");

    }
}