<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EnvironmentStatus.ascx.cs" Inherits="Orion_F5_Controls_EnvironmentStatus" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="SolarWinds.F5.Web.ViewModels.SummaryStatus" %>

<!-- some newlines removed as it actually inserts extra whitespace into html -->
<table>
    <tr>
        <td>
            <div class="f5-environment-summary-resource f5-f5-summary-resource" id="environmentStatus-<%= UniqueID%>">
                <% if (Tiles != null && Tiles.Count > 0)
                    { %>
                <ul class="f5-tile-list">
                    <% foreach (Tile tile in Tiles)
                        { %><li class="f5-tile animated fadeIn">
                            <div class="f5-tile-head">
                                <img src="<%= tile.Icon%>" alt="" class="icon" width="24" height="24" />
                                <% if (tile.Count > 0 && !string.IsNullOrEmpty(tile.Link))
                                    { %>
                                <a href="<%= tile.Link%>">
                                    <span class="name"><%= tile.DisplayName%></span>&nbsp;(<span class="value"><%= tile.Count%></span>)
                                </a>
                                <% }
                                    else { %>
                                <span>
                                    <span class="name"><%= tile.DisplayName%></span>&nbsp;(<span class="value"><%= tile.Count%></span>)
                                </span>
                                <% } %>
                            </div>
                            <% if (tile.Count > 0 && tile.Details.Count > 0)
                                { %>
                            <div class="f5-tile-container f5-format-background f5-tile-metastatus f5-metastatus-<%= tile.GroupStatus%>" data-metastatus="<%= tile.GroupStatus%>">
                                <% if (string.IsNullOrEmpty(tile.Details[0].Link))
                                    { %>
                                <span class="f5-main-status f5-format-text">
                                    <% }
                                        else { %>
                                    <a href="<% = tile.Details[0].Link %>" class="f5-main-status f5-format-text">
                                        <% } %>
                                        <span class="status-icon">
                                            <img src="<% = tile.Details[0].Icon %>" alt="<% = tile.Details[0].DisplayName %>" title="<% = tile.Details[0].DisplayName %>" width="32" height="32" />
                                        </span>
                                        <span class="status-count">
                                            <% = tile.Details[0].Count %>
                                        </span>
                                        <span class="status-name">
                                            <%= tile.Details[0].DisplayName %>
                                        </span>
                                        <% if (string.IsNullOrEmpty(tile.Details[0].Link))
                                            { %>
                                </span>
                                <% }
                                    else { %>
				</a>
                <% } %>
                                <div class="f5-tile-body f5-tile-body-metastatus">
                                    <% if (tile.Details.Count() > 1)
                                        { %>
                                    <div class="f5-tile-body-centered">
                                        <div>
                                            <ul class="f5-tile-details">
                                                <% foreach (var detail in tile.Details.Skip(1))
                                                    { %>
                                                <li>
                                                    <% if (string.IsNullOrEmpty(detail.Link))
                                                        { %>
                                                    <span class="f5-detail-status f5-status-<%= detail.StatusName.ToLower(CultureInfo.CurrentCulture)%>" data-status="<%= detail.StatusName.ToLower(CultureInfo.CurrentCulture)%>">
                                                        <img src="<%= detail.Icon%>" alt="<%= detail.DisplayName%>" title="<%= detail.DisplayName%>" width="16" height="16" />
                                                        <span class="f5-format-text"><span class="status-count"><%= detail.Count%></span></span>
                                                    </span>
                                                    <% }
                                                        else { %>
                                                    <a href="<%= detail.Link%>" class="f5-detail-status f5-status-<%= detail.StatusName.ToLower(CultureInfo.CurrentCulture)%>" data-status="<%= detail.StatusName.ToLower(CultureInfo.CurrentCulture)%>">
                                                        <img src="<%= detail.Icon%>" alt="<%= detail.DisplayName%>" title="<%= detail.DisplayName%>" width="16" height="16" />
                                                        <span class="f5-format-text"><span class="status-count"><%= detail.Count%></span></span>
                                                    </a>
                                                    <% } %>
                                                </li>
                                                <% } %>
                                            </ul>
                                        </div>
                                    </div>
                                    <% }
                                        else if (tile.Details.Count == 1)
                                        { %>
                                    <div class="f5-tile-body-centered">
                                        <div>
                                            <p class="f5-text-secondary"><%= Resources.F5WebContent.F5WEBDATA_JP0_2 %></p>
                                        </div>
                                    </div>

                                    <% }
                                        else { %>

                                    <div class="f5-tile-body-centered">
                                        <div>
                                            <p class="f5-text-secondary"><%= string.Format(Resources.F5WebContent.F5WEBDATA_JP0_3, tile.DisplayName)%></p>
                                        </div>
                                    </div>
                                    <% } %>
                                </div>
                            </div>
                            <% }
                                else { %>
                            <div class="f5-tile-container f5-format-background f5-metastatus-unknown">
                                <div class="f5-main-status">
                                    <div class="status-count">
                                        0
                                    </div>
                                </div>
                                <div class="f5-tile-body f5-tile-body-metastatus">
                                    <div class="f5-tile-body-centered">
                                        <div>
                                            <p class="f5-text-secondary"><%= string.Format(Resources.F5WebContent.F5WEBDATA_JP0_3, tile.DisplayName)%></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <% } %></li>
                    <% } %>
                </ul>
                <% }
                    else { %>
                <p class="f5-no-resource-data">
                    <%= Resources.F5WebContent.F5WEBDATA_JP0_1 %>
                </p>
                <% } %>
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    $(function () {
        var control = $(document.getElementById("environmentStatus-<%= UniqueID%>"));
        var resource = control.closest('.ResourceWrapper');

        // set proper widths
        var handleLayout = function () {
            var margin = 15;
            var tiles = control.find(".f5-tile");
            var cssWidth = 'calc( 100% / ' + tiles.length + ' - ' + margin + 'px)';
            tiles.css('width', cssWidth);

            // reset any previous height
            control.find(".f5-tile-body-centered").css('height', '');
            control.find(".f5-tile-head").css('height', '');
            control.find(".f5-tile-body").css('height', '');
            control.find(".f5-main-status").css('height', '');

            // equalize height of components
            var selectorClasses = ['.f5-tile-head', '.f5-tile-body', '.f5-main-status'];

            $.each(selectorClasses, function (index, selectorClass) {
                var collection = tiles.find(selectorClass);

                var maxHeight = 0;
                collection.each(function (elementIndex, element) {
                    var elementHeight = $(element).height();
                    if (elementHeight > maxHeight) {
                        maxHeight = elementHeight;
                    }
                });
                collection.css('height', maxHeight + "px");

                if (selectorClass == ".f5-tile-body")
                    control.find(".f5-tile-body-centered").css('height', maxHeight + "px");
            });
        };

        $tiles = control.find(".f5-tile");
        $tiles.css("visibility", "hidden");

        var handleLayoutDelayed = function () {
            var display = $tiles.css('display');
            if (display === 'list-item') {
                setTimeout(handleLayoutDelayed, 1)
            }
            else {
                $tiles.css("visibility", "visible");
                handleLayout();
            };
        };

        resource
            .off('resized')
            .on('resized', handleLayout);

        handleLayoutDelayed();
    });
</script>

