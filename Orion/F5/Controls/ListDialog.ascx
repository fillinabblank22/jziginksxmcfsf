﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListDialog.ascx.cs" Inherits="Orion_F5_Controls_WebUserControl" %>
<%@ Import Namespace="SolarWinds.Newtonsoft.Json" %>
<%@ Register TagPrefix="f5" TagName="searchbox" Src="~/Orion/F5/Controls/SearchBox.ascx" %>
<orion:Include ID="Include1" runat="server" Module="F5" File="ListDialog.css" />
<orion:Include ID="Include3" runat="server" Module="F5" File="ListDialog.js" />

<asp:ScriptManagerProxy runat="server" >
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<div class="sw-f5-list-dialog hidden">

    <asp:PlaceHolder runat="server" ID="HeaderPlaceHolder" />

<%--<f5:searchbox runat="server" Placeholder="<%# Resources.F5WebContent.F5WEBDATA_JP0_20%>" />--%>

    <div class="error-message sw-suggestion sw-suggestion-fail" style="display: none;">
        <span class="sw-suggestion-icon"></span>
        <div class="text"></div>
    </div>

    <div id="ListDialog-Loading-<%= ResourceID %>" class="sw-f5-list-dialog-loading" style="display: none;">
        <div class="sw-f5-list-dialog-loading-table">
            <div class="sw-f5-list-dialog-loading-cell">
                <img src="/Orion/images/loading_gen_small.gif" />
                <span><%= Resources.F5WebContent.F5WEDATA_EK0_24%></span>                 
            </div>
        </div>
    </div>

    <table class="sw-f5-list-dialog-table NeedsZebraStripes sw-custom-query-table">
    </table>

    <div class="pagination">
        <span class="pagination-content"></span>
        <span class="pagination-displaying-text"></span>
    </div>

    <asp:PlaceHolder runat="server" ID="FooterPlaceHolder" />
    
    <textarea id="ListDialog-SWQL-<%=ResourceID %>" style="display:none;">
        <%= HttpUtility.HtmlEncode(SWQL) %>
    </textarea>

</div>
