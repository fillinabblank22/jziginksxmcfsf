﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditSitesAndServices.ascx.cs" Inherits="Orion_F5_Controls_EditResourceControls_EditSitesAndServices" EnableViewState="true" %>

<orion:Include File="F5/Styles/EditResource.css" runat="server" />

<div class="sw-res-editor-row">
    <b><%= Resources.CoreWebContent.WEBCODE_PSR_4 %></b><br/>
    <asp:TextBox runat="server" ID="RowsPerPage" CausesValidation="true"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RowsPerPageRequiredValidator" runat="server" ControlToValidate="RowsPerPage" Type="Integer" Display="Dynamic" ForeColor="Red"
        ErrorMessage="<%$ Resources: F5WebContent,F5WEDATA_LH0_31 %>" />
    <asp:RangeValidator ID="validator" runat="server" ControlToValidate="RowsPerPage" Type="Integer" Display="Dynamic" ForeColor="Red"
        MinimumValue="1" MaximumValue="99" ErrorMessage="<%$ Resources: F5WebContent,F5WEDATA_LH0_30 %>" />
</div>
