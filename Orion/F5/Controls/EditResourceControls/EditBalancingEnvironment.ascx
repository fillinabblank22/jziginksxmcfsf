﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditBalancingEnvironment.ascx.cs" Inherits="Orion_F5_Controls_EditResourceControls_EditBalancingEnvironment" %>
<table runat="server" id="BalancingEnvironmentSettings">
    <tr>
        <td>
            <b><%= Resources.F5WebContent.F5WEBDATA_PS0_4 %></b>
        </td>
    </tr>
    <tr>
        <td>
            <select runat="server" id="ShowObjects">
            </select>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <label>
	            <input type="checkbox" runat="server" id="ShowObjectCaptions"/>
		        <%= Resources.F5WebContent.F5WEBDATA_JP0_17 %>
            </label>
        </td>
    </tr>
</table>