﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditF5Events.ascx.cs" Inherits="Orion_F5_Controls_EditResourceControls_EditF5Events" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<orion:Include File="F5/Styles/EditResource.css" runat="server" />

<div class="sw-res-editor-row">
    <b><%= Resources.CoreWebContent.WEBCODE_PSR_4 %></b><br />
    <asp:TextBox runat="server" ID="RowsPerPage" CausesValidation="true"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RowsPerPageRequiredValidator" runat="server" ControlToValidate="RowsPerPage" Type="Integer" Display="Dynamic" ForeColor="Red"
        ErrorMessage="<%$ Resources: F5WebContent,F5WEDATA_LH0_31 %>" />
    <asp:RangeValidator ID="validator" runat="server" ControlToValidate="RowsPerPage" Type="Integer" Display="Dynamic" ForeColor="Red"
        MinimumValue="1" MaximumValue="99" ErrorMessage="<%$ Resources: F5WebContent,F5WEDATA_LH0_30 %>" />
</div>

<div class="sw-res-editor-row">
    <b><asp:Label runat="server" ID="label1" Text="<%$ Resources:CoreWebContent,WEBDATA_AK0_311 %>" /></b><br />
    <asp:DropDownList runat="server" ID="Period" />
</div>

<div class="sw-res-editor-row">

    <b><%= Resources.F5WebContent.F5WEBDATA_LH0_31 %></b><br />

    <asp:CheckBoxList runat="server" ID="NetobjectTypes">
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEBDATA_LH0_33 %>" Value="F5" />
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEBDATA_LH0_34 %>" Value="F5WIP" />
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEBDATA_LH0_35 %>" Value="F5VS" />
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEBDATA_LH0_36 %>" Value="F5LVA" />
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEBDATA_LH0_37 %>" Value="F5P" />
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEBDATA_LH0_38 %>" Value="F5PM" />
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEBDATA_LH0_39 %>" Value="F5S" />
        <asp:ListItem Text="<%$ Resources: F5WebContent,F5WEDATA_JT0_5 %>" Value="F5LHM" />        
    </asp:CheckBoxList>
    <asp:CustomValidator ID="NetobjectTypesValidator" ClientValidationFunction="validateNetobjectTypes" ErrorMessage="<%$ Resources: F5WebContent,F5WEBDATA_LH0_32 %>" ForeColor="Red" Display="Dynamic" EnableClientScript="true" runat="server" />
</div>

<script>
    function validateNetobjectTypes(source, arguments) {
        var chkList = document.getElementById('<%= NetobjectTypes.ClientID %>');
        var inputs = chkList.getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked) {
                arguments.IsValid = true;
                return;
            }
        }
        arguments.IsValid = false;
    }
</script>
