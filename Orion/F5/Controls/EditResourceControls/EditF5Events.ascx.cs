﻿using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_F5_Controls_EditResourceControls_EditF5Events : BaseResourceEditControl
{
    private const string propertyRowsPerPage = "RowsPerPage";
    private const string propertyNetobjectTypes = "NetobjectTypes";
    private const string propertyPeriod = "Period";
    private const string NetobjectTypesSeparator = ";";

    private const string defaultRowsPerPage = "10";
    private const string defaultPeriod = "Last 12 Months";
    private readonly Dictionary<string, object> properties = new Dictionary<string, object>();

    public override Dictionary<string, object> Properties
    {
        get
        {
            properties[propertyPeriod] = Period.Text;
            properties[propertyRowsPerPage] = RowsPerPage.Text;
            properties[propertyNetobjectTypes] = GetSelectedNetobjectTypesAsText();

            return properties;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RowsPerPage.Text = GetProperty(propertyRowsPerPage, defaultRowsPerPage);
            var previouslySelectedNetobjectTypes = GetProperty(propertyNetobjectTypes, string.Empty);
            RestoreSelectedNetobjectTypes(previouslySelectedNetobjectTypes);

            this.Period.SelectedValue = GetProperty(propertyPeriod, defaultPeriod);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        foreach (string period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));
        
        this.Period.Text = Resource.Properties["Period"];
    }

    /// <summary>
    /// Gets selected netobject types prefixes separated by semicolon.
    /// </summary>
    /// <returns>Netobject types prefixes separated by semicolon</returns>
    private string GetSelectedNetobjectTypesAsText()
    {
        var values = NetobjectTypes.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value);
        return string.Join(NetobjectTypesSeparator, values.ToArray());
    }

    private void RestoreSelectedNetobjectTypes(string previousSelection)
    {
        var separators = new string[1] { NetobjectTypesSeparator };
        string[] prefixes = previousSelection.Split(separators, StringSplitOptions.RemoveEmptyEntries);

        if (prefixes.Any())
        {
            var items = NetobjectTypes.Items.Cast<ListItem>();
            foreach (var listItem in items)
            {
                if (prefixes.Contains(listItem.Value))
                {
                    listItem.Selected = true;
                }
            }
        }
    }
}