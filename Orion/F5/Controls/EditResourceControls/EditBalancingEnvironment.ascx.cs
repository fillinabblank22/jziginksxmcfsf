﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Resources;
using Solarwinds.F5.Common;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_F5_Controls_EditResourceControls_EditBalancingEnvironment : BaseResourceEditControl
{
    private const bool DefaultF5BEShowObjectCaptions = false;

    private readonly Dictionary<string, object> properties = new Dictionary<string, object>();
    public override Dictionary<string, object> Properties
    {
        get
        {
            properties[Strings.ResourceProperyF5BEShowObjectCaptions] = ShowObjectCaptions.Checked.ToString();
            return properties;
        }
    }

    public bool IsPreselectedResource
    {
        get { return !string.IsNullOrEmpty(NetObjectID) && NetObjectID.StartsWith("F5"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        BalancingEnvironmentSettings.Visible = !IsPreselectedResource;

        if (!IsPostBack)
        {
            if (!IsPreselectedResource)
            {
                SetupShowOnlyProblems();
            }

            SetupShowObjectCaptions();
        }
        else
        {
            SaveShowOnlyProblems();
        }
    }

    private void SetupShowOnlyProblems()
    {
        var options = new List<ListItem>
        {
            new ListItem(F5WebContent.F5WEBDATA_PS0_2, "allObjects"),
            new ListItem(F5WebContent.F5WEBDATA_PS0_3, "onlyProblems")
        };

        options.ForEach(li => ShowObjects.Items.Add(li));

        bool showOnlyProblems;
        if (OrionConfiguration.IsDemoServer)
        {
            var storedValue = HttpContext.Current.Session[Strings.WebUserSettingsF5BEShowOnlyProblems];
            showOnlyProblems = Convert.ToBoolean(storedValue);
        }
        else
        {
            var storedValue = WebUserSettingsDAL.Get(Strings.WebUserSettingsF5BEShowOnlyProblems);
            showOnlyProblems = Convert.ToBoolean(storedValue);
        }

        ShowObjects.SelectedIndex = showOnlyProblems ? 1 : 0;
    }

    private void SaveShowOnlyProblems()
    {
        var newValue = (ShowObjects.SelectedIndex == 1).ToString();
        if (OrionConfiguration.IsDemoServer)
        {
            HttpContext.Current.Session[Strings.WebUserSettingsF5BEShowOnlyProblems] = newValue;
        }
        else
        {
            WebUserSettingsDAL.Set(Strings.WebUserSettingsF5BEShowOnlyProblems, newValue);
        }
    }

    private void SetupShowObjectCaptions()
    {
        var showNamesInDb = GetProperty(Strings.ResourceProperyF5BEShowObjectCaptions, DefaultF5BEShowObjectCaptions.ToString());
        ShowObjectCaptions.Checked = Convert.ToBoolean(showNamesInDb);
    }
}