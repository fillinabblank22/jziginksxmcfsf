﻿using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

public partial class Orion_F5_Controls_EditResourceControls_EditSitesAndServices : BaseResourceEditControl
{
    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    private const string _propertyRowsPerPage = "RowsPerPage";
    private const string _defaultRowsPerPage = "5";

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        RowsPerPage.Text = Resource.Properties[_propertyRowsPerPage] ?? _defaultRowsPerPage;
    }



    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                {_propertyRowsPerPage, RowsPerPage.Text}
            };

            return properties;
        }
    }
}