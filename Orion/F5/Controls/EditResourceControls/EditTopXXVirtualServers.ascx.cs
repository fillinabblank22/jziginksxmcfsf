﻿using Castle.Core.Internal;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_F5_Controls_EditResourceControls_EditTopXXVirtualServers : BaseResourceEditControl, IChartEditorSettings
{
    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    const int defaultTopXX = 5; // 5 virtual server series
    const int defaultTimeRange = 1; // last 1 day
    const string defaultChartZoom = "24h";
    const string defaultSampleSize = "10"; // 10 minutes

    const string topXXProperty = "NumberOfSeriesToShow";
    const string timeRangeProperty = "ChartDateSpan";
    const string chartZoomProperty = "ChartInitialZoom";
    const string sampleSizeProperty = "samplesize";
    const string chartNameProperty = "ChartName";

    

    protected void Page_Load(object sender, EventArgs e)
    {
        var chartName = Resource.Properties[chartNameProperty];
        SeriesMaximunNumberLabel.InnerText = GetMaximunNumberLabel(chartName);
        TopXXRangeValidator.ErrorMessage = GetMaximunNumberErrorMessage(chartName);
        if (!IsPostBack)
        {
            TopXX.Text = string.IsNullOrEmpty(Resource.Properties[topXXProperty]) ? defaultTopXX.ToString(CultureInfo.InvariantCulture) : Resource.Properties[topXXProperty];
            ChartTimeSpan.SelectedValue = string.IsNullOrEmpty(Resource.Properties[timeRangeProperty]) ? defaultTimeRange.ToString(CultureInfo.InvariantCulture) : Resource.Properties[timeRangeProperty];
            ChartZoom.SelectedValue = string.IsNullOrEmpty(Resource.Properties[chartZoomProperty]) ? defaultChartZoom.ToString(CultureInfo.InvariantCulture) : Resource.Properties[chartZoomProperty];
            HideTimeIntervalsForSpan(ChartTimeSpan.SelectedValue);
            SampleSize.SelectedValue = string.IsNullOrEmpty(Resource.Properties[sampleSizeProperty]) ? defaultSampleSize.ToString(CultureInfo.InvariantCulture) : Resource.Properties[sampleSizeProperty];            
        }        
    }


    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties[topXXProperty] = TopXX.Text;
            _properties[timeRangeProperty] = ChartTimeSpan.SelectedValue;
            _properties[chartZoomProperty] = ChartZoom.SelectedValue;
            _properties[sampleSizeProperty] = SampleSize.SelectedValue;

            return _properties;
        }
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
       
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        foreach (var property in Properties)
        {
            if (properties.ContainsKey(property.Key))
                properties[property.Key] = property.Value;
            else
                properties.Add(property.Key, property.Value);
        }       
    }

    protected string GetMaximunNumberLabel(string chartName)
    {
        switch (chartName.ToLower(CultureInfo.InvariantCulture))
        {
            case "topxvirtualserversforservice":
            case "topxvirtualservers":
                return Resources.F5WebContent.F5WEBDATA_LH0_13;
            case "topxpoolmembers":
            case "topxpoolmembersforvirtualservers":
                return Resources.F5WebContent.F5WEBDATA_AO0_01;
            case "topxwideipdnsresolutions":
                return Resources.F5WebContent.F5WEBDATA_JF0_01;
            default:
                return string.Empty;
        }
    }

    protected string GetMaximunNumberErrorMessage(string chartName)
    {
        switch (chartName.ToLower(CultureInfo.InvariantCulture))
        {
            case "topxvirtualserversforservice":
            case "topxvirtualservers":
                return Resources.F5WebContent.F5WEBDATA_LH0_14;
            case "topxpoolmembers":
            case "topxpoolmembersforvirtualservers":
                return Resources.F5WebContent.F5WEBDATA_AO0_02;
            case "topxwideipdnsresolutions":
                return Resources.F5WebContent.F5WEBDATA_JF0_02;
            default:
                return string.Empty;
        }
    }


    protected void HideTimeIntervalsForSpan(string timespan)
    {
        int timespanvalue = 0;
        if (int.TryParse(timespan, out timespanvalue))
        {
            HideTimeIntervalsForSpan(timespanvalue);
        }        
    }

    protected void HideTimeIntervalsForSpan(int timespan)
    {
        Func<int, bool> ShowIntervalsForMonth = (int interval) => interval >= 60;
        Func<int, bool> ShowIntervalsForDay = (int interval) => interval < 1440; ;
        Func<int, bool> ShowIntervalsForWeek = (int interval) => interval < 10080;

        Func<int, bool> calculationFunc = null;

        switch (timespan)
        {
            case 1: calculationFunc = ShowIntervalsForDay;
                break;
            case 7: calculationFunc = ShowIntervalsForWeek;
                break;
            case 30: calculationFunc = ShowIntervalsForMonth;
                break;
        }
		
        SampleSize.Items.Cast<ListItem>().ForEach(x => x.Enabled = calculationFunc(int.Parse(x.Value)));
    }

    protected void OnTimeSpentChanged(object sender, EventArgs e)
    {        
        HideTimeIntervalsForSpan(ChartTimeSpan.SelectedValue);
        if (ChartTimeSpan.SelectedValue == "1") SampleSize.SelectedValue = "10";
    }
}