﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTopXXVirtualServers.ascx.cs" Inherits="Orion_F5_Controls_EditResourceControls_EditTopXXVirtualServers" EnableViewState="true" %>

<orion:Include File="F5/Styles/EditResource.css" runat="server" />

<div class="sw-res-editor-row" runat="server" id="TopXXPanel">
    <span class="label">
        <label runat="server" id="SeriesMaximunNumberLabel" />        
    </span><br />
    <asp:TextBox runat="server" ID="TopXX" Width="50" />
    <asp:RangeValidator ID="TopXXRangeValidator" runat="server"
        Display="Dynamic" ControlToValidate="TopXX" MinimumValue="1" MaximumValue="10"
        Type="Integer" />
    <asp:RequiredFieldValidator ID="TopXXRequiredFieldValidator" runat="server" 
        ErrorMessage="<%$ Resources: F5WebContent,F5WEDATA_LH0_31 %>" ControlToValidate="TopXX" />
</div>

<div class="sw-res-editor-row" runat="server" ID="TimeSpanPanel">
    <span class="label"><%= Resources.CoreWebContent.WEBDATA_IB0_108%></span><br />
    <asp:DropDownList runat="server" ID="ChartTimeSpan" AutoPostBack="true" OnSelectedIndexChanged="OnTimeSpentChanged">
        <asp:ListItem Value="1" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 24)%>"></asp:ListItem>
        <asp:ListItem Value="7" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
        <asp:ListItem Value="30" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
    </asp:DropDownList>
</div>

<div class="sw-res-editor-row" runat="server" id="ZoomPanel">
    <span class="label"><%= Resources.CoreWebContent.WEBDATA_IB0_98%></span><br />   
    <asp:DropDownList runat="server" ID="ChartZoom">
        <asp:ListItem Value="1h" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_99%>"></asp:ListItem>
        <asp:ListItem Value="2h" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 2)%>"></asp:ListItem>
        <asp:ListItem Value="24h" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 24)%>"></asp:ListItem>
        <asp:ListItem Value="today" Text="<%$ Resources: CoreWebContent, WEBCODE_VB0_133%>"></asp:ListItem>
        <asp:ListItem Value="yesterday" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_101%>"></asp:ListItem>
        <asp:ListItem Value="7d" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
        <asp:ListItem Value="thisMonth" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_102%>"></asp:ListItem>
        <asp:ListItem Value="lastMonth" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
        <asp:ListItem Value="30d" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_188%>"></asp:ListItem>
    </asp:DropDownList>
</div>

<div class="sw-res-editor-row" runat="server" ID="SampleSizePanel">
    <span class="label"><%= Resources.CoreWebContent.WEBDATA_IB0_109%></span><br />   
    <asp:DropDownList runat="server" ID="SampleSize">
        <asp:ListItem Value="1" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_110 %>"></asp:ListItem>
        <asp:ListItem Value="5" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 5)%>"></asp:ListItem>
        <asp:ListItem Value="10" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 10)%>"></asp:ListItem>
        <asp:ListItem Value="15" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 15)%>"></asp:ListItem>
        <asp:ListItem Value="30" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 30)%>"></asp:ListItem>
        <asp:ListItem Value="60" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_112 %>"></asp:ListItem>
        <asp:ListItem Value="120" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 2)%>"></asp:ListItem>
        <asp:ListItem Value="360" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 6)%>"></asp:ListItem>
        <asp:ListItem Value="720" Text="<%$ Code: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 12)%>"></asp:ListItem>
        <asp:ListItem Value="1440" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_114 %>"></asp:ListItem>
        <asp:ListItem Value="10080" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_115 %>"></asp:ListItem>
    </asp:DropDownList>
    <div class="sw-text-helpful">
        <%= Resources.CoreWebContent.WEBDATA_IB0_116%>
    </div>
</div>
