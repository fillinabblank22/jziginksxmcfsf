<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchBox.ascx.cs" Inherits="Orion_F5_Controls_SearchBox" %>

<orion:Include ID="Include1" runat="server" Module="F5" File="SearchBox.js" Section="Top" SpecificOrder="-1" />
<orion:Include ID="Include2" runat="server" Module="F5" File="SearchBox.css" Section="Top" SpecificOrder="-1" />

<span class="<%= ClassNames %> f5-searchbox" ID="<%= ClientID %>">
    <input type="text" class="f5-searchbox-input" autocomplete="<%= Autocomplete ? "on" : "off" %>" placeholder="<%= Placeholder %>" data-placeholder="<%= Placeholder %>" value="" ID="<%= InputClientID%>" />
    <span class="f5-searchbox-button f5-searchbox-button-active" ID="<%= SearchButtonClientID%>"></span>
</span>
