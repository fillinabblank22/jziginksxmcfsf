using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_F5_Controls_F5ApiConfigCheck : UserControl
{
    public int NodeId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool isPollerEnabled = false;
        bool isCredentials = false;
        int? lastApiPollingError = null;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var query = @"
                SELECT p.Enabled, d.LastApiPollingError
                FROM Orion.NodeSettings ns
                JOIN Orion.Pollers p ON ns.NodeID = p.NetObjectID AND p.NetObjectType = @NetObjectType
                JOIN Orion.F5.System.Device d ON d.NodeId = ns.NodeId
                WHERE ns.NodeID = @NodeId AND ns.SettingName = 'F5.API.CredentialID' AND p.PollerType = @PollerType";

            var resultsTable = swis.Query(query, new Dictionary<string, object>
            {
                { "NetObjectType", Solarwinds.F5.Common.Strings.F5NetObjectType },
                { "NodeId", NodeId },
                { "PollerType", Solarwinds.F5.Common.Strings.F5ApiStatusPollerType }
            });

            if (resultsTable.Rows.Count != 0)
            {
                isCredentials = true;
                bool.TryParse(resultsTable.Rows[0][0].ToString(), out isPollerEnabled);

                int temp;
                if (int.TryParse(resultsTable.Rows[0][1].ToString(), out temp))
                {
                    lastApiPollingError = temp;
                }
            }
        }

        if (isPollerEnabled)
        {
            if (lastApiPollingError.HasValue)
            {
                var message = Resources.F5WebContent.F5WEDATA_EK0_0;
                switch (/*(F5ApiPollerError)*/lastApiPollingError.Value)
                {
                    case 0: //F5ApiPollerError.Unknown
                        message = Resources.F5WebContent.F5WEDATA_EK0_0;
                        break;
                    case 1: //F5ApiPollerError.CertificateIsIncorrect
                        message = Resources.F5WebContent.F5WEDATA_EK0_1;
                        break;
                    case 2: //F5ApiPollerError.CredentialsAreIncorrect
                        message = Resources.F5WebContent.F5WEDATA_EK0_2;
                        break;
                    case 3: //F5ApiPollerError.ServerIsUnavailable
                        message = Resources.F5WebContent.F5WEDATA_EK0_3;
                        break;
                    case 4: //F5ApiPollerError.ApiIsUnavailable
                        message = Resources.F5WebContent.F5WEDATA_EK0_4;
                        break;
                    case 5: //F5ApiPollerError.IncorrectApiResponse
                        message = Resources.F5WebContent.F5WEDATA_EK0_5;
                        break;
                }
                ConfigHelperBox.InnerHtml = string.Format(message, GetF5NodeEditLink(), GetF5NodeDetailsLink());
            }
            else
            {
                this.Visible = false;
            }
        }
        else
        {
            this.Visible = true;

            ConfigHelperBox.InnerHtml = string.Format(
                isCredentials ? Resources.F5WebContent.F5WEDATA_JT0_4 : Resources.F5WebContent.F5WEDATA_JT0_3,
                GetF5NodeEditLink());
        }
    }

    protected string GetF5NodeEditLink()
    {
        string returnUrl = "";
        if (Request.UrlReferrer != null)
        {
            returnUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
        }

        return string.Format("/Orion/Nodes/NodeProperties.aspx?Nodes={0}&ReturnTo={1}#F5ApiCredentialsSection", NodeId, returnUrl);
    }
    protected string GetF5NodeDetailsLink()
    {
        return string.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", NodeId);
    }
}
