﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Newtonsoft.Json;

public partial class Orion_F5_Controls_WebUserControl : System.Web.UI.UserControl
{
    public int ResourceID { get; set; }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Header
    {
        get { return HeaderPlaceHolder; }
    }
    
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Footer
    {
        get { return FooterPlaceHolder; }
    }

    public string SWQL { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
    }
}