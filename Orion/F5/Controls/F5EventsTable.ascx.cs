﻿using SolarWinds.F5.Web.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

public partial class Orion_F5_Controls_F5EventsTable : System.Web.UI.UserControl
{
    public ResourceInfo Resource { get; set; }

    public int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public int NodeID { get; set; }

    /// <summary>
    /// Lists of netobject types used to filter events.
    /// </summary>
    public IEnumerable<string> NetObjectTypes { get; set; }

    public IEnumerable<NetObjectDto> SpecificNetobjects { get; set; }

    string GetDefaultPeriod()
    {
        return "Last 12 Months";
    }

    public string SWQL
    {
        get
        {
            return @"SELECT
                Events.NetObjectType as _NetObjectType,
                Events.NetObjectID as _NetObjectID,
                Events.EventType,
                Events.Message,                
                ToLocal(Events.EventTime) as EventTime,
                ISNULL(EventTypes.BackColor, 0) as _BackColor, n.NodeID as _LinkNodeID
                FROM Orion.F5.System.Device(NOLOCK = true) n
                INNER JOIN Orion.Events(NOLOCK = true) Events ON n.NodeID = Events.NetworkNode
                INNER JOIN Orion.EventTypes(NOLOCK = true) EventTypes ON (EventTypes.EventType = Events.EventType)
                WHERE Events.Acknowledged = 'false'
                AND n.NodeID=@nodeID
                AND @NetObjectCondition
                AND Events.EventTime >= @fromDate AND Events.EventTime <= @toDate";
        }
    }

    public string OrderBy
    {
        get
        {
            return "EventTime DESC";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.OrderBy = OrderBy;

        DateTime periodBegin = new DateTime();
        DateTime periodEnd = new DateTime();

        // get period from resources
        string periodName = Resource.Properties["Period"];

        // if there is no valid period set "Today" as default
        if (string.IsNullOrEmpty(periodName))
            periodName = GetDefaultPeriod();

        // parse period to begin and end
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

        if ((NetObjectTypes == null || !NetObjectTypes.Any()) && (SpecificNetobjects == null || !SpecificNetobjects.Any()))
        {
            throw new InvalidOperationException("Expected at least one netobject type but was provided none.");
        }

        string netObjectTypes = string.Join(",", NetObjectTypes.Select(netobject => string.Format("'{0}'", netobject)).ToArray());

        string noCondition = NetObjectTypes.Any() ? "Events.NetobjectType IN(@netObjectTypes)".Replace("@netObjectTypes", netObjectTypes) : BuildSpecificNetobjectCondition();

        string parametrizedSwql = SWQL
            .Replace("@nodeID", NodeID.ToString(CultureInfo.InvariantCulture))
            .Replace("@fromDate", string.Format("'{0}'", periodBegin.ToUniversalTime().ToString(CultureInfo.InvariantCulture)))
            .Replace("@toDate", string.Format("'{0}'", periodEnd.ToUniversalTime().ToString(CultureInfo.InvariantCulture)))
            .Replace("@NetObjectCondition", noCondition);
        CustomTable.SWQL = parametrizedSwql;
    }

    private string BuildSpecificNetobjectCondition()
    {
        if (SpecificNetobjects == null || !SpecificNetobjects.Any())
        {
            // dummy condition
            return "1=1";
        };

        return string.Join(" OR ", SpecificNetobjects.Select(no => string.Format("(Events.NetObjectType = '{0}' AND Events.NetObjectID = {1})", no.NetObjectType, no.NetObjectID)).ToArray());
    }
}