using System;
using System.Web.UI;

public partial class Orion_F5_Controls_SearchBox : UserControl
{
    public string ClassNames { get; set; }
    public bool Autocomplete { get; set; }
    public string Placeholder { get; set; }

    public string SearchButtonClientID {
    	get { return ClientID + "_button"; }
    }

    public string InputClientID {
    	get { return ClientID + "_input"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBind();
    }
}
