﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeRotationPresence.ascx.cs" Inherits="Orion_F5_Controls_ChangeRotationPresence" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="f5" TagName="ListDialog" Src="~/Orion/F5/Controls/ListDialog.ascx" %>

<orion:Include ID="Include1" runat="server" Module="F5" File="ChangeRotationPresence.js" />
<orion:Include ID="Include2" runat="server" Module="F5" File="ChangeRotationPresence.css" />

<f5:ListDialog runat="server" ID="ListDialog">
    <Header>
        <div><%=Resources.F5WebContent.F5WEDATA_EK0_21 %></div>
    </Header>
    <Footer>
        <div class="change-rotation-presence-note" id="divNote" runat="server">
            <%=Resources.F5WebContent.F5WEDATA_ZB0_4 %>:
            <br />
            <textarea id="tbDisableReason" maxlength="1024"></textarea>
        </div>
        <div class="sw-pg-hint-blue">
            <div class="sw-pg-hint-body-info">
                <div class="sw-pg-hint-text">
                    <p><b><%=Resources.F5WebContent.F5WEDATA_EK0_22 %></b></p>
                    <p><%= string.Format(Resources.F5WebContent.F5WEDATA_EK0_23, HelpHelper.GetHelpUrl(this.HelpLinkFragment)) %></p>
                </div>
            </div>
        </div>
        <% if (!string.IsNullOrEmpty(WarningMessage))
            {%>
        <br />
        <div class="sw-pg-hint-yellow">
            <div class="sw-pg-hint-body-warning">
                <div class="sw-pg-hint-text">
                    <%=WarningMessage %>
                </div>
            </div>
        </div>
        <%}%>
        <div class="change-rotation-presence-error">
            <div class="sw-pg-hint-red">
                <div class="sw-pg-hint-body-error">
                    <div class="sw-pg-hint-text">
                    </div>
                </div>
            </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButtonLink ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" />
            <orion:LocalizableButtonLink ID="btnClose" runat="server" DisplayType="Secondary" LocalizedText="Close" />
        </div>
    </Footer>
</f5:ListDialog>
