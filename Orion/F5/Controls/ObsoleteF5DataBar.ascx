﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObsoleteF5DataBar.ascx.cs" Inherits="Orion_F5_Controls_ObsoleteF5DataBar" %>

<div style="margin: 7px 7px 7px 14px;" runat="server" id="PollerOffMessageBar">
    <div class="sw-suggestion sw-suggestion-warn">
        <span class='sw-suggestion-icon'></span>
        <%= Resources.F5WebContent.F5WEBDATA_LH0_43 %> <a href="" target="_blank" class="sw-suggestion-link" runat="server" id="PolledDisabledLearnMoreLink">&raquo;<%= Resources.F5WebContent.F5WEBDATA_LH0_44 %></a>
    </div>
</div>
