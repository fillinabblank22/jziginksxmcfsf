﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Solarwinds.F5.Common.Helpers;
using Solarwinds.F5.Common.Models.API;
using SolarWinds.F5.Common;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using Strings = Solarwinds.F5.Common.Strings;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Models;

public partial class Orion_F5_Controls_F5ApiCredentials : UserControl, INodePropertyPlugin
{
    private IF5CredentialManager _credentialManager = new F5CredentialManager();
    private Dictionary<string, object> _propertyBag;
    private IList<Node> _nodes;
    private NodePropertyPluginExecutionMode _mode;

    private readonly IEngineDAL engineDal = new EngineDAL();

    private Orion_Nodes_Controls_PollingMethodSelector _pollingMethodSelector;

    protected enum TestResult
    {
        None,
        Success,
        Failure
    }

    protected string enteredUserName = String.Empty;
    protected string enteredPassword = String.Empty;
    protected bool enteredSsl;
    protected int enteredPort;

    private const int DEFAULT_API_PORT = 443;
    private const bool DEFAULT_API_HTTPS = true;

    private const string PLUGIN_NAME = "F5ApiCredentials";
    private const string USERNAME_SESSIONID = "F5ApiCredentials_UserName";
    private const string PASSWORD_SESSIONID = "F5ApiCredentials_Password";
    private const string SSL_SESSIONID = "F5ApiCredentials_UseSSL";
    private const string PORT_SESSIONID = "F5ApiCredentials_Port";
    private const string ADVANCEDSETTINGS_SESSISONID = "F5ApiCredentials_AdvancedSettings";
    private const string ISF5_SESSIONID = "F5ApiCredentials_IsF5";
    private const string TESTRESULT_SESSIONID = "F5ApiCredentials_TestResult";
    private const string INVALID_CERTIFICATES_SESSIONID = "F5ApiCredentials_InvalidCertificates";

    #region Properties

    /// <summary>
    /// Defines if plugin "Enable multiple selection" checkbox is visible. Normally property is set from this.Initialize()
    /// </summary>
    public bool MultipleSelectionVisible
    {
        get { return cbMultipleSelection.Visible; }
        set { cbMultipleSelection.Visible = value; }
    }

    /// <summary>
    /// Check if polling of N.F5.API.Status is enabled. Return string.Empty if poller job for specified node ID does not exist.
    /// </summary>
    private string GetF5PollingEnabled(Node node)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            string query = "SELECT Enabled FROM Orion.Pollers WHERE NetObjectType = @NetObjectType AND NetObjectID = @NodeId AND PollerType = @PollerType";

            var nodeIdTable = swis.Query(query, new Dictionary<string, object>
            {
                { "NetObjectType", Strings.F5NetObjectType },
                { "NodeId", node.ID },
                { "PollerType", Strings.F5ApiStatusPollerType }
            });

            if (nodeIdTable.Rows.Count == 0)
            {
                return string.Empty;
            }

            return nodeIdTable.Rows[0][0].ToString();
        }
    }

    /// <summary>
    /// Check if F5 iControl polling is supported for supplied nodes. To qualify for iControl
    /// polling, all nodes need to have an entry in F5 System Device entity and be polled only
    /// through supported engine types.
    /// </summary>
    /// <param name="nodeIds">IDs of the nodes to be checked</param>
    /// <param name="supportedEngineTypes">Engine types supporting F5 iControl polling</param>
    /// <returns>
    /// True only if all the nodes have reference to F5 System Device entity and are polled via
    /// supported engine types
    /// </returns>
    /// <remarks>'RemoteCollector' engine type is currently unsupported for iControl polling.</remarks>
    private bool IsF5iControlSupportedForNodes(IEnumerable<int> nodeIds, HashSet<EngineServerType> supportedEngineTypes)
    {
        var supportedEngineTypesConcatenated = string.Join(",", supportedEngineTypes.Select(s => $"'{s}'"));
        var nodeIdsConcatenated = string.Join(",", nodeIds);

        log.Debug($"Determining support for F5 iControl polling plugin for nodeIDs={nodeIdsConcatenated}");

        try
        {
            using (var swis = InformationServiceProxy.CreateV3())
            {
                var query = $@"SELECT NodeID FROM Orion.F5.System.Device d 
                               WHERE d.Node.Engine.ServerType IN ({supportedEngineTypesConcatenated}) 
                               AND NodeID IN ({nodeIdsConcatenated})";
                var nodeIdTable = swis.Query(query);
                var isSupported = nodeIdTable.Rows.Count.Equals(nodeIds.Count());

                log.Debug($"Support for F5 iControl polling plugin determined as '{isSupported}'");
                return isSupported;
            }
        }
        catch (Exception exception)
        {
            log.Warn(
                $"Unable to get information about F5 System Device for node IDs ({nodeIdsConcatenated}). " +
                "F5 iControl plugin will be considered as unsupported.", exception);
            return false;
        }
    }

    //

    /// <summary>
    /// Defines if plugin "Enable" checkbox is visible. Normally property is set from this.Initialize()
    /// </summary>
    public bool IsF5Node
    {
        get
        {
            if (!_propertyBag.ContainsKey(ISF5_SESSIONID))
            {
                _propertyBag[ISF5_SESSIONID] = false;
            }
            return (bool)_propertyBag[ISF5_SESSIONID];
        }
        set
        {
            _propertyBag[ISF5_SESSIONID] = value;
        }
    }

    /// <summary>
    /// Defines if plugin is enabled to edit multiple items. Normally property is set from this.Initialize()
    /// </summary>
    public bool AllowSettingsChange
    {
        get { return cbMultipleSelection.Checked; }
        set
        {
            cbMultipleSelection.Checked = value;
            cbMultipleSelection_CheckedChanged(this, EventArgs.Empty);
        }
    }

    public string EnteredUserName
    {
        get
        {
            if (!_propertyBag.ContainsKey(USERNAME_SESSIONID))
            {
                enteredUserName = String.Empty;
                _propertyBag[USERNAME_SESSIONID] = enteredUserName;
            }
            return (string)_propertyBag[USERNAME_SESSIONID];
        }
        set
        {
            if (value == null)
            {
                if (_propertyBag.ContainsKey(USERNAME_SESSIONID))
                {
                    _propertyBag.Remove(USERNAME_SESSIONID);
                }
            }
            else
            {
                enteredUserName = value;
                _propertyBag[USERNAME_SESSIONID] = value;
            }
        }
    }

    public string EnteredPassword
    {
        get
        {
            if (!_propertyBag.ContainsKey(PASSWORD_SESSIONID))
            {
                enteredPassword = String.Empty;
                _propertyBag[PASSWORD_SESSIONID] = enteredPassword;
            }
            return (string)_propertyBag[PASSWORD_SESSIONID];
        }
        set
        {
            if (value == null)
            {
                if (_propertyBag.ContainsKey(PASSWORD_SESSIONID))
                {
                    _propertyBag.Remove(PASSWORD_SESSIONID);
                }
            }
            else
            {
                enteredPassword = value;
                _propertyBag[PASSWORD_SESSIONID] = value;
            }
        }
    }

    public bool IsAdvancedSettings
    {
        get
        {
            if (!_propertyBag.ContainsKey(ADVANCEDSETTINGS_SESSISONID))
            {
                _propertyBag[ADVANCEDSETTINGS_SESSISONID] = false;
            }
            return (bool)_propertyBag[ADVANCEDSETTINGS_SESSISONID];
        }
        set
        {
            _propertyBag[ADVANCEDSETTINGS_SESSISONID] = value;
        }
    }


    public bool EnteredSsl
    {
        get
        {
            if (!_propertyBag.ContainsKey(SSL_SESSIONID))
            {
                _propertyBag[SSL_SESSIONID] = enteredSsl = DEFAULT_API_HTTPS;
            }
            return (bool)_propertyBag[SSL_SESSIONID];
        }
        set
        {
            _propertyBag[SSL_SESSIONID] = enteredSsl = value;
        }
    }

    public int EnteredPort
    {
        get
        {
            if (!_propertyBag.ContainsKey(PORT_SESSIONID))
            {
                enteredPort = DEFAULT_API_PORT;
                _propertyBag[PORT_SESSIONID] = enteredPort;
            }
            return (int)_propertyBag[PORT_SESSIONID];
        }
        set
        {
            enteredPort = value;
            _propertyBag[PORT_SESSIONID] = value;
        }
    }

    public Dictionary<string, string> InvalidCertificatesMap
    {
        get
        {
            if (!_propertyBag.ContainsKey(INVALID_CERTIFICATES_SESSIONID))
            {
                _propertyBag[INVALID_CERTIFICATES_SESSIONID] = new Dictionary<string, string>();
            }
            return (Dictionary<string, string>)_propertyBag[INVALID_CERTIFICATES_SESSIONID];
        }
    }

    private Orion_Nodes_Controls_PollingMethodSelector PollingMethodSelector
    {
        get
        {
            if (_pollingMethodSelector == null)
            {
                _pollingMethodSelector = ControlHelper.FindControlRecursive(this.Page, "PollingMethodSelector") as Orion_Nodes_Controls_PollingMethodSelector;
            }
            return _pollingMethodSelector;
        }
    }

    /// <summary>
    /// This property depends of _nodes field. This property may be changed after PageLoad executed
    /// </summary>
    private bool IsSnmpPollingMethod
    {
        get
        {
            return PollingMethodSelector != null && PollingMethodSelector.SNMP;
        }
    }

    #endregion

    protected void Page_PreRender(object sender, EventArgs e)
    {
        cbAdvancedSettings.Checked = IsAdvancedSettings;
        cbAdvancedSettings.Attributes.Add("onclick", "f5ApiCredentialsAdvancedSettingsHandler_" + ClientID + "();");
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "F5ApiCredentials_toggleAdvancedSettings", @"
            var f5ApiCredentialsAdvancedSettingsHandler_" + ClientID + @" = function () {
                    var enabled = $(this).prop('checked');
                    $('#" + tableAdvancedSettings.ClientID + @"').toggle(enabled);
                    ValidatorEnable($('#" + requiredPortNumberValidator.ClientID + @"')[0], enabled);
                    ValidatorEnable($('#" + rangePortNumberValidator.ClientID + @"')[0], enabled);
                }", true);
        tableAdvancedSettings.Attributes.Add("style", IsAdvancedSettings ? "display: table;" : "display: none;");

        cbIsF5Node.Enabled = AllowSettingsChange && IsSnmpPollingMethod;
        F5ApiCredentialsBox.Visible = IsF5Node && cbIsF5Node.Enabled;
        F5ApiNoSNMPErrorMessage.Visible = !IsSnmpPollingMethod;

        FixPasswordBox();
    }

    private void FixPasswordBox()
    {
        // This is a fix for NPM-2686
        // Should be removed when Core component wil be fixed. Check JIRA case CORE-5216
        if (string.IsNullOrEmpty(EnteredPassword))
        {
            var textBox = txtPassword.FindControl("PasswordBox") as System.Web.UI.WebControls.TextBox;
            if (textBox != null)
            {
                textBox.Text = string.Empty;
                textBox.Attributes["value"] = string.Empty;
            }
        }
    }

    #region Event handlers

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        IsF5Node = cbIsF5Node.Checked && AllowSettingsChange;
    }

    protected void cbIsF5Node_CheckedChanged(object sender, EventArgs e)
    {
        IsF5Node = cbIsF5Node.Checked && AllowSettingsChange;
    }

    protected void TestButton_Click(object sender, EventArgs e)
    {
        ClearTestResultMessage();
        UpdateData();

        ValidateInternal();
    }

    protected void SslCertificateImportButton_Click(object sender, EventArgs e)
    {
        foreach (Node node in _nodes)
        {
            if (InvalidCertificatesMap.ContainsKey(node.IpAddress))
            {
                node.NodeSettings[Strings.ReservedSslCertificateIdentitySettingName] = InvalidCertificatesMap[node.IpAddress];
            }
        }
        F5ApiTestInvalidCertificate.Visible = false;

        TestButton_Click(null, null);
    }

    #endregion

    #region Helpers

    private void ClearTestResultMessage()
    {
        HttpContext.Current.Session[TESTRESULT_SESSIONID] = TestResult.None;
        F5ApiTestSuccess.Visible = false;
        F5ApiTestFailure.Visible = false;
        InvalidCertificatesMap.Clear();
    }

    private void ReportTestSuccess()
    {
        HttpContext.Current.Session[TESTRESULT_SESSIONID] = TestResult.Success;
        F5ApiTestSuccess.Visible = true;
        F5ApiTestFailure.Visible = false;
        F5ApiTestInvalidCertificate.Visible = false;
    }

    private void ReportTestFailure(string errorText, bool showCertificateImportOption)
    {
        HttpContext.Current.Session[TESTRESULT_SESSIONID] = TestResult.Failure;
        F5ApiTestSuccess.Visible = false;
        F5ApiTestFailure.Visible = true;
        F5ApiTestInvalidCertificate.Visible = showCertificateImportOption;
        F5ApiTestFailureReason.Text = errorText;
    }

    private void UpdateData()
    {
        try
        {
            IsAdvancedSettings = cbAdvancedSettings.Checked;
            if (IsAdvancedSettings)
            {
                try
                {
                    EnteredPort = Convert.ToInt32(txtPortNumber.Text);
                }
                catch
                {
                    EnteredPort = DEFAULT_API_PORT;
                }
                EnteredSsl = cbUseSSL.Checked;
            }
            else
            {
                EnteredPort = DEFAULT_API_PORT;
                EnteredSsl = DEFAULT_API_HTTPS;
            }
            EnteredUserName = txtUserName.Text;
            EnteredPassword = txtPassword.Text;
            // update PasswordKey
            txtPassword.Text = EnteredPassword;
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }
    }

    private F5ApiTestConnectionResult TestConnection(string nodeDnsName, string nodeIpAddress, int portNumber, string userName, string password, bool ssl, int engineId, string reservedSslCertificateIdentity)
    {
        var effectiveEngineId = engineDal.GetOrionEngine(engineId).EngineID;
        using (var proxy = F5BusinessLayerProxyFactory.Instance.Create(effectiveEngineId))
        {
            try
            {
                return proxy.Api.TestApiPolling(nodeDnsName, nodeIpAddress, portNumber, ssl, userName, password, reservedSslCertificateIdentity);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw;
            }
        }
    }

    private void ClearDataFromSession()
    {
        EnteredUserName = String.Empty;
        EnteredPassword = String.Empty;
        EnteredPort = DEFAULT_API_PORT;
        EnteredSsl = DEFAULT_API_HTTPS;
        InvalidCertificatesMap.Clear();
    }

    private void FillFirstTimeValues()
    {
        //Try to get data from session and fill them in

        txtUserName.Text = EnteredUserName;
        txtPassword.Text = EnteredPassword;
        cbUseSSL.Checked = EnteredSsl;

        if (EnteredPort > 0)
        {
            txtPortNumber.Text = EnteredPort.ToString();
        }
        else
        {
            txtPortNumber.Text = String.Empty;
        }

        cbIsF5Node.Checked = IsF5Node;
        cbIsF5Node_CheckedChanged(this, EventArgs.Empty);

        ClearTestResultMessage();
    }

    private void CheckSameRememberIfNew(string value, ref string referenceValue, ref bool match)
    {
        if (referenceValue != value)
        {
            if (string.IsNullOrEmpty(referenceValue))
            {
                referenceValue = value;
            }
            else
            {
                match = false;
            }
        }
    }

    private void UpdateF5ApiSettingsInDbForNode(Node node)
    {
        if (!IsSnmpPollingMethod)
        {
            SWISVerbsHelper.InvokeV3("Orion.F5.System.Device", "DisableApiPolling", node.Id);
        }
        else if (AllowSettingsChange)
        {
            if (IsF5Node)
            {
                if (node.NodeSettings.ContainsKey(Strings.ReservedSslCertificateIdentitySettingName))
                {
                    var certException = node.NodeSettings[Strings.ReservedSslCertificateIdentitySettingName];
                    SWISVerbsHelper.InvokeV3("Orion.F5.System.Device", "EnableApiPolling", node.Id, EnteredPort,
                        EnteredSsl, EnteredUserName, EnteredPassword, certException);
                }
                else
                {
                    SWISVerbsHelper.InvokeV3("Orion.F5.System.Device", "EnableApiPolling", node.Id, EnteredPort,
                        EnteredSsl, EnteredUserName, EnteredPassword);
                }
            }
            else
            {
                SWISVerbsHelper.InvokeV3("Orion.F5.System.Device", "DisableApiPolling", node.Id);
            }
        }
    }

    private Engine GetOrionEngine(Node node)
    {
        return engineDal.GetOrionEngine(node.EngineID);
    }

    #endregion

    #region BL exception handling

    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    #endregion

    #region INodePropertyPlugin members

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        _propertyBag = pluginState;
        _nodes = nodes;
        _mode = mode;

        if (!IsPostBack)
        {
            FillFirstTimeValues();

            switch (_mode)
            {
                case NodePropertyPluginExecutionMode.EditProperies:
                    {
                        var pluginVisible = SetEditNodePluginVisibility(_nodes.Select(n => n.ID));

                        if (!pluginVisible)
                        {
                            return;
                        }

                        bool multipleNodes = _nodes.Count > 1;

                        //Handle Multiple nodes.
                        //The user has to explicitly check the box if they want to edit F5 iControl API credentials for multiple nodes.
                        if (multipleNodes)
                        {
                            MultipleSelectionVisible = true;
                            AllowSettingsChange = false;
                        }
                        else
                        {
                            MultipleSelectionVisible = false;
                            AllowSettingsChange = true;
                        }

                        string isPollingEnabled = string.Empty;
                        string userNameToFill = string.Empty;
                        string passwordToFill = string.Empty;
                        string portToFill = string.Empty;
                        string useSSLToFill = string.Empty;

                        bool areAllCredsSame = true;

                        foreach (var node in _nodes)
                        {
                            string value;
                            if (node.NodeSettings.TryGetValue(Strings.CredentialIdSettingName, out value))
                            {
                                cbIsF5Node_CheckedChanged(this, EventArgs.Empty);

                                int credentialId = -1;
                                if (Int32.TryParse(value, out credentialId) && -1 != credentialId)
                                {
                                    var apiCredentials = _credentialManager.GetCredential(credentialId);
                                    CheckSameRememberIfNew(apiCredentials.Username, ref userNameToFill, ref areAllCredsSame);
                                    CheckSameRememberIfNew(apiCredentials.Password, ref passwordToFill, ref areAllCredsSame);
                                }

                                if (node.NodeSettings.TryGetValue(Strings.PortSettingName, out value))
                                {
                                    CheckSameRememberIfNew(value, ref portToFill, ref areAllCredsSame);
                                }

                                if (node.NodeSettings.TryGetValue(Strings.UseSslSettingName, out value))
                                {
                                    CheckSameRememberIfNew(value, ref useSSLToFill, ref areAllCredsSame);
                                }

                                var pollingEnabled = GetF5PollingEnabled(node);
                                CheckSameRememberIfNew(pollingEnabled, ref isPollingEnabled, ref areAllCredsSame);
                            }

                            if (!areAllCredsSame)
                            {
                                break;
                            }
                        }

                        if (areAllCredsSame)
                        {
                            bool pollingEnabled;
                            bool.TryParse(isPollingEnabled, out pollingEnabled);
                            cbIsF5Node.Checked = pollingEnabled;
                            cbIsF5Node_CheckedChanged(this, EventArgs.Empty);

                            txtUserName.Text = userNameToFill;
                            EnteredUserName = userNameToFill;

                            EnteredPassword = passwordToFill;
                            txtPassword.Text = EnteredPassword;

                            int port;
                            if (!Int32.TryParse(portToFill, out port))
                            {
                                port = DEFAULT_API_PORT;
                            }
                            EnteredPort = port;
                            txtPortNumber.Text = port.ToString();

                            bool useSsl;
                            if (!Boolean.TryParse(useSSLToFill, out useSsl))
                            {
                                useSsl = DEFAULT_API_HTTPS;
                            }
                            EnteredSsl = useSsl;
                            cbUseSSL.Checked = useSsl;

                            IsAdvancedSettings = (EnteredPort != DEFAULT_API_PORT || EnteredSsl != DEFAULT_API_HTTPS);
                        }
                        else
                        {
                            if (multipleNodes)
                            {
                                F5ApiMultipleCredentialsWarning.Visible = true;
                            }
                        }

                        break;
                    }

                case NodePropertyPluginExecutionMode.WizDefineNode:
                    {
                        cbIsF5Node.Checked = true;
                        cbIsF5Node_CheckedChanged(this, EventArgs.Empty);
                        cbIsF5Node.Visible = false;

                        MultipleSelectionVisible = false;
                        AllowSettingsChange = true;
                        break;
                    }
                default:
                    {
                        MultipleSelectionVisible = false;
                        AllowSettingsChange = true;
                        break;
                    }
            }
        }
        else //If postback
        {
            //update data on non postback events (submits etc).
            UpdateData();
        }

        //When the test takes long time, a new page load is issued after it completes. This should
        //make the test message display properly even in that case.

        if (_propertyBag.ContainsKey(TESTRESULT_SESSIONID))
        {
            TestResult testMessageId = (TestResult)_propertyBag[TESTRESULT_SESSIONID];

            switch (testMessageId)
            {
                case TestResult.None:
                    ClearTestResultMessage();
                    break;

                case TestResult.Success:
                    ReportTestSuccess();
                    break;

                case TestResult.Failure:
                    ReportTestFailure("", false);
                    break;
            }
        }
    }

    private bool SetEditNodePluginVisibility(IEnumerable<int> nodeIds)
    {
        log.Debug("Determining visibility of F5 iControl 'Edit node' plugin.");
        var plugin = NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == PLUGIN_NAME);

        if (plugin == null)
        {
            throw new InvalidOperationException($"No 'Edit node' plugin named {PLUGIN_NAME} found.");
        }

        var supportedEngineTypes = plugin.SupportedPollingEngineTypes;
        var shouldBeVisible = IsF5iControlSupportedForNodes(nodeIds, supportedEngineTypes);

        log.Debug($"Visibility of F5 iControl 'Edit node' plugin will be set as '{shouldBeVisible}'.");

        plugin.Visible = shouldBeVisible;

        return shouldBeVisible;
    }

    public bool Update()
    {
        switch (_mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode: //Add node first step never calls update
                return true;

            case NodePropertyPluginExecutionMode.WizChangeProperties: //Add node last step
                if (IsF5Node && IsSnmpPollingMethod)
                {
                    UpdateF5ApiSettingsInDbForNode(NodeWorkflowHelper.Node);
                    ClearDataFromSession();
                }
                return true;

            case NodePropertyPluginExecutionMode.EditProperies:
                if (_nodes != null && _nodes.Count > 0)
                {
                    foreach (var node in _nodes)
                    {
                        UpdateF5ApiSettingsInDbForNode(node);
                    }
                }

                ClearDataFromSession();
                return true;

            default:
                return false;
        }
    }

    public bool Validate()
    {
        if (!ValidateInternal())
        {
            throw new ArgumentException(Resources.F5WebContent.F5WEBDATA_JT0_11);
        }

        return true;
    }

    private bool ValidateInternal()
    {
        if (!IsF5Node || !IsSnmpPollingMethod)
        {
            return true;
        }

        UpdateData();

        bool allOK = true;
        StringBuilder errorCodeBuilder = new StringBuilder();
        var multiplyNodes = _nodes.Count > 1;
        InvalidCertificatesMap.Clear();
        foreach (Node node in _nodes)
        {
            string nodeIp = node.IpAddress;
            string nodeDns = node.DNS;
            if (_nodes.Count == 1 && !string.IsNullOrEmpty(_propertyBag["__nodeIp"] as string))
            {
                nodeIp = _propertyBag["__nodeIp"].ToString();
            }

            if (string.IsNullOrEmpty(nodeDns))
            {
                nodeDns = DnsHelper.ReverseLookup(nodeIp);
            }

            var certException = node.NodeSettings.ContainsKey(Strings.ReservedSslCertificateIdentitySettingName) ? node.NodeSettings[Strings.ReservedSslCertificateIdentitySettingName] : null;
            var result = TestConnection(nodeDns, nodeIp, EnteredPort, EnteredUserName, EnteredPassword, EnteredSsl, node.EngineID, certException);
            allOK &= result.IsSuccessful;
            if (result.Error != null)
            {
                string helpUrl = HelpHelper.GetHelpUrl("orionphicontrolconfigissues");
                string message = string.Format(Resources.F5WebContent.F5WEDATA_EK0_6, helpUrl);

                switch (result.Error.Value)
                {
                    case F5ApiError.Unknown:
                        // intentionally left blank, we use the default message, no need to override it
                        break;
                    case F5ApiError.CertificateIsIncorrect:
                        message = Resources.F5WebContent.F5WEDATA_EK0_7;
                        InvalidCertificatesMap.Add(nodeIp, result.SslCertificateIdentity);
                        break;
                    case F5ApiError.CredentialsAreIncorrect:
                        message = Resources.F5WebContent.F5WEDATA_EK0_8;
                        break;
                    case F5ApiError.ServerIsUnavailable:
                        message = Resources.F5WebContent.F5WEDATA_EK0_9;
                        break;
                    case F5ApiError.ApiIsUnavailable:
                        message = Resources.F5WebContent.F5WEDATA_EK0_10;
                        break;
                    case F5ApiError.IncorrectApiResponse:
                        message = Resources.F5WebContent.F5WEDATA_EK0_11;
                        break;
                }
                if (multiplyNodes)
                {
                    errorCodeBuilder.AppendFormat(Resources.F5WebContent.F5WEDATA_EK0_12, node.Caption);
                }
                errorCodeBuilder.AppendLine(message);
                errorCodeBuilder.AppendLine("<br />");
            }
        }

        if (allOK)
        {
            ReportTestSuccess();
        }
        else
        {
            ReportTestFailure(errorCodeBuilder.ToString(), InvalidCertificatesMap.Any());
        }

        return allOK;
    }

    #endregion
}