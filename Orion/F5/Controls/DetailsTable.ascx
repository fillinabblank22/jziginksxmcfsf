﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DetailsTable.ascx.cs" Inherits="Orion_F5_Controls_DetailsTable" %>

<orion:Include ID="Include1" runat="server" Module="F5" File="DetailsTable.js" />
<orion:Include ID="Include2" runat="server" Module="F5" File="DetailsTable.css" />

<script type="text/javascript">
    $(function () {
        var o = SW.F5.DetailsTable.SetupTable('<%= ID %>', '<%= Route %>', '<%= Action %>', '<%= ElementId %>');
        $("#DetailsTableContent-<%= ID %>").data("sw-f5-genericTable", o);
    });
</script>

<script id="DetailsTableTemplate-<%= ID %>" type="text/x-template">
    <table class="NeedsZebraStripes">
    <tbody>
    {# _.each(rows, function(current, index) { #}
    <tr>
        <td class="PropertyHeader">{{ current.RowTitle }}</td>
        <td class="Property">{# if (current.RowContent) { #}{{ current.RowContent }}{# } else { #}-{# } #}</td>
    </tr>
    {# }); #}
    </tbody>
    </table>
</script>
<script id="DetailsTableError-<%= ID %>" type="text/x-template">
    <div class="sw-suggestion sw-suggestion-fail">
    <span class="sw-suggestion-icon"></span>
    {{ message }}
    </div>
</script>
<div id="DetailsTableContent-<%= ID %>" class="sw-f5-genericTable">
    
</div>