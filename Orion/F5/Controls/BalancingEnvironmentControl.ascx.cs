﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources;
using Solarwinds.F5.Common;
using SolarWinds.F5.Web.Interfaces.BE;
using SolarWinds.F5.Web.Models.BE;
using SolarWinds.F5.Web.StatePersistence.BalancingEnvironment;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_F5_Controls_BalancingEnvironmentControl : UserControl
{
    public ResourceInfo Resource { get; set; }

    private static readonly JavaScriptSerializer Serializer = new JavaScriptSerializer();
    private static readonly IBalancingEnvironmentStateManager SessionStateManager = new StateManager();

    protected string ActionsSerialized = Serializer.Serialize(new Dictionary<string, string>
    {
        {"GetData", "GetData"},
        {"RememberState", "RememberState"},
        {"GetSearchResults", "GetSearchResults"},
        {"SaveUserSettings", "SaveUserSettings"}
    });

    protected string ConfigSerialized;

    protected const string Route = "/sapi/BalancingEnvironment";

    public int ResourceID { protected get; set; }
    public PlaceHolder HeaderButtons { protected get; set; }
    public NetObject PreselectedObject { protected get; set; }
    public BalancingEnvironmentResourceConfig ResourceConfig { get { return _resourceConfig; } }

    private int ParentId { get { return PreselectedObject == null ? 0 : PreselectedObject.GetProperty<int>("ID"); } }
    private BalancingEnvironmentResourceConfig _resourceConfig;

    protected void Page_Load(object sender, EventArgs e)
    {
        _resourceConfig = GetResourceConfig();

        ConfigSerialized = Serializer.Serialize(_resourceConfig);

        AddSelectToHeader();
    }

    private BalancingEnvironmentResourceConfig GetResourceConfig()
    {
        var isPreselected = PreselectedObject != null;
        var selectedObject = isPreselected ? PreselectedObject.NetObjectID : GetSelectedObject();

        bool showOnlyProblems = GetShowOnlyProblems();
        bool showObjectCaptions = Resource != null && Convert.ToBoolean(Resource.Properties[Strings.ResourceProperyF5BEShowObjectCaptions]);

        var config = new BalancingEnvironmentResourceConfig
        {
            ParentId = ParentId,
            ResourceId = ResourceID,
            ShowObjectCaptions = showObjectCaptions,
            ShowStatusSummary = !isPreselected,
            ShowOnlyProblems = showOnlyProblems,
            SelectedObject = selectedObject,
            PreselectedObject = isPreselected,
        };

        return config;
    }

    private bool GetShowOnlyProblems()
    {
        if (OrionConfiguration.IsDemoServer)
        {
            var storedValue = HttpContext.Current.Session[Strings.WebUserSettingsF5BEShowOnlyProblems];
            return Convert.ToBoolean(storedValue);
        }
        else
        {
            var storedValue = WebUserSettingsDAL.Get(Strings.WebUserSettingsF5BEShowOnlyProblems);
            return Convert.ToBoolean(storedValue);
        }
    }

    private string GetSelectedObject()
    {
        var state = SessionStateManager.LoadState(new Context(ResourceID, ParentId));
        if (state != null)
        {
            return state.SelectedObject;
        }

        return null;
    }

    private void AddSelectToHeader()
    {
        if (HeaderButtons == null)
            return;

        var options = new List<ListItem>
        {
            new ListItem(F5WebContent.F5WEBDATA_PS0_2, "allObjects"),
            new ListItem(F5WebContent.F5WEBDATA_PS0_3, "onlyProblems")
        };
        var select = new HtmlSelect();
        select.ClientIDMode = ClientIDMode.Static;
        select.ID = "ShowObjects-" + ResourceID;
        select.Attributes.Add("class", "sw-be-dropdown");

        options.ForEach(li => select.Items.Add(li));

        select.SelectedIndex = _resourceConfig.ShowOnlyProblems ? 1 : 0;

        HeaderButtons.Controls.Add(select);
    }
}