﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_F5_Controls_ChangeRotationPresence : System.Web.UI.UserControl
{
    public int ResourceID
    {
        get { return this.ListDialog.ResourceID; }
        set { this.ListDialog.ResourceID = value; }
    }

    public string SWQL 
    { 
        get { return this.ListDialog.SWQL; } 
        set { this.ListDialog.SWQL = value; } 
    }

    public int NodeId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Profile.AllowAdmin)
        {
            WarningMessage = Resources.F5WebContent.F5WEDATA_EK0_25;
            this.divNote.Visible = false;
            this.btnSubmit.Visible = false;
        }
        else if (!CheckIfF5ApiCredentialsAreSupplied())
        {
            WarningMessage = string.Format(Resources.F5WebContent.F5WEDATA_EK0_26, GetF5NodeEditLink());
            this.divNote.Visible = false;
            this.btnSubmit.Visible = false;
        }
        else
        {
            this.btnSubmit.NavigateUrl = string.Format("javascript:SW.F5.ChangeRotationPresence.submit({0});", ResourceID);    
        }

        this.btnClose.NavigateUrl = string.Format("javascript:SW.F5.ChangeRotationPresence.closeDialog({0});", ResourceID);
    }
    
    public string HelpLinkFragment
    {
        get { return "orionphdisablef5poolmember"; }
    }

    public string WarningMessage { get; set; }

    protected bool CheckIfF5ApiCredentialsAreSupplied()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var query = @"SELECT ns.SettingName
                FROM Orion.NodeSettings ns
                WHERE ns.NodeID = @NodeId AND ns.SettingName = 'F5.API.CredentialID'";

            var resultsTable = swis.Query(query, new Dictionary<string, object>
            {
                { "NodeId", NodeId }
            });

            if (resultsTable.Rows.Count != 0)
            {
                return true;
            }
        }
        return false;
    }

    protected string GetF5NodeEditLink()
    {
        string returnUrl = "";
        if (Request.UrlReferrer != null)
        {
            returnUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
        }

        return string.Format("/Orion/Nodes/NodeProperties.aspx?Nodes={0}&ReturnTo={1}#F5ApiCredentialsSection", NodeId, returnUrl);
    }
}