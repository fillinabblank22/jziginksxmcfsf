﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="F5ApiCredentials.ascx.cs" Inherits="Orion_F5_Controls_F5ApiCredentials" %>
<%@ Reference Control="~/Orion/Nodes/Controls/PollingMethodSelector.ascx" %>
<%@ Register Src="~/Orion/Controls/PasswordTextBox.ascx"  TagPrefix="orion" TagName="PasswordTextBox" %>

<orion:Include ID="Include" runat="server" File="F5/Styles/F5ApiCredentials.css" />
<div class="contentBlock">
    <div class="contentBlockHeader" runat="server" id="F5ApiCredentialsHeader">
        <table>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
                    <asp:Literal runat="server" Text="<%$ Resources:F5WebContent,F5WEBDATA_JT0_01%>" />
                </td>
                <td class="rightInputColumn">
                    <asp:CheckBox ID="cbIsF5Node" runat="server" AutoPostBack="True" OnCheckedChanged="cbIsF5Node_CheckedChanged" Enabled="false" Text="<%$ Resources:F5WebContent,F5WEBDATA_JT0_02%>" />
                    <div class="helpfulText" style="padding: 0;" runat="server" id="F5ApiNoSNMPErrorMessage">
                        <%=Resources.F5WebContent.F5WEDATA_EK0_17%>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="F5ApiCredentialsSection"></div>
    <div class="blueBox" runat="server" id="F5ApiCredentialsBox">
        <asp:UpdatePanel runat="server" ID="F5ApiUpdatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <table runat="server" id="tableBasicSettings">

                    <tr>
                        <td class="leftLabelColumn">
                            <%=Resources.F5WebContent.F5WEBDATA_JT0_05%>:
                        </td>
                        <td class="rightInputColumn">
                            <asp:TextBox runat="server" ID="txtUserName" Width="250" AutoCompleteType="Disabled" />
                            <asp:RequiredFieldValidator ID="RequiredUserNameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="txtUserName" Text="<%$ Resources: F5WebContent, F5WEDATA_EK0_15 %>" ></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>

                    <tr>
                        <td class="leftLabelColumn">
                            <%=Resources.F5WebContent.F5WEBDATA_JT0_06%>:
                        </td>

                        <td class="rightInputColumn">
                            <orion:PasswordTextBox runat="server" ID="txtPassword" Width="250" IsEnabledPasswordTextBox="True" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>

                    <tr>
                        <td class="leftLabelColumn"></td>
                        <td class="rightInputColumn">
                            <asp:CheckBox runat="server" ID="cbAdvancedSettings" AutoPostBack="False" Text="<%$ Resources:F5WebContent,F5WEBDATA_JT0_14%>" CssClass="f5-api-credentials-checkbox" />
                        </td>
                    </tr>
                </table>
                <table runat="server" id="tableAdvancedSettings">
                    <tr>
                        <td class="leftLabelColumn">
                            <%=Resources.F5WebContent.F5WebCODE_JF0_003%>:
                        </td>
                        <td class="rightInputColumn">
                            <asp:TextBox runat="server" ID="txtPortNumber" Width="250" />
                            <asp:RangeValidator ID="rangePortNumberValidator" runat="server" ControlToValidate="txtPortNumber" ErrorMessage="(1-65535)" MaximumValue="65535" MinimumValue="1" Display="Dynamic" SetFocusOnError="True" Type="Integer" />
                            <asp:RequiredFieldValidator ID="requiredPortNumberValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="txtPortNumber" Text="<%$ Resources: F5WebContent, F5WEDATA_EK0_16 %>"></asp:RequiredFieldValidator>
                        </td>
                        <td class="helpfulText">
                            <asp:Label runat="server" AssociatedControlID="txtPortNumber" ID="txtPortHelp" Text="<%$ Resources:F5WebContent,F5WEBDATA_JT0_03%>" />
                        </td>
                    </tr>

                    <tr>
                        <td class="leftLabelColumn"></td>
                        <td class="rightInputColumn">
                            <asp:CheckBox runat="server" ID="cbUseSSL" Text="<%$ Resources:F5WebContent,F5WEBDATA_JT0_04%>" CssClass="f5-api-credentials-checkbox"></asp:CheckBox>
                        </td>
                        <td class="helpfulText"></td>
                    </tr>
                </table>
                <table runat="server" id="tableTestButton">
                    <tr>
                        <td class="leftLabelColumn"></td>
                        <td>
                            <orion:LocalizableButton ID="F5ApiTestButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: F5WebContent, F5WEBDATA_JT0_13 %>" CausesValidation="true" OnClick="TestButton_Click" />
                        </td>
                        <td>
                            <asp:UpdateProgress runat="server" ID="F5ApiTestUpdateProgress" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="F5ApiUpdatePanel" style="margin-left: 10px">
                                <ProgressTemplate>
                                        &nbsp;<img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                        <span style="font-weight:bold;"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_8 %>" /></span>                  
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                </table>
                <div runat="server" id="F5ApiTestSuccess" class="f5-api-test-message-ok" visible="false">
                    <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif"/><%=Resources.F5WebContent.F5WEBDATA_JT0_10%>
                </div>
                <div runat="server" id="F5ApiTestFailure" class="f5-api-test-message-warning" visible="false">
                    <img src="/Orion/images/nodemgmt_art/icons/icon_warning.gif"/>
                    <%= Resources.F5WebContent.F5WEBDATA_JT0_11 %><br/>
                    <asp:Label runat="server" ID="F5ApiTestFailureReason"></asp:Label>
                </div>
                <div runat="server" id="F5ApiTestInvalidCertificate" class="f5-api-test-message-warning" visible="false">
                    <br />
                    <%= Resources.F5WebContent.F5WEDATA_EK0_13 %><br/><br/>
                    <orion:LocalizableButton ID="F5ApiSslCertificateImport" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: F5WebContent, F5WEDATA_EK0_14 %>" CausesValidation="true" OnClick="SslCertificateImportButton_Click" />
                </div>
                <div runat="server" id="F5ApiMultipleCredentialsWarning" class="f5-api-test-message-warning" visible="false">
                    <%=Resources.F5WebContent.F5WEBDATA_JT0_12%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
