﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="F5EventsTable.ascx.cs" Inherits="Orion_F5_Controls_F5EventsTable" %>
<%@ Register Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" TagPrefix="orion" TagName="CustomQueryTable" %>

<div class="Events">
    <orion:CustomQueryTable runat="server" ID="CustomTable" />
</div>
<script type="text/javascript">
    $(function () {
        SW.Core.Resources.CustomQuery.initialize(
            {
                uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: false,
                        columnSettings: {
                            
                            "EventType": {
                                
                                header: '',
                                formatter: function (value, row, cellInfo) {

                                    var color = row[5];
                                    fixedColor = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
                                    fixedColorHex = fixedColor.toString(16);

                                    return "<div class=\"event-icon\" style=\"background-color:#" + fixedColorHex + ";\">"
                                        +"<img src=\"/NetPerfMon/images/Event-" + value + ".gif\" />"
                                        +"</div>";
                                },
                                isHtml: true,
                            },
                            "Message": {
                                header: '<%= Resources.F5WebContent.F5WEBDATA_LH0_16 %>',
                                formatter: function (value, row, cellInfo) {
                                    
                                    var netobjectType = row[0];
                                    var netobjectId = row[1];
                                    var nodeId = row[6];

                                    var netobject;
                                    var url;

                                    if(netobjectType !== undefined && netobjectId !== undefined)
                                    {
                                        netobject = netobjectType + ":" + netobjectId;
                                    } else if(nodeId !== undefined){
                                        netobject = "N:" + nodeId;
                                    }
                                    
                                    if(netobject !== undefined){
                                        url = "/orion/view.aspx?netobject=" + netobject;
                                    }

                                    if(url !== undefined){
                                        return "<a href=\"" + url + "\">" + value + "</a>";
                                    }

                                    return value;
                                },
                                isHtml: true
                            },
                            "EventTime": {
                                header: '<%= Resources.F5WebContent.F5WEBDATA_LH0_17 %>',
                            }
                        
                        }
                    });
 
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });    
</script>
