﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
    CodeFile="F5GTMWideIPDetails.aspx.cs" Inherits="Orion_F5_F5GTMWideIPDetails" Title="F5 Service Details" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="f5" Namespace="SolarWinds.F5.Web.UI" Assembly="SolarWinds.F5.Web" %>
<%@ Register TagPrefix="orion" TagName="ObsoleteF5DataBar" Src="~/Orion/F5/Controls/ObsoleteF5DataBar.ascx" %>
<%@ Reference Control="~/Orion/View.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
	<h1>
	    <%= F5WebContent.F5WEBDATA_JP0_12 %>:
        <asp:Image ID="GTMWideIPStatusIcon" runat="server" style="vertical-align: text-bottom" />
        <%= WideIPName %>
	</h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:ObsoleteF5DataBar runat="server" ID="ObsoleteDataMessageBar" />

    <f5:F5GTMWideIPResourceHost runat="server" ID="gtmWideIpHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</f5:F5GTMWideIPResourceHost>
</asp:Content>

