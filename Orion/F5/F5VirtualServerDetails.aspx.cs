﻿using System;
using System.Web;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;
using SolarWinds.F5.Web.NetObjects;
using SolarWinds.Shared;

public partial class Orion_F5_F5VirtualServerDetails : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var vs = NetObject as F5VirtualServer;
        if (vs == null)
            throw new IndexOutOfRangeException(string.Format(F5WebContent.F5WEBCODE_PS0_15, NetObject.GetType().Name, NetObject.NetObjectID));

        ((Orion_View)this.Master).HelpFragment = "orionphresourcef5virtualserverdetails";

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.VSStatusIcon.ImageUrl = "~" + StatusIconProvider.Instance.GetImagePath("Orion.F5.LTM.VirtualServer", (int)vs.OrionStatus, StatusIconSize.Small);
        this.VSStatusIcon.AlternateText = this.VSStatusIcon.ToolTip = StatusInfo.GetStatus((int)vs.OrionStatus).ShortDescription;

        this.VirtualServerName = HttpUtility.HtmlEncode(vs.ShortName);
        this.Title = string.Concat(this.ViewInfo.ViewTitle, " - ", this.VirtualServerName);
        ObsoleteDataMessageBar.NodeID = vs.NodeId;
    }

    protected string VirtualServerName { get; set; }

    public override string ViewType
    {
        get { return "F5VirtualServerDetails"; }
    }
}