﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using Resources;
using SolarWinds.Logging;
using SolarWinds.NetMan.Cisco.Web.Controllers;
using SolarWinds.NetMan.Cisco.Web.Credentials;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.NetMan.Cisco.Common.Credentials;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Model.SharedCredentials;

public partial class Orion_CiscoAci_Controls_ApicCheckBox : UserControl, INodePropertyPlugin
{
    private Dictionary<string, object> _propertyBag;
    private NodePropertyPluginExecutionMode _mode;
    private IList<Node> _nodes;

    private static readonly Log Log = new Log();

    public int NodePollingEngineId
    {
        get
        {
            return _nodes.First().EngineID;
        }
    }

    public int NodeOrionEngineId
    {
        get
        {
            return GetEngineId(_nodes.First());
        }
    }

    public string NodeIpAddress
    {
        get { return Regex.Replace(_nodes.First().IpAddress, @"\s+", ""); }
    }

    public bool IsInvalidCertificateApproved
    {
        get { return _nodes.First().NodeSettings.ContainsKey(INVALID_CERTIFICATES) &&
                    _nodes.First().NodeSettings[INVALID_CERTIFICATES] == NodeIpAddress; }
    }

    private const string ISAPIC_SESSIONID = "ApicCheckBox_IsApic";
    private const string WASAPIC_SESSIONID = "ApicCheckBox_WasApic";
    private const string USERNAME_SESSIONID = "ApicCredentials_Username";
    private const string PASSWORD_SESSIONID = "ApicCredentials_Password";
    private const string INVALID_CERTIFICATES = "ApicCredentials_InvalidCertificatesAccepted";

    private readonly string[] unsupportedEngineTypes = { "RemoteCollector" };

    private readonly IEngineDAL engineDal = new EngineDAL();

    private readonly Dictionary<int,int> orionEngineIdCache = new Dictionary<int, int>();

    private AciCredentialManager CreateAciCredentialManager(Node node)
    {
        return CreateAciCredentialManager(GetEngineId(node));
    }

    private AciCredentialManager CreateAciCredentialManager(int engineId)
    {
        return AciCredentialManager.CreateForEngine(engineId, engineDal);
    }

    public bool IsApicNode
    {
        get
        {
            if (!_propertyBag.ContainsKey(ISAPIC_SESSIONID))
            {
                _propertyBag[ISAPIC_SESSIONID] = false;
            }

            return (bool)_propertyBag[ISAPIC_SESSIONID];
        }
        set
        {
            _propertyBag[ISAPIC_SESSIONID] = value;
        }
    }

    public bool InitialApicNodeState
    {
        get
        {
            if (!_propertyBag.ContainsKey(WASAPIC_SESSIONID))
            {
                _propertyBag[WASAPIC_SESSIONID] = false;
            }

            return (bool)_propertyBag[WASAPIC_SESSIONID];
        }
        set
        {
            _propertyBag[WASAPIC_SESSIONID] = value;
        }
    }

    private string EnteredUsername
    {
        get
        {
            if (!_propertyBag.ContainsKey(USERNAME_SESSIONID))
            {
                _propertyBag[USERNAME_SESSIONID] = string.Empty;
            }

            return (string)_propertyBag[USERNAME_SESSIONID];
        }
        set { _propertyBag[USERNAME_SESSIONID] = value; }
    }

    private string EnteredPassword
    {
        get
        {
            if (!_propertyBag.ContainsKey(PASSWORD_SESSIONID))
            {
                _propertyBag[PASSWORD_SESSIONID] = string.Empty;
            }

            return (string)_propertyBag[PASSWORD_SESSIONID];
        }
        set { _propertyBag[PASSWORD_SESSIONID] = value; }
    }

    protected void cbIsApicNode_CheckedChanged(object sender, EventArgs e)
    {
        IsApicNode = cbIsApicNode.Checked;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        cbIsApicNode.Enabled = true;
        AciCredentialsSection.Visible = IsApicNode;
    }

    private void SetAciForNode(Node node)
    {
        CreateAciCredentialManager(node).AssignCredentialsToNode(UsernamePasswordCredentialsControl.UserName, UsernamePasswordCredentialsControl.Password, node.Id);
    }

    private void UpdateAciCredentials(Node node)
    {
        CreateAciCredentialManager(node).UpdateCredentialsOnNode(UsernamePasswordCredentialsControl.UserName, UsernamePasswordCredentialsControl.Password, node.Id);
    }

    private void DeleteAciForNode(Node node)
    {
        CreateAciCredentialManager(node).DeleteCredentialsOnNode(node.Id);
    }

    private bool GetIsAciPollingAssigned(Node node)
    {
        return CreateAciCredentialManager(node).AreCredentialsAssigned(node.Id);
    }

    private Action SslCertificateApproval()
    {
        Action result = () => _nodes.First().NodeSettings[INVALID_CERTIFICATES] = NodeIpAddress;
        return result;
    }

    private void UpdateSslCertificateApprovalInNodeSettings(Node node, string ipAddress)
    {
        CreateAciCredentialManager(node).SetNodeSettings(node.Id, INVALID_CERTIFICATES, ipAddress);
    }

    #region INodePropertyPlugin members

    public void Page_Load(object sender, EventArgs e)
    {
        UsernamePasswordCredentialsControl.RegisterAction(CredentialsValidationController.SSL_INVALID_CERTIFICATE_ACTION, SslCertificateApproval());
    }

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        _propertyBag = pluginState;
        _nodes = nodes;
        _mode = mode;

        if (!IsPostBack)
        {
            // fill first time values
            cbIsApicNode.Checked = IsApicNode;
            cbIsApicNode_CheckedChanged(this, EventArgs.Empty);
            UsernamePasswordCredentialsControl.UserName = EnteredUsername;
            UsernamePasswordCredentialsControl.Password = EnteredPassword;

            switch (_mode)
            {
                case NodePropertyPluginExecutionMode.EditProperies:
                    if (_nodes != null && _nodes.Count == 1)
                    {
                        var serverTypeSupported = !unsupportedEngineTypes.Contains(GetEngineType());

                        //show on Edit node page, the plugin name has to be the same as defined in the plugin file
                        foreach (var plugin in NodePropertyPluginManager.Plugins.Where(p => p.Name == "ApicCheckBox"))
                        {
                            plugin.Visible = serverTypeSupported;
                        }

                        if (!serverTypeSupported)
                        {
                            return;
                        }

                        cbIsApicNode.Checked = InitialApicNodeState = GetIsAciPollingAssigned(_nodes[0]);
                        cbIsApicNode_CheckedChanged(this, EventArgs.Empty);

                        if (IsApicNode)
                        {
                            var nodeUri = CreateAciCredentialManager(_nodes.First()).GetNodeSwisUri(_nodes[0].Id);
                            UsernamePasswordCredentialsControl.LoadUserNamePasswordCredentialsForOrionEntity(
                                CredentialConsts.CredentialRelationUse, nodeUri);
                        }
                    }
                    break;
                case NodePropertyPluginExecutionMode.WizDefineNode:
                    cbIsApicNode.Checked = true;
                    cbIsApicNode_CheckedChanged(this, EventArgs.Empty);
                    cbIsApicNode.Visible = false;
                    cbIsApicDescription.Visible = false;
                    break;
                default:
                    break;
            }
        }
        else
        {
            EnteredUsername = UsernamePasswordCredentialsControl.UserName;
            EnteredPassword = UsernamePasswordCredentialsControl.Password;
        }
    }

    public bool Update()
    {
        switch (_mode)
        {
            case NodePropertyPluginExecutionMode.WizChangeProperties: // Add node last step
                if (cbIsApicNode.Checked)
                {
                    SetAciForNode(NodeWorkflowHelper.Node);
                }
                return true;

            case NodePropertyPluginExecutionMode.EditProperies:
                if (_nodes != null && _nodes.Count == 1)
                {
                    if (cbIsApicNode.Checked && !InitialApicNodeState)
                    {
                        SetAciForNode(_nodes[0]);
                        UpdateSslCertificateApprovalInNodeSettings(_nodes[0], NodeIpAddress);
                    }
                    else if (cbIsApicNode.Checked)
                    {
                        UpdateAciCredentials(_nodes[0]);
                        UpdateSslCertificateApprovalInNodeSettings(_nodes[0], NodeIpAddress);
                    }
                    else if (!cbIsApicNode.Checked && InitialApicNodeState)
                    {
                        DeleteAciForNode(_nodes[0]);
                        UpdateSslCertificateApprovalInNodeSettings(_nodes[0], "0");
                    }
                }
                return true;

            default:
                return true;
        }
    }

    public bool Validate()
    {
        if (cbIsApicNode.Checked)
        {
            var sharedCredentials = new SharedCredential
                                    {
                                        Username = UsernamePasswordCredentialsControl.UserName,
                                        Password = UsernamePasswordCredentialsControl.Password
                                    };

            var sharedCredentialsWithIp = new CredentialsValidationData
                                          {
                                              HostOrIpAddress = NodeIpAddress,
                                              Credentials = sharedCredentials,
                                              EngineId = NodeOrionEngineId,
                                              IsInvalidCertificateApproved = IsInvalidCertificateApproved
                                          };

            var credentilValidationControler = new CredentialsValidationController();
            var result = credentilValidationControler.ValidateCredentialsForEngine(sharedCredentialsWithIp);

            if (result.ValidationResultState == ValidationResultState.Error)
            {
                throw new ArgumentException($"{NetManCiscoWebContent.ErrorMessage_Apic_AddNode_TestCredentials}: {result.Message}");
            }
            else if (result.ValidationResultState == ValidationResultState.Warning)
            {
                throw new ArgumentException($"{NetManCiscoWebContent.ErrorMessage_Apic_InvalidCertificate_TestCredentials}");
            }
            return true;
        }
        return true;
    }

    private string GetEngineType()
    {
        return engineDal.GetEngine(NodePollingEngineId).ServerType;
    }

    private int GetEngineId(Node node)
    {
        var nodeEngineId = node.EngineID;

        int effectiveEngineId;
        if (orionEngineIdCache.TryGetValue(nodeEngineId, out effectiveEngineId))
        {
            return effectiveEngineId;
        }

        effectiveEngineId = engineDal.GetOrionEngine(nodeEngineId).EngineID;
        orionEngineIdCache[nodeEngineId] = effectiveEngineId;
        return effectiveEngineId;
    }
    #endregion
}
