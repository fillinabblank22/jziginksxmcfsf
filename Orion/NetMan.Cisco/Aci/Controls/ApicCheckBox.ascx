﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApicCheckBox.ascx.cs" Inherits="Orion_CiscoAci_Controls_ApicCheckBox" %>
<%@ Import Namespace="SolarWinds.Orion.NetMan.Cisco.Common.Credentials" %>

<%@ Register Src="~/Orion/Controls/UsernamePasswordCredentialsControl.ascx" TagPrefix="orion" TagName="UsernamePasswordCredentialsControl" %>

<div id="aciCredentialsNodeProperties" data-engine-id="<%= NodeOrionEngineId %>" data-is-invalid-certificate-approved="<%= IsInvalidCertificateApproved %>" style="display: none;"></div>

<div class="contentBlock">
    <div class="contentBlockHeader" runat="server" id="ApicCheckBoxHeader">
        <table>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:Literal ID="lCiscoAci" runat="server" Text="<%$ Resources:NetManCiscoWebContent,EditNode_Apic_Header%>" />
                </td>
                <td class="rightInputColumn">
                    <asp:CheckBox ID="cbIsApicNode" runat="server" AutoPostBack="True" OnCheckedChanged="cbIsApicNode_CheckedChanged" Enabled="false" />
                    <asp:Literal ID ="cbIsApicDescription" runat="server" Text="<%$ Resources:NetManCiscoWebContent,EditNode_Apic_Description%>" />
                </td>
            </tr>
        </table>
    </div>
    
    <div id="AciCredentialsSection" runat="server" class="blueBox">
        <div>
            <orion:UsernamePasswordCredentialsControl
                runat="server"
                CredentialRelationUse="<%# CredentialConsts.CredentialRelationUse %>"
                ID="UsernamePasswordCredentialsControl"
                HideFailedTestDetailsLink="True"
                JavaScriptTestCredentialsHandler="function(credentials, callback) 
                {
                    var nodeProperties = $('#aciCredentialsNodeProperties');

                    var hostOrIpInputBox = document.querySelector('[id*=txtHostNameIP]') ||
                                           document.querySelector('[id*=pollingIP]') ||
                                           document.querySelector('[id*=NodeNameIP_tbIP]');
                    
                    if (hostOrIpInputBox === null) {
                        throw 'Host or IP address input box not found.';
                    }

                    var hostOrIpAddress = hostOrIpInputBox.value ? hostOrIpInputBox.value : hostOrIpInputBox.innerHTML;
                    hostOrIpAddress = hostOrIpAddress.replace(/\s/g, ''); 

                    var engineIdSelectBox = document.querySelector('[id*=ddlPollingEngine]');

                    var engineId;
                    
                    // Engine select box is displayed only in first phase of Add Node Wizard if user has some APs
                    if (engineIdSelectBox === null)
                    {
                        engineId = nodeProperties.data('engineId'); 
                    } else {
                        var selectedOptionIndex = engineIdSelectBox.selectedIndex;
                        engineId = engineIdSelectBox.options[selectedOptionIndex].value;
                    }
                    
                    var isInvalidCertificateApproved = nodeProperties.data('isInvalidCertificateApproved');

                    SW.Core.Services.callControllerAction('/sapi/CredentialsValidation/',
                                                            'ValidateCredentialsForEngine',
                                                            { 
                                                                Credentials: credentials, 
                                                                HostOrIpAddress: hostOrIpAddress,
                                                                EngineId: engineId,
                                                                IsInvalidCertificateApproved: isInvalidCertificateApproved,
                                                            },
                                                            callback,
                                                            console.error);
                }"
            />
        </div>
    </div>
</div>