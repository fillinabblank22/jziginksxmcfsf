<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceNotFound.ascx.cs" Inherits="ResourceNotFound" %>

<div class="Error">The "<%= DefaultSanitizer.SanitizeHtml(Resource.Title) %>" resource is missing.<br>The file <%= DefaultSanitizer.SanitizeHtml(Resource.File) %> is not found.<br><br></div>
