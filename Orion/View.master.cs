﻿using System;
using System.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_View : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AngularJsHelper.ApplyNgNonBindable(titleTableCaption);
        
        if (!string.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }
    }

	public string GetRequestString()
	{
		return
            string.Format("/Orion/Admin/CustomizeView.aspx?ViewID={0}{1}&ReturnTo={2}",
                View.ViewInfo.ViewID,
                (!string.IsNullOrEmpty(Request["NetObject"]) ? string.Format("&NetObject={0}", HttpUtility.UrlEncode(Request["NetObject"])) : ""),
                View.ReturnUrl);
	}

    public OrionView View => Page as OrionView;
    
    public string HelpFragment { get; set; }
}
