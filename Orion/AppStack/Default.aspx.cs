﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using Castle.Windsor;
using SolarWinds.AppStack;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Model.FieldPicker;
using SolarWinds.Orion.Web.Services;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Orion.Core.Common.Swis;

public partial class Orion_AppStack_Default : SolarWinds.AppStack.Web.AppStackHomePage
{
    protected bool SpotlightEnabled { get; set; }

    protected Guid TraceId { get; set; }

    protected string LayoutTitle
    {
        get
        {
            return Layouts.Settings.LayoutDetails.IsLoadedMinistack
                ? AppStackDefaults.WebResourceTitle
                : Layouts.Settings.LayoutDetails.Title;
        }
    }

    protected string LocalizedImageFolder
    {
        get
        {
            return Resources.CoreWebContent.CurrentHelpLanguage.Equals("en", StringComparison.InvariantCultureIgnoreCase) ? "" : $"{Resources.CoreWebContent.CurrentHelpLanguage}/";
        }
    }

    public Orion_AppStack_Default()
        :this(null) {}

    /// <summary>
    /// Constructor - IoC
    /// </summary>
    public Orion_AppStack_Default(IWindsorContainer container)
        :base(container) {}

    protected override void OnInit(EventArgs e)
    {
        if (HandleWebServicePageLoad())
            return;

        if (HandleReconfigurePageLoad())
            return;

        var factory = new AppStackFactory();

        var config = factory.Config().InitForWebPage();

        var environment = factory.Environment(config);

        var utility = factory.UiUtility();

        FieldPickerCtrl.DataSource = new DataSource
        {
            RefId = Guid.NewGuid(),
            MasterEntity = "Orion.Nodes",
            Type = DataSourceType.Entities,
            DynamicSelectionType = DynamicSelectionType.Simple,
            Name = "All Nodes"
        };
        FieldFilterCtrl.RotatedTitleImage = $"/orion/appstack/images/{LocalizedImageFolder}text.filterpaneltitle.png";

        TraceId = config.SessionId.Value;
        Title = SolarWinds.AppStack.Strings.Archive.AppStack_SummaryPageTitle;
        SpotlightEnabled = environment.Settings.SpotlightEnabled;

        if (environment.Settings.ShowNames)
        {
            var file = OrionInclude.CoreFile("orionmasterpage.js");
            if (file != null) file.AddJsInit("$(document.documentElement).addClass('sw-appstack-shownames')");
        }

        if (utility.IsDebugModeEnabled)
        {
            var file = OrionInclude.CoreFile("orionmasterpage.js");
            if (file != null) file.AddJsInit("$(document.documentElement).addClass('sw-appstack-debug')");
        }

        Sections.Environment = environment.AllSectionsPartial(config.SelectedIds);

        if (Sections.Environment.SwisErrors != null && Sections.Environment.SwisErrors.Any())
        {
            Sections.ErrorMessages = Sections.Environment.SwisErrors.Select(err => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(err)).ToList();
        }
		
		var countView = environment.AllCountsPartial();

		FieldFilterCtrl.Model = countView.Model;
		FieldFilterCtrl.ViewModel = Sections.Environment.Settings.FilterPanelSettings;
		this.appstackcrumbs.Model = countView.Model;

		var settingsController = factory.SettingsController();

		Layouts.Model = settingsController.All(Sections.Environment.Settings.LayoutDetails.ResourceId);
		Layouts.Settings = Sections.Environment.Settings;	

        base.OnInit(e);
    }

    public string ReturnUrl
    {
        get
        {
            return ReferrerRedirectorBase.GetReturnUrl();
        }
    }
}
