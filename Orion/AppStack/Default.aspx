<%@ Page Language="C#" AutoEventWireup="true" CodeFileBaseClass="SolarWinds.AppStack.Web.AppStackHomePage" CodeFile="Default.aspx.cs" Inherits="Orion_AppStack_Default" Title="AppStack" MasterPageFile="~/Orion/OrionMasterPage.master"  %>
<%@ Register TagPrefix="appstack" Namespace="SolarWinds.AppStack.Web" assembly="SolarWinds.AppStack" %>
<%@ Register TagPrefix="orion" TagName="FieldFilterPanel" Src="~/Orion/Controls/FieldFilterPanel.ascx" %>
<%@ Register TagPrefix="orion" TagName="FieldPicker" Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="appstack" TagName="LayoutMenu" Src="~/Orion/Appstack/Controls/LayoutMenu.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
	<!-- <div class="sw-appstack-refresheditor" style="display: inline-block;">Update Interval: <span id="appstackrefreshrate">*</span></div> -->
    <a href="#" onclick="return false;" class="sw-appstack-dd sw-pg-stackhide sw-menuoption-selectable" id="appstack-dd-views" style="background-position: center right; padding-left: 4px; padding-right: 20px; font-weight: bold;"><%: LayoutTitle %></a>
    <orion:IconHelpButton runat="server" HelpUrlFragment="samagappstacktop" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <orion:Include runat="server" File="charts/js/allcharts.js" Section="Bottom" />

    <orion:InlineCss runat="server">
        /* *_pdfsig_4C5187C5165A4075878201F882534984__4C5187C5165A4075878201F882534984_pdfsig_/
            #userName, #evalMsg, #tabs, .sw-pg-stackhide, .sw-hdr-links-export, .sw-appstack-topleft-links, #ogs-container, #hubbleWindow, .sw-hdr-links-export { display: none; }
        /* */
    </orion:InlineCss>
    
    <div id="appstackframe" sw:isfullappstackpage="1" style="padding: 0 10px 0 0;"><div>
        <appstack:LayoutMenu ID="Layouts" runat="server"/>

        <div style="margin: 6px 0 6px 14px; padding: 0; display: inline-block;">
            <table style="border-collapse: collapse;">
            <tr style="vertical-align: top;">
                <td class="sw-appstack-pgtitle">
                    <span><%= SolarWinds.AppStack.Strings.Archive.AppStack_SummaryPageTitleAppStack %></span>
                </td>
                <td class="sw-appstack-topleft-links">
                    <a class='sw-link appStackExpandAll'  ext:qtip="<%: SolarWinds.AppStack.Strings.Archive.AppStack_ExpandAllTooltip %>" style="background-image: url(/orion/appstack/images/expand.png);"><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbExpandAll %></a>
                    <a class='sw-link appStackCollapseAll' ext:qtip="<%: SolarWinds.AppStack.Strings.Archive.AppStack_CollapseAllTooltip%>" style="background-image: url(/orion/appstack/images/collapse.png);"><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbCollapseAll %></a>
                    <a class='sw-link appStackShowNames' style="padding-left: 13px;">
                        <span class="sw-appstack-showname" ext:qtip="<%: SolarWinds.AppStack.Strings.Archive.AppStack_HideNamesIsOnHint %>"><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbShowNames %></span>
                        <span class="sw-appstack-hidename" ext:qtip="<%: SolarWinds.AppStack.Strings.Archive.AppStack_HideNamesIsOffHint %>"><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbHideNames %></span>
                    </a>
                    <a class='sw-link sw-appstack-debuglink' href='Debug.aspx'>Debug</a>
                </td>
            </tr>
            </table>
        </div>

        <div id="appstackmenuframe" style="margin-top: 4px;margin-bottom:15px" sw:traceid="<%: TraceId.ToString("N") %>">
            
        <table style="width: 100%; border-collapse: collapse;">
            <tr>
                <td style="vertical-align: top; padding-left: 10px;border-bottom: solid 1px #D5D5D5;" class="sw-pg-stackhide sw-appstack-filter-panel">
                    <orion:FieldFilterPanel ID="FieldFilterCtrl" 
                        TitleText="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_NarrowYourEnvironment %>"
                        FieldPickerInstanceId="Appstack" 
                        UseValuePicker="True"
                        runat="server" />
                    <orion:FieldPicker ID="FieldPickerCtrl" ClientInstanceName="Appstack" runat="server" DataProviderUrl="/orion/appstack/default.aspx?op=" />
                </td>
                <td style="width: 6px; vertical-align: top; border-left: 1px solid #D5D5D5; ">&nbsp;&nbsp;</td>
                <td style="width: 100%; vertical-align: top;">
                    
                <orion:FilterCrumbs runat="server" ID="appstackcrumbs" ClientIDMode="Static" />

            <div id="appstackoverview" style="margin: 2px; " class="sw-appstack-sel-pane">

                <table style="border-collapse: collapse; width: 100%;">
                    <tr style="vertical-align: middle;">
                        <td style="width: 1px; white-space: nowrap; padding-left: 10px; padding-right: 10px; vertical-align: middle; background: url(/orion/appstack/images/pane.splitter.png) center right no-repeat;">
                            <div class="sw-appstack-sel-text" sw:textempty="<%: SolarWinds.AppStack.Strings.Archive.AppStack_SelectionIsEmptyHint %>" sw:textsome="<%: SolarWinds.AppStack.Strings.Archive.AppStack_SelectionIsNotEmptyHint %>"></div>
                            <div class="sw-appstack-sel-items"></div>
                        </td>
                        <td style="padding-left: 10px; white-space: nowrap;" class="sw-appstack-chart-parent"><span class="sw-appstack-overview-label" style="text-transform: uppercase; color: #888; font-size: 11px;"><%: SolarWinds.AppStack.Strings.Archive.AppStack_OverviewTitle %></span> <div class="sw-appstack-chart"></div></td>
                        <td style="width: 1px; white-space: nowrap; font-size: 11px; background: url(/orion/appstack/images/pane.splitter.png) center right no-repeat; padding-right: 10px;"><div class="sw-appstack-pie-count" sw:textselection="<%: SolarWinds.AppStack.Strings.Archive.AppStack_LbObjectCountSelected %>" sw:textoverview="<%: SolarWinds.AppStack.Strings.Archive.AppStack_LbObjectCountOverview %>"></div></td>
                        <td style="padding-left: 16px; font-size: 11px;"><span style="text-transform: uppercase; color: #888;"><%: SolarWinds.AppStack.Strings.Archive.AppStack_lbIssues %></span>
                            <div class="sw-appstack-issues"></div>
                        </td>
                        <td id="appstackselectionoptions" style="padding-left: 10px; padding-right: 10px; font-size: 11px; text-align: right; width: 1px; white-space: nowrap; vertical-align: middle; background: url(/orion/appstack/images/pane.splitter.png) center left no-repeat;">
                            <a class='sw-appstack-spotlight sw-menuoption-selectable <%: this.SpotlightEnabled ? "sw-menuoption-selected" : "" %>' href="#" ext:qtip="<%: this.SpotlightEnabled ? SolarWinds.AppStack.Strings.Archive.AppStack_SpotlightIsOnHint : SolarWinds.AppStack.Strings.Archive.AppStack_SpotlightIsOffHint %>" onclick="return false;" style="line-height: 18px;"><img src="/orion/appstack/images/pane.toggledimmed.png" style="vertical-align: middle;"/> <%: SolarWinds.AppStack.Strings.Archive.AppStack_lbSpotlight %></a>
                            <a class='sw-link sw-appstack-clearselect' href="#" onclick="return false;" ext:qtip="Clears the active selection" style="line-height: 18px; margin-left: 8px;">X</a>
                        </td>
                    </tr>
                </table>
            </div>

        <appstack:IconSectionFrameControl runat="server" ID="Sections"/>
                </td>
            </tr>
        </table>
        </div>
    </div></div>
    
    <div id="appstackmsg" style="display: none;">
        <span class="sw-pg-msg-objects" style="display: none;"><%: SolarWinds.AppStack.Strings.Archive.AppStack_MessageFetchingObjects %></span>
        <span class="sw-pg-msg-counts" style="display: none;"><%: SolarWinds.AppStack.Strings.Archive.AppStack_MessageFetchingCounts %></span>
        <span class="sw-pg-msg-error" style="display: none;"><%: SolarWinds.AppStack.Strings.Archive.AppStack_MessageErrorSeen %></span>
        <a id="appstackmsgx" href="#" onclick="return false;">X</a>
    </div>

<orion:InlineCss runat="server">
    <%--// your css (inlined) goes here--%>
    .sw-appstack-topleft-links a { margin-right: 8px; background-position: left center; padding-left: 15px; background-repeat: no-repeat; font-size: 11px; }
    .appStackShowNames { background-image: url(/orion/appstack/images/names.show.png); }
    .sw-appstack-shownames .appStackShowNames { background-image: url(/orion/appstack/images/names.hide.png); }
    .appStackShowNames .sw-appstack-hidename, .sw-appstack-shownames .appStackShowNames .sw-appstack-showname { display: none; }
    .sw-appstack-shownames .appStackShowNames .sw-appstack-hidename { display: inline; }
</orion:InlineCss>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="outsideFormPlaceHolder" Runat="Server">
</asp:Content>
