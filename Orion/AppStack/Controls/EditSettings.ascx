﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditSettings.ascx.cs" Inherits="Orion_AppStack_Controls_EditSettings" %>
<%@ Import Namespace="SolarWinds.AppStack" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" assembly="OrionWeb" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<Orion:Include runat="server" File="AppStack\appstack.css" />
<div class="sw-appstack-settings-frame">
	<div>
	    <asp:PlaceHolder ID="NamingSection" Visible="False" runat="server">
		<div class="sw-pg-ctrl-section" style="padding-bottom: 20px;">
		    <p><strong><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbLayoutName %></strong> <asp:Literal ID="LbLayoutLabel" ClientIDMode="Static" Visible="false" runat="server"></asp:Literal></p> <asp:TextBox ID="TxtLayoutName" ClientIDMode="Static" runat="server"></asp:TextBox>
		</div>
        </asp:PlaceHolder>
        
        <table class="sw-appstack-settings-table">
            <tr>
                <td>
                    <p><strong><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbUpLimit%></strong></p>
                    <div>
                        <asp:RangeValidator runat="server" Type="Integer" MinimumValue="0" MaximumValue="250" ControlToValidate="txtUpLimit" 
                                    ErrorMessage="Value must be an integer number between 0 and 250" Display="Dynamic" />
                    </div>
                    <asp:CheckBox ID="cbUpLimit" runat="server" Text="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_RadioUpLimit1 %>" />
			        <asp:TextBox ID="txtUpLimit" runat="server" CssClass="sw-appstack-settings-uplimit-count" />    
                </td>
                <td class="sw-appstack-settings-summary-info">
                    <%: SolarWinds.AppStack.Strings.Archive.AppStack_UpNotice %> &nbsp; <orion:HelpLink ID="lbHelpUpLimit" runat="server" HelpUrlFragment="SAMAGAppStackUpLearnMore" CssClass="coloredLink" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p><strong><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbObjectNames %></strong></p>
                    <asp:CheckBox ID="cbShowNames" runat="server" Text="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_HideNamesIsOnHint %>" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p><strong><%: SolarWinds.AppStack.Strings.Archive.AppStack_AlignObjects %></strong></p>
                    <asp:RadioButton ID="rdAlignLeft" runat="server" GroupName="radioalign" />
                    <label for="<%: rdAlignLeft.ClientID %>">
		                <div class="sw-appstack-settings-align">
			                <%: SolarWinds.AppStack.Strings.Archive.AppStack_AlignLeft %>
			                <div class="sw-appstack-settings-align-left"></div>
		                </div>
                    </label>
                    <asp:RadioButton ID="rdAlignCenter" runat="server" GroupName="radioalign" />
                    <label for="<%: rdAlignCenter.ClientID %>">
			            <div class="sw-appstack-settings-align">
				            <%: SolarWinds.AppStack.Strings.Archive.AppStack_AlignCenter %>
				            <div class="sw-appstack-settings-align-center"></div>
			            </div>
                    </label>
                    <asp:RadioButton ID="rdAlignRight" runat="server" GroupName="radioalign" />
                    <label for="<%: rdAlignRight.ClientID %>">
			            <div class="sw-appstack-settings-align">
				            <%: SolarWinds.AppStack.Strings.Archive.AppStack_AlignRight %>
				            <div class="sw-appstack-settings-align-right"></div>
			            </div>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbSummary %></strong></p>
                    <asp:RadioButton ID="rdSummaryAlways" runat="server" GroupName="radioshowsummary" Text="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_SummaryShowAlways %>" />
                    <br />
                    <asp:RadioButton ID="rdSummaryWhenCollapsed" runat="server" GroupName="radioshowsummary" Text="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_SummaryShowWhenCollapsed %>" />    
                </td>
                <td class="sw-appstack-settings-summary-explanation"><p class="sw-appstack-settings-summary-explanation-text"><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbSummaryImageText %></p></td>
            </tr>
            <tr>
                <td>
                    <p><strong><%: SolarWinds.AppStack.Strings.Archive.AppStack_LbEmptyCategories %></strong></p>
                    <asp:CheckBox ID="cbHideEmpty" runat="server" Text="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_HideEmptyCategories %>" />
                </td>
                <td class="sw-appstack-settings-summary-info">
                    <%: SolarWinds.AppStack.Strings.Archive.AppStack_EmptyCategoriesNotice %>
                    <p><orion:HelpLink ID="lbHelpEmptyCategories" runat="server" HelpUrlFragment="SAMAGAppStackCAD" CssClass="coloredLink" /></p>
                    <p runat="server" id="MinistackHint" Visible="False"><%= SolarWinds.AppStack.Strings.Archive.AppStack_MinistackHint %></p>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td class="sw-appstack-settings-rate">
                    <p><strong><%: SolarWinds.AppStack.Strings.Archive.AppStack_UpdateInterval %></strong></p>
			        <div id="minutes" runat="server" EnableViewState="false" class="sw-appstack-settings-minutes">
			            <asp:TextBox ID="RefreshMinutes" ClientIDMode="Static" runat="server" /> <%: SolarWinds.AppStack.Strings.Archive.AppStack_LbMinutes %>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="RefreshMinutes" ErrorMessage="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_LbMustBeNumber %>" />
                    </div>
			        <div id="seconds" runat="server" EnableViewState="false" class="sw-appstack-settings-seconds">
			            <asp:TextBox ID="RefreshSeconds" ClientIDMode="Static" runat="server" /> <%: SolarWinds.AppStack.Strings.Archive.AppStack_LbSeconds %>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="RefreshSeconds" ErrorMessage="<%$ Code: SolarWinds.AppStack.Strings.Archive.AppStack_LbMustBeNumber %>" />
                    </div>
                </td>
                <td style="padding-top: 1em;"><div class="sw-appstack-settings-summary-info">
                    <asp:Label runat="server" ID="IntervalPlaceholder" EnableViewState="false" />
                </div></td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
    function updateUpLimitValue() {
        $('#<%= txtUpLimit.ClientID %>').val($('#<%= cbUpLimit.ClientID %>').attr('checked') ? '<%= AppStackDefaults.UpLimitCount %>' : '');
    }

    function updateUpLimitEnabled() {
        var txt = $('#<%= txtUpLimit.ClientID %>').val();
        $('#<%= cbUpLimit.ClientID %>').attr('checked', (txt && txt != '') ? true : false);
    }

    $('#<%= cbUpLimit.ClientID %>').click(updateUpLimitValue);
    $('#<%= txtUpLimit.ClientID %>').keyup(updateUpLimitEnabled);
</script>