﻿using System;
using System.Globalization;
using System.Web.Security.AntiXss;
using System.Web.UI;
using SolarWinds.AppStack.Contract.Models;
using SolarWinds.AppStack.Interfaces;
using SolarWinds.AppStack.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_AppStack_Controls_EditSettings : UserControl
{
    public const string LinkStart = "&raquo; ";

    public AppStackVisualSettings Model { get; set; }
    public AppStackVisualSettings Settings { get; set; }
    public bool ShowLayoutName { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        double d = WebSettingsDAL.AutoRefreshSeconds / 60.0;
        IntervalPlaceholder.Text = string.Format(SolarWinds.AppStack.Strings.Archive.AppStack_UpdateIntervalTip, d);
    }

    public IValidator Bind(AppStackPresenterBindDirection direction)
    {
        lbHelpEmptyCategories.HelpDescription = LinkStart + SolarWinds.AppStack.Strings.Archive.AppStack_EmptyCategoriesHelpLink;
        lbHelpUpLimit.HelpDescription = LinkStart + SolarWinds.AppStack.Strings.Archive.AppStack_LearnMore;

        if (ShowLayoutName && !Model.LayoutDetails.IsLoadedMinistack)
        {
            NamingSection.Visible = true;
            TxtLayoutName.Visible = Model.LayoutDetails.ResourceDetails != null;
            LbLayoutLabel.Visible = !TxtLayoutName.Visible;
        }

        var defSeconds = SolarWinds.Orion.Web.DAL.WebSettingsDAL.AutoRefreshSeconds;

        if (direction == AppStackPresenterBindDirection.PresenterToModel)
        {
            // populate the user control's settings

            if (cbUpLimit.Checked)
            {
                var txt = string.IsNullOrWhiteSpace(txtUpLimit.Text) ? "0" : txtUpLimit.Text;

                int count;

                if (!int.TryParse(txt, out count) || count < 0)
                {
                    return new AppStackBindFailure { ErrorMessage = "Up object limit is invalid" };
                }

                Model.NonIssueObjectLimit = count;
            }
            else
            {
                Model.NonIssueObjectLimit = null;
            }

            Model.ShowNames = cbShowNames.Checked;
            Model.AlwaysShowSummaryCounts = rdSummaryAlways.Checked;
            Model.HideEmptyCategories = cbHideEmpty.Checked;

            if (rdAlignCenter.Checked)
                Model.IconAlignment = AppStackAlign.Center;
            else if (rdAlignRight.Checked)
                Model.IconAlignment = AppStackAlign.Right;
            else
                Model.IconAlignment = AppStackAlign.Left;

            if (ShowLayoutName && Model.LayoutDetails.ResourceDetails != null)
            {
                Model.LayoutDetails.Title = TxtLayoutName.Text;
            }

            int ttlS = 0;
            double vm = 0;
            if (double.TryParse(RefreshMinutes.Text, out vm)) ttlS += (int) System.Math.Round(vm*60.0);
            int vs = 0;
            if (int.TryParse(RefreshSeconds.Text, out vs)) ttlS += (int) vs;
            if (ttlS < 30) ttlS = 30;

            Model.RefreshRateSeconds = ttlS == defSeconds ? null : (int?) ttlS;
        }
        else
        {
            MinistackHint.Visible = Model != null && Model.LayoutDetails.IsLoadedMinistack;

            if (ShowLayoutName)
            {
                TxtLayoutName.Text = Model.LayoutDetails.Title;
                LbLayoutLabel.Text = AntiXssEncoder.HtmlEncode(Model.LayoutDetails.Title, true);
            }

            txtUpLimit.Text = Model.NonIssueObjectLimit.HasValue
                ? Model.NonIssueObjectLimit.Value.ToString(CultureInfo.InvariantCulture)
                : string.Empty;

            cbUpLimit.Checked = Model.NonIssueObjectLimit.HasValue;
            rdAlignLeft.Checked = Model.IconAlignment == AppStackAlign.Left || Model.IconAlignment == AppStackAlign.Unspecified;
            rdAlignCenter.Checked = Model.IconAlignment == AppStackAlign.Center;
            rdAlignRight.Checked = Model.IconAlignment == AppStackAlign.Right;
            rdSummaryAlways.Checked = Model.AlwaysShowSummaryCounts;
            rdSummaryWhenCollapsed.Checked = !Model.AlwaysShowSummaryCounts;
            cbHideEmpty.Checked = Model.HideEmptyCategories;
            cbShowNames.Checked = Model.ShowNames;

            int ttlS = Model.RefreshRateSeconds ?? defSeconds;
            if (ttlS < 30) ttlS = 30;

            int m = ttlS / 60;
            int s = ttlS % 60;
            RefreshMinutes.Text = m.ToString(CultureInfo.InvariantCulture);
            RefreshSeconds.Text = s.ToString(CultureInfo.InvariantCulture);
            if (m == 0) minutes.Visible = false;
            if (s == 0) seconds.Visible = false;
        }

        return null;
    }
}