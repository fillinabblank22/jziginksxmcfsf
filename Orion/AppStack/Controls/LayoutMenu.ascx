﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LayoutMenu.ascx.cs" Inherits="Orion_AppStack_Controls_LayoutMenu" %>

<div id="AppStackLayoutMenu" class="sw-pg-menu" style="display: none;">
  <div class="sw-pg-layouts sw-pg-menugroup">
      <asp:Literal ID="LayoutLinks" EnableViewState="False" runat="server"></asp:Literal>
  </div>
  <asp:Literal ID="WriteGroups" EnableViewState="False" runat="server"></asp:Literal>
</div>

