﻿using System;
using System.Text;
using System.Web;
using System.Web.Security.AntiXss;
using SolarWinds.AppStack;
using SolarWinds.AppStack.Contract.Models;

public partial class Orion_AppStack_Controls_LayoutMenu : System.Web.UI.UserControl
{
    public AppStackLayoutDetails[] Model { get; set; }

    public AppStackVisualSettings Settings { get; set; }

    protected override void OnPreRender(EventArgs e)
    {
        RenderLayoutLinks();
        RenderMenuWriteOnlyGroups();
        base.OnPreRender(e);
    }

    protected void RenderMenuWriteOnlyGroups()
    {
        var factory = new AppStackFactory();

        if (factory.SettingsController().HasWriteAccess == false)
        {
            return;
        }

        var sb = new StringBuilder();

        sb.AppendLine(@"<div class='sw-pg-menugroup'><div class='sw-pg-menubreak'></div>");
        sb.AppendFormat(@"<a href='{0}'>{1}</a>", 
            AntiXssEncoder.HtmlEncode(factory.UiUtility().GetCustomizeUrl(Settings), true), 
            SolarWinds.AppStack.Strings.Archive.AppStack_LayoutSettings );
        sb.AppendLine(@"</div>");

        sb.AppendLine(@"<div class='sw-pg-menugroup'><div class='sw-pg-menubreak'></div>");
        sb.AppendFormat( Settings.LayoutDetails.ResourceId.HasValue == false
                ? @"<div class='sw-pg-menudisable'>{0}</div>"
                : @"<a id='AppstackLayoutMenuSave' href='#' onclick='return false;'>{0}</a>", SolarWinds.AppStack.Strings.Archive.AppStack_LbSaveLayout);
        sb.AppendFormat("<a id='AppstackLayoutMenuSaveAs' href='#' onclick='return false;'>{0}</a>", SolarWinds.AppStack.Strings.Archive.AppStack_LbSaveAsLayout);
        sb.AppendLine(@"</div>");

        sb.AppendLine(@"<div class='sw-pg-menugroup'><div class='sw-pg-menubreak'></div>");
        sb.AppendFormat(Settings.LayoutDetails.IsDefaultLayout || Settings.LayoutDetails.IsLoadedMinistack
                ? @"<div class='sw-pg-menudisable'>{0}</div>"
                : @"<a id='AppstackLayoutMenuDelete' href='#' onclick='return false;'>{0}</a>", SolarWinds.AppStack.Strings.Archive.AppStack_LbDeleteLayout);
        sb.AppendLine(@"</div>");

        WriteGroups.Text = sb.ToString();
        WriteGroups.Visible = true;
    }

    protected void RenderLayoutLinks()
    {
        var sb = new StringBuilder();

        foreach (var item in Model)
        {
            string title = item.Title ?? string.Empty;
            string cls = item.ResourceId == Settings.LayoutDetails.ResourceId ? "sw-pg-selected" : string.Empty;
            sb.AppendFormat("<a class=\"{0}\" href=\"{1}\">{2}</a>", cls, 
                new AppStackFactory().UiUtility().GetEnvironmentUrl(item), 
                AntiXssEncoder.HtmlEncode(title, true));
            sb.AppendLine();
        }

        LayoutLinks.Text = sb.ToString();
    }
}