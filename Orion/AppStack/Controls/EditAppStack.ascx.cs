﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.AppStack;
using SolarWinds.AppStack.Contract.Models;
using SolarWinds.AppStack.Interfaces;
using SolarWinds.AppStack.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_AppStack_Controls_EditAppStack : BaseResourceEditControl
{
    private AppStackVisualSettings _settings = null;

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        _settings = new AppStackFactory().SettingsController().LoadFromResourceId(Resource.ID);

        ctrlSettings.Settings = ctrlSettings.Model = _settings;

        var result = ctrlSettings.Bind(Page.IsPostBack ? AppStackPresenterBindDirection.PresenterToModel : AppStackPresenterBindDirection.ModelToPresenter);

        if (result != null)
            Page.Validators.Add(result);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            properties[AppStackDefaults.WebResourceSettingName] = new AppStackFactory().SettingsController().WebResourcePartialSave(_settings);

            return properties;
        }
    }
}
