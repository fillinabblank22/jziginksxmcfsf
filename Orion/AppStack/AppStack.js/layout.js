﻿
panes.layout = {};
panes.layout.init = function() {
    if (!$('#appstackoverview')[0]) return;

    var traceId = state.FindId();
    var texts = {
        DeleteLayoutTitle: '@{R=AppStack.Strings;K=Appstack_DeleteLayoutTitle;E=js}',
        DeleteLayoutMsg: '@{R=AppStack.Strings;K=Appstack_DeleteLayoutMsg;E=js}',
        NewLayout: '@{R=AppStack.Strings;K=Appstack_NewLayout;E=js}',
        SaveAsTitle: '@{R=AppStack.Strings;K=Appstack_SaveAsTitle;E=js}',
        SaveAsMsg: '@{R=AppStack.Strings;K=Appstack_SaveAsMsg;E=js}',
        BtnSave: '@{R=AppStack.Strings;K=Appstack_BtnSave;E=js}',
        BtnCancel: '@{R=AppStack.Strings;K=Appstack_BtnCancel;E=js}'
    };

    $(document).on('click', '#appstack-dd-views', function () {
        var on = {
            'show.before': function () { $('#appstack-dd-views').addClass('sw-menuoption-selected'); },
            'hide.after': function () { $('#appstack-dd-views').removeClass('sw-menuoption-selected'); }
        };

        $('#AppStackLayoutMenu').sw_dropdiv('toggle', { alignTo: '#appstack-dd-views', alignMy: 'left bottom', alignThem: 'left top', on: on });
    });

    $(document).on('click', '#AppstackLayoutMenuSave', function() {

        SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=",
            "Save",
            { traceId: traceId },
            function (data) {  },
            function (msg) { /* console.log(arguments); */ }
        );

        SW.Core.Widgets.Dropdiv('', 'cancel');
    });

    $(document).on('click', '#AppstackLayoutMenuDelete', function () {
        function tryDelete() {
            SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=",
                "Delete",
                { traceId: traceId },
                function (data) { if (data) window.location = data; },
                function (msg) { /* console.log(arguments); */ }
            );
        }

        Ext.Msg.show({
            title: texts.DeleteLayoutTitle,
            msg: texts.DeleteLayoutMsg,
            buttons: Ext.Msg.YESNO,
            fn: function(btn) {
                if (btn == 'yes') tryDelete();
            },
            animEl: 'AppstackLayoutMenuDelete',
            icon: Ext.MessageBox.WARNING
        });

        SW.Core.Widgets.Dropdiv('', 'cancel');
    });

    $(document).on('click', '#AppstackLayoutMenuSaveAs', function () {
        function trySaveAs(title) {

            SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=",
                "SaveAs",
                { traceId: traceId, title: title || texts.NewLayout },
                function (data) { if (data) window.location = data; },
                function (msg) { /* console.log(arguments); */ }
            );
        }

        var text = ($('#appstack-dd-views').text() || '').trim(), newText;

        if (text == '') {
            newText = texts.NewLayout;
        }
        else {
            newText = text.replace(/\s+\([0-9]+\)$/, '') + ' (2)';
            if (newText == text) newText = text.replace(/\s+\([0-9]+\)$/, '') + ' (3)';
        }

        Ext.Msg.show({
            title: texts.SaveAsTitle,
            prompt: true,
            animEl: 'AppstackLayoutMenuSaveAs',
            fn: function (btn, value) {
                if (btn == 'ok') trySaveAs(value);
            },
            msg: texts.SaveAsMsg,
            value: newText,
            icon: Ext.MessageBox.QUESTION,
            buttons: { 'ok': texts.BtnSave, 'cancel': texts.BtnCancel }
        });

        SW.Core.Widgets.Dropdiv('', 'cancel');
    });
};
