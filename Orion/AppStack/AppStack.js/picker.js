﻿
var OnFieldSelect = function (picker) {

    var fields = picker.GetSelectedFields();
    var m = SW.Core.Widgets.FieldFilterPanel.State().Current();
    var ret = [];

    var have = {};
    $.each(m, function (i, v) { have[v.FieldRefId] = 1; });

    for (var i = 0, imax = fields.length; i < imax; ++i) {
        var f = fields[i], r = f.RefID, field = f.Field;

        if (field.NavigationPath)
            r = field.NavigationPathItems[0].SourceEntityName + '>' + r;

        if (have[r]) continue;

        ret.push(have[r] = {
            FieldRefId: r,
            DisplayName: f.DisplayName,
            ApplicationType: ((f.Field || {})['DataTypeInfo'] || {})['ApplicationType'] || null
        });
    };

    SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=",
        "RefreshCounts",
        { properties: ret },
        function (data) { SW.Core.Widgets.FieldFilterPanel.addPropertyValues(data); },
        function (msg) { /* console.log(arguments); */ }
        );
};

$(function () {
    if (SW.Core.Widgets.FieldFilterPanel) {
        SW.Core.Widgets.FieldFilterPanel.on('property.selected', OnFieldSelect);
        SW.Core.Widgets.FieldFilterPanel.on('model.save', function () { return app.trigger('filter.change', { refresh: true }); });
        SW.Core.Widgets.FieldFilterPanel.on('viewmodel.changed', function () { app.trigger('panel.change', { refresh: false }); });
    }
});
