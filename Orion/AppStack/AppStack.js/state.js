﻿// variables available to all functions.

var app = SW.Core.namespace('SW.Core.Appstack');
var trace = SW.Core.namespace('SW.Core.Appstack.Trace');
var util = _internals.utility = {};
var panes = _internals.panes = { selection: { charts: {} } };
var state = _internals.state = {};
var messages = _internals.messages = {};
var noop = function () { };

app.IsFullAppStackPage = false; // set by init.

(function () {

SW.Core.Observer.makeCancellableObserver(app);

var map = {};

state.FindId = function (dom) { return $(dom || '.sw-appstack-frame').closest('[sw\\:traceid]').attr('sw:traceid'); };

state.Add = function (dom, obj) {
    var id = state.FindId(dom);
    if (!id) return null;
    map[id] = obj;
    obj._id = id;
    return id;
};

state.Get = function (dom) { return map[state.FindId(dom)] || null; };

state.GetById = function(id) { return map[id] || null; };


function UiDetails(preclone,specificid) {

    var id = specificid || state.FindId(), s = map[id];

    if (!s) return null;

    // get selections

    var selected = $.map($('#' + id + ' .sw-appstack-selected'), function(v, i) { return $(v).attr('sw:id'); });

    // category states

    var csMap = {};
    var collapsed = $.map($('#' + id + ' .sw-appstack-section.sw-collapsed'), function (v, i) { return $(v).attr('sw:section'); });
    s.CategoryStates = s.CategoryStates || [];
    $.each(s.CategoryStates, function(i, v) { (csMap[v.Id] = v).Collapsed = jQuery.inArray(v.Id, collapsed) >= 0; });
    $.each(collapsed, function (i, section) { if (!csMap[section]) s.CategoryStates.push(csMap[section] = { Id: section, Collapsed: true, CountBoost: 0 }); });

    $.each($('#' + id + ' .sw-appstack-m'), function (i, v) {
        var section = $(v).parents('.sw-appstack-section').attr('sw:section'), c = csMap[section], boost = parseInt($(v).attr('sw:at'), 10);
        if (!c && boost) s.CategoryStates.push(csMap[section] = { Id: section, Collapsed: false, CountBoost: boost });
        else if(c) c.CountBoost = boost;
    });

    // modfy & clone

    s.LastUpdateRequestId = (s.LastUpdateRequestId || 0) + 1;

    if (preclone) {
        preclone(s);
        s = $.extend({}, s);
    }

    return {
        traceid: id,
        selected: selected,
        model: s
    };
};

function UpdateModel(next, s) {

    s = s || UiDetails();
    next = next || noop;

    var fnSucess = function (data) { if(data) next(); }
    var fnError = function (msg) { /* todo: inform user */ }

    SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=", "UpdateViewModel", { traceId: s.traceid, model: s.model, save: false }, fnSucess, fnError);
    s.model.LastUpdateRequestId = (s.model.LastUpdateRequestId||0)+1;
}

function Refresh(doupdate, s) {

    if (doupdate) {
        UpdateModel(Refresh, s);
        return;
    }

    s = s || UiDetails();
    var ls = s;

    if (app.trigger('ui.refresh.before', { state: ls, id: ls._id }) == false)
        return;

    if (app.IsFullAppStackPage) {
        
        messages.Show('counts', { clear: 'error' });

        SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=",
            "RefreshCounts",
            { traceId: s.traceid, properties: s.model.Filter },
            function (data) { messages.Hide('counts'); SW.Core.Widgets.FieldFilterPanel.addPropertyValues(data, true); },
            function(msg) { messages.Show('error', { clear: 'counts' }); }
        );
    }

    var config = { 'SelectedIds': (s.selected || []).join(','), };
    var wrid = app.IsFullAppStackPage ? null : $('#' + s.traceid).attr('sw:resourceid');
    if (wrid) config.ResourceId = wrid;

    messages.Show('objects', { clear: 'error' });

    SW.Core.Loader.Control('#' + s.traceid,
        { Control: '~/Orion/AppStack/Controls/IconSectionFrameRefresh.ascx', 'config.TraceId': s.traceid, },
        { config: config },
        'replace',
        function (e) {
            if (e == 'session.timeout')
                window.location = '/orion/login.aspx?sessionTimeout=yes&ReturnUrl=' + encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + '&';
            else
                messages.Show('error', { clear: 'objects' });
        });
};

util.RefreshFrame = function(id) {
    var s = UiDetails(null, id);
    Refresh(false, s);
};

app.Refresh = function() { return Refresh(); };

// ui setting change
app.on('ui.change', function (data) {
    var s = state.Get(), update = 0, refresh = 0;

    if (data.name == 'ShowNames' || data.name == 'SpotlightEnabled' || data.name == "RefreshRateSeconds") {
        s[data.name] = data.value;
        update = !!data.update;
        refresh = !!data.refresh;
    }

    if (refresh) Refresh(update);
    else UpdateModel();
});

// selection change
var tSelection = null;
app.on('selection.change', function (ctrl) {
    var any = $('.sw-appstack-selected')[0];
    $('body')[any ? 'addClass' : 'removeClass']('sw-appstack-hasselection');
    var fnContinue = function () { tSelection = null; app.Refresh(); }
    if (tSelection != null) window.clearTimeout(tSelection);
    tSelection = window.setTimeout(fnContinue, ctrl ? 1000 : 500);
});

// category change
app.on('category.change', function (data) {
    var s = UiDetails(noop);
    s.model.FilterPanelSettings = null;
    s.model.Filter = null;
    if (data && data.refresh) Refresh(true, s);
    else UpdateModel(null,s);
});

// panel change
app.on('panel.change', function (data) {
    var s = UiDetails(function(model) { model.FilterPanelSettings = SW.Core.Widgets.FieldFilterPanel.ViewState(); });
    s.model.Filter = null;
    s.model.CategoryStates = null;
    if (data && data.refresh) Refresh(true, s);
    else UpdateModel(null, s);
});

// filter change
app.on('filter.change', function (data) {
    var s = state.Get();
    var f = SW.Core.Widgets.FieldFilterPanel.State().Current();

    // remove any values that are both: not in use and not sticky.
    var meta = SW.Core.Widgets.FieldFilterPanel.Meta;
    for (var i = 0, imax = f.length; i < imax; ++i) {
        var p = f[i];
        p.Values = $.map(p.Values || [], function (v) { return (v.IsInUse || meta(v, 'sticky') == 'true') ? v : undefined; });
    }

    s.Filter = f;

    if (data && data.refresh) Refresh(true);
    return true;
});

// state change (post-refresh/post-load)
app.on('state.change', function(domid) {
    if (app.IsFullAppStackPage) {
        SW.Core.Widgets.FilterCrumbs.Redo('appstackcrumbs', state.Get().Filter);
    }
});

})();