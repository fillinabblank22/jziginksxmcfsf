﻿var refresh = {};

(function() {

    var refreshes = {}, resources = {};
    var mainpageinterval = null;
    var text = {
        minutes: '@{R=AppStack.Strings;K=AppStack_RefreshLbMinutes;E=js}',
        seconds: '@{R=AppStack.Strings;K=AppStack_RefreshLbSeconds;E=js}',
        both: '@{R=AppStack.Strings;K=AppStack_RefreshLbMinutesAndSeconds;E=js}',
        next: '@{R=AppStack.Strings;K=AppStack_RefreshLbNextRefresh;E=js}',
        now: '@{R=AppStack.Strings;K=AppStack_RefreshLbNow;E=js}'
    };
    var halted = false;

    function rateDetails(id) {
        var v = refreshes[id];
        if (!v) return null;
        var r = v.rate;
        return { m: Math.floor(r / 60), s: r % 60, src: v };
    }

    function remainDetails(next) {
        var r = Math.round( (next - (new Date()).getTime()) / 1000 );
        r = r < 0 ? 0 : r;
        return { m: Math.floor(r / 60), s: r % 60, ttl: r };
    }

    function haltAll() {
        halted = true;

        $.each(refreshes, function(k, v) {
            if (v.inflight) return;
            if (v.timerid) v.timerid = (window.clearTimeout(v.timerid), undefined);
        });
    }

    function onTimeout(id) {
        var v = refreshes[id];
        v.timerid = null;
        touch(id);
        util.RefreshFrame(id);
        v.inflight = 1;
    }

    function updateRemaining(id) {
        var props = rateDetails(id);
        if (!props) return;

        var sel = $('#appstackrefreshrate');
        if (!sel[0]) return;

        if (sel.text() == '*') {
            var fmt = !props.m ? text.seconds : (props.s ? text.both : text.minutes);
            props.m = '<span class="sw-appstack-minutes">' + props.m + '</span>';
            props.s = '<span class="sw-appstack-seconds">' + props.s + '</span>';
            sel.html(util.format(fmt, props));
        }

        var t = text.next, remain = remainDetails(props.src.next);

        if (props.src.inflight || remain.ttl < 1) t += text.now;
        else {
            var fmt = !remain.m ? text.seconds : (remain.s ? text.both : text.minutes);
            t += util.format(fmt, remain);
        }

        sel.attr('title', t);
    }

    function hookRefreshUi(id) {

        var id2 = id;
        var target;

        function editorComplete(isEnter) {
            var value = $('.sw-appstack-texteditor input').val();
            var isMinute = $(target).hasClass('sw-appstack-minutes');
            var done = false;
            var props = rateDetails(id2);
            var o = props.m * 60 + props.s;

            if (value.match(/^\s*[0-9]+(?:\.[0-9]+)?\s*$/)) {
                done = true;

                var v = parseFloat(value, 10);

                if (!isMinute && v >= 0) props.s = v;
                else if (isMinute) props.m = v;
                else done = false;

                if (done) {
                    var r = Math.round(props.m * 60 + props.s);
                    var rm = app.MinRefreshRate || 30;
                    if (r < rm) r = rm;

                    if (o != r) {
                        props.src.rate = r;
                        touch(id2);
                        $('#appstackrefreshrate').text('*');
                        updateRemaining(id2);
                        app.trigger('ui.change', {name: 'RefreshRateSeconds', value: refresh.GetPersistableRate(id) || null, update: true, refresh: false });
                    }
                }
            }

            if (!isEnter || done)
                $('.sw-appstack-refresheditor .sw-appstack-texteditor').remove();
        }

        function editorCancel() {
            $('.sw-appstack-refresheditor .sw-appstack-texteditor').remove();
        }

        function doEditor(e) {
            editorCancel();

            target = e.target;
            var isMinute = $(target).hasClass('sw-appstack-minutes');
            var parent = $(target).closest('.sw-appstack-refresheditor');
            var top = $(target).offset().bottom - parent.offset().top;
            var left = $(target).offset().left - parent.offset().left + Math.round($(target).outerWidth() / 2);
            var props = rateDetails(id2);
            var t = '' + (isMinute ? props.m : props.s);
            var root = $('.sw-appstack-refresheditor');
            root.append('<div class="sw-appstack-texteditor"><div><input type="text"></input></div></div>');
            root.find('.sw-appstack-texteditor').css({ position: 'absolute', top: top, left: left });
            var input = root.find('input')[0];
            input.focus();
            input.value = t;

            $(input).one('blur', editorComplete);
            $(input).on('keyup', function (e) { if (e.keyCode == 27) editorCancel(); });
            $(input).on('keypress', function (e) { if (e.keyCode == 13) editorComplete(true); });
        }

        $(document).on('click', '.sw-appstack-minutes, .sw-appstack-seconds', doEditor);
    };

    function touch(id) {
        var id2 = id, v = refreshes[id2];
        if (!v) return false;

        if (v.timerid) v.timerid = (window.clearTimeout(v.timerid), undefined);
        v.inflight = 0;

        if (halted) return false;

        v.timerid = window.setTimeout(function () { onTimeout(id2); }, v.rate * 1000);
        v.next = (new Date()).getTime() + v.rate * 1000;
        
        return true;
    };

    refresh.Init = function (id, opts) {
        opts = opts || {};

        var defRate = opts.DefaultRefreshRate, v = refreshes[id], s = state.GetById(id);
        if (!s || v) return false;

        refreshes[id] = v = { rate: s.RefreshRateSeconds || defRate || 300, defaultRate: defRate };
        v.next = (new Date()).getTime() + v.rate * 1000;

        if (opts.ResourceId && SW.Core.View) {
            var rid = opts.ResourceId;
            if (resources[rid]) SW.Core.View.on('refresh.start[' + rid + ']', function () { return false; }); // we are in control of refresh. return false to prevent forced refresh.
            resources[rid] = id;
        }

        if (app.IsFullAppStackPage && !mainpageinterval) {
            var id2 = id;
            mainpageinterval = window.setInterval(function () { updateRemaining(id2); }, 2500);
            updateRemaining(id2);
            hookRefreshUi(id2);
        }

        return touch(id);
    };

    refresh.GetPersistableRate = function(id) {
        var v = refreshes[id];
        if (v.rate == v.defaultRate) return null;
        return v.rate;
    };

    app.on('ui.refresh.before', function (args) { touch(args.id); });

    app.on('ui.refresh.after', function (args) { touch(args.id); });

    if (SW.Core.View && SW.Core.View.on) SW.Core.View.on('refresh.halt', haltAll);

})();
