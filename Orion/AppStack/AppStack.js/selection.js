﻿(function(){

var statusLookup = SW.Core.Status.Get;
var hooked = false;

panes.selection.reflowissues = function(domRoot, data) {

    var d = [], c = 0, sel = panes.selection;

    $.each(data, function(i,v) {
        var s = statusLookup(v.status);
        d.push({ name: s.ShortDescription, y: v.count, color: util.jsonValue(s.DisplayProperties,"pie","back") || util.jsonValue(s.DisplayProperties,"textcell","fore") || s.Color });
        c += v.count;
    });

    if (d.length == 0) {
        var s = statusLookup(0);
        d.push({ name: s.ShortDescription, y: 1, color: util.jsonValue(s.DisplayProperties, "pie", "back") || util.jsonValue(s.DisplayProperties, "textcell", "fore") || s.Color });
    }

    var chart = sel.charts[domRoot];

    if (chart) {

        chart.series[0].setData(d, true);

    } else {

        var obj = $(domRoot + ' .sw-appstack-chart')[0];
        var chartId = obj.id = SW.Core.Id(obj);

        sel.charts[domRoot] = new Highcharts.Chart({
            chart: { type: 'pie', renderTo: chartId, plotBackgroundColor: null, plotBorderWidth: 0, plotShadow: false, spacing: [0,0,0,0], margin: [0,0,0,0], backgroundColor: 'transparent', borderWidth: 0 },
            exporting: { enabled: false },
            title: { enabled: false, text: '', y: 0 },
            tooltip: { enabled: false },
            credits: { enabled: false },
            plotOptions: { pie: { size: 33, animation: false, dataLabels: { enabled: false }, startAngle: 0, endAngle: 360, center: ['50%', '50%'], shadow: false, borderWidth: 0 } },
            series: [{ type: 'pie', name: 'objects', innerSize: '33%', data: d, states: { hover: { enabled: false } } }
        ]
        });
    }
    
    var selected = $(domRoot + ' .sw-appstack-selected'), anyselected = !!selected[0];
    var txt = $(domRoot + ' .sw-appstack-pie-count').attr( anyselected ? 'sw:textselection' : 'sw:textoverview');
    $(domRoot + ' .sw-appstack-pie-count').html( String.format(txt, '' + c) );
};

panes.selection.reflowitems = function(domRoot) {
    var selected = $(domRoot + ' .sw-appstack-selected'), textcell = $(domRoot + ' .sw-appstack-sel-text'), textsome = textcell.attr('sw:textsome'), textempty = textcell.attr('sw:textempty');

    var markup = $.map(selected, function(v, i) {
        var href = $(v).attr('href'), m = v.className.match(/\bsw-xstatus-s-24-([^\s]+)\b/);
        if (!m) return;
        return '<div class="sw-appstack-i2"><a class="sw-link sw-xstatus-s-12-' + m[1]  + '" href="' + href + '">' + $(v).html() + '</a></div>';
    });

    if (markup.length < 1) {
        $(domRoot + ' .sw-appstack-sel-text').html('<span style="font-size: 11px; font-style: italic;">' + textempty + '</span>');
        $(domRoot + ' .sw-appstack-sel-items').hide();
    }
    else if (markup.length < 2) {
        $(domRoot + ' .sw-appstack-sel-text').html( markup[0] );
        $(domRoot + ' .sw-appstack-sel-items').hide();
    }
    else {
        $(domRoot + ' .sw-appstack-sel-text').html('<a href="#" onclick="return false;" class="sw-menuoption-selectable sw-appstack-dd sw-appstack-sel-expand">'+ String.format(textsome,markup.length) + '</a>');
        $(domRoot + ' .sw-appstack-sel-items').html( markup.join('') );
        $(domRoot + ' .sw-appstack-sel-items').hide();
    }
};


panes.selection.reflowall = function (id) {
    
    panes.selection.hook();

    // no overview bar? no reflow

    var domRoot = '#' + SW.Core.Id($('#' + id).parent()[0]);

    var overview = $(domRoot + " .sw-appstack-sel-pane");

    if (!overview[0]) return;

    var all = [];
    var found = {};
    var ret = [];

    $.each( $(domRoot + ' .sw-appstack-c > span'), function (i, v) {
        var sv = $(v).attr('sw:status'), sc = parseInt($(v).text()) || 0;

        var o = found[sv];
        if( !o ) all.push( o = found[sv] = { count: 0, status: sv, order: statusLookup(sv).UiOrder } );
        o.count += sc;
    });

    all.sort(function(a, b){ return a.order - b.order; });

    $.each(all, function(i, v) {
        if (v.status == "1" || v.status == "29") return; // skip up and other.
        ret.push('<div><div class="sw-xstatus-s-12-' + statusLookup(v.status).IconPostfix + '"></div><span>' + v.count + '</span></div>');
    });

    if (!ret.length) ret.push('<span>@{R=AppStack.Strings;K=Appstack_LbNoIssues;E=js}</span>');

    $(domRoot + ' .sw-appstack-issues').html(ret.join(''));

    panes.selection.reflowissues(domRoot, all);

    panes.selection.reflowitems(domRoot);
};

    app.IconSelect = function(dom,ctrl) {
        var me = $(dom), isSelected = me.hasClass('sw-appstack-selected');

        if (!isSelected && !ctrl) {
            $('.sw-appstack-selected').removeClass('sw-appstack-selected');
            me.addClass('sw-appstack-selected').removeClass('sw-appstack-dim');
        }
        else if (isSelected) me.removeClass('sw-appstack-selected');
        else if (ctrl) me.addClass('sw-appstack-selected').removeClass('sw-appstack-dim');

        app.trigger('selection.change',ctrl);
    };

    app.IconDrilldown = function (dom) {
        var r = app.trigger('drilldown', dom);
        if (r !== undefined && !r) return;
        window.location = dom.href;
    };

    var clickstate = { timeoutid: null, lastclick: null };
    
    var onClick = function(e) {
        e.preventDefault();
        e.stopPropagation();

        var t = e.target, ctrl = e.ctrlKey;
        if (t.tagName != 'A') t = $(t).parents('A')[0]; 

        var cs = clickstate, lk = cs.lastclick;
        cs.lastclick = null;

        if (ctrl) {
            if (cs.timeoutid) {
                clearTimeout(cs.timeoutid);
                cs.timeoutid = null;
            }

            app.IconSelect(t, true);
            return;
        }

        if (cs.timeoutid) {
            clearTimeout(cs.timeoutid);
            cs.timeoutid = null;

            if (t == lk) {
                app.IconDrilldown(lk);
                return;
            }

            app.IconSelect(lk);
        }

        cs.lastclick = t;

        cs.timeoutid = window.setTimeout(function() {
            cs.timeoutid = null;
            app.IconSelect(cs.lastclick, cs.ctrl);
        }, 250);
    };

panes.selection.hook = function() {
    if (hooked) return;
    hooked = 1;

    var clickSelector = '.sw-appstack-i2 a';
    if (!app.IsFullAppStackPage) clickSelector += ', .sw-appstack-i a:not(.sw-appstack-m)';

    $(document).on('click', clickSelector, function (e) {
        e.preventDefault();
        e.stopPropagation();
        var a = $(e.target).closest('a');
        app.IconDrilldown(a[0]);
    });

    if (!app.IsFullAppStackPage) return;

    $(document).on('click', '.sw-appstack-sel-expand', function() {
        var on = {
            'show.before': function() { $('.sw-appstack-sel-expand').addClass('sw-menuoption-selected'); },
            'hide.after': function() { $('.sw-appstack-sel-expand').removeClass('sw-menuoption-selected'); }
        };

        $('.sw-appstack-sel-items').sw_dropdiv('toggle', { alignTo: '.sw-appstack-sel-expand', alignMy: 'left bottom', alignThem: 'left top', on: on });
    });

    $(document).on('click', '.sw-appstack-i a:not(.sw-appstack-m)', onClick);
};
    

})();