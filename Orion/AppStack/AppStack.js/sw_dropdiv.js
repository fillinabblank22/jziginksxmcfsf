﻿(function ($) {

    var widgets = SW.Core.namespace('SW.Core.Widgets');
    if (widgets.Dropdiv) return;


    var state = { id: null, on: {} }, hooked;

    function docb(id, ctx, args) {
        var cb = state.on[id];
        return cb ? cb.apply(ctx, args || []) : undefined;
    }

    function getdom(id) {
        if (!id) return null;
        return $(id)[0];
    }

    function trackClicks(e) {
        if (!state.id) return;
        var t = e.target;
        if (getdom(state.id) != t &&
            getdom(state.alignTo) != t &&
            !$(t).parents(state.id)[0] &&
            !$(t).parents(state.alignTo)[0]) {
            hide(); // click outside of the div   
        }
    };

    function trackKeys(e) {
        if (!state.id) return;
        if (e.which == 27) hide();
    };

    function show(dom, options) {
        if (hide() == false) return false;

        if (options) {
            state = $.extend({}, options);
            state.on = state.on || {};
        }

        if (docb('show.before', getdom(dom)) === false) return false;

        if (!hooked) {
            $(document).on("click", trackClicks);
            $(document).on("keyup", trackKeys);
            hooked = 1;
        }

        var alignTo = $(state.alignTo);
        var poss = alignTo.offset();
        $(state.id).css({ position: 'absolute', top: poss.top + alignTo.outerHeight(), left: poss.left }).show();

        docb('show.after', getdom(dom));
        return true;
    };

    function hide() {
        if (!state.id) return;
        var dom = getdom(state.id);
        if (docb('hide.before', dom) === false) return false;
        $(state.id).hide();
        docb('hide.after', dom);
        state = { id: null, on: {} };
        return true;
    };

    function dropdiv(div, action, options) {

        if (action == 'toggle') {
            if (state.id) action = 'cancel';
            else action = 'open';
        }

        if (action == 'cancel') { hide(); return; }
        if (action != 'open') return;

        var opt = $.extend({ align: 'bottom left', alignTo: null, alignMy: 'left bottom', alignThem: 'left top', on: {} }, options);

        if (!div.id)
            div.id = SW.Core.Id();

        opt.id = '#' + div.id;

        if (typeof (opt.alignTo) != "string") {
            var pat = opt.alignTo.id;
            if (!pat) {
                opt.alignTo.id = SW.Core.Id();
                opt.alignTo = '#' + opt.alignTo.id;
            }
        }

        if (opt.alignTo === state.alignTo && div.id === state.id) show(div);
        else show(div, opt);
    };

    widgets.Dropdiv = dropdiv;

    // jquery integration

    $.fn.sw_dropdiv = function (action, options) {
        var me = this.length ? this[this.length - 1] : this;
        dropdiv(me, action, options);
        return this;
    };

})(jQuery);