﻿
(function(){

var util = _internals.utility || (_internals.utility={});
var trace = SW.Core.namespace('SW.Core.Appstack.Trace');

util.qsReplace = function (url, name, val) {
    url = url.replace(/#.*$/, '');
    if (url.indexOf('?') < 0) url += "?";
    else if (url.lastIndexOf('&') != url.length - 1) url += "&";
    var rx = new RegExp('(^.*[?&]'+ name +'=)([^&]*)($|&.*)', "i");
    var m = rx.exec(url);
    if (!m) return url + name + "=" + encodeURIComponent(val) + "&";
    return m[1] + encodeURIComponent(val) + m[3];
};

util.jsonOrNull = function (v) { try { return JSON.parse(v); } catch (e) { return null; } };

util.jsonValue = function (o) {
    var args = arguments;
    o = o || {};
    for (var i = 1, imax = args.length - 1; i < imax; ++i) o = o[args[i]] || {};
    return o[args[i]] || null;
};

util.format = function (s, params) {
    var args = Array.prototype.slice.call(arguments, 1), obj = params || {};

    return s.replace(/[{][:]?[a-z0-9_\-.]+[}]/ig, function (m) {
        var tok = m.substring(1, m.length - 1), n, r, e = 0;
        if (tok[0] == ':') { e = 1, tok = tok.substring(1); }
        if (((n = parseInt(tok)) === NaN) || ((r = args[n]) === undefined)) r = obj[tok];
        if (e) return $('<div/>').text('' + r).html();
        return '' + r;
    });
};



var traceIndexes = {}, store = localStorage || {}, exporthookup = false;

function nextTraceIndex(traceid) {

    var ord = traceIndexes['' + traceid];

    if (typeof (ord) == "undefined")
    {
        var prefix = "appstack." + traceid + ".";
        ord = 0;
        while (typeof (store[prefix + ord]) != "undefined")
            ord++;
    }

    traceIndexes['' + traceid] = ord + 1;
    return ord;
}

trace.Add = function (traceid, obj) {
    if (exporthookup == false) {
        var id = traceid;
        if( SW.Core.Export && SW.Core.Export.Excel) 
            SW.Core.Export.Excel.Register('appstack.' + traceid, function() { return { Rows: trace.Get(id) }; }, function() { return 1; });
        exporthookup = true;
    }

    if (jQuery.isArray(obj) == false) obj = [obj];
    var key = "appstack." + traceid + "." + nextTraceIndex(traceid);

    try {
        store[key] = JSON.stringify(obj);
        return;
    }
    catch (ex) {
    }


    try {
        trace.Clear();
        store[key] = JSON.stringify(obj);
    }
    catch (ex) {
    }
};

trace.Get = function (traceid) {
    var ord = 0, item, traces = [], keys = ['Timestamp', 'DurationMs', 'Message', 'Details'], seenkeys = {};

    traces.push(keys);
    $.each(keys, function(i, v) { seenkeys[v] = 1 });

    var items = [];

    while (typeof (item = store["appstack." + traceid + "." + (ord++)]) != "undefined") {
        item = JSON.parse(item);
        for (var j = 0, jmax = item.length; j < jmax; ++j) {
            var r = item[j];
            items.push(r);
            $.each(r, function (i, v) { if (!seenkeys[i]) seenkeys[i] = keys.push(i); });
        }
    }

    $.each( items, function(i, v) {
        var row = [];
        traces.push(row);

        for (var i = 0, imax = keys.length; i < imax; ++i) {
            var cell = v[keys[i]];
            if (typeof(cell) == "undefined") cell = '';
            row.push(cell);
        }
    })

    return traces;
};

trace.Clear = function () {
    for (var key in store) {
        if (key.indexOf('appstack.') != 0) continue;
        delete store[key];
    }
};

// automate the journal into the page for qa instrumentation

if (('' + window.location).indexOf('qainstrument=true') >= 0) {
    $(document).ready(function() {
        window.setTimeout(function() {
            var contents = trace.Get($('#appstackmenuframe').attr('sw:traceid'));
            $('form').append('<input style="display: none;" type=text id="hdnTraceMap">');
            $('#hdnTraceMap')[0].value = JSON.stringify(contents);
            $('form').append('<input style="display: none;" type=text id="hdnTraceTimeTaken">');
            $('#hdnTraceTimeTaken')[0].value = '' + contents[contents.length - 1][1];
        }, 200);
    })
}

})();
