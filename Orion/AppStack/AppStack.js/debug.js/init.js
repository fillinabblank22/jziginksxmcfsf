(function() {
    var labelType = 'Native';
    var util = _internals.utility || (_internals.utility = {});
    var vol = SW.Core.namespace('SW.Core.Volatile.AppStack');
    var dbg = SW.Core.namespace('SW.Core.Appstack.Debug');
    var urlParams = SW.Core.UriHelper.decodeURIParams(null);
    var resizing = false;
    var getStatus = SW.Core.Status.Get;

    var options = {
        MinDepth: parseInt(urlParams['D'] || urlParams['d'] || '1', 10),
        Focus: urlParams['S'] || urlParams['s'] || '',
        AnimationDelay: parseFloat(urlParams['A'] || urlParams['a'] || '2.5') * 1000,
        LevelDistance: parseInt(urlParams['LD'] || urlParams['ld'] || '130', 10),
        UserScale: parseFloat(urlParams['US'] || urlParams['us'] || '')
    };

    dbg.Load = function(key2) {
        var newp = $.extend({}, urlParams);
        if (newp['']) delete newp[''];
        if (newp['s']) delete newp['s'];
        if (newp['S']) delete newp['S'];
        newp['S'] = key2;
        window.location = '/orion/AppStack/Debug.aspx?' + SW.Core.UriHelper.encodeURIParams(newp);
        return false;
    }

    function showVis(raw) {
        var c = cookRelianceData(raw);
        vol.Raw = raw;
        vol.Cooked = c;

        vol.Vis.graph.empty();
        vol.Vis.canvas.clear();
        $(document.documentElement).removeClass('sw-pg-editmode');

        renderGraph(vol.Vis, c);
    }

    function uiResizeSync() {
        var footer = $('#footer');
        var fH = footer.is(':visible') ? $('#footer').height() + 20 : 6; // 20px to cover the 'footermark'
        var hH = $('#debugtop').offset().top;
        var wH = $(window).height();
        var bbH = $('#debugbtnbar').outerHeight();
        if (wH < 600) wH = 600;

        var mapdom = $('#debuggraph0'),
            detdom = $('.sw-pg-debugdetails'),
            content = $('#content'),
            mW = content.width() - detdom.width() - 20;

        mapdom.height(wH - fH - bbH - hH);
        $('#debugjson').height(wH - fH - bbH - hH).width(mW);

        if (mapdom[0] && detdom[0]) {
            var my = mapdom.offset().top, mh = mapdom.height(), dy = detdom.offset().top, dh = (my + mh) - (dy) - 20;
            detdom.height(dh);

            if (vol.Vis) {
                resizing = true;
                try {
                    vol.Vis.canvas.resize(mW - 2, dh);
                } catch (ex) {
                }
                resizing = false;
            }
        }
    };

    function cookRelianceData(d) {
        var nodemap = {};
        var nodes = [];
        var depth = {};
        var crumbs = {};

        $.each(d.Nodes, function(k, v) {
            
            if (v.DisplayName === null) {
                v.DisplayName = ((v.Uri || '').match( /^.*\/([^\/]+)$/ ) || [])[1] || 'null';
            }

            var n = {
                id: "n" + v.Id,
                name: "" + v.DisplayName,
                data: { "$color": v.IsFocalObject ? "#FF922D" : "#83548B", "$type": "circle", "$dim": 10, orig: v, },
                depth: 0
            };
            nodemap[n.id] = n;
            nodes.push(n);
        });

        $.each(d.Reliances, function(k, v) {
            var from = 'n' + v.Id;
            var node = nodemap[from];

            if (!node) return;

            node.adjacencies = $.map(v.Neighbors || [], function(vv, kk) {

                var to = "n" + vv;
                var ret = {
                    nodeTo: to,
                    nodeFrom: from,
                    data: {
                        "$color": "#557EAA",
                        "$type": "arrow",
                        "$direction": [from, to]
                    }
                };

                var prev = crumbs[to + ">" + from];
                if (prev) prev.data["$type"] = ret.data["$type"] = "double_arrow";
                else crumbs[from + ">" + to] = ret;

                depth[to] = (depth[to] || 0) + 1;

                return ret;
            });
        });

        $.each(nodes, function(k, v) {
            v.data.depth = depth[v.id] || 0;
            if (!(v.data.depth >= options.MinDepth)) v.name = '';
        });

        return vol.Cooked = nodes;
    }

    function sortInfoByStatus(a, b) {
        var a1 = getStatus(a.Status).UiOrder, b1 = getStatus(b.Status).UiOrder;
        if (a1 < b1) return -1;
        if (a1 > b1) return 1;
        if (a.Severity < b.Severity) return -1;
        if (a.Severity > b.Severity) return 1;
        return (a.DisplayName || '').localeCompare(b.DisplayName, 'en', { sensitivity: "base" });
    }

    function renderNodeDetails(node) { 
      try {
          if (!node) return;

          var count = 0;
          var keys = [];
          var bytype = {};
          var markup = [];
          node.eachAdjacency(function(adj) {
              var t = adj.nodeTo.data.orig.EntityType;
              var a = bytype[t];
              if (!a) {
                  keys.push(t);
                  a = bytype[t] = [];
              }
              a.push(adj.nodeTo.data.orig);
              count++;
          });

          keys.sort();

          // Build the right column relations list.  
          // This is done by traversing the clicked node connections.  
          markup.push(util.format(
              "<p><b><a href='{:link}' onclick='return SW.Core.Appstack.Debug.Load(\"{:Key2}\");'><span class='sw-pg-debugicon16 {:StatusHint}'></span>{:DisplayName}</a></b></p><p><b>type</b>: {EntityType}</b></p><p><b> connections:</b> {connections}</p>", {
                  DisplayName: node.data.orig.DisplayName,
                  Key2: node.data.orig.Key2,
                  EntityType: node.data.orig.EntityType,
                  StatusHint: (node.data.orig.StatusHint || '').replace(/s-24-/i, 's-16-'),
                  connections: count,
                  link: node.data.orig.DetailsUrl || '#'
              }));

          $.each(keys, function(k, v) {
              var list = bytype[v];

              list.sort(sortInfoByStatus);

              markup.push(util.format("<div class='sw-pg-debugblock'><p>{0} ({1})</p>", v, list.length));

              $.each(list, function(kk, vv) {
                  markup.push(util.format("<a href='{:link}' onclick='return SW.Core.Appstack.Debug.Load(\"{:Key2}\");'><span class='sw-pg-debugicon12 {:StatusHint}'></span>{:DisplayName}</a>", {
                      DisplayName: vv.DisplayName,
                      Key2: vv.Key2,
                      link: vv.DetailsUrl || "#",
                      StatusHint: (vv.StatusHint || '').replace(/s-24-/i, 's-12-')
                  }));
              });

              markup.push("</div>");
          });

          //append connections information  
          $jit.id('inner-details').innerHTML = markup.join('');

      } catch (err) {
          //  console.log(err);
      }
    }

    function renderNodeTip(tip, node) {
        //count connections  
        var count = 0;
        node.eachAdjacency(function() { count++; });
        //display node info in tooltip  
        tip.innerHTML = util.format(
            "<div class='tip-title'>{:DisplayName}</div><div class='tip-text'><p><b>type:</b> {EntityType}</p><p><b>connections:</b> {connections}</p></div>",
            { DisplayName: node.data.orig.DisplayName, EntityType: node.data.orig.EntityType, connections: count });
    }

    function initGraph() {

        var fd = new $jit.ForceDirected({
            //id of the visualization container  
            injectInto: 'debuggraph0',

            //Enable zooming and panning  
            //by scrolling and DnD  
            Navigation: {
                enable: true,
                //Enable panning events only if we're dragging the empty  
                //canvas (and not a node).  
                panning: 'avoid nodes',
                zooming: 100 //zoom speed. higher is more sensible  
            },
            // Change node and edge styles such as  
            // color and width.  
            // These properties are also set per node  
            // with dollar prefixed data-properties in the  
            // JSON structure.  
            Node: {
                overridable: true
            },
            Edge: {
                overridable: true,
                color: '#23A4FF',
                lineWidth: 0.4
            },
            //Native canvas text styling  
            Label: {
                type: labelType, //Native or HTML  
                size: 10,
                style: 'bold',
                color: "#000000"
            },
            //Add Tips  
            Tips: {
                enable: true,
                onShow: renderNodeTip
            },

            // Add node events  
            Events: {
                enable: true,
                type: 'Native',
                //Change cursor style when hovering a node  
                onMouseEnter: function() {
                    fd.canvas.getElement().style.cursor = 'move';
                },
                onMouseLeave: function() {
                    fd.canvas.getElement().style.cursor = '';
                },
                //Update node positions when dragged  
                onDragMove: function(node, eventInfo, e) {
                    var pos = eventInfo.getPos();
                    node.pos.setc(pos.x, pos.y);
                    fd.plot();
                },
                //Implement the same handler for touchscreens  
                onTouchMove: function(node, eventInfo, e) {
                    $jit.util.event.stop(e); //stop default touchmove event  
                    this.onDragMove(node, eventInfo, e);
                },
                //Add also a click handler to nodes  
                onClick: renderNodeDetails
            },

            //Number of iterations for the FD algorithm  
            iterations: 200,

            //Edge length  
            levelDistance: options.LevelDistance,

            // Add text to the labels. This method is only triggered  
            // on label creation and only for DOM labels (not native canvas ones).  
            onCreateLabel: function(domElement, node) {
                domElement.innerHTML = node.name;
                var style = domElement.style;
                style.fontSize = "0.8em";
                style.color = "#000";
                style.fontWeight = 'bold';
            },
            // Change node styles when DOM labels are placed  
            // or moved.  
            onPlaceLabel: function(domElement, node) {
                var style = domElement.style;
                var left = parseInt(style.left);
                var top = parseInt(style.top);
                var w = domElement.offsetWidth;
                style.left = (left - w / 2) + 'px';
                style.top = (top + 10) + 'px';
            },

            onBeforePlotLine: function(adj) {
                if (!resizing) return;

                var p;
                var node;

                node = adj.nodeFrom;
                if (node.remapped == false) {
                    p = node.getPos('current');
                    p.theta = (Math.PI * 2) - (p.theta / 2);
                    node.setPos(p, 'current');
                    node.remapped = true;
                }

                node = adj.nodeTo;
                if (node.remapped == false) {
                    p = node.getPos('current');
                    p.theta = (Math.PI * 2) - (p.theta / 2);
                    node.setPos(p, 'current');
                    node.remapped = true;
                }
            },
        });

        return fd;
    };

    function renderGraph(graph, json) {

        if (isNaN(options.UserScale) == false) {
            graph.canvas.scale(options.UserScale, options.UserScale, true);
        }

        // load JSON data.  
        graph.loadJSON(json);

        // compute positions incrementally and animate.  
        graph.computeIncremental({
            iter: 40,
            property: 'end',
            onStep: function(perc) {
                // Log.write(perc + '% loaded...');  
            },
            onComplete: function() {
                // Log.write('done');  

                graph.animate({
                    modes: ['linear'],
                    transition: $jit.Trans.Elastic.easeOut,
                    duration: options.AnimationDelay
                });

                // attempt to find our selected node, if any
                if (options.Focus && options.Focus.indexOf(',') < 0) {
                    $.each( SW.Core.Volatile.AppStack.Cooked, function(i, v) {
                        if (v.data.orig.Key2 == options.Focus) {
                            var node = SW.Core.Volatile.AppStack.Vis.graph.getNode(v.id);
                            if (node) renderNodeDetails(node);
                            return false;
                        }
                    });
                }
            }
        });
    }

    dbg.Init = function() {

        // resize the ui immediately

        uiResizeSync();

        // ui events

        $('#DebugCancelLink').click(function(event) {
            event.preventDefault();
            event.stopPropagation();
            $('#debugjson').val('');
            $(document.documentElement).removeClass('sw-pg-editmode');
        });

        $('#DebugModifyLink').click(function(event) {
            event.preventDefault();
            event.stopPropagation();
            $('#debugjson').val(window.JSON.stringify(vol.Raw, null, 2));
            $(document.documentElement).addClass('sw-pg-editmode');
        });

        $('#DebugSaveLink').click(function(event) {
            event.preventDefault();
            event.stopPropagation();

            try {
                var json = $('#debugjson').val();
                var r = JSON.parse(json);

                SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=",
                    "SaveTraverse", { data: r },
                    function(data) {
                        if (data) showVis(r);
                    },
                    function(msg) {}
                );

            } catch (ex) {
                if (window.console && window.console.log) console.log(ex);
                return;
            }
        });

        $('#DebugApplyLink').click(function(event) {
            event.preventDefault();
            event.stopPropagation();

            try {
                var json = $('#debugjson').val();
                var r = JSON.parse(json);
                showVis(r);
            } catch (ex) {
                if (window.console && window.console.log) console.log(ex);
                return;
            }
        });

        $(window).resize(_.debounce(uiResizeSync, 300));

        // jit config

        $jit.ForceDirected.Plot.EdgeTypes.implement({
            'double_arrow': {
                'render': function(adj, canvas) {
                    var from = adj.nodeFrom.pos.getc(true),
                        to = adj.nodeTo.pos.getc(true),
                        dim = adj.getData('dim'),
                        ctx = canvas.getCtx(),
                        vect = new $jit.Complex(to.x - from.x, to.y - from.y);
                    vect.$scale(dim / vect.norm());
                    //Needed for drawing the first arrow 
                    var intermediatePoint = new $jit.Complex(to.x - vect.x, to.y - vect.y),
                        normal = new $jit.Complex(-vect.y / 2, vect.x / 2),
                        v1 = intermediatePoint.add(normal),
                        v2 = intermediatePoint.$add(normal.$scale(-1));

                    var vect2 = new $jit.Complex(to.x - from.x, to.y - from.y);
                    vect2.$scale(dim / vect2.norm());
                    //Needed for drawing the second arrow 
                    var intermediatePoint2 = new $jit.Complex(from.x + vect2.x, from.y + vect2.y),
                        normal = new $jit.Complex(-vect2.y / 2, vect2.x / 2),
                        v12 = intermediatePoint2.add(normal),
                        v22 = intermediatePoint2.$add(normal.$scale(-1));

                    //Drawing the double arrow on the canvas, first the line, then the ends 
                    ctx.beginPath();
                    ctx.moveTo(from.x, from.y);
                    ctx.lineTo(to.x, to.y);
                    ctx.stroke();
                    ctx.beginPath();
                    ctx.moveTo(v1.x, v1.y);
                    ctx.lineTo(v2.x, v2.y);
                    ctx.lineTo(to.x, to.y);
                    ctx.closePath();
                    ctx.fill();
                    ctx.beginPath();
                    ctx.moveTo(v12.x, v12.y);
                    ctx.lineTo(v22.x, v22.y);
                    ctx.lineTo(from.x, from.y);
                    ctx.closePath();
                    ctx.fill();
                }
            }
        });

        // load and render the data

        if ($('#debuggraph0')[0]) {
            SW.Core.Services.callWebService("/orion/appstack/default.aspx?op=",
                "Traverse",
                { focus: options.Focus },
                function(data) {
                    vol.Raw = data;
                    vol.Cooked = cookRelianceData(data);
                    vol.Vis = initGraph();
                    renderGraph(vol.Vis, vol.Cooked);
                },
                function(msg) {}
            );
        }
    }

})();