﻿(function () {

    function collapseSection(dom,forceExpand) {
        var me = $(dom).closest('.sw-collapsible'), isExpanded = !me.hasClass('sw-collapsed'), expand = forceExpand === undefined ? !isExpanded : forceExpand;
        if (expand == isExpanded) return false;
        var targets = me.children(':not(.sw-collapsible-h)'), xor = me.find('.sw-collapse-xor');
        me[expand ? 'removeClass' : 'addClass']('sw-collapsed');
        targets[expand ? 'show' : 'hide'](400);
        xor[expand ? 'hide' : 'show'](400);
        return true;
    }

    function toggleCollapse(e) {
        if (collapseSection(e.target)) app.trigger('category.change');
    }

    function toggleCollapseAll(forceExpand) {
        var change = 0;
        $.each($('.sw-appstack-h .sw-collapsible-toggle'), function(i,v) { change = collapseSection(v, forceExpand) || change; });
        if(change) app.trigger('category.change');
    }

    function toggleShowNames() {
        var tag = $(document.documentElement), isOn = tag.hasClass('sw-appstack-shownames');
        tag[isOn ? 'removeClass' : 'addClass']('sw-appstack-shownames');
        app.trigger('ui.change', { name: 'ShowNames', value: !isOn, update: true, refresh: false });
    };

    function toggleDimmed() {
        var tag = $('.sw-appstack-spotlight'), isOn = tag.hasClass('sw-menuoption-selected'), refresh = $('body').hasClass('sw-appstack-hasselection');
        tag[isOn ? 'removeClass' : 'addClass']('sw-menuoption-selected');
        app.trigger('ui.change', { name: 'SpotlightEnabled', value: !isOn, update: true, refresh: refresh });
    }

    function clearSelection() {
        $('.sw-appstack-selected').removeClass('sw-appstack-selected');
        app.trigger('selection.change');
    }

    function showMoreIcons(e) {
        e.preventDefault();
        e.stopPropagation();

        var a = $($(e.target).closest('a')[0]), boost = parseInt(a.attr('sw:at'), 10), boostby = state.Get(a[0]).CategoryBoostSize;

        if (app.IsFullAppStackPage) {
            a.attr('sw:at', boost + boostby);
            app.trigger('category.change', { refresh: true });
        } else {
            var section = a.closest('[sw\\:section]').attr('sw:section');
            var next = a.closest('.sw-appstack-ministack').find('.sw-ministack-view-entire-environment').attr('href');
            if (section && next) window.location = util.qsReplace(next, 'c:' + section, boostby);
        }
    }

    function disableFilterValue(details) {
        var propid = details.propertyid, valueid = details.valueid, ffp = SW.Core.Widgets.FieldFilterPanel,
            props = ffp.State(), prop = props.PropertyGet(propid);

        if (prop) {
            props.ValueDisable(propid, valueid);
            ffp.mergePropertyValues(props.Current(), true, false);
            ffp.Save();
        }
    }

    function disableFilter() {
        var ffp = SW.Core.Widgets.FieldFilterPanel, props = ffp.State(), ids = props.PropertyIds();
        $.each(ids, function (k, v) { $.each(props.PropertyGet(v).Values || [], function (kk, vv) { vv.IsInUse = false; }); });
        ffp.mergePropertyValues(props.Current(), true, false);
        ffp.Save();
    }

    function fixTips() {
        if (!SW.Core.NetObjectTips) return;

        // orphaned content confuses current implementation of hovers; better to just hide on refresh

        $.each(SW.Core.NetObjectTips.ActiveHovers(), function (k, v) {
            if (!$(v).closest(document.documentElement)[0]) {
                if ($(v).closest('.sw-appstack-frame')[0]) {
                    SW.Core.NetObjectTips.CancelHover(v);
                }
            }
        });
    }

    var debugKeys = [];
    function debugDetector(e) {
        debugKeys.push(String.fromCharCode(e.which));
        if (debugKeys.length > 5) debugKeys.shift();
        var isDebug = debugKeys.join('').toLowerCase() == 'debug';
        $(document.documentElement)[isDebug ? 'addClass' : 'removeClass']('sw-appstack-debug');
    }

    function debugPageClick(e) {
        var s = $.map($('.sw-appstack-selected'), function(v, i) { return $(v).attr('sw:id'); }).join(',');
        if (!s) return;

        var params = SW.Core.UriHelper.decodeURIParams(null);
        var z = params['Z'] || params['z'];

        e.stopPropagation();
        e.preventDefault();

        window.location = '/Orion/AppStack/Debug.aspx?Z=' + z + '&S=' + encodeURIComponent(s) + "&";
    }

    messages.Show = function(type,options) {
        if (!app.IsFullAppStackPage) return;

        var parent = $('#appstackmsg'),  sel = $('#appstackmsg .sw-pg-msg-' + type);

        if (! parent.is(":visible")) {
            sel.show();
            sel = parent;
        }

        sel.show(200);
        
        if (options) 
            $.each((options.clear || '').split(','), function(k, v) { $('#appstackmsg .sw-pg-msg-' + v).hide(); });
    };

    messages.Hide = function(type,now) {
        if (!app.IsFullAppStackPage) return;

        if (type == 'all') {
            $('#appstackmsg, #appstackmsg > span').hide();
            return;
        }

        var me = $('#appstackmsg .sw-pg-msg-' + type);
        if (me.is(':hidden')) return;

        var remaining = $('#appstackmsg > span:visible').length - 1;
        if (remaining > 0) me.hide(now ? 0 : 200);
        else $('#appstackmsg, #appstackmsg > span').hide(now ? 0 : 200);
    };

    var init = false;

    app.Init = function (domid, opts, vmopts) {

        app.IsFullAppStackPage = !!$('#appstackframe[sw\\:isfullappstackpage]')[0];

        var stateid = state.Add(domid, opts || {});

        $(document.documentElement)[opts.ShowNames ? 'addClass' : 'removeClass']('sw-appstack-shownames');

        var anyselected = $('.sw-appstack-selected')[0];
        $('body')[anyselected ? 'addClass' : 'removeClass']('sw-appstack-hasselection');

        if (!init) {

            $(document).on('click', '.sw-appstack-h > a, .sw-appstack-h > label', toggleCollapse);
            $(document).on('click', ".sw-appstack-m", showMoreIcons);
            $('.appStackExpandAll').click(function(e) { toggleCollapseAll(true); });
            $('.appStackCollapseAll').click(function(e) { toggleCollapseAll(false); });
            $('.appStackShowNames').click(toggleShowNames);
            $('.sw-appstack-spotlight').click(toggleDimmed);
            $('.sw-appstack-clearselect').click(clearSelection);
            panes.layout.init();
            
            if (app.IsFullAppStackPage) {

                SW.Core.Widgets.FilterCrumbs.Get('appstackcrumbs')
                    .on('value.remove', disableFilterValue)
                    .on('value.remove.all', disableFilter);

                var celem = document.createElement('canvas'), isCanvasSupported = !!(celem.getContext && celem.getContext('2d'));

                if (isCanvasSupported) {
                    $(document).keypress(debugDetector);
                    $('.sw-appstack-debuglink').click(debugPageClick);
                }

                $('#appstackmsgx').click(function(event){ 
                    event.stopPropagation(); 
                    event.preventDefault();
                    messages.Hide('all');
                });
            }

            init = true;
        } else {
            app.trigger('ui.refresh.after', { state: state.GetById(stateid), id: stateid, success: true });
            messages.Hide('objects');

            if(SW.Core.Widgets.FilterCrumbs) SW.Core.Widgets.FilterCrumbs.Redo('appstackcrumbs', opts.Filter);

            fixTips();
        }

        refresh.Init(stateid, {
            DefaultRefreshRate: (vmopts || {}).DefaultRefreshRate,
            ResourceId: $(domid).attr('sw:resourceid')
        });

        panes.selection.reflowall(stateid);

        $('.sw-appstack-why').cluetip({
            local: true,
            cursor: 'pointer',
            dropShadow: true,
            dropShadowSteps: 2,
            waitImage: false,
            sticky: true,
            cluetipClass: 'sw-appstack-hint',
            width: '280px',
            closePosition: 'title',
            closeText: "<img src='/orion/appstack/images/close.gif' height='15' width='15'>",
            mouseOutClose: true
        });

        app.trigger('state.change', domid);
    };

})();
