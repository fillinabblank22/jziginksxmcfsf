﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Debug.aspx.cs" Inherits="Orion_AppStack_Debug" Title="AppStack Environment - Debug" MasterPageFile="~/Orion/OrionMasterPage.master"  %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:Include runat="server" File="AppStack\AppStack.css" />
    <orion:Include runat="server" File="AppStack\AppStack.js\debug.js"  />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="padding: 0 10px 0 10px;"><div>
    
        <h1 class="sw-hdr-title" style="margin: 15px 0 6px 0; padding: 0;"><%: Page.Title %></h1>

        <div id="debugtop">
            <div style="margin-right: 244px;">
                <div id='debuggraph0' style="height: 350px; width: 100%;"></div>
                <textarea id="debugjson" class="sw-pg-editlink" style="position: absolute; left: 0; top: 0; z-index: 999; padding: 0; border: none;"></textarea>
            </div>
            <div style="position: absolute; right: 0; top: 0; width: 244px;">
                <div class="sw-pg-debugdetails"><div id="inner-details">
                    <p><i>Instructions</i></p>
                    <p><i>On the main AppStack page: Select at least one object and click the 'debug' link.</i></p>
                    <p><i>Click an object to show its connections</i></p>
                    <p><i>Click plus drag, moves an object or pans the viewport.</i></p>
                    <p><i>Scroll your mousewheel to zoom in or out.</i></p>
                    <p><i>Click 'Edit' to see the raw output.</i></p>
                </div></div>
            </div>
        </div>

        <div id="debugbtnbar" class='sw-btn-bar'>
            <orion:LocalizableButtonLink ID="DebugDoneLink" ClientIDMode="Static" runat="server" LocalizedText="Done" DisplayType="Primary" CssClass="sw-pg-viewlink" />
            <orion:LocalizableButtonLink ID="DebugModifyLink" ClientIDMode="Static" runat="server" LocalizedText="Edit" DisplayType="Secondary" CssClass="sw-pg-viewlink" />
            <orion:LocalizableButtonLink ID="DebugSaveLink" ClientIDMode="Static" runat="server" LocalizedText="CustomText" Text="Save Within Diagnostics" DisplayType="Secondary" CssClass="sw-pg-editlink" />
            <orion:LocalizableButtonLink ID="DebugApplyLink" ClientIDMode="Static" runat="server" LocalizedText="Apply" DisplayType="Secondary" CssClass="sw-pg-editlink" />
            <orion:LocalizableButtonLink ID="DebugCancelLink" ClientIDMode="Static" runat="server" LocalizedText="Cancel" DisplayType="Primary" CssClass="sw-pg-editlink" />
        </div>

    </div></div>

<orion:InlineCss runat="server">
    #content { padding-bottom: 0; }
    #debugtop { position: relative; min-width: 800px; border: 1px solid #ccc; border-left: none; border-right: none; }
    #debugjson { }
    .sw-pg-debugdetails { border-left: 1px solid #ccc; margin-top: 10px; overflow: hidden; overflow-y: auto; }
    #debugbtnbar { margin: 0 0 0 10px; position: relative; padding: 10px 0 0 0; margin-left: 0; }
    #inner-details { padding-left: 10px; }
    #inner-details > p { margin: 0; padding: 0; margin-top: 8px; }
    #inner-details > p:first-child { margin-top: 0; font-size: 16px; }
    #_tooltip { border: 1px solid #666; border-radius: 4px; background-color: #ffe; padding: 4px; box-shadow: 3px 3px 3px #ccc; }
    #_tooltip .tip-text p { padding: 0; margin: 0; margin-top: 3px; }
    #_tooltip .tip-title { font-weight: bold; border-bottom: 1px solid #ccc; margin-bottom: 4px; }
    .sw-pg-debugblock a { display: block; margin-top: 5px; }
    .sw-pg-debugblock > p { font-weight: bold; margin-bottom: 6px; }
    .sw-pg-debugicon12 { height: 12px; width: 12px; display: inline-block; margin-right: 6px; }
    .sw-pg-debugicon16 { height: 16px; width: 16px; display: inline-block; margin-right: 6px; }
    .sw-pg-editlink { display: none; }
    .sw-pg-editmode .sw-pg-editlink { display: inline-block; }
    .sw-pg-editmode .sw-pg-viewlink { display: none; }
</orion:InlineCss>

</asp:Content>

<script runat="server" type="c#">
    protected override void OnInitComplete(EventArgs e)
    {
        base.OnInitComplete(e);
        OrionInclude.CoreFile("AppStack/AppStack.js/Debug.js", OrionInclude.Section.Bottom).AddJsInit("$(SW.Core.Appstack.Debug.Init);");
    }
</script>

