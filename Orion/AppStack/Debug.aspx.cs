﻿using System;
using System.Linq;
using System.Web.UI;
using SolarWinds.AppStack;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_AppStack_Debug : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var utility = new AppStackFactory().UiUtility();

        utility.EnableDebugMode();

        var items = UrlHelper.ParseQueryString(Request.Url.PathAndQuery)
                .Where(x => utility.IsKnownUrlParameter(x.Key));

        DebugDoneLink.NavigateUrl = UrlHelper.ReplaceQueryString("/Orion/AppStack/Default.aspx?", items);
    }
}