﻿using System;
using System.Web;
using System.Web.UI;
using SolarWinds.AppStack;
using SolarWinds.AppStack.Interfaces;
using SolarWinds.AppStack.Web;
using SolarWinds.Orion.Web;

public partial class Orion_AppStack_Settings : Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Title = SolarWinds.AppStack.Strings.Archive.AppStack_SettingsTitle;
    }

    private void LoadValues()
    {
        var factory = new AppStackFactory();

        var config = factory.Config().InitForWebPage();

        this.ctrlSettings.Model = this.ctrlSettings.Settings = config.Settings;
        var result = this.ctrlSettings.Bind(AppStackPresenterBindDirection.ModelToPresenter);

        if (result != null)
        {
            Page.Validators.Add(result);
            Page.Validate();
        }
    }

    private void SaveValues()
    {
        var factory = new AppStackFactory();

        var config = factory.Config().InitForWebPage();

        this.ctrlSettings.Model = this.ctrlSettings.Settings = config.Settings;
        var result = this.ctrlSettings.Bind(AppStackPresenterBindDirection.PresenterToModel);

        if (result != null)
        {
            Page.Validators.Add(result);
            Page.Validate();
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Page.IsPostBack == false) LoadValues();
    }

    public void SaveClick(object sender, EventArgs e)
    {
        SaveValues();
        
        if (Page.IsValid)
        {
            var _controller = new AppStackFactory().SettingsController();
            _controller.Save(ctrlSettings.Model);
            ReferrerRedirectorBase.Return(new AppStackFactory().UiUtility().GetEnvironmentUrl(null,null));
        }
    }

    public void CancelClick(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Return(new AppStackFactory().UiUtility().GetEnvironmentUrl(null, null));
    }
}
