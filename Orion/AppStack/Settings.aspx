﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Orion_AppStack_Settings" MasterPageFile="~/Orion/OrionMasterPage.master"  %>
<%@ Register Src="~/Orion/AppStack/Controls/EditSettings.ascx" TagPrefix="appstack" TagName="Settings" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div id="appstacksettingsframe" style="padding: 0 10px 0 10px;"><div>
    
        <h1 class="sw-hdr-title" style="margin: 15px 0 6px 0; padding: 0;">
            <%: Page.Title %>
        </h1>
        
        <appstack:Settings ID="ctrlSettings" ShowLayoutName="True" runat="server" />

        <div id="cntSubmit" class="sw-btn-bar" runat="server">
	        <orion:LocalizableButton ID="btnSubmit" runat="server" LocalizedText="Submit" DisplayType="Primary" OnClick="SaveClick" />
	        <orion:LocalizableButton ID="btnCancel" runat="server" LocalizedText="Cancel" CausesValidation="false" OnClick="CancelClick" />
        </div>

    </div></div>

</asp:Content>

<asp:Content ContentPlaceHolderID="outsideFormPlaceHolder" Runat="Server">
</asp:Content>
