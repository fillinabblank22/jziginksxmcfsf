<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB1_20 %>" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
      Response.Redirect(string.Format("/Orion/Reports/ViewReports.aspx{0}", 
            (Request.QueryString["IsMobileView"] != null) ? string.Format("?IsMobileView={0}", Request.QueryString["IsMobileView"]) : string.Empty));
        base.OnLoad(e);
    }
</script>
