﻿<%@ WebHandler Language="C#" Class="PollerExporter" %>

using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Web;
using SolarWinds.DeviceStudio.BusinessLayer.Client;
using SolarWinds.DeviceStudio.Common;
using SolarWinds.DeviceStudio.Common.Models;

public class PollerExporter : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //The browser will treat this as a file to download.
        context.Response.ContentType = "application/file";

        //Get the poller id from query string, verify if it exists.
        string pollerIdString = context.Request.QueryString["pollerId"];
        if (string.IsNullOrEmpty(pollerIdString) == false)
        {
            Guid pollerId = Guid.Empty;
            Guid.TryParse(pollerIdString, out pollerId);
            if (pollerId != Guid.Empty)
            {
                using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create())
                {
                    Poller poller = proxy.Api.LoadPoller(pollerId);
                    if (poller != null)
                    {
                        //Check if the poller is not SW certified, only serve it if it isn't.
                        if (IsSolarWindsCertifiedPoller(poller) == false)
                        {
                            //Set the file name, we use the poller extension because of a security check in chrome,
                            //which marks XMLs served in an iframe as potentially malicious.
                            string fileNameHeader = string.Format("attachment; filename=\"{0}.poller\"", poller.Name);
                            context.Response.AddHeader("Content-Disposition", fileNameHeader);
                            var serializer = new DataContractSerializer(typeof(Poller), "Poller", Strings.ServiceNamespace);
                            var settings = new XmlWriterSettings() { Indent = true };
                            using (var writer = XmlWriter.Create(context.Response.OutputStream, settings))
                            {
                                serializer.WriteObject(writer, poller);
                                context.Response.StatusCode = 200;
                                return;
                            }
                        }
                    }
                }
            }
            //If the happy case didn't yield a result, we throw an error and let the client side deal with it.
            throw new InvalidOperationException();
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private bool IsSolarWindsCertifiedPoller(Poller poller)
    {
        if (string.IsNullOrEmpty(poller.Author) == false)
        {
            return poller.Author.Equals("Solarwinds", StringComparison.InvariantCultureIgnoreCase);
        }
        return false;
    }

    private void HandleError(Exception ex)
    {
        throw ex;
    }
}