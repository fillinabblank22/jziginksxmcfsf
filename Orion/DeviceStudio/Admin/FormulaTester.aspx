﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" %>
<%@ Register TagPrefix="ds" TagName="FormulaTester" Src="~/Orion/DeviceStudio/Controls/FormulaTester.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <style>
	    #area {width:400px;height:130px;resize:none;display:block;border:1px solid #CCC;outline:none;}
	    .sw_ds_fe_inputList li input {width:222px;border:1px solid #DDD;padding:4px 8px;margin:0 0 4px 0;}
    </style>
		
    <textarea id="area">[T2.Some] - 10 + [Alpha] / [T1.Beta] AND [T2.Some] - [Gamma] / [T1.BetaB] + [T2.Thing] - [Delta]</textarea>

    <ds:FormulaTester ID="ft1" runat="server" />
    
    <script>

        $(document).ready(function () {

            // init the formula tester
            var ft = new SW.DeviceStudio.FormulaTester("#area", true);

        });

    </script>

</asp:Content>

