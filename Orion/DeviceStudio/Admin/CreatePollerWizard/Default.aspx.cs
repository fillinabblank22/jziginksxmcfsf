﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Configuration;
using SolarWinds.DeviceStudio.BusinessLayer.Client;
using SolarWinds.DeviceStudio.Web.Wizard;
using SolarWinds.DeviceStudio.Common.Models;
using SolarWinds.DeviceStudio.Common.Helpers;
using SolarWinds.DeviceStudio.Framework.Polling;
using SolarWinds.DeviceStudio.Framework;
using SolarWinds.DeviceStudio.Framework.Inventory.Snmp;
using SolarWinds.DeviceStudio.Framework.Inventory.Snmp.Operations;
using SolarWinds.DeviceStudio.Web;

public partial class Orion_DeviceStudio_Admin_CreatePollerWizard_Default : System.Web.UI.Page
{
    #region Session

    public DSWizardDataStore DataStore
    {
        get
        {
            if (Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] == null)
                Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] = new DSWizardDataStore();

            return Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] as DSWizardDataStore;
        }
        protected set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] = value;
        }
    }

    private DataSource DataSource
    {
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] = value;
        }
    }

    #endregion

    protected void MultiView1_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
            DataStore.TechnologyChanged(Guid.Empty.ToString());

        LoadViews();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // detect poller argument and if present use as edit wizard
            string fromPollerArg = Request["fromPoller"];
            string editPollerArg = Request["editPoller"];
            Guid pollerId;


            bool isPollerIdPresent = Guid.TryParse(editPollerArg, out pollerId);
            bool isEditOfExistingPoller = isPollerIdPresent;

            if (!isPollerIdPresent)
            {
                isPollerIdPresent = Guid.TryParse(fromPollerArg, out pollerId);
            }
            if (isPollerIdPresent)
            {
                using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create())
                {
                    var poller = proxy.Api.LoadPoller(pollerId, true);

                    if (poller == null)
                    {
                        throw new InvalidOperationException("Invalid poller id was detected, wizard cannot continue");
                    }

                    if (isEditOfExistingPoller)
                    {
                        DataStore.LoadFromPoller(poller);
                    }
                    else
                    {
                        DataStore.LoadFromPollerDuplicate(poller);
                    }
                    DataStore.LoadDataSources = true;
                }
            }

            UpdateUI();
        }
    }

    private void LoadViews()
    {
        foreach (StepModel step in DataStore.Steps)
        {
            DSWizardStepBase stepControl = LoadControl(step.Path) as DSWizardStepBase;
            stepControl.DataStore = this.DataStore;
            stepControl.UpdateParentUI = this.UpdateUI;

            View stepView = new View();
            stepView.ID = step.Name;
            stepView.Controls.Add(stepControl);
            MultiView1.Views.Add(stepView);
        }

        if (!IsPostBack)
            MultiView1.ActiveViewIndex = 0;
    }

    #region UI updates

    public void UpdateUI()
    {
        // progress indicator
        UpdateProgressIndicator();

        // navigation buttons
        UpdateNavigationButtons();

        if (MultiView1.ActiveViewIndex >= 0)
        {
            View view = MultiView1.GetActiveView();
            DSWizardStepBase stepControl = view.Controls[0] as DSWizardStepBase;
            if (stepControl != null)
                stepControl.DynamicLoadControls();
        }
    }

    protected void UpdateProgressIndicator()
    {
        var indicator = cdpProgress;
        if (indicator == null) return;

        indicator.Steps.Clear();
        foreach (var step in DataStore.Steps)
        {
            indicator.Steps.Add(step.DisplayName);
        }

        indicator.SelectedStep = indicator.Steps[MultiView1.ActiveViewIndex];
        hiddenCurrentStep.Value = DataStore.Steps[MultiView1.ActiveViewIndex].Name;

        indicator.DataBind();
    }

    protected void UpdateNavigationButtons()
    {
        btnPrevCDPStep.Enabled = (MultiView1.ActiveViewIndex > 0);
        btnPrevCDPStep.Visible = (MultiView1.ActiveViewIndex > 0);

        btnNextCDPStep.Enabled = (MultiView1.ActiveViewIndex < (DataStore.Steps.Count - 1));
        btnNextCDPStep.Visible = (MultiView1.ActiveViewIndex < (DataStore.Steps.Count - 1));

        btnFinishCDP.Enabled = ((MultiView1.ActiveViewIndex == (DataStore.Steps.Count - 1)) && (DataStore.pollerTechnology != Guid.Empty.ToString()));
        btnFinishCDP.Visible = ((MultiView1.ActiveViewIndex == (DataStore.Steps.Count - 1)) && (DataStore.pollerTechnology != Guid.Empty.ToString()));
    }

    #endregion

    #region Navigation event handlers

    protected void btnPrevCDPStep_Click(object sender, EventArgs e)
    {
        StoreData();
        if (MultiView1.ActiveViewIndex > 0)
            MultiView1.ActiveViewIndex--;

        UpdateUI();
    }

    protected void btnNextCDPStep_Click(object sender, EventArgs e)
    {
        if (!IsValid)
        {
            //On the second step we've a server-side customvalidator
            //If it fails, it runs page_load as postback, so the ui won't be updated, which results in
            //the wizard bar disappearing.
            UpdateUI();
            return;
        }

        StoreData();
        if (MultiView1.ActiveViewIndex < MultiView1.Views.Count - 1)
            MultiView1.ActiveViewIndex++;

        UpdateUI();
    }

    protected void btnCancelCDP_Click(object sender, EventArgs e)
    {
        DataStore.TechnologyChanged(Guid.Empty.ToString());

        // clear datasource in session
        DataSource = null;

        Response.Redirect("/Orion/Admin/Pollers/ManagePollers.aspx");
    }

    protected void btnFinishCDP_Click(object sender, EventArgs e)
    {
        if (!IsValid)
        {
            return;
        }

        StoreData();

        var pollingConfig = DataStore.poller.Configs.GetConfig<PollingConfig>(PollerConfigNames.PollingConfig);
        if (pollingConfig != null)
        {
            // removing unused properties
            var propertyCleaner = pollingConfig as IPropertyCleaner;
            if (propertyCleaner != null)
            {
                propertyCleaner.RemoveUnusedProperties();
            }

            // create inventory config from polling config
            if (pollingConfig.DataSourceCreateConfig != null)
            {
                var inventoryConfig = pollingConfig.DataSourceCreateConfig.CreateInventoryConfig();
                DataStore.poller.Configs.AddConfig<Config>(PollerConfigNames.InventoryConfig, inventoryConfig);
            }
            else
            {
                // create empty inventory config
                if(DataStore.poller.PollingMethod == PollingMethods.Snmp)
                {
                   var inventoryConfig = new SnmpInventoryConfig() { Operations = new List<ISnmpInventoryConfigOperation>() };
                   DataStore.poller.Configs.AddConfig<Config>(PollerConfigNames.InventoryConfig, inventoryConfig);
                }
            }

            // save changed polling config back to session
            DataStore.poller.Configs.AddConfig<Config>(PollerConfigNames.PollingConfig, pollingConfig);
        }

        // save created poller to db
        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create())
        {
            proxy.Api.SavePoller(DataStore.poller);
        }

        // clear datasource in session
        DataSource = null;

        // navigate to manage pollers page and notify it about created poller
        var pollerId = TechnologyPollingIdConverter.GetTechnologyPollingIdFromGuid(DataStore.poller.PollerID);
        string action = PollerActions.PollerCreated;
        string url = string.Format("{0}?action={1}&poller={2}", "/Orion/Admin/Pollers/ManagePollers.aspx", action, pollerId);
        Response.Redirect(url);
    }

    #endregion

    private void StoreData()
    {
        View activeView = MultiView1.GetActiveView();
        DSWizardStepBase control = activeView.Controls[0] as DSWizardStepBase;
        control.StoreData();
    }
}
