﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Summary.ascx.cs" Inherits="Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_Summary" %>

<style type="text/css">
 #adminContent td.firstCollumn {font-weight: bold; padding-right:20px;}
</style>
<h3><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_52 %></h3>

<div id="newPollerCreatedWarning" runat="server">
    <span class="sw-suggestion sw-suggestion-warn">
        <span class="sw-suggestion-icon"></span>
        <%= Resources.DeviceStudioWebContent.WEBDATA_LF0_50 %>
    </span>
</div>
<table class="sw_ds_wizard_form" style="border-collapse:collapse">
    <tr>
        <td class="firstCollumn"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_11 %></td>
        <td><asp:Label ID="Poller_TechnologyID" runat="server" /></td>
    </tr>
    <tr>
        <td class="firstCollumn"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_10 %></td>
        <td><asp:Label ID="Poller_Name" runat="server" /></td>
    </tr>
    <tr>
        <td class="firstCollumn"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_6 %></td>
        <td><asp:Label ID="Poller_Description" runat="server" /></td>
   </tr>
    <tr>
        <td class="firstCollumn"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_3 %></td>
        <td><asp:Label ID="Poller_Tags" runat="server" /></td>
   </tr>
    <tr style="display:none">
        <td class="firstCollumn"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_4 %></td>
        <td><asp:Label ID="Poller_Vendor" runat="server" /></td>
   </tr>
   <tr>
        <td class="firstCollumn"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_7 %></td>
        <td><asp:Label ID="Poller_Author" runat="server" /></td>
   </tr>
</table>
