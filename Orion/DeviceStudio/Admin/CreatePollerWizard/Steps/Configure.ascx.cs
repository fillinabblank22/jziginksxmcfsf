using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.DeviceStudio.Web.Wizard;
using System.Data;
using System.Configuration;
using System.Web.Security.AntiXss;
using SolarWinds.DeviceStudio.BusinessLayer.Client;
using SolarWinds.DeviceStudio.Common.Models;
using i18nExternalized = SolarWinds.DeviceStudio.Strings.Resources;

public partial class Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_Configure : DSWizardStepBase
{
    private DataSource DataSource
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] as DataSource;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] = value;
        }
    }

    public override string Name
    {
        get { return "Configure"; }
    }

    /// <summary>
    /// if you need change this, please change also definition in 
    /// SolarWinds.DeviceStudio.Web.Wizard.StepModel
    /// </summary>
    public override string DisplayName
    {
        get { return i18nExternalized.WEBDATA_LF0_55; }
    }

    public override string Description
    {
        get { return i18nExternalized.WEBDATA_LF0_56; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadItemsForDDL();
            UpdateUI();
        }
        else
        {
            if ((frm_configureStep_technology.Items.Count > 0) && (frm_configureStep_technology.Items[0].Value == Guid.Empty.ToString()) && (frm_configureStep_technology.SelectedValue != Guid.Empty.ToString()))
                frm_configureStep_technology.Items.RemoveAt(0);
        }
        string strNodeId = this.Request.QueryString["NodeId"];
        if (!string.IsNullOrEmpty(strNodeId))
        {            
            frm_NodeIdFromListResource.Text = strNodeId;         
        }
    }

    public override void StoreData()
    {
        DataStore.pollerTechnology = frm_configureStep_technology.SelectedValue;
        DataStore.pollerNetObject = frm_configureStep_node.Text;

        DataStore.poller.Name = AntiXssEncoder.HtmlEncode(frm_configureStep_name.Text, false);
        DataStore.poller.Tags = AntiXssEncoder.HtmlEncode(frm_configureStep_tags.Text, false);
        
        if(frm_configureStep_vendor.Items.Count>0)
            DataStore.poller.Vendor = frm_configureStep_vendor.SelectedValue;

        DataStore.poller.Description = AntiXssEncoder.HtmlEncode(frm_configureStep_description.Text, false);
        DataStore.poller.Author = AntiXssEncoder.HtmlEncode(frm_configureStep_author.Text, false);
    }

    public override void DynamicLoadControls()
    {
        NetObjectPickerPlace.Controls.Add(LoadControl("~/orion/controls/NetObjectPicker.ascx"));
    }

    protected void UpdateUI()
    {
        frm_configureStep_technology.SelectedValue = DataStore.pollerTechnology;
        frm_configureStep_node.Text = DataStore.pollerNetObject;

        frm_configureStep_name.Text = Server.HtmlDecode(DataStore.poller.Name);
        frm_configureStep_tags.Text = Server.HtmlDecode(DataStore.poller.Tags);
        if (frm_configureStep_vendor.Items.Count > 0)
            frm_configureStep_vendor.SelectedValue = DataStore.poller.Vendor;

        frm_configureStep_description.Text = Server.HtmlDecode(DataStore.poller.Description);
        frm_configureStep_author.Text = Server.HtmlDecode(DataStore.poller.Author);

        frm_configureStep_technologyDescription.Text = string.IsNullOrEmpty(DataStore.selectedTechnologyIcon) ? "" : string.Format("<img src=\"{0}\" style=\"vertical-align:middle;\" /> ", DataStore.selectedTechnologyIcon) + DataStore.selectedTechnologyDescription;
    }

    protected void frm_configureStep_technology_SelectedIndexChanged(object sender, EventArgs e)
    {
        // former poller valid for previously selected technology
        var originalPoler = DataStore.poller;

        DataStore.TechnologyChanged(frm_configureStep_technology.SelectedValue);
        DataSource = new DataSource();

        // let's copy information which is technology independent to the "new" poller
        if (originalPoler != null)
        {
            //DataStore.poller.CloneBasicSettings(originalPoller);
            DataStore.poller.Author = originalPoler.Author;
            DataStore.poller.Description = originalPoler.Description;
            DataStore.poller.Name = originalPoler.Name;
            DataStore.poller.Tags = originalPoler.Tags;
            DataStore.poller.Vendor = originalPoler.Vendor;
        }

        UpdateUI();
        UpdateParentUI();
    }

    private void LoadItemsForDDL()
    {
        frm_configureStep_technology.Items.Add(new ListItem(i18nExternalized.WEBDATA_LF0_57, Guid.Empty.ToString()));
        frm_configureStep_technology.Items.AddRange(LoadTechnologies());

        frm_configureStep_vendor.Items.Clear();
        frm_configureStep_vendor.Items.Add(new ListItem(i18nExternalized.WEBDATA_LF0_57, ""));
        frm_configureStep_vendor.Items.AddRange(LoadVendors());

    }

    protected ListItem[] LoadTechnologies()
    {
        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create())
        {
            // load all technologies in DevMode
            DataStore.Technologies = DevMode ? proxy.Api.GetTechnologies() : proxy.Api.GetTechnologies().Where(t => !t.ReadOnly).ToList();
            return DataStore.Technologies.Select(t => new ListItem(t.Name, t.TechnologyID.ToString())).ToArray();
        }
    }
    protected ListItem[] LoadVendors()
    {
        var result = new List<string>();
        DataTable vendors;

        using (var swis = SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3())
        {
            vendors = swis.Query(@"SELECT DISTINCT Vendor FROM Orion.Nodes");
        }

        if (vendors != null)
        {
            foreach (System.Data.DataRow row in vendors.Rows)
            {
                result.Add(HttpUtility.HtmlEncode(Convert.ToString(row["Vendor"])));
            }
        }

        var vendorList = result.Select(t => new ListItem(t, t)).ToList();
        
        // when poller contains vendor which is not yet in the system then add it to the list
        if(DataStore != null && DataStore.poller != null && string.IsNullOrEmpty(DataStore.poller.Vendor)==false && !vendorList.Any(item=> string.Equals(item.Text, DataStore.poller.Vendor, StringComparison.InvariantCultureIgnoreCase)))
        {
            string vendor = DataStore.poller.Vendor;
            vendorList.Add(new ListItem(vendor, vendor));
        }

        return vendorList.OrderBy(i => i.Text).ToArray();
    }
}
