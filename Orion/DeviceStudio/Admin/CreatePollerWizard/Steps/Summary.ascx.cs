﻿using System;
using SolarWinds.DeviceStudio.BusinessLayer.Client;
using SolarWinds.DeviceStudio.Web.Wizard;
using i18nExternalized = SolarWinds.DeviceStudio.Strings.Resources;


public partial class Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_Summary : DSWizardStepBase
{
    public override string Name
    {
        get { return "Summary"; }
    }

    public override string DisplayName
    {
        get { return i18nExternalized.WEBDATA_LF0_54; }
    }

    public override string Description
    {
        get { return i18nExternalized.WEBDATA_LF0_53; }
    }

    public override void StoreData()
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.newPollerCreatedWarning.Visible = false;     

        LoadData();
    }

    private void LoadData()
    {
        this.Poller_TechnologyID.Text = GetTechnologyName(DataStore.poller.TechnologyID.ToString());
        this.Poller_Name.Text = DataStore.poller.Name;
        this.Poller_Description.Text = DataStore.poller.Description;
        this.Poller_Tags.Text = DataStore.poller.Tags;
        this.Poller_Vendor.Text = DataStore.poller.Vendor;
        this.Poller_Author.Text = DataStore.poller.Author;
    }
        
    protected string GetTechnologyName(string technologyId)
    {
        string rv = "";
        
        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create())
        {
            if (string.IsNullOrEmpty(technologyId))
            {
                return "";
            }

            var technology = proxy.Api.GetTechnology(Guid.Parse(technologyId));
            rv = technology.Name;
        }

        return rv;
    }
}
