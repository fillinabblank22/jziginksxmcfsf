﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Discovery.ascx.cs" Inherits="Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_Discovery" %>


<style type="text/css">
    #adminContent td.firstCollumn {
        font-weight: bold;
        padding-right: 20px;
    }
</style>
<h3><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_45 %></h3>

<div id="newPollerCreatedWarning" runat="server">
    <span class="sw-suggestion sw-suggestion-warn">
        <span class="sw-suggestion-icon"></span>
        <%= Resources.DeviceStudioWebContent.WEBDATA_LF0_44 %>
    </span>
</div>

<table style="width: 100%; margin-bottom: 20px;">
    <tr>
        <td>
            <asp:CheckBox ID="Discovery_Enabled" runat="server" />
        </td>
        <td>
            <label for="Discovery_Enabled"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_46 %></label>
            <br />
            <br />
            <%= Resources.DeviceStudioWebContent.WEBDATA_PD0_04 %>
        </td>
    </tr>
</table>
