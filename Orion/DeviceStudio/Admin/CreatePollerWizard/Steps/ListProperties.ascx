﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListProperties.ascx.cs" Inherits="Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_ListProperties" %>
<%@ Register Src="~/Orion/DeviceStudio/Controls/PollerDataSourceManager.ascx" TagPrefix="ds" TagName="PollerDataSourceManager" %>
<orion:Include ID="JS_ListPropertiesStep" runat="server" File="/DeviceStudio/js/CreatePollerWizard/DeviceStudio.CreatePollerWizard.Steps.ListProperties.js" />

<ds:PollerDataSourceManager ID="pdsm" runat="server" />

<h3><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_47 %></h3>
<p style="width:auto;margin-bottom:10px;"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_48 %></p>
<input type="hidden" id="hiddenNetObjectId" value="" runat="server" />
<input type="hidden" id="hiddenTechnologyName" value="" runat="server" />
<div id="cdpListPropertiesGrid"></div>
<div id="cdpListPropertiesValidator">
    <asp:CustomValidator ID="cv1" runat="server" EnableClientScript="false" OnServerValidate="cv1_ServerValidate" ClientValidationFunction="cdpValidateProperties" ErrorMessage="<%$ Resources: DeviceStudioWebContent, WEBDATA_LF0_49 %>" />
</div>
<div id="devModeSection" runat="server">
    <asp:DropDownList runat="server" ID="lbConfigs" OnSelectedIndexChanged="lbConfigs_SelectedIndexChanged" AutoPostBack="True">
        <asp:ListItem>select</asp:ListItem>
        <asp:ListItem>Inventory</asp:ListItem>
        <asp:ListItem>Polling</asp:ListItem>
    </asp:DropDownList>
    <asp:Button runat="server" ID="btnUpdate" OnClick="btnUpdate_OnClick" Text="Update" Enabled="False"/>
    <br/>
    <asp:TextBox runat="server" ID="tbConfig" Width="1209" Height="500" TextMode="MultiLine" />
</div>

<script type="text/javascript">
  $(document).ready(function () {
    // step check -> avoid executing js for previous step
    if (document.getElementById('hiddenCurrentStep').value !== 'ListProperties') return;
    // init grid
    SW.DeviceStudio.CreatePollerWizard.Steps.ListProperties.init('cdpListPropertiesGrid');
  });
</script>