﻿using System;
using SolarWinds.DeviceStudio.Web.Wizard;
using SolarWinds.DeviceStudio.Common;
using i18nExternalized = SolarWinds.DeviceStudio.Strings.Resources;

public partial class Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_Discovery : DSWizardStepBase
{
    public override string Name
    {
        get { return "Discovery"; }
    }

    public override string DisplayName
    {
        get { return i18nExternalized.WEBDATA_LF0_60; }
    }

    public override string Description
    {
        get { return i18nExternalized.WEBDATA_LF0_61; }
    }

    public override void StoreData()
    {
        DataStore.poller.Enabled = this.Discovery_Enabled.Checked;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        LoadData();    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.newPollerCreatedWarning.Visible = false;            
    }
        
    private void LoadData()
    {
        this.Discovery_Enabled.Checked = DataStore.poller.Enabled;        
    }    
}
