﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Configure.ascx.cs" Inherits="Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_Configure" ClientIDMode="Static" %>
<orion:Include ID="Include1" runat="server" File="/DeviceStudio/js/CreatePollerWizard/DeviceStudio.CreatePollerWizard.Steps.Configure.js" />
<style>
  .tooltip { display: none; position: absolute; width: 200px; }
  td label {font-weight: bold; padding-right:20px;}
</style>
<h3><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_12 %></h3>
<p style="margin-bottom:10px;"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_13 %></p>
<br />
<table class="sw_ds_wizard_form">
    <tr>
        <td style="width:185px;">
            <label for="frm_configureStep_technology"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_2 %>
            <img src="/Orion/Nodes/images/icons/Icon.info.gif" alt="info" align="top" id="PortInfoIcon" /></label>
            <div id="TechnologyInfoTooltip" class="tooltip sw-suggestion sw-suggestion-noicon" >
    <p><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_37 %></p>
    </div>
        </td>
        <td>
            <asp:DropDownList 
                ID="frm_configureStep_technology"
                runat="server"  
                AutoPostBack="true" onselectedindexchanged="frm_configureStep_technology_SelectedIndexChanged"
                />
        </td>
        <td>
                <asp:Label ID="frm_configureStep_technologyDescription" runat="server" Text="" />
        </td>
    </tr>
    <tr>
        <td>
            <label for="frm_configureStep_name"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_1 %></label></td>
        <td>
            <asp:TextBox ID="frm_configureStep_name" runat="server" CssClass="long_input" Text="Dummy" />
        </td>
        <td>
            <%--validation--%>
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidator2" 
                runat="server" 
                ErrorMessage="<%$ Resources: DeviceStudioWebContent, WEBDATA_LF0_43 %>"
                ControlToValidate="frm_configureStep_name" CssClass="sw_ds_error_text"
                ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr style="display:none;">
        <td>
            <label for="frm_configureStep_vendor"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_4 %></label>
        </td>
        <td>
            <asp:DropDownList 
                ID="frm_configureStep_vendor" 
                runat="server"
                />
        </td>
        <td>
            <%--validation--%>
        </td>
    </tr>
    <tr>
        <td>
            <label for="frm_configureStep_node"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_5 %>
            <img src="/Orion/Nodes/images/icons/Icon.info.gif" alt="info" align="top" id="TestNodeInfoIcon" /></label>
            <div id="TestNodeTooltip" class="tooltip sw-suggestion sw-suggestion-noicon" >
                <p><%= Resources.DeviceStudioWebContent.WEBDATA_YK0_01%></p>
            </div>
        </td>
        <td>
            <asp:TextBox ID="frm_configureStep_node" runat="server" Text="1"  style="display:none"/>
            <asp:Label ID="frm_configureStep_nodeDescription" runat="server"/> <a href="" style=" color: #336699;text-decoration: underline;" id="frm_configureStep_nodeHR"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_41 %></a>
            <asp:TextBox ID="frm_NodeIdFromListResource" runat="server" Text=""  style="display:none"/>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="nodeValidator"
                ErrorMessage="<%$ Resources: DeviceStudioWebContent, WEBDATA_LF0_42 %>"
                ControlToValidate="frm_configureStep_node"
                runat="server" 
                CssClass="sw_ds_error_text"
                />
        </td>
    </tr>
    <tr>
        <td>
            <label for="configureKit_tags"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_3 %></label>
        </td>
        <td>
            <asp:TextBox ID="frm_configureStep_tags" runat="server" CssClass="long_input" /><br />
        </td>
        <td>
            <%--validation--%>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
                <%= Resources.DeviceStudioWebContent.WEBDATA_LF0_38 %>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <label for="frm_configureStep_description"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_6 %></label>
        </td>
        <td>
            <asp:TextBox ID="frm_configureStep_description" runat="server" CssClass="long_input" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <label for="frm_configureStep_author"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_7 %></label>
        </td>
        <td>
            <asp:TextBox ID="frm_configureStep_author" runat="server" CssClass="long_input" />
        </td>
        <td>
       <asp:RegularExpressionValidator 
                ID="RegularExpressionValidator1"
                runat="server" 
                ErrorMessage="<%$ Resources: DeviceStudioWebContent, WEBDATA_LF0_39 %>"
                ControlToValidate="frm_configureStep_author"
                ValidationExpression="(((?![sS][oO][lL][aA][rR][wW][iI][nN][dD][sS]).)*)" 	
                CssClass="sw_ds_error_text"
                ></asp:RegularExpressionValidator>
        </td>
    </tr>
    <asp:Panel runat="server" ID="frm_configureStep_advancedOptions" Visible="false">
        <tr>
            <td>
                <label for="frm_configureStep_pollingFrequency"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_40 %></label>
            </td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server" Text="300" Width="60" />
                seconds
            </td>
            <td>
                <%--validation--%>
            </td>
        </tr>
    </asp:Panel>
    <tr>
        <td colspan="3"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_8 %></td>
    </tr>
</table>

 <div id="selectordlg" style="display: none;">
        <div style="margin-bottom: 10px;"></div>
        <asp:PlaceHolder ID="NetObjectPickerPlace" runat="server" />
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
            <orion:LocalizableButton ID="selectordlgprimary" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_GK0_1 %>" OnClientClick="return false;"/>
            <orion:LocalizableButton ID="selectordlgcancel" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel"  OnClientClick="return false;" />
        </div>
</div>
