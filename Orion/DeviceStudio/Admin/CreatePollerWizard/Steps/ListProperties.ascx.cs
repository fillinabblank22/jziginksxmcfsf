﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DeviceStudio.Framework;
using SolarWinds.DeviceStudio.Web.Wizard;
using SolarWinds.DeviceStudio.Framework.Polling;
using SolarWinds.DeviceStudio.Common.Helpers;
using SolarWinds.DeviceStudio.Framework.Polling.Output;
using System.Configuration;
using SolarWinds.DeviceStudio.Common.Models;
using SolarWinds.MapEngine.Serialization;
using i18nExternalized = SolarWinds.DeviceStudio.Strings.Resources;


public partial class Orion_DeviceStudio_Admin_CreatePollerWizard_Steps_ListProperties : DSWizardStepBase
{
    public override string Name
    {
        get { return "ListProperties"; }
    }

    public override string DisplayName
    {
        get { return i18nExternalized.WEBDATA_LF0_58; }
    }

    public override string Description
    {
        get { return i18nExternalized.WEBDATA_LF0_59; }
    }

    public override void StoreData()
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        hiddenNetObjectId.Value = DataStore.pollerNetObject;
        hiddenTechnologyName.Value = DataStore.pollerTechnology;
        devModeSection.Visible = DevMode;
    }
    protected void cv1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        var pollingConfig = DataStore.poller.Configs.GetConfig<PollingConfig>(PollerConfigNames.PollingConfig);
        string errMsg;
        var outputConfig = pollingConfig.DataSourceOutputConfig;
        outputConfig.Bind(Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] as DataSource);
        bool isValid = outputConfig.TryValidate(out errMsg);
        if (isValid)
        {
            cv1.Visible = false;
            args.IsValid = true;
        }
        else
        {
            cv1.Visible = true;
            cv1.ErrorMessage = errMsg;
            args.IsValid = false;
        }
    }

    protected void lbConfigs_SelectedIndexChanged(object sender, EventArgs e)
    {
        var selection = lbConfigs.SelectedValue;
        switch (selection)
        {
            case "select":
                btnUpdate.Enabled = false;
                tbConfig.Text = string.Empty;
                break;
            default:
                btnUpdate.Enabled = true;
                var pollingConfig = DataStore.poller.Configs.GetConfig<Config>(selection);
                tbConfig.Text = SerializationHelper.ToXML<Config>(pollingConfig);
                break;
        }
    }

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        DataStore.poller.Configs[lbConfigs.SelectedValue] = tbConfig.Text;
    }
}
