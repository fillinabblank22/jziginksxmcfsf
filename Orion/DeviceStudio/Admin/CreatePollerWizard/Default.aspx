﻿<%@ Page Title="Create Poller Package" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" ClientIDMode="Static" CodeFile="Default.aspx.cs" Inherits="Orion_DeviceStudio_Admin_CreatePollerWizard_Default" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagPrefix="orion" TagName="ProgressIndicator" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/DeviceStudio/Controls/WhatsNewWithPollers.ascx" TagPrefix="orion" TagName="WhatsNewWithPollers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="JS_IncludeCore" File="OrionCore.js" runat="server" />
    <orion:Include ID="JS_IncludeExt" Framework="Ext" FrameworkVersion="4.2" runat="server" />
    <orion:Include ID="JS_IncludeWizardBundle" File="/DeviceStudio/js/CreatePollerWizard/WizardBaseBundle.js" runat="server" />
    <orion:Include ID="CSS_DeviceStudio" File="/DeviceStudio/styles/WizardBaseBundle.css" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton" HelpUrlFragment="OrionCorePHDeviceStudioCreatingPollers" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <orion:WhatsNewWithPollers runat="server"/>
    <div class="sw_ds_wizard">
        <h1><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_67 %></h1>
		 <div><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_68 %></div>
         <div class="TooltipLinkPlace">
            <a href="<%= HelpHelper.GetHelpUrl("npm", "OrionPHUniversalDevicePollerHome.htm") %>" class="TooltipLink" target="_blank"><span id="toolTip"></span></a>
            <script type="text/javascript">
                $().ready(function () {
                    $('#toolTip').html(GetParametrizedTooltipHtml('<%= Resources.DeviceStudioWebContent.WEBDATA_LF0_61 %>',
                        '<%= Resources.DeviceStudioWebContent.WEBDATA_LF0_62 %>',
                        '<%= Resources.DeviceStudioWebContent.WEBDATA_LF0_64 %>',
                        '<%= Resources.DeviceStudioWebContent.WEBDATA_LF0_65 %>',
                        '<%= Resources.DeviceStudioWebContent.WEBDATA_LF0_66 %>'));
                });
            </script>
        </div>
        <br />
        <orion:ProgressIndicator runat="server" ID="cdpProgress" IndicatorHeight="OneLine" />
        <input type="hidden" id="hiddenCurrentStep" value="" runat="server" />
        <div class="sw_ds_wizard_step">
            <asp:MultiView ID="MultiView1" runat="server" OnInit="MultiView1_Init" />
        </div>
    </div>
        
    <div class="sw_ds_buttonBar">
        <orion:LocalizableButton ClientIDMode="Static" runat="server" DisplayType="Secondary" ID="btnPrevCDPStep" OnClick="btnPrevCDPStep_Click" LocalizedText="Back"  />
        <orion:LocalizableButton ClientIDMode="Static" runat="server" DisplayType="Primary" ID="btnNextCDPStep" OnClick="btnNextCDPStep_Click" LocalizedText="Next" />
        <orion:LocalizableButton ClientIDMode="Static" runat="server" DisplayType="Primary" ID="btnFinishCDP" OnClick="btnFinishCDP_Click" LocalizedText="Submit" />
        &nbsp;&nbsp;&nbsp;
        <orion:LocalizableButton ClientIDMode="Static" runat="server" DisplayType="Secondary" ID="btnCancelCDP" OnClick="btnCancelCDP_Click" LocalizedText="Cancel" CausesValidation="false" />
    </div>
    <script type="text/javascript">
        $(function () {
            if ("<%= OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") {
                Ext.MessageBox.alert('<%= Resources.DeviceStudioWebContent.WEBDATA_PS0_3 %>', '<%= Resources.DeviceStudioWebContent.WEBDATA_PS0_4 %>', function () {
                    window.location = "/Orion/Admin/Pollers/ManagePollers.aspx";
                });
                return;
            }
            if (!SW.DeviceStudio.WhatIsNewInPollers.WasDialogHidden()) {
                SW.DeviceStudio.WhatIsNewInPollers.ShowDialog();
            }
			
			// MIB Browser reads the demo flag...
			SW.Core.namespace("SW.MIBBrowser").IsDemo = ("<%= OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") ? true : false;
        });
		
		
    </script>

</asp:Content>
