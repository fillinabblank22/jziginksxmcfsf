﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DeviceStudio_Admin_ThwackPollers : System.Web.UI.Page
{
    private static readonly JavaScriptSerializer JsonSerializer = new JavaScriptSerializer();
    protected String ThwackUserInfo = JsonSerializer.Serialize(new { name = String.Empty, pass = String.Empty });

    protected void Page_Load(object sender, EventArgs e)
    {
        var nCred = Session["ThwackCredential"] as NetworkCredential;
        if (nCred != null)
        {
            var uiCred = new
            {
                name = nCred.UserName,
                pass = nCred.Password
            };
            ThwackUserInfo = JsonSerializer.Serialize(uiCred);
        }
    }
}