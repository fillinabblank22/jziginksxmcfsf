﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="ThwackPollers.aspx.cs" Inherits="Orion_DeviceStudio_Admin_ThwackPollers"
    Title="<%$ Resources: DeviceStudioWebContent, WEBDATA_LH0_1 %>" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" TagName="TopPageItems" Src="~/Orion/Controls/ManagePollersTopPageControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="NetObjectPicker" Src="~/Orion/Controls/NetObjectPicker.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="Include2" runat="server" Framework="Ext" FrameworkVersion="3.4" Section="Top" SpecificOrder="0" />
    <orion:Include ID="Include5" runat="server" Framework="Ext" FrameworkVersion="4.2" Section="Top" SpecificOrder="1" />
    <orion:Include ID="Include6" runat="server" File="OrionMinReqs.js" />
    <orion:Include ID="Include7" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include8" runat="server" File="Admin/js/ThwackCommon.js" />
    <orion:Include ID="Include9" runat="server" Module="DeviceStudio" File="js/ThwackPollers/app.js" Section="Bottom" />
    <orion:Include ID="Include1" runat="server" Module="DeviceStudio" File="js/ThwackPollers/i18n.js" />
    <orion:Include ID="Include4" runat="server" Module="DeviceStudio" File="styles/ThwackPollers.css" />
    <orion:Include ID="Include3" runat="server" Module="DeviceStudio" File="js/ThwackPollers/testPoller.js" />

    <script type="text/javascript">
        SW.Core.namespace("SW.DeviceStudio").IsDemo = ("<%= OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") ? true : false;
        var thwackUserInfo = <%=ThwackUserInfo%>;

        Ext42.onReady(function () {
            // fix font style issue caused by EXTJS 4.2
            if ($('body').hasClass('x42-body')) {
                $('body').removeClass('x42-body');
            };
        });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <br />
    <div class="tableWidth" id="mainContent">
        <h1 class="sw-hdr-title"><%=Page.Title%></h1>
        <!-- place for top page items -->
        <orion:TopPageItems runat="server" />
        <div id="tabPanel" class="tab-top">
            <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <input type="hidden" name="ThwackPollers_AllowAdmin" id="ThwackPollers_AllowAdmin" value='<%=this.Profile.AllowAdmin%>' />
                        <input type="hidden" name="ThwackPollers_PageSize" id="ThwackPollers_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ThwackPollers_PageSize", "20")%>' />
						<input type="hidden" name="ThwackPollers_Locale" id="ThwackPollers_Locale" value='<%=(SolarWinds.Orion.Core.Common.i18n.LocaleConfiguration.PrimaryLocale.Substring(0, 2)) %>' />
                        <div id="appContent"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="selectordlg" style="display: none;">
        <div style="margin-bottom: 10px;"></div>
        <orion:NetObjectPicker runat="server" />
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
            <a id="nob_testPollerButton" class="sw-btn-primary sw-btn"><span class="sw-btn-c"><span class="sw-btn-t">
                <asp:Literal runat="server" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_LH0_18%>" /></span></span></a>
            <a id="nob_cancelButton" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t">
                <asp:Literal runat="server" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_LH0_19%>" /></span></span></a>
        </div>
    </div>

    <div id="progressDlg" class="hidden">
        <div class="dlgContent">
            <div class="statusText">
                &nbsp;
            </div>
            <table id="progressTable" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="progressLabel">
                        <%= DeviceStudioWebContent.WEBDATA_PS0_5 %>
                    </td>
                    <td>
                        <div id="progressBar" class="progressBar" />
                    </td>
                    <td>
                        <div id="progressPercentage">&nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="progressSeparator">&nbsp;
                    </td>
                </tr>
            </table>
            <div style="text-align: right">
                <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel"
                    CausesValidation="false" OnClientClick="SW.DeviceStudio.ThwackPollers.TestPoller.cancelTest(); return false;" />
            </div>
        </div>
    </div>

    <div id="finishedDlg" class="hidden">
        <div class="dlgContent">
            <p>
                <b><%= DeviceStudioWebContent.WEBDATA_PS0_7 %></b>
            </p>
            <p>
                <span id="pollerNameText"></span>
            </p>
            <p>
                <%= DeviceStudioWebContent.WEBDATA_PS0_8 %>
            </p>
            <table class="finishedOptionsTable">
                <tr>
                    <th><span id="optionConfigure"></span></th>
                </tr>
                <tr>
                    <td><%= DeviceStudioWebContent.WEBDATA_PS0_9 %></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th><%= DeviceStudioWebContent.WEBDATA_PS0_10 %></th>
                </tr>
                <tr>
                    <td><%= DeviceStudioWebContent.WEBDATA_PS0_11 %></td>
                </tr>
            </table>
            <div style="text-align: right">
                <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_PS0_6 %>"
                    CausesValidation="false" OnClientClick="SW.DeviceStudio.ThwackPollers.TestPoller.configurePoller(); return false;" />
                <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Done"
                    CausesValidation="false" OnClientClick="SW.DeviceStudio.ThwackPollers.TestPoller.closeFinishDialog(); return false;" />
            </div>
        </div>
    </div>
    <div id="testPollerResultsDialog" style="display: none; width: 890px,">
        <div style="margin-bottom: 10px;"></div>
        <div>
            <b><%= DeviceStudioWebContent.WEBDATA_LH0_20 %></b>
            <div><%= DeviceStudioWebContent.WEBDATA_LH0_21 %></div>
        </div>
        <div id="testPollerResultsTable" style="overflow: hidden"></div>
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
            <a id="testPollerResultsDialog_installButton" class="sw-btn-primary sw-btn"><span class="sw-btn-c"><span class="sw-btn-t">
                <asp:Literal runat="server" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_LH0_22%>" /></span></span></a>
            <a id="testPollerResultsDialog_cancelButton" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t">
                <asp:Literal runat="server" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_LH0_19%>" /></span></span></a>
        </div>
    </div>

    <div id="testPollerErrorDialog" style="display: none; width: 890px,">
        <div style="margin-bottom: 10px;"></div>
        <div>
            <b><%= DeviceStudioWebContent.WEBDATA_LH0_20 %></b>
            <div><%= DeviceStudioWebContent.WEBDATA_LH0_23 %></div>
            <div><a class="link" target="_blank" href="<%= HelpHelper.GetHelpUrl("orion", "OrionCorePHDeviceStudioTestDeviceStudioPoller.htm") %>">&raquo;<%= DeviceStudioWebContent.WEBDATA_LH0_24 %></a></div>
            <div id="testPollerErrorDialog_errorMessage"></div>
        </div>
        <div>
            <table style="width: 100%">
                <tr>
                    <td>
                        <div class="installWarningWrapper">
                            <div class="sw-suggestion sw-suggestion-warn">
                                <span class="sw-suggestion-icon"></span>
                                <div>
                                    <b><%= DeviceStudioWebContent.WEBDATA_LH0_25 %></b><br />
                                    <input type="checkbox" id="testPollerErrorDialog_confirmInstall">
                                    <label for="testPollerErrorDialog_confirmInstall"><%= DeviceStudioWebContent.WEBDATA_LH0_26 %></label>
                                    <div class="installButtonWrapper">
                                        <a id="testPollerErrorDialog_installButton" class="sw-btn-primary sw-btn"><span class="sw-btn-c"><span class="sw-btn-t">
                                            <asp:Literal runat="server" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_LH0_22%>" /></span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td align="right"><div class="cancelButtonWrapper">
                        <a id="testPollerErrorDialog_cancelButton" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t">
                            <asp:Literal runat="server" Text="<%$ Resources: DeviceStudioWebContent, WEBDATA_LH0_19%>" /></span></span></a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionCorePHDeviceStudioThwackPollers" />
</asp:Content>
