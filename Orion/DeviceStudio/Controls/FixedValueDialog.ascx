﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FixedValueDialog.ascx.cs" Inherits="Orion_DeviceStudio_Controls_FixedValueDialog" %>
<orion:Include ID="IncludeJS1" runat="server" File="/DeviceStudio/js/CreatePollerWizard/DeviceStudio.CreatePollerWizard.FixedValueDialog.js" />
<div id="FVD" style="display:none;">
    <p><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_08 %></p>
    
    <%= Resources.DeviceStudioWebContent.WEBDATA_PD0_06 %> <span id="FVDproperty"></span> <%--Constant value for: --%>
    <input type="text" id="FVDinput" style="margin-left: 0; width:97%;" runat="server" />
    
    <p class="sw_ds_error_text"></p>
    <div class="sw_ds_buttonBar">
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Primary" ID="submitFVD" LocalizedText="Submit" />
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Secondary" ID="cancelFVD" LocalizedText="Cancel" />
    </div>
</div>