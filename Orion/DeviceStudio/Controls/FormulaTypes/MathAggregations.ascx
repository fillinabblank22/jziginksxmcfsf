﻿<%@ Control Language="C#" ClassName="MathAggregations" ClientIDMode="Static"%>


<script type="text/javascript">
    SW.Core.namespace("SW.DeviceStudio.CreatePollerWizard.FormulaEditor").MathAggregations = function () {

        var initialized = false;

        function getColumns() {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("GetColumns", "{}", function (data, status) {
                $("#ds_fe_aggregation_property").html("");
                $.each(data.d, function (i, o) {
                    $("#ds_fe_aggregation_property").append("<option value='" + o + "'>" + o + "</option>");
                });
            });
        }

        function testResult(text) {
            $('#ds_fe_aggregation_testresult').html(text);
        }

        function test(onSuccessCallback, onFailCallback) {

            // Don't validate editor here. It's not our responsibility.
            // It's validation should be done in separate method call before calling this method.

            testResult('');

            var resultType = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultProperty();
            // ↓type validation isn't necessary -> there's always something selected;
            var type = $("#ds_fe_aggregation_operations").val();
            var column = $("#ds_fe_aggregation_property").val();
            if (!column) {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('emptyColumn', 'Table column is not set.');
                if (onFailCallback) onFailCallback();
                return;
            }
            else {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('emptyColumn');
            }
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("TestAggregation", "{ 'property':'" + resultType + "','type':'" + type + "', 'column':'" + column + "' }", function (data, status) {
                if (!data.d.Passed) {
                    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('aggregationsTest', data.d.Message);
                    if (onFailCallback) onFailCallback();
                }
                else {
                    testResult(data.d.Message);
                    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('aggregationsTest');
                    if (onSuccessCallback) onSuccessCallback();
                }
            });
        }

        function init() {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Aggregations: init');

            getColumns();

            if (initialized)
                return;

            $('#ds_fe_aggregation_test').on('click', function () {
                if (SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Validate()) {
                    test();
                }
            });

            initialized = true;
        }

        function submit() {

            // No validation here.
            // Validation of editor and active tab should be called before calling submit.

            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Aggregations: submit');

            var resultType = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultProperty();
            var type = $("#ds_fe_aggregation_operations").val();
            var column = $("#ds_fe_aggregation_property").val();

            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("AddAggregation", "{ 'property':'" + resultType + "','type':'" + type + "', 'column':'" + column + "' }", function (data, status) {
            });
        }

        function show() {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Aggregations: show');
            $("#FormulaEditor_ResultTypeSection").hide();
            $("#FormulaEditor_MathAggregations").show();
        }

        function hide() {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Aggregations: hide');
            $("#FormulaEditor_MathAggregations").hide();
            $("#FormulaEditor_ResultTypeSection").show();
        }

        function onResultTypeChange(newtype) {
        }

        function clear() {
            //getColumns();
            $('#ds_fe_aggregation_testresult').html('');
        }

        return {
            DisplayName: "Aggregation", // used in type selection UI
            Description: "find the average from a column of values", // used in type selection UI
            Order: 2, // order in dropdown
            Init: init,
            Show: show,
            Hide: hide,
            Submit: submit,
            OnResultTypeChange: onResultTypeChange,
            Validate: test,
            Clear: clear
        }
    } ();

    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddType(SW.DeviceStudio.CreatePollerWizard.FormulaEditor.MathAggregations);

</script>

<div id="FormulaEditor_MathAggregations" class="hidden formulaType">

Calculate item as 
<select id="ds_fe_aggregation_operations">
    <option value="0">rows #</option>
    <option value="1">summary</option>
    <option value="2">average</option>
</select>
of
<select id="ds_fe_aggregation_property">
</select>

<div style="margin-top:10px;">
    <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Small" ID="ds_fe_aggregation_test" LocalizedText="Test" />
    <span id="ds_fe_aggregation_testresult" style="display:inline-block;margin-left:10px"></span>
</div>

</div>