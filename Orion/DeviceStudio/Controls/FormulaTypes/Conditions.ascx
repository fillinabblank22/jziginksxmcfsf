﻿<%@ Control Language="C#" ClassName="Conditions" ClientIDMode="Static"%>


<script type="text/javascript">
    SW.Core.namespace("SW.DeviceStudio.CreatePollerWizard.FormulaEditor").Conditions = function () {

        var resultType;
        var initialized = false;

        function initConditions() {
            var data = { conditions: [{ left: "true", right: ""}] };
            var template = $('#CondtitionTemplate').html();
            var filledTemplate = _.template(template, data);
            $("#ds_fe_conditions").html(filledTemplate);
        }

        function addCondition() {
            var data = { conditions: [{ left: "", right: ""}] };
            var template = $('#CondtitionTemplate').html();
            var filledTemplate = _.template(template, data);
            $("#ds_fe_conditions").prepend(filledTemplate);
        }

        function removeCondition() {
            // remove tr from table
            $(this).parent().parent().remove();
        }

        function testResult(text) {
            $('#ds_fe_conditions_testresult').html(text);
        }

        function test(onSuccessCallback, onFailCallback) {

            // Don't validate editor here. It's not our responsibility.
            // It's validation should be done in separate method call before calling this method.

            testResult('');

            var output = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultProperty();

            var conditions = getConditions();
            if (!conditions) {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('conditionsList', 'There is an error in the conditions list.');
                if (onFailCallback) onFailCallback();
                return;
            }
            else {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('conditionsList');
            }

            if (resultType) {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("TestConditionTable", "{ 'table':'" + resultType + "','column':'" + output + "', 'conditions':" + conditions + " }", function (data, status) {
                    if (!data.d.Passed) {
                        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('conditionsTest', data.d.Message);
                        if (onFailCallback) onFailCallback();
                    }
                    else {
                        testResult(data.d.Message);
                        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('conditionsTest');
                        if (onSuccessCallback) onSuccessCallback();
                    }
                });
            }
            else {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("TestCondition", "{ 'property':'" + output + "', 'conditions':" + conditions + " }", function (data, status) {
                    if (!data.d.Passed) {
                        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('conditionsTest', data.d.Message);
                        if (onFailCallback) onFailCallback();
                    }
                    else {
                        testResult(data.d.Message);
                        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('conditionsTest');
                        if (onSuccessCallback) onSuccessCallback();
                    }
                });
            }
        }

        function getConditions() {
            var error = false;
            var expressions = new Array();

            $.each($("#ds_fe_conditions tr"), function (i, o) {
                var left = $.trim($(".left", o).val());
                var right = $.trim($(".right", o).val());

                if (!left || !right) {
                    error = true;
                    return;
                }

                left = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.EncodeJSON(left);
                right = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.EncodeJSON(right);

                expressions.push("{'__type':'FormulaEditor+KVP', 'Left':'" + left + "', 'Right':'" + right + "'}");
            });

            if (error) {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('emptyCondition', 'Condition is empty.');
                return null;
            }
            else {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('emptyCondition', 'Condition is empty/.');
            }

            return "[" + expressions.join(",") + "]";
        }

        function init() {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Conditions: init');
            resultType = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultType();

            if (initialized)
                return;

            initConditions();

            $('#ds_fe_conditions_add').on('click', addCondition);
            $('#ds_fe_conditions_test').on('click', function () {
                if (SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Validate()) {
                    test();
                }
            });

            $(document).on("click", "#ds_fe_conditions .removeCondition", removeCondition);

            initialized = true;
        }

        function submit() {

            // No validation here.
            // Validation of editor and active tab should be called before calling submit.

            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Conditions: submit');

            var output = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultProperty();
            var conditions = getConditions();

            if (resultType) {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("AddConditionTable", "{ 'table':'" + resultType + "','column':'" + output + "', 'conditions':" + conditions + " }", function (data, status) {
                });
            }
            else {
                SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("AddCondition", "{ 'property':'" + output + "', 'conditions':" + conditions + " }", function (data, status) {
                });
            }
        }

        function show() {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Conditions: show');
            $('#FormulaEditor_Conditions').show();
        }

        function hide() {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log('Conditions: hide');
            $('#FormulaEditor_Conditions').hide();
        }

        function onResultTypeChange(newtype) {
            resultType = newtype;
        }

        function clear() {
            initConditions();
            $('#ds_fe_conditions_testresult').html('');
        }

        return {
            DisplayName: "Condition", // used in type selection UI
            Description: "if value X is returned, show Y", // used in type selection UI
            Order: 1, // order in dropdown
            Init: init,
            Show: show,
            Hide: hide,
            Submit: submit,
            OnResultTypeChange: onResultTypeChange,
            Validate: test,
            Clear: clear
        }
    } ();

    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddType(SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Conditions);

</script>

<script id="CondtitionTemplate" type="text/x-template">
{#  _.each(conditions, function(current, index) { #}
    <tr>
		<td>If&nbsp;</td>    
        <td><input placeholder="condition" type="text" value="{{current.left}}" class="left"/></td>
        <td>then return</td>
		<td><input placeholder="formula" type="text" value="{{current.right}}" class="right"/></td>
        <td><img src="/Orion/images/NestedExpressionBuilder/delete_icon.png" class="removeCondition" alt="remove" /></td>
    </tr>
{# }); #}
</script>

<div id="FormulaEditor_Conditions" class="hidden formulaType">

    <table id="ds_fe_conditions">
    </table>

    <a id="ds_fe_conditions_add">Add Condition</a>

	<p style="font-size:11px; color:#808080; margin:10px 0 0 0;">Use [OID] and +, -, * etc. to represent the input/output value and condition. For example: [OID] == 0<br />
		<a href="javascript:void(0)" class="sw_ds_link">» More Examples</a>
	</p>

    <div style="margin-top:10px;">
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Small" ID="ds_fe_conditions_test" LocalizedText="Test" />
        <span id="ds_fe_conditions_testresult"></span>
    </div>

</div>