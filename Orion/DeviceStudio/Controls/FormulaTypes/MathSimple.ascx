﻿<%@ Control Language="C#" ClassName="MathSimple" ClientIDMode="Static"%>

<div id="FormulaEditor_MathSimple" class="hidden formulaType">
    
    <div>
        <select name="" id="ds_fe_mathsimple_operations">
            <option value=""><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_97 %></option>
            <optgroup label="------------------------------">
                <option value="KiloToByte()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_KiloToByte, "KiloToByte") %></option>
                <option value="MegaToByte()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_MegaToByte, "MegaToByte") %></option>
                <option value="GigaToByte()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_GigaToByte, "GigaToByte") %></option>
            </optgroup>
            <optgroup label="------------------------------">
                <option value="Avg()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Average, "Average") %></option>
                <option value="Sum()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Sum, "Sum") %></option>
                <option value="Count()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Count, "Count") %></option>
            </optgroup>
            <optgroup label="------------------------------">
                <option value="If(,,)"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Condition, "Condition") %></option>
            </optgroup>
            <optgroup label="------------------------------">
                <option value="Truncate()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Truncate, "Truncate") %></option>
                <option value="Length()"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Length, "Length") %></option>
                <option value="Replace(,,)"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Replace, "Replace") %></option>
                <option value="IndexOf(,)"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_IndexOf, "IndexOf") %></option>
                <option value="SubString(,,)"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_SubString, "SubString") %></option>	
                <option value="Regex(,)"><%= string.Format(Resources.DeviceStudioWebContent.FormulaEditor_Function_Regex, "Regex") %></option>	
            </optgroup>
        </select>
        <select id="ds_fe_mathsimple_variables"></select>
    </div>
    
    <textarea id="ds_fe_mathsimple_formula" class="formula"></textarea>
    <div style="margin:6px 0;">
        <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Small" ID="ds_fe_mathsimple_test" LocalizedText="Test" />&nbsp;<span id="ds_fe_mathsimple_netobject"></span>
        <br />
        <span id="ds_fe_mathsimple_testresult"></span>
        <span id="ds_fe_mathsimple_testerror" style="color:red;"></span>
        <table id="ds_fe_mathsimple_visualization">
            <tr>
                <td style="width:50%;margin:0;padding:0;vertical-align:top;">
                    <strong><u><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_02 %>:</u></strong>
                    <div class="ds_fe_visualization_input" style="padding-top:10px;"></div>
                </td>
                <td style="width:50%;margin:0;padding:0;vertical-align:top;">
                    <strong><u><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_03 %>:</u></strong>
                    <div class="ds_fe_visualization_output" style="padding-top:10px;"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid #CCC;">&nbsp;</td>
            </tr>
        </table>
    </div>
</div>