﻿<%@ Control Language="C#" ClassName="FormulaTester" %>
<orion:Include ID="IncludeJS1" runat="server" File="/DeviceStudio/js/CreatePollerWizard/FormulaTesterBeta.js" />
<table id="sw_ds_formulaTester">
    <tr>
        <td colspan="2"><strong><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_79 %></strong></td>
    </tr>
    <tr id="sw_ds_FormulaTester_InputRow">
        <td>Input:</td>
        <td>
            <ul class="sw_ds_fe_inputList"></ul>
            <div class="sw_ds_fe_inputListControls">
                <a class="sw_ds_fe_loadBtn" href='javascript:void(0)'><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_78 %></a>
            </div>
        </td>
    </tr>
    <tr id="sw_ds_FormulaTester_OutputRow">
        <td>
            <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Small" CssClass="sw_ds_fe_testBtn" ID="lbTest" LocalizedText="Test" />
        </td>
        <td>
            <strong class="sw_ds_FormulaTester_OutputLabel"></strong>
        </td>
    </tr>
</table>