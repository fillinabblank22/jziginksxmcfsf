﻿using SolarWinds.Orion.Web.DAL;
using System.Web;

public partial class Orion_DeviceStudio_Controls_WhatsNewWithPollers : System.Web.UI.UserControl
{
    /// <summary>
    /// Returns TRUE if was dialog explicitly hidden by user.
    /// </summary>
    public bool WasDialogHidden
    {
        get
        {
            return IsPostBack || bool.Parse(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "DontShowWhatIsNewWithPollersDialog", "false"));
        }
    }

    public string ConvertTagsToHtml(string target)
    {
        return target.Replace("[[", "<")
            .Replace("]]", ">");
    }
}