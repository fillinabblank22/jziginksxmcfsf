﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormulaEditor.ascx.cs" Inherits="Orion_DeviceStudio_Controls_FormulaEditor" ClientIDMode="Static"%>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register Src="~/Orion/DeviceStudio/Controls/FormulaTypes/MathSimple.ascx" TagPrefix="ds" TagName="MathSimple" %>
<%@ Register Src="~/Orion/DeviceStudio/Controls/FormulaTypes/Conditions.ascx" TagPrefix="ds" TagName="Conditions" %>
<%@ Register Src="~/Orion/DeviceStudio/Controls/FormulaTypes/MathAggregations.ascx" TagPrefix="ds" TagName="MathAggregations" %>

<orion:Include ID="IncludeJS1" runat="server" File="/DeviceStudio/js/CreatePollerWizard/FormulaEditor.js" />
<orion:Include ID="IncludeJS2" runat="server" File="/DeviceStudio/js/CreatePollerWizard/MathSimple.js" />
<orion:Include ID="IncludeJS3" runat="server" File="/DeviceStudio/js/jquery.sw.cyaable.js" />
<orion:Include ID="IncludeCSS1" runat="server" File="/DeviceStudio/styles/jquery.sw.cyaable.css" />

<style type="text/css"> 
    .hidden { display: none }
    .leftAlignment { text-align: left }
    .rightAlignment { text-align: right }
</style>

<div id="FormulaEditor" class="hidden">
    <div>
        <p>
            <%= Resources.DeviceStudioWebContent.WEBDATA_PD0_01 %>
        </p>
        <ul>
            <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_72 %></li> 
            <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_73 %></li>
            <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_74 %></li>
        </ul>
        <p>
            » <a href="<%= HelpHelper.GetHelpUrl("orion", "OrionCorePHDeviceStudioWhatIsFormula.htm") %>" style="font-size:11px" target="_blank"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_70 %></a><br />
            » <a href="<%= HelpHelper.GetHelpUrl("orion", "OrionCorePHDeviceStudioCommonFormulas.htm") %>" style="font-size:11px" target="_blank"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_71 %></a>
        </p>
    </div>

    <table id="FormulaEditor_MetaTable">
        <tr>
            <td style="vertical-align:middle;"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_75 %></td>
            <td><input id="FormulaEditor_ResultProperty" type="text" disabled="disabled"/></td>
        </tr>
    </table>

    <ds:MathSimple ID="MathSimple1" runat="server" />

    <p>
        <strong><span id="fePropertyNameLabel"></span> <%= Resources.DeviceStudioWebContent.WEBDATA_LF0_93 %></strong><br />
        <span id="fePropertyDescriptionLabel"></span>
    </p>

    <ul id="FormulaEditor_Errors">
    </ul>

    <div id="FormulaEditor_Debug" class="hidden">
        <div id="FormulaEditor_DebugType"></div>
        <div id="FormulaEditor_DebugMessage"></div>
        <div id="FormulaEditor_DebugStackTrace"></div>
    </div>

    <div id="FormulaEditor_Actions">
        <orion:LocalizableButtonLink ID="FormulaEditor_Submit" runat="server" DisplayType="Primary" LocalizedText="Submit" />
        <orion:LocalizableButtonLink ID="FormulaEditor_Cancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" />
    </div>
</div>