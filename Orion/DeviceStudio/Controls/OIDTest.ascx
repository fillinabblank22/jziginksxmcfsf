﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OIDTest.ascx.cs" Inherits="Orion_DeviceStudio_Controls_OIDTest" ClientIDMode="Static" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register Src="~/Orion/Controls/DynamicControlsContainer.ascx" TagPrefix="orion" TagName="notificationsContainer" %>
<orion:Include ID="Include1" runat="server" File="../DeviceStudio/js/CreatePollerWizard/OIDTest.js" />
<orion:Include ID="Include5" runat="server" File="OidPickerDefinitions.js" />
<orion:Include ID="Include2" runat="server" File="OidPicker.js" />
<orion:Include ID="Include3" runat="server" File="OidPicker.css" />
<orion:Include ID="Include4" runat="server" File="../DeviceStudio/styles/DeviceStudio.css" />

<div id="OIDTest" style="display: none; width: 869px; height: 604px; border: 1px solid gray; padding: 10px; background:white;">
    <strong><div id="propertyNameHolder"></div></strong>
    <div id="propertyDescriptionHolder"></div><br />
    <strong><%= Resources.DeviceStudioWebContent.WEBDATA_JF0_10 %></strong>
    <ul id="helpText" class="sw-ds-oidpicker-list">
        <li><%= Resources.DeviceStudioWebContent.WEBDATA_JF0_11 %></li>
        <li><%= Resources.DeviceStudioWebContent.WEBDATA_JF0_12 %></li>
        <li><%= Resources.DeviceStudioWebContent.WEBDATA_JF0_13 %></li>
        <li>
            <a href="<%= WhatIsMibKbLink %>" target="_blank"><%= Resources.DeviceStudioWebContent.WEBDATA_JF0_14 %></a>
        </li>
    </ul>
    <br />
    <strong><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_10 %></strong> <%--Don't need an OID?--%>
    <ul id="dontNeedAnOID" class="sw-ds-oidpicker-list">
        <li><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_11 %></li> <%--You can manually {fixedvalue}enter a fixed value{a} instead.--%>
    </ul>
    <br/>
    <orion:notificationsContainer runat="server" WebSettingsKey="MibBrowserNotificationControls" CssClasses="mibBrowser-notifications" />
    <div class="sw_ds_oidtest_objectTitle"><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_01 %></div>
    <div id="pickerHolder" style="width:870px; height:604px;"></div>
    <div id="loaderBackground" class="sw_ds_oidtest_loaderBackground"></div>
    <div id="loader" class="sw_ds_oidtest_loader">
        <span class="sw_ds_oidtest_text"><img src="/Orion/images/AJAX-Loader.gif" style="margin-right: 5px;"/><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_80 %></span>
    </div>
    <div class="sw_ds_oidtest_objectTitle"><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_02 %></div>
    <input id="manualEntry" type="checkbox"><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_08 %></input><br />
    <div id="manualEntryPanel" style="display:none;">
        <table>
            <tr>
                <td><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_75 %></td>
                <td><input id="manualOIDName" type="text" /><div id="manualOIDNameMessageFrame" class="sw_ds_oidtest_message sw-suggestion" style="display:none"><div class="sw-suggestion-icon" ></div><span></span></div></td>
            </tr>
            <tr>
                <td><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_03 %></td>
                <td><input id="manualOID" type="text" /><div id="manualOIDMessageFrame" class="sw_ds_oidtest_message sw-suggestion" style="display:none"><div class="sw-suggestion-icon" ></div><span></span></div></td>
            </tr>
            <tr>
                <td><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_04 %><br/><a href="<%=HelpHelper.GetHelpUrl("orion","OrionCorePHDeviceStudioSNMPGETType.htm") %>" target="_blank"><%= Resources.DeviceStudioWebContent.WEBDATA_JF0_08 %></a></td>
                <td>
                    <input id="manualOIDGet" type="radio" name="SNMPType" /><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_05 %>
                    <input id="manualOIDGetNext" type="radio" name="SNMPType" checked="checked" /><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_06 %>
                    <input id="manualOIDGetTable" type="radio" name="SNMPType" /><%= Resources.DeviceStudioWebContent.WEBDATA_ZB0_07 %>
                    <div id="manualOIDSNMPMessageFrame" class="sw_ds_oidtest_message sw-suggestion" style="display:none"><div class="sw-suggestion-icon" ></div><span></span></div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><a id="manualOIDPollCurrentValue" onclick="return false;" class="sw-btn-secondary sw-btn sw-btn-cancel"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.DeviceStudioWebContent.WEBDATA_JF0_09 %></span></span></a></td>
            </tr>
        </table>
    </div>
    <div class="sw_ds_oidtest_buttonsOffset">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div id="oidPickerError" class="sw-suggestion sw-suggestion-fail" style="display:none"><div class="sw-suggestion-icon" ></div><span id="oidPickerErrorMessage"></span></div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <a id="btnOK" onclick="return false;" class="sw-btn-primary sw-btn" automation="OK"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_81 %></span></span></a>
                    <a id="btnCancel" onclick="return false;" class="sw-btn-secondary sw-btn sw-btn-cancel" automation="Cancel"><span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_82 %></span></span></a>
                </td>
            </tr>
        </table>
    </div>
</div>
