﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNewWithPollers.ascx.cs" Inherits="Orion_DeviceStudio_Controls_WhatsNewWithPollers" %>
<asp:Panel ID="WhatIsNewDialog" CssClass="whatIsNew" runat="server">
        <div class="whatIsNew-Header">
            <span><b><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_4 %></b></span><br/>
            <div style="margin-top: 15px;">
                <p><%= ConvertTagsToHtml(Resources.DeviceStudioWebContent.WEBDATA_GK0_5) %></p>
                <p><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_6 %></p>
                <p><%= ConvertTagsToHtml(Resources.DeviceStudioWebContent.WEBDATA_GK0_7) %></p>
            </div>
        </div>
        <div class="whatIsNew-Body">
        <div class="whatIsNew-WorkflowSection">
            <table>
                <tr>
                    <td class="content">
                        <span class="title"><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_8 %></span>
                        <ul>
                            <li><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_9 %></li>                            
                        </ul>
                    </td>
                    <td class="empty"></td>
                    <td class="content">
                        <span class="title"><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_10 %></span>
                        <ul>
                            <li><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_11 %></li>
                            <li><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_12 %></li>
                            <li><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_05 %></li> <%--Enter a fixed value.--%>
                        </ul>
                    </td>
                    <td class="empty" style="width: 17px;"></td>
                    <td class="content">
                        <span class="title"><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_13 %></span>
                        <ul>
                            <li><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_14 %></li>
                            <li><%= Resources.DeviceStudioWebContent.WEBDATA_GK0_15 %></li>
                        </ul>
                    </td>
                    <td class="empty" style="width: 19px;"></td>
                </tr>
            </table>
        </div>
            <div class="whatIsNew-WorkflowSection-Footer" style="background-image: url('/Orion/DeviceStudio/Images/whatsnewinpollers.png');">
        </div>
        </div>
        <table class = "buttonsBar" style="width: 680px;padding-top:5px;">
            <tr>
                <td style="padding-left:20px;width: 535px;"><input type="checkbox" id="chbDontShowThisAgain" /><label for="chbDontShowThisAgain">&nbsp;<%= Resources.DeviceStudioWebContent.WEBDATA_GK0_16 %></label></td>
                <td><span ID="btnGetStarted" /></td>
            </tr>
        </table>
    </asp:Panel>
    <style>
        .whatIsNew {
            display: none;
            overflow: hidden !important;
            background-color: #FFFFFF !important;
            }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $(window).resize(function () {
                $("#" + "<%= WhatIsNewDialog.ClientID %>").dialog("option", "position", "center");
            });
            var dialog = $("#" + "<%= WhatIsNewDialog.ClientID %>").dialog({
                modal: true,
                draggable: true,
                resizable: false,
                autoOpen: false,
                width: "auto",
                height: "auto",
                show: 'blind',
                hide: 'blind',
                title: "<%= Resources.DeviceStudioWebContent.WEBDATA_GK0_3 %>",
                close: function () {
                    SW.DeviceStudio.WhatIsNewInPollers.SaveShowStatus(function () {
                    }, function () {
                    });
                },
                open: function () {
                    
                }
            });

            SW.DeviceStudio.WhatIsNewInPollers = function (dialog) {
                // CONSTRUCTOR
                var wasDialogHidden = eval("<%= this.WasDialogHidden %>".toLowerCase());

                $("#chbDontShowThisAgain").prop("checked", wasDialogHidden);

                $("#btnGetStarted").empty();
                $(SW.Core.Widgets.Button('<%= Resources.DeviceStudioWebContent.WEBDATA_GK0_17 %>', { type: 'primary', id: 'btnGetStarted1' })).appendTo("#btnGetStarted").click(function () {
                    SW.DeviceStudio.WhatIsNewInPollers.HideDialog();
                });

                // PRIVATE METHODS

                var showDialog = function (closeFunction) {
                    if (typeof (closeFunction) != "undefined") {
                        dialog.on("dialogclose", closeFunction);
                    } else {
                        dialog.off("dialogclose");
                    }

                    dialog.dialog("open");
                };

                // PUBLIC METHODS
                return {
                    WasDialogHidden: function () {
                        return wasDialogHidden;
                    },

                    ShowDialog: function (closeFunction) {
                        showDialog(closeFunction);
                    },

                    HideDialog: function () {
                        dialog.dialog("close");
                    },
                    SaveShowStatus: function (onSuccess, onFailure) {
                        SW.Core.Services.callWebService('/Orion/DeviceStudio/Services/Wizard.asmx', "SetShowWhatsNewInPollersDialog", { show: !$("#chbDontShowThisAgain").prop("checked") }, onSuccess, onFailure);
                    }
                };
            }(dialog);
        });


    </script>
