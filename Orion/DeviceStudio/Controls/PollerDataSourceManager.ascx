﻿<%@ Control Language="C#" ClassName="PollerDataSourceManager" %>
<%@ Register TagPrefix="ds" TagName="FormulaEditor" Src="~/Orion/DeviceStudio/Controls/FormulaEditor.ascx" %>
<%@ Register TagPrefix="ds" TagName="OIDPicker" Src="~/Orion/DeviceStudio/Controls/OIDTest.ascx" %>
<%@ Register TagPrefix="ds" TagName="FixedValueDialog" Src="~/Orion/DeviceStudio/Controls/FixedValueDialog.ascx" %>

<orion:Include ID="JS_DSM" runat="server" File="/DeviceStudio/js/CreatePollerWizard/DeviceStudio.CreatePollerWizard.DataSourceManager.js" />
<orion:Include ID="JS_ExpressionHelper" runat="server" File="/DeviceStudio/js/CreatePollerWizard/ExpressionHelper.js" />


<ds:FormulaEditor ID="fe1" runat="server" />
<ds:OIDPicker ID="OIDTest" runat="server" />
<ds:FixedValueDialog ID="fvd1" runat="server" />

<div id="cdpDataSourceDlg">

  <div id="sw_ds_DataSourceManager_PanelWrapper">
    <div id="sw_ds_DatasourceManager_CompatiblePanel" class="sw_ds_DatasourceManager_panel">
      <p><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_83 %></p>
      <ul>
        <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_84 %></li>
        <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_85 %></li>
        <li><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_09 %></li> <%--» {fixedvalue}Select an additional fixed value{a} and {formulaeditor}combine the values{a}.--%>
      </ul>
      <div class="sw_ds_valueVisualization"></div>
    </div>
    <div id="sw_ds_DatasourceManager_IncompatiblePanel" class="sw_ds_DatasourceManager_panel">
      <p id="incompatibleTable"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_86 %></p>
      <p id="incompatibleProperty"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_87 %></p>
      <p id="incompatiblePropertyTable"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_88 %></p>
      <p id="incompatibleTableType"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_95 %></p>
      <div class="sw_ds_valueVisualization"></div>
    </div>
    <div id="sw_ds_DatasourceManager_ErrorPanel" class="sw_ds_DatasourceManager_panel">
      <div id="errorPanelDefultMessage">
        <p><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_89 %></p>
        <ul>
          <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_90 %></li>
          <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_91 %></li>
          <li><%= Resources.DeviceStudioWebContent.WEBDATA_PD0_09 %></li> <%--» {fixedvalue}Select an additional fixed value{a} and {formulaeditor}combine the values{a}.--%>
        </ul>
        <br />
        <p style="margin: 0"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_92 %></p>
      </div>
    </div>
    <div id="sw_ds_DatasourceManager_UnsupportedPanel" class="sw_ds_DatasourceManager_panel">
      <div id="errorNullvalueProperty">
        <p><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_102 %></p>
        <ul class="incompatibleOptionsList">
          <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_104 %></li>  
		  <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_106 %></li>		  
          <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_105 %></li>
        </ul>
      </div>
      <div id="errorNullvalueTable">
        <p><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_103 %></p>
        <ul class="incompatibleOptionsList">
          <li><a data-target="oidpicker" href="javascript:void(0)"><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_104 %></a></li>
          <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_106 %></li>
          <li><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_105 %></li>
        </ul>
      </div>

    </div>
  </div>

  <p>
    <strong><span id="cdpDataSourcePropertyNameLabel"></span><span id="cdpDataSourcePropertyRequiredLabel"></span><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_93 %></strong><br />
    <span id="cdpDataSourcePropertyDescriptionLabel"></span>
  </p>

  <table style="width: 100%">
    <tr>
      <td>
        <strong><%= Resources.DeviceStudioWebContent.WEBDATA_LF0_94 %></strong>
      </td>
      <td style="white-space: nowrap; text-align: right; display: none;" id="cdpDataSourceSearch">
        <input type="text" id="cdpDataSourceSearchBox" placeholder="Search ..." />
        <asp:ImageButton ID="cdpDataSourceSearchClear" ImageUrl="~/Orion/images/clear_button.gif" runat="server" />
        <asp:ImageButton ID="cdpDataSourceSearchBtn" ImageUrl="~/Orion/images/search_button.gif" runat="server" />
      </td>
    </tr>
  </table>

  <div id="cdpDataSourceGrid"></div>

  <div id="cdpDataSourceActions">
    <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Small" ID="cdpDataSourceBrowseActionButton" LocalizedText="CustomText" Text="<%$ Resources:DeviceStudioWebContent, WEBDATA_LF0_99 %>" />
    <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Small" ID="cdpDataSourceCalculateActionButton" LocalizedText="CustomText" Text="<%$ Resources:DeviceStudioWebContent, WEBDATA_LF0_100 %>" />
  <orion:LocalizableButtonLink 
	ClientIDMode="Static" 
	runat="server" 
	DisplayType="Small" 
	ID="cdpDataSourceFixedValueActionButton" 
	LocalizedText="CustomText" 
	Text="<%$ Resources:DeviceStudioWebContent, WEBDATA_PS0_12 %>" />
  </div>

  <div class="sw_ds_buttonBar">
    <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Primary" ID="submitBtn" LocalizedText="CustomText" Text="<%$ Resources:DeviceStudioWebContent, WEBDATA_LF0_101 %>" />
    <orion:LocalizableButtonLink ClientIDMode="Static" runat="server" DisplayType="Secondary" ID="cancelBtn" LocalizedText="Cancel" />
  </div>
</div>
