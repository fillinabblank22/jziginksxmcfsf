﻿Ext.namespace('SW.DeviceStudio');
Ext.namespace('SW.DeviceStudio.Importer');
Ext.QuickTips.init();

(function (self, $, undefined) {

    var local = self._local = self._local || {};

    var seal = self._seal = self._seal || function() {
        delete self._local;
        delete self._seal;
    };
    var unseal = self._unseal = self._unseal || function() {
        self._local = local;
        self._seal = seal;
        self._unseal = unseal;
    };

    self.init = function () {
        if (SW.Core.ManagePollers) {
            SW.Core.ManagePollers.registerHandler("DeviceStudio.DoImport", doImport);
        }
        seal();
    };

    function doImport(params, actionCallback, successfulImport, errorHandler) {
        if (params.state === 'oldundp') {
            var kbLink = SW.Core.KnowledgebaseHelper.getKBUrlWithLang('5189');
            var message = '@{R=DeviceStudio.Strings;K=WEBJS_PS0_22;E=js}';
            message += ' <a class="importErrorLink" target="_blank" href="' + kbLink + '"><span class=\"LinkArrow\">»</span> @{R=DeviceStudio.Strings;K=WEBDATA_GK0_46;E=js}</a>';
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_SO0_24;E=js}',
                msg: message,
                minWidth: 300,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                fn: function() {
                    successfulImport(true);
                }
            });
        } else if (params.state === 'loaded') {
            if (params.alreadyExists) {
                SW.Core.ManagePollers.Extensions.showDialog({
                    title: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_38;E=js}',
                    msg: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_39;E=js}',
                    width: 450,
                    buttons: [
                        {
                            type: 'yes',
                            text: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_40;E=js}',
                            primary: true
                        },
                        {
                            type: 'no',
                            text: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_41;E=js}'
                        },
                        {
                            type: 'cancel',
                            text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}'
                        }
                    ],
                    fn: function (btn) {
                        Ext.getBody().unmask();
                        if (btn === 'yes') {
                            actionCallback({ justCheck: false, key: params.key, createNew: true }, function () { successfulImport(false); }, errorHandler);
                        } else if (btn === 'no') {
                            actionCallback({ justCheck: false, key: params.key, createNew: false }, function () { successfulImport(false); }, errorHandler);
                        } else if (btn === 'cancel') {
                            successfulImport(true);
                            return;
                        }
                    }
                });
                
            } else {
                actionCallback({ justCheck: false, key: params.key, createNew: false }, function () { successfulImport(false); }, errorHandler);
            }
        } else {
            errorHandler('@{R=DeviceStudio.Strings;K=WEBJS_PS0_24;E=js}');
        }
    }

}(SW.DeviceStudio.Importer = SW.DeviceStudio.Importer || {}, jQuery));

Ext.onReady(window.SW.DeviceStudio.Importer.init, window.SW.DeviceStudio.Importer);
if (window.SW && SW.Core && SW.Core.Loader) SW.Core.Loader._cbLoaded('jsfile', { url: '/orion/devicestudio/js/Importer.js' });
