﻿/// <reference path="../../../js/extjs/4.2.2/debug/ext-all-sandbox-42-debug.js" />
/// <reference path="../../../js/OrionMinReqs.js/underscore-min.js" />
Ext42.Loader.setConfig({ enabled: true });

Ext42.define('Ext.data.proxy.ServerOverride', {
    override: 'Ext.data.proxy.Server',

    processResponse: function (success, operation, request, response, callback, scope) {
        operation.serverResponse = response;
        this.fireEvent("responseProcessed", this, success, operation, request, response);
        this.callParent(arguments);
    }
});

Ext42.application({
    name: 'ThwackPollers',
    appFolder: '../js/ThwackPollers/app',
    requires: [
        'Ext.layout.container.*',
        'Ext.resizer.Splitter',
        'Ext.fx.target.Element',
        'Ext.fx.target.Component',
        'ThwackPollers.view.Sidebar',
        'ThwackPollers.view.MainGrid',
        'ThwackPollers.view.PollerDescriptionWindow',
        'ThwackPollers.view.TestPollerResultsGrid'

    ],
    models: ['Poller'],
    stores: ['Poller', 'GroupingOptions', 'GroupingValues', 'TestPollerResults'],
    controllers: ['Poller', 'Sidebar', 'Toolbar', 'PollerDescription', 'TestPollerResults'],
    listeners: {
        resize: {
            fn: function () {
                this.setMainGridHeight();
                Ext42.getCmp("appContainer").setHeight(Ext42.get("appContent").getHeight());
                Ext42.getCmp("appContainer").doLayout();
            }
        }
    },
    options: {
        selectedProperty: '',
        selectedValue: '',
        search: ''
    },
    launch: function () {

        ORION.prefix = "ThwackPollers_";

        Ext42.create("Ext.container.Container", {
            renderTo: Ext42.getElementById("appContent"),
            autoCreateViewPort: false,
            layout: 'border',
            id: "appContainer",
            listeners: {
                afterrender: function () {
                    ThwackPollers.getApplication().setMainGridHeight();
                    Ext42.getCmp("appContainer").setHeight(Ext42.get("appContent").getHeight());
                    Ext42.getCmp("appContainer").doLayout();

                    Ext42.EventManager.onWindowResize(_.debounce(function () {
                        ThwackPollers.getApplication().setMainGridHeight();
                        Ext42.getCmp("appContainer").setHeight(Ext42.get("appContent").getHeight());
                        Ext42.getCmp("appContainer").doLayout();
                    }, 50));
                }
            },
            items: [
                {
                    xtype: "sidebar",
                    region: 'west',
                    width: 200,
                    minWidth: 100,
                    maxWidth: 400,
                    split: true,
                    collapsible: true,
                    collapseMode: 'mini',
                },
                {
                    xtype: "maingrid",
                    region: 'center'
                }
            ]
        });

        Ext42.create("ThwackPollers.view.PollerDescriptionWindow");
        Ext42.create("ThwackPollers.view.TestPollerResultsGrid", {
            renderTo: Ext42.getElementById("testPollerResultsTable")
        });
    },
    autoCreateViewport: false,
    setMainGridHeight: function () {
        var contentPanel = $('#appContent');
        var maxHeight = this.calculateMaxGridHeight(contentPanel);
        var height = maxHeight;
        contentPanel.height((Math.max(height, 300)));
    },
    calculateMaxGridHeight: function (contentPanel) {
        var contentPanelTop = contentPanel.offset().top;
        var height = $(window).height() - contentPanelTop - $('#footer').outerHeight() - 23;
        return height;
    },
    showErrorMessage: function (message) {
        Ext42.MessageBox.show({
            title: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_10;E=js}',
            msg: message,
            buttons: Ext42.MessageBox.OK,
            icon: Ext42.MessageBox.ERROR
        });
    }
});
