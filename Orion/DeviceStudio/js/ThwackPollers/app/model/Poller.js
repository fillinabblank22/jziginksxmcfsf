﻿Ext42.define('ThwackPollers.model.Poller', {
    extend: 'Ext.data.Model',
    fields: [
        'Id',
        'Title',
        'Link',
        'PubDate',
        'DownloadCount',
        'ViewCount',
        'Rating',
        'Owner',
        'Tags',
        'Certified',
        'Technology'
    ]
});