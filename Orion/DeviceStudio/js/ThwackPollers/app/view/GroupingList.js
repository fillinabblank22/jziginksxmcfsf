﻿Ext42.define('ThwackPollers.view.GroupingList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.groupinglist',
    requires: [
        
    ],
    layout: 'fit',
    columns: [
        { text: 'Value', dataIndex: 'Caption', flex: 1, xtype: 'templatecolumn', tpl: '{Caption} ({Count})' }
    ],
    store: 'GroupingValues',
    hideHeaders: true,
    frame: false,

    initComponent: function () {
        this.callParent();
    }

});