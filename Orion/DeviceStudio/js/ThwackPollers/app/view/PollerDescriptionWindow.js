﻿Ext42.define('ThwackPollers.view.PollerDescriptionWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.pollerdescriptionwindow',
    
    layout: 'fit',
    width: 400,
    height: 300,
    closeAction: 'hide',
    plain: true,
    resizable: false,
    modal: true,
    autoScroll: true,
    bodyCls: 'pollerDescriptionWindowBody',
    items: [
    {
        xtype: "box",
        id: "PollerDescriptionContent"
    }],
    buttons: [{
        text: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_6; E=js}", // Close
        id: 'ClosePollerDescriptionWindowButton'
    }]
});
