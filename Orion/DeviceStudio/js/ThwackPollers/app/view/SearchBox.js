﻿Ext42.define('ThwackPollers.view.SearchBox', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.searchbox',
    initComponent: function () {
        var me = this;
        me.callParent(arguments);

        this.addEvents('searchEvent');
        
        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        this.on('focus', function () {
            this.el.applyStyles('font-style:normal;');
            if (Ext42.isEmpty(this.getValue()) || this.getValue() == this.emptyText) {
                this.setValue('');
            }
        }, this);

        // reset search field text style
        this.on('blur', function () {
            if (Ext42.isEmpty(this.getValue()) || this.getValue() == this.emptyText) {
                this.el.applyStyles('font-style:italic;');
            }
        }, this);

        this.on('render', function () {
            setTimeout(function () {
                me.hideTrigger1();
            }, 0);
        }, this);
    },

    id: 'searchfield',
    validationEvent: false,
    validateOnBlur: false,
    
    trigger1Cls: 'x42-form-clear-trigger',
    trigger2Cls: 'x42-form-search-trigger',

    // default search field text style
    style: 'font-style: italic',
    emptyText: '@{R=Core.Strings;K=WEBJS_TM0_53;E=js}',

    width: 180,
    hasSearch: false,
    
    hideTrigger1: function () {
        this.triggerCell.item(0).setDisplayed(false);
        this.updateLayout();
    },
    showTrigger1: function () {
        this.triggerCell.item(0).setDisplayed(true);
        this.updateLayout();
    },
    
    // this is the clear (X) icon button in the search field
    onTrigger1Click: function () {
        if (this.hasSearch) {
            this.setValue('');
            this.fireEvent('searchEvent', null);
            this.hideTrigger1();

            this.hasSearch = false;
        }
    },

    //  this is the search icon button in the search field
    onTrigger2Click: function () {

        //set the global variable for the SQL query and regex highlighting
        var filterText = this.getRawValue();

        if (filterText.length < 1) {
            this.onTrigger1Click();
            return;
        }

        // convert search string to SQL format (i.e. replace * with %)
        var tmpfilterText = "";
        tmpfilterText = filterText.replace(/\*/g, "%");

        if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
            tmpfilterText = "";
        }

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%")) {
                tmpfilterText = "%" + tmpfilterText;
            }
            if (!tmpfilterText.match("%$")) {
                tmpfilterText = tmpfilterText + "%";
            }
        }

        this.fireEvent('searchEvent', tmpfilterText);
        this.hasSearch = true;
        this.showTrigger1();
    }
});