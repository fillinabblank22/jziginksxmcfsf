﻿Ext42.define('ThwackPollers.view.MainToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.maintoolbar',
    requires: [
        'ThwackPollers.view.SearchBox'
    ],
    items: [
        {
            icon: '/Orion/images/CPE/import.png',
            text: "@{R=DeviceStudio.Strings;K=WEBJS_PS0_28; E=js}",
            id: 'importButton'
        },
        {
            icon: '/Orion/DeviceStudio/images/test_device_kit_icon16x16v1.png',
            text: '@{R=DeviceStudio.Strings;K=WEBJS_LH0_7;E=js}',
            id: 'testPollerButton'
        },
        '->',
        {
            xtype: 'searchbox',
            name: 'search'
        }
    ],
    initComponent: function () {
        this.callParent();
    }

});