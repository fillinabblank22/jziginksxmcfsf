﻿Ext42.define('ThwackPollers.view.PageSizeBox', {
    extend: 'Ext.form.NumberField',
    alias: 'widget.pagesizebox',
    id: 'PageSizeField',
    enableKeyEvents: true,
    allowNegative: false,
    width: 40,
    allowBlank: false,
    minValue: 1,
    maxValue: 100
});
