﻿Ext42.define('ThwackPollers.view.GroupingCombobox', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.groupingcombobox',
    requires: [
        'ThwackPollers.store.GroupingOptions'
    ],
    displayField: 'Caption',
    valueField: 'Value',
    store: 'GroupingOptions',
    autoSelect: true,
    editable: false,
    mode: 'local',
    value : 'tags',
    initComponent: function () {
        
        this.callParent();
    }
});