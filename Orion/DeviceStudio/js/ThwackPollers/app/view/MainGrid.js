﻿Ext42.define('ThwackPollers.view.MainGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.maingrid',
    requires: [
        'ThwackPollers.view.MainToolbar',
        'ThwackPollers.view.PageSizeBox',
        'ThwackPollers.view.RatingColumn'
    ],
    refs: [
        {
            ref: 'searchBox',
            selector: 'maintoolbar searchbox'
        }
    ],
    layout: 'fit',
    cls: 'mainGrid',
    columns: [
        {
            text: '@{R=DeviceStudio.Strings;K=DSOXMLDATA_GK0_01; E=js}',
            dataIndex: 'Title',
            flex: 1,
            hideable: false,
            renderer: function (value, meta, record) {
                var titleHtml = this.renderString(value);
                return this.renderTitle(titleHtml, meta, record);
            }
        },
        {
            text: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_56; E=js}',
            dataIndex: 'Owner',
            flex: 1,
            hideable: true,
            renderer: function (value, meta, record) {
                return this.renderOwner(value, meta, record);
            },
        },
        {
            text: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_58; E=js}',
            dataIndex: 'PubDate',
            flex: 1,
            hideable: true,
            renderer: function (value) {
                return this.renderPubDate(value);
            }
        },
        {
            xtype: 'ratingcolumn',
            text: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_57; E=js}',
            dataIndex: 'Rating',
            flex: 1,
            hideable: true
        },
        {
            text: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_59; E=js}',
            dataIndex: 'DownloadCount',
            flex: 1,
            hideable: true
        },
        {
            text: '@{R=DeviceStudio.Strings;K=WEBDATA_PS1_24; E=js}',
            dataIndex: 'Technology',
            flex: 1,
            hideable: true
        },
        {
            text: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_21; E=js}',
            dataIndex: 'Tags',
            flex: 1,
            hideable: true,
            sortable: false,
            renderer: function (value, meta, record) {
                return this.renderTags(value, meta, record);
            }
        }
    ],
    store: 'Poller',
    selType: 'checkboxmodel',

    dockedItems: [
        {
            xtype: 'maintoolbar',
            dock: 'top'
        },
        {
            xtype: 'pagingtoolbar',
            store: 'Poller',
            dock: 'bottom',
            displayInfo: true,
            items: [
                '-',
                { xtype: 'tbspacer', width: 10 },
                { xtype: 'label', text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}' },
                { xtype: 'pagesizebox' }
            ]
        }],
    listeners: {
        beforecellclick: {
            fn: function (table, td, cellIndex, record, tr, rowIndex, e) {

                // register when user clicks to checkbox cell but not on the checkbox...
                var isCheckBoxCellClicked = cellIndex === 0 && e.target.className !== 'x42-grid-row-checker';

                if (isCheckBoxCellClicked) {
                    var sm = table.getSelectionModel();

                    if (sm.isSelected(record)) {
                        sm.deselect([record]);
                    } else {
                        sm.select([record], true);
                    }
                }

                return !isCheckBoxCellClicked;
            },
            scope: this
        }
    },

    renderString: function (value) {
        if (!value || value.length === 0) {
            return value;
        }
        var filterText = this.searchText;
        if (!filterText || filterText.length === 0) {
            return this.encodeHTML(value);
        }
        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + this.encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = this.encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    },
    encodeHTML: function (value) {
        return Ext42.util.Format.htmlEncode(value);
    },
    searchText: '',
    columnMapping: {},
    renderPubDate: function (value) {
        var result = "";

        try {
            if (value) {
                var d = new Date(value);
                result = d.toLocaleDateString();
            }
        }
        finally {
            return result;
        }
    },
    renderTags: function (value, meta, record) {

        var result = "";

        if (value && _.isArray(value)) {
            var highlightedTags = _.map(value, function (item) {
                return { 'html': this.renderString(item, this.searchText), 'value': this.encodeHTML(item) };
            }, this);

            var tpl = new Ext42.XTemplate('<tpl for=".">' +
                '<span class="link" onclick="ThwackPollers.getApplication().getPollerController().selectTag(\'{value}\')">{html}</span>' +
                '<tpl if="xindex &lt; xcount">, </tpl>' +
                '</tpl>');

            result = tpl.apply(highlightedTags);
        }

        return result;
    },
    renderOwner: function (value, meta, record) {
        var iconCls = 'source-icon';
        if (record.get('Certified') == 1) {
            iconCls = 'source-icon-certified';
            value = '@{R=DeviceStudio.Strings;K=WEBDATA_PS1_22; E=js}';
        }
        var result = this.renderString(value);
        var tpl = new Ext42.XTemplate('<span class="' + iconCls + '">{.}</span>');
        return tpl.apply(result);
    },
    renderTitle: function (value, meta, record) {
        var titleData = {
            id: record.get('Id'),
            caption: value
        };
        var tpl = new Ext42.XTemplate("<a class=\"pollerTitle link\" href=\"#\">{caption}</a>");
        var result = tpl.apply(titleData);
        return result;
    }
});