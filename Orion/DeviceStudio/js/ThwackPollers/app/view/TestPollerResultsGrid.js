﻿Ext42.define('ThwackPollers.view.TestPollerResultsGrid', {
    extend: 'Ext.grid.Panel',
    id: 'testPollerResultsGrid',
    stores: ['TestPollerResults'],
    columns: [
        { text: '@{R=DeviceStudio.Strings;K=WEBJS_LH0_9; E=js}', dataIndex: 'Attribute', hideable: false, width: 200 },
        { text: '@{R=DeviceStudio.Strings;K=WEBJS_LH0_10; E=js}', dataIndex: 'Value', hideable: false, flex: 1 }
    ],
    store: 'TestPollerResults',
    width: 860,
    height: 300,
    sortRoot: "Attribute"
});