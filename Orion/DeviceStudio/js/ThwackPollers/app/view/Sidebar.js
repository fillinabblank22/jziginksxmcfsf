﻿Ext42.define('ThwackPollers.view.Sidebar', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.sidebar',
    requires: [
        'ThwackPollers.view.GroupingCombobox',
        'ThwackPollers.view.GroupingList'
    ],
    layout: 'fit',
    title: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_13; E=js}',
    items: [
        {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align : 'stretch',
                pack  : 'start'
            },
            cls: 'sidebar',
            items: [
                { xtype: 'groupingcombobox', margin: 10 },
                { xtype: 'groupinglist', flex: 1 }
            ]
        }
    ],
    initComponent: function () {
        this.callParent();
    }
});