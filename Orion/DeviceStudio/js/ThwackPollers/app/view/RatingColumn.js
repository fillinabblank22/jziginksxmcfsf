﻿Ext42.define('ThwackPollers.view.RatingColumn', {
    extend: 'Ext.grid.column.Template',
    alias: 'widget.ratingcolumn',
    tpl: '<tpl if="Rating == 0"><div class="rating-star-noicon">@{R=DeviceStudio.Strings;K=WEBJS_PS0_25; E=js}</div>' +
        '<tpl else>' +
        '<tpl>' +
        '<div class="rating-star rating-star-on" style="width: {[16 * values.Rating]}px"></div>' +
        '</tpl>' +
        '<tpl if="Rating &lt; 5">' +
        '<div class="rating-star-noicon" style="width: {[16 * (5 - values.Rating)]}px"><div class="rating-star-inner rating-star-off" style="margin-left: -{[16 * (values.Rating - Math.floor(values.Rating))]}px"></div></div>' +
        '</tpl>' +
        '</tpl>',
    initRenderData: function () {
        var me = this;
        return Ext42.applyIf(me.callParent(arguments), {
            dataIndex: me.dataIndex
        });
    },
    initComponent: function () {
        this.callParent(arguments);
    },
});