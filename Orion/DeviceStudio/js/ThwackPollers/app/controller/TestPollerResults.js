﻿Ext42.define('ThwackPollers.controller.TestPollerResults', {
    extend: 'Ext.app.Controller',
    requires: ['ThwackPollers.view.TestPollerResultsGrid'],
    stores: ['TestPollerResults'],
    init: function () {
        this.callParent();
    },
    onLaunch: function () {
        this.callParent();

        var store = this.getTestPollerResultsStore();
        store.reload();
    },
    loadResult: function (data) {
        var store = this.getTestPollerResultsStore();
        store.removeAll();

        var propertyList = [];

        // process properties
        _.each(data.Properties, function (property) {
            
            var propName = property.Key.Name;
            var propValue = property.Value;

            if (property.Key.Units) {
                propName = String.format(propName + " ({0})", property.Key.Units)
            }

            var record = {
                'Attribute': propName,
                'Value': propValue
            };

            propertyList.push(record);
        });

        // process columns
        _.each(data.Columns, function (property) {

            var propName = property.Key.Name;
            var propValue = "";

            if (_.isArray(property.Value)) {
                var propValue = property.Value.join(", ");
            }

            var record = {
                'Attribute': propName,
                'Value': propValue
            };

            propertyList.push(record);
        });


        
        // the load is deferred a bit as otherwise the grid header is not shown
        Ext42.Function.defer(function () {
            // inject data to store
            store.loadData(propertyList, false);
        }, 50);
        
    }
}
);