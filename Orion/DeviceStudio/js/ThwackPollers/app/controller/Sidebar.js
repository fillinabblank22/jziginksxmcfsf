﻿Ext42.define('ThwackPollers.controller.Sidebar', {
    extend: 'Ext.app.Controller',

    stores: ['GroupingValues', 'Poller'],
    refs: [
        {
            ref: 'combobox',
            selector: 'sidebar groupingcombobox'
        },
        {
            ref: 'list',
            selector: 'sidebar groupinglist'
        }
    ],

    init: function () {

        var store = this.getGroupingValuesStore();
        store.on({ "beforeload": { fn: this.onBeforeStoreLoad, scope: this } });

        this.control({
            'sidebar groupingcombobox': {
                change: this.onGroupingOptionChange, scope: this
            }
        });

        this.control({
            'sidebar groupinglist': {
                selectionchange: this.onGroupingValueChange, scope: this
            }
        });
        this.callParent();
    },

    onLaunch: function () {
        var store = this.getGroupingValuesStore();
        store.getProxy().on({
            'exception': { fn: this.onError, scope: this }
        });

        store.load();
    },
    onBeforeStoreLoad: function () {
        var combobox = this.getCombobox();
        var groupByProperty = combobox.getValue();
        var groupingValuesStore = this.getGroupingValuesStore();

        Ext42.apply(groupingValuesStore.getProxy().extraParams, { 'property': groupByProperty });
    },
    onGroupingOptionChange: function (sender, newValue, oldValue) {

        if (newValue) {
            this.getGroupingValuesStore().load();
        } else {
            var groupingValuesStore = this.getGroupingValuesStore();
            groupingValuesStore.removeAll();

            this.getPollerStore().loadPage(1);
        }

    },

    onGroupingValueChange: function (sender, selected) {

        if (selected.length > 0) {
            var store = this.getPollerStore();
            store.loadPage(1);
        }
    },
    /*
     * Loads data according specified argument.
     * options = {property:'', value:'', search:''}
     */
    loadBySelection: function (options) {

        var defaults = {
            property: '',
            value: '',
            search: ''
        };

        Ext42.apply(defaults, options);

        var currentProperty = this.getCombobox().getValue();

        if (currentProperty != defaults.property) {

            this.getGroupingValuesStore().on({
                "load": { fn: this.onStoreLoadedDoSelection, scope: this, options: defaults },
            });
            this.getCombobox().setValue(defaults.property);
        } else {
            this.selectGroupingValue(defaults.value);
        }
    },
    onStoreLoadedDoSelection: function (store, records, successful, eOpts) {
        this.getGroupingValuesStore().un("load", this.onStoreLoadedDoSelection, this);
        this.selectGroupingValue(eOpts.options.value);
    },
    selectGroupingValue: function (value) {
        var list = this.getList();
        var store = this.getGroupingValuesStore();

        var index = store.findExact("Value", value);
        var record = store.getAt(index);

        list.getSelectionModel().select([record]);
    },
    onError: function () {
        var message = '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_12;E=js}';
        var app = this.getApplication();
        app.showErrorMessage(message);
    }

});