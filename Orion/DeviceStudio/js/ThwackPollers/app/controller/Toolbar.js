﻿Ext42.define('ThwackPollers.controller.Toolbar', function () {
    function doImport(controller) {
        var me = controller;
        var grid = me.getPollerGrid();
        var waitMsg = Ext42.Msg.wait('@{R=DeviceStudio.Strings;K=WEBJS_PS0_33;E=js}');
        var selectedItems = grid.getSelectionModel().getSelection();
        var data = selectedItems[0].get('Id');

        SW.Core.Services.callController('/sapi/ThwackPollers/ImportDeviceKit', data, function (result) {
            waitMsg.hide();
            if (result.Success) {
                if (result.PollerExists) {
                    duplicate(result);
                } else {
                    finishImport(result);
                }
            } else {
                var msg = '@{R=DeviceStudio.Strings;K=WEBJS_PS0_32;E=js}<br />' + result.Message;
                Ext42.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_TM0_74;E=js}', msg: msg, minWidth: 300, buttons: Ext42.Msg.OK, icon: Ext42.MessageBox.ERROR });
            }
        }, function (fail) {
            waitMsg.hide();
            Ext42.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_TM0_74;E=js}', msg: fail, minWidth: 300, buttons: Ext42.Msg.OK, icon: Ext42.MessageBox.ERROR });
        });
    }
    
    function finishImport(result) {
        var msg = String.format('@{R=DeviceStudio.Strings;K=WEBJS_PS0_31;E=js}', result.Message);
        Ext42.Msg.show({
            title: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_15;E=js}',
            msg: msg,
            minWidth: 300,
            buttons: Ext42.Msg.YESNO,
            icon: Ext42.MessageBox.QUESTION,
            fn: function (button) {
                if (button == 'yes') {
                    window.location = String.format("/Orion/Admin/Pollers/EnableDisablePollers.aspx?poller={0}&groupBy=PollerStatus&groupByValue=Disabled", result.PollerId);
                }
            }
        });
    }

    function confirmSave(oldResult, createNew) {
        var data = oldResult.Message + ';' + createNew;
        var waitMsg = Ext42.Msg.wait('@{R=DeviceStudio.Strings;K=WEBJS_PS0_33;E=js}');
        
        SW.Core.Services.callController('/sapi/ThwackPollers/SaveDeviceKit', data, function (result) {
            waitMsg.hide();
            if (result.Success) {
                finishImport(result);
            } else {
                var msg = '@{R=DeviceStudio.Strings;K=WEBJS_PS0_32;E=js}<br />' + result.Message;
                Ext42.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_TM0_74;E=js}', msg: msg, minWidth: 300, buttons: Ext42.Msg.OK, icon: Ext42.MessageBox.ERROR });
            }
        }, function (fail) {
            waitMsg.hide();
            Ext42.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_TM0_74;E=js}', msg: fail, minWidth: 300, buttons: Ext42.Msg.OK, icon: Ext42.MessageBox.ERROR });
        });
    }

    function duplicate(result) {
        Ext42.Msg.show({
            title: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_38;E=js}',
            msg: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_39;E=js}',
            icon: Ext42.Msg.WARNING,
            minWidth: 300,
            buttons: Ext42.Msg.YESNOCANCEL,
            buttonText: {
                yes: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_40;E=js}',
                no: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_41;E=js}',
                cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}'
            },
            fn: function (btn) {
                if (btn === 'yes') {
                    confirmSave(result, true);
                } else if (btn === 'no') {
                    confirmSave(result, false);
                } else if (btn === 'cancel') {
                    return;
                }
            }
        });
    }

    return {
        extend: 'Ext.app.Controller',
        stores: ['Poller', 'GroupingValues'],
        refs: [
            {
                ref: 'importButton',
                selector: 'maintoolbar > #importButton'
            },
            {
                ref: 'testPollerButton',
                selector: 'maintoolbar > #testPollerButton'
            },
            {
                ref: 'pollerGrid',
                selector: 'maingrid'
            }
            ],

        init: function () {

            this.control({
                'maintoolbar > #importButton': {
                    click: this.onImportPollers, scope: this
                }
            });

            this.control({
                'maintoolbar > #testPollerButton': {
                    click: this.onTestDevicePoller, scope: this
                }
            });

            this.control({
                'maingrid': {
                    selectionchange: this.onMainGridSelectionChange, scope: this
                }
            });

            this.callParent();
        },

        onLaunch: function () {
            this.setupButtonsState(true);
        },

        onImportPollers: function () {
            var me = this;
            this.loginToThwack(function () { doImport(me) });
        },

        onMainGridSelectionChange: function (sender, selected) {
            var isEmptySelection = (selected && selected.length == 1) === false;
            this.setupButtonsState(isEmptySelection);
        },
        
        setupButtonsState: function (isDisabled) {
            this.getImportButton().setDisabled(isDisabled);
            this.getTestPollerButton().setDisabled(isDisabled);
        },

        onTestDevicePoller: function () {
            var me = this;
            this.loginToThwack(function () { me.testPoller.call(me) });
            
        },

        testPoller: function(){
            var grid = this.getPollerGrid();
            var selectedItems = grid.getSelectionModel().getSelection();
            // TODO: For now we support just testing of one poller.
            var pollerId = selectedItems[0].get('Id');

            SW.DeviceStudio.ThwackPollers.TestPoller.setTestedPollerId(pollerId);

            // invoke netobject picker to select node to test on            
            $selectorDialog = $('#selectordlg');
            
            SW.Orion.NetObjectPicker.SetFilter('');           
            SW.Orion.NetObjectPicker.SetFilter("ObjectSubType = 'SNMP'");
            SW.Orion.NetObjectPicker.SetUpEntities(
              [{ MasterEntity: 'Orion.Nodes' }],
              'Orion.Nodes',
              [SW.DeviceStudio.ThwackPollers.TestPoller.getEntity()],
              'Single',
              true
            );
            $selectorDialog.dialog({
                position: 'center',
                width: 890,
                height: 'auto',
                modal: true,
                draggable: true,
                title: '@{R=DeviceStudio.Strings;K=WEBJS_LH0_8;E=js}',
                show: { duration: 0 }
            });
        },

        /*
         * Shows log-in to thwack dialog and in case the login process is successful a callback is triggered.
         */
        loginToThwack: function (success) {
            if (thwackUserInfo.name && thwackUserInfo.pass) {
                if (typeof (success) == "function") {
                    success();
                }
                return;
            }

            SW.Core.ThwackCommon.LoginToThwack('/sapi/ThwackPollers', 'StoreCredentials', thwackUserInfo.name, thwackUserInfo.pass, function (creds, result) {
                if (result) {
                    thwackUserInfo = { name: creds.username, pass: creds.password };
                    if (typeof (success) == "function") {
                        success();
                    }
                } else {
                    Ext42.Msg.show({ title: '@{R=DeviceStudio.Strings;K=WEBJS_PS0_29;E=js}', msg: '@{R=DeviceStudio.Strings;K=WEBJS_PS0_30;E=js}', minWidth: 300, buttons: Ext42.Msg.OK, icon: Ext42.MessageBox.ERROR });
                }
                return result;
            });
        }
    };
});
