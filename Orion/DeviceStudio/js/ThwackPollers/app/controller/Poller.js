﻿Ext42.define('ThwackPollers.controller.Poller', {
    extend: 'Ext.app.Controller',
    stores: ['Poller', 'GroupingValues'],
    controllers: ['Sidebar', 'PollerDescription'],
    refs: [
        {
            ref: 'pagingToolbar',
            selector: 'maingrid pagingtoolbar'
        },
        {
            ref: 'pageSizeBox',
            selector: 'maingrid pagingtoolbar pagesizebox'
        },
        {
            ref: 'combobox',
            selector: 'sidebar groupingcombobox'
        },
        {
            ref: 'groupingList',
            selector: 'sidebar groupinglist'
        },
        {
            ref: 'searchBox',
            selector: 'maingrid maintoolbar searchbox'
        },
        {
            ref: 'mainGrid',
            selector: 'maingrid'
        }
    ],
    init: function () {
        this.control({
            'maingrid pagesizebox': {
                'change': _.debounce(this.onPageSizeBoxValueChange, 300)
            }
        });

        this.control({
            'maingrid': {
                'itemclick': { fn: this.onGridClick, scope: this }
            }
        });

        var store = this.getPollerStore();
        store.on({
            "beforeload": { fn: this.onBeforeStoreLoad, scope: this },
        });
    },
    onLaunch: function () {
        var store = this.getPollerStore();
        store.pageSize = ORION.Prefs.load('PageSize');
        store.getProxy().on({
            'responseProcessed': { fn: this.onResponseprocessed, scope: this }
        });
        store.getProxy().on({
            'exception': { fn: this.onError, scope: this }
        });
        this.getPageSizeBox().setRawValue(store.pageSize);

        var searchbox = this.getSearchBox();
        searchbox.on({
            "searchEvent": { fn: this.onSearchEvent, scope: this },
        });

        store.load();
    },
    getPageSize: function () {

        return this.getPollerStore().pageSize;
    },
    setPageSize: function (value) {
        if (value) {
            this.getPollerStore().pageSize = value;
            ORION.Prefs.save('PageSize', value);
        }
    },
    onPageSizeBoxValueChange: function (sender, newValue, oldValue) {
        var pSize = parseInt(newValue);
        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
            Ext.Msg.show({
                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",//Invalid page size
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });
            return;
        }

        var pagingToolbar = this.getPagingToolbar();

        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
            this.setPageSize(pSize);
            pagingToolbar.moveFirst();
        }
    },
    warnAboutRssFeedNotAvailable: function () {

        var topic = "OrionCorePHDeviceStudioConnectToThwack.htm";
        var lang = ORION.Prefs.load("Locale");
        var helpLinkUrl = SW.Core.HelpDocumentationHelper.getHelpLink(topic);

        var text = String.format('@{R=DeviceStudio.Strings;K=WEBJS_PS0_26;E=js}', '<a href="https://thwack.solarwinds.com" target="_blank">https://thwack.solarwinds.com</a>');
        var helplink = String.format("<a href='{0}' class='link' target='_blank'>&raquo; {1}</a>", helpLinkUrl, '@{R=DeviceStudio.Strings;K=WEBJS_LH0_15;E=js}');
        var message = String.format('<b>{0}</b><br/><br/>{1}', text, helplink);
        
        Ext42.MessageBox.show({
            title: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_10;E=js}',
            msg: message,
            buttons: Ext42.MessageBox.OK,
            icon: Ext42.MessageBox.ERROR
        });
    },
    onError: function () {
        var message = '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_12;E=js}';
        var app = this.getApplication();
        app.showErrorMessage(message);
    },
    onResponseprocessed: function (proxy, success, operation, request, response) {

        if (success && response.responseText) {
            data = JSON.parse(response.responseText);

            if (data.ErrorCode === 1) {
                this.warnAboutRssFeedNotAvailable();
            }
        }
    },
    selectTag: function (tagName) {
        var o = {};
        o.property = 'tags';
        o.value = tagName;
        o.search = '';
        this.loadBySelection(o);

    },
    onBeforeStoreLoad: function (store, operation) {
        var combobox = this.getCombobox();
        var groupingList = this.getGroupingList();
        var groupingListSelection = groupingList.getSelectionModel().getSelection();
        var groupByProperty = combobox.getValue();

        if (groupingListSelection.length > 0) {
            var selectedRow = _.first(groupingListSelection);
            var groupByValue = selectedRow.get('Value');

            if (groupByProperty && typeof (groupByValue) === "string") {
                Ext42.apply(store.getProxy().extraParams, { 'property': groupByProperty, 'value': groupByValue });
            }
        } else {
            Ext42.apply(store.getProxy().extraParams, { 'property': null, 'value': null });
        }
    },
    onSearchEvent: function (searchText) {
        var stores = [this.getPollerStore(), this.getGroupingValuesStore()];
        $.each(stores, function (index, element) {
            Ext42.apply(element.getProxy().extraParams, { 'search': searchText });
            element.loadPage(1);
        });
        this.getMainGrid().searchText = this.getSearchBox().getValue();
    },

    /*
     * Loads data according specified argument.
     * options = {property:'', value:'', search:''}
     */
    loadBySelection: function (options) {

        var defaults = {
            property: '',
            value: '',
            search: ''
        };

        Ext42.apply(defaults, options);
        var sidebarController = this.getSidebarController();
        sidebarController.loadBySelection(defaults, /*silent:*/ true);
    },

    /*
     * Handles click to the main grid
     */
    onGridClick: function (dataView, record, item, index, e) {

        var element = $(e.target);
        if (element.hasClass("pollerTitle") || element.parents().hasClass("pollerTitle")) {
            e.preventDefault();
            var pollerdescriptionController = this.getPollerDescriptionController();
            var pollerFeedItemId = record.get('Id');
            var pollerFeedItemTitle = record.get('Title');
            var pollerData = {
                'id': pollerFeedItemId,
                'title': pollerFeedItemTitle
            };

            pollerdescriptionController.show(pollerData);
        }
    }
});
