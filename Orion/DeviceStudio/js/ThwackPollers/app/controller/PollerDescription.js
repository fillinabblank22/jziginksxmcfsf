﻿Ext42.define('ThwackPollers.controller.PollerDescription',
    {
        extend: 'Ext.app.Controller',
        requires: ['ThwackPollers.view.PollerDescriptionWindow'],
        refs: [
            {
                ref: 'pollerDescriptionWindow',
                selector: 'pollerdescriptionwindow'
            }
        ],
        init: function () {
            this.control({
                'pollerdescriptionwindow button': {
                    'click': { fn: this.hide, scope: this }
                }
            });
        },
        onLaunch: function () {

        },
        messages: {
            loadingDescription: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_1; E=js}", //'Getting description for poller {title}. Please wait...'
            windowTitle: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_2; E=js}", //'Description of {title}'
            noDescription: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_3; E=js}", //'No description available.'
            errorMessageTitle: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_4; E=js}", //'Error'
            errorMessageContent: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_5; E=js}", //"Couldn't get description of poller {title}."
        },

        show: function (pollerData) {

            var view = this.getPollerDescriptionWindow();
            var controller = this;

            // show mask
            var body = Ext42.getBody();
            var tpl = new Ext42.XTemplate(controller.messages.loadingDescription);
            var maskMessage = tpl.apply(pollerData);
            body.mask(maskMessage);

            var jqxhr = this.getDescription(pollerData.id);

            jqxhr.done(function (data) {

                var tpl = new Ext42.XTemplate(controller.messages.windowTitle);
                var title = tpl.apply(pollerData);
                view.setTitle(title);

                if (!data) {
                    data = controller.messages.noDescription;
                }

                Ext42.getCmp("PollerDescriptionContent").update(data);

                if (view && !view.isVisible()) {
                    view.show();
                }
            });

            jqxhr.fail(function(){
                
                var tpl = new Ext42.XTemplate(controller.messages.errorMessageContent);
                var message = tpl.apply(pollerData);

                Ext42.Msg.show({
                    title: controller.messages.errorMessageTitle,
                    msg: message,
                    buttons: Ext42.MessageBox.OK,
                    icon: Ext42.MessageBox.ERROR
                });
            });

            jqxhr.always(function () {
                // hide mask
                body.unmask();
            });

            
        },
        hide: function () {
            var view = this.getPollerDescriptionWindow();
            if (view && view.isVisible()) {
                view.hide();
            }
        },

        getDescription: function (pollerFeedItemId, success, fail, always) {
            var jqxhr = $.get("/sapi/ThwackPollers/GetPollerDescription", { 'pollerId': pollerFeedItemId });

            jqxhr
                .done(function (data) { if (typeof (success) == "function") success(data); })
                .fail(function () { if (typeof (fail) == "function") fail(); })
                .always(function () { if (typeof (always) == "function") always(); });

            return jqxhr;

        }
    }
);