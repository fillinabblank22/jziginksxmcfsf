﻿Ext42.define('ThwackPollers.store.GroupingOptions', {
    extend: 'Ext.data.Store',
    fields: ['Value', 'Caption'],
    data: [
        { 'Value': '', 'Caption': '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_20; E=js}' },
        { 'Value': 'tags', 'Caption': '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_21; E=js}' },
        { 'Value': 'source', 'Caption': '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_56; E=js}' },
        { 'Value': 'technology', 'Caption': '@{R=DeviceStudio.Strings;K=WEBDATA_PS1_24; E=js}' }
    ]
});