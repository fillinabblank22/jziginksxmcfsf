﻿Ext42.define('ThwackPollers.store.TestPollerResults', {
    extend: 'Ext.data.Store',

    fields: ['Attribute', 'Value']
});