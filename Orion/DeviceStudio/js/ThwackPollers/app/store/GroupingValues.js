﻿Ext42.define('ThwackPollers.store.GroupingValues', {
    extend: 'Ext.data.Store',

    fields: ['Caption', 'Value', 'Count'],

    proxy: {
        type: 'ajax',
        url: '/sapi/ThwackPollers/GetThwackPollerGroupValues',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        },
        reader: {
            type: 'json',
            root: 'Data'
        }
    }
});