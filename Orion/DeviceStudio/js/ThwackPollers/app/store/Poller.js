﻿Ext42.define('ThwackPollers.store.Poller', {
    extend: 'Ext.data.Store',
    requires: 'ThwackPollers.model.Poller',
    model: 'ThwackPollers.model.Poller',
    remoteSort: true,
    
    proxy: {
        type: 'ajax',
        url: '/sapi/ThwackPollers/Get',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        },
        reader: {
            type: 'json',
            root: 'Data',
            totalProperty: 'TotalCount'
        }
    }
});