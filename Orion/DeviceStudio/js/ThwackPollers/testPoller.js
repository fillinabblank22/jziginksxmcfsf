﻿
Ext42.namespace("SW.DeviceStudio.ThwackPollers.TestPoller");

var testPoller = function () {

    var $testButton = $("#nob_testPollerButton");
    var $cancelButton = $("#nob_cancelButton");
    var $selectNodeDialog = $("#selectordlg");
    var $pollerResultsDialog = $("#testPollerResultsDialog");
    var $installPollerButton = $("#testPollerResultsDialog_installButton");
    
    var $closePollerResultsDialogButton = $("#testPollerResultsDialog_cancelButton");
    var $closeTestPollerErrorDialogButton = $("#testPollerErrorDialog_cancelButton");

    var $testPollerErrorDialog = $("#testPollerErrorDialog");
    var $testPollerErrorDialogMessage = $("#testPollerErrorDialog_errorMessage");
    var $installPollerButtonOnErrorDialog = $("#testPollerErrorDialog_installButton");
    var $confirmInstallCheckbox = $("#testPollerErrorDialog_confirmInstall");

    var selectedNodeId;
    /*
     * SWIS url to the entity. Required by netobject picker to display correct entity (as preselection).
     */
    var selectedEntity;
    /*
     * Array of thwack pollers ids to test on.
     */
    var thwackPollerId;

    /*
     * Test ID guid
     */
    var testId;
    var progressTimer;
    var canceled;
    /*
     * Installed poller ID used for link
     */
    var pollerId;
    var pollerName;

    /**
     * keeps information whether the imported poller already is present (installed) in the system.
     * In that case a dialog with question about poller rename/overwrite should be shown...
     */
    var pollerExists = false;

    $testButton.on('click', getSelectedEntity);
    $cancelButton.on('click', closeSelectorDialog);
    $installPollerButton.on('click', installPoller);
    $confirmInstallCheckbox.on('change', turnInstallButtonAccordingConfirmation);
    $installPollerButtonOnErrorDialog.on('click', installPollerWhenAcceptedTerms);

    $closePollerResultsDialogButton.on("click", function () { $pollerResultsDialog.dialog("close"); })
    $closeTestPollerErrorDialogButton.on("click", function () { $testPollerErrorDialog.dialog("close"); })

    /*
     * Closes the NetObject picker dialog.
     */
    function closeSelectorDialog() {
        $selectNodeDialog.dialog('close');
    }

    /**
     * Extracts selected entity from Netobject picker and stores it in selectedEntity and selectedNodeId fields.
     */
    function getSelectedEntity() {
        var selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntities();
        if (selectedEntities.length > 0) {

            var x = selectedEntities[0];
            selectedEntity = x;

            var start = x.indexOf('=');
            if ((start >= 0) && (start < x.length)) {
                {
                    selectedNodeId = x.substring(start + 1, x.length);
                    closeSelectorDialog();
                    startTestOnNode(selectedNodeId, thwackPollerId);
                }
            } else {
                selectedNodeId = null;
            }
        } else {
            selectedNodeId = null;
        }
    }

    /*
     * Tests the pollers (represented by the thwack poller IDs) on given node represented by its ID.
     */
    function startTestOnNode(nodeId, pollerId) {
        showProgressDialog();
        $('#progressDlg .statusText').text('@{R=DeviceStudio.Strings;K=WEBJS_PS0_37;E=js}');
        var data = { nodeId: Number(nodeId), pollerIds: [pollerId] };
        SW.Core.Services.callController('/sapi/ThwackPollers/TestKit', data, function (result) {
            if (result === '00000000-0000-0000-0000-000000000000') {
                failMsg('@{R=DeviceStudio.Strings;K=WEBJS_PS0_38;E=js}');
            } else {
                testId = result;
                canceled = false;
                getProgress();
            }
        }, function (fail) {
            closeProgressDialog();
            // error msg
            failMsg(fail);
        });
    }

    function showProgressDialog() {
        var encodedTitle = $("<div/>").text('@{R=DeviceStudio.Strings;K=WEBDATA_PS1_18;E=js}').html();
        $("#progressDlg").show().dialog({
            width: '550px', height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle
        });
        $(".ui-dialog-titlebar-close").unbind('click');
        $(".ui-dialog-titlebar-close").bind('click', cancelTest);
    }

    function getProgress() {
        SW.Core.Services.callController('/sapi/ThwackPollers/GetProgress', testId, function (result) {
            onProgressRecieved(result);
        }, function (fail) {
            closeProgressDialog();
            // error msg
            failMsg(fail);
        });
    }

    function onProgressRecieved(result) {
        if (result == null) {
            progressTimer = setTimeout(getProgress, 500);
            return;
        }

        if (result.TestState == 1) { // running
            $('#progressDlg .statusText').text(result.Message);
            setProgressBar(result.DoneSteps, result.TotalSteps);
            if (!canceled) {
                progressTimer = setTimeout(getProgress, 500);
            }
        } else {
            // test finished
            clearTimeout(progressTimer);
            closeProgressDialog();
            // show result
            showTestPollerResults(testId);

        }
    }

    function cancelTest() {
        if (testId && testId != '') {
            SW.Core.Services.callController('/sapi/ThwackPollers/CancelTest', testId, function () {
                $('#progressDlg .statusText').text('@{R=DeviceStudio.Strings;K=WEBJS_PS0_39;E=js}');
                canceled = true;
            }, function (fail) {
                closeProgressDialog();
                // error msg
                failMsg(fail);
            });
        }
    }

    function setProgressBar(value, maxValue) {
        if (value > maxValue) value = maxValue;
        var percentString = ((100 / maxValue) * value) + "%";
        $("#progressPercentage").text(percentString);
        if (value == 0) {
            $("#progressBar").empty();
        }
        else {
            if ($("#progressBar img").length == 0) {
                $("#progressBar").append($("<img />").attr("src", "/Orion/Discovery/images/progBar_on.gif").height(20));
            }
            $("#progressBar img").width((300 / maxValue) * value);
        }
    }

    function getResults() {
        if (testId && testId != '') {
            SW.Core.Services.callController('/sapi/ThwackPollers/GetResults', testId, function (result) {
                if (result == null) {
                    failMsg('@{R=DeviceStudio.Strings;K=WEBJS_PS0_40;E=js}');
                    return;
                }
                var properties = result.Properties;
                var tables = result.Columns;
                // show results
            }, function (fail) {
                closeProgressDialog();
                // error msg
                failMsg(fail);
            });
        }
    }

    function showFinishedDialog(result) {

        var pollerName = result.PollerName;
        pollerId = result.PollerId;

        var encodedTitle = $("<div/>").text('@{R=DeviceStudio.Strings;K=WEBJS_PS0_41;E=js}').html();
        $("#pollerNameText").text(String.format('@{R=DeviceStudio.Strings;K=WEBJS_PS0_42;E=js}', pollerName));
        $("#optionConfigure").text(String.format('@{R=DeviceStudio.Strings;K=WEBJS_PS0_43;E=js}', pollerName));
        $("#finishedDlg").show().dialog({
            width: '550px', height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle
        });
        $(".ui-dialog-titlebar-close").unbind('click');
        $(".ui-dialog-titlebar-close").bind('click', closeFinishDialog);
    }

    function closeProgressDialog() {
        $("#progressDlg").dialog("close");
    }

    function closeFinishDialog() {
        $("#finishedDlg").dialog("close");
    }

    function failMsg(msg) {
        Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_TM0_74;E=js}', msg: msg, minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
    }

    function configurePoller() {
        if (pollerId) {
            window.location = String.format("/Orion/Admin/Pollers/EnableDisablePollers.aspx?poller={0}&groupBy=PollerStatus&groupByValue=Disabled", pollerId);
        }
    }

    function showTestPollerResults(pollerResultId) {
        var url = "/sapi/ThwackPollers/GetPollerResult";
        var data = pollerResultId;

        var success = function (result) {

            pollerExists = result.PollerExists;

            if (result.TestSuccessful && result.IsInstalable) {
                transformResultData(result);

                // show dialog and show poller results there
                $pollerResultsDialog.dialog({
                    position: 'center',
                    width: 890,
                    height: 'auto',
                    modal: true,
                    draggable: true,
                    title: '@{R=DeviceStudio.Strings;K=WEBJS_LH0_11;E=js}',
                    show: { duration: 0 }
                });
            }
            else if (!result.TestSuccessful && result.IsInstalable) {
                showTestPollerErrorDialog(result.ErrorMessage);
            }
            else {
                failMsg(result.ErrorMessage);
            }

        };

        SW.Core.Services.callController(url, data, success);
    }

    function showTestPollerErrorDialog(message) {
        //disable install button until user accepts terms
        $confirmInstallCheckbox.attr("checked", false);
        $installPollerButtonOnErrorDialog.addClass("sw-btn-disabled");


        $testPollerErrorDialogMessage.text(message);
        $testPollerErrorDialog.dialog({
            position: 'center',
            width: 800,
            height: 'auto',
            modal: true,
            draggable: true,
            title: '@{R=DeviceStudio.Strings;K=WEBJS_LH0_11;E=js}',
            show: { duration: 0 }
        });
    }

    /**
     * Transforms result from test poller and fills the poller results store
     */
    function transformResultData(result) {
        var app = ThwackPollers.getApplication();
        controller = app.getController('TestPollerResults');
        controller.loadResult(result);
    }

    /**
     * Handler for Install poller button on Test poller results dialog
     */
    function installPoller() {



        if (pollerExists) {
            // show question whether overwrite existing poller or not...

            duplicate();

        } else {
            //save poller
            runInstallPoller(true);
        }
    }

    function duplicate() {
        Ext42.Msg.show({
            title: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_38;E=js}',
            msg: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_39;E=js}',
            icon: Ext42.Msg.WARNING,
            minWidth: 300,
            buttons: Ext42.Msg.YESNOCANCEL,
            buttonText: {
                yes: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_40;E=js}',
                no: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_41;E=js}',
                cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}'
            },
            fn: function (btn) {
                if (btn === 'yes') {
                    // create new poller (copy)
                    runInstallPoller(false);
                } else if (btn === 'no') {
                    // overwrite
                    runInstallPoller(true);
                } else if (btn === 'cancel') {
                    return;
                }
            }
        });
    }






    /**
     * Installs the poller
     * @param overwrite True to overwrite any existing poller which might be already in teh system with the same ID. When false provided, poller is imported as new poller with new unique ID.
     */
    function runInstallPoller(overwrite) {

        // as this is common method triggered from both dialogs, we actually can't tell which one to close, 
        // thats why we closing both (even one of them can be already closed)
        $pollerResultsDialog.dialog("close");
        $testPollerErrorDialog.dialog("close");

        var url = "/sapi/ThwackPollers/InstallPoller";
        var data = 0;

        data = { PollerId: thwackPollerId, Overwrite: overwrite };


        var success = function (result) {
            showFinishedDialog(result);
        };

        var fail = function () {
            failMsg("@{R=DeviceStudio.Strings;K=WEBJS_LH0_16;E=js}");
        }

        if (thwackPollerId > 0) {
            SW.Core.Services.callController(url, data, success, fail);
        } else {
            failMsg("@{R=DeviceStudio.Strings;K=WEBJS_LH0_12;E=js}");
        }
    }

    /**
     * handler for click on Understands risks of installing poller which (partially) failed polling.
     */
    function turnInstallButtonAccordingConfirmation() {
        var isChecked = $confirmInstallCheckbox.is(":checked");

        if (isChecked) {
            // enable install button
            $installPollerButtonOnErrorDialog.removeClass("sw-btn-disabled");
            
        } else{
            $installPollerButtonOnErrorDialog.addClass("sw-btn-disabled");
        }

    }

    function installPollerWhenAcceptedTerms() {
        var isChecked = $confirmInstallCheckbox.is(":checked");

        if (isChecked) {
            $installPollerButtonOnErrorDialog.dialog("close");
            installPoller();
        } else {
            Ext.Msg.show({ title: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_14;E=js}", msg: "@{R=DeviceStudio.Strings;K=WEBJS_LH0_13;E=js}", minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO });
        }
    }


    return {
        getEntity: function () { return selectedEntity; },
        getNodeId: function () { return selectedNodeId; },
        setTestedPollerId: function (pollerId) { thwackPollerId = pollerId; },
        cancelTest: function () { cancelTest(); },
        closeFinishDialog: function () { closeFinishDialog(); },
        configurePoller: function () { configurePoller(); }
    };
};

$(function () {
    SW.DeviceStudio.ThwackPollers.TestPoller = testPoller();
});




