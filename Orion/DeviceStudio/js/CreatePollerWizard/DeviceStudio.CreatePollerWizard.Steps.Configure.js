﻿/* global SW */ // JSHint directive
(function (self) {

    /*
     * CONFIG:
     */

    /*
     * STRINGS:
     */

    var strings = {
        selectorDialogTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_3;E=js}'
    };

    /*
     * PRIVATE:
     */

    var $selectorDialog; // div containing netObjectPicker
    var $selectorDialogCancelBtn; // cancel button
    var $selectorDialogLauncher; // link/button used to open sel.dialog
    var $selectorDialogSubmitBtn; // submit button
    var $tooltip;
    var $testNodeTooltip;

    function initVars() {
        $selectorDialog = $('#selectordlg');
        $selectorDialogCancelBtn = $('#selectordlgcancel');
        $selectorDialogLauncher = $('#frm_configureStep_nodeHR');
        $selectorDialogSubmitBtn = $('#selectordlgprimary');
        $tooltip = $("#TechnologyInfoTooltip");
        $testNodeTooltip = $("#TestNodeTooltip");
    }

    function openSelectorDialog() {
        var vendor = $("#frm_configureStep_vendor option:selected").val();
        if (vendor !== "") {
            SW.Orion.NetObjectPicker.SetFilter("Vendor = '" + vendor + "'");
        } else {
            SW.Orion.NetObjectPicker.SetFilter('');
        }
        SW.Orion.NetObjectPicker.SetFilter("ObjectSubType = 'SNMP'");
        SW.Orion.NetObjectPicker.SetUpEntities(
          [{ MasterEntity: 'Orion.Nodes' }],
          'Orion.Nodes',
          SW.DeviceStudio.CreatePollerWizard.NodePickerLastSelected,
          'Single',
          true
        );
        $selectorDialog.dialog({
            position: 'center',
            width: 890,
            height: 'auto',
            modal: true,
            draggable: true,
            title: strings.selectorDialogTitle,
            show: { duration: 0 }
        });
    }

    function closeSelectorDialog() {
        $selectorDialog.dialog('close');
    }

    function showTooltip() {
        var pos = $("#PortInfoIcon").position();
        $tooltip.css('top', pos.top).css('left', pos.left + 20);
        $tooltip.show();
    }

    function hideTooltip() {
        $tooltip.hide();
    }

    /*
     * PUBLIC:
     */

    self.loadNode = function (nodeId) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/Orion/DeviceStudio/Services/Wizard.asmx/LoadNode",
            data: JSON.stringify({ nodeId: nodeId }),
            success: function (data) {
                $("#frm_configureStep_nodeDescription").html(data.d);
                closeSelectorDialog();
            },
            error: function () {
                closeSelectorDialog();
            },
            dataType: 'json'
        });
    };

    self.clearValidator = function () {
        $("#nodeValidator").css('visibility', 'hidden');
    };

    self.init = function () {

        initVars();

        $selectorDialogLauncher.on("click", function (e) {
            e.preventDefault();
            openSelectorDialog();
        });
		
        $("#TestNodeInfoIcon").hover(function () {
            var pos = $(this).position();
            $testNodeTooltip.css('top', pos.top).css('left', pos.left + 20);
            $testNodeTooltip.show();
        }, function () {
            $testNodeTooltip.hide();
        });

        $selectorDialogSubmitBtn.on('click', function () {
            var selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntities();
            if (selectedEntities.length > 0) {
                var x = selectedEntities[0];
                SW.DeviceStudio.CreatePollerWizard.NodePickerLastSelected = selectedEntities;
                var start = x.indexOf('=');
                if ((start >= 0) && (start < x.length)) {
                    {
                        var nodeId = x.substring(start + 1, x.length);
                        $("#frm_configureStep_node").val(nodeId);
                        self.loadNode(nodeId);
                        self.clearValidator();
                    }
                }
                $("#frm_NodeIdFromListResource").val('');
            }
        });

        $selectorDialogCancelBtn.on('click', function () {
            closeSelectorDialog();
        });

        $("#PortInfoIcon").hover(function () {
            showTooltip();
        }, function () {
            hideTooltip();
        });

        var passedNodeId = $("#frm_NodeIdFromListResource").val();
        if (passedNodeId && passedNodeId !== '') {
            var entityIds = [];
            var itemType = 'Orion.Nodes';
            entityIds.push(passedNodeId);
            SW.Core.Services.callWebService(
              "/Orion/Services/AddRemoveObjects.asmx",
              "LoadEntitiesByIds",
              { entityType: itemType, uris: entityIds },
              function (result) {
                  if (result.length > 0) {
                      var entities = [];
                      entities.push(result[0].Uri);
                      SW.DeviceStudio.CreatePollerWizard.NodePickerLastSelected = entities;
                      $("#frm_configureStep_node").val(passedNodeId);
                  }
              }
            );
            self.loadNode(passedNodeId);
            self.clearValidator();
        } else {
            var nodeId = $("#frm_configureStep_node").val();
            if (nodeId && nodeId !== '') {
                self.loadNode(nodeId);
            }
        }

    };

})(SW.Core.namespace("SW.DeviceStudio.CreatePollerWizard.Steps.Configure"));

$(document).ready(function () {
    if (document.getElementById('hiddenCurrentStep').value !== 'Configure') {
        return;
    }
    SW.DeviceStudio.CreatePollerWizard.Steps.Configure.init();
});