﻿/* global SW:true */ // JSHint directive

// This whole helper serves one main purpose: Minimize the callback hell in code.
// When new expression is being added, you first need to generate name for it,
// then find out if it works with table properties and finally test (validate) it.
// All these steps are being done on server, so the final code in JavaScript
// is quite awful.
/*
Test
  AJAX-getTableName
    + switch
      isTable
        AJAX-TestTable
          + submit
          - show error
      isProperty
        AJAX-TestProperty
          + submit
          - show error
*/
SW.Core.namespace('SW.DeviceStudio.CreatePollerWizard').ExpressionHelper = (function () {

    var cfg = {
        formulaEditorService: '/Orion/DeviceStudio/Services/FormulaEditor.asmx',
        testExpressionMethod: 'TestExpression',
        testExpressionTableMethod: 'TestExpressionTable'
    }

    //
    // AJAX

    function extractTableNameFromExpressionCall(expression, successAction, errorAction) {
        var service = cfg.formulaEditorService;
        var method = "GenerateResultType";
        var args = { expression: expression };
        var onSuccess = successAction || function () {};
        var onError = errorAction || function() {};
        SW.Core.Services.callWebService(service, method, args, onSuccess, onError);
    }

    function testExpressionCall(property, expression, successAction, errorAction) {
        var service = cfg.formulaEditorService;
        var method = cfg.testExpressionMethod;
        var args = {
            property: property,
            expression: expression
        };
        var onSuccess = successAction || function () { };
        var onError = errorAction || function () { };
        SW.Core.Services.callWebService(service, method, args, onSuccess, onError);
    }

    function testExpressionTableCall(table, property, expression, successAction, errorAction) {
        var service = cfg.formulaEditorService;
        var method = cfg.testExpressionTableMethod;
        var args = {
            table: table,
            property: property,
            expression: expression
        };
        var onSuccess = successAction || function () { };
        var onError = errorAction || function () { };
        SW.Core.Services.callWebService(service, method, args, onSuccess, onError);
    }

    //
    // Putting everything together for public use

    function test(datasourcePropertyName, expression, onSuccess, onError) {
        var extractTableSuccessAction = function (response) {
            if (response === "")
                testExpressionCall(datasourcePropertyName, expression, onSuccess, onError);
            else
                testExpressionTableCall(response, datasourcePropertyName, expression, onSuccess, onError);
        };
        extractTableNameFromExpressionCall(expression, extractTableSuccessAction, onError);
    }

    return {
        test: test
    }

}());