﻿/* global SW:true */ // JSHint directive

SW.Core.namespace("SW.DeviceStudio.CreatePollerWizard.FormulaEditor").MathSimple = function () {

  var initialized = false;
  var resultType;
  var visWrapper;
  var visInPlaceholder;
  var visOutPlaceholder;

  var TEST_SINGLE_VALUE_FORMAT = '<strong>{0}</strong>: {1}';

  function log(msg) {
    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Log(msg);
  }

  function onResultTypeChange(newtype) {
    resultType = newtype;
    clear();
    fillVariables(resultType);
  }

  function fillVariables() {
    // i left those calls here, otherwise the dropdown is empty and minimalized until service returns values
    $("#ds_fe_mathsimple_variables").html("");
    $("#ds_fe_mathsimple_variables").append("<option value=''>@{R=DeviceStudio.Strings;K=WEBDATA_LF0_64; E=js}</option>");

    var output = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultProperty();

    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("GetVariables", "{ 'output':'" + output + "' }", function (data) {
      $("#ds_fe_mathsimple_variables").html("");
      $("#ds_fe_mathsimple_variables").append("<option value=''>@{R=DeviceStudio.Strings;K=WEBDATA_LF0_64; E=js}</option>");
      $.each(data.d, function (i, o) {
        $("#ds_fe_mathsimple_variables").append("<option value='[" + o + "]'>" + o + "</option>");
      });
    });
  }

  function getCaret(el) {
    if (el.selectionStart) {
      return el.selectionStart;
    } else if (document.selection) {
      el.focus();

      var r = document.selection.createRange();
      if (r === null) {
        return 0;
      }

      var re = el.createTextRange(), rc = re.duplicate();
      re.moveToBookmark(r.getBookmark());
      rc.setEndPoint('EndToStart', re);

      return rc.text.length;
    }
    return 0;
  }

  function setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
      var range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', selectionEnd);
      range.moveStart('character', selectionStart);
      range.select();
    }
  }

  function setCaretToPos(input, pos) {
    setSelectionRange(input, pos, pos);
  }

  function insertText(text) {
    var textarea = document.getElementById('ds_fe_mathsimple_formula');
    var currentPos = getCaret(textarea);

    var strLeft = textarea.value.substring(0, currentPos);
    var strRight = textarea.value.substring(currentPos, textarea.value.length);
    textarea.value = strLeft + text + strRight;
    textarea.focus();
    setCaretToPos(textarea, (strLeft + text).length);
  }

  function init() {
    log('Math Simple: init');

    resultType = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultType();
    fillVariables(resultType);
    loadNetObjectHtml();

    if (initialized) {
      return;
    }

    $('#ds_fe_mathsimple_variables').on('change', function () {
      insertText(this.value);
      $(this).val("");
    });

    $('#ds_fe_mathsimple_operations').on('change', function () {
      insertText(this.value);
      $(this).val("");
    });

    $("#ds_fe_mathsimple_test").on('click', function () {
      if (SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Validate()) {
        // test + visualization without any callbacks:
        test(null, null, true);
      }
    });

    visWrapper = $('#ds_fe_mathsimple_visualization');
    visInPlaceholder = visWrapper.find('.ds_fe_visualization_input');
    visOutPlaceholder = visWrapper.find('.ds_fe_visualization_output');

    initialized = true;
  }

  function getFormula() {
    var text = $.trim($("#ds_fe_mathsimple_formula").val());
    if (text) {
      text = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.EncodeJSON(text);
    }
    return text;
  }

  function testResult(text) {
    if (!text) {
      $('#ds_fe_mathsimple_testresult').empty();
    } else {
      $('#ds_fe_mathsimple_testresult').html(text);
    }
  }

  function test(onSuccessCallback, onFailCallback, visualize) {

    // Don't validate editor here. It's not our responsibility.
    // It's validation should be done in separate method call before calling this method.
    // Test results will be shown even if you don't provide callback parameters

    testResult('');
    clearVisualization();

    var output = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultProperty();
    var formula = getFormula();
    var formulaRaw = $.trim($("#ds_fe_mathsimple_formula").val());
    if (formula === '') {
      SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('emptyFormula', '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_48; E=js}');
      if (onFailCallback) { 
        onFailCallback();
      }
      return;
    } else {
      SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('emptyFormula');
    }

    var formulaVariables = extractVars(formula);

    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("GenerateResultType", "{ 'expression':'" + formula + "' }", function (data, status) {
      resultType = data.d;
      if (resultType) {
        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("TestExpressionTable", "{ 'table':'" + resultType + "','column':'" + output + "', 'expression':'" + formula + "' }", function (data, status) {
          if (!data.d.Passed) {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('simpleMathTest', data.d.Message);
            if (onFailCallback) {
              onFailCallback();
            }
          } else {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('simpleMathTest');
            if (visualize) {
              if (formulaVariables) {
                getVisualizationInput(
                  formulaVariables,
                  function (response) {
                    // There are cases, where tabular result has just one row.
                    // For these I am extending the received data by one null
                    // value to process it as a table. Yes, it's a workaround.
                    if (data.d.Values.length === 1) {
                      data.d.Values.push(null);
                    }
                    renderVisualization(response.d, data.d.Values, output);
                  }
                );
              } else {
                renderVisualizationFallback(formulaRaw, data.d.Message, output);
              }
            }
            if (onSuccessCallback) {
              onSuccessCallback();
            }
          }
        });
      } else {
        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("TestExpression", "{ 'property':'" + output + "', 'expression':'" + formula + "' }", function (data, status) {
          if (!data.d.Passed) {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddError('simpleMathTest', data.d.Message);
            if (onFailCallback) {
              onFailCallback();
            }
          } else {
            SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ClearError('simpleMathTest');
            if (visualize) {
              if (formulaVariables) {
                getVisualizationInput(
                  formulaVariables,
                  function (response) {
                    renderVisualization(response.d, data.d.Values, output);
                  }
                );
              } else {
                renderVisualizationFallback(formulaRaw, data.d.Message, output);
              }
            }
            if (onSuccessCallback) {
              onSuccessCallback();
            }
          }
        });
      }
    });
  }

  function submit(onSubmit) {

    // No validation here.
    // Validation of editor and active tab should be called before calling submit.

    log('Simple Math: submit');

    var output = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.ResultProperty();

    if (resultType) {
      SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("AddExpressionTable", "{ 'table':'" + resultType + "','column':'" + output + "', 'expression':'" + getFormula() + "' }", function (data, status) {
        if (onSubmit) {
          onSubmit();
        }
      });
    } else {
      SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService("AddExpression", "{ 'property':'" + output + "', 'expression':'" + getFormula() + "' }", function (data, status) {
        if (onSubmit) {
          onSubmit();
        }
      });
    }
  }

  function show() {
    log('Simple Math: show');
    $('#FormulaEditor_MathSimple').show();
  }

  function hide() {
    log('Simple Math: hide');
    $('#FormulaEditor_MathSimple').hide();
  }

  function clear() {
    $('#ds_fe_mathsimple_formula').val('');
    $('#ds_fe_mathsimple_testresult').empty();
    $('#ds_fe_mathsimple_netobject').empty();
    visWrapper.hide();
  }
  
  function loadNetObjectHtml() {
    var id = SW.DeviceStudio.CreatePollerWizard.FormulaEditor.GetNetObjectId();
    $.ajax({
      url: '/Orion/DeviceStudio/Services/Wizard.asmx/LoadNode',
      type: 'POST',
      contentType: 'application/json',
      dataType: 'json',
      data: JSON.stringify({
        nodeId: id
      }),
      success: function (result) {
        $('#ds_fe_mathsimple_netobject').html('<a target="_blank" href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + id + '"><span class="oidPickerNode">' + result.d + '</span></a>');
      }
    });
  }

  // If I want to visualize values, I have to know what properties are being
  // used in formula. Then I have to find their values in datasource.
  // Datasource is at server in session = AJAX and callbacks time. Response 
  // should be formatted as object acceptable by cyaable plugin and passed
  // to callback function that will handle rendering.
  function getVisualizationInput(variables, onSuccess) {
    if (variables.length === 0 || !onSuccess) {
      return null;
    }
    SW.DeviceStudio.CreatePollerWizard.FormulaEditor.CallService(
      "GetValuesForVisualization",
      JSON.stringify({vars: variables}),
      function (response) {
        onSuccess(response);
      }
    );
  }

  // I need to show one table for each input property/variable.
  // Cyaable plugin is done in a way, that will render every property of the
  // given object as a column in one table that is created as a child DOM 
  // node of given placeholder. So here I am iterating over the input 
  // properties, creating new placeholder for each and calling cyaable 
  // plugin on it to split them into separate tables.
  // Also, because I can get mixed input (single vs. tabular) values, I have
  // to add another conditional statement and use cyaable only for tabular
  // values (= the property in input has multiple values).
  function renderVisualizationInput(target, input) {
    var placeholder, source;
    target.empty();
    for (var i in input) {
      if (Object.prototype.hasOwnProperty.call(input, i)) {
        placeholder = $('<div>');
        placeholder.attr('data-name', i);
        placeholder.addClass('ds_fe_mathsimple_visualizationItem');
        placeholder.appendTo(target);
        if (input[i].length > 1) {
          source = {};
          source[i] = input[i];
          placeholder.cyaable({
            data: source,
            headerInfo: String.format('@{R=DeviceStudio.Strings;K=WEBDATA_PD0_01; E=js}', 5),
            emptyText: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_63; E=js}' 
          });
        } else {
          placeholder.html(String.format(
            TEST_SINGLE_VALUE_FORMAT,
            i,
            input[i])
          );
        }
      }
    }
  }

  // I have to render one table or single value (depends on input) here.
  // If I get multiple values, I want to render table with cyaable. If I get
  // only single value, no special rendering will be used.
  function renderVisualizationOutput(target, inputData, inputName) {
    var placeholder, source;
    target.empty();
    placeholder = $('<div>');
    placeholder.attr('data-name', inputName);
    placeholder.addClass('ds_fe_mathsimple_visualizationItem');
    placeholder.appendTo(target);
    if (inputData.length > 1) {
      source = {};
      source[inputName] = inputData;
      placeholder.cyaable({
        data: source,
        headerInfo: String.format('@{R=DeviceStudio.Strings;K=WEBDATA_PD0_01; E=js}', 5),
        emptyText: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_63; E=js}'
      });
    } else {
      placeholder.html(String.format(
        TEST_SINGLE_VALUE_FORMAT,
        inputName,
        inputData[0])
      );
    }
  }

  // wrapper function
  function renderVisualization(inInput, inOutput, inName) {
    renderVisualizationInput(visInPlaceholder, inInput);
    renderVisualizationOutput(visOutPlaceholder, inOutput, inName);
    visWrapper.show();
  }

  // wrapper function
  function renderVisualizationFallback(input, output, name) {
    visInPlaceholder.html(input);
    visOutPlaceholder.html(String.format(
      TEST_SINGLE_VALUE_FORMAT,
      name,
      output)
    );
    visWrapper.show();
  }

  // wrapper function
  function clearVisualization() {
    visInPlaceholder.empty();
    visOutPlaceholder.empty();
    visWrapper.hide();
  }

  // I need to find out what variables/properties were used in formula
  // so I can then find data for them. I can just pass the formula here
  // and get filtered array of variable names. Regexp is needed only if the
  // default one isn't good enough.
  function extractVars(formula, regexp) {
    if (!formula) {
      return null;
    }
    var rxp = regexp || /[\w\.]+(?=\])/g;
    var matchedVars = formula.match(rxp);
    if (!matchedVars) {
      return null;
    }
    var uniqueVars = [];
    $.each(matchedVars, function (i, item) {
      if ($.inArray(item, uniqueVars) === -1) {
        uniqueVars.push(item);
      }
    });
    return uniqueVars;
  }

  return {
    DisplayName: "Formula", // used in type selection UI
    Description: "add, multiply, divide", // used in type selection UI
    Order: 0, // order in dropdown
    Init: init,
    Show: show,
    Hide: hide,
    Submit: submit,
    OnResultTypeChange: onResultTypeChange,
    Validate: test,
    Clear: clear
  };
} ();

SW.DeviceStudio.CreatePollerWizard.FormulaEditor.AddType(SW.DeviceStudio.CreatePollerWizard.FormulaEditor.MathSimple);