﻿/* global SW:true */ // JSHint directive

// Implementation of a Fixed Value Dialog used in Device Studio.
// The constructor asks for several query selectors (what you usually use 
// to select elements with jQuery) because part of the component is 
// actually an User Control (.ascx), so some DOM references are needed.
SW.Core.namespace('SW.DeviceStudio.CreatePollerWizard').FixedValueDialog = (function () {

    var $dlg;
    var $property;
    var $input;
    var $submitBtn;
    var $cancelBtn;
    var $errorMessage;

    // references to markup in: /Orion/DeviceStudio/Controls/FixedValueDialog
    var defaults = (function () {
        var base = '#FVD';
        var propertySelector = '#FVDproperty';
        var inputSelector = 'input[id*="FVDinput"]';
        var submitSelector = '#submitFVD';
        var cancelSelector = '#cancelFVD';
        var errorSelector = '.sw_ds_error_text';
        return {
            dialogElSelector: base,
            propertyNameElSelector: base + ' ' + propertySelector,
            inputElSelector: base + ' ' + inputSelector,
            submitBtnElSelector: base + ' ' + submitSelector,
            cancelBtnElSelector: base + ' ' + cancelSelector,
            errorMessageElSelector: base + ' ' + errorSelector
        }
    })();

    // I am passing the whole instance here because
    // I need to access the 'property' property when the
    // submit button is pressed.
    // The 'property' (contains selected property name)
    // is set when dialog is opened, so I don't know
    // it's value in constructor.
    // I could read it from DOM, but I don't like that.
    // When I pass the instance, I'll have access to
    // the 'property' property directly.
    function init(instance) {

        // init dialog
        $dlg = $(instance.dialogElSelector);
        if (!$dlg[0])
            throw 'No element has been found with provided dialogElSelector: ' + instance.dialogElSelector;
        $dlg.dialog({
            modal: true,
            autoOpen: false,
            title: '@{R=DeviceStudio.Strings;K=WEBDATA_PD0_02; E=js}', // Use a constant value
            open: instance.onOpen || function () { },
            close: instance.onClose || function () { },
        });

        // init property name placeholder
        $property = $(instance.propertyNameElSelector);
        if (!$property[0]) {
            throw 'No element has been found with provided propertyElSelector: ' + instance.propertyNameElSelector;
        }

        // init input
        $input = $(instance.inputElSelector);
        if (!$input[0])
            throw 'No element has been found with provided inputElSelector: ' + instance.inputElSelector;
        $input.on('keyup', function (e) {
            if (e.keyCode === 13 || e.which === 13)
                submit(instance);
        });

        // init submit button
        $submitBtn = $(instance.submitBtnElSelector);
        if ($submitBtn[0]) {
            $submitBtn.on('click', function () {
                submit(instance);
            });
        }

        // init cancel button
        $cancelBtn = $(instance.cancelBtnElSelector);
        if ($cancelBtn[0]) {
            $cancelBtn.on('click', function () {
                $dlg.dialog('close');
            });
        }

        // init error message
        $errorMessage = $(instance.errorMessageElSelector);
    }

    function submit(context) {

        var cv = context.getCurrentValue();

        // Validation for empty valuee
        if (!cv) {
            $errorMessage.html('Cannot proceed with an empty value.'); // TODO i18n
            return;
        }

        // The submitted value will be stored under some key,
        // this key has to be generated first.
        SW.Core.Services.callWebService(
          '/Orion/DeviceStudio/Services/FormulaEditor.asmx',
          'GeneratePropertyName',
          {
              preffix: context.table + context.property // preffix: 'FixedValue'
          },
          function (data) {
              // The name has been generated,
              // so we can proceed with next
              // service call.
              // Since user can pass anything in, and I need to
              // wrap strings in '', I need to be able to
              // identify them.
              if (!isNumber(cv)) {
                  cv = '\'' + cv + '\'';
              }
              var params = {
                  property: data,
                  expression: cv
              };

              // The expression needs to be validated first
              SW.DeviceStudio.CreatePollerWizard.ExpressionHelper.test(
                  params.property,
                  params.expression,
                  function(response) {
                      // test call succeeded, check results
                      // console.log('test response: ', response);

                      // proceed with adding
                      var successCallback = function () {
                          if (context.onSubmit)
                              context.onSubmit();
                          context.close();
                      };
                      var errorCallback = function () {
                          // show error
                      }
                      SW.Core.Services.callWebService(
                          '/Orion/DeviceStudio/Services/FormulaEditor.asmx',
                          'AddExpression',
                          params,
                          successCallback,
                          errorCallback
                      );
                  },
                  function() {
                      // test call failed, log error?
                  }
              );
          },
          function () {
              // There was an issue while
              // trying to generate a new
              // name. Log, notify?
          }
        );
    }

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    // ReSharper disable once InconsistentNaming - This is constructor function
    function FVD(options) {

        if (options === null || typeof options === 'undefined')
            throw 'Options argument is required!';

        this.identity = FVD.identity++;
        this.cls = 'sw-ds-fvd-' + this.identity;
        this.property = '';
        this.table = '';
        this.dialogElSelector = options.dialogElSelector || defaults.dialogElSelector;
        this.propertyNameElSelector = options.propertyNameElSelector || defaults.propertyNameElSelector;
        this.inputElSelector = options.inputElSelector || defaults.inputElSelector;
        this.submitBtnElSelector = options.submitBtnElSelector || defaults.submitBtnElSelector;
        this.cancelBtnElSelector = options.cancelBtnElSelector || defaults.cancelBtnElSelector;
        this.errorMessageElSelector = options.errorMessageElSelector || defaults.errorMessageElSelector;
        this.onOpen = options.onOpen || null;
        this.onClose = options.onClose || null;
        this.onSubmit = options.onSubmit || null;

        init(this);

    }

    FVD.prototype.open = function (propertyName, tableName) {
        if (!propertyName)
            throw 'Dialog cannot be opened without propertyName argument!';
        this.property = propertyName;
        this.table = tableName || '';
        if ($property)
            $property.html(propertyName);
        if ($errorMessage)
            $errorMessage.html('');
        $input.val('');
        $dlg.dialog('open');
    }

    FVD.prototype.close = function () {
        $dlg.dialog('close');
    }

    FVD.prototype.validate = function (value) {
        var regexp = /^[\w&.\-\s]+$/g;
        if (regexp.test(value))
            return true;
        else
            return false;
    }

    FVD.prototype.getCurrentValue = function (selector) {
        var $source;
        if (selector) {
            $source = $(selector);
            if (!$source[0])
                throw 'No element has been found with provided selector: ' + selector;
        } else {
            $source = $(this.inputElSelector);
        }
        var cv = $source.val();
        if (!cv)
            return '';
        if (cv.trim)
            return cv.trim();
        else
            return cv;
    }

    FVD.identity = 1;

    return FVD;

}());