﻿SW.Core.namespace("SW.DeviceStudio").FormulaTesterBeta = function (editorSelector, enableLog) {

    // PUBLIC ▼

    this.getVariables = function () {
        extractVariables();
        log("FT: Variables: ");
        log(variables);
    };

    this.getValues = function () {
        extractValues();
        log("FT: Values: ");
        log(values);
    };

    this.getFormulaWithVariables = function () {
        log("FT: Formula: " + formula);
        return formula;
    };

    this.getFormulaWithValues = function () {
        log("FT: Parsed formula: " + parseFormula);
        return parseFormula();
    };

    this.setTestTable = function (name) {
        testDataSourceTable = name;
    };

    this.setTestColumn = function (name) {
        testDataSourceColumn = name;
    };

    this.setTestPreCheckFunction = function (checkFn) {
        testPreCheck = checkFn;
    };

    this.setTestSuccessCallback = function (callbackFn) {
        testSuccessCallback = callbackFn;
        log("FT: Test success callback set.");
    };

    this.setTestMessage = function (msg) {
        testOutput.html(msg);
    };

    this.clearTestMessage = function () {
        testOutput.html('');
    };

    this.getRealValuesFlag = function () {
        return realValuesLoaded;
    };

    this.hasVariablesInFormula = function () {
        if (formula && formula !== '') {
            if (variables.length > 0) return true;
            else return false;
        }
        else {
            return false;
        }
    }

    this.reset = function () {
        log("FT: Reset.")
        reset();
        updateList();
    };

    // PRIVATE ↓

    if (!String.format) {
        String.format = function (format) {
            var args = Array.prototype.slice.call(arguments, 1);
            return format.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
            });
        };
    }

    var editor, // control containing the text to parse
        formula, // the formula written in editor
        placeholder, // table containing all the needed controls
        list, // jQuery element - list with inputs
        listItems, // set of jQuery elements - list items
        variablesWithActiveInputs, // array containing names of variables that have their inputs generated
        variables, // array containing variables used in editor
        variablesRegexp, // regex for matching the variables in editor
        values, // array containg values for variables
        controlsWrapper, // div containing some control elements for input list
        testBtn, // jQuery element - button launching the ajax requests
        testOutput, // jQuery element - text node containing the response from test service
        loadBtn, // jQuery element - text button for loading values for inputs
        testDataSourceTable, // name of table in testing datasource
        testDataSourceColumn, // name of column in testing datasource
        testPreCheck, // function executed before launching test
        testSuccessCallback, // callback function used for test button
        realValuesLoaded, // flag which is true when the current values are loaded in test inputs
        logging; // enable/disable logging

    function log(msg) {
        if (logging) {
            if (console) {
                console.log(msg);
            }
        }
    }

    function prepareEditor(selector) {
        editor = $(selector);
        editor.focus();
        log('FT: Editor prepared.');
    }

    function preparePlaceholder() {
        placeholder = $("#sw_ds_formulaTester");
        list = $(".sw_ds_fe_inputList");
        editor.after(placeholder);
        log("FT: Placeholder prepared.");
        prepareControls();
    }

    function prepareControls() {

        // create elements
        controlsWrapper = $(".sw_ds_fe_inputListControls");
        loadBtn = $(".sw_ds_fe_loadBtn");
        testBtn = $(".sw_ds_fe_testBtn");
        testOutput = $("#sw_ds_FormulaTester_OutputLabel");

        // attach event handlers
        loadBtn.on('click', function (e) {
            e.preventDefault();
            loadValues();
        });
        testBtn.on('click', function (e) {
            e.preventDefault();
            extractValues();
            if (testPreCheck && typeof testPreCheck !== 'undefined') {
                var requirementsCheck = testPreCheck();
                if (!requirementsCheck) {
                    return;
                }
            }
            testOutput.html('@{R=DeviceStudio.Strings;K=WEBDATA_LF0_30; E=js}');
            testOutput.show();

            var encodedFormula = formula.replace(/'/g, '|').replace(/"/g, '|');

            $.ajax({
                type: "POST",
                url: "/Orion/DeviceStudio/Services/FormulaEditor.asmx/TestFormulaBeta",
                data: JSON.stringify({ formula: encodedFormula, variables: variables, values: values, table: testDataSourceTable || "", column: testDataSourceColumn || "" }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error: function (error) {
                    testOutput.hide();
                    log(error);
                },
                success: function (response) {
                    testOutput.html("Current values loaded");
                    if (testSuccessCallback && typeof testSuccessCallback !== 'undefined') {
                        testSuccessCallback(response);
                    } else {
                        log("No callback defined.");
                    }
                }
            });
        });

        log('FT: Controls prepared.');
    }

    function bindEditorEvents() {
        editor.on('input cut paste change', function (e) {
            extractVariables();
            if (e.keyCode === 13) {
                log(variables);
            }
            updateList();
        });
        log('FT: Events binded.');
    }

    function updateList() {
        if (variables && variables.length > 0) {
            // remove inputs of removed variables (cleanup)
            $.each(variablesWithActiveInputs, function (index, name) {
                if ($.inArray(name, variables) === -1) {
                    removeVariableInput(name);
                    testOutput.hide();
                }
            });
            // add inputs for new variables
            $.each(variables, function (index, name) {
                if ($.inArray(name, variablesWithActiveInputs) === -1) {
                    createVariableInput(name);
                    testOutput.hide();
                }
            });
            list.append(listItems);
            placeholder.show();
            log('List updated.');
        }
        else {
            listItems = $();
            list.children().remove();
            placeholder.hide()
        }
    }

    function createVariableInput(variableName) {
        var li = $("<li class='sw_ds_fe_inputListItem'>");
        li.attr('data-name', variableName);
        var textbox = $("<input type='text' class=''>");
        textbox.attr('placeholder', variableName);
        textbox.attr('title', '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_31; E=js}'.format({'0' : variableName}));
        textbox.on('change', function () {
            realValuesLoaded = false;
        });
        textbox.appendTo(li);
        listItems = listItems.add(li);
        variablesWithActiveInputs.push(variableName);
    }

    function removeVariableInput(variableName) {
        var itemToRemove = list.children("[data-name='" + variableName + "']");
        itemToRemove.fadeOut(444, function () {
            itemToRemove.remove();
            // clean up the jQuery list items collection
            listItems = listItems.not("[data-name='" + variableName + "']");
            // remove the associated name from helper array        
            var removeIndex = variablesWithActiveInputs.indexOf(variableName);
            variablesWithActiveInputs.splice(removeIndex, 1);
        });
    }

    function extractVariables() {
        formula = editor.val();
        if (formula && formula === '') {
            reset();
        }
        else {
            var matchedVariables = formula.match(variablesRegexp);
            // remove duplicates:
            variables = [];
            if (matchedVariables) {
                $.each(matchedVariables, function (index, item) {
                    if ($.inArray(item, variables) === -1) {
                        variables.push(item);
                    }
                });
            }
            log('Variables extracted.');
        }
    }

    function extractValues() {
        values = [];
        $.each(listItems, function (index, item) {
            var input = $(this).find('input');
            values.push(input.val());
        });
        log('Values extracted.');
    }

    function loadValues() {

        testOutput.html('@{R=DeviceStudio.Strings;K=WEBDATA_LF0_30; E=js}');
        testOutput.show();

        $.ajax({
            type: "POST",
            url: "/Orion/DeviceStudio/Services/FormulaEditor.asmx/GetCurrentValuesBeta",
            data: JSON.stringify({ variables: variables, table: testDataSourceTable || "" }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            error: function (error) {
                log(error);
            },
            success: function (response) {
                // iterate over the inputs and set the loaded value
                $(".sw_ds_fe_inputList li input").each(function (index, item) {
                    item.value = response.d[index];
                });
                testOutput.hide();
                realValuesLoaded = true;
            }
        });
    }

    function parseFormula() {
        extractVariables();
        extractValues();
        var tmpFormula = formula,
            tmpPattern,
            tmpRegex;
        // iterate over the variables and try to replace each by value at same index
        $.each(variables, function (index, item) {
            tmpPattern = "\\[" + item + "\\]";
            tmpRegex = new RegExp(tmpPattern, "g");
            tmpFormula = tmpFormula.replace(tmpRegex, values[index] || '[' + item + ']');
        });
        return tmpFormula;
    }

    function reset() {
        variables = [];
        variablesWithActiveInputs = [];
    }

    // CTOR ↓

    if (enableLog) logging = true;
    listItems = $();
    variablesWithActiveInputs = [];
    variables = [];
    variablesRegexp = /[\w\.]+(?=\])/g;
    values = [];

    // prepare DOM
    prepareEditor(editorSelector);
    preparePlaceholder();

    // bind the events to editor node
    bindEditorEvents();

    // if there's already some formula in textbox and we want to parse it immediately
    extractVariables();
    updateList();

    return this;

};