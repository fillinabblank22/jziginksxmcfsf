﻿SW.Core.namespace("SW.DeviceStudio.CreatePollerWizard").FormulaEditor = function () {
    var initialized = false;
    var selectedTab = 0;
    var tabs = new Array();
    var resultType;
    var selectedTypeLabel;
    var errors = {};
    var logging;
    var errorsList;
    var callbackStatus;
    var resultPropertyElement;
    var netObjectId;

    function onError(data) {
        $("#FormulaEditor_DebugType").html(data.statusText);

        var r = $.parseJSON(data.responseText);
        $("#FormulaEditor_DebugMessage").html(r.Message);
        $("#FormulaEditor_DebugStackTrace").html(r.StackTrace);
    }

    // replaces ' and " to |
    function encodeJSON(value) {
        value = value.replace(/'/g, '||').replace(/"/g, '||');
        return value;
    }

    function callService(method, data, onSuccess) {
        log("calling webmethod: " + method + " params: " + data);
        $.ajax({
            type: "POST",
            url: "/Orion/DeviceStudio/Services/FormulaEditor.asmx/" + method,
            data: data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            error: onError,
            success: onSuccess
        });
    }

    function getResultProperty() {
        return $.trim(resultPropertyElement.val());
    }

    function validate() {
        if (getResultProperty() === '') {
            addError('emptyName', '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_26; E=js}');
            return false;
        }
        else {
            clearError('emptyName');
            return true;
        }
    }

    function restoreDataSource(callback) {
        callService('RestoreDataSource', '{}', function (data, status) { if (callback) callback(); });
    }

    function init(options) {
        
        logging = options.loggingEnabled;

        dlg = $("#FormulaEditor");
        dlg.dialog({
            width: 660,
            height: 'auto',
            modal: true,
            autoOpen: false,
            title: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_27; E=js}',
            close: function () {
                if (callbackStatus == 'CANCEL') {
                    restoreDataSource(options.callback(callbackStatus));
                }
                else {
                    options.callback(callbackStatus);
                }
            }
        });

        netObjectId = options.objectId;

        // set description texts
        $("#fePropertyNameLabel").html(options.propertyName);
        $("#fePropertyDescriptionLabel").html(options.description);

        if (!initialized) {

            // attach event handlers for action buttons
            $("#FormulaEditor_Submit").on('click', submit);
            $("#FormulaEditor_Cancel").on('click', function () {
                callbackStatus = 'CANCEL';
                dlg.dialog('close');
            });

            errorsList = $("#FormulaEditor_Errors");
            resultPropertyElement = $("#FormulaEditor_ResultProperty");
            
            initialized = true;
        }
    }

    function generatePropertyName(table, property, callback) {
        resultPropertyElement.val('');
        callService("GeneratePropertyName", JSON.stringify({preffix: table + property}), function (data, status) {
            resultPropertyElement.val(data.d);
            if (callback) callback();
        });
    }

    function loadFormula(itemId, callback){
        var textarea = $('#ds_fe_mathsimple_formula');
        textarea.val('@{R=DeviceStudio.Strings;K=WEBDATA_LF0_28; E=js}');
        callService("LoadExpression", JSON.stringify({itemIdentifier: itemId}), function (data, status) {
            textarea.val(data.d);
            if (callback) callback();
        });
    }

    function showDialog(options) {

        // reinit controls to reload data
        $.each(tabs, function (index, value) {
            value.Init();
        });

        tabs[selectedTab].Show();
        clear();
        for (var tab in tabs) {
            if (tabs.hasOwnProperty(tab) && tabs[tab].hasOwnProperty('Clear')) {
                tabs[tab].Clear();
            }
        }

        // edit mode
        if (options && options.edit === true && options.editItemName !== '') {
            resultPropertyElement.val(options.editItemName);
            loadFormula(options.editItemId);
            dlg.triggerHandler("focus");
        }

        callbackStatus = 'CANCEL'; // setting default (will be returned when ESC key is used to close dialog)
        dlg.dialog("open"); // we need a loader... otherwise there is a delay between clicking the button and opening a dialog for first time
    }

    function launch(options) {

        // OPTIONS: 
        // table - name of a table containing the selected property (List Properties / Output Config)
        // property - name of a selected property (List Properties / Output Config)
        // description - property description text
        // callback - function that will be called once formula editor is closed
        // edit - flag: enables edit mode (editing existing formula)
        // editItemId - item idenfitier of source selected for editing
        // editItemName - name of source selected for editing
        // logging - flag: enables logging
        // objectId

        init(options);

        if (options.edit === true){
            log('editing: ', options.editItemName);
            dlg.dialog('option', 'title', '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_29; E=js}');
            resultPropertyElement.val(options.editItemName);
            showDialog(options);
        }
        else {
            generatePropertyName(options.table, options.propertyName, function(){
                showDialog();
            });
        }
    }

    function submit() {
        
        // To submit formula, we have to:
        //
        // 1. Validate editor (name field is set)
        // 2. Validate (pass the test) active tab
        //
        // Active tab testing is usually asynchronous so here we are
        // passing callbacks instead of immediately processing it.        
        
        function onSubmit() {
            callbackStatus = 'OK';
            dlg.dialog("close");
        }

        function onSuccessCallback() {
            log('FE: Test success callback.');
            tabs[selectedTab].Submit(onSubmit);
            resetErrors();
        }

        function onFailCallback() {
            log('FE: Test fail callback.');
            showErrors();
        }

        // validate editor
        if (!validate()) return;

        // validate active tab
        tabs[selectedTab].Validate(onSuccessCallback, onFailCallback);
    }
    

    function addType(type) {
        if (!tabs[type.Order]) {
            log("adding " + type.DisplayName + " " + type.Order);
            tabs[type.Order] = type;
        }
    }

    function clear() {
        resetErrors();
    }

    function log(msg) {
        if (logging && window.console) {
            console.log(msg);
        }
    }

    function addError(key, message) {
        errors[key] = {};
        errors[key]['key'] = key;
        errors[key]['message'] = message;
        log("Error registered: " + key);
        showErrors();
        return true;
    }

    function clearError(key) {
        if (errors.hasOwnProperty(key)) {
            delete errors[key];
            showErrors();
            log('Error "' + key + '" cleared.');
            return true;
        }
        else {
            log('Can\'t clear error "' + key + '" because it\'s not registered');
            return false;
        }
    }

    function hasErrors() {
        for (var key in errors) {
            if (errors.hasOwnProperty(key)) {
                return true;
            }
        }
        return false;
    }

    function showErrors() {
        errorsList.html('');
        for (var key in errors) {
            if (errors.hasOwnProperty(key)) {
                $('<li>' + errors[key].message + '</li>').appendTo(errorsList);
            }
        }
    }

    function resetErrors() {
        errors = {};
        errorsList.html('');
    }

    // PUBLIC ↓

    return {
        AddType: addType,
        Launch: launch,
        CallService: callService,
        ResultType: function () { return resultType; },
        ResultProperty: getResultProperty,
        Validate: validate,
        EncodeJSON: encodeJSON,
        AddError: addError,
        ClearError: clearError,
        HasErrors: hasErrors,
        ShowErrors: showErrors,
        GetNetObjectId: function() { return netObjectId; }, 
        Log: log
    }
} ();