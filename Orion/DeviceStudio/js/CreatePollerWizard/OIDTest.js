﻿SW.Core.namespace("SW.DeviceStudio").OIDTest = function () {

    var dialogData = dialogData || {};
    var NodeID = NodeID || 0;
    var oidPicker;
    var propertyIsTable = false;
    var manualEntry = manualEntry || {};
    var onFixedValueLinkClick; // function that should be called when user chooses to click 'enter a fixed value' link

    function resetData() {
        dialogData.success = false;
        dialogData.nodeID = '';
        dialogData.response = '';
        $('#pickerHolder').html("");
        $("#oidPickerError").hide();
    }

    function saveToPoller(selection, cback) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/Orion/DeviceStudio/Services/Wizard.asmx/SaveToPoller",
            data: JSON.stringify({ oidArray: selection }),
            success: function (data) {
                dialogData.success = true;
                dialogData.nodeID = NodeID;
                dialogData.response = selection;
                cback('OK', dialogData);
                $("#loaderBackground").css('visibility', 'hidden');
                $("#loader").css('visibility', 'hidden');
                $("#OIDTest").dialog('close');
            },
            error: function (xhr) {
                dialogData.success = false;
            },
            dataType: 'json'
        });
    }

    function testManualOID(cbSuccess, cbError) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/Orion/DeviceStudio/Services/Wizard.asmx/TestManualOID",
            data: JSON.stringify({ nodeID: NodeID, name: manualEntry.Name, oid: manualEntry.OID, pollingType: manualEntry.SNMPType }),
            success: function (data) {
                var result = data.d;
                if (result.MessageType != 0) {
                    clearAllErrorWarning();
                    if (result.MessageType >= 4) {
                        writeMessage(result.MessageType, result.Message);
                        cbError(true, result.Message);
                        return;
                    }
                    else {
                        writeMessage(result.MessageType, result.Message);
                        if ($.trim(result.OriginalName) !== "") {
                            manualEntry.Name = result.OriginalName;
                        }
                        SW.OidPicker.fillManualResult(manualEntry.Name, manualEntry.OID, result.Response, (manualEntry.SNMPType == 2));
                        cbError(false, result.Message);
                        return;
                    }
                }

                if (result.Response[0].Type === 'UNSUPPORTED') {
                    cbError(true, '@{R=Core.Strings;K=WEBJS_GK0_52; E=js}');
                    return;
                }

                SW.OidPicker.fillManualResult(manualEntry.Name, manualEntry.OID, result.Response, (manualEntry.SNMPType == 2));
                cbSuccess();
            },
            error: function (xhr) {
                cbError(true, null);
            },
            dataType: 'json'
        });
    }

    function testManualEntry()
    {
        manualEntry.Name = $.trim($("#manualOIDName").val());
        manualEntry.OID = $.trim($("#manualOID").val());
        manualEntry.SNMPType = -1;
        clearAllErrorWarning();

        if (manualEntry.Name === "") {
            writeError($("#manualOIDNameMessageFrame"), '@{R=DeviceStudio.Strings;K=WEBJS_JF0_01;E=js}');
            return false;
        }

        if (manualEntry.OID === "") {
            writeError($("#manualOIDMessageFrame"), '@{R=DeviceStudio.Strings;K=WEBJS_JF0_02;E=js}');
            return false;
        }

	if (manualEntry.Name.match(/[.]/)!==null) {
            writeError($("#manualOIDNameMessageFrame"), '@{R=DeviceStudio.Strings;K=WEBJS_JF0_05;E=js}');
            return false;
        }

        if (manualEntry.OID.match(/^1.[023](.(\d+))+/)===null) {
            writeError($("#manualOIDMessageFrame"), '@{R=DeviceStudio.Strings;K=WEBJS_JF0_03;E=js}');
            return false;
        }

        if ($("#manualOIDGet").attr('checked') == 'checked') {
            manualEntry.SNMPType = 0;
        }

        if ($("#manualOIDGetNext").attr('checked') == 'checked') {
            manualEntry.SNMPType = 1;
        }

        if ($("#manualOIDGetTable").attr('checked') == 'checked') {
            manualEntry.SNMPType = 2;
        }

        if (manualEntry.SNMPType == -1) {
            writeError($("#manualOIDSNMPMessageFrame"), '@{R=DeviceStudio.Strings;K=WEBJS_JF0_04;E=js}')
            return false;
        }
        else {
            clearAllErrorWarning();
        }

        return true;
    }

    function writeMessage(level, message) {
        switch (level) {
            case 1:
                writeWarning($("#manualOIDNameMessageFrame"), message);
                return;
            case 2:
                writeWarning($("#manualOIDMessageFrame"), message);
                return;
            case 3:
                writeWarning($("#manualOIDSNMPMessageFrame"), message);
                return;
            case 4:
                writeError($("#manualOIDNameMessageFrame"), message);
                return;
            case 5:
                writeError($("#manualOIDMessageFrame"), message);
                return;
            case 6:
                writeError($("#manualOIDNameMessageFrame"), message);
                return;
            default:
                return;
        }
    }

    function writeError(control, message) {
        control.find("SPAN").text(message);
        control.addClass('sw-suggestion-fail');
        control.show();
    }

    function writeWarning(control, message) {
        control.find("SPAN").text(message);
        control.addClass('sw-suggestion-warn');
        control.show();
    }

    function clearAllErrorWarning() {
        $("#manualOIDNameMessageFrame").hide();
        $("#manualOIDNameMessageFrame").removeClass('sw-suggestion-fail sw-suggestion-warn');
        $('#manualOIDNameMessageFrame SPAN').text('');

        $("#manualOIDMessageFrame").hide();
        $("#manualOIDMessageFrame").removeClass('sw-suggestion-fail sw-suggestion-warn');
        $('#manualOIDMessageFrame SPAN').text('');

        $("#manualOIDSNMPMessageFrame").hide();
        $("#manualOIDSNMPMessageFrame").removeClass('sw-suggestion-fail sw-suggestion-warn');
        $('#manualOIDSNMPMessageFrame SPAN').text('');
    }

    function clearManualEntry() {
        $("#manualEntry").prop('checked', false);
        $("#manualEntryPanel").css('display', 'none');
        $("#manualOIDName").val('');
        $("#manualOID").val('');
        $("#manualOIDGetNext").attr('checked', true);
    }

    function prepareLinks() {
        var x = $("#helpText").html();
        x = x.replace(/\{Google\}/g, '<a href="http://www.google.com" target="_blank">@{R=DeviceStudio.Strings;K=WEBJS_JF0_06;E=js}</a>');
        x = x.replace(/\{Thwack\}/g, '<a href="http://thwack.solarwinds.com/welcome" target="_blank">@{R=DeviceStudio.Strings;K=WEBJS_JF0_07;E=js}</a>');
        $("#helpText").html(x);

        var $fvdSection = $('#dontNeedAnOID');
        var fvdSectionContent = $fvdSection.html();
        fvdSectionContent = fvdSectionContent.replace(/\{fixedvalue\}/g, '<a href="javascript:void(0);" data-target="fixedvalue">');
        fvdSectionContent = fvdSectionContent.replace(/\{a\}/g, '</a>');
        $fvdSection.html(fvdSectionContent);

        $fvdSection.off('click').on('click', function(e) {
            if (e.target.getAttribute('data-target') === 'fixedvalue') {
                $('#OIDTest').dialog('close');
                onFixedValueLinkClick();
            }
        });
    }

    function xinit(cback, nodeId, propertyName, propertyDescription, propertyRequired, propertyTable, fixedValueLinkCallback) {
        NodeID = nodeId;
        onFixedValueLinkClick = fixedValueLinkCallback;

        prepareLinks();

        if (propertyTable && propertyTable.length > 0) {
            propertyIsTable = true;
        } else {
            propertyIsTable = false;
        }
        if (propertyName !== undefined) {
            if (propertyRequired === true) {
                $('#propertyNameHolder').text(String.format('@{R=DeviceStudio.Strings;K=WEBDATA_GK0_44;E=js}', propertyName));
            } else {
                $('#propertyNameHolder').text(String.format('@{R=DeviceStudio.Strings;K=WEBDATA_GK0_45;E=js}', propertyName));
            }
            $('#propertyDescriptionHolder').html(propertyDescription);
        }
        if (propertyRequired === true) {
            $('#propertyRequiredHolder').text('@{R=DeviceStudio.Strings;K=WEBDATA_GK0_45;E=js}');
        }

        clearManualEntry();
        clearAllErrorWarning();

        $("#btnOK").off("click").on("click", function (e) {
            e.preventDefault();
            if (!$("#manualEntry")[0].checked) {
                if (SW.OidPicker.hasValidOid()) {
                    $('#oidPickerError').hide();
                    $("#loaderBackground").css('visibility', 'visible');
                    $("#loader").css('visibility', 'visible');
                    saveToPoller(SW.OidPicker.getSelection(), cback);
                }
                else {
                    $('#oidPickerError').show();
                    $("#oidPickerErrorMessage").html(SW.OidPicker.getFailureReason());
                }
            }
            else {
                if (testManualEntry()) {
                    testManualOID(function () {
                        $("#oidPickerError").hide();

                        var result = [];
                        result.push({
                            isManual: true,
                            oid: manualEntry.OID,
                            name: manualEntry.Name,
                            snmpType: manualEntry.SNMPType
                        });
                        saveToPoller(result, cback);
                    },                   
                    function (isError, message) {
                        if (isError) {
                            $('#oidPickerError').show();
                            $("#oidPickerErrorMessage").html(message);
                        }
                        else {
                            var result = [];
                            result.push({
                                isManual: true,
                                oid: manualEntry.OID,
                                name: manualEntry.Name,
                                snmpType: manualEntry.SNMPType
                            });
                            saveToPoller(result, cback);
                        }
                    });
                }
            }
        });

        $("#btnCancel").off("click").on("click", function (e) {
            e.preventDefault();
            cback('cancel', dialogData);
            $("#OIDTest").dialog('close');
        });

        $("#manualEntry").off("change").on("change", function (e) {
            e.preventDefault();
            $("#oidPickerError").hide();
            if (this.checked) {
                SW.OidPicker.clearSelection();
                $("#manualEntryPanel").css('display', 'inherit');
            }
            else {
                $("#manualEntryPanel").css('display', 'none');
            }
        });

        $("#manualOIDPollCurrentValue").off("click").on("click", function (e) {
            e.preventDefault();
            $("#oidPickerError").hide();
            if (testManualEntry()) {
                testManualOID(function () { }, function (isError, message) {
                    if (isError) {
                        $('#oidPickerError').show();
                        $("#oidPickerErrorMessage").html(message);
                    }
                });

            }           
        });

        var dialogTitle;
        if (propertyName !== undefined) {
            dialogTitle = String.format('@{R=DeviceStudio.Strings;K=WEBDATA_GK0_42;E=js}', propertyName);
        } else {
            dialogTitle = '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_43;E=js}';
        }
        $('#OIDTest').dialog({
            title: dialogTitle,
            width: 900,
            height: 'auto',
            modal: true,
            close: function () {
                SW.OidPicker.reset();
            }
        });
    };

    return {
        dialogData: dialogData,
        init: xinit,
        showDialog: function () {
            resetData();
            $("#OIDTest").show();
            oidPicker = SW.OidPicker.init({
                serviceUrl: '/Orion/Services/OidPicker.asmx',
                wizardUrl: '/Orion/DeviceStudio/Services/Wizard.asmx',
                nodeId: NodeID,
                renderTo: 'pickerHolder',
                propertyIsTable : propertyIsTable
            });
            oidPicker.on('OIDPicker.SelectionChange', function () {
                $("#oidPickerError").hide();
                $("#manualEntry").prop('checked', false);
                $("#manualEntryPanel").css('display', 'none');
            });
        }
    }
}();

