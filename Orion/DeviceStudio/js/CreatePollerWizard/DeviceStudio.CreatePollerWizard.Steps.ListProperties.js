﻿/* global SW:true, Ext42 */ // JSHint directive
(function (self) {

  /*
   * CONFIG:
   */

  var config = {
    dsmLinkClass: 'sw_ds_openDsm',
    validatorClass: 'sw_ds_firstTimeError',
    toolbarIconClass: 'SW-DS-X42-ICON'
  };

  /*
   * STRINGS:
   */

  var strings = {
    dsmLinkFormat: '<a href="javascript:void(0)" class="' + config.dsmLinkClass + '" data-itemId="{0}">{1}</a>{2}',
    dsmLinkOptionalText: ' (optional)', // TODO: i18n
    dsmLinkText: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_23; E=js}',
    propertyIsRequired: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_65; E=js}',
    propertyIsOptional: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_66; E=js}',
    descriptionLabel: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_67; E=js}',
    requiredIconTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_22; E=js}',
    emptyGrid: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_10; E=js}',
    nameColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_11; E=js}',
    tableColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_12; E=js}',
    mappingColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_13; E=js}',
    valueColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_14; E=js}',
    editBtnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_15; E=js}',
    pollBtnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_16; E=js}'
  };

  /*
   * PRIVATE:
   */

  var dsm; // DataSourceManager instance
  var expandedRowTemplate; // html that will be shown when a row is expanded
  var grid; // main extjs grid showing output config
  var gridDataStore; // extjs store connected to webservice with AJAX proxy
  var gridSelectionModel; // extjs selectionModel - using checkboxes with single select option
  var placeholderId; // id of element used for rendering
  var wrapper; // extjs wrapper for sidebar and grid

  Ext42.define('OutputProperty', {
    extend: 'Ext42.data.Model',
    fields: [
      'Name',
      'Type',
      'Mapping',
      'Value',
      'Table',
      'Description',
      'Required',
      'DisplayName'
    ]
  });

  gridDataStore = Ext42.create('Ext42.data.Store', {
    model: 'OutputProperty',
    autoLoad: false,
    proxy: {
      type: 'ajax',
      url: '/Orion/DeviceStudio/Services/PollerDatasourceManager.asmx/GetOutputConfig',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      reader: {
        type: 'json',
        root: 'd',
        id: 'Name'
      },
      pageParam: false,
      startParam: false,
      limitParam: false,
      noCache: false
    }
  });

  gridSelectionModel = Ext42.create('Ext.selection.CheckboxModel', {
    mode: 'single',
    checkOnly: true,
    allowDeselect: true,
    listeners: {
      selectionchange: selectionChangedHandler
    }
  });

  expandedRowTemplate = new Ext42.XTemplate(
    '<div class="outputPropertyDetail"><table>',
      '<tr><td></td><td><strong>',
        '<tpl if="Required==true">' + strings.propertyIsRequired + '</tpl>',
        '<tpl if="Required==false">' + strings.propertyIsOptional + '</tpl>',
        '<img src="/Orion/Nodes/images/icons/Icon.info.gif" alt="info" title="' + strings.requiredIconTitle + '" align="top" class="outputPropertyRequiredFlagInfo" />',
      '</strong></td></tr>',
      '<tr><td><strong>' + strings.descriptionLabel + '</strong></td><td><textarea>{Description}</textarea></td></tr>',
    '</table></div>'
  );

  grid = Ext42.create('Ext42.grid.Panel', {
    id: 'outputGrid',
    store: gridDataStore,
    selModel: gridSelectionModel,
    plugins: {
      ptype: 'rowexpander',
      rowBodyTpl: expandedRowTemplate,
      selectRowOnExpand: false
    },
    layout: {
      type: 'fit',
      reserveScrollbar: false
    },
    viewConfig: {
      emptyText: strings.emptyGrid
    },
    columns: [
        {
          text: strings.nameColumnTitle,
          dataIndex: 'DisplayName',
          flex: 4,
          xtype: 'templatecolumn',
          tpl: '<strong>{DisplayName}</strong>'
        },
        {
          text: strings.tableColumnTitle,
          dataIndex: 'Table',
          flex: 2
        },
        {
          text: strings.mappingColumnTitle,
          dataIndex: 'Mapping',
          flex: 4,
          renderer: dataSourceRenderer
        },
        {
          text: strings.valueColumnTitle,
          dataIndex: 'Value',
          flex: 3
        }
    ],
    tbar: [
        {
          text: strings.editBtnTitle,
          itemId: 'editBtn',
          disabled: true,
          handler: function () {
            var selectedItem = grid.getSelectionModel().getSelection()[0];
            var iid  = selectedItem.data.Table + '.' + selectedItem.data.Name;
            dsmLinkClickHandler(iid);
          },
          icon: '/Orion/Nodes/images/icons/icon_edit.gif',
          cls: config.toolbarIconClass
        },
        {
          text: strings.pollBtnTitle,
          itemId: 'pollBtn',
          handler: function () {
            load(true);
          },
          icon: '/Orion/DeviceStudio/Images/test_credentials_16x16.png',
          cls: config.toolbarIconClass
        }
    ]
  });

  // We allow deselection in a grid => there can be 0 rows selected
  // We want to disable row-related buttons (e.g. edit) in such case.
  function selectionChangedHandler(sm,selection) {
    grid.down('#editBtn').setDisabled(selection.length === 0);
  }

  // Will render a value + link or just a link if value isn't set yet
  function dataSourceRenderer(value, record, item) {
    var id = item.data.Table + '.' + item.data.Name;
    var displayText = value || strings.dsmLinkText;
    var optionalText = item.data.Required ? '' : strings.dsmLinkOptionalText;
    return String.format(strings.dsmLinkFormat, id, displayText, optionalText);
  }

  // Grid contains links that are supposed to open DataSourceManager
  // I need to attach the correct handler to them
  // The selector should translate to: $('#placeholderId .sw_ds_openDsm')
  function attachHandlersToGrid() {
    var selector = '#' + placeholderId + ' .' + config.dsmLinkClass;
    var links = $(selector);
      links.off('click').on('click', function () {
        dsmLinkClickHandler($(this).attr('data-itemId'));
      });
  }

  // Before we open the DataSourceManager, we have to set (or create new)
  // matching datasource at server. If this is successful, we also store 
  // the metadata on client side and then open DataSourceManager.
  function dsmLinkClickHandler(itemId){
    $.ajax({
      type: 'POST',
      contentType: 'application/json',
      url: '/Orion/DeviceStudio/Services/Wizard.asmx/OutputPropertySelected',
      data: JSON.stringify({ itemIdentifier: itemId }),
      success: function (response) {
        storePropertyMetadata(response.d);
        dsm.open();
      },
      error: function () {
      }
    });
  }

  // When some property is selected, I want to store it's metadata (returned
  // from server) so I can access them later from other components/pages.
  function storePropertyMetadata(metadata) {
    var meta = {};
    meta.name = metadata.name;
    meta.displayName = metadata.displayName || metadata.name;
    meta.table = metadata.table;
    meta.description = metadata.description;
    meta.type = metadata.type;
    meta.units = metadata.units;
    meta.mapping = metadata.mapping;
	meta.required = metadata.required;
    SW.DeviceStudio.CreatePollerWizard.SelectedPropertyMetadata = meta;
  }

  // This function handles loading data for grid from store (web service)
  function load(pollValues) {
    // we are deleting error messages from validator (if there are any) here
    // because there is asp net server side validation and the error message
    // remains at the page until next postback (-we don't want to wait for it)
    var validator = $("#cdpListPropertiesValidator .sw-validation-error");
    if (validator) {
      if (validator.hasClass(config.validatorClass)) {
        validator.html('');
      } else {
        validator.addClass(config.validatorClass);
      }
    }
    // Params have to be encoded and service must allow GET requests.
    // Once we load the data, we want to assign correct handlers to links.
    grid.store.load({
      params: {
        pollValues: Ext42.encode(pollValues || false)
      },
      callback: function(){
        // TODO: Might be refactored. We don't have to attach handlers every single
        // time in this case because output properties probably won't change.
        attachHandlersToGrid();
      }
    });
  }

  // Rendering the Ext JS components to placeholder
  function render() {
    grid.render(placeholderId);
  }

  /*
   * PUBLIC:
   */

  self.init = function(domNodeId) {
    placeholderId = domNodeId;
    dsm = new SW.DeviceStudio.CreatePollerWizard.DataSourceManager({
      logging: true,
      onCloseCallback: function() {
        load(true);
      }
    });
    load();
    render();
  };

  self.refresh = load;

})(SW.Core.namespace("SW.DeviceStudio.CreatePollerWizard.Steps.ListProperties"));