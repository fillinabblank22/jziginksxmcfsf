﻿/* global SW:true, Ext42 */ // JSHint directive

/*
options: {
  logging: true, // enable|disable log for debug purposes,
  onCloseCallback: function(){} // this is executed when is dialog closed
}
*/

SW.Core.namespace('SW.DeviceStudio.CreatePollerWizard').DataSourceManager =
function (options) {

    var self = this;

    //
    // CONFIG

    // Default values here can be overriden by passed options object
    var config = $.extend({
        dialogWidth: 800,
        dialogHeight: 'auto',
        gridHeight: 200,
        gridWidth: 770,
        gridPlaceholderId: 'cdpDataSourceGrid',
        dialogPlaceholderId: 'cdpDataSourceDlg',
        disabledRowClass: 'sw_ds_DatasourceManager_record_enabled',
        enabledRowClass: 'sw_ds_DatasourceManager_record_disabled',
        noTableValue: 'No Table',
        openFixedValueDialogForTechnology: '7f842ebb-addd-4857-a10b-e7443cd97e32'
    }, options);

    //
    // STRINGS

    var strings = {
        optionalFlagText: ' (optional)',
        dialogTitleFormat: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_68; E=js}',
        defaultDialogTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_23; E=js}',
        nameColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_18; E=js}',
        detailsColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_33; E=js}',
        valueColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_34; E=js}',
        editColumnTitle: '@{R=DeviceStudio.Strings;K=WEBDATA_GK0_05; E=js}',
        removeColumnTitle: '@{R=DeviceStudio.Strings;K=DatasourceManager_ColumnHeader_01; E=js}',
        editDialogTitle: '@{R=DeviceStudio.Strings;K=DatasourceManager_ConfirmEdit_Title; E=js}',
        editDialogText1: '@{R=DeviceStudio.Strings;K=DatasourceManager_ConfirmEdit_Text_01; E=js}',
        editDialogText2: '@{R=DeviceStudio.Strings;K=DatasourceManager_ConfirmEdit_Text_02; E=js}',
        removeDialogTitle: '@{R=DeviceStudio.Strings;K=DatasourceManager_ConfirmRemove_Title; E=js}',
        removeDialogText1: '@{R=DeviceStudio.Strings;K=DatasourceManager_ConfirmRemove_Text_01; E=js}',
        removeDialogText2: '@{R=DeviceStudio.Strings;K=DatasourceManager_ConfirmRemove_Text_02; E=js}',
        removeDialogYesBtn: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_8;E=js}',
        removeDialogNoBtn: '@{R=DeviceStudio.Strings;K=WEBDATA_LH0_9;E=js}'
    };

    //
    // PRIVATE

    var $browseBtn; // jQuery-wrapped 'Browse OIDs' button
    var $calculateBtn; // jQuery-wrapped 'Add calculated value' button
    var $fixedValueBtn; // jQuery-wrapped 'Add fixed value' button
    var $cancelBtn; // jQuery-wrapped cancel button
    var $dlg; // jQuery UI dialog
    var $outputPropertyDescriptionLabel; // DOM placeholder for description
    var $outputPropertyNameLabel; // DOM placeholder for name
    var $outputPropertyRequiredLabel; // DOM placeholder for required flag
    var $submitBtn; // jQuery-wrapped submit button
    var grid; // extjs grid with datasource properties
    var gridSelectionModel; // extjs selectionModel - using checkboxes with single select option
    var gridStore; // extjs store connected to webservice with AJAX proxy
    var meta; // metadata related to selected output property
    var objectId; // Node ID (read from DOM)
    var rendered; // bool for state control
    var selectedTechnology; // GUID of selected technology (read from DOM) - hidden input
    var fvd; // instance of FixedValueDialog

    // The following panels (and way they are used) are terrible
    // but there is a tight connection to DOM and specific states of DSM.
    // It was caused by a mix of poor initial design and zillion of changes 
    // from user point of view (UX). 
    // I would need a new task (=time) to improve it. Don't have that now. 
    var $panelWrapper; // div container
    var $compatiblePanel; // div container
    var $incompatiblePanel; // div container
    var $unsupportedPanel; // div container
    var $errorPanel; // div container
    var $incompatiblePropertyTable; // paragraph with info text/template
    var $incompatibleProperty; // paragraph with info text/template
    var $incompatibleTable; // paragraph with info text/template
    var $incompatibleTableType; // paragraph with info text/template
    var $errorNullvalueProperty; // paragraph with info text/template
    var $errorNullvalueTable; // paragraph with info text/template

    Ext42.define('DataSourceProperty', {
        extend: 'Ext42.data.Model',
        fields: ['IconUrl', 'Name', 'Table', 'Value', 'Details', 'ItemIdentifier', 'RecordType', 'IsCompatibleWithOutput', 'InCompatibleIcon']
    });

    gridStore = Ext42.create('Ext42.data.Store', {
        model: 'DataSourceProperty',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '/Orion/DeviceStudio/Services/PollerDatasourceManager.asmx/GetDatasourceRecords',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            reader: {
                type: 'json',
                root: 'd',
                id: 'Name'
            },
            pageParam: false,
            startParam: false,
            limitParam: false,
            noCache: false
        }
    });

    gridSelectionModel = Ext42.create('Ext.selection.CheckboxModel', {
        mode: 'single',
        checkOnly: true,
        allowDeselect: false,
        listeners: {
            beforeselect: function (sm, record) {
                // When a row is incompatible with wanted output, it shouldn't be possible to select it.
                // By default, it won't be selected, but the current selection visualization disappears.
                // This is a workaround to that problem:
                // If we find out that the user is trying to select incompatible row, we will get the last
                // known selection and selected that instead. This will fire the 'selectionchange' event,
                // which is not optimal, but I haven't found a better solution. {PD}
                if (record.data.IsCompatibleWithOutput) {
                    return true;
                } else {
                    // TODO: try to reselect only if the compatible row was selected before (not when it's empty)
                    var lastRow = sm.getLastSelected();
                    if (lastRow) {
                        sm.select(lastRow.index);
                    }
                    return false;
                }
            },
            selectionchange: function (sm, selection) {
                // Note: Currently, the selection model will always deselect current item first and then select new one,
                // but we would like to keep the selection in case the new one isn't compatible (you aren't allowed to select it).
                // I thought this could be handled in 'beforedeselect' event, but it doesn't have any information about the new one.
                // Possible solution would be to store the new row (that you are trying to select) from another event in some shared variable
                // and use that, but solution like this would be quite ugly and I would like to avoid it.
                // Note2: I also tried (record.setDisabled(true)) but that removes the row from grid completely.
                var selectedRow = selection[0];
                if (!selectedRow) {
                    return;
                }
                var selectedItemIdentifier = selectedRow.data.ItemIdentifier;
                if (!selectedItemIdentifier) {
                    return;
                }
                $.ajax({
                    type: 'post',
                    contentType: 'application/json',
                    data: JSON.stringify({ itemIdentifier: selectedItemIdentifier }),
                    url: '/Orion/DeviceStudio/Services/PollerDatasourceManager.asmx/UpdateLatestDatasourceRecord',
                    success: function () {
                        analyzeState();
                    },
                    error: function (err) {
                        log(err);
                    }
                });
            }
        }
    });

    grid = Ext42.create('Ext42.grid.Panel', {
        id: 'dataStoreGrid',
        height: config.gridHeight,
        width: config.gridWidth,
        store: gridStore,
        selModel: gridSelectionModel,
        layout: {
            type: 'fit',
            reserveScrollbar: true
        },
        autoScroll: true,
        autoFit: true,
        viewConfig: {
            //Return CSS class to apply to rows depending upon data values
            getRowClass: function (record) {
                if (record.data.IsCompatibleWithOutput) {
                    return config.disabledRowClass;
                }
                return config.enabledRowClass;
            }
        },
        columns: [
          {
              text: strings.nameColumnTitle,
              dataIndex: 'Name',
              flex: 6,
              xtype: 'templatecolumn',
              tpl: '<img src="/Orion/images/OidPicker/{IconUrl}" style="float:left" /> <span style="float:left;margin-top:2px;margin-left:2px">{Name}</span> {InCompatibleIcon}'
          },
          {
              text: strings.detailsColumnTitle,
              dataIndex: 'Details',
              flex: 8
          },
          {
              text: strings.valueColumnTitle,
              dataIndex: 'Value',
              flex: 4
          },
          {
              text: strings.editColumnTitle,
              width: 60,
              sortable: false,
              menuDisabled: true,
              align: 'center',
              renderer: editColumnRenderer
          },
          {
              text: strings.removeColumnTitle,
              width: 60,
              sortable: false,
              menuDisabled: true,
              align: 'center',
              xtype: 'templatecolumn',
              tpl: '<img class="sw_ds_DatasourceManager_removeIcon" data-recordId="{ItemIdentifier}" data-recordName="{Name}" src="/Orion/images/delete_16x16.gif" alt="" />'
          }
        ]
    });

    // Edit function shouldn't be available for OIDs, it's just for formulas
    function editColumnRenderer(value, record, item) {
        if (item.data.RecordType === 'OID') {
            return '';
        } else {
            return '<img class="sw_ds_DatasourceManager_editIcon" data-recordName="' + item.data.Name + '" data-recordId="' + item.data.ItemIdentifier + '" src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />';
        }
    }

    function log(msg) {
        if (options.logging && window.console) {
            console.log(msg);
        }
    }

    function init() {
        initVars();
        initDialog();
        attachControlButtonHandlers();
    }

    function initVars() {
        rendered = false;
        objectId = $('#hiddenNetObjectId').val();
        selectedTechnology = $('#hiddenTechnologyName').val();
        $dlg = $('#' + config.dialogPlaceholderId);
        $outputPropertyNameLabel = $('#cdpDataSourcePropertyNameLabel');
        $outputPropertyRequiredLabel = $('#cdpDataSourcePropertyRequiredLabel');
        $outputPropertyDescriptionLabel = $('#cdpDataSourcePropertyDescriptionLabel');
        $submitBtn = $('#cdpDataSourceDlg #submitBtn');
        $cancelBtn = $('#cdpDataSourceDlg #cancelBtn');
        $browseBtn = $('#cdpDataSourceDlg #cdpDataSourceBrowseActionButton');
        $calculateBtn = $('#cdpDataSourceDlg #cdpDataSourceCalculateActionButton');
        $fixedValueBtn = $('#cdpDataSourceDlg #cdpDataSourceFixedValueActionButton');
        $panelWrapper = $('#sw_ds_DataSourceManager_PanelWrapper');
        $compatiblePanel = $('#sw_ds_DatasourceManager_CompatiblePanel');
        $incompatiblePanel = $('#sw_ds_DatasourceManager_IncompatiblePanel');
        $unsupportedPanel = $('#sw_ds_DatasourceManager_UnsupportedPanel');
        $errorPanel = $('#sw_ds_DatasourceManager_ErrorPanel');
        $incompatiblePropertyTable = $('#incompatiblePropertyTable');
        $incompatibleProperty = $('#incompatibleProperty');
        $incompatibleTable = $('#incompatibleTable');
        $incompatibleTableType = $('#incompatibleTableType');
        $errorNullvalueProperty = $('#errorNullvalueProperty');
        $errorNullvalueTable = $('#errorNullvalueTable');
        fvd = new SW.DeviceStudio.CreatePollerWizard.FixedValueDialog({
            onSubmit: load
        });
    }

    function initDialog() {
        $dlg.dialog({
            width: config.dialogWidth,
            height: config.dialogHeight,
            modal: true,
            autoOpen: false,
            title: strings.defaultDialogTitle,
            open: dialogOpenHandler,
            close: dialogCloseHandler
        });
    }

    function dialogOpenHandler() {
        if (!rendered) {
            render();
            rendered = true;
        }
        load();
    }

    function dialogCloseHandler() {
        options.onCloseCallback();
    }

    function openDialog() {
        $dlg.dialog('open');
    }

    function closeDialog() {
        $dlg.dialog('close');
    }

    function attachControlButtonHandlers() {
        // When user confirms his mapping (datasource), update mapping on server
        $submitBtn.on('click', function (e) {
            e.preventDefault();
            var selectedRecord = grid.getSelectionModel().getSelection()[0];
            if (!selectedRecord) {
                log('Nothing was selected for assignment.');
            } else {
                updatePropertyMapping(
                  meta.table,
                  meta.name,
                  selectedRecord.data.Table,
                  selectedRecord.data.Name,
                  closeDialog
                );
            }
        });
        // When user cancels the dsm dialog, rollback all changes he might have done
        $cancelBtn.on('click', function (e) {
            e.preventDefault();
            rollbackConfigChanges(closeDialog);
        });
        // User wants to choose OID, open OID picker
        $browseBtn.on('click', function (e) {
            e.preventDefault();
            openOidPicker();
        });
        // User wants to transform value, open Formula Editor
        $calculateBtn.on('click', function (e) {
            e.preventDefault();
            openFormulaEditor();
        });
        // User wants to add fixed value, open Fixed Value Dialog
        $fixedValueBtn.on('click', function (e) {
            e.preventDefault();
            openFixedValueDialog();
        });
    }

    // save chosen property as a mapping for outputProperty in outputTable
    function updatePropertyMapping(outTab, outProp, mapTab, mapProp, onSuccess) {
        SW.Core.Services.callWebService(
          '/Orion/DeviceStudio/Services/PollerDatasourceManager.asmx',
          'UpdateOutputConfig',
          {
              outputTable: outTab === config.noTableValue ? null : outTab,
              outputProperty: outProp,
              mappingTable: mapTab,
              mappingProperty: mapProp
          },
          onSuccess,
          function (error) {
              log('Update output config error:');
              log(error);
          }
        );
    }

    // We want to restore polling config from backup because of real-time changes
    // that might have happened in the meantime.
    function rollbackConfigChanges(onSuccess) {
        SW.Core.Services.callWebService(
          '/Orion/DeviceStudio/Services/PollerDatasourceManager.asmx',
          'RestorePollingConfig',
          {},
          onSuccess,
          function (error) {
              log('Failed to restore polling config.');
              log(error);
          }
        );
    }

    function openOidPicker() {
        SW.DeviceStudio.OIDTest.init(function (status, data) {
            if (data.success === true) {
                load();
            }
        }, objectId, meta.displayName, meta.description, meta.required, meta.table, openFixedValueDialog); // Adding the last argument to be able to open FVD from OID Browser
        SW.DeviceStudio.OIDTest.showDialog();
    }

    function openFormulaEditor() {
        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Launch({
            table: meta.table,
            property: meta.name,
            propertyName: meta.displayName,
            description: meta.description,
            callback: formulaEditorCloseHandler,
            edit: false,
            logging: false,
            objectId: objectId
        });
    }

    function openFixedValueDialog() {
        fvd.open(meta.displayName, meta.table);
    }

    function openFormulaEditorForEdit(id, name) {
        SW.DeviceStudio.CreatePollerWizard.FormulaEditor.Launch({
            table: meta.table,
            property: meta.name,
            propertyName: meta.displayName,
            description: meta.description,
            callback: formulaEditorCloseHandler,
            edit: true,
            editItemId: id,
            editItemName: name,
            logging: false,
            objectId: objectId
        });
    }

    function formulaEditorCloseHandler(status) {
        // when we are done creating/editing formulas, we want to reload the grid data
        // we don't have to reload it if nothing was changed (CANCEL status received)
        if (status !== 'CANCEL') {
            load();
        }
    }

    function updateDOM() {
        $outputPropertyNameLabel.html(meta.displayName);
        $outputPropertyDescriptionLabel.html(meta.description);
        if (meta.required) {
            $outputPropertyRequiredLabel.html('');
        } else {
            $outputPropertyRequiredLabel.html(strings.optionalFlagText);
        }
        $dlg.dialog('option', 'title', String.format(strings.dialogTitleFormat, meta.displayName));
    }

    function load() {
        grid.store.load({
            params: {
                outputTable: Ext42.encode(meta.table),
                outputProperty: Ext42.encode(meta.name)
            },
            callback: storeLoadedCallback
        });
    }

    function storeLoadedCallback() {
        // check the current metadata and show info box when necessary
        analyzeState();
        // show OID picker if the grid is empty
        if (grid.store.count() === 0) {
            if (selectedTechnology === config.openFixedValueDialogForTechnology) {
                openFixedValueDialog();
            } else {
                openOidPicker();
                // there is an issue with getting right focus, this is a workaround
                var tree = Ext42.getCmp('oidPickerTree');
                if (tree) {
                    tree.focus();
                }
            }
        }
        // there might be different records after every load, so we have to
        // re-attach handlers to the links and buttons
        attachGridButtonHandlers();
    }

    function attachGridButtonHandlers() {
        // attach click handlers to edit icons
        // note: the .dman part in event name is a namespace
        $('#cdpDataSourceGrid .sw_ds_DatasourceManager_editIcon')
        .off('click.dman')
        .on('click.dman', function () {
            var self = $(this);
            var name = self.attr('data-recordName');
            var id = self.attr('data-recordId');
            var message = '<strong>'
              + String.format(strings.editDialogText1, name)
              + '</strong><br />'
              + strings.editDialogText2;
            var title = strings.editDialogTitle;
            Ext42.MessageBox.confirm(title, message, function (btn) {
                if (btn === 'yes') {
                    openFormulaEditorForEdit(id, name);
                }
            });
        });
        // attach click handlers to remove icons
        $('#cdpDataSourceGrid .sw_ds_DatasourceManager_removeIcon')
        .off('click.dman')
        .on('click.dman', function () {
            var self = $(this);
            var name = self.attr('data-recordName');
            var id = self.attr('data-recordId');
            var message = '<strong>'
              + String.format(strings.removeDialogText1, name)
              + '</strong><br />'
              + strings.removeDialogText2;
            var title = strings.removeDialogTitle;
            Ext42.Msg.show({
                title: title,
                msg: message,
                buttons: Ext42.Msg.YESNO,
                buttonText: {
                    yes: strings.removeDialogYesBtn,
                    no: strings.removeDialogNoBtn
                },
                icon: Ext42.Msg.ERROR,
                fn: function (action) {
                    if (action === 'yes') {
                        removeRecord(id, load);
                    }
                }
            });
        });
    }

    function analyzeState() {
        hideInfoPanels();
        $.ajax({
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({ outputTable: meta.table, outputProperty: meta.name }),
            url: '/Orion/DeviceStudio/Services/PollerDatasourceManager.asmx/GetLatestDatasourceRecord',
            success: function (data) {
                var result = data.d;
                switch (result.status) {
                    case 'compatible': {
                        showInfoPanel($compatiblePanel, result);
                        selectGridRow(result.itemIdentifier);
                    }
                        break;
                    case 'incompatible': {
                        showInfoPanel($incompatiblePanel, result);
                    }
                        break;
                    default: {
                        if (grid.store.count() === 0) {
                            showInfoPanel($errorPanel, result);
                        } else {
                            if (result.expectedColumnOrProperty === 'column') {
                                $("#errorNullvalueTable").show();
                            } else {
                                $("#errorNullvalueProperty").show();
                            }
                            showInfoPanel($unsupportedPanel, result);
                        }
                    }
                }
            },
            error: function () {
                // if there is a server error, we will hide the panel wrapper to
                // get rid of a big white space
                $panelWrapper.hide();
                log('Failed to get a response from web service.');
            }
        });
    }

    // TODO: REFACTOR THIS! It's terrible, but I don't have the energy to do it
    function showInfoPanel(panel, paneldata) {
        var v;
        var isTabular = false;
        // the value returned is always an array
        if (paneldata.currentValue === null || typeof paneldata.currentValue === 'undefined') {
            v = '';
        } else { // OID not polled
            {
                // multiple values - column            
                var identifiers = paneldata.itemIdentifier.split('.');
                if (identifiers[0] !== '') {
                    isTabular = true;
                    v = paneldata.currentValue.join(', ');
                    if (paneldata.expectedColumnOrProperty === 'column') {
                        $incompatibleTableType.show();
                    } else {
                        $incompatibleTable.show();
                    }
                } else { // single value - property
                    if (paneldata.expectedColumnOrProperty === 'property') {
                        $incompatibleProperty.show();
                    } else {
                        $incompatiblePropertyTable.show();
                    }
                    v = paneldata.currentValue[0];
                    if (v === null) {
                        v = '';
                    }
                }
                // if the value is too long, we have to cut it and provide whole text in title      
                if (v.length && v.length > 30) {
                    var temp = v.substring(0, 30) + '...';
                    v = '<span title="' + v + '">' + temp + '</span>';
                }
            }
        }

        // we need to show the panel wrapper if it is hidden (because of server error in previous call)
        if (!$panelWrapper.is(':visible')) {
            $panelWrapper.show();
        }

        // this handles modified strings because of internationalization
        var panelContent = panel.html();
        panelContent = panelContent.replace(/\{a\}/g, '</a>');
        panelContent = panelContent.replace(/\{value\}/g, '<span class="sw-highlighted" data-var="value"></span>');
        panelContent = panelContent.replace(/\{units\}/g, '<span data-var="units"></span>');
        panelContent = panelContent.replace(/\{name\}/g, '<span data-var="name"></span>');
        panelContent = panelContent.replace(/\{type\}/g, '<span data-var="type"></span>');
        panelContent = panelContent.replace(/\{formulaeditor\}/g, '<a href="javascript:void(0);" data-target="formulaeditor">');
        panelContent = panelContent.replace(/\{oidpicker\}/g, '<a href="javascript:void(0);" data-target="oidpicker">');
        panelContent = panelContent.replace(/\{fixedvalue\}/g, '<a href="javascript:void(0);" data-target="fixedvalue">');
        panel.html(panelContent);

        // update texts
        panel.find('span[data-var="value"]').html(v);
        panel.find('span[data-var="units"]').html(paneldata.expectedUnits || '');
        panel.find('span[data-var="type"]').html(paneldata.expectedType || '');
        panel.find('span[data-var="name"]').html(meta.displayName || '');

        // show the table visualization if we got tabular value
        var visualHolder = panel.find('.sw_ds_valueVisualization');
        if (isTabular) {

            // Construct data object for cyaable plugin, which renders each property
            // as a new column. I want to render just one column here, which
            // is named by received itemIdentifier and rows are loaded from
            // currentValue.
            var cyaableData = {};
            cyaableData[paneldata.itemIdentifier] = paneldata.currentValue;

            // Generating the node status icon with node name
            var headerHtml = '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_62; E=js}';
            headerHtml = headerHtml.replace('{0}', ''); // Don't need the OID name here, because it's added automatically from cyaableData
            headerHtml = headerHtml.replace('{1}', '<span><a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + objectId + '" target="_blank"><span class="oidPickerNode"></span></a></span>');

            visualHolder.cyaable({
                data: cyaableData,
                headerInfo: headerHtml,
                emptyText: '@{R=DeviceStudio.Strings;K=WEBDATA_LF0_63; E=js}'
            });

            $.ajax({
                url: '/Orion/DeviceStudio/Services/Wizard.asmx/LoadNode',
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    nodeId: objectId
                }),
                success: function (result) {
                    $('.oidPickerNode').html(result.d);
                }
            });

        } else {
            visualHolder.html('');
        }

        // set event handlers
        panel.find('a[data-target="oidpicker"]').off('click', openOidPicker).on('click', openOidPicker);
        panel.find('a[data-target="formulaeditor"]').off('click', openFormulaEditor).on('click', openFormulaEditor);
        panel.find('a[data-target="fixedvalue"]').off('click', openFixedValueDialog).on('click', openFixedValueDialog);

        // show
        panel.show();
    }

    function hideInfoPanels() {
        $compatiblePanel.hide();
        $incompatiblePanel.hide();
        $unsupportedPanel.hide();
        $errorPanel.hide();
        $incompatiblePropertyTable.hide();
        $incompatibleProperty.hide();
        $incompatibleTable.hide();
        $incompatibleTableType.hide();
        $errorNullvalueProperty.hide();
        $errorNullvalueTable.hide();
    }

    function selectGridRow(iid) {
        var record = grid.store.findRecord('ItemIdentifier', iid);
        if (record) {
            grid.getSelectionModel().select(record.index);
        }
    }

    function removeRecord(id, callback) {
        $.ajax({
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({ recordId: id }),
            url: '/Orion/DeviceStudio/Services/PollerDatasourceManager.asmx/RemoveRecord',
            success: function (data) {
                callback(data);
            },
            error: function (err) {
                log(err);
            }
        });
    }

    function render() {
        grid.render(config.gridPlaceholderId);
    }

    //
    // PUBLIC

    // We stored metadata in ListProperties step to memory, so we are going
    // to work with those unless we get a specific metadata object as parameter.
    self.open = function (metadata) {
        meta = metadata || SW.DeviceStudio.CreatePollerWizard.SelectedPropertyMetadata;
        updateDOM();
        openDialog();
    };

    self.refresh = function () {
        load();
    };

    //
    // CTOR

    init();
    return self;

};