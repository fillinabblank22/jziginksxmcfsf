﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../DeviceStudio.js" />

module("DeviceStudio.js related tests", {
    setup: function () { },
    teardown: function () { }
});

test("String.format replaces wildcards", function() {
    var stringWithPlaceholders = "Hello {0}!";
    var result = String.format(stringWithPlaceholders, "QUnit");

    equal(result, "Hello QUnit!");
});