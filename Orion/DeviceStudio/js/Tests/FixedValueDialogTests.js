﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../../../js/jquery-1.7.1/jquery-ui.min.js"/>
/// <reference path="../CreatePollerWizard/DeviceStudio.CreatePollerWizard.FixedValueDialog.js" />
module('Fixed Value Dialog', {
    setup: function () {
        this.fvd = new SW.DeviceStudio.CreatePollerWizard.FixedValueDialog({
            dialogElSelector: '#qunit-fixture',
            propertyNameElSelector: '#qunit-fixture',
            inputElSelector: '#qunit-fixture'
        });
    },
    teardown: function () {
        delete this.fvd;
    }
});

// Verify constructor implementation. 
test('Fixed Value Dialog module constructor works', function (assert) {

    assert.equal(this.fvd.property, '', 'Property name is initialzed as an empty string.');

    assert.throws(function () {
        var fvd = new SW.DeviceStudio.CreatePollerWizard.FixedValueDialog();
    }, "Constructor without arguments fails.");

    var fvd2 = new SW.DeviceStudio.CreatePollerWizard.FixedValueDialog({
        dialogElSelector: '#qunit-fixture',
        propertyNameElSelector: '#qunit-fixture',
        inputElSelector: '#qunit-fixture'
    });

    assert.ok(this.fvd.identity < fvd2.identity, 'Identity is being auto-incremented');

    assert.equal(this.fvd.cls, 'sw-ds-fvd-' + this.fvd.identity, 'Class is generated correctly ("' + this.fvd.cls + '")');

    assert.throws(function () {
        var incorrectSelector = new SW.DeviceStudio.CreatePollerWizard.FixedValueDialog({
            dialogElSelector: '#xxxx'
        });
    }, 'Incorrect dialogElSelector makes constructor throw.');

    assert.throws(function () {
        var incorrectSelector = new SW.DeviceStudio.CreatePollerWizard.FixedValueDialog({
            dialogElSelector: '#qunit-fixture',
            propertyElSelector: '#xxxx'
        });
    }, 'Incorrect propertyElSelector makes constructor throw.');

});

// The property name is needed for datasource identification and it might
// be also shown to user. Dialog shouldn't open without it.
test('Dialog cannot be opened without the propertyName argument.', function (assert) {
    var f = this.fvd;
    assert.throws(function () {
        f.open();
    }, 'Exception was thrown.');
    delete f;
});

// User needs to know what property is he working with -> Dialog needs to know it.
// This property can be different every time we open the dialog -> It should be set before 'open'
// Table is optional.
test('Dialog refreshes the property name every time it\'s being opened.', function (assert) {

    assert.equal(this.fvd.property, '', 'Property name is empty by default');

    var dummyName = 'dummyName';
    this.fvd.open(dummyName);
    assert.equal(this.fvd.property, dummyName, 'Property name is changed after after opening.');
    this.fvd.close();

    var secondDummyName = 'secondDummyName';
    this.fvd.open(secondDummyName);
    assert.equal(this.fvd.property, secondDummyName, 'Property name is changed every time dialog is opened.');
    this.fvd.close();

});

// I don't want to test the DOM creation and manipulation here, so 
// I will just test if the custom callback is called = dialog was opened/closed.
asyncTest('Dialog open/close callbacks being called', function (assert) {

    QUnit.expect(2);

    var f = new SW.DeviceStudio.CreatePollerWizard.FixedValueDialog({
        dialogElSelector: '#qunit-fixture',
        propertyNameElSelector: '#qunit-fixture',
        inputElSelector: '#qunit-fixture',
        onOpen: openCallback,
        onClose: closeCallback
    });

    function openCallback() {
        assert.ok(true, 'Open callback was called');
        f.close();
    }

    function closeCallback() {
        assert.ok(true, 'Close callback was called.');
        QUnit.start();
    }

    f.open('dummyName');

});

// Dialog let's user enter a value -> needs validation.
test('Validation works', function(assert) {
    assert.ok(this.fvd.validate('Dummy'), 'Dummy [OK]');
    assert.ok(this.fvd.validate('Dum my'), 'Dum my [OK]');
    assert.ok(this.fvd.validate('  abc   '), '   abc   [OK]');
    assert.ok(this.fvd.validate('abc.abc'), 'abc.abc [OK]');
    assert.ok(this.fvd.validate('abc-abc'), 'abc-abc [OK]');
    assert.ok(this.fvd.validate('abc_-_abc'), 'abc_-_abc [OK]');
    assert.ok(this.fvd.validate('_abc_abc_'), '_abc_abc_ [OK]');
    assert.ok(this.fvd.validate('123'), '123 [OK]');
    // You shall not pass!:
    assert.equal(this.fvd.validate("Dummy'Text"), false, "Dummy'Text [INVALID]");
    assert.equal(this.fvd.validate('Dummy"Text'), false, 'Dummy"Text [INVALID]');
    assert.equal(this.fvd.validate('Dummy<Script>'), false, 'Dummy<Script> [INVALID]');
});