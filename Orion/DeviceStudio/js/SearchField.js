﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.ns('Ext.ux.form');

var filterText = ''; //global search value

var filterPropertyParam = '';
var filterValueParam = '';

Ext.ux.form.SearchField = Ext.extend(Ext.form.TwinTriggerField, {
    initComponent: function () {
        Ext.ux.form.SearchField.superclass.initComponent.call(this);
        this.triggerConfig = {
            tag: 'span', cls: 'x-form-twin-triggers', cn: [
            { tag: "img", src: "/Orion/images/clear_button.gif", cls: "x-form-trigger " + this.trigger1Class },
            { tag: "img", src: "/Orion/images/search_button.gif", cls: "x-form-trigger " + this.trigger2Class }
            ]
        };

        this.addEvents('searchStarted');

        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        // set search field text styles
        this.on('focus', function () {
            this.el.applyStyles('font-style:normal;');
        }, this);

        // reset search field text style
        this.on('blur', function () {
            if (Ext.isEmpty(this.el.dom.value) || this.el.dom.value == this.emptyText) {
                this.el.applyStyles('font-style:italic;');
            }
        }, this);
    },


    id: 'searchfield',
    validationEvent: false,
    validateOnBlur: false,

    trigger1Class: 'x-form-clear-trigger',
    trigger2Class: 'x-form-search-trigger',

    // default search field text style
    style: 'font-style: italic',
    emptyText: '@{R=Core.Strings;K=WEBJS_TM0_53;E=js}',

    hideTrigger1: true,
    width: 180,
    hasSearch: false,

    // this is the clear (X) icon button in the search field
    onTrigger1Click: function () {
        if (this.hasSearch) {
            this.el.dom.value = '';
            filterText = this.getRawValue();
            this.store.proxy.conn.jsonData = { property: filterPropertyParam, value: filterValueParam, search: '' };
            var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
            this.store.load({ params: o });

            this.triggers[0].hide();
            this.hasSearch = false;
            this.fireEvent('searchStarted', filterText);
        }
    },

    //  this is the search icon button in the search field
    onTrigger2Click: function () {

        //set the global variable for the SQL query and regex highlighting
        filterText = this.getRawValue();

        if (filterText.length < 1) {
            this.onTrigger1Click();
            return;
        }

        // convert search string to SQL format (i.e. replace * with %)
        var tmpfilterText = "";
        tmpfilterText = filterText.replace(/\*/g, "%");

        if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
            tmpfilterText = "";
        }

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        // provide search string for Id param when loading the page
        this.store.proxy.conn.jsonData = { property: filterPropertyParam, value: filterValueParam, search: tmpfilterText };


        var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
        this.store.load({ params: o });

        this.hasSearch = true;
        this.triggers[0].show();

        this.fireEvent('searchStarted', filterText);
    }
});