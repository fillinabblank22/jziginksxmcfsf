﻿// Making Object.keys(obj) available for older browsers. Sources:
// http://tokenposts.blogspot.com.au/2012/04/javascript-objectkeys-browser.html
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys?redirectlocale=en-US&redirectslug=JavaScript%2FReference%2FGlobal_Objects%2FObject%2Fkeys
if (!Object.keys) {
  Object.keys = function (o) {
    if (o !== Object(o)) {
      throw new TypeError('Object.keys called on a non-object');
    }
    var k = [], p;
    for (p in o) {
      if (Object.prototype.hasOwnProperty.call(o, p)) {
        k.push(p);
      }
    }
    return k;
  };
}

// cyaable jQuery plugin
// creates the cyan tables that started appearing in NPM:Titan
// created by: petr.damborsky@solarwinds.com
; (function ($, window, undefined) {
  $.fn.cyaable = function (settings) {

    // Create defaults, extending them with any settings that were provided
    var options = $.extend({
      'data': {},
      'cssClass': '',
      'limit': 5,
      'emptyText': '[empty cell]',
      'headerInfo': '(first 5 rows)'
    }, settings);

    var CSS_PLACEHOLDER_CLASS = 'sw_cyaable_placeholder';
    var CSS_TABLE_CLASS = 'sw_cyaable';
    var CSS_HEADERINFO_CLASS = 'sw_cyaable_headerInfo';
    var CSS_ZEBRASTRIPES_CLASS = 'sw_cyaable_zebra';
    var CSS_EMPTY_CLASS = 'sw_cyaable_empty';
    var CSS_CELLWRAPPER_CLASS = 'sw_cyaable_cellWrapper';

    function preparePlaceholder(placeholder) {
      if (!placeholder.hasClass(CSS_PLACEHOLDER_CLASS)) {
        placeholder.addClass(CSS_PLACEHOLDER_CLASS);
      }
      placeholder.html('');
    }

    function createTable(options) {
      var table = $('<table>');
      table.addClass(CSS_TABLE_CLASS);
      if (options.cssClass) {
        table.addClass(options.cssClass);
      }
      return table;
    }

    function createHead(options) {
      var tHead = $('<thead>');
      var tRow = $('<tr>').appendTo(tHead);
      var tCell, headerInfo;
      for (var column in options.data) {
        if (Object.prototype.hasOwnProperty.call(options.data, column)) {
          tCell = $('<th>');
          tCell.text(column);
          headerInfo = $('<span>');
          headerInfo.html(options.headerInfo);
          headerInfo.addClass(CSS_HEADERINFO_CLASS);
          headerInfo.appendTo(tCell);
          tCell.appendTo(tRow);
        }
      }
      return tHead;
    }

    function createBody(options) {
      var tBody = $('<tbody>');
      var tRow, tCell, value, valueWrapper;
      for (var i = 0; i < options.limit; i++) {
        tRow = $('<tr>');
        if (i % 2 !== 0) {
          tRow.addClass(CSS_ZEBRASTRIPES_CLASS);
        }
        for (var column in options.data) {
          if (Object.prototype.hasOwnProperty.call(options.data, column)) {
            tCell = $('<td>');
            // This is needed for overflow in table cell
            valueWrapper = $('<div>').addClass(CSS_CELLWRAPPER_CLASS); 
            if (options.data[column][i] || options.data[column][i] === 0) {
              value = options.data[column][i];
            } else {
              value = options.emptyText;
              tCell.addClass(CSS_EMPTY_CLASS);
            }
            valueWrapper.html(value);
            // Parts of long values are now hidden because of css overflow.
            // Adding them as title will let user read them to end.
            valueWrapper.attr('title', value);
            valueWrapper.appendTo(tCell);
            tCell.appendTo(tRow);
          }
        }
        tRow.appendTo(tBody);
      }
      return tBody;
    }

    function make(options) {
      var cyaable = createTable(options);
      createHead(options).appendTo(cyaable);
      createBody(options).appendTo(cyaable);
      return cyaable;
    }

    return this.each(function () {

      var self = $(this);

      preparePlaceholder(self, options);
      self.append(make(options));

    });

  };
})(jQuery, window);