﻿<%@ WebService Language="C#" Class="Wizard" %>

using System;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Services;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SolarWinds.DeviceStudio.BusinessLayer.Client;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.DeviceStudio.Common.Helpers;
using SolarWinds.DeviceStudio.Common.Models;
using SolarWinds.DeviceStudio.Framework;
using SolarWinds.DeviceStudio.Framework.Polling;
using SolarWinds.DeviceStudio.Framework.Polling.Create.Snmp;
using SolarWinds.DeviceStudio.Framework.PollingMethods.Snmp.Oid;
using SolarWinds.DeviceStudio.Framework.Polling.Create.Snmp.Operations;
using SolarWinds.DeviceStudio.Framework.Polling.Output;
using DSModel=SolarWinds.DeviceStudio.Common.Models;
using SolarWinds.DeviceStudio.Web.Wizard;
using SolarWinds.DeviceStudio.Framework.Polling.Create;
using SolarWinds.DeviceStudio.Framework.Polling.Transform;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

[Serializable]
public enum messageType
{
    None,
    WarningName,
    WarningOID,
    WarningSNMP,
    ErrorName,
    ErrorOID,
    ErrorSNMP,
}

public class OIDobject
{
    public bool isManual;
    public bool isTabular;
    public string name;
    public string oid;
    public string tableName;
    public int snmpType;
}

[Serializable]
public class ManualOIDResult
{
    public string Message { get; set; }
    public messageType MessageType { get; set; }
    public string OriginalName { get; set; }
    public List<DSModel.SnmpResponse> Response { get; set; }
}

// This service should handle only data related to wizard ('create device kit' atm)
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class Wizard : WebService
{
    private static readonly Log Log = new Log();

    public DSModel.DataSource DataSource
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] as DSModel.DataSource;
        }
    }

    public DSWizardDataStore DataStore
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] as DSWizardDataStore;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] = value;
        }
    }

    private DSModel.ItemIdentifier LatestItemIdentifier
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_LatestItemIdentifier"]] as DSModel.ItemIdentifier;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_LatestItemIdentifier"]] = value;
        }
    }

    [WebMethod]
    public List<string> GetVendors()
    {
		try
		{
			return GetVendorsInternal();
		}
		catch (Exception ex)
		{
			Log.Error($"{nameof(GetVendors)} service call failed.", ex);
			throw;
		}
	}

    [WebMethod]
    public List<DSModel.SnmpResponse> TestOID(int nodeID, string oid, int pollingType)
    {
		try
		{
			return TestOidInternal(nodeID, oid, pollingType);
		}
		catch (Exception ex)
		{
			Log.Error($"{nameof(TestOID)} service call failed.", ex);
			throw;
		}
	}

    [WebMethod(EnableSession = true)]
    public ManualOIDResult TestManualOID(int nodeID, string name, string oid, int pollingType)
    {
		try
		{
			return TestManualOidInternal(nodeID, name, oid, pollingType);
		}
		catch (Exception ex)
		{
			Log.Error($"{nameof(TestManualOID)} service call failed.", ex);
			throw;
		}
	}

    [WebMethod(EnableSession = true)]
    public void SaveToPoller(OIDobject[] oidArray)
    {
		try
		{
			SaveToPollerInternal(oidArray);
		}
		catch (Exception ex)
		{
			Log.Error($"{nameof(SaveToPoller)} service call failed.", ex);
			throw;
		}
	}

    [WebMethod]
    public string LoadNode(int nodeId)
    {
		try
		{
			return LoadNodeInternal(nodeId);
		}
		catch (Exception ex)
		{
			Log.Error($"{nameof(LoadNode)} service call failed.", ex);
			throw;
		}
	}

    private List<string> GetVendorsInternal()
    {
        AuthorizationChecker.AllowAdmin();
        // call swis to check current wendors in db
        System.Data.DataTable data;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            data = swis.Query(@"SELECT distinct Vendor FROM Orion.DeviceStudio.Pollers");
        }

        List<string> list = new List<string>();

        foreach (System.Data.DataRow row in data.Rows)
        {
            list.Add(HttpUtility.HtmlEncode(Convert.ToString(row["Vendor"])));
        }

        return list;
    }

    private List<DSModel.SnmpResponse> TestOidInternal(int nodeID, string oid, int pollingType)
    {
        AuthorizationChecker.AllowAdmin();

        var result = new List<DSModel.SnmpResponse>();

        if ((!Regex.IsMatch(oid, @"(\d{1,9})(\.\d{1,9})*")) || (oid.Trim() == "1"))
            throw new ArgumentException("Wrong OID");

        var node = NodeDAL.GetNode(nodeID);
        if (node == null)
            throw new Exception("Node not found");

		using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(node.EngineID))
		{
			switch(pollingType)
			{
				case 0:
					var responseGet = proxy.Api.SnmpGet(node, oid);
					if (responseGet.OID != oid)
						goto default;

					result.Add(responseGet);
					break;
				case 1:
					var responseGetNext = proxy.Api.SnmpGetNext(node, oid);
					if (!responseGetNext.OID.StartsWith(oid + "."))
						goto default;

					result.Add(responseGetNext);
					break;
				case 2:
					var responseGetSubtree = proxy.Api.SnmpGetSubtreeCount(node, oid, 20);
					if (responseGetSubtree.Count == 0)
						goto default;

					result.AddRange(responseGetSubtree);
					break;
				default:
					result.Add(new DSModel.SnmpResponse()
					{
						OID = oid,
						HexValue = null,
						Value = null,
						Type = "UNSUPPORTED"
					});
					break;
			}
			return result;
		}
    }

    private ManualOIDResult TestManualOidInternal(int nodeID, string name, string oid, int pollingType)
    {
        AuthorizationChecker.AllowAdmin();

        if (DataStore.poller == null)
            return new ManualOIDResult { Message = Resources.DeviceStudioWebContent.WEBDATA_JF0_01, MessageType = messageType.ErrorSNMP };

        var pollingConfig = DataStore.poller.Configs.GetConfig<PollingConfig>(PollerConfigNames.PollingConfig);
        if (pollingConfig == null)
            return new ManualOIDResult { Message = Resources.DeviceStudioWebContent.WEBDATA_JF0_02, MessageType = messageType.ErrorSNMP };

        var transformConfig = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;

        var createConfig = pollingConfig.DataSourceCreateConfig as SnmpDataSourceCreateConfig;
        if (createConfig == null)
        {
            pollingConfig.DataSourceCreateConfig = new SnmpDataSourceCreateConfig();
            createConfig = pollingConfig.DataSourceCreateConfig as SnmpDataSourceCreateConfig;
        }

        switch(pollingType)
        {
            case 0:
                var resultGet = createConfig.Operations.OfType<SnmpGet>().FirstOrDefault(f => f.Oid.OID == oid);
                if (resultGet != null)
                {
                    return new ManualOIDResult
                    {
                        Response = TestOID(nodeID, oid, pollingType),
                        OriginalName = resultGet.Output.Property,
                        Message = string.Format(Resources.DeviceStudioWebContent.WEBDATA_JF0_03, resultGet.Output.Property),
                        MessageType = messageType.WarningOID
                    };
                }

                resultGet = createConfig.Operations.OfType<SnmpGet>().FirstOrDefault(f => f.Output.Property == name);
                if (resultGet != null)
                    return new ManualOIDResult { Message = Resources.DeviceStudioWebContent.WEBDATA_JF0_04, MessageType = messageType.ErrorName };

                if ((transformConfig != null)&&(transformConfig.ContainsProperty(new ItemIdentifier(null, name))))
                    return new ManualOIDResult { Message = Resources.DeviceStudioWebContent.WEBDATA_JF0_04, MessageType = messageType.ErrorName };

                try
                {
                    return new ManualOIDResult { Response = TestOID(nodeID, oid, pollingType) };
                }
                catch (Exception ex)
                {
                    if (!ex.Message.StartsWith("Error 31002 -"))
                        throw;

                    return new ManualOIDResult
                    {
                        Response = new List<SnmpResponse> { new DSModel.SnmpResponse
                                                                                    {
                                                                                        OID = oid,
                                                                                        HexValue = null,
                                                                                        Value = null,
                                                                                        Type = "UNSUPPORTED"
                                                                                    }
                                                            }
                    };
                }
            case 1:
                var resultGetNext = createConfig.Operations.OfType<SnmpGetNext>().FirstOrDefault(f => f.Oid.OID == oid);
                if (resultGetNext != null)
                {
                    return new ManualOIDResult
                    {
                        Response = TestOID(nodeID, oid, pollingType),
                        OriginalName = resultGetNext.Output.Property,
                        Message = string.Format(Resources.DeviceStudioWebContent.WEBDATA_JF0_03, resultGetNext.Output.Property),
                        MessageType = messageType.WarningOID
                    };
                }

                resultGetNext = createConfig.Operations.OfType<SnmpGetNext>().FirstOrDefault(f => f.Output.Property == name);
                if (resultGetNext != null)
                    return new ManualOIDResult { Message = Resources.DeviceStudioWebContent.WEBDATA_JF0_04, MessageType = messageType.ErrorName };

                if ((transformConfig != null)&&(transformConfig.ContainsProperty(new ItemIdentifier(null, name))))
                    return new ManualOIDResult { Message = Resources.DeviceStudioWebContent.WEBDATA_JF0_04, MessageType = messageType.ErrorName };

                return new ManualOIDResult { Response = TestOID(nodeID, oid, pollingType) };
            case 2:
                var tables = createConfig.Operations.OfType<SnmpGetTable>().ToList();
                foreach(var table in tables)
                {
                    var column = table.Columns.FirstOrDefault(f => f.OID == oid);
                    if(column!=null)
                    {
                        return new ManualOIDResult
                        {
                            Response = TestOID(nodeID, oid, pollingType),
                            OriginalName = column.Name,
                            Message = string.Format(Resources.DeviceStudioWebContent.WEBDATA_JF0_05, table.Table, column.Name),
                            MessageType = messageType.WarningOID
                        };
                    }

                    column = table.Columns.FirstOrDefault(f => f.Name == name);
                    if ((column != null) && (column.OID.Substring(0, column.OID.LastIndexOf('.') + 1) == oid.Substring(0, oid.LastIndexOf('.') + 1)))
                        return new ManualOIDResult { Message = string.Format(Resources.DeviceStudioWebContent.WEBDATA_JF0_06, table.Table), MessageType = messageType.ErrorName };

                    if ((transformConfig != null) && (transformConfig.ContainsProperty(new ItemIdentifier(table.Table, name))))
                        return new ManualOIDResult { Message = string.Format(Resources.DeviceStudioWebContent.WEBDATA_JF0_06, table.Table), MessageType = messageType.ErrorName };

                }

                return new ManualOIDResult { Response = TestOID(nodeID, oid, pollingType) };
            default:
                return new ManualOIDResult { Message = Resources.DeviceStudioWebContent.WEBDATA_JF0_07, MessageType = messageType.ErrorSNMP };
        }
    }

    private void SaveToPollerInternal(OIDobject[] oidArray)
    {
        AuthorizationChecker.AllowAdmin();

        if (oidArray.Length != 1)
            return;

        OIDobject oidObject = oidArray[0];

        if (DataStore.poller != null)
        {
            var pollingConfig = DataStore.poller.Configs.GetConfig<PollingConfig>(PollerConfigNames.PollingConfig);
            if (pollingConfig != null)
            {
                var createConfig = pollingConfig.DataSourceCreateConfig as SnmpDataSourceCreateConfig;

                if (createConfig == null)
                {
                    pollingConfig.DataSourceCreateConfig = new SnmpDataSourceCreateConfig();
                    createConfig = pollingConfig.DataSourceCreateConfig as SnmpDataSourceCreateConfig;
                }

                PrepareManualEntry(oidObject, createConfig);

                int nodeID = int.Parse(DataStore.pollerNetObject);
                AddOrMergeOperation(createConfig, oidObject);

                DataStore.poller.Configs.AddConfig<Config>(PollerConfigNames.PollingConfig, pollingConfig);

                GetData(nodeID, pollingConfig, oidObject);
            }
        }
    }

    private string LoadNodeInternal(int nodeId)
    {
        AuthorizationChecker.AllowAdmin();

        DataTable data;
        string html = "";

        using (var swis = InformationServiceProxy.CreateV3())
        {
            data = swis.Query(@"SELECT Caption,Status FROM Orion.Nodes where NodeID = " + nodeId.ToString());
        }

        if (data.Rows.Count > 0)
        {
            html = string.Format("<img src=\"/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={0}&size=small\">&nbsp;{1}&nbsp;", data.Rows[0]["Status"],
                WebSecurityHelper.HtmlEncode(Convert.ToString(data.Rows[0]["Caption"])));

        }
        return html;
    }

    private void PrepareManualEntry(OIDobject oidObject, SnmpDataSourceCreateConfig createConfig)
    {
        if (!oidObject.isManual)
            return;

        oidObject.isTabular = (oidObject.snmpType == 2);

        if (oidObject.isTabular)
        {
            string tableOid = oidObject.oid.Substring(0, oidObject.oid.LastIndexOf('.') + 1);

            var table = createConfig.Operations.OfType<SnmpGetTable>().FirstOrDefault(t => t.Columns[0].OID.Substring(0, t.Columns[0].OID.LastIndexOf('.') + 1) == tableOid);
            if (table == null)
            {
                var ds = DataStore;
                int num = ++ds.tableNameSuffixNumber;
                DataStore = ds;
                oidObject.tableName = string.Format("Table{0}", num);
            }
            else
                oidObject.tableName = table.Table;

        }
    }

    private void AddOrMergeOperation(SnmpDataSourceCreateConfig createConfig, OIDobject oidObject)
    {
        LatestItemIdentifier = new DSModel.ItemIdentifier(oidObject.tableName, oidObject.name);
        if (createConfig.ContainsProperty(LatestItemIdentifier))
            return;

        if (!oidObject.isManual)
        {
            if (oidObject.isTabular)
                oidObject.snmpType = 2;
            else
                oidObject.snmpType = 1;
        }

        switch (oidObject.snmpType)
        {
            case 0:
                SnmpGet opGet = new SnmpGet(oidObject.name, oidObject.oid);
                createConfig.Operations.Add(opGet);
                break;
            case 1:
                SnmpGetNext opGetNext = new SnmpGetNext(oidObject.name, oidObject.oid);
                createConfig.Operations.Add(opGetNext);
                break;
            case 2:
                SnmpGetTable opGetTable = createConfig.Operations.OfType<SnmpGetTable>().FirstOrDefault(f => f.Table == oidObject.tableName);
                if (opGetTable != null)
                {
                    opGetTable.Columns.Add(new Oid(oidObject.name, oidObject.oid));
                }
                else
                {
                    opGetTable = new SnmpGetTable(oidObject.tableName, null, new List<Oid> { new Oid(oidObject.name, oidObject.oid) });
                    createConfig.Operations.Add(opGetTable);
                }
                break;
            default:
                throw new Exception(string.Format("Unknown SNMP operation {0}", oidObject.snmpType));
        }
    }

    private void GetData(int nodeID, PollingConfig pollingConfig, OIDobject oidObject)
    {
        var id = new ItemIdentifier(oidObject.tableName, oidObject.name);
        var node = NodeDAL.GetNode(nodeID);

        DataSource ds;
        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(node.EngineID))
        {
            ds = proxy.Api.DoPoll(node, PollingMethods.Snmp, SerializationHelper.ToXML<PollingConfig>(pollingConfig), true, 5, false);
        }

        var localItems = DataStore.LocalDataSource.GetItems().ToList();
        if (!localItems.Contains(id))
            localItems.Add(id);

        foreach (var item in ds.GetItems())
        {
            // remove all unreleated items from polled DS
            if (!localItems.Contains(item))
                ds.RemoveItem(item);
        }

        // refresh local datasource
        DataStore.LocalDataSource = ds;
    }

    // Creates the new local data source. If selected property has no mapping yet, it will be empty.
    // Otherwise the filtered output of global data source will be used.
    // It also creates pollingConfig backup which we need when editing datasource properties (formulas).
    // Edit causes immediate changes in datasource and if user decides to cancel, we need to restore initial state.
    [WebMethod(EnableSession = true)]
    public bool SetLocalDatasource(ItemIdentifier iid)
    {
        AuthorizationChecker.AllowAdmin();

        var pollingConfig = DataStore.GetPollingConfigObject();
        if (pollingConfig != null)
        {
            // backup polling config so we can restore it when user changes his mind after editing something
            DataStore.CreatePollingConfigBackup();

            var op = DataStore.FindOutputPropertyByItemIdentifier(iid);

            DataStore.LocalDataSource = new DataSource(DataSource);

            if (string.IsNullOrEmpty(op.Mapping))
            {
                pollingConfig.FilterDataSource(DataStore.LocalDataSource);
                LatestItemIdentifier = null;
            }
            else
            {
                var dsId = DataStore.FindOutputMapping(iid);
                pollingConfig.FilterDataSource(DataStore.LocalDataSource, dsId);

                // remove indexes
                var createConfig = pollingConfig.DataSourceCreateConfig as DataSourceCreateConfig;
                if (createConfig != null)
                {
                    createConfig.GetIndexes().ToList().ForEach(i => DataStore.LocalDataSource.RemoveItem(i));
                }

                LatestItemIdentifier = dsId;
            }

            return true;
        }
        return false;
    }

    [WebMethod(EnableSession=true)]
    public void SetShowWhatsNewInPollersDialog(bool show)
    {
        AuthorizationChecker.AllowAdmin();

        WebUserSettingsDAL.Set(HttpContext.Current.Profile.UserName, "DontShowWhatIsNewWithPollersDialog", Convert.ToString(!show));
    }

    // Ideally call this every time user selects a property in list properties.
    // save the selection on server
    // save the returned values on client to access from other componets
    [WebMethod(EnableSession = true)]
    public object OutputPropertySelected(string itemIdentifier)
    {
        AuthorizationChecker.AllowAdmin();

        ItemIdentifier iid = new ItemIdentifier(itemIdentifier);
        SetLocalDatasource(iid);
        SetActiveOutputProperty(iid);
        OutputProperty op = DataStore.SelectedOutputProperty;
        return new
        {
            name = op.Name,
            displayName = op.DisplayName,
            table = iid.Table,
            description = op.Description,
            mapping = op.Mapping,
            type = op.Type.ToString(),
            units = op.Units,
            required = !op.Optional
        };
    }

    public void SetActiveOutputProperty(ItemIdentifier iid)
    {
        DataStore.SelectedOutputProperty = DataStore.FindOutputPropertyByItemIdentifier(iid);
    }

}
