﻿<%@ WebService Language="C#" Class="FormulaEditor" %>

using System;
using System.Linq;
using System.Web.Services;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.DeviceStudio.Common.Models;
using SolarWinds.DeviceStudio.Framework.Polling;
using SolarWinds.DeviceStudio.Common.Helpers;
using SolarWinds.DeviceStudio.Framework.Polling.Create;
using SolarWinds.DeviceStudio.Framework.Polling.Transform;
using SolarWinds.DeviceStudio.Framework.Polling.Transform.Operations;
using SolarWinds.DeviceStudio.Framework;
using System.Configuration;
using SolarWinds.DeviceStudio.BusinessLayer.Client;
using SolarWinds.DeviceStudio.Web.Wizard;
using SolarWinds.Logging;
using SolarWinds.DeviceStudio.Web.Helpers;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class FormulaEditor : System.Web.Services.WebService
{
    private static readonly Log logger = new Log();
    private static void ProcessException(Exception ex)
    {
        logger.Error("Formula Editor exception: ", ex);
    }

    private DataSource LocalDataSourceBackup
    {
        get { return Session["LocalDataSourceBackup"] as DataSource; }
        set { Session["LocalDataSourceBackup"] = value; }
    }

    private DSWizardDataStore DataStore
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] as DSWizardDataStore;
        }
    }

    private Poller Poller
    {
        get
        {
            return DataStore.poller;
        }
    }

    private PollingConfig PollingConfig
    {
        get
        {
            return Poller.Configs.GetConfig<PollingConfig>(PollerConfigNames.PollingConfig);
        }
        set
        {
            Poller.Configs.AddConfig<Config>(PollerConfigNames.PollingConfig, value);
        }
    }

    private DataSource DataSource
    {
        get { return Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] as DataSource; }
        set { Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] = value; }
    }

    private ItemIdentifier LatestItemIdentifier
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_LatestItemIdentifier"]] as ItemIdentifier;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_LatestItemIdentifier"]] = value;
        }
    }

    [WebMethod(EnableSession = true)]
    public List<string> GetTables()
    {
        AuthorizationChecker.AllowAdmin();

        return DataStore.LocalDataSource.GetTableNames().ToList();
    }

    [WebMethod(EnableSession = true)]
    public List<string> GetVariables(string output)
    {
        AuthorizationChecker.AllowAdmin();

        // create backup of local DS
        LocalDataSourceBackup = new DataSource(DataStore.LocalDataSource);

        var selectedItem = DataStore.LocalDataSource.GetItems().FirstOrDefault(i => i.Property.Equals(output));
        if (selectedItem != null)
        {
            // remove selected item and all other items using this item from datasource
            PollingConfig.RemovePropertyFromDataSource(DataStore.LocalDataSource, selectedItem);
        }

        // returns all properties
        return DataStore.LocalDataSource.GetItems().ToList().Select(i => i.IsProperty ? i.Property : i.ToString()).ToList();
    }

    [WebMethod(EnableSession = true)]
    public void RestoreDataSource()
    {
        AuthorizationChecker.AllowAdmin();

        // reload datasource from backup
        if (LocalDataSourceBackup != null)
        {
            DataStore.LocalDataSource = LocalDataSourceBackup;
            LocalDataSourceBackup = null;
        }
    }

    private static string DecodeJSON(string text)
    {
        return text.Replace("||", "'");
    }

    public class TestResult
    {
        public bool Passed { get; set; }
        public string Message { get; set; }
        public List<string> Values { get; set; }

        public TestResult(bool passed, string message)
        {
            Passed = passed;
            Message = message;
        }

        public TestResult(bool passed, string message, List<string> values)
        {
            Passed = passed;
            Message = message;
            Values = values;
        }
    }

    [WebMethod(EnableSession = true)]
    public void AddExpression(string property, string expression)
    {
        AuthorizationChecker.AllowAdmin();

        expression = DecodeJSON(expression);

        if (PollingConfig != null)
        {
            var pollingConfig = PollingConfig;
            var itemId = new ItemIdentifier(null, property);

            if (pollingConfig.DataSourceTransformConfig == null)
                pollingConfig.DataSourceTransformConfig = new DataSourceTransformConfig() { Operations = new List<IDataSourceTransformOperation>() };

            var transforms = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
            var id = new ItemIdentifier(property);
            var operation = new NewExpression(id, expression);

            using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
            {
                DataStore.LocalDataSource = proxy.Api.DoTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
            }

            // are we editing existing transformation?
            if (transforms.ContainsProperty(id))
            {
                var existingOperation = transforms.Operations.OfType<NewExpression>().FirstOrDefault(o => o.Output.Equals(id));
                if (existingOperation != null)
                {
                    // edit existing expression
                    existingOperation.Expression = expression;
                }
            }
            else
            {
                transforms.Operations.Add(operation);
            }

            PollingConfig = pollingConfig;
            LatestItemIdentifier = itemId;
        }
    }

    [WebMethod(EnableSession = true)]
    public TestResult TestExpression(string property, string expression)
    {
        AuthorizationChecker.AllowAdmin();

        expression = DecodeJSON(expression);

        var itemId = new ItemIdentifier(null, property);
        var pollingConfig = DataStore.GetPollingConfigObject();
        var transformConfig = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
        var createConfig = pollingConfig.DataSourceCreateConfig as DataSourceCreateConfig;
        string resultValue;

        var operation = new NewExpression(itemId, expression);
        TryTransformResult result;

        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
        {
            result = proxy.Api.TryTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
        }

        if (result.Error)
        {
            return new TestResult(false, result.ErrorMessage);
        }
        else
        {
            resultValue = FormulaHelpers.ValueToString(result.DataSource.Properties[property], "null");
            return new TestResult(true, resultValue, new List<string> { resultValue });
        }
    }

    public TestResult TestExpressionBeta(string property, string expression, DataSource ds)
    {
        expression = DecodeJSON(expression);

        var operation = new NewExpression(new ItemIdentifier(property), expression);
        TryTransformResult result;

        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
        {
            result = proxy.Api.TryTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), ds);
        }

        return result.Error ? new TestResult(false, result.ErrorMessage) : new TestResult(true, result.DataSource.Properties[property].ToString());
    }

    [WebMethod(EnableSession = true)]
    public void AddExpressionTable(string table, string column, string expression)
    {
        AuthorizationChecker.AllowAdmin();

        expression = DecodeJSON(expression);

        if (PollingConfig != null)
        {
            var pollingConfig = PollingConfig;

            if (pollingConfig.DataSourceTransformConfig == null)
                pollingConfig.DataSourceTransformConfig = new DataSourceTransformConfig() { Operations = new List<IDataSourceTransformOperation>() };

            var transforms = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
            var id = new ItemIdentifier(table, column);
            var operation = new NewExpression(id, expression);

            using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
            {
                DataStore.LocalDataSource = proxy.Api.DoTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
            }

            // are we editing existing transformation?
            if (transforms.ContainsProperty(id))
            {
                var existingOperation = transforms.Operations.OfType<NewExpression>().FirstOrDefault(o => o.Output.Equals(id));
                if (existingOperation != null)
                {
                    // edit existing expression
                    existingOperation.Expression = expression;
                }
            }
            else
            {
                transforms.Operations.Add(operation);
            }

            PollingConfig = pollingConfig;
            LatestItemIdentifier = new ItemIdentifier(table, column);
        }
    }

    [WebMethod(EnableSession = true)]
    public TestResult TestExpressionTable(string table, string column, string expression)
    {
        AuthorizationChecker.AllowAdmin();

        expression = DecodeJSON(expression);

        var itemId = new ItemIdentifier(table, column);
        var pollingConfig = DataStore.GetPollingConfigObject();
        var transformConfig = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
        var createConfig = pollingConfig.DataSourceCreateConfig as DataSourceCreateConfig;
        var operation = new NewExpression(itemId, expression);
        TryTransformResult result;

        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
        {
            result = proxy.Api.TryTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
        }

        if (result.Error)
        {
            return new TestResult(false, result.ErrorMessage);
        }
        else
        {
            var results = new List<string>();
            var t = result.DataSource.GetTable(table);
            t.GetColumn(column).ForEach(v => results.Add(FormulaHelpers.ValueToString(v, "null")));
            return new TestResult(true, String.Join(", ", results), results);
        }
    }

    [WebMethod(EnableSession = true)]
    public TestResult TestExpressionTableBeta(string table, string column, string expression, DataSource ds)
    {
        AuthorizationChecker.AllowAdmin();

        expression = DecodeJSON(expression);

        var operation = new NewExpression(new ItemIdentifier(table, column), expression);
        TryTransformResult result;

        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
        {
            result = proxy.Api.TryTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), ds);
        }

        if (result.Error)
        {
            return new TestResult(false, result.ErrorMessage);
        }
        else
        {
            var results = new List<object>();
            var t = result.DataSource.GetTable(table);
            foreach (var row in t.Rows)
            {
                results.Add(row[column]);
            }

            return new TestResult(true, String.Join(", ", results));
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetValuesForVisualization(List<string> vars)
    {
        AuthorizationChecker.AllowAdmin();

        var rv = new Dictionary<string, object>();
        ItemIdentifier itemId;
        List<object> values;
        foreach (var v in vars)
        {
            itemId = new ItemIdentifier(v);
            values = DataStore.GetValueFromDataSource(itemId, 5);
            // skip items without values
            if (values.Count == 0)
                continue;

            rv.Add(v, values);
        }
        return rv;
    }

    public class KVP
    {
        public string Left { get; set; }
        public string Right { get; set; }
    }

    [WebMethod(EnableSession = true)]
    public void AddCondition(string property, List<KVP> conditions)
    {
        AuthorizationChecker.AllowAdmin();

        var conds = new List<KeyValuePair<string, string>>();
        conditions.ForEach(c => conds.Add(new KeyValuePair<string, string>(DecodeJSON(c.Left), DecodeJSON(c.Right))));

        if (PollingConfig != null)
        {
            var pollingConfig = PollingConfig;

            if (pollingConfig.DataSourceTransformConfig == null)
                pollingConfig.DataSourceTransformConfig = new DataSourceTransformConfig() { Operations = new List<IDataSourceTransformOperation>() };

            var transforms = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
            var operation = new NewCondition(new ItemIdentifier(property), conds);

            using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
            {
                DataStore.LocalDataSource = proxy.Api.DoTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
            }

            transforms.Operations.Add(operation);
            PollingConfig = pollingConfig;
            LatestItemIdentifier = new ItemIdentifier(null, property);
        }
    }

    [WebMethod(EnableSession = true)]
    public TestResult TestCondition(string property, List<KVP> conditions)
    {
        AuthorizationChecker.AllowAdmin();

        var conds = new List<KeyValuePair<string, string>>();
        conditions.ForEach(c => conds.Add(new KeyValuePair<string, string>(DecodeJSON(c.Left), DecodeJSON(c.Right))));

        var operation = new NewCondition(new ItemIdentifier(property), conds);
        TryTransformResult result;

        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
        {
            result = proxy.Api.TryTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
        }

        return result.Error ? new TestResult(false, result.ErrorMessage) : new TestResult(true, result.DataSource.Properties[property].ToString());
    }

    [WebMethod(EnableSession = true)]
    public void AddConditionTable(string table, string column, List<KVP> conditions)
    {
        AuthorizationChecker.AllowAdmin();

        var conds = new List<KeyValuePair<string, string>>();
        conditions.ForEach(c => conds.Add(new KeyValuePair<string, string>(DecodeJSON(c.Left), DecodeJSON(c.Right))));

        if (PollingConfig != null)
        {
            var pollingConfig = PollingConfig;

            if (pollingConfig.DataSourceTransformConfig == null)
                pollingConfig.DataSourceTransformConfig = new DataSourceTransformConfig() { Operations = new List<IDataSourceTransformOperation>() };

            var transforms = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
            var operation = new NewCondition(new ItemIdentifier(table, column), conds);

            using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
            {
                DataStore.LocalDataSource = proxy.Api.DoTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
            }

            transforms.Operations.Add(operation);
            PollingConfig = pollingConfig;
            LatestItemIdentifier = new ItemIdentifier(table, column);
        }
    }

    [WebMethod(EnableSession = true)]
    public TestResult TestConditionTable(string table, string column, List<KVP> conditions)
    {
        AuthorizationChecker.AllowAdmin();

        var conds = new List<KeyValuePair<string, string>>();
        conditions.ForEach(c => conds.Add(new KeyValuePair<string, string>(DecodeJSON(c.Left), DecodeJSON(c.Right))));

        var operation = new NewCondition(new ItemIdentifier(table, column), conds);
        TryTransformResult result;

        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
        {
            result = proxy.Api.TryTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
        }

        if (result.Error)
        {
            return new TestResult(false, result.ErrorMessage);
        }
        else
        {
            var results = new List<object>();
            var t = result.DataSource.GetTable(table);
            foreach (var row in t.Rows)
            {
                results.Add(row[column]);
            }

            return new TestResult(true, String.Join(", ", results));
        }
    }

    [WebMethod(EnableSession = true)]
    public string TestFormula(string formula, string[] variables, string[] values)
    {
        AuthorizationChecker.AllowAdmin();

        if (variables == null) throw new ArgumentNullException("variables");
        if (values == null) throw new ArgumentNullException("values");
        if (variables.Length != values.Length) throw new Exception("The amount of variables doesn't match the amount of values.");

        var ds = CreateDataSource(variables, values);

        // TODO: testing

        return "Tables: "
            + string.Join(", ", ds.Tables.Select(t => t.TableName))
            + ", Non-table properties: "
            + string.Join(", ", ds.Properties.Select(t => t.Key));
    }

    [WebMethod(EnableSession = true)]
    public TestResult TestFormulaBeta(string formula, string[] variables, string[] values, string table, string column)
    {
        AuthorizationChecker.AllowAdmin();

        if (variables == null) throw new ArgumentNullException("variables");
        if (values == null) throw new ArgumentNullException("values");
        if (variables.Length == 0 || values.Length == 0) throw new Exception("Missing values");
        if (variables.Length != values.Length) throw new Exception("The amount of variables doesn't match the amount of values.");

        DataSource ds = null;
        var properties = new Dictionary<string, object>();
        var tables = new List<Table>();

        // merge the input arrays to have a data structure that
        // is easier to manipulate(filter) here
        var items = new List<DataSourceRecordHelper>();
        for (var i = 0; i < variables.Length; i++)
        {
            var id = new ItemIdentifier(variables[i]);
            items.Add(new DataSourceRecordHelper { Table = id.Table, Property = id.Property, Value = values[i] });
        }

        foreach (var x in items.Where(i => String.IsNullOrEmpty(i.Table)))
        {
            object tmp = x.Value;
            int intValue;
            float floatValue;
            if (Int32.TryParse(tmp.ToString(), out intValue))
            {
                properties.Add(x.Property, intValue);
            }
            else if (float.TryParse(tmp.ToString(), out floatValue))
            {
                properties.Add(x.Property, floatValue);
            }
            else
            {
                properties.Add(x.Property, x.Value);
            }
        }

        if (String.IsNullOrEmpty(table))
        {
            ds = new DataSource(properties, tables);
            return TestExpressionBeta(column, formula, ds);
        }

        // prepare stuff needed for table creation
        Table tab;
        var tableItems = items.Where(x => !String.IsNullOrEmpty(x.Table));
        List<object> rowValues;
        int rowsCount = 0;
        if (tableItems != null)
        {
            tab = new Table(table);
            // create columns and find the amount of rows
            foreach (var item in tableItems)
            {
                tab.AddColumn(item.Property);
                var valueSplit = item.Value.Split('|');
                if (valueSplit.Length > rowsCount)
                {
                    rowsCount = valueSplit.Length;
                }
            }
            // fill rows with item data
            for (var i = 0; i < rowsCount; i++)
            {
                rowValues = new List<object>();
                foreach (var item in tableItems)
                {
                    var valueSplit = item.Value.Split('|');
                    if (valueSplit.Length - 1 < i)
                    {
                        rowValues.Add(null);
                    }
                    else
                    {
                        object tmp = valueSplit[i];
                        int intValue;
                        float floatValue;
                        if (Int32.TryParse(tmp.ToString(), out intValue))
                        {
                            rowValues.Add(intValue);
                        }
                        else if (float.TryParse(tmp.ToString(), out floatValue))
                        {
                            rowValues.Add(floatValue);
                        }
                        else
                        {
                            rowValues.Add(tmp);
                        }
                    }
                }
                tab.AddRow(rowValues.ToArray());
            }
            tables.Add(tab);
        }
        ds = new DataSource(properties, tables);
        return TestExpressionTableBeta(table, column, formula, ds);

    }

    [WebMethod(EnableSession = true)]
    public string[] GetCurrentValues(string[] variables)
    {
        AuthorizationChecker.AllowAdmin();

        if (variables == null) throw new ArgumentNullException("variables");

        var items = new List<DataSourceRecordHelper>();
        var rv = new string[variables.Length];
        string tmpTable, tmpProperty;
        string[] tmpVals;

        // loop throught variables and create helper collection
        for (var i = 0; i < variables.Length; i++)
        {
            tmpVals = variables[i].Split(new char[] { '.' });
            if (tmpVals.Count() == 1)
            {
                tmpTable = "";
                tmpProperty = tmpVals[0];
            }
            else
            {
                tmpTable = tmpVals[0];
                tmpProperty = tmpVals[1];
            }
            items.Add(new DataSourceRecordHelper { Table = tmpTable, Property = tmpProperty, Value = "N/A" });
        }

        // find the values for items in helper collection
        foreach (DataSourceRecordHelper item in items)
        {
            try
            {
                // if the item's table is null, search in properties
                if (string.IsNullOrEmpty(item.Table))
                {
                    var props = DataSource.Properties.Where(p => p.Key == item.Property);
                    if (props.Count() > 0)
                    {
                        var prop = props.First();
                        if (!prop.Equals(null))
                        {
                            item.Value = prop.Value.ToString();
                        }
                    }
                }
                // if the table is set, search for the value in that table
                else if (DataSource.Tables != null && DataSource.Tables.Count > 0)
                {
                    var tab = DataSource.Tables.FirstOrDefault(t => t.TableName == item.Table);
                    if (tab != null)
                    {
                        var row = tab.Rows.First();
                        var val = row[item.Property].ToString();
                        if (!string.IsNullOrEmpty(val))
                        {
                            item.Value = val;
                        }
                    }
                }
            }
            catch (NullReferenceException ne)
            {
                ProcessException(ne);
            }
        }

        return items.Select(i => i.Value).ToArray();
    }

    [WebMethod(EnableSession = true)]
    public string[] GetCurrentValuesBeta(string[] variables, string table)
    {
        AuthorizationChecker.AllowAdmin();

        try
        {
            if (variables == null)
            {
                throw new ArgumentNullException("variables");
            }
            string[] rv = new string[variables.Length];

            if (string.IsNullOrEmpty(table))
            {
                for (var i = 0; i < variables.Length; i++)
                {
                    if (DataSource.Properties.ContainsKey(variables[i]))
                    {
                        rv[i] = DataSource.Properties[variables[i]].ToString();
                        // WTF is this?! This is how you get item from dictionary?
                        // rv[i] = DataSource.Properties.Where(x => x.Key == variables[i]).Single().Value.ToString();
                    }
                    else
                    {
                        rv[i] = "unknown variable";
                    }
                }
            }
            else
            {
                if (DataSource.ContainsTable(table))
                {
                    Table t = DataSource.GetTable(table);

                    for (var i = 0; i < variables.Length; i++)
                    {
                        var id = new ItemIdentifier(variables[i]);
                        if (id.IsColumn && t.Columns.Contains(id.Property))
                        {
                            var values = t.GetColumn(id.Property).Select(v => v.ToString());
                            rv[i] = String.Join(" | ", values);
                        }
                        else if (id.IsProperty && DataSource.ContainsProperty(id.Property))
                        {
                            rv[i] = DataSource.GetProperty(id.Property).ToString();
                        }
                        else
                        {
                            rv[i] = "unknown variable";
                        }
                    }
                }
            }

            return rv;

        }
        catch (Exception ex)
        {
            ProcessException(ex);
            return new string[] { };
        }

    }

    private DataSource CreateDataSource(string[] variables, string[] values)
    {
        // merge the input arrays to have a data structure that
        // is easier to manipulate(filter) here
        var items = new List<DataSourceRecordHelper>();
        string tmpTable, tmpProperty;
        for (var i = 0; i < variables.Length; i++)
        {
            var tmpVals = variables[i].Split(new char[] { '.' });
            if (tmpVals.Count() == 1)
            {
                tmpTable = "";
                tmpProperty = tmpVals[0];
            }
            else
            {
                tmpTable = tmpVals[0];
                tmpProperty = tmpVals[1];
            }
            items.Add(new DataSourceRecordHelper { Table = tmpTable, Property = tmpProperty, Value = values[i] });
        }


        // extract the table names from input variables
        List<string> tabNames = (from i in items
                                 where i.Table != ""
                                 select i.Table).Distinct().ToList();

        // prepare the data structure for tables
        var tables = new List<Table>();

        // create new table for each tableName
        // create columns and add row with values
        Table tab;
        IEnumerable<DataSourceRecordHelper> filteredItems;
        List<Object> rowValues;
        foreach (string n in tabNames)
        {
            tab = new Table(n);
            filteredItems = items.Where(i => i.Table.Equals(n));
            rowValues = new List<Object>();
            foreach (var item in filteredItems)
            {
                tab.AddColumn(item.Property);
                rowValues.Add(item.Value);
            }
            tab.AddRow(rowValues.ToArray());
            tables.Add(tab);
        }

        // create properties
        var properties = new Dictionary<string, object>();
        items.Where(i => i.Table == "").ToList().ForEach(i => properties.Add(i.Property, i.Value));

        // create datasource to pass for testing
        return new DataSource(properties, tables);
    }

    private class DataSourceRecordHelper
    {
        public string Table { get; set; }
        public string Property { get; set; }
        public string Value { get; set; }
    }

    [WebMethod(EnableSession = true)]
    public List<string> GetColumns()
    {
        AuthorizationChecker.AllowAdmin();

        return DataStore.LocalDataSource.GetColumnIdentifiers().Select(c => c.ToString()).ToList();
    }

    [WebMethod(EnableSession = true)]
    public TestResult TestAggregation(string property, int type, string column)
    {
        AuthorizationChecker.AllowAdmin();

        var output = new ItemIdentifier(property);
        var id = new ItemIdentifier(column);
        var pollingConfig = DataStore.GetPollingConfigObject();
        var transformConfig = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
        var createConfig = pollingConfig.DataSourceCreateConfig as DataSourceCreateConfig;
        /*
        if ((transformConfig != null && transformConfig.ContainsProperty(output))
            || (createConfig != null && createConfig.ContainsProperty(output)))
        {
            return new TestResult(false, "Name '" + property + "' is already used, please choose a different one.");
        }
         * */

        var operation = new Aggregation(property, (AggregationType)type, id.Table, id.Property);
        TryTransformResult result;

        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
        {
            result = proxy.Api.TryTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
        }

        return result.Error ? new TestResult(false, result.ErrorMessage) : new TestResult(true, result.DataSource.Properties[property].ToString());
    }

    [WebMethod(EnableSession = true)]
    public void AddAggregation(string property, int type, string column)
    {
        AuthorizationChecker.AllowAdmin();

        var id = new ItemIdentifier(column);

        if (PollingConfig != null)
        {
            var pollingConfig = PollingConfig;

            if (pollingConfig.DataSourceTransformConfig == null)
                pollingConfig.DataSourceTransformConfig = new DataSourceTransformConfig() { Operations = new List<IDataSourceTransformOperation>() };

            var transforms = pollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
            var operation = new Aggregation(property, (AggregationType)type, id.Table, id.Property);

            using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(GetEngineID()))
            {
                DataStore.LocalDataSource = proxy.Api.DoTransform(SerializationHelper.ToXML<DataSourceTransformOperation>(operation), DataStore.LocalDataSource);
            }

            transforms.Operations.Add(operation);
            PollingConfig = pollingConfig;

            LatestItemIdentifier = new ItemIdentifier(null, property);
        }
    }

    [WebMethod(EnableSession = true)]
    public string GeneratePropertyName(string preffix)
    {
        AuthorizationChecker.AllowAdmin();

        return FormulaHelpers.GeneratePropertyName(preffix, DataStore.LocalDataSource);
    }

    // This just decides if expression contains any table properties
    // or not. It's used to determine the name of newly created
    // datasource item.
    [WebMethod(EnableSession = true)]
    public string GenerateResultType(string expression)
    {
        AuthorizationChecker.AllowAdmin();

        var id = FormulaHelpers.GenerateResultType("bla", expression, new string[] { "count", "sum", "avg" });
        return id.IsProperty ? String.Empty : id.Table;
    }

    [WebMethod(EnableSession = true)]
    public string LoadExpression(string itemIdentifier)
    {
        AuthorizationChecker.AllowAdmin();

        ItemIdentifier item = new ItemIdentifier(itemIdentifier);

        if (PollingConfig != null)
        {
            var transformConfig = PollingConfig.DataSourceTransformConfig as DataSourceTransformConfig;
            // this works because we save all operations as NewExpression, might need rewrite
            var expr = transformConfig.Operations.FindLast(x => (x as NewExpression).Output.Equals(item));
            if (expr != null)
            {
                return expr.ToString();
            }
        }

        return string.Empty;
    }

    private int GetEngineID()
    {
        int nodeID = int.Parse(DataStore.pollerNetObject);
        var node = NodeDAL.GetNode(nodeID);

        return node.EngineID;
    }
}