﻿<%@ WebService Language="C#" Class="PollerDatasourceManager" %>

using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.DeviceStudio.Common.Models;
using SolarWinds.DeviceStudio.Common.Helpers;
using SolarWinds.DeviceStudio.Framework;
using SolarWinds.DeviceStudio.Framework.Polling;
using SolarWinds.DeviceStudio.Framework.Polling.Create;
using SolarWinds.DeviceStudio.Framework.Polling.Output;
using SolarWinds.DeviceStudio.Web.Wizard;
using SolarWinds.DeviceStudio.Web.Helpers;
using System.Web.Script.Services;
using SolarWinds.DeviceStudio.BusinessLayer.Client;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PollerDatasourceManager  : System.Web.Services.WebService
{
    // Test
    [WebMethod(EnableSession = true)]
    public string Test()
    {
        AuthorizationChecker.AllowAdmin();

        var p = Poller;
        var n = NetObjectID;
        var st = SelectedTable;
        var sp = SelectedProperty;
        return string.Format("{0}, {1}, {2}, {3}", p, n, st, sp);
    }

    string GetUnavailableMessage(bool isOID)
    {
        return isOID ? Resources.DeviceStudioWebContent.CreatePoller_OID_Unavailable : Resources.DeviceStudioWebContent.CreatePoller_Formula_Unavailable;
    }
    
    string GetErrorMessage(ItemIdentifier current, ItemIdentifier output)
    {
        var outputProperty = FindOutputPropertyByItemIdentifier(output);
        var type = outputProperty.Type.ToString();
        var units = outputProperty.Units;

        // result types (colum vs property)
        if (output.IsColumn && !current.IsColumn)
        {            
            return String.Format(Resources.DeviceStudioWebContent.WEBDATA_LF0_9, outputProperty.Name, type);
        }

        if (output.IsProperty && !current.IsProperty)
        {
            return String.Format(Resources.DeviceStudioWebContent.WEBDATA_LF0_14, outputProperty.Name, type);
        }
        
        // must be problem with types
        if (output.IsColumn)
        {        
            return String.Format(Resources.DeviceStudioWebContent.WEBDATA_LF0_15, type, units, outputProperty.Name);
        }

        if (output.IsProperty)
        {
            return String.Format(Resources.DeviceStudioWebContent.WEBDATA_LF0_16, type, units, outputProperty.Name);
        }
        
        return String.Empty;
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<DatasourceRecordViewModel> GetDatasourceRecords(string outputTable, string outputProperty)
    {
        AuthorizationChecker.AllowAdmin();

        if (string.IsNullOrEmpty(outputProperty))
        {
            return null;
        }
         
        if (DataStore.LocalDataSource == null)
            DataStore.LocalDataSource = new DataSource();

        var ds = DataStore.LocalDataSource;
        
        var rv = new List<DatasourceRecordViewModel>();
        if (ds == null)
            return rv;

        // adding opid (instance of ItemIdentifier) to be able to do validation of types in datasource (string vs. int etc)
        // we already had output table and property in datasource manager javascript, so it was easy to send it here, but
        // we are passing these parameters at multiple places now, so maybe we could just set them once from list properties step (TODO)
        var opid = new ItemIdentifier(outputTable,outputProperty);
        
        var pollingConfig = GetPollingConfigObject();
        var createConfig = pollingConfig.DataSourceCreateConfig as DataSourceCreateConfig;
        
        // get 'no table' properties
        foreach (var property in ds.Properties)
        {
            var id = new ItemIdentifier(property.Key);
            var propInfo = pollingConfig.GetPropertyInfo(id);
            bool isOid = createConfig != null ? createConfig.ContainsProperty(id) : false;
            bool isNull = IsNull(id, ds);
            bool isCompatible = !isNull && CheckCompatibilityWithOutputProperty(opid, id, new List<object> { property.Value });

            rv.Add(new DatasourceRecordViewModel()
            {
                IconUrl = isOid ? "icon_point_v2.png" : "Transformations_icon16x16.png",
                Name = property.Key,
                Table = "",
                Value = isNull ? Resources.DeviceStudioWebContent.CreatePoller_Value_Unavailable : FormulaHelpers.ValueToString(property.Value),
                Details = propInfo.ToString(),
                ItemIdentifier = id.ToString(),
                RecordType = isOid ? "OID" : "Transformation",
                IsCompatibleWithOutput = isCompatible,
                InCompatibleIcon = isCompatible ? "" : "<img src=\"/Orion/DeviceStudio/Images/stop_16x16.png\" style=\"float:right\" title='" + (isNull ? GetUnavailableMessage(isOid) : GetErrorMessage(id,opid)) + "' />"
            });
        }
        
        // get properties from each table in datasource
        List<DatasourceRecordViewModel> items;
        Table tab;
        foreach (var table in ds.GetTableNames())
        {
            items = new List<DatasourceRecordViewModel>();
            tab = ds.GetTable(table);
            foreach (var column in tab.Columns)
            {
                var id = new ItemIdentifier(tab.TableName, column);
                var propInfo = pollingConfig.GetPropertyInfo(id);
                bool isOid = createConfig != null ? createConfig.ContainsProperty(id) : false;
                bool isNull = IsNull(id, ds);
                var values = GetColumnValues(tab, column);
                bool isCompatible = !isNull && CheckCompatibilityWithOutputProperty(opid, id, tab.GetColumn(column));
                
                items.Add(new DatasourceRecordViewModel { 
                    IconUrl = isOid ? "icon_column_v2.png" : "Transformations_icon16x16.png", 
                    Name = column, 
                    Table = table,
                    Value = isNull ? Resources.DeviceStudioWebContent.CreatePoller_Value_Unavailable : values, 
                    Details = propInfo == null ? string.Empty : propInfo.ToString(), 
                    ItemIdentifier = id.ToString(),
                    RecordType = isOid ? "OID" : "Transformation",
                    IsCompatibleWithOutput = isCompatible,
                    InCompatibleIcon = isCompatible ? "" : "<img src=\"/Orion/DeviceStudio/Images/stop_16x16.png\" style=\"float:right\" title='" + (isNull ? GetUnavailableMessage(isOid) : GetErrorMessage(id, opid)) + "' />"
                });
            }
            rv.AddRange(items);
        }

        return rv;
    }

    private bool IsNull(ItemIdentifier id, DataSource ds)
    {
        // return true if id does not exist in datasource
        if (!ds.ContainsItem(id))
            return true;

        if (id.IsProperty)
        {
            return ds.GetProperty(id.Property) == null;
        }
        else
        {
            var table = ds.GetTable(id.Table);
            var values = table.GetColumn(id.Property);

            if (values.Count == 0)
                return true;

            // return true if all rows are null
            return !values.Exists(v => v != null);
        }
    }
    
    private string GetColumnValues(Table table, string columnName)
    {
        var results = new List<string>();
        table.GetColumn(columnName).ForEach(v => results.Add(FormulaHelpers.ValueToString(v)));
        return String.Join(", ", results);
    }
    
    public class DatasourceRecordViewModel
    {
        public string IconUrl { get; set; }
        public string Name { get; set; }
        public string Table { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public string ItemIdentifier { get; set; }
        public string RecordType { get; set; }
        public bool IsCompatibleWithOutput { get; set; }
        public string InCompatibleIcon { get; set; }
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<OutputPropertyViewModel> GetOutputConfig(bool pollValues)
    {
        AuthorizationChecker.AllowAdmin();

        List<OutputPropertyViewModel> rv = new List<OutputPropertyViewModel>();
        var pollingConfig = GetPollingConfigObject();
        DataSourceOutputConfig outputConfig = GetOutputConfigObject(pollingConfig, pollValues);
        if (outputConfig == null)
            return rv;

        bool hasProperties = outputConfig.Properties != null && outputConfig.Properties.Count > 0;

        outputConfig.Tables = outputConfig.Tables ?? new List<OutputTable>();
        
        if (DataStore.LoadDataSources)
        {
            if (DataSource == null)
                DataSource = new DataSource();

            if (pollingConfig != null)
            {
                GetValuesFromDevice(pollingConfig);
            }

            DataStore.LoadDataSources = false;
        }

        if (hasProperties)
        {
          rv.AddRange(TransformProperties(outputConfig, outputConfig.Properties, ""));
        }
        outputConfig.Tables.ForEach(t => rv.AddRange(TransformProperties(outputConfig, t.Properties, t.Name)));
        return rv;
    }

    // helper method
    private List<OutputPropertyViewModel> TransformProperties(DataSourceOutputConfig outputConfig, List<OutputProperty> properties, string table)
    {
        var rv = from p in properties
                 select new OutputPropertyViewModel
                 {
                     Name = p.Name,
                     Type = p.Type.GetType().Name,
                     Mapping = p.Mapping,
                     Value = PollValue(outputConfig, table, p.Name),
                     Description = HttpUtility.HtmlEncode(p.Description),
                     Table = string.IsNullOrEmpty(table) ? "" : table,
                     Required = !p.Optional,
                     DisplayName = p.DisplayName ?? p.Name
                 };
        return rv.ToList<OutputPropertyViewModel>();
    }

    public class OutputPropertyViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Mapping { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Table { get; set; }
        public bool Required { get; set; }
        public string DisplayName { get; set; }
    }

    [WebMethod(EnableSession = true)]
    public void UpdateOutputConfig(string outputTable, string outputProperty, string mappingTable, string mappingProperty)
    {
        AuthorizationChecker.AllowAdmin();

        var pollingConfig = GetPollingConfigObject();
        var outputConfig = GetOutputConfigObject(pollingConfig, false);
        if (outputConfig != null)
        {
            if (!String.IsNullOrEmpty(outputTable))
            {
                var table = outputConfig.Tables.FirstOrDefault(t => t.Name == outputTable);
                if (table != null)
                {
                    // right now we do not support tables with more than one property because we do not support joins yet
                    // so we have to remap table mapping as well when column mapping is changed
                    table.Mapping = mappingTable;

                    var property = table.Properties.FirstOrDefault(p => p.Name == outputProperty);
                    if (property != null)
                    {
                        property.Mapping = mappingProperty;
                    }
                }
            }
            else
            {
                var property = outputConfig.Properties.FirstOrDefault(p => p.Name == outputProperty);
                if (property != null)
                {
                    property.Mapping = mappingProperty;
                }                            
            }
        }

        DataStore.poller.Configs.AddConfig<Config>(PollerConfigNames.PollingConfig, pollingConfig);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<GroupingValueViewModel> GetGroupingValues(string filterType)
    {
        AuthorizationChecker.AllowAdmin();

        var rv = new List<GroupingValueViewModel>();

        var outputConfig = GetOutputConfigObject(false);
        if (outputConfig == null)
            return rv;
        
        outputConfig.Tables = outputConfig.Tables ?? new List<OutputTable>();
                    
        switch (filterType)
        {
            case "table":
                if (outputConfig.Properties != null)
                    rv.Add(new GroupingValueViewModel { Name = "No Table", Count = outputConfig.Properties.Count });

                outputConfig.Tables.ForEach(t => rv.Add(new GroupingValueViewModel { Name = t.Name, Count = t.Properties.Count }));
                return rv;
            case "required":
                int rCount = 0;
                if (outputConfig.Properties != null)
                    rCount += outputConfig.Properties.Count(p => p.Optional == false);

                outputConfig.Tables.ForEach(t => rCount += t.Properties.Count(p => p.Optional == false));
                rv.Add(new GroupingValueViewModel { Name = "Required", Count = rCount });

                int oCount = 0;
                if (outputConfig.Properties != null)
                    oCount += outputConfig.Properties.Count(p => p.Optional == true);

                outputConfig.Tables.ForEach(t => oCount += t.Properties.Count(p => p.Optional == true));
                rv.Add(new GroupingValueViewModel { Name = "Optional", Count = oCount });
                return rv;
            default:
                return rv;
        }
    }

    public class GroupingValueViewModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }

    private string PollValue(DataSourceOutputConfig outputConfig, string table, string property)
    {
        OutputTable t; // table in output config
        string dsTableName; // matching table name in datasource
        Table dsTable; // table object from datasource
        
        OutputProperty p; // property in output config
        string dsPropertyName; // matching property name in datasource
        object dsProperty; // property object from datasource
        
        List<string> tableResult;

        if (outputConfig == null)
            return "";
        
        // table set -> find the right table
        if (!string.IsNullOrEmpty(table))
        {
            t = outputConfig.Tables.FirstOrDefault(x => x.Name == table);
            if (t == null)
                return "";

            dsTableName = t.Mapping;
            dsTable = DataSource.GetTable(dsTableName);
            if (dsTable == null)
                return "";

            p = t.Properties.FirstOrDefault(x => x.Name == property);
            if (p == null || String.IsNullOrEmpty(p.Mapping))
                return "";
            
            // table is empty
            if (dsTable.Rows.Count() == 0)
                return Resources.DeviceStudioWebContent.CreatePoller_Value_Unavailable;

            dsPropertyName = p.Mapping;
            tableResult = new List<string>();
            foreach (SolarWinds.DeviceStudio.Common.Models.Table.TableRow row in dsTable.Rows)
            {
                string value = FormulaHelpers.ValueToString(row[dsPropertyName]);
                tableResult.Add(value);
            }

            return string.Join(", ", tableResult.ToArray());
        }

        // no table set -> check if it's standalone property
        if (!string.IsNullOrEmpty(property))
        {
            p = outputConfig.Properties.FirstOrDefault(x => x.Name == property);
            if (p == null)
                return "";

            dsPropertyName = p.Mapping;
            if (string.IsNullOrEmpty(dsPropertyName))
                return "";

            if (!DataSource.ContainsProperty(dsPropertyName))
                return "";
                
            dsProperty = DataSource.GetProperty(dsPropertyName);
            return FormulaHelpers.ValueToString(dsProperty, Resources.DeviceStudioWebContent.CreatePoller_Value_Unavailable);
        }

        return "N/A";
    }

    private PollingConfig GetPollingConfigObject()
    {
        if (DataStore.poller == null)
            return null;

        return DataStore.poller.Configs.GetConfig<PollingConfig>(PollerConfigNames.PollingConfig);
    }

    private DataSourceOutputConfig GetOutputConfigObject(PollingConfig pollingConfig, bool pollValues)
    {
        if (pollingConfig == null)
            return null;

        if (pollValues)
            GetValuesFromDevice(pollingConfig);

        var outputConfig = pollingConfig.DataSourceOutputConfig as DataSourceOutputConfig;
        return outputConfig;
    }
    
    private DataSourceOutputConfig GetOutputConfigObject(bool pollValues)
    {
        var pollingConfig = GetPollingConfigObject();
        if (pollingConfig == null)
            return null;

        if (pollValues)
            GetValuesFromDevice(pollingConfig);
        
        var outputConfig = pollingConfig.DataSourceOutputConfig as DataSourceOutputConfig;
        return outputConfig;
    }

    private void GetValuesFromDevice(Config pollingConfig)
    {
        int nodeID = int.Parse(DataStore.pollerNetObject);
        var node = NodeDAL.GetNode(nodeID);

        var pc = pollingConfig as PollingConfig; 
        pc.RemoveUnusedProperties();
        DataStore.poller.Configs.AddConfig<Config>(PollerConfigNames.PollingConfig, pc);
        
        using (var proxy = DeviceStudioBusinessLayerProxyFactory.Instance.Create(node.EngineID))
        {
            DataSource = proxy.Api.DoPoll(node, PollingMethods.Snmp, SerializationHelper.ToXML<PollingConfig>(pc), true, 5, false);
        }        
    }

    [WebMethod(EnableSession = true)]
    public object GetLatestDatasourceRecord(string outputTable, string outputProperty)
    {
        AuthorizationChecker.AllowAdmin();

        //
        // 1. Get property from output config (will always need it at least for metadata).
        // 2. Check for existence of LatestItemIdentifier.
        // 3. Get the value from datasource.
        // 4. Validate.
        // 5. Put it together and return
        //

        ItemIdentifier opId;
        OutputProperty op;
        List<object> dv;
        string opUnits, opType, errorMessage;
        bool isCompatible = true;
        
        // 1
        opId = new ItemIdentifier(outputTable, outputProperty);
        op = FindOutputPropertyByItemIdentifier(opId);
        opUnits = op.Units ?? string.Empty;
        opType = op.Type.ToString() ?? string.Empty;
        
        // 2
        if (LatestItemIdentifier == null)
        {
            return new
            {
                status = "error",
                message = "Latest Item is not set. There are probably no data in datasource.",
                expectedUnits = opUnits,
                expectedType = opType
            };
        }
        
        // 3
        dv = FindDataSourceValueByItemIdentifier(LatestItemIdentifier, out errorMessage);
        if ((dv.Count == 0) || (LatestItemIdentifier.IsProperty && (dv.Count == 1) && (dv[0] == null)))
        {
            return new
            {
                status = "error",
                message = errorMessage,
                expectedUnits = opUnits,
                expectedType = opType,
                expectedColumnOrProperty = opId.IsColumn ? "column" : "property"
            };
        }
        
        // 4
        
        isCompatible = CheckCompatibilityWithOutputProperty(opId, LatestItemIdentifier, dv);

        // 5
        return new
        {
            status = isCompatible == true ? "compatible" : "incompatible",
            message = "OK",            
            currentValue = dv,
            expectedUnits = opUnits,
            expectedType = opType,
            itemIdentifier = LatestItemIdentifier.ToString(),
            expectedColumnOrProperty = opId.IsColumn ? "column" : "property"
        };
    }

    [WebMethod(EnableSession = true)]
    public bool RemoveRecord(string recordId)
    {
        AuthorizationChecker.AllowAdmin();

        if (string.IsNullOrEmpty(recordId))
            return false;

        var id = new ItemIdentifier(recordId);
        var pollingConfig = GetPollingConfigObject();

        if (pollingConfig != null)
        {
            pollingConfig.RemovePropertyFromDataSource(DataStore.LocalDataSource, id);
            // if current item was deleted, we want to automatically reselect first record in datasource to avoid 'empty' message
            if (!DataStore.LocalDataSource.ContainsItem(LatestItemIdentifier))
            {
                LatestItemIdentifier = DataStore.LocalDataSource.GetItems().FirstOrDefault();
            }
            return true;
        }
        
        return false;
    }

    [WebMethod(EnableSession = true)]
    public void RestorePollingConfig()
    {
        AuthorizationChecker.AllowAdmin();

        DataStore.RestorePollingConfigFromBackup();
    }
        
    /// <summary>
    /// There are multiple places where we need to check the compatibility between output and datasource records.
    /// This function should do it.
    /// </summary>
    /// <param name="output">ItemIdentifier of output property (this sets the expectations)</param>
    /// <param name="source">ItemIdentifier of datasource property (this is being validated)</param>
    /// <param name="sourceValues">List of datasource property values used for validation</param>    
    /// <returns></returns>
    private bool CheckCompatibilityWithOutputProperty(ItemIdentifier output, ItemIdentifier source, List<object> sourceValues)
    {                
        if ((output.IsColumn && source.IsProperty) || (output.IsProperty && source.IsColumn)) {
            return false;
        }
        
        OutputProperty op = FindOutputPropertyByItemIdentifier(output);
        if (op == null)
        {
            return false;
        }

        string errorMessage;
        foreach (object value in sourceValues)
        {
            if (!op.Type.TryValidate(value, out errorMessage))
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Will return a value of matching column or property from datasource if match is found.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="errorMessage"></param>
    /// <returns>list of object values or null</returns>
    private List<object> FindDataSourceValueByItemIdentifier(ItemIdentifier item, out string errorMessage)
    {
        errorMessage = string.Empty;
        var rv = new List<object>();

        if (item != null && DataStore.LocalDataSource.ContainsItem(item))
        {
            if (item.IsProperty)
            {
                rv.Add(DataStore.LocalDataSource.Properties[item.Property]);
            }
            else if (item.IsColumn)
            {
                Table t = DataStore.LocalDataSource.Tables.FirstOrDefault(x => x.TableName == item.Table);
                if (t != null)
                {
                    rv = t.GetColumn(item.Property);
                }
            }
        }
        
        errorMessage = "Failed to find '" + item.ToString() + "' in datasource.";
        return rv;
    }

    /// <summary>
    /// Will return a single property or column from output config if match is found.
    /// </summary>
    /// <param name="item"></param>
    /// <returns>property, column or null</returns>
    private OutputProperty FindOutputPropertyByItemIdentifier(ItemIdentifier item)
    {
        if (item == null)
        {
            return null;
        }

        DataSourceOutputConfig outputConfig = GetOutputConfigObject(false);
        
        if (item.IsProperty)
        {
            return outputConfig.Properties.FirstOrDefault(x => x.Name == item.Property);
        }
        else if (item.IsColumn)
        {
            var table = outputConfig.Tables.FirstOrDefault(x =>x.Name == item.Table);
            if (table != null) {
                return table.Properties.FirstOrDefault(x=>x.Name == item.Property);
            }
        }

        return null;
    }

    [WebMethod(EnableSession = true)]
    public string UpdateLatestDatasourceRecord(string itemIdentifier)
    {
        AuthorizationChecker.AllowAdmin();

        LatestItemIdentifier = new ItemIdentifier(itemIdentifier);
        return LatestItemIdentifier.ToString();
    }
    
    private DSWizardDataStore DataStore
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_Storage"]] as DSWizardDataStore;
        }
    }
    
    private Poller Poller
    {
        get
        {
            return DataStore.poller;
        }
    }

    private DataSource DataSource
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] as DataSource;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_DataSource"]] = value;
        }
    }

    private int NetObjectID
    {
        get
        {
            object id = Session[ConfigurationManager.AppSettings["CDP_Session_NetObjectID"]];
            if (id == null) return -1;
            return (int)id;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_NetObjectID"]] = value;
        }
    }

    private string SelectedTable
    {
        get
        {
            object t = Session[ConfigurationManager.AppSettings["CDP_Session_SelectedTable"]];
            if (t == null) return "";
            return (string)t;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_SelectedTable"]] = value;
        }
    }
    
    private string SelectedProperty
    {
        get
        {
            object p = Session[ConfigurationManager.AppSettings["CDP_Session_SelectedProperty"]];
            if (p == null) return "";
            return (string)p;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_SelectedProperty"]] = value;
        }
    }

    private ItemIdentifier LatestItemIdentifier
    {
        get
        {
            return Session[ConfigurationManager.AppSettings["CDP_Session_LatestItemIdentifier"]] as ItemIdentifier;
        }
        set
        {
            Session[ConfigurationManager.AppSettings["CDP_Session_LatestItemIdentifier"]] = value;
        }
    }
}
