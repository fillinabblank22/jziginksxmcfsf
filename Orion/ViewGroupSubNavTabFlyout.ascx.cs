using System;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using System.Collections.Generic;

public partial class ViewGroupSubNavTabFlyout : BaseViewGroupControl
{
    private System.Data.DataTable _viewResources;
    protected System.Data.DataRow[] Column1Resources
    {
        get { 
            if( _viewResources == null )
                _viewResources = ResourceManager.GetResourcesForViewGroup(View.ViewGroup);
            return _viewResources.Select("ViewColumn = 1 And ViewId =" + View.ViewID);
        }
    }
    protected System.Data.DataRow[] Column2Resources
    {
        get
        {
            if (_viewResources == null)
                _viewResources = ResourceManager.GetResourcesForViewGroup(View.ViewGroup);
            return _viewResources.Select("ViewColumn = 2 And ViewId =" + View.ViewID);
        }
    }
    protected System.Data.DataRow[] Column3Resources
    {
        get
        {
            if (_viewResources == null)
                _viewResources = ResourceManager.GetResourcesForViewGroup(View.ViewGroup);
            return _viewResources.Select("ViewColumn = 3 And ViewId =" + View.ViewID);
        }
    }

}
