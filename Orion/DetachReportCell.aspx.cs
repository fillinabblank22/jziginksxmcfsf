﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.Reporting.ReportCharts;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Layout;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;

public partial class Orion_DetachReportCell : Page, IResourceContainer
{
    private Report _report;
    private Guid _cellId;
    private bool _showResourceOnly;
    
    public Report Report
    {
        get { return _report; }
    }

    public Guid CellId
    {
        get { return _cellId; }
    }

    protected override void OnPreInit(EventArgs e)
    {
        var resourceOnlyParam = Request.QueryString["ResourceOnly"] ?? "0";
        _showResourceOnly = resourceOnlyParam.Equals("1", StringComparison.OrdinalIgnoreCase);

        int reportId;
        Guid reportGuid;
        if (Guid.TryParse(Request.QueryString["ReportGuid"], out reportGuid))
            _report = ReportStorage.Instance.GetReport(reportGuid);
        else if (int.TryParse(Request.QueryString["ReportId"], out reportId))
            _report = Loader.Deserialize(XElement.Parse((OrionReportHelper.GetReportbyID(reportId).Definition)));
        else
            throw new InvalidOperationException("ReportGuid parameter is missing or invalid within the Query String.");
        
        if (!Guid.TryParse(Request.QueryString["ReportCellGuid"], out _cellId))
            throw new InvalidOperationException("ReportCellGuid parameter is missing or invalid within the Query String.");

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        SectionCell sectionCell = null;
        foreach (var column in _report.Sections.SelectMany(section => section.Columns))
        {
            foreach (var cell in column.Cells.Where(cell => cell.RefId == _cellId))
            {
                sectionCell = cell;
                break;
            }
        }
        
        if (sectionCell!=null)
        {
            this.Title = Macros.ParseMacros(sectionCell.DisplayName, string.Empty);
        }

        if (!_showResourceOnly)
            this.divContainer.Style[HtmlTextWriterStyle.MarginLeft] = "12px";

        if (_showResourceOnly)
        {
            TopRightPageLinks.Visible = false;
            ResourceTitle.Visible = false;
        }

        divContainer.Controls.Add(new ReportCellPreviewControl
        {
            Model = _report,
            CellId = _cellId,
            AllowToolTips = true
        });

        HtmlMeta metaTag = new HtmlMeta();
        metaTag.HttpEquiv = "REFRESH";
        metaTag.Content = string.Format("{0}; URL='{1}'", WebSettingsDAL.AutoRefreshSeconds, this.Request.Url.ToString());
        this.Page.Header.Controls.Add(metaTag);

        OrionInclude.CoreThemeStylesheets();

        base.OnInit(e);
    }
}