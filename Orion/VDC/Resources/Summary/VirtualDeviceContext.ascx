﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualDeviceContext.ascx.cs" Inherits="Orion_VDC_Resources_Summary_VirtualDeviceContext" %>

<orion:Include ID="Include" runat="server" File="OrionCore.js" />

<style>
    .vdc-name {
        font-weight: bold;
    }

    .this-node {
        color: #767676;
        font-style: italic;
    }

    .vdc-status-cell {
        padding-right: 0px !important;
        width: 32px;
    }

    .vdc-name-cell {
        width:  50%;
    }

    .vdc-admin-cell {
        width: 50%;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        var resourceId = $('#<%=VirtualDeviceContextResource.ClientID%>');
        var nodeId = <%=NodeID%>;
        var hasContexts = <%=HasContexts.ToString().ToLowerInvariant()%>;

        var init = function(nodeId) {
            hideMask(false);
            
            $.ajax({
                type: "GET",
                url: '/api2/vdc/'+ nodeId,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, status, xhr) {
                    hideMask(true);
                    onSuccess(data);
                },
                error: function(xhr, status, errorThrown) {
                    hideMask(true);
                    onError(xhr);
                }
            });
        };
        
        var hideMask = function(hide) {
            if (hide) {
                resourceId.find('#LoadingMask').hide();
            } else {
                resourceId.find('#LoadingMask').show();
            }
        };
        
        var onSuccess = function(data) {
            if (data && data.length > 0) {
                var markup = buildMarkup(data);
                var content = resourceId.find('#Content');
                content.html(markup);
            }
        };

        var onError = function(error) {
            showError("<%=Resources.VdcWebContent.VDC_ServerErrorMsg %>");
        };

        var showError = function(msg) {
            var content = resourceId.find('#Content');
            content.html(String.format("<div class='sw-suggestion sw-suggestion-fail'> \
                                            <div class='sw-suggestion-icon'></div> \
                                                {0} \
                                        </div>", msg));
        };

        var buildStatusImage = function (statusIcon) {
            return String.format("<img src='/Orion/images/StatusIcons/{0}'>", statusIcon === '' ? "Unknown.gif" : statusIcon);
        };

        var buildLinkToAddNodePage = function() {
            return "<a href='/Orion/Nodes/Add/Default.aspx' target='__blank'><%=Resources.VdcWebContent.VDC_MonitorNodeLabel %></a>";
        };

        var buildLinkToNodeDetailsPage = function(detailsUrl, name) {
            return String.format("<a href='{0}'>{1}</a>", detailsUrl, name);
        };

        var buildMarkup = function(data) {
            var markup = "<table class='NeedsZebraStripes' width='100%' cellspacing='0' cellpadding='0' border='0'>";
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                var escapedName = _.escape(item.name);
                var escapedNodeName = _.escape(item.nodeName);
                markup += String.format("<tr> \
                                            <td class='vdc-status-cell'> \
                                                {0} \
                                            </td> \
                                            <td class='vdc-name-cell'> \
                                                <div class='{1}'>{2}</div> \
                                                <div>{3}</div> \
                                            </td> \
                                            <td class='vdc-admin-cell'> \
                                                {4} \
                                            </td> \
                                         </tr>",
                                        buildStatusImage(item.statusIcon),
                                        item.nodeID === nodeId ? "<%=Resources.VdcWebContent.VDC_ThisNodeLabel %>" : "vdc-name", 
                                        item.nodeID === nodeId ? "<%=Resources.VdcWebContent.VDC_ThisNodeLabel %>," : item.nodeID == null ? escapedName : buildLinkToNodeDetailsPage(item.detailsUrl, escapedName),
                                        item.nodeID === nodeId ? escapedName : item.nodeID == null ? buildLinkToAddNodePage() : escapedNodeName,
                                        item.isAdminContext ? "Admin" : "");
            }
            markup += "</table>";
            return markup;
        };
        
        if (hasContexts) {
            init(nodeId);
        }
    });
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div id="VirtualDeviceContextResource" runat="server">
            <span id="LoadingMask" style="font-weight:bold;color:#777;text-align:center;display:block;margin-left:auto;margin-right:auto;">
                <img  src="/Orion/images/AJAX-Loader.gif" style="margin-right:5px;"/><%=Resources.VdcWebContent.VDC_LoadingLabel %>
            </span>
            <div id="Content"></div>
        </div>
    </Content>
</orion:resourceWrapper>
