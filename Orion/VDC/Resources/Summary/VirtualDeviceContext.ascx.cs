﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using System.Collections.Generic;
using SolarWinds.Orion.Web.InformationService;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_VDC_Resources_Summary_VirtualDeviceContext : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return Resources.VdcWebContent.VDC_DefaultTitle;
        }
    }

    protected int NodeID { get; set; }

    protected bool HasContexts { get; set; }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNCMVirtualDeviceContext"; }
    }

    protected override void OnInit(EventArgs e)
    {
        var nodeProvider = GetInterfaceInstance<INodeProvider>();

        NodeID  = nodeProvider?.Node != null ? nodeProvider.Node.NodeID : 0;
        HasContexts = HasNodeContexts(NodeID);

        Wrapper.Visible = HasContexts;

        base.OnInit(e);
    }

    private bool HasNodeContexts(int nodeId)
    {
        if (nodeId <= 0)
        {
            return false;
        }

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var dt = proxy.Query("select top 1 SystemId from Vdc.Contexts where NodeId=@nodeId", new Dictionary<string, object>() { { "nodeId", nodeId } });
            return dt.Rows.Count > 0;
        }
    }
}
