<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertTimeIntervals.ascx.cs" Inherits="Orion_Controls_AlertTimeIntervals" %>

<orion:Include ID="Include1" runat="server" File="AlertTimeIntervals.js" />

<asp:TextBox runat="server" ID="alertTimeValue" Width="30px" />
<asp:DropDownList runat="server" ID="alertTimeType" />
<span id="validatorMessageField" class="sw-suggestion sw-suggestion-fail sw-validation-error" style="color: red; visibility: hidden;"></span>
&nbsp;
<asp:CustomValidator runat="server" ControlToValidate="alertTimeValue" ID="alertTimeValueValidator" Display="None" ValidateEmptyText="true"
                     ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_YP0_14 %>" OnServerValidate="alertTimeValueValidator_ServerValidate" />

<script type="text/javascript">
    var timeIntervals_<%= ClientID %>;

    (function() {
        var config = {
            alertTimeValueId: '<%= alertTimeValue.ClientID %>',
            alertTimeTypeId: '<%= alertTimeType.ClientID %>',
            alertTimeValueValidatorId: '<%= alertTimeValueValidator.ClientID %>',
            minTimeInSeconds: <%= MinTimeInSeconds%>,
            minTimeUnit: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %>',
            maxTimeInSeconds: <%= MaxTimeInSeconds%>
        };
        timeIntervals_<%= ClientID %> = new SW.Core.Controls.AlertTimeIntervals(config);
    })();
</script>


