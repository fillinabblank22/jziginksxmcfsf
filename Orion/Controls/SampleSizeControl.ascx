<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SampleSizeControl.ascx.cs" Inherits="SampleSizeControl" %>

<tr>
<% if( !InLine ) { %>
    <td align="justify" style="font-weight: bold;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_290) %>
    </td>
    <td class="formRightInput">
<% } else { %>
    <td colspan="3">
    <p>
    <div align="justify" style="font-weight: bold;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_290) %>
    </div>
    <div style="text-align: left" class="sw-text-helpful">
        <%= DefaultSanitizer.SanitizeHtml(HelpfulText) %>
    </div>
    <div>
 <% } %>

    <asp:DropDownList ID="SampleSizeList" runat="server">
    </asp:DropDownList>

<% if( !InLine ) { %>
    </td>
    <td class="formHelpfulTxt">
        <%= DefaultSanitizer.SanitizeHtml(HelpfulText) %>
<% } else { %>
    </div>
</p>
 <% } %>
</td>
</tr>