﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class GenericNotificationsControl : BaseNotificationsControl
{
    public NotificationItemType SelectedNotificationItemType { get; set; }
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    public override void RefreshContent()
    {
        base.RefreshContent();

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            var items = proxy.GetNotificationItemsByType(SelectedNotificationItemType.Id, ShowIgnoredCheckBox.Checked);

            UpdatesTable.DataSource = items;
            UpdatesTable.DataBind();

            NoResultsMessage.Style["display"] = items.Count == 0 ? "block" : "none";
        }
    }

    protected void IgnoreSelectedItems(object sender, EventArgs e)
    {
        var updateKeys = new List<Guid>();
        foreach (RepeaterItem item in UpdatesTable.Items)
        {
            var cb = item.FindControl("SelectItemCheckBox") as CustomCheckBox;
            if (cb != null)
            {
                if (cb.Checked && cb.Enabled)
                {
                    if (cb.CustomValue != null)
                    {
                        updateKeys.Add(new Guid(cb.CustomValue.ToString()));
                    }
                }
            }
        }

        if (updateKeys.Count > 0)
        {
            IgnoreItemsBulk(updateKeys);
        }
    }
}
