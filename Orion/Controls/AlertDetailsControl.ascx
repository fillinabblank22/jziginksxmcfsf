<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>

<asp:ScriptManagerProxy runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Services/AlertsAdmin.asmx" />
    </Services>
</asp:ScriptManagerProxy>
<div id="divAlertDetaisDialog" style="display: none;">
    <iframe frameborder="0" id="alertDetaisFrame"></iframe>
    <div id="divAlertDetais" style="position: fixed; z-index: 1000;
        width: 100%; left: 0px;">
        <div runat="server" id="divDialog" style="margin-left: auto; margin-right: auto;
            width: 800px; height: 100%; background-color: White;">
            <table width="100%" style="border-bottom-width: 1px; border-bottom-style: solid;
                border-bottom-color: Black; border-top-width: 1px; border-top-style: solid; border-top-color: Black;
                border-left-width: 1px; border-left-style: solid; border-left-color: Black; border-right-width: 1px;
                border-right-style: solid; border-right-color: Black;">
                <tr>
                    <td style="border: 0px; background-image: url('/Orion/images/PopupHeader2Rows.gif');
                        background-repeat: repeat-x;" width="100%">
                        <table style="margin-bottom: 1px;" width="100%">
                            <tr>
                                <td>
                                    <span id="AlertDetailsHeader" style="font-weight:bold; font-size:large;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_153) %></span>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="CloseCross" runat="server" ImageUrl="/Orion/images/Button.CloseCross.gif"
                                        OnClientClick="HideAlertDetails(); return false;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_154) %></b>&nbsp;<span id="AlertName"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_155) %></b>&nbsp;<span id="AlertTime"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_156) %></b>&nbsp;<span id="AlertType"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_157) %></b>&nbsp;<span id="NetworkObject"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_158) %></b>&nbsp;<span id="AcknowledgedBy"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_159) %></b>&nbsp;<span id="AcknowledgedTime"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <span id="NotesLabel" style="font-weight: bold;" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_152) %></span><br />
                        <asp:TextBox ID="Notes" runat="server" Wrap="false" TextMode="MultiLine" Rows="20"
                            Font-Size="8pt" Width="780px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding: 5px;">
                        <div class="sw-btn-bar-wizard">
                            <orion:LocalizableButton runat="server" ID="Ok" LocalizedText="Ok" DisplayType="Primary"
                                OnClientClick="HideAlertDetails(); return false;" />
                            <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="Secondary" 
                                OnClientClick="HideAlertDetails(); return false;" />
                            <orion:LocalizableButton runat="server" ID="Close" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEB_JS_CODE_VB0_3%>" DisplayType="Primary" 
                                OnClientClick="HideAlertDetails(); return false;" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
    function getWindowHeight() {
        var windowHeight = 0;
        if (typeof (window.innerHeight) == 'number') {
            windowHeight = window.innerHeight;
        }
        else {
            if (document.documentElement && document.documentElement.clientHeight) {
                windowHeight = document.documentElement.clientHeight;
            }
            else {
                if (document.body && document.body.clientHeight) {
                    windowHeight = document.body.clientHeight;
                }
            }
        }
        return windowHeight;
    }

    function setAlertDetaisDialog() {
        var outY = getWindowHeight();
        var div = document.getElementById('divAlertDetais');
        var inY;
        var nextTop;
        if (div != null) {
            inY = div.offsetHeight;
            nextTop = (parseInt(outY) - parseInt(inY)) / 2;
            // check if IE
            if ($.browser.msie) {
                var offsetParent = div.offsetParent;
                while (offsetParent != null) {
                    nextTop = nextTop + offsetParent.scrollTop;
                    offsetParent = offsetParent.offsetParent;
                }
            }
            div.style.top = nextTop + 'px';
        }
    }

    function SetupPopupControl(isBasic, allowEventClear) {
        if (isBasic || !allowEventClear) {
            $('#<%=Ok.ClientID %>').css({ display: "none" });
            $('#<%=Cancel.ClientID %>').css({ display: "none" });
            $('#<%=Notes.ClientID %>').attr("disabled", "disabled");
            $('#<%=Notes.ClientID %>').attr("readonly", "readonly");
            $('#<%=Notes.ClientID %>').attr("class", "disabledAlertNote");
            $('#<%=Close.ClientID %>').css({ display: "" });
            $('#NotesLabel').text("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_6) %>");
        }
        else {
            $('#<%=Ok.ClientID %>').css({ display: "" });
            $('#<%=Cancel.ClientID %>').css({ display: "" });
            $('#<%=Notes.ClientID %>').removeAttr("disabled");
            $('#<%=Notes.ClientID %>').removeAttr("readonly");
            $('#<%=Notes.ClientID %>').removeAttr("class");
            $('#<%=Close.ClientID %>').css({ display: "none" });
            $('#NotesLabel').text("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_TM0_152) %>");
        }
    }

   

    function ShowAlertDetails(alertId, netObjectId, objectType, allowEventClear) {
        AlertsAdmin.GetAlertDetailsById(alertId, netObjectId, function (result) {
            if (result != null && result.Rows.length > 0) {
                var row = result.Rows[0];
                if (result.Columns.length == 23) {
                    $('#AlertName').text(row[1]);
                    $('#AlertTime').text(row[2]._toFormattedString(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern, Sys.CultureInfo.CurrentCulture) + ' ' + row[2]._toFormattedString(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern, Sys.CultureInfo.CurrentCulture));
                    $('#AlertType').text(row[10]);
                    $('#NetworkObject').text(row[4]);
                    $('#AcknowledgedBy').text(row[7]);

                    var isAdvAlert = (row[10] == '<%=OrionMessagesHelper.GetLocalizedMessageTypeLabel(OrionMessageType.ADVANCED_ALERT, true, false)%>');

                    $('#<%=Notes.ClientID %>').val(row[22]);

                    SetupPopupControl(!isAdvAlert, allowEventClear);

                    if (isAdvAlert && row[8]._toFormattedString("yyyy", Sys.CultureInfo.InvariantCulture) != "1899")
                        $('#AcknowledgedTime').text(row[8]._toFormattedString(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern, Sys.CultureInfo.CurrentCulture) + ' ' + row[8]._toFormattedString(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern, Sys.CultureInfo.CurrentCulture));
                    else
                        $('#AcknowledgedTime').text("");
                }

                $('#<%=Ok.ClientID %>').unbind('click');
                $('#<%=Ok.ClientID %>').click(function () {
                    AlertsAdmin.UpdateAdvancedAlertNote(alertId, netObjectId, objectType, $('#<%=Notes.ClientID %>').val(),
                        function (result) {
                            HideAlertDetails();
                        },
                        function (ex) {
                            alert(ex.get_message());
                            HideAlertDetails();
                        }
                        );
                });

                $('#divAlertDetaisDialog').show();
                setAlertDetaisDialog();
            }
        },
        function (ex) { });
    }

    function HideAlertDetails() {
        $('#divAlertDetaisDialog').hide();
    }

    
    $('#alertDetaisFrame').attr('style', 'position: fixed; width: 100%; height: 100%; top:0px; bottom:0px; left:0px; right:0px; overflow:hidden; padding:0; margin:0; background-color: Black; opacity: 0.5; -moz-opacity: 0.5;  filter: alpha(opacity=50); z-index: 1000;');
    
    window.onresize = function () { setAlertDetaisDialog(); }
</script>
<!--[if IE]>
    <script type="text/javascript">
        window.onscroll = function() { setAlertDetaisDialog(); }
    </script>
<![endif]-->
