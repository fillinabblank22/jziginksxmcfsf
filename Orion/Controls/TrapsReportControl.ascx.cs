using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SyslogTraps.Common;
using Limitation = SolarWinds.Orion.Web.Limitation;

public partial class TrapsReportControl : System.Web.UI.UserControl
{
    private static Log log = new Log();
    private string[] _columns;

    protected override void OnInit(EventArgs e)
    {
        _columns = SolarWinds.Orion.Core.Common.RegistrySettings.GetSetting(
            "Traps Server", "Settings", "Display Fields", "DateTime,IP Address,Hostname,Community,TrapType,Trap Details").Split(',');
        base.OnInit(e);
    }

    public string[] Columns
    {
        get { return _columns; }
    }

    public static string GetColumnName(string column)
    {
        switch (column.ToUpper())
        {
            case "ACKNOWLEDGED":
            case "MSGID":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_186;
            case "HOSTNAME":
                return Resources.SyslogTrapsWebContent.WEBDATA_TM0_4;
            case "DATETIME":
                return Resources.SyslogTrapsWebContent.WEBDATA_TM0_9;
            case "CAPTION":
                return Resources.SyslogTrapsWebContent.WEBDATA_AK0_155;
            case "NODEID":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_189;
            case "DYNAMICIP":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_123;
            case "DNS":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_126;
            case "COMMUNITY":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_190;
            case "SYSNAME":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_191;
            case "VENDOR":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_124;
            case "MACHINETYPE":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_16;
            case "TRAP DETAILS":
                return Resources.SyslogTrapsWebContent.WEBDATA_TM0_10;
            case "IP ADDRESS":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_13;
            case "TRAPTYPE":
                return Resources.SyslogTrapsWebContent.WEBDATA_TM0_5;
            default:
                return column;
        }
    }

    public void GenerateReport(string netObjectID, string period, string deviceType, string trapType, string ipAddress, string communityString, int maxRecords)
    {
        DateTime periodBegin = new DateTime();
        DateTime periodEnd = new DateTime();

        Periods.Parse(ref period, ref periodBegin, ref periodEnd);

        int netObjID = 0;
        Int32.TryParse(netObjectID.Substring(netObjectID.IndexOf(":") + 1), out netObjID);

        DataTable table;

        using (var proxy = SyslogTrapBusinessLayerProxyFactory.Instance.Create())
        {
            table = proxy.Api.GetTrapData(netObjID,
                    deviceType,
                    trapType,
                    ipAddress,
                    communityString,
                    periodBegin,
                    periodEnd,
                    maxRecords,
                    Limitation.GetCurrentListOfLimitationIDs(), new List<string>(_columns));

            if (table.Rows.Count == 0 && !proxy.Api.AreTrapsAvailable())
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine("<table cellpadding=\"5\"><tr><td><table width=\"100%\" style=\"background-color: #f8f8f8;\" cellpadding=\"3\">");
                message.AppendLine("<tr>");
                message.AppendLine("    <td><img src=\"/Orion/images/Icon.Info.gif\" alt=\"Info\" />&nbsp;<b>");
                message.Append(String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_62,
                    "</b></td></tr><tr>    <td>",
                    "</td></tr><tr>    <td>",
                    string.Format("<a href=\"{0}\" style=\"color: #336699\" target=\"_blank\" rel=\"noopener noreferrer\"><u>", HelpHelper.GetHelpUrl("OrionPHConfiguringRequiredServices")),
                    "</u></a>"
                    ));
                message.Append("</td>");
                message.AppendLine("</tr>");
                message.AppendLine("</table></td></tr></table>");
                tablePlaceHolder.Controls.Add(new LiteralControl("<div style=\"text-align: left; margin: 8px;background-color: #f8f8f8; \">" + message.ToString() + "</div>"));
            }
            else
            {
                GenerateHTMLReport(table);
            }
        }
    }

    private void GenerateHTMLReport(DataTable dataTable)
    {
        tablePlaceHolder.Controls.Add(new LiteralControl("<table class=\"Report\" style='width: 100%;'  cellpadding='0px' cellspacing='0px'><thead style='background-color: #dddcd0;'><tr>"));
        foreach (string column in Columns)
        {
            if (column.Equals("Community", StringComparison.OrdinalIgnoreCase) && !Profile.AllowAdmin)
            {
                continue;
            }
            tablePlaceHolder.Controls.Add(new LiteralControl("<td style='text-transform:uppercase;'><b>" + GetColumnName(column) + "</b></td>"));
        }
        tablePlaceHolder.Controls.Add(new LiteralControl("</tr></thead>"));

        string trapID = string.Empty;
        bool newRecord = false;
        long loopcntr = -1;
        StringBuilder html = new StringBuilder();
        bool encodeTraps = false;
        bool.TryParse(ConfigurationManager.AppSettings["HTMLEncodeTraps"], out encodeTraps);
        foreach (DataRow row in dataTable.Rows)
        {
            string cls = string.Empty;// "class=\"Property\" ";

            if (string.IsNullOrEmpty(trapID) || trapID != row["TrapID"].ToString())
            {
                newRecord = true;
                loopcntr++;
                trapID = row["TrapID"].ToString();
            }
            else
            {
                newRecord = false;
            }

            string linkStart = string.Empty;
            string linkEnd = string.Empty;
            int nodeId;
            if (row["NodeID"] != null && row["NodeID"] != DBNull.Value && int.TryParse(row["NodeID"].ToString(), out nodeId) && nodeId > 0)
            {
                linkStart = string.Format("<a target=\"_parent\" href=\"/Orion/View.aspx?View=NodeDetails&NetObject=N:{0}\">", nodeId);
                linkEnd = "</a>";
            }

            if (newRecord)
            {
                string bgrColor;
                string rgbColor = string.Empty;
                int colorCode;
                if (int.TryParse(row["ColorCode"].ToString(), out colorCode))
                {
                    string hexColorCode = string.Format("{0:X6}", colorCode);
                    bgrColor = (string.IsNullOrEmpty(hexColorCode) || hexColorCode.Length >= 7) ? "000000" :
                        string.Format("{0:X6}", colorCode);
                    rgbColor = bgrColor.Substring(bgrColor.Length - 2, 2) + bgrColor.Substring(2, 2) + bgrColor.Substring(0, 2);
                }

                if (loopcntr == 0)
                {
                    html.AppendLine("<tr style=\"background-color:#" + rgbColor + ";\">");
                }
                else
                {
                    html.AppendLine("</td></tr><tr style=\"background-color:#" + rgbColor + ";\">");
                }
            }

            foreach (string column in Columns)
            {
                if (newRecord)
                {
                    switch (column.ToUpper(CultureInfo.InvariantCulture))
                    {
                        case "DATETIME":
                            DateTime dateTime;
                            if (DateTime.TryParse(row["DateTime"].ToString(), out dateTime))
                            {
                                html.AppendLine("<td " + cls + ">" + dateTime.ToString(CultureInfo.CurrentCulture) + "</td>");
                            }
                            else
                            {
                                html.AppendLine("<td " + cls + ">&nbsp;</td>");
                            }
                            break;
                        case "HOSTNAME":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td " + cls + ">" + linkStart + HttpUtility.HtmlEncode(row["Hostname"].ToString().Trim()) + linkEnd + "&nbsp;</td>");
                            }
                            else
                            {
                                html.AppendLine("<td " + cls + ">" + linkStart + row["Hostname"].ToString().Trim() + linkEnd + "&nbsp;</td>");
                            }
                            break;
                        case "CAPTION":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + linkStart + HttpUtility.HtmlEncode(row["Caption"].ToString().Trim()) + linkEnd + "&nbsp;</td>");
                            }
                            else
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + linkStart + row["Caption"].ToString().Trim() + linkEnd + "&nbsp;</td>");
                            }
                            break;
                        case "NODEID":
                            html.AppendLine("<td nowrap " + cls + ">" + row["NodeID"].ToString().Trim() + "&nbsp;</td>");
                            break;
                        case "DYNAMICIP":
                            html.AppendLine("<td " + cls + ">" + row["DynamicIP"].ToString().Trim() + "&nbsp;</td>");
                            break;
                        case "DNS":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + HttpUtility.HtmlEncode(row["DNS"].ToString().Trim()) + "&nbsp;</td>");
                            }
                            else
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + row["DNS"].ToString().Trim() + "&nbsp;</td>");
                            }
                            break;
                        case "COMMUNITY":
                            //If isAdmin Then mResponse.Write "    <td nowrap " & Class & ">" & Trim$(TB.Fields("Community").Value & "") & "&nbsp;</td>" & vbCrLf
                            if (Profile.AllowAdmin)
                            {
                                if (encodeTraps)
                                {
                                    html.AppendLine("<td nowrap " + cls + ">" + HttpUtility.HtmlEncode(row["Community"].ToString().Trim()) + "&nbsp;</td>");
                                }
                                else
                                {
                                    html.AppendLine("<td nowrap " + cls + ">" + row["Community"].ToString().Trim() + "&nbsp;</td>");
                                }
                            }
                            break;
                        case "SYSNAME":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + HttpUtility.HtmlEncode(row["SysName"].ToString().Trim()) + "&nbsp;</td>");
                            }
                            else
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + row["SysName"].ToString().Trim() + "&nbsp;</td>");
                            }
                            break;
                        case "VENDOR":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + HttpUtility.HtmlEncode(row["Vendor"].ToString().Trim()) + "&nbsp;</td>");
                            }
                            else
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + row["Vendor"].ToString().Trim() + "&nbsp;</td>");
                            }
                            break;
                        case "MACHINETYPE":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + HttpUtility.HtmlEncode(row["MachineType"].ToString().Trim()) + "&nbsp;</td>");
                            }
                            else
                            {
                                html.AppendLine("<td nowrap " + cls + ">" + row["MachineType"].ToString().Trim() + "&nbsp;</td>");
                            }
                            break;
                        case "TRAP DETAILS":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td>" + HttpUtility.HtmlEncode(row["OIDName"].ToString().Trim()) + " = " + HttpUtility.HtmlEncode(RemoveZeroWidthSpaceFromUrl(FormatHelper.MakeBreakableString(row["OIDValue"].ToString().Trim()))) + "&nbsp;<br/>");
                            }
                            else
                            {
                                html.AppendLine("<td>" + row["OIDName"].ToString().Trim() + " = " + RemoveZeroWidthSpaceFromUrl(FormatHelper.MakeBreakableString(row["OIDValue"].ToString().Trim())) + "&nbsp;<br/>");
                            }
                            break;
                        case "IP ADDRESS":
                            if (encodeTraps)
                            {
                                html.AppendLine("<td " + cls + ">" + HttpUtility.HtmlEncode(row["IPAddress"].ToString().Trim()) + "&nbsp;</td>");
                            }
                            else
                            {
                                html.AppendLine("<td " + cls + ">" + row["IPAddress"].ToString().Trim() + "&nbsp;</td>");
                            }
                            break;
                        case "TRAPTYPE":
                            html.AppendLine("<td " + cls + ">" + row["TrapType"].ToString().Trim() + "&nbsp;</td>");
                            break;
                    }
                }
                else
                {
                    if (column.Equals("TRAP DETAILS", StringComparison.OrdinalIgnoreCase))
                    {
                        if (encodeTraps)
                        {
                            html.AppendLine(HttpUtility.HtmlEncode(row["OIDName"].ToString().Trim()) + " = " + HttpUtility.HtmlEncode(RemoveZeroWidthSpaceFromUrl(FormatHelper.MakeBreakableString(row["OIDValue"].ToString().Trim()))) + "&nbsp;<br/>");
                        }
                        else
                        {
                            html.AppendLine(row["OIDName"].ToString().Trim() + " = " + RemoveZeroWidthSpaceFromUrl(FormatHelper.MakeBreakableString(row["OIDValue"].ToString().Trim())) + "&nbsp;<br/>");
                        }
                    }
                }
            }
        }
        tablePlaceHolder.Controls.Add(new LiteralControl(html.ToString()));
        tablePlaceHolder.Controls.Add(new LiteralControl("</table>"));
    }

    private string RemoveZeroWidthSpaceFromUrl(string url)
    {
        var resultUrl = url;

        //if trap details contain url - remove zero width space characters from it
        foreach (Match match in new Regex(@"\b(?:https?:\/\/|www\.)\S+\b").Matches(resultUrl))
        {
            var sanitizedUrl = match.ToString().Replace("\u200B", "");
            resultUrl = resultUrl.Replace(match.ToString(), sanitizedUrl);
        }

        return resultUrl;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}