using System;

public partial class Orion_Controls_NotePageRecipientsControl : System.Web.UI.UserControl
{
    public string Resipients { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        recipientsList.Value = string.IsNullOrEmpty(Resipients) ? string.Empty : Resipients;
    }
}