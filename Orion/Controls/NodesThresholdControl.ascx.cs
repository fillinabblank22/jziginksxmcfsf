﻿using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.InformationService;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Thresholds;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controllers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Model.Thresholds;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using InformationServiceProxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy;
using Node = SolarWinds.Orion.Core.Common.Models.Node;

public partial class NodesThresholdControl : System.Web.UI.UserControl, INodePropertyPlugin
{
    private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private IList<Node> _nodes;
    private string entityType = "Orion.Nodes";
    private string[] icmpThresholds = new string[] { "'Nodes.Stats.ResponseTime'", "'Nodes.Stats.PercentLoss'" };
    private DataTable ExistingThresholds;
    public string submitButton = "imbtnSubmit"; // used for suppress submit if some threshold is not validd
    private bool IsMultiEdit { get { return (_nodes.Count > 1); } }
    private string[] capacityPlanningEnabledFor = new string[] { "Nodes.Stats.CpuLoad", "Nodes.Stats.PercentMemoryUsed" };
    private Dictionary<string, int> thresholdDefaultOperators = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_nodes.Any(x => x.Status.Trim() == "11")) // External Node
        {
            return;
        }

        using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
        {
            DataTable thresholdsnames = service.Query(
                string.Format(
                    "SELECT [Name], [DisplayName], [DefaultThresholdOperator], [Unit] FROM Orion.ThresholdsNames WHERE [EntityType] = '{0}' ORDER BY [ThresholdOrder]",
                    entityType));

            thresholdDefaultOperators = thresholdsnames.Rows.Cast<DataRow>().ToDictionary(row => row["Name"].ToString(), row => Convert.ToInt32(row["DefaultThresholdOperator"]));

            if (!IsPostBack)
            {
                if (_nodes.Any(x => x.ObjectSubType == "ICMP"))
                {
                    thresholdsnames = thresholdsnames.Select(string.Format("[Name] in ({0})", string.Join(", ", icmpThresholds))).CopyToDataTable();
                }

                repThresholds.DataSource = thresholdsnames.Rows;
                repThresholds.DataBind();
            }
        }
    }

    private void LoadExistingThresholds()
    {
        using (InformationServiceProxyFactory factory = new InformationServiceProxyFactory())
        {
            using (IInformationServiceProxy2 proxy = factory.CreateConnection())
            {
                ExistingThresholds = proxy.Query(string.Format(@"
SELECT 
    ntm.[Name],
    ntm.[WarningEnabled], ntm.[Level1Value], ntm.[Level1Formula],
    ntm.[CriticalEnabled], ntm.[Level2Value], ntm.[Level2Formula],
    ntm.[InstanceId],
    ntm.[InstanceType],
    ntm.[ThresholdOperator],
    ntm.[ThresholdType],
    ntm.[EntityType],
    ntm.[CurrentValue],
    nt.[GlobalWarningValue],
    nt.[GlobalCriticalValue], 
    ntm.[WarningPolls],
    ntm.[WarningPollsInterval],
    ntm.[CriticalPolls],
    ntm.[CriticalPollsInterval]
FROM Orion.NodesThresholds ntm
INNER JOIN Orion.NodesThresholds nt ON nt.Name = ntm.Name and nt.InstanceID=0 and nt.InstanceType = ntm.InstanceType and ntm.EntityType = nt.EntityType
WHERE [EntityType] = @entityType AND [InstanceId] IN (0, {0})", string.Join(",", _nodes.Select(x => x.Id.ToString()).ToArray())),
                new Dictionary<string, object>
                {
                    {"entityType", entityType} 
                });
            }
        }
    }

    private bool IsExistingThreshold(Threshold threshold)
    {
        var condition = new StringBuilder();

        condition.AppendFormat("[EntityType] = '{0}' AND [InstanceId] = {1} AND [ThresholdType] = {2} AND [Name] = '{3}' ",
            entityType, threshold.InstanceId, (int)threshold.ThresholdType, threshold.ThresholdName);

        if (threshold.ThresholdType != ThresholdType.Global)
        {
            condition.Append($" AND [CriticalEnabled] = {threshold.CriticalEnabled}");
            condition.Append($" AND [WarningEnabled] = {threshold.WarningEnabled}");

            condition.AppendFormat(" AND [ThresholdOperator] = {0}", (int)threshold.ThresholdOperator);

            condition.Append($" AND [WarningPolls] = {threshold.WarningPolls}");
            condition.Append($" AND [WarningPollsInterval] = {threshold.WarningPollsInterval}");
            condition.Append($" AND [CriticalPolls] = {threshold.CriticalPolls}");
            condition.Append($" AND [CriticalPollsInterval] = {threshold.CriticalPollsInterval}");

            if (threshold.ThresholdType == ThresholdType.Static)
            {
                condition.AppendFormat(" AND [Level1Value] = {0} AND [Level2Value] = {1}",
                    ThresholdsHelper.FromNumeric(threshold.Warning),
                    ThresholdsHelper.FromNumeric(threshold.Critical));
            }

            if (threshold.ThresholdType == ThresholdType.Dynamic)
            {
                condition.AppendFormat(ToggleableThresholdCondition("Level1Formula", threshold.WarningFormula));
                condition.AppendFormat(ToggleableThresholdCondition("Level2Formula", threshold.CriticalFormula));
            }
        }

        return ExistingThresholds.Select(condition.ToString()).Any();
    }

    private string ToggleableThresholdCondition(string property, string value) 
        => string.IsNullOrEmpty(value) ? $" AND [{property}] = '{value}'" : $" AND [{property}] IS NULL";

    private DataRow GetRelatedThreshold(DataRow rowFromThresholdNames, int instanceId)
    {
        string selectQuery = string.Format("[Name] = '{0}' AND [InstanceId] = {1}", rowFromThresholdNames["Name"], instanceId);
        DataRow[] result = ExistingThresholds.Select(selectQuery);

        return result.Count() > 0 ? result[0] : null;
    }

    private void AddOnSubmitValidating(string thresholdIsValidHfClientId, string thresholdName)
    {
        ScriptManager.RegisterOnSubmitStatement(
            this,
            this.GetType(),
            "ValidateThreshold_" + thresholdName.Replace(".", "_"),
            "if ($('#" + this.hfFlagSubmit.ClientID + "').val() == '1' && $('#" + thresholdIsValidHfClientId + "').val() == '0') return false;" + Environment.NewLine);
    }

    protected void repThresholds_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ThresholdControlExt control = e.Item.FindControl("thresholdControl") as ThresholdControlExt;

            if (control == null)
                return;

            control.ObjectIDs = string.Join(" ", _nodes.Select(x => x.ID.ToString()).ToArray());
            control.IsMultiedit = _nodes.Count > 1;
            AddOnSubmitValidating(control.IsValidFieldClientId, control.ThresholdName);

            if (IsMultiEdit)
            {
                DataRow generalThreshold = GetRelatedThreshold(((DataRow)e.Item.DataItem), 0);

                if (generalThreshold != null)
                {
                    SetGlobalThresholdValues(
                        control,
                        Convert.ToDouble(generalThreshold["GlobalWarningValue"]),
                        Convert.ToDouble(generalThreshold["GlobalCriticalValue"]),
                        (ThresholdOperatorEnum)thresholdDefaultOperators[generalThreshold["Name"].ToString()]);
                }
            }
            else
            {
                DataRow rowThreshold = GetRelatedThreshold(((DataRow)e.Item.DataItem), _nodes[0].ID);

                if (rowThreshold != null)
                {
                    ThresholdType thrType = (ThresholdType)rowThreshold["ThresholdType"];
                    ThresholdOperatorEnum thrOperator = (ThresholdOperatorEnum)rowThreshold["ThresholdOperator"];

                    control.ThisThresholdType = thrType;
                    control.ThisThresholdOperator = thrOperator;

                    SetGlobalThresholdValues(
                        control,
                        Convert.ToDouble(rowThreshold["GlobalWarningValue"]),
                        Convert.ToDouble(rowThreshold["GlobalCriticalValue"]),
                        (ThresholdOperatorEnum)thresholdDefaultOperators[rowThreshold["Name"].ToString()]);

                    if (thrType == ThresholdType.Global)
                    {
                        control.WarningPollsValue = ThresholdsHelper.FromNumeric(1);
                        control.WarningPollsIntervalValue = ThresholdsHelper.FromNumeric(1);
                        control.CriticalPollsValue = ThresholdsHelper.FromNumeric(1);
                        control.CriticalPollsIntervalValue = ThresholdsHelper.FromNumeric(1);
                    }
                    else
                    {
                        control.WarningPollsValue = ThresholdsHelper.FromNumeric((int)rowThreshold["WarningPolls"]);
                        control.WarningPollsIntervalValue = ThresholdsHelper.FromNumeric((int)rowThreshold["WarningPollsInterval"]);
                        control.CriticalPollsValue = ThresholdsHelper.FromNumeric((int)rowThreshold["CriticalPolls"]);
                        control.CriticalPollsIntervalValue = ThresholdsHelper.FromNumeric((int)rowThreshold["CriticalPollsInterval"]);
                    }

                    control.IsWarningEnabled = Convert.ToBoolean(rowThreshold["WarningEnabled"]);
                    control.IsCriticalEnabled = Convert.ToBoolean(rowThreshold["CriticalEnabled"]);

                    if (thrType == ThresholdType.Static)
                    {
                        control.WarningValue = ThresholdsHelper.FromNumeric(Convert.ToDouble(rowThreshold["Level1Value"]));
                        control.CriticalValue = ThresholdsHelper.FromNumeric(Convert.ToDouble(rowThreshold["Level2Value"]));
                    }

                    if (thrType == ThresholdType.Dynamic)
                    {
                        control.WarningValue = Convert.ToString(rowThreshold["Level1Formula"]) ?? string.Empty;
                        control.CriticalValue = Convert.ToString(rowThreshold["Level2Formula"]) ?? string.Empty;
                    }
                }
            }

            // Capacity planning setup is same for both multi and single edit
            if (capacityPlanningEnabledFor.Contains(control.ThresholdName))
            {
                control.EnableCapacityPlanning = true;
                control.CapacityPlanningType = ForecastingCapacityDAL.TryGetCapacityPlanningType(_nodes.FirstOrDefault().ID, control.ThresholdName);
            }
        }
    }

    private void SetGlobalThresholdValues(ThresholdControlExt control, double warning, double critical, ThresholdOperatorEnum op)
    {
        control.GlobalThresholdOperator = op;
        control.GlobalWarningValue = Convert.ToString(warning);
        control.GlobalCriticalValue = Convert.ToString(critical);

        control.IsGlobalWarningEnabled = ThresholdsHelper.CalculateEnabled(warning, control.GlobalThresholdOperator);
        control.IsGlobalCriticalEnabled = ThresholdsHelper.CalculateEnabled(critical, control.GlobalThresholdOperator);
    }

    private static Threshold ComputeThresholdValues(Threshold threshold)
    {
        try
        {
            var request = new ComputeSustainedThresholdRequest
            {
                ThresholdName = threshold.ThresholdName,
                InstancesId = new[] { threshold.InstanceId },
                Operator = threshold.ThresholdOperator,
                Warning = new ComputeSustainedThresholdRequest.ThresholdData
                {
                    Enabled = threshold.WarningEnabled,
                    Formula = threshold.WarningFormula,
                    Sustained = new ComputeSustainedThresholdRequest.SustainedData
                    {
                        Polls = ThresholdsHelper.FromNumeric(1),
                        Interval = ThresholdsHelper.FromNumeric(1)
                    }
                },
                Critical = new ComputeSustainedThresholdRequest.ThresholdData
                {
                    Enabled = threshold.CriticalEnabled,
                    Formula = threshold.CriticalFormula,
                    Sustained = new ComputeSustainedThresholdRequest.SustainedData
                    {
                        Polls = ThresholdsHelper.FromNumeric(1),
                        Interval = ThresholdsHelper.FromNumeric(1)
                    }
                }
            };

            ComputeThresholdResponse response = new ThresholdsController().ComputeExt(request);

            if (response.IsComputed)
            {
                threshold.Warning = response.WarningThreshold;
                threshold.Critical = response.CriticalThreshold;
            }
        }
        catch (Exception e)
        {
            _log.Error(string.Format("Can't compute current values for threshold {0}.", threshold), e);
        }

        return threshold;
    }

    #region interface INodePropertyPlugin

    public void Initialize(IList<SolarWinds.Orion.Core.Common.Models.Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        _nodes = nodes;
        LoadExistingThresholds();
    }

    public bool Validate()
    {
        return true;
    }

    private int ParseSustainedValue(string input, bool enabled)
    {
        int result;
        return enabled && int.TryParse(input, out result) ? result : 1;
    }

    public bool Update()
    {
        List<Threshold> thresholds = new List<Threshold>();

        foreach (RepeaterItem repItem in repThresholds.Items)
        {
            ThresholdControlExt control = repItem.FindControl("thresholdControl") as ThresholdControlExt;

            if (control.IsMultiedit)
            {
                if (!control.OverrideMultipleObjects)
                {
                    continue;
                }
            }

            if (control.ThisThresholdType != ThresholdType.Global)
            {

                int[] invalidInstances = IsMultiEdit ? (string.IsNullOrEmpty(control.InvalidInstances) ? "-1" : control.InvalidInstances).Split(',').Select<string, int>(int.Parse).ToArray() : new int[] { -1 };

                foreach (var node in _nodes)
                {
                    Node nodeTmp = node;

                    if (nodeTmp.Id == 0)
                    {
                        nodeTmp = NodeWorkflowHelper.Node;
                    }

                    if (nodeTmp.Id == 0)
                    {
                        continue;
                    }

                    if (IsMultiEdit && invalidInstances.Contains(nodeTmp.Id))
                    {
                        continue;
                    }

                    double warningValue;
                    double criticalValue;
                    bool bothNumeric = 
                        ThresholdsHelper.IsNumeric(control.WarningValue, control.IsWarningEnabled, out warningValue) &
                        ThresholdsHelper.IsNumeric(control.CriticalValue, control.IsCriticalEnabled, out criticalValue);

                    Threshold threshold = new Threshold()
                    {
                        InstanceId = nodeTmp.Id,
                        ThresholdName = control.ThresholdName,
                        ThresholdOperator = control.ThisThresholdOperator,
                        ThresholdType = (bothNumeric ? ThresholdType.Static : ThresholdType.Dynamic),
                        WarningEnabled = control.IsWarningEnabled,
                        CriticalEnabled = control.IsCriticalEnabled,
                        Warning = null,
                        Critical = null,
                        WarningFormula = null,
                        CriticalFormula = null,
                        WarningPolls = ParseSustainedValue(control.WarningPollsValue, control.IsWarningEnabled),
                        WarningPollsInterval = ParseSustainedValue(control.WarningPollsIntervalValue, control.IsWarningEnabled),
                        CriticalPolls = ParseSustainedValue(control.CriticalPollsValue, control.IsCriticalEnabled),
                        CriticalPollsInterval = ParseSustainedValue(control.CriticalPollsIntervalValue, control.IsCriticalEnabled)
                    };

                    if (threshold.ThresholdType == ThresholdType.Dynamic)
                    {
                        threshold.WarningFormula = control.IsWarningEnabled ? control.WarningValue : null;
                        threshold.CriticalFormula = control.IsCriticalEnabled ? control.CriticalValue : null;

                        threshold = ComputeThresholdValues(threshold);
                        thresholds.Add(threshold);
                    }
                    else if (threshold.ThresholdType == ThresholdType.Static)
                    {
                        var disabledValue = ThresholdsHelper.GetDisabledValue(control.ThisThresholdOperator);
                        threshold.Warning = control.IsWarningEnabled ? warningValue : disabledValue;
                        threshold.Critical = control.IsCriticalEnabled ? criticalValue : disabledValue;
                        thresholds.Add(threshold);
                    }

                    // Store the capacity planning
                    if (control.EnableCapacityPlanning && control.CapacityPlanningType != PlanningType.None)
                    {
                        ForecastingCapacityDAL.SetCapacityPlanningType(nodeTmp.Id, control.ThresholdName, control.CapacityPlanningType);
                    }
                }
            }
            else
            {
                foreach (var node in _nodes)
                {
                    Threshold threshold = new Threshold()
                    {
                        InstanceId = node.Id,
                        ThresholdName = control.ThresholdName,
                        ThresholdType = ThresholdType.Global,
                        ThresholdOperator = control.GlobalThresholdOperator,
                        WarningEnabled = control.IsGlobalWarningEnabled,
                        CriticalEnabled = control.IsGlobalCriticalEnabled,
                        Warning = null,
                        Critical = null,
                        WarningFormula = null,
                        CriticalFormula = null,
                        WarningPolls = 1,
                        WarningPollsInterval = 1,
                        CriticalPolls = 1,
                        CriticalPollsInterval = 1
                    };

                    thresholds.Add(threshold);

                    // Remove the capacity planning when the override checkbox isn't selected
                    if (control.EnableCapacityPlanning)
                    {
                        ForecastingCapacityDAL.SetCapacityPlanningType(node.Id, control.ThresholdName, PlanningType.None);
                    }
                }
            }
        }

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            foreach (var threshold in thresholds)
            {
                if (!IsExistingThreshold(threshold))
                {
                    proxy.SetThreshold(threshold);
                }
            }
        }

        return true;
    }

    #endregion
}