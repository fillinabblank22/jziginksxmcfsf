using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DrawColor = System.Drawing.Color;


/// <summary> Control that display menu used on the page. An example is on Network tab inside Discovery workflow. </summary>
public partial class Orion_Controls_PageMenu : System.Web.UI.UserControl
{
    private TitleSummary _title;

    public MenuItemCollection Items
	{
        get { return this.PageMenu.Items; }
    }

	public MenuItem SelectedItem
	{
		get { return this.PageMenu.SelectedItem; }
	}

	public string PostBackControlId
	{
		get { return this.PageMenu.UniqueID; }
	}

    public TitleSummary Title
    {
        get
        {
            if(_title == null) 
               _title = new TitleSummary(imgTitle, lblTitle, panTitle);
            _title.Image.Visible = string.IsNullOrEmpty(_title.TextLabel.Text);
            return _title;
        }
    }

	public event MenuEventHandler Clicked;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
    }

    /// <summary> Called when menu item is clicked </summary>
    protected void PageMenu_Clicked(object sender, MenuEventArgs e)
    {
        if (Clicked != null){
            Clicked(sender, e);
        }
    }

    /// <summary>
    /// class for grouping and customizing settings of the Title
    /// </summary>
    public class TitleSummary
    {
        #region private fields
        private Image _imgTitle;
        private Label _lblTitle;
        private Panel _panTitle;
        #endregion

        #region public properties
        /// <summary>
        /// Image for title. 
        /// Note: If used Image, then TextTitle will not showing.
        /// </summary>
        public Image Image
        {
            get { return _imgTitle; }
        }

        /// <summary>
        /// Text for title. 
        /// Note: If used text, then ImageTitle will not showing.
        /// </summary>
        public Label TextLabel
        {
            get { return _lblTitle; }
        }

        /// <summary>
        /// Background image url for text
        /// </summary>
        public string TextBackImageUrl
        {
            get { return _panTitle.BackImageUrl; }
            set { _panTitle.BackImageUrl = value; }
        }

        /// <summary>
        /// Background color for text
        /// </summary>
        public DrawColor TextBackColor
        {
            get { return _panTitle.BackColor; }
            set { _panTitle.BackColor = value; }
        }

        /// <summary>
        /// CSS Class for container
        /// </summary>
        public string CSSClass
        {
            get { return _panTitle.CssClass; }
            set { _panTitle.CssClass = value; }
        }

        #endregion

        public TitleSummary(Image imgTitle, Label lblTitle, Panel panTitle)
        {
            _imgTitle = imgTitle;
            _lblTitle = lblTitle;
            _panTitle = panTitle;
            _lblTitle.ForeColor = DrawColor.White;
        }
    }
}
