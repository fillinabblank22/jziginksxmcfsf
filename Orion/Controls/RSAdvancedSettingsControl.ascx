<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RSAdvancedSettingsControl.ascx.cs" Inherits="Orion_Controls_RSAdvancedSettings" %>
<%@ Register TagPrefix="orion" TagName="UserAccountControl" Src="~/Orion/Controls/UserAccountControl.ascx" %>
<style type="text/css">   
    #userCredentialsSelectionContainer {margin-left: 25px; padding: 10px 10px 10px 4px;}
</style>
<script type="text/javascript">
    function collapseExpandUserCreds() {
        var div = document.getElementById('advancedSettings');
        if (div.style.display == "none") {
            div.style.display = "block";
            $("#collapseExpandButton").attr('src', '/Orion/images/Button.Collapse.gif');
        }
        else {
            div.style.display = "none";
            $("#collapseExpandButton").attr('src', '/Orion/images/Button.Expand.gif');
        }
    }
</script>
        <span style="margin-left: 5px;"><img alt="" style="margin: 0 5px -2px 5px;" id = "collapseExpandButton" src="/Orion/images/Button.Collapse.gif" onclick="collapseExpandUserCreds();"/><label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_YK0_30) %></label></span> 
        <div id="advancedSettings">
        <div id="userCredentialsSelectionContainer">
        <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_53) %><img alt="" ext:qtitle="" ext:qtip="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_71) %>" style="margin: 0 5px -3px 5px;" src="/Orion/images/Icon.Info.gif"/></span>
        <orion:UserAccountControl AllowNoUser="false" ID="userAccount" runat="server"/>
        </div>
        <div id="websitePicker" runat="server" style="margin-left: 25px;padding: 5px;">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_86) %><img alt="" ext:qtitle="" ext:qtip="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_87) %>" style="margin: 0 5px -3px 5px;" src="/Orion/images/Icon.Info.gif"/></span>
            <br/>
            <asp:DropDownList ID="selectWebsite" runat="server" Width="225px">
            </asp:DropDownList>
        </div>        
        </div>      



