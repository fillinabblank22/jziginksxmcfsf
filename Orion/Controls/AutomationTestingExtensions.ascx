﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutomationTestingExtensions.ascx.cs" Inherits="Orion_Controls_AutomationTestingExtensions" %>
<script type="text/javascript">
<% if (SolarWinds.Orion.Common.OrionConfiguration.IsAutomationTestServer) {%>
    //Ajax requests
    var tmpAutomationTestExtensions = {
        numberOfActiveRequests: 0,
        activeRequestUrls: [],
		waitingStarted: false,
		waitingFinished: false,
		numberOfActiveJS: 0,

        onAjaxSend: function (url) {
            SW_AUTOMATION_TEST_EXTENSIONS.numberOfActiveRequests++;
            SW_AUTOMATION_TEST_EXTENSIONS.activeRequestUrls.push(url);
            SW_AUTOMATION_TEST_EXTENSIONS.updateActiveRequestInfo();
        },

        onAjaxComplete: function (url) {
            var i;
            var found = false;
            for (i = 0; i < SW_AUTOMATION_TEST_EXTENSIONS.activeRequestUrls.length; i++) {
                if (SW_AUTOMATION_TEST_EXTENSIONS.activeRequestUrls[i] == url) {
                    found = true;
                    break;
                }
            }
            if (found) {
				SW_AUTOMATION_TEST_EXTENSIONS.numberOfActiveRequests--;
                SW_AUTOMATION_TEST_EXTENSIONS.activeRequestUrls.splice(i, 1);
            }
            SW_AUTOMATION_TEST_EXTENSIONS.updateActiveRequestInfo();
			SW_AUTOMATION_TEST_EXTENSIONS.clickCalled();
        },

        updateActiveRequestInfo: function () {
            $('#activeRequestUrlsHidden').val(SW_AUTOMATION_TEST_EXTENSIONS.activeRequestUrls.join(';'));
        },
		
		clickCalled: function() {
			SW_AUTOMATION_TEST_EXTENSIONS.waitingStarted = true;
			SW_AUTOMATION_TEST_EXTENSIONS.waitingFinished = false;
			SW_AUTOMATION_TEST_EXTENSIONS.updateClickHistory('started:' + new Date().getTime(), SW_AUTOMATION_TEST_EXTENSIONS.numberOfActiveJS == 0);
			SW_AUTOMATION_TEST_EXTENSIONS.numberOfActiveJS++;
			setTimeout('SW_AUTOMATION_TEST_EXTENSIONS.executionEnds();',0);
		},
		
		executionEnds: function() {
			SW_AUTOMATION_TEST_EXTENSIONS.waitingStarted = false;
			SW_AUTOMATION_TEST_EXTENSIONS.waitingFinished = true;
			SW_AUTOMATION_TEST_EXTENSIONS.numberOfActiveJS--;
			SW_AUTOMATION_TEST_EXTENSIONS.updateClickHistory('finished:' + new Date().getTime(), false);
		},
		
		updateClickHistory: function(val, clear) {
			var cur = $('#clickHistoryHidden').val();
			if (clear) {
				cur = val;
			}
			else {
				cur = cur + ';' + val;
			}
			$('#clickHistoryHidden').val(cur);
		}
    }

    //jQuery Ajax
    $(document).ajaxSend(function (event, request, options) {
        if (typeof (SW_AUTOMATION_TEST_EXTENSIONS) != "undefined")
            SW_AUTOMATION_TEST_EXTENSIONS.onAjaxSend(options.url);
    });

    $(document).ajaxComplete(function (event, request, options) {
        if (typeof (SW_AUTOMATION_TEST_EXTENSIONS) != "undefined")
            SW_AUTOMATION_TEST_EXTENSIONS.onAjaxComplete(options.url);
    });
	
    $(document).ajaxError(function (event, request, options) {
        if (typeof (SW_AUTOMATION_TEST_EXTENSIONS) != "undefined")
            SW_AUTOMATION_TEST_EXTENSIONS.onAjaxComplete(options.url);
    });

    $(document).ready(function () {
		if (typeof (window.SW_AUTOMATION_TEST_EXTENSIONS) == "undefined") {
			window.SW_AUTOMATION_TEST_EXTENSIONS = tmpAutomationTestExtensions;
		} else {
			window.SW_AUTOMATION_TEST_EXTENSIONS = $.extend(window.SW_AUTOMATION_TEST_EXTENSIONS, tmpAutomationTestExtensions);
		}
		//MS Ajax
		if (typeof (Sys) !== 'undefined') {
			Sys.Net.WebRequestManager.add_invokingRequest(function (sender, eventArgs) {
				SW_AUTOMATION_TEST_EXTENSIONS.onAjaxSend(eventArgs.get_webRequest().get_url());
			});

			Sys.Net.WebRequestManager.add_completedRequest(function (sender, eventArgs) {
				SW_AUTOMATION_TEST_EXTENSIONS.onAjaxComplete(sender.get_webRequest().get_url());
			});
		}
		
        $("body").append('<input id="activeRequestUrlsHidden" type="hidden" />');
		$("body").append('<input id="clickHistoryHidden" type="hidden" />');
    });
    <%} %>
</script>