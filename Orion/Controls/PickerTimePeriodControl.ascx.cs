using System;
using System.Web.UI.WebControls;
using System.Text;

using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;

public partial class PickerTimePeriodControl : System.Web.UI.UserControl
{
	private ListControl _sampleSizeList;
	private bool _inLine = false;

	public bool InLine
	{
		get { return _inLine; }
		set { _inLine = value; }
	}
	public void AttachSampleSizeControl(ListControl SampleSizeControl)
	{
		_sampleSizeList = SampleSizeControl; 
	}
	
	protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ShowHideCustomPeriodEditor",
@"(function($) {
    var TimePeriodListId = '" + TimePeriodList.ClientID + @"';
    var CustomPeriodEditorId = '" + CustomPeriodEditor.ClientID + @"';

	$(function() {
        $('#' + TimePeriodListId).change(function () {
            var editor = $('#' + CustomPeriodEditorId);
            if ($(this).val() == 13)
                editor.show();
            else
                editor.hide();
        }).change();
	});
})(jQuery);
", true);

        if (TimePeriodValue != "13")
		{
			CustomPeriodBegin = string.Empty;
			CustomPeriodEnd = string.Empty;
		}
        this.PreRender += new EventHandler(PickerTimePeriodControl_PreRender);
	}

	#region Check Sample Size
    void PickerTimePeriodControl_PreRender(object sender, EventArgs e)
	{
		if (_sampleSizeList != null)
		{
			System.Web.UI.ScriptManager.RegisterClientScriptBlock( this.Page, this.GetType(), "TimePeriodInitialScript", GetInitializeScript(), true);
			TimePeriodList.Attributes.Add("onChange","CheckTimePeriods()");
            SolarWinds.Orion.Web.OrionInclude.CoreFile("TimePeriodScript.js");
        }
		
	}

	private string GetInitializeScript() 
	{
		if (ViewState["PickerTimePeriodInitialScript"] == null) 
		{
			StringBuilder initializescript = new StringBuilder();
			string ids = string.Empty; 
			string values = string.Empty;
			GetTimePeriods(ref ids, ref values);
			initializescript.Append(ids);
			initializescript.Append(values);
			initializescript.Append(GetTimeRelations());
			initializescript.Append("$(function(){");
			initializescript.AppendFormat("window.timePeriodControl = document.getElementById('{0}');", TimePeriodList.ClientID);
			initializescript.AppendFormat("window.sampleTimePeriodControl = document.getElementById('{0}');", _sampleSizeList.ClientID);
			initializescript.Append("$(CheckTimePeriods);});");
			ViewState["PickerTimePeriodInitialScript"] = initializescript.ToString();
		}
		return ViewState["PickerTimePeriodInitialScript"].ToString();
	}

	private void GetTimePeriods(ref string IDs, ref string Values) 
	{
		StringBuilder _ids = new StringBuilder("var sampleTimeIDS = [");
		StringBuilder _values = new StringBuilder("var sampleTimeValue = [");
		for (int i = 0; i < _sampleSizeList.Items.Count; i++ )
		{
			_ids.Append(string.Format("{0}'{1}'", (i==0)?"":",", _sampleSizeList.Items[i].Value));
			_values.Append(string.Format("{0}'{1}'", (i == 0) ? "" : ",", _sampleSizeList.Items[i].Text));
		}
		_ids.Append("];");
		_values.Append("];");
		IDs = _ids.ToString();
		Values = _values.ToString();
	}

	private string GetTimeRelations()
	{
		StringBuilder result = new StringBuilder("var sampleTimeRelations = [");
		for (int i = 0; i < TimePeriodList.Items.Count; i++)
		{
			StringBuilder samplesizeassigned = new StringBuilder("[");
            string timePeriod = TimePeriod.GetPreset(Convert.ToInt32(TimePeriodList.Items[i].Value)).KeyText.ToUpperInvariant();
			string separator = string.Empty;
			foreach (ListItem sli in _sampleSizeList.Items)
			{
				if  ((timePeriod == "CUSTOM") || (CheckTimePeriodValue(timePeriod, sli.Value.ToUpperInvariant())))  
				{
					samplesizeassigned.Append(string.Format("{0}'{1}'", separator, sli.Value));
					separator = ",";
				}
			}
			samplesizeassigned.Append("]");
			result.Append(string.Format("{0}{1}", ((i == 0) ? "" : ","), samplesizeassigned.ToString()));
		}
		result.Append("];");
		return result.ToString();
	}
	
	
	private bool CheckTimePeriodValue(string TimePeriod, string SampleSize) 
	{
			DateTime _startTime = new DateTime();
			DateTime _endTime = new DateTime();
			Periods.Parse(ref TimePeriod, ref _startTime, ref _endTime);
			double numberOfSamples = _endTime.Subtract(_startTime).TotalMinutes / Periods.ConvertIntervalToMinutes(SampleSize);
			return ((numberOfSamples <= 3000) && (numberOfSamples >= 2));
		}
	#endregion

		private void SetupControlFromRequest()
	{
		if (Request["Period"] != null)
		{
			switch (Request["Period"].ToUpperInvariant())
			{
				case "LAST HOUR":
				case "PAST HOUR":
					TimePeriodValue = "1";
					break;
				case "LAST 2 HOURS":
					TimePeriodValue = "2";
					break;
				case "LAST 24 HOURS":
					TimePeriodValue = "3";
					break;
				case "TODAY":
					TimePeriodValue = "4";
					break;
				case "YESTERDAY":
					TimePeriodValue = "5";
					break;
				case "LAST 7 DAYS":
					TimePeriodValue = "6";
					break;
				case "THIS MONTH":
					TimePeriodValue = "7";
					break;
				case "LAST MONTH":
					TimePeriodValue = "8";
					break;
				case "LAST 30 DAYS":
					TimePeriodValue = "9";
					break;
				case "LAST 3 MONTHS":
					TimePeriodValue = "10";
					break;
				case "THIS YEAR":
					TimePeriodValue = "11";
					break;
				case "LAST 12 MONTHS":
					TimePeriodValue = "12";
					break;
				case "CUSTOM":
					TimePeriodValue = "13";
					break;
				default:
					if (Request["Period"].Contains("~"))
					{
						TimePeriodValue = "13";
						string[] periods = Request["Period"].Split('~');
						CustomPeriodBegin = periods[0];
						CustomPeriodEnd = periods[1];
					}
					else
					{
						TimePeriodValue = "4";
					}
					break; 
			}
		}

		CustomPeriodBegin = (Request["PeriodBegin"] != null) ? Request["PeriodBegin"].ToString() : CustomPeriodBegin;
		CustomPeriodEnd = (Request["PeriodEnd"] != null) ? Request["PeriodEnd"].ToString() : CustomPeriodEnd;
	}

	protected override void OnInit(EventArgs e)
	{
		InitTimePeriodList();

		if (!Page.IsPostBack)
		{
			SetupControlFromRequest();
		}

		base.OnInit(e);
	}

	public string TimePeriodText
	{
		get
		{
			return TimePeriod.GetPreset(Convert.ToInt32(TimePeriodList.Items[TimePeriodList.SelectedIndex].Value)).KeyText;
		}
		set
		{
            if (!string.IsNullOrEmpty(value))
            {
                string localized = Periods.GetLocalizedPeriod(value);
				ListItem item = TimePeriodList.Items.FindByText(localized); 
				if (item == null)
				foreach (ListItem li in TimePeriodList.Items) 
					if (li.Text.Equals(localized, StringComparison.InvariantCultureIgnoreCase)) 
					{
						item = li;
						break;
					}
                if (item != null)
                {
                    TimePeriodList.SelectedValue = item.Value;
                }
            }
		}
	}

	public string TimePeriodValue
	{
		get
		{
			return TimePeriodList.Items[TimePeriodList.SelectedIndex].Value;
		}
		set
		{
			TimePeriodList.SelectedValue = value;
		}
	}

	public string CustomPeriodBegin
	{
		get
		{
            if (dtPeriodBegin.Value == DateTime.MinValue)
            {
                return string.Empty;
            }
            else
            {
                return dtPeriodBegin.Value.ToString();
            }
		}
		set
		{
            if (!string.IsNullOrEmpty(value))
            {
                dtPeriodBegin.Value = Convert.ToDateTime(value, System.Globalization.CultureInfo.CurrentCulture);
            }
		}
	}

	public string CustomPeriodEnd
	{
		get
		{
            if (dtPeriodEnd.Value == DateTime.MinValue)
            {
                return string.Empty;
            }
            else
            {
                return dtPeriodEnd.Value.ToString();
            }
        }
		set
		{
            if (!string.IsNullOrEmpty(value))
            {
                dtPeriodEnd.Value = Convert.ToDateTime(value, System.Globalization.CultureInfo.CurrentCulture);
            }
        }
	}

	private void InitTimePeriodList()
	{
		TimePeriodList.DataSource = TimePeriod.GetTimePeriodList();
		TimePeriodList.DataTextField = "LocalizedText";
		TimePeriodList.DataValueField = "ID";
		TimePeriodList.DataBind();
		TimePeriodList.SelectedIndex = 3;
	}
	protected void BeginDateValidator_ServerValidate(object source, ServerValidateEventArgs args)
	{
		try
		{
            DateTime periodBegin = dtPeriodBegin.Value;
			CustomPeriodBegin = periodBegin.ToString();
			args.IsValid = true;

		}
		catch
		{
			args.IsValid = false;
		}
	}
	protected void EndDateValidator_ServerValidate(object source, ServerValidateEventArgs args)
	{
		try
		{
            DateTime periodEnd = dtPeriodEnd.Value;
			CustomPeriodEnd = periodEnd.ToString();
			args.IsValid = true;
		}
		catch
		{
			args.IsValid = false;
		}
	}

	protected void Validator_ServerValidate(object source, ServerValidateEventArgs args)
	{
		try
		{
			DateTime periodBegin = dtPeriodBegin.Value;
			DateTime periodEnd = dtPeriodEnd.Value;

			if (periodBegin.CompareTo(periodEnd) < 0)
			{
				args.IsValid = true;
			}
			else
			{
				args.IsValid = false;
			}
		}
		catch
		{
            args.IsValid = false;
		}
	}
}
