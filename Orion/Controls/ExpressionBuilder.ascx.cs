﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SolarWinds.Logging;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Models.Alerting;


public partial class Orion_Controls_ExpressionBuilder : System.Web.UI.UserControl
{
	public bool IsMaster { get; set; }
    public string RootNodeTitle { get; set; }

    public string ObjectType { get; set; }
    public string EntityFullName { get; set; }
    public string ExprTree { get; set; }
    public string FieldPickerDataProviderUrl { get; set; }
    public string DataProviderUrl { get; set; }
    public bool ShowOnlyNonTransientFields { get; set; } 
    public string ClientIdPrefix { get; set; }

    private string clientInstanceName;
    public string ClientInstanceName
    {
        get
        {
            if (string.IsNullOrEmpty(clientInstanceName))
                return this.ID;
            else
                return this.clientInstanceName;
        }
        set 
        { 
            clientInstanceName = value;
            FieldPicker.ClientInstanceName = "fieldPickerInput_Instance" + value;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
	    FieldPicker.ExtraRequestParams = new Dictionary<string, object> {{"ObjectType", "Orion.Nodes"}};
	    FieldPicker.DataProviderUrl = this.FieldPickerDataProviderUrl ?? "/Orion/Services/AlertingFieldPickerDataProvider.asmx";

	    if (String.IsNullOrEmpty(DataProviderUrl))
	        DataProviderUrl = "/api/ExpressionBuilderData/";
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        FieldPicker.ClientInstanceName = FieldPicker.ClientInstanceName + ClientInstanceName;
	}

	public string GetFieldConstantConditionType()
	{
        var expr = new JObject
            {
                {"NodeType", (int)ExprType.Operator},
                {"Child", new JArray {
                     new JObject{{"NodeType", (int)ExprType.Field}, {"ConditionType", "Simple"} },
                     new JObject{{"NodeType", (int)ExprType.Constant}, {"ConditionType", "Simple"} },
                  }
                }
            };

        return JsonConvert.SerializeObject(expr);
	}

	public string GetFieldFieldConditionType()
	{
        var expr = new JObject
            {
                {"NodeType", (int)ExprType.Operator},
                {"Child", new JArray {
                     new JObject{{"NodeType", (int)ExprType.Field}, {"ConditionType", "Advanced"} },
                     new JObject{{"NodeType", (int)ExprType.Field}, {"ConditionType", "Advanced"} },
                  }
                },
                
            };

        return JsonConvert.SerializeObject(expr);
	}

    public string GetConstConstConditionType()
    {
        var expr = new JObject
            {
                {"NodeType", (int)ExprType.Operator},
                {"Child", new JArray {
                     new JObject{{"NodeType", (int)ExprType.Constant}, {"ConditionType", "AdvancedConst"} },
                     new JObject{{"NodeType", (int)ExprType.Constant}, {"ConditionType", "AdvancedConst"} },
                  }
                },
                
            };

        return JsonConvert.SerializeObject(expr);
    }

    public string GetEventConditionType()
    {
        var expr = new JObject
            {
                {"NodeType", (int)ExprType.Event},
                {"Child", new JArray {
                     new JObject{{"NodeType", (int)ExprType.Constant}, {"Value", "true"} },
                     new JObject{{"NodeType", (int)ExprType.Constant}, {"Value", "0"} },
                     new JObject{{"NodeType", (int)ExprType.Constant}, {"Value", "0"} },
                     new JObject{{"NodeType", (int)ExprType.Operator}, {"Value", "AND"}, { "Child", 
                            new JArray {
                                new JObject
                                    {
                                        {"NodeType", (int)ExprType.Operator},
                                        {"Child", new JArray {
                                             new JObject{{"NodeType", (int)ExprType.Field}, {"ConditionType", "Simple"} },
                                             new JObject{{"NodeType", (int)ExprType.Constant}, {"ConditionType", "Simple"} },
                                          }
                                        }
                                    } 
                            }}} 
                }}
             };

        return JsonConvert.SerializeObject(expr);
    }

	public string ConditionTitle
	{
		set { conditionTitle.InnerText = value; }
	}
}
