<%@ Control Language="C#" ClassName="MaintenanceSchedulerDialog" CodeFile="MaintenanceSchedulerDialog.ascx.cs" Inherits="Orion_Controls_MaintenanceSchedulerDialog" %>
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>
<%@ Register Src="~/Orion/Controls/TimeZonePicker.ascx" TagPrefix="orion" TagName="TimeZonePicker" %>

<%  var printable = false;
    bool.TryParse(Request["Printable"], out printable);
    if (!printable)
    { %>
<script type="text/javascript">
//<![CDATA[
$(function () {
    var maintenanceModeNamespace = SW.Core.namespace('SW.Core.MaintenanceMode');

    var width, height;
    var minWidth = 550, minHeight = 'auto';

    var maintenanceSchedulerDialog = (function () {

        var localDateTime = new Date();

        var getDefaultConfig = function () {
            return {
                title: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_LH0_25) %>",
                schedules: [
                     {
                         key: "alertSuppression",
                         from: localDateTime,
                         until: new Date(localDateTime.getTime() + 24 * 60 * 60 * 1000)
                     },
                     {
                         key: "unmanage",
                         from: localDateTime,
                         until: new Date(localDateTime.getTime() + 24 * 60 * 60 * 1000)
                     }],
                // this callback has to be overriden by input config. It specifies function to be called when Schedule button is pressed.
                onSchedule: function (schedule, success, fail) { onScheduleCallback(false); }
            };
        };

        var settings = {};

        var timezoneCookie = "<%= timeZonePicker.ClientID %>.offset";

        var localSummerTimezoneOffset = -(new Date(localDateTime.getFullYear(), 6, 1).getTimezoneOffset());
        var localWinterTimezoneOffset = -(new Date(localDateTime.getFullYear(), 0, 1).getTimezoneOffset());

        var elements = {
            dialog: $("#<%= dialog.ClientID %>"),
            closeButton: $("#<%= buttonClose.ClientID %>"),
            timezone: $("#<%= timeZonePicker.ClientID %>"),
            from: $('#<%= periodFrom.ClientID %>'),
            until: $('#<%= periodUntil.ClientID %>'),
            radioButtons: $('#<%= dialog.ClientID %> input[type="radio"]'),
            scheduleButton: $('#<%= buttonSchedule.ClientID %>'),
            errorMessage: $('#<%= dialog.ClientID %> .dialog-scheduleNotification.sw-suggestion-fail'),
            infoMessage: $('#<%= dialog.ClientID %> .dialog-scheduleNotification.sw-suggestion-info'),
            progressOverlay: $('#<%= dialog.ClientID %> .dialog-progressOverlay'),
            controls: $('#<%= dialog.ClientID %> .dialog-controls'),
            dateTimeValidationError: $("#<%= dateTimeValidationError.ClientID %>")
        };

        var activeSchedule = {};

        var findScheduleInArray = function (schedules, key) {
            return _.find(schedules,
                function (scheduleToFind) {
                    return scheduleToFind.key === key;
                });
        };

        var getActiveScheduleFromSettings = function () {
            var activeRadioValue = elements.radioButtons.filter(":checked").val();
            return findScheduleInArray(settings.schedules, activeRadioValue);
        };

        var calculateOverallOffset = function (selectedOffsetCouple) {
            var localTimezoneOffset = -(localDateTime.getTimezoneOffset());
            var selectedOffset = localTimezoneOffset === localSummerTimezoneOffset
                ? parseInt(selectedOffsetCouple.summer)
                : parseInt(selectedOffsetCouple.winter);
            return (selectedOffset - localTimezoneOffset) * 60 * 1000;
        };

        var updateScheduleFromControls = function (schedule) {
            var selectedOffsetCouple = elements.timezone.getSelectedOffsetCouple();
            setCookie(timezoneCookie, JSON.stringify(selectedOffsetCouple), "hours", 1);
            if (settings.multiMode) {
                schedule.from = elements.from.orionGetDate();
                schedule.until = elements.until.orionGetDate();
            } else {
                var offset = calculateOverallOffset(selectedOffsetCouple);
                var dateTimeFrom = elements.from.orionGetDate();
                var dateTimeUntil = elements.until.orionGetDate();
                dateTimeFrom.setTime(dateTimeFrom.getTime() - offset);
                dateTimeUntil.setTime(dateTimeUntil.getTime() - offset);
                schedule.from = dateTimeFrom;
                schedule.until = dateTimeUntil;
            }
        };

        var updateControlsFromSchedule = function (schedule) {
            var localTimezoneOffsetCouple = { 
                summer: localSummerTimezoneOffset, 
                winter: localWinterTimezoneOffset
            };
            var storedTimezoneOffsetCouple;
            if (settings.multiMode 
                || !(storedTimezoneOffsetCouple = JSON.parse(getCookie(timezoneCookie)))
                || storedTimezoneOffsetCouple === localTimezoneOffsetCouple) {
                elements.timezone.setSelectedOffsetCouple(localTimezoneOffsetCouple);
                if (schedule.from) elements.from.orionSetDate(schedule.from);
                if (schedule.until) elements.until.orionSetDate(schedule.until);
            } else {
                elements.timezone.setSelectedOffsetCouple(storedTimezoneOffsetCouple);
                var offset = calculateOverallOffset(storedTimezoneOffsetCouple);
                if (schedule.from) {
                    var dateTimeFrom = schedule.from;
                    dateTimeFrom.setTime(dateTimeFrom.getTime() + offset);
                    elements.from.orionSetDate(dateTimeFrom);
                }
                if (schedule.until) {
                    var dateTimeUntil = schedule.until;
                    dateTimeUntil.setTime(dateTimeUntil.getTime() + offset)
                    elements.until.orionSetDate(dateTimeUntil);
                }
            }
        };

        var hide = function () {
            elements.dialog.dialog("close");
        };

        var validate = function () {
            var error = elements.dateTimeValidationError;
            if (!elements.from.isValid() || !elements.until.isValid()) {
                // actual validation message shown by the control already
                return false;
            }

            var fromDate = elements.from.orionGetDate();
            var untilDate = elements.until.orionGetDate();

            $(error).hide();
            if (fromDate > untilDate) {
                $(error).text('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VS1_12) %>');
                $(error).show();
                return false;
            }
            var now = new Date();
            if (untilDate < now) {
                $(error).text('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZB0_7) %>');
                $(error).show();
                return false;
            }
            return true;
        };

        var unbindEventHandlers = function () {
            elements.radioButtons.off('change');
            elements.scheduleButton.off('click');
            elements.from.find('input').off('change');
            elements.until.find('input').off('change');
        };

        var bindEventHandlers = function () {
            elements.radioButtons.on('change', function (e) {
                updateScheduleFromControls(activeSchedule);
                activeSchedule = getActiveScheduleFromSettings();
                updateControlsFromSchedule(activeSchedule);
            });
            elements.scheduleButton.on('click', onScheduleButtonClick);
            elements.from.find('input').on('change', validate);
            elements.until.find('input').on('change', validate);
        };

        var onScheduleButtonClick = function (e) {
            e.preventDefault();
            var isValid = validate();
            if (isValid) {
                updateScheduleFromControls(activeSchedule);
                // call create schedule
                elements.scheduleButton.text("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_LH0_24) %>");
                elements.scheduleButton.off("click");
                elements.scheduleButton.addClass("not-allowed");
                elements.errorMessage.hide();
                elements.infoMessage.hide();
                settings.onSchedule(activeSchedule, scheduleSuccessCallback, scheduleFailCallback);
            }
        };

        var onScheduleCallback = function () {
            elements.scheduleButton.text("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_LH0_23) %>");
            elements.scheduleButton.on("click", onScheduleButtonClick);
            elements.scheduleButton.removeClass("not-allowed");
        };

        var scheduleSuccessCallback = function () {
            onScheduleCallback();
            hide();
        };

        var toggleProgressOverlay = function(visible) {
            if (visible) {
                elements.progressOverlay.show();
                elements.controls.toggleClass("overlayed", true);
            } else {
                elements.progressOverlay.hide();
                elements.controls.toggleClass("overlayed", false);
            }
        };

        var showErrorMessage = function (message) {
            elements.errorMessage.find("span.notificationMessage").text(message);
            elements.errorMessage.show();
        };

        var showInfoMessage = function (message) {
            elements.infoMessage.find("span.notificationMessage").text(message);
            elements.infoMessage.show();
        };

        var scheduleFailCallback = function (message) {
            showErrorMessage(message);

            onScheduleCallback();
        };

        var isValidDate = function (date) {
            return date && date.getFullYear() < 9998;
        };

        var getValidSchedules = function (schedules) {
            var validSchedules = [];

            schedules.forEach(
                function (schedule) {
                    validSchedules.push(isValidDate(schedule.from) && isValidDate(schedule.until) ? schedule : { key: schedule.key });
                });

            return validSchedules;
        };

        var schedulesLoaded = false;

        var init = function (config) {
            schedulesLoaded = false;
            toggleProgressOverlay(false);
            elements.errorMessage.hide();
            elements.infoMessage.hide();
            elements.dateTimeValidationError.hide();

            if (config.infoMessage) {
                showInfoMessage(config.infoMessage);
            }

            // start showing progress overlay after 500 ms to avoid flicker, in most cases the progress overlay will not be shown at all
            setTimeout(function () {
                if (!schedulesLoaded) {
                    toggleProgressOverlay(true);
                }
            }, 500);

            unbindEventHandlers();
            elements.closeButton.on('click', hide);
            elements.radioButtons.first().prop("checked", true);

            var defaultConfig = getDefaultConfig();

            var caption = String.format("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_LH0_18) %>",
                config.title ? config.title : defaultConfig.title);

            elements.dialog.dialog({
                dialogClass: 'maintenanceSchedulerDialogWrapper',
                width: (width >= minWidth) ? width : minWidth,
                height: (height >= minHeight) ? height : minHeight,
                modal: true,
                open: function () {
                    $(this).find('input').attr("tabindex", "1");
                },
                overlay: { "background-color": "black", opacity: 0.4 }, title: caption, resizable: false
            });

            $.when(config.schedules)
                .done(function (schedules) {
                    schedulesLoaded = true;

                    config.schedules = getValidSchedules(schedules);
                    settings = $.extend(true, {}, defaultConfig, config);
                    settings.multiMode = schedules.length === 0;

                    activeSchedule = getActiveScheduleFromSettings();
                    updateControlsFromSchedule(activeSchedule);
                    bindEventHandlers();
                    toggleProgressOverlay(false);
                })
                .fail(function(error) {
                    showErrorMessage(error);
                    toggleProgressOverlay(false);
                });
        };

        return {
            show: function (config) {
                init(config);
                elements.dialog.dialog("open");
            },
            hide: hide
        };
    })();

    maintenanceModeNamespace.MaintenanceSchedulerDialog = maintenanceSchedulerDialog;
});
//]]>
</script>
<% } %>

<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="styles/MaintenanceSchedulerDialog.css" />
<orion:include runat="server" File="js/MaintenanceMode/MaintenanceScheduler.js" />
<orion:include runat="server" File="js/MaintenanceMode/AlertSuppressionHandlers.js" />
<orion:include runat="server" File="js/MaintenanceMode/UnmanageHandlers.js" />
<orion:include runat="server" File="js/MaintenanceMode/MaintenanceModeUtils.js" />

<% if (!printable) { %>
<div id="dialog" runat="server" style="display: none;" class="disposable, maintenanceSchedulerDialog">
    <div class="dialog-progressOverlay">
        <img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>"/>
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.MaintenanceSchedulerDialog_LoadingSchedules) %></span>
    </div>

    <div class="dialog-scheduleNotification sw-suggestion sw-suggestion-fail" style="display: none;">
        <span class="sw-suggestion-icon"></span>
        <span class="notificationMessage"></span>
    </div>
    
    <div class="dialog-scheduleNotification sw-suggestion sw-suggestion-info" style="display: none;">
        <span class="sw-suggestion-icon"></span>
        <span class="notificationMessage"></span>
    </div>
    
    <div class="dialog-controls">
        <div class="dialog-mode-selection">
            <input type="radio" name="mode" value="alertSuppression" id="mode-alertSuppression" checked />
            <label for="mode-alertSuppression"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_19) %></label><br />
            <input type="radio" name="mode" value="unmanage" id="mode-unmanage" />
            <label for="mode-unmanage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_20) %></label>
        </div>

        <div>
            <div class="dialog-section-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_24) %></div>
            <div class="dialog-timepicker">
            <orion:TimeZonePicker runat="server" ID="timeZonePicker" />
            </div>
        </div>

        <div>
            <div class="dialog-section-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_21) %></div>
            <div class="dialog-timepicker">
            <orion:DateTimePicker runat="server" ID="periodFrom" EnableDateTimeValidation="True" EnableEmptyValidation="True" ValidationGroup="MaintenanceSchedulerDialogValidationGroup" />
            </div>
        </div>

        <div>
            <div class="dialog-section-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_22) %></div>
            <div class="dialog-timepicker">
            <orion:DateTimePicker runat="server" ID="periodUntil" EnableDateTimeValidation="True" EnableEmptyValidation="True" ValidationGroup="MaintenanceSchedulerDialogValidationGroup" />
            </div>
        </div>

        <div id="dateTimeValidationError" style="display: none;" runat="server" class="sw-validation-error"></div>
    </div>

    <div class="bottom">
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_LH0_23 %>" DisplayType="Primary" ID="buttonSchedule" />
            <orion:LocalizableButtonLink runat="server" LocalizedText="Close" DisplayType="Secondary" ID="buttonClose" />
        </div>
    </div>
</div>
<% } %>
