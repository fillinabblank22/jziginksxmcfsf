<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimeSelectionControl.ascx.cs" Inherits="Orion_Controls_TimeSelectionControl" %>
<orion:Include ID="Include1" runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include ID="Include2" runat="server" File="js/jquery/timePicker.css" />
<div class="timeSelector" frequency_Id ="<%= FrequencyId %>" >
    <div style="float:left; margin-top:10px; margin-left:15px">
                            <span id="onceDateBlock" class="forOnceDateField" runat="server">
                            <asp:TextBox ID="txtDatePicker" runat="server" Width="78px" CssClass="datePicker disposable" />
                            <label class="dateValidationMessage" style="display: none; color: red; font-size:12px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_GenericValidationError_Date) %></label>
                            </span>
                            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_35) %></label>
                            <asp:TextBox ID="txtTimePicker" runat="server" Width="78px" CssClass="timePicker disposable" /> 
                            <label class="timeValidationMessage" style="display: none; color: red; font-size:12px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_508) %></label>
                            </div>
                            <div class="crossButton" style="float: right;">
                                <img style="margin:5px 5px; cursor: pointer;" alt="" src="/Orion/images/Reports/delete_netobject.png">
                                </div>
</div>
<script type="text/javascript">
    (function ($) {
        var regionalSettings = <%= DatePickerRegionalSettings.GetDatePickerRegionalSettings() %>;
        $.fn.orionGetDate = function () {
            var datepicker = $('.datePicker', this);
            var timepicker = $('.timePicker', this);
            var datepart = $.datepicker.parseDate(regionalSettings.dateFormat, datepicker.val());
            var timepart = timepicker[0].timePicker.getTime();
            return new Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());
        };

        $.fn.orionSetDate = function (date) {
            $('.datePicker', this).val($.datepicker.formatDate(regionalSettings.dateFormat, date));
            var time = date.getTime(); // timePicker.setTime screws up the object. preserve it for our poor caller
            $('.timePicker', this)[0].timePicker.setTime(date);
            date.setTime(time);
        };

        $.datepicker.setDefaults(regionalSettings);
        $('.datePicker').datepicker({ mandatory: true, closeAtTop: false });
        $('.timePicker').timePicker({ separator: regionalSettings.timeSeparator, show24Hours: regionalSettings.show24Hours });

          $.fn.validateDateField = function(elem) {
                try {
                    // validate input
                    $.datepicker.parseDate(regionalSettings.dateFormat, $(elem).val());
                    return true;
                } catch(err) {
                    return false;
                }

            };
         $.fn.validateTimeField = function(elem) {
                var dtRegex;
                if (regionalSettings.show24Hours)
                    dtRegex = new RegExp('^(?:[01]?[0-9]|2[0-3]):[0-5][0-9]$');
                else
                    dtRegex = new RegExp('^(?:[0]?[1-9]|1[0-2]):[0-5][0-9]( AM| PM| a\\.m\\.| p\\.m\\.)?$');
                if (!dtRegex.test($(elem).val()))
                    return false;
                else 
                    return true;
            };

        $.fn.attachDateTimeValidation = function(elem) {
            $(elem).find('.datePicker').on('change', function() {
                if ($.fn.validateDateField(this))
                     $(elem).find(".dateValidationMessage").hide();
                else 
                     $(elem).find(".dateValidationMessage").show();
            });
            $(elem).find('.timePicker').on('change',function() {
                if ($.fn.validateTimeField(this))
                     $(elem).find(".timeValidationMessage").hide();
                else 
                     $(elem).find(".timeValidationMessage").show();
            });
        };
        $(".crossButton").one("click", function () {
            var $currentContainer = $(this).closest(".TimeControl");
            if ($currentContainer.find(".timeSelector").length == 2) {
                $currentContainer.find(".timeSelector").find(".crossButton").hide();
            } else {
                $currentContainer.find(".timeSelector").find(".crossButton").show();
            }
            $(this).closest(".timeSelector").remove();
        });
    })(jQuery);
</script>


