﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Orion_Controls_Slider : System.Web.UI.UserControl
{
	protected HtmlGenericControl SliderSpan;
	protected TextBox SliderValue;
	protected RangeValidator SliderValidator;
	protected RequiredFieldValidator SliderEmptyValidator;
	
	#region Properties
	/// <summary>
	/// Current value of the slider
	/// </summary>
	public int Value
	{
		get
		{
			this.EnsureChildControls();
			return Convert.ToInt32(this.SliderValue.Text);
		}
		set
		{
			this.EnsureChildControls();
			this.SliderValue.Text = value.ToString();
		}
	}

	/// <summary>
	/// Slider label
	/// </summary>
	public string Label { get; set; }

	/// <summary>
	/// Optional label for units
	/// </summary>
	public string Unit { get; set; }

	/// <summary>
	/// Start of slider range
	/// </summary>
	public int RangeFrom { get; set; }

	/// <summary>
	/// End of slider range
	/// </summary>
	public int RangeTo { get; set; }

	/// <summary>
	/// Minimum value of change when slider is dragged
	/// </summary>
	public int Step { get; set; }

	/// <summary>
	/// Number of table cells to generate control in
	/// </summary>
	public int TableCellCount { get; set; }

	/// <summary>
	/// Set true to validate range
	/// </summary>
	public bool ValidateRange { get; set; }

	/// <summary>
	/// Validator error message for out of range. String can contain two format items like for string.Format method which represents Range values.
	/// Format item {0} represents RangeFrom value, item {1} represents RangeTo value.
	/// </summary>
	public string ErrorMessage { get; set; }

    /// <summary>
    /// Use this property to store any custom value to control's viewstate
    /// for later use in postback events
    /// </summary>
    public object CustomValue
    {
        get
        {
            return this.ViewState["CustomValue"];
        }
        set
        {
            this.ViewState["CustomValue"] = value;
        }
    }
	#endregion

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		this.Page.ClientScript.RegisterStartupScript(typeof(Orion_Controls_Slider), this.SliderSpan.ClientID, 
			String.Format(@"<script type=""text/javascript""> $(CreateSlider('{0}', '{1}', '{2}', {3}, {4}, {5}, {6})); </script>", 
				this.SliderSpan.ClientID, 
				this.SliderValue.ClientID, 
				this.SliderValidator != null ? this.SliderValidator.ClientID : String.Empty,
				this.Value, this.RangeFrom, this.RangeTo, this.Step));
	}

    private HtmlTableCell CreateTableCell(string className)
    {
        HtmlTableCell cell = new HtmlTableCell();
        if(!String.IsNullOrEmpty(className))
            cell.Attributes["class"] = className;
        return cell;
    }

	protected override void CreateChildControls()
	{
		base.CreateChildControls();

		Control content = this.ContentPlaceHolder;

		if (this.TableCellCount > 0)
		{
            content = CreateTableCell("slider-column1");
			this.ContentPlaceHolder.Controls.Add(content);
		}
		content.Controls.Add(new Label() { Text = this.Label });
		if (this.TableCellCount > 1)
		{
            content = CreateTableCell("slider-column2");
            this.ContentPlaceHolder.Controls.Add(content);
		}
		this.SliderSpan = new HtmlGenericControl("div");
		content.Controls.Add(this.SliderSpan);
		if (this.TableCellCount > 2)
		{
            content = CreateTableCell("slider-column3");
            this.ContentPlaceHolder.Controls.Add(content);
		}
		this.SliderValue = new TextBox() { ID = "st", Width = new Unit(40), MaxLength = 5, Text = "0" };
		this.SliderValue.Style[HtmlTextWriterStyle.TextAlign] = "right";
		content.Controls.Add(this.SliderValue);
		if (!String.IsNullOrEmpty(this.Unit))
		{
			content.Controls.Add(new LiteralControl("&nbsp;" + this.Unit));
		}
		if (this.ValidateRange)
		{
			this.SliderValidator = new RangeValidator()
			{
				ErrorMessage = string.Format(this.ErrorMessage ?? string.Empty, this.RangeFrom, this.RangeTo),
				MinimumValue = this.RangeFrom.ToString(),
				MaximumValue = this.RangeTo.ToString(),
				Type = ValidationDataType.Integer,
				Display = ValidatorDisplay.Dynamic,
				ControlToValidate = this.SliderValue.ID
			};
			content.Controls.Add(this.SliderValidator);
		}
		if (this.TableCellCount > 3)
		{
			content = new HtmlTableCell() { ColSpan = (this.TableCellCount - 3) };
			content.Controls.Add(new LiteralControl("&nbsp;"));
			this.ContentPlaceHolder.Controls.Add(content);
		}
		this.SliderEmptyValidator = new RequiredFieldValidator()
		{
			ErrorMessage = Resources.CoreWebContent.WEBCODE_VB0_27,
			Display = ValidatorDisplay.Dynamic,
			ControlToValidate = this.SliderValue.ID
		};
		content.Controls.Add(this.SliderEmptyValidator);
	}
}
