<%@ Control Language="C#" ClassName="IconHelpButton" %>
<%@ Reference Control="~/Orion/Controls/HelpButton.ascx" %>

<script runat="server">
	public string HelpUrlFragment { get; set; }
	
    protected string HelpURL
    {
        get
        {
			return ASP.HelpButton.GetHelpUrl(this.HelpUrlFragment);
        }
    }
	
	private string _imageName = "/Orion/images/Icon.Help.gif";

	public string ImageName
	{
		get { return _imageName; }
		set { _imageName = value; }
	}
		 
</script>

<span class="IconHelpButton">
<a class="HelpText" href="<%= DefaultSanitizer.SanitizeHtml(this.HelpURL) %>" style="background: transparent url(<%= DefaultSanitizer.SanitizeHtml(this.ImageName) %>) scroll no-repeat left center; padding: 2px 0px 2px 20px;" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_54) %></a>
</span>