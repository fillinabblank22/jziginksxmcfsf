<%@ Control Language="C#" ClassName="Refresher" EnableViewState="false" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<script runat="server">
    /// <summary>This setting will enable the previous behavior where the current page, and all its referenced files are redownloaded regardless of sent cache or expires headers</summary>
    public bool ForceRefreshAll { get; set; }

    private int _autoRefreshSeconds = WebSettingsDAL.AutoRefreshSeconds;

    public int AutoRefreshSeconds
    {
        get { return _autoRefreshSeconds; }
        set { _autoRefreshSeconds = value; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        var view = this.Page as OrionView;

        if (view == null || !OrionMinReqsMaster.IsNOCView)
        {
            int refreshdelay = AutoRefreshSeconds;

            string reloadScript = string.Format(@"
$(function() {{
    SW.Core.View.InitRefreshInterval({0}, {1});
}});
",
                refreshdelay*1000,
                ForceRefreshAll ? "true" : "false");

            if (ForceRefreshAll == false)
                this.Response.Cache.SetMaxAge(TimeSpan.FromSeconds(0));

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "timeout", reloadScript, true);
        }
        base.OnPreRender(e);
    }
</script>
