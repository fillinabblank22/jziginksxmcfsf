﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePickerDialog.ascx.cs" Inherits="Orion_Controls_NodePickerDialog" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>

<orion:Include File="NodePickerDialog.css" runat="server" />

<div id="nodePickerDialog" class="node-picker-dialog offPage">
    <orion:NetObjectPicker runat="server" />

    <div id="nodePickerDialogButtons" class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
    </div>
</div>

<orion:Include Framework="Ext" FrameworkVersion="3.4" runat="server"/>
<orion:Include File="OrionCore.js" runat="server"/>
<orion:Include File="NodePickerDialog.js" runat="server"/>