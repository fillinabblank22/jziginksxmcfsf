﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.ComponentModel;
using SolarWinds.Orion.Web.Controls;

//
// This class was taken from the IPAM codebase
// (see //depot/Dev/Main/IPAM/Src/Web/Orion/IPAM/Controls/NavigationTabBar.ascx )
// 
// and changed for generic usage
//
[ParseChildren(typeof(NavigationTabItem), ChildrenAsProperties = true, DefaultProperty = "Tabs")]
public partial class Orion_Controls_NavigationTabBar : UserControl
{
    private Dictionary<string, string> _added = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
    private bool _alreadyactive = false;
    private string _pageTitleOverride = null;

	public Orion_Controls_NavigationTabBar()
	{
		this.Tabs = new NavigationTabItemCollection();
	}

    public string PageTitleOverride
    {
        get
        {
            return _pageTitleOverride;
        }
        set
        {
            _pageTitleOverride = value;
        }
    }

	public NavigationTabItemCollection Tabs { get; private set; }

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		foreach (NavigationTabItem tab in this.Tabs)
			this.AddTab(tab.Name, VirtualPathUtility.ToAbsolute(tab.Url));
	}

	public void AddTab(string displayName, string url)
	{
		bool active = Request.Url.AbsolutePath.ToLowerInvariant().Contains(url.ToLowerInvariant());
		this.AddTab(displayName, url, active);
	}

    public void AddTab(string displayName, string url, bool Active, bool Enabled)
    {
        if (_added.ContainsKey(url))
            return;

        if (_alreadyactive)
            Active = false;

        _added.Add(url, displayName);

        const string innerTabFormat = "<em class=\"x-tab-left\"><span class=\"x-tab-strip-inner\"><span class=\"x-tab-strip-text\">{0}</span></span></em>";
        const string NonClickingFormat = "<li class=\"sw-tab {0}\"><a href=\"{1}\" onclick=\"return false;\" class=\"x-tab-right\">{2}</a></li>";
        const string ClickingFormat = "<li class=\"sw-tab {0}\"><a href=\"{1}\" class=\"x-tab-right\">{2}</a></li>";

        string fullpath = VirtualPathUtility.ToAbsolute(url);
        bool isurl = fullpath.Equals(Request.Url.AbsolutePath, StringComparison.OrdinalIgnoreCase);

        string fmt = NonClickingFormat;
        string p1;
        string p2;

        if (!Enabled)
        {
            p1 = "x-item-disabled";
            p2 = "#";
        }
        else if (Active)
        {
            _alreadyactive = true;
            p1 = "x-tab-strip-active";

            if (isurl)
            {
                p2 = "#";
            }
            else
            {
                p2 = HttpUtility.HtmlAttributeEncode(url);
                fmt = ClickingFormat;
            }
        }
        else
        {
            p1 = "";
            p2 = HttpUtility.HtmlAttributeEncode(url);
            fmt = ClickingFormat;
        }

        string inner = string.Format(innerTabFormat, HttpUtility.HtmlEncode(displayName));
        string newTab = string.Format(fmt, p1, p2, inner);
        TabPH.Text += newTab;
    }

    public void AddTab(string displayName, string url, bool Active)
    {
        AddTab(displayName, url, Active, true);
    }

    public bool IsPageRegexMatch(string pattern)
    {
        return Regex.IsMatch(Request.Url.AbsolutePath, pattern, RegexOptions.IgnoreCase);
    }
}
