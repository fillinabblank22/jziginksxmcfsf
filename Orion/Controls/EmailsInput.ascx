<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailsInput.ascx.cs" Inherits="Orion_Controls_EmailsInput" %>
<orion:Include File="EmailsInput.css" runat="server" />
<div runat="server" id="EmailPickerControl" class="emailPickerContainer" style="padding-bottom: 10px">
    <div class="emailInputField" id="txtEmail" runat="server">
        <input type="text" class="emailInputText" style="width: 100%;"/>
    </div>
	<div class="validationMessage sw-suggestion sw-suggestion-fail" style="display: none;">
		<span class="sw-suggestion-icon"></span>
		<span style="color: red; font-size: 12px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0049) %></span>
	</div>
    <input type="hidden" ID="emailsString" runat="server"/>
    <div style="clear: both;"></div>
</div>
<div style="clear: both;">
</div>
<img alt="" src="/Orion/images/Small_Delete_Cross.png" style="display: none;" />
<script type="text/javascript">
    (function () {
        var regexMacros = /^\$\{([a-zA-Z0-9_.+-=;\s\$\{\}\'\"])+\}\;*$/;
        function isEmail(email, allowMacros) {
            var validationResult;
            var regex = /.+@.+/;
            validationResult = regex.test(email);
            if (!validationResult && allowMacros)
                validationResult = isValidMacro(email);
            return validationResult;
        }

        function isValidMacro(macro) {
            return regexMacros.test(macro);
        }

        function isValueMacro(macroCandidate) {
            return macroCandidate.startsWith("$");
        }

        function addEmailKeyUpHandler() {
            var emailSplitRegex = /[\s,;)]+/;
            var $input = $(this);
            var $elem = $input.parent();
            var val = $input.val();
            if (isValueMacro(val)) {
                if (isValidMacro(val) && $("#ui-active-menuitem").length == 0) {
                    addEmailInBox($elem, val);
                }
            } else {
                var wordsArray = val.split(emailSplitRegex);
                if (wordsArray.length > 1) {
                    $.each(wordsArray, function () {
                        addEmailInBox($elem, this);
                    });
                }
            }

            $elem.find(".emailInputText").focus();
        }

        function validateEmailsInput(value, elem) {
            var validateEmailSplitRegex = /[\,)]+/;
            var validationResult = true;
            var allowMacros = $(elem).parents('.emailPickerContainer').find('input[type="hidden"]').attr("AllowMacros") == 'True';
            elem.removeClass("invalidEmail");
            $.each(value.split(validateEmailSplitRegex), function () {
                if (!isEmail($.trim(this), allowMacros)) {
                    validationResult = false;
                    elem.addClass("invalidEmail");
                }
            });
            return validationResult;
        }
        function addTextArrea(elem) {
            elem.append('<input type="text" class="emailInputText">');
            addAutocomplete(elem.find(".emailInputText"));
            elem.find(".emailInputText").on('focusout', function () {
                if ($(this).val() != "" && $("#ui-active-menuitem").length == 0)
                    addEmailInBox($(this).parent(), $(this).val());
            });
            elem.find(".emailInputText").keydown(function (e) {
                if (e.keyCode == 13) {
                    addEmailInBox($(this).parent(), $(this).val());
                    elem.find(".emailInputText").focus();
                    return false;
                }
            });
            var emails = elem.find(".emailItem");
            var width = elem.width();
            if (emails.length != 0)
                width = elem.width() - elem.find(".emailInputText").position().left + elem.position().left - elem.css("padding-left").replace("px", "");
            elem.find(".emailInputText").width(width);
            elem.find(".emailInputText").keyup(addEmailKeyUpHandler);
        }
				
        function addEmailInBox(elem, text) {
            if (text.trim() != "" && (elem.hasClass("multiple") || elem.find(".emailItem").length == 0)) {
                elem.append("<span class='emailItem'>" + Ext.util.Format.htmlEncode(text) + "<img alt='' src='/Orion/images/Small_Delete_Cross.png' /></span>");
            }
            var itemsArray = elem.find(".emailItem");
            var $hiddenValuefield = elem.closest(".emailPickerContainer").find("input:hidden");
            elem.empty();
            $hiddenValuefield.val("");
            var valid = true;
            $.each(itemsArray, function () {
                var emailCandidate = $(this).text();
                elem.append(this);
                valid = validateEmailsInput(emailCandidate, $(this)) && valid;
                if ($hiddenValuefield.val() == "")
                    $hiddenValuefield.val(emailCandidate);
                else
                    $hiddenValuefield.val(String.format("{0},{1}", $hiddenValuefield.val(), emailCandidate));

            });
            if (elem.hasClass("multiple") || itemsArray.length == 0)
                addTextArrea(elem);

            if (valid) {
                elem.removeClass('invalidValue');
            }
            elem.find(".emailItem img").click(function () {
                if ($(this).parent().attr("disabled") != "disabled") {
                    $(this).parent().remove();
                    addEmailInBox(elem, "");
                }
            });
            elem.find(".emailItem").dblclick(function () { emailEdit(this); });
        }

        function emailEdit(item) {
            $(item).unbind("dblclick");
            if ($(item).attr("disabled") != "disabled") {
                $(item).attr('contentEditable', true);
                $(item).addClass("inputMode");
                $(item).find("img").remove();
                $(item).focus();
                $(item).keydown(function (e) {
                    if (e.keyCode == 13) {
                        $(this).focusout();
                        return false;
                    }
                });
                $(item).on("focusout", function () {
                    $(this).attr('contentEditable', false);
                    $(item).removeClass("inputMode");
                    validateEmailsInput($(this).text(), $(this));
                    $(this).append("<img alt='' src='/Orion/images/Small_Delete_Cross.png' />");
                    var elem = $(this).closest(".emailInputField");
                    $(this).find("img").click(function () {
                        if ($(this).parent().attr("disabled") != "disabled") {
                            $(this).parent().remove();
                            addEmailInBox(elem, "");
                        }
                    });
                    addEmailInBox(elem, "");
                });
            }
        }

        function addAutocomplete(input) {
            input.autocomplete({
                disabled: false,
                delay: 500,
                minLength: 0,
                source: function(request, respond) {
                    var param = { SearchTerm: request.term };
                    SW.Core.Services.callController("/api/Email/GetFieldAutoCompleteValues", param, function(result) {
                        respond(result);
                    }, function() {
                        response([]);
                        $.error("Can't load autocomplete data");
                    });
                },
                select: function(event, data) {
                    addEmailInBox($(this).parent(), data.item.value);
                    $(this).autocomplete('disable');
                }
            }).blur(function() {
                $(this).autocomplete('enable');
            });
        }

        function initControl() {
            var emailSplitRegex = /,|;(?!M=)/;
            if ($.browser.msie && $.browser.version > 8) {
                document.execCommand("AutoUrlDetect", false, false);
            }
            $.each($(".emailPickerContainer"), function () {
                var $container = $(this);

                addAutocomplete($container.find(".emailInputText"));

                $container.find(".emailInputText").keydown(function (e) {
                    if (e.keyCode == 13) {
                        addEmailInBox($(this).parent(), $(this).val());
                        return false;
                    }
                });
                $container.find(".emailInputText").keyup(addEmailKeyUpHandler);

                $container.find(".emailInputText").on('focusout', function () {
                    if ($("#ui-active-menuitem").length == 0) {
                        addEmailInBox($(this).parent(), $(this).val());
                    }
                });
                var $hiddenValuefield = $container.find("input:hidden");
                var elem = $container.find(".emailInputField");
                if ($hiddenValuefield.val() != "") {
                    var emails = $hiddenValuefield.val().split(emailSplitRegex);
                    elem.empty();
                    $.each(emails, function () {
                        if (this.length != 0) {
                            elem.append("<span class='emailItem'>" + this + "<img alt='' src='/Orion/images/Small_Delete_Cross.png' /></span>");
                        }
                    });
                    if ((elem.hasClass("multiple") || emails.length == 0))
                        addTextArrea(elem);
                }
                $container.find(".emailItem img").click(function () {
                    if ($(this).parent().attr("disabled") != "disabled") {
                        var $elem = $(this).closest(".emailInputField");
                        $(this).parent().remove();
                        addEmailInBox($elem, "");
                    }
                });
                $container.find(".emailItem").dblclick(function () { emailEdit(this); });
            });
        }
        initControl();
    })();

</script>

