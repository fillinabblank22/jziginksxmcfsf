<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserAccountControl.ascx.cs"
    Inherits="Orion_Controls_UserAccountControl" %>
<style type="text/css">
    #userCredentials table td
    {
        padding: 3px;
    }
    #userCredentials
    {
        background-color: #f5f5f5;
        width: 320px;
        padding: 10px;
    }
    #userCredentials table
    {
        width: 300px;
    }
</style>
<table>
    <tr>
        <td>
            <input id="useCurrentUser" class="userSelection" type="radio" onclick="SW.Core.UserAccountControl.radioSelectionChange(this);" runat="server" value="0" />
        </td>
        <td>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_54) %></label>
            <span style="color: gray; margin: 0 5px;">(<%= DefaultSanitizer.SanitizeHtml(this.Profile.UserName) %>)</span>
        </td>
    </tr>
    <tr>
        <td>
            <input id="useAnotherUser" class="userSelection" type="radio" onclick="SW.Core.UserAccountControl.radioSelectionChange(this);" runat="server" value="1" />
        </td>
        <td>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_55) %></label>
        </td>
    </tr>
    <tr runat="server" visible="false" id="noUserDefined">
        <td>
            <input id="notUseUser" class="userSelection" type="radio" onclick="SW.Core.UserAccountControl.radioSelectionChange(this);" runat="server" value="0" />
        </td>
        <td>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_72) %></label>
        </td>
    </tr>
</table>
<div id="userCredentials" style="display: none; margin: 10px;">
    <table>
        <tr>
            <td colspan="2">
                <label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_64) %></label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HiddenField ID="curentUserName" Value="<%$ HtmlEncodedCode: this.Profile.UserName %>" runat="server" />
                <asp:TextBox ID="userName" runat="server" Width="100%"></asp:TextBox>
                <br />
                <span class="hintText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_65) %></span>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_8) %></label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="password" runat="server" TextMode="Password" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <orion:LocalizableButton runat='server' ID='test' LocalizedText='Test' DisplayType='Small'
                    CausesValidation='false' OnClientClick="SW.Core.UserAccountControl.testCredentials(); return false;" />
            </td>
            <td>
                <div id="testResultSuccess" class="sw-suggestion sw-suggestion-pass" style="display: none;
                    margin-bottom: 10px; margin-left: 0px;">
                    <span class="sw-suggestion-icon"></span>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_55) %></div>
                <div id="testResultFailed" class="sw-suggestion sw-suggestion-fail" style="display: none;
                    margin-bottom: 10px; margin-left: 0px;">
                    <span class="sw-suggestion-icon"></span>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_56) %></div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(function() {
        SW.Core.namespace("SW.Core").UserAccountControl = new function() {

            this.init = function() {
                this.radioSelectionChange($('input.userSelection:checked'));
            };

            this.getUserName = function() {
                if ($("input[id*='useCurrentUser']").is(':checked')) {
                    return $("input[id*='curentUserName']").val();
                } else if ($("input[id*='notUseUser']").is(':checked')) {
                    return "";
                }
                else {
                    return $("input[id*='userName']").val();
                }
            };

            this.radioSelectionChange = function(elem) {
                if ($(elem).attr('value') == "1") {
                    $("#userCredentials").show();
                }
                else {
                    $("#userCredentials").hide();
                };
            };

            this.testCredentials = function() {
                if ($(elem).attr('value') == "1") {
                    $("#userCredentials").show();
                }
                else {
                    $("#userCredentials").hide();
                }
            };

            this.testCredentials = function(callback) {
                var name = $("#<%=userName.ClientID%>").val().trim();
                var password = $("#<%=password.ClientID%>").val();
                var creds = { name: name, password: password };
                ORION.callWebService('/Orion/Services/ReportScheduler.asmx', "TestCredentials", creds, function(result) {
                    if (result) {
                        $('#testResultSuccess').show();
                        $('#testResultFailed').hide();
                    } else {
                        $('#testResultSuccess').hide();
                        $('#testResultFailed').show();
                    }
                    if ($.isFunction(callback))
                        callback(result);
                });
            };
        };
      
        SW.Core.UserAccountControl.init();
    });
</script>
