﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShowTrendControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //for custom object resource 
        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            showTrend.Attributes.Add("onclick", "javascript:SaveData('ShowTrend', this.checked ? 'True' : 'False');");
        }
    }

    public bool Checked
    {
        set
        {
            showTrend.Checked = value;
        }
        get
        {
            return showTrend.Checked;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool InLine { get; set; }
}
