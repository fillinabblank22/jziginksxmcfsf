<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomPropertyValueControl.ascx.cs" Inherits="Orion_Controls_EditCustomPropertyValueControl" %>
<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="js/jquery/timePicker.css" />
<orion:Include runat="server" File="EditCustomPropertyValueController.js" />
<div id="editCpContainer">
    <div runat="server" id="TextBoxEditor" Visible="False">
        <asp:TextBox runat="server" ID="TextBoxEditorValue" data-edited='EditorValue' data-from="<%$ HtmlEncodedCode:  SolarWinds.Orion.Core.Actions.Impl.CustomProperty.CustomPropertyConstants.ValueKey %>" data-form="<%$ HtmlEncodedCode:  SolarWinds.Orion.Core.Actions.Impl.CustomProperty.CustomPropertyConstants.ValueKey %>"></asp:TextBox>
        <orion:LocalizableButton ID="ValueInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: SolarWinds.Orion.Core.Actions.Impl.CustomProperty.CustomPropertyConstants.ValueKey %>" runat="server"/>
    </div>
    <div runat="server" id="YesNoEditor" Visible="False">
        <asp:DropDownList runat="server" ID="YesNoValue" data-edited='EditorValue'>
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_124 %>" Value="True"></asp:ListItem>
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_125 %>" Value="False" Selected="True"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div runat="server" id="DateTimeEditor" Visible="False">
        <asp:TextBox ID="TextBoxDatePickerValue" runat="server" Width="140px" CssClass="datePicker disposable" data-edited='EditorValue'/>
        <asp:TextBox ID="TextBoxTimePickerValue" runat="server" Width="104px" CssClass="timePicker disposable" data-edited='EditorValue'/>
    </div>
    <div runat="server" ID="RestrictedValuesEditor" Visible="False">
        <asp:DropDownList runat="server" ID="RestrictedValues" data-edited='EditorValue'></asp:DropDownList>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        SW.Core.EditCustomPropertyValueController = new SW.Core.EditCustomPropertyValue(
        {
            containerID: 'editCpContainer',
            regionalSettings: <%= DatePickerRegionalSettings.GetDatePickerRegionalSettings() %>,
            datatype: '<%= Type %>'
        });
        SW.Core.EditCustomPropertyValueController.init();
    });
</script>