﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrionMessagesReportControl.ascx.cs" Inherits="OrionMessagesReportControl" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<%@ Register Src="~/Orion/Controls/AlertDetailsControl.ascx" TagName="AlertDetails" TagPrefix="orion" %>
<%@ Register TagPrefix="swisf" TagName="swiserrorcontrol" Src="~/Orion/SwisfErrorControl.ascx" %>

<orion:Include runat="server" File="legacyevents.css" /><%-- severity css --%>
<orion:Include runat="server" File="auditevent.css" /><%-- audit css --%>

<script type="text/javascript">
function DemoCheck() {
            <% if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) { %>
            return true;
                    <% } else { %>
            demoAction('Core_Alerting_Acknowledge', this);
            return false;
                    <% } %>
        }
    </script>

<style type="text/css">
    .disabledAlertNote { background-color: #ecedee; }     
     .sw-pg-nonbutton { background: transparent; border: 0; padding: 0; margin-right: 4px; font-size:9pt;}
     .sw-pg-events thead { background-color: #ecedee; }
     .sw-pg-events thead tr:first-child td { padding: 0; }
     .sw-pg-events thead .sw-pg-coltime { padding-right: 26px; }
     .sw-pg-colcbox { text-align: center; width: 30px;  }
     .sw-pg-coltime { white-space: nowrap; }
     .sw-pg-colimg { text-align: center; width: 25px; }
     </style>

<div>
    <swisf:swiserrorcontrol runat="server" id="SwisErrorControl" />
</div>

<orion:AlertDetails runat="server" ID="AlertDetailsPopup" />
<table style="width:100%;">
    <tr>
        <td>
            <div class="sw-btn-bar" ID="MessageActionsTable" runat="server">
                    <orion:LocalizableButton ID="ImageButton1" runat="server" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_140 %>" DisplayType="Small"
                        OnClientClick="$('.MessCheckBox input:enabled').attr('checked','checked'); return false;" />
                    <orion:LocalizableButton ID="ImageButton2" runat="server" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_225 %>" DisplayType="Small"
                        OnClientClick="$('.MessCheckBox input:enabled').removeAttr('checked'); return false;" />
                    <orion:LocalizableButton ID="AcknowledgeMessButton" runat="server" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_226 %>" DisplayType="Small"
                        OnClientClick="return DemoCheck()" OnClick="AcknowledgeMess_Click" />
            </div>          
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdateProgress runat ="server" AssociatedUpdatePanelID="updatePanel" DynamicLayout="true">
                <ProgressTemplate><img alt="<%= Resources.CoreWebContent.WEBDATA_VB0_398 %>" src="../images/AJAX-Loader.gif" /></ProgressTemplate>
            </asp:UpdateProgress>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <table class="Report sw-pg-events" cellpadding="0" cellspacing="0" width="100%" >
                        <thead>
                            <tr>
                                <td class="sw-pg-colcbox" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-coltime" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colimg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <% if (SwisFederationInfo.IsFederationEnabled) { %>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <% } %>
                            </tr>
                            <tr class="sw-pg-text">
                            <td class="sw-pg-colcbox"></td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByTime" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_399 %>" OnClick="SortChange" />
                                <asp:PlaceHolder runat="server" ID="TimeDirImage"></asp:PlaceHolder> 
                            </td>
                            <td class="sw-pg-coltime" >
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByMessageType" runat="server" OnClick="SortChange" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_400 %>" />
                                <asp:PlaceHolder runat="server" ID="MessageTypeDirImage"></asp:PlaceHolder>
                            </td>
                            <td class="sw-pg-colimg"></td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByMessage" runat="server" OnClick="SortChange" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_23 %>" />
                                <asp:PlaceHolder runat="server" ID="MessageDirImage"></asp:PlaceHolder>
                            </td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByCaption" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_TM0_4 %>" OnClick="SortChange"/>
                                <asp:PlaceHolder runat="server" ID="CaptionDirImage"></asp:PlaceHolder>
                            </td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByIpAddress" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_402 %>" OnClick="SortChange"/>
                                <asp:PlaceHolder runat="server" ID="IpAddressDirImage"></asp:PlaceHolder>
                            </td>
                            <% if (SwisFederationInfo.IsFederationEnabled) { %>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortBySolarWindsServer" runat="server" Text="<%$ Resources: CoreWebContent, Events_SolarwindsServerColumnHeader %>" OnClick="SortChange"/>
                                <asp:PlaceHolder runat="server" ID="SolarWindsServerDirImage"></asp:PlaceHolder>
                            </td>
                            <% } %>
                            </tr>
                       </thead>
                    <asp:Repeater runat="server" ID="table" OnItemDataBound="table_OnItemDataBound" >                
                        <ItemTemplate>
                            <tr>
                                <%# FormatCheckBoxCellBeginTag(Eval("MessageType"), Eval("Icon")) %>
                                    <asp:CheckBox ID = "cbMess" runat="server" CssClass = "MessCheckBox"
			                            Checked = '<%#(Eval("Acknowledged").ToString() == "1") ? true : false%>'
			                            Enabled = '<%#(Eval("Acknowledged").ToString() != "1" && Eval("MessageType").ToString() != "basic alert" && Eval("MessageType").ToString() != "audit") ? true : false%>'
                                        Visible = '<%# AllowEventClear %>'/>
                                    <asp:HiddenField runat="server" ID="MsgKey" Value='<%#FormatHiddenRowValue(Eval("MessageType"), Eval("MsgID"), Eval("ObjectID"),Eval("ObjectType"), Eval("SiteId")) %>'  />
                                </td>
                        </ItemTemplate>
                    </asp:Repeater>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            <% if (AllowEventClear && !Printable)
               {%>
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton ID="ImageButton3" runat="server" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_140 %>" DisplayType="Small"
                            OnClientClick="$('.MessCheckBox input:enabled').attr('checked','checked'); return false;" />
                        <orion:LocalizableButton ID="ImageButton4" runat="server" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_225 %>" DisplayType="Small"
                            OnClientClick="$('.MessCheckBox input:enabled').removeAttr('checked'); return false;" />
                        <orion:LocalizableButton ID="ImageButton5" runat="server" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_VB0_226 %>" DisplayType="Small" 
                            OnClientClick="return DemoCheck()" OnClick="AcknowledgeMess_Click" />
                    </div>                       
            <%} %>
        </td>
    </tr>
</table>
