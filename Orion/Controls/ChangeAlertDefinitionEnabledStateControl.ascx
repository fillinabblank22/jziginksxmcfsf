﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeAlertDefinitionEnabledStateControl.ascx.cs" Inherits="Orion_Controls_ChangeAlertDefinitionEnabledStateControl_ascx" %>

<script type="text/javascript">
    jQuery(document).ready(function () {

        SW.Core.namespace("SW.Orion.Alerts").ChangeAlertDefinitionEnabledStateControl = function() {
            return {
                SetAlertEnabledState: function(alertDefId, valueToSet, onSucc, onFail) {
                    <% if (!this.IsDemoServer) { %>
                    ORION.callWebService("/Orion/Services/NewAlertingService.asmx",
                        "EnableDisableAlert", { alertId: alertDefId, value: valueToSet }, onSucc, onFail);
                    <% } else { %>
                    demoAction('Core_Alerting_EnableDisableAlert', this);
                    <% } %>
                }
            };
        }();

    });
</script>