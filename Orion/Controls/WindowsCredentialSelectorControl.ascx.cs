﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Actions.Utility;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web;

public partial class Orion_Controls_WindowsCredentialSelectorControl : System.Web.UI.UserControl
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private static readonly string localizedNtAuthority = new SecurityIdentifier(WellKnownSidType.LocalSystemSid, null)
        .Translate(typeof(NTAccount)).Value.Split('\\')[0];
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private const string asteriskPass = "**********";
    private const int NonExistingWmiCredentialID = 0;
    private List<UsernamePasswordCredential> wmiCredentialList;
    public string defaultUserName;

    private int CredentialsId
    {
        get
        {
            int credentialId;
            if (!int.TryParse(DdlWmiCredentials.SelectedValue, out credentialId))
                credentialId = NonExistingWmiCredentialID;

            return credentialId;
        }        
    }    

    public string Login
    {
        get { return tbLogin.Text; }
        set { tbLogin.Text = value; }
    }

    public string Password
    {
        get { return tbPassword.Text; }
        set { EncodePassword(value); }
    }

    public string CredentialName
    {
        get { return tbCaption.Text; }
        set { tbCaption.Text = value; }
    }

    /// <summary>
    /// Set password fields values to asterisks and store plain password to PlainPassword property
    /// </summary>
    private void EncodePassword(string value)
    {        
        string passBlock = GetPassBlock(value);

        tbPassword.Text =
            tbPasswordConfirmation.Text =
            this.tbPassword.Attributes["value"] =
            this.tbPasswordConfirmation.Attributes["value"] = passBlock;        
    }

    private string GetPassBlock(string value)
    {
        return (!string.IsNullOrEmpty(value)) ? asteriskPass : "";
    }

    private void LoadData()
    {
        using (var businessProxy = _blProxyCreator.Create(ex => { _log.Error(ex); }))
        {//use standard credentialsmanager to get creds. redo default credential
            wmiCredentialList = businessProxy.GetSharedWmiCredentials(CoreConstants.CoreCredentialOwner);
            wmiCredentialList.Insert(0, new UsernamePasswordCredential() { Name = Resources.CoreWebContent.WindowsCredentialSelector_NewCredential, ID = (int)NonExistingWmiCredentialID });

            wmiCredentialList.ForEach(c => DdlWmiCredentials.Items.Add(new ListItem(c.Name,c.ID.Value.ToString())));            
        }
    }    

    private void SetCredentialsFromDropDown()
    {
        long credentialId = CredentialsId;
        if (credentialId == NonExistingWmiCredentialID)
        {
            this.CredentialName = string.Empty;
            this.Login = string.Empty;
            this.Password = string.Empty;
           
            CredentialControlsSetEnabledState(true);

            return;
        }

        UsernamePasswordCredential selected = wmiCredentialList.First(c => c.ID == credentialId);
        this.CredentialName = selected.Name;
        this.Login = selected.Username;
        this.Password = selected.Password;
              
        CredentialControlsSetEnabledState(false);
    }
    
    /// <summary>
    /// Method takes care about proper setting credentials controls Enable property state.
    /// </summary>
    /// <param name="state">if set to <c>true</c> [set credential controls enabled to true] otherwise set credential controls enabled to false.</param>
    private void CredentialControlsSetEnabledState(bool state)
    {
        tbCaption.Enabled = state;
        tbLogin.Enabled = state;
        tbPassword.Enabled = state;
        tbPasswordConfirmation.Enabled = state;

        if (state)
        {
            tbCaption.CssClass = String.Empty;
            tbLogin.CssClass = String.Empty;
            tbPassword.CssClass = String.Empty;
            tbPasswordConfirmation.CssClass = String.Empty;
        }
        else
        {
            tbCaption.CssClass = "disabled";
            tbLogin.CssClass = "disabled";
            tbPassword.CssClass = "disabled";
            tbPasswordConfirmation.CssClass = "disabled";
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LoadData();
        var defaultCredential = ActionCredentialHelper.GetDefaultActionCredential();

        defaultUserName = defaultCredential.Username;
        string[] domainUsername = defaultUserName.Split('\\');
        if (domainUsername[0].Equals(localizedNtAuthority, StringComparison.OrdinalIgnoreCase))
        {
            defaultUserName = domainUsername[1];
        }
    }

    public void SetCredentials(string credentialsId)
    {
        int credId;
        if (string.IsNullOrEmpty(credentialsId) || !int.TryParse(credentialsId, out credId))
        {
            useDefaultUser.Checked = true;
        }
        else
        {
            useDefinedUser.Checked = true;

            var credential = new CredentialManager().GetCredential<UsernamePasswordCredential>(credId);
            DdlWmiCredentials.SelectedValue = credentialsId;
            SetCredentialsFromDropDown();
        }
    }
}
