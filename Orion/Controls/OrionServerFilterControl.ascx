<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrionServerFilterControl.ascx.cs" Inherits="Orion_Controls_OrionServerFilterControl" %>

<orion:Include ID="Include1" runat="server" File="OrionServerFilterControl.css" />

<script type="text/javascript">

    var ColumnsNumber = 4;

    $(function()
    {
        var containerWidth = $(".orionServerFilterControl").width();
        var columnWidth = (containerWidth - ColumnsNumber * 10) / ColumnsNumber;

        $(".orionServerFilterControl .orionServerOption").css("display", "inline-block");
        $(".orionServerFilterControl .orionServerOption").css("width", columnWidth + "px");
    });
</script>

<div class="orionServerFilterControl">
    <asp:Repeater runat="server" ID="serverRepeater">
	    <HeaderTemplate>
	    
	        <asp:Label ID="m_Title" runat="server">
		        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.OrionServerFilterControl_Title) %></b><br />
            </asp:Label>

		    <span><%= DefaultSanitizer.SanitizeHtml(SubTitle) %></span><br />
            
		    <p style="border: 0; margin-top: 5px; margin-left: 5px; margin-bottom: 5px;">
	    </HeaderTemplate>

	    <ItemTemplate>
	        <div class="orionServerOption">
		        <asp:CheckBox runat="server" style="font-size: small;" ID="chkOrionServer"
			        Text='<%# DefaultSanitizer.SanitizeHtml(Eval("ServerName")) %>' Checked='<%# DefaultSanitizer.SanitizeHtml(Eval("Selected")) %>' OrionID='<%# DefaultSanitizer.SanitizeHtml(Eval("OrionID")) %>' />
            </div>
	    </ItemTemplate>

	    <FooterTemplate>
		    </p>
	    </FooterTemplate>
    </asp:Repeater>
</div>