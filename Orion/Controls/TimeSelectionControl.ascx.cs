using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_Controls_TimeSelectionControl : TimeSelectionBaseControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public override DateTime Time { get; set; }
    
    [PersistenceMode(PersistenceMode.Attribute)]
    public override int FrequencyId { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool IsOnce { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsOnce)
        {
            onceDateBlock.Attributes.Add("style", "display:none");
        }
        SetTime();
    }

    private void SetTime()
    {
        txtDatePicker.Text = Time.ToShortDateString();
        txtTimePicker.Text = GetShortTimeString(Time);
    }

    /// <summary>
    /// Will return short time string which will meet implementation in JavaScript component timepicker (jquery.timePicker.js)
    /// </summary>
    /// <param name="dateTime">DateTime from which return formated short time.</param>
    /// <returns>Short time which will meet implementation of timepicker JavaScript component</returns>
    private string GetShortTimeString(DateTime dateTime)
    {
        var settings = SolarWinds.Orion.Web.DatePickerRegionalSettings.GetDatePickerRegionalSettingsObject();
        var culture = new System.Globalization.CultureInfo("en-US");
        return settings.show24Hours ? dateTime.ToString($"HH{settings.timeSeparator}mm", culture) : dateTime.ToString($"hh{settings.timeSeparator}mm ttt", culture);
    }
}