using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Plugins;
using Node = SolarWinds.Orion.NPM.Web.Node;
using SolarWinds.Orion.Core.Common.Models.Alerts;

public partial class Orion_NetPerfMon_Resources_NodeDetails_OrionManagementPlugin : ManagementTasksPluginBase
    {
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        OrionInclude.CoreFile("/js/jquery/timePicker.css");
        OrionInclude.CoreFile("/styles/NodeMNG.css");
    }

    public ManagementTaskItem GetMibBrowserSection(Node node, string nodeSectionName)
    {
        // MIB browser is available in demo mode without any permission restriction but node management permission is required when not in demo mode.
        // That's why there is actually not the permission check presents here but one level higher (before is method called).
        if (PackageManager.InstanceWithCache.IsPackageInstalled("Orion.DeviceStudio") && node.IsSNMP && !node.External)
        {
            return new ManagementTaskItem
            {
                Section = nodeSectionName,
                LinkInnerHtml = "<img src='/Orion/Nodes/images/icons/icon_assignpoller.gif' alt='' />&nbsp;" + Resources.CoreWebContent.WEBDATA_VB0_79,
                LinkUrl = string.Format("/Orion/MIBBrowser.aspx?NetObject={0}", node.NodeID)
            };
        }

        return null;
    }

    public override IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        var node = netObject as Node;
        bool isDemo = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        
        if (node == null)
            yield break;

        string nodeSectionName = Resources.CoreWebContent.WEBDATA_AK0_171;

        string maintenanceModeGroupLabel = $"<img src='/Orion/Nodes/images/icons/maintenance_mode_icon16.png' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_MH0_01}";

        if (isDemo)
        {
            // actions enabled in demo mode go here:
            var mibBrowserSection = GetMibBrowserSection(node, nodeSectionName);
            if (mibBrowserSection != null)
            {
                yield return mibBrowserSection;
            }

            yield break;
        }

        if (Profile.AllowNodeManagement)
        {
            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_edit.gif' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_VB0_115}",
                LinkUrl = $"/Orion/Nodes/NodeProperties.aspx?Nodes={node.NodeID}&GuidID={Guid.NewGuid()}&ReturnTo={returnUrl}"
            };

            if (node.IsSNMP || node.ObjectSubType.Equals("wmi", StringComparison.OrdinalIgnoreCase) || node.ObjectSubType.Equals("Agent", StringComparison.OrdinalIgnoreCase))
            {
                yield return new ManagementTaskItem
                {
                    Section = nodeSectionName,
                    LinkInnerHtml = "<img src='/Orion/Nodes/images/icons/icon_list.gif' alt='' />&nbsp;" + Resources.CoreWebContent.WEBDATA_VB0_116,
                    LinkUrl = $"/Orion/Nodes/ListResources.aspx?Nodes={node.NodeID}&ReturnTo={returnUrl}&SubType={node.ObjectSubType}"
                };
            }
        }

        if (!node.External && Profile.AllowUnmanage)
        {
            if (MaintenanceModeDAL.IsMaintenanceModeEnabled)
            {
                if (!node.UnManaged)
                {
                    yield return new ManagementTaskItem
                    {
                        Section = nodeSectionName,
                        LinkInnerHtml = $"<img src='/Orion/Images/nodemgmt_art/icons/icon_unmanage.png' alt='' />&nbsp;{Resources.CoreWebContent.WEBCODE_PS1_16}",
                        OnClick = $"return SW.Core.MaintenanceMode.CreateMaintenanceModeDialog.show({{ 'NetObjectIDs' : ['N:{node.NodeID}'] }});"
                    };
                }
                else
                {
                    yield return new ManagementTaskItem
                    {
                        Section = nodeSectionName,
                        LinkInnerHtml = $"<img src='/Orion/Images/nodemgmt_art/icons/icon_remanage.png' alt='' />&nbsp;{Resources.CoreWebContent.WEBCODE_PS1_15}",
                        OnClick = $"return remanageNodes([{node.NodeID}]);"
                    };
                }
            }
            else
            {
                if (!node.UnManaged)
                {
                    yield return new ManagementTaskItem
                    {
                        Section = nodeSectionName,
                        Group = maintenanceModeGroupLabel,
                        MouseHoverTooltip = Resources.CoreWebContent.WEBDATA_MH0_03,
                        ClientID = "unmanageNowLink",
                        LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_manage.gif' alt='' />&nbsp;{GetLocalizedString("@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow;E=xml}")}",
                        OnClick = IsDemoServer ? GetDemoActionCallback("Core_Alerting_Unmanage") : $"return SW.Core.MaintenanceMode.UnmanageNowSingleEntity('{node.NetObjectID}');"
                    };
                }
                else
                {
                    yield return new ManagementTaskItem
                    {
                        Section = nodeSectionName,
                        Group = maintenanceModeGroupLabel,
                        ClientID = "remanageLink",
                        LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_remanage.gif' alt='' />&nbsp;{GetLocalizedString("@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain;E=xml}")}",
                        OnClick = IsDemoServer ? GetDemoActionCallback("Core_Alerting_Unmanage") : $"return SW.Core.MaintenanceMode.ManageAgainSingleEntity('{node.NetObjectID}');"
                    };
                }
            }
        }

        if (!node.External && Profile.AllowNodeManagement)
        {
            if (node.IsSNMP && ModuleManager.InstanceWithCache.IsThereModule("NPM"))
            {
                yield return new ManagementTaskItem
                {
                    Section = nodeSectionName,
                    LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_assignpoller.gif' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_VB0_121}",
                    LinkUrl = $"/Orion/NPM/NodeCustomPollers.aspx?Nodes={node.NodeID}&ReturnTo={returnUrl}"
                };
            }

            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                ClientID = "pollNowLnk",
                LinkInnerHtml = "<img src='/Orion/images/pollnow_16x16.gif' alt='' />&nbsp;" + Resources.CoreWebContent.WEBDATA_VB0_117,
                OnClick = $"return showPollNowDialog(['N:{node.NodeID}']);"
            };

            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                ClientID = "rediscoverLnk",
                LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_discover.gif' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_VB0_118}",
                OnClick = $"return showRediscoveryDialog(['N:{node.NodeID}']);"
            };

            var mibBrowserSection = GetMibBrowserSection(node, nodeSectionName);
            if (mibBrowserSection != null)
            {
                yield return mibBrowserSection;
            }
        }

        if (Profile.AllowAlertManagement)
        {
            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/CreateAlertOnThisNode_icons16x16v1.png' alt='' />&nbsp;{Resources.CoreWebContent.Add_New_Alert}",
                LinkUrl = $"/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={Guid.NewGuid()}&ObjectType=Node&Object=<this>.Orion.Nodes|instance&Uri={HttpUtility.UrlEncode(node.SwisUri)}"
            };
        }

        if (!node.External && Profile.AllowUnmanage)
        {
            if (node.AlertSuppressionState.SuppressionMode == EntityAlertSuppressionMode.SuppressedByItself)
            {
                yield return new ManagementTaskItem
                {
                    Section = nodeSectionName,
                    Group = maintenanceModeGroupLabel,
                    ClientID = "resumeAlertsLink",
                    LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_remanage.gif' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_LH0_15}",
                    OnClick = IsDemoServer ? GetDemoActionCallback("Core_Alerting_Resume") : $"return SW.Core.MaintenanceMode.ResumeAlertsSingleEntity('{Server.HtmlEncode(node.SwisUri)}')"
                };
            }
            
            if (node.AlertSuppressionState.SuppressionMode == EntityAlertSuppressionMode.NotSuppressed ||
               node.AlertSuppressionState.SuppressionMode == EntityAlertSuppressionMode.SuppressionScheduledForItself)
            {
                yield return new ManagementTaskItem
                {
                    Section = nodeSectionName,
                    Group = maintenanceModeGroupLabel,
                    MouseHoverTooltip = Resources.CoreWebContent.HoverTooltipMutedAlerts,
                    ClientID = "suppressAlertsNowLink",
                    LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_manage.gif' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_LH0_16}",
                    OnClick = IsDemoServer ?
                        GetDemoActionCallback("Core_Alerting_Suppress") :
                        $"return SW.Core.MaintenanceMode.SuppressAlertsNowSingleEntity('{Server.HtmlEncode(node.SwisUri)}')"
                };
            }
            
            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                Group = maintenanceModeGroupLabel,
                ClientID = "scheduleMaintenanceLnk",
                LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_manage.gif' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_LH0_17}",
                OnClick = IsDemoServer ? 
                    GetDemoActionCallback("Core_Alerting_Schedule") : 
                    $"SW.Core.MaintenanceMode.MaintenanceScheduler.ShowDialogForSingleEntity('{Server.HtmlEncode(node.SwisUri)}', '{node.NetObjectID}', '{Server.HtmlEncode(node.Name)}'); return false;"
            };
        }
        if (Profile.AllowNodeManagement)
        {
            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/icon_delete.gif' alt='' />&nbsp;{Resources.CoreWebContent.DeleteNode}",
                OnClick = $"showDeleteNodeDialog('{node.NetObjectID}:{ EnginesDAL.GetPrimaryEngineId()}');"
            };

        }
    }
}
