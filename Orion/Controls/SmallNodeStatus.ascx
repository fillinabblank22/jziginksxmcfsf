<%@ Control Language="C#" ClassName="SmallNodeStatus" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>

<script runat="server">
     
    public CompoundStatus StatusValue
    {
        get { return (null == ViewState["StatusValue"]) ? new CompoundStatus() : (CompoundStatus)ViewState["StatusValue"]; }
        set { ViewState["StatusValue"] = value; }
    }

    protected string ImagePath
    {
        get
        {
            return this.StatusValue.ToString("smallimgpath", null);
        }
    }

    protected string AltText
    {
        get
        {
            if (this.StatusValue.ChildStatus.Value == OBJECT_STATUS.Up)
                return string.Format(Resources.CoreWebContent.WEBCODE_AK0_46, this.StatusValue.ParentStatus);

            return string.Format(Resources.CoreWebContent.WEBCODE_AK0_47, this.StatusValue.ParentStatus, this.StatusValue.ChildStatus);
        }
    }   
</script>

<img class="StatusIcon" alt="<%= DefaultSanitizer.SanitizeHtml(AltText) %>" src="<%= DefaultSanitizer.SanitizeHtml(ImagePath) %>" />