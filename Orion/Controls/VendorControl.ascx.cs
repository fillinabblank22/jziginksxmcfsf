﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using Limitation = SolarWinds.Orion.Web.Limitation;

public partial class VendorControl : System.Web.UI.UserControl
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public string VendorName
	{
		get { return vendors.SelectedValue; }
		set { vendors.SelectedValue = value; }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		string selectedvalue = string.Empty;
		if (!string.IsNullOrEmpty(VendorName))
		{
			selectedvalue = VendorName;
		}

		vendors.Items.Clear();
        vendors.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_192, string.Empty));        

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			List<string> lstVendors = proxy.GetAllVendors(Limitation.GetCurrentListOfLimitationIDs());
			foreach (string vendor in lstVendors)
			{
				vendors.Items.Add(new ListItem(vendor, vendor));
			}
		}

		VendorName = selectedvalue;
	}
}
