﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressDialog.ascx.cs" Inherits="Orion_Controls_ProgressDialog" %>

<orion:Include runat="server" File="js/StringUtils.js" />
<orion:include runat="server" File="js/ProgressDialog.js" />

<style>
    .iconLink
    {        
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0 2px 20px;
        
    }
    
    .success
    {
        background-image: url(/Orion/images/ok_16x16.gif);	
        color:Green;
    }
    
    .failed
    {
    	background-image: url(/Orion/images/failed_16x16.gif);	
        color:Red;
    }
</style>
