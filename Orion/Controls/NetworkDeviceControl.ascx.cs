using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Common;

public partial class NetworkDeviceControl : System.Web.UI.UserControl
{
	private void SetupControlFromRequest()
	{
		if (Request["NetObject"] != null)
		{
			NetDeviceValue = Request["NetObject"].ToString();
		}
	}

	protected override void OnInit(EventArgs e)
	{
		InitNetworkDeviceList();

		if (!Page.IsPostBack)
		{
			SetupControlFromRequest();
		}

		base.OnInit(e);
	}

	public string DescriptionText
	{
		get
		{
			return Description.Text;
		}
		set
		{
			Description.Text = value;
		}
	}

	public string NetDeviceValue
	{
		get
		{
			return NetworkDeviceList.SelectedValue;
		}
		set
		{
			NetworkDeviceList.SelectedValue = value;
		}
	}

	public string NetDeviceText
	{
		get
		{
			return NetworkDeviceList.Items[NetworkDeviceList.SelectedIndex].Text;
		}
	}

	private void InitNetworkDeviceList()
	{
		List<NetObject> objs = new List<NetObject>(Node.GetNetDevices());
		objs.Insert(0, Node.CreateNode("", "All Network Devices"));

		NetworkDeviceList.DataSource = objs;
		NetworkDeviceList.DataTextField = "Name";
		NetworkDeviceList.DataValueField = "NetObjectID";
		NetworkDeviceList.DataBind();

	}
	
}
