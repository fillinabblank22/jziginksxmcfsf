<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LayoutBuilderControl.ascx.cs"
    Inherits="Orion_Controls_LayoutBuilderControl" %>
<%@ Register TagPrefix="orion" TagName="AddContentLoader" Src="~/Orion/Controls/AddContentLoader.ascx" %>
<%@ Register Src="~/Orion/Reports/Controls/TimePeriodsControl.ascx" TagPrefix="orion" TagName="TimePeriods" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="LayoutBuilder.js" />
<orion:Include runat="server" File="LayoutBuilder.css" />
<div id="layoutBuilder" style="width: 1024px;">
    <table id="layoutBuilderContent" style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="padding: 10px 10px 5px 30px;">
                    <h2>
                        <%= DefaultSanitizer.SanitizeHtml(Title) %>
                    </h2>
            </td>
            <td style="text-align: right; padding: 0 10px 0 10px">
                <table width="100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding-right: 7px;">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_67) %>
                        </td>
                        <td style="text-align: right; width: 40px;">
                            <div id="layoutSelect">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <orion:TimePeriods runat="server" ID="timePeriods" Visible="false"/>
    <div id="cachedImages" style="display: none;">
        <img src="/Orion/images/LayoutBuilder/l01.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l02.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l03.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l04.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l05.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l06.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l07.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l08.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l09.png" alt="" />
        <img src="/Orion/images/LayoutBuilder/l10.png" alt="" />
    </div>
    <div id="layoutContainer">
    </div>
    <input type="hidden" id="emptyMessageText" value="<%= DefaultSanitizer.SanitizeHtml(EmptyMessage) %>" />
    <input type="hidden" id="sectionsSessionIdentifier" runat="server" />
    <input type="hidden" id="configsSessionIdentifier" runat="server" />
    <input type="hidden" id="ReturnToUrl" value="<%= DefaultSanitizer.SanitizeHtml(ReturnUrl) %>" />
</div>
<orion:AddContentLoader runat="server" ID="AddContentLoader" OnCreatedJs="SW.Orion.LayoutBuilder.ResourcePickerLoaderCreated" SingleMode="True"
    OnSelectedJs="SW.Orion.LayoutBuilder.SaveSelectedResources"/>

<script type="text/javascript">
    jQuery(document).ready(function() {
        SW.Orion.LayoutBuilder.setInitialConfig({
            WhatIsNewReportDialog: SW.Orion.WhatIsNewInWebBasedReportDialog
        });
    });
</script>

<%-- Template for error reporting in section cell --%>    
<script id="sectionCellErrorMessageTemplate" type="text/x-template">
    <div style="color: red; font-weight: normal; cursor: default; margin-left: 30px; margin-top: 15px;">
        <img src="/Orion/images/warning_16x16.gif" width="16" height="16" style="margin-right: 5px; vertical-align: middle;"/>
        {{ errorMessage }}        
    </div>
</script>