<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateTimeRangePicker.ascx.cs" Inherits="Orion_Controls_DateTimeRangePicker" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>

<orion:Include runat="server" File="DateTimeRangePicker.css" />
<orion:Include runat="server" File="js/Placeholder.js" />
<orion:Include runat="server" File="js/DateTimeRangePicker.js" />

<div id="<%= DefaultSanitizer.SanitizeHtml(ClientID) %>" class="sw-dateTimeRangePicker">
    
    <div class="sw-dateTimeRangePicker-startingOn">
        <label><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_AB0_33) %></label><br/>
        <asp:DropDownList ID="StartDateDropdown" runat="server" CssClass="sw-dateTimeRangePicker-startTimeTypeSelect">
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_140 %>" Value="RunNow" />
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_29 %>" Value="SpecificDate" />
        </asp:DropDownList>
    
        <div class="sw-dateTimeRangePicker-startingOn-picker" style="display: none">
            <orion:DateTimePicker runat="server" ID="startDateTime" EnableDateTimeValidation="True" ForceMarkupClientScriptBlockUsage="true"   />    
        </div>
    </div>
    
    <div class="sw-dateTimeRangePicker-endingOn">
        <label>
            <asp:CheckBox runat="server" ID="endingOnEnable" CssClass="sw-dateTimeRangePicker-endingOnCheckbox" />
            <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_AB0_34) %>
        </label>
        
        <div class="sw-dateTimeRangePicker-endingOn-picker" style="display: none">
            <orion:DateTimePicker runat="server" ID="endDateTime" EnableDateTimeValidation="True" ForceMarkupClientScriptBlockUsage="true"   />    
        </div>
    </div>

</div>
