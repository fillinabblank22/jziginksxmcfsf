<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PickerTimePeriodControl.ascx.cs" Inherits="PickerTimePeriodControl" %>
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>

<!--table width="100%" cellpadding="0" cellspacing="0" border="1"-->
<asp:DropDownList ID="TimePeriodList" runat="server"></asp:DropDownList>
<span runat="server" id="CustomPeriodEditor" style="display:none" >
<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_231) %><orion:DateTimePicker runat="server" ID="dtPeriodBegin" />
<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_232) %><orion:DateTimePicker runat="server" ID="dtPeriodEnd" />
</span>

<!--/table-->
