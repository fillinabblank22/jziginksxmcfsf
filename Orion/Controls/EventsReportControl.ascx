<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventsReportControl.ascx.cs" Inherits="Orion_Controls_EventsReportControl" %>

<div id="EventsGridDiv">
    <asp:Repeater ID="EventsGrid" runat="server">
        <HeaderTemplate>
            <table width="100%" cellspacing="0" cellpadding="3" class="Events NeedsZebraStripes">
        <% if (ShowTableHeader){%>
                <tr>
                    <td class="ReportHeader" style="width: 100px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_LastXXEvents_DateTimeColumnHeader) %></td>
                    <td class="ReportHeader" style="width: 25px;"></td>
                    <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_LastXXEvents_EventColumnHeader) %></td>
                </tr>
                <%}%>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td style="width: 100px;" ><%# Eval("EventTime", EventTimeFormater) %></td>
 				<td>
                	<div class="event-icon" style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">
	                    <img src="/NetPerfMon/images/Event-<%# DefaultSanitizer.SanitizeHtml(Eval("EventType")) %>.gif" alt="Event Type" >
                	</div>
            	</td>
				<td>
                	<asp:HyperLink ID="ItemHyperLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("URL")) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("Message")) %></asp:HyperLink>
            	</td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
