using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

[Obsolete("Does now work with Alerting V2")]
public partial class AlertNameControl : System.Web.UI.UserControl
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public string AlertID
	{
		get { return alerts.SelectedValue; }
		set { alerts.SelectedValue = value; }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);		

		string selectedvalue = string.Empty;
        if (!string.IsNullOrEmpty(AlertID))
		{
			selectedvalue = AlertID;
		}

		alerts.Items.Clear();
		alerts.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_TM0_56, string.Empty));

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			List<KeyValuePair<string, string>> items = proxy.GetAlertList();

			foreach (KeyValuePair<string, string> item in items)
			{
				alerts.Items.Add(new ListItem(item.Value, item.Key));
			}
		}

		AlertID = selectedvalue;
	}
}
