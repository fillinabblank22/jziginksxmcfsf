<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotePageRecipientsControl.ascx.cs" Inherits="Orion_Controls_NotePageRecipientsControl" %>
<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include2" runat="server" File="js/NotePageRecipientsSelector.js" />
<div id="recipientsSelection">
    <asp:HiddenField ID="recipientsList" runat="server" />
</div>
<script type="text/javascript">
    if ($("#recipientsSelection").children("div").length == 0) {
        Ext.onReady(SW.Orion.NotePageRecipientsSelector.init, SW.Orion.NotePageRecipientsSelector);
    }
</script>



