﻿using System;
using System.Web.UI.WebControls;

public partial class Orion_Controls_AlertTimeIntervals : System.Web.UI.UserControl
{
    private bool frequencySet = false;

    private TimeSpan minTimeFrequency = TimeSpan.FromSeconds(0);
    public TimeSpan MinTimeFrequency
    {
        get { return minTimeFrequency; }
        set { minTimeFrequency = value; }
    }

    public TimeSpan? MaxTimeFrequency { get; set; }

    public int DefaultTimeKindIndex { get; set; }

    public bool ServerValidationEnabled { get; set; }

    public string JsInstanceName { get { return "timeIntervals_" + ClientID; } }

    protected int MinTimeInSeconds
    {
        get { return (int)MinTimeFrequency.TotalSeconds; }
    }
    
    protected int MaxTimeInSeconds
    {
        get { return MaxTimeFrequency != null ? (int) MaxTimeFrequency.Value.TotalSeconds : int.MaxValue; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!IsPostBack)
        {
            alertTimeType.Items.AddRange(new[]
                                             {
                                                 new ListItem(Resources.CoreWebContent.WEBDATA_IB0_46, "1"),
                                                 new ListItem(Resources.CoreWebContent.WEBDATA_IB0_48, "60"),
                                                 new ListItem(Resources.CoreWebContent.WEBDATA_VB0_427, "3600")
                                             });
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            if (!frequencySet && DefaultTimeKindIndex <= alertTimeType.Items.Count)
            {
                alertTimeType.SelectedIndex = DefaultTimeKindIndex;
            }

            alertTimeValueValidator.ClientValidationFunction = JsInstanceName + ".clientValidate";
        }
    }

    protected void alertTimeValueValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!ServerValidationEnabled)
        {
            args.IsValid = true;
            return;
        }

        int val;
        if (int.TryParse(args.Value, out val))
        {
            args.IsValid = GetTimeFrequency() >= minTimeFrequency;
            alertTimeValueValidator.ErrorMessage = string.Format(Resources.CoreWebContent.WEBDATA_IB0_188, minTimeFrequency.TotalSeconds);
        }
        else
        {
            args.IsValid = false;
            alertTimeValueValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Integer;
        }

        if (args.IsValid)
        {
            alertTimeValue.CssClass = string.Empty;
            alertTimeValue.Attributes.Remove("title");
        }
        else
        {
            alertTimeValue.CssClass = "sw-validation-input-error";
            alertTimeValue.Attributes.Add("title", alertTimeValueValidator.ErrorMessage);
        }
    }

    public void SetTimeFrequency(TimeSpan frequency)
    {
        frequencySet = true;
        int totalSeconds = (int) frequency.TotalSeconds;

        if (totalSeconds == 0)
        {
            alertTimeValue.Text = "0";
            alertTimeType.SelectedValue = "1";
            return;
        }

        if (totalSeconds / 3600 > 0 && totalSeconds % 3600 == 0)
        {
            alertTimeValue.Text = (frequency.TotalSeconds / 3600).ToString();
            alertTimeType.SelectedValue = "3600";
            return;
        }

        if (totalSeconds / 60 > 0 && totalSeconds % 60 == 0)
        {
            alertTimeValue.Text = (frequency.TotalSeconds / 60).ToString();
            alertTimeType.SelectedValue = "60";
            return;
        }

        alertTimeValue.Text = frequency.TotalSeconds.ToString();
        alertTimeType.SelectedValue = "1";
    }

    public TimeSpan? GetTimeFrequency()
    {
        int timeValue;
        if (int.TryParse(alertTimeValue.Text, out timeValue))
        {
            return new TimeSpan(0, 0, timeValue * int.Parse(alertTimeType.SelectedValue));
        }
        return null;
    }
}