<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumesThresholdControl.ascx.cs" Inherits="VolumesThresholdControl" %>
<%@ Register Src="~/Orion/Controls/ThresholdControlExt.ascx" TagPrefix="orion" TagName="ThresholdControlExt" %>
<%@ Register Src="~/Orion/Controls/BaselineDetails.ascx" TagPrefix="orion" TagName="BaselineDetails" %>
<script type="text/javascript">
    $(document).ready(function () {
        $('.sw-btn').click(function () {

            var hfFlagSubmit = $('#<%= hfFlagSubmit.ClientID %>');

            if (this.id.lastIndexOf('<%= this.submitButton %>') === this.id.length - '<%= this.submitButton %>'.length) {
                hfFlagSubmit.val("1");
            } else {
                hfFlagSubmit.val("0");
            }
        });
    });
</script>
<table width="100%">
    <tr>
        <td class="contentBlockHeader">
            <asp:Literal ID="TrasholdsBlockTitle" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VolumeThresholds  %>" /> <%-- Volume Thresholds --%>
        </td>
        <td style="text-align: right">
            <span>
                <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                <a href="/Orion/NetPerfMon/Admin/NetPerfMonSettings.aspx" id="manage" target="_blank" class="RenewalsLink"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_44) %></a> <%-- Manage Orion General Thresholds --%>
            </span>
        </td>
    </tr>
</table>
<asp:HiddenField runat="server" ID="hfFlagSubmit"></asp:HiddenField>
<asp:Repeater runat="server" ID="repThresholds" OnItemDataBound="repThresholds_ItemDataBound">
    <ItemTemplate>
        <orion:ThresholdControlExt EnableViewState="True" runat="server" ID="thresholdControl"
            DisplayName='<%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["DisplayName"]) %>' 
            ThresholdName='<%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["Name"]) %>' 
            Unit='<%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["Unit"]) %>' 
            />
    </ItemTemplate>
</asp:Repeater>
<orion:BaselineDetails runat="server" ID="BaselineDetails" />