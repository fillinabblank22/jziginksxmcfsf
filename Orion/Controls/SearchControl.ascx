<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchControl.ascx.cs" Inherits="Orion_Controls_SearchControl" %>
<%-- Orion Global Search - expanding text input control --%>
<div id="ogs-container">
    <div id="ogs-content">
        <form id="searchform" name="searchform" action="/Orion/OGS/ResultPage.aspx" method="get">
            <div class="ogs-searchfield-container">
                <input type="text" name="SearchString" id="s" class="ogs-searchfield" placeholder="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AM_01) %>" tabindex="1" />
                <div class="ogs-fader"></div>
                <div id="ogs_searchbtn" title="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AM_02) %>">
                    <div id="ogs_mg"></div>
                </div>
            </div>
        </form>
    </div>
</div>

<%-- need to ensure that this layout script is always loaded along with the body --%>
<script type="text/javascript">
    $(document).ready(function () {
        var regex = new RegExp("[\\?&]SearchString=([^&#]*)");
        var results = regex.exec(location.search);
        var term = (results == null) ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

        $("#s").val(term);

        $("#s").focus(function () {
            $(this).css("padding-right", "40px");
            $(".ogs-fader").hide();
            $("#ogs_searchbtn").css("background", "#f99d1c");

            $("#ogs_mg").addClass("mg-active");
            setTimeout(function () { $("#s").select(); }, 100);
        });
        $("#s").focusout(function () {
            $(this).css("padding-right", "5px");
            setTimeout(function () { $(".ogs-fader").show(); }, 150);
            $("#ogs_searchbtn").css("background", "#222222");
            $("#ogs_mg").removeClass("mg-active");
        });

        $(".ogs-fader").click(function () {
            $("#s").trigger("focus");
        });

        $("#ogs_searchbtn").click(function () {
            if ($("#s").val().length > 0) {
                setCookie("StartClientTime", new Date().getTime(), 'days', 1);
                $("#searchform")[0].submit();
            }
        });

        $("#s").keypress(function (e) {
            if (e.which == 13 && $("#s").val().length > 0) {
                setCookie("StartClientTime", new Date().getTime(), 'days', 1);
                $("#searchform")[0].submit();
            }
        });
    });
</script>

<%-- manually handle placeholder text for IE only because they don't support it --%>
<!--[if IE]>
<script type="text/javascript">
    $(document).ready(function() {
        var ogs_search = $("#s");
        ogs_search.attr("placeholder", "<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AM_01) %>");
        ogs_search.focus(function() {
            if ( $(this).val() == $(this).attr("placeholder") ) {
                $(this).val("");
            }
            $(this).css({"color":"#222222","font-style":"normal"});
        });
        ogs_search.blur(function() {
            if ($(this).val().length == 0) {
                $(this).val($(this).attr("placeholder"));
                $(this).css({ "color": "#aaaaaa", "font-style": "italic" });
            } 
            else {
                $(this).css({"color":"#ffffff","font-style":"normal"});
            }
        });
        ogs_search.trigger("blur");
    });
</script>
<![endif]-->

<%-- notification panel dynamically added --%>