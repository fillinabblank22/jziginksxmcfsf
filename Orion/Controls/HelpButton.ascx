<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>
<%@ Control Language="C#" ClassName="HelpButton" %>

<script runat="server">
	private string _helpUrlFragment;

	public string HelpUrlFragment
	{
		get { return _helpUrlFragment; }
		set { _helpUrlFragment = value; }
	}

	public static string GetHelpUrl(string fragment)
	{
	    return HelpHelper.GetHelpUrl(fragment);
	}
	
    protected string HelpURL
    {
        get
        {
			return GetHelpUrl(this.HelpUrlFragment);
        }
    }

    [Obsolete("Use orion:LocalizableButton or orion:LocalizableButtonLink with custom text")]
	public string ImageName
	{
		get { return string.Empty; }
		set { }
	}
</script>
<%-- don't copy this code, use a localizable button --%>
<a class="sw-btn sw-btn-resource HelpButton" automation="Help" href="<%= DefaultSanitizer.SanitizeHtml(this.HelpURL) %>" target="_blank" rel="noopener noreferrer">
	<span class="sw-btn-c"><span class="sw-btn-t"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcesAll_Help) %></span></span>
</a>