﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AcknowledgeAlertDialogControl.ascx.cs" Inherits="Orion_Controls_AcknowledgeAlertDialogControl" %>

<style>
	.subheader {
		color: #646464;
		font-size: 10px;
	}
</style>

<asp:Panel ID="AcknowledgeDialog" CssClass="acknowledgeAlertDialog" runat="server">
    <span>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_105) %>
    </span>
    <div style="font-weight: bold; padding-top: 5px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_108) %></div>
    <div>
        <textarea style="width: 565px; height: 150px;" id="Message_<%= ClientID %>"></textarea>
    </div>
    <div style="height: 50px">&nbsp;</div>
    <table style="width: 100%;">
        <tr>
            <td>
				<table>
					<tr>
						<td><input type="checkbox" id="DontAsk_<%= ClientID %>" class="dontAsk" /></td>
						<td><label for="DontAsk_<%= ClientID %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_107) %></label></td>
					</tr>
					<tr>
						<td></td>
						<td><span class="subheader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0042) %></span></td>
					</tr>
				</table>
            </td>
            <td style="padding: 5px; white-space: nowrap; vertical-align: top;">
                <div class="sw-btn-bar-wizard">
                      <span class="acknowledgeButtonPlaceHolder"></span>
                      <span class="cancelButtonPlaceHolder"></span>       
                </div>   
            </td>
        </tr>
    </table>
</asp:Panel>

<style type="text/css">
    .acknowledgeAlertDialog
    {            
         display: none;
         overflow: hidden !important;
         background-color: #FFFFFF !important;
         padding-bottom: 20px !important;
    }
</style>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $(window).resize(function () {
            $("#<%= AcknowledgeDialog.ClientID %>").dialog("option", "position", "center");
        });

        var dialog = $("#<%= AcknowledgeDialog.ClientID %>").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            autoOpen: false,
            width: "615px",
            height: "auto",
            title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_109) %>",
            close: function () {

            },
            open: function () {
            }
        });

        var confirmFn = null;

        $("#<%= AcknowledgeDialog.ClientID %> span.acknowledgeButtonPlaceHolder").empty();
        $("#<%= AcknowledgeDialog.ClientID %> span.cancelButtonPlaceHolder").empty();
        $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_110) %>', { type: 'primary'})).appendTo($("#<%= AcknowledgeDialog.ClientID %> span.acknowledgeButtonPlaceHolder")).click(function() {
            if (confirmFn != null && jQuery.isFunction(confirmFn)) {
                confirmFn($("#Message_<%= ClientID %>").val());
            }
        });

        $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_111) %>', { type: 'secondary' })).appendTo($("#<%= AcknowledgeDialog.ClientID %> span.cancelButtonPlaceHolder")).click(function () {
            dialog.dialog("close");
        });

        SW.Core.namespace("SW.Orion.Alerts").AcknowledgeAlertDialog = function (dialog) {
            // CONSTRUCTOR

            // PRIVATE METHODS
            var showDialog = function (closeFunction) {
                if (typeof (closeFunction) != "undefined") {
                    dialog.on("dialogclose", closeFunction);
                } else {
                    dialog.off("dialogclose");
                }

                dialog.dialog("open");
            };

            // PUBLIC METHODS
            return {
                ClearDialog: function () {
                    $("#Message_<%= ClientID %>").val('');
                },
                ShowDialog: function (confirmFunction, closeFunction) {
                    confirmFn = confirmFunction;
                    showDialog(closeFunction);
                },
                HideDialog: function () {
                    dialog.dialog("close");
                },
                /**
                @returns True in the case, that dialog is Enabled for current user, otherwise false.
                */
                GetShowStatus: function () {
                    var res = true;
                    SW.Core.Services.callControllerSync("/api/ActiveAlert/IsShowAcknowledgeAlertDialogEnabled", null, function (result) {
                        res = result;
                    }, function (failure) {
                    });

                    return res;
                },
                SaveShowStatus: function (onSuccess, onFailure) {
                    SW.Core.Services.callController("/api/ActiveAlert/SetShowAcknowledgeAlertDialog", !$("#<%= AcknowledgeDialog.ClientID %> #DontAsk_<%= ClientID %>").prop("checked"), onSuccess, onFailure);
                },
                SetActualDialogShowStatus: function(showNoteDialog) {
                    $("#<%= AcknowledgeDialog.ClientID %> #DontAsk_<%= ClientID %>").prop("checked", !showNoteDialog);
                },
                AcknowledgeAlerts: function (alertObjectId, onSucc, onFail) {
                    var getAlertItemsFromIds = function (items, acknowledgeNote) {
                        var ids = [];                       
                        for (var i = 0; i < items.length; i++) {
                            ids.push({
                                AlertObjectID: items[i].AlertObjectID,
                                AlertDefID: items[i].AlertDefID,
                                ActiveNetObject: items[i].ActiveNetObject,
                                ObjectType: items[i].ObjectType,
                                SiteID: items[i].SiteID,
                                Note: acknowledgeNote
                            });
                        }

                        return ids;
                    };

                    var acknowledgeFn = function (acknowledgeNote) {
                        <% if (!this.IsDemoServer) { %>
                            SW.Core.Services.callController('/api/ActiveAlertsGrid/AcknowledgeActiveAlerts', getAlertItemsFromIds(alertObjectId, acknowledgeNote),
                                function (result) {
                                    if (result.Acknowledged) {
                                        onSucc(result);
                                    }

                                    SW.Orion.Alerts.AcknowledgeAlertDialog.SaveShowStatus(function() {}, function() {});
                                    SW.Orion.Alerts.AcknowledgeAlertDialog.HideDialog();
                                }, function (error) {
                                onFail(error);
                            });
                        <% } else { %>
                            demoAction('Core_Alerting_Acknowledge', this);
                        <% } %>
                    };

                    var showDialogStatus = SW.Orion.Alerts.AcknowledgeAlertDialog.GetShowStatus();
                    SW.Orion.Alerts.AcknowledgeAlertDialog.SetActualDialogShowStatus(showDialogStatus);
                    if (showDialogStatus) {
                        SW.Orion.Alerts.AcknowledgeAlertDialog.ClearDialog();
                        SW.Orion.Alerts.AcknowledgeAlertDialog.ShowDialog(acknowledgeFn);
                    } else {
                        acknowledgeFn("");
                    }
                },
                UnacknowledgeAlert: function (alertObjectId, onSucc, onFail) {
                    <% if (!this.IsDemoServer) { %>
                    SW.Core.Services.callController('/api/ActiveAlertsGrid/UnAcknowledgeActiveAlert', { AlertObjectID: alertObjectId },
                        function(result) {
                            onSucc(result);
                        }, function (error) {
                        onFail(error);
                        });
                    <% } else { %>
                    demoAction('Core_Alerting_Unacknowledge', this);
                    <% } %>
                }
            };
        }(dialog);
    });
</script>
