﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PercentStatusBar.ascx.cs"
    Inherits="Orion_Controls_PercentStatusBar" %>
<orion:Include runat="server" File="Resources.css" />

<asp:Panel ID="OuterBorder" runat="server" Width="102px" CssClass="BarBackground">
    <asp:Panel ID="StatusPanel" runat="server" Font-Size="1px">
            &nbsp;
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
