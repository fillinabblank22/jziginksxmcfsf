<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimePeriodSelectionControl.ascx.cs" Inherits="Orion_Controls_TimePeriodSelectionControl" %>
<orion:Include ID="Include1" runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include ID="Include2" runat="server" File="js/jquery/timePicker.css" />
<div class="timeSelector" runat="server" id="timeSelectorContainer"> 
    <div style="float:left; margin-top:10px; margin-left:15px">
                             <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_205) %></label>
                            <asp:TextBox ID="txtIntervalStartTime" data-form="periodStartTime" runat="server" Width="78px" CssClass="timePicker disposable" />
                            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_206) %></label>
                            <asp:TextBox ID="txtIntervalEndTime" data-form="periodEndTime" runat="server" Width="78px" CssClass="timePicker disposable" /> 
                            </div>                     
                            <div style="margin-top:10px; margin-right: 5px; float: right;" class="allDaySection">
                                <input type="checkbox" class="allDayCheckBox"/>
                                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_16) %></label>
                            </div>

                            <div class="crossButton" style="float: right;">
                                <img style="margin:5px 5px; cursor: pointer;" alt="" src="/Orion/images/Reports/delete_netobject.png">
                            </div>
    </div>
    <div style="margin-bottom: 10px;">
		<div>
			<div class="timeValidationMessage sw-suggestion sw-suggestion-fail" validate-for="periodStartTime" style="display: none;">
				<span class="sw-suggestion-icon"></span>
				<label style="color: red; font-size:12px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0045) %></label>
			</div>
		</div>
		<div class="timeValidationMessage sw-suggestion sw-suggestion-fail" validate-for="periodEndTime" style="display: none;">
			<span class="sw-suggestion-icon"></span>
			<label style="color: red; font-size:12px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0046) %></label>
		</div>
	</div>

<script type="text/javascript">
    (function ($) {
        var startDateTime;
        var endDateTime;
        var regionalSettings = <%= DatePickerRegionalSettings.GetDatePickerRegionalSettings() %>;
        $.fn.orionGetDate = function () {
            var timepicker = $('.timePicker', this);
            var datepart = new DateTime;
            var timepart = timepicker[0].timePicker.getTime();
            return new Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());
        };

        $.fn.orionSetDate = function (date) {
            $('.timePicker', this)[0].timePicker.setTime(date);
            date.setTime(time);
        };

        $('.timePicker').timePicker({ separator: regionalSettings.timeSeparator, show24Hours: regionalSettings.show24Hours });
             $.fn.validateDateField = function(elem) {
                    try {
                        // validate input
                        $.datepicker.parseDate(regionalSettings.dateFormat, $(elem).val());
                        return true;
                    } catch(err) {
                        return false;
                    }

                };
         $.fn.validateTimeField = function(elem) {
                var dtRegex;
                if (regionalSettings.show24Hours)
                    dtRegex = new RegExp('^(?:[01]?[0-9]|2[0-3]):[0-5][0-9]$');
                else
                    dtRegex = new RegExp('^(?:[0]?[1-9]|1[0-2]):[0-5][0-9]( AM| PM)?$');
                if (!dtRegex.test($(elem).val()))
                    return false;
                else 
                    return true;
            };

         $.fn.attachDateTimeValidation = function(elem) {
         	$(elem).find('.timePicker').on('change',function() {
         		if ($.fn.validateTimeField(this)) {
         			$(elem).parent().find(".timeValidationMessage[validate-for='"+ $(this).attr("data-form") +"']").hide();
         			$(this).removeClass("sw-validation-input-error");
         		}
         		else  {
         			$(elem).parent().find(".timeValidationMessage[validate-for='"+ $(this).attr("data-form") +"']").show();
         			$(this).addClass("sw-validation-input-error");
         		}
            });
        };

        $("#<%= timeSelectorContainer.ClientID %>").find(".crossButton").one("click", function () {
            var $currentContainer = $(this).closest(".TimeControl");
            if ($currentContainer.find(".timeSelector").length == 2) {
                $currentContainer.find(".timeSelector").find(".crossButton").hide();
                $currentContainer.find(".timeSelector").find(".allDaySection").show();
                
                if ($currentContainer.find(".timeSelector").find(".allDaySection .allDayCheckBox").is(":checked")&&
                   (startDateTime != undefined && endDateTime != undefined) ) {
                    $currentContainer.find(".timePicker")[0].timePicker.setTime(new Date(startDateTime));
                    $currentContainer.find(".timePicker")[1].timePicker.setTime(new Date(endDateTime));
                }

                $currentContainer.find(".timePicker").attr("disabled", false);
                $currentContainer.find(".timeSelector").find(".allDaySection .allDayCheckBox").attr('checked', false);

            } else {
                $currentContainer.find(".timeSelector").find(".crossButton").show();
                $currentContainer.find(".timeSelector").find(".allDaySection").hide();
            }
            $(this).closest(".timeSelector").remove();
        });

        $("#<%= timeSelectorContainer.ClientID %>").find(".allDaySection input").on("click", function() {
            var $currentContainer = $(this).closest(".timeSelector");
            var isChecked = $currentContainer.find(".allDayCheckBox").is(':checked');
            if (isChecked) {
                startDateTime = $currentContainer.find(".timePicker")[0].timePicker.getTime();
                endDateTime = $currentContainer.find(".timePicker")[1].timePicker.getTime();
            }
            $currentContainer.find(".timePicker").attr("disabled", isChecked);
            if (isChecked) {
                $currentContainer.find(".timePicker")[0].timePicker.setTime(new Date(0, 0, 0, 0, 0, 0));
                $currentContainer.find(".timePicker")[1].timePicker.setTime(new Date(0, 0, 0, 0, 0, 0));
            } else {
                $currentContainer.find(".timePicker")[0].timePicker.setTime(new Date(startDateTime));
                $currentContainer.find(".timePicker")[1].timePicker.setTime(new Date(endDateTime));
            }
        });
    })(jQuery);
</script>


