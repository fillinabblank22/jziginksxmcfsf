﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Actions.Impl.ExecuteExternalProgram;
using SolarWinds.Orion.Core.Actions.Utility;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web.Actions;
using System.Security.Principal;

public partial class Orion_Controls_WindowsCredentialsControl : System.Web.UI.UserControl
{
    public string userName;
    private static readonly string localizedNtAuthority = new SecurityIdentifier(WellKnownSidType.LocalSystemSid, null)
            .Translate(typeof(NTAccount)).Value.Split('\\')[0];

    protected void Page_Load(object sender, EventArgs e)
    {
        var credential = ActionCredentialHelper.GetDefaultActionCredential();

        userName = credential.Username;
        string[] domainUsername = userName.Split('\\');
        if (domainUsername[0].Equals(localizedNtAuthority, StringComparison.OrdinalIgnoreCase))
        {
            userName = domainUsername[1];
        }
    }

    public void SetCredentials(string credentialsId)
    {
        int credId;
        if (credentialsId == ExecuteExternalProgramConstants.NotCredentialsDefinedId || !int.TryParse(credentialsId, out credId))
        {
            notUseUser.Checked = true;
        }
        else
        {
            useUser.Checked = true;
            UserName.Text = new CredentialManager().GetCredential<UsernamePasswordCredential>(credId).Name;
        }
    }
}
