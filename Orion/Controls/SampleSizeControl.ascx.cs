using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;

public partial class SampleSizeControl : System.Web.UI.UserControl
{
	private string _helpfulText = Resources.CoreWebContent.WEBCODE_AK0_97;

	[PersistenceMode(PersistenceMode.Attribute)]
	public string HelpfulText
	{
		get { return _helpfulText; }
		set { _helpfulText = value; }

	}

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool InLine { get; set; }

	private void SetupControlFromRequest()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);

		if (!string.IsNullOrEmpty(resource.Properties["SampleSize"]))
		{
			SampleSizeValue = resource.Properties["SampleSize"];
		}
		else
		{
			SampleSizeValue = "30M";
		}
	}

	private void OnInitListSampleSizes()
	{
		List<SampleSize> list = new List<SampleSize>();

		list = SampleSizes.GenerateSelectionList();
		SampleSizeList.DataSource = list;
		SampleSizeList.DataTextField = "Name";
		SampleSizeList.DataValueField = "SizeName";
		SampleSizeList.DataBind();
	}

	protected override void OnInit(EventArgs e)
	{
		OnInitListSampleSizes();

		if (!Page.IsPostBack)
		{
			SetupControlFromRequest();
		}

        //for custom object resource 
        if (HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            SampleSizeList.Attributes.Add("onchange", "javascript:SaveData('SampleSize', this.value);");
        }

		base.OnInit(e);
	}

	public string SampleSizeValue
	{
		get { return SampleSizeList.SelectedValue; }
		set { SampleSizeList.SelectedValue = value; }
	}
	public DropDownList SampleSizeListControl 
	{
		get 
		{
			return SampleSizeList;
		}
	}
}
