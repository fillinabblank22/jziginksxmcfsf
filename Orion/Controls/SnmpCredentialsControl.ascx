<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SnmpCredentialsControl.ascx.cs"
    Inherits="Orion_Controls_SnmpCredentialsControl" %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="SnmpCredentialsController.js" />
<style>
    .credContainer{ border: 1px dashed gray; margin: 10px 0; padding: 10px; }
    .errorLabel { color: red; }
    .labels{ float: left; padding:0 5px; max-width: 250px;}
    .labelText { line-height: 22px;}
</style>
<script type="text/javascript">
    $(function() {
        SW.Core.SNMPCredentialsController = new SW.Core.SnmpCredentials(
            { 
                containerID: 'SNMPSettingsControl'
            });
        SW.Core.SNMPCredentialsController.init();
    })    
</script>
<div id="SNMPSettingsControl">
    <span class="sectionHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_63) %></span>
        <asp:DropDownList ID="SnmpType" runat="server" data-form="SNMPVersion"/>
    <div id="credEditV1V2Controls" class="credContainer">
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_64) %></span>
        <asp:TextBox ID="CommunityStringTB" runat="server" Width="270" Enabled="true" autocomplete="off" MaxLength="250" data-form="communityString"/>
        <div id="EmptyCommunityStringErrorPanel" style="display: none;">
            <span class="errorLabel">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_65) %></span>
        </div>
        <div id="AmbigousCommunityString" style="display: none;">
            <span class="errorLabel">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_66) %></span>
        </div>
    </div>
    <div id="credEditV3Controls" class="credContainer">
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_67) %>:</span>
        <br/>
        <div class="labels">
            <div>
                <span class="labelText">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_73) %>
                </span>
             </div>
            <div>
                <span class="labelText">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_75) %>
                </span>
                </div>
                <div>
                <span class="labelText">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_76) %>
                </span>
            </div>
            <div>
                <span class="labelText">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_77) %>
                </span>
               </div>
                <div>
                <span class="labelText">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_78) %>
                </span>
               
            </div>
        </div>
        <div>
             <div>
                <span>
                    <asp:TextBox ID="UserNameTextBox" runat="server" data-form="snmpCredUserName" Width="150" Enabled="true" autocomplete="off" />
                </span>
            </div>
            <div>
                <span>
                    <asp:DropDownList ID="AuthMetodDropList" runat="server" Width="100" data-form="authMethodType" />
                </span>
                </div>
                <div class="authMethodSettings">
                <span>
                    <asp:TextBox ID="AuthPassword" data-form="authPassword" runat="server" Width="150" Enabled="true" autocomplete="off" />
                </span>
                <span>
                    <asp:CheckBox ID="AuthPasswordIsKey" data-form="authPasswordIsKey" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_57 %>" />
                </span>
            </div>
            <div>
                <span>
                    <asp:DropDownList ID="PrivMethodDropList" runat="server" Width="100" data-form="privMethodType" />
                </span>
                </div>
                <div class="privMethodSettings">
                <span>
                    <asp:TextBox ID="PrivPassword" data-form="privPassword" runat="server" Width="150" Enabled="true" autocomplete="off"/>
                </span>
                <span>
                    <asp:CheckBox ID="PrivPasswordIsKey" data-form="privPasswordIsKey" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_57 %>" />
                </span>
            </div>
            </div>
    </div>
</div>
