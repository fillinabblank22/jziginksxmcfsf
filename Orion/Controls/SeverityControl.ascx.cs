using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

using SolarWinds.Logging;
using SolarWinds.Orion.Web.SyslogTrapInterfaces;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class SeverityControl : System.Web.UI.UserControl, ISeverityControl
{
	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private bool _addShowAllItem = true;

    public bool AddShowAllItem
    {
        get { return _addShowAllItem; }
        set { _addShowAllItem = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string DataForm { get; set; }

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public String SeverityCode
	{
		get { return severities.SelectedValue; }
		set { severities.SelectedValue = value; }
	}

	public string SeverityName
	{
		set
		{
			foreach (ListItem item in severities.Items)
			{
				if (item.Text.Equals(value, StringComparison.OrdinalIgnoreCase))
				{
					severities.SelectedValue = item.Value;
					break;
				}
			}
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        if (!string.IsNullOrEmpty(DataForm))
            severities.Attributes.Add("data-form", DataForm);

		string selectedvalue = string.Empty;
		if (!string.IsNullOrEmpty(SeverityCode))
		{
			selectedvalue = SeverityCode;
		}

		severities.Items.Clear();
        if (_addShowAllItem)
            severities.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_183, string.Empty));

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			StringDictionary sevDictionary = proxy.GetSeverities();
			
			foreach (string sevKey in sevDictionary.Keys)
			{
				severities.Items.Add(new ListItem(sevDictionary[sevKey], sevKey));
			}
		}

		SeverityCode = selectedvalue;
	}
}
