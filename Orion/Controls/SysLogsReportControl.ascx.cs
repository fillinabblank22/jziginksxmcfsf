using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.SyslogTraps.Common;
using Limitation = SolarWinds.Orion.Web.Limitation;

public partial class SysLogsReportControl : System.Web.UI.UserControl
{
    private static readonly Log _log = new Log();

    private bool _printable;
    private string[] _columns;

    protected override void OnInit(EventArgs e)
    {
        _columns = RegistrySettings.GetSetting(
            "Syslog Viewer", "Settings", "Display Fields", "DateTime,Hostname,SeverityName,Message").Split(',');
        base.OnInit(e);
    }

    public string[] Columns
    {
        get { return _columns; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Printable
    {
        set { _printable = value; }
        get { return _printable; }
    }

    public bool AllowEventClear
    {
        get
        {
            bool result = false;
            if (bool.TryParse(HttpContext.Current.Profile.PropertyValues["AllowEventClear"].PropertyValue.ToString(), out result))
            {
                return result;
            }
            else
            {
                return false;
            }
        }
    }

    public static string GetColumnName(string column)
    {
        switch (column.ToUpper())
        {
            case "ACKNOWLEDGED":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_185;
            case "MSGID":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_186;
            case "HOSTNAME":
                return Resources.SyslogTrapsWebContent.WEBDATA_TM0_4;
            case "DATETIME":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_187;
            case "IP":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_13;
            case "MESSAGE":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_150;
            case "MESSAGETYPE":
                return Resources.SyslogTrapsWebContent.WEBCODE_AK0_91;
            case "FACILITYNAME":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_188;
            case "SEVERITYNAME":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_200;
            case "SYSLOGFACILITY":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_188;
            case "SYSLOGSEVERITY":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_200;
            case "SEVERITYICON":
                return "&nbsp;";
            case "CAPTION":
                return Resources.SyslogTrapsWebContent.NetworkObjectType_Node;
            case "NODEID":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_189;
            case "DYNAMICIP":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_123;
            case "DNS":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_126;
            case "COMMUNITY":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_190;
            case "SYSNAME":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_191;
            case "VENDOR":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_124;
            case "MACHINETYPE":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB0_16;
            case "SYSLOGTAG":
                return Resources.SyslogTrapsWebContent.WEBDATA_VB1_34;
            default:
                return column;
        }
    }

    public static string FormatColumnValue(string column, string value, string nodeID)
    {
        string linkStart = string.Empty;
        string linkEnd = string.Empty;
        if (!string.IsNullOrEmpty(nodeID))
        {
            linkStart = "<a href=\"/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeID + "\">";
            linkEnd = "</a>";
        }
        else
        {
            linkStart = string.Empty;
            linkEnd = string.Empty;
        }

        switch (column.ToUpper())
        {
            case "ACKNOWLEDGED":
                return (string.Compare(value.Trim(), "0", StringComparison.InvariantCultureIgnoreCase) == 0) ? Resources.SyslogTrapsWebContent.WEBDATA_VB0_125 : Resources.SyslogTrapsWebContent.WEBDATA_VB0_124;
            case "MSGID":
                return linkStart + value.Trim() + linkEnd;
            case "DATETIME":
                DateTime dateTime = new DateTime();
                if (DateTime.TryParse(value, out dateTime))
                {
                    return dateTime.ToString(CultureInfo.CurrentCulture);
                    //If Int(Now) <> Int(DateTime) Then
                    //    mResponse.Write "    <td " & Class & ">" & Format(DateTime, "Short Date") & " " & FormatToLocalTime(DateTime) & "&nbsp;</td>" & vbCrLf
                    //Else
                    //    mResponse.Write "    <td " & Class & ">" & FormatToLocalTime(DateTime) & "&nbsp;</td>" & vbCrLf
                    //End If
                }
                else
                {
                    return "&nbsp;";
                }
            case "IP":
            case "HOSTNAME":
                return linkStart + value.Trim() + linkEnd;
            case "MESSAGE":
                //Message = Trim$(TB.Fields("Message").Value & "")
                //If Len(mRelatedIP) > 0 Then Message = Replace(Message, mRelatedIP, "<b>" & mRelatedIP & "</b>")
                //mResponse.Write "    <td " & Class & ">" & LinkStart & Message & LinkEnd & "&nbsp;</td>" & vbCrLf
                return HttpUtility.HtmlEncode(value.Trim());
            case "MESSAGETYPE":
            case "FACILITYNAME":
            case "SYSLOGFACILITY":
            case "SYSLOGSEVERITY":
            case "SEVERITYNAME":
                return value.Trim();
            case "SEVERITYICON":
                return "<img src=\"/NetPerfMon/images/Severity" + value.Trim() + ".gif\">";
            case "CAPTION":
            case "NODEID":
            case "DNS":
                return linkStart + value.Trim() + linkEnd;
            case "DYNAMICIP":
                switch (value.Trim().ToUpper())
                {
                    case "":
                        return "";
                    case "TRUE":
                    case "YES":
                    case "Y":
                    case "1":
                        return linkStart + Resources.SyslogTrapsWebContent.WEBDATA_VB0_124 + linkEnd;
                    default:
                        return linkStart + Resources.SyslogTrapsWebContent.WEBDATA_VB0_125 + linkEnd;
                }
            default:
                return value.Trim();
        }
    }

    public void GenerateReport(string netObjectID, string deviceType, string vendor, string ipAddress, byte severityCode, byte facilityCode, string messageType, string messagePattern, string tag, string period, int maxRecords, bool showCleared, string relatedIP, string relatedMAC)
    {
        table.ItemDataBound += new RepeaterItemEventHandler(table_ItemDataBound);

        DateTime periodBegin = new DateTime();
        DateTime periodEnd = new DateTime();

        Periods.Parse(ref period, ref periodBegin, ref periodEnd);
        using (var proxy = SyslogTrapBusinessLayerProxyFactory.Instance.Create())
        {
            DataTable syslogs = proxy.Api.GetSysLogTable(netObjectID,
                deviceType,
                vendor,
                ipAddress,
                severityCode,
                facilityCode,
                messageType,
                messagePattern,
                tag,
                periodBegin,
                periodEnd,
                maxRecords,
                showCleared,
                relatedIP,
                relatedMAC,
                Limitation.GetCurrentListOfLimitationIDs());

            if (syslogs.Rows.Count == 0 && !proxy.Api.AreSyslogMessagesAvailable())
            {
                noSyslogsError.Visible = true;
                locClear.Visible = false;
                locButtonSelect.Visible = false;
                locButtonDeselect.Visible = false;
                table.Visible = false;
            }
            else
            {
                table.Visible = true;
                noSyslogsError.Visible = false;
                if (AllowEventClear && !Printable)
                {
                    locClear.Visible = true;
                    locButtonSelect.Visible = true;
                    locButtonDeselect.Visible = true;
                }
                else
                {
                    locClear.Visible = false;
                    locButtonSelect.Visible = false;
                    locButtonDeselect.Visible = false;
                }
                table.DataSource = syslogs;
                table.DataBind();
            }
        }
        this.SyslogActions.Visible = this.Profile.AllowEventClear
            && !Printable && (table.Items.Count >= CommonConstants.MIN_ROWS_NUMBER);
    }

    void table_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {
            for (int i = 0; i < Columns.Length; i++)
            {
                StringBuilder control = new StringBuilder();
                control.Append("<td class='Severity");
                control.Append(DataBinder.Eval(e.Item.DataItem, "SysLogSeverity").ToString());
                control.Append("'>");
                control.Append(FormatColumnValue(Columns[i],
                    DataBinder.Eval(e.Item.DataItem, (Columns[i].Equals("SEVERITYICON", StringComparison.InvariantCultureIgnoreCase)) ? "SysLogSeverity" : Columns[i]).ToString(),
                    DataBinder.Eval(e.Item.DataItem, "NodeID").ToString()));
                control.Append("</td>");

                e.Item.Controls.Add(new LiteralControl(control.ToString()));
            }
            e.Item.Controls.Add(new LiteralControl("</tr>"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ClearMessages_Click(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            return;

        List<long> sysLogKeys = new List<long>();
        foreach (RepeaterItem item in table.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("cbSysLog");
            if (cb != null)
            {
                if (cb.Checked && cb.Enabled)
                {
                    HiddenField hf = (HiddenField)item.FindControl("sysLogKey");
                    if (hf != null)
                    {
                        sysLogKeys.Add(Convert.ToInt64(hf.Value));
                    }
                }
            }
        }

        using (var proxy = SyslogTrapBusinessLayerProxyFactory.Instance.Create())
        {
            proxy.Api.ClearMessages(sysLogKeys);
        }
    }
}
