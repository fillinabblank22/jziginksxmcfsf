<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertsReportControl.ascx.cs" Inherits="AlertsReportControl" %>

<%@ Register Src="~/Orion/Controls/AlertDetailsControl.ascx" TagName="AlertDetails" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Controls/AcknowledgeAlertsControl.ascx" TagName="AcknowledgeAlerts" TagPrefix="orion" %>

<style type="text/css">
    .disabledAlertNote
    {
        background-color: #EEECDE;
    }      
</style>
<orion:AlertDetails runat="server" ID="AlertDetailsPopup" />
<orion:AcknowledgeAlerts runat="server" ID="AcknowledgeAlerts" />

<table style="width:100%;">
    <tr>
        <td>
            <table width="100%" cellpadding="10" runat="server" id="AlertActionsTable">
                <tr>
                    <td colspan="3">
                        <div class="sw-btn-bar">
                            <orion:LocalizableButton runat="server" ID="ImageButton1" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_140 %>"
                                DisplayType="Small" OnClientClick="$('.AlertCheckBox input:enabled').attr('checked','checked'); return false;" />

                            <orion:LocalizableButton runat="server" ID="ImageButton2" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_225 %>"
                                DisplayType="Small" OnClientClick="$('.AlertCheckBox input:enabled').removeAttr('checked'); return false;" />

                            <orion:LocalizableButton runat="server" ID="ClearEventsButton" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_151 %>"
                                DisplayType="Small" OnClientClick="ShowAlertAcknowledge(); return false;" />
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                            <table class="Report" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                <td class="sw-pg-colcbox" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-coltime" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colimg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                                <td class="sw-pg-colmsg" style="height: 1px;"><!-- ie --></td>
                            </tr>
                            <tr class="sw-pg-text">
                            <td class="sw-pg-colcbox"></td>
                            <td class="sw-pg-colcbox"></td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByTime" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_20 %>" OnClick="SortChange" />
                                <asp:PlaceHolder runat="server" ID="TimeDirImage"></asp:PlaceHolder> 
                            </td>
                            <td class="sw-pg-coltime" >
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByAlertName" runat="server" OnClick="SortChange" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_146 %>" />
                                <asp:PlaceHolder runat="server" ID="AlertNameDirImage"></asp:PlaceHolder>
                            </td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByAlertType" runat="server" OnClick="SortChange" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_147 %>" />
                                <asp:PlaceHolder runat="server" ID="AlertTypeDirImage"></asp:PlaceHolder>
                            </td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByCaption" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_148 %>" OnClick="SortChange"/>
                                <asp:PlaceHolder runat="server" ID="CaptionDirImage"></asp:PlaceHolder>
                            </td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByAcknowledgedBy" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_149 %>" OnClick="SortChange"/>
                                <asp:PlaceHolder runat="server" ID="AcknowledgedByDirImage"></asp:PlaceHolder>
                            </td>
                            <td class="sw-pg-coltime">
                                <asp:LinkButton class="sw-pg-nonbutton" ID="SortByAcknowledgedTime" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_150 %>" OnClick="SortChange"/>
                                <asp:PlaceHolder runat="server" ID="AcknowledgedTimeDirImage"></asp:PlaceHolder>
                            </td>
                            </tr>
                               </thead>
                        <asp:Repeater runat="server" ID="table" OnItemDataBound="TableItemDataBound" >                
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:CheckBox runat="server" CssClass="AlertCheckBox" ID="cbAlert"
                                        Checked='<%# DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1" ? true : false %>' 
                                        Enabled='<%# AbleToAcknowledge(Eval("Acknowledged"), Eval("MonitoredProperty")) %>'
                                        Visible='<%# AllowEventClear ? true : false %>'/>
                                    <asp:HiddenField runat="server" ID="AlertKey" Value='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "AlertID") + ":" + DataBinder.Eval(Container.DataItem, "ObjectID") + ":" + DataBinder.Eval(Container.DataItem, "ObjectType")) %>'  />                                    
                                </td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><img alt="" src="/NetPerfMon/images/Event-19.gif" /></a></td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><%# DefaultSanitizer.SanitizeHtml(ReformatDateTime(Eval("AlertTime").ToString())) %></a></td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("AlertName")) %></a></td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Common.OrionMessagesHelper.GetLocalizedMessageTypeLabel(Eval("MonitoredProperty").ToString(), false, false)) %></a></td>
                                <td runat="server" id="netObjectTD"><asp:Label runat="server" ID="lblObjectName" Text='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "ObjectName").ToString()) %>' Visible="false"></asp:Label></td>
                                <td><asp:Label runat="server" ID="lblAcknowledgedBy" Text='<%# DefaultSanitizer.SanitizeHtml((DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1") ? DataBinder.Eval(Container.DataItem, "AcknowledgedBy") : "") %>'></asp:Label></td>
                                <td><asp:Label runat="server" ID ="lblAcknowledgedTime" Text='<%# DefaultSanitizer.SanitizeHtml((DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1") ? DataBinder.Eval(Container.DataItem, "AcknowledgedTime") : "") %>'></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr style="background-color:#f8f8f8;">
                                <td>
                                    <asp:CheckBox runat="server" CssClass="AlertCheckBox"
                                        ID="cbAlert"
                                        Checked='<%# DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1" ? true : false %>' 
                                        Enabled='<%# AbleToAcknowledge(Eval("Acknowledged"), Eval("MonitoredProperty"))? true : false %>' 
                                        Visible='<%# AllowEventClear ? true : false %>'/>
                                    <asp:HiddenField runat="server" ID="AlertKey" Value='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "AlertID") + ":" + DataBinder.Eval(Container.DataItem, "ObjectID") + ":" + DataBinder.Eval(Container.DataItem, "ObjectType")) %>'  />
                                </td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><img alt="" src="/NetPerfMon/images/Event-19.gif" /></a></td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><%# DefaultSanitizer.SanitizeHtml(ReformatDateTime(Eval("AlertTime").ToString())) %></a></td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("AlertName")) %></a></td>
                                <td><a href='<%# DefaultSanitizer.SanitizeHtml(string.Format("javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});", Eval("AlertID"), GetNetObjectIdByObjectType(Eval("ObjectType").ToString(), Eval("ObjectID").ToString(), Eval("MonitoredProperty").ToString()), Eval("ObjectType"), AllowEventClear.ToString().ToLowerInvariant())) %>'><%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Common.OrionMessagesHelper.GetLocalizedMessageTypeLabel(Eval("MonitoredProperty").ToString(), false, false)) %></a></td>
                                <td runat="server" id="netObjectTD"><asp:Label runat="server" ID="lblObjectName" Text='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "ObjectName").ToString()) %>' Visible="false"></asp:Label></td>
                                <td><asp:Label runat="server" ID="lblAcknowledgedBy" Text='<%# DefaultSanitizer.SanitizeHtml((DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1") ? DataBinder.Eval(Container.DataItem, "AcknowledgedBy") : "") %>'></asp:Label></td>
                                <td><asp:Label runat="server" ID ="lblAcknowledgedTime" Text='<%# DefaultSanitizer.SanitizeHtml((DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1") ? DataBinder.Eval(Container.DataItem, "AcknowledgedTime") : "") %>'></asp:Label></td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            <% if (AllowEventClear && !Printable)
               { %>
               <table cellpadding="10"><tr>
                    <td colspan="3">
                        <div class="sw-btn-bar">
                            <orion:LocalizableButton runat="server" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_140 %>" DisplayType="Small"
                                OnClientClick="$('.AlertCheckBox input:enabled').attr('checked','checked'); return false;" />
                            <orion:LocalizableButton runat="server" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_225 %>" DisplayType="Small"
                                OnClientClick="$('.AlertCheckBox input:enabled').removeAttr('checked'); return false;"/>
                            <orion:LocalizableButton runat="server" ID="btnAcknowledge" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_151 %>" DisplayType="Small"
                                OnClientClick="ShowAlertAcknowledge(); return false;" />
                        </div>
                    </td>
                </tr></table>
            <%} %>
        </td>
    </tr>
</table>
