﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageHeader.ascx.cs" Inherits="Orion_Controls_PageHeader" EnableViewState="false" %>

<orion:Include File="Mainlayout.css" runat="server" />

<div id="pageHeader" class="sw-mainnav-branding">

    <% if (OrionMinReqsMaster.IsApolloEnabled)
        { %>
    <sw-mega-menu ng-if="<%= IsPrintMode ? "false" : "true" %>" class="xui"></sw-mega-menu>
    <% }
        else
        { %>

    <div id="CustomBanner"></div>
    <asp:PlaceHolder ID="EvalBannerHolder" runat="server" />
    <div id="swNavScroll" class="header-top">
        <div id="swNav">
            <% if (ShowLogo)
                { %>
            <a href="/Orion/View.aspx" title="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_34) %>">
                <img src="<%= DefaultSanitizer.SanitizeHtml(SiteLogoUri) %>" alt="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_34) %>" class="sw-mainnav-logo" />
            </a>
            <% } %>
            <!-- tabs dynamically added -->
            <asp:PlaceHolder ID="TemplateHolder" runat="server" />
        </div>

        <div id="userName">
            <asp:PlaceHolder Visible="<%# IsMobileMode %>" runat="server">
                <asp:LoginView ID="LoginView2" runat="server">
                    <AnonymousTemplate></AnonymousTemplate>
                    <LoggedInTemplate>
                        <ul class="content ui-helper-clearfix">
                            <li class="logout">
                                <a href="javascript:void(0)" onclick="$(window).unbind('unload'); document.location.href='/Orion/Logout.aspx';">
                                    <span class="icon"></span>
                                    <span class="text"><%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_8) %></span>
                                </a>
                            </li>
                        </ul>
                    </LoggedInTemplate>
                </asp:LoginView>
            </asp:PlaceHolder>
            <asp:PlaceHolder Visible="<%# !IsMobileMode %>" runat="server">
                <ul class="content">
                    <asp:PlaceHolder ID="NotificationsHolder" Visible="<%# AllowAdmin %>" runat="server" />
                    <asp:LoginView ID="LoginView1" runat="server">
                        <AnonymousTemplate>
                            <% if (!string.IsNullOrEmpty(UsernameLabel))
                                { %>
                            <li class="logout">
                                <a class="ui-helper-clearfix" href="javascript:void(0)" onclick="$(window).unbind('unload'); document.location.href='/Orion/Logout.aspx';">
                                    <span class="icon"></span>
                                    <span class="text nav-usertext"><%# DefaultSanitizer.SanitizeHtml(UsernameLabel.ToUpper()) %></span>
                                    <span class="text">(<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_9) %>)</span>
                                </a>
                            </li>
                            <li class="help">
                                <a class="ui-helper-clearfix" href="<%# DefaultSanitizer.SanitizeHtml(HelpUrl) %>" target="_blank">
                                    <span class="icon"></span>
                                    <span class="text"><%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcesAll_Help) %></span>
                                </a>
                            </li>
                            <% } %>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <% if (AllowAdmin && IsOGSWebInstalled)
                                { %>
                            <li class="search">
                                <a id="nav-search" class="ui-helper-clearfix" href="javascript:void(0);">
                                    <span class="icon"></span>
                                </a>
                                <form id="nav-search-form" name="searchform" class="ui-helper-clearfix" action="/Orion/OGS/ResultPage.aspx" method="get">
                                    <div class="nav-search-input-container">
                                        <button class="search-input-button sw-btn sw-btn-primary" type="submit"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AM_02) %></button>
                                        <input class="search-input" type="text" name="SearchString" id="s" placeholder="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AM_01) %>" tabindex="1" />
                                    </div>
                                </form>
                            </li>
                            <% } %>
                            <li class="logout">
                                <a class="ui-helper-clearfix" href="javascript:void(0)" onclick="$(window).unbind('unload'); document.location.href='/Orion/Logout.aspx'; document.execCommand('ClearAuthenticationCache', false);">
                                    <span class="icon"></span>
                                    <span class="text nav-usertext"><%# DefaultSanitizer.SanitizeHtml(UsernameLabel.ToUpper()) %></span>
                                    <span class="text">(<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_9) %>)</span>
                                </a>
                            </li>
                            <% if (IsSearchEnabled)
                                { %>
                            <li class="search">
                                <sw-orion-search></sw-orion-search>
                            </li>
                            <% } %>
                            <li class="help">
                                <a class="ui-helper-clearfix" href="<%# DefaultSanitizer.SanitizeHtml(HelpUrl) %>" target="_blank">
                                    <span class="icon"></span>
                                    <span class="text"><%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcesAll_Help) %></span>
                                </a>
                            </li>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </ul>
            </asp:PlaceHolder>
        </div>
        <% if (OrionMinReqsMaster.IsApolloEnabled && !Profile.IsAnonymous && Profile.AllowCustomize)
            { %>
        <div class="xui sw-dashboard__editing-buttons" ng-init="dashboardTitle = ''">
            <div class="sw-dashboard__editing-title">{{ drawerOpen ? 'Add Widgets' : '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_241) %>' }}</div>
            <xui-button display-style="primary" ng-click="drawerOpen = true" ng-show="!drawerOpen">
                <span _t>Add Widgets</span>
            </xui-button>
            <xui-button display-style="tertiary" icon="check" ng-click="editingDashboard = drawerOpen = false" ng-show="!drawerOpen">
                <span _t>Done Editing</span>
            </xui-button>
            <xui-button display-style="tertiary" icon="check" ng-click="drawerOpen = false" ng-show="drawerOpen">
                <span _t>Done Adding Widgets</span>
            </xui-button>
        </div>
        <% } %>
    </div>
    <% } %>
</div>
