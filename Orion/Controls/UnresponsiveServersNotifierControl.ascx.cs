﻿using System.Web.UI;

public partial class Orion_Controls_UnresponsiveServersNotifierControl : UserControl
{
    public string GetControlContainterID()
    {
        return FindControl("MessageContainer").ClientID;
    }
}