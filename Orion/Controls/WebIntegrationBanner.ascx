<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebIntegrationBanner.ascx.cs" Inherits="Orion_Controls_WebIntegrationBanner" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<orion:Include runat="server" File="WebIntegrationBanner.css" />
<orion:Include runat="server" File="WebIntegrationLogin.css" />

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        var enabled = SettingsDAL.GetSetting("CustomerPortalIntegration-Enabled");
        if (enabled != null && enabled.SettingValue == 0)
        {
            this.Visible = false;
        }
    }

</script>

<asp:Panel ID="WebIntegrationControlContainer" runat="server">
    <orion:Include runat="server" File="Admin/js/WebIntegration.js" />

    <div id="sw-settings-cp-notenabled">
        <h1><%= DefaultSanitizer.SanitizeHtml(Header) %></h1>       
        <table class="sw-webintegration-table">
            <tr>
                <td class="sw-webintegration-icon">
                     <img src="../Orion/images/SettingPageIcons/SW_logo_icon32x32.png">
                </td>
                <td><span><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_ZS0_078) %></span>                                      
                </td>
                <td class="sw-webintegration-button">
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZS0_063 %>" DisplayType="Primary" runat="server" NavigateUrl="javascript:SW.Core.Web.Integration.LogInCustomerPortal();" CssClass="sw-logincustomerportal-loginbuttons"/>
                </td>
            </tr>      
        </table>
    </div>

    <div id="sw-settings-cp-enabled">       
        <table class="sw-webintegration-table">
            <tr>
                <td><h1><%= DefaultSanitizer.SanitizeHtml(Header) %></h1></td>
                <td class="sw-webintegration-login-section">
                     <span><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_ZS0_064) %></span><span id="sw-settings-cp-username"> </span> <a class="sw-link" onclick="javascript:SW.Core.Web.Integration.LogOutFromCustomerPortal();"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_ZS0_065) %></a>
                </td>                
            </tr>      
            </table>
    </div>
</asp:Panel>
<script>
    SW.Core.Web.Integration.RenderCustomerPortalWidget();
</script>