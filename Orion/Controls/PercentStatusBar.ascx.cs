﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Orion_Controls_PercentStatusBar : System.Web.UI.UserControl
{
    private const int DefaultWidth = 100;
    private const int DefaultHeight = 10;

    #region Properties

    private bool reverse;
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Reverse
    {
        get { return reverse; }
        set { reverse = value; }
    }

    private int status;
    [PersistenceMode(PersistenceMode.Attribute)]
    public int Status
    {
        get { return status; }
        set { status = RangeCheck(value); }
    }

    private int warningLevel;
    [PersistenceMode(PersistenceMode.Attribute)]
    public int WarningLevel
    {
        get { return warningLevel; }
        set { warningLevel = RangeCheck(value); }
    }

    private int errorLevel;
    [PersistenceMode(PersistenceMode.Attribute)]
    public int ErrorLevel
    {
        get { return errorLevel; }
        set { errorLevel = RangeCheck(value); }
    }

    // show = true (visible normaly), show = false (&nbsp; instead of control)
    private bool show = true;
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Show
    {
        get { return show; }
        set { show = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public Unit Width { get; set; }
    
    [PersistenceMode(PersistenceMode.Attribute)]
    public Unit Height { get; set; }
    
    #endregion

    public int RangeCheck(int value)
    {
        int result = value;
        if (result < 0) result = 0;
        if (result > 100) result = 100;
        return result;
    }

    private Unit Subst(Unit u, int px)
    {
        return Unit.Pixel((int)(u.Value) - px);
    }

    protected override void OnInit(EventArgs e)
    {
        if ((this.Width == Unit.Empty) || (this.Width.Value < 10)) this.Width = Unit.Pixel(DefaultWidth);
        if ((this.Height == Unit.Empty) || (this.Height.Value < 4)) this.Height = Unit.Pixel(DefaultHeight);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Reverse)
        {
            if (Status < ErrorLevel)
                this.StatusPanel.CssClass = "RedBar";
            else if (Status < WarningLevel)
                this.StatusPanel.CssClass = "YellowBar";
            else
                this.StatusPanel.CssClass = "GreenBar";
        }
        else
        {
            if (Status > ErrorLevel)
                this.StatusPanel.CssClass = "RedBar";
            else if (Status > WarningLevel)
                this.StatusPanel.CssClass = "YellowBar";
            else
                this.StatusPanel.CssClass = "GreenBar";
        }
        this.OuterBorder.Width = Width;
        this.StatusPanel.Width = Unit.Pixel( Convert.ToInt32(Math.Round((double)Status * (Width.Value / 100))) );

        this.OuterBorder.Height = Height;

        if (Status == 0) StatusPanel.BackColor=Color.White;
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.Show)
        {
            this.OuterBorder.Visible = false;
            this.Controls.Add(new LiteralControl("&nbsp;"));
        }
        base.OnPreRender(e);
    }
}
