<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlotColorControl.ascx.cs" Inherits="PlotColorControl" %>

<!--table-->
    <tr>
        <td align="justify" style="height: 40px; width: 10%; text-align: left;">
            <b><asp:Label ID="Name" runat="server"></asp:Label></b>
        </td>
        <td align="justify"  style="height: 40px; width: 10%">
            <select ID="PlotColorList" runat="server"  style = "width: 75px; left:0px">
		        <option value="FF0000" style="background-color: #0000FF; color: #FFFFFF;" 
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_195 %>" />    
		        <option value="FF" style="background-color: #FF0000" 
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_196 %>" />
		        <option value="BF00" style="background-color: #00FF00"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_197 %>" />
		        <option value="FF00FF" style="background-color: #FF00FF;"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_198 %>" />
		        <option value="007FFF" style="background-color: #FF7F00;"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_199 %>" />
		        <option value="FFFF00" style="background-color: #00FFFF;"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_TM0_85 %>" />
		        <option value="FFFF" style="background-color: #FFFF00;"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_200 %>" />
		        <option value="7F7F7F" style="background-color: #7F7F7F;"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_201 %>" />
		        <option value=""
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_202 %>" />
	        </select>
	        <br />
	        <asp:TextBox runat="server" ID="CustomPlotColor" style ="width:69px; left:0px;"></asp:TextBox>  
	    </td>    
	    <td align="justify"  style="height: 40px; width: 50%">
	        <asp:Label ID="Description" runat="server"></asp:Label>
		</td>
    </tr>
<!--/table-->