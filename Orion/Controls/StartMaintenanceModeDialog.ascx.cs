﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using Resources;

public partial class Orion_Controls_StartMaintenanceModeDialog : UserControl
{

    protected string AccountId
    {
        get { return Profile.UserName; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var currentCulture = Thread.CurrentThread.CurrentCulture;
        MaintenanceStart.Text = CoreWebContent.WEBDATA_JP1_5.ToUpper(currentCulture);
        MaintenanceCancel.Text = CoreWebContent.WEBDATA_JP1_6.ToUpper(currentCulture);

        StartMaintenanceDialog.Attributes.Add("data-account-id", AccountId.ToString(CultureInfo.InvariantCulture));
    }

}
