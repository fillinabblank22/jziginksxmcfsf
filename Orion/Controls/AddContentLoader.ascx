<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddContentLoader.ascx.cs"
    Inherits="Orion_Controls_AddContentLoader" %>
<%@ Register TagPrefix="orion" TagName="ResourcePicker" Src="~/Orion/Controls/ResourcePicker/ResourcePicker.ascx" %>
<%@ Register TagPrefix="orion" TagName="DataSourcePicker" Src="~/Orion/Controls/DataSourcePicker.ascx" %>
<orion:Include File="ResourcePicker.css" runat="server" />
<orion:Include File="AddContentLoader.css" runat="server" />
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="AddContentLoader.js" />
<div id="reportContentDialog" class="resourcePickerLoader offPage">
    <div id="reportContentDialogAccordion">
        <h3 id="add-content-select-resource"><span id="add-content-select-resourceText"></span><span id="add-content-select-resourceName"></span><span id="add-content-select-resourceDescription"></span></h3>
        <div>
            <div id="resourcePickerLoader-loading-ph">
                <div class="content">
                    <img src="/Orion/images/loading_gen_small.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcePickerDialog_LoadingAltText) %>" />
                    <div class="mainText">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcePickerDialog_CacheBuilding) %></div>
                    <div class="subText">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcePickerDialog_CacheBuilding_subtext) %></div>
                </div>
            </div>
            <div id="resourcePickerLoader-content-ph" class="notVisible">
                <orion:ResourcePicker ID="myResPicker" runat="server" OnCreatedJs="onContentPickerCreated" />
            </div>
        </div>
        <h3 id="add-content-select-datasource" style="margin-top: 10px;"><span id="add-content-select-datasourceName"></span></h3>
        <div style="width: auto; min-width: 900px; max-width: 1000px;">
            <orion:DataSourcePicker runat="server" ID="DataSourcePicker" />
        </div>
    </div>
    
    <div class="bottom">
        <div class="sw-btn-bar-wizard" id="report-content-dialog-btn-ph">
        </div>
    </div>
</div>
<script type="text/javascript">

    function onContentPickerCreated(picker) {
        var contentLoader = new SW.Core.AddContentLoader({
                resourcePicker: picker,
                onSelected: <%= OnSelectedJs %> ,
                onCreated: <%= OnCreatedJs %> ,
                resourceHeader: $('#add-content-select-resource'),
                dataSourceHeader: $('#add-content-select-datasource'),
                dialogElement: $('#reportContentDialog'),
                accordionElement: $('#reportContentDialogAccordion')
            });
    }
</script>
