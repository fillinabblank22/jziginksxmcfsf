<%@ Control Language="C#" ClassName="StartMaintenanceDialog" CodeFile="StartMaintenanceModeDialog.ascx.cs" Inherits="Orion_Controls_StartMaintenanceModeDialog" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Controls/DateTimeRangePicker.ascx" TagPrefix="orion" TagName="DateTimeRangePicker" %>

<asp:ScriptManagerProxy runat="server">
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="js/MaintenanceMode/PlanServiceFactory.js" />
<orion:Include runat="server" File="js/MaintenanceMode/CreateMaintenanceModeDialog.js" />
<orion:Include runat="server" File="StartMaintenanceDialog.css" />

<div id="StartMaintenanceDialog" runat="server" style="display: none;" class="disposable, sw-startMaintenanceDialog">
    <div class="sw-startMaintenanceDialog-intro"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JP1_2) %></div>
    
    <div class="blueBox">
        <label><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JP1_3) %></label><br/>
        <asp:TextBox runat="server" ID="reason" CssClass="sw-startMaintenanceDialog-reason" />
    </div>
    
    <orion:DateTimeRangePicker runat="server" ID="startEndRange" />
    
    <div class="sw-suggestion sw-startMaintenanceDialog-indefiniteModeWarning"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JP1_4) %></div>
    
    <div class="bottom">
        <div class="sw-btn-bar-wizard">
            <img src="/Orion/images/AJAX-Loader.gif" class="sw-startMaintenanceDialog-ajaxLoader" style="display: none" alt="<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JP1_9) %>" />
            <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" DisplayType="Primary" ID="MaintenanceStart" />
            <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" DisplayType="Secondary" ID="MaintenanceCancel" />
        </div>
    </div>
</div>
