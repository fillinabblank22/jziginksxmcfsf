﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using System.Globalization;

using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.UI;

public partial class ProductBlogControl : System.Web.UI.UserControl
{
	public delegate void IgnoreProductBlogItemEventHender();
	public event IgnoreProductBlogItemEventHender IgnoreProductBlogItem;

	private static Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	public bool StoreItems
	{
		get { return (SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("ProductsBlog-EnableContent").SettingValue == 1) ? true : false; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			string showCount = WebUserSettingsDAL.Get("ProductBlogFilter_Count");
			showPostsCount.Text = (!string.IsNullOrEmpty(showCount)) ? showCount : "20";

			string showIgnored = WebUserSettingsDAL.Get("ProductBlogFilter_ShowIgnored");
			if (!string.IsNullOrEmpty(showIgnored))
			{
				cbShowIgnored.Checked = (showIgnored.Equals("true", StringComparison.OrdinalIgnoreCase)) ? true : false;
			}

		    OrionSetting prodBlogCount = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("ProductsBlog-StoredPostsCount");
            if (prodBlogCount != null)
            {
                storePostsCount.Text = prodBlogCount.SettingValue.ToString();
            }

		    OrionSetting prodBlogEnableContent = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("ProductsBlog-EnableContent");
		    if(prodBlogEnableContent != null)
		    {
		        cbEnableCont.Checked = (prodBlogEnableContent.SettingValue == 1) ? true : false;
		    }

			if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
			{
				storePostsCount.Enabled = false;
				cbEnableCont.Enabled = false;
			}
		}
	}

	public void LoadControls()
	{
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			var blogs = proxy.GetBlogNotificationItems(int.Parse(showPostsCount.Text), cbShowIgnored.Checked);

			ProductBlogTable.DataSource = blogs;
			ProductBlogTable.DataBind();

			NoResultsMessage.Style["display"] = blogs.Count == 0 ? "block" : "none";
		}
	}

	protected void Refresh(object sender, EventArgs e)
	{
		LoadControls();
	}

	protected void SaveSettings(object sender, EventArgs e)
	{
		if (cbEnableCont.Checked)
			SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("ProductsBlog-EnableContent", 1);
		else
			SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("ProductsBlog-EnableContent", 0);

		WebUserSettingsDAL.Set("ProductBlogFilter_Count", showPostsCount.Text);
		WebUserSettingsDAL.Set("ProductBlogFilter_ShowIgnored", cbShowIgnored.Checked.ToString());

		SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("ProductsBlog-StoredPostsCount", double.Parse(storePostsCount.Text));

		LoadControls();
	}

	protected void CancelClick(object sender, EventArgs e)
	{
		storePostsCount.Text = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("ProductsBlog-StoredPostsCount").SettingValue.ToString();
		cbEnableCont.Checked = (SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("ProductsBlog-EnableContent").SettingValue == 1) ? true : false;
	}

	protected void AcknowledgeBlogItem(object sender, EventArgs e)
	{
		LocalizableButton imgButton = (LocalizableButton)sender;
		if (imgButton != null)
		{
			Control parent = imgButton.Parent;
			HiddenField hf = (HiddenField)parent.FindControl("hPostGuid");
			if (hf != null)
			{
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				{
					proxy.IgnoreNotificationItem(new Guid(hf.Value));
				}

				LoadControls();

				if (IgnoreProductBlogItem != null)
					IgnoreProductBlogItem();
			}
		}
	}

	protected void CheckNow(object sender, EventArgs e)
	{
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			proxy.ForceBlogUpdatesCheck();
		}

		LoadControls();
		if (IgnoreProductBlogItem != null)
			IgnoreProductBlogItem();
	}
}
