using System;
using System.Web;
using SolarWinds.Orion.Web;

public partial class ChartFormatControl : System.Web.UI.UserControl
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}

	private void SetupControlFromRequest()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);

		if (!string.IsNullOrEmpty(resource.Properties["PlotStyle"]))
		{
			ChartType = resource.Properties["PlotStyle"];
		}
		else
		{
			ChartType = "Line";
		}
	}

	protected override void OnInit(EventArgs e)
	{
		if (!Page.IsPostBack)
		{
			SetupControlFromRequest();
		}

        if (HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            ChartFormat.Attributes.Add("onchange", "javascript:SaveData('PlotStyle', this.value);");
        }
		base.OnInit(e);
	}

	public string ChartType
	{
		get { return ChartFormat.Items[ChartFormat.SelectedIndex].Value; }
		set { ChartFormat.SelectedValue = value; }
	}
}
