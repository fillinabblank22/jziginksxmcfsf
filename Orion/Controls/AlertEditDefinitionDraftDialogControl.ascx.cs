﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Controls_AlertEditDefinitionDraftDialogControl : System.Web.UI.UserControl
{
    protected string SafeUniqueID
    {
        get
        {
            return GetSafeUniqueID();
        }
    }

    private string GetSafeUniqueID()
    {
        string strRes = string.Empty;
        if (!string.IsNullOrEmpty(UniqueID))
        {
            strRes = UniqueID.Replace("$", "-");
        }

        return strRes;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}