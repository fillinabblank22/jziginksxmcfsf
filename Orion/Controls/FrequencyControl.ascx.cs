﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Interfaces;
using SolarWinds.Orion.Core.Models.Schedules;

public partial class Orion_Controls_FrequencyControl : UserControl
{
    public Guid WizardGuid { get; set; }
    public bool SingleScheduleMode { get; set; }
    public bool UseTimePeriodMode { get; set; }
    private IEnumerable<ISchedule> schedules;
    public IEnumerable<ISchedule> Schedules
    {
        get { return schedules ?? Enumerable.Empty<ISchedule>(); }
        set { schedules = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (UseTimePeriodMode)
            {
                schedules = jsonActionsContainer.GetObject<TimePeriodSchedule[]>();
            }
            else
            {
                schedules = jsonActionsContainer.GetObject<ReportSchedule[]>();
            }
        }
    }
}
