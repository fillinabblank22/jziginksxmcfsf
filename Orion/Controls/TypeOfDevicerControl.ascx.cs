using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Federation;

public partial class TypeOfDevicerControl : UserControl
{
	private static readonly Log log = new Log();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public string DeviceType
	{
		get { return netObjectTypes.SelectedValue; }
		set { netObjectTypes.SelectedValue = value; }
	}

    public List<SwisErrorMessage> SwisErrors { get; private set; }

    protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);
		string selectedvalue = string.Empty;
		if (!string.IsNullOrEmpty(DeviceType))
		{
			selectedvalue = DeviceType;
		}

		netObjectTypes.Items.Clear();
		netObjectTypes.Items.Add(new ListItem(CoreWebContent.WEBCODE_VB0_182, string.Empty));
        this.SwisErrors = new List<SwisErrorMessage>();
        Dictionary<string, string> types = NetworkDeviceDAL.GetNetworkDeviceTypes(this.SwisErrors);
	    foreach (string key in types.Keys)
        {
            netObjectTypes.Items.Add(new ListItem(types[key], key));
        }
		DeviceType = selectedvalue;
	}
}
