<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GoogleTagManager.ascx.cs" Inherits="Orion_Controls_GoogleTagManager"%>

<% if (GtmEnabled && OipInstalled && !CollectingDisabled) { %>
<!-- Google Tag Manager -->
<script type="text/javascript">
    var dataLayer = JSON.parse("<%= modules %>");
    dataLayer.push({'oipUserId':'<%= oipUserID %>','bootstrapperId':'<%= bootstrapperID %>','isInternal':'<%= !IsExternal %>', 'UserAccountHash':'<%= userHash %>', 'GoogleAnalyticsAccountKey': '<%= gaAccountKey %>', 'AdminStatus': '<%= IsAdmin %>'});
    var licenseTypes = JSON.parse("<%= licenseTypes %>");
    dataLayer = dataLayer.concat(licenseTypes);
    var moduleElementCounts = JSON.parse("<%= moduleElementCounts %>");
    dataLayer = dataLayer.concat(moduleElementCounts);
    var moduleInstallDates = JSON.parse("<%= moduleInstallDates %>");
    dataLayer = dataLayer.concat(moduleInstallDates);
</script>
<script type="text/javascript">
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MW4D6T8');
</script>
<!-- End Google Tag Manager -->

<% } %>