using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MaxNumberControl : System.Web.UI.UserControl
{
	protected override void OnInit(EventArgs e)
	{
		if (!Page.IsPostBack)
		{
			SetupControlFromRequest();
		}

		base.OnInit(e);
	}

	private void SetupControlFromRequest()
	{
		int error;

		if (Request["MaxMessages"] != null)
		{
			MaxNumberValue = Request["MaxMessages"].ToString();
		}

		if (!int.TryParse(MaxNumberValue, out error))
		{
			MaxNumberValue = "250";
		}
	}

	public string TitleText
	{
		get
		{
			return Title.Text;
		}
		set
		{
			Title.Text = value;
		}
	}

	public string MaxNumberValue
	{
		get
		{
			return MaxNumber.Text;
		}
		set
		{
			MaxNumber.Text = value;
		}
	}
}
