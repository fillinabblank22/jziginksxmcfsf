﻿﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataSourcePicker.ascx.cs" Inherits="Orion_Controls_DataSourcePicker" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@Register tagPrefix="orion" tagName="NetObjectPicker" src="NetObjectPicker.ascx" %>
<%@ Register Src="~/Orion/Controls/DynamicDataSourcePicker.ascx" TagPrefix="orion" TagName="DynamicDataSourcePicker" %>

<orion:Include runat="server" File="DataSourcePicker.css"/>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />

<orion:Include runat="server" File="DataSourcePicker.js" />

<script type="text/javascript">
//<![CDATA[
    var swisDataSourceOnly = <%= SwisDataSourceOnly.ToString(CultureInfo.InvariantCulture).ToLower() %>;
    $(function () {
        $('#dataSourcePickerBox').dataSources(undefined, swisDataSourceOnly);
    });
//]]>
</script>

<div id="dataSourcePickerBox" style="background-color: white;">
    <div id="selectDataSrcHeader" style="margin: 10px 0px;">
        <div>
            <input id="createdDataSources" type="radio" name="selectObjects" value="1" checked="checked" />
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_42) %>
        </div>
        <div>
            <select id="ddDataSources" style="margin: 0px 0px 10px 15px; width: 260px;">
                <option value="NewSelection"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_43) %></option>
            </select>
        </div>
        <div>
            <input type="radio" name="selectObjects" value="2" />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_44) %>
        </div>
    </div>

    <div id="newSelection">
        <div class="swis-error-wrapper"></div>
		<div id="helpfullTextForSpecificSelection"></div>
        <p id="selectionMethodCtrl">
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_50) %></b><br />
            <select id="dataSourceType" style="margin: 10px 0px 10px 0px;">
                <option value="Entities" selected="selected"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_51) %></option>
                <option value="Dynamic"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_45) %></option>
                <option value="Custom"><%= DefaultSanitizer.SanitizeHtml(SwisDataSourceOnly ? Resources.CoreWebContent.Reports_CustomDataSourcePicker_WebOption_SWQL : Resources.CoreWebContent.WEBCODE_SO0_46) %></option>
            </select>
            <span id="selectionMethodDescription" style="color:#5F5F5F;font-size:13px;">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_1) %></span>
               </p>
       
        <%-- ENTITIES --%>
        <div id="entitiesModeEditor" class="data-source-editor">
            <orion:NetObjectPicker runat="server"></orion:NetObjectPicker>
            <div id="singleSelectWarning" class="sw-suggestion" style="display:none;"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_281) %></div>      
        </div>
        
        <%-- DYNAMIC --%>
        <div id="dynamicModeEditor"  class="data-source-editor">
            <orion:DynamicDataSourcePicker runat="server" ID="dynamicDataSourcePicker" />    
        </div>
                
        <%-- CUSTOM --%>
        <div id="customModeEditor" style="display: none;" class="data-source-editor">
            <%if (OrionConfiguration.IsDemoServer){ %><div id="demoModeWarning"></div><% } %>
            <table>
                <tr>
                    <td colspan="2" id="container-query-type-header">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_48) %></b>
                    </td>
                </tr>
                <tr>
                    <td style="width: 80px;" id="container-query-type-swis">
                        <input type="radio" name="queryType" value="2" checked="checked" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_282) %>
                    </td>
                    <td id="container-query-type-sql">
                        <input type="radio" name="queryType" value="3" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_283) %>
                    </td>
                    <td style="text-align: right">
                        <%= DefaultSanitizer.SanitizeHtml(String.Format("<a class=\"coloredLink\" target=\"_blank\" href='" + HelpHelper.GetHelpUrl("OrionCorePHUsingSWQL") + "'>{0}</a>", SwisDataSourceOnly ? Resources.CoreWebContent.Reports_CustomDataSourcePicker_WebHelpLink_SWQL : Resources.CoreWebContent.WEBCODE_SO0_49)) %>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <textarea id="customQueryText" rows="12" style="border:1px solid #ABADB3; width:763px;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div id="previewDataSourcesBtn" style="display: none;" ></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div id="previewDataSources" ></div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="errorContainer" class="sw-suggestion sw-suggestion-fail" style="display:none"><span class="sw-suggestion-icon"></span><span id="errorMessage">&nbsp;</span></div>
        <div class="blueBox" style="margin-top:25px;">
            <table>
                <tr>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_47) %>
                    </td>
                    <td>
                        <input type="text" style="width: 300px;" id="dataSourceName" />
                    </td>
                    <td>
                        <div id="selectionNameErrorMessage" style="font-size: 11px; color: red">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<input type="hidden" runat="server" id="dataSourcesSessionIdentifier" />
<input type="hidden" id="allowAdmin" value="<%= Profile.AllowAdmin %>"/>
<input type="hidden" id="netObjectId" value="<%= DefaultSanitizer.SanitizeHtml(this.GetNetObjectId()) %>"/>
