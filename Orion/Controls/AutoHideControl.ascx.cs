using System;
using System.Web.UI;
using SolarWinds.Orion.Web;

public partial class AutoHideControl : System.Web.UI.UserControl
{
	ResourceInfo _resource;
	private string _description;

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool InLine { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected override void OnInit(EventArgs e)
	{
		_resource = new ResourceInfo();
		_resource.ID = Convert.ToInt32(this.Request["ResourceID"]);
		if (_resource.Properties["AutoHide"] != "0" && !string.IsNullOrEmpty(_resource.Properties["AutoHide"]))
			autoHideYes.Checked = true;
		else
			autoHideNo.Checked = true;

        //for custom object resource 
        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            autoHideYes.Attributes.Add("onclick", "javascript:SaveData('AutoHide', this.checked ? '1' : '0');");
            autoHideNo.Attributes.Add("onclick", "javascript:SaveData('AutoHide', this.checked ? '0' : '1');");
        }

		base.OnInit(e);
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public string Description
	{
		get {
			if (string.IsNullOrEmpty(_description))
				return Resources.CoreWebContent.WEBCODE_AK0_98;
			else return _description;
		}
		set {
			_description = value;
		}
	}

	public string AutoHideValue
	{
		get 
		{
			if (autoHideYes.Checked)
				return "1";
			else
				return "0";
		}
        set
        {
            autoHideYes.Checked = value.Equals("1") ? true : false;
            autoHideNo.Checked = !autoHideYes.Checked;
        }
	}

}
