<%@ Control Language="C#" ClassName="EditResourceTitle" %>

<script runat="server">
    private string ResourceNotFound = Resources.CoreWebContent.WEBCODE_VB0_101;

    public string ResourceTitleID
    {
        get
        {
            return txtTitle.ClientID;
        }
    }
    public string ResourceTitle
    {
        get
        {
            return SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(txtTitle.Text);
        }
        set
        {
            txtTitle.Text = value;
        }
    }

    public string ResourceSubTitle
    {
        get
        {
            return SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(txtSubTitle.Text);
        }
        set
        {
            txtSubTitle.Text = value;
        }
    }

    public bool ShowTitle
    {
        get
        {
            return this.paraTitle.Visible;
        }
        set
        {
            this.paraTitle.Visible = value;
        }
    }

    public bool ShowSubTitle
    {
        get
        {
            return this.paraSubtitle.Visible;
        }
        set
        {
            this.paraSubtitle.Visible = value;
        }
    }

    public bool ShowSubTitleHintMessage
    {
        get
        {
            return this.Message.Visible;
        }
        set
        {
            this.Message.Visible = value;
        }
    }

    public string SubTitleHintMessage
    {
        get
        {
            return this.Message.Text;
        }
        set
        {
            this.Message.Text = value;
        }
    }
    
    
</script>

<p runat="server" id="paraTitle" style="padding-bottom: 0px;">
    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_102) %></b><br />
    <asp:TextBox runat="server" ID="txtTitle" Rows="1" Columns="30" />
</p>
<p runat="server" id="paraSubtitle" style="padding-bottom: 13px; margin-bottom: 0px;">
    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_163) %></b><br />
    <asp:TextBox runat="server" ID="txtSubTitle" Rows="1" Columns="30" />
    <br />
    <asp:Label ID="Message" runat="server" Visible="false"></asp:Label>
</p>
