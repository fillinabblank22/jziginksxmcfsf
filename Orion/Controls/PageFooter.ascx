<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageFooter.ascx.cs" EnableViewState="false" Inherits="Orion_Controls_PageFooter" %>
<%-- 

orionmaster.js & livequery.js added dynamically

--%><div id="footermark"><!-- --></div>
<%if (CommonWebHelper.IsMobileView(Request)) { %>
<div id="mobileFooter">
    <a class="GoToFull" href='<%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.FullSiteHref(Request)) %>'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_11) %>  &raquo;</a>
</div>
<script type="text/javascript">
    $(function () {
        $("a[href!='#'][class!='GoToFull']").livequery(function () {
            if (this.href && !this.href.toLowerCase().match("ismobileview") && !this.href.toLowerCase().match("javascript:"))
                this.href += (this.href.indexOf("?") > -1) ? "&IsMobileView=True" : "?IsMobileView=True";
        });
    });
</script>
<% } else { %>
<div id="footer">
    <a href="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.SolarWindsComLocUrl) %>"><img alt="SolarWinds" height="19" width="77" src="/orion/images/SolarWinds.Logo.Footer.svg" /></a><%= DefaultSanitizer.SanitizeHtml(FooterString) %></div>
    <script type="text/javascript">
        $(function () {
            if (typeof (eopdf) == "object") {
                $("#footer").css("bottom", "16px");
            }
        });
    </script>
</div>
<% } %>
