﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web.DAL;

public partial class ThresholdControl : System.Web.UI.UserControl
{

    public bool OverrideMultipleObjects
    {
        get { if (ViewState["OverrideMultipleObjects" + this.ClientID] == null) return false; else return (bool)ViewState["OverrideMultipleObjects" + this.ClientID]; }
        private set { ViewState["OverrideMultipleObjects" + this.ClientID] = value; }
    }

    public bool SuppressGeneralThresholds
    {
        get { if (ViewState["SuppressGeneralThresholds" + this.ClientID] == null) return false; else return (bool)ViewState["SuppressGeneralThresholds" + this.ClientID]; }
        set { ViewState["SuppressGeneralThresholds" + this.ClientID] = value; }
    }

    public bool IsValid
    {
        get { if (ViewState["IsValid" + this.ClientID] == null) return true; else return (bool)ViewState["IsValid" + this.ClientID]; }
        private set { ViewState["IsValid" + this.ClientID] = value; }
    }

    public string InvalidInstances
    {
        get { if (ViewState["InvalidInstances" + this.ClientID] == null) return ""; else return ViewState["InvalidInstances" + this.ClientID].ToString(); }
        set { ViewState["InvalidInstances" + this.ClientID] = value; }
    }

    public bool IsMultiedit
    {
        get { if (ViewState["IsMultiedit" + this.ClientID] == null) return false; else return (bool)ViewState["IsMultiedit" + this.ClientID]; }
        set { ViewState["IsMultiedit" + this.ClientID] = value; }
    }

    public string DisplayName
    {
        get { if (ViewState["DisplayName" + this.ClientID] == null) return ""; else return ViewState["DisplayName" + this.ClientID].ToString(); }
        set { ViewState["DisplayName" + this.ClientID] = value; }
    }

    public string ThresholdName
    {
        get { if (ViewState["ThresholdName" + this.ClientID] == null) return ""; else return ViewState["ThresholdName" + this.ClientID].ToString(); }
        set { ViewState["ThresholdName" + this.ClientID] = value; }
    }

    public string Unit
    {
        get { if (ViewState["Unit" + this.ClientID] == null) return ""; else return ViewState["Unit" + this.ClientID].ToString(); }
        set { ViewState["Unit" + this.ClientID] = value; }
    }

    public string WarningValue
    {
        get { if (ViewState["WarningValue" + this.ClientID] == null) return ""; else return ViewState["WarningValue" + this.ClientID].ToString(); }
        set { ViewState["WarningValue" + this.ClientID] = value; }
    }

    public string CriticalValue
    {
        get { if (ViewState["CriticalValue" + this.ClientID] == null) return ""; else return ViewState["CriticalValue" + this.ClientID].ToString(); }
        set { ViewState["CriticalValue" + this.ClientID] = value; }
    }

    public string GlobalWarningValue
    {
        get { if (ViewState["GlobalWarningValue" + this.ClientID] == null) return ""; else return ViewState["GlobalWarningValue" + this.ClientID].ToString(); }
        set { ViewState["GlobalWarningValue" + this.ClientID] = value; }
    }

    public string GlobalCriticalValue
    {
        get { if (ViewState["GlobalCriticalValue" + this.ClientID] == null) return ""; else return ViewState["GlobalCriticalValue" + this.ClientID].ToString(); }
        set { ViewState["GlobalCriticalValue" + this.ClientID] = value; }
    }

    public ThresholdOperatorEnum GlobalThresholdOperator
    {
        get { if (ViewState["DefaultThresholdOperator" + this.ClientID] == null) return ThresholdOperatorEnum.Greater; else return (ThresholdOperatorEnum)ViewState["DefaultThresholdOperator" + this.ClientID]; }
        set { ViewState["DefaultThresholdOperator" + this.ClientID] = value; }
    }

    public ThresholdType ThisThresholdType
    {
        get { if (ViewState["ThisThresholdType" + this.ClientID] == null) return ThresholdType.Global; else return (ThresholdType)ViewState["ThisThresholdType" + this.ClientID]; }
        set { ViewState["ThisThresholdType" + this.ClientID] = value; }
    }

    public ThresholdOperatorEnum ThisThresholdOperator
    {
        get { if (ViewState["ThisThresholdOperator" + this.ClientID] == null) return ThresholdOperatorEnum.Greater; else return (ThresholdOperatorEnum)ViewState["ThisThresholdOperator" + this.ClientID]; }
        set { ViewState["ThisThresholdOperator" + this.ClientID] = value; }
    }

    public string ObjectIDs
    {
        get { if (ViewState["ObjectIDs" + this.ClientID] == null) return ""; else return ViewState["ObjectIDs" + this.ClientID].ToString(); }
        set { ViewState["ObjectIDs" + this.ClientID] = value; }
    }

    public string IsValidFieldClientId
    {
        get { return hfIsValid.ClientID; }
    }

    public bool EnableCapacityPlanning
    {
        get { if (ViewState["EnableCapacityPlanning" + this.ClientID] == null) return false; else return (bool)ViewState["EnableCapacityPlanning" + this.ClientID]; }
        set { ViewState["EnableCapacityPlanning" + this.ClientID] = value; }
    }

    public PlanningType CapacityPlanningType
    {
        get { if (ViewState["CapacityPlanningType" + this.ClientID] == null) return PlanningType.None; else return (PlanningType)ViewState["CapacityPlanningType" + this.ClientID]; }
        set { ViewState["CapacityPlanningType" + this.ClientID] = value; }
    }

    /// <summary>
    /// JS function reference (name) to format in a custom user friendly way values in the statistics table.
    /// Example available in \web\orion\js\baselinedetails.js
    /// </summary>
    public string StatisticsFormatterFunction
    {
        get { if (ViewState["StatisticsFormatterFunction" + this.ClientID] == null) return ""; return ViewState["StatisticsFormatterFunction" + this.ClientID].ToString(); }
        set { ViewState["StatisticsFormatterFunction" + this.ClientID] = value; }
    }

    protected bool IsGlobal { get { return ThisThresholdType == ThresholdType.Global; } }

    private void FillDdlOperator(DropDownList ddlOperator)
    {
        ddlOperator.DataValueField = "Value";
        ddlOperator.DataTextField = "Text";
        ddlOperator.DataSource =
            Enum.GetValues(typeof(ThresholdOperatorEnum))
                .Cast<ThresholdOperatorEnum>()
                .Select(x => new { Value = string.Format("{0}|{1}", (int)x, x.ToString()), Text = x.LocalizedLabel() });
        ddlOperator.DataBind();
    }

    private void InitializeState()
    {
        FillDdlOperator(ddlOperator);
        lblOperatorText.Text = ddlOperator.SelectedItem.Text;

        lblGeneralWarningValue.Text = string.Format("{0} {1} {2}", GlobalThresholdOperator.LocalizedLabel().ToLower(), GlobalWarningValue, Unit);
        lblGeneralCriticalValue.Text = string.Format("{0} {1} {2}", GlobalThresholdOperator.LocalizedLabel().ToLower(), GlobalCriticalValue, Unit);

        if (!EnableCapacityPlanning)
        {
            capacityPlanning.Visible = false;
            capacityPlanningGlobal.Visible = false;
        }

        if (SuppressGeneralThresholds)
            chbOverrideGeneral.Style.Add("display", "none");

        if (IsMultiedit)
        {
            chbOverrideMultipleObjects.Visible = true;
            chbOverrideMultipleObjects.Checked = OverrideMultipleObjects;

            sectionOverrideGeneral.Style.Add("display", "none");
            sectionThresholdDefinition.Style.Add("display", "none");
            sectionDetailHyperlink.Style.Add("display", "none");

            if (SuppressGeneralThresholds)
            {
                sectionGeneralThresholds.Style.Add("display", "none");
                sectionCustomThresholds.Style.Add("display", "block");

                tbWarningValue.Text = WarningValue;
                tbCriticalValue.Text = CriticalValue;

            }
            else
            {
                tbWarningValue.Text = GlobalWarningValue;
                tbCriticalValue.Text = GlobalCriticalValue;

                if (EnableCapacityPlanning)
                {
                    capacityPlanningGlobal.Visible = true;
                }
            }

            // capacity planning block for multiedit
            if (EnableCapacityPlanning)
            {
                capacityPlanning.Visible = true;
                capacityPlanningGlobal.Visible = true;

                if (CapacityPlanningType == PlanningType.Peak)
                {
                    capPlanTypePeak.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_10;
                }
                else
                {
                    capPlanTypeAverage.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_09;
                }
            }
            else
            {
                capacityPlanning.Visible = false;
                capacityPlanningGlobal.Visible = false;
            }
        }
        else
        {
            chbOverrideMultipleObjects.Visible = false;
            sectionOverrideGeneral.Style.Add("display", "block");
            sectionDetailHyperlink.Style.Add("display", "block");

            if (ThisThresholdType == ThresholdType.Global && !SuppressGeneralThresholds)
            {
                sectionGeneralThresholds.Style.Add("display", "block");
                sectionCustomThresholds.Style.Add("display", "none");
                tbWarningValue.Text = GlobalWarningValue;
                tbCriticalValue.Text = GlobalCriticalValue;
            }
            else
            {
                sectionGeneralThresholds.Style.Add("display", "none");
                sectionCustomThresholds.Style.Add("display", "block");

                chbOverrideGeneral.Checked = true;

                ddlOperator.ClearSelection();
                ddlOperator.Items.FindByValue(string.Format("{0}|{1}", (int)ThisThresholdOperator, ThisThresholdOperator.ToString())).Selected = true;

                lblOperatorText.Text = ddlOperator.SelectedItem.Text;

                tbWarningValue.Text = WarningValue;
                tbCriticalValue.Text = CriticalValue;

            }
            
            // capacity planning block for single edit
            if (EnableCapacityPlanning)
            {
                capacityPlanning.Visible = true;
                capacityPlanningGlobal.Visible = true;

                if (CapacityPlanningType == PlanningType.Peak)
                {
                    capPlanTypePeak.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_10;
                }
                else
                {
                    capPlanTypeAverage.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_09;
                }
            }
            else
            {
                capacityPlanning.Visible = false;
                capacityPlanningGlobal.Visible = false;
            }
        }
    }

    private void GetCurrentState()
    {
        InvalidInstances = hfInvalidInstances.Value;
        IsValid = hfIsValid.Value == "1";

        if (IsMultiedit)
        {
            OverrideMultipleObjects = chbOverrideMultipleObjects.Checked;
        }

        if (!chbOverrideGeneral.Checked)
        {
            ThisThresholdType = ThresholdType.Global;
        }
        else
        {
            ThisThresholdType = ThresholdType.Dynamic;
            WarningValue = tbWarningValue.Text;
            CriticalValue = tbCriticalValue.Text;

            ThisThresholdOperator = (ThresholdOperatorEnum)Enum.Parse(typeof(ThresholdOperatorEnum), ddlOperator.SelectedValue.Split('|')[1], true);
            
            if (EnableCapacityPlanning)
            {
                if (capPlanTypePeak.Checked)
                {
                    CapacityPlanningType = PlanningType.Peak;
                }
                else
                {
                    CapacityPlanningType = PlanningType.Average;
                }
            }
            
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            GetCurrentState();
        }

        InitializeState();
    }
}