<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChartFormatControl.ascx.cs" Inherits="ChartFormatControl" %>

<!--table-->
    <tr>
        <td align="justify" style="height: 30px; width: 10%; white-space: nowrap;">
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_203) %></b>
        </td>
        <td align="justify"  style="height: 30px; width: 10%">
            <asp:DropDownList ID="ChartFormat" runat="server" >
                <asp:ListItem Value = "Line" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_204 %>" />
                <asp:ListItem Value = "Bar" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_205 %>" />
                <asp:ListItem Value = "Step" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_206 %>" />
                <asp:ListItem Value = "Area" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_207 %>" />
                <asp:ListItem Value = "Spline" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_209 %>" />
                <asp:ListItem Value = "Ribbon" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_208 %>" />
            </asp:DropDownList>
        </td>
        <td align="justify"  style="height: 30px; width: 50%">&nbsp;
        </td>
    </tr>
<!--/table-->