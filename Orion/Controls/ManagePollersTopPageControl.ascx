﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagePollersTopPageControl.ascx.cs" Inherits="Orion_Controls_ManagePollersTopPageControl" %>


<script type="text/javascript">
    $(document).ready(function () {
        $(".sw-tabs li.sw-tab").bind('mouseover', function () { $(this).addClass('x-tab-strip-over'); });
        $(".sw-tabs li.sw-tab").bind('mouseout', function () { $(this).removeClass('x-tab-strip-over'); });
    });
</script>

<asp:Panel id="topPanel" ClientIDMode="Static" runat="server"></asp:Panel>

<asp:Panel id="tabsPanel" ClientIDMode="Static" runat="server">
<div class="sw-tabs">
    <div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain">
        <div class="x-tab-strip-wrap">
            <ul class="x-tab-strip x-tab-strip-top" id="tabsUl" runat="server" style="width: 100%;">
                <%-- tab items will be placed here --%>
                <li class="x-tab-edge"></li>
                <div class="x-clear"><!-- --></div>
            </ul>
            <div class="x-tab-strip-spacer" style="border-bottom: none;"><!-- --></div>
        </div>
    </div>
</div>
</asp:Panel>
