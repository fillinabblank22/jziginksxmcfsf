﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClearTriggeredAlertsControl.ascx.cs" Inherits="Orion_Controls_ClearTriggeredAlertsControl" %>
<script type="text/javascript">
    jQuery(document).ready(function () {

        SW.Core.namespace("SW.Orion.Alerts").ClearTriggeredAlertsControl = function() {
            return {
                ClearAlerts: function(alertObjectIds, onSucc, onFail) {
                    <% if (!this.IsDemoServer) { %>
                    SW.Core.Services.callController("/api/ActiveAlertsGrid/ClearActiveAlerts", alertObjectIds, onSucc, onFail);
                    <% } else { %>
                    demoAction('Core_Alerting_Clear', this);
                    <% } %>
                }
            };
        }();
    });
</script>