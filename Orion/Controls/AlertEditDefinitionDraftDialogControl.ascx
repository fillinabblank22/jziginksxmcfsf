﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertEditDefinitionDraftDialogControl.ascx.cs" Inherits="Orion_Controls_AlertEditDefinitionDraftDialogControl" %>

        <div id="draftDialog-<%= SafeUniqueID %>" style="display:none;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_292) %>">
           <div><b><span class="lastChange"><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_329, SolarWinds.Orion.Web.Alerts.AlertWorkflowDraftCache.DraftExists() ? SolarWinds.Orion.Web.Alerts.AlertWorkflowDraftCache.GetCurrentUserDraft().LastChange.ToString("t") : String.Empty)) %></span></b></div>
           <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_330) %></div>
           <div class="sw-btn-bar-wizard" style="padding-top: 30px;">
              <orion:LocalizableButtonLink runat="server" ID="btnEditDraft" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_295 %>" DisplayType="Primary" />
              <orion:LocalizableButtonLink runat="server" ID="btnContinue" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_331 %>" DisplayType="Secondary" onclick="return false;" />
          </div>
        </div>

<script type="text/javascript">
    SW.Core.namespace("SW.Core.Alerting.Management");
    SW.Core.namespace("SW.Core.Alerts");
    SW.Core.Alerting.Management.EditAlertDefinition = function () {
        // CONSTRUCTOR
        var draftInfo = null;
        var IsFederationEnabled = SW.Core.Alerts.IsFederationEnabled;
        
        function federationUrlHelper(siteId) {
            if (!IsFederationEnabled)
                return "";

            var prefix = SW.Core.String.Format("/Server/{0}", siteId);

            return prefix;
        }

        // PRIVATE METHODS
        function alertDraftExists(siteId) {
            var exists = false;

            if( siteId != <%= SolarWinds.Orion.Core.Common.Caching.OrionSitesCache.Instance.GetLocalOrionSiteID() %> )
                return exists;


            SW.Core.Services.callControllerSync("/sapi/AlertDefinitionDetails/GetDraftInfo", null,
                function (succeed) {
                    exists = succeed.Exists;
                    draftInfo = succeed;
                    $("#draftDialog-<%= SafeUniqueID %> .lastChange").text(SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_329) %>", draftInfo.LastChange));
                }, function (failure) {
                    draftInfo = null;
                });

            return exists;
        };

        function getBtnContinueID() {
            return '<%= btnContinue.ClientID %>';
        };

        function getBtnEditDraftID() {
            return '<%= btnEditDraft.ClientID %>';
        };

        function checkForDraft (btnContinueText, defaultAction, alertId, siteId) {
            if (alertDraftExists(siteId)) {
                $("#draftDialog-<%= SafeUniqueID %>").dialog({
                                resizable: false,
                                width: 500,
                                modal: true
                            });
                            $('#' + getBtnContinueID() + " .sw-btn-t").text(btnContinueText);
                            $('#' + getBtnContinueID()).click(function () { defaultAction(alertId, siteId); });
                            $('#' + getBtnEditDraftID()).click(function () { window.location.href = draftInfo.DraftURL; });
                        } else {
                            defaultAction(alertId, siteId);
                        }
        };

        function redirectToEdit(alertId, siteId) {
            var guid = SW.Core.Services.generateNewGuid();
            var editParams = {
                AlertID: alertId
            };


           location.href = SW.Core.String.Format('{1}/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}&AlertID={2}', guid, federationUrlHelper(siteId), alertId);
        };

        // PUBLIC METHODS
        return {
            Edit: function (alertDefId) { 
                checkForDraft("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_135) %>", redirectToEdit, alertDefId, <%= SolarWinds.Orion.Core.Common.Caching.OrionSitesCache.Instance.GetLocalOrionSiteID() %>);
            },

            EditRemote: function (alertDefId, siteId) {
                checkForDraft("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_135) %>", redirectToEdit, alertDefId, siteId);
            }
        };
    }();
</script>

