<%@ Control Language="C#" ClassName="UnmanageDialog" CodeFile="UnmanageDialog.ascx.cs" Inherits="Orion_Controls_UnmanageDialog" %>
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>

<orion:Include runat="server" File="UnmanageDialog.css" />

<style type="text/css">
    .ui-dialog-titlebar
    {
        padding-right:30px;
    }
</style>

<%  var printable = false;
    bool.TryParse(Request["Printable"], out printable);
    if (!printable) { %>
<script type="text/javascript">
//<![CDATA[
    var showUnmanageDialog, remanageNodes, remanageInterfaces;

    var dialog, width, height;
    var minWidht = 550, minHeight = 'auto';

    $(function () {
        // Displays unamanage dialog and fills in specified values for from and to dates
        showUnmanageDialogWithDates = function (netObjectIds, nodeCaption, isInterfaces, from, to) {
            showUnmanageDialog(netObjectIds, nodeCaption, isInterfaces);

            $('#<%= unmanageStart.ClientID %>').orionSetDate(from);
            $('#<%= unmanageEnd.ClientID %>').orionSetDate(to);
        }

        showUnmanageDialog = function(netObjectIds, nodeCaption, isInterfaces) {
            if (netObjectIds.N == null) {
                var temp = netObjectIds;
                netObjectIds = { N: [], I: [] };
                if (isInterfaces)
                    netObjectIds.I = temp;
                else
                    netObjectIds.N = temp;
            }

            if (nodeCaption == null) {
                nodeCaption = "";
            } else {
				nodeCaption = unescape(nodeCaption);
                nodeCaption = nodeCaption.replace("&apos;", "'");
                nodeCaption = nodeCaption.replace("&quot;", '"');
            }
            $("#<%= UnmanageOk.ClientID %>").unbind().click(function() {
                var startDate = $('#<%= unmanageStart.ClientID %>').orionGetDate();
                var endDate = $('#<%= unmanageEnd.ClientID %>').orionGetDate();

                if (startDate > endDate) {
                    alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VS1_12) %>');
                    return false;
                }

            <% if (IsDemoServer) { %>
                demoAction("Core_Alerting_Unmanage", this);
                return false;
            <% } else { %>
                if (netObjectIds.N.length > 0)
                    NodeManagement.UnmanageNodes(netObjectIds.N, startDate, endDate,
                        function() {
                            if (netObjectIds.I.length == 0) {
                                window.location.reload();
                            }
                        },
                        function(error) {
                            alert(error.get_message() + '\n' + error.get_stackTrace());
                            if (netObjectIds.I.length == 0) {
                                $("#<%= unmanageDialog.ClientID %>").dialog("close");
                            }
                        }
                    );
                if (netObjectIds.I.length > 0)
                    NodeManagement.UnmanageInterfaces(netObjectIds.I, startDate, endDate,
                        function() { window.location.reload(); },
                        function(error) {
                            alert(error.get_message() + '\n' + error.get_stackTrace());
                            $("#<%= unmanageDialog.ClientID %>").dialog("close");
                        }
                    );
            <% } %>
            });

            caption = String.format("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_13) %>", (nodeCaption != "") ? nodeCaption :
            (String.format("{0}{1}", (netObjectIds.N.length > 0) ? (netObjectIds.N.length == 1 ? String.format("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_14) %>", netObjectIds.N.length) : String.format("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_15) %>", netObjectIds.N.length)) : "",
                netObjectIds.I.length > 0 ? (((netObjectIds.N.length > 0) ? String.format(" {0}", "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.String_Separator_1_VB0_59) %>") : " ") + (netObjectIds.I.length == 1 ? String.format("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_16) %>", netObjectIds.I.length) : String.format("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_17) %>", netObjectIds.I.length))) : "")));

            var textSize = $("#fontSizeTest")[0].offsetHeight;
            width = 88 * textSize;
            height = 27 * textSize;
            $("#<%= unmanageDialog.ClientID %>").width((width >= minWidht) ? width : minWidht);
            $("#<%= unmanageDialog.ClientID %>").height((height >= minHeight) ? height : minHeight);
            $("#<%= unmanageDialog.ClientID %>").find('input').attr("tabindex", "-1");

            dialog = $("#<%= unmanageDialog.ClientID %>").dialog({
                width: (width >= minWidht) ? width : minWidht, height: (height >= minHeight) ? height : minHeight, modal: true,
                open: function() {
                    //Workaround JQuery dialog issue - according to the jQuery UI ticket this will be fixed in version 1.9 by using the focusSelector option
                    $(this).find('input').attr("tabindex", "1");
                },
                overlay: { "background-color": "black", opacity: 0.4 }, title: caption, resizable: false
            });
            dialog.css('overflow', 'hidden');
            dialog.show();

            return false;
        };
        
        remanageNodes = function(nodeIds) {   
            <% if (IsDemoServer) { %>
            demoAction("Core_Alerting_RemanageNodes", this);
            return false;
            <% } else { %>
            NodeManagement.RemanageNodes(nodeIds,
                function() { window.location.reload(); },
                function(error) {
                    alert(error.get_message() + '\n' + error.get_stackTrace());
                }
            );
            <% } %>
        };

        remanageInterfaces = function(interfaceIds) {
            <% if (IsDemoServer) { %>
            demoAction("Core_Alerting_RemanageInterfaces", this);
            return false;
            <% } else { %>
            NodeManagement.RemanageInterfaces(interfaceIds,
                function() { window.location.reload(); },
                function(error) {
                    alert(error.get_message() + '\n' + error.get_stackTrace());
                }
            );
            <% } %>
        };

        var now = new Date();
        $('#<%=unmanageStart.ClientID%>').orionSetDate(now);
        now = new Date(now.getTime() + 24 * 60 * 60 * 1000);
        $('#<%=unmanageEnd.ClientID%>').orionSetDate(now);

        $("#<%= UnmanageCancel.ClientID %>").click(function () {
            $("#<%= unmanageDialog.ClientID%>").dialog("close");
        });
    });
    //]]>
</script>
<% } %>

<asp:ScriptManagerProxy runat="server">
	<Services>
		<asp:ServiceReference path="../Services/NodeManagement.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="js/StringUtils.js" />

<% if (!printable) { %>
<div id="fontSizeTest" style="position:absolute; line-height:0.5em; visibility:hidden!important;">text size test</div>
<div id="unmanageDialog" runat="server" style="display:none;" class="disposable, unmanageDialog">
	<div style=" margin-left:3px;margin-right:4px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_249) %> <br /></div>
	<table width="100%" style=" margin-left:3px;margin-right:4px;">
		<tr>
			<td rowspan="3" style="vertical-align: top;"><img src="/Orion/images/StatusIcons/Unmanaged.gif" alt="" /></td>
			<td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_250) %></td><br />
			<td colspan="2"><orion:DateTimePicker runat="server" ID="unmanageStart" /></td>
		</tr>
		<tr>
			<!--<td rowspan="3">big unmanage icon</td>-->
			<td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_251) %></td>
			<td colspan="2"><orion:DateTimePicker runat="server" ID="unmanageEnd" /></td>
		</tr>
		<tr>
			<!--<td rowspan="3">big unmanage icon</td>-->
			<td colspan="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_252) %></td>
		</tr>
	</table>

	<div class="bottom">
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButtonLink runat="server" LocalizedText="Ok" DisplayType="Primary" ID="UnmanageOk" />
		<orion:LocalizableButtonLink runat="server" LocalizedText="Cancel" DisplayType="Secondary" ID="UnmanageCancel" />
        </div>
	</div>
</div>
<% } %>
