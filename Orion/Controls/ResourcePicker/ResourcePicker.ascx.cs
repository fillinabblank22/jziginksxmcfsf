﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_ResourcePicker_ResourcePicker : System.Web.UI.UserControl
{
    public string ViewType { get; set; }

    private const string DummyJsAction = "function(){}";

    /// <summary>
    /// Javascript function which will be called when object is created.
    /// Signature: onCreated(picker);
    /// </summary>
    public string OnCreatedJs { get; set; }

    public bool SingleMode { get; set; }

    public Orion_ResourcePicker_ResourcePicker()
    {
        ViewType = string.Empty;
        OnCreatedJs = DummyJsAction;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}