<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourcePicker.ascx.cs" Inherits="Orion_ResourcePicker_ResourcePicker" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>

<orion:Include ID="Include4" File="ResourcePicker.css" runat="server" /> 
<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include3" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" File="ResourcePicker.js" runat="server" />

<input type="hidden" name="ResourcePicker_SelectedColumns" id="ResourcePicker_SelectedColumns" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ResourcePicker_SelectedColumns", "Title,Category")) %>' />

<table class="resourcePicker">  
    <tr>
        <td class="groupByColumn headerColumn">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_36) %>
        </td>
        <td class="gridColumn headerColumn" >
            <input type="text" id="tbSearch" runat="server" class="searchInput" />
            <span runat="server" id="btnHolder" ></span>
        </td>
        <td class="selectedResourcesColumn headerColumn">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_35) %>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <div id="myResPicker"></div>
        </td>
        <td valign="top" class="selectedResourcesColumn">
            <div id="selectedResources" class="selectedResources" >
                 <div id="selectedResourcesEmpty"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_37) %></div>
                <table><tbody></tbody></table>
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    $(function() {
        var viewType = '<%= ViewType %>';

        var resourcePicker = new SW.Core.ResourcePicker({
             renderTo: 'myResPicker',
             selectedResourcesTo: 'selectedResources',
             viewType: viewType,
             selectionMode: '<%= SingleMode ? "Single" : "Multiple" %>', 
             onCreated: <%= OnCreatedJs %>
         });
        
        var searcher = new  SW.Core.ResourcePickerSearcher ({
            searchButtonHolder : '#<%= btnHolder.ClientID %>',
            searchInput : '#<%= tbSearch.ClientID %>', 
            onSearched : resourcePicker.Search,
            viewType: viewType
        });
        
    });
</script>
