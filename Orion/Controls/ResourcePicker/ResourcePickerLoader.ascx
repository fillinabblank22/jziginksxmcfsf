<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourcePickerLoader.ascx.cs" Inherits="Orion_ResourcePicker_ResourcePickerLoader" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourcePicker" Src="~/Orion/Controls/ResourcePicker/ResourcePicker.ascx" %>

<orion:Include ID="Include4" File="ResourcePicker.css" runat="server" />
<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include3" runat="server" File="OrionCore.js" />
<orion:Include ID="Include5" File="ResourcePicker.js" runat="server" />

<div id="resourcePickerDialog" class="resourcePickerLoader offPage">
    <div id="resourcePickerLoader-loading">
        <div class="content">
             <img src="/Orion/images/loading_gen_small.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcePickerDialog_LoadingAltText) %>" />
            
            <div class="mainText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcePickerDialog_CacheBuilding) %></div>
            <div class="subText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcePickerDialog_CacheBuilding_subtext) %></div>
        </div>
       
    </div>
    <div id="resourcePickerLoader-content" class="notVisible">

        <orion:ResourcePicker ID="myResPicker" runat="server" OnCreatedJs="onResourcePickerCreated" />

        <div class="sw-btn-bar resourcePickerDialogBtns">
            <orion:LocalizableButtonLink ID="btnDialogOk" runat="server" DisplayType="Primary" LocalizedText="CustomText" automation="Add selected resources" Text="<%$ HtmlEncodedResources: CoreWebContent, ResourcePickerDialog_Save %>"  />
            <orion:LocalizableButtonLink ID="btnDialogCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" automation="Cancel" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>" />
        </div>
    </div>
</div>

<script type="text/javascript">

    function onResourcePickerCreated(picker) {
        var loader = new SW.Core.ResourcePickerLoader({
            resourcePicker : picker,
            dialogElement : '#resourcePickerDialog',
            loadingElement : '#resourcePickerLoader-loading',
            contentElement : '#resourcePickerLoader-content',
            btnDialogOk : '#<%= btnDialogOk.ClientID %>',
            btnDialogCancel : '#<%= btnDialogCancel.ClientID %>',
            dialogTitle : '<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.ResourcePickerDialog_DialogTitle) %>',
            onSelected :  <%= OnSelectedJs %>,
            onCreated : <%= OnCreatedJs %>
            });
    }
</script>
