﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_ResourcePicker_ResourcePickerLoader : System.Web.UI.UserControl
{
    private const string DummyJsFunction = "function(){}";

    private string _viewType;

    public string ViewType
    {
        get { return _viewType; }
        set {
            _viewType = value;
            if (myResPicker != null)
            {
                myResPicker.ViewType = value;
            }
        }
    }
    
    public string OnSelectedJs { get; set; }
    
    public string OnCreatedJs { get; set; }
    
    public bool SingleMode
    {
        get { return myResPicker.SingleMode; }
        set { myResPicker.SingleMode = value; }
    }
    
    public string OkButtonText 
    {
        get { return btnDialogOk.Text; }
        set { btnDialogOk.Text = value; }
    } 

    public Orion_ResourcePicker_ResourcePickerLoader()
    {
        OnSelectedJs = DummyJsFunction;
        OnCreatedJs = DummyJsFunction;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //If not set, assume we are in reporting  
        if (string.IsNullOrEmpty(ViewType))
            myResPicker.ViewType = ResourceTemplateGroupsProvider.ReportingViewType;

    }
}