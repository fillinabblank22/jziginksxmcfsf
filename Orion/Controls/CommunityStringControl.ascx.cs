using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.SyslogTrapInterfaces;
using SolarWinds.SyslogTraps.Common;

public partial class CommunityStringControl : System.Web.UI.UserControl, ICommunityStringControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    public string CommunityString
    {
        get { return communityStrings.SelectedValue; }
        set { communityStrings.SelectedValue = value; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string selectedvalue = string.Empty;
        if (!string.IsNullOrEmpty(CommunityString))
        {
            selectedvalue = CommunityString;
        }

        communityStrings.Items.Clear();
        communityStrings.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBDATA_VB0_234, string.Empty));

        using (var proxy = SyslogTrapBusinessLayerProxyFactory.Instance.Create())
        {
            List<string> commStrings = proxy.Api.GetTrapsCommunities();

            foreach (string commString in commStrings)
            {
                communityStrings.Items.Add(new ListItem(commString, commString));
            }
        }

        CommunityString = selectedvalue;
    }
}
