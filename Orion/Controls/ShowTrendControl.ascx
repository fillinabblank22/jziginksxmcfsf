<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShowTrendControl.ascx.cs" Inherits="ShowTrendControl" %>

<tr>
    <% if( !InLine ) { %>
	        <td class="style1" style="font-weight: bold; text-align:left;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_292) %></td>
	        <td class="formRightInput">
    <% } else { %>
	        <td colspan="3">
                <p>
                <div class="style1" style="font-weight: bold; text-align:left;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_292) %>
                </div>
                <div>
    <% } %>

        <asp:CheckBox TextAlign="Right" Checked="true" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_291 %>" id="showTrend" runat="server" /> 

    <% if( InLine ) { %>
                </div>
                </p>
    <% } %>

    </td>
</tr>
<tr>
	<td colspan="3"><hr class="formDivider"/></td>
</tr>
