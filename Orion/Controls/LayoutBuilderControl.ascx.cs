﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Reporting;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Reporting.Models.Tables;
using SolarWinds.Reporting.Models.Timing;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Impl.Presentation;
using SolarWinds.Orion.Core.Common.InformationService;

public partial class Orion_Controls_LayoutBuilderControl : UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public string WorkflowGuid
    {
        get
        {
            if (ViewState["LayoutBuilderGuid"] == null)
                ViewState["LayoutBuilderGuid"] = Guid.NewGuid().ToString();
            return (string) ViewState["LayoutBuilderGuid"];
        }
        set
        {
            ViewState["LayoutBuilderGuid"] = value;
            sectionsSessionIdentifier.Value = String.Format("SectionsState-{0}", value);
            configsSessionIdentifier.Value = String.Format("ConfigsState-{0}", value);
            timePeriods.WorkflowGuid = value;
            AddContentLoader.WorkflowGuid = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string EmptyMessage { get; set; }

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }

    public Section[] Sections
    {
        get
        {
            if (Session[sectionsSessionIdentifier.Value] != null)
            {
                var serializer = new DataContractJsonSerializer(typeof(Section[]));
                return (Section[])serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes((string)Session[sectionsSessionIdentifier.Value])));
            }
            return null;
        }
        set
        {
            var serializer = new DataContractJsonSerializer(typeof(Section[]));
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, value);
                Session[sectionsSessionIdentifier.Value] = Encoding.UTF8.GetString(ms.ToArray());
            }
            
        }
    }

    public TimeFrame[] TimeFrames
    {
        get { return timePeriods.TimeFrames; }
        set { timePeriods.TimeFrames = value; }
    }

    public DataSource[] DataSources
    {
        get { return AddContentLoader.DataSources; }
        set { AddContentLoader.DataSources = value; }
    }


    public ConfigurationData[] Configs
    {
        get
        {
            if (Session[configsSessionIdentifier.Value] == null)
            {
                Session[configsSessionIdentifier.Value] = new ConfigurationData[0];
            }
            return (ConfigurationData[])Session[configsSessionIdentifier.Value];
        }
        set
        {
            Session[configsSessionIdentifier.Value] = value;
        }
    }

    protected override void  OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!IsPostBack)
        {
            sectionsSessionIdentifier.Value = String.Format("SectionsState-{0}", WorkflowGuid);
            configsSessionIdentifier.Value = String.Format("ConfigsState-{0}", WorkflowGuid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public bool Validate()
    {
        if (DataSources == null)
        {
            return true;
        }

        bool result = true;
        var dataSourceFieldProvidersDictionary = new Dictionary<Guid, IDataSourceFieldProvider>();
        var sectionCellsWithErrors = new Dictionary<Guid, string>();
        var additionalErrors = new Dictionary<Guid, string>();
                
        // get field providers for all data sources
        foreach (DataSource dataSource in DataSources)
        {
            IDataSourceFieldProvider fieldProvider = DataSourceFactory.GetFieldProvider(dataSource, new InformationServiceProxyCreator(() => SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3()));
            dataSourceFieldProvidersDictionary[dataSource.RefId] = fieldProvider;
        }
        var reqFields = new Dictionary<Guid, List<FieldRef>>();

        var repo = ReportingRepository.GetInstance<IDataPresenterRepository>();
        // validates configuration data in each section cell
        foreach (SectionCell sectionCell in Sections.SelectMany(s => s.Columns).SelectMany(c => c.Cells))
        {
            ConfigurationData config = Configs.First(c => c.RefId == sectionCell.ConfigId);
            var fields = new List<FieldRef>();
            if (config is TableConfiguration)
            {
                var tableConfig = (TableConfiguration) config;

                if (tableConfig.Columns != null)
                {
                    fields = tableConfig.Columns.Select(c => c.Field.RefID).ToList();

                    foreach (var column in tableConfig.Columns)
                    {
                        if (column.Presenters == null) continue;
                        var dt =
                            DataSources.FirstOrDefault(
                                dataSource => dataSource.RefId == sectionCell.DataSelectionRefId);
                        if (dt == null ||
                            (dt.Type != DataSourceType.CustomSWQL && dt.Type != DataSourceType.CustomSQL)) continue;
                        foreach (var cPresenter in column.Presenters)
                        {
                            var p = repo.GetRef(cPresenter.PresenterId);
                            var instance = p.Factory.NewInstance(dt, column.Field, null);

                            if (instance != null && instance.RequiredProperties != null)
                            {
                                foreach (var rp in instance.RequiredProperties)
                                {
                                    var data = rp.Data;
                                    //add additional columns required for presenter
                                    if (reqFields.ContainsKey(sectionCell.DataSelectionRefId))
                                    {
                                        reqFields[sectionCell.DataSelectionRefId].Add(data);
                                    }
                                    else
                                    {
                                        reqFields[sectionCell.DataSelectionRefId] = new List<FieldRef> {data};
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (config is ChartConfiguration)
            {
                var chartConfig = (ChartConfiguration) config;
                fields = chartConfig.GetFields().Select(f => f.RefID).ToList();
                if (chartConfig.CaptionField != null)
                {
                    fields.Add(chartConfig.CaptionField.RefID);
                }
                if (chartConfig.KeyField != null)
                {
                    fields.Add(chartConfig.KeyField.RefID);
                }
            }

            // all fields used in configuration are subset of assigned data source
            Field field;
            bool selectedFieldsAreCoveredByDataSource = (sectionCell.DataSelectionRefId == Guid.Empty)
                                                            ? fields.Count() == 0
                                                            : fields.All(f => dataSourceFieldProvidersDictionary[sectionCell.DataSelectionRefId].TryGetField(f, out field));
            if (!selectedFieldsAreCoveredByDataSource)
            {
                string dataSourceName = DataSources.First(ds => ds.RefId == sectionCell.DataSelectionRefId).Name;
                sectionCellsWithErrors.Add(sectionCell.RefId, dataSourceName);
                result = false;
            }

            if (reqFields.Count > 0)
            {
                //validate if required for presenter field exists within the datasource
                bool selectedRequiredFieldsAreCoveredByDataSource =
                    !reqFields.ContainsKey(sectionCell.DataSelectionRefId) ||
                    reqFields[sectionCell.DataSelectionRefId].All(f => dataSourceFieldProvidersDictionary[sectionCell.DataSelectionRefId].TryGetField(f, out field));
                if (!selectedRequiredFieldsAreCoveredByDataSource && selectedFieldsAreCoveredByDataSource &&
                    reqFields.ContainsKey(sectionCell.DataSelectionRefId))
                {
                    string dataSourceName = string.Join(",", reqFields[sectionCell.DataSelectionRefId].Distinct());
                    additionalErrors.Add(sectionCell.RefId, dataSourceName);
                    result = false;
                }
            }
        }        

        if (!result)
        {
            var jsCalls = new StringBuilder();
            foreach (var jsCall in from cellWithError in sectionCellsWithErrors let errorMessage = string.Format(CoreWebContent.WEBCODE_PF0_10, "<b>", cellWithError.Value, "</b>") select string.Format("SW.Orion.LayoutBuilder.showErrorInSectionCell('{0}','{1}', true);", cellWithError.Key, errorMessage))
            {
                jsCalls.AppendLine(jsCall);
            }

            if (sectionCellsWithErrors.Count == 0)
            {
                foreach (var jsCall in from error in additionalErrors let errorMessage = string.Format(CoreWebContent.WEBCODE_YK0_25, error.Value) select string.Format("SW.Orion.LayoutBuilder.showErrorInSectionCell('{0}','{1}', true);",
                                                                                                                                                                        error.Key, errorMessage))
                {
                    jsCalls.AppendLine(jsCall);
                }
            }

            string js = string.Format(@"SW.Orion.LayoutBuilder.reportErrors = function() {{ {0} }};", jsCalls);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "myerrors", js, true);
        }
        return result;
    }
}