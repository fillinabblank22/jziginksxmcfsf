using System.Web.UI;

public partial class Orion_Controls_EventList : UserControl
{
    public object DataSource
    {
        get { return EventsGrid.DataSource; }
        set
        {
            EventsGrid.DataSource = value;
            EventsGrid.DataBind();
        }
    }
}