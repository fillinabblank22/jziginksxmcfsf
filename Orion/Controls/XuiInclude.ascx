<%@ Control Language="C#" AutoEventWireup="true" CodeFile="XuiInclude.ascx.cs" Inherits="Orion_Controls_XuiInclude" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Settings" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.IO" %>

<% if (OrionMinReqsMaster.IsApolloEnabled)
   {
       bool minify = GeneralSettings.Instance.MinifyContent;
    %>
<!-- XuiInclude Start -->
<link href="/ui/bundles/apolloisolate/css?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" rel="stylesheet">
<script src="/ui/scripts/environment?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" type="text/javascript"></script>
<script src="/ui/scripts/apolloisolate?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" type="text/javascript"></script>
<script src="/api2/l10n/resources/web?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" type="text/javascript"></script>
<script src="/api2/l10n/resources/apollo?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" type="text/javascript"></script>
<script src="/api2/l10n/resources/widgets?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" type="text/javascript"></script>
<script src="/api2/l10n/resources/ui?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" type="text/javascript"></script>
<script src="/api2/l10n/resources/search?<%= DefaultSanitizer.SanitizeHtml(WebsiteRevision.VersionID) %>" type="text/javascript"></script>

<%-- Localization for angular's components --%>
<% if (File.Exists(Request.MapPath(AngularLocaleJSPath))) {%>
<script src="<%= DefaultSanitizer.SanitizeHtml(AngularLocaleJSPath) %>" type="text/javascript"></script>
<% } %>

<% if (File.Exists(Request.MapPath(MomentJSPath))) {%>
<script src="<%= DefaultSanitizer.SanitizeHtml(MomentJSPath) %>" type="text/javascript"></script>
<% } %>

<!-- XuiInclude End -->
<% } %>