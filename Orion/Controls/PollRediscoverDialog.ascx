<%@ Control Language="C#" ClassName="PollRediscoverDialog" %>
<%@ Register TagPrefix="orion" TagName="ProgressDialog" Src="~/Orion/Controls/ProgressDialog.ascx" %>

<orion:ProgressDialog ID="ProgressDialog" runat="server" />  

<script type="text/javascript">
//<![CDATA[
     function ProgressDialog(options) {
         SW.Core.MessageBox.ProgressDialog(options);
    };
        
    function showPollNowDialog(ids){
        ProgressDialog({
            title: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_4) %>",
            items: ids,
            serverMethod: NodeManagement.PollNetObjNow,
            getArg: function(id) { return id },
            sucessMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_5) %>",
            failMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_6) %>",
            afterSucceeded: function(id) { setTimeout(function() { window.location.reload(true); }, 100); }
        });
        return false;
    }

    function showRediscoveryDialog(ids) {
        ProgressDialog({
            title: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_7) %>",
            items: ids,
            serverMethod: NodeManagement.RediscoverNow,
            getArg: function(id) { return id },
            sucessMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_8) %>",
            failMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_9) %>",
            afterSucceeded: function(id) { }
        });
        return false;
    }

    function removeBox() {
        
        $(".StatusDialog").dialog('destroy').remove();
    }     
      
//]]>    
</script>

