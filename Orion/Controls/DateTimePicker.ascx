﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateTimePicker.ascx.cs" Inherits="Orion_Controls_DateTimePicker" %>

<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="js/jquery/timePicker.css" />
<span id="<%= ClientID %>">
	<asp:TextBox ID="txtDatePicker" runat="server" Width="78px" CssClass="datePicker disposable" customplaceholder="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VM0_8 %>" />
    <asp:CompareValidator id="dateValidator" runat="server"  Type="Date" Operator="DataTypeCheck" 
    ControlToValidate="txtDatePicker" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_GenericValidationError_Date %>" Display="Dynamic"/>
    <asp:RequiredFieldValidator ID="emptyDateValidator" runat="server" ControlToValidate="txtDatePicker" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_GenericValidationError_Date %>" Display="Dynamic"></asp:RequiredFieldValidator>
    <asp:TextBox ID="txtTimePicker" runat="server" Width="78px" CssClass="timePicker disposable" customplaceholder="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VM0_9 %>"/>
    <asp:CustomValidator ID="timeValidator" runat="server" ControlToValidate="txtTimePicker" 
    Display="Dynamic" 
    ClientValidationFunction = "$.fn.validateTime" 
    ErrorMessage= "<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_508 %>" EnableClientScript="true"
     />
    <asp:RequiredFieldValidator ID="emptyTimeValidator" runat="server" ControlToValidate="txtTimePicker" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_508 %>" Display="Dynamic"></asp:RequiredFieldValidator>
</span>

<% if (ForceMarkupClientScriptBlockUsage) {%>
    <script type="text/javascript">
         <%= ClientScriptString  %>
    </script>
<% } %>
 
