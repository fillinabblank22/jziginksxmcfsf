<%@ Control Language="C#" ClassName="OverridePowerLevel" %>

<script type="text/javascript">
    var showPowerLevelDialog;

    $(function()
    {
        showPowerLevelDialog = function(netObjectIds, nodeCaption, isInterfaces)
        {
            if (netObjectIds.N == null)
            {
                var temp = netObjectIds;
                netObjectIds = { N: [], I: [] };
                if (isInterfaces)
                    netObjectIds.I = temp;
                else
                    netObjectIds.N = temp;
            }
            if (nodeCaption == null) nodeCaption = "";
            $('.dialogButtonOk').unbind().click(function ()
            {
                var level = $("#powerLevel").val();
                
                /*
                if (netObjectIds.N.length > 0)
                NodeManagement.UnmanageNodes(netObjectIds.N, startDate, endDate,
                function() { window.location.reload(); },
                function(error)
                {
                alert(error.get_message() + '\n' + error.get_stackTrace());
                $("#powerLevelDialog").dialog("close");
                }
                );
                */
                if (netObjectIds.I.length > 0)
                    NodeManagement.EnergyWiseSetInterfacePowerLevel(netObjectIds.I, level,
					function() { window.location.reload(); },
					function(error)
					{
					    /*alert(error.get_message() + '\n' + error.get_stackTrace());*/
					    alert(error.get_message());
					    $("#powerLevelDialog").dialog("close");
					}
				);
            });
            caption = ((nodeCaption != "") ? nodeCaption : (
		    ((netObjectIds.N.length > 0) ? (netObjectIds.N.length + (netObjectIds.N.length == 1 ? "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_20) %>" : "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_21) %>")) : "")
		    + (netObjectIds.I.length > 0 ? (((netObjectIds.N.length > 0) ? "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_22) %>" : " ") + netObjectIds.I.length + (netObjectIds.I.length == 1 ? "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_23) %>" : "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_24) %>")) : ""))
            );

            $("#powerLevelDialog").dialog({
                width: 300, height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: caption
            }).show();
            return false;
        };

        $(".dialogButtonCancel").click(function () { $("#powerLevelDialog").dialog("close"); });
    });
</script>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	<Services>
		<asp:ServiceReference path="../Services/NodeManagement.asmx" />
	</Services>
</asp:ScriptManagerProxy>


<div id="powerLevelDialog" class="dialog" style="display:none;overflow:hidden">
	<div style="padding: 5px; width: 270px">
	    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_510) %> 
	    <select id="powerLevel" style="width: auto">
            <option value="0"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_500) %></option>
		    <option value="1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_499) %></option>
		    <option value="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_501) %></option>
		    <option value="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_502) %></option>
		    <option value="4"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_503) %></option>
		    <option value="5"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_504) %></option>
		    <option value="6"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_505) %></option>
		    <option value="7"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_506) %></option>
		    <option value="8"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_507) %></option>
		    <option value="9"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_508) %></option>
		    <option value="10"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_509) %></option>
	    </select>
	</div>
    <div style="padding: 10px; font-size:x-small;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_511) %></div>
	<div style="text-align: right; padding: 10px;">
		<orion:LocalizableButtonLink LocalizedText="Ok" DisplayType="Primary" id="buttonOK" runat="server" CssClass="dialogButtonOk"/>
		<orion:LocalizableButtonLink LocalizedText="Cancel" DisplayType="Secondary" id="buttonCancel" runat="server" CssClass="dialogButtonCancel"/>
	</div>
</div>
