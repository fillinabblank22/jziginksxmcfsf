<%@ Control Language="C#" ClassName="HelpLink" %>
<%@ Reference Control="~/Orion/Controls/HelpButton.ascx" %>

<script runat="server">
    public string HelpUrlFragment { get; set; }

    protected string HelpURL
    {
        get
        {
            return ASP.HelpButton.GetHelpUrl(this.HelpUrlFragment);
        }
    }

    private string helpDescription = String.Empty;

    public string HelpDescription
    {
        get { return helpDescription; }
        set { helpDescription = value; }
    }

    public string CssClass
    {
        get;
        set;
    }	 
</script>

<a href="<%= DefaultSanitizer.SanitizeHtml(this.HelpURL) %>" target="_blank" rel="noopener noreferrer" class="<%= DefaultSanitizer.SanitizeHtml(CssClass) %>">
	<%= DefaultSanitizer.SanitizeHtml(HelpDescription) %>
</a>
