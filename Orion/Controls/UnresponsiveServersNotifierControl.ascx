﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UnresponsiveServersNotifierControl.ascx.cs" Inherits="Orion_Controls_UnresponsiveServersNotifierControl" %>

<orion:Include ID="Include1" File="UnresponsiveServersNotifier.js" runat="server" />

<div class='sw-suggestion sw-suggestion-fail' style='width: 100%; padding-left: 0px !important; padding-right: 0px !important; margin-top: 5px; margin-bottom: 5px; display: none;' runat="server" ID="MessageContainer">
    <span class='sw-suggestion-icon'></span>
    <span style="padding-left: 25px; font-size: small;" class="sw-unresponsive-servers-notifier-title"></span>
    <br/>
    <span style="padding-left: 25px; margin-top: 3px; font-size: small; font-weight: normal; float: left;" class="sw-unresponsive-servers-notifier-msg"></span>
</div>