﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertSuppressionStatusDescription.ascx.cs" Inherits="Orion_Controls_AlertSuppressionStatusDescription" %>
<orion:Include runat="server" File="js/MaintenanceMode/AlertSuppressionHandlers.js" />
<orion:Include runat="server" File="js/MaintenanceMode/AlertSuppressionStatusDescription.js" />
<orion:include runat="server" File="js/MaintenanceMode/MaintenanceModeUtils.js" />

<asp:ScriptManagerProxy runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Services/Information.asmx" />
    </Services>
</asp:ScriptManagerProxy>