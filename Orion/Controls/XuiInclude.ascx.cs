﻿using System;
using System.Threading;
using System.Globalization;

public partial class Orion_Controls_XuiInclude : System.Web.UI.UserControl
{
    protected CultureInfo Culture
    {
        get
        {
            return Thread.CurrentThread.CurrentCulture;
        }
    }

    protected string AngularLocaleJSPath
    {
        get
        {
            return $"/ui/modules/ui-bundle/novajs/i18n/angular-locale_{Culture.Name}.js";
        }
    }

    protected string MomentJSPath
    {
        get
        {
            return $"/ui/modules/ui-bundle/novajs/i18n/moment/{Culture.TwoLetterISOLanguageName}.js";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}