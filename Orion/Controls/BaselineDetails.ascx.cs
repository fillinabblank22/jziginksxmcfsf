﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Controls_BaselineDetails : UserControl
{
    private int? baselineCollectionDuration;
    private List<TimeFrame> timeFrames;
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    #region Protected methods

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            this.LoadTimeFrames();
            this.tftWorkDays.TimeFrame = timeFrames != null ? timeFrames.Find(t => t.Name.Equals(this.tftWorkDays.TimeFrameName)) : null;
            this.tftWorkDays.Title = Resources.CoreWebContent.WEBDATA_TK0_31; // Work Days
            this.tftOffDays.TimeFrame = timeFrames != null ? timeFrames.Find(t => t.Name.Equals(this.tftOffDays.TimeFrameName)) : null;
            this.tftOffDays.Title = Resources.CoreWebContent.WEBDATA_TK0_32; // Evenings and Weekends

            this.LoadBaselineCollectionDuration();
            this.mbDetailsHint.MessageHtml = string.Format(Resources.CoreWebContent.WEBDATA_TK0_33, baselineCollectionDuration != null && baselineCollectionDuration != -1 ? baselineCollectionDuration.ToString() : " "); // The amount of data to be collected in the baseline is currently set to <span class=""bold"">{0} days</span>.
            this.mbDetailsHint.MessageHtml += string.Format(@"&nbsp;<a href=""/Orion/Admin/PollingSettings.aspx"" target=""_blank"">&gt;&gt; {0}</a>", Resources.CoreWebContent.WEBDATA_TK0_34); // Change this setting

            this.InitAreaChart();
            this.InitMinMaxChart();

            this.ccArea.LegendTitle = Resources.CoreWebContent.WEBDATA_TK0_35; // Show Statistics in Graph
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);

        writer.Write(GetAreaChartScripts());
        writer.Write(GetMinMaxChartScripts());
    }

    #endregion

    #region Private methods

    private void InitAreaChart()
    {
        this.ccArea.CustomChartOptions = this.GetAreaChartOptions();
        this.ccArea.CustomChartSettings = this.GetAreaChartSettings();
    }

    private void InitMinMaxChart()
    {
        this.ccHistorical.CustomChartOptions = this.GetMinMaxChartOptions();
        this.ccHistorical.CustomChartSettings = this.GetMinMaxChartSettings();
    }

    private void LoadBaselineCollectionDuration()
    {
        baselineCollectionDuration = SettingsDAL.GetCurrentInt("SWNetPerfMon-Settings-Baseline Collection Duration", 7);
    }

    private string GetThresholdsUnits()
    {
        StringBuilder result = new StringBuilder();
        DataTable allThresholds = null;

        using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
        {
            allThresholds = service.Query("SELECT Name, Unit FROM Orion.ThresholdsNames");
        }

        if (allThresholds != null && allThresholds.Rows.Count > 0)
        {
            foreach (DataRow row in allThresholds.Rows)
            {
                var escapedName = WebSecurityHelper.JavaScriptStringEncode(row["Name"].ToString());
                var escapedUnit = WebSecurityHelper.JavaScriptStringEncode(row["Unit"].ToString());
                result.AppendLine(String.Format("case '{0}': return '{1}';", escapedName, escapedUnit));
            }
        }

        return result.ToString();
    }

    private void LoadTimeFrames()
    {
        using (var proxy = _blProxyCreator.Create(delegate(Exception ex) { throw ex; }))
        {
           timeFrames = proxy.GetCoreTimeFrames();
        }
    }

    #region Area chart

    private string GetAreaChartOptions()
    {
return string.Format(@"
{{
    highchartsChartType: 'chart',
    chart: {{
        width: {0},
        height: {1},
        marginTop: 10,        
        events: {{
            load: function() {{ }}
        }}
    }},
    rangeSelector: {{
        enabled: false
    }},               
    scrollbar: {{
	  	enabled: false
	}},    
    tooltip: {{
            shared: true,
			positioningEnabled: true,   
            formatter: function() {{
                            var minValue = this.x;
                            var interval = this.points[0].series.closestPointRange;
                            var maxValue = minValue + interval;

                            if(interval != null && parseFloat(interval))
                            {{
                                if(!((parseFloat(maxValue) == parseInt(maxValue,10)) && !isNaN(maxValue)))
                                {{
                                    maxValue = maxValue.toFixed(1);
                                }}
                                _tooltip = '<div id=""highchartTooltip"">{2} ' + minValue + ' - ' + maxValue;
                            }}
                            else
                            {{
                                _tooltip = '<div id=""highchartTooltip"">{2} ' + minValue + ' - ' + minValue;
                            }}

                            $.each(this.points, function(i, point) {{
                                _tooltip += '<br/><span style=""color:'+point.series.color+'"">'+point.series.name+'</span>: <b>' + point.y + 'x</b>';
                            }});

                            _tooltip += '</div>';
                            return _tooltip;
           }}
    }},
    navigator: {{
	  	enabled: false
	}},
    plotOptions: {{
	   area: {{
	    }},
       series: {{
            connectNulls: true
       }}
    }},
    xAxis: {{
        events: {{
            afterSetExtremes: function() {{ }}
        }}
    }},
    yAxis: [{{
		title: {{
            margin: 5,
			text: '{3}'
		}},
        unit: 'x',
		minRange: 0
	}}],
    seriesTemplates: {{
	    Core_WorkHours: {{
		    name: '{4}',
		    type : 'area',
            color: '#4F81BD',
		    zIndex: 3
	    }},
	    Core_NightAndWeekend: {{
		    name: '{5}',
		    type : 'area',
            color: '#B43633',
		    zIndex: 2
	    }},
        Core_All: {{
		    name: '{6}',
		    type : 'area',
            color: '#9BBB59',
		    zIndex: 1
	    }}
    }}
}}", 450,
   235,
   Resources.CoreWebContent.WEBDATA_TK0_53,
   Resources.CoreWebContent.WEBDATA_TK0_54,
   Resources.CoreWebContent.WEBDATA_TK0_57,
   Resources.CoreWebContent.WEBDATA_TK0_56,
   Resources.CoreWebContent.WEBDATA_TK0_55);
    }

    private Dictionary<string, object> GetAreaChartSettings()
    {
        var details = new Dictionary<string, object>();

        details["dataUrl"] = "/api/thresholds/GetHistogramForStatisticalData";

        return details;
    }

    private string GetAreaChartScripts()
    {
        var js = string.Format(@"
            <script type='text/javascript'>    

                var getUnit{0} = function (thresholdName) {{
                    switch (thresholdName)
                    {{
                        {1}                        
                        default: return '';
                    }}
                }};                           

                var transformSettings{0} = function (parameters, settings) {{        
                    settings.netObjectIds[0] = parameters[0];        
                    settings.thresholdName = parameters[1];
                    return settings;
                }};
    
                var transformOptions{0} = function (parameters, options) {{   
                    var unit = getUnit{0}(parameters[1]);
                    options.chart.events.load = function() {{ new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts(); new SW.Core.Baselines.BaselineDetails().redrawXAxis(parameters[2], unit); }};
                    options.xAxis.events.afterSetExtremes = function() {{ new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts(); new SW.Core.Baselines.BaselineDetails().redrawXAxis(parameters[2], unit); }};    
                    if (parameters[4])
                    {{
                        options.chart.width = parameters[3];
                    }}
                    if (unit)
                    {{
                        options.tooltip.formatter = function() {{
                            var _tooltip = '';
                            var minValue = this.x;
                            var interval = this.points[0].series.closestPointRange;
                            var maxValue = minValue + interval;

                            if(interval != null && parseFloat(interval))
                            {{
                                if(!((parseFloat(maxValue) == parseInt(maxValue,10)) && !isNaN(maxValue)))
                                {{
                                    maxValue = maxValue.toFixed(1);
                                }}
                                _tooltip = '<div id=""highchartTooltip"">{2} ' + minValue + ' - ' + maxValue + ' ' + unit;
                            }}
                            else
                            {{
                                _tooltip = '<div id=""highchartTooltip"">{2} ' + minValue + ' - ' + minValue + ' ' + unit;
                            }}

                            $.each(this.points, function(i, point) {{
                                _tooltip += '<br/><span style=""color:'+point.series.color+'"">'+point.series.name+'</span>: <b>' + point.y + 'x</b>';
                            }});

                            _tooltip += '</div>';
                            return _tooltip;
                        }}
                    }}

                    return options;
                }};
         </script>", this.ccArea.ID, this.GetThresholdsUnits(), Resources.CoreWebContent.WEBDATA_TK0_53);

        return js;
    }

    #endregion

    #region MinMax chart

    private string GetMinMaxChartOptions()
    {
        return string.Format(@"
{{
    chart: {{
        width: {0},
        height: {1},
        marginTop: 10,        
        events: {{
            load: function() {{ new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts(); }}
        }}
    }},
    rangeSelector: {{
        enabled: false
    }},                    
    xAxis: {{
        events: {{
            afterSetExtremes: function() {{ new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts(); }}
        }}    
    }},
    plotOptions: {{
       series: {{
            connectNulls: true
       }}
    }},
    yAxis: [{{
        title: {{
            useHTML: true,
            offset: 60,
            style: {{
                width:'200px'
            }}    
        }}
    }}]
}}", 460, 280);
    }

    private Dictionary<string, object> GetMinMaxChartSettings()
    {
        var details = new Dictionary<string, object>();



        return details;
    }

    private string GetMinMaxChartScripts()
    {
        var js = string.Format(@"
            <script type='text/javascript'>

                var getUnit{0} = function (thresholdName) {{
                    switch (thresholdName)
                    {{
                        {1}                    
                        default: return '';
                    }}
                }};                

                var transformSettings{0} = function (parameters, settings) {{        
                   
                    settings.netObjectIds[0] = parameters[0];
                    settings.dataUrl = parameters[4].DataUrl;
                    settings.timespanInDays = parameters[4].BaselineCollectionDuration;
                    settings.sampleSizeInMinutes = 5;
                
                    return settings;
                }};
    
                var transformOptions{0} = function (parameters, options) {{
                   
                   
                    var chartOptions = parameters[4].ChartOptions;

                    options = $.extend(true, options, chartOptions);

                    options.yAxis[0].title.text = parameters[4].ChartDisplayName;
                    options.yAxis[0].unit = getUnit{0}(parameters[1]);
                    options.showTitle = true;
                    if (parameters[3])
                    {{
                        options.chart.width = parameters[4];
                    }}

                    return options;
                }};
         </script>", this.ccHistorical.ID, this.GetThresholdsUnits());

        return js;
    }

    #endregion

    #endregion
}
