<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MacroVariablePicker.ascx.cs" Inherits="Orion_Controls_MacroVariablePicker" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<orion:Include runat="server" File="js/MacroVariablePickerController.js" />
<orion:Include runat="server" File="Admin/js/SearchField.js" />
<style type="text/css">
    #modelSelector {
        margin: 5px 0px 5px 0px;
    }
        
    #sqlModeSelector {
        margin-right: 5px;
    }

    #swqlModeSelector {
        margin-right: 5px;
        margin-left: 40px;
    }

    .selectedVariables .selectedVariable {
         border: 2px solid #ECECEC;
         background: #ECECEC;
     }

    .selectedVariables .left {
        float: left;
    }
    
    .selectedVariables .preView, .selectedVariables .editView{
        padding: 5px 5px 5px 10px;
    }

    .selectedVariables .editView{
        background: #FFFDCC;
    }
   
    .selectedVariables .expanded {
        border-color: #FFE89E;
        background: #FFE89E;
    }
    
    .selectedVariables .greyText{
        color: #696969;
    }
    
    .x-small-text{
        font-size: x-small !important;
    }

    .selectedVariables .empty{
        height: 3px;
        clear: both;
    }

    .selectedVariables .selectedVariable .btn{
        cursor: pointer;
    }
    .boldLabel {
        font-weight: bold;
    }
    .variablePickerTitle {
        margin-top:10px;
        margin-bottom: 10px
    }
    .variablePickerTitle .helpLink {
        vertical-align: middle;
        display: inline-block
    }
    .variablePickerTitle .searchPanel {
        float: right; 
        display: inline-block;
    }
    .selectedVariablesContent {
        float: left;
        padding-bottom: 5px
    }
    .selectVariableButtons {
        text-align: right;
        padding: 15px 0 10px 0
    }
</style>
<script id="variableTemplate" type="text/x-template">
    <div class='selectedVariable left' data-variableid='{{selectedVariable.ID}}'>
        <div class='preView left'>
            <div class='left' style='width:678px;'>
                <span  class='variableFormatPlaceHolder'>{{formatMacroFromVariable(selectedVariable)}}</span>
                <span  class='greyText formattedValuePlaceHolder'>{{selectedVariable.FormattedValue}}</span>
                <%--selectedVariable always has Original data formatter--%>
                {# if (selectedVariable.Formatters && selectedVariable.Formatters.length > 1 ) { #}  
                <span style='margin-left:5px; display:inline-block'><a class="sw-link" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_4) %></a></span>
                {# } #}
            </div>
            <div style='float: right' class='delete btn'><img alt="" src='/Orion/images/delete_icon_gray_16x16.png' /></div> 
        </div>
        <%--selectedVariable always has Original data formatter--%>
        {# if (selectedVariable.Formatters && selectedVariable.Formatters.length > 1 ) { #}  
            <div class='editView' style="clear:both; display:none" >
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_2) %></span>
                <select>
                    {# _.each(selectedVariable.Formatters, function(format) { #} 
                        {# if (selectedVariable.DefaultFormatter  === format.FormatId ) { #}       
                            <option value ='{{format.FormatId}}' selected>{{format.DisplayName}}</option>
                        {# } else { #}
                            <option value ='{{format.FormatId}}'>{{format.DisplayName}}</option>                      
                        {# } #}
                    {# }); #}
                </select>
                <span class='greyText x-small-text'>
                    <span class='originalPlaceHolder'>{{selectedVariable.OriginalValue}}&nbsp;</span>
                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_3) %>&nbsp;</span>
                    <span class='previewPlaceHolder'>{{selectedVariable.FormattedValue}}</span>
                </span> 
                <orion:LocalizableButton runat="server" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_253 %>" DisplayType="Primary" OnClientClick="return false"/>
            </div>
        {# } #}
    </div>
    <div class='empty' ></div>   
</script>

<div runat="server" id="selectVariableControlContainer" style="display: none" >
    <div id="variablePickerHeaderTitle" class="variablePickerTitle">
        <div class="helpLink">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_78) %></span> 
            <span class="LinkArrow">&#0187;</span>
            <orion:HelpLink ID="HelpLink" runat="server" HelpUrlFragment="OrionCoreAG-AlertExampleMessagesUsingVariables" HelpDescription="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VL0_79 %>" CssClass="coloredLink" />
        </div>
        <div id="macroSearchPanel" class="searchPanel"></div>
    </div>   
    <div runat="server" id="macroPickerGrid"></div>
    
    <div id="changeNetObjectSection" style="margin-top: 5px; margin-bottom: 15px; display:none">
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_7) %></span>
        <span id="currentNetObjectPlaceHolder">
        </span>
        <span><a id="changeNetObjectLink" class="sw-link x-small-text" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_6) %></a></span>
        
        <div id="insertVariableNetObjectPickerDlg" style="display: none">
            <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
                <orion:LocalizableButton ID="btnNetObjectChange"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_6 %>" OnClientClick="return false;" />
                <orion:LocalizableButton ID="btnNetObjectCancel"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="return false;" />
            </div>
        </div>
    </div>
    <div id ="simpleMode">
        <div class="boldLabel selectedVariablesContent">
            <span id="selectedVariablesCountHolder"></span>
        </div>
        <div style="float: right">
            <span><a id="removeAllSelections" class="sw-link" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_5) %></a></span>
        </div>  
        <div style="clear: both;"></div>
        <div id ="selectedVariablesPlaceHolder" class="selectedVariables">
        </div>
    </div>
    <div id="queryMode" style="display: none">
        <div class="boldLabel">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_13) %>
        </div>
        <div id="modelSelector" style="display:inline;">
            <input type="radio" id="sqlModeSelector" name="modeSelector" title="SQL" checked="checked"/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_283) %>
            <input type="radio" id="swqlModeSelector" name="modeSelector" title="SWQL"/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_282) %>
        </div>
        <div style="float: right;">
            <%= string.Format(@"<a class='coloredLink' href='{0}' target='_blank'>{1}</a>", SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetSaleForceKBUrl(20489), DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_252))  %>
        </div>

        <asp:TextBox runat="server" Width="99%" TextMode="MultiLine" Rows="6" ID="txtEmailMsessage" data-form="sqlVariable" />
    </div>
    <asp:CheckBox runat="server" CssClass="" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_12 %>" data-form="enableSqlVariable"/>
    <div id="selectVariableButtons" class="selectVariableButtons">
        <orion:LocalizableButton ID="insertVariableBtn" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Primary" runat="server" OnClientClick="return false"/>
        <orion:LocalizableButton ID="cancelBtn" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>" DisplayType="Secondary" runat="server" OnClientClick="return false"/>
    </div>
</div>
<script type="text/javascript">
    (function() {
        SW.Core.MacroVariablePickerController = new SW.Core.MacroVariablePicker({
            containerID: '<%= selectVariableControlContainer.ClientID %>',
            renderTo: '<%= macroPickerGrid.ClientID %>',
            macroContexts: <%= string.IsNullOrEmpty(MacroContextsJson) ? "[]" : MacroContextsJson %>,
            insertVariableButtonID: '<%= insertVariableBtn.ClientID %>',
            cancelButtonID: '<%= cancelBtn.ClientID %>',
            entityType : '<%= EntityType %>'
        });        
    })();
</script>
