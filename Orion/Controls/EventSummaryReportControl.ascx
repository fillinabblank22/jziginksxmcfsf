<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventSummaryReportControl.ascx.cs" Inherits="Orion_Controls_EventSummaryReportControl" %>

<asp:Repeater ID="EventsGrid" runat="server">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="3" class="Events">
        <% if (ShowTableHeader){%>
        <tr>
            <td class="ReportHeader" style="width: 15px;"></td>
            <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_LastXXEvents_CountColumnHeader) %></td>
            <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_LastXXEvents_EventColumnHeader) %></td>
        </tr>
        <%}%>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td style="width: 22px; text-align:center; background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">
                <img src="/NetPerfMon/images/Event-<%# DefaultSanitizer.SanitizeHtml(Eval("EventType")) %>.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_209) %>" >
            </td>
            <td style="width: 10px; font-weight:bold; text-align:center;" ><%# DefaultSanitizer.SanitizeHtml(Eval("Total")) %></td>
            <td>
                <asp:HyperLink ID="HyperLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("URL")) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %></asp:HyperLink></td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
