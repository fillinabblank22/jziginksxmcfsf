﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BaselineDetails.ascx.cs" Inherits="Orion_Controls_BaselineDetails" %>
<%@ Register TagPrefix="orion" TagName="MessageBox" Src="~/Orion/Controls/MessageBox.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomChartControl" Src="~/Orion/Controls/CustomChartControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="TimeFrameTooltip" Src="~/Orion/Controls/TimeFrameTooltip.ascx" %> 

<orion:Include File="BaselineDetails.js"  runat="server" />
<orion:Include File="BaselineDetails.css" runat="server" />

<div id="LatestBaselineDetails" class="baseline baselineDialog">
    <input type="text" id="ThresholdOperator" name="thresholdOperator" class="hidden" />
    <div class="container">
        <orion:TimeFrameTooltip runat="server" id="tftWorkDays" IconUrl="/Orion/images/baselineDay.png" TimeFrameName="Core_WorkHours" />
        <orion:TimeFrameTooltip runat="server" id="tftOffDays" IconUrl="/Orion/images/baselineNight.png" TimeFrameName="Core_NightAndWeekend" />
        <div id="BaselineDetailsErrorDialog" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_38) %><%--Baseline Thresholds Error--%>" class="baseline baselineDialog">
        <div class="container">
            <p  class="section">
                <span class="message"></span>
            </p>     
            <br />                      
            <div class="sw-btn-bar" >
                <orion:LocalizableButtonLink ID="LocalizableButtonLink1" class="close" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TK0_39 %>"
                     onclick="$('#BaselineDetailsErrorDialog').dialog('close');"/>                
            </div> 
        </div>
    </div>
        <span id="HelpLinkUrl" class="hidden"><%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionBaselineDataCalculation")) %></span>
        <div class="leftColumnWrapper">
            <div class="leftColumn">
                <div>
                    <p class="sectionHeader">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_5) %><%--Latest Baseline Graphs--%> <span class="thresholdName"></span>
                    </p> 
                    <div id="Tabs">
                        <ul>
                            <li>
                                <img alt="icon_ocurrences" src="/Orion/images/baselineOccurrences.png" />
                                <a href="#tabOccurrences"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_6) %><%--Occurrences--%></a>
                            </li>    
                            <li>
                                <img alt="icon_metricOverTime" src="/Orion/images/baselineMetricOverTime.png" />
                                <a href="#tabMetricOverTime"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_7) %><%--Metric over Time--%></a>
                            </li>                                                                                                                        
                        </ul>                                
                        <div id="tabOccurrences" class="chartContainer">                        
                            <orion:CustomChartControl runat="server" id="ccArea" LoadOnDemand="true" />
                        </div>
                        <div id="tabMetricOverTime" class="chartContainer">
                            <orion:CustomChartControl runat="server" id="ccHistorical" LoadOnDemand="true" />                                                  
                        </div>                    
                        <input type="text" id="ChartsWidth" name="chartWidth" class="hidden" />
                    </div>
                </div>
                <div class="statistics">
                    <p class="sectionHeader">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_8) %><%--Latest Baseline Statistics--%> <span class="thresholdName"></span>
                    </p> 
                    <table id="tblStatistics">
                        <thead>
                            <tr>
                                <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_9) %><%--TIME PERIOD--%>
                                    <span id="StastisticsTooltip">
                                        <span class="text">
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_10) %><%--A value of--%>&nbsp;<span class="value" id="StastisticsTooltipValue"></span>
                                            <br/><br/>
                                            <div id="lblRecomendedWarning" class="recommended">
                                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_11) %><%--Recommended <span class="warning">Warning</span> Threshold--%>
                                            </div>
                                            <div id="lblRecomendedCritical" class="recommended">
                                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_12) %><%--Recommended <span class="critical">Critical</span> Threshold--%>
                                            </div>                                                                                
                                            <a id="lnkSetWarning">
                                                » <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_13) %><%--Set as <span class="warning">Warning</span> Threshold--%>
                                            </a>                                        
                                            <br/>
                                            <a id="lnkSetCritical">
                                                » <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_14) %><%--Set as <span class="critical">Critical</span> Threshold--%>
                                            </a>
                                        </span>
                                    </span>                                   
                                </th>
                                <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_15) %><%--MIN--%></th>
                                <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_16) %><%--MAX--%></th>
                                <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_17) %><%--STD DEV--%> (σ)</th>
                                <th>-3σ</th>
                                <th>-2σ</th>
                                <th>-1σ</th>
                                <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_18) %><%--MEAN--%></th>
                                <th>1σ</th>
                                <th>2σ</th>
                                <th>3σ</th>
                            </tr>
                        </thead>
                        <tbody>                            
                        </tbody>
                    </table>           
                    <div class="legendContainer">
                        <div class="legend">
                            <div class="warning"></div>&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_19) %><%--Warning Threshold--%>
                        </div>                    
                        <div class="legend">
                            <div class="critical"></div>&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_20) %><%--Critical Threshold--%>
                        </div>    
                    </div>
                    <div class="resetContainer">
                        <a id="ResetToRecommendedThresholds">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_21) %><%--Reset to Recommended Thresholds--%>
                        </a>
                    </div>
                </div>
            </div>      
        </div>            
        <div class="rightColumn">
            <div id="BaselineDetails" class="section">
                <p class="sectionHeader">                       
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_22) %><%--LATEST BASELINE DETAILS--%>
                </p>
                <p>
                    <span class="bold">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_23) %><%--Calculated On:--%>
                    </span>
                    <br/>
                    <span class="calculatedOn"></span>
                </p> 
                <p>
                    <span class="bold">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_24) %><%--Baseline Data Used:--%>
                    </span>
                    <br/>
                    <span  class="dataUsed"></span>
                </p>                                 
                <orion:MessageBox runat="server" id="mbDetailsHint" Type="Hint" Margin="0px 10px" />                                                                                          
            </div>
            <div id="DataForCurrentThresholds" class="section">
                <p class="sectionHeader">                       
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_25) %><%--BASELINE DATA FOR CURRENT THRESHOLDS--%>
                </p> 
                <p>
                    <span class="bold">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_23) %><%--Calculated On:--%>
                    </span>
                    <br/>
                    <span class="calculatedOn"></span>
                </p> 
                <p>
                    <span class="bold">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_24) %><%--Baseline Data Used:--%>
                    </span>
                    <br/>
                    <span class="dataUsed" style="font-size:11px"></span>
                </p> 
            </div>                
            <div id="CurrentThresholdSettings" class="section">
                <p class="sectionHeader">                       
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_40) %><%--CURRENT THRESHOLD SETTINGS--%>
                </p> 
                <div> 
                    <table>
				        <tr class="label">
					        <td>
						        <img src="/Orion/images/StatusIcons/Small-Up-Warn.gif" alt="" />&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_41) %><%--Warning:--%>
					        </td>
                            <td>
                                <span id="SelectedWarningThreshold"></span>
                            </td> 
                        </tr>
				        <tr class="label">
					        <td>
						        <img src="/Orion/images/StatusIcons/Small-Up-Critical.gif" alt="" />&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_42) %><%--Critical:--%>
					        </td>
                            <td>
                                <span id="SelectedCriticalThreshold"></span>
                            </td>
                        </tr>
			        </table>
                    <input type="checkbox" id="ApplyThresholds" name="ApplyThresholds" class="hidden" />
                </div>
            </div>                
        </div>            
        <div class="sw-btn-bar" >                
            <orion:LocalizableButtonLink ID="btnDialogApply" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TK0_36 %>"
                 onclick="$('#LatestBaselineDetails #ApplyThresholds').attr('checked', true); $('#LatestBaselineDetails').dialog('close');"/>
            <orion:LocalizableButtonLink ID="btnDialogClose" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TK0_37 %>"
                 onclick="$('#LatestBaselineDetails #ApplyThresholds').attr('checked', false); $('#LatestBaselineDetails').dialog('close');" />
        </div>    
    </div>
</div>