﻿using System;
using System.Collections;
using System.Web.UI;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using System.Web.UI.WebControls;

public partial class TabsEditorControl : System.Web.UI.UserControl
{
	private readonly TabManager manager = new TabManager();
	private string _accountID;

	public string AccountID
	{
		get { return _accountID; }
		set { _accountID = value; }
	}

    private string GetTabText(string key)
    {
        const string managerid = "Core.Strings";
        const string prefix = "NavigationTab";

        ResourceManagerRegistrar registrar = ResourceManagerRegistrar.Instance;
        string lookupkey = registrar.CleanResxKey(prefix, key);
        return registrar.SearchAll(lookupkey, new[] { managerid }) ?? key; // look up core last, and default to the incoming key.
    }

	protected override void OnInit(EventArgs e)
	{
		AccountID = Request.QueryString["AccountID"];
		
		base.OnInit(e);
	}
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
			InitControls();
	}

	private void InitControls()
	{
	    foreach (Tab tab in manager.GetTabs(AccountID,false))
	    {
	        lbxTabs.Items.Add(new ListItem(GetTabText(tab.TabName),tab.TabId.ToString()));
	    }
 	}

	public void ReorderTabs()
	{
		for (int i = 0; i < lbxTabs.Items.Count; i++)
			manager.SetPositionById(AccountID, int.Parse(lbxTabs.Items[i].Value), i + 1);
	}

	protected void btnMoveTop_Click(object sender, EventArgs e) 
	{
		int[] selection = lbxTabs.GetSelectedIndices();
		ListItem[] selectedItems = new ListItem[selection.Length];
		for (int i = 0; i < selection.Length; i++)
			selectedItems[i] = lbxTabs.Items[selection[i]];

		for (int i = selection.Length - 1; i >= 0; i--)
			lbxTabs.Items.RemoveAt(selection[i]);

		for (int i = 0; i < selectedItems.Length; i++)
			lbxTabs.Items.Insert(i, selectedItems[i]);
	}

	protected void btnMoveUp_Click(object sender, EventArgs e)
	{
		int[] selection = lbxTabs.GetSelectedIndices();
		for (int i = 0; i < selection.Length; i++)
		{
			int selected = selection[i];
			if (selected > i)
			{
				ListItem item = lbxTabs.Items[selected];
				lbxTabs.Items.RemoveAt(selected);
				lbxTabs.Items.Insert(selected - 1, item);
			}
		}
	}

	protected void btnMoveDown_Click(object sender, EventArgs e) 
	{
		int[] selection = lbxTabs.GetSelectedIndices();

		Array.Reverse(selection);
		for (int i = 0; i < selection.Length; i++)
		{
			int selected = selection[i];
			if (selected < lbxTabs.Items.Count - (i + 1))
			{
				ListItem item = lbxTabs.Items[selected];
				lbxTabs.Items.RemoveAt(selected);
				lbxTabs.Items.Insert(selected + 1, item);
			}
		}
	}

	protected void btnMoveBottom_Click(object sender, EventArgs e) 
	{
		int[] selection = lbxTabs.GetSelectedIndices();
		ListItem[] selectedItems = new ListItem[selection.Length];
		for (int i = 0; i < selection.Length; i++)
			selectedItems[i] = lbxTabs.Items[selection[i]];

		for (int i = selection.Length - 1; i >= 0; i--)
			lbxTabs.Items.RemoveAt(selection[i]);

		lbxTabs.Items.AddRange(selectedItems);
	}
}
