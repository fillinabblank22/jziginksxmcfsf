<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SysLogsReportControl.ascx.cs" Inherits="SysLogsReportControl" %>
<orion:Include runat="server" File="legacyevents.css" /><%-- severity css --%>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>

<script type="text/javascript">
    function DemoCheck() {
            <% if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) { %>
    return true;
                    <% } else { %>
    demoAction('Core_Syslog_Acknowledge', this);
    return false;
                    <% } %>
}
    </script>

<table style="width:100%;">
    <tr>
        <td>
            <div class="sw-btn-bar-wizard" id="SyslogActions" runat="server" style="float:left;" >
                            <orion:LocalizableButton ID="LocalizableButton1" DisplayType="Small" LocalizedText="CustomText" runat="server" 
                            Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_AK0_140 %>" OnClientClick="$('.EventCheckBox input:enabled').attr('checked','checked'); return false;"/>
                            <orion:LocalizableButton ID="LocalizableButton2" DisplayType="Small" LocalizedText="CustomText" runat="server" 
                            Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_225 %>" OnClientClick="$('.EventCheckBox input:enabled').removeAttr('checked'); return false;"/>
                            <orion:LocalizableButton ID="LocalizableButton3" DisplayType="Small" LocalizedText="CustomText" 
                            Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_226 %>" runat="server" OnClientClick="return DemoCheck()" OnClick="ClearMessages_Click" />
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width:100%;">
                <tr>
                    <td>
                        <div style="text-align:left; margin: 8px;background-color: #f8f8f8;">
                        <asp:Label runat="server" ID="noSyslogsError" Visible="true" >
                        <table cellpadding="5"><tr><td><table cellpadding="3">
                            <tr>
                                <td><img src="/Orion/images/Icon.Info.gif" alt="Info" />&nbsp;<b><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_227 %></b></td>
                            </tr>
                            <tr>
                                <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_228 %></td>
                            </tr>
                            <tr>
                                <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_229 %><a href="<%= HelpHelper.GetHelpUrl("OrionPHConfiguringRequiredServices") %>" style="color: #336699" target="_blank" rel="noopener noreferrer"><%= Resources.SyslogTrapsWebContent.Punctuation_mark_left_quota %><u><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_230 %></u><%= Resources.SyslogTrapsWebContent.Punctuation_mark_rigth_quota %></a><%= Resources.SyslogTrapsWebContent.Punctuation_mark_period %></td>
                            </tr>
                        </table></td></tr></table>
                        </asp:Label></div>
                        <asp:Repeater runat="server" ID="table">
                            <HeaderTemplate>

                                <table class="Report" cellpadding="0" cellspacing="0" width="100%">
                                   
                                    <thead style="text-transform:uppercase;background-color: #dddcd0;">
                                        <td>&nbsp;</td>
                                        <% for(int i=0; i<Columns.Length; i++) {%>
                                        <td><b><%=GetColumnName(Columns[i])%></b></td>
                                        <%} %>
                                   </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class='Severity<%# DataBinder.Eval(Container.DataItem, "SysLogSeverity").ToString()%>'>
	                                    <asp:Image runat="server" ID="imgSysLog" ImageUrl="~/NetPerfMon/images/ClearEvent.gif" Visible='<%# (DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1" && AllowEventClear) ? true : false%>' />
                                        <asp:CheckBox runat="server" ID="cbSysLog" Visible='<%# (DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() != "1" && AllowEventClear) ? true : false%>' CssClass="EventCheckBox"/>
                                        <asp:HiddenField runat="server" ID="sysLogKey" Value='<%# DataBinder.Eval(Container.DataItem, "MsgID").ToString() %>' />
                                    </td>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr>
                                    <td class='Severity<%# DataBinder.Eval(Container.DataItem, "SysLogSeverity").ToString()%>'>
	                                    <asp:Image runat="server" ID="imgSysLog" ImageUrl="~/NetPerfMon/images/ClearEvent.gif" Visible='<%# (DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() == "1" && AllowEventClear) ? true : false%>' />
                                        <asp:CheckBox runat="server" ID="cbSysLog" Visible='<%# (DataBinder.Eval(Container.DataItem, "Acknowledged").ToString() != "1" && AllowEventClear) ? true : false%>' CssClass="EventCheckBox"/>
                                        <asp:HiddenField runat="server" ID="sysLogKey" Value='<%# DataBinder.Eval(Container.DataItem, "MsgID").ToString() %>' />
                                    </td>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>                        
                </tr>
            </table>                        
        </td>
    </tr>
    <tr>
        <td>
            <div class="sw-btn-bar-wizard" id="Div1" runat="server" style="float:left;" >
                <orion:LocalizableButton ID="locButtonSelect" DisplayType="Small" LocalizedText="CustomText" runat="server" 
                Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_AK0_140 %>" OnClientClick="$('.EventCheckBox input:enabled').attr('checked','checked'); return false;"/>
                <orion:LocalizableButton ID="locButtonDeselect" DisplayType= "Small" LocalizedText="CustomText" runat="server" 
                Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_225 %>" OnClientClick="$('.EventCheckBox input:enabled').removeAttr('checked'); return false;"/>
                <orion:LocalizableButton ID="locClear" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_226 %>" 
                runat="server" OnClientClick="return DemoCheck()" OnClick="ClearMessages_Click" />       
            </div>  
        </td>
    </tr>
</table>