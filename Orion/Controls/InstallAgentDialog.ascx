<%@ Control Language="C#" ClassName="InstallAgentDialog" %>

<%  var printable = false;
    bool.TryParse(Request["Printable"], out printable);
    if (!printable) { %>
<script type="text/javascript">
//<![CDATA[
    var ShowInstallAgentDialog = function (dialogTitle, dialogIcon, dialogContent, detectionInfo, okButtonText, cancelButtonText, onOK, onCancel) {
        dialog = $("#<%= installAgentDialog.ClientID%>").dialog({
            width: 420,
            modal: true,
            open: function () {
                $('#installAgentDialogContent').css('background-image', 'url(' + dialogIcon + ')');
                $('#contentText').html(dialogContent);
            },
            close: function () {
                if (typeof onCancel == "function") {
                    onCancel();
                }
            },
            overlay: { "background-color": "black", opacity: 0.4 },
            title: dialogTitle,
            resizable: false
        });

        $("#<%= OKButton.ClientID %> > span > span").html(okButtonText);
        $("#<%= OKButton.ClientID %>").show();
        $("#<%= CancelButton.ClientID %> > span > span").html(cancelButtonText);
        $("#<%= CancelButton.ClientID %>").show();
        $("#installAgentDialogProgressLabel").hide();

        $("#<%= OKButton.ClientID %>").unbind().click(function () {
            $("#<%= OKButton.ClientID %>").hide();
            $("#<%= CancelButton.ClientID %>").hide();
            $("#installAgentDialogProgressLabel").show();
            onOK(function () {
                dialog.dialog('option', 'close', null);
                dialog.dialog('close');
            });
        });

        if (detectionInfo === '1') {
            $('#detectionInfoWarning').css({ 'display': 'block' });
            $('#detectionInfoWarningText').html("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_AgentInstalledWarning) %>");
        } else if (detectionInfo === '2') {
            $('#detectionInfoWarning').css({ 'display': 'block' });
            $('#detectionInfoWarningText').html("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_DifferentServerWarning) %>");
        } else {
            $('#detectionInfoWarning').css({ 'display': 'none' });
        }

        dialog.show();
    }

    $(function () {
        $("#<%= CancelButton.ClientID %>").click(function () {
            $("#<%= installAgentDialog.ClientID%>").dialog('close');
        });
    });

//]]>
</script>
<% } %>

<% if (!printable) { %>
<div id="installAgentDialog" runat="server" style="display:none;" class="disposable, common-dialog">
    <div id="installAgentDialogContent" class="content" style="padding-left:60px; background: url('/Orion/images/icon.question_32x32.png') top left no-repeat">
        <div id="contentText">
        </div>

        <div id="detectionInfoWarning" class="sw-pg-hint-yellow" style="display:none;">
            <div class="sw-pg-hint-body-warning">
                <span id="detectionInfoWarningText" class="sw-pg-hint-text"></span>
            </div>
        </div>
    </div>

	<div class="bottom">
        <div class="sw-btn-bar-wizard">
            <span id="installAgentDialogProgressLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_InProgressLabel) %></span>
            <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" Text="" DisplayType="Primary" ID="OKButton" onclick="return true;" />
		    <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" Text="" DisplayType="Secondary" ID="CancelButton" onclick="return false;" />
        </div>
	</div>

</div>
<% } %>
