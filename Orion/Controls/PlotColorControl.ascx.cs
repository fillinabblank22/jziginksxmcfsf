using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;

public partial class PlotColorControl : System.Web.UI.UserControl
{
	private string _plotColorField;

	protected void Page_Load(object sender, EventArgs e)
	{
		
	}

	public void SetupControl()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);

		if (!string.IsNullOrEmpty(resource.Properties[_plotColorField]))
		{
			Color = resource.Properties[_plotColorField];
		}
	}

	public string PlotColorField
	{
		get { return _plotColorField; }
		set { _plotColorField = value; }
	}

	public string Color
	{
	    get 
		{
			if (!string.IsNullOrEmpty(CustomPlotColor.Text))
			{
				return CustomPlotColor.Text;
			}
			else
			{
				return PlotColorList.Items[PlotColorList.SelectedIndex].Value;
			}
		}

		set 
		{
			if (PlotColorList.Items.FindByValue(value) != null)
			{
				PlotColorList.SelectedIndex = PlotColorList.Items.IndexOf(PlotColorList.Items.FindByValue(value));
			}
			else
			{
				CustomPlotColor.Text = value;
				PlotColorList.SelectedIndex = PlotColorList.Items.IndexOf(PlotColorList.Items.FindByValue(""));
			}
		}
	}

	public string ControlName
	{
		get { return Name.Text; }
		set { Name.Text = value; }
	}

	public string ControlDescription
	{
		get { return Description.Text; }
		set { Description.Text = value; }
	}
}
