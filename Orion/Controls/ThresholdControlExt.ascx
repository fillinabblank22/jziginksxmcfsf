<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdControlExt.ascx.cs" Inherits="ThresholdControlExt" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Models.Thresholds" %>

<orion:Include ID="Include1" File="ThresholdControl.css" runat="server" />

<script type="text/javascript">

    function ApplyThresholdValuesOf_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(result, warning, critical) {
        if (result == true) {
            if (warning != null && warning != '') {
                $('#<%= lblComputedWarningValue.ClientID %>').html("");
                $('#<%= tbWarningValue.ClientID %>').val(warning);
            }
            if (critical != null && critical != '') {
                $('#<%= lblComputedCriticalValue.ClientID %>').html("");
                $('#<%= tbCriticalValue.ClientID %>').val(critical);
            }

            if (warning != null && warning != '' || critical != null && critical != '') {
                $('#<%= tbWarningValue.ClientID %>').change(); // force start recompute (fire event 'change' on TB)
            }

        }
    }

    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function () {
        threshold_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(false);
    });

    $(document).ready(function () {
        threshold_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(false);
    });

    function threshold_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(suppressRecompute) {

        var controls = {
            override: $('#<%= chbOverrideGeneral.ClientID %>'),
            operator: $('#<%= ddlOperator.ClientID %>'),
            warning: {
                enabled: $('#<%= chbWarningEnabled.ClientID %>'),
                value: $('#<%= tbWarningValue.ClientID %>'),
                computed: $('#<%= lblComputedWarningValue.ClientID %>'),
                sustained: {
                    select: $('#<%= ddlWarnPolls.ClientID %>'),
                    polls: $('#<%= tbWarnPoll.ClientID %>'),
                    interval: $('#<%= tbWarnInterval.ClientID %>'),
                    tOutOf: $('#<%= lOutOfWarn.ClientID %>'),
                    tPolls: $('#<%= lPollsWarn.ClientID %>')
                }
            },
            critical: {
                enabled: $('#<%= chbCriticalEnabled.ClientID %>'),
                value: $('#<%= tbCriticalValue.ClientID %>'),
                computed: $('#<%= lblComputedCriticalValue.ClientID %>'),
                sustained: {
                    select: $('#<%= ddlCritPolls.ClientID %>'),
                    polls: $('#<%= tbCritPoll.ClientID %>'),
                    interval: $('#<%= tbCritInterval.ClientID %>'),
                    tOutOf: $('#<%= lOutOfCrit.ClientID %>'),
                    tPolls: $('#<%= lPollsCrit.ClientID %>')
                }
            }
        };

        if (controls.override.is(':checked')) {
            $.each([
                controls.warning.sustained,
                controls.critical.sustained
            ], function (i, sustained) {

                if (sustained.polls.val() == '1' && sustained.interval.val() == '1') {
                    sustained.select.val("SinglePoll");
                    singlePollHiding.call(sustained);

                } else if (sustained.polls.val() != '1' && sustained.interval.val() == sustained.polls.val()) {
                    sustained.select.val("XPolls");
                    xPollHiding.call(sustained);

                } else {
                    sustained.select.val("XoutOfYPolls");
                    xOutOfYPollHiding.call(sustained);
                }
            });
        }

        if (!isMultiEdit() && !isGlobal() && !suppressRecompute) {
            recompute();
        }

        function isMultiEdit() {
            return (("<%= this.IsMultiedit %>" == "True") ? true : false);
        }

        function isGlobal() {
            return (("<%= this.IsGlobal %>" == "True") ? true : false);
        }

        function getObjectsIDs() {
            var objectIdsString = "<%= this.ObjectIDs %>";

            var objectIds = objectIdsString.split(" ");
            for (var i = 0; i < objectIds.length; i++) {
                objectIds[i] = +objectIds[i];
            }

            return objectIds;
        }

        function clearErrorMessage() {
            var sectionErrorMessage = $('#<%= sectionErrorMessage.ClientID %>');
            var hfIsValid = $('#<%= hfIsValid.ClientID %>');

            hfIsValid.val("1");
            sectionErrorMessage.html('');
            sectionErrorMessage.hide();
        }

        function clearWarningMessage() {
            var sectionWarningMessage = $('#<%= sectionWarningMessage.ClientID %>');

            sectionWarningMessage.hide();
        }

        function handleWarning(warningMessage) {
            var sectionWarningMessage = $('#<%= sectionWarningMessage.ClientID %>');

            sectionWarningMessage.html(warningMessage);
            sectionWarningMessage.show();
        }

        function handleError(errorMessages) {
            var sectionErrorMessage = $('#<%= sectionErrorMessage.ClientID %>');
            var hfIsValid = $('#<%= hfIsValid.ClientID %>');

            hfIsValid.val("0");
            var errorMessage = "";
            var allMessageSame = true;

            for (var i = 0; i < errorMessages.length - 1; i++) {
                if (errorMessages[i] !== errorMessages[i + 1]) {
                    allMessageSame = false;
                    break;
                }
            }

            if (allMessageSame) {
                errorMessage = errorMessages[0];
            } else {
                for (var i = 0; i < errorMessages.length; i++) {
                    errorMessage = errorMessage + errorMessages[i];

                    if (i < errorMessages.length - 1) {
                        errorMessage = errorMessage + "<br/><br/>";
                    }
                }
            }

            sectionErrorMessage.html(errorMessage);
            sectionErrorMessage.show();
        }

        function handleMultieditError(errorMessages, invalidInstances) {
            var sectionErrorMessage = $('#<%= sectionErrorMessage.ClientID %>');
            var errorMessage = "";

            for (var i = 0; i < errorMessages.length; i++) {
                errorMessage = errorMessage + errorMessages[i];

                if (i < errorMessages.length - 1) {
                    errorMessage = errorMessage + "<br/><br/>";
                }
            }

            sectionErrorMessage.html(errorMessage);
            sectionErrorMessage.show();
        }

        function setComputedValue(ctrls, html) {
            var enabled = ctrls.enabled[0].checked;
            ctrls.computed.html(enabled ? html : "");
        }

        function recompute() {

            clearErrorMessage();
            clearWarningMessage();

            var hfIsValid = $('#<%= hfIsValid.ClientID %>');
            hfIsValid.val("2");

            var hfInvalidInstances = $('#<%= hfInvalidInstances.ClientID %>');

            var operatorId = controls.operator.val().split("|")[0];

            var thresholds = {};
            $.each({
                warning: controls.warning,
                critical: controls.critical
            }, function (key, ctrls) {
                var result = {
                    Enabled: ctrls.enabled[0].checked,
                    Formula: ctrls.value.val(),
                };
                switch (ctrls.sustained.select.val()) {
                    case "SinglePoll":
                        result.Sustained = {
                            Polls: '1',
                            Interval: '1'
                        };
                        break;
                    case "XPolls":
                        result.Sustained = {
                            Polls: ctrls.sustained.polls.val(),
                            Interval: ctrls.sustained.polls.val()
                        };
                        break;
                    case "XoutOfYPolls":
                        result.Sustained = {
                            Polls: ctrls.sustained.polls.val(),
                            Interval: ctrls.sustained.interval.val()
                        };
                        break;
                    default:
                        console.error("Selected undefined polls type: " + ctrls.sustained.select.val());
                        break;
                }
                thresholds[key] = result;
            });

            toggleThesholdControls(controls.warning, false);
            toggleThesholdControls(controls.critical, false);

            var request = {
                ThresholdName: "<%= this.ThresholdName %>",
                InstancesId: getObjectsIDs(),
                Operator: operatorId,
                Warning: thresholds.warning,
                Critical: thresholds.critical
            };

            SW.Core.Services.callController("/api/Thresholds/ComputeExt", request, function (response) {

                toggleThesholdControls(controls.warning);
                toggleThesholdControls(controls.critical);

                if (isMultiEdit() == false) {
                    setComputedValue(controls.warning, "?");
                    setComputedValue(controls.critical, "?");
                }

                if (response.IsValid == false) {
                    if (isMultiEdit() == true) {
                        hfIsValid.val("1");
                        hfInvalidInstances.val(response.InvalidInstances);
                        handleMultieditError(response.ErrorMessages);
                    } else {
                        handleError(response.ErrorMessages);
                    }
                } else {
                    hfIsValid.val("1");
                    hfInvalidInstances.val("");

                    if (response.IsComputed) {
                        setComputedValue(controls.warning, "= " + response.WarningThreshold + " " + "<%= DefaultSanitizer.SanitizeHtml(this.Unit) %>");
                        setComputedValue(controls.critical, "= " + response.CriticalThreshold + " " + "<%= DefaultSanitizer.SanitizeHtml(this.Unit) %>");
                    }

                    if (response.WarningMessage) {
                        handleWarning(response.WarningMessage);
                    }
                }
            });
        }

        function toggleThesholdControls(ctrls, enabled)
        {
            if (typeof (enabled) !== "boolean") {
                enabled = ctrls.enabled[0].checked;
            }

            $.each([
                ctrls.value,
                ctrls.sustained.select,
                ctrls.sustained.polls,
                ctrls.sustained.interval
            ], function (i, ctrl) {
                enabled
                    ? ctrl.removeAttr("disabled").removeAttr("readonly")
                    : ctrl.attr("disabled", true).attr("readonly", "readonly");
            });
        }

        function toggleSustainedControls(sustained, fixInterval) {
            switch (sustained.select.val()) {
                case "SinglePoll":
                    singlePollHiding.call(sustained);
                    break;
                case "XPolls":
                    xPollHiding.call(sustained);
                    break;
                case "XoutOfYPolls":
                    xOutOfYPollHiding.call(sustained);
                    // prevent immediate validation error
                    fixInterval && sustained.interval.val(sustained.polls.val());
                    break;
                default:
                    console.error("Selected undefined polls type: " + sustained.select.val());
                    break;
            }
        }

        function singlePollHiding() {
            this.polls.hide().val('1');
            this.interval.hide().val('1');
            this.tOutOf.hide();
            this.tPolls.hide();
        }

        function xPollHiding() {
            this.polls.show();
            this.interval.hide().val(this.polls.val());
            this.tOutOf.hide();
            this.tPolls.show();
        }

        function xOutOfYPollHiding() {
            this.polls.show();
            this.interval.show();
            this.tOutOf.show();
            this.tPolls.show();
        }

        $('#<%= chbOverrideMultipleObjects.ClientID %>').click(function () {

            var chbOverrideGeneral = $('#<%= chbOverrideGeneral.ClientID %>');
            var sectionThresholdDefinition = $('#<%= sectionThresholdDefinition.ClientID %>');
            var sectionOverrideGeneral = $('#<%= sectionOverrideGeneral.ClientID %>');

            if ($(this).is(':checked')) {
                chbOverrideGeneral.prop("disabled", false);
                sectionOverrideGeneral.show();
                sectionThresholdDefinition.show();
            } else {
                chbOverrideGeneral.prop("disabled", true);
                sectionOverrideGeneral.hide();
                sectionThresholdDefinition.hide();
            }
        });

        controls.override.click(function () {
             
            var sectionGeneralThresholds = $('#<%= sectionGeneralThresholds.ClientID %>');
            var sectionCustomThresholds = $('#<%= sectionCustomThresholds.ClientID %>');

            if ($(this).is(':checked')) {
                toggleSustainedControls(controls.warning.sustained, false);
                toggleSustainedControls(controls.critical.sustained, false);
                sectionGeneralThresholds.hide();
                sectionCustomThresholds.show();
                recompute();
            } else {
                sectionGeneralThresholds.show();
                sectionCustomThresholds.hide();
                clearErrorMessage();
                clearWarningMessage();
            }
        });

        controls.operator.change(function () {
            var lblOperatorText = $('#<%= lblOperatorText.ClientID %>');
            lblOperatorText.html($(this).find("option:selected").text());
            recompute();
        });

        controls.warning.sustained.select.change(function () {
            toggleSustainedControls(controls.warning.sustained, true);
            recompute();
        });

        controls.warning.enabled.change(function () {
            toggleThesholdControls(controls.warning);
            recompute();
        });

        controls.critical.sustained.select.change(function () {
            toggleSustainedControls(controls.critical.sustained, true);
            recompute();
        });

        controls.critical.enabled.change(function () {
            toggleThesholdControls(controls.critical);
            recompute();
        });

        $.each([
            controls.warning,
            controls.critical
        ], function (i, ctrls) {
            $.each([
                ctrls.value,
                ctrls.sustained.polls,
                ctrls.sustained.interval
            ], function (i, elm) {
                elm.change(function () { recompute(); });
            });
        });

        $('#<%= btnUseLatestBaseline.ClientID %>').click(function (e) {

            controls.warning.value.val("${USE_BASELINE_WARNING}");
            controls.critical.value.val("${USE_BASELINE_CRITICAL}");

            if (isMultiEdit()) {
                controls.warning.computed.html("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB2_1) %>");
                controls.critical.computed.html("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB2_1) %>");
            }

            recompute();
        });

        $('#<%= hlShowDetailDialog.ClientID %>').click(function (e) {

            var operatorId = parseInt(controls.operator.val().split("|")[0]);
            var instanceId = getObjectsIDs()[0];

            SW.Core.Baselines.showBaselineDetailsDialog({
                id: instanceId,
                thresholdName: "<%= this.ThresholdName %>",
                thresholdDisplayName: "<%= DefaultSanitizer.SanitizeHtml(this.DisplayName) %>",
                operator: operatorId,
                returnFunction: ApplyThresholdValuesOf_<%= this.ThresholdName.Replace(".","_")%>
                <% if(!string.IsNullOrWhiteSpace(StatisticsFormatterFunction)) { %>
                , statisticsFormatter: <%= StatisticsFormatterFunction %>
                <% } %>
            });
        });
    }
</script>

<asp:HiddenField runat="server" ID="hfIsValid" Value="1"/>
<asp:HiddenField runat="server" ID="hfInvalidInstances"/>

<table class="thresholds">
    <tr>
        <td class="leftLabelColumn" style="text-align: left;">
            <asp:CheckBox runat="server" ID="chbOverrideMultipleObjects"/><%= DefaultSanitizer.SanitizeHtml(this.DisplayName) %>
        </td>
        <td class="rightInputColumn">
            <div runat="server" ID="sectionOverrideGeneral">
                <asp:CheckBox Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TK0_45  %>" runat="server" ID="chbOverrideGeneral" /> <%--Override Orion General Thresholds --%>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px; padding-right: 20px;">
            <div runat="server" ID="sectionThresholdDefinition" class="thresholdControl">
                <table width="100%">
                    <tr>
                        <td>
                            <div ID="sectionGeneralThresholds" runat="server" class="thresholdGlobal">
                                <table>
                                    <tr>
                                        <td style="width: 30px">
                                            <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>" src="/Orion/images/ThresholdControl/Small-Up-Warn.gif" />
                                        </td>
                                        <td style="width: 180px">
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>: <%-- Warning --%>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblGeneralWarningValue"></asp:Label> <%-- greater than --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>" src="/Orion/images/ThresholdControl/Small-Up-Critical.gif" />
                                        </td>
                                        <td>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>: <%-- Critical --%>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblGeneralCriticalValue"></asp:Label> <%-- greater than --%>
                                        </td>
                                    </tr>
                                    <%-- Global Capacity Planning --%>
                                    <tr id="capacityPlanningGlobal" runat="server">
                                        <td>
                                            <img src="/Orion/images/ForecastingIcons/capPlanIcon24x16.gif" alt="Capacity Trending Icon" />
                                        </td>
                                        <td>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_11) %> <%-- Capacity Trending --%>
                                        </td>
                                        <td>
                                            <asp:Literal ID="capacityPlanningGlobalValue" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div ID="sectionCustomThresholds" runat="server" class="thresholdCustom" style="width: 100%; display: none;">
                                <table style="width: 100%;" cellspacing="10">
                                    <tr style="height: 30px;">
                                        <td style="width: 18px"><asp:CheckBox runat="server" ID="chbWarningEnabled" automation="chbWarningEnabled" /></td>
                                        <td style="width: 18px"><img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>" src="/Orion/images/ThresholdControl/Small-Up-Warn.gif" /></td>
                                        <td style="width: 140px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>:</td>
                                        <td style="width: 180px;"><asp:DropDownList cssclass="ddlInTable" runat="server" ID="ddlOperator" automation="ddlOperator" /></td>
                                        <td style="width: 225px; white-space: nowrap;"><asp:TextBox runat="server" ID="tbWarningValue" CssClass="formulaTextbox" automation="tbThresholdValue"></asp:TextBox><%= DefaultSanitizer.SanitizeHtml(this.Unit) %></td>
                                        <td style="width: 225px; white-space: nowrap;">
                                            <asp:Label runat="server" ID="lblFor" style="margin-left: 10px; margin-right: 10px;" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_label_for %>"></asp:Label>
                                            <asp:DropDownList CssClass="ddlInTable" runat="server" ID="ddlWarnPolls" automation="ddlPolls_Warning">
                                                <asp:ListItem Value="SinglePoll" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_ddl_singlepoll %>"></asp:ListItem>
                                                <asp:ListItem Value="XPolls" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_ddl_xpolls %>"></asp:ListItem>
                                                <asp:ListItem Value="XoutOfYPolls" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_ddl_xoutofypolls %>"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox runat="server" ID="tbWarnPoll" CssClass="pollsTextbox" automation="tbPolls_Warning"></asp:TextBox>
                                            <asp:Label runat="server" ID="lOutOfWarn" style="margin-left: 10px;" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_label_outof %>"></asp:Label>
                                            <asp:TextBox runat="server" ID="tbWarnInterval" CssClass="pollsTextbox" automation="tbInterval_Warning"></asp:TextBox>
                                            <asp:Label runat="server" ID="lPollsWarn" style="margin-left: 10px;" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_label_polls %>"></asp:Label>
                                        </td>
                                        <td ID="tdComputedWarningValue" runat="server"><asp:Label runat="server" ID="lblComputedWarningValue"></asp:Label></td>
                                        <td rowspan="2" style="width: 280px;">
                                            <div runat="server" ID="sectionLatestBaseline" class="relatedActionBlock">
                                                <table>
                                                    <tr>
                                                        <td><asp:Button runat="server" ID="btnUseLatestBaseline" UseSubmitBehavior="False" OnClientClick="return false;" CssClass="button sw-btn sw-btn-primary" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TK0_48  %>" ></asp:Button></td><%-- Use Dynamic Baseline Thresholds--%>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div runat="server" ID="sectionDetailHyperlink">
                                                                <asp:HyperLink runat="server" ID="hlShowDetailDialog" Text="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_49) %>"></asp:HyperLink><%--» Latest Baseline Details--%>
                                                            </div>
                                                            <div class="info">
                                                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_50) %> <a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionCoreAGBaselineDataCalculation")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_51) %></a>  <%-- Thresholds calculated using baseline data allow for more accurate alerting. --%> <%-- » Learn more --%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="height: 30px;">
                                        <td><asp:CheckBox runat="server" ID="chbCriticalEnabled" automation="chbCriticalEnabled" /></td>
                                        <td><img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>" src="/Orion/images/ThresholdControl/Small-Up-Critical.gif" /></td>
                                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>:</td>
                                        <td><asp:Label runat="server" ID="lblOperatorText"></asp:Label></td>
                                        <td style="white-space: nowrap;"><asp:TextBox runat="server" ID="tbCriticalValue" CssClass="formulaTextbox" automation="tbThresholdValue"></asp:TextBox><%= DefaultSanitizer.SanitizeHtml(this.Unit) %></td>
                                        <td style="width: 225px; white-space: nowrap;">
                                            <asp:Label runat="server" ID="Label1" style="margin-left: 10px;margin-right: 10px;" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_label_for %>"></asp:Label>
                                            <asp:DropDownList CssClass="ddlInTable" runat="server" ID="ddlCritPolls" automation="ddlPolls_Critical">
                                                <asp:ListItem Value="SinglePoll" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_ddl_singlepoll %>"></asp:ListItem>
                                                <asp:ListItem Value="XPolls" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_ddl_xpolls %>"></asp:ListItem>
                                                <asp:ListItem Value="XoutOfYPolls" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_ddl_xoutofypolls %>"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox runat="server" ID="tbCritPoll" CssClass="pollsTextbox" automation="tbPolls_Critical"></asp:TextBox>
                                            <asp:Label runat="server" ID="lOutOfCrit" style="margin-left: 10px;" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_label_outof %>"></asp:Label>
                                            <asp:TextBox runat="server" ID="tbCritInterval" CssClass="pollsTextbox" automation="tbInterval_Critical"></asp:TextBox>
                                            <asp:Label runat="server" ID="lPollsCrit" style="margin-left: 10px;" Text="<%$ HtmlEncodedResources: CoreWebContent, ThresholdPolls_label_polls %>"></asp:Label>
                                        </td>
                                        <td><asp:Label runat="server" ID="lblComputedCriticalValue"></asp:Label></td>
                                    </tr>
                                    <%-- Local Capacity Planning --%>
                                    <tr id="capacityPlanning" class="capacityPlanningRow" runat="server">
                                        <td>&nbsp;</td>
                                        <td><img src="/Orion/images/ForecastingIcons/capPlanIcon24x16.gif" alt="Capacity Trending Icon" /></td>
                                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_11) %>: <%-- Capacity Trending --%></td>
                                        <td colspan="4">
                                            <asp:RadioButton ID="capPlanTypeAverage" Text="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_09) %>" GroupName="capPlanTypeGroup" runat="server" />
                                            &nbsp;
                                            <asp:RadioButton ID="capPlanTypePeak" Text="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_10) %>" GroupName="capPlanTypeGroup" runat="server" />
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div runat="server" id="sectionErrorMessage" style="overflow-y: auto; max-height:200px; width: 700px; display: none;" class="sw-suggestion sw-suggestion-fail"></div>
                            <div runat="server" id="sectionWarningMessage" style="width: 700px; display: none;" class="sw-suggestion sw-suggestion-warn"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>