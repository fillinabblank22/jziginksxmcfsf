using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

using SolarWinds.Logging;
using SolarWinds.Orion.Web.SyslogTrapInterfaces;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class FacilityControl : System.Web.UI.UserControl, IFacilityControl
{
	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private bool _addShowAllItem = true;

    public bool AddShowAllItem
    {
        get { return _addShowAllItem; }
        set { _addShowAllItem = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string DataForm { get; set; }

    protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public string FacilityCode
	{
		get { return facilities.SelectedValue; }
		set { facilities.SelectedValue = value; }
	}

	public string FacilityName
	{
		set
		{
			foreach (ListItem item in facilities.Items)
			{
				if (item.Text.Equals(value, StringComparison.OrdinalIgnoreCase))
				{
					facilities.SelectedValue = item.Value;
					break;
				}
			}
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        if (!string.IsNullOrEmpty(DataForm))
            facilities.Attributes.Add("data-form", DataForm);

		string selectedvalue = string.Empty;
		if (!string.IsNullOrEmpty(FacilityCode))
		{
			selectedvalue = FacilityCode;
		}

		facilities.Items.Clear();
        if (_addShowAllItem)
            facilities.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_184, string.Empty));

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			StringDictionary facilDictionary = proxy.GetFacilities();

			foreach (string facilKey in facilDictionary.Keys)
			{
				facilities.Items.Add(new ListItem(facilDictionary[facilKey], facilKey));
			}
		}

		FacilityCode = selectedvalue;
	}
}
