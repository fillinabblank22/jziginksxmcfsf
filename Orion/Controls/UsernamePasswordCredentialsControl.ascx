﻿<%@ control language="C#" autoeventwireup="true" codefile="UsernamePasswordCredentialsControl.ascx.cs" inherits="Orion_Controls_UsernamePasswordCredentialsControl" %>

<orion:include runat="server" file="js/UsernamePasswordCredentialsControl.js" />
<orion:include runat="server" file="styles/UsernamePasswordCredentialsControl.css" />

<asp:panel runat="server" defaultbutton="btnTestCredentials">

<asp:HiddenField runat="server" ID="hfCustomData" />

<table id="usernamePasswordCredentials-<%= this.JavaScriptInstanceNameSuffix %>" class="usernamePasswordCredentials">
    

    <%-- User name rows --%>
    <tr>
        <td class="leftLabelColumn">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_59) %>:
        </td>
        <td class="rightInputColumn">
            <asp:TextBox runat="server" ID="tbUserName" data-form="userName"  Width="250" />
            <span id="userNameError-<%= this.JavaScriptInstanceNameSuffix %>" class="sw-validation-error"></span>
            <asp:CustomValidator runat="server" ID="cvUsername" ValidateEmptyText="True" ControlToValidate="tbUserName"></asp:CustomValidator>
        </td>
    </tr>
    <%
        if (!string.IsNullOrWhiteSpace(this.UserNameHint))
        {
    %>
    <tr>
        <td>&nbsp;</td>
        <td class="helpfulText">
            <%= DefaultSanitizer.SanitizeHtml(this.UserNameHint) %>
        </td>
    </tr>
    <%
                }
    %>
    
    <%-- Password rows --%>
    <tr>
        <td class="leftLabelColumn">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_8) %>:
        </td>
        <td class="rightInputColumn">
            <asp:TextBox ID="tbPassword" TextMode="Password" runat="server" data-form="password" AutoCompleteType="Disabled" Width="250" />
            <span id="passwordError-<%= this.JavaScriptInstanceNameSuffix %>" class="sw-validation-error"></span>
            <asp:CustomValidator runat="server" ID="cvPassword" ValidateEmptyText="True" ControlToValidate="tbPassword"></asp:CustomValidator>
        </td>
    </tr>
    <%
        if (!string.IsNullOrWhiteSpace(this.PasswordHint))
        {
    %>
    <tr>
        <td>&nbsp;</td>
        <td class="helpfulText">
            <%= DefaultSanitizer.SanitizeHtml(this.PasswordHint) %>
        </td>
    </tr>
    <%
        }
    %>
    
    <%-- Test button and validation row --%>
    <tr>
        <td class="leftLabelColumn"></td>
        <td class="rightInputColumn">
            <orion:LocalizableButton runat="server" ID="btnTestCredentials" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_62 %>" DisplayType="Small" />
            <div id="testProgress-<%= this.JavaScriptInstanceNameSuffix %>" class="sw-suggestion sw-suggestion-info" style="display: none;">
                <span class="testProgress sw-suggestion-icon"></span>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_8) %>
            </div>
            <div id="validationMessageBox-<%= this.JavaScriptInstanceNameSuffix %>" style="display: none;">
                <span id="validationMessageBoxIcon-<%= this.JavaScriptInstanceNameSuffix %>" class="sw-suggestion-icon"></span>
                <span id="validationMessageBoxText-<%= this.JavaScriptInstanceNameSuffix %>" style="white-space: pre-line"></span>
                <div class="centeredDiv">
                    <orion:LocalizableButton runat="server" ID="validationMessageBoxButton" DisplayType="Small" OnClick="validationMessageBoxButton_OnClick"  />
                </div>
                <input type="hidden" ID="buttonActionKey" runat="server"/>
                <%if (!HideFailedTestDetailsLink)
                  {
                %>
                  <a id="debugDetails-<%= this.JavaScriptInstanceNameSuffix %>" class="debugDetails"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_105) %></a>
                <%
                  }
                %>
                <div id="failureDebugTooltip-<%= this.JavaScriptInstanceNameSuffix %>" class="failureDebugTooltip sw-suggestion sw-suggestion-nostyle"></div>
            </div>
            <input type="hidden" id="tbCredentialsId" data-form="credentialsId" runat="server"/>
            <input type="hidden" id="tbCredentialsRelationUse" data-form="credentialsRelationUse" runat="server" />
        </td>
    </tr>
</table>
</asp:panel>
