﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Orion_Controls_DateTimePicker : UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool UseScriptManager { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool EnableDateTimeValidation { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool EnableEmptyValidation { get; set; }

    public bool ForceMarkupClientScriptBlockUsage { get; set; } // RenderControl wants this

	public string ValidationGroup { get; set; }

    protected string ClientScriptString = @"(function($) {
    var regionalSettings = " +
                     DatePickerRegionalSettings.GetDatePickerRegionalSettings() +
                     @"
    $.fn.orionGetDate = function() {
        var datepicker = $('.datePicker', this);
        var timepicker = $('.timePicker', this);
        var datepart = $.datepicker.parseDate(regionalSettings.dateFormat, datepicker.val());
        var timepart = timepicker[0].timePicker.getTime();
        return new Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());
    };

    $.fn.orionSetDate = function(date) {
        $('.datePicker', this).val($.datepicker.formatDate(regionalSettings.dateFormat, date));
        var time = date.getTime(); // timePicker.setTime screws up the object. preserve it for our poor caller
        $('.timePicker', this)[0].timePicker.setTime(date);
        date.setTime(time);
    };

    $.fn.validateTime=function(oSrc, args) {        
        var dtRegex;
        var timeSep =  regionalSettings.timeSeparator == '.' ? '\\.' : regionalSettings.timeSeparator;
        
        var normTimeString;
        if(regionalSettings.show24Hours)
            normTimeString = '^(?:[01]?[0-9]|2[0-3])' + timeSep + '[0-5][0-9]$';
        else
            normTimeString = '^(?:[0]?[1-9]|1[0-2])' + timeSep + '[0-5][0-9]( AM| PM| a\\.m\\.| p\\.m\\.)?$';

        dtRegex = new RegExp(normTimeString);
        args.IsValid = dtRegex.test(args.Value);
    };

    $.fn.isValid = function()
    {
        return $('.sw-validation-error:visible', this).length == 0;
    };

	$(function() {
		$.datepicker.setDefaults(regionalSettings);
		$('.datePicker').datepicker({mandatory: true, closeAtTop: false});
		$('.timePicker').timePicker({separator:regionalSettings.timeSeparator, show24Hours:regionalSettings.show24Hours});         
	});
})(jQuery);";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!ForceMarkupClientScriptBlockUsage)
        {
            if (UseScriptManager)
                ScriptManager.RegisterStartupScript(this, GetType(), "datePickerDefaults", ClientScriptString, true);
            else
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "datePickerDefaults", ClientScriptString,
                    true);
        }
        this.dateValidator.Enabled = this.EnableDateTimeValidation;
        this.emptyDateValidator.Enabled = this.emptyTimeValidator.Enabled = this.EnableEmptyValidation;

	    if (!string.IsNullOrEmpty(ValidationGroup))
	    {
		    this.dateValidator.ValidationGroup = ValidationGroup;
		    this.emptyDateValidator.ValidationGroup = ValidationGroup;
		    this.timeValidator.ValidationGroup = ValidationGroup;
		    this.emptyTimeValidator.ValidationGroup = ValidationGroup;
	    }
    }

    public DateTime Value
    {
        get
        {
            DateTime value;
            if (DateTime.TryParse(txtDatePicker.Text + " " + txtTimePicker.Text, out value))
                return value;
            else
                return DateTime.MinValue;
        }

        set
        {
            txtDatePicker.Text = value.ToShortDateString();
            txtTimePicker.Text = value.ToShortTimeString();
        }
    }   

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool TimePickerVisibility
    {
        get { return txtTimePicker.Visible; }
        set { txtTimePicker.Visible = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool DatePickerVisibility
    {
        get { return txtDatePicker.Visible; }
        set { txtDatePicker.Visible = value; }
    }

    public string TimeValidatorClientId => timeValidator.ClientID;
}
