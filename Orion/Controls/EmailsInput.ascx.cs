using System;
using System.Web.UI;

public partial class Orion_Controls_EmailsInput : UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public string DataForm { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool MultipleMode { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool AllowMacros { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (MultipleMode)
            txtEmail.Attributes.Add("class", "emailInputField multiple");
        emailsString.Attributes.Add("data-form", DataForm);
        if (AllowMacros)
            emailsString.Attributes.Add("AllowMacros", AllowMacros.ToString());
    }

    public string Value {
        get { return emailsString.Value; }
        set { emailsString.Value = value; }
    }

    public string ToolTip
    {
        get
        {
            var titleAttribute = EmailPickerControl.Attributes["title"];
            return titleAttribute != null ? null : EmailPickerControl.Attributes["title"];
        }
        set { EmailPickerControl.Attributes["title"] = value; }
    }
}