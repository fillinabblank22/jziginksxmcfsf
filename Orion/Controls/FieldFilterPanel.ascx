﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FieldFilterPanel.ascx.cs" Inherits="Orion.Controls.FieldFilterPanel" %>

<div id="leftpanel_filter" style="width: <%= CollapsedWidth() %>px;">
    <div class="expand_collapse_title <%= CollapsedCssName() %> <%= NeedTextRotate ? "needrotate" : "" %>"><span id="labelPanelTitle"><%: TitleText %></span><img ClientIDMode="Static" id="imgPanelTitle" class="sw-ffp-titleimg" runat="server" Visible="false" EnableViewState="false" /></div>
    <div class="expand_collapse_btn <%= CollapsedCssName() %>" title="Collapse" <%= IsCollapsible ? "" : "style='display: none;'" %>></div>
    
    <div id="top_action_links"><a id="linkApplyTopLink" class="ffp-link" <%= CollapsedStyleTag() %>><span id="labelApplyLink"><%: Resources.CoreWebContent.FFP_ApplyFilter %></span></a></div>
    
    <div id="key_list" <%= DefaultSanitizer.SanitizeHtml(CollapsedStyleTag()) %>></div>

    <div id="bottom_action_links" <%= DefaultSanitizer.SanitizeHtml(CollapsedStyleTag()) %>>
        <table>
            <tr>
                <td colspan="2">
                    <a id="linkAddFilterProperty" class="ffp-link"><span id="labelAddProperty"><%: Resources.CoreWebContent.FFP_AddFilterProperties %></span></a>
                </td>
            </tr>
            <tr>
                <td>
                    <orion:LocalizableButtonLink DisplayType="Secondary" ID="linkApplyButton" ClientIDMode="Static" Text="<%$ HtmlEncodedResources:CoreWebContent,FFP_ApplyFilter %>" href="#" onclick="return false;" LocalizedText="CustomText"  runat="server" />
                </td>
                <td>
                    <a id="linkClearAll" class="ffp-link"><span id="labelClearLink"><%: Resources.CoreWebContent.FFP_ClearAll %></span></a>
                </td>
            </tr>
        </table>
    </div>
</div>

<div style="position:absolute; left:-1000px; top:-1000px;">
<div id="fieldValuePickerDialog" ClientIDMode="Static" runat="server" style="background-color: #fff !important;">
    
    <div style="padding: 6px 0 8px 0;"><span class="sw-fvp-intro"></span></div>
        
    <div class="sw-pg-holder" style="padding: 0; margin: 0; height: 550px;"></div>

    <div class="sw-btn-bar" style="white-space: nowrap; float: right; margin-top: 16px;">
        <orion:LocalizableButtonLink ID="fvpOk" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="Apply" />
        <orion:LocalizableButtonLink ID="fvpCancel" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" style="margin-right: 0;" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>" />
    </div>
</div>
</div>
