﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Nodes_Controls_PasswordTextBox : System.Web.UI.UserControl
{
    public string AsteriskMask = "**********";
    [PersistenceMode(PersistenceMode.Attribute)]
    public event EventHandler TextChanged;
    public bool AutoPostBack { get; set; }

    public bool IsEnabledPasswordTextBox
    {
        get { return PasswordBox.Enabled; }
        set
        {
            PasswordBox.Enabled = value;
            RemovePassword.Enabled = value;
        }
    }

    private string PasswordKey
    {
        get
        {
            if (ViewState[PasswordBox.ClientID] != null)
            {
                return Convert.ToString(ViewState[PasswordBox.ClientID]);
            }
            return null;
        }
        set
        {
            ViewState[PasswordBox.ClientID] = value;
        }
    }

    public Unit Width
    {
        get { return PasswordBox.Width; }
        set { PasswordBox.Width = value; }
    }

    public string Text
    {
        get
        {
            if (PasswordBox.Text.Equals(AsteriskMask))
                return PasswordKey;
            return PasswordBox.Text;
        }
        set
        {
            PasswordKey = value;
            if (!string.IsNullOrEmpty(PasswordBox.Text) || (IsEnabledPasswordTextBox && !string.IsNullOrEmpty(PasswordKey)))
            {
                PasswordBox.Text = AsteriskMask;
                PasswordBox.Attributes.Add("value", AsteriskMask);
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (AutoPostBack)
        {
            PasswordBox.AutoPostBack = AutoPostBack;
            PasswordBox.TextChanged += (sender, args) => TextChanged(sender, args);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            PasswordBox.Attributes.Add("onclick", "clearPasswordBox('" + PasswordBox.ClientID + "')");
            PasswordBox.Attributes.Add("onblur", "setPasswordBoxValue('" + PasswordBox.ClientID + "','" + AsteriskMask + "')");
            RemovePassword.Attributes.Add("onclick", "return clearPasswordKey('" + PasswordBox.ClientID + "', '" + Resources.CoreWebContent.WEBDATA_AY0_107 + "')");
        }

    }
}