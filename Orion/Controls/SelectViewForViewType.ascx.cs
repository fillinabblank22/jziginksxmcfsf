using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Controls_SelectViewForViewType : ProfilePropEditUserControl
{
	private bool _allowViewsByDeviceType;

	public bool AllowViewsByDeviceType
	{
		get { return _allowViewsByDeviceType; }
		set { _allowViewsByDeviceType = value; }
	}

	private string _viewType;

	public string ViewType
	{
		get { return _viewType; }
		set { _viewType = value; }
	}

	#region ProfilePropEditUserControl members
	public override string PropertyValue
	{
		get
		{
			return this.lbxViewPicker.SelectedValue;
		}
		set
		{
			ListItem item = this.lbxViewPicker.Items.FindByValue(value);
			if (null != item)
			{
				item.Selected = true;
			}
			else
			{
				if (_allowViewsByDeviceType)
				{
					// default to "by device type"
					this.lbxViewPicker.Items.FindByValue("-1").Selected = true;
				}
				else
				{
					// default to "Default"
					this.lbxViewPicker.Items.FindByValue("-2").Selected = true;
				}
			}
		}
	}
	#endregion

	protected override void OnInit(EventArgs e)
	{
		if (_allowViewsByDeviceType)
			this.lbxViewPicker.Items.Add(new ListItem("By Device Type", "-1"));

		foreach (ViewInfo info in ViewManager.GetViewsByType(_viewType))
		{
			ListItem item = new ListItem(info.ViewTitle, info.ViewID.ToString());
			this.lbxViewPicker.Items.Add(item);
		}

		base.OnInit(e);
	}
}
