﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimeZonePicker.ascx.cs" Inherits="Orion_Controls_TimeZonePicker" %>

<span id="<%= ClientID %>">
    <asp:DropDownList runat="server" ID="timeZonePicker" DataTextField="DisplayName" DataValueField="OffsetCouple" Width="500px" />
</span>

<script type="text/javascript">
    //<![CDATA[
    (function ($) {
        var timeZonePicker = document.getElementById('<%= timeZonePicker.ClientID %>');

        $.fn.getSelectedOffsetCouple = function () {
            var selectedOffsets = timeZonePicker.options[timeZonePicker.selectedIndex].value.split("|");
            return {
                summer: selectedOffsets[0],
                winter: selectedOffsets[1]
            };
        };

        $.fn.setSelectedOffsetCouple = function (offsetCouple) {
            var offsetCoupleString = String.format("{0}|{1}", offsetCouple.summer, offsetCouple.winter);
            for (let i = 0; i < timeZonePicker.options.length; i++) {
                if (timeZonePicker.options[i].value == offsetCoupleString) {
                    timeZonePicker.options[i].selected = true;
                    $(timeZonePicker).change();
                    break;
                }
            }
        };

        $(function () {
            $(timeZonePicker).change(function () {
                let timeZonePickerWrapper = document.getElementById('<%= ClientID %>');
                timeZonePickerWrapper.title = this.options[this.selectedIndex].innerText;
            });
        });
    })(jQuery);
    //]]>
</script>