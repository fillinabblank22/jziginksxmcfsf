﻿using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Controls_DynamicControlsContainer : System.Web.UI.UserControl
{
    protected static readonly Log log = new Log();

    /// <summary>
    /// String of space separated CSS classes to pass onto the element
    /// </summary>
    public string CssClasses { get; set; }

    /// <summary>
    /// Key name from WebSettings table to load controls paths from
    /// </summary>
    public string WebSettingsKey { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(WebSettingsKey))
        {
            throw new InvalidOperationException(nameof(WebSettingsKey) + " property must be set");
        }

        var controlPaths = GetControlPaths(WebSettingsKey);
        LoadControls(controlPaths);

        Controls.Attributes.Add("class", CssClasses);
    }

    private void LoadControls(IEnumerable<string> controlPaths)
    {
        foreach (var controlPath in controlPaths)
        {
            try
            {
                var control = Page.LoadControl(controlPath);

                Controls.Controls.Add(control);
            }
            catch (Exception exception)
            {
                log.Error(string.Format("Failed to load '{0}' control", controlPath), exception);
            }
        }
    }

    private IEnumerable<string> GetControlPaths(string webSettingsKey)
    {
        string controlPathString;
        IEnumerable<string> controlPaths =
            WebSettingsDAL.TryGetValue(webSettingsKey, out controlPathString)
                ? controlPathString.Split('|')
                : new string[] { };

        return controlPaths;
    }
}