<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PeriodControl.ascx.cs" Inherits="PeriodControl" %>

<!--table border="1" width="100%"-->
<tr>

<% if( !InLine ) { %>
        <td align="justify" style="font-weight:bold;" >
<% } else { %>
        <td colspan="3">
        <p align="justify"  >
            <div style="font-weight:bold; white-space: nowrap">
<% } %>

    <asp:Label runat="server" ID="periodLabel" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_40 %>" />

<% if( !InLine ) { %>
    </td>
    <td class="formRightInput">
<% } else { %>
    </div>
    <div>
<% } %>

    <asp:DropDownList ID="PeriodList" runat="server" >
    </asp:DropDownList>
    <asp:CustomValidator ID="CustomValidator1" runat="server" OnServerValidate="ValidateNumberOfPoints" Display="None"></asp:CustomValidator>
<% if( !InLine ) { %>
        </td>
        <td class="formHelpfulTxt">&nbsp;
        </td>
<% } else { %>
            </div>
        </p>
    </td>
<% } %>

</tr>
<!--/table-->