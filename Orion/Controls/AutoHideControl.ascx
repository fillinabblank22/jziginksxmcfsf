<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoHideControl.ascx.cs" Inherits="AutoHideControl" %>

<% if( !InLine ) { %>
    <tr><td style="width: 24em;">
<% } %>
    <div style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_310) %></div>
<% if( !InLine ) { %>
    </td><td style="width: 10em;">
<% } else { %>
    <div class="sw-text-helpful">
        <%= DefaultSanitizer.SanitizeHtml(Description) %>
    </div>
    <div style="padding: 6px 0;">
<% } %>

    <asp:RadioButton runat = "server" ID = "autoHideYes" GroupName="autoHideRadio" Text = "<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_124 %>" Checked="false" /> <br />
    <asp:RadioButton runat = "server" ID = "autoHideNo" GroupName="autoHideRadio" Text = "<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_125 %>" Checked="false" /> 

<% if( !InLine ) { %>
    </td>
    <td class="formHelpfulTxt">
        <%= DefaultSanitizer.SanitizeHtml(Description) %>
</td>
</tr>
<% } else { %>
    </div>
<% } %>
