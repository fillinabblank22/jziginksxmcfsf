<%@ Control Language="C#" ClassName="InstallAgentProgress" %>

<%  var printable = false;
    bool.TryParse(Request["Printable"], out printable);
    if (!printable) { %>
<script type="text/javascript">
//<![CDATA[
        ShowInstallAgentProgress = function (progressTitle, agentId, nodeCaption, callback, onClose) {

            dialog = $("#<%= installAgentProgress.ClientID%>").dialog({
                width: 450,
                modal: true,
                close: function(){
                    if (typeof onClose == "function") {
                        onClose();
                    }
                },
                overlay: { "background-color": "black", opacity: 0.4 },
                title: progressTitle,
                resizable: false
            });
            dialog.show();
            UpdateInstallAgentProgress(agentId, nodeCaption, callback);
        }

        UpdateInstallAgentProgress = function (agentId, nodeCaption, callback) {
            params = JSON.stringify({
                agentId: agentId
            });
            $.ajax({
                type: 'POST',
                url: '/Orion/Services/AgentManagerService.asmx/GetDeployingStatus',
                data: params,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    var deploymentInfo = result.d;
                    if (deploymentInfo === null) {
                        Close();
                        callback(agentId, nodeCaption, { Status: 2 , StatusMessage: 'status not found' });
                    } else if ((deploymentInfo.StatusInfo.Status == 1) || (deploymentInfo.StatusInfo.Status == 2) || (deploymentInfo.StatusInfo.Status == 3)) {
                        //finished or failed or pending reboot
                        Close();
                        callback(agentId, nodeCaption, deploymentInfo.StatusInfo);
                    } else {
                        //all other states are interpreted as 'in progress'
                        UpdateStatus(deploymentInfo);
                        if ($("#<%= installAgentProgress.ClientID%>").dialog('isOpen') === true)
                            setTimeout(function () { UpdateInstallAgentProgress(agentId, nodeCaption, callback) }, 500);
                        //else ??? probably nothing to do
                    }
                },
                error: function (error) {
                    Close();
                    callback(agentId, nodeCaption, { Status: 2, StatusMessage: 'Installing agent failed' });
                }
            });

        }

        CloseInstallAgentProgress = function () {
            Close();
        }

        Close = function () {
            $("#<%= installAgentProgress.ClientID%>").dialog('close');
        }

        UpdateStatus = function (status) {
            $("#progressTextStage").text(status.StageMessage);
            //$("#progressTextStatus").text(status.StatusMessage);

            if (status.Progress >= 0) {
                $("#progressText").removeAttr('style');
                $("#progressBar").css({ 'display': 'block' });

                $("#progressBar").progressbar({
                    value: status.Progress
                });
                $("#progressBar > div").css({ 'background': '#ffac00' });
                $("#progressBar > div").css({ 'border-color': '#6b6b6b' });
                $("#progressBar > div").css({ 'border-radius': '0px 0px 0px 0px' });
                $("#progressBar").css({ 'background': '#6b6b6b' });
                $("#progressBar").css({ 'border-color': '#6b6b6b' });
                $("#progressBar").css({ 'border-radius': '0px 0px 0px 0px' });
            } else {
                $("#progressBar").css({ 'display': 'none' });

                $("#progressText").css({ 'background': 'url(/Orion/images/animated_loading_sm3_whbg.gif) top left no-repeat' });
                $("#progressText").css({ 'padding-left': '32px' });
                $("#progressText").css({ 'height': '28px' });
                $("#progressText").css({ 'line-height': '28px' });
            }
        }
//]]>
</script>
<% } %>


<% if (!printable) { %>
<div id="installAgentProgress" runat="server" style="display:none;" class="disposable, common-dialog">
    <div id="progressInfo" style="padding: 40px 30px 40px 30px;">
        <div id="progressBar" style="height:20px; margin-bottom: 5px;"></div>
        <div id="progressText">
            <span id="progressTextStage" ></span>
        </div>
    </div>

	<div>
        <span class="LinkArrow">&#0187;</span>
        <a href="/Orion/AgentManagement/Admin/ManageAgents.aspx"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_AMSLinkText) %></a>
	</div>
</div>
<% } %>
