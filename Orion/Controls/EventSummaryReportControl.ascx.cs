using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Federation;

public partial class Orion_Controls_EventSummaryReportControl : System.Web.UI.UserControl
{
	private Log _log = new Log();
    private readonly List<SwisErrorMessage> errors = new List<SwisErrorMessage>();

    public List<SwisErrorMessage> Errors
    {
        get { return this.errors; }
    }

    public bool ShowTableHeader
    {
        get { return ((DataTable)EventsGrid.DataSource).Rows.Count > 0; }
    }

	protected void BLExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

   public void LoadData(int netObjectID, string netObjectType, string period)
    {

        try
        {
            DataTable eventsTable = new DataTable();

            // get period interval
            DateTime periodBegin = new DateTime();
            DateTime periodEnd = new DateTime();

            string periodTemp = period;
            Periods.Parse(ref periodTemp, ref periodBegin, ref periodEnd);

            eventsTable = EventDAL.GetEventSummaryTable(netObjectID, netObjectType, periodBegin, periodEnd);
            this.errors.AddRange(SwisDataTableParser.GetDataTableErrors(eventsTable));

            // sort by Total
            eventsTable.DefaultView.Sort = eventsTable.Columns["Total"] + " DESC";

            // adds new column to the table for URL resolving
            eventsTable.Columns.Add("URL");

            foreach (DataRow r in eventsTable.Rows)
            {
                StringBuilder url = new StringBuilder("/Orion/NetPerfMon/Events.aspx?");

                if (netObjectID > 0) url.AppendFormat("NetObject={0}:{1}", netObjectType.Trim(), netObjectID);
                url.AppendFormat("&Period={0}", period);
                url.AppendFormat("&EventType={0}", r["EventType"]);

                r["URL"] = url.ToString();
                int color = (int) r["BackColor"];
                r["BackColor"] = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
            }

            // data bind
            LoadData(eventsTable);                                                                       

        }
        catch (Exception ex)
        {
            BLExceptionHandler(ex);
            throw;
        }
    }

    public void LoadData(DataTable eventsTable)
    {
        if (eventsTable != null)
        {
            // data bind
            this.EventsGrid.DataSource = eventsTable;
            this.EventsGrid.DataBind();
        }
    }
}
