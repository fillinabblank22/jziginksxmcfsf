﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Reporting.Models.Selection;

public partial class Orion_Controls_DataSourcePicker : UserControl
{
    public DataSource[] DataSources {
        get
        {
            if (Session[String.Format("DataSource-{0}", WorkflowGuid)] != null)
            {
                var serializer = new DataContractJsonSerializer(typeof(DataSource[]));
                return (DataSource[])serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes((string)Session[String.Format("DataSource-{0}", WorkflowGuid)])));
            }
            return null;
        }
        set
        {
            var serializer = new DataContractJsonSerializer(typeof(DataSource[]));
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, value);
                Session[String.Format("DataSource-{0}", WorkflowGuid)] = Encoding.UTF8.GetString(ms.ToArray());
            }

        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string WorkflowGuid
    {
        get
        {
            if (ViewState["DataSourceGuid"] == null)
                ViewState["DataSourceGuid"] = Guid.NewGuid().ToString();
            return (string)ViewState["DataSourceGuid"];
        }
        set
        {
            ViewState["DataSourceGuid"] = value;
            dataSourcesSessionIdentifier.Value = String.Format("DataSource-{0}", value);
        }
    }

    public bool SwisDataSourceOnly
    {
        get { return SwisFederationInfo.IsFederationEnabled; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dataSourcesSessionIdentifier.Value = String.Format("DataSource-{0}", WorkflowGuid);
    }

    public string GetNetObjectId() {
        var netObjectId = this.Request.QueryString["NetObject"] as string;
        return string.IsNullOrEmpty(netObjectId) ? string.Empty : netObjectId;
    }
}