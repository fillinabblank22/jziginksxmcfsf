using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.InformationService;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web.CPE;
using InformationServiceProxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy;

public partial class Orion_Controls_EditCustomPropertyValueControl : UserControl
{
    private Dictionary<string, object> _viewContextConfiguration;

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ObjectType
    {
        get
        {
            if (ViewState[string.Format("ObjectType_{0}", ClientID)] == null)
            {
                ViewState[string.Format("ObjectType_{0}", ClientID)] = typeof(string);
            }
            return (string)ViewState[string.Format("ObjectType_{0}", ClientID)];
        }
        set
        {
            ViewState[string.Format("ObjectType_{0}", ClientID)] = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string PropertyName
    {
        get
        {
            if (ViewState[string.Format("PropertyName_{0}", ClientID)] == null)
            {
                ViewState[string.Format("PropertyName_{0}", ClientID)] = typeof(string);
            }
            return (string)ViewState[string.Format("PropertyName_{0}", ClientID)];
        }
        set
        {
            ViewState[string.Format("PropertyName_{0}", ClientID)] = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string PropertyValue
    {
        get
        {
            if (ViewState[string.Format("PropertyValue_{0}", ClientID)] == null)
            {
                ViewState[string.Format("PropertyValue_{0}", ClientID)] = typeof(string);
            }
            return (string)ViewState[string.Format("PropertyValue_{0}", ClientID)];
        }
        set
        {
            ViewState[string.Format("PropertyValue_{0}", ClientID)] = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public Type PropertyType
    {
        get
        {
            if (ViewState[string.Format("PropertyType_{0}", ClientID)] == null)
            {
                ViewState[string.Format("PropertyType_{0}", ClientID)] = typeof(string);
            }
            return (Type)ViewState[string.Format("PropertyType_{0}", ClientID)];
        }
        set
        {
            ViewState[string.Format("PropertyType_{0}", ClientID)] = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string[] EnumerationValues
    {
        get
        {
            if (ViewState[string.Format("EnumerationValues_{0}", ClientID)] == null)
            {
                ViewState[string.Format("EnumerationValues_{0}", ClientID)] = new string[0];
            }
            return (string[])ViewState[string.Format("EnumerationValues_{0}", ClientID)];
        }
        set
        {
            ViewState[string.Format("EnumerationValues_{0}", ClientID)] = value;
        }
    }

    public string Type { get; set; }

    public Dictionary<string, object> ViewContextConfiguration
    {
        set
        {
            _viewContextConfiguration = value;
            SetUpProperties();
            SetUpConfiguration();
        }
    }

    private void SetUpProperties()
    {
        object objectTypeObj;
        object customPropertyNameObj;
        object customPropertyValueObj;
        if (_viewContextConfiguration.TryGetValue("ObjectType", out objectTypeObj))
        {
            ObjectType = objectTypeObj.ToString();
        }
        if (_viewContextConfiguration.TryGetValue("Name", out customPropertyNameObj))
        {
            PropertyName = customPropertyNameObj.ToString();
        }
        PropertyValue = _viewContextConfiguration.TryGetValue("Value", out customPropertyValueObj) ? customPropertyValueObj.ToString() : string.Empty;
        InitCustomProperty();
    }

    public void SetUpProperties(string objectType, string customPropertyName, string customPropertyValue)
    {
        ObjectType = objectType;
        PropertyName = customPropertyName;
        PropertyValue = customPropertyValue;
        InitCustomProperty();
    }

    private void InitCustomProperty()
    {
        var customProperties = PopulateCustomProperties(ObjectType);
        var property =
            customProperties.First(
                cp => string.Equals(cp.PropertyName, PropertyName, StringComparison.InvariantCultureIgnoreCase));
        if (property == null) return;
        PropertyType = property.PropertyType;
        EnumerationValues = property.Values;
    }

    public void SetUpConfiguration()
    {
        RestrictedValuesEditor.Visible = false;
        TextBoxEditor.Visible = false;
        YesNoEditor.Visible = false;
        DateTimeEditor.Visible = false;
        ValueInsertVariable.Visible = false;

        if (EnumerationValues != null && EnumerationValues.Length > 0)
        {
            Type = "enum";
            RestrictedValuesEditor.Visible = true;
            if (RestrictedValues.Items.Count != 0) return;
            foreach (var currentValue in EnumerationValues.Select(val => CustomPropertyHelper.GetCurrentCultureString(val, PropertyType)))
            {
                RestrictedValues.Items.Add(new ListItem(currentValue, currentValue));
            }
            RestrictedValues.SelectedValue = PropertyValue;
        }
        else
        {
            if (PropertyType == typeof(bool))
            {
                Type = "bool";
                YesNoEditor.Visible = true;
                bool pVal;
                bool.TryParse(PropertyValue, out pVal);
                YesNoValue.SelectedValue = (pVal) ? "True" : "False";
            }
            else if (PropertyType == typeof(DateTime))
            {
                Type = "datetime";
                DateTimeEditor.Visible = true;
                DateTime dVal;
                if (!DateTime.TryParse(PropertyValue, out dVal)) return;
                TextBoxDatePickerValue.Text = dVal.ToString("dd/MM/yy");
                TextBoxTimePickerValue.Text = (dVal.Second > 0) ? dVal.ToLongTimeString() : dVal.ToShortTimeString();
            }
            else
            {
                ValueInsertVariable.Visible = true;
                if (PropertyType == typeof(string))
                {
                    Type = "string";
                }
                else if (PropertyType == typeof(int))
                {
                    Type = "int";
                }
                else if (PropertyType == typeof(float))
                {
                    Type = "float";
                }
                else if (PropertyType == typeof(double))
                {
                    Type = "double";
                }
                TextBoxEditor.Visible = true;
                TextBoxEditorValue.Text = CustomPropertyHelper.GetCurrentCultureString(PropertyValue, PropertyType);
            }
        }
    }

    private IEnumerable<CustomProperty> PopulateCustomProperties(string entityType)
    {
        var entity = GetEntityByType(entityType);
        string targetEntity;
        try
        {
            targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(entity.FullName, true);
        }
        catch {
            // unable to get target entity, try to get CPs by source entity
            return CustomPropertyMgr.GetCustomPropertiesForSourceEntity(entity.FullName).ToList();
        }
        return CustomPropertyMgr.GetCustomPropertiesForEntity(targetEntity).ToList();
    }

    private Entity GetEntityByType(string entityType)
    {
        var swisSchemaProvider = new SwisSchemaProvider(new InformationServiceProxyCreator(InformationServiceProxy.CreateV3));
        var entityProvider = new EntityProviderDynamic(swisSchemaProvider);
        var entity = entityProvider.GetEntityByObjectType(entityType);
        return entity;
    }

    protected void Page_Load(object sender, EventArgs e) {  }
}