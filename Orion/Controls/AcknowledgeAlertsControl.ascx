<%@ Control Language="C#" AutoEventWireup="true" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Services/AlertsAdmin.asmx" />
    </Services>
</asp:ScriptManagerProxy>
<div id="AcknowledgeDialog" style="display: none;">
    <iframe frameborder="0" id="acknowledgeAlertsFrame"></iframe>
    <div id="divAcknowledge" style="position: fixed; z-index: 1000;
        width: 100%; left: 0px;">
        <div runat="server" id="divDialog" style="margin-left: auto; margin-right: auto;
            width: 800px; height: 100%; background-color: White;">
            <table width="100%" style="border-bottom-width: 1px; border-bottom-style: solid;
                border-bottom-color: Black; border-top-width: 1px; border-top-style: solid; border-top-color: Black;
                border-left-width: 1px; border-left-style: solid; border-left-color: Black; border-right-width: 1px;
                border-right-style: solid; border-right-color: Black;">
                <tr>
                    <td style="border: 0px; background-image: url('/Orion/images/PopupHeader2Rows.gif');
                        background-repeat: repeat-x;" width="100%">
                        <table style="margin-bottom: 1px;" width="100%">
                            <tr>
                                <td>
                                    <span id="AlertDetailsHeader" style="font-weight:bold; font-size:large;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_151) %></span>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="CloseCross" runat="server" ImageUrl="/Orion/images/Button.CloseCross.gif"
                                        OnClientClick="HideAcknowledgeAlerts(); return false;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>                                
                <tr>
                    <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                        font-size: 8pt;">
                        <span id="NotesLabel" style="font-weight: bold;" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_152) %></span><br />
                        <asp:TextBox ID="Notes" runat="server" Wrap="false" TextMode="MultiLine" Rows="20"
                            Font-Size="8pt" Width="780px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding: 5px;">
                        <div class="sw-btn-bar-wizard">
                            <orion:LocalizableButton runat="server" ID="Ok" LocalizedText="Ok" 
                                DisplayType="Primary" OnClientClick="HideAcknowledgeAlerts(); return false;" />
                            <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel"
                                DisplayType="Secondary" OnClientClick="HideAcknowledgeAlerts(); return false;" />        
                        </div>            
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<script language="javascript" type="text/javascript">
    function getWindowHeight() {
        var windowHeight = 0;
        if (typeof (window.innerHeight) == 'number') {
            windowHeight = window.innerHeight;
        }
        else {
            if (document.documentElement && document.documentElement.clientHeight) {
                windowHeight = document.documentElement.clientHeight;
            }
            else {
                if (document.body && document.body.clientHeight) {
                    windowHeight = document.body.clientHeight;
                }
            }
        }
        return windowHeight;
    }

    function setAcknowledgeDialog() {
        var outY = getWindowHeight();
        var div = document.getElementById('divAcknowledge');
        var inY;
        var nextTop;
        if (div != null) {
            inY = div.offsetHeight;
            nextTop = (parseInt(outY) - parseInt(inY)) / 2;
            // check if IE
            if ($.browser.msie) {
                var offsetParent = div.offsetParent;
                while (offsetParent != null) {
                    nextTop = nextTop + offsetParent.scrollTop;
                    offsetParent = offsetParent.offsetParent;
                }
            }
            div.style.top = nextTop + 'px';
        }
    }

    function ShowAlertAcknowledge() {
        var alerts = new Array();
        $('.AlertCheckBox input[type=checkbox]').each(function () {
            if (this.checked && !this.disabled) {
                var scope = $(this).parents("tr:first");
                var alertValue = $('input[type=hidden]', scope).val();
                //var alertValues = alertValue.split(",");
                alerts.push(alertValue);                
            }
        });
        $('#<%=Ok.ClientID %>').unbind('click');
        $('#<%=Ok.ClientID %>').click(function () {                        
            var text = $('#<%=Notes.ClientID%>').val();            
            AlertsAdmin.UpdateAcknowledgeAlerts(alerts,'<%=HttpContext.Current.Profile.UserName.Replace("\\", "\\\\")%>',text, 
                        function(result) {
                             HideAcknowledgeAlerts();
                             <%=Page.ClientScript.GetPostBackEventReference(this, "MyCustomArgument")%>;
                        },
                        function(ex) {
                            alert(ex.get_message());
                             HideAcknowledgeAlerts();
                        }
                        );
        });
        $('#AcknowledgeDialog').show();
        setAcknowledgeDialog();
        $('#<%=Notes.ClientID %>').val('');
    }

    function HideAcknowledgeAlerts() {
        $('#AcknowledgeDialog').hide();        
    }

    $('#acknowledgeAlertsFrame').attr('style', 'position: fixed; width: 100%; height: 100%; top:0px; bottom:0px; left:0px; right:0px; overflow:hidden; padding:0; margin:0; background-color: Black; opacity: 0.5; -moz-opacity: 0.5;  filter: alpha(opacity=50); z-index: 1000;');
    
    window.onresize = function () { setAcknowledgeDialog(); }
</script>
<!--[if IE]>
    <script type="text/javascript">
        window.onscroll = function() { setAcknowledgeDialog(); }
    </script>
<![endif]-->