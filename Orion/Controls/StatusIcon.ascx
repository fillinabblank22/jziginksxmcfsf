<%@ Control Language="C#" ClassName="StatusIcon" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>

<script runat="server">
    public Status StatusValue
    {
        get
        {
            if (null == ViewState["StatusValue"])
            {
                return new Status(OBJECT_STATUS.Unknown);
            }

            return (Status)ViewState["StatusValue"];
        }

        set
        {
            ViewState["StatusValue"] = value;
        }
    }

    protected string ImagePath
    {
        get
        {
            return this.StatusValue.ToString("imgpath", null);
        }
    }
</script>

<img class="StatusIcon" alt="<%= DefaultSanitizer.SanitizeHtml(StatusValue.ToLocalizedString()) %>" src="<%= DefaultSanitizer.SanitizeHtml(ImagePath) %>" />