<%@ Control Language="C#" ClassName="Reposter" EnableViewState="false" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>

<script runat="server">
    protected override void OnPreRender(EventArgs e)
    {
		string reloadScript = string.Format(@"
var orionPageRefreshTimeout=setTimeout('__doPostBack({1}, {2})', {0});
var orionPageRefreshMilSecs = {0};", WebSettingsDAL.AutoRefreshSeconds * 1000, "\"__Page\"", "\"\"");

		this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "timeout", reloadScript, true);
        base.OnPreRender(e);
    }
</script>
