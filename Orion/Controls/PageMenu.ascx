<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageMenu.ascx.cs" Inherits="Orion_Controls_PageMenu" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<orion:Include runat="server" File="PageMenu.css" />

<div class="PageMenu">
    <asp:Panel ID="panTitle" runat="server">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
        <asp:Image ID="imgTitle" runat="server" />
    </asp:Panel>
    <asp:Menu ID="PageMenu" SkipLinkText="" runat="server" OnMenuItemClick="PageMenu_Clicked" RenderingMode="Table" >
        <StaticMenuStyle CssClass="PageMenu" />
        <StaticSelectedStyle CssClass="SelectedMenuItem" />
        <StaticMenuItemStyle CssClass="MenuItem"/>
    </asp:Menu>
</div>