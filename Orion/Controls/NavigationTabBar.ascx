<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationTabBar.ascx.cs" 
    Inherits="Orion_Controls_NavigationTabBar" %>
    
<style type="text/css">
.sw-tabs .x-tab-strip-text { font-size: 13px !important; }
.tab-top 
{
    padding:1em;
}
ul.x-tab-strip {
    width:100%!important;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
  $(".sw-tabs li.sw-tab").bind('mouseover', function(){ $(this).addClass('x-tab-strip-over'); });
  $(".sw-tabs li.sw-tab").bind('mouseout', function(){ $(this).removeClass('x-tab-strip-over'); });
});
</script>

<div class="sw-tabs"><div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain">

<div class="x-tab-strip-wrap">
  <%--   <div class="x-tab-strip-spacer" style="display: none;"><!-- --></div>  --%>

  <ul class="x-tab-strip x-tab-strip-top">

    <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />

    <li class="x-tab-edge"></li>
    <div class="x-clear"><!-- --></div>
  </ul>

  <div class="x-tab-strip-spacer" style="border-bottom: none;"><!-- --></div>
</div>

</div></div>

    
