﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using SolarWinds.Data.Query;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Licensing;
using SolarWinds.Orion.Core.Common.Settings;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Security;
using SolarWinds.Orion.Web.Helpers;

/// <summary>
/// Summary description for GoogleTagManager
/// </summary>
public partial class Orion_Controls_GoogleTagManager : System.Web.UI.UserControl
{
    private static readonly Log Log = new Log();

    protected bool OipInstalled = false;
    protected bool CollectingDisabled = false;
    protected bool IsExternal = false;
    protected bool IsAdmin = false;
    protected bool GtmEnabled = false;
    protected string gaAccountKey;
    protected string orionPath = string.Format("Software\\{0}SolarWinds\\Orion\\Improvement", IntPtr.Size == 8 ? "Wow6432Node\\" : string.Empty);
    protected string installerPath = string.Format("Software\\{0}SolarWinds\\InstallerNumber", IntPtr.Size == 8 ? "Wow6432Node\\" : string.Empty);
    protected string oipUserID = string.Empty;
    protected string bootstrapperID = string.Empty;
    protected string modules;
    protected string licenseTypes = string.Empty;
    protected string moduleElementCounts;
    protected string moduleInstallDates;
    protected string userHash;

    private string emptyJsArray = "[]";

    private bool Authenticated => !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name);

    protected override void OnInit(EventArgs e)
    {
        using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(orionPath))
        {
            if (key != null)
            {
                OipInstalled = (int)key.GetValue("OptIn", 0) != 0;
                CollectingDisabled = (int)key.GetValue("StopCollecting", 0) != 0;
                IsExternal = ((string)key.GetValue("IsExternal", "")).Equals("True", StringComparison.OrdinalIgnoreCase);
                gaAccountKey = (string)key.GetValue("AccountKey", "UA-1803473-5");
            }
        }

        using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(installerPath))
        {
            if (key != null)
            {
                bootstrapperID = (string)key.GetValue("InstallerNumber", "");
            }
        }

        oipUserID = SolarWinds.Orion.Web.DAL.WebSettingsDAL.MainPollerOipUserId;
        GtmEnabled = GtmSettings.Instance.GtmEnabled;

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GtmEnabled && OipInstalled && !CollectingDisabled)
        {
            IsAdmin = GetAdminStatus();
            licenseTypes = Authenticated ? GetModuleLicenses() : emptyJsArray;
            modules = Authenticated ? GetModuleVersions() : emptyJsArray;
            moduleElementCounts = Authenticated ? GetModuleElementCounts() : emptyJsArray;
            moduleInstallDates = Authenticated ? GetModuleInstallDates() : emptyJsArray;

            userHash = AnonymizationHelper.AnonymizeUserName(
                HttpContext.Current.User.Identity.Name,
                SolarWinds.Orion.Web.DAL.WebSettingsDAL.GetValue("OipSalt", oipUserID));
        }
    }

    private bool GetAdminStatus()
    {
        try
        {
            return (bool)HttpContext.Current.Profile.GetPropertyValue("AllowAdmin");
        }
        catch (System.Configuration.SettingsPropertyNotFoundException)
        {
            //user is not authenticated, therefore not an admin
            return false;
        }
    }

    private string GetModuleVersions()
    {
        var moduleVersions = (string)Cache.Get("GA_InstalledModules");
        if (string.IsNullOrEmpty(moduleVersions))
        {
            var moduleBuilder = new StringBuilder();
            JsonSerializer.CreateDefault().Serialize(new JsonTextWriter(new StringWriter(moduleBuilder)),
                ModulesCollector.GetInstalledModules().Select(module => new Dictionary<string, string> { { module.ProductTag, module.Version } }).ToArray());

            moduleVersions = moduleBuilder.ToString().Replace(@"""", @"\""");
            Cache.Insert("GA_InstalledModules", moduleVersions, null, DateTime.UtcNow.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        return moduleVersions;
    }

    private string GetModuleLicenses()
    {
        var licenses = (string)Cache.Get("GA_LicenseTypes");
        if (string.IsNullOrEmpty(licenses))
        {
            var licenseTypeBuilder = new StringBuilder();
            JsonSerializer.CreateDefault().Serialize(new JsonTextWriter(new StringWriter(licenseTypeBuilder)),
                ModuleLicenseInfoProvider.GetModuleLicenseInformation().Select(module => new Dictionary<string, string> { { module.ModuleName + "_LicenseType", module.IsEval ? "Evaluation" : "Licensed" } }).ToArray());

            licenses = licenseTypeBuilder.ToString().Replace(@"""", @"\""");
            Cache.Insert("GA_LicenseTypes", licenses, null, DateTime.UtcNow.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        return licenses;
    }

    private string GetModuleElementCounts()
    {
        var moduleElementData = (string)Cache.Get("GA_ModuleElementCounts");
        if (string.IsNullOrEmpty(moduleElementData))
        {
            try
            {
                DataTable elementCounts;
                using (var factory = new SwisConnectionProxyFactory())
                using (var swis = factory.CreateSwisConnection())
                {
                    elementCounts =
                        swis.Query("SELECT ElementType, ElementCount, MaxCount FROM Orion.LicenseSaturation");
                }

                var moduleElementInfoStorage = new Dictionary<string, string>();
                foreach (var module in ModulesCollector.GetInstalledModules())
                {
                    var moduleElementTypes = module.ElementCountQueries.ToDictionary(
                        q => string.IsNullOrEmpty(q.ElementDisplayName) ? q.ElementType : q.ElementDisplayName,
                        q => q.ElementType);

                    foreach (var row in elementCounts?.Select(
                                            "ElementType IN ('" + string.Join("','", moduleElementTypes.Keys) + "')") ??
                                        new DataRow[0])
                    {
                        moduleElementInfoStorage.Add(
                            $"{module.ProductTag}_{moduleElementTypes[row["ElementType"].ToString()]}_ElementCount",
                            row["ElementCount"].ToString());
                        moduleElementInfoStorage.Add(
                            $"{module.ProductTag}_{moduleElementTypes[row["ElementType"].ToString()]}_ElementMax",
                            row["MaxCount"].ToString());
                    }
                }

                var moduleElementsBuilder = new StringBuilder();
                JsonSerializer.CreateDefault().Serialize(new JsonTextWriter(new StringWriter(moduleElementsBuilder)),
                    new[] { moduleElementInfoStorage });

                moduleElementData = moduleElementsBuilder.ToString().Replace(@"""", @"\""");
                Cache.Insert("GA_ModuleElementCounts", moduleElementData, null, DateTime.UtcNow.AddHours(12),
                    System.Web.Caching.Cache.NoSlidingExpiration);
            }
            catch (Exception e)
            {
                Log.Error("Unable to get element counts for modules", e);
                return emptyJsArray;
            }
        }
        return moduleElementData;
    }

    private string GetModuleInstallDates()
    {
        var moduleInstallDates = WebSecurityHelper.SanitizeHtmlV2((string)Cache.Get("GA_ModuleInstallDates"));

        if (string.IsNullOrEmpty(moduleInstallDates))
        {
            try
            {
                var moduleInstallDataBuilder = new System.Text.StringBuilder();
                JsonSerializer.CreateDefault().Serialize(new JsonTextWriter(new StringWriter(moduleInstallDataBuilder)),
                    new[] { new SolarWinds.Orion.Web.DAL.ConfigWizardLogDAL().GetModuleFirstInstallDates()
                    .ToDictionary(m=>m.ModuleName.Replace(" ", "") + "_InstallDate",
                        m=>m.InstallDate.Date.ToString("yyyy-MM-dd")) });
                moduleInstallDates = WebSecurityHelper.SanitizeHtmlV2(moduleInstallDataBuilder.ToString()).Replace(@"""", @"\""");

                Cache.Insert("GA_ModuleInstallDates", moduleInstallDates, null, DateTime.UtcNow.AddHours(12),
                    System.Web.Caching.Cache.NoSlidingExpiration);
            }
            catch (Exception e)
            {
                Log.Error("Unable to get install dates for modules", e);
                return emptyJsArray;
            }
        }
        return moduleInstallDates;
    }
}
