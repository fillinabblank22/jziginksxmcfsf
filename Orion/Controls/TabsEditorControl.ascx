<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TabsEditorControl.ascx.cs" Inherits="TabsEditorControl" %>

<td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_291) %></td>
<td>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table>
                <tr valign="top">
                    <td>
                        <asp:ListBox runat="server" ID="lbxTabs" Width="140px" Rows="9" SelectionMode="multiple"></asp:ListBox>
                    </td>
                    <td class="viewColumnButtons">
                        <asp:ImageButton ID="btnMoveTop" runat="server" ImageUrl="~/NetPerfMon/images/Button.MoveTop.gif" OnClick="btnMoveTop_Click" /><br />
		                <asp:ImageButton ID="btnMoveUp" runat="server" ImageUrl="~/NetPerfMon/images/Button.MoveUp.gif" OnClick="btnMoveUp_Click" /><br />
		                <asp:ImageButton ID="btnMoveDown" runat="server" ImageUrl="~/NetPerfMon/images/Button.MoveDown.gif" OnClick="btnMoveDown_Click" /><br />
		                <asp:ImageButton ID="btnMoveBottom" runat="server" ImageUrl="~/NetPerfMon/images/Button.MoveBottom.gif" OnClick="btnMoveBottom_Click" /><br />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</td>
<td></td>