﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Reporting.Models.Selection;

public partial class Orion_Controls_AddContentLoader : System.Web.UI.UserControl
{
    private const string DummyJsFunction = "function(){}";

    private string viewType;

    public string ViewType
    {
        get { return viewType; }
        set {
            viewType = value;
            if (myResPicker != null)
            {
                myResPicker.ViewType = value;
            }
        }
    }

    public string WorkflowGuid
    { 
        get
        {
            if (ViewState["AddContentLoaderrGuid"] == null)
                ViewState["AddContentLoaderGuid"] = Guid.NewGuid().ToString();
            return (string) ViewState["AddContentLoaderGuid"];
        }
        set
        {
            ViewState["AddContentLoaderGuid"] = value;
            DataSourcePicker.WorkflowGuid = value;
        }
    }

    public DataSource[] DataSources
    {
        get { return DataSourcePicker.DataSources; }
        set { DataSourcePicker.DataSources = value; }
    }
    
    public string OnSelectedJs { get; set; }
    public string OnCreatedJs { get; set; }
    public bool SingleMode
    {
        get { return myResPicker.SingleMode; }
        set { myResPicker.SingleMode = value; }
    }


    public Orion_Controls_AddContentLoader()
    {
        OnSelectedJs = DummyJsFunction;
        OnCreatedJs = DummyJsFunction;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(ViewType))
            myResPicker.ViewType = ResourceTemplateGroupsProvider.ReportingViewType;

    }
}