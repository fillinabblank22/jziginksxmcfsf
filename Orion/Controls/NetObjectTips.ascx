<%@ Control Language="C#" ClassName="NetObjectTips" %>

<orion:Include runat="server" File="jquery/jquery.hoverIntent.js" />
<orion:Include runat="server" File="jquery/jquery.cluetip.js" />
<orion:Include runat="server" File="jquery/jquery.tooltip.js" />
<orion:Include runat="server" File="NetObjectTips.js" />

<orion:Include runat="server" File="js/jquery/jquery.cluetip.css" />
<orion:Include runat="server" File="js/jquery/jquery.tooltip.css" />
<orion:Include runat="server" File="NetObjectTipsAll.css" />

<script type="text/javascript">
//<![CDATA[
$(function(){
    SW.Core.NetObjectTips.Init({
        ViewLimitationID: <%= SolarWinds.Orion.Web.Limitation.GetCurrentViewLimitationID() %>,
        NetObjectTypeToTipPagePathRaw: <%= ControlHelper.ToJSON(NetObjectFactory.GetNetObjectTipMap()) %>
    });
});
//]]>
</script>
