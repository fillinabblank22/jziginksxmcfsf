﻿using System;
using System.Linq;
using System.Web.UI;

public partial class Orion_Controls_TimeZonePicker : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack) return;

        var currentYear = DateTime.Now.Year;
        timeZonePicker.DataSource = TimeZoneInfo
            .GetSystemTimeZones()
            .GroupBy(z => new
            {
                SummerOffset = z.GetUtcOffset(new DateTime(currentYear, 7, 15)).TotalMinutes,
                WinterOffset = z.GetUtcOffset(new DateTime(currentYear, 1, 15)).TotalMinutes
            }, z => z)
            .Select(g => new
            {
                OffsetCouple = $"{g.Key.SummerOffset}|{g.Key.WinterOffset}",
                DisplayName = $"{SplitTimeZoneDisplayName(g.First().DisplayName)[0]} " +
                              $"{string.Join(" | ", g.Select(z => SplitTimeZoneDisplayName(z.DisplayName)[1]))}"
            });

        timeZonePicker.DataBind();
    }

    private static string[] SplitTimeZoneDisplayName(string displayName) => displayName.Split(new[] { ' ' }, 2);
}
