using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.SyslogTrapInterfaces;
using SolarWinds.SyslogTraps.Common;

public partial class TrapTypeControl : System.Web.UI.UserControl, ITrapTypeControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public string TrapType
    {
        get { return trapTypes.SelectedValue; }
        set { trapTypes.SelectedValue = value; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string selectedvalue = string.Empty;
        if (!string.IsNullOrEmpty(TrapType))
        {
            selectedvalue = TrapType;
        }

        trapTypes.Items.Clear();
        trapTypes.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBCODE_VB0_202, string.Empty));

        using (var proxy = SyslogTrapBusinessLayerProxyFactory.Instance.Create())
        {
            List<string> trTypes = proxy.Api.GetTrapTypes();

            foreach (string trType in trTypes)
            {
                trapTypes.Items.Add(new ListItem(trType, trType));
            }
        }

        TrapType = selectedvalue;
    }
}
