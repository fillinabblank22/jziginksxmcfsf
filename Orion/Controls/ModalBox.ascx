<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModalBox.ascx.cs" Inherits="ModalBox" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:UpdatePanel runat="server" ID="upModalBox" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <div runat="server" id="divContainer" style="display: none;" >
            <iframe frameborder="0" src="iframe.html" id="iFrame" runat="server">               
            </iframe>
            <div id="divCover" name="divCover" runat="server" style="position: fixed; z-index: 1000; width: 100%;left: 0px;">
                <div runat="server" id="divDialog" style="margin-left: auto; margin-right: auto; width: 800px; height: 100%; background-color: White;">
                    <asp:PlaceHolder runat="server" ID="phContents" />
                </div>
            </div>
        </div> 
        <script language="javascript" type="text/javascript" >
            window.onresize = function() { setModalBox(); }
        </script>
        <!--[if IE]>
        <script type="text/javascript">
            window.onscroll = function() { setModalBox(); }
        </script>
        <![endif]-->
    </ContentTemplate>
</asp:UpdatePanel>
