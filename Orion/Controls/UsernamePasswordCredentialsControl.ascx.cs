﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web;

public partial class Orion_Controls_UsernamePasswordCredentialsControl : System.Web.UI.UserControl
{
    private static readonly Log Log = new Log();

    public static readonly string NewCredentialsId = string.Empty;

    private readonly WebDataProtection webDataProtection = new WebDataProtection(new WebDataProtectionCryptoKeyProvider());

    /// <summary>
    /// Suffix for 'SW.Core.UsernamePasswordCredentialsControl' instance name to support multiple instances on single page.
    /// </summary>
    public string JavaScriptInstanceNameSuffix
    {
        get { return this.ClientID; }
    }

    public string JavaScriptTestCredentialsHandler { get; set; }

    /// <summary>
    /// Orion.CredentialRelation SWIS entity Use parameter value to search for available credentials.
    /// </summary>
    public string CredentialRelationUse
    {
        get { return tbCredentialsRelationUse.Value; }
        set { tbCredentialsRelationUse.Value = value; }
    }

    /// <summary>
    /// Localized hint for user name, shown only when non empty.
    /// </summary>
    public string UserNameHint { get; set; }

    /// <summary>
    /// Localized hint for user password, shown only when non empty.
    /// </summary>
    public string PasswordHint { get; set; }

    /// <summary>
    /// When true, user will be able to edit username and password text fields; otherwise this will not be possible.
    /// </summary>
    public bool Editable { get; set; } = true;

    /// <summary>
    /// When property will be setup to true user will not be able to submit form when validation of username and password format will fail.
    /// </summary>
    public bool PreventSubmitWhenValidationFails { get; set; } = false;

    /// <summary>
    /// Allows to hide details link in error message which shows technical details.
    /// </summary>
    public bool HideFailedTestDetailsLink { get; set; }

    public string UserName
    {
        get { return tbUserName.Text; }
        set { tbUserName.Text = value; }
    }

    public string Password
    {
        get
        {
            if (string.IsNullOrEmpty(tbPassword.Text))
                return string.Empty;

            return webDataProtection.UnprotectData(tbPassword.Text);
        }
        set
        {
            // Clear password when empty
            if (string.IsNullOrEmpty(value))
            {
                tbPassword.Text = value;
                tbPassword.Attributes.Remove("value");
                return;
            }

            // Update and protect password only when changed, do not protect it multiple times
            if (StringComparer.Ordinal.Equals(tbPassword.Attributes["value"], value))
                return;

            tbPassword.Text = webDataProtection.ProtectData(value);
            tbPassword.Attributes["value"] = tbPassword.Text;
        }
    }

    public string CustomData {
        get { return hfCustomData.Value; }
        set { hfCustomData.Value = value; }
    }

    /// <summary>
    /// Registers integration javascript instance script to ensure it is initialized during page load and validation callback are bound after postback.
    /// </summary>
    /// <returns>Javascript instance name specific for this control</returns>
    private string RegisterIntegrationJavaScript()
    {
        var jsObjectName = $"SW.Core.UsernamePasswordCredentialsControl_{JavaScriptInstanceNameSuffix}";

        // Register integration javascript on update panel
        // By registering the script in this manner we ensure it runs when it is loaded via update panel. Standard scripts are not run.
        var jsScript = new StringBuilder();

        // Initialize only once during first page load
        if (!IsPostBack)
        {
            jsScript.AppendFormat(CultureInfo.InvariantCulture,
                "SW.Core.namespace('SW.Core').UsernamePasswordCredentialsControl_{0} = new SW.Core.UsernamePasswordCredentialsControlSettings();\n" +
                "{1}.init('{0}');", JavaScriptInstanceNameSuffix, jsObjectName);

            if (!string.IsNullOrEmpty(JavaScriptTestCredentialsHandler))
            {
                jsScript.AppendFormat(CultureInfo.InvariantCulture, "{0}.SetTestCredentialsHandler({1});", jsObjectName,
                    JavaScriptTestCredentialsHandler);
            }
        }
        else
        {
            jsScript.AppendFormat(CultureInfo.InvariantCulture, "{0}.OnPostBackFinished();", jsObjectName);
        }

        var script = $@"
    <script type='text/javascript'>
        //<![CDATA[
          {jsScript}
        //]]>
    </script>";
        ScriptManager.RegisterStartupScript(this, GetType(), jsObjectName, script, false);
        return jsObjectName;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var jsObjectName = RegisterIntegrationJavaScript();

        if (PreventSubmitWhenValidationFails)
        {
            this.cvUsername.ClientValidationFunction = $"{jsObjectName}.CustomValidationNetValidateUserName";
            this.cvPassword.ClientValidationFunction = $"{jsObjectName}.CustomValidationNetValidatePassword";
        }

        // Bind to javascript instance
        this.btnTestCredentials.OnClientClick = $"{jsObjectName}.testCredentials();return false";
        SetEditable(Editable);

        // Preserve password in UI on postback 
        if (IsPostBack)
        {
            Password = tbPassword.Text;
        }
    }

    /// <summary>
    /// Sets credentials that are not persisted in Orion to control.
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    public void SetCredentials(string userName, string password)
    {
        UserName = userName;
        Password = password;
        tbCredentialsId.Value = NewCredentialsId;
        SetEditable(Editable);
    }

    public void ClearCredentials()
    {
        SetCredentials(string.Empty, string.Empty);
    }

    private void SetEditable(bool editable)
    {
        tbUserName.Enabled = editable;
        tbPassword.Enabled = editable;
    }

    /// <summary>
    /// Display username and password credentials for Orion entity identified by entityUri and for purpose defined by parameter use.
    /// </summary>
    /// <param name="use">Purpose of credentials.</param>
    /// <param name="entityUri">Unique identifier of Orion entity.</param>
    public void LoadUserNamePasswordCredentialsForOrionEntity(string use, string entityUri)
    {
        OrionEntitySharedCredentialManager<UsernamePasswordCredential> usernamePassCredManager =
            new OrionEntitySharedCredentialManager<UsernamePasswordCredential>();
        var cred = usernamePassCredManager.GetCredentialsForEntity(use, entityUri);
        SetCredentials(cred?.Credential);
    }

    /// <summary>
    /// Display username and password credentials for
    /// </summary>
    /// <param name="use">Purpose of credentials.</param>
    /// <param name="entityType">Orion entity type.</param>
    /// <param name="entityId">Orion entity ID.</param>
    public void LoadUserNamePasswordCredentialsForOrionEntity(string use, string entityType, long entityId)
    {
        OrionEntitySharedCredentialManager<UsernamePasswordCredential> usernamePassCredManager =
            new OrionEntitySharedCredentialManager<UsernamePasswordCredential>();
        var cred = usernamePassCredManager.GetCredentialsForEntity(use, entityId, entityType);
        SetCredentials(cred?.Credential);
    }

    private void SetCredentials(UsernamePasswordCredential credential)
    {
        if (credential != null)
        {
            SetCredentials(credential.Username, credential.Password);
            tbCredentialsId.Value = credential.ID?.ToString() ?? NewCredentialsId;
        }
        else
        {
            Log.ErrorFormat("Failed to load credentials for {0}", ID);
            ClearCredentials();
        }
    }

    private readonly Dictionary<string, Action> registeredActions = new Dictionary<string, Action>();

    /// <summary>
    /// It registers an action under the unique key. The action will be executed when the validation message box button is clicked.
    /// The key must match with the MessageButtonAction property of CredentialValidationResult.
    /// </summary>
    /// <param name="key">Unique identification of action.</param>
    /// <param name="action">Action to be invoked when the button is clicked.</param>
    public void RegisterAction(string key, Action action)
    {
        registeredActions.Add(key, action);
    }
    
    protected void validationMessageBoxButton_OnClick(object sender, EventArgs e)
    {
        if (registeredActions.ContainsKey(buttonActionKey.Value))
        {
            registeredActions[buttonActionKey.Value].Invoke();
        }
    }
}