<%@ Control Language="C#" ClassName="NodeStatus" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>

<script runat="server">     
    public CompoundStatus StatusValue
    {
        get { return (null == ViewState["StatusValue"]) ? new CompoundStatus() : (CompoundStatus)ViewState["StatusValue"]; }
        set { ViewState["StatusValue"] = value; }
    }

    protected string ImagePath
    {
        get
        {
            switch (IconType)
            {
                case IconType.Normal: return this.StatusValue.ToString("imgpath", null);
                case IconType.Small: return this.StatusValue.ToString("smallimgpath", null);
                default: return this.StatusValue.ToString("imgpath", null);
            }
        }
    }

    protected string AltText
    {
        get
        {
            string localizedParentEnum;
            localizedParentEnum = Resources.CoreWebContent.ResourceManager.GetString(string.Format("{0}_{1}", typeof(Status).Name,
                this.StatusValue.ParentStatus));
            if (this.StatusValue.ChildStatus.Value == OBJECT_STATUS.Up)
            {
                return string.Format(Resources.CoreWebContent.WEBCODE_VB0_43, localizedParentEnum);
            }

            string localizedChildEnum = Resources.CoreWebContent.ResourceManager.GetString(string.Format("{0}_{1}", typeof(Status).Name,
                this.StatusValue.ChildStatus));
            return string.Format(Resources.CoreWebContent.WEBCODE_VB0_44,
                                    localizedParentEnum, localizedChildEnum);
        }
    }

    public IconType IconType
    {
        get
        {
            object o = ViewState["IconType"];
            return (o == null ? IconType.Normal : (IconType)Enum.Parse(typeof(IconType), ViewState["IconType"].ToString()));
        }
        set
        {
            ViewState["IconType"] = value;
        }
    }
</script>

<img class="StatusIcon" alt="<%= DefaultSanitizer.SanitizeHtml(AltText) %>" src="<%= DefaultSanitizer.SanitizeHtml(ImagePath) %>" />