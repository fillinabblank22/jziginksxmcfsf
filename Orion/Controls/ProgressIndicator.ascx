<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressIndicator.ascx.cs" 
    Inherits="ProgressIndicator"%>

<orion:Include runat="server" File="ProgressIndicator.css" />

<div class="ProgressIndicator" id="ProgressIndicatorDiv" runat="server"></div>
<input type="button" style="display: none" runat="server" ID="redirectBtn" OnServerClick="ProgressStepClick"/>
<asp:HiddenField runat="server" id="hfMaxEnabledRedirectStep" Value="0" />
<script type="text/javascript">
    // <![CDATA[
    var redirectEnabled = <%=RedirectEnabled.ToString().ToLowerInvariant() %> ;
    var redirectAllowedSteps = parseInt($("[id*=hfMaxEnabledRedirectStep]").val());
    var isRedirectAllowedFunction = null;

    function RedirectOrBlock(url, link) {
        if (!redirectEnabled) return false;
        
        var stepNum = parseInt(link.getAttribute("order"));
        if (stepNum <= redirectAllowedSteps) {
            var current = $(".PI_on").find('a');
            if (current.length == 0) return false;
            
            var currentStep = parseInt(current[0].getAttribute("order"));
            // if we click on current step then do nothing 
            if (stepNum != currentStep) {
                if ($.isFunction(isRedirectAllowedFunction) && isRedirectAllowedFunction() == false)
                    return false;
                __doPostBack('<%=redirectBtn.UniqueID%>', url);
            }
            return false;
        }
        return false;
    }

    if (redirectEnabled && redirectAllowedSteps > 0) {
        $.each($(".PI_off"), function () {
            var parent = $(this);
            $(this).find('a').each(function () {
                var stepNum = parseInt(this.getAttribute("order"));
                if (stepNum <= redirectAllowedSteps)
                    parent.addClass("enabled");
            });
        });
    }
    // ]]>
    </script>