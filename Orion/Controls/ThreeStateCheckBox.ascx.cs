﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ThreeStateCheckBox : System.Web.UI.UserControl
{
    /// <summary> State valeus for three state check box. </summary>
    public enum CheckBoxState { Unchecked = 1, Checked = 2, HalfChecked = 0 }

    /// <summary> State default </summary>
    public const CheckBoxState DefaultState = CheckBoxState.Unchecked;

    /// <summary> Checkbox state property. </summary>
    [PersistenceMode(PersistenceMode.Attribute)]
    public CheckBoxState State
    {
        get 
        {
            return (CheckBoxState)Convert.ToInt32(this.CBState.Value);
        }   
        set
        {
            this.CBState.Value = ((int)value).ToString();
        }
    }

    protected string ImageString
    {
        get
        {
            switch (this.State)
            {
                case CheckBoxState.Checked: return "../images/CheckBoxChecked.gif";
                case CheckBoxState.HalfChecked: return "../images/CheckBoxHalfChecked.gif";
                default: return "../images/CheckBoxUnChecked.gif";
            }
        }
    }
}
