﻿<%@ Control Language="C#" ClassName="TransparentBox" %>
<orion:Include runat="server" File="Orion.css" />

<script runat="server">

    private bool blockVisible = false;
    
    /// <summary>
    /// sets 'display' style attribute to show/hide screen blocker
    /// no viewstate support yet
    /// </summary>
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowBlock
    {
        get 
        {
            return this.blockVisible;
        }
        set 
        {
            this.blockVisible = value;
            this.ScreenBlock.Style[HtmlTextWriterStyle.Display] = value ? "block" : "none";
            this.MessageDialog.Style[HtmlTextWriterStyle.Display] = value ? "block" : "none";
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Message
    {
        get
        {
            return this.MessageLabel.Text;
        }

        set
        {
            this.MessageLabel.Text = value;
            this.MessageDialog.Visible = !String.IsNullOrEmpty(value);
        }
    }
   
</script>

<div runat="server" id="ScreenBlock" class="TransparentBox TransparentBoxID" />
<div runat="server" id="MessageDialog" class="TransparentBoxMessage TransparentBoxID" visible="false">
    <asp:Label ID="MessageLabel" runat="server" Text="" Font-Size="Larger" ForeColor="White" />
</div>
