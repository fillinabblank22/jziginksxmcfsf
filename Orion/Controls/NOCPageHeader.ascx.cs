﻿using System;
using System.Web.UI;

using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Controls_NOCPageHeader : UserControl
{
    protected string NocSiteLogoUri
    {
        get
        {
            string logo;
            if (SolarWinds.Orion.Web.DAL.WebSettingsDAL.TryGetNOCLogo(out logo))
            {
                return "/Orion/LogoImageHandler.ashx?f=logo&id=SiteNoclogoImage&time=" + DateTime.Now.Ticks;
            }
            else
                return logo;
        }
    }
    protected bool ShowLogo
    {
        get
        {
            string logo;
            SolarWinds.Orion.Web.DAL.WebSettingsDAL.TryGetNOCLogo(out logo);
            return !string.IsNullOrEmpty(logo);
        }
    }

    protected int ViewId
    {
        get { return (Page is OrionView) ? ((OrionView) Page).ViewInfo.ViewID : 0; }
    }

    protected string ViewTitle
    {
        get { return (Page is OrionView) ? ((OrionView) Page).ViewInfo.ViewGroupName : Page.Title; }
    }

    protected string ReturnUrl
    {
        get { return (Page is OrionView) ? ((OrionView) Page).ReturnUrl : string.Empty; }
    }

    protected string ExitFromNOCUrl
    {
        get { return UrlHelper.UrlRemoveParameter(Request.Url.AbsoluteUri, "isNOCView"); }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        DataBind();
    }
}