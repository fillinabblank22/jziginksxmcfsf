﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Common.Models;

public partial class Orion_Controls_TimeFrameTooltip : System.Web.UI.UserControl
{
    public string Title { get; set; }
    public string IconUrl { get; set; }
    public string TimeFrameName { get; set; }
    public TimeFrame TimeFrame { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            this.lblTitle.Text = this.Title;

            this.imgIcon.ImageUrl = this.IconUrl;
            this.imgIcon.ImageAlign = ImageAlign.Right;

            this.divTimeFrameTooltip.Attributes["class"] += string.Format(" {0}", this.TimeFrameName);

            this.litTimeFrames.Text = this.GetTimeFramesHtml();            
        }
    }

    private string GetTimeFramesHtml()
    {
        StringBuilder html = new StringBuilder();

        if (this.TimeFrame != null)
        {
            if (TimeFrame.StartTime.HasValue && TimeFrame.EndTime.HasValue)
            {
                // convert to current date, because daylight saving offset isn't correct when using 1900/1/1
                DateTime startDate = DateTime.SpecifyKind(new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, this.TimeFrame.StartTime.Value.Hour, this.TimeFrame.StartTime.Value.Minute, this.TimeFrame.StartTime.Value.Second), DateTimeKind.Utc);
                DateTime endDate = DateTime.SpecifyKind(new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, this.TimeFrame.EndTime.Value.Hour, this.TimeFrame.EndTime.Value.Minute, this.TimeFrame.EndTime.Value.Second), DateTimeKind.Utc);
                html.Append(startDate.ToLocalTime().ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append(" - ");
                html.AppendLine(endDate.ToLocalTime().ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append("<br/>");
                html.Append(this.GetDaysString(this.TimeFrame.GetTimeFrameDays(false)));
            }

            if (this.TimeFrame.HasWholeDays())
            {
                if (TimeFrame.StartTime.HasValue && !this.TimeFrame.EndTime.HasValue)
                {
                    html.Append("<br/><br/>");
                }
                html.AppendLine(new DateTime(1900, 1, 1, 0, 0, 0).ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append(" - ");
                html.AppendLine(new DateTime(1900, 1, 1, 23, 59, 59).ToString("h:mm tt", CultureInfo.InvariantCulture));
                html.Append("<br/>");
                html.Append(this.GetDaysString(this.TimeFrame.GetTimeFrameDays(true)));
            }
        }

        return html.ToString();
    }

    private string GetDaysString(List<DayNames> dayNames)
    {        
        // just one day
        if (dayNames.Count == 1)
        {
            return dayNames[0].LocalizedLabel();
        }
                
        // common weekend
        if (dayNames.Count == 2)
        {
            return string.Format(Resources.CoreWebContent.WEBDATA_TK0_58, dayNames[0].Equals(DayNames.Sunday) ? dayNames[1].LocalizedLabel() : dayNames[0].LocalizedLabel(), dayNames[0].Equals(DayNames.Sunday) ? dayNames[0].LocalizedLabel() : dayNames[1].LocalizedLabel());
        }

        // common weekdays
        if (dayNames.Count == 5 && dayNames.Contains(DayNames.Monday) && dayNames.Contains(DayNames.Tuesday) && dayNames.Contains(DayNames.Wednesday) && dayNames.Contains(DayNames.Thursday) && dayNames.Contains(DayNames.Friday))
        {
            return string.Format("{0} - {1}", DayNames.Monday.LocalizedLabel(), DayNames.Friday.LocalizedLabel());
        }

        // general
        return string.Join(", ", dayNames.Select(p => p.LocalizedLabel()));
    }
}