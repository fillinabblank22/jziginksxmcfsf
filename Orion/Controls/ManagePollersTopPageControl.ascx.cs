﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Controls_ManagePollersTopPageControl : UserControl
{
    private readonly Dictionary<string, string> _added = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
    private bool _alreadyactive = false;

    private const string InnerTabFormat = "<em class=\"x-tab-left\"><span class=\"x-tab-strip-inner\"><span class=\"x-tab-strip-text\">{0}</span></span></em>";
    private const string NonClickingFormat = "<a href=\"{0}\" onclick=\"return false;\" class=\"x-tab-right\">{1}</a>";
    private const string ClickingFormat = "<a href=\"{0}\" class=\"x-tab-right\">{1}</a>";

    protected void Page_Load(object sender, EventArgs e)
    {
        var action = Request["action"];
        var pollerId = Request["poller"];

        var plugins = OrionModuleManager.GetManagePollersPlugins().ToArray();

        // plugin specific resources (css, js)
        foreach (var plugin in plugins.Where(p => p.PluginResources != null))
        {
            foreach (var resource in plugin.PluginResources)
            {
                OrionInclude.ModuleFile(plugin.ModuleFolderName, resource);
            }
        }

        // top page items
        var topPageItems = plugins
            .SelectMany(p => p.GetTopPanelItems(action, pollerId) ?? new KeyValuePair<int, string>[0])
            .Where(p => !string.IsNullOrEmpty(p.Value))
            .ToLookup(p => p.Key, p => p.Value)
            .OrderBy(p => p.Key)
            .SelectMany(p => p)
            .ToArray();

        if (topPageItems.Length > 0)
        {
            foreach (var control in topPageItems.Select(topPageItem => new HtmlGenericControl("div") {InnerHtml = topPageItem}))
            {
                topPanel.Controls.Add(control);
            }
        }
        else
        {
            topPanel.Visible = false;
        }

        // tabs
        var tabs = plugins
            .Where(p => p.TabItems != null)
            .SelectMany(p => p.TabItems)
            .Select(p => new KeyValuePair<string, string>(VirtualPathUtility.ToAbsolute(p.Key), p.Value))
            .ToList();

        if (tabs.Count > 0)
        {
            // inject core tabs when there is any from plugin(s)
            ManagePollersHelper.NativePollersPlugin.TabItems.Reverse().ToList().ForEach(ti => tabs.Insert(0, ti));

            foreach (var tab in tabs)
            {
                if (_added.ContainsKey(tab.Key))
                    return;

                _added.Add(tab.Key, tab.Value);

                var fullpath = VirtualPathUtility.ToAbsolute(tab.Key);
                var isurl = fullpath.Equals(Request.Url.AbsolutePath, StringComparison.OrdinalIgnoreCase);

                var fmt = ClickingFormat;
                string tabClass;
                string tabLink;

                var active = Request.Url.AbsolutePath.StartsWith(fullpath);
                if (active && !_alreadyactive)
                {
                    _alreadyactive = true;
                    tabClass = "x-tab-strip-active";

                    if (isurl)
                    {
                        tabLink = "#";
                        fmt = NonClickingFormat;
                    }
                    else
                    {
                        tabLink = HttpUtility.HtmlAttributeEncode(fullpath);
                    }
                }
                else
                {
                    tabClass = "";
                    tabLink = HttpUtility.HtmlAttributeEncode(fullpath);
                }

                var inner = string.Format(InnerTabFormat, HttpUtility.HtmlEncode(tab.Value));
                var newTab = new HtmlGenericControl("li");
                newTab.Attributes.Add("class", "sw-tab " + tabClass);
                newTab.InnerHtml = string.Format(fmt, tabLink, inner);
                tabsUl.Controls.AddAt(tabsUl.Controls.Count - 1, newTab);
            }
        }
        else
        {
            tabsPanel.Visible = false;
        }
    }
}