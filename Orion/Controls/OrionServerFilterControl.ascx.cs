﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.Enums;

public partial class Orion_Controls_OrionServerFilterControl : UserControl
{
    public bool Enabled
    {
        get
        {
            object enabled = ViewState["Enabled"];
            if (enabled != null)
            {
                return (bool)enabled;
            }

            return true;
        }
        set
        {
            ViewState["Enabled"] = value;
        }
    }

	public Boolean Reporting
	{
		get;
		set;
	}

	public String SubTitle
	{
		get
		{
			return Reporting
				? Resources.CoreWebContent.OrionServerFilterControl_ReportingSubTitle
				: Resources.CoreWebContent.OrionServerFilterControl_SubTitle;
		}
	}

    public OrionServerFilterControlDisplayOptions DisplayOption
    {
        get
        {
            object displayOption = ViewState["DisplayOption"];
            if (displayOption != null)
            {
                return (OrionServerFilterControlDisplayOptions)displayOption;
            }

            return OrionServerFilterControlDisplayOptions.RemoteServers;
        }
        set
        {
            ViewState["DisplayOption"] = value;
        }
    }

	private IEnumerable<String> m_InitialServerIDsToSkip;
    public IEnumerable<String> InitialServerIDsToSkip
    {
	    get
	    {
		    return m_InitialServerIDsToSkip;
	    }
	    set
	    {
		    m_InitialServerIDsToSkip = value;
		    DoDataBinding();
	    }
    }

	public void Initialize(String value)
	{
		InitialServerIDsToSkip = String.IsNullOrEmpty(value) ? null :
			value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
	}

	private void DoDataBinding()
	{
		if (!Page.IsPostBack)
		{
            if (!Enabled)
            {
                Visible = false;
                return;
            }

			DataTable dataSourceTable = GetServerRepeaterDataSource();

            if (dataSourceTable.Rows.Count > 0)
            {
                serverRepeater.DataSource = dataSourceTable;
                serverRepeater.DataBind();
            }
            else
            {
                Visible = false;
            }
		}
	}

	private DataTable GetServerRepeaterDataSource()
	{
		DataTable dataSourceTable = new DataTable();

		dataSourceTable.Columns.Add("OrionID", typeof(string));
		dataSourceTable.Columns.Add("ServerName", typeof(string));
		dataSourceTable.Columns.Add("Selected", typeof(bool));

        IEnumerable<string> initialServerIDsToSkip = InitialServerIDsToSkip ?? new string[] { };

        DataTable availableOrionServersData = GetAvailableServersInfo();

        foreach (DataRow row in availableOrionServersData.Rows)
		{
            string serverID = ((int)row["OrionID"]).ToString(CultureInfo.InvariantCulture);

            dataSourceTable.Rows.Add(serverID, row["ServerName"], !initialServerIDsToSkip.Contains(serverID));
		}

		return dataSourceTable;
	}

    private DataTable GetAvailableServersInfo()
    {
        string whereCondition;

        switch (DisplayOption)
        {
            case OrionServerFilterControlDisplayOptions.LocalAndRemoteServers:
                whereCondition = "1 = 1";
                break;
            case OrionServerFilterControlDisplayOptions.LocalServer:
                whereCondition = "IsLocal = true";
                break;
            case OrionServerFilterControlDisplayOptions.RemoteServers:
                whereCondition = "IsLocal = false";
                break;
            default:
                throw new Exception("Unknown display option");
        }

        string query = String.Format("SELECT SiteID AS OrionID, Name AS ServerName FROM Orion.Sites WHERE {0} ORDER BY ServerName", whereCondition);

        using (SolarWinds.InformationService.Linq.Plugins.CoreSwisContext proxy = SwisContextFactory.CreateContext())
        {
            return proxy.Query(query);
        }
    }

    public string GetCommaSeparatedServerIDsToSkip()
	{
		List<string> serverIDsToSkip = new List<string>();

		foreach (RepeaterItem item in serverRepeater.Items)
		{
			CheckBox chkOrionServer = (item.FindControl("chkOrionServer") as CheckBox);

			if (!chkOrionServer.Checked)
			{
                serverIDsToSkip.Add(chkOrionServer.Attributes["OrionID"]);
			}
		}

		return string.Join(",", serverIDsToSkip);
	}
}