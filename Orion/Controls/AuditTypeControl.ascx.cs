using System;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Logging;

using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class AuditTypeControl : System.Web.UI.UserControl
{
	private static Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public string AuditTypeID
	{
		get { return auditTypesList.SelectedValue; }
		set { auditTypesList.SelectedValue = value; }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		string selectedvalue = string.Empty;
		
        if (!string.IsNullOrEmpty(AuditTypeID))
		{
			selectedvalue = AuditTypeID;
		}

		auditTypesList.Items.Clear();
        auditTypesList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_DP0_12, string.Empty));

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			DataTable dt = proxy.GetAuditingTypesTable();

			if (dt != null && dt.Rows.Count > 0)
			{
				foreach (DataRow row in dt.Rows)
				{
					auditTypesList.Items.Add(new ListItem(row["ActionTypeDisplayName"].ToString(), row["ActionTypeID"].ToString()));
				}
			}
		}

		AuditTypeID = selectedvalue;
	}
}
