﻿<%@ Control Language="C#" ClassName="HelpHint" %>
<script runat="server">
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate HelpContent
    {
        get;
        set;
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        
        if (null != this.HelpContent)
        {
            this.HelpContent.InstantiateIn(this.phContents);
        }
    }
</script>
<table class="sw-help-hint">
    <tr>
        <td style="width: 16px; padding-right: 0px !important; vertical-align: middle;">
            <img class="BucketIcon" src="/Orion/images/lightbulb_tip_16x16.gif" alt="" />
        </td>
        <td style="vertical-align: middle;">
            <asp:PlaceHolder runat="server" ID="phContents" />
        </td>
    </tr>
</table>
