﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomChartControl.ascx.cs" Inherits="Orion_Controls_Charts_CustomChart" %>
  
<asp:Panel ID="pnlCustomChart" runat="server">
    <div id="divChart" runat="server"></div>
    <span id="spanChartLegendTitle" class="chartLegendTitle" runat="server"></span>
    <table id="tblLegend" class="chartLegend" runat="server"></table>
</asp:Panel>
     
