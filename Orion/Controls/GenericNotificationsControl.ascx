﻿<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GenericNotificationsControl.ascx.cs" Inherits="GenericNotificationsControl" %>
<style type="text/css">
    #adminContentTable
    {
    	margin: 10px 10px 0px 0px;
    }
    #adminContentTable td
    {
    	padding: 10px 10px 10px 10px;
    }
    #adminContent p
    {
    	width: auto!important;
    }
    .UpdateDescription
    {
    	background-color: White!important;
   	}
</style>
<table id="adminContentTable" width="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr>
        <td>
            <div>
                <br />
                <asp:ImageButton ID="RefreshButton" runat="server" ImageUrl="~/Orion/images/Button.Refresh.gif" OnClick="OnRefreshClick" />
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="UpdatesItems">
            <div class="UpdateDescription">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr class="UpdateDescription" >
                    <td class="BlogSettings" style="vertical-align:middle;">
                        <asp:CheckBox ID="ShowIgnoredCheckBox" runat="server" AutoPostBack="false" Text="Show all notifications" />
                    </td>
                </tr>
            </table>
            </div>
            
            <div class="ProductUpdates"><%= DefaultSanitizer.SanitizeHtml(SelectedNotificationItemType.Description) %>:</div>
            <div class="NoResultsMessage" runat="server" id="NoResultsMessage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_205) %></div>
            <asp:Repeater runat="server" ID="UpdatesTable">
                <HeaderTemplate>
                    <table id="inside_table" cellpadding="0" cellspacing="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr><td colspan="2" style="padding: 0px 0px 0px 0px!important; height: 10px;"></td></tr>
                    <tr class="UpdatesItem">
                        <td style="width: 10px;">
                            <orion:CustomCheckBox runat="server" ID="SelectItemCheckBox" CustomValue='<%# DefaultSanitizer.SanitizeHtml(Eval("Id")) %>' Visible='<%# !Convert.ToBoolean(Eval("Ignored")) %>' />
                        </td>
                        <td class="UpdateDescription">
                            <b><%# DefaultSanitizer.SanitizeHtml(Eval("Title")) %></b>
                            <br />
                            <br />
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Description").ToString()) %>
                            <br />
                            <br />
                            <asp:HyperLink ID="MoreDetailsLink" CssClass="RenewalsLink" runat="server" Visible='<%# Eval("Url").ToString().Length > 0 %>' NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("Url").ToString()) %>'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_204) %></asp:HyperLink>
                        </td>
                        <td>
                            <asp:ImageButton ID="IgnoreButton" ImageUrl="~/Orion/images/Button.Sec.Ignore.gif" 
                                    runat="server" 
                                    OnCommand="OnItemCommand" CommandArgument='<%# DefaultSanitizer.SanitizeHtml(Eval("Id")) %>' CommandName="Ignore"
                                    Visible='<%# !Convert.ToBoolean(Eval("Ignored")) %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
        </td>
    </tr>
</table>
<br />   
<asp:ImageButton ID="IgnoreSelected" ImageUrl="~/Orion/images/Button.Ignore.Selected.gif" runat="server" OnClick="IgnoreSelectedItems"/>
<br />
