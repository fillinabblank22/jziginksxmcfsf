﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Caching;
using SolarWinds.Orion.Core.Common.Federation;
using SolarWinds.Orion.Core.Common.Interfaces;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.Federation;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.HtmlSanitizer;
using SolarWinds.Orion.Web.Plugins;

public delegate void ClearOrionMessageHandler();
public delegate void OrionMessageSortChangeHandler();
public partial class OrionMessagesReportControl : System.Web.UI.UserControl
{
    private static Dictionary<string, string> _netObjectTypes;
    private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private readonly string[] _columns = new string[7] { "DateTime", "MessageType", "Icon", "Message", "Caption", "IPAddress", "SiteName" };
    private bool _printable;
    private string _searchString;
    public event ClearOrionMessageHandler ClearOrionMessage;
    public event OrionMessageSortChangeHandler SortChanged;
    private readonly ISyslogTrapMessageCenterService service = new MessageCenterPlugin();

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Printable
    {
        set { _printable = value; }
        get { return _printable; }
    }

    public bool AllowEventClear
    {
        get
        {
            bool result = false;
            if (bool.TryParse(HttpContext.Current.Profile.PropertyValues["AllowEventClear"].PropertyValue.ToString(), out result))
            {
                return result;
            }
            else
            {
                return false;
            }
        }
    }

    public string SortString
    {
        get
        {
            object val = this.ViewState["sortString"];
            if (val != null && val is string)
                return (string)val;

            return "DateTime";
        }
        set
        {
            this.ViewState["sortString"] = value;
        }
    }

    private string LinkTarget
    {
        get
        {
            if (SwisFederationInfo.IsFederationEnabled)
            {
                return "target =\"_blank\"";
            }
            return string.Empty;
        }
    }

    public SortDirection SortDir
    {
        get
        {
            object val = this.ViewState["sortingDirection"];
            if (val != null && val is SortDirection)
                return (SortDirection)val;

            return SortDirection.Descending;
        }
        set
        {
            this.ViewState["sortingDirection"] = value;
        }
    }

    public DataTable OrionMessageTable
    {
        get
        {
            object val = this.ViewState["orionMessageTable"];
            if (val != null && val is DataTable)
            {
                return (DataTable)val;
            }
            return new DataTable();
        }
        set
        {
            this.ViewState["orionMessageTable"] = value;
        }
    }

    protected string FormatCheckBoxCellBeginTag(object messageType, object icon)
    {
        return FormatCellBeginTag(OrionMessagesHelper.MapOrionMessageType(messageType.ToString()), icon, 15);
    }

    protected string FormatCellBeginTag(OrionMessageType messType, object icon, int width)
    {
        switch (messType)
        {
            case OrionMessageType.SYSLOG_MESSAGE:
                return string.Format("<td {1} class='Severity{0}'>", icon.ToString(), (width > 0) ? string.Format("style=\"width: {0}px;\"", width) : "");
            case OrionMessageType.EVENT_MESSAGE:
            case OrionMessageType.TRAP_MESSAGE:
                return string.Format("<td style='border-bottom: 1px solid #ecedee;{0}'>", (width > 0) ? string.Format("width: {0}px;", width) : "");
            case OrionMessageType.AUDIT_MESSAGE:
                return string.Format("<td {1} class='AuditEventMessage'>", icon.ToString(), (width > 0) ? string.Format("style=\"width: {0}px;\"", width) : "");
            default:
                return string.Format("<td style='border-bottom: 1px solid #ecedee;{0}'>", (width > 0) ? string.Format("width: {0}px;", width) : "");
        }
    }

    protected string FormatHiddenRowValue(object messageType, object messageId, object objectId, object objectType, object siteId)
    {
        OrionMessageType messType = OrionMessagesHelper.MapOrionMessageType(messageType.ToString());
        if (messType == OrionMessageType.ADVANCED_ALERT)
            return string.Format("{0}:{1}:{2}:{3}:{4}", OrionMessagesHelper.GetMessageTypeString(messType), messageId, objectId, objectType, siteId);

        return string.Format("{0}:{1}:{2}", OrionMessagesHelper.GetMessageTypeString(messType), messageId, siteId);
    }

    private string GetObjectPrefixByObjectType(string objectType, OrionMessageType messageType)
    {
        if (messageType != OrionMessageType.ADVANCED_ALERT)
        {
            return string.IsNullOrEmpty(objectType) ? string.Empty : string.Format("{0}:", objectType);
        }

        foreach (var type in _netObjectTypes)
            if (type.Key.Equals(objectType, StringComparison.OrdinalIgnoreCase))
                return string.Format("{0}:", type.Value);
        return string.Empty;
    }

    private Tuple<string, string> GetLinkStartEnd(string objectID, string objectType, string prefix, string objectID2, string siteId)
    {
        string linkStart = string.Empty;
        string linkEnd = string.Empty;

        int objID;
        int.TryParse(objectID, out objID);

        if (!string.IsNullOrEmpty(prefix) && prefix.StartsWith("/Orion/", StringComparison.InvariantCultureIgnoreCase))
        {
            linkStart = string.Format("<a href=\"{0}{1}\" {2}>", FederationUrlHelper.GetLinkPrefix(siteId), prefix, LinkTarget);
            linkEnd = "</a>";
        }
        else if (objID > 0 && !string.IsNullOrEmpty(objectType) && !string.IsNullOrEmpty(prefix))
        {
            int objID2;
            int.TryParse(objectID2, out objID2);

            linkStart = string.Format("<a href=\"{3}/Orion/View.aspx?NetObject={0}:{1}{2}\" {4}>",
                prefix, objectID, (objID2 > 0) ? ":" + objectID2 : string.Empty, FederationUrlHelper.GetLinkPrefix(siteId), LinkTarget);
            linkEnd = "</a>";
        }
        else
        {
            linkStart = string.Empty;
            linkEnd = string.Empty;
        }

        return Tuple.Create(linkStart, linkEnd);
    }

    private string FormatColumnValue(string column, OrionMessageType messageType, string value, string objectType, string prefix, string objectID, string objectID2, string backColor, string siteId, string siteName)
    {
        Tuple<string, string> linkStartEnd;

        switch (column.ToUpper())
        {
            case "ACKNOWLEDGED":
                return (string.Compare(value.Trim(), "0", StringComparison.InvariantCultureIgnoreCase) == 0) ? "No" : "Yes";
            case "MSGID":
            case "CAPTION":
            case "IPADDRESS":
                linkStartEnd = GetLinkStartEnd(objectID, objectType, prefix, objectID2, siteId);
                if (string.IsNullOrEmpty(value.Trim()))
                    return linkStartEnd.Item1 + "&nbsp;" + linkStartEnd.Item2;

                return linkStartEnd.Item1 + HttpUtility.HtmlEncode(value).Trim() + linkStartEnd.Item2;
            case "DATETIME":
                DateTime dateTime = new DateTime();
                if (DateTime.TryParse(value, out dateTime))
                    return string.Format("<span style='white-space: nowrap;'>&nbsp;{0}&nbsp;</span>", dateTime.ToString(CultureInfo.CurrentCulture));
                else
                    return "&nbsp;";
            case "MESSAGE":
                string mess = value;

                if (messageType == OrionMessageType.SYSLOG_MESSAGE)
                {
                    if (!string.IsNullOrEmpty(_searchString))
                    {
                        string[] arr = Regex.Split(mess, Regex.Escape(_searchString), RegexOptions.IgnoreCase);
                        foreach (string item in arr)
                            if (!string.IsNullOrEmpty(item))
                                mess = mess.Replace(item, HttpUtility.HtmlEncode(item));
                        mess = Regex.Replace(
                                mess,
                                Regex.Escape(_searchString),
                                new MatchEvaluator(ReplaceEncodeAndHighlight),
                                RegexOptions.IgnoreCase);
                    }
                    else
                    {
                        mess = HttpUtility.HtmlEncode(mess);
                    }
                }
                else
                {
                    bool encodeTraps = false;
                    bool.TryParse(ConfigurationManager.AppSettings["HTMLEncodeTraps"], out encodeTraps);
                    if ((messageType == OrionMessageType.TRAP_MESSAGE) && encodeTraps)
                    {
                        mess = System.Web.HttpUtility.HtmlEncode(mess.Replace("<br />", "[[br]]").Replace("<br/>", "[[br]]")).Replace("[[br]]", "<br />");
                    }

                    if (!string.IsNullOrEmpty(_searchString))
                    {
                        mess = Regex.Replace(
                                mess,
                                Regex.Escape(_searchString),
                                new MatchEvaluator(ReplaceHighlight),
                                RegexOptions.IgnoreCase);
                    }
                }

                switch (messageType)
                {
                    case OrionMessageType.EVENT_MESSAGE:
                    case OrionMessageType.AUDIT_MESSAGE:
                        linkStartEnd = GetLinkStartEnd(objectID, objectType, prefix, objectID2, siteId);
                        return linkStartEnd.Item1 + WebSecurityHelper.SanitizeHtmlV2(mess).Trim() + linkStartEnd.Item2;
                    default:
                        return mess.Trim();
                }
            case "MESSAGETYPE":
                return OrionMessagesHelper.GetLocalizedMessageTypeLabel(messageType, true, false);
            case "ICON":
                string imageHtml = null;

                var sitePrefix = FederationUrlHelper.GetImagePrefix(siteId);
                switch (messageType)
                {
                    case OrionMessageType.ADVANCED_ALERT:
                    case OrionMessageType.BASIC_ALERT:
                        imageHtml = string.Format("<img src=\"{0}/NetPerfMon/images/Event-19.gif\" alt=''/>", sitePrefix);
                        break;
                    case OrionMessageType.EVENT_MESSAGE:
                        imageHtml = string.Format("<img alt='' src=\"{1}/NetPerfMon/images/Event-{0}.gif\"/>", value.Trim(), sitePrefix);
                        break;
                    case OrionMessageType.SYSLOG_MESSAGE:
                        imageHtml = string.Format("<img alt='' src=\"{1}/NetPerfMon/images/Severity{0}.gif\"/>", value.Trim(), sitePrefix);
                        break;
                    case OrionMessageType.TRAP_MESSAGE:
                        imageHtml = string.Format("<img alt='' src=\"{0}/NetPerfMon/images/trap.gif\"/>", sitePrefix);
                        break;
                    case OrionMessageType.AUDIT_MESSAGE:
                        imageHtml = string.Format("<img alt='' src=\"{0}/NetPerfMon/images/audit_event.gif\"/>", sitePrefix);
                        break;
                    default:
                        throw new InvalidOperationException("Message type '" + messageType + "' is not supported.");
                }

                int color = Color.White.ToArgb();
                int.TryParse(backColor, out color);
                color = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);

                return string.Format("<div style='background-color:#{0};' class='event-icon'>{1}</div>",
                    color.ToString("X"), imageHtml);
            case "SITENAME":
                return string.Format("<a href=\"/Server/{0}\" target=\"_blank\">{1}</a>", siteId, HttpUtility.HtmlEncode(siteName));
            default:
                return HttpUtility.HtmlEncode(value.Trim().Replace("<br />", "[[br]]").Replace("<br/>", "[[br]]")).Replace("[[br]]", "<br />");
        }
    }

    private string ReplaceEncodeAndHighlight(Match m)
    {
        return string.Format("<span class=\"selectedSearchStr\" >{0}</span>", HttpUtility.HtmlEncode(m.ToString()));
    }

    private string ReplaceHighlight(Match m)
    {
        return string.Format("<span class=\"selectedSearchStr\" >{0}</span>", m);
    }

    private void PublishData()
    {
        table.DataSource = OrionMessageTable;
        table.DataBind();
    }

    private void SortOnPage(string column, SortDirection direction)
    {
        if (OrionMessageTable.Rows.Count == 0)
            return;

        string sortD = " DESC";
        if (direction == SortDirection.Ascending) sortD = " ASC";

        OrionMessageTable.DefaultView.Sort = OrionMessageTable.Columns[column] + sortD;
    }

    private void ShowDirImage()
    {
        TimeDirImage.Controls.Clear();
        MessageTypeDirImage.Controls.Clear();
        MessageDirImage.Controls.Clear();
        CaptionDirImage.Controls.Clear();
        IpAddressDirImage.Controls.Clear();
        SolarWindsServerDirImage.Controls.Clear();

        string img = (SortDir == SortDirection.Ascending) ? "/Orion/images/sortable_arrow_up.gif" : "/Orion/images/sortable_arrow_down.gif";

        switch (SortString)
        {
            case "DateTime":
                TimeDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "MessageType":
                MessageTypeDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "Message":
                MessageDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "Caption":
                CaptionDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "IPAddress":
                IpAddressDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "SiteName":
                SolarWindsServerDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_netObjectTypes == null)
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                _netObjectTypes = proxy.GetNetObjectData();
            }
        }
    }

    protected void table_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string messageType = DataBinder.Eval(e.Item.DataItem, "MessageType").ToString();
            string objectType = DataBinder.Eval(e.Item.DataItem, "ObjectType").ToString().Trim();
            string objectPrefix = DataBinder.Eval(e.Item.DataItem, "NetObjectPrefix").ToString().Trim();
            string netObjectId = DataBinder.Eval(e.Item.DataItem, "ActiveNetObject").ToString().Trim();
            string objectId = DataBinder.Eval(e.Item.DataItem, "ObjectID").ToString().Trim();
            string objectId2 = DataBinder.Eval(e.Item.DataItem, "ObjectID2").ToString().Trim();
            string netObjectValue = DataBinder.Eval(e.Item.DataItem, "NetObjectValue").ToString().Trim();
            if (string.IsNullOrEmpty(netObjectId))
            {
                netObjectId = netObjectValue;
            }
            string siteId = DataBinder.Eval(e.Item.DataItem, "SiteId").ToString();
            string siteName = DataBinder.Eval(e.Item.DataItem, "SiteName").ToString().Trim();

            OrionMessageType messType = OrionMessagesHelper.MapOrionMessageType(messageType);

            string icon = DataBinder.Eval(e.Item.DataItem, "Icon").ToString();
            string backColor = string.Empty;
            if (messType == OrionMessageType.EVENT_MESSAGE || messType == OrionMessageType.TRAP_MESSAGE)
                backColor = DataBinder.Eval(e.Item.DataItem, "BackColor").ToString();

            var htmlLinkSanitizer = HtmlLinksSanitizerFactory.CreateInstance();
            for (int i = 0; i < _columns.Length; i++)
            {
                if (!SwisFederationInfo.IsFederationEnabled && _columns[i].Equals("SiteName", StringComparison.OrdinalIgnoreCase))
                    continue;

                string value = string.Empty;
                if (_columns[i].Equals("MessageType", StringComparison.OrdinalIgnoreCase))
                    value = messageType;
                else if (_columns[i].Equals("Icon", StringComparison.OrdinalIgnoreCase))
                    value = icon;
                else
                    value = DataBinder.Eval(e.Item.DataItem, _columns[i]).ToString();

                if (_columns[i].Equals("Message", StringComparison.OrdinalIgnoreCase)
                    && (messType == OrionMessageType.ADVANCED_ALERT || messType == OrionMessageType.BASIC_ALERT))
                {
                    e.Item.Controls.Add(new LiteralControl(FormatCellBeginTag(messType, icon, 0)));
                    var msg = FormatColumnValue(_columns[i], messType, value, objectType, objectPrefix, netObjectId, objectId2, backColor, siteId, siteName);
                    // Sanitize input and remove all a href tags from alert message
                    msg = htmlLinkSanitizer.SanitizeHtmlLinks(msg, null);
                    msg = htmlLinkSanitizer.RemoveHtmlLinks(msg);
                    string htmlContent;
                    if (objectType == "AAT")
                        htmlContent = string.Format("<a href='{2}/Orion/View.aspx?NetObject=AAT:{0}' {3}>{1}</a>", objectId, msg, FederationUrlHelper.GetLinkPrefix(siteId), LinkTarget);
                    else
                        htmlContent = string.Format("<a href='javascript:ShowAlertDetails(\"{0}\",\"{1}\",\"{2}\",{3});'>{4}</a>",
                            DataBinder.Eval(e.Item.DataItem, "MsgID"),
                            string.Format("{0}{1}", GetObjectPrefixByObjectType(objectType, messType), objectId),
                            objectType,
                            AllowEventClear.ToString().ToLowerInvariant(),
                            msg);

                    e.Item.Controls.Add(new LiteralControl(htmlContent));
                    e.Item.Controls.Add(new LiteralControl("</td>"));
                }
                else
                {
                    string columnFormat = FormatCellBeginTag(messType, icon, 0) + "{0}</td>";
                    e.Item.Controls.Add(new LiteralControl(string.Format(columnFormat, FormatColumnValue(_columns[i], messType, value, objectType, objectPrefix, netObjectId, objectId2, backColor, siteId, siteName))));
                }
            }
            e.Item.Controls.Add(new LiteralControl("</tr>"));
        }
    }

    protected void AcknowledgeMess_Click(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            return;

        Dictionary<int, List<int>> eventKeys = new Dictionary<int, List<int>>();
        List<string> alertKeys = new List<string>();
        Dictionary<int, List<int>> newAlertObjectIds = new Dictionary<int, List<int>>();
        List<Int64> syslogKeys = new List<Int64>();
        List<Int64> trapKeys = new List<Int64>();

        foreach (RepeaterItem item in table.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("cbMess");
            if (cb != null)
            {
                if (cb.Checked && cb.Enabled)
                {
                    HiddenField hf = (HiddenField)item.FindControl("MsgKey");
                    if (hf != null)
                    {
                        string[] parts = hf.Value.Split(':');

                        switch (OrionMessagesHelper.MapOrionMessageType(parts[0]))
                        {
                            case OrionMessageType.ADVANCED_ALERT:
                                if (parts[1].StartsWith("AA-"))
                                {
                                    int objectId, siteId;
                                    if (int.TryParse(parts[2], out objectId) && int.TryParse(parts[4], out siteId))
                                    {
                                        if (!newAlertObjectIds.ContainsKey(siteId))
                                            newAlertObjectIds.Add(siteId, new List<int>());

                                        newAlertObjectIds[siteId].Add(objectId);
                                    }
                                }
                                else
                                    alertKeys.Add(string.Format("{0}:{1}:{2}", parts[1], parts[2], parts[3]));
                                break;
                            case OrionMessageType.EVENT_MESSAGE:
                                var orionId = int.Parse(parts[2]);
                                if (!eventKeys.ContainsKey(orionId))
                                    eventKeys.Add(orionId, new List<int>());

                                eventKeys[orionId].Add(int.Parse(parts[1]));
                                break;
                            case OrionMessageType.SYSLOG_MESSAGE:
                                syslogKeys.Add(long.Parse(parts[1]));
                                break;
                            case OrionMessageType.TRAP_MESSAGE:
                                trapKeys.Add(Int64.Parse(parts[1]));
                                break;
                        }
                    }
                }
            }
        }

        if (newAlertObjectIds.Count > 0)
        {
            foreach (var siteId in newAlertObjectIds.Keys)
            {
                if (siteId == OrionSitesCache.Instance.GetLocalOrionSiteID())
                {
                    using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                    {
                        proxy.AcknowledgeAlertsV2(newAlertObjectIds[siteId], HttpContext.Current.Profile.UserName, string.Empty, DateTime.UtcNow);
                    }
                }
                else
                {
                    var proxyProvider = new OrionSiteSwisProxyProvider();
                    using (var siteProxy = proxyProvider.CreateProxy(siteId))
                    {
                        siteProxy.Invoke<bool>("Orion.AlertActive", "Acknowledge", newAlertObjectIds[siteId], string.Empty);
                    }
                }
            }
        }

        if (alertKeys.Count > 0)
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                proxy.AcknowledgeAlertsAction(alertKeys, HttpContext.Current.Profile.UserName);

        if (eventKeys.Count > 0)
        {
            foreach (var siteId in eventKeys.Keys)
            {
                if (siteId == OrionSitesCache.Instance.GetLocalOrionSiteID())
                {
                    using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                    {
                        proxy.AcknowledgeEvents(eventKeys[siteId]);
                    }
                }
                else
                {
                    var proxyProvider = new OrionSiteSwisProxyProvider();
                    using (var siteProxy = proxyProvider.CreateProxy(siteId))
                    {
                        siteProxy.Invoke<bool>("Orion.Events", "Acknowledge", eventKeys[siteId]);
                    }
                }
            }
        }

        if (syslogKeys.Count > 0)
            service.ClearMessages(syslogKeys);

        if (trapKeys.Count > 0)
            service.AcknowledgeTraps(trapKeys);


        if (ClearOrionMessage != null)
            ClearOrionMessage();
    }

    protected void SortChange(object sender, EventArgs e)
    {
        if (sender as LinkButton == null) return;

        string ID = (sender as LinkButton).ID;
        switch (ID.ToLowerInvariant())
        {
            case "sortbytime":
                if (SortString == "DateTime") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "DateTime"; SortDir = SortDirection.Descending; }
                break;
            case "sortbymessagetype":
                if (SortString == "MessageType") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "MessageType"; SortDir = SortDirection.Descending; }
                break;
            case "sortbymessage":
                if (SortString == "Message") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "Message"; SortDir = SortDirection.Descending; }
                break;
            case "sortbycaption":
                if (SortString == "Caption") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "Caption"; SortDir = SortDirection.Descending; }
                break;
            case "sortbyipaddress":
                if (SortString == "IPAddress") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "IPAddress"; SortDir = SortDirection.Descending; }
                break;
            case "sortbysolarwindsserver":
                if (SortString == "SiteName") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "SiteName"; SortDir = SortDirection.Descending; }
                break;
        }

        ShowDirImage();
        SortOnPage(SortString, SortDir);
        PublishData();

        if (SortChanged != null)
            SortChanged();
    }

    public void GenerateReport(OrionMessagesFilter filter)
    {
        _searchString = filter.SearchString;
        filter.AlertCategoryLimitation = this.Profile.AlertCategory;

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            OrionMessageTable = proxy.GetOrionMessagesTable(filter);
        }

        var errors = SwisDataTableParser.GetDataTableErrors(OrionMessageTable);
        SwisErrorControl.SetError(errors);
        if (SwisErrorControl.ServersWithErrors == null || SwisErrorControl.ServersWithErrors.Length == 0)
            SwisErrorControl.Visible = false;

        ShowDirImage();
        SortOnPage(SortString, SortDir);
        PublishData();

        this.MessageActionsTable.Visible = this.Profile.AllowEventClear
            && !Printable && (table.Items.Count >= CommonConstants.MIN_ROWS_NUMBER);
    }
}
