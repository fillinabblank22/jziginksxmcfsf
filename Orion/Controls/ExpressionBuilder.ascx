<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExpressionBuilder.ascx.cs" Inherits="Orion_Controls_ExpressionBuilder" %>

<%@ Register TagPrefix="orion" TagName="FieldPicker" Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" %>

<%@ Import Namespace="Newtonsoft.Json" %>
<orion:Include runat="server" File="AlertConditionPicker.css" />
<orion:Include runat="server" File="js/jquery/timePicker.css" />
<orion:Include runat="server" Framework="Ext"  FrameworkVersion="3.4" />
<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="jquery/jquery.customSelect.sw.js" />
<orion:Include runat="server" File="SearchComboBox.js" />
<orion:Include runat="server" File="ExpressionBuilder.css" />
<orion:Include runat="server" File="ExpressionBuilder.js" />

<div id="dynamicModeEditor<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>" class="data-source-editor">
    <div style="padding:0px 0 4px 0;">
        <table width="100%">
            <tr>
                <td>
                    <span class="neb-condition-title" id="conditionTitle" runat="server"></span>
                </td>
            </tr>
        </table>
    </div>
    <orion:FieldPicker runat="server" ClientInstanceName="fieldPickerInput_Instance" ID="FieldPicker" SingleMode="True" UseNetObjectPicker="True" UseDataAggregation="True" DisableNavigationPathLimitation="True" 
        AddFieldsButtonTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_92 %>"
        DialogTitleTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_91 %>"
        GroupByHeaderTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_93 %>"
        SearchWatermarkTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_94 %>"/>

    <div id="expr<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>"></div>
    <br />

    <div id="errorContainer" class="sw-suggestion sw-suggestion-fail" style="display: none"><span class="sw-suggestion-icon"></span><span id="errorMessage">&nbsp;</span></div>

    <div class="sw-btn-bar-wizard alertConditionPicker" style="display:none" width="100%">
        <orion:LocalizableButton ID="bt_Export" runat="server" DisplayType="Secondary" LocalizedText="CustomText"
            Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_142 %>" />
        <div class="file-uploader">
            <asp:FileUpload ID="fileDialog" runat="server" CssClass="file" />
            <div class="fakefile" id="dFakeFile<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>" style="white-space: nowrap;">

                <orion:LocalizableButton runat="server" ID="fileUpload"  LocalizedText="CustomText"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_143 %>" />
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#<%=fileUpload.ClientID%>").mousemove(function(e) {
        $("#<%=fileDialog.ClientID %>").css({
            'left': e.pageX - $("#dFakeFile<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>").offset().left - 110,
            'top': e.pageY - $("#dFakeFile<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>").offset().top - 10
        });
    });

    var hash = {
        '.xml': 1
    };

    (function(conditionTypeDynamic) {

        function preferenceItemClick(ActionId)
        {
            if(ActionId == 'CLEAR')
            {
                var baseConfig = conditionTypeDynamic.exprBuilderConfig;

                var selectedEntity = { ObjectType: getSelectedMasterObjectType() };
                var picker = SW.Core.Pickers.FieldPicker.Instances[getFieldPickerId()];

                var emptyDataSource = {
                    "CommandText": "",
                    "MasterEntity": "",
                    "ObjectType": "",
                    "Filter": null,
                    "EntityUri": []
                };

                dataSource = _.extend(emptyDataSource, dataSource, {
                    Type: 1, //Dynamic
                    DynamicSelectionType: "2",
                    MasterEntity: getSelectedMasterEntity(),
                    ObjectType: getSelectedMasterObjectType()
                });

                picker.SetExtraRequestParams(selectedEntity);

                var exprConfig = $.extend(true, { }, baseConfig, {
                    dataSource: dataSource,
                    expr: null,
                    onEmpty: function(isEmpty) {
                    },
                    extraRequestParams: picker.GetExtraRequestParams(),
                    dataProviderUrl: "<%= this.DataProviderUrl %>",
                    enableWatermark: true
                });

                getExprBuilderElement().expression_builder_v2(exprConfig);
                return;
            }

            if (ActionId == 'SHOW_SWQL') 
            {
                SW.Core.Alerts.Add.DynamicConditionController.Instance['<%=this.ClientIdPrefix%>'].showSwqlQueryDialog();
                return;
            }

            alert(ActionId + ' is not currently supported');
        }

        conditionTypeDynamic.exprBuilderConfig = {
            allowGroups: true,
            fieldPickerInstanceName: getFieldPickerId(),
            groupOperators: <%= JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic.OperatorProvider()).GetGroupOperators().ToArray()) %>,
            logicalOperators: <%= JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic.OperatorProvider()).GetLogicalOperators().ToArray()) %>,
            ruleTypes: [
                { ruleType: 1, displayName: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Expression_Builder_Add_Single_Value_Comparison) %>", expr: <%= GetFieldConstantConditionType() %>, hint: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0010) %>", visible:true }, // Add Single Value Comparison - Recommended
                { ruleType: 2, displayName: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Expression_Builder_Add_Double_Value_Comparison) %>", expr: <%= GetFieldFieldConditionType() %>, hint: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0011) %>", visible:true }, // Add Double Value Comparison
                { ruleType: 3, displayName: "", expr: <%= GetConstConstConditionType() %>, hint: "", visible:false }, // Constant to Constant
                { ruleType: 5, displayName: "", expr: <%= GetEventConditionType() %>, hint: "", visible:false } // Event
            ],
            preferenceOptions: [
                { displayName: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_TK0_1) %>", actionId: "CLEAR", callback: preferenceItemClick }, // Clear condition
                { displayName: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_TK0_6) %>", actionId: "SHOW_SWQL", callback: preferenceItemClick } // Show SWQL query
            ],
            fieldInput: SW.Core.Controls.ExpressionBuilder.Input.ConstantFieldSwitch,
            constantInput: SW.Core.Controls.ExpressionBuilder.Input.ConstantFieldSwitch,
            enableWatermark: true,
            rootNodeText: '<%=RootNodeTitle %>',
            onChangeInterval: 1500,
            onChangeHandler: function() {},
            showOnlyNonTransientFields: <%= ShowOnlyNonTransientFields.ToString().ToLower() %>,
            correlationEngine: false
        };

        var showNeb = false;
        var reset = false;
        var dataSource = null;


        function showChangesLostConfirm() {
            var currentExpState = getExpr();
            if (currentExpState) {
                if (confirm('Your changes will be lost. Do you want to proceed?')) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                return true;
            }
        }

        function getExpr() {
            return (showNeb) ? getExprBuilderElement().expression_builder_v2("getExpression") : null;
        }

        function getParentElement() {
            return $("#dynamicModeEditor<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>");
        }

        function getFieldPickerId() {
            return 'fieldPickerInput_Instance<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>';
        }  

        function redrawExprBuilder() {
            if (showNeb) 
            {
                var exprBuilderConfig = createExprBuilderConfig();
                getExprBuilderElement().expression_builder_v2(exprBuilderConfig);
            } 
            else 
            {
                createAddConditionBlock();
            }
        };

        function getExprBuilderElement() {
            return getParentElement().find('#expr<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>');
        };

        function createExprBuilderConfig(exprTree) {
            var baseConfig = conditionTypeDynamic.exprBuilderConfig;

            var emptyDataSource = {
                "CommandText": "",
                "MasterEntity": "",
                "ObjectType": "",
                "Filter": null,
                "EntityUri": []
            };

            dataSource = _.extend(emptyDataSource, dataSource, {
                Type: 1, //Dynamic
                DynamicSelectionType: "2",
                MasterEntity: getSelectedMasterEntity(),
                ObjectType: getSelectedMasterObjectType()
            });

            var selectedEntity = { ObjectType: getSelectedMasterObjectType() };
            var picker = SW.Core.Pickers.FieldPicker.Instances[getFieldPickerId()];

            picker.SetExtraRequestParams(selectedEntity);

            // clone datasource and remove filter, so we're not sending it to the web methods
            var expr;
            if(exprTree!= null)
                expr = exprTree;
            else
                expr = dataSource.Filter;

            var ds = $.extend({ }, dataSource);
            ds.Filter = null;

            var exprConfig = $.extend(true, { }, baseConfig, {
                dataSource: ds,
                expr: expr,
                onEmpty: function(isEmpty) {
                },
                updateSourceNameDelegate: null,
                extraRequestParams: picker.GetExtraRequestParams(),
                dataProviderUrl:"<%= this.DataProviderUrl %>",
                enableWatermark: true
            });

            return exprConfig;
        };

        function getSelectedMasterObjectType() {
            return '<%= this.ObjectType %>';
        }

        function getSelectedMasterEntity() {
            return '<%= this.EntityFullName %>';
        }

        function createAddConditionBlock() {
            getExprBuilderElement()
                .html("<table><tr><td data-name='showNEB' class='neb-add-simple'>Add Condition</td></tr></table>")
                .find('[data-name="showNEB"]')
                .click(function() {
                    showNeb = true;
                    redrawExprBuilder();
                });
        };

        conditionTypeDynamic.GetExpressionTree = function() {
            var expression = getExprBuilderElement().expression_builder_v2("getExpression");
            return expression;
        };

        conditionTypeDynamic.SetOnChangeHandler = function(handler) {
            conditionTypeDynamic.exprBuilderConfig.onChangeHandler = handler;
            getExprBuilderElement().data("expression_builder_v2").setOnChangeHandler(handler);
        };

        conditionTypeDynamic.SetOnChangeInterval = function(interval) {
            conditionTypeDynamic.exprBuilderConfig.onChangeInterval = interval;
            getExprBuilderElement().data("expression_builder_v2").setOnChangeInterval(interval);
        };

        conditionTypeDynamic.GetRootElement = function() {
            return getExprBuilderElement();
        };

        conditionTypeDynamic.IsCorrelationEngineEnabled = function() {
            return getExprBuilderElement().data("expression_builder_v2").isCorrelationEngineEnabled();
        };

        conditionTypeDynamic.ClearInfoPanel = function() {
            getExprBuilderElement().data("expression_builder_v2").clearInfoPanel();
        };

        conditionTypeDynamic.AddToInfoPanel = function(id,text,imageUrl) {
            getExprBuilderElement().data("expression_builder_v2").addToInfoPanel(id,text,imageUrl);
        };

        conditionTypeDynamic.RemoveFromInfoPanel = function(id) {
            getExprBuilderElement().data("expression_builder_v2").removeFromInfoPanel(id);
        };

        conditionTypeDynamic.LoadTree = function(objectType, tree) {
            var exprConfig = this.createExprBuilderConfig(tree);

            getExprBuilderElement().expression_builder_v2(exprConfig);
            return;
        };

        function decodeValue(value) {
            var elem = document.createElement('textarea');
            elem.innerHTML = value;
            return elem.value;
        }

        function decodeExprChilds(expr) {

            expr.Value = decodeValue(expr.Value);

            if (expr.Child != null)
            {
                $.each(expr.Child, function(key, value){
                    decodeExprChilds(value);
                });
            }
        }

        $(function() {

         var selectedEntity = { ObjectType: getSelectedMasterObjectType() };
         getParentElement().css('display', 'block');
         
        <% if (String.IsNullOrEmpty(this.ExprTree)) %>
        <% { %>
            var expr = null;
        <% } else { %>
            var expr = <% =this.ExprTree %>;

            if (expr != null) {
                decodeExprChilds(expr);
            }

        <%} %>

         var picker = SW.Core.Pickers.FieldPicker.Instances[getFieldPickerId()];
         picker.SetExtraRequestParams(selectedEntity);

         var exprConfig = createExprBuilderConfig(expr);

         getExprBuilderElement().expression_builder_v2(exprConfig);
         
         $(".alertTab").click(function(e) {
             var expression = getExprBuilderElement().expression_builder_v2("getExpression");

             ORION.callWebService('/Orion/Services/NewAlertingService.asmx', 'SetSessionData', { name: 'AlertExpressionData<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>', value: JSON.stringify(expression) }, function() {});
         });
     });

    })(SW.Core.namespace("SW.Core.Controls.ExpressionBuilder.Instance.<%= DefaultSanitizer.SanitizeHtml(ClientInstanceName) %>"));

</script>
