using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using Limitation = SolarWinds.Orion.Web.Limitation;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common.Swis;

public partial class NetworkObjectControl : System.Web.UI.UserControl
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	private CorePageType _pageType = CorePageType.Traps;

	public string NetObjectID
	{
	    get { return netObjects.SelectedValue; }
	    set { netObjects.SelectedValue = value; }
	}

	public string HostName
	{
	    set 
		{
			foreach (ListItem item in netObjects.Items)
			{
				if (item.Text.Equals(value, StringComparison.OrdinalIgnoreCase))
				{
					netObjects.SelectedValue = item.Value;
					break;
				}
			}
		}
	}
	
	[PersistenceMode(PersistenceMode.Attribute)]
	public string PageType
	{
		set 
		{
			switch (value.ToUpper())
			{
				case "ALL":
					_pageType = CorePageType.All;
					break;
				case "ALERTS":
					_pageType = CorePageType.Alerts;
					break;
				case "SYSLOG":
					_pageType = CorePageType.SysLog;
					break;
				case "EVENTS":
					_pageType = CorePageType.Events;
					break;
				default:
					_pageType = CorePageType.Traps;
					break;
			}
		}
	}

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string selectedvalue = string.Empty;
        if (!string.IsNullOrEmpty(NetObjectID))
        {
            selectedvalue = NetObjectID;
        }

        netObjects.Items.Clear();
        netObjects.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_181, string.Empty));

        Dictionary<int, string> devices = new Dictionary<int, string>();
        if (_pageType == CorePageType.All)
            devices = NodeDAL.GetNodeIdAndCaption(Limitation.GetCurrentListOfLimitationIDs());
        else
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                devices = proxy.GetDeviceNamesForPage(_pageType, Limitation.GetCurrentListOfLimitationIDs(), true);
        netObjects.Items.AddRange(
            devices.OrderBy(p => p.Value).Select(p => new ListItem(p.Value, "N:" + p.Key)).ToArray());
        NetObjectID = selectedvalue;
    }
}
