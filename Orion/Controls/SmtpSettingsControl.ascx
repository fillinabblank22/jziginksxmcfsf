<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SmtpSettingsControl.ascx.cs"
    Inherits="Orion_Controls_SmtpSettingsControl" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<%@ Register TagPrefix="orion" TagName="EmailsInput" Src="~/Orion/Controls/EmailsInput.ascx" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="SmtpSettingsController.js" />

<style type="text/css">
    .credentials-table { padding: 10px; background-color: #ebebeb; }
    .smtp-input-checkbox input {padding: 3px;}
    .smtp-table input.smtp-input-box {border: solid 1px #d5d5d5}

    .sw-suggestion { margin-left: 10px; }
    .sw-suggestion-pass { border: 1px solid #00A000; }
    .ui-dialog .ui-dialog-content { background: white; }
    .invalidValue {       
        background-color: #FFE0E0;
        background-image: url('/Orion/images/failed_16x16.gif');
        background-repeat: no-repeat;
        padding-left: 20px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
</style>

<script type="text/javascript">
    $(function() {
        SW.Core.SMTPServerSettingsController = new SW.Core.SMTPServerSettings(
            { containerID: 'smtpSettings',
                onReadyCallback: <%= OnReadyJsCallback %>,
                mode: '<%= Mode %>' 
            });
        SW.Core.SMTPServerSettingsController.init();
    })    
</script>

<div id="smtpSettings">
<table class="smtp-table">    
    <tr runat="server" ID="smtpHelpLink">        
        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_90) %></td>
        <td style="text-align: right;"> <orion:HelpLink runat="server" HelpUrlFragment="OrionCorePHWhatIsSMTPServer" HelpDescription="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YK0_66 %>" CssClass="coloredLink" /></td>
        </tr>
    <tr>
        <td colspan="2"><asp:DropDownList runat="server" ID="ddSmtpServers" Width="370" data-form="smtpServerID" onchange="SW.Core.SMTPServerSettingsController.refreshSmtpServersList(this); return false;"/></td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 15px;">
            <table runat="server" ID="smtpServerTbl" data-form="smtpServerSettings">
                <tr>
                    <td colspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_67) %><span class="hostnameIpExist" style="float: right; color: red; display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_92) %></span></td>
                    <td><input type="hidden" id="isDefault" data-form="isDefault" runat="server"/></td>
                    <td><input type="hidden" id="isPswChanged" data-form="isPswChanged" runat="server" value="false" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="tbSmtpServer" CssClass="smtp-input-box" Width="345" data-form="smtpServerIP"></asp:TextBox><br/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_68) %></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="tbSmtpPort" CssClass="smtp-input-box" Text="25" Width="70" data-form="smtpServerPort" /><br/>
                    </td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbEnableSSL" CssClass="smtp-input-checkbox cbl" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK1_6 %>" data-form="smtpServerEnableSSL" /></td>
                    <td style="text-align: right;"><orion:HelpLink runat="server" HelpUrlFragment="OrionCorePHWhatIsSSL" HelpDescription="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YK0_69 %>" CssClass="coloredLink" /></td>
                </tr>
                <tr>
                    <td colspan="2"><asp:CheckBox runat="server" ID="cbAuth" CssClass="smtp-input-checkbox cbl" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_YK0_70 %>" data-form="smtpServerUseCredentials" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table runat="server" ID="credentialsTbl" style="display: none;"  class="credentials-table" data-form="smtpServerCredsContainer">
                            <tr>
                                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_239) %></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="tbUserName" autocomplete="new-password" Width="300" CssClass="smtp-input-box" data-form="smtpServerUsername" /><br/>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="tbUserName" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_DP0_1 %>"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_8) %></td>
                            </tr>
                            <tr>
                                <td><asp:TextBox runat="server" ID="tbPassword" TextMode="Password" autocomplete="new-password" Width="160" CssClass="smtp-input-box" data-form="smtpServerPassword" /></td>
                            </tr>
                            <tr>
                                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_71) %></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="tbConfirmPassword" TextMode="Password" autocomplete="new-password" Width="160" CssClass="smtp-input-box" data-form="smtpServerPasswordConfirm" /><br/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="testResult"></div>
                        <span id="testProgress" style="display: none;"><img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>"/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %></span>                        
                        <div>
                            <orion:LocalizableButton Enabled="<%# !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>" runat="server" ID="btnTestCredentials" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VM0_12 %>" DisplayType="Small" OnClientClick="SW.Core.SMTPServerSettingsController.testSmtpCreadentials(); return false;" />
                        </div>
                    </td>
                </tr>
                <tr runat="server" ID="backupServersContent">
                     <td colspan="2">
                         <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_36) %></div>
                         <asp:DropDownList runat="server" ID="ddBackupServers" Width="345" data-form="smtpBackupServerID" onchange="SW.Core.SMTPServerSettingsController.refreshSmtpServersList(this); return false;"/>
                     </td>       
                </tr>               
            </table>            
        </td>
    </tr>
</table>
<div style="display: none">
    <div id="smtpSettingsEmailFromToDialog">
        <div class="email-label">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_28) %>
        </div>
        <orion:EmailsInput ID = "txtSmtpSettingsEmailTo" runat="server" DataForm ="smtpSettingsEmailTo" Visible="True" MultipleMode="False" />
        <div class="email-label">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_82) %>
        </div>
        <orion:EmailsInput ID="txtSmtpSettingsEmailFrom" runat="server" DataForm ="smtpSettingsEmailFrom"  MultipleMode="False" />
        <br />
        <div style="text-align: right">
            <orion:LocalizableButton CssClass="smtp-settings-send-btn" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_192 %>" OnClientClick="return false;" />
            <orion:LocalizableButton CssClass="smtp-settings-cancel-btn" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="return false;" />
        </div>
    </div>    
</div>
</div>
