using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;

public enum ProgressIndicatorHeight
{
    OneLine,
    ThreeLine
}

public delegate void ProgressIndicatorClick(string url);

public partial class ProgressIndicator : UserControl, IProgressIndicator
{
	private readonly List<string> _steps = new List<string>();
	private string _selectedStep;
	private ProgressIndicatorHeight _indicatorHeight = ProgressIndicatorHeight.ThreeLine;

    public event ProgressIndicatorClick OnProgressIndicatorClick;

    public bool RedirectEnabled { get; set; }

    public int MaxEnabledRedirectStep
    {
        get
        {
            int result;
            int.TryParse(hfMaxEnabledRedirectStep.Value, out result);
            return result;
        }
        set { hfMaxEnabledRedirectStep.Value = value.ToString(); }
    }

    public IList<string> Steps
	{
		get { return _steps; }
	}

	public string SelectedStep
	{
		get { return _selectedStep; }
		set { _selectedStep = value; }
	}

	public ProgressIndicatorHeight IndicatorHeight
	{
		get { return _indicatorHeight; }
		set { _indicatorHeight = value; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
	}

	protected override void OnDataBinding(EventArgs e)
	{
		base.OnDataBinding(e);
		BuildSteps();
	}

    protected void ProgressStepClick(object sender, EventArgs e)
    {
        var url = Request["__EVENTARGUMENT"];
        if (OnProgressIndicatorClick != null)
            OnProgressIndicatorClick(url);
    }

	private void BuildSteps()
	{
		ProgressIndicatorDiv.Controls.Clear();

		int selectedIndex = this.SelectedIndex;
		string imageSize = String.Empty;

		if (this.IndicatorHeight == ProgressIndicatorHeight.OneLine)
		{
			imageSize = "_sm";
		}
		else
		{
			ProgressIndicatorDiv.Attributes["class"] += " ProgressIndicatorThreeLine";
		}

		for (int i = 0; i < _steps.Count; ++i)
		{
			string thisItemsState = (selectedIndex == i) ? "on" : "off";
			string nextItemsState = GetNextItemsState(i, selectedIndex);
			string baseClass = String.Format("PI_{0}", thisItemsState);

			HtmlGenericControl div = new HtmlGenericControl("div");
			div.Attributes["class"] = baseClass;
			if (this.IndicatorHeight == ProgressIndicatorHeight.ThreeLine)
				div.Attributes["class"] += String.Format(" {0}_ThreeLine", baseClass);

			div.InnerHtml = (i == 0) ? "&nbsp;" + _steps[i] : _steps[i];

			HtmlImage img = new HtmlImage();
			img.Src = String.Format("/Orion/Images/ProgressIndicator/PI_sep_{0}_{1}{2}.gif",
									thisItemsState, nextItemsState, imageSize);
			img.Alt = "";

			ProgressIndicatorDiv.Controls.Add(div);
			ProgressIndicatorDiv.Controls.Add(img);
		}
	}

	private string GetNextItemsState(int currentIndex, int selectedIndex)
	{
		int nextIndex = currentIndex + 1;
		if (nextIndex == _steps.Count)
			return "off";

		return (selectedIndex == nextIndex) ? "on" : "off";
	}

	private int SelectedIndex
	{
		get
		{
			return _steps.FindIndex(delegate(string s)
			{
				return String.Equals(s, _selectedStep, StringComparison.OrdinalIgnoreCase);
			});
		}
	}
}