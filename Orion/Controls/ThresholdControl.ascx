<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdControl.ascx.cs" Inherits="ThresholdControl" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Models.Thresholds" %>

<orion:Include ID="Include1" File="ThresholdControl.css" runat="server" />

<script type="text/javascript">

    function ApplyThresholdValuesOf_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(result, warning, critical) {
        if (result == true) {
            if (warning != null && warning != '') {
                $('#<%= lblComputedWarningValue.ClientID %>').html("");
                $('#<%= tbWarningValue.ClientID %>').val(warning);
            }
            if (critical != null && critical != '') {
                $('#<%= lblComputedCriticalValue.ClientID %>').html("");
                $('#<%= tbCriticalValue.ClientID %>').val(critical);
            }

            if (warning != null && warning != '' || critical != null && critical != '') {
                $('#<%= tbWarningValue.ClientID %>').change(); // force start recompute (fire event 'change' on TB)
            }
        }
    }

    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function () {
        threshold_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(false);
    });

    $(document).ready(function () {
        threshold_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(false);
    });

    function threshold_<%# DefaultSanitizer.SanitizeHtml(this.ThresholdName.Replace(".","_")) %>(suppressRecompute) {
        if (!isMultiEdit() && !isGlobal() && !suppressRecompute) {
            recompute();
        }

        function isMultiEdit() {
            return (("<%= this.IsMultiedit %>" == "True") ? true : false);
        }

        function isGlobal() {
            return (("<%= this.IsGlobal %>" == "True") ? true : false);
        }

        function getObjectsIDs() {
            var objectIdsString = "<%= this.ObjectIDs %>";

            var objectIds = objectIdsString.split(" ");
            for (var i = 0; i < objectIds.length; i++) {
                objectIds[i] = +objectIds[i];
            }

            return objectIds;
        }

        function clearErrorMessage() {
            var sectionErrorMessage = $('#<%= sectionErrorMessage.ClientID %>');
            var hfIsValid = $('#<%= hfIsValid.ClientID %>');

            hfIsValid.val("1");
            sectionErrorMessage.html('');
            sectionErrorMessage.hide();
        }

        function clearWarningMessage() {
            var sectionWarningMessage = $('#<%= sectionWarningMessage.ClientID %>');

            sectionWarningMessage.hide();
        }

        function handleWarning(warningMessage) {
            var sectionWarningMessage = $('#<%= sectionWarningMessage.ClientID %>');

            sectionWarningMessage.html(warningMessage);
            sectionWarningMessage.show();
        }

        function handleError(errorMessages) {
            var sectionErrorMessage = $('#<%= sectionErrorMessage.ClientID %>');
            var hfIsValid = $('#<%= hfIsValid.ClientID %>');

            hfIsValid.val("0");
            var errorMessage = "";
            var allMessageSame = true;

            for (var i = 0; i < errorMessages.length - 1; i++) {
                if (errorMessages[i] !== errorMessages[i + 1]) {
                    allMessageSame = false;
                    break;
                }
            }

            if (allMessageSame) {
                errorMessage = errorMessages[0];
            } else {
                for (var i = 0; i < errorMessages.length; i++) {
                    errorMessage = errorMessage + errorMessages[i];

                    if (i < errorMessages.length - 1) {
                        errorMessage = errorMessage + "<br/><br/>";
                    }
                }
            }

            sectionErrorMessage.html(errorMessage);
            sectionErrorMessage.show();
        }

        function handleMultieditError(errorMessages, invalidInstances) {
            var sectionErrorMessage = $('#<%= sectionErrorMessage.ClientID %>');
            var errorMessage = "";

            for (var i = 0; i < errorMessages.length; i++) {
                errorMessage = errorMessage + errorMessages[i];

                if (i < errorMessages.length - 1) {
                    errorMessage = errorMessage + "<br/><br/>";
                }
            }

            sectionErrorMessage.html(errorMessage);
            sectionErrorMessage.show();
        }

        function recompute() {

            clearErrorMessage();
            clearWarningMessage();

            var hfIsValid = $('#<%= hfIsValid.ClientID %>');
            hfIsValid.val("2");

            var tbWarningValue = $('#<%= tbWarningValue.ClientID %>');
            var tbCriticalValue = $('#<%= tbCriticalValue.ClientID %>');
            var ddlOperator = $('#<%= ddlOperator.ClientID %>');
            var lblComputedWarningValue = $('#<%= lblComputedWarningValue.ClientID %>');
            var lblComputedCriticalValue = $('#<%= lblComputedCriticalValue.ClientID %>');
            var hfInvalidInstances = $('#<%= hfInvalidInstances.ClientID %>');

            var operatorId = ddlOperator.val().split("|")[0];

            tbWarningValue.attr("disabled", true);
            tbCriticalValue.attr("disabled", true);
            ddlOperator.attr("disabled", true);

            var request = {
                ThresholdName: "<%= this.ThresholdName %>",
                InstancesId: getObjectsIDs(),
                CriticalFormula: tbCriticalValue.val(),
                WarningFormula: tbWarningValue.val(),
                Operator: operatorId
            };

            SW.Core.Services.callController("/api/Thresholds/Compute", request, function (response) {

                tbWarningValue.attr("disabled", false);
                tbCriticalValue.attr("disabled", false);
                ddlOperator.attr("disabled", false);

                if (isMultiEdit() == false) {
                    lblComputedWarningValue.html("?");
                    lblComputedCriticalValue.html("?");
                }

                if (response.IsValid == false) {
                    if (isMultiEdit() == true) {
                        hfIsValid.val("1");
                        hfInvalidInstances.val(response.InvalidInstances);
                        handleMultieditError(response.ErrorMessages);
                    } else {
                        handleError(response.ErrorMessages);
                    }
                } else {
                    hfIsValid.val("1");
                    hfInvalidInstances.val("");

                    if (response.IsComputed) {
                        lblComputedWarningValue.html("= " + response.WarningThreshold + " " + "<%= DefaultSanitizer.SanitizeHtml(this.Unit) %>");
                        lblComputedCriticalValue.html("= " + response.CriticalThreshold + " " + "<%= DefaultSanitizer.SanitizeHtml(this.Unit) %>");
                    }

                    if (response.WarningMessage) {
                        handleWarning(response.WarningMessage);
                    }
                }
            });
        }


        $('#<%= chbOverrideMultipleObjects.ClientID %>').click(function () {

            var chbOverrideGeneral = $('#<%= chbOverrideGeneral.ClientID %>');
            var sectionThresholdDefinition = $('#<%= sectionThresholdDefinition.ClientID %>');
            var sectionOverrideGeneral = $('#<%= sectionOverrideGeneral.ClientID %>');

            if ($(this).is(':checked')) {
                chbOverrideGeneral.prop("disabled", false);
                sectionOverrideGeneral.show();
                sectionThresholdDefinition.show();
            } else {
                chbOverrideGeneral.prop("disabled", true);
                sectionOverrideGeneral.hide();
                sectionThresholdDefinition.hide();
            }
        });

        $('#<%= chbOverrideGeneral.ClientID %>').click(function () {

            var sectionGeneralThresholds = $('#<%= sectionGeneralThresholds.ClientID %>');
            var sectionCustomThresholds = $('#<%= sectionCustomThresholds.ClientID %>');

            if ($(this).is(':checked')) {
                sectionGeneralThresholds.hide();
                sectionCustomThresholds.show();
                recompute();
            } else {
                sectionGeneralThresholds.show();
                sectionCustomThresholds.hide();
                clearErrorMessage();
                clearWarningMessage();
            }
        });

        $('#<%= tbWarningValue.ClientID %>').change(function () {
            recompute();
        });

        $('#<%= tbCriticalValue.ClientID %>').change(function () {
            recompute();
        });

        $('#<%= btnUseLatestBaseline.ClientID %>').click(function (e) {

            var tbWarningValue = $('#<%= tbWarningValue.ClientID %>');
            var tbCriticalValue = $('#<%= tbCriticalValue.ClientID %>');
            tbWarningValue.val("${USE_BASELINE_WARNING}");
            tbCriticalValue.val("${USE_BASELINE_CRITICAL}");

            var lblComputedWarningValue = $('#<%= lblComputedWarningValue.ClientID %>');
            var lblComputedCriticalValue = $('#<%= lblComputedCriticalValue.ClientID %>');

            if (isMultiEdit()) {
                lblComputedWarningValue.html("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB2_1) %>");
                lblComputedCriticalValue.html("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB2_1) %>");
            }

            recompute();
        });

        $('#<%= ddlOperator.ClientID %>').change(function () {
            var lblOperatorText = $('#<%= lblOperatorText.ClientID %>');
            lblOperatorText.html($(this).find("option:selected").text());

            recompute();
        });

        $('#<%= hlShowDetailDialog.ClientID %>').click(function (e) {

            var operatorId = parseInt($('#<%= ddlOperator.ClientID %>').val().split("|")[0]);
            var instanceId = getObjectsIDs()[0];

            SW.Core.Baselines.showBaselineDetailsDialog({
                id: instanceId,
                thresholdName: "<%= this.ThresholdName %>",
                thresholdDisplayName: "<%= DefaultSanitizer.SanitizeHtml(this.DisplayName) %>",
                operator: operatorId,
                returnFunction: ApplyThresholdValuesOf_<%= this.ThresholdName.Replace(".","_")%>
                <% if(!string.IsNullOrWhiteSpace(StatisticsFormatterFunction)) { %>
                , statisticsFormatter: <%= StatisticsFormatterFunction %>
                <% } %>
            });
        });
    }
</script>

<asp:HiddenField runat="server" ID="hfIsValid" Value="1"/>
<asp:HiddenField runat="server" ID="hfInvalidInstances"/>

<table class="thresholds">
    <tr>
        <td class="leftLabelColumn" style="text-align: left;">
            <asp:CheckBox runat="server" ID="chbOverrideMultipleObjects"/><%= DefaultSanitizer.SanitizeHtml(this.DisplayName) %>
        </td>
        <td class="rightInputColumn">
            <div runat="server" ID="sectionOverrideGeneral">
                <asp:CheckBox Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TK0_45  %>" runat="server" ID="chbOverrideGeneral" /> <%--Override Orion General Thresholds --%>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px; padding-right: 20px;">
            <div runat="server" ID="sectionThresholdDefinition" class="thresholdControl">
                <table width="100%">
                    <tr>
                        <td>
                            <div ID="sectionGeneralThresholds" runat="server" class="thresholdGlobal">
                                <table>
                                    <tr>
                                        <td style="width: 30px">
                                            <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>" src="/Orion/images/ThresholdControl/Small-Up-Warn.gif" />
                                        </td>
                                        <td style="width: 180px">
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>: <%-- Warning --%>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblGeneralWarningValue"></asp:Label> <%-- greater than --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>" src="/Orion/images/ThresholdControl/Small-Up-Critical.gif" />
                                        </td>
                                        <td>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>: <%-- Critical --%>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblGeneralCriticalValue"></asp:Label> <%-- greater than --%>
                                        </td>
                                    </tr>
                                    <%-- Global Capacity Planning --%>
                                    <tr id="capacityPlanningGlobal" runat="server">
                                        <td>
                                            <img src="/Orion/images/ForecastingIcons/capPlanIcon24x16.gif" alt="Capacity Trending Icon" />
                                        </td>
                                        <td>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_11) %> <%-- Capacity Trending --%>
                                        </td>
                                        <td>
                                            <asp:Literal ID="capacityPlanningGlobalValue" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div ID="sectionCustomThresholds" runat="server" class="thresholdCustom" style="width: 100%; display: none;">
                                <table style="width: 100%;" cellspacing="10">
                                    <tr style="height: 30px;">
                                        <td style="width: 30px"><img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>" src="/Orion/images/ThresholdControl/Small-Up-Warn.gif" /></td>
                                        <td style="width: 140px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_46) %>:</td>
                                        <td style="width: 180px;"><asp:DropDownList cssclass="ddlInTable" runat="server" ID="ddlOperator" /></td>
                                        <td style="width: 225px; white-space: nowrap;"><asp:TextBox runat="server" ID="tbWarningValue" CssClass="formulaTextbox"></asp:TextBox><%= DefaultSanitizer.SanitizeHtml(this.Unit) %></td>
                                        <td ID="tdComputedWarningValue" runat="server"><asp:Label runat="server" ID="lblComputedWarningValue"></asp:Label></td>
                                        <td rowspan="2" style="width: 280px;">
                                            <div runat="server" ID="sectionLatestBaseline" class="relatedActionBlock">
                                                <table>
                                                    <tr>
                                                        <td><asp:Button runat="server" ID="btnUseLatestBaseline" UseSubmitBehavior="False" OnClientClick="return false;" CssClass="button sw-btn sw-btn-primary" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TK0_48  %>" ></asp:Button></td><%-- Use Dynamic Baseline Thresholds--%>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div runat="server" ID="sectionDetailHyperlink">
                                                                <asp:HyperLink runat="server" ID="hlShowDetailDialog" Text="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_49) %>"></asp:HyperLink><%--» Latest Baseline Details--%>
                                                            </div>
                                                            <div class="info">
                                                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_50) %> <a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionCoreAGBaselineDataCalculation")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_51) %></a>  <%-- Thresholds calculated using baseline data allow for more accurate alerting. --%> <%-- » Learn more --%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="height: 30px;">
                                        <td><img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>" src="/Orion/images/ThresholdControl/Small-Up-Critical.gif" /></td>
                                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_47) %>:</td>
                                        <td><asp:Label runat="server" ID="lblOperatorText"></asp:Label></td>
                                        <td style="white-space: nowrap;"><asp:TextBox runat="server" ID="tbCriticalValue" CssClass="formulaTextbox" ></asp:TextBox><%= DefaultSanitizer.SanitizeHtml(this.Unit) %></td>
                                        <td><asp:Label runat="server" ID="lblComputedCriticalValue"></asp:Label></td>
                                    </tr>
                                    <%-- Local Capacity Planning --%>
                                    <tr id="capacityPlanning" class="capacityPlanningRow" runat="server">
                                        <td>
                                            <img src="/Orion/images/ForecastingIcons/capPlanIcon24x16.gif" alt="Capacity Trending Icon" />
                                        </td>
                                        <td>
                                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_11) %>: <%-- Capacity Trending --%>
                                        </td>
                                        <td colspan="4">
                                            <asp:RadioButton ID="capPlanTypeAverage" Text="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_09) %>" GroupName="capPlanTypeGroup" runat="server" />
                                            &nbsp;
                                            <asp:RadioButton ID="capPlanTypePeak" Text="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PD0_10) %>" GroupName="capPlanTypeGroup" runat="server" />
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div runat="server" id="sectionErrorMessage" style="overflow-y: auto; max-height:200px; width: 700px; display: none;" class="sw-suggestion sw-suggestion-fail"></div>
                            <div runat="server" id="sectionWarningMessage" style="width: 700px; display: none;" class="sw-suggestion sw-suggestion-warn"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>