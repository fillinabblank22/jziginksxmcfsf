﻿<%@ Control Language="C#" ClassName="IncludeExtJs" %>

<script runat="server">
    private bool _debug = false;
    private string _version = string.Empty;
    
    public bool Debug
    {
        get { return _debug; }
        set { _debug = value; }
    }
    public string Version
    {
        get { return _version; }
        set { _version = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        OrionInclude.CoreFramework(Debug ? OrionInclude.Framework.ExtDebug : OrionInclude.Framework.Ext, Version)
            .SetSection(OrionInclude.Section.Middle)
            .SetOrder(50); // allow scripts to be orderd ahead of extjs, if needed.
    }
</script>
