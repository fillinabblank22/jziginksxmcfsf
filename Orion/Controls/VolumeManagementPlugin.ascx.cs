﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Volume = SolarWinds.Orion.NPM.Web.Volume;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_NetPerfMon_Resources_VolumeDetails_VolumeManagementPlugin : ManagementTasksPluginBase
{
    public override IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        var volumeNetObject = netObject as Volume;

        if (volumeNetObject == null || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            yield break;

        var sectionName = Resources.CoreWebContent.WEBDATA_AK0_183;

        string maintenanceModeGroupLabel = $"<img src='/Orion/Nodes/images/icons/maintenance_mode_icon16.png' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_MH0_01}";

        if (Profile.AllowNodeManagement)
        {
            yield return new ManagementTaskItem
            {
                Section = sectionName,
                ClientID = "editVolume",
                LinkInnerHtml = string.Format(
                    "<img src='/Orion/Nodes/images/icons/icon_edit.gif' alt='' />&nbsp;{0}", Resources.CoreWebContent.WEBDATA_TM0_110),
                LinkUrl = string.Format("/Orion/Nodes/VolumeProperties.aspx?Volumes={0}&ReturnTo={1}", volumeNetObject.VolumeID, returnUrl)
            };

            yield return new ManagementTaskItem
            {
                Section = sectionName,
                ClientID = "pollNowLnk",
                LinkInnerHtml =
                    "<img src='/Orion/images/pollnow_16x16.gif' alt='' />&nbsp;" +
                    Resources.CoreWebContent.WEBDATA_VB0_117,
                OnClick = string.Format("return showPollNowDialog(['{0}']);", volumeNetObject.NetObjectID)
            };

            yield return new ManagementTaskItem
            {
                Section = sectionName,
                ClientID = "rediscoverLnk",
                LinkInnerHtml =
                    string.Format("<img src='/Orion/Nodes/images/icons/icon_discover.gif' alt='' />&nbsp;{0}",
                        Resources.CoreWebContent.WEBDATA_VB0_118),
                OnClick = string.Format("return showRediscoveryDialog(['{0}']);", volumeNetObject.NetObjectID)
            };
        }

        if (Profile.AllowAlertManagement)
        {
            yield return new ManagementTaskItem
            {
                Section = sectionName,
                LinkInnerHtml = $"<img src='/Orion/Nodes/images/icons/CreateAlertOnThisNode_icons16x16v1.png' alt='' />&nbsp;{Resources.CoreWebContent.Add_New_Alert}",
                LinkUrl = $"/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={Guid.NewGuid()}&ObjectType=Volume&Object=<this>.Orion.Volumes|instance&Uri={HttpUtility.UrlEncode(volumeNetObject.Uri)}"
            };
        }

        if (Profile.AllowUnmanage)
        {
            if (!(volumeNetObject.UnManaged || volumeNetObject.Node.UnManaged))
            {
                yield return new ManagementTaskItem
                {
                    Section = sectionName,
                    Group = maintenanceModeGroupLabel,
                    MouseHoverTooltip = Resources.CoreWebContent.WEBDATA_MH0_03,
                    ClientID = "unmanageNowLink",
                    LinkInnerHtml =
                        string.Format("<img src='/Orion/Nodes/images/icons/icon_manage.gif' alt='' />&nbsp;{0}",
                            GetLocalizedString("@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow;E=xml}")),
                    OnClick = IsDemoServer ? GetDemoActionCallback("Core_Alerting_Unmanage") :
                        string.Format("return SW.Core.MaintenanceMode.UnmanageNowSingleEntity('{0}');", volumeNetObject.NetObjectID)
                };
            }

            if (volumeNetObject.UnManaged && !volumeNetObject.Node.UnManaged)
            {
                yield return new ManagementTaskItem
                {
                    Section = sectionName,
                    Group = maintenanceModeGroupLabel,
                    ClientID = "remanageLink",
                    LinkInnerHtml =
                        string.Format("<img src='/Orion/Nodes/images/icons/icon_remanage.gif' alt='' />&nbsp;{0}",
                            GetLocalizedString("@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain;E=xml}")),
                    OnClick = IsDemoServer ? GetDemoActionCallback("Core_Alerting_Unmanage") :
                        string.Format("return SW.Core.MaintenanceMode.ManageAgainSingleEntity('{0}');", volumeNetObject.NetObjectID)
                };
            }
        }

        yield return new ManagementTaskItem
        {
            Section = sectionName,
            ClientID = "performanceAnalyzer",
            LinkInnerHtml = string.Format("<img src='/Orion/PerfStack/images/icons/perfstack_launch.svg' height='16' width='16' alt='' />&nbsp;{0}",
                Resources.CoreWebContent.WEBDATA_Management_PerformanceAnalyzer),
            LinkUrl = string.Format("/ui/perfstack/?presetTime=last12Hours&charts=0_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskQueueLength;0_Orion.Volumes_{0}-" + 
                "Orion.VolumePerformanceHistory.AvgDiskReads,0_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskWrites;0_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskTransfer;" + 
                "0_Orion.Volumes_{0}-Orion.VolumeUsageHistory.AvgDiskUsed,0_Orion.Volumes_{0}-Orion.VolumeUsageHistory.PercentDiskUsed;0_Orion.Volumes_{0}-Orion.PerfStack.Status;", volumeNetObject.VolumeID)
        };
    }
}