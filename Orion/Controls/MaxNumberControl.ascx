<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaxNumberControl.ascx.cs" Inherits="MaxNumberControl" %>
<table>
<tr>
<td align="justify" style="height: 40px; width: 30%">
<asp:Label ID="Title" runat="server" Font-Bold="True"></asp:Label>
</td>
<td align="justify" style="height: 40px; width: 70%" colspan = "2">
<asp:TextBox ID="MaxNumber" runat="server">250</asp:TextBox>
</td>
</tr>
<tr><td style="width: 30%"></td>
<td colspan="3" style="width: 70%">
<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="MaxNumber" 
    ErrorMessage="Please enter correct number from 1 till 1000" MaximumValue="1000" MinimumValue="1" Type="Integer" Display="Dynamic"></asp:RangeValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="MaxNumber"
    Display="Dynamic" ErrorMessage="Please enter correct number from 1 till 1000"></asp:RequiredFieldValidator>
</td>
</tr>
</table>
