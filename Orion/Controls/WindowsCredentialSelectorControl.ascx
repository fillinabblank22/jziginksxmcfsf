<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WindowsCredentialSelectorControl.ascx.cs" Inherits="Orion_Controls_WindowsCredentialSelectorControl" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<orion:Include runat="server" File="WindowsCredentialSelectorControl.js" />

<style>
    .paragraph { padding-top: 10px; }
    #userCredentials { background-color: #ecedee; padding: 5px; }
    #userCredentials [data-form=credentialsSelect] { width: 137px; }
    #userCredentials img.edit-icon { position: relative; top: 3px; }
    #userCredentials .error-message { color: red; }
    #userCredentials .smallText { color: #979797; font-size: 9px; }
    #userCredentials .field-label { float: right; }
    #userCredentials .leftLabelColumn, #userCredentials .rightInputColumn { padding-bottom: 5px; }
    #userCredentials .modifiedLeftLabelColumn { padding-bottom: 15px; }
    #userCredentials .sw-suggestion-warn { max-width: 549px;; }
    .selectionContainer .radio-label { vertical-align: top; }
</style>

<div class="contentBlock">
    <div class="selectionContainer">
        <div class="paragraph">
             <label> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_WindowsAuthentication) %></label>
        </div>
        <div class="paragraph">
             <div>
                <input id="useDefaultUser" type="radio" onclick="SW.Core.WindowsCredentialSelectorControl.radioSelectionChange(this);"
                    runat="server" value="0" />
                <span class="radio-label">
                    <%= String.Format("{0} ({1})", ControlHelper.EncodeJsString(Resources.CoreWebContent.WindowsCredentialSelector_DefaultUser), defaultUserName) %>
                </span>
            </div>
            <div>
                <input id="useDefinedUser" type="radio" onclick="SW.Core.WindowsCredentialSelectorControl.radioSelectionChange(this);"
                    runat="server" value="1" />
                <span class="radio-label">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_DefineUser) %>
                </span>
            </div>
        </div>
    </div>
    <div class="blueBox" id="userCredentials">
        <table>
            <tr>
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="lblTemplateLabel" CssClass="field-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_ChooseCredential) %>&nbsp;</asp:Label>
                </td>
                <td class="rightInputColumn" colspan="2">
                    <asp:DropDownList ID="DdlWmiCredentials" runat="server" data-form="credentialsSelect" onchange="SW.Core.WindowsCredentialSelectorControl.credentialChange(this);">
                    </asp:DropDownList>
                    &nbsp;
                    <%if ((this.Profile.AllowAdmin) || (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
                        {%>
                    <span>
                        <img class="edit-icon" src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                        <a href="/Orion/Admin/Credentials/CredentialManager.aspx" id="manage" target="_blank" class="RenewalsLink">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_ManageWindowsCredentials) %> </a>
                    </span>
                    <%} %>
                </td>
            </tr>
            <tr>
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label1" CssClass="field-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_CredentialName) %>&nbsp;</asp:Label>
                </td>
                <td class="rightInputColumn">
                    <asp:TextBox ID="tbCaption" runat="server" MaxLength="50" data-form="credName"></asp:TextBox>
                    <span id="requiredCredentialName" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_CredentialNameRequired) %></span>
                    <span id="credentialNameAlreadyExists" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_CredentialNameExists) %></span>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="modifiedLeftLabelColumn">
                    <asp:Label runat="server" ID="Label3" CssClass="field-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_Username) %>&nbsp;</asp:Label>
                </td>
                <td class="rightInputColumn">
                    <asp:TextBox ID="tbLogin" runat="server" MaxLength="50" data-form="userName"></asp:TextBox>
                    <span id="requiredUserName" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_UsernameRequired) %></span>
                    <div class="smallText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_UsernameHint) %></div>
                </td>
            </tr>
            <tr>
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label5" CssClass="field-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_Password) %>&nbsp;</asp:Label>
                </td>
                <td class="rightInputColumn">
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="127" autocomplete="off" data-form="password"></asp:TextBox>
                    <span id="requiredPassword" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_PasswordRequired) %></span>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label7" CssClass="field-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_ConfirmPassword) %>&nbsp;</asp:Label>
                </td>
                <td class="rightInputColumn">
                    <asp:TextBox ID="tbPasswordConfirmation" runat="server" TextMode="Password" MaxLength="127" autocomplete="off" data-form="confirmPassword"></asp:TextBox>
                    <span id="requiredConfirmPassword" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_PasswordRequired) %></span>
                    <span id="compareConfirmPassword" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_PasswordsMustBeSame) %></span>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <div style="padding-top: 5px">
        <span id="testProgress" style="display: none;">
            <img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_Wait) %>" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %></span>
        <div id="testResultSuccess" class="sw-suggestion sw-suggestion-pass" style="display: none;
            margin-bottom: 10px; margin-left: 0px;">
            <span class="sw-suggestion-icon"></span>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_TestSuccessful) %></div>
        <div id="testResultFailed" style="display: none;">
            <div class="sw-suggestion sw-suggestion-fail" style="margin-bottom: 10px; margin-left: 0px;"><span class="sw-suggestion-icon"></span>
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_TestFailed) %></span>
                <div id="testResultFailedDetail" style="display: none;"></div>
            </div><br/>
            <div class="sw-suggestion sw-suggestion-warn" style="margin-bottom: 10px; margin-left: 0px;"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_TestFailedHint) %><br/>
                <a class="sw-link" target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Common.KnowledgebaseLinkHelper.GetSalesForceKBUrl(8058)) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WindowsCredentialSelector_TestFailedHintLink) %></a>
            </div>
        </div>
        <div>
            <orion:LocalizableButton runat="server" ID="btnTestCredentials" Text="<%$ HtmlEncodedResources: CoreWebContent, WindowsCredentialSelector_TestCredentials %>"
                OnClientClick="SW.Core.WindowsCredentialSelectorControl.testCredentials();return false"
                DisplayType="Small" />
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        SW.Core.namespace("SW.Core").WindowsCredentialSelectorControl = new SW.Core.WindowsCredentialSelectorSettings();
        SW.Core.WindowsCredentialSelectorControl.init();
    });
</script>
