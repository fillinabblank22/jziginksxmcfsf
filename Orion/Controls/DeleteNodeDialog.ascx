<%@ Control Language="C#" ClassName="DeleteNodeDialog" %>
<orion:include runat="server" File="js/OrionMaster.js" />
<script type="text/javascript">
//<![CDATA[
        
    function showDeleteNodeDialog(ids) {
        SW.Core.MessageBox.Dialog({
            title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Delete_node_dialog_title) %>',
            modal: true,
            html: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Delete_node_dialog_message) %>' + '<br/><br/>',
            buttons: [
                {
                    id: 'confirmDeletion',
                    html: '<span style="margin-right: 130px;">' +
                        $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_149) %>', { type: 'secondary', id: 'confirmDeletion' })).css('margin-right', '130px;')[0].outerHTML
                        + '</span>',
                    handler: function (msgbox) {
                        var onSucc = function () {
                            msgbox.dialog('close');
                            window.location.href = '/Orion/SummaryView.aspx';
                        };
                        var onFail = function () {alert('Error: Node not deleted')};
                        NodeManagement.DeleteObjNow(ids, onSucc, onFail, null);
                    }
                },
                {
                    id: 'cancelDeletion',
                    html: SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_150) %>', { type: 'primary', id: 'cancelDeletion' }),
                    handler: function (msgbox) {
                        msgbox.dialog('close');
                    }
                }
            ]
        });
    };
      
//]]>    
</script>

