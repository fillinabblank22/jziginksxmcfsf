﻿using System;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Settings;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Controls_PageHeader : System.Web.UI.UserControl
{
    public class Info : Control, INamingContainer
    {
        public Orion_Controls_PageHeader Companion { get; set; }
        public string UserNameLabel { get { return Companion.UsernameLabel; } }
        public string UserName { get { return Companion.UserName; } }
        public bool AllowAdmin { get { return Companion.AllowAdmin; } }
        public bool IsPrintMode { get { return Companion.IsPrintMode; } }
        public bool IsMobileMode { get { return Companion.IsMobileMode; } }
    }

    private static readonly Log log = new Log();

    private static IFeatureManager featureManager;

    private static Lazy<bool> isOgsWebIntalled = new Lazy<bool>(() =>
    {
        try
        {
            // we can't use SolarWinds.Orion.Core.Common.ModuleManager.ModuleManager.InstanceWithCache.IsThereModule("OGS") because OGS doesn't have license
            var modulesDescription = OrionModuleManager.GetInstalledModules();
            bool isInstalled = modulesDescription.Any(moduleDescription => (moduleDescription.ProductShortName == "OGS" && !string.IsNullOrEmpty(moduleDescription.NavigationTabName)));
            return isInstalled;
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Error checking for OGS module: {0}", ex);
        }

        return false;
    },
		System.Threading.LazyThreadSafetyMode.PublicationOnly
    );

    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateContainer(typeof(Info))]
    public ITemplate MobileTemplate { get; set; }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateContainer(typeof(Info))]
    public ITemplate NormalTemplate { get; set; }

    public bool Minimalistic { get; set; }

    protected bool IsOGSWebInstalled
    {
        get { return isOgsWebIntalled.Value; }
    }

    protected bool IsSearchEnabled
    {
        get { return FeatureToggles.Instance.SwSearch; }
    }

    private static readonly bool _isDemoServer = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    protected bool IsDemoServer
    {
        get { return _isDemoServer; }
    }

    protected bool IsErrorPage
    {
        get
        {
            var context = HttpContext.Current;

            if (context == null)
                return false;

            var request = context.Request;
            var url = request.CurrentExecutionFilePath ?? String.Empty;
            return url.Equals("/Orion/Error.aspx", StringComparison.OrdinalIgnoreCase);
        }
    }

    protected string SiteLogoUri
    {
        get
        {
            string logo;
            if (SolarWinds.Orion.Web.DAL.WebSettingsDAL.TryGetLogo(out logo))
            {
                return "/Orion/LogoImageHandler.ashx?f=logo&id=SitelogoImage&time=" + DateTime.Now.Ticks;
            }
            else
                return logo;
        }
    }

    protected bool ShowLogo
    {
        get
        {
            string logo;
            SolarWinds.Orion.Web.DAL.WebSettingsDAL.TryGetLogo(out logo);
            return !string.IsNullOrEmpty(logo);
        }
    }

    protected string UsernameLabel
    {
        get
        {
            try
            {
                if (Profile.IsAnonymous)
                {
                    //UsernameLabel is called in AnonymousTemplate, maybe for some Miscrosoft bug. Added by changelist 376688 / FB 325732
                    //Adding this to avoid useless exceptions.
                    return string.Empty;
                }

                string groupName = string.Empty;
                if (Profile.AccountType == 4 && Profile.GroupInfo.Length > 0)
                {
                    groupName = " - " + Profile.GroupInfo;
                }
                return Profile.UserName + groupName;
            }
            catch (Exception ex)
            {
                if (OrionMinReqsMaster.IsErrorPage == false) //This exception may be thrown under IIS6 when unexpected exception thrownsomewhere. No need to log this if we're on error page.
                {
                    log.Error(ex);
                }
                return string.Empty;
            }
        }
    }

	protected string HelpUrl
	{
		get
		{
			return string.Format(HelpHelper.GetHelpLinkTemplate(), string.Empty);
		}
	}

    protected string UserName
    {
        get
        {
            try
            {
                return Profile.UserName;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error getting username: {0}", ex);
                return string.Empty;
            }
        }
    }

    protected bool AllowAdmin
    {
        get
        {
            try
            {
                return Profile.AllowAdmin;
            }
            catch
            {
                // swallow the exception
                return false;
            }
        }
    }

    protected bool IsPrintMode
    {
        get
        {
            return OrionMinReqsMaster.IsPrintable;
        }
    }

    private bool? ismobile;
    protected bool IsMobileMode
    {
        get
        {
            return ismobile.HasValue ? ismobile.Value : 
                (ismobile = CommonWebHelper.IsMobileView(Request)).Value;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        DataBind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (OrionMinReqsMaster.IsPrintable)
        {
            // if printable, hide the header.
            Visible = false;
        }

        bool isMobile = CommonWebHelper.IsMobileView(Request);

        var info = new Info { Companion = this };

        if (isMobile)
        {
            if (MobileTemplate != null)
                MobileTemplate.InstantiateIn(info);
        }
        else
        {
            if (NormalTemplate != null)
                NormalTemplate.InstantiateIn(info);

            if (!IsPrintMode && !Minimalistic)
            {
                EvalBannerHolder.Controls.Add(new EvalBanner { RenderMode = EvalBannerRenderMode.Eval, CssClass = "sw-mainnav-eval-header" });
                EvalBannerHolder.Controls.Add(new EvalBanner { RenderMode = EvalBannerRenderMode.RC });
                info.Controls.Add(new SWTab { User = UserName, AllowAdmin = AllowAdmin });
            }
        }
        
        if (info.Controls.Count > 0)
        {
            TemplateHolder.Controls.Add(new LiteralControl("<div class='main-menu-container'>"));
            TemplateHolder.Controls.Add(info);

            if (!IsPrintMode && !Minimalistic)
                NotificationsHolder.Controls.Add(new SWNotificationPanel());

            TemplateHolder.Controls.Add(new LiteralControl("</div>"));
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
        }
        catch (Exception)
        {
            if (OrionMinReqsMaster.IsErrorPage == false)
                throw;
        }
    }
}