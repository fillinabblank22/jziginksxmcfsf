<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkDeviceControl.ascx.cs" Inherits="NetworkDeviceControl" %>

<table>
<tr>
<td align="justify" style="height: 40px; width: 30%">
<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_203) %></b>
</td>
<td align="justify"  style="height: 40px; width: 30%">
<asp:DropDownList ID="NetworkDeviceList" runat="server">
</asp:DropDownList>
</td>
<td align="justify" style="height: 40px; width: 40%">
<asp:Label ID="Description" runat="server" Height="35px"></asp:Label>
</td>
</tr>
</table>
