using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using Limitation = SolarWinds.Orion.Web.Limitation;

[Obsolete("Does now work with Alerting V2")]
public partial class AlertsReportControl : UserControl
{
	private static Dictionary<string, string> _netObjectTypes;
	private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	private const string DetailsViewPattern = "../View.aspx?NetObject={0}:{1}";

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

    #region properties
    [PersistenceMode(PersistenceMode.Attribute)]
	public bool Printable
	{
		set;
		get;
	}

	public bool AllowEventClear
	{
		get
		{
			bool result;
			if (bool.TryParse(HttpContext.Current.Profile.PropertyValues["AllowEventClear"].PropertyValue.ToString(), out result))
			{
				return result;
			}

			return false;
		}
	}

    public string SortString
    {
        get
        {
            object val = this.ViewState["sortString"];
            if (val != null && val is string)
                return (string)val;

            return "AlertTime";
        }
        set
        {
            this.ViewState["sortString"] = value;
        }
    }

    public SortDirection SortDir
    {
        get
        {
            object val = this.ViewState["sortingDirection"];
            if (val != null && val is SortDirection)
                return (SortDirection)val;

            return SortDirection.Descending;
        }
        set
        {
            this.ViewState["sortingDirection"] = value;
        }
    }

    public DataTable AlertsTable
    {
        get
        {
            object val = this.ViewState["alertsTable"];
            if (val != null && val is DataTable)
            {
                return (DataTable)val;
            }
            return new DataTable();
        }
        set
        {
            this.ViewState["alertsTable"] = value;
        }
    }
    #endregion 

	public void GenerateReport(string netObjectId, string deviceType, string alertId, int maxRecords, bool showAcknowledged)
	{		
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
            AlertsTable = proxy.GetSortableAlertTable(netObjectId, deviceType, alertId, "AlertTime DESC ", maxRecords, showAcknowledged, Limitation.GetCurrentListOfLimitationIDs(), true);
		}

        ShowDirImage();
        SortOnPage(SortString, SortDir);
        PublishData();

		AlertActionsTable.Visible = Profile.AllowEventClear
			&& !Printable && (table.Items.Count >= CommonConstants.MIN_ROWS_NUMBER);
	}

	protected static string ReformatDateTime(string dateTimeString)
	{
		DateTime dateTime;
		if (DateTime.TryParse(dateTimeString, out dateTime))
		{
			return dateTime.ToString("g", CultureInfo.CurrentCulture);
		}

		return dateTimeString;
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (_netObjectTypes != null) return;
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			_netObjectTypes = proxy.GetNetObjectData();
		}
	}

	private static string GetObjectPrefix(string objectType, string messageType)
	{
		// if basic alert then objectType is equals to object prefix
		if (!messageType.Equals(OrionMessagesHelper.GetMessageTypeString(OrionMessageType.ADVANCED_ALERT), 
			StringComparison.OrdinalIgnoreCase))
			return objectType;

		// try to look for object prefix within the dictionary
		foreach (var type in _netObjectTypes.Where(type => type.Key.Equals(objectType, StringComparison.OrdinalIgnoreCase))) {
			return type.Value;
		}

		// if there isn't prefix for this object type 
		// within the _netObjectTypes dictionary  then return null
		return null;
	}

	protected string GetNetObjectIdByObjectType(string objectType, string objectId, string messageType)
	{
		var prefix = GetObjectPrefix(objectType, messageType);
		return (prefix == null) ? string.Empty : string.Format("{0}:{1}", prefix, objectId);
	}

	protected bool AbleToAcknowledge(object acknowledged, object monitoredProperty)
	{
		if (acknowledged == null || monitoredProperty == null) return false;

		return acknowledged.ToString().Equals("0", StringComparison.OrdinalIgnoreCase)
			&& monitoredProperty.ToString().Equals(OrionMessagesHelper.GetMessageTypeString(OrionMessageType.ADVANCED_ALERT));
	}

	protected void TableItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

		var cell = (HtmlTableCell)e.Item.FindControl("netObjectTD");
		var text = new Literal
					{
						Text = DataBinder.Eval(e.Item.DataItem, "ObjectName").ToString()
					};

		var netObjPrefix = DataBinder.Eval(e.Item.DataItem, "NetObjectPrefix").ToString();
		var netObjId = DataBinder.Eval(e.Item.DataItem, "ActiveNetObject").ToString();

		if (!string.IsNullOrEmpty(netObjPrefix) && !string.IsNullOrEmpty(netObjId))
		{
			var anchor = new HtmlAnchor { HRef = String.Format(DetailsViewPattern, netObjPrefix, netObjId) };
			anchor.Controls.Add(text);
			cell.Controls.Add(anchor);
		}
		else
		{
			cell.Controls.Add(text);
		}
	}

    protected void SortChange(object sender, EventArgs e)
    {
        if (sender as LinkButton == null) return;

        string ID = (sender as LinkButton).ID;
        switch (ID.ToLowerInvariant())
        {
            case "sortbytime":
                if (SortString == "AlertTime") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "AlertTime"; SortDir = SortDirection.Descending; }
                break;
            case "sortbyalertname":
                if (SortString == "AlertName") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "AlertName"; SortDir = SortDirection.Descending; }
                break;
            case "sortbyalerttype":
                if (SortString == "MonitoredProperty") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "MonitoredProperty"; SortDir = SortDirection.Descending; }
                break;
            case "sortbycaption":
                if (SortString == "ObjectName") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "ObjectName"; SortDir = SortDirection.Descending; }
                break;
            case "sortbyacknowledgedby":
                if (SortString == "AcknowledgedBy") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "AcknowledgedBy"; SortDir = SortDirection.Descending; }
                break;
            case "sortbyacknowledgedtime":
                if (SortString == "AcknowledgedTime") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
                else { SortString = "AcknowledgedTime"; SortDir = SortDirection.Descending; }
                break;
        }
        
    }

    private void ShowDirImage()
    {
        TimeDirImage.Controls.Clear();
        AlertNameDirImage.Controls.Clear();
        AlertTypeDirImage.Controls.Clear();
        CaptionDirImage.Controls.Clear();
        AcknowledgedByDirImage.Controls.Clear();
        AcknowledgedTimeDirImage.Controls.Clear();

        string img = (SortDir == SortDirection.Ascending) ? "/Orion/images/sortable_arrow_up.gif" : "/Orion/images/sortable_arrow_down.gif";

        switch (SortString)
        {
            case "AlertTime":
                TimeDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "AlertName":
                AlertNameDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "MonitoredProperty":
                AlertTypeDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "ObjectName":
                CaptionDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "AcknowledgedBy":
                AcknowledgedByDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
            case "AcknowledgedTime":
                AcknowledgedTimeDirImage.Controls.Add(new LiteralControl(string.Format("<img src=\"{0}\" alt=''/>", img)));
                break;
        } 
    }

    private void SortOnPage(string column, SortDirection direction)
    {
        if (AlertsTable.Rows.Count == 0)
            return;

        string sortD = " DESC";
        if (direction == SortDirection.Ascending) sortD = " ASC";

        AlertsTable.DefaultView.Sort = AlertsTable.Columns[column] + sortD;
    }

    private void PublishData()
    {
        table.DataSource = AlertsTable;
        table.DataBind();
    }
}
