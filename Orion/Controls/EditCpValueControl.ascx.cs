﻿using System;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using System.Web;

public partial class Orion_Controls_EditCpValueControl : UserControl
{
    private const string AddNewSelValue = "-";
    private const string SelNoneValue = "-none-";

    #region public properties

    [PersistenceMode(PersistenceMode.Attribute)]
    public Type PropertyType
    {
        get
        {
            if (ViewState[string.Format("PropertyType_{0}", ClientID)] == null)
            {
                ViewState[string.Format("PropertyType_{0}", ClientID)] = typeof (string);
            }
            return (Type) ViewState[string.Format("PropertyType_{0}", ClientID)];
        }
        set { ViewState[string.Format("PropertyType_{0}", ClientID)] = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool BlockAddingValues { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Mandatory
    {
        get
        {
            if (ViewState[string.Format("Mandatory_{0}", ClientID)] == null)
            {
                ViewState[string.Format("Mandatory_{0}", ClientID)] = false;
            }
            return (bool) ViewState[string.Format("Mandatory_{0}", ClientID)];
        }
        set { ViewState[string.Format("Mandatory_{0}", ClientID)] = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Default
    {
        get
        {
            if (ViewState[string.Format("Default_{0}", ClientID)] == null)
            {
                ViewState[string.Format("Default_{0}", ClientID)] = string.Empty;
            }
            return (string) ViewState[string.Format("Default_{0}", ClientID)];
        }
        set { ViewState[string.Format("Default_{0}", ClientID)] = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Text
    {
        get { return GetSelectedValue(true); }
        set { SelectValue(value); }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string[] EnumerationValues
    {
        get
        {
            if (ViewState[string.Format("EnumerationValues_{0}", ClientID)] == null)
            {
                ViewState[string.Format("EnumerationValues_{0}", ClientID)] = new string[0];
            }
            return (string[]) ViewState[string.Format("EnumerationValues_{0}", ClientID)];
        }
        set { ViewState[string.Format("EnumerationValues_{0}", ClientID)] = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string[] ObjectIds
    {
        get
        {
            if (ViewState[string.Format("ObjectIds_{0}", ClientID)] == null)
            {
                ViewState[string.Format("ObjectIds_{0}", ClientID)] = new string[0];
            }
            return (string[]) ViewState[string.Format("ObjectIds_{0}", ClientID)];
        }
        set { ViewState[string.Format("ObjectIds_{0}", ClientID)] = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool EditObjectMode
    {
        get
        {
            if (ViewState[string.Format("EditObjectMode_{0}", ClientID)] == null)
            {
                ViewState[string.Format("EditObjectMode_{0}", ClientID)] = false;
            }
            return (bool) ViewState[string.Format("EditObjectMode_{0}", ClientID)];
        }
        set { ViewState[string.Format("EditObjectMode_{0}", ClientID)] = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int Size
    {
        get
        {
            if (ViewState[string.Format("Size_{0}", ClientID)] == null)
            {
                ViewState[string.Format("Size_{0}", ClientID)] = -1;
            }
            return (int) ViewState[string.Format("Size_{0}", ClientID)];
        }
        set { ViewState[string.Format("Size_{0}", ClientID)] = value; }
    }

    public bool Enabled
    {
        get
        {
            if (ViewState[string.Format("Enabled_{0}", ClientID)] == null)
            {
                ViewState[string.Format("Enabled_{0}", ClientID)] = true;
            }
            return (bool) ViewState[string.Format("Enabled_{0}", ClientID)];
        }
        set
        {
            ViewState[string.Format("Enabled_{0}", ClientID)] = value;

            TextBoxEditorValue.Enabled = value;
            TextBoxEditorValue.CssClass = value ? string.Empty : "disabled";

            YesNoValue.Enabled = value;
            YesNoValue.CssClass = value ? string.Empty : "disabled";

            txtDatePicker.Enabled = value;
            txtDatePicker.CssClass = value ? "datePicker disposable" : "disabled";

            txtTimePicker.Enabled = value;
            txtTimePicker.CssClass = value ? "timePicker disposable" : "disabled";

            RestrictedValues.Enabled = value;
            RestrictedValues.CssClass = value ? string.Empty : "disabled";

            AdditionalRestrictedValue.Enabled = value;
            AdditionalRestrictedValue.CssClass = value ? string.Empty : "disabled";
        }
    }

    #endregion

    public void Clear()
    {
        RestrictedValues.Items.Clear();
    }

    private void EnsureRestrictedValuesPopulated()
    {
        if (RestrictedValues.Items.Count == 0)
        {
            RestrictedValues.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_AK1_10, SelNoneValue));

            foreach (var val in EnumerationValues)
            {
                if (string.IsNullOrEmpty(val)) continue;
                // convert restricted values to current culture to show on web site
                var currentValue = CustomPropertyHelper.GetCurrentCultureString(val, PropertyType);
                RestrictedValues.Items.Add(new ListItem(currentValue, currentValue));
            }

            if (this.Mandatory && !string.IsNullOrEmpty(this.Default) && EnumerationValues.Contains(this.Default) && BlockAddingValues)
                RestrictedValues.Items.FindByValue(this.Default).Selected = true;

            if (Profile.AllowAdmin && !this.BlockAddingValues)
            {
                // add "add new value" point
                RestrictedValues.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_136, AddNewSelValue));
            }
        }
    }

    private void SetUpControlType()
    {
        RestrictedValuesEditor.Visible = false;
        TextBoxEditor.Visible = false;
        YesNoEditor.Visible = false;
        DateTimeEditor.Visible = false;

        if (EnumerationValues != null && EnumerationValues.Length > 0)
        {
            RestrictedValuesEditor.Visible = true;
            EnsureRestrictedValuesPopulated();
            RestrictedValuesEditor_requiredText.Visible = this.Mandatory;
        }
        else
        {
            if (PropertyType == typeof (bool))
            {
                YesNoEditor.Visible = true;
                YesNoEditor_requiredText.Visible = this.Mandatory;
            }
            else if (PropertyType == typeof (DateTime))
            {
                DateTimeEditor.Visible = true;
                DateTimeEditor_requiredText.Visible = this.Mandatory;
            }
            else
            {
                TextBoxEditor.Visible = true;
                TextBoxEditor_requiredText.Visible = this.Mandatory;
            }
        }
    }

    private void SelectValue(string value)
    {
        if (EnumerationValues != null && EnumerationValues.Length > 0)
        {
            EnsureRestrictedValuesPopulated();

            // we should convert selcted value to current culture to show on web site and correctly select values
            var curentStringValue = CustomPropertyHelper.GetCurrentCultureString(value, PropertyType);
            if (!string.IsNullOrEmpty(curentStringValue) &&
                RestrictedValues.Items.FindByValue(curentStringValue) == null)
            {
                RestrictedValues.SelectedValue = AddNewSelValue;
                RestrictedValuePanel.Visible = true;
                AdditionalRestrictedValue.Text = curentStringValue;
            }
            else
            {
                if (string.IsNullOrEmpty(curentStringValue))
                {
                    if (this.Mandatory && !string.IsNullOrEmpty(this.Default) &&
                        EnumerationValues.Contains(this.Default))
                        RestrictedValues.Items.FindByValue(this.Default).Selected = true;
                    else if (!this.Mandatory)
                    {
                        RestrictedValues.SelectedValue = SelNoneValue;
                        RestrictedValuePanel.Visible = false;
                    }
                }
                else if (Profile.AllowAdmin &&
                         curentStringValue.Equals(AddNewSelValue, StringComparison.OrdinalIgnoreCase))
                {
                    RestrictedValues.SelectedValue = curentStringValue;
                    RestrictedValuePanel.Visible = true;
                }
                else
                {
                    RestrictedValues.SelectedValue = curentStringValue;
                    RestrictedValuePanel.Visible = false;
                }
            }
        }
        else
        {
            if (PropertyType == typeof (bool))
            {
                if (string.IsNullOrEmpty(value) && this.Mandatory && !string.IsNullOrEmpty(this.Default))
                {
                    YesNoValue.SelectedValue = this.Default;
                }
                else
                {
                    bool pVal;
                    bool.TryParse(value, out pVal);
                    YesNoValue.SelectedValue = (pVal) ? "True" : "False";
                }
            }
            else if (PropertyType == typeof (DateTime))
            {
                DateTime val;
                if (this.Mandatory && !string.IsNullOrEmpty(this.Default) && string.IsNullOrEmpty(value))
                {
                    if (DateTime.TryParse(this.Default, out val))
                    {
                        txtDatePicker.Text = val.ToShortDateString();
                        txtTimePicker.Text = (val.Second > 0) ? val.ToLongTimeString() : val.ToShortTimeString();
                    }
                }
                else if (DateTime.TryParse(value, out val))
                {
                    txtDatePicker.Text = val.ToShortDateString();
                    txtTimePicker.Text = (val.Second > 0) ? val.ToLongTimeString() : val.ToShortTimeString();
                }
            }
            else
            {
                if (this.Mandatory && !string.IsNullOrEmpty(this.Default) && string.IsNullOrEmpty(value))
                {
                    TextBoxEditorValue.Text = this.Default;
                }
                else
                    TextBoxEditorValue.Text = SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular(CustomPropertyHelper.GetCurrentCultureString(value, PropertyType));
            }
        }
    }

    private string GetSelectedValue(bool invariant)
    {
        if (RestrictedValuesEditor.Visible)
        {
            if (RestrictedValuePanel.Visible)
            {
                // if invariant is true we try to convert value to invariant culture
                // e.g. for double value should be "1.1" but not "1,1"
                return (invariant)
                    ? CustomPropertyHelper.GetInvariantCultureString(AdditionalRestrictedValue.Text, PropertyType)
                    : AdditionalRestrictedValue.Text;
            }

            if (!RestrictedValues.SelectedValue.Equals(SelNoneValue, StringComparison.OrdinalIgnoreCase))
            {
                // if invariant is true we try to convert value to invariant culture
                return (invariant)
                    ? CustomPropertyHelper.GetInvariantCultureString(RestrictedValues.SelectedValue, PropertyType)
                    : RestrictedValues.SelectedValue;
            }
            return string.Empty;
        }

        if (PropertyType == typeof (bool))
        {
            return YesNoValue.SelectedValue;
        }

        if (PropertyType == typeof (DateTime))
        {
            DateTime value;
            var strDateTime = txtDatePicker.Text + " " + txtTimePicker.Text;
            if (DateTime.TryParse(strDateTime, out value))
                return value.ToString("s");

            return strDateTime.Trim();
        }

        return (invariant)
            ? CustomPropertyHelper.GetInvariantCultureString(TextBoxEditorValue.Text, PropertyType)
            : TextBoxEditorValue.Text;
    }

    protected void RestrictedValues_SelectedIndexChanged(object sender, EventArgs e)
    {
        RestrictedValuePanel.Visible = RestrictedValues.SelectedValue.Equals(AddNewSelValue,
            StringComparison.OrdinalIgnoreCase);
        if (this.Mandatory && BlockAddingValues)
            this.Default = RestrictedValues.SelectedValue;
    }

    protected void ValueValidation(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator) source;

        // if no objects for assignment then don't validate
        if (ObjectIds != null && ObjectIds.Length == 0 && !EditObjectMode || !Enabled)
        {
            args.IsValid = true;
            return;
        }

        // for validation we should get value in current culture 
        var text = GetSelectedValue(false);
        // In case Text is null or empty we're set CP value to DbNull

        if (string.IsNullOrEmpty(text.Trim()) && this.Mandatory)
        {
            cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Mandatory;
            args.IsValid = false;
            return;
        }

        if (string.IsNullOrEmpty(text) && (!RestrictedValuesEditor.Visible || !RestrictedValuePanel.Visible))
        {
            args.IsValid = true;
            return;
        }

        if (PropertyType == typeof (float))
        {
            float dNumber;
            args.IsValid = float.TryParse(text, NumberStyles.Float, CultureInfo.CurrentCulture, out dNumber);
            cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Float;
        }
        else if (PropertyType == typeof(double))
        {
            double dNumber;
            args.IsValid = double.TryParse(text, NumberStyles.Float, CultureInfo.CurrentCulture, out dNumber);
            cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Double;
        }
        else if (PropertyType == typeof (int))
        {
            int iNumber;
            args.IsValid = int.TryParse(text, out iNumber);
            cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_GenericValidationError_Integer;
        }
        else if (PropertyType == typeof (DateTime))
        {
            DateTime dt;
            if (!DateTime.TryParse(text, out dt) || dt <= SqlDateTime.MinValue)
            {
                args.IsValid = false;
                cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_SO0_5;
            }
        }
        else if (PropertyType == typeof (bool))
        {
            int iValue;
            if (int.TryParse(text, out iValue))
            {
                args.IsValid = (iValue == 0 || iValue == 1);
            }
            else
            {
                bool bValue;
                args.IsValid = bool.TryParse(text, out bValue);
            }
            cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_SO0_4;
        }
        else if (PropertyType == typeof (string))
        {
            if (string.IsNullOrEmpty(text) || text.Length <= (Size > 0 ? Size : int.MaxValue))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
                cv.ErrorMessage = string.Format(Resources.CoreWebContent.WEBDATA_IB0_195,
                    (Size == int.MaxValue) ? int.MaxValue : Size);
            }
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "datePickerDefaultSettings",
            @"(function($) {
    var regionalSettings = " + DatePickerRegionalSettings.GetDatePickerRegionalSettings() + @"

    $.fn.orionGetDate = function() {
        var datepicker = $('.datePicker', this);
        var timepicker = $('.timePicker', this);
        var datepart = $.datepicker.parseDate(regionalSettings.dateFormat, datepicker.val());
        var timepart = timepicker[0].timePicker.getTime();
        return new Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());
    };

    $.fn.orionSetDate = function(date) {
        $('.datePicker', this).val($.datepicker.formatDate(regionalSettings.dateFormat, date));
        var time = date.getTime(); // timePicker.setTime screws up the object. preserve it for our poor caller
        $('.timePicker', this)[0].timePicker.setTime(date);
        date.setTime(time);
    };

	$(function() {
		$.datepicker.setDefaults(regionalSettings);
		$('.datePicker').datepicker({mandatory: true, closeAtTop: false});
		$('.timePicker').timePicker({separator:regionalSettings.timeSeparator, show24Hours:regionalSettings.show24Hours});
	});
})(jQuery);
", true);
    }

    //todo
    //remove all 3 below public properties
    //use just this method to init!!!
    public void Configure(bool editObjectMode, Type propertyType, string[] values)
    {
        Configure(editObjectMode, propertyType, values, -1);
    }

    public void Configure(bool editObjectMode, Type propertyType, string[] values, int size)
    {
        this.Mandatory = false;
        this.EditObjectMode = editObjectMode;
        this.PropertyType = propertyType;
        this.EnumerationValues = values;
        this.Size = size;
        SetUpControlType();
    }

    public void Configure(bool editObjectMode, CustomProperty cp, bool blockAddingValues = false)
    {
        this.BlockAddingValues = blockAddingValues;
        this.Mandatory = cp.Mandatory;
        this.EditObjectMode = editObjectMode;
        this.PropertyType = cp.PropertyType;
        this.EnumerationValues = cp.Values;
        this.Size = cp.Size.HasValue ? cp.Size.Value : -1;
        this.Default = cp.Default;
        SetUpControlType();
    }
}
