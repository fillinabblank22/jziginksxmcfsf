﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Models.Filtering;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI.Localizer;
using System.Data;
using SolarWinds.Orion.Web;

namespace Orion.Controls
{
    public partial class FieldFilterPanel : System.Web.UI.UserControl
    {
        public const string CookiePaneWidth = "last_panel_width";

        public FilterProperties Model { get; set; }

        public FilterPanelVisualSettings ViewModel { get; set; }

        public int WidthMax { get; set; }
        public int WidthMin { get; set; }
        public bool IsCollapsible { get; set; }
        public string FieldPickerInstanceId { get; set; }
        public bool UseValuePicker { get; set; }
        
        public string TitleText { get; set; }
        public string RotatedTitleImage { get; set; }

        protected int WidthCurrent
        {
            get
            {
                int ret = ViewModel != null ? ViewModel.WidthCurrent : GetCookieValue(CookiePaneWidth, 280);
                return ret < 64 ? 64 : ret;
            }
        }

        protected bool NeedTextRotate { get { return string.IsNullOrWhiteSpace(RotatedTitleImage); } }

        public FieldFilterPanel()
        {
            WidthMax = 500;
            WidthMin = 200;
            IsCollapsible = true;
            TitleText = "Narrow your Environment";
        }

        protected string CollapsedStyleTag()
        {
            bool isCollapsed = IsCollapsible && ViewModel != null && ViewModel.IsCollapsed;
            return isCollapsed ? "style='display: none;'" : string.Empty;
        }

        protected string CollapsedCssName()
        {
            bool isCollapsed = IsCollapsible && ViewModel != null && ViewModel.IsCollapsed;
            return isCollapsed ? "collapsed" : "expanded";
        }

        protected int CollapsedWidth()
        {
            bool isCollapsed = IsCollapsible && ViewModel != null && ViewModel.IsCollapsed;

            int w = ViewModel != null ? ViewModel.WidthCurrent : GetCookieValue(CookiePaneWidth, 280);
            w = w < 64 ? 64 : w;

            return isCollapsed ? 20 : w;
        }

        private string[] FetchStatusApplicationTypes()
        {
            const string query = @"-- FieldFilterPanel; FetchStatusPresenters
SELECT
  DISTINCT P.Value
FROM 
  Metadata.PropertyMetadata P
JOIN
  Metadata.Entity M ON P.EntityName = M.FullName
WHERE 
  M.Type ISA 'System.ManagedEntity' AND
  P.PropertyName = 'status' AND
  P.Name = 'applicationType'
";
            
            using (var proxy = InformationServiceProxy.CreateV3())
            {
                using (var data = proxy.Query(query))
                {
                    return data.Rows.Cast<DataRow>().Select(x => x.ItemArray[0].ToString()).ToArray();
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(RotatedTitleImage) == false)
            {
                string path = PathResolver.GetVirtualPath("Orion", RotatedTitleImage);

                imgPanelTitle.Visible = true;
                imgPanelTitle.Src = string.IsNullOrWhiteSpace(path) ? RotatedTitleImage : path;
            }

            OrionInclude.CoreFile("FieldFilterPanel.js");
            OrionInclude.CoreFile("FieldFilterPanel.css");

            var sb = new StringBuilder();
            var appTypes = FetchStatusApplicationTypes();

            var d = new Dictionary<string, object>
            {
                { "WidthMax", WidthMax }, 
                { "WidthMin", WidthMin }, 
                { "IsCollapsible", IsCollapsible },
                { "FieldPickerInstanceId", FieldPickerInstanceId },
                { "Title", TitleText },
                { "RotatedTitleImage", RotatedTitleImage },
                { "UseValuePicker", UseValuePicker },
                { "StatusApplicationTypes", appTypes },
            };

            sb.AppendLine("/*<!--*/");
            sb.Append("$(function(){ SW.Core.Widgets.FieldFilterPanel.Init(");
            sb.Append(JsonConvert.SerializeObject(d));
            sb.Append(",");
            sb.Append(Model == null ? "[]" : JsonConvert.SerializeObject(Model));
            sb.Append(',');
            sb.Append(JsonConvert.SerializeObject(ViewModel ?? new FilterPanelVisualSettings { IsCollapsed = false, WidthCurrent = WidthCurrent }));
            sb.AppendLine("); });");
            sb.AppendLine("/*-->*/");

            OrionInclude.InlineJs(sb.ToString());

            base.OnPreRender(e);
        }

        private int GetCookieValue(string name, int defValue)
        {
            var c = Request.Cookies[name];

            if (c == null)
                return defValue;

            int v;
            if (int.TryParse(c.Value, out v) == false)
                v = defValue;

            if (v < 64)
                v = 64;

            return v;
        }
    }
}
