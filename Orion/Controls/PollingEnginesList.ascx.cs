﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Controls_PollingEnginesList : System.Web.UI.UserControl
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	private void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
			Initialize();
	}

	private void Initialize()
	{
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			lbPollingEngines.DataSource = proxy.EnumerateJobEngines();
			lbPollingEngines.DataTextField = "HostName";
			lbPollingEngines.DataValueField = "Id";
			lbPollingEngines.DataBind();
		}
	}


	public bool AllSelected
	{
		get
		{
			if (lbPollingEngines.Items[0].Selected) return true;

			foreach (ListItem item in lbPollingEngines.Items)
			{
				if (!item.Selected) return false;
			}

			return true;
		}
	}

	public List<int> SelectedEngines
	{
		get
		{
			List<int> selected = new List<int>();

			foreach (ListItem item in lbPollingEngines.Items)
			{
				if (item.Selected)
					selected.Add(Convert.ToInt32(item.Value));
			}

			return selected;
		}
	}
}
