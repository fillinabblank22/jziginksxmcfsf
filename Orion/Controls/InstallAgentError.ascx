<%@ Control Language="C#" ClassName="InstallAgentError" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<%  var printable = false;
    bool.TryParse(Request["Printable"], out printable);
    if (!printable) { %>

<style type="text/css">
        #errorDialogContent .dialogItem { margin-bottom: 10px; }
        #errorDialogContent { margin-top: 20px; padding-left:60px; background: url('/Orion/images/stop_32x32.gif') top left no-repeat}
</style>

<script type="text/javascript">
    //<![CDATA[
    $(function () {

        ShowInstallAgentErrorDialog = function (nodeCaption, errorMessage) {
            dialog = $("#<%= installAgentErrorDialog.ClientID%>").dialog({
                width: 420,
                modal: true,
                open: function () {
                    $('#errorDialogText').html(String.format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_AgentError_Text) %>', nodeCaption));
                    $('#errorDialogMessage').html(errorMessage);
                },
                overlay: { "background-color": "black", opacity: 0.4 },
                title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_AgentError_Title) %>',
                resizable: false
            });
            dialog.show();
        }

        CloseInstallAgentErrorDialog = function () {
            $("#<%= installAgentErrorDialog.ClientID%>").dialog('close');
            __doPostBack('', '');
        }
    });
//]]>
</script>

<div id="installAgentErrorDialog" runat="server" style="display:none;" class="disposable, common-dialog">
    <div id="errorDialogContent" class="content">
        <div id="errorDialogText" class="dialogItem"></div>
        <div id="errorDialogMessage" class="dialogItem"></div>
        <div class="dialogItem">
            <span class="LinkArrow">&#0187;</span>
            <orion:HelpLink ID="HelpLink1" runat="server" HelpUrlFragment="OrionAgent-TroubleshootingAgentInstalls"
                HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, AddNodeWizard_AgentError_LinkText %>" CssClass="sw-link" />
        </div>
        <div class="dialogItem"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_AgentError_PollingText) %></div>
        <div class="dialogItem">
            <span class="LinkArrow">&#0187;</span>
            <orion:HelpLink ID="HelpLink2" runat="server" HelpUrlFragment="OrionCoreAG-ChoosingPollingMethods"
                HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, AddNodeWizard_AgentError_PollingLinkText %>" CssClass="sw-link" />
        </div>
    </div>
    <div class="bottom">
        <div class="sw-btn-bar-wizard">
		    <orion:LocalizableButton runat="server" ID="CloseButton" LocalizedText="CustomText" Text="Close" DisplayType="Primary" OnClientClick="CloseInstallAgentErrorDialog(); return false;" />
        </div>
	</div>
</div>

<% } %>
