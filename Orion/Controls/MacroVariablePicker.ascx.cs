﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Models.MacroParsing;

public partial class Orion_Controls_MacroVariablePicker : System.Web.UI.UserControl
{
    public IEnumerable<ContextBase> MacroContexts;

    public string EntityType { get; set; }

    protected string MacroContextsJson
    {
        get
        {
            var settings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Objects};
            settings.StringEscapeHandling = StringEscapeHandling.EscapeHtml;

            return JsonConvert.SerializeObject(MacroContexts, Formatting.Indented, settings);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}