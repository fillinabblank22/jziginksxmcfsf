using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Web;

public partial class Orion_Controls_RSAdvancedSettings : UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (selectWebsite.Items.Count == 0)
            loadWebsites();
        else if (selectWebsite.Items.Count == 1)
            websitePicker.Visible = false;
    }

    private void loadWebsites()
    {
        var websites = WebsitesDAL.GetAllSites();
        foreach (KeyValuePair<int, string> site in websites)
        {
            selectWebsite.Items.Add(new ListItem(site.Value, site.Key.ToString()));
        }
    }

    public int getWebsite()
    {
        return Convert.ToInt32(selectWebsite.SelectedValue);
    }
    public void setWebsite(int id)
    {
        if (selectWebsite.Items.Count == 0)
            loadWebsites();
        if (id == 0)
        {
            selectWebsite.SelectedIndex = id;
        }
        else
        {
            var selectedIndex = selectWebsite.Items.IndexOf(selectWebsite.Items.FindByValue(id.ToString()));
            selectWebsite.SelectedIndex = selectedIndex;
        }
    }

    public string GetSelectWebsiteId()
    {
        return selectWebsite.ClientID;
    }

    public string getAccount()
    {
       return userAccount.getAccount();
    }

    public void setAccount(string accountName)
    {
        userAccount.setAccount(accountName);
    }
}