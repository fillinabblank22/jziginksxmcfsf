<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NOCPageHeader.ascx.cs" Inherits="Orion_Controls_NOCPageHeader" EnableViewState="false" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>

<link rel="stylesheet" type="text/css" href="/orion/styles/nocpageheader.css"/>

<div id="pageHeader" class="sw-mainnav-branding">
    <div id="CustomBanner"></div>
    <div id="swNavScroll" class="header-top">
        <div id="swNav" class="NOCSiteLogoImgMenu">
            <% if(ShowLogo) { %>
                <img src="<%= DefaultSanitizer.SanitizeHtml(NocSiteLogoUri) %>" alt="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_34) %>" class="sw-mainnav-logo NOCSiteLogoImg" />
            <% } %>
        </div>

        <div class="NOCSiteMenu">
          <div id="NOCMenu">
             <div id = "menuImgHover" class="imgHover">
                <i class="menuImg"></i>
             </div>
             <ul id="NOCMunuItems">
             <% if (Profile.AllowCustomize || OrionConfiguration.IsDemoServer){
                    if (Profile.AllowCustomize){%>
                   <li><a id="CustomizeNOCItem" href="/Orion/Admin/CustomizeView.aspx?ViewID=<%= DefaultSanitizer.SanitizeHtml(ViewId) %>&ReturnTo=<%= DefaultSanitizer.SanitizeHtml(ReturnUrl) %>"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_TM0_241) %></a></li>
                   <li><a id="CustomizeLink" href="/Orion/Admin/Settings.aspx" target="_blank"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_172) %></a></li>
                   <% } %>
                   <li><a id="ToggleSlideShowPausedItem" href="javascript:void(0);" onclick="SW.Core.ResourceContainer.ToggleSlideShowPaused(this);return false;" data-paused='false'><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_SO0_166) %></a></li>
                   <% } %>
                   <li class="NOCMenuDivider"></li>
                   <li><a id="ExitNOCItem" href="<%= DefaultSanitizer.SanitizeHtml(ExitFromNOCUrl) %>"><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_AB0_43) %></a></li>
             </ul>
            </div>
           <div class="noc-slides-navigation"></div>
        </div>

        <div class="NOCSiteLogo">
          <span><%= DefaultSanitizer.SanitizeHtml(string.Format(CoreWebContent.WEBDATA_IB0_185, ViewTitle)) %></span>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        var timeout;
        var $nocMenu = $("#NOCMenu");

        $nocMenu
            .mouseenter(function () {
                clearTimeout(timeout);
                timeout = null;
                $nocMenu.addClass("noc-menu-open");
            })
            .mouseleave(function () {
                if (!timeout) {
                    timeout = setTimeout(function() {
                        $nocMenu.removeClass("noc-menu-open");
                    }, 400);
                }
            });
    });
</script>
