using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

public partial class Orion_Controls_SmtpSettingsControl : System.Web.UI.UserControl
{
    public enum SmtpSettingsMode
    {
        Inline,
        New,
        Edit,
        AlertSettingsEdit
    }

    private const string AddNewServer = "-1";
    private const string NoBackupServer = "0";
    private const string RefreshOptions = "-2";
    private const string FakePassword = "********";

    private Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private int smtpServerID;
    private SmtpSettingsMode mode;
    private string onReadyJsCallback = "function(){}";

    public string OnReadyJsCallback
    {
        get { return onReadyJsCallback; }
        set { onReadyJsCallback = value; }
    }

    public SmtpSettingsMode Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public int SMTPServerID
    {
        get { return smtpServerID; }
        set { smtpServerID = value; }
    }

    private Dictionary<int, SmtpServer> SmtpServers
    {
        get
        {
            if (ViewState[string.Format("SmtpServers_{0}", ClientID)] == null)
                ViewState[string.Format("SmtpServers_{0}", ClientID)] = GetAvailableSmtpServers();
            return (Dictionary<int, SmtpServer>)ViewState[string.Format("SmtpServers_{0}", ClientID)];
        }
        set { ViewState[string.Format("SmtpServers_{0}", ClientID)] = value; }
    }

    public SmtpServer SmtpServer
    {
        get { return GetSmtpServer(); }
        set { SetSmtpServer(value); }
    }

    public bool IsPasswordChanged { get; private set; }

    private void SetSmtpServer(SmtpServer server)
    {
        if (Mode == SmtpSettingsMode.Inline)
        {
            if (server == null)
            {
                ddSmtpServers.SelectedValue = EmailConstants.DefaultSMTPServerID;
                return;
            }
            if (SmtpServers.ContainsKey(server.ServerID))
            {
                ddSmtpServers.SelectedValue = server.ServerID.ToString();
                return;
            }
            ddSmtpServers.SelectedValue = AddNewServer;
        }
        else
        {
            tbSmtpServer.Text = server.Address;
        }
        ddBackupServers.SelectedValue = SmtpServers.ContainsKey(server.BackupServerID)
                                            ? server.BackupServerID.ToString()
                                            : NoBackupServer;
        tbSmtpServer.Text = server.Address;
        tbSmtpPort.Text = server.Port.ToString();
        cbEnableSSL.Checked = server.EnableSSL;
        isDefault.Value = server.IsDefault.ToString();
        if (server.Credentials != null)
        {
            credentialsTbl.Style.Add("display", "table");
            cbAuth.Checked = true;
            credentialsTbl.Visible = true;
            tbUserName.Text = server.Credentials.Username;
            if (Mode == SmtpSettingsMode.AlertSettingsEdit)
            {
                // valid password mustn't be visible on frontend, neither in console
                tbPassword.Text = FakePassword;
                tbConfirmPassword.Text = FakePassword;
            }
        }
        else
        {
            credentialsTbl.Style.Add("display", "none");
            cbAuth.Checked = false;
            tbUserName.Text = string.Empty;
            tbPassword.Text = string.Empty;
            tbConfirmPassword.Text = string.Empty;
        }
    }

    private SmtpServer GetSmtpServer()
    {
        var smtpServer = new SmtpServer();
        if (!ddSmtpServers.SelectedValue.Equals(AddNewServer) && Mode != SmtpSettingsMode.AlertSettingsEdit)
        {
            smtpServer = SmtpServers[int.Parse(ddSmtpServers.SelectedValue)];
            return smtpServer;
        }

        int port;
        smtpServer.Address = tbSmtpServer.Text;
        if (Mode != SmtpSettingsMode.AlertSettingsEdit)
        {
            smtpServer.BackupServerID = int.Parse(ddBackupServers.SelectedValue);
        }
        smtpServer.EnableSSL = cbEnableSSL.Checked;
        smtpServer.IsDefault = false; //todo: temp solution
        int.TryParse(tbSmtpPort.Text, out port);
        smtpServer.Port = port;
        if (cbAuth.Checked)
        {
            smtpServer.Credentials = new SmtpServerCredential
            {
                Password = tbPassword.Text,
                Username = tbUserName.Text,
                Name = tbSmtpServer.Text
            };
        }

        bool isPasswordChanged;
        if (Boolean.TryParse(isPswChanged.Value, out isPasswordChanged))
        {
            IsPasswordChanged = isPasswordChanged;
        }

        return smtpServer;
    }

    private Dictionary<int, SmtpServer> GetAvailableSmtpServers()
    {
        using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
        {
            var servers = proxy.GetAvailableSmtpServers();
            return servers.ToDictionary(smtpServer => smtpServer.ServerID);
        }
    }

    private bool DefaultSmtpServerExists()
    {
        using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
        {
            return proxy.DefaultSmtpServerExists();
        }
    }

    private void LoadSmtpServersDropDown()
    {
        bool defaultSmtpServerExists = DefaultSmtpServerExists();
        if (defaultSmtpServerExists)
        {
            ddSmtpServers.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VL0_77, EmailConstants.DefaultSMTPServerID));
        }
        foreach (var smtpServer in SmtpServers.Values.ToList())
        {
            ddSmtpServers.Items.Add(new ListItem(smtpServer.Address, smtpServer.ServerID.ToString()));
        }

        smtpServerTbl.Attributes["style"] = "display:none";
        if (Profile.AllowAdmin && !OrionConfiguration.IsDemoServer)
        {
            smtpServerTbl.Attributes["style"] = ddSmtpServers.Items.Count == 0 ? "display:block" : "display:none";
            ddSmtpServers.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_YK0_74, AddNewServer));
            if (Mode == SmtpSettingsMode.Inline)
            {
                ddSmtpServers.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_AY0_75, RefreshOptions));
            }
        }
    }

    private void LoadBackupSmtpServersDropDown()
    {
        foreach (var smtpServer in SmtpServers.Values.Where(server => server.ServerID != SMTPServerID).ToList())
        {
            ddBackupServers.Items.Add(new ListItem(smtpServer.Address, smtpServer.ServerID.ToString()));
        }
        ddBackupServers.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VL0_37, NoBackupServer));
        if (Mode == SmtpSettingsMode.Inline)
        {
            ddBackupServers.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_AY0_75, RefreshOptions));
        }
        ddBackupServers.SelectedValue = NoBackupServer;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (IsPostBack) return;

        isDefault.Value = bool.FalseString;
        if (Mode != SmtpSettingsMode.AlertSettingsEdit)
        {
            LoadBackupSmtpServersDropDown();
        }
        if (Mode == SmtpSettingsMode.Inline)
        {
            LoadSmtpServersDropDown();
        }
        else
        {
            BindConfiguration();
        }
        if (Mode == SmtpSettingsMode.AlertSettingsEdit)
        {
            InitUiHints();
        }
    }

    private void InitUiHints()
    {
        tbSmtpServer.ToolTip = "${DefaultEmailSmtpAddress}";
        tbSmtpPort.ToolTip = "${DefaultEmailSmtpPort}";
        cbEnableSSL.ToolTip = "${DefaultEmailSmtpSSL}";
        tbUserName.ToolTip = "${DefaultEmailUserName}";
        tbPassword.ToolTip = "${DefaultEmailPassword}";
        tbConfirmPassword.ToolTip = "${DefaultEmailPassword}";
    }

    private void BindConfiguration()
    {
        ddSmtpServers.Visible = false;
        smtpHelpLink.Visible = false;
        if (Mode == SmtpSettingsMode.Edit)
        {
            using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
            {
                this.SmtpServer = proxy.GetSmtpServer(this.SMTPServerID);
            }
        }
        if (Mode == SmtpSettingsMode.AlertSettingsEdit)
        {
            backupServersContent.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        tbPassword.Attributes.Add("value", tbPassword.Text);
        tbConfirmPassword.Attributes.Add("value", tbConfirmPassword.Text);

        // blocks send test email in case of demo mode
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            btnTestCredentials.OnClientClick = "demoAction('Core_SmtpSettings_SendTestEmail', this); return false;";
        }
    }
}
