<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebIntegrationSettingsPageControl.ascx.cs" Inherits="Orion_Controls_WebIntegrationSettingsPageControl" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>

<orion:Include File="Admin/js/WebIntegration.js" runat="server"/>
<orion:Include File="js/jquery/jquery.tooltip.js" runat="server"/>
<orion:Include File="js/jquery/jquery.tooltip.css" runat="server"/>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        var enabled = SettingsDAL.GetSetting("CustomerPortalIntegration-Enabled");
        if (enabled != null && enabled.SettingValue == 0)
        {
            this.Visible = false;
        }
    }

    // this has to be done because localized images are placed in Orion/Images/de and orion/images/ja folder
    private readonly string _localizedImageFolder = Resources.CoreWebContent.CurrentHelpLanguage == "en" ? 
        "" : Resources.CoreWebContent.CurrentHelpLanguage + "/";
</script>

<div id="sw-settings-cp-notenabled" style="display: none">
    <div class="sw-settings-rightbox">
        <div class="sw-settings-icon sw-settings-icon-swlogo">&nbsp;</div>
        <div class="sw-settings-rightbox-content">
            <div class="sw-settings-rightbox-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JD0_01) %></div>

            <p>
                <orion:LocalizableButtonLink runat="server" ID="loginButton" DisplayType="Primary" NavigateUrl="javascript:SW.Core.Web.Integration.LogInCustomerPortal()" Text=<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JD0_07) %> />
            </p>

            <p>
                <img src="/Orion/images/<%= DefaultSanitizer.SanitizeHtml(_localizedImageFolder) %>cp-widgets-preview-new.png"/>
            </p>
        </div>
    </div>
</div>
<div id="sw-settings-cp-enabled" style="display: none">
    <div class="sw-settings-rightbox">
        <div class="sw-settings-icon sw-settings-icon-swlogo">&nbsp;</div>
        <div class="sw-settings-rightbox-content">
            <div class="sw-settings-rightbox-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JD0_03) %></div>

            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JD0_04) %> <span id="sw-settings-cp-username"></span> <a href="javascript:SW.Core.Web.Integration.LogOutFromCustomerPortal()" onclick="SW.Core.Analytics.TrackEvent('Customer Portal Integration', 'Disabled');" class="sw-link"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JD0_06) %></a></p>
            
            <div id="sw-settings-rightbox-inpst-container" style="display: none">
                <div id="sw-settings-rightbox-SC-container" style="display: none">
                    <div class="sw-settings-rightbox-header-links" id="sw-settings-rightbox-SC-header-links"></div>
                    <div class="sw-settings-rightbox-header-caption" id="sw-settings-rightbox-SC-header"></div> 
                    <div id="sw-settings-rightbox-SC-content"></div> 
                </div>
                <div id="sw-settings-rightbox-LAM-container" style="display: none">
                    <div class="sw-settings-rightbox-header-links" id="sw-settings-rightbox-LAM-header-links"></div>
                    <div class="sw-settings-rightbox-header-caption" id="sw-settings-rightbox-LAM-header"></div>
                    <div id="sw-settings-rightbox-LAM-content"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //<![CDATA[
        $().ready(function() {
            SW.Core.Web.Integration.RenderCustomerPortalWidget();
        });
    //]]>        
</script>