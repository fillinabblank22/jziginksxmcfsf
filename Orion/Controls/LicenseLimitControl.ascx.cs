﻿using System;
using System.Collections.Generic;

public partial class Orion_Controls_LicenseLimitControl : System.Web.UI.UserControl
{
    /// <summary>Optional property which set "margin" css style of notification </summary>
    public string Margin
    {
        get;
        set;
    }

    /// <summary>Optional property which set "max-width" css style of notification</summary>
    public string MaxWidth
    {
        get;
        set;
    }

    /// <summary>Dictionary of string rows with information about elements that might hit their license limit
    /// Key is a display name of element, Value is its license characteristic
    /// If this dictionary is empty or null notification will be hidden
    /// You have to envelop part which you want to be highlited with tags {0} and {1}</summary>
    public Dictionary<string, string> LicenseLimitInfoList
    {
        get;
        set;
    }

    /// <summary>Title of notification; Default: Approaching license limit!</summary>
    public string Title
    {
        get;
        set;
    }

    /// <summary>Title of column with possible solutions; Default: You can add objects up to the number remaining in your license, or...</summary>
    public string HintColumnTitle
    {
        get;
        set;
    }

    /// <summary>Dictionary of string rows with information about possible solutions and hints
    /// Key is a hint phrase, Value is a link
    /// You have to envelop part which you want to have as link with tags {0} and {1}</summary>
    public Dictionary<string, string> HintColumnRows
    {
        get;
        set;
    }

    /// <summary>Optional parameter identify whether content is put in two columns</summary>
    public bool AreTwoColumns
    {
        get;
        set;
    }

    /// <summary>Optional parameter. Number of remaining items</summary>
    public int RemainingItemsCount
    {
        get;
        set;
    }

    /// <summary>Optional parameter. Specifies whether volumes are not included in license</summary>
    public bool HideLicensePopupLink
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.pnlContainer.CssClass = "sw-license-limit-hint";

            this.pnlContainer.Style.Add("display", "none");

            if(LicenseLimitInfoList==null || LicenseLimitInfoList.Count==0)
                this.Visible = false; 

            if (!string.IsNullOrEmpty(this.MaxWidth))
            {
                this.pnlContainer.Attributes.CssStyle.Add("max-width", this.MaxWidth.Trim());
            }

            if (!string.IsNullOrEmpty(this.Margin))
            {
                this.pnlContainer.Attributes.CssStyle.Add("margin", this.Margin.Trim());
            }
            
        }
        if (String.IsNullOrEmpty(Title))
            Title = Resources.CoreWebContent.WEBDATA_ZS0_004;//Approaching license limit!

        if (String.IsNullOrEmpty(HintColumnTitle))
            HintColumnTitle = Resources.CoreWebContent.WEBDATA_ZS0_005;//You can add objects up to the number remaining in your license, or...

        if (HintColumnRows != null && !HideLicensePopupLink)
        {
            HintColumnRows.Add(Resources.CoreWebContent.WEBDATA_ZS0_007, "javascript: SW.Core.SalesTrigger.ShowLicensePopupAsync()");
            //HintColumnRows.Add(Resources.CoreWebContent.WEBDATA_ZS0_008, "#");//Sign up for unlimited licenses
        }
    }

}