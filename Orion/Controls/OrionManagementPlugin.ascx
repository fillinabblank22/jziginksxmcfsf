﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrionManagementPlugin.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_OrionManagementPlugin" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/Controls/UnmanageDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollRediscoverDialog" Src="~/Orion/Controls/PollRediscoverDialog.ascx" %>
<%@ Register tagPrefix="orion" TagName="CreateMaintenancePlan" Src="~/Orion/Controls/StartMaintenanceModeDialog.ascx"%>
<%@ Register tagPrefix="orion" TagName="MaintenanceSchedulerDialog" Src="~/Orion/Controls/MaintenanceSchedulerDialog.ascx"%>
<%@ Register tagPrefix="orion" TagName="DeleteNodeDialog" Src="~/Orion/Controls/DeleteNodeDialog.ascx"%>

<orion:PollRediscoverDialog runat="server" />
<orion:UnmanageDialog runat="server" />
<orion:CreateMaintenancePlan runat="server" />
<orion:MaintenanceSchedulerDialog runat="server" />
<orion:DeleteNodeDialog runat="server" />
