﻿using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web.DAL;
using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;


public partial class ThresholdControlExt : System.Web.UI.UserControl
{

    public bool OverrideMultipleObjects
    {
        get { if (ViewState["OverrideMultipleObjects" + this.ClientID] == null) return false; else return (bool)ViewState["OverrideMultipleObjects" + this.ClientID]; }
        private set { ViewState["OverrideMultipleObjects" + this.ClientID] = value; }
    }

    public bool IsValid
    {
        get { if (ViewState["IsValid" + this.ClientID] == null) return true; else return (bool)ViewState["IsValid" + this.ClientID]; }
        private set { ViewState["IsValid" + this.ClientID] = value; }
    }

    public string InvalidInstances
    {
        get { if (ViewState["InvalidInstances" + this.ClientID] == null) return ""; else return ViewState["InvalidInstances" + this.ClientID].ToString(); }
        set { ViewState["InvalidInstances" + this.ClientID] = value; }
    }

    public bool IsMultiedit
    {
        get { if (ViewState["IsMultiedit" + this.ClientID] == null) return false; else return (bool)ViewState["IsMultiedit" + this.ClientID]; }
        set { ViewState["IsMultiedit" + this.ClientID] = value; }
    }

    public string DisplayName
    {
        get { if (ViewState["DisplayName" + this.ClientID] == null) return ""; else return ViewState["DisplayName" + this.ClientID].ToString(); }
        set { ViewState["DisplayName" + this.ClientID] = value; }
    }

    public string ThresholdName
    {
        get { if (ViewState["ThresholdName" + this.ClientID] == null) return ""; else return ViewState["ThresholdName" + this.ClientID].ToString(); }
        set { ViewState["ThresholdName" + this.ClientID] = value; }
    }

    public string Unit
    {
        get { if (ViewState["Unit" + this.ClientID] == null) return ""; else return ViewState["Unit" + this.ClientID].ToString(); }
        set { ViewState["Unit" + this.ClientID] = value; }
    }

    #region Warning properties

    public bool IsWarningEnabled
    {
        get { if (ViewState["IsWarningEnabled" + this.ClientID] == null) return false; else return (bool)ViewState["IsWarningEnabled" + this.ClientID]; }
        set { ViewState["IsWarningEnabled" + this.ClientID] = value; }
    }

    public string WarningValue
    {
        get { if (ViewState["WarningValue" + this.ClientID] == null) return ""; else return ViewState["WarningValue" + this.ClientID].ToString(); }
        set { ViewState["WarningValue" + this.ClientID] = value; }
    }

    public bool IsGlobalWarningEnabled
    {
        get { if (ViewState["IsGlobalWarningEnabled" + this.ClientID] == null) return false; else return (bool)ViewState["IsGlobalWarningEnabled" + this.ClientID]; }
        set { ViewState["IsGlobalWarningEnabled" + this.ClientID] = value; }
    }

    public string GlobalWarningValue
    {
        get { if (ViewState["GlobalWarningValue" + this.ClientID] == null) return ""; else return ViewState["GlobalWarningValue" + this.ClientID].ToString(); }
        set { ViewState["GlobalWarningValue" + this.ClientID] = value; }
    }

    public string WarningPollsValue
    {
        get { if (ViewState["WarningPollsValue" + this.ClientID] == null) return ""; else return ViewState["WarningPollsValue" + this.ClientID].ToString(); }
        set { ViewState["WarningPollsValue" + this.ClientID] = value; }
    }

    public string WarningPollsIntervalValue
    {
        get { if (ViewState["WarningPollsIntervalValue" + this.ClientID] == null) return ""; else return ViewState["WarningPollsIntervalValue" + this.ClientID].ToString(); }
        set { ViewState["WarningPollsIntervalValue" + this.ClientID] = value; }
    }

    #endregion // Warning properties

    #region Critical properties

    public bool IsCriticalEnabled
    {
        get { if (ViewState["IsCriticalEnabled" + this.ClientID] == null) return false; else return (bool)ViewState["IsCriticalEnabled" + this.ClientID]; }
        set { ViewState["IsCriticalEnabled" + this.ClientID] = value; }
    }

    public string CriticalValue
    {
        get { if (ViewState["CriticalValue" + this.ClientID] == null) return ""; else return ViewState["CriticalValue" + this.ClientID].ToString(); }
        set { ViewState["CriticalValue" + this.ClientID] = value; }
    }

    public bool IsGlobalCriticalEnabled
    {
        get { if (ViewState["IsGlobalCriticalEnabled" + this.ClientID] == null) return false; else return (bool)ViewState["IsGlobalCriticalEnabled" + this.ClientID]; }
        set { ViewState["IsGlobalCriticalEnabled" + this.ClientID] = value; }
    }

    public string GlobalCriticalValue
    {
        get { if (ViewState["GlobalCriticalValue" + this.ClientID] == null) return ""; else return ViewState["GlobalCriticalValue" + this.ClientID].ToString(); }
        set { ViewState["GlobalCriticalValue" + this.ClientID] = value; }
    }

    public string CriticalPollsValue
    {
        get { if (ViewState["CriticalPollsValue" + this.ClientID] == null) return ""; else return ViewState["CriticalPollsValue" + this.ClientID].ToString(); }
        set { ViewState["CriticalPollsValue" + this.ClientID] = value; }
    }

    public string CriticalPollsIntervalValue
    {
        get { if (ViewState["CriticalPollsIntervalValue" + this.ClientID] == null) return ""; else return ViewState["CriticalPollsIntervalValue" + this.ClientID].ToString(); }
        set { ViewState["CriticalPollsIntervalValue" + this.ClientID] = value; }
    }

    #endregion // Critical properties

    public ThresholdOperatorEnum GlobalThresholdOperator
    {
        get { if (ViewState["DefaultThresholdOperator" + this.ClientID] == null) return ThresholdOperatorEnum.Greater; else return (ThresholdOperatorEnum)ViewState["DefaultThresholdOperator" + this.ClientID]; }
        set { ViewState["DefaultThresholdOperator" + this.ClientID] = value; }
    }

    public ThresholdType ThisThresholdType
    {
        get { if (ViewState["ThisThresholdType" + this.ClientID] == null) return ThresholdType.Global; else return (ThresholdType)ViewState["ThisThresholdType" + this.ClientID]; }
        set { ViewState["ThisThresholdType" + this.ClientID] = value; }
    }

    public ThresholdOperatorEnum ThisThresholdOperator
    {
        get { if (ViewState["ThisThresholdOperator" + this.ClientID] == null) return ThresholdOperatorEnum.Greater; else return (ThresholdOperatorEnum)ViewState["ThisThresholdOperator" + this.ClientID]; }
        set { ViewState["ThisThresholdOperator" + this.ClientID] = value; }
    }

    public string ObjectIDs
    {
        get { if (ViewState["ObjectIDs" + this.ClientID] == null) return ""; else return ViewState["ObjectIDs" + this.ClientID].ToString(); }
        set { ViewState["ObjectIDs" + this.ClientID] = value; }
    }

    public string IsValidFieldClientId
    {
        get { return hfIsValid.ClientID; }
    }

    public bool EnableCapacityPlanning
    {
        get { if (ViewState["EnableCapacityPlanning" + this.ClientID] == null) return false; else return (bool)ViewState["EnableCapacityPlanning" + this.ClientID]; }
        set { ViewState["EnableCapacityPlanning" + this.ClientID] = value; }
    }

    public PlanningType CapacityPlanningType
    {
        get { if (ViewState["CapacityPlanningType" + this.ClientID] == null) return PlanningType.None; else return (PlanningType)ViewState["CapacityPlanningType" + this.ClientID]; }
        set { ViewState["CapacityPlanningType" + this.ClientID] = value; }
    }

    /// <summary>
    /// JS function reference (name) to format in a custom user friendly way values in the statistics table.
    /// Example available in \web\orion\js\baselinedetails.js
    /// </summary>
    public string StatisticsFormatterFunction
    {
        get { if (ViewState["StatisticsFormatterFunction" + this.ClientID] == null) return ""; return ViewState["StatisticsFormatterFunction" + this.ClientID].ToString(); }
        set { ViewState["StatisticsFormatterFunction" + this.ClientID] = value; }
    }

    protected bool IsGlobal { get { return ThisThresholdType == ThresholdType.Global; } }

    private void FillDdlOperator(DropDownList ddlOperator)
    {
        ddlOperator.DataValueField = "Value";
        ddlOperator.DataTextField = "Text";
        ddlOperator.DataSource =
            Enum.GetValues(typeof(ThresholdOperatorEnum))
                .Cast<ThresholdOperatorEnum>()
                .Select(x => new { Value = string.Format("{0}|{1}", (int)x, x.ToString()), Text = x.LocalizedLabel() });
        ddlOperator.DataBind();
    }

    private void InitializeState()
    {
        FillDdlOperator(ddlOperator);
        lblOperatorText.Text = ddlOperator.SelectedItem.Text;

        lblGeneralWarningValue.Text = !IsGlobalWarningEnabled ? Resources.CoreWebContent.WEBDATA_GLOBAL_THRESHOLD_LEVEL_DISABLE :
            string.Format("{0} {1} {2}", GlobalThresholdOperator.LocalizedLabel().ToLower(), GlobalWarningValue, Unit);

        lblGeneralCriticalValue.Text = !IsGlobalCriticalEnabled ? Resources.CoreWebContent.WEBDATA_GLOBAL_THRESHOLD_LEVEL_DISABLE :
            string.Format("{0} {1} {2}", GlobalThresholdOperator.LocalizedLabel().ToLower(), GlobalCriticalValue, Unit);

        if (!EnableCapacityPlanning)
        {
            capacityPlanning.Visible = false;
            capacityPlanningGlobal.Visible = false;
        }

        if (IsMultiedit)
        {
            chbOverrideMultipleObjects.Visible = true;
            chbOverrideMultipleObjects.Checked = OverrideMultipleObjects;

            sectionOverrideGeneral.Style.Add("display", "none");
            sectionThresholdDefinition.Style.Add("display", "none");
            sectionDetailHyperlink.Style.Add("display", "none");

            chbWarningEnabled.Checked = IsGlobalWarningEnabled;
            chbCriticalEnabled.Checked = IsGlobalCriticalEnabled;
            tbWarningValue.Text = IsGlobalWarningEnabled ? GlobalWarningValue : string.Empty;
            tbCriticalValue.Text = IsGlobalCriticalEnabled ? GlobalCriticalValue: string.Empty;

            if (EnableCapacityPlanning)
            {
                capacityPlanningGlobal.Visible = true;
            }

            // capacity planning block for multiedit
            if (EnableCapacityPlanning)
            {
                capacityPlanning.Visible = true;
                capacityPlanningGlobal.Visible = true;

                if (CapacityPlanningType == PlanningType.Peak)
                {
                    capPlanTypePeak.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_10;
                }
                else
                {
                    capPlanTypeAverage.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_09;
                }
            }
            else
            {
                capacityPlanning.Visible = false;
                capacityPlanningGlobal.Visible = false;
            }
        }
        else
        {
            chbOverrideMultipleObjects.Visible = false;
            sectionOverrideGeneral.Style.Add("display", "block");
            sectionDetailHyperlink.Style.Add("display", "block");

            if (ThisThresholdType == ThresholdType.Global)
            {
                sectionGeneralThresholds.Style.Add("display", "block");
                sectionCustomThresholds.Style.Add("display", "none");
                chbWarningEnabled.Checked = IsGlobalWarningEnabled;
                chbCriticalEnabled.Checked = IsGlobalCriticalEnabled;
                tbWarningValue.Text = IsGlobalWarningEnabled ? GlobalWarningValue : string.Empty;
                tbCriticalValue.Text = IsGlobalCriticalEnabled ? GlobalCriticalValue : string.Empty;
            }
            else
            {
                sectionGeneralThresholds.Style.Add("display", "none");
                sectionCustomThresholds.Style.Add("display", "block");

                chbOverrideGeneral.Checked = true;

                ddlOperator.ClearSelection();
                ddlOperator.Items.FindByValue(string.Format("{0}|{1}", (int)ThisThresholdOperator, ThisThresholdOperator.ToString())).Selected = true;

                lblOperatorText.Text = ddlOperator.SelectedItem.Text;

                chbWarningEnabled.Checked = IsWarningEnabled;
                chbCriticalEnabled.Checked = IsCriticalEnabled;
                tbWarningValue.Text = IsWarningEnabled ? WarningValue : string.Empty;
                tbCriticalValue.Text = IsCriticalEnabled ? CriticalValue : string.Empty;
                this.tbWarnPoll.Text = WarningPollsValue;
                this.tbWarnInterval.Text = WarningPollsIntervalValue;
                this.tbCritPoll.Text = CriticalPollsValue;
                this.tbCritInterval.Text = CriticalPollsIntervalValue;

            }

            // capacity planning block for single edit
            if (EnableCapacityPlanning)
            {
                capacityPlanning.Visible = true;
                capacityPlanningGlobal.Visible = true;

                if (CapacityPlanningType == PlanningType.Peak)
                {
                    capPlanTypePeak.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_10;
                }
                else
                {
                    capPlanTypeAverage.Checked = true;
                    capacityPlanningGlobalValue.Text = Resources.CoreWebContent.WEBDATA_PD0_09;
                }
            }
            else
            {
                capacityPlanning.Visible = false;
                capacityPlanningGlobal.Visible = false;
            }
        }

        InitializeEnableCheckboxes();
    }

    private void InitializeEnableCheckboxes()
    {
        var isWarningEnabled = chbWarningEnabled.Checked;
        tbWarningValue.Enabled = isWarningEnabled;
        ddlWarnPolls.Enabled = isWarningEnabled;
        tbWarnPoll.Enabled = isWarningEnabled;
        tbWarnInterval.Enabled = isWarningEnabled;

        var isCriticalEnabled = chbCriticalEnabled.Checked;
        tbCriticalValue.Enabled = isCriticalEnabled;
        ddlCritPolls.Enabled = isCriticalEnabled;
        tbCritPoll.Enabled = isCriticalEnabled;
        tbCritInterval.Enabled = isCriticalEnabled;
    }

    private void GetCurrentState()
    {
        InvalidInstances = hfInvalidInstances.Value;
        IsValid = hfIsValid.Value == "1";

        if (IsMultiedit)
        {
            OverrideMultipleObjects = chbOverrideMultipleObjects.Checked;
        }

        if (!chbOverrideGeneral.Checked)
        {
            ThisThresholdType = ThresholdType.Global;
        }
        else
        {
            ThisThresholdType = ThresholdType.Dynamic;
            IsWarningEnabled = chbWarningEnabled.Checked;
            IsCriticalEnabled = chbCriticalEnabled.Checked;
            WarningValue = tbWarningValue.Text;
            CriticalValue = tbCriticalValue.Text;
            WarningPollsValue = this.tbWarnPoll.Text;
            WarningPollsIntervalValue = this.tbWarnInterval.Text;
            CriticalPollsValue = this.tbCritPoll.Text;
            CriticalPollsIntervalValue = this.tbCritInterval.Text;
            var selectedWarnPolls = this.ddlWarnPolls.SelectedValue;
            var selectedCritPolls = this.ddlCritPolls.SelectedValue;
            if (selectedWarnPolls == "SinglePoll")
            {
                WarningPollsValue = "1";
                WarningPollsIntervalValue = "1";
            }
            if (selectedWarnPolls == "XPolls")
            {
                WarningPollsIntervalValue = WarningPollsValue;
            }
            if (selectedCritPolls == "SinglePoll")
            {
                CriticalPollsValue = "1";
                CriticalPollsIntervalValue = "1";
            }
            if (selectedCritPolls == "XPolls")
            {
                CriticalPollsIntervalValue = CriticalPollsValue;
            }

            ThisThresholdOperator = (ThresholdOperatorEnum)Enum.Parse(typeof(ThresholdOperatorEnum), ddlOperator.SelectedValue.Split('|')[1], true);

            if (EnableCapacityPlanning)
            {
                if (capPlanTypePeak.Checked)
                {
                    CapacityPlanningType = PlanningType.Peak;
                }
                else
                {
                    CapacityPlanningType = PlanningType.Average;
                }
            }

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            GetCurrentState();
        }

        InitializeState();
    }
}