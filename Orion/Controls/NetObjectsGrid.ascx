﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetObjectsGrid.ascx.cs" Inherits="Orion_Controls_NetObjectsGrid" %>

<style type="text/css">
        .hide-header .x-grid3-header {
        display: none;
    }
    
    .searchImg
    {
        /*position: fixed !important;
            height: 22px !important;
            width: 23px !important;*/
        vertical-align: bottom;
    }
    
    .rightSearch
    {
    	margin-left: -2px;
        text-align: right !important;
    }
    /*hd-menu-open .x-grid3-hd-inner {
            background-color :#EBF3FD !important;
            background-image :url(../images/default/grid/grid3-hrow-over.gif) !important;
        }*/
</style>

<script type="text/javascript">
    var encodedSearchString = '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_VB0_131) %>';
    $(function () {
        $('#txtSerch').each(function () {
            this.value = encodedSearchString;
            this.style.color = '#555';
            this.style.fontStyle = 'italic';
            this.style.fontSize = '9pt';
        });

        $('#txtSerch').click(function () {
            if (this.value == encodedSearchString) {
                this.value = '';
                this.style.color = '#000';
            }
            this.style.fontStyle = 'normal';
            this.style.fontSize = '9pt';
        });

        $('#txtSerch').blur(function () {
            if (this.value == '') {
                this.value = encodedSearchString;
                this.style.color = '#555';
                this.style.fontStyle = 'italic';
                this.style.fontSize = '9pt';
            }
        });

        /*$('#txtSerch').onChange(function () {
        if (this.value == '') {
        this.value = encodedSearchString;
        this.style.color = '#555';
        this.style.fontStyle = 'italic';
        this.style.fontSize = '9pt';
        }
        });*/
    });
</script>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Services>
        <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference Path="/Orion/Services/WebAdmin.asmx" />
        <asp:ServiceReference Path="/Orion/Services/Containers.asmx" />
        <asp:ServiceReference Path="/Orion/Services/DependenciesTree.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="NetObjectsGrid.js" />

<div id="NetObjectsGrid-ErrorMessage"></div>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="entityName" runat="server"/>
        <input type="hidden" id="entityNameRef" value='<%= entityName.ClientID %>' />

        <asp:HiddenField runat="server" ID="selObjectId" OnValueChanged="ValueChanged" />
        <input type="hidden" id="selObjectIdRef" value='<%= selObjectId.ClientID %>' />
            
        <asp:HiddenField runat="server" ID="groupingGridValue" />
        <input type="hidden" id="groupingGridValueIdRef" value='<%= groupingGridValue.ClientID %>' />
            
        <asp:HiddenField runat="server" ID="groupingCellValue" />
        <input type="hidden" id="groupingCellValueIdRef" value='<%= groupingCellValue.ClientID %>' />
            
        <asp:HiddenField runat="server" ID="groupingSelectedRow" />
        <input type="hidden" id="groupingSelectedRowIdRef" value='<%= groupingSelectedRow.ClientID %>' />

        <asp:HiddenField runat="server" ID="isParentDepenedency" />
        <input type="hidden" id="isParentIdRef" value='<%= isParentDepenedency.ClientID %>' />
    </ContentTemplate>
</asp:UpdatePanel>
<input type="hidden" id="objType" value="<%= DefaultSanitizer.SanitizeHtml(this.NetObjectType) %>" />
<div style="text-align: right; margin-bottom: 5px; width:1078px;">
    <div class="rightSearch x-form-field-wrap x-form-field-trigger-wrap x-trigger-wrap-focus" >
        <input type="text" name="ext-comp-1054" id="txtSerch" size="16" class="x-form-text x-form-field x-form-focus" style="width: 200px; height: 17px; border-color: #B5B8C8;" 
        /><img class="rightSearch searchImg" style="padding-top: 1px; cursor: pointer;" src="/Orion/images/search_button.gif" id="searchImg" alt="" />
    </div>
</div>
<div id="divGrid"></div>
