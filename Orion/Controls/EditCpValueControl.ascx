<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCpValueControl.ascx.cs"
    Inherits="Orion_Controls_EditCpValueControl" EnableViewState="true" %>
<orion:Include ID="Include1" runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include ID="Include2" runat="server" File="js/jquery/timePicker.css" />
<style type="text/css">
    .cpValueFormat td {padding:0px !important;}
</style>
<asp:Panel runat="server" ID="TextBoxEditor" CssClass="edit-cp-value-div cpValueFormat">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:TextBox runat="server" ID="TextBoxEditorValue" Width="252px"></asp:TextBox>
            </td>
            <td>
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>" ID = "TextBoxEditor_requiredText"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CustomValidator ID="ValueValidator" runat="server" ControlToValidate="TextBoxEditorValue"
                    OnServerValidate="ValueValidation" ErrorMessage="*" EnableClientScript="false" ValidationGroup="EditCpValue"
                    Display="Dynamic" ValidateEmptyText="true"> 
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel runat="server" ID="YesNoEditor" CssClass="edit-cp-value-div cpValueFormat">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 256px;">
                <asp:DropDownList runat="server" ID="YesNoValue" Width="256px">
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_124 %>" Value="True"></asp:ListItem>
                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_125 %>" Value="False" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>" ID = "YesNoEditor_requiredText"/>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel runat="server" ID="DateTimeEditor" CssClass="edit-cp-value-div cpValueFormat">
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="width: 256px;">
                <span id="<%= ClientID %>">
                    <asp:TextBox ID="txtDatePicker" runat="server" Width="54%" CssClass="datePicker disposable" />
                    <asp:TextBox ID="txtTimePicker" runat="server" Width="41%" CssClass="timePicker disposable" />
                </span>
            </td>
            <td>
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>" ID = "DateTimeEditor_requiredText"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtDatePicker"
                    OnServerValidate="ValueValidation" ErrorMessage="*" EnableClientScript="false" ValidationGroup="EditCpValue"
                    Display="Dynamic" ValidateEmptyText="true"> 
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel runat="server" ID="RestrictedValuesEditor" Visible="false" CssClass="edit-cp-value-div cpValueFormat">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="restrict-selector">
                <asp:DropDownList runat="server" ID="RestrictedValues" AutoPostBack="true" Width="256px"
                    OnSelectedIndexChanged="RestrictedValues_SelectedIndexChanged" CausesValidation="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_169 %>" ID = "RestrictedValuesEditor_requiredText"/>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 5px!important;" colspan="2">
                <asp:Panel runat="server" ID="RestrictedValuePanel" Visible="false">
                    <%if (EditObjectMode)
                      {%>
                    <div style="padding: 5px 0px 5px 0px">
                        <asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_137 %>"></asp:Label><br />
                        <%} %>
                        <asp:TextBox runat="server" ID="AdditionalRestrictedValue" Width="252px"></asp:TextBox>
                        <%if (EditObjectMode)
                          {%>
                    </div>
                    <%} %>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="AdditionalRestrictedValue"
                    OnServerValidate="ValueValidation" ErrorMessage="*" EnableClientScript="false" ValidationGroup="EditCpValue"
                    Display="Dynamic" ValidateEmptyText="true"> 
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
