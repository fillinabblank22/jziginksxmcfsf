using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ModalBox : System.Web.UI.UserControl
{
    public override string ClientID
    {
        get
        {
            return this.divContainer.ClientID;
        }
    }

    private ITemplate _dialogContents;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate DialogContents
    {
        get
        {
            return _dialogContents;
        }
        set
        {
            this._dialogContents = value;
        }
    }


    public int Width
    {
        get
        {
            string widthStr = this.divDialog.Style[HtmlTextWriterStyle.Width];
            if (string.IsNullOrEmpty(widthStr))
            {
                return 350;
            }
            else
            {
                return Convert.ToInt32(widthStr.Substring(0, widthStr.Length - 2));
            }
        }
        set
        {
            this.divDialog.Style[HtmlTextWriterStyle.Width] = value.ToString() + "px";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (Page.Request.Browser.Browser.Equals("IE", StringComparison.InvariantCultureIgnoreCase))
        {
            this.iFrame.Attributes["style"] = MASK_STYLE;
            this.divCover.Attributes["style"] = MASK_SCROLL;
        }
        else
        {
            this.iFrame.Attributes["style"] = MASK_STYLE_FIREFOX;
        }
        
        if (null != this.DialogContents)
        {
            this.DialogContents.InstantiateIn(this.phContents);
        }
	    
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key_setModalBoxScripts", MODAL_BOX_SCRIPTS.Replace("@divCoverId", divCover.ClientID), true);
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key_setModalBox", "setModalBox()", true);
    }

    private void CreateContents()
    {

    }

    public void Hide()
    {
        this.divContainer.Attributes["style"] = "display: none;";
        this.upModalBox.Update();
    }

    public void Show()
    {
        this.divContainer.Attributes["style"] = "";
        this.upModalBox.Update();
    }

    private const string MASK_STYLE = @"
    position: absolute; 
    left: 0px; 
	top: 0px; 
	width: expression(this.offsetParent.scrollWidth); 
	height: expression(this.offsetParent.clientHeight+this.offsetParent.scrollTop);
    opacity: 0.5;
    -moz-opacity: 0.5;
    filter: alpha(opacity=50);
    z-index: 1000;
    ";

    private const string MASK_STYLE_FIREFOX = @"
    position: fixed;
    width: 100%;
 	height: 100%; 
    top:0px; 
	bottom:0px; 
	left:0px;
	right:0px;
	overflow:hidden; 
	padding:0; 
	margin:0; 
    background-color: Black;
    opacity: 0.5;
    -moz-opacity: 0.5;
    filter: alpha(opacity=50);
    z-index: 1000;
    ";

    private const string MASK_SCROLL = @"
    position: absolute;
    top: expression((this.offsetParent.clientHeight/2)-(this.clientHeight/2)+this.offsetParent.scrollTop);
    left: expression((this.offsetParent.clientWidth/2)-(this.clientWidth/2)+this.offsetParent.scrollLeft); 
    z-index: 1000;
    width: 100%;
    ";

    private const string MODAL_BOX_SCRIPTS = @" function getWindowHeight() {
		        var windowHeight = 0;
		        if (typeof(window.innerHeight) == 'number') {
			        windowHeight=window.innerHeight;
		        }
		        else {
			        if (document.documentElement && document.documentElement.clientHeight) {
				        windowHeight = document.documentElement.clientHeight;
			        }
			        else {
				        if (document.body && document.body.clientHeight) {
					        windowHeight=document.body.clientHeight;
				        }
			        }
		        }
		        return windowHeight;
	        }
	
            function setModalBox() {
                var outY = getWindowHeight();                    
                var div = document.getElementById('@divCoverId');
                var inY;
                var nextTop;
                if(div != null) {
                    inY = div.offsetHeight;
                    nextTop = (parseInt(outY) - parseInt(inY)) / 2;
                    // check if IE
                    if (navigator.appName == 'Microsoft Internet Explorer'){
		        var offsetParent = div.offsetParent;
		        while(offsetParent != null)
		        {
			        nextTop = nextTop + offsetParent.scrollTop;
			        offsetParent = offsetParent.offsetParent;
		        }
                    }
                    div.style.top = nextTop + 'px';                        
                }
            }";
}
