<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventList.ascx.cs" Inherits="Orion_Controls_EventList" %>

<asp:Repeater ID="EventsGrid" runat="server">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="0" class="Events">
    </HeaderTemplate>

    <ItemTemplate>
        <tr class="EventRow Event<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "EventType")) %>">
            <td width="100">
                <%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "EventTime", "{0:g}")) %>
            </td>
            <td width="19">
                <div class="event-icon EventImageCell<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "EventType")) %>">
                    <img src="/NetPerfMon/images/Event-<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "EventType")) %>.gif" border="0" alt="Event Icon">
                </div>
            </td>
            <td>
                <%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Message")) %>
            </td>
        </tr>
    </ItemTemplate>

    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>