﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_Controls_Charts_CustomChart : System.Web.UI.UserControl
{        
    #region Properties

    public bool LoadOnDemand
    {
        get;
        set;
    }

    public string Height
    {
        get;
        set;
    }

    public string Width
    {
        get;
        set;
    }

    public string DataUrl
    {
        get;
        set;
    }

    private Dictionary<string, object> customChartSettings;
    public Dictionary<string, object> CustomChartSettings
    {
        get
        {
            if (this.customChartSettings == null)
                this.customChartSettings = new Dictionary<string,object>();

            return this.customChartSettings;
        }
        set
        {
            this.customChartSettings = value;
        }
    }

    public string CustomChartOptions
    {
        get;
        set;
    }

    public string LegendTitle
    {
        get;
        set;
    }

    #endregion

    #region Protected methods

    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("Charts/Charts.css");
        OrionInclude.CoreFile("Charts/js/AllCharts.js");

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(this.Height))
            {
                this.pnlCustomChart.Attributes.CssStyle.Add("height", this.Height.Trim());
            }

            if (!string.IsNullOrEmpty(this.Width))
            {
                this.pnlCustomChart.Attributes.CssStyle.Add("width", this.Width.Trim());
            }

            if (string.IsNullOrEmpty(this.LegendTitle))
            {                
                this.spanChartLegendTitle.InnerText = "no title";
                this.spanChartLegendTitle.Attributes.Add("style", "display:none;");
            }
            else
            {
                this.spanChartLegendTitle.InnerText = this.LegendTitle;   
            }
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);

        RenderChartInitializerScript(writer);
        RenderChartLegendScript(writer);
    }

    #endregion

    #region Private methods

    private void RenderChartInitializerScript(HtmlTextWriter writer)
    {
        var js = string.Format(@"
            <script type='text/javascript'>                
                var GetChartSettings{0} = function (parameters) {{
                    var settings = {1};
                   
                    try {{
                        transformSettings{0}(parameters, settings);
                    }}
                    catch(ex) {{ }};

                    return settings;
                }};

                var GetChartOptions{0} = function (parameters) {{
                    var options = {2};
                   
                    try {{
                        transformOptions{0}(parameters,options);                   
                    }}
                    catch(ex) {{ }};

                    return options;
                }};

                var Load{0} = function (parameters) {{ 
                var refresh = function() {{ 
                       SW.Core.Charts.initializeStandardChart(GetChartSettings{0}(parameters), GetChartOptions{0}(parameters), 
                            function() {{ if (SW.Core.VisibilityObserver) {{ SW.Core.VisibilityObserver.loadingCompleted('{3}'); }} }}); }}; 
                
                    if((typeof(SW.Core.View) != 'undefined') && (typeof(SW.Core.View.AddOnRefresh) != 'undefined')) {{            
                        SW.Core.View.AddOnRefresh(refresh, '{3}');
                    }}

                    if (SW.Core.VisibilityObserver) {{
                        SW.Core.VisibilityObserver.registerByResourceId('{3}', refresh, true);
                    }} else {{
                        refresh();
                    }}
                }};
             </script>", this.ID, GetChartSettings(), GetChartOptions(), this.divChart.ClientID);

        writer.Write(js);
    }

    private void RenderChartLegendScript(HtmlTextWriter control)
    {
        var js = string.Format(@"
                    <script type='text/javascript'>
                        SW.Core.Charts.Legend.{0} = function (chart, dataUrlParameters) {{
                            $('#{1}').empty();
                            SW.Core.Charts.Legend.createStandardLegend(chart, dataUrlParameters, '{1}', false);
                        }};
                    </script>", string.Format("core_standardLegendInitializer__{0}", this.divChart.ClientID), this.tblLegend.ClientID);    
              
        control.Write(js);
    }

    private string GetChartOptions()
    {
        return CustomChartOptions;
    }

    private string GetChartSettings()
    {
        var details = this.CustomChartSettings;

        details["renderTo"] = this.divChart.ClientID;
        details["legendInitializer"] = string.Format("core_standardLegendInitializer__{0}", this.divChart.ClientID);

        if (!details.ContainsKey("showTitle"))
            details["showTitle"] = false;
        if (!details.ContainsKey("title"))
            details["title"] = string.Empty;
        if (!details.ContainsKey("subtitle"))
            details["subtitle"] = string.Empty;
        if (!details.ContainsKey("loadingMode"))
            details["loadingMode"] = LoadingModeEnum.StandardLoading;
        if (!details.ContainsKey("sampleSizeInMinutes"))
            details["sampleSizeInMinutes"] = 30;
        if (!details.ContainsKey("netObjectIds"))
            details["netObjectIds"] = new string[] { };
        if (!details.ContainsKey("ResourceProperties"))
            details["ResourceProperties"] = new Dictionary<string, object>();

        return new JavaScriptSerializer().Serialize(details);
    }    

    #endregion
}