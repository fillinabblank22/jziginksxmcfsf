<%@ Control Language="C#" ClassName="FilterNodesSql" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.PackageManager" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>

<script runat="server">
    public const string HideInterfaceFilterQueryKey = "HideInterfaceFilter";
    public const string HideNodeFilterQueryKey = "HideNodeFilter";
    public const string ShowVolumeFilterQueryKey = "ShowVolumeFilter";
    public const string IsSQLModeQueryKey = "IsSQLMode";
          
	public TextBox FilterTextBox { get { return Filter; } }

    protected bool isInterfacesAllowed;

    protected bool HideInterfaceFilter
    {
        get
        {
            return !isInterfacesAllowed || this.Request.QueryString[HideInterfaceFilterQueryKey] == "True";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.isInterfacesAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");
    }

    protected bool HideNodeFilter
    {
        get 
        {
            return this.Request.QueryString[HideNodeFilterQueryKey] == null ||
            this.Request.QueryString[HideNodeFilterQueryKey] == "False"; 
        }
    }

    protected bool HideVolumeFilter
    {
        get
        {
            return this.Request.QueryString[ShowVolumeFilterQueryKey] == null ||
			this.Request.QueryString[ShowVolumeFilterQueryKey] != "True";
        }
    }

    protected bool IsSQLMode
    {
        get
        {
            return this.Request.QueryString[IsSQLModeQueryKey] == null || (String.Compare(this.Request.QueryString[IsSQLModeQueryKey], "True", StringComparison.OrdinalIgnoreCase) == 0); 
        }
    }
      
</script>

<p>
	<b><%= DefaultSanitizer.SanitizeHtml((IsSQLMode)?  Resources.CoreWebContent.WEBDATA_TM0_12 : Resources.CoreWebContent.WEBDATA_PCC_WM_ER_FILTERNODES) %></b><br />
	<asp:TextBox runat="server" ID="Filter" Width="330"></asp:TextBox>
	
</p>

<p><%= DefaultSanitizer.SanitizeHtml((IsSQLMode)? Resources.CoreWebContent.WEBDATA_TM0_13 : Resources.CoreWebContent.WEBDATA_YB0_1) %></p>


<orion:CollapsePanel runat="server" Collapsed="true">
	<TitleTemplate><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_15) %></b></TitleTemplate>
	
	<BlockTemplate>
        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_01) %></p>

		        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_02) %><br />
		        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_03) %></b></p>

		        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_04) %>
		
			        </p><table>

				        <tbody><tr>
					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_05) %></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_06) %></td>
				        </tr>
				        <tr>
					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_07) %></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_08) %></td>
				        </tr><tr>

					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_09) %></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_10) %></td>
				        </tr><tr>
					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_11) %></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_12) %></td>
				        </tr>
			        </tbody></table>
		

		        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_13) %><br />
		        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_14) %></b></p>

		        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_15) %><br />
		        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_16) %></b></p>
						
		        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_17) %><br />
		        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_18) %></b></p>

		        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_19) %><br />
		        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_20) %></b></p>

		        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_21) %><br /> 
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_22) %></b></p>
		        <p>

    <%if (this.HideNodeFilter) { %>
		<orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
			<TitleTemplate><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_37) %></b></TitleTemplate>
			<BlockTemplate>
				<ul>
					<%foreach (string column in (IsSQLMode)? SolarWinds.Orion.NPM.Web.Node.GetAllColumnNames():WebDAL.GetNodeColumnNames()) {%>
						<li><%= DefaultSanitizer.SanitizeHtml(column) %></li>
					<%}%>
				</ul>
			</BlockTemplate>
		</orion:CollapsePanel>
    <%} %>


    <%if (!this.HideInterfaceFilter) { %>
		<orion:CollapsePanel ID="CollapsePanel2" runat="server" Collapsed="true">
			<TitleTemplate><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_38) %></b></TitleTemplate>
			<BlockTemplate>
				<ul>
					<%foreach (string column in  (IsSQLMode)?SolarWinds.Orion.NPM.Web.Interface.GetAllColumnNames():WebDAL.GetInterfaceColumnNames()) {%>
						<li><%= DefaultSanitizer.SanitizeHtml(column) %></li>
					<%}%>
				</ul>
			</BlockTemplate>
		</orion:CollapsePanel>
		
	<%} %>
	
	<%if (!this.HideVolumeFilter) { %>
		<orion:CollapsePanel ID="CollapsePanel3" runat="server" Collapsed="true">
			<TitleTemplate><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_39) %></b></TitleTemplate>
			<BlockTemplate>
				<ul>
					<%foreach (string column in (IsSQLMode)?SolarWinds.Orion.NPM.Web.Volume.GetAllColumnNames():WebDAL.GetVolumeColumnNames()) {%>
						<li><%= DefaultSanitizer.SanitizeHtml(column) %></li>
					<%}%>
				</ul>
			</BlockTemplate>
		</orion:CollapsePanel>
		
	<%} %>

	</BlockTemplate>
</orion:CollapsePanel>
