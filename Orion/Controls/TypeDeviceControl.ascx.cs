using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;

public partial class TypeDeviceControl : System.Web.UI.UserControl
{
	private void SetupControlFromRequest()
	{
		if (Request["DeviceType"] != null)
		{
			TypeDeviceValue = Request["DeviceType"].ToString();
		}
	}

	protected override void OnInit(EventArgs e)
	{
		InitTypeDeviceList();

		if (!Page.IsPostBack)
		{
			SetupControlFromRequest();
		}

		base.OnInit(e);
	}
	public string DescriptionText
	{
		get
		{
			return Description.Text;
		}
		set
		{
			Description.Text = value;
		}
	}

	public string TypeDeviceValue
	{
		get
		{
			return TypeDeviceList.SelectedValue;
		}
		set
		{
			TypeDeviceList.SelectedValue = value;
		}
	}

	private void InitTypeDeviceList()
	{
		List<DeviceType> list = new List<DeviceType>(DeviceTypeManager.GetAllDeviceType());
		list.Insert(0, new DeviceType("", "All Device Type"));

		TypeDeviceList.DataSource = list;
		TypeDeviceList.DataTextField = "Name";
		TypeDeviceList.DataValueField = "Value";
		TypeDeviceList.DataBind();
	}
}
