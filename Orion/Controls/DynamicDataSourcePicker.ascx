<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DynamicDataSourcePicker.ascx.cs" Inherits="Orion_Controls_DynamicDataSourcePicker" %>

<%@ Register TagPrefix="orion" TagName="FieldPicker" Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" %>

<%@Import namespace="SolarWinds.Reporting.Models.Selection" %>
<%@Import namespace="Newtonsoft.Json" %>

<orion:Include ID="Include1" runat="server" File="DynamicDataSourcePicker.js" />
<orion:Include ID="Include2" runat="server" File="NestedExpressionBuilder.css" />
<orion:Include ID="Include3" runat="server" File="NestedExpressionBuilder.js" />

<script runat="server">

    public string GetFieldConstantConditionType()
    {
        var fieldConstant = new Expr
        {
            NodeType = ExprType.Operator,
            Child = new[] {
                new Expr {
                    NodeType = ExprType.Field
                },
                new Expr {
                    NodeType = ExprType.Constant
                }
            }
        };
        return JsonConvert.SerializeObject(fieldConstant);
    }

    public string GetFieldFieldConditionType()
    {
        var fieldField = new Expr
        {
            NodeType = ExprType.Operator,
            Child = new[] {
                new Expr {
                    NodeType = ExprType.Field
                },
                new Expr {
                    NodeType = ExprType.Field
                }
            }
        };
        return JsonConvert.SerializeObject(fieldField);
    }

</script>    


<script type="text/javascript">
    (function (dynamicDataSourcePicker) {
   dynamicDataSourcePicker.exprBuilderConfigSimple = {
        allowGroups: false,
        groupOperators: <%= JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetGroupOperators().ToArray()) %>,
        logicalOperators: <%= JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetLogicalOperators().ToArray()) %>,
        ruleTypes: [
            { displayName: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_JP2_5) %>", expr: <%= GetFieldConstantConditionType() %> },   // Add Simple Condition
        ],
        fieldInput: SW.Core.NestedExpressionBuilder.Input.Field.DropDown
    };

   dynamicDataSourcePicker.exprBuilderConfigAdvanced = {
        allowGroups: true,
        groupOperators: <%= JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetGroupOperators().ToArray()) %>,
        logicalOperators: <%= JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetLogicalOperators().ToArray()) %>,
        ruleTypes: [
            { displayName: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_JP2_5) %>", expr: <%= GetFieldConstantConditionType() %>, hint: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_JP2_7) %>" },   // Add Simple Condition
            { displayName: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_JP2_6) %>", expr: <%= GetFieldFieldConditionType() %>, hint: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_JP2_8) %>" }       // Add Advanced Condition
        ],
        fieldInput: SW.Core.NestedExpressionBuilder.Input.Field.Picker
    };


}(SW.Core.Pickers.DynamicDataSourcePicker));
</script>


<orion:FieldPicker runat="server" ID="FieldPicker" ClientInstanceName="fieldPickerInput_Instance"  SingleMode="True"/>
<div ID="ShowSWQLDialog" style="display:none; background-color: white;">
        <textarea rows="20" cols="54" id="showSWQLArea"></textarea>

		<div class="sw-btn-bar-wizard">
			<span class="closeButtonPlaceHolder"></span>
		</div>
</div>