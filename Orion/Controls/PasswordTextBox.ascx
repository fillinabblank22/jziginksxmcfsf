<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PasswordTextBox.ascx.cs" Inherits="Orion_Nodes_Controls_PasswordTextBox" EnableViewState="true" ViewStateMode="Enabled" %>
<orion:Include runat="server" File="js/PasswordTextBox.js" />

 <style type="text/css">
    .clear-button
    {
        vertical-align: sub;
        margin-left: -18px;
        height: 14px;
        width: 15px;
        background: white;
        color: white;
        border: 0;
        -webkit-appearance: none;
    }
</style>

<asp:TextBox ID="PasswordBox" runat="server" TextMode="Password" AutoPostBack="False" Enabled="False"  autocomplete="off"></asp:TextBox>
<asp:ImageButton runat="server" ID="RemovePassword" ImageUrl="~/Orion/images/Clear.png" CssClass="clear-button" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_106%>" />
