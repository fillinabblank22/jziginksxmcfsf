﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WindowsCredentialsControl.ascx.cs"
    Inherits="Orion_Controls_WindowsCredentialsControl" %>

<orion:Include runat="server" File="WindowsCredentialsControl.js" />

<style type="text/css">
    .paragraph { padding-top: 10px; }
    .hintText { color: rgb(95, 95, 95); font-size: 10px; }
    #userCredentials { padding-left: 10px; padding-bottom: 10px; background-color: #f5f5f5; margin-top: 10px; margin-left: 5px; }
    .selectionContainer { padding-left: 10px; }
</style>
<div class="paragraph">
    <label> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_73) %></label>
</div>
<div class="selectionContainer">
    <div class="paragraph">
        <div>
            <input id="useUser" type="radio" class="userSelection" onclick="SW.Core.WindowsCredentialsControl.radioSelectionChange(this);"
                runat="server" value="1" />
            <span>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_74) %></span>
        </div>
        <div>
            <%-- netUseUser = DefaultUser --%>
            <input id="notUseUser" type="radio" onclick="SW.Core.WindowsCredentialsControl.radioSelectionChange(this);"
                runat="server" value="0" />
            <span>
                <%= String.Format("{0}{1}", ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_VL0_80), userName) %>
            </span>
        </div>
    </div>
</div>
<div id="userCredentials">
    <div>
        <div class="paragraph">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_59) %></div>
        <asp:TextBox runat="server" ID="UserName" Width="300" data-form="userName" />
        <div class="windowsUserName">
            <span class="hintText">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_166) %></span>
        </div>
    </div>
    <div>
        <div class="paragraph">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_8) %></div>
        <asp:TextBox ID="Password" TextMode="Password" runat="server" data-form="password" />
    </div>
    <div style="padding-top: 5px">
        <span id="testProgress" style="display: none;">
            <img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %></span>
        <div id="testResultSuccess" class="sw-suggestion sw-suggestion-pass" style="display: none;
            margin-bottom: 10px; margin-left: 0px;">
            <span class="sw-suggestion-icon"></span>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_55) %></div>
        <div id="testResultFailed" style="display: none;">
            <div class="sw-suggestion sw-suggestion-fail" style="margin-bottom: 10px; margin-left: 0px;"><span class="sw-suggestion-icon"></span>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_56) %></div><br/>
            <div class="sw-suggestion sw-suggestion-warn" style="margin-bottom: 10px; margin-left: 0px;"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MG0_20) %><br/>
                <a class="sw-link" target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Common.KnowledgebaseLinkHelper.GetSalesForceKBUrl(8058)) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MG0_21) %></a>
            </div>
        </div>
        <div>
            <orion:LocalizableButton runat="server" ID="btnTestCredentials" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_62 %>"
                OnClientClick="SW.Core.WindowsCredentialsControl.testCredentials();return false"
                DisplayType="Small" />
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        SW.Core.namespace("SW.Core").WindowsCredentialsControl = new SW.Core.WindowsCredentialsSettings();
        SW.Core.WindowsCredentialsControl.init();
    });
</script>
