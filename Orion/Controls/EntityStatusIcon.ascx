<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EntityStatusIcon.ascx.cs" Inherits="Orion_Controls_EntityStatusIcon" %>

<% if (WrapInBox) { %><span class="entityIconBox"><% } %>
    <img src="/Orion/StatusIcon.ashx?entity=<%= DefaultSanitizer.SanitizeHtml(this.Entity) %>&id=<%= DefaultSanitizer.SanitizeHtml(this.EntityId) %>&status=<%= DefaultSanitizer.SanitizeHtml(this.Status) %>&size=<%= DefaultSanitizer.SanitizeHtml(this.IconSize) %><%= DefaultSanitizer.SanitizeHtml(this.GetServerParam()) %>" />
<% if (WrapInBox) { %></span><% } %>
