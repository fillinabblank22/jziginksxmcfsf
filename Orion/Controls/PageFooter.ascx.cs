using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Services;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Controls_PageFooter : System.Web.UI.UserControl
{
    public string InstalledProducts
    {
        get
        {
            return OrionErrorPageBase.IsSqlFaulted ?
                "Orion" :
                System.Text.RegularExpressions.Regex.Replace(SolarWinds.Orion.Web.OrionModuleManager.GetInstalledModulesVersionString(), "^SolarWinds ", "");
        }
    }
    public string FooterString => 
        OrionErrorPageBase.IsSqlFaulted ? "Orion" : OrionModuleManager.GetOrionFooterString();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (OrionMinReqsMaster.IsPrintable)
        {
            // if printable, hide the footer.
            Visible = false;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        bool isMobile = OrionMinReqsMaster.IsMobileView;

        if (isMobile)
        {
            SolarWinds.Orion.Web.OrionInclude.CoreFile( "jquery-1.7.1/jquery.livequery.js" ); // needed for the live query 'gotofull' link updating.
        }

        SolarWinds.Orion.Web.OrionInclude.CoreFile("OrionMaster.js")
            .SetSection(SolarWinds.Orion.Web.OrionInclude.Section.Bottom)
            .AddJsInit(string.Format("SW.Core.Footer.Init({{ismobile:{0}}});", OrionMinReqsMaster.IsMobileView ? 1 : 0));

        base.OnPreRender(e);
    }

    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
        }
        catch (Exception)
        {
            if (OrionMinReqsMaster.IsErrorPage == false)
                throw;
        }
    }
}
