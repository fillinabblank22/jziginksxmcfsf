﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductBlogControl.ascx.cs" Inherits="ProductBlogControl" %>

<style type="text/css">
    #adminContentTable
    {
    	margin: 10px 10px 0px 0px;
    }
    #adminContentTable td
    {
    	padding: 10px 10px 10px 10px;
    }
    #adminContent p
    {
    	width: auto!important;
    }
    .UpdateDescription
    {
    	background-color: White!important;
   	}
</style>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Services>
        <asp:ServiceReference path="~/Orion/Services/Thwack.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<table id="adminContentTable" width="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr>
        <td style="font-size: 8pt;">
            <table class="UpdatesSettingsTable" cellpadding=0 cellspacing=0>
                <tr>    
                    <td><asp:CheckBox ID="cbEnableCont" runat="server" /></td>
                    <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_161) %></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <table cellpadding="0" cellspacing="0" class="UpdatesItems" style="border: none; width: auto"> 
                            <tr>
                                <td colspan="3" style="width: auto; display:inline !important;">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_178) %>&nbsp;<asp:TextBox runat="server" ID="storePostsCount"  Width="52px" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="storePostsCount" ErrorMessage="*" Display="Dynamic" />
                                    <asp:CompareValidator Type="Integer" ControlToValidate="storePostsCount" Operator="GreaterThan" ValueToCompare="0" ID="storePostsCountValidator" runat="server" ErrorMessage="*"></asp:CompareValidator>
                                    <asp:CompareValidator Type="Integer" ControlToValidate="storePostsCount" Operator="LessThanEqual" ValueToCompare="1000000" ID="CompareValidator2" runat="server" ErrorMessage="<% $Resources: CoreWebContent,WEBDATA_TM1_2 %>"></asp:CompareValidator>
                                </td>                              
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td style="text-align:right;">
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="ImageButton5" OnClick="CheckNow" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_170 %>" DisplayType="Small" Enabled="<%$ Code: !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin%>"/> 
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="sw-btn-bar">
                <orion:LocalizableButton runat="server" ID="ImageButton1" OnClick="SaveSettings" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_169 %>" DisplayType="Primary" Enabled="<%$ Code: !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin%>" />
                <orion:LocalizableButton runat="server" ID="ImageButton2" OnClick="CancelClick" LocalizedText="Cancel" DisplayType="Secondary" />
                <orion:LocalizableButton runat="server" ID="ImageButton3" OnClick="Refresh" LocalizedText="Refresh" DisplayType="Secondary" />
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="UpdatesItems">
                <div class="UpdateDescription">
                <table cellpadding="0" cellspacing="0" >
                    <tr class="UpdateDescription" >
                        <td class="BlogSettings" style="vertical-align:middle;">
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_163) %>&nbsp;&nbsp;</b>
                        </td>
                        <td class="BlogSettings">&nbsp;&nbsp;</td>
                        <td class="BlogSettings">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_179) %>&nbsp;<asp:TextBox runat="server" ID="showPostsCount" Width="52px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="showPostsCount" ErrorMessage="*" Display="Dynamic" />
                            <asp:CompareValidator Type="Integer" ControlToValidate="showPostsCount" Operator="GreaterThan" ValueToCompare="0" ID="CompareValidator1" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:CompareValidator Type="Integer" ControlToValidate="showPostsCount" Operator="LessThanEqual" ValueToCompare="1000000" ID="CompareValidator3" runat="server" ErrorMessage="<% $Resources: CoreWebContent,WEBDATA_TM1_2 %>"></asp:CompareValidator>
                        </td>
                        <td class="BlogSettings">&nbsp;&nbsp;</td>
                        <td class="BlogSettings"><asp:CheckBox runat="server" ID="cbShowIgnored" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_164 %>" /></td>
                    </tr>
                </table>
                </div>
                <div class="ProductUpdates" runat="server" id="ProductUpdatesMessage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_166) %></div>
                <div class="NoResultsMessage" runat="server" id="NoResultsMessage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_165) %></div>
                <asp:Repeater runat="server" ID="ProductBlogTable">
                    <HeaderTemplate>
                        <table id="inside_table" cellpadding="0" cellspacing="0" width="100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr><td colspan="3" style="padding: 0px 0px 0px 0px!important; height: 10px;"></td></tr>
                        <tr>
                            <td class="UpdateDescription" style="width: 80px;">&nbsp;</td>
                            <td class="UpdateDescription" style="padding-bottom: 0px; font-size: 12pt;"> 
                                <b><%# DefaultSanitizer.SanitizeHtml(Eval("Title").ToString()) %></b>
                            </td>
                            <td class="UpdateDescription" style="padding-bottom: 0px; width: 100%;">
                                <div class="sw-btn-bar-wizard">
                                    <orion:LocalizableButton runat="server" ID="ImageButton4" OnClick="AcknowledgeBlogItem" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_42 %>" 
                                        DisplayType="Small" Visible='<%# !Convert.ToBoolean(Eval("Ignored")) %>' Enabled="<%$ Code: !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin%>" />
                                    <asp:HiddenField runat="server" ID="hPostGuid" Value='<%# DefaultSanitizer.SanitizeHtml(Eval("Id").ToString()) %>' />
                                </div>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="UpdateDescription">&nbsp;</td>
                            <td class="UpdateDescription" style="padding-left: 50px; width: 100%;">
                                <p style="font-size:8pt; text-transform:uppercase; padding-top: 0px!important;"><%# DefaultSanitizer.SanitizeHtml(((DateTime)Eval("PublicationDate")).ToString("f", System.Globalization.CultureInfo.CurrentCulture)) %>
                                 &raquo;&nbsp;&nbsp;<%# DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_171, Eval("Owner"))) %></p>
                                <%# DefaultSanitizer.SanitizeHtml(Eval("Description").ToString()) %>
                                <div style="padding-top: 10px; border-top: solid 1px silver;">
                                     &raquo;&nbsp;<a class="RenewalsLink" href='<%# DefaultSanitizer.SanitizeHtml(Eval("Url").ToString()) %>'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_167) %></a>&nbsp;&nbsp;&nbsp;
                                     &raquo;&nbsp;<a class="RenewalsLink" href='<%# DefaultSanitizer.SanitizeHtml(Eval("CommentsUrl").ToString()) %>'><%# DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_168, Eval("CommentsCount"))) %> </a>
                                </div>
                            </td>
                            <td class="UpdateDescription">&nbsp;</td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </td>
    </tr>
</table>
