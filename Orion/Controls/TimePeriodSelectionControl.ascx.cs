using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_Controls_TimePeriodSelectionControl : TimeSelectionBaseControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public override DateTime Time { get; set; }
    
    [PersistenceMode(PersistenceMode.Attribute)]
    public override int FrequencyId { get; set; }
    
    public DateTime IntervalEndTime { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        SetTime();
        var timeSelectorContainer = FindControl("timeSelectorContainer") as HtmlGenericControl;
        if (timeSelectorContainer != null)
        {
            timeSelectorContainer.Attributes.Add("frequency_Id", FrequencyId.ToString());
        }
    }
    
    private void SetTime()
    {
        if (Time != null)
        {
            txtIntervalStartTime.Text = Time.ToShortTimeString();
            txtIntervalEndTime.Text = IntervalEndTime.ToShortTimeString();
        }
    }

}