<%@ Control Language="C#" ClassName="MobileAlertReportControl" %>
<asp:ScriptManagerProxy runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Services/Information.asmx" />
        <asp:ServiceReference Path="~/Orion/Services/WebAdmin.asmx" />
        <asp:ServiceReference Path="~/Orion/Services/AlertsAdmin.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<script type="text/javascript">
    jQuery.stringFormat = function(format, arguments) {
        var str = format;
        for (i = 0; i < arguments.length; i++) {
            str = str.replace('{' + i + '}', arguments[i]);
        }
        return str;
    };

    function isTouchDevice() {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    }

    function touchScroll(id) {
        if (isTouchDevice()) { 
            var el = document.getElementById(id);
            var scrollStartPos = 0;

            document.getElementById(id).addEventListener("touchstart", function (event) {
                scrollStartPos = this.scrollTop + event.touches[0].pageY;
                event.preventDefault();
            }, false);

            document.getElementById(id).addEventListener("touchmove", function (event) {
                this.scrollTop = scrollStartPos - event.touches[0].pageY;
                event.preventDefault();
            }, false);
        }
    }

    RunQuery = function(query, succeeded, failed) {
        Information.Query(query, function(result) {
            var table = [];
            for (var y = 0; y < result.Rows.length; ++y) {
                var row = result.Rows[y];
                var tableRow = {};
                for (var x = 0; x < result.Columns.length; ++x) {
                    tableRow[result.Columns[x]] = row[x];
                }
                table.push(tableRow);
            }
            succeeded({ Rows: table });
        }, function(error) {
            if (error.get_message() == "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_10) %>") {
                // login cookie expired. reload the page so we get bounced to the login page.
                alert('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_01) %>');
                window.location.reload();
            } else {
                alert('Error: ' + error.get_message() + '\n' + error.get_stackTrace());
            }
            failed(error);
        });
    };

    GetPageableAlerts = function (period, fromRow, toRow, succeeded, failed) {
        var showType = "";
        var alertId = "";
        var parts = $("#alertTypeValue")[0].value.split(':');
        var showAcknAlerts = !($("#showAckValue")[0].value == "hide");

        if (parts[0] != "all") {
            showType = parts[0];
            if ((parts[0] == "advanced" || parts[0] == "basic") && parts[1] != "all")
                alertId = parts[1];
        }

        AlertsAdmin.GetPageableAlerts(period, fromRow, toRow, showType, alertId, showAcknAlerts, function (result) {
            var table = [];
            for (var y = 0; y < result.Rows.length; ++y) {
                var row = result.Rows[y];
                var tableRow = {};
                for (var x = 0; x < result.Columns.length; ++x) {
                    tableRow[result.Columns[x]] = row[x];
                }
                table.push(tableRow);
            }
            succeeded({ Rows: table });
        }, function (error) {
            if (error.get_message() == "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_10) %>") {
                // login cookie expired. reload the page so we get bounced to the login page.
                alert('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_01) %>');
                window.location.reload();
            } else {
                alert('Error: ' + error.get_message() + '\n' + error.get_stackTrace());
            }
            failed(error);
        });
    };

    LoadAlertTypes = function() {
        var divCont = $("#selAlertTypeForm");

        var list = $("<ul></ul>").appendTo(divCont);
        var tempListItem = "<li><a class='selAlertName' href='javascript:void(0)'>{0}</a><input name='alertId' type='hidden' value='{1}:{2}'/></li>";

        $($.stringFormat(tempListItem, ["<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_02) %>", "all", "all"])).appendTo(list);
        $($.stringFormat(tempListItem, ["<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_03) %>", "advanced", "all"])).appendTo(list);

        var advancedAlertsSql = "SELECT AlertDefID, Name FROM Orion.AlertDefinitions ORDER BY Name";
        RunQuery(advancedAlertsSql, function(results) {
            if (results.Rows.length > 0)
                for (var i = 0; i < results.Rows.length; i++)
                $($.stringFormat(tempListItem, [results.Rows[i]["Name"], "advanced", results.Rows[i]["AlertDefID"]])).appendTo(list);

            $($.stringFormat(tempListItem, ["<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_04) %>", "basic", "all"])).appendTo(list);

            var basicAlertsSql = "Select AlertID,MonitoredProperty From Orion.ActiveAlerts Group By AlertID, MonitoredProperty Order By MonitoredProperty";
            RunQuery(basicAlertsSql, function(results) {
                if (results.Rows.length > 0)
                    for (var i = 0; i < results.Rows.length; i++)
                    $($.stringFormat(tempListItem, [results.Rows[i]["MonitoredProperty"], "basic", results.Rows[i]["AlertID"]])).appendTo(list);

                $(".selAlertName").click(function() {
                    SetFilterValue("name", this.innerHTML, $(this).next()[0].value);
                    $("#selAlertTypeForm").hide();
                    RefreshAlertTable();
                    $("#alertArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif";
                });
            }, function(error) {
            });
        }, function(error) {
        });
    };

    ShowSelectForm = function (link, menu, arrow) {
        var defaultHeight = 250;
        $(".FilterDropDown").each(function () { if (this.id != menu[0].id) $(this).hide(); });

        $("#alertArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif";
        $("#periodArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif";
        $("#showAckArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif";

        if (menu.is(":visible")) {
            arrow[0].src = "/Orion/images/breadcrumb_arrow.gif";
            menu.hide();
        }
        else {
            var submenuHeight = menu.height();
            //            if (submenuHeight > defaultHeight) {
            //                menu.css({ overflowY: 'scroll' }).css("height", defaultHeight);
            //                touchScroll(menu[0].id);
            //            }
            menu.show();
            arrow[0].src = "/Orion/images/breadcrumb_arrow_d.gif";
            menu.css("top", link.offset().top + 21).css("left", link.offset().left - 10).show();
        }
    };

    SetFilterValue = function(filterType, name, value) {
        switch (filterType.toUpperCase()) {
            case "NAME":
                $("#alertTypeLink")[0].innerHTML = name + '&nbsp;<img id="alertArrow" style="vertical-align: top;" src="/Orion/images/breadcrumb_arrow.gif"/>';
                $("#alertTypeValue")[0].value = value;
                break;
            case "PERIOD":
                $("#alertPeriodLink")[0].innerHTML = name + '&nbsp;<img id="periodArrow" style="vertical-align: top;" src="/Orion/images/breadcrumb_arrow.gif"/>';
                $("#alertPeriodValue")[0].value = value;
                break;
            case "SHOWACK":
                $("#showAckLink")[0].innerHTML = name + '&nbsp;<img id="showAckArrow" style="vertical-align: top;" src="/Orion/images/breadcrumb_arrow.gif"/>';
                $("#showAckValue")[0].value = value;
                break;
        }
    }

    GetBasicAlertSQL = function (alertId, dates) {
        var parts = $("#alertTypeValue")[0].value.split(':');
        var alertGuid = "Null";

        if (parts[0] == "basic") alertGuid = "''";

        var sql = " Select \
                            AA.AlertTime AS AlertTime, \
                            AA.MonitoredProperty AS AlertName, \
                            'Basic' AS AlertType, \
                            AA.ObjectType + '::' + AA.NodeName + '::' + AA.ObjectName AS ObjectName, \
                            '0' AS Acknowledged, \
                            " + alertGuid + " AS AlertID, \
                            AA.AlertID AS BAlertID, \
                            AA.ObjectType as ObjectType, \
                            AA.ObjectID AS ActiveObject \
                            From Orion.ActiveAlerts AA";

        var whereConjunction = " Where ";

        if (alertId != "") {
            sql += whereConjunction + " (AA.AlertID=" + alertId + ") ";
            whereConjunction = " AND "
        }

        sql += whereConjunction + " AA.AlertTime >= '" + dates[0] + "' AND AA.AlertTime<='" + dates[1] + "' ";

        return sql;
    }

    GetAlertsSQL = function(dates) {
        var isGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/;
        var parts = $("#alertTypeValue")[0].value.split(':');
        var alertId = "";

        if (parts[0] != "all") {
            if ((parts[0] == "advanced" || parts[0] == "basic") && parts[1] != "all")
                alertId = parts[1];
        }

        var sql = " SELECT \
                            AST.TriggerTimeStamp AS AlertTime, \
                            AD.Name AS AlertName, \
                            'Advanced' AS AlertType, \
                            AST.ObjectName as ObjectName, \
                            AST.Acknowledged AS Acknowledged, \
                            AST.AlertDefID as AlertID, \
                            0 AS BAlertID, \
                            AST.ObjectType AS ObjectType, \
                            AST.ActiveObject as ActiveObject \
                            FROM Orion.AlertStatus AST \
                            INNER JOIN Orion.AlertDefinitions AD ON AST.AlertDefID = AD.AlertDefID \
                            WHERE AST.State=2 ";


        // Build WHERE clause
        var whereConjunction = " AND ";

        if ($("#showAckValue")[0].value == "hide")
            sql += whereConjunction + " AST.Acknowledged=0 ";

        sql += whereConjunction + " AST.TriggerTimeStamp >= '" + dates[0] + "' AND AST.TriggerTimeStamp<='" + dates[1] + "' ";

        var period = $("#alertPeriodValue")[0].value;

        // Specific Alert Type
        if (alertId.match(isGuid))
            sql += whereConjunction + " (AST.AlertDefID='" + alertId + "') ";
        else if (alertId != "")
            sql += whereConjunction + " (AlertStatus.AlertDefID='00000000-0000-0000-0000-000000000000') ";

        return sql;
    };

    GetAlertsCountSQL = function(period) {
        var showType = "";
        var alertId = "";

        var parts = $("#alertTypeValue")[0].value.split(':');

        var dates = period.split('~');
        var sql = GetAlertsSQL(dates);

        if (parts[0] != "all") {
            showType = parts[0];
            if ((parts[0] == "advanced" || parts[0] == "basic") && parts[1] != "all")
                alertId = parts[1];
        }

        switch (showType.toUpperCase()) {
            case "ADVANCED":
                return $.stringFormat("SELECT COUNT(a.AlertID) AS Number FROM ( {0} )a  ", [sql]);
            case "BASIC":
                return $.stringFormat("SELECT COUNT(a.AlertID) AS Number FROM ( {0} )a ", [GetBasicAlertSQL(alertId, dates)]);
            default:
                return $.stringFormat("SELECT COUNT(a.AlertID) AS Number FROM ( {0} Union ( {1} ))a ", [sql, GetBasicAlertSQL(alertId, dates)]);
        }
    }

    AcknowledgeAlerts = function() {
        var alertIds = new Array();
        $('.AlertCheckBox').each(function() { if (!this.disabled && this.checked) alertIds.push($(this).next()[0].value) });
        AlertsAdmin.AcknowledgeAlerts(alertIds, function (result) {
            var toRow = parseInt($("#ToRow")[0].value)
            LoadAlertTable(1, toRow, true);
        }, function(ex) { });
    };

    GetNetObjectLink = function(alertType, objectType, objectId) {
        if (alertType.toUpperCase() == "BASIC") return $.stringFormat("../View.aspx?NetObject={0}:{1}", [objectType, objectId]);

        var objType = "N";
        switch (objectType.toUpperCase()) {
            case "CUSTOM INTERFACE POLLER":
                objType = "I";
                break;
            case "INTERFACE":
                objType = "I";
                break;
            case "VOLUME":
                objType = "V";
                break;
        }
        return $.stringFormat("../View.aspx?NetObject={0}:{1}", [objType, objectId]);
    }

    RefreshAlertTable = function() {
        $("#ToRow")[0].value = "10";
        LoadAlertTable(1, 10, true);
    }

    LoadAlertTable = function (fromRow, toRow, isReload) {
        WebAdmin.GetDateTimeLimits($("#alertPeriodValue")[0].value,
            function (result) {
                GetPageableAlerts(result, fromRow, toRow, function (results) {
                    var table = $("#alertsTable");
                    var tempListItem = "<tr {8}>\
                        <td  width='15px'><input ID='cbAlert' class='AlertCheckBox' type='checkbox' {3} {4} /><input type='hidden' value='{5}:{6}:{7}'/></td>\
                        <td  width='15px'><img src='/NetPerfMon/images/Event-19.gif' /></td>\
                        <td>{0}&nbsp;<a href='javascript:void(0)' onclick=\"ShowDetails('{10}:{11}');\">{1}</a> - <a href='{9}'>{2}</a></td>\
                        </tr>";

                    if (isReload) {
                        table.empty();
                        $("<tr><td width='15px'><input type='checkbox' class='SelectALL' /></td><td colspan='2'><input type='button' value='<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_06) %>' onclick='AcknowledgeAlerts();' /></td></tr>").appendTo(table);
                    }

                    if (results.Rows.length > 0)
                        for (var i = 0; i < results.Rows.length; i++)
                            $($.stringFormat(tempListItem,
                                [results.Rows[i]["AlertTime"].localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.FullDateTimePattern),
                                results.Rows[i]["AlertName"],
                                (results.Rows[i]["AlertType"].toUpperCase() == 'BASIC') ? ((results.Rows[i]["ObjectName"].split('::')[0] != 'N') ? results.Rows[i]["ObjectName"].split('::')[1] + "-" + results.Rows[i]["ObjectName"].split('::')[2] : results.Rows[i]["ObjectName"].split('::')[1]) : results.Rows[i]["ObjectName"],
                                (results.Rows[i]["AlertType"].toUpperCase() == 'BASIC' || results.Rows[i]["Acknowledged"] == '1') ? "disabled=disabled" : "",
                                (results.Rows[i]["Acknowledged"] == '1') ? "checked=checked" : "",
                                results.Rows[i]["AlertID"],
                                results.Rows[i]["ActiveObject"],
                                results.Rows[i]["ObjectType"],
                                (i % 2 == 0) ? "style='background-color:#f8f8f8'" : "",
                                GetNetObjectLink(results.Rows[i]["AlertType"], results.Rows[i]["ObjectType"], results.Rows[i]["ActiveObject"]),
                                results.Rows[i]["AlertType"],
                                (results.Rows[i]["AlertType"].toUpperCase() == 'BASIC') ? results.Rows[i]["BAlertID"] : results.Rows[i]["AlertID"]
                                ])).appendTo(table);


                    $("<tr id='moreTR' style='display: none;'><td colspan='3'><div class='mobileMoreBg'><a id='showMore' href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_05) %></a></div></td></tr>").appendTo(table);
                    $("<tr id='acknowledgedTR'><td colspan='3'><input type='button' value='<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_06) %>' onclick='AcknowledgeAlerts();' /></td></tr>").appendTo(table);

                    $("#showMore").click(function () {
                        $("#moreTR").remove();
                        $("#acknowledgedTR").remove();
                        var toRow = parseInt($("#ToRow")[0].value);

                        LoadAlertTable(toRow + 1, toRow + 10, false);
                    });

                    if (isReload) {
                        $(".selAlertName").click(function () { SetFilterValue("name", this.innerHTML, $(this).next()[0].value); $("#selAlertTypeForm").hide(); });
                        $(".SelectALL").click(function () {
                            if (this.checked) $('.AlertCheckBox').each(function () { if (!this.disabled) this.checked = true; });
                            else $('.AlertCheckBox').each(function () { if (!this.disabled) this.checked = false; });
                        });
                    }

                    RunQuery(GetAlertsCountSQL(result), function (res) {
                        //This condition determines whether to display "More" button to user. We don't show it in case:
                        //1) There are no any records to display
                        //2) We've already showed all records
                        if (parseInt(res.Rows[0]["Number"]) > toRow && results.Rows.length > 0 && results.Rows.length >= toRow - fromRow)
                            $("#moreTR").show();
                        else
                            $("#moreTR").hide();

                        $("#ToRow")[0].value = toRow.toString();
                        if (typeof SetFooter == "function") SetFooter();
                    }, function (error) {
                    });

                }, function (error) {
                });
            }, function (ex) {
            });
    };

    Close_Click = function() {
        $("#divCover").dialog("close");
    };

    ShowDetails = function(alert) {
        var parts = alert.split(':'); 
        if (parts[0].toUpperCase() == "ADVANCED") {
            var advSQL = "SELECT TOP 1 \
                            AST.TriggerTimeStamp AS AlertTime, \
                            AD.Name AS AlertName, \
                            AST.ObjectName as ObjectName, \
                            AST.AcknowledgedBy AS AcknowledgedBy, \
                            AST.AcknowledgedTime AS AcknowledgedTime, \
                            AST.AlertNotes As Notes \
                            FROM Orion.AlertStatus AST \
                            INNER JOIN Orion.AlertDefinitions AD ON AST.AlertDefID = AD.AlertDefID \
                            WHERE AST.AlertDefID='" + parts[1] + "' ";

            RunQuery(advSQL, function(result) {
                if (result.Rows.length > 0) {
                    $("#AlertName")[0].innerHTML = result.Rows[0]["AlertName"];
                    $("#AlertTime")[0].innerHTML = result.Rows[0]["AlertTime"].format(Sys.CultureInfo.CurrentCulture.dateTimeFormat.FullDateTimePattern);
                    $("#AlertType")[0].innerHTML = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_07) %>";
                    $("#AcknowledgedBy")[0].innerHTML = result.Rows[0]["AcknowledgedBy"];
                    $("#NetworkObject")[0].innerHTML = result.Rows[0]["ObjectName"];
                    $("#AcknowledgedTime")[0].innerHTML = result.Rows[0]["AcknowledgedTime"].format(Sys.CultureInfo.CurrentCulture.dateTimeFormat.FullDateTimePattern);
                    $("#Notes")[0].innerHTML = result.Rows[0]["Notes"];
                    $("#divCover").dialog({ width: "90%", height: 400, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_08) %>" }).show();
                    $(".ui-dialog-container").css({ overflow: 'visible' });
                }
            }, function(ex) { });
        }
        else {
            var basicSQL = "Select TOP 1 \
                            AA.AlertTime AS AlertTime, \
                            AA.MonitoredProperty AS AlertName, \
                            AA.ObjectType + '::' + AA.NodeName + '::' + AA.ObjectName AS ObjectName \
                            From Orion.ActiveAlerts AA WHERE  (AA.AlertID=" + parts[1] + ") ";
            RunQuery(basicSQL, function(result) {
                if (result.Rows.length > 0) {
                    $("#AlertName")[0].innerHTML = result.Rows[0]["AlertName"];
                    $("#AlertTime")[0].innerHTML = result.Rows[0]["AlertTime"].format(Sys.CultureInfo.CurrentCulture.dateTimeFormat.FullDateTimePattern);
                    $("#AlertType")[0].innerHTML = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_09) %>";
                    $("#NetworkObject")[0].innerHTML = (result.Rows[0]["ObjectName"].split('::')[0] != 'N') ? result.Rows[0]["ObjectName"].split('::')[1] + "-" + result.Rows[0]["ObjectName"].split('::')[2] : result.Rows[0]["ObjectName"].split('::')[1];
                    $("#divCover").dialog({ width: "90%", height: 300, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_PS0_08) %>" }).show();
                    $(".ui-dialog-container").css({ overflow: 'visible' });
                }
            }, function(ex) { });
        }
    };

    $(function() {
        var isClicked = false;
        $(this).click(function() {
            $("#selAlertTypeForm").hide();
            $("#selPeriodForm").hide();
            $("#selShowAckForm").hide();

            $("#alertArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif";
            $("#periodArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif";
            $("#showAckArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif";
        });

        $("#alertTypeLink").click(function() { ShowSelectForm($(this), $("#selAlertTypeForm"), $("#alertArrow")); return false; });
        $("#alertPeriodLink").click(function() { ShowSelectForm($(this), $("#selPeriodForm"), $("#periodArrow")); return false; });
        $("#showAckLink").click(function() { ShowSelectForm($(this), $("#selShowAckForm"), $("#showAckArrow")); return false; });
        $(".selPeriodName").click(function() { SetFilterValue("period", this.innerHTML, $(this).next()[0].value); $("#selPeriodForm").hide(); RefreshAlertTable(); $("#periodArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif"; });
        $(".selShowAck").click(function() { SetFilterValue("showack", this.innerHTML, $(this).next()[0].value); $("#selShowAckForm").hide(); RefreshAlertTable(); $("#showAckArrow")[0].src = "/Orion/images/breadcrumb_arrow.gif"; });
    });
</script>

<div class="FilterDiv">
    <div style="display: inline; padding: 10px 10px 10px 10px; ">
        <a id="alertTypeLink" href="javascript:void(0)"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_32) %>&nbsp;<img id="alertArrow"
            style="vertical-align: top;" src="/Orion/images/breadcrumb_arrow.gif" /></a>
        <input id="alertTypeValue" type="hidden" value="" />
    </div>
    <div style="display: inline; padding: 10px 10px 10px 10px;">
        <a id="alertPeriodLink" href="javascript:void(0)"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_16) %>&nbsp;<img id="periodArrow"
            style="vertical-align: top;" src="/Orion/images/breadcrumb_arrow.gif" /></a>
        <input id="alertPeriodValue" type="hidden" value="LAST HOUR" />
    </div>
    <div style="display: inline; padding: 10px 10px 10px 10px;">
        <a id="showAckLink" href="javascript:void(0)"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_31) %>&nbsp;<img id="showAckArrow"
            style="vertical-align: top;" src="/Orion/images/breadcrumb_arrow.gif" /></a>
        <input id="showAckValue" type="hidden" value="hide" />
    </div>
</div>
<div id="selAlertTypeForm" class="FilterDropDown">
</div>
<div id="selPeriodForm" class="FilterDropDown">
    <ul>
        <li><a class="selPeriodName" href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_16) %></a><input name='periodName'
            type='hidden' value='LAST HOUR' /></li>
        <li><a class="selPeriodName" href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_17) %></a><input name='periodName'
            type='hidden' value='LAST 2 HOURS' /></li>
        <li><a class="selPeriodName" href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_18) %></a><input name='periodName'
            type='hidden' value='LAST 6 HOURS' /></li>
        <li><a class="selPeriodName" href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_19) %></a><input name='periodName'
            type='hidden' value='LAST 24 HOURS' /></li>
        <li><a class="selPeriodName" href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_20) %></a><input name='periodName'
            type='hidden' value='LAST 7 DAYS' /></li>
    </ul>
</div>
<div id="selShowAckForm" class="FilterDropDown">
    <ul>
        <li><a class="selShowAck" href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_21) %></a><input name='showAck'
            type='hidden' value='show' /></li>
        <li><a class="selShowAck" href='javascript:void(0)'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_22) %></a><input name='showAck'
            type='hidden' value='hide' /></li>
    </ul>
</div>
<table id="alertsTable" class="Report" width="100%" cellpadding="0" cellspacing="0">
</table>
<div class='mobileMoreBg'>
    <a href="/Orion/SummaryView.aspx?IsMobileView=true"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_23) %> �</a>
</div>
<input type="hidden" id="ToRow" value="10" />
    <div id="divCover" name="divCover" style="position: relative; display: none;
        width: 99%; height: 100%; left: 0px; top: 0px; background-color:White;">
        <table width="100%" >
            <tr>
                <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                    font-family: sans-serif; font-size: 16px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_24) %></b>&nbsp;<label id="AlertName"></label>
                </td>
            </tr>
            <tr>
                <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                    font-family: sans-serif; font-size: 16px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_25) %></b>&nbsp;<label id="AlertTime"></label>
                </td>
            </tr>
            <tr>
                <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                    font-family: sans-serif; font-size: 16px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_26) %></b>&nbsp;<label id="AlertType"></label>
                </td>
            </tr>
            <tr>
                <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                    font-family: sans-serif; font-size: 16px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_27) %></b>&nbsp;<label id="NetworkObject"></label>
                </td>
            </tr>
            <tr>
                <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                    font-family: sans-serif; font-size: 16px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_28) %></b>&nbsp;<label id="AcknowledgedBy"></label>
                </td>
            </tr>
            <tr>
                <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                    font-family: sans-serif; font-size: 16px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_29) %></b>&nbsp;<label id="AcknowledgedTime"></label>
                </td>
            </tr>
            <tr>
                <td align="left" style="vertical-align: top; width: 100%; padding: 0px 5px 0px 5px;
                    font-family: sans-serif; font-size: 16px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_30) %></b><br />
                    <textarea id="Notes" style="font-family: sans-serif; font-size: 13px; width: 99%" cols="20" rows="8"></textarea>
                </td>
            </tr>
            <tr>
                <td align="right" style="padding: 5px;">
                    <input type="button" id="Close" value="Close" onclick="Close_Click();" />
                </td>
            </tr>
        </table>
    </div>

<script type="text/javascript">
    LoadAlertTypes();
    RefreshAlertTable();
</script>

