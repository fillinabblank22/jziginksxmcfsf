<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FrequencyControl.ascx.cs"
    Inherits="Orion_Controls_FrequencyControl" %>
<%@ Reference Control="~/Orion/Controls/SchedulerControl.ascx" %><orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="FrequencyPicker.css" />
<orion:Include runat="server" File="FrequencyPicker.js" />
<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="js/jquery/timePicker.css" />
<orion:Include runat="server" File="js/FrequencyController.js" />
<orion:Include runat="server" File="ReportSchedule.css"  />
<orion:JsonInput runat="server" ID="jsonActionsContainer" ClientIDMode="Static" />
<div id="timeOfDayContainer">
    <div id="frequencyContainer">
        <div data-form="friquenciesList"></div>
    </div>
    <div id="dialogFrequencyContainer"></div> 
    <div id="scheduleDialogContainer" class="schedulePickerControl" data-index="0" >
        <div id="schPickerContainer"></div>
        <div class="buttons"></div>
    </div>
    <input type="hidden" name="timeFormat" id="timeFormat" value='<%= DefaultSanitizer.SanitizeHtml(DatePickerRegionalSettings.GetDatePickerRegionalSettings().Replace("'","")) %>' />
    <script id="noFrequencyTemplate" type="text/x-template">
	{#
		if (useTimePeriodMode) {
			var noneYetText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0033) %>';
			var buttonText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0032) %>';
		} else {
			var noneYetText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_62) %>';
			var buttonText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_32) %>';
		}
	#}
    <div id="noFrequencies" runat="server" class="noFrequenciesBtnContainer">
        <span class="noFrequencyInfo">{{noneYetText}}</span>
             <span>
				<orion:LocalizableButton runat="server" ID="NoFrequencyAddBtn" data-form="addNewFrequency" LocalizedText="CustomText" Text="{{buttonText}}" OnClientClick="return false;" DisplayType="Primary"  CausesValidation="False"/>
            </span>
        <span class="rightFrequencyInfo">&nbsp;</span>
    </div>
    </script>

    
    <script id="frequenciesListTemplate" type="text/x-template">
            <div runat="server" id="frequencyControlHeader" class="tableHeader" >
            <span class="freqName" style="width: 30%">
				<% if (UseTimePeriodMode) { %>
					<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0036) %></span>
				<% } else { %>
					<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_59) %></span>
				<% } %>
            <span class="deleteFreq buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_31) %></span>
            <% if (!SingleScheduleMode)
               { %>
            <span class="copyFrequency buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZT0_41) %></span>
            <% } %>
            <span class="editFrequency buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></span>
            <span class="frequencyInfo buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_141) %></span>
        </div>
        <div class="schedulesGrid">
        {# 
        _.each(frequencies, function(current, index) {
        #}
            <div class="frequencyControlGridItem"><span class="freqName" style="width: 30%">{{current[0].DisplayName}}
                <span class="stateDuringTimePeriod">{{current[0].state}}</span></span>
                <span class="deleteFreq buttonColumn" data-index="{{index}}" data-form="deleteFrequency">
                    <img alt="" src="/Orion/images/Reports/delete_netobject.png"/>
                </span>
                <% if (!SingleScheduleMode)
                    { %>
                <span class="copyFrequency buttonColumn" data-index="{{index}}" data-form="copyFrequency">
                    <img alt="" src="/Orion/images/clone_icon_gray_16x16.png"/>
                </span>
                <% } %>
                <span class="editFrequency buttonColumn" data-index="{{index}}" data-form="editFrequency">
                            <img alt="" src="/Orion/images/edit_icon_gray_16x16.png"/>
                </span>
                <span class="frequencyInfo buttonColumn" ext:qtitle=""
                                                            ext:qtip="{{current[0].ToolTip}}"
                                                            data-index="{{index}}">
                    <img alt="" src="/Orion/images/Show_Last_Note_icon16x16.png"/>
                </span> 
            </div>
    
         {# });
         #}
        </div>
			{#
				if (useTimePeriodMode) {
					var buttonText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0032) %>';
				} else {
					var buttonText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_32) %>';
				}
			#}
            <div class="bigButton addFreq">
                <span class="noFrequencyInfo">&nbsp;</span>
                <orion:LocalizableButton runat="server" ID="AddFrequency"  LocalizedText="CustomText" data-form="addNewFrequency"
                Text="{{buttonText}}" OnClientClick="return false;"
                DisplayType="Secondary" CausesValidation="False"/>
            <span class="rightFrequencyInfo">&nbsp;</span>
            </div>

    </script>


    <script type="text/javascript">

        SW.Orion.FrequencyPickerController = SW.Orion.FrequencyPickerControl({
            useTimePeriodMode: <%= UseTimePeriodMode.ToString().ToLower() %>,
            frequencyTypes: {
                once: "<%= Orion_Controls_SchedulerControl.FrequenciesType.Once.ToString() %>",
                daily: "<%= Orion_Controls_SchedulerControl.FrequenciesType.Daily.ToString() %>",
                weekly: "<%= Orion_Controls_SchedulerControl.FrequenciesType.Weekly.ToString() %>",
                monthly: "<%= Orion_Controls_SchedulerControl.FrequenciesType.Monthly.ToString() %>"
            }
        });
        SW.Orion.FrequencyPickerController.init();
      
    </script>
</div>