<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectViewForViewType.ascx.cs" Inherits="Orion_Controls_SelectViewForViewType" %>

<asp:ListBox runat="server" ID="lbxViewPicker" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$HtmlEncodedResources:CoreWebContent,WEBDATA_AK1_9%>" Value="-2" />
    <asp:ListItem Text="<%$HtmlEncodedResources:CoreWebContent,WEBDATA_AK1_10%>" Value="0" />
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>