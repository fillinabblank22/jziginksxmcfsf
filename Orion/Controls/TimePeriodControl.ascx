<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimePeriodControl.ascx.cs" Inherits="TimePeriodControl" %>
<!--table width="100%" cellpadding="0" cellspacing="0" border="1"-->
<div>
<%if (!InLine){ %>
<tr>
	<td colspan="3" class="formGroupHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_337) %></td>
</tr>
<tr>
	<td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_366) %></td>
	<td class="formRightInput"><%} %><asp:DropDownList ID="TimePeriodList" runat="server"></asp:DropDownList><%if (!InLine){ %></td>
	<td class="formHelpfulText">&nbsp;</td>
</tr>
<tr>
	<td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_367) %></td>
	<td class="formRightInput">&nbsp;</td>
	<td class="formHelpfulText">&nbsp;</td>
</tr>
<tr>
	<td class="formLeftTitle"><%} %><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_369) %><%if (!InLine){ %></td>
	<td class="formRightInput"><%} %><asp:TextBox ID="PeriodBegin" runat="server"></asp:TextBox><%if (!InLine){ %></td>
	<td class="formHelpfulTxt" rowspan="3"><asp:Label ID="Description1" runat="server">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_368) %>&nbsp;</asp:Label><asp:Label ID="Description2" runat="server" Font-Bold="False"></asp:Label></td>
</tr>
<tr>
	<td class="formLeftTitle"><asp:CustomValidator ID="BeginDateValidator" runat="server" ControlToValidate="PeriodBegin"
    Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_372 %>" OnServerValidate="BeginDateValidator_ServerValidate"></asp:CustomValidator></td>
	<td class="formRightInput">&nbsp;</td>

</tr>
<tr>
	<td class="formLeftTitle"><%} %><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_370) %><%if (!InLine){ %></td>
	<td class="formRightInput"><%} %><asp:TextBox ID="PeriodEnd" runat="server"></asp:TextBox><%if (!InLine){ %></td>
</tr>
<tr>
	<td class="formLeftTitle"><asp:CustomValidator ID="EndDateValidator" runat="server" ControlToValidate="PeriodEnd"
    Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_372 %>" OnServerValidate="EndDateValidator_ServerValidate" ></asp:CustomValidator>
<asp:CustomValidator ID="Validator" runat="server"
    Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_371 %>" OnServerValidate="Validator_ServerValidate" ></asp:CustomValidator></td>
	<td class="formRightInput">&nbsp;</td>
	<td class="formHelpfulText">&nbsp;</td>
	</tr><%} %>
    </div>
<!--/table-->


