using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Models.Events;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Federation;

public partial class Orion_Controls_EventsReportControl : System.Web.UI.UserControl
{
	private readonly Log _log = new Log();
    private const string OrionDetailViewUrl = "/Orion/View.aspx?NetObject={0}:{1}";
    private string eventTimeFormater = "{0:g}";
    private readonly List<SwisErrorMessage> errors = new List<SwisErrorMessage>();

    public List<SwisErrorMessage> Errors
    {
        get { return this.errors; }
    }

    public bool ShowTableHeader
    {
        get { return ((DataTable) EventsGrid.DataSource).Rows.Count > 0; }
    }

    public string EventTimeFormater
    {
        set { eventTimeFormater = value; }
        get { return eventTimeFormater; }
    }

    protected void BLExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

    public void LoadData(int nodeID, int netObjectID, string netObjectType, string deviceType, int eventType, DateTime periodBegin, DateTime periodEnd, bool showClearedEvents, int maxRecords)
    {
        try
        {
            // load data from SWIS
            DataTable eventsTable;

            var param = new GetEventsParameter()
            {
                NodeId = nodeID,
                NetObjectId = netObjectID,
                NetObjectType = netObjectType,
                DeviceType = deviceType,
                EventType = eventType,
                FromDate = periodBegin,
                ToDate = periodEnd,
                IncludeAcknowledged = showClearedEvents,
                MaxRecords = maxRecords
            };
            
            eventsTable = EventDAL.GetEvents(param, false);
            this.errors.AddRange(SwisDataTableParser.GetDataTableErrors(eventsTable));
            // adds new column to the table for URL resolving
            eventsTable.Columns.Add("URL");

            foreach (DataRow r in eventsTable.Rows)
            {
                string objectType = string.Empty;

                object netObjId = r["NetObjectID"];
                object netObjType = r["NetObjectType"];
                object netObjValue = r["NetObjectValue"];

                if (netObjType != DBNull.Value)
                    objectType = (netObjType.ToString().Trim().Equals("Y", StringComparison.OrdinalIgnoreCase))? "AM": (netObjType.ToString().Trim().Equals("Z", StringComparison.OrdinalIgnoreCase))? "AA": netObjType.ToString().Trim().ToUpper();

                if (netObjType != DBNull.Value
                    && !String.IsNullOrEmpty((string) netObjType)
                    && netObjValue != DBNull.Value
                    && !String.IsNullOrEmpty((string) netObjValue))
                {
                    r["URL"] = String.Format(CultureInfo.InvariantCulture, OrionDetailViewUrl, objectType, netObjValue);
                }
                else if ((netObjType is DBNull) || (netObjId is DBNull) || (netObjType.ToString().Trim() == String.Empty) ||((int) netObjId == 0))
                {
                    r["URL"] = String.Empty;
                }
                else
                {
                    r["URL"] = String.Format(CultureInfo.InvariantCulture, OrionDetailViewUrl, objectType, netObjId);
                }

                int color = (int) r["BackColor"];
                r["BackColor"] = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);

            }

            // sort by EventTime
            eventsTable.DefaultView.Sort = eventsTable.Columns["EventTime"] + " DESC";

            // data bind
            LoadData(eventsTable);
        }
        catch (Exception ex)
        {
            BLExceptionHandler(ex);
            throw;
        }
	}
    
    public void LoadData(DataTable eventsTable)
    {
        // data bind
        this.EventsGrid.DataSource = eventsTable;
        this.EventsGrid.DataBind();
    }
}