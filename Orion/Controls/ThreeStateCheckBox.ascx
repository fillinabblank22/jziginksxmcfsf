<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThreeStateCheckBox.ascx.cs"
    Inherits="ThreeStateCheckBox" %>

<script type="text/javascript">
    //<![CDATA[
    var ChangeState = function() {

        var state = document.getElementById("<%=this.CBState.ClientID%>");

        if (state.value == 0) {
            state.value = 1;
        }
        else if (state.value == 1) {
            state.value = 2;
        }
        else {
            state.value = 1;
        }

        var cb = document.getElementById("cbImage");

        switch (state.value) {
            case "0":
                cb.src = "../images/CheckBoxHalfChecked.gif";
                break;
            case "1":
                cb.src = "../images/CheckBoxUnchecked.gif";
                break;
            case "2":
                cb.src = "../images/CheckBoxChecked.gif";
        }
    }
    //]]>
</script>

<img id="cbImage" src="<%= DefaultSanitizer.SanitizeHtml(this.ImageString) %>" onclick="ChangeState()" />
<asp:HiddenField ID="CBState" runat="server" />