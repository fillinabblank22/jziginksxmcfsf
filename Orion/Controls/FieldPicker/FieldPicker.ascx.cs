﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Newtonsoft.Json;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Reporting.Models.Selection;

public partial class FieldPickerUserControl : UserControl
{
    public string ViewType { get; set; }
    public string ClientInstanceName { get; set; }
    private const string DummyJsAction = "function(){}";

    /// <summary>
    /// Javascript function which will be called when object is created.
    /// Signature: onCreated(picker);
    /// </summary>
    public string OnCreatedJs { get; set; }

    public string OnSelectedJs { get; set; }

    public bool SingleMode { get; set; }

    public bool UseNetObjectPicker { get; set; }

    public bool UseDataAggregation { get; set; }

    public DataSource DataSource { get; set; }

    protected string DataSourceJson
    {
        get
        {
            if (DataSource != null)
            {
                DataSource.Name = WebSecurityHelper.HtmlEncode(DataSource.Name);
            }
            return JsonConvert.SerializeObject(DataSource);
        }
    }

    #region Localizable texts

    public string GroupByHeaderTextI18N { get; set; }
    public string SelectedFieldsTextI18N { get; set; }
    public string NoSelectedFieldsTextI18N { get; set; }

    public string AddFieldsButtonTextI18N
    {
        get { return btnDialogOk.Text; }
        set { btnDialogOk.Text = value; }
    }

    public string DialogTitleTextI18N { get; set; }
    public string SearchWatermarkTextI18N { get; set; }
    public string FieldsGridHeaderTextI18N { get; set; }

    #endregion

    /// <summary>
    /// Get or set the collection of field's ref ids which are used for limitations.
    /// According to this collection is computed the longest navigation path which limit the selection.
    /// </summary>
    public IEnumerable<string> NavigationPathFilterFields { get; set; }

    /// <summary>
    /// Get or set if the navigation path limitation should be disabled. 
    /// Limitation prevents againts cross-joins in result query. 
    /// </summary>
    public bool DisableNavigationPathLimitation { get; set; }

    /// <summary>
    /// Url of web service which will provide data for field picker.
    /// </summary>
    public string DataProviderUrl { get; set; }

    public IDictionary<string, object> ExtraRequestParams { get; set; }

    protected string ExtraRequestParamsJson
    {
        get { return JsonConvert.SerializeObject(ExtraRequestParams); }
    }

    protected string NavigationPathFilterFieldsJson
    {
        get { return JsonConvert.SerializeObject(NavigationPathFilterFields); }
    }

    public FieldPickerUserControl()
    {
        ViewType = string.Empty;
        OnCreatedJs = DummyJsAction;
        OnSelectedJs = DummyJsAction;
        NavigationPathFilterFields = Enumerable.Empty<string>();
    }

    protected override void OnInit(EventArgs e)
    {
        SetDefaultTexts();

        base.OnInit(e);
    }

    private void SetDefaultTexts()
    {
        GroupByHeaderTextI18N = GroupByHeaderTextI18N ?? Resources.CoreWebContent.WEBDATA_ZT0_2;
        SelectedFieldsTextI18N = SelectedFieldsTextI18N ?? Resources.CoreWebContent.WEBDATA_ZT0_3;
        NoSelectedFieldsTextI18N = NoSelectedFieldsTextI18N ?? Resources.CoreWebContent.WEBDATA_ZT0_4;
        AddFieldsButtonTextI18N = string.IsNullOrEmpty(AddFieldsButtonTextI18N) ? Resources.CoreWebContent.WEBDATA_ZT0_6 : AddFieldsButtonTextI18N;
        DialogTitleTextI18N = DialogTitleTextI18N ?? Resources.CoreWebContent.WEBDATA_ZT0_5;
        SearchWatermarkTextI18N = SearchWatermarkTextI18N ?? Resources.CoreWebContent.WEBDATA_ZT0_7;
        FieldsGridHeaderTextI18N = FieldsGridHeaderTextI18N ?? Resources.CoreWebContent.WEBDATA_ZT0_12;
    }
}