<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FieldPicker.ascx.cs" Inherits="FieldPickerUserControl" %>

<orion:Include ID="Include4" File="ItemPicker.css" runat="server" /> 
<orion:Include ID="Include1" runat="server" Framework="Ext"  FrameworkVersion="3.4" />
<orion:Include ID="Include3" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" File="ItemPicker.js" runat="server" />
<orion:Include ID="Include5" File="FieldsPicker.js" runat="server" />

<div class="itemPickerOffPage" id="itemPickerContainer" runat="server">
   
<div id="fieldPickerDialog" runat="server" style="background-color: #fff !important"  >

    <table class="itemPicker">
        <tr>
            <td class="groupByColumn headerColumn">
                <%= DefaultSanitizer.SanitizeHtml(GroupByHeaderTextI18N) %>
            </td>

            <td class="gridColumn headerColumn">
                <input type="text" id="tbSearch" runat="server" class="searchInput" />
                <span runat="server" id="btnHolder"></span>
            </td>

            <td class="selectedItemsColumn headerColumn ">
                <%= DefaultSanitizer.SanitizeHtml(SelectedFieldsTextI18N) %>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <div runat="server" id="fieldPickerGrid"></div>
            </td>
            <td valign="top" class="selectedItemsColumn">
                <div id="selectedFields" runat="server" class="selectedItems">

                    <div class="selectedItemsEmpty">
                        <%= DefaultSanitizer.SanitizeHtml(NoSelectedFieldsTextI18N) %>
                    </div>
                    <table><tbody></tbody></table>
                </div>
            </td>
        </tr>
		<tr>
			<td colspan="2">
				<div id="changeNetObjectSection" style="margin-top: 5px; margin-bottom: 15px; max-width: 950px;">
					<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_7) %></span>
					<span id="currentNetObjectPlaceHolder">
					</span>
					<span><a id="changeNetObjectLink" class="sw-link x-small-text objectLink" href="javascript:void(0);"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_6) %></a></span>
        
					<div id="insertVariableNetObjectPickerDlg" class="objectPickerDlg" style="display: none" runat="server">
						<div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
							<orion:LocalizableButton ID="btnNetObjectChange"  CausesValidation="false" CssClass="objectChange" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_6 %>" OnClientClick="return false;" />
							<orion:LocalizableButton ID="btnNetObjectCancel"  CausesValidation="false" CssClass="objectCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="return false;" />
						</div>
					</div>
				</div>
			</td>
		</tr>
        <tr>
            <td colspan="2">
              <div id="dataAggregationSection" runat="server"></div>
            </td>
        </tr>
    </table>
    <div class="sw-btn-bar" style="white-space: nowrap; float: right;">
        <orion:LocalizableButtonLink ID="btnDialogOk" runat="server" DisplayType="Primary" LocalizedText="CustomText"  />
        <orion:LocalizableButtonLink ID="btnDialogCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>" />
    </div>

</div>
</div>
<script type="text/javascript">
    $(function() {

        var picker = new SW.Core.Pickers.FieldPicker({
            containerElementId : '<%= itemPickerContainer.ClientID %>',
            renderTo: '<%= fieldPickerGrid.ClientID %>',
            dataAggregationRenderTo: '<%= dataAggregationSection.ClientID %>',
            selectedItemsRenderTo: '<%= selectedFields.ClientID %>',
            onCreated: <%= OnCreatedJs %>,
            onSelected :  <%= OnSelectedJs %>,
            selectionMode: '<%= SingleMode ? "Single" : "Multiple" %>', 
            dataSourceJson: <%= string.IsNullOrEmpty(DataSourceJson) ? "[]" : DataSourceJson %>,
            fieldsGridHeaderText: '<%= FieldsGridHeaderTextI18N %>',
            netObjectDialogId: '<%=insertVariableNetObjectPickerDlg.ClientID %>',
            useNetOjectPicker: '<%= UseNetObjectPicker%>',
            useDataAggregation:'<%= UseDataAggregation%>',
            dialog: {
                renderTo: '<%= fieldPickerDialog.ClientID %>',
                title: '<%= DialogTitleTextI18N %>',
                btnOk: '#<%= btnDialogOk.ClientID %>',
                btnCancel: '#<%= btnDialogCancel.ClientID %>'
            },
            navigationPathFilterFieldsJson : <%= NavigationPathFilterFieldsJson %>,
            disableNavigationPathLimitation : <%= DisableNavigationPathLimitation.ToString().ToLower() %>,
            dataProviderUrl : '<%= DataProviderUrl %>',
            extraRequestParams : <%= ExtraRequestParamsJson %>
        });
        
           
        var searcher = new  SW.Core.Pickers.ItemPickerSearcher ({
            searchButtonHolder : '#<%= btnHolder.ClientID %>',
            searchInput : '#<%= tbSearch.ClientID %>', 
            onSearched : picker.Search,
            autocomplete: {
                 enabled: false
            },
              watermarkText: '<%= SearchWatermarkTextI18N %>'
          });

        var clientInstanceName = '<%= ClientInstanceName %>';
        if (clientInstanceName != '') {
            SW.Core.Pickers.FieldPicker.Instances[clientInstanceName] = picker;
        }
    });
</script>
