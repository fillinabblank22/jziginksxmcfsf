using System;
using System.Linq;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.DAL;
using SNMPv3AuthType = SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType;
using SNMPv3PrivacyType = SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType;
using SnmpCredentials = SolarWinds.Orion.Core.Common.Models.SnmpCredentials;

public partial class Orion_Controls_SnmpCredentialsControl : System.Web.UI.UserControl
{
	private readonly bool fipsModeEnabled = EnginesDAL.IsFIPSModeEnabledOnAnyEngine();

    public SnmpCredentials EditedCredentials { get; set; }
    public SNMPVersion SnmpVersion { get; set; }
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        SnmpType.Items.Add(new ListItem("SNMP1", SNMPVersion.SNMP1.ToString()));
        SnmpType.Items.Add(new ListItem("SNMP2c", SNMPVersion.SNMP2c.ToString()));
        SnmpType.Items.Add(new ListItem("SNMP3", SNMPVersion.SNMP3.ToString()));

        if (!fipsModeEnabled)
        {
            AuthMetodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_75, SNMPv3AuthType.MD5.ToString()));
        }
        AuthMetodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_76, SNMPv3AuthType.SHA1.ToString()));

        if (!fipsModeEnabled)
        {
            PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_77, SNMPv3PrivacyType.DES56.ToString()));
        }
        PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_78, SNMPv3PrivacyType.AES128.ToString()));
        PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_85, SNMPv3PrivacyType.AES192.ToString()));
        PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_86, SNMPv3PrivacyType.AES256.ToString()));
    }

    // on load event
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

       // bind credentials to datagrid
        if (!IsPostBack)
        {
            if (EditedCredentials == null)
            {
                InitV3Controls();
            }
            else
            {
                ApplySNMPCredentialsEntry();
            }
        }
        PrivPassword.Attributes["type"] = "password";
        AuthPassword.Attributes["type"] = "password";
    }

    private void ApplySNMPCredentialsEntry()
    {
        if (EditedCredentials != null)
        {
            SnmpType.SelectedValue = SnmpVersion.ToString();
            CommunityStringTB.Text = string.IsNullOrEmpty(EditedCredentials.CommunityString) ? "public" : EditedCredentials.CommunityString;
            AuthPassword.Text = EditedCredentials.SNMPv3AuthPassword;
            PrivPassword.Text = EditedCredentials.SNMPv3PrivacyPassword;
            AuthPasswordIsKey.Checked = EditedCredentials.SNMPV3AuthKeyIsPwd;
            PrivPasswordIsKey.Checked = EditedCredentials.SNMPV3PrivKeyIsPwd;
            UserNameTextBox.Text = EditedCredentials.SNMPv3UserName;
            SetAuthMethodInDropDownList(EditedCredentials.SNMPv3AuthType);
            SetPrivacyMethodDropDownList(EditedCredentials.SNMPv3PrivacyType);
        }

    }

    protected void InitV3Controls()
    {
        
        this.AuthMetodDropList.SelectedIndex = 0;

        this.PrivMethodDropList.SelectedIndex = 0;
        
        this.AuthPasswordIsKey.Checked = false;
        
        this.PrivPasswordIsKey.Checked = false;

        this.UserNameTextBox.Text = String.Empty;
    }

    private void SetAuthMethodInDropDownList(SNMPv3AuthType authenticationType)
    {
        switch (authenticationType)
        {
            case SNMPv3AuthType.MD5:// MD5 is not present when FIPS mode is enabled
                if (this.AuthMetodDropList.Items.Cast<ListItem>().Any(item => string.Equals(item.Value, SNMPv3AuthType.MD5.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    this.AuthMetodDropList.SelectedValue = authenticationType.ToString();
                }
                else
                {
                    this.AuthMetodDropList.SelectedIndex = 0;
                }
                break;
            case SNMPv3AuthType.SHA1:
                this.AuthMetodDropList.SelectedValue = authenticationType.ToString();
                break;
        }
    }

    private void SetPrivacyMethodDropDownList(SNMPv3PrivacyType privacyType)
    {
        switch (privacyType)
        {
            case SNMPv3PrivacyType.DES56:
                if (this.PrivMethodDropList.Items.Cast<ListItem>().Any(item => string.Equals(item.Value, SNMPv3PrivacyType.DES56.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    this.PrivMethodDropList.SelectedValue = privacyType.ToString();
                }
                else
                {
                    this.PrivMethodDropList.SelectedIndex = 0;
                }
                break;
            case SNMPv3PrivacyType.AES128:
            case SNMPv3PrivacyType.AES192:
            case SNMPv3PrivacyType.AES256:
                this.PrivMethodDropList.SelectedValue = privacyType.ToString();
                break;
        }
    }
}
	
