<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SchedulerControl.ascx.cs"
    Inherits="Orion_Controls_SchedulerControl" %>
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>

<style type="text/css">
    .placeholder { color: #aaa; }
</style>

<script type="text/javascript">
    $(function () {
        $('.datePicker').datepicker('option', 'onClose', handleBlur);
        $('[customplaceholder]').focus(handleFocus).blur(handleBlur).blur();
        $("[customplaceholder]").change(handleChange);
        $('[customplaceholder]').parents('form').submit(handleCleanupIfEmpty);

        function handleFocus() {
            var input = $(this);
            if (input.val() == input.attr('customplaceholder')) {
                input.val('');
                input.addClass('placeholder');
            }
            else {
                input.removeClass('placeholder');
            }
        }

        function handleBlur() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('customplaceholder')) {
                input.addClass('placeholder');
                input.val(input.attr('customplaceholder'));
            }
            else {
                input.removeClass('placeholder');
            }
        }

        function handleChange() {
            var input = $(this);
            if (input.val() == input.attr('customplaceholder')) {
                input.addClass('placeholder');
            }
            else {
                input.removeClass('placeholder');
            }
        }

        function handleCleanupIfEmpty() {
            $(this).find('[customplaceholder]').each(function () {
                var input = $(this);
                if (input.val() == input.attr('customplaceholder')) {
                    input.val('');
                }
            });
        }

    	<% if (UseTimePeriodMode) { %>
	    	function setEnableDisableEveryString() {
	    		var everyStrings = $(".everystring");

	    		var enableEvery = $("#<%= impactFrequency.ClientID %> input:checked").val();

	    		if (enableEvery == "<%= Boolean.TrueString %>") {
			        everyStrings.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_205) %>");
			        $(".dailyFrequency").text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0034) %>");

			    } else {
	    			everyStrings.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_206) %>");
	    			$(".dailyFrequency").text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0035) %>");
	    		}
	    	}

    		$("#<%= impactFrequency.ClientID %> input").change(setEnableDisableEveryString);

    		setEnableDisableEveryString();
		<% } %>
    });
</script>

<div class="frequencySelection">
    <div class="frequencyName">
    <label class="boldLabel">
		<% if (UseTimePeriodMode) { %>
			<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0013) %>
		<% } else { %>
			<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_59) %>
		<% } %>
    </label>
        <br/>
    <asp:TextBox ID="frequencyName" Width="100%" runat="server" placeholder="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_99 %>" MaxLength="255"></asp:TextBox><span class="emptyName"></span>
    </div>
    <br />
    <div>
        <% if (UseTimePeriodMode)
           { %>
            <asp:RadioButtonList ID="impactFrequency" runat="server" RepeatDirection="Vertical" CssClass="impactFrequency">
                <asp:ListItem  Value="<%$ HtmlEncodedCode: bool.TrueString %>" Selected="True"></asp:ListItem>
                <asp:ListItem  Value="<%$ HtmlEncodedCode: bool.FalseString %>"></asp:ListItem>
            </asp:RadioButtonList> 
        <% } %>
    </div>
    <div style="padding: 5px;">
        <label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_46) %></label>
        <br/>
        <asp:DropDownList ID="selectFrequency" class="friqSelect" runat="server" Width="225px" onchange="SW.Orion.ScheduleConfigurationController.DropdownSelectionChange(this);">
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_72 %>" Value="<%$ HtmlEncodedCode: FrequenciesType.Once.ToString()%>"></asp:ListItem>
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_115 %>" Value="<%$ HtmlEncodedCode: FrequenciesType.Daily.ToString()%>"></asp:ListItem>
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_17 %>" Value="<%$ HtmlEncodedCode: FrequenciesType.Weekly.ToString()%>"></asp:ListItem>
            <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_18 %>" Value="<%$ HtmlEncodedCode: FrequenciesType.Monthly.ToString()%>"></asp:ListItem>
        </asp:DropDownList>
        <div class="frequencyBoxes" style="margin: 10px 0;">
            <div id="frequencyTypeOnce" class="frequencyHeader" runat="server">
               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_19) %>
            </div>
            <div id="frequencyTypeDaily" class="frequencyHeader" runat="server">
				<span class="everystring dailyFrequency"><%= DefaultSanitizer.SanitizeHtml(EveryString) %></span>
                <asp:RadioButton ID="dayOfMonth" runat="server" GroupName="EnableDisableEvery" />
                <asp:DropDownList ID="daySelectionDaily" runat="server" Width="50px">
                </asp:DropDownList>
                 <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_30) %>
                <asp:RadioButton ID="workDayOfWeek" CssClass="workDayOfWeek" runat="server" GroupName="EnableDisableEvery" />
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_20) %></label>
            </div>
            <div id="frequencyTypeWeekly" runat="server">
				<span class="everystring"><%= DefaultSanitizer.SanitizeHtml(EveryString) %></span>
                <table class="chackboxTable">
                    <tr>
                        <td>
                            <asp:CheckBoxList ID="cblWeekDays" CssClass="cbl cblWeekDays" runat="server" RepeatDirection="Horizontal"
                                RepeatColumns="3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="float: right; color: dodgerblue; cursor: pointer; text-decoration: underline; margin: 0 5px;" 
                                   onclick="SW.Orion.ScheduleConfigurationController.CheckALL(this)">
                                 <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_140) %></label>
                        </td>
                        <td>
                            <label style="color: dodgerblue; cursor: pointer; text-decoration: underline; margin: 0 5px;" 
                                   onclick="SW.Orion.ScheduleConfigurationController.CheckNone(this)">
                               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_10) %></label>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="frequencyTypeMonthly" runat="server">
                <div>
                    <span class="everystring"><%= DefaultSanitizer.SanitizeHtml(EveryString) %></span>
                </div>
                <table class="chackboxTable">
                    <tr>
                        <td>
                            <asp:CheckBoxList ID="cblMonths" CssClass="cbl cblMonths" runat="server" RepeatDirection="Horizontal"
                                RepeatColumns="3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="float: right; color: dodgerblue; cursor: pointer; text-decoration: underline; margin: 0 5px;" 
                                   onclick="SW.Orion.ScheduleConfigurationController.CheckALL(this)">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_140) %></label>
                        </td>
                        <td>
                            <label style="color: dodgerblue; cursor: pointer; text-decoration: underline; margin: 0 10px;" 
                                   onclick="SW.Orion.ScheduleConfigurationController.CheckNone(this)">
                               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_10) %></label>
                        </td>
                    </tr>
                </table>
                <div class="monthlyCondions">
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_22) %></label>
                    <table class="monthlyCondionsTable">
                        <tr>
                            <td>
                                <asp:RadioButton ID="SpecificDayRadioButton" CssClass="SpecificDayRadioButton" runat="server" onclick="SW.Orion.ScheduleConfigurationController.RadioButtonStateChange(this);"/>
                            </td>
                            <td>
                                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_235) %></label>
                                <asp:DropDownList ID="daySelection" CssClass="specificDaySelection" runat="server" Width="50px">
                                </asp:DropDownList>
                                <span class = "invalidDayValidationMessage" style="display:none; color:red; font-size: 12px;"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="advancedDaySelection" CssClass="advancedDaySelection" runat="server" onclick="SW.Orion.ScheduleConfigurationController.RadioButtonStateChange(this);" />
                            </td>
                            <td>
                                <asp:DropDownList ID="weekNumberSelection" CssClass="weekNumberSelection" runat="server" Width="100px">
                                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_23 %>" Value="First"></asp:ListItem>
                                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_24 %>" Value="Second"></asp:ListItem>
                                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_25 %>" Value="Third"></asp:ListItem>
                                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_26 %>" Value="Forth"></asp:ListItem>
                                    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_27 %>" Value="Fifth"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="dayOfWeekSelection" CssClass="dayOfWeekSelection" runat="server" Width="100px">
                                </asp:DropDownList>
                                 <span class = "weekRiskMessage" style="display:none; color: #5F5F5F; font-size: 10px;"></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="timePickers" style="border-left: 1px solid #BBBBBB; border-bottom: 1px solid #BBBBBB; border-right: 1px solid #BBBBBB; padding: 19px; min-height: 30px;">
                <div class = "TimeControl"></div>
                                        <% if (!SingleScheduleMode) { %>
                                        <div class="addTimeButton">
                                            <% if (!UseTimePeriodMode)
                                               { %>
                                            <orion:LocalizableButton runat="server" ID="AddTime" OnClientClick="SW.Orion.ScheduleConfigurationController.CallAddTimeControl(null,this); return false;"
                                                LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_31 %>" DisplayType="Small" />
                                                <% } else { %>
                                            <orion:LocalizableButton runat="server" ID="AddTimePeriod" OnClientClick="SW.Orion.ScheduleConfigurationController.CallAddTimePeriodControl(null,null,this); return false;"
                                                LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_105 %>" DisplayType="Small" />
                                                <% } %>

                                        </div>
                                        <% } %>
                <asp:HiddenField ID="dateTimeJson" runat="server" />
                <asp:HiddenField ID="intervalEndTimeJson" runat="server" />
                <asp:HiddenField ID="frequenciesIdsJson" runat="server" />
            </div>
        </div>
        <% if (UseTimePeriodMode)
            { %>
            <div style="margin-bottom: 10px">
                <div style="float: left">
                    <img style="margin: 0 5px -3px 5px;" alt="" 
                 ext:qwidth="315" 
                 ext:qtip="" src="/Orion/images/SpanMidnightIcon.png"/>
                </div>
                <div style="float: left">
                    <div><b><label style="font-size: 11px;color: #646464"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_17) %></label></b></div>
                    <div style="width: 400px; margin-bottom: 30px;">
                      <label style="font-size: 10px;color: #646464"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_18) %></label> </div>
                    </div>
            </div>
        <br style="clear:both;"/>
        <% } %>

        <table class="startEndTimePicker" cellpadding="0" cellspacing="0">
        <tr><td><label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_33) %></label>
        </td></tr>
        <tr><td>
            <asp:DropDownList ID="StartDateDropdown" runat="server" class="startTimePicker" onchange="SW.Orion.ScheduleConfigurationController.StartTimeSelectionChange(this);">
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_140 %>" Value="RunNow"></asp:ListItem>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_29 %>" Value="SpecificDate"></asp:ListItem>
            </asp:DropDownList>
            <div class="specStartDateTime">
                <orion:DateTimePicker runat="server" ID="specStartDate" EnableDateTimeValidation="True" ForceMarkupClientScriptBlockUsage="true"  />
            </div>
            </td>
        </tr>
        <tr><td style="padding-top: 15px;">
            <asp:CheckBox ID="endTimeIsSpecified" runat="server" class="endTimeCheckBox" onclick="SW.Orion.ScheduleConfigurationController.CheckEndTimeSpec(this);"/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_34) %></label>
            <img style="margin: 0 5px -3px 5px;" alt="" 
                 ext:qwidth="315" 
                 ext:qtitle="" 
                 ext:qtip="<%= DefaultSanitizer.SanitizeHtml(UseTimePeriodMode ? Resources.CoreWebContent.WEBDATA_BV0_0044 : Resources.CoreWebContent.WEBDATA_AB0_37) %>" src="/Orion/images/Info_Icon_MoreDecent_16x16.png"/>
            </td>
        </tr>
        <tr>
        <td>
        <div class="endTimePicker">
            <orion:DateTimePicker runat="server" ID="specEndDate" EnableDateTimeValidation="True" ForceMarkupClientScriptBlockUsage="true" />
        </div>
        </td>
        </tr>
        </table>
    </div>
</div>

