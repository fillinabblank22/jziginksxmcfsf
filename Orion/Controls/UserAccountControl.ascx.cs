﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_Controls_UserAccountControl : System.Web.UI.UserControl
{
    private string User
    {
        get { return userName.Text.Trim(); }

        set { userName.Text = value; }
    }

    private string Pass
    {
        get { return password.Text; }

        set { password.Text = value; }
    }

    public bool AllowNoUser { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        noUserDefined.Visible = AllowNoUser;

    }

    public string getAccount()
    {
        var accountName = string.Empty;
        if (useCurrentUser.Checked)
        {
            accountName = this.Profile.UserName;
        }
        else if (useAnotherUser.Checked)
        {
            accountName = User;
        }
        if (!useCurrentUser.Checked && !TestCredentials())
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AB0_68, Resources.CoreWebContent.WEBDATA_AB0_69));
            return string.Empty;
        }
        return accountName;
    }

    public void setAccount(string accountName)
    {
        if (accountName == this.Profile.UserName)
        {
            useCurrentUser.Checked = true;
            useAnotherUser.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "updateSelection", "<script language='javascript'>if (typeof collapseExpandUserCreds === 'function'){ collapseExpandUserCreds();}</script>", false);
        }
        else if (string.IsNullOrEmpty(accountName))
        {
            notUseUser.Checked = true;
        }
        else
        {
            useAnotherUser.Checked = true;
            useCurrentUser.Checked = false;
        }
        User = accountName;
    }

    private void registerScript(string errorsMessage)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
    }

    private bool TestCredentials()
    {
        if (string.IsNullOrEmpty(User))
        {
            return false;
        }

        try
        {
            if ((User.Contains("\\")) || (User.Contains("@"))) //domain creds
            {
                WindowsIdentity logonIdentity = OrionMixedModeAuth.GetIdentity(User, Pass);

                return AuthorizationManager.AuthorizeUser(logonIdentity);
            }

            return Membership.ValidateUser(User, Pass);
        }
        catch
        {
            return false;
        }
    }
}