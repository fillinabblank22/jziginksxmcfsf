<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetObjectPicker.ascx.cs"
    Inherits="Orion_Controls_NetObjectPicker" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Register TagPrefix="orion" TagName="UnresponsiveServersNotifier" Src="~/Orion/Controls/UnresponsiveServersNotifierControl.ascx" %>
<orion:Include runat="server" File="Admin/Containers/Containers.css" />
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="NetObjectPicker.js" />
<orion:InlineCss runat="server">
.selected-objects-panel {padding-left: 10px; vertical-align: top;}
#selectedObjectsEmpty, .selected-objects-panel table {background-color: #ECECEC; width: 100%;}
#selectedObjectsEmpty { padding: 3px 3px 3px 3px;}
.selected-objects-panel table td { padding: 3px 3px 3px 3px;}
.GroupSection select {width:99%!important;}
</orion:InlineCss>
<div id="netObjectPickerPlaceHolder">
    <% if (SwisFederationInfo.IsFederationEnabled)
       { %>
        <orion:UnresponsiveServersNotifier ID="UnresponsiveServersNotifier" runat="server" />
    <% } %>
    <input type="hidden" name="isFederationEnabled" id="isFederationEnabled" value= <%= SwisFederationInfo.IsFederationEnabled %> />
    <div id="GroupItemsSelector">
        <div class="GroupSection">
            <table style="width: 100%">
                <tr>
                    <% if (SwisFederationInfo.IsFederationEnabled)
                       { %>
                       <td style="width: 40%">
                            <label for="solarwindsServersSelect">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.NetObjectPicker_SWServer) %></label>
                                <select id="solarwindsServersSelect"></select>
                        </td>
                       <% } %>
                    <td style="width: 30%">
                        <label for="entitiesSelect">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_252) %></label>
                        <select id="entitiesSelect">
                        </select>
                    </td>
                    <td style="width: 40%">
                        <label for="groupBySelect">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_5) %></label>
                        <select id="groupBySelect">
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="GroupSection">
            <label for="searchBox">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_225) %></label>
            <table>
                <tr>
                    <td>
                        <input type="text" class="searchBox" id="searchBox" name="searchBox" />
                    </td>
                    <td>
                        <a href="javascript:void(0);" id="searchButton">
                            <img src="/Orion/images/Button.SearchIcon.gif" /></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table style="width: 100%; table-layout: fixed;">
        <tr>
            <td style="width: 50%; font-weight: bold;" id="label-available">
            </td>
            <td style="width: 40%; font-weight: bold; padding-left: 10px;" id="label-selected">
            </td>
            <td style="width: 10%; text-align: right">
                <a href="javascript:void(0);" style="color: #336699; font-size: 10px;" id="remove-all-button"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_164) %></a>
            </td>
        </tr>
        <tr>
            <td style="width: 50%" class="netObject-tree-panel" id="netobject-tree-panel">
            </td>
            <td class="selected-objects-panel" id="selected-objects-panel" colspan="2">
                <div style="height: 370px; overflow-y: auto; overflow-x: hidden;">
                    <div id="selectedObjectsEmpty">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_104) %></div>
                    <table>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="groupByType" />
    <input type="hidden" name="Orion_NetObjectPicker_MaxSelectedCount" id="Orion_NetObjectPicker_MaxSelectedCount" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebSettingsDAL.Get("Orion_NetObjectPicker_MaxSelectedCount", "300")) %>' />
    <asp:HiddenField ID="gridItems" runat="server" /> 
</div>
<script type="text/javascript">
    // <![CDATA[
    SW.Orion.NetObjectPicker.SetGridItemsFieldClientID('<%= gridItems.ClientID %>');
    <% if (SwisFederationInfo.IsFederationEnabled) { %>
    SW.Orion.NetObjectPicker.SetUnresponsiveServersNotifierClientID('<%= UnresponsiveServersNotifier.GetControlContainterID() %>');
    <% } %>
    // ]]>
</script>