﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Interfaces;
using SolarWinds.Orion.Core.Models.Schedules;

public partial class Orion_Controls_SchedulerControl : UserControl
{
    //Our croon expression recognize days starting from 0 to 6;
    private readonly List<string> weekIntervals = new List<string>() { "1-7", "8-14", "15-21", "22-28", "29-31" };
    private List<ISchedule> schedules = new List<ISchedule>();
    public bool SingleScheduleMode { get; set; }
    public bool UseTimePeriodMode { get; set; }

    public string DisableTimePeriodStr { get; set; }
    public string EnableTimePeriodStr { get; set; }
    public int BrowserUtcOffset { get; set; }

    protected string EveryString 
    {
        get { return UseTimePeriodMode ? Resources.CoreWebContent.WEBCODE_TM0_120 : Resources.CoreWebContent.WEBDATA_AB0_21; }
    }

    public string JsonSchedules
    {
        set
        {
            var tempSchedules = new List<ISchedule>();
            if (UseTimePeriodMode)
            {
                var deserializedList = JsonConvert.DeserializeObject<List<TimePeriodSchedule>>(value);
                deserializedList.ForEach(tempSchedules.Add);
            }
            else
            {
                var deserializedList = JsonConvert.DeserializeObject<List<ReportSchedule>>(value);
                deserializedList.ForEach(tempSchedules.Add);
            }

            Schedules = tempSchedules;
        }
    }

    private ISchedule ScheduleItem
    {
        get
        {
            return UseTimePeriodMode ? (ISchedule)new TimePeriodSchedule() : new ReportSchedule();
        }
    }
    public List<ISchedule> Schedules
    {
        get { return schedules; }
        set
        {
            schedules = value;
            SetSchedule();
        }
    }
    public enum FrequenciesType
    {
        Once,
        Daily,
        Weekly,
        Monthly
    };
    protected void Page_Load(object sender, EventArgs e)
    {
        if (UseTimePeriodMode)
        {
            selectFrequency.Items.Remove(selectFrequency.Items.FindByValue(FrequenciesType.Once.ToString()));
            frequencyTypeOnce.Attributes["style"] = "display:none;";
        }
        if (cblWeekDays.Items.Count == 0)
            InternalWeekDaysListInit();
        if (cblMonths.Items.Count == 0)
            InternalMonthsListInit();
        if (daySelection.Items.Count == 0)
            InternalMonthDaysListInit();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateFriquency", "<script type='text/javascript'>SW.Orion.ScheduleConfigurationController.UpdateControlWithSelections();</script>", false);

        impactFrequency.Items[0].Text = !string.IsNullOrEmpty(DisableTimePeriodStr) ? DisableTimePeriodStr : Resources.CoreWebContent.WEBDATA_OM0_1;
        impactFrequency.Items[1].Text = !string.IsNullOrEmpty(EnableTimePeriodStr) ? EnableTimePeriodStr : Resources.CoreWebContent.WEBDATA_OM0_2;
    }

    private void InternalWeekDaysListInit()
    {
        foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
        {
            cblWeekDays.Items.Add(new ListItem(CronHelper.GetLocalizedDayOfWeek(day.ToString()), day.ToString()));
            dayOfWeekSelection.Items.Add(new ListItem(CronHelper.GetLocalizedDayOfWeek(day.ToString()), day.ToString()));
        }
    }

    private void InternalMonthDaysListInit()
    {
        for (int i = 1; i <= 31; i++)
        {
            daySelection.Items.Add(i.ToString());
            daySelectionDaily.Items.Add(i.ToString());
        }
    }

    private void InternalMonthsListInit()
    {
        for (int i = 1; i <= 12; i++)
            cblMonths.Items.Add(new ListItem(CronHelper.GetLocalizedMonthName(i.ToString()), i.ToString()));
    }

    private string CollectCtrontabExpression(DateTime dateTime)
    {
        string expression = string.Empty;
        switch (selectFrequency.SelectedValue)
        {
            case "Once":
                expression = null;
                break;
            case "Daily":
                if (!workDayOfWeek.Checked)
                    expression = string.Format("{0} {1} */{2} * *", dateTime.Minute, dateTime.Hour.ToString(), daySelectionDaily.SelectedValue);
                else
                    expression = string.Format("{0} {1} * * {2}", dateTime.Minute, dateTime.Hour.ToString(), CommonConstants.WorkingDayOfWeekInterval);
                break;
            case "Weekly":
                string daysOfWeek = cblWeekDays.Items.Cast<ListItem>()
                   .Where(item => item.Selected)
                   .Aggregate("", (current, item) => current + ((cblWeekDays.Items.IndexOf(item)) + ",")).TrimEnd(new[] { ',', ' ' });
                expression = string.Format("{0} {1} * * {2}", dateTime.Minute, dateTime.Hour.ToString(),
                                           daysOfWeek);
                break;
            case "Monthly":
                var monthesOfYear = cblMonths.Items.Cast<ListItem>()
                                              .Where(item => item.Selected)
                                              .Aggregate("", (current, item) => current + (item.Value + ",")).TrimEnd(new[] { ',', ' ' });
                if (string.IsNullOrEmpty(monthesOfYear))
                    monthesOfYear = "0";
                string daysOfWeekString = "*";
                string specificDay = "*";
                if (SpecificDayRadioButton.Checked)
                {
                    specificDay = daySelection.SelectedValue;
                }
                else if (advancedDaySelection.Checked)
                {
                    specificDay = weekIntervals[weekNumberSelection.SelectedIndex];
                    daysOfWeekString = dayOfWeekSelection.SelectedIndex.ToString();
                }
                expression = string.Format("{0} {1} {2} {3} {4}", dateTime.Minute, dateTime.Hour.ToString(), specificDay, monthesOfYear, daysOfWeekString);
                break;
        }
        return expression;
    }

    public List<ISchedule> GetSchedule()
    {
        List<ISchedule> schedulesList = new List<ISchedule>();
        var timesList = new List<DateTime>();
        var frequencyIdsList = new List<int>();
        if (!string.IsNullOrEmpty(frequenciesIdsJson.Value))
            frequencyIdsList = (List<int>)OrionSerializationHelper.FromJSON(frequenciesIdsJson.Value, typeof(List<int>));
        if (!string.IsNullOrEmpty(dateTimeJson.Value))
        {
            var dateTimeList = (Dictionary<int, string>)OrionSerializationHelper.FromJSON(dateTimeJson.Value, typeof(Dictionary<int, string>));
            if (dateTimeList != null)
            {
                foreach (KeyValuePair<int, string> time in dateTimeList)
                {
                    timesList.Add(DateTime.Parse(time.Value));
                }
            }
        }
        else
        {
            timesList.Add(DateTime.Now);
        }
        var timeIntervalsEndList = new List<DateTime>();
        if (UseTimePeriodMode)
        {
            if (!string.IsNullOrEmpty(intervalEndTimeJson.Value))
            {
                var intervalsEndList = (Dictionary<int, string>)OrionSerializationHelper.FromJSON(intervalEndTimeJson.Value, typeof(Dictionary<int, string>));
                if (timeIntervalsEndList != null)
                {
                    foreach (KeyValuePair<int, string> time in intervalsEndList)
                    {
                        timeIntervalsEndList.Add(DateTime.Parse(time.Value));
                    }
                }
            }
            else
            {
                timeIntervalsEndList.Add(DateTime.Now);
            }
        }
        for(int i = 0; i < timesList.Count; i++)
        {
            var schedule = ScheduleItem;
            schedule.CronExpression = CollectCtrontabExpression(timesList[i]);

            if (selectFrequency.SelectedValue == FrequenciesType.Once.ToString())
            {
                schedule.StartTime = timesList[i];
            }
            else
            {
                if (StartDateDropdown.SelectedIndex == 0)
                {
                    schedule.StartTime = DateTime.UtcNow.AddMinutes(BrowserUtcOffset);
                }
                else
                {
                    schedule.StartTime = specStartDate.Value;
                }
            }
            if (endTimeIsSpecified.Checked && selectFrequency.SelectedValue != FrequenciesType.Once.ToString())
            {
                DateTime endTime = specEndDate.Value;
                if (DateTime.Compare(endTime, DateTime.UtcNow.AddMinutes(BrowserUtcOffset)) >= 0)
                    schedule.EndTime = endTime;
            }
            if (string.IsNullOrEmpty(frequencyName.Text))
            {
                if (selectFrequency.SelectedValue == FrequenciesType.Once.ToString())
                    schedule.DisplayName = Resources.CoreWebContent.WEBDATA_AB0_72;
                else
                    schedule.DisplayName = CronHelper.GetFrequencyName(schedule, false);
            }
            else
            {
                schedule.DisplayName = frequencyName.Text;
            }
            if (UseTimePeriodMode)
            {
                (schedule as TimePeriodSchedule).EnabledDuringTimePeriod = impactFrequency.SelectedValue == bool.TrueString;
                TimeSpan time = timeIntervalsEndList[i] - timesList[i];
                if (time.Ticks < 0)
                {
                    time = new TimeSpan(time.Ticks + TimeSpan.FromHours(24).Ticks);
                }
                (schedule as TimePeriodSchedule).Duration = time;
            }
            if (frequencyIdsList.Count > i)
                schedule.FrequencyId = frequencyIdsList[i];
            schedulesList.Add(schedule);
        }
        return schedulesList;
    }


    public void SetSchedule()
    {
        if (cblWeekDays.Items.Count == 0)
            InternalWeekDaysListInit();
        if (cblMonths.Items.Count == 0)
            InternalMonthsListInit();
        if (daySelection.Items.Count == 0)
            InternalMonthDaysListInit();

        frequencyName.Text = Schedules[0].DisplayName;
        // 'null' - Once
        if (string.IsNullOrEmpty(Schedules[0].CronExpression))
        {
            selectFrequency.SelectedValue = FrequenciesType.Once.ToString();
        }
        else
        {
            if (UseTimePeriodMode)
            {
                impactFrequency.SelectedValue = (Schedules[0] as TimePeriodSchedule).EnabledDuringTimePeriod.ToString();
            }
            var expArray = Schedules[0].CronExpression.Split(' ');

            // 'X X */X * *' or 'X X * * 1-5' - Daily
            if ((expArray[2].Contains("*/") || expArray[4] == CommonConstants.WorkingDayOfWeekInterval) && expArray[3] == "*")
            {
                selectFrequency.SelectedValue = FrequenciesType.Daily.ToString();
                if (expArray[2].Contains("*/"))
                {
                    dayOfMonth.Checked = true;
                    daySelectionDaily.Items[Convert.ToInt32(expArray[2].Split('/')[1]) - 1].Selected = true;
                }
                else
                {
                    daySelectionDaily.Items[0].Selected = true;
                    workDayOfWeek.Checked = expArray[4] == CommonConstants.WorkingDayOfWeekInterval;
                }
            }
            // 'X X * * X' - Weekly
            else if (expArray[4] != "*" && expArray[3] == "*")
            {
                selectFrequency.SelectedValue = FrequenciesType.Weekly.ToString();
                cblWeekDays.ClearSelection();
                foreach (var day in expArray[4].Split(','))
                {
                    if (!string.IsNullOrEmpty(day))
                        cblWeekDays.Items[Convert.ToInt32(day)].Selected = true;
                }
            }
            // 'X X X-X X X' or 'X X X X *' - Monthly
            else if (expArray[3] != "*")
            {
                selectFrequency.SelectedValue = FrequenciesType.Monthly.ToString();
                cblMonths.ClearSelection();
                foreach (var month in expArray[3].Split(','))
                {
                    if (!string.IsNullOrEmpty(month) && month != "0")
                        cblMonths.Items[Convert.ToInt32(month) - 1].Selected = true;
                }
                if (!expArray[2].Contains("-") && expArray[2] != "*")
                {
                    SpecificDayRadioButton.Checked = true;
                    daySelection.Items[Convert.ToInt32(expArray[2]) - 1].Selected = true;
                }
                else
                {
                    advancedDaySelection.Checked = true;
                    dayOfWeekSelection.Items[Convert.ToInt32(expArray[4])].Selected = true;
                    weekNumberSelection.SelectedIndex = weekIntervals.IndexOf(expArray[2]);
                }
            }
           
            StartDateDropdown.SelectedIndex = 1;
            
            if (Schedules[0].StartTime == DateTime.MinValue)
            {
                specStartDate.Value = DateTime.UtcNow.AddMinutes(BrowserUtcOffset);
            }
            else
            {
                specStartDate.Value = Schedules[0].StartTime.AddMinutes(BrowserUtcOffset);
            }

            if (Schedules[0].EndTime == null)
            {
                endTimeIsSpecified.Checked = false;
            }
            else
            {
                endTimeIsSpecified.Checked = true;
                specEndDate.Value = Schedules[0].EndTime.Value.AddMinutes(BrowserUtcOffset);
            }
        }
        BindData();
    }

    public void BindData()
    {
        List<int> frequencyIds = new List<int>();
        Dictionary<int, string> scheduleTimesList = new Dictionary<int, string>();
        for (int i = 0; i < Schedules.Count; i++)
        {
            var reportSchedule = Schedules[i];
            frequencyIds.Add(reportSchedule.FrequencyId);

            DateTime startTime = CronHelper.GetTimeFromCronEx(reportSchedule.CronExpression,
                reportSchedule.CronExpression == null
                    ? reportSchedule.StartTime
                    : TimeZoneInfo.ConvertTime(DateTime.UtcNow, reportSchedule.CronExpressionTimeZoneInfo));

            if (reportSchedule.FrequencyId > 0)
            {
                if (reportSchedule.CronExpression != null)//ronExpression == null anly for 'One' schedule. Convert to UTC no needed as start time always in UTC.
                startTime = TimeZoneInfo.ConvertTimeToUtc(startTime, reportSchedule.CronExpressionTimeZoneInfo); // convert to UTC
                startTime = startTime.AddMinutes(BrowserUtcOffset); // add browser local UTC offset to have time in browser's local time
            }
            else if(reportSchedule.CronExpression == null)
            {
               startTime = startTime.AddMinutes(BrowserUtcOffset);
            }
            else if (Math.Abs(reportSchedule.CronExpressionTimeZoneInfo.BaseUtcOffset.TotalMinutes - BrowserUtcOffset) > 0)
            {
                startTime = TimeZoneInfo.ConvertTimeToUtc(startTime, reportSchedule.CronExpressionTimeZoneInfo);
                startTime = startTime.AddMinutes(BrowserUtcOffset);
            }
            scheduleTimesList.Add(i, startTime.ToString());
        }
        frequenciesIdsJson.Value = OrionSerializationHelper.ToJSON(frequencyIds, typeof(List<int>));
        dateTimeJson.Value = OrionSerializationHelper.ToJSON(scheduleTimesList, typeof(Dictionary<int, string>));
        if (UseTimePeriodMode)
        {
            Dictionary<int, string> scheduleIntervalEndTimesList = new Dictionary<int, string>();
            for (int i = 0; i < Schedules.Count; i++)
            {
                var schedule = Schedules[i] as TimePeriodSchedule;
                scheduleIntervalEndTimesList.Add(i, DateTime.Parse(scheduleTimesList[i]).Add(schedule.Duration).ToString());
            }
            intervalEndTimeJson.Value = OrionSerializationHelper.ToJSON(scheduleIntervalEndTimesList, typeof(Dictionary<int, string>));
        }
    }

    protected void AddTime_Click(object sender, EventArgs e)
    {
        schedules.Add(ScheduleItem);
        BindData();
    }
}
