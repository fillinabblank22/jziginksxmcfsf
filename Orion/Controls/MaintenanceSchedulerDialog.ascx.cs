﻿using System;

/// <summary>
/// Summary description for Mute alerts dialog
/// </summary>
public partial class Orion_Controls_MaintenanceSchedulerDialog : System.Web.UI.UserControl
{
    protected bool IsDemoServer
    {
        get
        {
            return SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}