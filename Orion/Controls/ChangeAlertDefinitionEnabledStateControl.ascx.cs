﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Controls_ChangeAlertDefinitionEnabledStateControl_ascx : System.Web.UI.UserControl
{
    protected bool IsDemoServer
    {
        get
        {
            return SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}