﻿using System;
using System.Globalization;
using System.Web.UI;
using SolarWinds.Orion.Web.UI.StatusIcons;

public partial class Orion_Controls_EntityStatusIcon : UserControl
{
    private StatusIconSize iconSize = StatusIconSize.Small;
	private Object entityId = 0;
    private bool wrapInBox = true;
    private int? serverId;

    public String Entity { get; set; }

    public Int32 Status { get; set; }

    public StatusIconSize IconSize
    {
        get { return iconSize; }
        set { iconSize = value; }
    }

	public Object EntityId
	{
		get { return entityId; }
		set
		{
			Int32 id;
			Int32.TryParse((value ?? 0).ToString(), out id);

			entityId = id;
		}
	}

    public bool WrapInBox
    {
        get { return wrapInBox; }
        set { wrapInBox = value; }
    }

    public int? ServerId
    {
        get { return serverId; }
        set { serverId = value; }
    }

    public string GetServerParam()
    {
        return serverId.HasValue ?
            String.Format(CultureInfo.InvariantCulture, "&server={0}", serverId.Value) : String.Empty;
    }
}