<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LicenseLimitControl.ascx.cs" Inherits="Orion_Controls_LicenseLimitControl" %>
<orion:Include runat="server" File="LicenseLimitNotification.css" />

<asp:Panel ID="pnlContainer" runat="server" class="sw-license-limit-hint">
        
    <div class ="sw-license-limit-title-bar">
        <img src="/orion/images/NotificationImages/Approaching_license_limit_icon24x24.png" alt="!">
        <span><%= DefaultSanitizer.SanitizeHtml(Title) %></span>
        <a href="javascript:SW.Core.LicenseLimitNotification.Hide();" role="button" ><span class="sw-license-limit-close-icon">close</span></a>
    </div>
    

    <table class="sw-license-limit-body">
        <tr>
            <td class="sw-license-limit-hint-block">
                <div>
                    <table cellspacing="0" cellpadding="0" class="sw-license-limit-table">
                        <%if(LicenseLimitInfoList!=null)
                            foreach (var elementLicenseSaturationInfo in LicenseLimitInfoList)
                           {%>
                            <tr class="sw-license-limit-remaining-items-row"  data-sw-license-limit-remaining-count="<%= RemainingItemsCount %>">
                                <td class="sw-license-limit-first-column"><%= DefaultSanitizer.SanitizeHtml(elementLicenseSaturationInfo.Key) %></td>
                                <td><%= DefaultSanitizer.SanitizeHtml(String.Format(elementLicenseSaturationInfo.Value, "<span class=\"sw-license-limit-highlighted\">", "</span><span class=\"sw-license-limit-second-element-align\">")+"</span>") %></td>
                            </tr>                  
                            <%} %>
                    </table>
                </div>
            </td>
            <% if (!AreTwoColumns)
               { %>
            </tr><tr> <% } %>
            <td class="sw-license-limit-hint-block">
                 <div>
                    <p class="sw-license-limit-block-title"><%= DefaultSanitizer.SanitizeHtml(HintColumnTitle) %></p>
                     <%if(HintColumnRows!=null)
                            foreach (var row in HintColumnRows)
                           {%>
                    <p class="sw-license-limit-hint-block-row"><%= DefaultSanitizer.SanitizeHtml((String.Format("<a class=\"sw-link sw-license-limit-hint-link\" href=\"{0}\">&#0187;&nbsp;", row.Value)+ String.Format(row.Key,"</a>"))) %></p>     
                         <% } %>               
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>

<script type="text/javascript">
//<![CDATA[
    (function (Core) {
        var LicenseLimitNotification = Core.LicenseLimitNotification = Core.LicenseLimitNotification || {};
        
        LicenseLimitNotification.Hide = function () { $(".sw-license-limit-hint").hide(); };
        LicenseLimitNotification.Show = function () { $(".sw-license-limit-hint").show(); };
       
        LicenseLimitNotification.UpdateLicenseLimitNotification = function (result) {
            var remainingCount = parseInt($(".sw-license-limit-remaining-items-row").attr('data-sw-license-limit-remaining-count'));
            if (result < remainingCount + 1) {
                LicenseLimitNotification.Hide();
            } else {
                LicenseLimitNotification.Show();
            }
            $(".sw-license-limit-highlighted").text(result);
        };
    })(SW.Core);
//]]>
</script>
