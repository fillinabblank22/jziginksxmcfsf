using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using System.Text;

public partial class PeriodControl : System.Web.UI.UserControl
{
    private ListControl _sampleSizeList;
    public void AttachSampleSizeControl(ListControl SampleSizeControl)
    {
        _sampleSizeList = SampleSizeControl;
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool InLine { get; set; }

    private string GetInitializeScript()
    {
        if (ViewState["PeriodInitialScript"] == null)
        {
            StringBuilder initializescript = new StringBuilder();
            string ids = string.Empty;
            string values = string.Empty;
            GetTimePeriods(ref ids, ref values);
            initializescript.Append(ids);
            initializescript.Append(values);
            initializescript.Append(GetTimeRelations());
            initializescript.Append("$(function(){");
            initializescript.AppendFormat("window.timePeriodControl = document.getElementById('{0}');", PeriodList.ClientID);
            initializescript.AppendFormat("window.sampleTimePeriodControl = document.getElementById('{0}');", _sampleSizeList.ClientID);
            initializescript.Append("$(document).ready(function() { if(typeof(CheckTimePeriods) != 'undefined') $(CheckTimePeriods); }); });");
            ViewState["PeriodInitialScript"] = initializescript.ToString();
        }

        return ViewState["PeriodInitialScript"].ToString();
    }

    private void GetTimePeriods(ref string IDs, ref string Values)
    {
        StringBuilder _ids = new StringBuilder("var sampleTimeIDS = [");
        StringBuilder _values = new StringBuilder("var sampleTimeValue = [");
        for (int i = 0; i < _sampleSizeList.Items.Count; i++)
        {
            _ids.Append(string.Format("{0}'{1}'", (i == 0) ? "" : ",", _sampleSizeList.Items[i].Value));
            _values.Append(string.Format("{0}'{1}'", (i == 0) ? "" : ",", _sampleSizeList.Items[i].Text));
        }

        _ids.Append("];");
        _values.Append("];");
        IDs = _ids.ToString();
        Values = _values.ToString();
    }

    private string GetTimeRelations()
    {
        StringBuilder result = new StringBuilder("var sampleTimeRelations = [");
        for (int i = 0; i < PeriodList.Items.Count; i++)
        {
            StringBuilder samplesizeassigned = new StringBuilder("[");
            string TimePeriod = PeriodList.Items[i].Value; // SolarWinds.Orion.NPM.Web.TimePeriod.GetPreset(Convert.ToInt32(PeriodList.Items[i].Value)).KeyText.ToUpper();
            string separator = string.Empty;
            foreach (ListItem sli in _sampleSizeList.Items)
            {
                if (CheckTimePeriodValue(TimePeriod, sli.Value.ToUpper()))
                {
                    samplesizeassigned.Append(string.Format("{0}'{1}'", separator, sli.Value));
                    separator = ",";
                }

            }

            samplesizeassigned.Append("]");
            result.Append(string.Format("{0}{1}", ((i == 0) ? "" : ","), samplesizeassigned.ToString()));
        }

        result.Append("];");
        return result.ToString();
    }



    private bool CheckTimePeriodValue(string TimePeriod, string SampleSize)
    {
        DateTime _startTime = new DateTime();
        DateTime _endTime = new DateTime();
        Periods.Parse(ref TimePeriod, ref _startTime, ref _endTime);
        double numberOfSamples = _endTime.Subtract(_startTime).TotalMinutes / Periods.ConvertIntervalToMinutes(SampleSize);
        return ((numberOfSamples <= 3000) && (numberOfSamples >= 2));
    }

	private void OnInitPeriodList()
	{
		List<string> list = new List<string>();
		
		list = Periods.GenerateSelectionList();

        for(int i = 0; i < list.Count; i++)
        {
            PeriodList.Items.Add(new ListItem(Periods.GetLocalizedPeriod(list[i]), list[i])); 
        }

	}

	private void SetupControlFromRequest()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);

		if (!string.IsNullOrEmpty(resource.Properties["Period"]))
		{
			PeriodName = resource.Properties["Period"];
		}
		else 
		{
			PeriodName = "Today";
		}
	}

	protected override void OnInit(EventArgs e)
	{
		OnInitPeriodList();

		if (!Page.IsPostBack)
		{
			SetupControlFromRequest();
		}

		//for custom object resource 
		if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
		{
			PeriodList.Attributes.Add("onchange", "javascript:SaveData('Period', this.value);");
		}

		base.OnInit(e);
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        this.PreRender += new EventHandler(PeriodControl_PreRender);
    }

    protected void PeriodControl_PreRender(object sender, EventArgs e)
    {
        if (_sampleSizeList != null)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "TimePeriodInitialScript", GetInitializeScript(), true);
            PeriodList.Attributes.Add("onChange", "CheckTimePeriods()");
            SolarWinds.Orion.Web.OrionInclude.CoreFile("TimePeriodScript.js");
        }
    }

    protected void ValidateNumberOfPoints(object source, ServerValidateEventArgs args)
    {
        if (_sampleSizeList != null)
        {
            args.IsValid = CheckTimePeriodValue(PeriodList.SelectedValue, _sampleSizeList.SelectedValue);
        }
    }

	public string PeriodName
	{
	    get {return PeriodList.SelectedItem.Value.ToString(); }
		set { PeriodList.SelectedValue = value; }
	}

	public string PeriodLabel
	{
		get { return this.periodLabel.Text; }
		set { this.periodLabel.Text = value; }
	}
}
