﻿using SolarWinds.Orion.Web;

public partial class SwisfErrorControl : BaseSwisfErrorControl
{
    protected string ErrorDescriptHtml { get; private set; }

    protected override void UpdateErrorText()
    {
        base.UpdateErrorText();
        ErrorDescriptHtml = GetErrorMessage(SwisErrors);
    }
}