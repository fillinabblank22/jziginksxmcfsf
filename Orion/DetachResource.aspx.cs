using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using System.Dynamic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_DetachResource : System.Web.UI.Page, IResourceContainer
{
    private static readonly Log log = new Log();
    
    private NetObject _netObject;
    private ResourceInfo _resource;
    private bool _showResourceOnly;

    public NetObject NetObject
    {
        get { return _netObject; }
    }

    public ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnPreInit(EventArgs e)
    {
        var resourceOnlyParam = Request.QueryString["ResourceOnly"] ?? "0";
        _showResourceOnly = resourceOnlyParam.Equals("1", StringComparison.OrdinalIgnoreCase);

        var netObject = WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["NetObject"]).Trim();

        if (!string.IsNullOrEmpty(netObject))
        {
            try
            {
                _netObject = NetObjectFactory.Create(netObject);
            }
            catch (AccountLimitationException)
            {
                this.Page.Server.Transfer(
                    string.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", netObject)
                    );
            }
        }

        if (string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
            throw new InvalidOperationException("ResourceID missing from Query String.");

        _resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));
        this.Context.Items[typeof(ViewInfo).Name] = this.Resource.View;

        base.OnPreInit(e);
    }
    
    private string GetTitle()
    {
        if (null == this.NetObject)
        {
            return Macros.ParseMacros(this.Resource.Title, string.Empty);
        }
        else
        {
            if (this.NetObject is SolarWinds.Orion.NPM.Web.Interface)
            {
                return string.Format("{0} - {1}", ((SolarWinds.Orion.NPM.Web.Interface)this.NetObject).Caption, Macros.ParseMacros(this.Resource.Title, this.NetObject.NetObjectID));
            }
            else
            {
                return string.Format("{0} - {1}", this.NetObject.Name, Macros.ParseMacros(this.Resource.Title, this.NetObject.NetObjectID));
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        this.Title = HttpUtility.HtmlEncode(GetTitle());

        this.divContainer.Style[HtmlTextWriterStyle.Width] = string.Format("{0}px", Resource.Width);
        this.divContainer.Attributes["sw-view-info"] = this.SerializedViewInfo;

        if (!_showResourceOnly)
            this.divContainer.Style[HtmlTextWriterStyle.MarginLeft] = "12px";

        if (_showResourceOnly)
        {
            TopRightPageLinks.Visible = false;
            ResourceTitle.Visible = false;
        }

		if (Resource.IsAspNetControl)
		{
			BaseResourceControl ctrl = (BaseResourceControl)LoadControl(Resource.File);
			ctrl.Resource = Resource;
			ResourceHostControl host = ResourceHostManager.GetSupportingControl(ctrl, Resource.View.ViewType);

			this.divContainer.Controls.Add(host);
			host.LoadFromRequest();

			host.Controls.Add(ctrl);
		}
		else
		{
			this.divContainer.Controls.Add(new Label { Text = string.Format("{0} is an unsupported legacy resource.", Resource.File) });
		}

        OrionInclude.CoreThemeStylesheets();

        var mgr = ScriptManager.GetCurrent(Page);
        if (mgr != null)
        {
            mgr.Load += ScriptManagerOnLoad;
        }

        base.OnInit(e);
    }

    private void ScriptManagerOnLoad(object sender, EventArgs e)
    {
        try
        {
            if (this.Resource.View != null && this.Resource.View.LimitationID > 0)
            {
                var mgr = ScriptManager.GetCurrent(Page);
                mgr.Controls.Add(new LiteralControl(string.Format("<script type=\"text/javascript\">SW.Core.View.LimitationID = {0};</script>", this.Resource.View.LimitationID)));
            }
        }
        catch (Exception ex)
        {
            log.Error("Unable to add literal to ScriptManager, in order to set SW.Core.View.LimitationID", ex);
        }
    }

    public string SerializedViewInfo
    {
        get
        {
            // swViewInfo properties for apollo based resources
            dynamic swViewInfo = new ExpandoObject();

            if (NetObject != null)
            {
                swViewInfo.netObject = NetObject.NetObjectID;

                var dynamicInfoDict = new Dictionary<string, string>
                {
                    {"uri", NetObject.Uri}
                };

                var nodeParent = NetObject.GetParentOfType<Node>();
                if (nodeParent != null)
                {
                    dynamicInfoDict.Add("nodeUri", nodeParent.Uri);
                }

                swViewInfo.dynamicInfo = dynamicInfoDict;
            }

            return JsonConvert.SerializeObject(swViewInfo,
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
        }
    }
}
