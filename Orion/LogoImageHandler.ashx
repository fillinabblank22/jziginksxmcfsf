﻿<%@ WebHandler Language="C#" Class="LogoImageHandler" %>

using System;
using System.Web;
using SolarWinds.Logging;

using System.Net;
using SolarWinds.Orion.Web.DAL;

public class LogoImageHandler : IHttpHandler
{
    private static Log _log = new Log();

    public bool IsReusable
    {
        get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
        var queryparts = HttpUtility.ParseQueryString(context.Request.Url.Query);
        try
        {
            string base64;
            switch (queryparts["id"])
            {
                case "SitelogoImage":
                    base64 = WebSettingsDAL.NewSiteLogo;
                    break;
                case "SiteNoclogoImage":
                    base64 = WebSettingsDAL.NewNOCSiteLogo;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(queryparts["id"]);
            }

            var data = Convert.FromBase64String(base64);

            if (data == null || data.Length == 0) //empty file. to avoid red X on IE - loading empty logo
            {
                if (System.IO.File.Exists(HttpContext.Current.Server.MapPath("//NetPerfMon//images//NoLogo.gif")))
                    data =  System.IO.File.ReadAllBytes(
                            HttpContext.Current.Server.MapPath("//NetPerfMon//images//NoLogo.gif"));
            }

            string mimetype;

            if (data.Length >= 2 &&
                        data[0] == 0xff && data[1] == 0xd8)
                mimetype = "image/jpeg";
            else if (data.Length >= 3 &&
                data[0] == 0x47 && data[1] == 0x49 && data[2] == 0x46)
                mimetype = "image/gif";
            else if (data.Length >= 8 &&
                data[0] == 0x89 && data[1] == 0x50 && data[2] == 0x4e && data[3] == 0x47 &&
                data[4] == 0x0d && data[5] == 0x0a && data[6] == 0x1a && data[7] == 0x0a)
                // PNG Specification: The first eight bytes of a PNG file always contain the following (decimal) values: 137 80 78 71 13 10 26 10
                mimetype = "image/png";
            else
                mimetype = "image/jpeg"; // browsers can figure it out if it's wrong, they have for years - PC.

            context.Response.OutputStream.Write(data, 0, data.Length);
            context.Response.ContentType = mimetype;
            context.Response.Cache.SetCacheability(HttpCacheability.Private);
            context.Response.StatusDescription = "OK";
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            return;
        }
        catch (Exception ex)
        {
            _log.Error("Unexpected error trying to provide logo image for the page.", ex);
        }

        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.StatusDescription = "NO IMAGE";
        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
    }
}