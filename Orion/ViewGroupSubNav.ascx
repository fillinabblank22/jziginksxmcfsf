<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewGroupSubNav.ascx.cs" Inherits="ViewGroupSubNav" %>
<%@ Register Src="~/Orion/ViewGroupSubNavTab.ascx" TagPrefix="orion" TagName="ViewGroupSubNavTab" %>

 <orion:Include ID="Include1" runat="server" File="SubViews.css" />
 <asp:PlaceHolder ID="ViewGroupSubNavHolder" runat="server" Visible="false">
    <div id="subNavContainer" class="subNavContainer" runat="server" sw-dashboard-functions>
       
        <table id="tblSubNavTabLayout" sw-dashboard-functions>
        <tbody style="width: 100%;">
                <tr><td><a id="btnCollapse" class="btnCollapse" href="#"></a></td></tr>
                <asp:Repeater runat="server" ID="rptTabs">
                <ItemTemplate>
                    <tr class="ViewGroupSubNavTabRow <%# DefaultSanitizer.SanitizeHtml(GetTabRowStyleClass((ViewInfo)Container.DataItem)) %>"><td>
                        <orion:ViewGroupSubNavTab runat="server" id="LeftSubNav" View='<%# DefaultSanitizer.SanitizeHtml((ViewInfo)Container.DataItem) %>' 
                            ParentView="<%# DefaultSanitizer.SanitizeHtml(View) %>" IsEdit='<%# this.IsEdit %>' IsActive='<%# this.IsActive %>' IsSelected='<%# ((ViewInfo)Container.DataItem).ViewID == View.ViewID %>' ></orion:ViewGroupSubNavTab>   
                    </td></tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
        </table>
    </div>
    
    <%--Edit Mode controls--%>
    <div id="dialogConfimDeleteTab" style="display:none;">
	    <b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_VB1_64, "<span class=\"jsTabName\"></span>")) %></b>
        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_63) %></p>
       <div class="sw-btn-bar" >
            <orion:LocalizableButtonLink ID="btnDialogOk" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_62 %>"  ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_62 %>"/>
            <orion:LocalizableButtonLink ID="btnDialogCancel" runat="server" DisplayType="Primary" LocalizedText="Cancel" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_28 %>"/>
        </div>
    </div>

    <script type="text/javascript">
        function OpenDialogConfirmDeleteTab(tabName, successCallback) {
            $('#<%=btnDialogOk.ClientID %>').unbind('click').click(successCallback);
            $('#<%=btnDialogCancel.ClientID %>').unbind('click').click(function () { $('#dialogConfimDeleteTab').dialog("close"); });
            $('.jsTabName').html(tabName);
            $('#dialogConfimDeleteTab').dialog({
                title: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB1_58) %>",
                modal: true, 
                width: 400
                        });
        }
    </script>
    <%--End Edit Mode controls--%>

    <asp:PlaceHolder ID="pnlScript" runat="server" >
        <script type="text/javascript">
            $(document).ready(function () {
                
                //FB141769 - IE7 zindex fix(keeps edge of tabs from showing up on the hover menu)    
                var zIndexNumber = 1000;
                $("html.sw-is-msie-7 .subNavContainer div").each(function () {
                    $(this).css('zIndex', zIndexNumber);
                    zIndexNumber -= 10;
                });
                

                $(".btnCollapse").click(function () {
                    $('.subNavContainer').toggleClass("collapsed");

                    var isExpanded = true;
                    if ($('.subNavContainer').hasClass("collapsed"))
                        isExpanded = false;
                    SW.Core.Cookie.Set('SWSettings', '{subNavExpanded:' + isExpanded + '}', 'days', 5);
                    return false;
                });


                //FB162506 Turning off hovers for now.
//                $(".SubNav-TabHover").hover(
//                    function () {
//                        $('.tabFlyout', $(this)).show();
//                    },
//                    function () {
//                        $('.tabFlyout', $(this)).hide();

//                    });
            });
        </script>
    </asp:PlaceHolder>
</asp:PlaceHolder>