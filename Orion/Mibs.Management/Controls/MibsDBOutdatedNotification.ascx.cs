﻿using SolarWinds.Orion.Mibs.Management.Common.Settings;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using SolarWinds.Orion.Mibs.Management.Common.MibVersioning.Models;

public partial class Management_Controls_MibsDBOutdatedNotification : System.Web.UI.UserControl
{
    /// <summary>
    /// Describes status of 'MIBs DB outdated' notification
    /// </summary>
    public enum NotificationStatus
    {
        Unknown,
        /// <summary>
        /// Determining available MIBs version is disabled, notification will be hidden
        /// </summary>
        Disabled,
        /// <summary>
        /// Failed to determine available MIBs version, warning shown
        /// </summary>
        FailedToDetermine,
        /// <summary>
        /// Local MIBs database is outdated, update available, info shown
        /// </summary>
        Outdated,
        /// <summary>
        /// Local MIBs database is up-to-date, notification hidden
        /// </summary>
        UpToDate
    }

    private readonly IMibsManagementSettings settings = new MibsManagementSettings();

    private NotificationStatus notificationStatus = NotificationStatus.Unknown;

    public NotificationStatus Status
    {
        get { return notificationStatus; }
    }

    IDictionary<NotificationStatus, Orion_Controls_MessageBox.MessageBoxType> messageTypes = new Dictionary<NotificationStatus, Orion_Controls_MessageBox.MessageBoxType>
    {
        { NotificationStatus.FailedToDetermine, Orion_Controls_MessageBox.MessageBoxType.Warn },
        { NotificationStatus.Outdated, Orion_Controls_MessageBox.MessageBoxType.Info },
    };

    IDictionary<NotificationStatus, string> messages = new Dictionary<NotificationStatus, string>
    {
        { NotificationStatus.FailedToDetermine, Resources.MibsManagementWebContent.OutdatedNotification_FailedToDetermine_Message },
        { NotificationStatus.Outdated, Resources.MibsManagementWebContent.OutdatedNotification_Outdated_Message },
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        notificationStatus = GetNotificationStatus();
        if (
            notificationStatus == NotificationStatus.Unknown ||
            notificationStatus == NotificationStatus.Disabled ||
            notificationStatus == NotificationStatus.UpToDate)
        {
            Visible = false;
        }
        else
        {
            Orion_Controls_MessageBox.MessageBoxType messageBoxType;
            NotificationMessage.Type = messageTypes.TryGetValue(notificationStatus, out messageBoxType) ? messageBoxType : Orion_Controls_MessageBox.MessageBoxType.Hint;

            string messageHtml;
            messages.TryGetValue(notificationStatus, out messageHtml);
            messageHtml += " <a href='/apps/mibs-management/mibs-management'>" + Resources.MibsManagementWebContent.OutdatedNotification_LinkText + "</a>";
            NotificationMessage.MessageHtml = messageHtml;
        }
    }

    private NotificationStatus GetNotificationStatus()
    {
        if (!settings.ShowMibsDatabaseNewVersionNotification)
        {
            return NotificationStatus.Disabled;
        }

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return NotificationStatus.Outdated;
        }

        using (var service = InformationServiceProxy.CreateV3())
        {
            var databaseState = service.Invoke<DatabaseState>("Orion.Mibs.Management", "GetDatabaseState");
            if (databaseState == null || !databaseState.AreListedEnginesUpToDate.HasValue)
            {
                return NotificationStatus.FailedToDetermine;
            }

            return databaseState.AreListedEnginesUpToDate.Value
                ? NotificationStatus.UpToDate
                : NotificationStatus.Outdated;
        }
    }
}