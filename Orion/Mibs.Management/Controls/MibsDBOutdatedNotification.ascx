﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MibsDBOutdatedNotification.ascx.cs" Inherits="Management_Controls_MibsDBOutdatedNotification" %>
<%@ Register TagPrefix="orion" TagName="MessageBox" Src="~/Orion/Controls/MessageBox.ascx" %>

<orion:Include ID="Include" runat="server" Module="Mibs.Management" File="MibsDBOutdatedNotification.css" Section="Top" />

<div class="mibs-management-outdated-notification">
    <orion:messagebox runat="server" ID="NotificationMessage" AllowInPostBack="true" />
</div>
