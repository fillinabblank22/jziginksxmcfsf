<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceException.ascx.cs" Inherits="ResourceException" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>

<div class="ResourceWrapper" style="width: <%= this.Width + 2 %>px" ResourceId="<%= GetParentResource().Resource.ID %>">
	<div class="HeaderBar" id="headerBar" runat="server">
	    <orion:HelpButton runat="server" ID="HelpButton" />
	    <asp:PlaceHolder ID="phEditButton" runat="server">
		    <orion:LocalizableButtonLink runat="server" href="<%# DefaultSanitizer.SanitizeHtml(this.EditLink) %>" DisplayType="Resource" CssClass="EditResourceButton" LocalizedText="Edit" />
		</asp:PlaceHolder>

   		<asp:HyperLink runat="server" ID="ManageButton" CssClass="EditResourceButton" />
		
		<asp:PlaceHolder ID="HeaderButtonsPlaceHolder" runat="server"></asp:PlaceHolder>
		
		<h1><a href='<%= DefaultSanitizer.SanitizeHtml((IsPresentInDB())?DetachURL:string.Empty) %>' target="_blank"><%= DefaultSanitizer.SanitizeHtml(Server.HtmlEncode(Title)) %></a></h1>
		<h2><%= DefaultSanitizer.SanitizeHtml(Server.HtmlEncode(SubTitle)) %></h2>
	</div>
    
    <orion:ExceptionWarning Text='<%# DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_336, Resource.Title)) %>' DataSource="<%# DefaultSanitizer.SanitizeHtml(ErrorDetails) %>" runat="server" />

	<asp:PlaceHolder runat="server" ID="ResourcePlaceHolder" />
</div>
