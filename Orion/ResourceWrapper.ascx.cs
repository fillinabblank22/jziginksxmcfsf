using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using SolarWinds.Logging;
using SolarWinds.Newtonsoft.Json;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Platform.Converters;
using SolarWinds.Orion.Core.Common.Caching;


public partial class Orion_ResourceWrapper : BaseResourceWrapper
{
	private bool _showDrDownMenu = false;
    private bool _isNOCView = false;
	private readonly string _customChartPage = "/Orion/NetPerfMon/CustomChart.aspx";
	private readonly string _chartDataPage = "/Orion/NetPerfMon/ChartData.aspx";

	private readonly string[] _customChartPropertyNames = { "ChartName","CustomPollerID","Rows","PlotStyle",
															  "ResourceID","NetObject","NetObjectPrefix","Width" };
	private readonly string[] _chartDataPropertyNames = { "ChartName","CustomPollerID","Rows","NetObject","Period",
															"SampleSize","PeriodBegin","PeriodEnd" };
    private static readonly string[] _ignoredJsonProperties = { "Selector", "Type", "Owner", "FeatureToggle" };

    private static Log log = new Log();

    private const string NetObjectKey = "netobject";
    private const string OpidKey = "opid";
    private const string LimitationIdsKey = "limitations";

    [PersistenceMode(PersistenceMode.InnerProperty)]
	public override PlaceHolder Content => ResourcePlaceHolder;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public override PlaceHolder HeaderButtons => HeaderButtonsPlaceHolder;

    protected string PropertiesAsJson
    {
        get
        {
            var parent = GetParentResource();
            var properties = parent?.Resource?.Properties;
            var obj = new Dictionary<string, string>();

            if (properties != null && properties.Count > 0)
            {
                foreach (string key in properties.Keys)
                {
                    if (_ignoredJsonProperties.Contains(key, StringComparer.OrdinalIgnoreCase)) continue;
                    obj[key] = properties[key];
                }
            }

            string apolloNetObjValue = parent?.GetNetObjectId();
            string opidValue = null;
            string objectModeStr;
            int objectMode;
            string embeddedNetObjValue = null;
            if (obj.TryGetValue("customobjectmode", out objectModeStr) && int.TryParse(objectModeStr, out objectMode) && objectMode == (int) OrionChartingHelper.CustomObjectMode.SingleObjectMode)
            {
                // get NetObject from CustomObjectResource (COR)
                obj.TryGetValue("netobjectid", out embeddedNetObjValue);
            }

            //use NetObject from Apollo chart if available
            //use embedded NetObject if rescource is in COR
            var netObjValue = string.IsNullOrEmpty(apolloNetObjValue) ? string.IsNullOrEmpty(embeddedNetObjValue) ? Request.Params[NetObjectKey] : embeddedNetObjValue : apolloNetObjValue;
            
            if (!string.IsNullOrWhiteSpace(netObjValue) && !obj.ContainsKey(OpidKey))
            {
                try
                {
                    opidValue = new NetObjectOpidConverter()
                        .GetOpid(netObjValue, OrionSitesCache.Instance.GetLocalOrionSiteID())?.ToString();
                }
                catch (Exception ex)
                {
                    log.WarnFormat("Failed to get Opid property for neobject: {0}, Error - {1}", netObjValue, ex);
                }

                if (!string.IsNullOrEmpty(opidValue))
                {
                    obj.Add(OpidKey, opidValue);
                }
                else
                {
                    log.WarnFormat("Opid property could not be provided for resources with neobject: {0}", netObjValue);
                }
            }

            if (!obj.ContainsKey(LimitationIdsKey))
            {
                var ids = Limitation.GetCurrentListOfLimitationIDs();
                if (ids.Count > 0)
                {
                    obj.Add(LimitationIdsKey, string.Join(",", ids));
                }
            }

            return obj.Count != 0 ? JsonConvert.SerializeObject(obj) : "{}";
        }
    }

    public bool HideHeaderPropertyExists
    {
        get
        {
            return GetBooleanProperty("hideheader");
        }
    }

    private bool GetBooleanProperty(string property)
    {
        var parent = GetParentResource();
        var properties = parent?.Resource?.Properties;

        bool propertyValue = false;

        if (properties != null && properties.ContainsKey(property))
        {
            if (!Boolean.TryParse(properties[property], out propertyValue)) {
                log.WarnFormat("Resource property [{0}] can not be parsed to boolean", property);
            }
        }

        return propertyValue;
    }

    /// <summary>
    /// Controls the Visiblity for a given resource/widget.  Note that setting visible to false will just hide the
    /// wrapper through CSS, and omit the content.  This is so that a dashboard in edit mode can still manipulate the resource (reorder, delete, etc.)
    /// </summary>
    public override bool Visible 
    {
        get { return base.Visible; }
        set
        {
            //If someone is requesting us to hide, hide the content instead of the whole resource
            if (!value)
            {
                this.CssClass += " HiddenResourceWrapper";
                ResourcePlaceHolder.Visible = false;
            }
            else
            {
                base.Visible = true;
            }
        }
    }

    protected int ID
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null && parent.Resource != null)
            {
                return parent.Resource.ID;
            }
            else
                return -1;
        }
    }

    protected string Title
	{
		get 
        {
			BaseResourceControl parent = GetParentResource();

			if (parent != null)
                return BaseResourceWrapper.ParseMacros(parent, parent.DisplayTitle).Trim('"');
			else
				return Resources.CoreWebContent.WEBCODE_AK0_54;
        }
	}

    protected string DefaultTitle
    {
        get
        {
            BaseResourceControl parent = GetParentResource();

            if (parent != null)
                return BaseResourceWrapper.ParseMacros(parent, parent.GetDefaultTitle()).Trim('"');
            else
                return Resources.CoreWebContent.WEBCODE_AK0_54;
        }
    }

    protected string SubTitle
    {
        get
        {
			BaseResourceControl parent = GetParentResource();

			if (parent != null)
                return BaseResourceWrapper.ParseMacros(parent, parent.SubTitle).Trim('"');
			else
				return string.Empty;
		}
    }

	protected string HelpUrlFragment
	{
		get 
        {
			BaseResourceControl parent = GetParentResource();
			if (parent != null)
				return parent.HelpLinkFragment;
			else
				return string.Empty;
		}
	}

    protected string DetachURL
    {
        get 
        {
			BaseResourceControl parent = GetParentResource();
			if (parent != null)
				return parent.DetachURL;
			else
				return string.Empty;
		}
    }

    protected int Width
    {
        get
        {
			BaseResourceControl parent = GetParentResource();
			if (parent != null)
				return parent.Resource.Width;
			else
				return 0;
        }
    }

    protected string WidthString => Width == -1 ? "100%" : $"{this.Width + 2}px";

    protected bool IsAngularJsResource
    {
        get
        {
            var parent = GetParentResource();
            if (parent == null)
                return false;
            return parent.CanContainAngularJsResource || parent is IXuiResource;
        }
    }

    protected string EditLink
    {
        get
        {
			BaseResourceControl parent = GetParentResource();
			if (parent != null)
				return parent.EditURL;
			else
				return string.Empty;
        }
    }

    protected string IconPath
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null)
                return parent.IconPath;
            else
                return string.Empty;
        }
    }

	protected bool IsMobileView
	{
		get
		{
			var isMobileView = Request.QueryString["IsMobileView"];
			if (isMobileView != null && isMobileView.ToString().Equals("true", StringComparison.OrdinalIgnoreCase))
				return true;

			return false;
		}
	}

	protected bool IsPresentInDB()
	{
		BaseResourceControl parent = GetParentResource();
		if (parent != null && parent.Resource != null)
		{
			return parent.Resource.ID != 0;
		}
		else
		{
			return false;
		}
	}

	protected BaseResourceControl GetParentResource()
	{
		return GetParentResource(this);
	}

	protected BaseResourceControl GetParentResource(Control control)
	{
		if (control == null)
			return null;
		else if (control is BaseResourceControl && ((BaseResourceControl)control).Resource != null)
			return (BaseResourceControl)control;
		else
			return GetParentResource(control.Parent);
	}
    

    protected override void OnLoad(EventArgs e)
    {
		HelpButton.Visible = !(string.IsNullOrEmpty(HelpUrlFragment));
		this.HelpButton.HelpUrlFragment = this.HelpUrlFragment;

		base.OnLoad(e);
        bool.TryParse(Request.QueryString.Get("isNOCView"), out _isNOCView);
		drDownMenu.Attributes["onchange"] = "window.open(this.options[this.selectedIndex].value);this.selectedIndex=0";
		drDownMenu.Visible = _showDrDownMenu;
		headerBar.Visible = ShowHeaderBar && !HideHeaderPropertyExists;
        phEditButton.Visible = IsPresentInDB() && ShowEditButton && Profile.AllowCustomize &&
                               !string.IsNullOrEmpty(EditLink) && !_isNOCView && !GetBooleanProperty("hideedit");

        if (!string.IsNullOrWhiteSpace(IconPath))
        {
            resourceIcon.ImageUrl = IconPath;
            resourceIcon.Visible = true;
        }

        SetManageButton();
        lbtnEdit.DataBind();
	}

    private void SetManageButton()
    {        
        ManageButton.NavigateUrl = ManageButtonTarget;
        ManageButton.Visible = ShowManageButton && !_isNOCView;

        if (string.IsNullOrEmpty(ManageButtonText) == false)
        {            
            ManageButton.LocalizedText = CommonButtonType.CustomText;
            ManageButton.Text = ManageButtonText;
            ManageButton.Attributes["automation"] = ManageButtonId ?? ManageButtonText;
            return;
        }

        if (string.IsNullOrEmpty(_manageButtonImage))
        {
            ManageButton.Visible = false;
            return;
        }
            

        var file = System.IO.Path.GetFileName(this.MapPath(_manageButtonImage));

        // legacy support
        switch (file.ToLowerInvariant())
        {
            case "button.managenodes.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageNodes;
                return;
            case "button.manage.groups.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageGroups;
                return;
            case "button.manage.dependencies.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
                return;
            case "button.manage.resource.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_Manage;
                return;
            case "resourcethresholds.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_Thresholds;
                return;
            case "button.vmwaresettings.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_VMWareSettings;
                return;
            case "button.manageapps.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageApplications;
                return;
            case "button.appbuilder.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_AppBuilder;
                return;
            case "button_manage_list.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageUDTList;
                return;
            case "button_manage_ports.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManagePorts;
                return;
            case "button.manageoperations.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageOperations;
                return;
            case "button.managetransactions.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageWebTasks;
                return;
            case "button.resource.mngsrcs.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_ManageSources;
                return;
            case "button.credential.gif":
                ManageButton.Text = Resources.CoreWebContent.ResourcesAll_Credentials;
                return;
        }

        // i18n defect.
        ManageButton.ImageUrl = _manageButtonImage;
    }

	private Dictionary<string, string> GetMandatoryProperties(string chartName, ResourceInfo resource, string prefix)
	{
		Dictionary<string, string> parameters = new Dictionary<string, string>();

		parameters["CustomPollerID"] = resource.Properties["CustomPollerID"];
		parameters["PlotStyle"] = resource.Properties["PlotStyle"];
		parameters["NetObjectPrefix"] = prefix;
		parameters["ResourceID"] = resource.ID.ToString();
		parameters["ChartName"] = chartName;
		parameters["Width"] = "640";

		if (!string.IsNullOrEmpty(resource.Properties["Period"]))
		{
			if (resource.Properties["Period"].Contains("~"))
			{
				string[] periods = resource.Properties["Period"].Split('~');
				parameters["Period"] = string.Format("{0}~{1}",
					DateTime.FromBinary(long.Parse(periods[0])).ToString(), DateTime.FromBinary(long.Parse(periods[1])).ToString());
			}
			else
			{
				parameters["Period"] = resource.Properties["Period"];
			}
		}
		else
		{
			parameters["Period"] = Periods.DEFAULT_PERIOD_VALUE;
		}

		if (!string.IsNullOrEmpty(resource.Properties["SampleSize"]))
			parameters["SampleSize"] = resource.Properties["SampleSize"];
		else
			parameters["SampleSize"] = Periods.DEFAULT_SAMPLE_SIZE;

		DateTime periodBegin = new DateTime();
		DateTime periodEnd = new DateTime();
		string period = parameters["Period"];
		Periods.Parse(ref period, ref periodBegin, ref periodEnd);

		parameters["PeriodBegin"] = periodBegin.ToString();
		parameters["PeriodEnd"] = periodEnd.ToString();

		if (prefix == "N")
		{
			if (!string.IsNullOrEmpty(Request.Params[NetObjectKey]))
			{
				NetObject netObject = NetObjectFactory.Create(Request.Params[NetObjectKey]);
				if (netObject != null)
					parameters["NetObject"] = "N:" + netObject["NodeID"].ToString();
			}
		}
		else if (prefix == "S")
			parameters["NetObject"] = string.Empty;
		else
			parameters["NetObject"] = Request.Params[NetObjectKey];

		parameters["Rows"] = resource.Properties[string.Format("{0}:Rows", parameters["NetObject"])];

		return parameters;
	}

	private string ApplyCustomParams(string url, Dictionary<string, string> customParams)
	{
		if (customParams == null || customParams.Count == 0)
			return url;

		StringBuilder sb = new StringBuilder();
		sb.AppendFormat("{0}{1}", url, url.Contains("?") ? "&" : "?");

		List<string> keys = customParams.Keys.ToList();

		sb.AppendFormat("{0}={1}", keys[0], customParams[keys[0]]);

		if (customParams.Count > 1)
			for (int i = 1; i < keys.Count; i++)
				sb.AppendFormat("&{0}={1}", keys[i], customParams[keys[i]]);

		return sb.ToString();
	}

	private string FormatQueryString(Dictionary<string, string> arguments)
    {
        if (arguments.Count == 0)
            return String.Empty;

        StringBuilder sb = new StringBuilder("?");

        List<string> keys = arguments.Keys.ToList();

        sb.AppendFormat("{0}={1}", keys[0], arguments[keys[0]]);

        if (arguments.Count > 1)
            for (int i = 1; i < keys.Count; i++)
                sb.AppendFormat("&{0}={1}", keys[i], arguments[keys[i]]);

        return sb.ToString();
    }

    private string FormatQueryString(Dictionary<string, string> arguments, string[] keyFilter)
    {
        Dictionary<string, string> filteredArguments = new Dictionary<string, string>();

        keyFilter.ToList().ForEach(key =>
            {
                if (arguments.ContainsKey(key))
                    filteredArguments.Add(key, arguments[key]);
            });

        return FormatQueryString(filteredArguments);
    }

    private string AddPeriodAndSampleSize(string url, string period, string sampleSize)
    {
        return String.Format("{0}{1}Period={2}&SampleSize={3}", url, url.Last() != '?' ? "&" : String.Empty, period, sampleSize);
    }

    public void SetUpDrDownMenu(string chartName, ResourceInfo resource, string prefix, Dictionary<string, string> customParams)
	{
		_showDrDownMenu = true;

        Dictionary<string, string> properties = GetMandatoryProperties(chartName, resource, prefix);

		Dictionary<string, string> chartUrlCustomParams = null;
		Dictionary<string, string> chartDataUrlCustomParams = null;
		if (customParams != null)
		{
			chartUrlCustomParams =
				customParams.Where(item => !_customChartPropertyNames.Contains(item.Key)).ToDictionary(item => item.Key, item => item.Value);
			chartDataUrlCustomParams =
			   customParams.Where(item => !_chartDataPropertyNames.Contains(item.Key)).ToDictionary(item => item.Key, item => item.Value);
		}

        string customChartURL = 
			ApplyCustomParams(String.Format("{0}{1}", _customChartPage, FormatQueryString(properties, _customChartPropertyNames)),chartUrlCustomParams);
        string chartDataURL = 
			ApplyCustomParams(String.Format("{0}{1}", _chartDataPage, FormatQueryString(properties, _chartDataPropertyNames)), chartDataUrlCustomParams);
        
		foreach (ListItem item in drDownMenu.Items)
		{
			switch (item.Attributes["id"])
			{
                case "li7Days": 
                    item.Value = AddPeriodAndSampleSize(customChartURL, "Last 7 Days", "1H"); 
                    break;
                case "li30Days": 
                    item.Value = AddPeriodAndSampleSize(customChartURL, "Last 30 Days", "1D"); 
                    break;
                case "liEditChart": 
                    item.Value = AddPeriodAndSampleSize(customChartURL, properties["Period"], properties["SampleSize"]); 
                    break;
                case "liViewChartData": 
                    item.Value = chartDataURL; 
                    break;
                case "liViewExcel":
                    item.Value = String.Format("{0}{1}", chartDataURL, "&DataFormat=Excel"); 
                    break;
			}
		}
	}

	public void SetDrDownMenuParameters(string chartName, ResourceInfo resource)
	{
		SetDrDownMenuParameters(chartName, resource, string.Empty);
	}

    public void SetDrDownMenuParameters(string chartName, ResourceInfo resource, string prefix)
    {
		SetUpDrDownMenu(chartName, resource, string.Empty, null);
    }

	// this method is used by Multiple Chart resources in NPM v10.1
	[Obsolete("This method is obsolete.  Use SetUpDrDownMenu(string chartName, ResourceInfo resource, string prefix, Dictionary<string, string> customParams) method instead.")]
	public void SetDrDownMenuParameters(string chartName, ResourceInfo resource, string prefix, Dictionary<string, object> customData)
	{
		_showDrDownMenu = true;

		string period;
		if (!string.IsNullOrEmpty(resource.Properties["Period"]))
		{
			if (resource.Properties["Period"].Contains("~"))
			{
				string[] periods = resource.Properties["Period"].Split('~');
				period = DateTime.FromBinary(long.Parse(periods[0])).ToString() + "~" +
				DateTime.FromBinary(long.Parse(periods[1])).ToString();
			}
			else
			{
				period = resource.Properties["Period"];
			}
		}
		else
			period = Periods.DEFAULT_PERIOD_VALUE;

		string sampleSize;
		if (!string.IsNullOrEmpty(resource.Properties["SampleSize"]))
			sampleSize = resource.Properties["SampleSize"];
		else
			sampleSize = Periods.DEFAULT_SAMPLE_SIZE;

		DateTime periodBegin = new DateTime();
		DateTime periodEnd = new DateTime();
		Periods.Parse(ref period, ref periodBegin, ref periodEnd);

		string netObjectID = string.Empty;
		if (prefix == "N")
		{
			if (!string.IsNullOrEmpty(Request.Params[NetObjectKey]))
			{
				NetObject netObject = NetObjectFactory.Create(Request.Params[NetObjectKey]);
				if (netObject != null)
					netObjectID = "N:" + netObject["NodeID"].ToString();
			}
		}
		else if (prefix == "S")
			netObjectID = string.Empty;
		else
			netObjectID = Request.Params[NetObjectKey];

		string rowProperty = netObjectID + ":Rows";

		string selectedEntities = resource.Properties["SelectedEntities"];
		if (customData != null)
		{
			if (customData.ContainsKey("SelectedEntities"))
			{
				selectedEntities = (string)customData["SelectedEntities"];
			}
		}
		foreach (ListItem item in drDownMenu.Items)
		{
			switch (item.Text.Trim().ToUpperInvariant())
			{
                case "li7Days":
					item.Value = string.Format(item.Value, chartName, resource.Properties["CustomPollerID"], resource.Properties[rowProperty], resource.Properties["PlotStyle"], resource.ID.ToString(), Request["NetObject"], prefix);
					break;
                case "li30Days":
					item.Value = string.Format(item.Value, chartName, resource.Properties["CustomPollerID"], resource.Properties[rowProperty], resource.Properties["PlotStyle"], resource.ID.ToString(), Request["NetObject"], prefix);
					break;
                case "liEditChart":
					item.Value = string.Format(item.Value, chartName, resource.Properties["CustomPollerID"], resource.Properties[rowProperty], resource.Properties["PlotStyle"], resource.ID.ToString(), Request["NetObject"], period, sampleSize, prefix);
					break;
                case "liViewChartData":
					item.Value = string.Format(item.Value, chartName, resource.Properties["CustomPollerID"], resource.Properties[rowProperty], netObjectID, period, sampleSize, periodBegin.ToString(), periodEnd.ToString(), selectedEntities, resource.Properties["EntityName"], resource.Properties["ShowSum"], resource.Properties["DataColumnName"]);
					break;
                case "liViewExcel":
					item.Value = string.Format(item.Value, chartName, resource.Properties["CustomPollerID"], resource.Properties[rowProperty], netObjectID, period, sampleSize, periodBegin.ToString(), periodEnd.ToString(), selectedEntities, resource.Properties["EntityName"], resource.Properties["ShowSum"], resource.Properties["DataColumnName"]);
					break;
			}
		}
	}

    protected string AutomationTag
    {
        get
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsAutomationTestServer)
            {
                var resource = GetParentResource();
                if (resource != null)
                {
                    return String.Format("automation=\"{0}\"", resource.GetType());
                }
            }

            return String.Empty;
        }
    }
}
