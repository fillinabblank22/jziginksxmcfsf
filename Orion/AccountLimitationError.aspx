<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB1_27 %>" %>
<%@ Import Namespace="SolarWinds.Internationalization.Exceptions" %>

<script runat="server">
    protected string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["SubTitle"]))
                return SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["SubTitle"]);

            return string.Format(Resources.CoreWebContent.WEBCODE_VB1_8, this.NetObjectID);
        }
    }

    protected string NetObjectID
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
                return SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["NetObject"]);

            return string.Empty;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => string.Format(Resources.CoreWebContent.WEBCODE_VB1_8, this.NetObjectID)), Title = Resources.CoreWebContent.WEBDATA_VB1_27 });
    }
</script>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="contentHeader">&nbsp;</div>
    
    <div class="StatusMessage">
        <h1><%= DefaultSanitizer.SanitizeHtml(this.Title) %></h1>
    
        <h2><%= DefaultSanitizer.SanitizeHtml(this.SubTitle) %></h2>
    </div>
    
    
    <%--<div style="margin-left: 72px;">
        <orion:CollapsePanel runat="server" Collapsed="true">
            <TitleTemplate><strong>Additional Information</strong></TitleTemplate>
            <BlockTemplate>
            asdf
            </BlockTemplate>
        </orion:CollapsePanel>
    </div>--%>
</asp:Content>

