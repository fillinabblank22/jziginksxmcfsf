<%@ Page Language="C#" MasterPageFile="~/Orion/Voip/Summary.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_Voip_Summary" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Orion/Voip/Summary.master" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="ResourceContent" runat="server" ContentPlaceHolderID="ResourceContentPlaceHolder">
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>