﻿using System;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;

namespace Orion.Voip
{
    public partial class OrionVoipSipSipTrunkDetails : VoipView, ICcmSipTrunkProvider
    {
        protected static readonly Log Log = new Log();
        protected override void OnInit(EventArgs e)
        {
            try
            {
                resContainer.DataSource = ViewInfo;
                resContainer.DataBind();
                base.OnInit(e);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat($"OrionVoipSipSipTrunkDetails: {ex}");
                throw new Exception("Status is not polled for selected SIP Trunk yet");
            }
        }
        public override string ViewType => "Voip SIPTrunk";

        public override string HelpFragment => "OrionIPSLAManagerAdministratorGuide_AddingCallManagersToVNQM";

        public CcmSipTrunk CcmSipTrunk => new CcmSipTrunk(Request.QueryString["NetObject"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = string.Format(VNQMWebContent.VNQMWEB_MH_SIP_Trunk_Details_Page_Title, ViewInfo.ViewTitle, CcmSipTrunk.Name);
        }
        public override void SelectView()
        {
            if (NetObject == null)
            {
                NetObject = new CcmSipTrunk(Request.QueryString["NetObject"]);
            }
            base.SelectView();
        }
    }
}