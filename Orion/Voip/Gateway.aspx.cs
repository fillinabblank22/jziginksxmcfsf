﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.IpSla.Web;


public partial class Orion_Voip_Gateway : VoipView, IGatewayProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        base.OnInit(e);
    }
    public override string ViewType
    {
        get { return "Voip Gateway"; }
    }

   
    public Gateway Gateway
    {
        get { return (Gateway)this.NetObject; }
    }
    
    public override string HelpFragment
    {
        get { return "OrionIPSLAManagerPHViewGateway"; }
    }
   
}