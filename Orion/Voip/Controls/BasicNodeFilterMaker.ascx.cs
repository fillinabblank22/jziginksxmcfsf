using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.Filters;
using Node = SolarWinds.Orion.NPM.Web.Node;

[ValidationPropertyAttribute("IsFilterValid")]
public partial class Orion_Voip_Controls_BasicNodeFilterMaker : System.Web.UI.UserControl
{
    private const string SOURCE_SITE_COLUMN_NAME = "Source Site Name";
    private const string DEST_SITE_COLUMN_NAME = "Dest Site Name";

    private const string AutoCompleteStyleSheet = "/Orion/Voip/styles/jquery.autocomplete.css";

    private Filter filter = new Filter();
    private FilterType filterType = FilterType.Node;

    public Filter Filter
    {
        set
        {
            filter = value;
            LoadFilter(filter);
        }
        get
        {
            UpdateFilter();
            return filter;
        }
    }

    public bool IsFilterValid
    {
        get
        {
            return ddlField.SelectedIndex > 0;
        }
    }

    public FilterType FilterType
    {
        set
        {
            filterType = value;
        }
        get { return filterType; }
    }

    private void Initialize()
    {
        InitFieldItems();
        FIllDDLComparison();
    }

    void InsertStyleSheet()
    {
        HtmlLink linkTag = new HtmlLink();
        linkTag.Href = AutoCompleteStyleSheet;
        linkTag.Attributes["type"] = "text/css";
        linkTag.Attributes["rel"] = "stylesheet";
        this.Page.Header.Controls.Add(linkTag);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        InsertStyleSheet();

        if (!this.IsPostBack)
        {
            Initialize();
        }
    }

    void InitFieldItems()
    {
        ddlField.Items.Add("(none)");

        switch (filterType)
        {
            case FilterType.Node:
                ddlField.Items.Add(new ListItem("Location", "Location"));
                ddlField.Items.Add(new ListItem("Vendor", "Vendor"));

                IList<string> customProps = Node.GetCustomPropertyNames(true);
                if (customProps.Count != 0)
                {
                    foreach (string property in customProps)
                        ddlField.Items.Add(new ListItem(property, property));
                }
                break;
            case FilterType.CallPath:
                ddlField.Items.Add(new ListItem(SOURCE_SITE_COLUMN_NAME, "SrcName"));
                ddlField.Items.Add(new ListItem(DEST_SITE_COLUMN_NAME, "DstName"));
                break;
        }
    }

    void SetAccessibility()
    {
        bool fieldSelected = ddlField.SelectedIndex > 0;

        if ( ddlComparison.Enabled != fieldSelected )
            ddlComparison.Enabled = fieldSelected;

        if (ddlValue.Enabled != fieldSelected)
            ddlValue.Enabled = fieldSelected;
    }

    private void UpdateFilter()
    {        
        SqlOperators oper = (SqlOperators)Enum.Parse(typeof(SqlOperators), ddlComparison.SelectedValue);

        string fieldName = ddlField.SelectedValue;
        string value = ddlValue.Text;

        switch (filterType)
        {
            case FilterType.Node:
                filter = new Filter(fieldName, value, oper, fieldName, "Nodes");
                break;
            case FilterType.CallPath:
                string name = String.Empty;
                if (ddlField.SelectedItem != null)
                {
                    name = ddlField.SelectedItem.Text;
                }

                string pref = String.Empty;
                if (name.Equals(SOURCE_SITE_COLUMN_NAME))
                    pref = "SourceSite";
                else if (name.Equals(DEST_SITE_COLUMN_NAME))
                    pref = "DestSite";

                filter = new Filter("Name", value, oper, name, pref);
                break;
            default:
                this.filter = new Filter();
                break;
        }
    }

    private void FIllDDLComparison()
    {
        ddlComparison.Items.Clear();

        ddlComparison.Items.Add(new ListItem(FilterUtility.OperFilterToString(SqlOperators.Equal), SqlOperators.Equal.ToString()));
        ddlComparison.Items.Add(new ListItem(FilterUtility.OperFilterToString(SqlOperators.NotEqual), SqlOperators.NotEqual.ToString()));
        ddlComparison.Items.Add(new ListItem(FilterUtility.OperFilterToString(SqlOperators.Like), SqlOperators.Like.ToString()));
        ddlComparison.Items.Add(new ListItem(FilterUtility.OperFilterToString(SqlOperators.NotLike), SqlOperators.NotLike.ToString()));
        ddlComparison.Items.Add(new ListItem(FilterUtility.OperFilterToString(SqlOperators.StartsWith), SqlOperators.StartsWith.ToString()));
        ddlComparison.Items.Add(new ListItem(FilterUtility.OperFilterToString(SqlOperators.EndsWith), SqlOperators.EndsWith.ToString()));
    }


    private void LoadFilter(Filter filter)
    {
        ddlField.SelectedValue = filter.DisplayValue;
        ddlComparison.SelectedValue = filter.OperFilter.ToString();
        ddlValue.Text = filter.ValueFilter;
    }

    public void ClearBoxes()
    {
        ddlField.SelectedIndex = 0;
        ddlComparison.SelectedIndex = 0;
        ddlValue.Text = String.Empty;
    }

}
