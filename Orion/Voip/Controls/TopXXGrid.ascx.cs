﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;
using System.Data;
using SolarWinds.Orion.IpSla.Web.Resources;
using SolarWinds.Orion.IpSla.Web.DisplayTypes;

public partial class Orion_Voip_Controls_TopXXGrid : TopXXControl
{
    private const int DataColumnCount = 7;

    protected bool IsColumnVisible(int index)
    {
        return Metrics.Count > index;
    }

    protected string GetColumnName(int index)
    {
        if (IsColumnVisible(index))
        {
            TopXXMetric metric = Metrics[index];
            switch(metric)
            {
                case TopXXMetric.OneWayDelayDS:
                    return Resources.VNQMWebContent.VNQMWEBCODE_VB1_224;
                case TopXXMetric.OneWayDelaySD:
                    return Resources.VNQMWebContent.VNQMWEBCODE_VB1_225;
                default:
                    return GetLocalizedProperty("TopXXMetric", metric.ToString());
            }
        }

        return String.Empty;
    }

    protected string GetOperationImage(object item)
    {
        var row = (DataRowView) item;

        int index = row.Row.Table.Columns.IndexOf("OperationTypeID");
        if(index < 0)
            return String.Empty;

        short value = (short) row[index];
        var operationType = (OperationType) value;
        return IpSlaOperationHelper.GetTypeImageUrl(operationType, false);
    }

    protected object GetColumnValue(object item, int columnIndex)
    {
        if (!IsColumnVisible(columnIndex))
            return String.Empty;

        TopXXMetric metric = Metrics[columnIndex];

        DataRowView row = (DataRowView)item;

        object value = row[metric.ToString()];
        if (DBNull.Value.Equals(value))
             return "&nbsp;";

        object result;
        double numericValue = Convert.ToDouble(value);

        switch (metric)
        {
            case TopXXMetric.Jitter:
                result = new Jitter(numericValue);
                break;
            case TopXXMetric.Latency:
                result = new Latency(numericValue);
                break;
            case TopXXMetric.Mos:
                result = new MOS(numericValue);
                break;
            case TopXXMetric.OneWayDelayDS:
            case TopXXMetric.OneWayDelaySD:
                result = new Latency(numericValue);
                break;
            case TopXXMetric.PacketLoss:
                result = new VoipPacketLoss(numericValue);
                break;
            case TopXXMetric.HttpDnsRtt:
            case TopXXMetric.HttpTcpConnectRtt:
            case TopXXMetric.HttpTransactionRtt:
            case TopXXMetric.RoundTripTime:
                result = new RoundTripTime(numericValue);
                break;
            default:
                result = String.Empty;
                break;
        }

        return result.ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.DataSource == null || this.DataSource.Rows.Count == 0)
        {
            //NoDataPanel.Visible = true;
        }
        else
        {
            this.Repeater.ItemCreated += ItemCreated;
            this.Repeater.DataSource = this.DataSource;
            this.Repeater.DataBind();
        }
    }

    private void ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            HtmlTableCell cell = (HtmlTableCell) e.Item.FindControl("OperationTypeHeader");
            if (cell != null)
                cell.Visible = ShowOperationTypeColumn;

            ShowHideCell(e, "MetricCol");
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableCell cell = (HtmlTableCell)e.Item.FindControl("OperationTypeCell");
            if (cell != null)
                cell.Visible = ShowOperationTypeColumn;

            ShowHideCell(e, "MetricCell");
        }
    }

    private void ShowHideCell(RepeaterItemEventArgs e, string prefix)
    {
        for (int index = 0; index < DataColumnCount; index++)
        {
            if (IsColumnVisible(index))
                continue;
 
            HtmlTableCell col = (HtmlTableCell) e.Item.FindControl(prefix + index);
            if (col != null)
                col.Visible = false;
        }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
