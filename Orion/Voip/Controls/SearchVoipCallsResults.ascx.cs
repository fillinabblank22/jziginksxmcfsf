﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Controls_SearchVoipCallsResults : ScriptUserControlBase
{
    /// <summary>
    // Submit button id is used for enabling/disabling Submit button on SearchVoipCalls control
    /// </summary>
    public string SubmitButtonId { get; set; }

	 /// <summary>
    /// SearchCriteria Label Id is used to get the searchcriteria value for including it in Excel file when search results are exported
    /// </summary>
    public string SearchCriteriaLabelId { get; set; }
	
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.SearchVoipCallsResults", this.ClientID);
        descriptor.AddElementProperty("extArea", this.extArea.ClientID);
        descriptor.AddProperty("maxPageSize", IpSlaSettings.VoipCallSearchResultsMaxPageSize.Value.ToString());
        descriptor.AddProperty("submitButtonId", this.SubmitButtonId);
		descriptor.AddProperty("lblSearchCriteriaId", this.SearchCriteriaLabelId);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("/Orion/Voip/js/VoipExtStateProvider.js"));
        yield return new ScriptReference(this.ResolveUrl("/Orion/Voip/js/ScriptServiceInvoker.js"));
        yield return new ScriptReference(this.ResolveUrl("/Orion/Voip/js/ScriptServiceProxy.js"));
        yield return new ScriptReference(this.ResolveUrl("/Orion/Voip/js/RadioSelectionModel.js"));
        yield return new ScriptReference(this.ResolveUrl("SearchVoipCallsResults.js"));
    }
}