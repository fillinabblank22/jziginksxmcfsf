﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Voip_Controls_CliSettingsControl : ScriptUserControlBase
{
    #region properties

    public string Username
    {
        get { return tbUsername.Text; }
    }

    public string Password
    {
        get { return tbPassword.Text; }
    }

    public int EnableLevel
    {
        get
        {
            if (string.IsNullOrEmpty(hfReturnEnableLevelValue.Value))
                return -2;

            return Convert.ToInt32(hfReturnEnableLevelValue.Value);
        }
    }

    public string EnablePassword
    {
        get { return tbEnablePassword.Text; }
    }

    public CliConnectionProtocol Protocol
    {
        get { return ParseCliConnectionProtocol(hfReturnProtocolValue.Value); }
    }

    public int Port
    {
        get { return Convert.ToInt32(tbPort.Text); }
        set { tbPort.Text = value.ToString(); }
    }

    public int Timeout
    {
        get { return Convert.ToInt32(tbTimeout.Text); }
        set { tbTimeout.Text = value.ToString(); }
    }

    public string ValidationIp { get; set; }

    public string ValidationEngineId { get; set; }

    public int PreselectedCredentialsId { get; set; }

    public int CredentialsId
    {
        get
        {
            if (string.IsNullOrEmpty(hfReturnCredentialsId.Value))
                return 0;

            return int.Parse(hfReturnCredentialsId.Value);
        }
    }

    public string CredentialsName
    {
        get { return ddlSavedCredentials.Text; }
    }

    public IEnumerable<int> NodeIds { get; set; }

    public bool IsFipsEnabledOnAnyEngine
    {
        get
        {
            return EnginesDAL.IsFIPSModeEnabledOnAnyEngine();
        }
    }

    public bool HideEnableLevel { get; set; }

    #endregion

    protected static readonly string RequiredFieldError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_95;
    protected static readonly string PortNumberRequiredFieldError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_267;
    protected static readonly string PortNumberMinValueError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_268;
    protected static readonly string PortNumberMaxValueError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_269;
    protected static readonly string TimeoutRequiredFieldError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_270;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (NodeIds != null)
        {
            ValidationEngineId = Convert.ToString(CallManagersDAL.Instance.GetEngineIDByNodeID(NodeIds.FirstOrDefault()));
        }

        btnValidate.Attributes["onclick"] = "if (Page_ClientValidate('VoipCliCredentials')) { $find('" + this.ClientID + "').btnValidate_Click($(this)); } return false;";
        userNameValidator.ClientValidationFunction = "VoipCliValidateName_" + this.ClientID;
        passwordValidator.ClientValidationFunction = "VoipCliValidatePassword_" + this.ClientID;
        btnAdvanced.ElementID = this.advancedBody.ClientID;
        btnAdvanced.OnClientExpanded = "$find('" + this.ClientID + "').btnAdvanced_Expanded();";
    }

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.CliSettingsControl", this.ClientID);
        descriptor.AddComponentProperty("testResult", this.testResult.ClientID);
        descriptor.AddElementProperty("ddlSavedCredentials", this.ddlSavedCredentials.ClientID);
        descriptor.AddElementProperty("tbUsername", this.tbUsername.ClientID);
        descriptor.AddElementProperty("tbPassword", this.tbPassword.ClientID);
        descriptor.AddElementProperty("tbEnablePassword", this.tbEnablePassword.ClientID);
        descriptor.AddElementProperty("ddlEnableLevel", this.ddlEnableLevel.ClientID);
        descriptor.AddElementProperty("ddlProtocol", this.ddlProtocol.ClientID);
        descriptor.AddElementProperty("tbPort", this.tbPort.ClientID);
        descriptor.AddElementProperty("tbTimeout", this.tbTimeout.ClientID);
        descriptor.AddElementProperty("btnValidate", this.btnValidate.ClientID);
        descriptor.AddElementProperty("validationProgress", this.validationProgress.ClientID);
        descriptor.AddElementProperty("hfReturnCredentialsId", this.hfReturnCredentialsId.ClientID);
        descriptor.AddElementProperty("hfReturnEnableLevelValue", this.hfReturnEnableLevelValue.ClientID);
        descriptor.AddElementProperty("hfReturnProtocolValue", this.hfReturnProtocolValue.ClientID);

        descriptor.AddProperty("ValidationIp", ValidationIp);
        descriptor.AddProperty("ValidationEngineId", ValidationEngineId);
        descriptor.AddProperty("PreselectedCredentialsId", PreselectedCredentialsId);
        //descriptor.AddProperty("NodeIds", !IsPostBack ? NodeIds : Enumerable.Empty<int>());
        descriptor.AddProperty("NodeIds", NodeIds);
        descriptor.AddProperty("IsFipsEnabledOnAnyEngine", this.IsFipsEnabledOnAnyEngine);

        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("CliSettingsControl.js"));
    }

    private CliConnectionProtocol ParseCliConnectionProtocol(string value)
    {
        return (CliConnectionProtocol)Enum.Parse(typeof(CliConnectionProtocol), value);
    }

    protected void userNameValidator_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CredentialsId == 0 && string.IsNullOrEmpty(Username.Trim()))
            args.IsValid = false;
    }

    protected void passwordValidator_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CredentialsId == 0 && string.IsNullOrEmpty(Password.Trim()))
            args.IsValid = false;
    }
}