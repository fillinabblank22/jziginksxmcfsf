﻿using System;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;

public partial class Orion_Voip_Controls_OperationStatusImage : System.Web.UI.UserControl, IRenderable<OperationStatus>
{
    public OperationStatus OperationStatus { get; set; }
    public string StatusMessage { get; set; }

    public string ImageUrl
    {
        get
        {
            string imagePath = IpSlaOperationHelper.GetStatusImageUrl(this.OperationStatus);
            return imagePath;
        }
    }

    public string Text
    {
        get
        {
            string msg = GetLocalizedProperty("OperationStatus", OperationStatus.ToString());
            if (!String.IsNullOrEmpty(StatusMessage))
                msg = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_227, msg, StatusMessage);

            return msg;
        }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    #region IRenderable<OperationStatus> Members

    public void SetRenderableData(OperationStatus data)
    {
        OperationStatus = data;
    }

    #endregion
}
