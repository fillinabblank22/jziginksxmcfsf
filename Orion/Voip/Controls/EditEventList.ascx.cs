﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Controls_EditEventList : BaseResourceEditControl
{
	private static int defaultMaxEventCount = 25;
    protected static readonly string maxEventsLabel = Resources.VNQMWebContent.VNQMWEBCODE_AK1_66;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		// load values from resources

		this.MaxEventsText.Text = Resource.Properties[ResourcePropertiesHelper.MaxCountProperty] ?? defaultMaxEventCount.ToString();

        if (ResourcePropertiesHelper.IsCustomObjectResource())
        {
            MaxEventsText.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesHelper.MaxCountProperty + "', this.value);");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			try
			{
				Int32.Parse(this.MaxEventsText.Text);
                properties.Add(ResourcePropertiesHelper.MaxCountProperty, this.MaxEventsText.Text);
			}
			catch
			{
                properties.Add(ResourcePropertiesHelper.MaxCountProperty, defaultMaxEventCount.ToString());
			}

			return properties;
		}
	}
}
