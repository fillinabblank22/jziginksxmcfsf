using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Resources_PeriodSelector : System.Web.UI.UserControl
{
    private List<Period> NamedTimePeriods = SolarWinds.Orion.IpSla.Web.Periods.GetTimePeriodList();
    private const string DefaultPeriod = "Last 4 Hours";
    private static readonly string DefaultLocalizedPeriod = Resources.VNQMWebContent.VNQMWEBDATA_TM0_103;
    private bool isPeriodSet = false;
    [PersistenceMode(PersistenceMode.Attribute)]
    public string Period
    {
        get
        {
            return CreatePeriod();
        }
        set
        {
            isPeriodSet = true;
            SetupPeriod(value);
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string LocalizedPeriod
    {
        get
        {
            return CreateLocalizedPeriod();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.radAbsolute.Attributes.Add("onselect", this.ActuatorScriptName + "(false, false, true); return true;");
        this.radAbsolute.Attributes.Add("onclick", this.radAbsolute.Attributes["onselect"]);
        
        this.radNamed.Attributes.Add("onselect", this.ActuatorScriptName + "(true, false, false); return true;");
        this.radNamed.Attributes.Add("onclick", this.radNamed.Attributes["onselect"]);
        
        this.radRelative.Attributes.Add("onselect", this.ActuatorScriptName + "(false, true, false); return true;");
        this.radRelative.Attributes.Add("onclick", this.radRelative.Attributes["onselect"]);
       
        if (!this.IsPostBack)
        {
            this.lbxNamedPeriods.DataSource = NamedTimePeriods;
            this.lbxNamedPeriods.DataTextField = "DisplayName";
            this.lbxNamedPeriods.DataValueField = "Name";
            this.lbxNamedPeriods.DataBind();
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        if(!isPeriodSet)
            SetupPeriod(this.Period);

        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), this.ActuatorScriptName, this.BuildActuatorScript(), true);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ActuatorScriptName + "_Startup",
             this.ActuatorScriptName + string.Format("({0}, {1}, {2});", radNamed.Checked.ToString().ToLower(), radRelative.Checked.ToString().ToLower(), radAbsolute.Checked.ToString().ToLower()), true);
    }

    private string CreatePeriod()
    {
        if (this.radNamed.Checked)
            return this.lbxNamedPeriods.SelectedValue;
        else if (this.radRelative.Checked)
            return this.relativePeriod.TimePeriod;
        else if (this.radAbsolute.Checked)
        {
            return string.Concat(StartTime.DateTimeValue, "~", EndTime.DateTimeValue);
        }
        else
            return DefaultPeriod;
    }

    private string CreateLocalizedPeriod()
    {
        if (this.radNamed.Checked)
            return this.lbxNamedPeriods.SelectedItem.Text;
        else if (this.radRelative.Checked)
            return this.relativePeriod.LocalizedTimePeriod;
        else if (this.radAbsolute.Checked)
        {
            return string.Concat(StartTime.DateTimeValue, "~", EndTime.DateTimeValue);
        }
        else
            return DefaultLocalizedPeriod;
    }
    
    private void SetupPeriod(string period)
    {
        if (!period.Contains("~"))
        {
            SetupNamedPeriodForm(period);
            SetupRelativePeriodForm(period);
        }
        else
        {
            SetupAbsolutePeriodForm(period);
        }
    }
    
    private void SetupNamedPeriodForm(string period)
    {
        int index = 0;
        foreach (ListItem item in this.lbxNamedPeriods.Items)
        {
            if (period.Equals(item.Value, StringComparison.InvariantCultureIgnoreCase))
            {
                SelectPeriodType(true,false,false);
                break;
            }
            index++;
        }
        lbxNamedPeriods.SelectedIndex = index;
    }

    private void SetupRelativePeriodForm(string period)
    {
        this.relativePeriod.TimePeriod = period;
        if (relativePeriod.TimePeriod == period)
            SelectPeriodType(false,true,false);
    }
       
    private void SetupAbsolutePeriodForm(string period)
    {
        string _period = period;
        
        DateTime start = new DateTime();
        DateTime end= new DateTime();
        try
        {
            Periods.Parse(ref _period, ref start, ref end);

            this.StartTime.DateTimeValue = start.Date.ToString("MM/dd/yyyy");
            this.EndTime.DateTimeValue = end.Date.ToString("MM/dd/yyyy");
        }
        catch (FormatException)
        {
        }

        SelectPeriodType(false, false, true);
    }

    private void SelectPeriodType(bool namedPeriod, bool relPeriod, bool absPeriod)
    {
        radNamed.Checked = namedPeriod;
        radRelative.Checked = relPeriod;
        radAbsolute.Checked = absPeriod;
    }

    #region JavaScript
    
    private const string RADIO_CHANGE_TEMPLATE = @"

function {0}(enableNamed, enableRelative, enableAbsolute)
{{
    var namedDropDown = document.getElementById('{1}');
    
    
    if(enableNamed)
    {{
        namedDropDown.disabled = false;
    }}
    else
    {{
        namedDropDown.disabled = true;
    }}

    if(enableRelative)
    {{
        {3}
    }}
    else
    {{
        {4}
    }}

    if(enableAbsolute)
    {{
      //  igdrp_getComboById('{2}').setEnabled(true);
      //  igdrp_getComboById('{5}').setEnabled(true);
      
      document.getElementById('{2}').disabled = false;
      document.getElementById('{5}').disabled = false;
    }}
    else
    {{
      //if(igdrp_getComboById('{2}') == null)
      //   return;
  
    //igdrp_getComboById('{2}').setEnabled(false);
    //igdrp_getComboById('{5}').setEnabled(false);
      
      document.getElementById('{2}').disabled = true;
      document.getElementById('{5}').disabled = true;
    }}
}}

";

    private string BuildActuatorScript()
    {
        return string.Format(
            RADIO_CHANGE_TEMPLATE,
            this.ActuatorScriptName,    
            this.lbxNamedPeriods.ClientID, 
            this.StartTime.InputDateId, 
            this.relativePeriod.GetClientEnableScript(), 
            this.relativePeriod.GetClientDisableScript(),
            this.EndTime.InputDateId 
            );
    }

    private string ActuatorScriptName
    {
        get { return this.ClientID + "_Actuate"; }
    }

    #endregion
    
    
}
