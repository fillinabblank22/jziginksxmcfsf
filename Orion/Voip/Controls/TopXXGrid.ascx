﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXGrid.ascx.cs" Inherits="Orion_Voip_Controls_TopXXGrid" %>
<%@ Register TagPrefix="ipslam" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>

<asp:Repeater runat="server" ID="Repeater">
	<HeaderTemplate>
		<table width="100%" cellspacing="0" cellpadding="2" border="0" class="NeedsZebraStripes topXXTable">
			<thead>
				<tr>
					<td class="ReportHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_144 %></td>
					<td class="ReportHeader" id="OperationTypeHeader" runat="server" ><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_145 %></td>
						<td style="text-align:left;" class="ReportHeader" id="MetricCol6" runat="server" ><%= GetColumnName(6)%></td>
					<td style="text-align:left;" class="ReportHeader" id="MetricCol5" runat="server" ><%= GetColumnName(5)%></td>
					<td style="text-align:left;" class="ReportHeader" id="MetricCol4" runat="server" ><%= GetColumnName(4)%></td>
					<td style="text-align:left;" class="ReportHeader" id="MetricCol3" runat="server" ><%= GetColumnName(3)%></td>
					<td style="text-align:left;" class="ReportHeader" id="MetricCol2" runat="server" ><%= GetColumnName(2)%></td>
					<td style="text-align:left;" class="ReportHeader" id="MetricCol1" runat="server" ><%= GetColumnName(1)%></td>
					<td style="text-align:left;" class="ReportHeader" id="MetricCol0" runat="server" ><%= GetColumnName(0)%></td>
				</tr>
			</thead>
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td class="Property">
				<a href="<%# SolarWinds.Orion.IpSla.Web.IpSlaOperationHelper.GetOperationDetailPageLink((int)Eval("OperationInstanceID")) %>">
					<ipslam:StatusIcon ID="SmallNodeStatus" runat="server" OperationStatus='<%#Eval("DisplayStatus")%>' StatusMessage='<%#Eval("StatusMessage")%>'/>							
					<%#DataBinder.Eval(Container.DataItem, "OperationName")%>
				</a>
			</td>
			<td class="Property" id="OperationTypeCell" runat="server"><img src="<%# GetOperationImage(Container.DataItem)%>" style="vertical-align:middle;padding-right: 4px;"/><%#Eval("OperationTypeName")%></td>
			<td style="text-align:left;" class="Property" id="MetricCell6" runat="server"><%# GetColumnValue(Container.DataItem, 6)%></td>
			<td style="text-align:left;" class="Property" id="MetricCell5" runat="server"><%# GetColumnValue(Container.DataItem, 5)%></td>
			<td style="text-align:left;" class="Property" id="MetricCell4" runat="server"><%# GetColumnValue(Container.DataItem, 4)%></td>
			<td style="text-align:left;" class="Property" id="MetricCell3" runat="server"><%# GetColumnValue(Container.DataItem, 3)%></td>
			<td style="text-align:left;" class="Property" id="MetricCell2" runat="server"><%# GetColumnValue(Container.DataItem, 2)%></td>
			<td style="text-align:left;" class="Property" id="MetricCell1" runat="server"><%# GetColumnValue(Container.DataItem, 1)%></td>
			<td style="text-align:left;" class="Property" id="MetricCell0" runat="server"><%# GetColumnValue(Container.DataItem, 0)%></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>
<asp:Panel runat="server" ID="NoDataPanel" Visible="false">
<%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_226 %>
</asp:Panel>

