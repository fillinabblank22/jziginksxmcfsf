﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeGroupingSelector.ascx.cs" Inherits="Orion_Voip_Controls_NodeGroupingSelector" %>
<asp:DropDownList ID="ddlGroupBy" runat="server" AutoPostBack="True" 
    onselectedindexchanged="ddlGroupBy_SelectedIndexChanged">
    <asp:ListItem Value="MachineType" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_30 %>"></asp:ListItem>
    <asp:ListItem Value="Vendor" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_31 %>"></asp:ListItem>
</asp:DropDownList>
