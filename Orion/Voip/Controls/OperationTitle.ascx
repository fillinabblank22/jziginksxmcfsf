﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperationTitle.ascx.cs" Inherits="Orion_Voip_Controls_OperationTitle" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web"%>
<%@ Register TagPrefix="ipslam" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>

<ipslam:StatusIcon ID="OperationStatus" runat="server" />&nbsp;<%= OperationName %>