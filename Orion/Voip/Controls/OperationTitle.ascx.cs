﻿using System;
using System.Web;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Controls_OperationTitle : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack)
            return;

        OperationStatus.SetRenderableData(DisplayStatus);
    }

    public OperationStatus DisplayStatus
    {
        get { return Operation.OperationStatus; }
    }

    public string OperationName
    {
        get { return HttpUtility.HtmlEncode(IpSlaOperationHelper.FixName(Operation.Name)); }
    }

    public string SourceSiteName
    {
        get{ return HttpUtility.HtmlEncode(Operation.SourceSiteName); }
    }

    public string SourceSiteUrl
    {
        get { return BaseResourceControl.GetViewLink(Operation.SourceSite.SimulationNode); }
    }

    public string TargetSiteName
    {
        get
        {
            return HttpUtility.HtmlEncode(Operation.TargetSiteName);
        }
    }

    public string TargetSiteUrl
    {
        get 
        {
            if (Operation.HasTargetSite && Operation.TargetSite.VoipSite.NodeID > 0)
                return BaseResourceControl.GetViewLink(Operation.TargetSite.SimulationNode);

            return String.Empty;
        }
    }

    public IpSlaOperation Operation { get; set; }
}
