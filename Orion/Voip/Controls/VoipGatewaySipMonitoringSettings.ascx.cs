﻿using System.Web.UI;
using Resources;

namespace Orion.Voip.Controls
{
    public partial class OrionVoipControlsVoipGatewaySipMonitoringSettingsControl : UserControl
    {
        public bool SipTrunkMonitoringEnabled
        {
            get { return cbSipTrunkMonitoringEnabled.Checked; }
            set { cbSipTrunkMonitoringEnabled.Checked = value; }
        }
        public bool MaxConcurrentCallsOverwritten
        {
            get { return cbMaxConcurrentSipCallsOverwritten.Checked; }
            set { cbMaxConcurrentSipCallsOverwritten.Checked = value; }
        }

        public string MaxConcurrentSipCalls
        {
            get { return txtMaxConcurrentSipCalls.Text; }
            set { txtMaxConcurrentSipCalls.Text = value; }
        }

        public bool OverwrittenMaxConcurrentSipCallsVisible
        {
            get { return trMaxConcurrentSipCalls.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] == "visible"; }
            set { trMaxConcurrentSipCalls.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = value ? "visible" : "hidden"; }
        }

        public bool EnableMaxConcurrentSipCallsValidator
        {
            set { reqMaxConcurrentSipCalls.Enabled = value; }
        }

        public bool EnablePositiveMaxConcurrentSipCallsValidator
        {
            set { positiveMaxConcurrentSipCalls.Enabled = value; }
        }
    }
}