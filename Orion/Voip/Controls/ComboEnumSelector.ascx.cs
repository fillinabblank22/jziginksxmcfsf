﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Data;

public partial class Orion_Voip_Controls_ComboEnumSelector : System.Web.UI.UserControl
{
    Type enumType;
    public Type EnumType
    {
        get { return enumType; }
        set
        {
            enumType = value;
            if (!this.IsPostBack)
            {
                GenerateItems();
            }
        }
    }

    public string ValidatorErrorMessage { get; set; }
    public string ValidatorInitialValue { get; set; }

    public object SelectedValue
    {
        get
        {
            if (String.IsNullOrEmpty(this.DropDownList.SelectedValue))
                return null;

            return Enum.Parse(this.EnumType, this.DropDownList.SelectedValue);
        }
        set
        {
            SelectItem(value);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RequiredFieldValidator.InitialValue = ValidatorInitialValue;
        RequiredFieldValidator.ErrorMessage = ValidatorErrorMessage;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        //if (!this.IsPostBack)
        //{
        //    GenerateItems();
        //}
    }

    void SelectItem(object enumValue)
    {
        DropDownList.SelectedValue = enumValue.ToString();
    }

    void GenerateItems()
    {
        foreach (object value in Enum.GetValues(this.EnumType))
        {
            ListItem item = new ListItem(AttributeHelper.GetEnumDescription(value), value.ToString());
            DropDownList.Items.Add(item);
        }
    }

}
