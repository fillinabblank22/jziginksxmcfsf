using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.IpSla.Web.Filters;
using SolarWinds.Orion.IpSla.Data;

public partial class Orion_Voip_Controls_AdvancedNodeFilter : UserControl
{
    private ORFilter _filters = new ORFilter( );
    private FilterType _filterType = FilterType.Node;
    string _filterTitle = "Filter VoIP Simulation Node List";
    const string PreviousFilter = "PreviousFilter";
    const string FilterGroup = "FilterGroup";

    public string FilterTitle
    {
        get { return _filterTitle; }
        set { _filterTitle = value; }
    }


    public ORFilter FilterCollection
    {
        get
        {
            if(_filters.Filters.Count==0)
            {
                _filters.Filters.Add(new ANDFilter());
            }
            return _filters;
        }
        set
        {
             _filters = value;
        }
    }

    public FilterType FilterType
    {
        get { return _filterType; }
        set { _filterType = value; }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        object obj = this.ViewState[FilterGroup];
        if(obj!=null)
            _filters = (ORFilter) obj;
    }

    protected override object SaveViewState()
    {
        if (_filters.Filters.Count > 0)
            this.ViewState[FilterGroup] = _filters;
        
        return base.SaveViewState();
    }

    public void BackupCurrentFilter()
    {
        this.ViewState[PreviousFilter] = this.FilterCollection.SerializeFilter();
    }

    public void RestorePreviousFilter()
    {
        if (this.ViewState[PreviousFilter] != null)
        {
            string xmlFilter = (string)this.ViewState[PreviousFilter];
            IFilter filter = FilterUtility.XmlStrToFilter(xmlFilter);
            if (filter is ORFilter)
            {
                _filters = filter as ORFilter;
            }
        }
        else
        {
            _filters = new ORFilter();
        }

        this.OrFiltersRepeater.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["AdvancedCallPathFilter"] != null)
            {
                string xmlfilter = (string)Session["AdvancedCallPathFilter"];
                _filters = ((ORFilter)FilterUtility.XmlStrToFilter(xmlfilter));
            }
            this.OrFiltersRepeater.DataBind();
        }        
    }

    protected override void OnInit(EventArgs e)
    {
        NFM.FilterType = FilterType;
        base.OnInit(e);
    }

    protected void btnAddFilterGroup_Click(object sender, EventArgs e)
    {
        _filters.Filters.Add(new ANDFilter());
        
       this.OrFiltersRepeater.DataBind();
    }
    
    protected void btnDeleteFilterGroup_Click(object sender, EventArgs e)
    {
        RepeaterItem item = (RepeaterItem)((Control)sender).Parent;
        int extItemIndex = item.ItemIndex;
        
        _filters.Filters.RemoveAt(extItemIndex);

        this.OrFiltersRepeater.DataBind();
    }

    protected void btnAddFilter_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ImageButton btn = (ImageButton) sender;
            int extItemIndex = _filters.Filters.Count - 1;

            ((ANDFilter) _filters.Filters[extItemIndex]).Filters.Add(NFM.Filter);

            this.OrFiltersRepeater.DataBind();

            NFM.ClearBoxes();
        }
    }
   
    protected void btnDeleteFilter_Click(object sender, EventArgs e)
    {
        ImageButton btn = (ImageButton) sender;
        int innerItemIndex = Convert.ToInt32(btn.CommandArgument);
        int extItemIndex = Convert.ToInt32(btn.CommandName);

        ((ANDFilter)_filters.Filters[extItemIndex]).Filters.RemoveAt(innerItemIndex);
        
        this.OrFiltersRepeater.DataBind();
    }

    protected void ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = bool.Parse(args.Value);
    }

    protected void OrFiltersRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
    {
        switch (args.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                Control ctrl_delete = args.Item.FindControl("linkDeleteFilterGroup");
                Control ctrl_add = args.Item.FindControl("linkAddFilterGroup");
                ScriptManager scm = ScriptManager.GetCurrent(this.Page);
                scm.RegisterAsyncPostBackControl(ctrl_delete);
                scm.RegisterAsyncPostBackControl(ctrl_add);
                break;
        }
    }

    protected void FilterOk_Click(object sender, EventArgs e)
    {
        this.BackupCurrentFilter();
        OnFilterSubmitted(EventArgs.Empty);
    }

    protected void FilterCancel_Click(object sender, EventArgs e)
    {
        this.RestorePreviousFilter();
        OnFilterCancelled(EventArgs.Empty);
    }

    public event EventHandler FilterSubmitted;
    public void OnFilterSubmitted(EventArgs e)
    {
        if (FilterSubmitted != null)
            FilterSubmitted(this, e);
    }

    public event EventHandler FilterCancelled;
    public void OnFilterCancelled(EventArgs e)
    {
        if (FilterCancelled != null)
            FilterCancelled(this, e);
    }
}
