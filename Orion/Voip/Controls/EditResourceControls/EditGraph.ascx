﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditGraph.ascx.cs" Inherits="Orion_Voip_Controls_EditResourceControls_EditGraph" %>
<%@ Register TagPrefix="orion" TagName="DateTimePicker" Src="~/Orion/Controls/DateTimePicker.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<div>
	<asp:PlaceHolder ID="mainPlaceHolder" runat="server">
		<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
		<table>
			<tr class="voipGraphEditorCellTitleHeight">
				<td colspan="2">
					<h1 class="voipGraphEditorH1Style"><%= Page.Title %></h1>
				</td>
			</tr>
			<tr>
				<td class="voipGraphEditorCellHeader">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_211 %>
				</td>
				<td>
					<asp:DropDownList ID="TimePeriodsList" runat="server" Width="173" />
				</td>
			</tr>
			<tr>
				<td class="voipGraphEditorCellHeader">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_212 %>
				</td>
				<td>
					<asp:DropDownList ID="SampleSizeList" runat="server" Width="173" />
					<asp:CustomValidator ID="LargeIntervalValidator" runat="server" ControlToValidate="SampleSizeList"
						ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_213 %>" OnServerValidate="IntervalValidation"> *</asp:CustomValidator>
				</td>
			</tr>
			<tr>
				<td class="voipGraphEditorCellHeader">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_214%>
				</td>
				<td>
					<asp:CheckBox ID="CheckBoxCustom" runat="server" CssClass="voipGraphEditorCheckbox" />
				</td>
			</tr>
			<tr class="CustomDate">
				<td class="voipGraphEditorCellHeader">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_215 %>
				</td>
				<td>
					<orion:DateTimePicker ID="BeginDateTimePeriod" runat="server" />
					<asp:CustomValidator ID="PeriodBeginCustomValidator" runat="server" ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_216 %>"
						OnServerValidate="checkStartEndDate"> *</asp:CustomValidator>
				</td>
			</tr>
			<tr class="CustomDate">
				<td class="voipGraphEditorCellHeader">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_218 %>
				</td>
				<td>
					<orion:DateTimePicker ID="EndDateTimePeriod" runat="server" />
					<asp:CustomValidator ID="PeriodEndCustomValidator" runat="server" ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_216 %>"
						OnServerValidate="checkStartEndDate"> *</asp:CustomValidator>
				</td>
			</tr>
		</table>
	</asp:PlaceHolder>
</div>

<script type="text/javascript">
	function SetCustomDateVisibility() {
		if (document.getElementById('<%=CheckBoxCustom.ClientID %>').checked == true) {
					$(".CustomDate").show();
					document.getElementById('<%=TimePeriodsList.ClientID %>').disabled = true;
			 } else {
			 	$(".CustomDate").hide();
			 	document.getElementById('<%=TimePeriodsList.ClientID %>').disabled = false;
			 }

		 }
		 $('#<%=CheckBoxCustom.ClientID %>').change(SetCustomDateVisibility);
	SetCustomDateVisibility();
</script>
