﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
//
using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Charting;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using System.Globalization;

public partial class Orion_Voip_Controls_EditResourceControls_EditGraph : SolarWinds.Orion.Web.UI.BaseResourceEditControl
{
	public override string DefaultResourceTitle
	{
		get { return "Edit graph resource."; }
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			return new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
			{
				{"Period", this.GetSelectedTimePeriod},	
				{"IsCustom", this.CheckBoxCustom.Checked},
				{"SampleSize", SampleSizes.FindSampleSizeBySizeName(SampleSizeList.SelectedValue).SizeName ?? new SampleSize().SizeName},
				{"ResourceID", Resource.ID},
			};
		}
	}

	private string GetSelectedTimePeriod
	{
		get
		{
			DateTime perSt = DateTime.Now;
			DateTime perEnd = DateTime.Now;
			if (CheckBoxCustom.Checked)
			{
				if (DateTime.TryParse(BeginDateTimePeriod.Value.ToString(), out perSt) && DateTime.TryParse(EndDateTimePeriod.Value.ToString(), out perEnd))
					return string.Format("{0}~{1}", perSt.ToString(CultureInfo.InvariantCulture), perEnd.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				return TimePeriodsList.SelectedValue;
			}
			return null;
		}
	}

	private T GetPropValue<T>(string propertyName)
	{
		if (Resource.Properties.ContainsKey(propertyName))
			return (T)System.Convert.ChangeType(Resource.Properties[propertyName], typeof(T));
		else
			return default(T);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		Page.Title = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_216, Resource.Title);

		// If page is called from other then save previous page Url and fill a list
		if (!Page.IsPostBack)
		{
			//Filling dropdown list with periods
			foreach (var period in Periods.GetTimePeriodList())
			{
				TimePeriodsList.Items.Add(new ListItem(period.DisplayName, period.Name));
			}
			//isCustom = false;

			foreach (var sampleSize in SampleSizes.GenerateSelectionList())
			{
				SampleSizeList.Items.Add(new ListItem(GetLocalizedProperty("SampleSize", sampleSize.Name), sampleSize.SizeName));
			}

			getCurrentGraphData();

			if (BeginDateTimePeriod.Value == DateTime.MinValue)
			{
				DateTime now = DateTime.Now;
				setTimeValues(now, now);
			}
		}
	}

	/// <summary>
	/// Returns true if entered Interval fit in range
	/// </summary>
	/// <returns></returns>
	private bool IsIntervalCorrect()
	{
		DateTime _startTime = DateTime.Now;
		DateTime _endTime = DateTime.Now;
		if (CheckBoxCustom.Checked)
		{
			string _startTimeString = BeginDateTimePeriod.Value.ToString();
			string _endTimeString = EndDateTimePeriod.Value.ToString();
			_startTime = DateTime.Parse(_startTimeString);
			_endTime = DateTime.Parse(_endTimeString);
		}
		else
		{
			string _periodName = TimePeriodsList.SelectedValue;
			Periods.Parse(ref _periodName, ref _startTime, ref _endTime);
		}

		TimeSpan difference = _endTime - _startTime;

		TimeSpan sampleInterval = TimeSpan.FromMinutes((SampleSizes.FindSampleSizeBySizeName(SampleSizeList.SelectedValue) ?? new SampleSize()).Minutes);
		// Minimum value of interval is difference of start time and end time
		return (difference.TotalMinutes >= sampleInterval.TotalMinutes);
	}
	/// <summary>
	/// Custom validation of SampleSizeList
	/// </summary>
	/// <param name="source"></param>
	/// <param name="args"></param>
	protected void IntervalValidation(object source, ServerValidateEventArgs args)
	{
		args.IsValid = IsIntervalCorrect();
	}

	private void getCurrentGraphData()
	{
		bool isCustom = false;
		string _period = GetPropValue<string>("Period");
		DateTime _StartPeriod = DateTime.Now;
		DateTime _EndPeriod = DateTime.Now;
		string _SampleSize = GetPropValue<string>("SampleSize");

		//Gets current properties from database
		//GraphHelper.GetCurrentGraphProperties(resourceId, ref _period, ref _SampleSize);

		// If such period is custom period then parse it to StartPeriod and EndPeriod using Parse method 
		if (string.IsNullOrEmpty(_period))
		{
			isCustom = false;
			_period = GaugeConstants.DEFAULT_PERIOD_VALUE;
			_SampleSize = GaugeConstants.DEFAULT_SAMPLE_SIZE;
		}
		else
		{
			// If such period is custom period then parse it to StartPeriod and EndPeriod using Parse method 
			if (Periods.GetPeriod(_period) == null)
			{
				isCustom = true;
				Periods.Parse(ref _period, ref _StartPeriod, ref _EndPeriod);
			}
		}

		SampleSizeList.SelectedValue = _SampleSize;

		CheckBoxCustom.Checked = isCustom;
		if (isCustom)
		{
			setTimeValues(_StartPeriod, _EndPeriod);
		}
		else
		{
			TimePeriodsList.SelectedValue = _period;
		}
	}

	/// <summary>
	/// Fills current date values using American Calendar system currently
	/// </summary>
	/// <param name="startPeriod"></param>
	/// <param name="endPeriod"></param>
	private void setTimeValues(DateTime startPeriod, DateTime endPeriod)
	{
		BeginDateTimePeriod.Value = startPeriod.ToLocalTime();
		EndDateTimePeriod.Value = endPeriod.ToLocalTime();
	}

	protected void checkStartEndDate(object source, ServerValidateEventArgs args)
	{
		args.IsValid = IsStartEndTimeIntervalCorrect();
	}

	private bool IsStartEndTimeIntervalCorrect()
	{
		DateTime startPeriod = BeginDateTimePeriod.Value;
		DateTime endPeriod = EndDateTimePeriod.Value;

		return endPeriod >= startPeriod;
	}

	protected string GetLocalizedProperty(string prefix, string property)
	{
		ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
		string key = manager.CleanResxKey(prefix, property);
		return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
	}
}