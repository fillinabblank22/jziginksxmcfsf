<%@ Control Language="C#" ClassName="MOSBar" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.DisplayTypes" %>

<script runat="server">
	private MOS _value;

	public MOS Value
    {
        get { return _value; }
        set { _value = value; }
    }

    protected string ClassName
    {
        get
        {
			return string.Format("{0:class}", Value);
        }
    }
</script>

<td class="InlineBar <%=ClassName%>"><h3><div style="width:<%=100.0*Value.Value/5.0%>%; ; height: 10px; font-size: 7px;">&nbsp;</div></h3></td>
