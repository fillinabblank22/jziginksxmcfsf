﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls");

Orion.Voip.Controls.SearchVoipCalls = function (element) {
    Orion.Voip.Controls.SearchVoipCalls.initializeBase(this, [element]);

    this.rbSearchCriteriaOR = null;
    this.rbSearchCriteriaAND = null;

    this.ddlCallOriginationType = null;
    this.divCallOriginationIPAddressorPhone = null;
    this.txtOriginIPAddressorPhone = null;
    this.ddlOriginGateway = null;
    this.ddlOriginRegion = null;

    this.ddlCallDestinationType = null;
    this.divCallDestinationIPAddressorPhone = null;
    this.txtDestinationIPAddressorPhone = null;
    this.ddlDestinationGateway = null;
    this.ddlDestinationRegion = null;

    this.ddlCallManagers = null;

    this.cbFailed = null;
    this.cbNoZeroLength = null;
    this.cbSuccessful = null;
    this.cbCallWithIssue = null;
    this.cbConferenceCall = null;
    this.cbRedirectReason = null;
    this.ddlRedirectReason = null;
    this.cbCallTerminationCause = null;
    this.ddlCallTerminationCause = null;
    this.cbAvayaCondCode = null;
    this.ddlAvayaCondCode = null;
    
    this.ddlCallTime = null;
    this.divCallTime = null;
    this.txtStartDate = null;
    this.txtStartTime = null;
    this.txtEndDate = null;
    this.txtEndTime = null;

    this.cbMOS = null;
    this.ddlMOS = null;
    this.divMOS = null;
    this.txtMOSLimit1 = null;
    this.txtMOSLimit2 = null;
    this.cbLatency = null;
    this.ddlLatency = null;
    this.divLatency = null;
    this.txtLatencyLimit1 = null;
    this.txtLatencyLimit2 = null;
    this.cbJitter = null;
    this.ddlJitter = null;
    this.divJitter = null;
    this.txtJitterLimit1 = null;
    this.txtJitterLimit2 = null;
    this.cbPacketLoss = null;
    this.ddlPacketLoss = null;
    this.divPacketLoss = null;
    this.txtPacketLossLimit1 = null;
    this.txtPacketLossLimit2 = null;

    this.phMos = null;
    this.phLatency = null;
    this.phCiscoCallTerminatationCause = null;
    this.phCiscoRedirectReason = null;
    this.phAvayaCondCode = null;
    
    
    this.btnSubmit = null;
    this.lblSearchCriteria = null;

    this.resultsControl = null;

    this.cbIncomingCallProtocol = document.getElementById("cbIncomingCallProtocol");
    this.ddlIncomingCallProtocol = document.getElementById("ddlIncomingCallProtocol");
    this.cbOutgoingCallProtocol = document.getElementById("cbOutgoingCallProtocol");
    this.ddlOutgoingCallProtocol = document.getElementById("ddlOutgoingCallProtocol");
};
Orion.Voip.Controls.SearchVoipCalls.prototype = {

    get_rbSearchCriteriaOR: function () { return this.rbSearchCriteriaOR; },
    set_rbSearchCriteriaOR: function (value) { this.rbSearchCriteriaOR = value; },

    get_rbSearchCriteriaAND: function () { return this.rbSearchCriteriaAND; },
    set_rbSearchCriteriaAND: function (value) { this.rbSearchCriteriaAND = value; },

    get_cbFailed: function () { return this.cbFailed; },
    set_cbFailed: function (value) { this.cbFailed = value; },

    get_cbNoZeroLength: function () { return this.cbNoZeroLength; },
    set_cbNoZeroLength: function (value) { this.cbNoZeroLength = value; },

    get_cbRedirectReason: function () { return this.cbRedirectReason; },
    set_cbRedirectReason: function (value) { this.cbRedirectReason = value; },

    get_ddlRedirectReason: function () { return this.ddlRedirectReason; },
    set_ddlRedirectReason: function (value) { this.ddlRedirectReason = value; },

    get_cbCallTerminationCause: function () { return this.cbCallTerminationCause; },
    set_cbCallTerminationCause: function (value) { this.cbCallTerminationCause = value; },

    get_ddlCallTerminationCause: function () { return this.ddlCallTerminationCause; },
    set_ddlCallTerminationCause: function (value) { this.ddlCallTerminationCause = value; },

    get_ddlCallOriginationType: function () { return this.ddlCallOriginationType; },
    set_ddlCallOriginationType: function (value) { this.ddlCallOriginationType = value; },

    get_ddlCallDestinationType: function () { return this.ddlCallDestinationType; },
    set_ddlCallDestinationType: function (value) { this.ddlCallDestinationType = value; },

    get_ddlCallManagers: function () { return this.ddlCallManagers; },
    set_ddlCallManagers: function (value) { this.ddlCallManagers = value; },

    get_ddlCallTime: function () { return this.ddlCallTime; },
    set_ddlCallTime: function (value) { this.ddlCallTime = value; },

    get_cbMOS: function () { return this.cbMOS; },
    set_cbMOS: function (value) { this.cbMOS = value; },

    get_ddlMOS: function () { return this.ddlMOS; },
    set_ddlMOS: function (value) { this.ddlMOS = value; },

    get_cbLatency: function () { return this.cbLatency; },
    set_cbLatency: function (value) { this.cbLatency = value; },

    get_ddlLatency: function () { return this.ddlLatency; },
    set_ddlLatency: function (value) { this.ddlLatency = value; },

    get_cbJitter: function () { return this.cbJitter; },
    set_cbJitter: function (value) { this.cbJitter = value; },

    get_ddlJitter: function () { return this.ddlJitter; },
    set_ddlJitter: function (value) { this.ddlJitter = value; },

    get_cbPacketLoss: function () { return this.cbPacketLoss; },
    set_cbPacketLoss: function (value) { this.cbPacketLoss = value; },

    get_ddlPacketLoss: function () { return this.ddlPacketLoss; },
    set_ddlPacketLoss: function (value) { this.ddlPacketLoss = value; },

    get_divCallOriginationIPAddressorPhone: function () { return this.divCallOriginationIPAddressorPhone; },
    set_divCallOriginationIPAddressorPhone: function (value) { this.divCallOriginationIPAddressorPhone = value; },

    get_ddlOriginGateway: function () { return this.ddlOriginGateway; },
    set_ddlOriginGateway: function (value) { this.ddlOriginGateway = value; },

    get_ddlOriginRegion: function () { return this.ddlOriginRegion; },
    set_ddlOriginRegion: function (value) { this.ddlOriginRegion = value; },

    get_divCallDestinationIPAddressorPhone: function () { return this.divCallDestinationIPAddressorPhone; },
    set_divCallDestinationIPAddressorPhone: function (value) { this.divCallDestinationIPAddressorPhone = value; },

    get_ddlDestinationGateway: function () { return this.ddlDestinationGateway; },
    set_ddlDestinationGateway: function (value) { this.ddlDestinationGateway = value; },

    get_ddlDestinationRegion: function () { return this.ddlDestinationRegion; },
    set_ddlDestinationRegion: function (value) { this.ddlDestinationRegion = value; },

    get_divCallTime: function () { return this.divCallTime; },
    set_divCallTime: function (value) { this.divCallTime = value; },

    get_divMOS: function () { return this.divMOS; },
    set_divMOS: function (value) { this.divMOS = value; },

    get_divLatency: function () { return this.divLatency; },
    set_divLatency: function (value) { this.divLatency = value; },

    get_divJitter: function () { return this.divJitter; },
    set_divJitter: function (value) { this.divJitter = value; },

    get_divPacketLoss: function () { return this.divPacketLoss; },
    set_divPacketLoss: function (value) { this.divPacketLoss = value; },

    get_btnSubmit: function () { return this.btnSubmit; },
    set_btnSubmit: function (value) { this.btnSubmit = value; },

    get_txtOriginIPAddressorPhone: function () { return this.txtOriginIPAddressorPhone; },
    set_txtOriginIPAddressorPhone: function (value) { this.txtOriginIPAddressorPhone = value; },

    get_txtDestinationIPAddressorPhone: function () { return this.txtDestinationIPAddressorPhone; },
    set_txtDestinationIPAddressorPhone: function (value) { this.txtDestinationIPAddressorPhone = value; },

    get_txtStartDate: function () { return this.txtStartDate; },
    set_txtStartDate: function (value) { this.txtStartDate = value; },

    get_txtStartTime: function () { return this.txtStartTime; },
    set_txtStartTime: function (value) { this.txtStartTime = value; },

    get_txtEndDate: function () { return this.txtEndDate; },
    set_txtEndDate: function (value) { this.txtEndDate = value; },

    get_txtEndTime: function () { return this.txtEndTime; },
    set_txtEndTime: function (value) { this.txtEndTime = value; },

    get_txtMOSLimit1: function () { return this.txtMOSLimit1; },
    set_txtMOSLimit1: function (value) { this.txtMOSLimit1 = value; },

    get_txtMOSLimit2: function () { return this.txtMOSLimit2; },
    set_txtMOSLimit2: function (value) { this.txtMOSLimit2 = value; },

    get_txtLatencyLimit1: function () { return this.txtLatencyLimit1; },
    set_txtLatencyLimit1: function (value) { this.txtLatencyLimit1 = value; },

    get_txtLatencyLimit2: function () { return this.txtLatencyLimit2; },
    set_txtLatencyLimit2: function (value) { this.txtLatencyLimit2 = value; },

    get_txtJitterLimit1: function () { return this.txtJitterLimit1; },
    set_txtJitterLimit1: function (value) { this.txtJitterLimit1 = value; },

    get_txtJitterLimit2: function () { return this.txtJitterLimit2; },
    set_txtJitterLimit2: function (value) { this.txtJitterLimit2 = value; },

    get_txtPacketLossLimit1: function () { return this.txtPacketLossLimit1; },
    set_txtPacketLossLimit1: function (value) { this.txtPacketLossLimit1 = value; },

    get_txtPacketLossLimit2: function () { return this.txtPacketLossLimit2; },
    set_txtPacketLossLimit2: function (value) { this.txtPacketLossLimit2 = value; },

    get_cbSuccessful:function () { return this.cbSuccessful; },
    set_cbSuccessful:function (value) { this.cbSuccessful = value; },
    
    get_cbCallWithIssue: function () { return this.cbCallWithIssue; },
    set_cbCallWithIssue: function (value) { this.cbCallWithIssue = value; },
    
    get_cbConferenceCall: function () { return this.cbConferenceCall; },
    set_cbConferenceCall: function (value) { this.cbConferenceCall = value; },
    
    get_cbAvayaCondCode: function() { return this.cbAvayaCondCode; },
    set_cbAvayaCondCode: function(value) { this.cbAvayaCondCode = value; },

    get_ddlAvayaCondCode: function () { return this.ddlAvayaCondCode; },
    set_ddlAvayaCondCode: function (value) { this.ddlAvayaCondCode = value; },
        
    get_phMos: function () { return this.phMos; },
    set_phMos: function (value) { this.phMos = value; },

    get_phLatency: function () { return this.phMos; },
    set_phLatency: function (value) { this.phLatency = value; },
    
    get_phCiscoCallTerminatationCause: function () { return this.phCiscoCallTerminatationCause; },
    set_phCiscoCallTerminatationCause: function (value) { this.phCiscoCallTerminatationCause = value; },

    get_phCiscoRedirectReason: function() { return this.phCiscoRedirectReason; },
    set_phCiscoRedirectReason: function(value) { this.phCiscoRedirectReason = value; },

    get_phAvayaCondCode: function () { return this.phAvayaCondCode; },
    set_phAvayaCondCode: function (value) { this.phAvayaCondCode = value; },
    
    get_lblSearchCriteria: function () { return this.lblSearchCriteria; },
    set_lblSearchCriteria: function (value) { this.lblSearchCriteria = value; },

    get_resultsControl: function () { return this.resultsControl; },
    set_resultsControl: function (value) { this.resultsControl = value; },

    origintypechanged: function () {
        var originType = this.ddlCallOriginationType.options[this.ddlCallOriginationType.selectedIndex].value;

        if (originType == '-All-') {
            this.divCallOriginationIPAddressorPhone.style.display = "none";
            this.ddlOriginGateway.style.display = "none";
            this.ddlOriginRegion.style.display = "none";
        } else if (originType == 'PhoneNumber' || originType == 'IPAddress') {
            this.divCallOriginationIPAddressorPhone.style.display = "block";
            this.ddlOriginGateway.style.display = "none";
            this.ddlOriginRegion.style.display = "none";
        } else if (originType == 'Location') {
            this.divCallOriginationIPAddressorPhone.style.display = "none";
            this.ddlOriginGateway.style.display = "none";
            this.ddlOriginRegion.style.display = "block";
        } else {
            this.divCallOriginationIPAddressorPhone.style.display = "none";
            this.ddlOriginGateway.style.display = "block";
            this.ddlOriginRegion.style.display = "none";
        }
        return false;
    },

    destinationtypechanged: function () {
        var destinationType = this.ddlCallDestinationType.options[this.ddlCallDestinationType.selectedIndex].value;

        if (destinationType == '-All-') {
            this.divCallDestinationIPAddressorPhone.style.display = "none";
            this.ddlDestinationGateway.style.display = "none";
            this.ddlDestinationRegion.style.display = "none";
        } else if (destinationType == 'PhoneNumber' || destinationType == 'IPAddress') {
            this.divCallDestinationIPAddressorPhone.style.display = "block";
            this.ddlDestinationGateway.style.display = "none";
            this.ddlDestinationRegion.style.display = "none";
        } else if (destinationType == 'Location') {
            this.divCallDestinationIPAddressorPhone.style.display = "none";
            this.ddlDestinationGateway.style.display = "none";
            this.ddlDestinationRegion.style.display = "block";
        } else {
            this.divCallDestinationIPAddressorPhone.style.display = "none";
            this.ddlDestinationGateway.style.display = "block";
            this.ddlDestinationRegion.style.display = "none";
        }
        return false;
    },

    callqualitymoschanged: function () {
        if (this.ddlMOS.options[this.ddlMOS.selectedIndex].value == 'Between') {
            this.divMOS.style.display = "block";
        } else {
            this.divMOS.style.display = "none";
        }
        return false;
    },

    callqualitylatencychanged: function () {
        if (this.ddlLatency.options[this.ddlLatency.selectedIndex].value == 'Between') {
            this.divLatency.style.display = "block";
        } else {
            this.divLatency.style.display = "none";
        }
        return false;
    },

    callqualityjitterchanged: function () {
        if (this.ddlJitter.options[this.ddlJitter.selectedIndex].value == 'Between') {
            this.divJitter.style.display = "block";
        } else {
            this.divJitter.style.display = "none";
        }
        return false;
    },

    callqualitypacketlosschanged: function () {
        if (this.ddlPacketLoss.options[this.ddlPacketLoss.selectedIndex].value == 'Between') {
            this.divPacketLoss.style.display = "block";
        } else {
            this.divPacketLoss.style.display = "none";
        }
        return false;
    },

    calltimechanged: function () {
        if (this.ddlCallTime.options[this.ddlCallTime.selectedIndex].value == 'Between') {
            this.divCallTime.style.display = "block";
        } else {
            this.divCallTime.style.display = "none";
        }
        return false;
    },

    callmanagerchanged: function () {
        var ccm = this.getselectedcmmobj();

        this.updateVoipSearchFields(ccm.ccmType);
    },
    
    updateVoipSearchFields: function (ccmType) {

        if (ccmType == 'ALL') {
            this.phCiscoCallTerminatationCause.style.display = "none";
            this.phCiscoRedirectReason.style.display = "none";
            this.phAvayaCondCode.style.display = "none";

        } else {
            var showAvayaOnly = ccmType == 'ACM' || ccmType == "ALLAVAYA";

            this.phCiscoCallTerminatationCause.style.display = showAvayaOnly ? "none" : "";
            this.phCiscoRedirectReason.style.display = showAvayaOnly ? "none" : "";
            this.phAvayaCondCode.style.display = showAvayaOnly ? "" : "none";
        }
    },

    getselectedcmmobj: function () {
        var values = this.ddlCallManagers.options[this.ddlCallManagers.selectedIndex].value.split('|');
        var ccmObj = {};
        ccmObj.ccmID = values[0];
        ccmObj.ccmType = values[1];
        
        return ccmObj;
    },
    submitclick: function () {
        if ($('#' + this.btnSubmit.id).attr('searchInProgress') == 'true') {
            return false;
        }
        $('#' + this.btnSubmit.id).attr('searchInProgress', 'true');
        if (this.txtStartTime.value == '') {
            this.txtStartTime.value = '12:00 AM';
        }
        if (this.txtEndTime.value == '') {
            this.txtEndTime.value = '12:00 AM';
        }

        SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.Validate(this.ddlCallOriginationType.options[this.ddlCallOriginationType.selectedIndex].value,
                this.txtOriginIPAddressorPhone.value,
                this.ddlOriginRegion.options[this.ddlOriginRegion.selectedIndex].value,
                this.ddlOriginGateway.options[this.ddlOriginGateway.selectedIndex].value,
                this.ddlCallDestinationType.options[this.ddlCallDestinationType.selectedIndex].value,
                this.txtDestinationIPAddressorPhone.value,
                this.ddlDestinationRegion.options[this.ddlDestinationRegion.selectedIndex].value,
                this.ddlDestinationGateway.options[this.ddlDestinationGateway.selectedIndex].value,
                this.ddlCallTime.options[this.ddlCallTime.selectedIndex].value,
                this.txtStartDate.value,
                this.txtStartTime.value,
                this.txtEndDate.value,
                this.txtEndTime.value,
                this.cbMOS.checked,
                this.ddlMOS.options[this.ddlMOS.selectedIndex].value,
                this.txtMOSLimit1.value,
                this.txtMOSLimit2.value,
                this.cbLatency.checked,
                this.ddlLatency.options[this.ddlLatency.selectedIndex].value,
                this.txtLatencyLimit1.value,
                this.txtLatencyLimit2.value,
                this.cbJitter.checked,
                this.ddlJitter.options[this.ddlJitter.selectedIndex].value,
                this.txtJitterLimit1.value,
                this.txtJitterLimit2.value,
                this.cbPacketLoss.checked,
                this.ddlPacketLoss.options[this.ddlPacketLoss.selectedIndex].value,
                this.txtPacketLossLimit1.value,
                this.txtPacketLossLimit2.value,
                Function.createDelegate(this, this.oncomplete),
                Function.createDelegate(this, this.onfailure));

        return false;
    },

    validatestarttime: function () {
        SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.ValidateTime(this.txtStartTime.value, Function.createDelegate(this, this.onstarttimecomplete),
            Function.createDelegate(this, this.onfailure));

        return false;
    },

    validateendtime: function () {
        SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.ValidateTime(this.txtEndTime.value, Function.createDelegate(this, this.onendtimecomplete),
            Function.createDelegate(this, this.onfailure));

        return false;
    },

    onendtimecomplete: function (result) {
        if (result == 'error') {
            Ext.MessageBox.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_72;E=js}',
                msg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_73;E=js}',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.Error
            });
            this.txtEndTime.value = '';
            this.txtEndTime.focus();
        } else {
            this.txtEndTime.value = result;
        }
    },

    onstarttimecomplete: function (result) {
        //Ext.msg.
        if (result == 'error') {
            Ext.MessageBox.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_72;E=js}',
                msg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_74;E=js}',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.Error
            });
            this.txtStartTime.value = '';
            this.txtStartTime.focus();
        } else {
            this.txtStartTime.value = result;
        }
    },

    oncomplete: function (result) {

        if (result != '') {
            Ext.MessageBox.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_72;E=js}',
                msg: result,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
            $('#' + this.btnSubmit.id).removeAttr('searchInProgress');
            return;
        }
        var callQueryObj = {};

        if (this.rbSearchCriteriaAND.checked) {
            callQueryObj.CriteriaCondition = 'AND';
        } else {
            callQueryObj.CriteriaCondition = 'OR';
        }
        var searchOriginCriteria = '';
        callQueryObj.CallOriginationCriteria = {};
        if (this.ddlCallOriginationType.options[this.ddlCallOriginationType.selectedIndex].value == 'PhoneNumber') {
            if (this.txtOriginIPAddressorPhone.value != 'IP address or phone #') {
                callQueryObj.CallOriginationCriteria.CallDevice = 1;
                callQueryObj.CallOriginationCriteria.Value = this.txtOriginIPAddressorPhone.value;
                searchOriginCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_127;E=js}', this.txtOriginIPAddressorPhone.value);
            } else {
                searchOriginCriteria = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_128;E=js}';
            }
        }

        if (this.ddlCallOriginationType.options[this.ddlCallOriginationType.selectedIndex].value == 'IPAddress') {

            callQueryObj.CallOriginationCriteria.CallDevice = 4;
            callQueryObj.CallOriginationCriteria.Value = this.txtOriginIPAddressorPhone.value;

            searchOriginCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_111;E=js}', String.format('<b>{0}</b>', this.txtOriginIPAddressorPhone.value));
        }

        if (this.ddlCallOriginationType.options[this.ddlCallOriginationType.selectedIndex].value == 'Location') {
            if (this.ddlOriginRegion.options[this.ddlOriginRegion.selectedIndex].value != '0') {
                callQueryObj.CallOriginationCriteria.CallDevice = 3;
                callQueryObj.CallOriginationCriteria.Value = this.ddlOriginRegion.options[this.ddlOriginRegion.selectedIndex].value;

                searchOriginCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_111;E=js}', String.format('<b>{0}</b>', this.ddlOriginRegion.options[this.ddlOriginRegion.selectedIndex].text));
            }
        }

        if (this.ddlCallOriginationType.options[this.ddlCallOriginationType.selectedIndex].value == 'Gateway') {
            if (this.ddlOriginGateway.options[this.ddlOriginGateway.selectedIndex].value != '0') {
                callQueryObj.CallOriginationCriteria.CallDevice = 2;
                callQueryObj.CallOriginationCriteria.Value = this.ddlOriginGateway.options[this.ddlOriginGateway.selectedIndex].value;
                searchOriginCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_111;E=js}', String.format('<b>{0}</b>', this.ddlOriginGateway.options[this.ddlOriginGateway.selectedIndex].text));
            }
        }

        var searchDestinationCriteria = '';
        callQueryObj.CallDestCriteria = {};

        if (this.ddlCallDestinationType.options[this.ddlCallDestinationType.selectedIndex].value == 'PhoneNumber') {
            if (this.txtDestinationIPAddressorPhone.value != 'IP address or phone #') {
                callQueryObj.CallDestCriteria.CallDevice = 1;
                callQueryObj.CallDestCriteria.Value = this.txtDestinationIPAddressorPhone.value;
                searchDestinationCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_112;E=js}', String.format('<b>+{0}</b>', this.txtDestinationIPAddressorPhone.value));
            } else {
                searchDestinationCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_112;E=js}', '<b>+</b>');
            }
        }

        if (this.ddlCallDestinationType.options[this.ddlCallDestinationType.selectedIndex].value == 'IPAddress') {
            callQueryObj.CallDestCriteria.CallDevice = 4;
            callQueryObj.CallDestCriteria.Value = this.txtDestinationIPAddressorPhone.value;

            searchDestinationCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_112;E=js}', String.format('<b>{0}</b>', this.txtDestinationIPAddressorPhone.value));
        }

        if (this.ddlCallDestinationType.options[this.ddlCallDestinationType.selectedIndex].value == 'Location') {
            if (this.ddlDestinationRegion.options[this.ddlDestinationRegion.selectedIndex].value != '0') {
                callQueryObj.CallDestCriteria.CallDevice = 3;
                callQueryObj.CallDestCriteria.Value = this.ddlDestinationRegion.options[this.ddlDestinationRegion.selectedIndex].value;

                searchDestinationCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_112;E=js}', String.format('<b>{0}</b>', this.ddlDestinationRegion.options[this.ddlDestinationRegion.selectedIndex].text));
            }
        }

        if (this.ddlCallDestinationType.options[this.ddlCallDestinationType.selectedIndex].value == 'Gateway') {
            if (this.ddlDestinationGateway.options[this.ddlDestinationGateway.selectedIndex].value != '0') {
                callQueryObj.CallDestCriteria.CallDevice = 2;
                callQueryObj.CallDestCriteria.Value = this.ddlDestinationGateway.options[this.ddlDestinationGateway.selectedIndex].value;
                searchDestinationCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_112;E=js}', String.format('<b>{0}</b>', this.ddlDestinationGateway.options[this.ddlDestinationGateway.selectedIndex].text));
            }
        }

        var searchCallManagerCriteria = '';
        var ccmObj = this.getselectedcmmobj();

        if (ccmObj.ccmType == 'ALL')
            callQueryObj.CcmVendor = 1;
        else if (ccmObj.ccmType == 'ALLCISCO')
            callQueryObj.CcmVendor = 2;
        else if (ccmObj.ccmType == 'ALLAVAYA')
            callQueryObj.CcmVendor = 3;
        else
            callQueryObj.CcmVendor = 0;
        
        if (ccmObj.ccmID == '0' || ccmObj.ccmID =='-1' || ccmObj.ccmID =='-2') {//all, all cisco, all avaya
            callQueryObj.CCMonitoringID = null;
        } else {
            searchCallManagerCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_113;E=js}', String.format('<b>{0}</b>', this.ddlCallManagers.options[this.ddlCallManagers.selectedIndex].text));

            callQueryObj.CCMonitoringID = ccmObj.ccmID;
        }

        var searchCallTimeCriteria = '';
        var callTime = this.ddlCallTime.options[this.ddlCallTime.selectedIndex].value;
        callQueryObj.CallTime = callTime;
        if (callTime == 'Between') {
            callQueryObj.PeriodBeginString = this.txtStartDate.value + ' ' + this.txtStartTime.value;
            callQueryObj.PeriodEndString = this.txtEndDate.value + ' ' + this.txtEndTime.value;

            searchCallTimeCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_114;E=js}', String.format('<b>{0}</b>', String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_115;E=js}', this.txtStartDate.value, this.txtStartTime.value, this.txtEndDate.value, this.txtEndTime.value)));
        } else {
            searchCallTimeCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_114;E=js}', String.format('<b>{0}</b>', this.ddlCallTime.options[this.ddlCallTime.selectedIndex].text));
        }

        var searchFailedCriteria = '';

        callQueryObj.IsFailedSelected = this.cbFailed.checked;
        callQueryObj.IsNoZeroLengthSelected = this.cbNoZeroLength.checked;
        if (callQueryObj.IsFailedSelected == true) {
            if (callQueryObj.IsNoZeroLengthSelected == true) {
                searchFailedCriteria = String.format('<b>{0}</b>', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_129;E=js}');
            }
            else {
                searchFailedCriteria = String.format('<b>{0}</b>', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_130;E=js}');
            }
        }
        else
            if (callQueryObj.IsNoZeroLengthSelected == true) {
                searchFailedCriteria = String.format('<b>{0}</b>', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_131;E=js}');
            }

        callQueryObj.IsSuccessfulSelected = this.cbSuccessful.checked;
        if (callQueryObj.IsSuccessfulSelected)
        	var searchIsSuccessfulCriteria = String.format('<b>{0}</b>', '@{R=VNQM.Strings;K=VNQMWEBJS_VK2_2;E=js}');

        callQueryObj.IsConferenceCallSelected = this.cbConferenceCall.checked;
        if (callQueryObj.IsConferenceCallSelected)
        	var searchIsConferenceCallCriteria = String.format('<b>{0}</b>', '@{R=VNQM.Strings;K=VNQMWEBJS_VK2_1;E=js}');
		
        callQueryObj.IsCallWithIssueSelected = this.cbCallWithIssue.checked;
        if (callQueryObj.IsCallWithIssueSelected)
        	var searchIsCallWithIssueCriteria = String.format('<b>{0}</b>', '@{R=VNQM.Strings;K=VNQMWEBJS_VK2_3;E=js}');
        
        var searchRedirectReasonCriteria = '';
        callQueryObj.IsRedirectReasonSelected = this.cbRedirectReason.checked && this.phCiscoRedirectReason.style.display!= "none";
        if (callQueryObj.IsRedirectReasonSelected == true) {
            if (this.ddlRedirectReason.options[this.ddlRedirectReason.selectedIndex].value != '-1') {
                callQueryObj.RedirectReason = this.ddlRedirectReason.options[this.ddlRedirectReason.selectedIndex].value;

                searchRedirectReasonCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_116;E=js}', String.format('<b>{0}</b>', this.ddlRedirectReason.options[this.ddlRedirectReason.selectedIndex].text));
            } else {
                callQueryObj.IsRedirectReasonSelected = false;
            }
        }

        var searchCallTerminationCauseCriteria = '';
        callQueryObj.IsCallTerminationCauseSelected = this.cbCallTerminationCause.checked && this.phCiscoCallTerminatationCause.style.display != "none";
        if (callQueryObj.IsCallTerminationCauseSelected == true) {
            if (this.ddlCallTerminationCause.options[this.ddlCallTerminationCause.selectedIndex].value != '-1') {
                callQueryObj.CallTerminationCause = this.ddlCallTerminationCause.options[this.ddlCallTerminationCause.selectedIndex].value;

                searchCallTerminationCauseCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_117;E=js}', String.format('<b>{0}</b>', this.ddlCallTerminationCause.options[this.ddlCallTerminationCause.selectedIndex].text));
            } else {
                callQueryObj.IsCallTerminationCauseSelected = false;
            }
        }

        var searchConditionCodeCriteria = '';
        callQueryObj.IsAvayaCondCodeSelected = this.cbAvayaCondCode.checked && this.phAvayaCondCode.style.display != "none";
        if (callQueryObj.IsAvayaCondCodeSelected == true) {
            if (this.ddlAvayaCondCode.options[this.ddlAvayaCondCode.selectedIndex].value != '-1') {
                callQueryObj.AvayaCondCode = this.ddlAvayaCondCode.options[this.ddlAvayaCondCode.selectedIndex].value;
                searchConditionCodeCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_143;E=js}', String.format('<b>{0}</b>', this.ddlAvayaCondCode.options[this.ddlAvayaCondCode.selectedIndex].text));
            } else {
                callQueryObj.IsAvayaCondCodeSelected = false;
            }
        }

        var searchMOSCriteria = '';
        if (this.cbMOS.checked && this.phMos.style.display!= "none") {
            if (this.ddlMOS.options[this.ddlMOS.selectedIndex].value != '-All-') {
                callQueryObj.CallQualityMosValue = new Object();
                var mosObj = new Object();
                if (this.ddlMOS.options[this.ddlMOS.selectedIndex].value == 'Between') {
                    searchMOSCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_118;E=js}', String.format('<b>{0}</b>', String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_119;E=js}', this.txtMOSLimit1.value, this.txtMOSLimit2.value)));
                    mosObj.CallQualityVal = 5;
                    mosObj.CallQualityBegin = this.txtMOSLimit1.value;
                    mosObj.CallQualityEnd = this.txtMOSLimit2.value;
                } else {
                    searchMOSCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_118;E=js}', String.format('<b>{0}</b>', this.ddlMOS.options[this.ddlMOS.selectedIndex].text));
                    mosObj.CallQualityVal = this.ddlMOS.options[this.ddlMOS.selectedIndex].value;
                }
                callQueryObj.CallQualityMosValue = mosObj;
            }
        }

        var searchLatencyCriteria = '';

        if (this.cbLatency.checked && this.phLatency.style.display != "none") {
            if (this.ddlLatency.options[this.ddlLatency.selectedIndex].value != '-All-') {
                callQueryObj.CallQualityLatencyValue = new Object();
                var latencyObj = new Object();
                if (this.ddlLatency.options[this.ddlLatency.selectedIndex].value == 'Between') {
                    searchLatencyCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_120;E=js}', String.format('<b>{0}</b>', String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_121;E=js}', this.txtLatencyLimit1.value, this.txtLatencyLimit2.value)));
                    latencyObj.CallQualityVal = 5;
                    latencyObj.CallQualityBegin = this.txtLatencyLimit1.value;
                    latencyObj.CallQualityEnd = this.txtLatencyLimit2.value;
                } else {
                    searchLatencyCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_120;E=js}', String.format('<b>{0}</b>', this.ddlLatency.options[this.ddlLatency.selectedIndex].text));
                    latencyObj.CallQualityVal = this.ddlLatency.options[this.ddlLatency.selectedIndex].value;
                }
                callQueryObj.CallQualityLatencyValue = latencyObj;
            }
        }

        var searchJitterCriteria = '';

        if (this.cbJitter.checked) {
            if (this.ddlJitter.options[this.ddlJitter.selectedIndex].value != '-All-') {
                callQueryObj.CallQualityJitterValue = new Object();
                var jitterObj = new Object();
                if (this.ddlJitter.options[this.ddlJitter.selectedIndex].value == 'Between') {
                    searchJitterCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_122;E=js}', String.format('<b>{0}</b>', String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_121;E=js}', this.txtJitterLimit1.value, this.txtJitterLimit2.value)));
                    jitterObj.CallQualityVal = 5;
                    jitterObj.CallQualityBegin = this.txtJitterLimit1.value;
                    jitterObj.CallQualityEnd = this.txtJitterLimit2.value;
                } else {
                    searchJitterCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_122;E=js}', String.format('<b>{0}</b>', this.ddlJitter.options[this.ddlJitter.selectedIndex].text));
                    jitterObj.CallQualityVal = this.ddlJitter.options[this.ddlJitter.selectedIndex].value;
                }
                callQueryObj.CallQualityJitterValue = jitterObj;
            }
        }

        var searchPacketLossCriteria = '';

        if (this.cbPacketLoss.checked) {
            if (this.ddlPacketLoss.options[this.ddlPacketLoss.selectedIndex].value != '-All-') {
                callQueryObj.CallQualityPacketLostValue = new Object();
                var packetLossObj = new Object();
                if (this.ddlPacketLoss.options[this.ddlPacketLoss.selectedIndex].value == 'Between') {
                    searchPacketLossCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_123;E=js}', String.format('<b>{0}</b>', String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_124;E=js}', this.txtPacketLossLimit1.value, this.txtPacketLossLimit2.value)));
                    packetLossObj.CallQualityVal = 5;
                    packetLossObj.CallQualityBegin = this.txtPacketLossLimit1.value;
                    packetLossObj.CallQualityEnd = this.txtPacketLossLimit2.value;
                } else {
                    searchPacketLossCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_123;E=js}', String.format('<b>{0}</b>', this.ddlPacketLoss.options[this.ddlPacketLoss.selectedIndex].text));
                    packetLossObj.CallQualityVal = this.ddlPacketLoss.options[this.ddlPacketLoss.selectedIndex].value;
                }

                callQueryObj.CallQualityPacketLostValue = packetLossObj;
            }
        }
        var searchIncomingCallProtocolCriteria = '';

        if (this.cbIncomingCallProtocol.checked) {
            if (this.ddlIncomingCallProtocol.options[this.ddlIncomingCallProtocol.selectedIndex].value !== '-All-') {
                callQueryObj.IncomingCallProtocol = this.ddlIncomingCallProtocol.options[this.ddlIncomingCallProtocol.selectedIndex].value;
                searchIncomingCallProtocolCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_BO_IncomingCallProtocol;E=js} {0}', String.format('<b>{0}</b>', callQueryObj.IncomingCallProtocol));
            } else {
                callQueryObj.IncomingCallProtocol = "All";
            }
        } else {
            callQueryObj.IncomingCallProtocol = "All";
        }
    
        var searchOutgoingCallProtocolCriteria = '';

        if (this.cbOutgoingCallProtocol.checked) {
            if (this.ddlOutgoingCallProtocol.options[this.ddlOutgoingCallProtocol.selectedIndex].value !== '-All-') {
                callQueryObj.OutgoingCallProtocol = this.ddlOutgoingCallProtocol.options[this.ddlOutgoingCallProtocol.selectedIndex].value;
                searchOutgoingCallProtocolCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_BO_OutgoingCallProtocol;E=js} {0}', String.format('<b>{0}</b>', callQueryObj.OutgoingCallProtocol));
            } else {
                callQueryObj.OutgoingCallProtocol = "All";
            }
        } else {
            callQueryObj.OutgoingCallProtocol = "All";
        }
        var searchCriteria = searchOriginCriteria;

        if (searchDestinationCriteria.length > 0) {
            if (searchCriteria.length > 0) {
                searchCriteria = searchCriteria + ', ' + searchDestinationCriteria;
            }
            else {
                searchCriteria = searchDestinationCriteria;
            }
        }

        if (searchCallManagerCriteria.length > 0) {
            if (searchCriteria.length > 0) {
                searchCriteria = searchCriteria + ', ' + searchCallManagerCriteria;
            }
            else {
                searchCriteria = searchCallManagerCriteria;
            }
        }

        if (searchCallTimeCriteria.length > 0) {
            if (searchCriteria.length > 0) {
                searchCriteria = searchCriteria + ', ' + searchCallTimeCriteria;
            }
            else {
                searchCriteria = searchCallTimeCriteria;
            }
        }

        var searchCallStatusCriteria = addCallStatusCondition('', searchFailedCriteria);
        searchCallStatusCriteria = addCallStatusCondition(searchCallStatusCriteria, searchIsConferenceCallCriteria);
        searchCallStatusCriteria = addCallStatusCondition(searchCallStatusCriteria, searchIsSuccessfulCriteria);
        searchCallStatusCriteria = addCallStatusCondition(searchCallStatusCriteria, searchIsCallWithIssueCriteria);

        if (searchRedirectReasonCriteria.length > 0) {
            if (searchCallStatusCriteria.length > 0) {
                searchCallStatusCriteria = searchCallStatusCriteria + ', ' + searchRedirectReasonCriteria;
            } else {
                searchCallStatusCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_125;E=js}', searchRedirectReasonCriteria);
            }
        }

        if (searchCallTerminationCauseCriteria.length > 0) {
            if (searchCallStatusCriteria.length > 0) {
                searchCallStatusCriteria = searchCallStatusCriteria + ', ' + searchCallTerminationCauseCriteria;
            } else {
                searchCallStatusCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_125;E=js}', searchCallTerminationCauseCriteria);
            }
        }

        if (searchConditionCodeCriteria.length > 0) {
            if (searchCallStatusCriteria.length > 0) {
                searchCallStatusCriteria = searchCallStatusCriteria + ', ' + searchConditionCodeCriteria;
            } else {
                searchCallStatusCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_125;E=js}', searchConditionCodeCriteria);
            }
        }

        if (searchCallStatusCriteria.length > 0) {
            if (searchCriteria.length > 0) {
                searchCriteria = searchCriteria + ', ' + searchCallStatusCriteria;
            } else {
                searchCriteria = searchCallStatusCriteria;
            }
        }

        var searchCallQualityCriteria = '';

        if (searchMOSCriteria.length > 0) {
            searchCallQualityCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_126;E=js}', searchMOSCriteria);
        }

        if (searchLatencyCriteria.length > 0) {
            if (searchCallQualityCriteria.length > 0) {
                searchCallQualityCriteria = searchCallQualityCriteria + ', ' + searchLatencyCriteria;
            } else {
                searchCallQualityCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_126;E=js}' + searchLatencyCriteria);
            }
        }

        if (searchJitterCriteria.length > 0) {
            if (searchCallQualityCriteria.length > 0) {
                searchCallQualityCriteria = searchCallQualityCriteria + ', ' + searchJitterCriteria;
            } else {
                searchCallQualityCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_126;E=js}', searchJitterCriteria);
            }
        }

        if (searchPacketLossCriteria.length > 0) {
            if (searchCallQualityCriteria.length > 0) {
                searchCallQualityCriteria = searchCallQualityCriteria + ', ' + searchPacketLossCriteria;
            } else {
                searchCallQualityCriteria = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_126;E=js}', searchPacketLossCriteria);
            }
        }
        if (searchIncomingCallProtocolCriteria.length > 0) {
            if (searchCriteria.length > 0) {
                searchCriteria = searchCriteria + ', ' + searchIncomingCallProtocolCriteria;
            } else {
                searchCriteria = searchIncomingCallProtocolCriteria;
            }
        }
        
        if (searchOutgoingCallProtocolCriteria.length > 0) {
            if (searchCriteria.length > 0) {
                searchCriteria = searchCriteria + ', ' + searchOutgoingCallProtocolCriteria;
            } else {
                searchCriteria = searchOutgoingCallProtocolCriteria;
            }
        }

        if (searchCallQualityCriteria.length > 0) {
            if (searchCriteria.length > 0) {
                searchCriteria = searchCriteria + ', ' + searchCallQualityCriteria;
            } else {
                searchCriteria = searchCallQualityCriteria;
            }
        }
        this.lblSearchCriteria.innerHTML = searchCriteria;

        this.resultsControl.Search(callQueryObj);
    },

    getUrlVars: function () {
        var vars = [], hash;

        var querystring = location.search;
        if (!querystring) {
            return null;
        }
        var hashes = querystring.substring(1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },

    onfailure: function (error, context) {
        if (!this.checkSessionExpired(error)) {
            Ext.MessageBox.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_72;E=js}',
                msg: error.get_message(),
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    },

    checkSessionExpired: function (error) {
        if (error.get_statusCode() == 401 || error.get_statusCode() == 403) {
            alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_105;E=js}');
            window.location.reload();
            return true;
        } else {
            return false;
        }
    },

    initialize: function () {
        
        Orion.Voip.Controls.SearchVoipCalls.callBaseMethod(this, 'initialize');
        // Add custom initialization here
        Ext.QuickTips.init();

        this.updateVoipSearchFields('ALL');
        
        $(document).ready(Function.createDelegate(this, function () {
            var allVars = this.getUrlVars();
            if (allVars != null) {
                this.rbSearchCriteriaAND.checked = true;
                if (typeof (allVars['OrgPhoneNumber']) != 'undefined') {
                    this.ddlCallOriginationType.value = 'PhoneNumber';
                    this.origintypechanged();
                    if (allVars['OrgPhoneNumber'] != '') {
                        this.txtOriginIPAddressorPhone.value = decodeURIComponent(allVars['OrgPhoneNumber']);
                    }
                }
                else if (typeof (allVars['OrgIPAddress']) != 'undefined') {
                    this.ddlCallOriginationType.value = 'IPAddress';
                    this.origintypechanged();
                    this.txtOriginIPAddressorPhone.value = decodeURIComponent(allVars['OrgIPAddress']);
                } else if (typeof (allVars['OrgLocation']) != 'undefined') {
                    this.ddlCallOriginationType.value = 'Location';
                    this.origintypechanged();
                    this.ddlOriginRegion.value = decodeURIComponent(allVars['OrgLocation']);
                } else if (typeof (allVars['OrgGateway']) != 'undefined') {
                    this.ddlCallOriginationType.value = 'Gateway';
                    this.origintypechanged();
                    this.ddlOriginGateway.value = decodeURIComponent(allVars['OrgGateway']);
                }

                if (typeof (allVars['DestPhoneNumber']) != 'undefined') {
                    this.ddlCallDestinationType.value = 'PhoneNumber';
                    this.destinationtypechanged();
                    if (allVars['DestPhoneNumber'] != '') {
                        this.txtDestinationIPAddressorPhone.value = decodeURIComponent(allVars['DestPhoneNumber']);
                    }
                } else if (typeof (allVars['DestIPAddress']) != 'undefined') {
                    this.ddlCallDestinationType.value = 'IPAddress';
                    this.destinationtypechanged();
                    this.txtDestinationIPAddressorPhone.value = decodeURIComponent(allVars['DestIPAddress']);
                } else if (typeof (allVars['DestLocation']) != 'undefined') {
                    this.ddlCallDestinationType.value = 'Location';
                    this.destinationtypechanged();
                    this.ddlDestinationRegion.value = decodeURIComponent(allVars['DestLocation']);
                } else if (typeof (allVars['DestGateway']) != 'undefined') {
                    this.ddlCallDestinationType.value = 'Gateway';
                    this.destinationtypechanged();
                    this.ddlDestinationGateway.value = decodeURIComponent(allVars['DestGateway']);
                }

                this.ddlCallTime.value = allVars['CallTime'];

                if (typeof (allVars['CallFailed']) != 'undefined') {
                    this.cbFailed.checked = allVars['CallFailed'];
                }

                if (typeof (allVars['MOS']) != 'undefined') {
                    this.cbMOS.checked = true;
                    this.ddlMOS.value = allVars['MOS'];
                }
                if (typeof (allVars['Jitter']) != 'undefined') {
                    this.cbJitter.checked = true;
                    this.ddlJitter.value = allVars['Jitter'];
                }
                if (typeof (allVars['Latency']) != 'undefined') {
                    this.cbLatency.checked = true;
                    this.ddlLatency.value = allVars['Latency'];
                }
                if (typeof (allVars['PacketLoss']) != 'undefined') {
                    this.cbPacketLoss.checked = true;
                    this.ddlPacketLoss.value = allVars['PacketLoss'];
                }

                if (typeof (allVars['useOR']) != 'undefined') {
                    this.rbSearchCriteriaAND.checked = false;
                    this.rbSearchCriteriaOR.checked = true;
                } else {
                    this.rbSearchCriteriaAND.checked = true;
                    this.rbSearchCriteriaOR.checked = false;
                }
                this.oncomplete('');
            }
        }));

    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Controls.SearchVoipCalls.callBaseMethod(this, 'dispose');
    }
};

Orion.Voip.Controls.SearchVoipCalls.registerClass('Orion.Voip.Controls.SearchVoipCalls', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();

function addCallStatusCondition(callStatus, newCondition) {
	if (newCondition == null || newCondition.length == 0)
		return callStatus;

	if (callStatus.length === 0)
		return String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_125;E=js}', newCondition);
	else
		return callStatus + ', ' + newCondition;
}
