<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Filters" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedNodeFilter.ascx.cs"
    Inherits="Orion_Voip_Controls_AdvancedNodeFilter" %>
<%@ Register Src="~/Orion/Voip/Controls/FiltersBox.ascx" TagPrefix="voip" TagName="FiltersBox" %>
<%@ Register Src="~/Orion/Voip/Controls/BasicNodeFilterMaker.ascx" TagPrefix="voip"
    TagName="NodeFilterMaker" %>

<div id="AdvancedNodeFilterContent" runat="server"  class="AdvancedNodeFilter">
<div>

<asp:UpdatePanel ID="UP1" runat="server">
    <ContentTemplate>
        <div style="width: 100%">
            <h1><%= FilterTitle %> </h1>
            Add conditions to condition  set(s) below.
            <br /><br />
            <table>
                <tr>
                    <td>
                        <voip:NodeFilterMaker ID="NFM" runat="server" />
                    </td>
                    <td style="vertical-align: top; padding-top: 27px">
                        <asp:ImageButton runat="server" ID="btnAddFilter" OnClick="btnAddFilter_Click" ImageUrl="/Orion/Voip/images/button_addcondition.gif"
                            ToolTip="Add the displayed condition to this set"  ValidationGroup="AdvancedNodeFilter" />
                    </td>
                 </tr>
            </table>
            <br />
            <asp:CustomValidator ID="CV1" runat="server" ControlToValidate="NFM" OnServerValidate="ServerValidate" ValidationGroup="AdvancedNodeFilter"
                ErrorMessage="Please select Field for displayed condition at first." Font-Bold="true"
                Font-Size="Small" />
            <br />
            <div class="FilterGroupLowerBody">
                <asp:Repeater ID="OrFiltersRepeater" runat="server" DataSource='<%#FilterCollection.Filters %>' OnItemDataBound="OrFiltersRepeater_ItemDataBound">
                    <ItemTemplate>
                         <div class="FilterGroupMain">
                            <div class="FilterGroupHeader">
                            Condition Set
                            </div>
                            <div  class="FilterGroupBody">
                                <asp:ImageButton runat="server" ID="btnDeleteFilterGroup" OnClick="btnDeleteFilterGroup_Click"
                                    ImageUrl="/Orion/Voip/images/icon_delete.gif" Visible='<%#this.FilterCollection.Filters.Count != 1 %>'
                                    ToolTip="Delete this condition set" CausesValidation="false" CssClass="FilterGroupButton" />
                                <asp:LinkButton runat="server" ID="linkDeleteFilterGroup" OnClick="btnDeleteFilterGroup_Click" ToolTip="Remove this condition set" CausesValidation="false" CssClass="FilterGroupText">
                                    <%#this.FilterCollection.Filters.Count != 1 ? "Remove Condition Set" : "" %>
                                </asp:LinkButton>
                                &nbsp;&nbsp;
                                <asp:ImageButton runat="server" ID="btnAddFilterGroup" OnClick="btnAddFilterGroup_Click"
                                    ImageUrl="~/Orion/Voip/images/icon_add.gif" ToolTip="Add a new condition set"
                                    CausesValidation="false" CssClass="FilterGroupButton" />
                                <asp:LinkButton  runat="server" ID="linkAddFilterGroup" OnClick="btnAddFilterGroup_Click" ToolTip="Add a new condition set" CausesValidation="false" CssClass="FilterGroupText">
                                &nbsp;Add New Condition Set
                                </asp:LinkButton>
                            </div>
                            <div class="FilterGroupSpacer" ></div>
                            <br />
                            <div class="FiltersBox">
                                <voip:FiltersBox ID="fb" runat="server" AndFILTER='<%#(ANDFilter)Container.DataItem %>'
                                    OnDeleteFilter="btnDeleteFilter_Click" EXTItemIndex='<%#Container.ItemIndex %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                    <SeparatorTemplate>
                        <div class="FilterGroupSpacer" ></div>
                        <br />
                    </SeparatorTemplate>
                </asp:Repeater>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

</div>
<br />
      <div align="center">
              <asp:ImageButton ID="FilterOk"  runat="server" ImageUrl="~/Orion/images/Button.Submit.Primary.gif" OnClick="FilterOk_Click" />
              <asp:ImageButton ID="FilterCancel" runat="server" ImageUrl="~/Orion/Voip/images/Button.Close.gif" OnClick="FilterCancel_Click" />
      </div>
</div>
