using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.Web;

public partial class Orion_Voip_Controls_OperationTypeSelection : System.Web.UI.UserControl
{
    public ResourceInfo Resource { get; set; }

    public bool CheckedByDefault { get; set; }

    public bool AutoPostBack { get; set; }

    public List<OperationType> SelectedTypes
    {
        get { return GetSelectedTypes(); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.OperationTypeFilter.AutoPostBack = this.AutoPostBack;
        
        if (!this.IsPostBack)
        {
            Initialize(); 
        }
    }

    public void Initialize()
    {
        GenerateOperationTypes(OperationTypeFilter);

        this.Resource.LoadEnumSelection<OperationType>(OperationTypeFilter, ResourcePropertiesHelper.OperationTypePrefix);
    }

    private List<OperationType> GetSelectedTypes()
    {
        List<OperationType> list = new List<OperationType>();

        foreach (ListItem item in OperationTypeFilter.Items)
        {
            if (item.Selected && Enum.IsDefined(typeof(OperationType), item.Value))
            {
                OperationType type = (OperationType)Enum.Parse(typeof(OperationType), item.Value);
                list.Add(type);
            }
        }

        return list;
    }

    public void SaveChanges()
    {
        ResourcePropertiesHelper.SaveEnumSelection<OperationType>(this.Resource, OperationTypeFilter, ResourcePropertiesHelper.OperationTypePrefix);
    }

    void GenerateOperationTypes(CheckBoxList checkBoxList)
    {
        checkBoxList.Items.Clear();

        foreach (object value in IpSlaOperationHelper.GetSupportedOperationTypes())
        {
            ListItem item = new ListItem(GetLocalizedProperty("OperationType", value.ToString()), value.ToString());
            item.Selected = this.CheckedByDefault;
            if (checkBoxList.Items.FindByText(item.Text) == null) checkBoxList.Items.Add(item);
        }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
