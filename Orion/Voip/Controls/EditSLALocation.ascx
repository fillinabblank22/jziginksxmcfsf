﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditSLALocation.ascx.cs" Inherits="Orion_Voip_Controls_EditSLALocation" %>
<%@ Import Namespace="Resources" %>

<table style="background-color: white; height: 152px" border="0" cellpadding="0" cellspacing="0" width="514px;">
    <tr>
        <td style="padding-left: 10px; padding-top: 10px; height:40px;" colspan="3">
            <%= VNQMWebContent.VNQMWEBDATA_TM0_53 %>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 10px;">
            <div id="SourceDiv" runat="server" style="padding-bottom: 10px">
                <span class="voipSearchFont" style="width: 180px"><b><%= VNQMWebContent.VNQMWEBDATA_TM0_54 %></b></span>
                <asp:DropDownList runat="server" ID="ddlOriginRegion" Width="160px"></asp:DropDownList>
            </div>
            <div id="TargetDiv" runat="server">
                <span class="voipSearchFont" style="width: 180px"><b><%= VNQMWebContent.VNQMWEBDATA_TM0_55 %></b></span>
                <asp:DropDownList runat="server" ID="ddlTargetRegion" Width="160px"></asp:DropDownList>
            </div>
        </td>
    </tr>
</table>





