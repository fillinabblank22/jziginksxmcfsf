﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Controls_RenderableSmallStatusIcon : System.Web.UI.UserControl, IRenderable<Status>
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region IRenderable<Status> Members

    void IRenderable<Status>.SetRenderableData(Status data)
    {
        this.SmallStatusIconToRender.StatusValue = data;
    }

    #endregion
}
