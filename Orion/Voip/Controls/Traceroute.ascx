﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Traceroute.ascx.cs" Inherits="Orion_Voip_Controls_Traceroute" %>
<%@ Register TagPrefix="ipsla" Namespace="SolarWinds.Orion.IpSla.Web.UI.Grid" Assembly="SolarWinds.Orion.IpSla.Web" %>

<div style="margin-bottom: 6px;">
    <span id="HopCount" runat="server" />
</div>

<div style="margin-bottom: 4px;">
    <div style="font-size: 8pt;">
        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_188 %>
    </div>
    <div style="font-size: 8pt;">
        <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_189, "<b><i>", "</i></b>")%>
    </div>
</div>

<ipsla:ExpandableGrid ID="ExpandableGrid" runat="server" ItemCssStyle="padding:5px 2px 5px 0px;">
    <Columns>
        <ipsla:Column Caption="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_190 %>" HeaderCssClass="ReportHeader"
                HeaderCssStyle="padding:2px 2px 2px 0px;vertical-align:middle;"
                ItemCssStyle="padding:2px 2px 2px 0px;vertical-align:middle;">
            <Template>
                <div>
                    <% if (!DisableHopHyperlinks){ %><a href="<%# GetHopUrl(Container.DataItem) %>"><% } %>
                        <img alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_190 %>" src="/Orion/Voip/images/OperationTypeIcons/PathHop_16.gif" style="vertical-align: middle; padding: 1px 1px 1px 1px;" />&nbsp;<span style="vertical-align: middle;"><%# String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_191, GetHopIndex(Container.DataItem), GetHopName(Container.DataItem))%></span></div>
                    <% if (!DisableHopHyperlinks){ %></a><% } %>
                </div>
            </Template>
        </ipsla:Column>
    </Columns>
</ipsla:ExpandableGrid>
