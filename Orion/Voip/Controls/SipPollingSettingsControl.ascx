﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SipPollingSettingsControl.ascx.cs" Inherits="Orion_Voip_Controls_SipPollingSettingsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script type="text/javascript">
    function sipTrunkMonitoringEnabledCheckBoxChanged() {
        var reqSipTrunkFrequencyValidator = document.getElementById("reqSipTrunkFrequency");
        if ($('#cbSipTrunkMonitoringEnabled').is(":checked")) {
            $("#trSipTrunkFrequency").css("visibility", "visible");
            if ($("#txtSipTrunkFrequency").val().length == 0) {
                $("#txtSipTrunkFrequency").val('5');
            }
            ValidatorEnable(reqSipTrunkFrequencyValidator, true);
        } else {
            $("#trSipTrunkFrequency").css("visibility", "hidden");
            ValidatorEnable(reqSipTrunkFrequencyValidator, false);
        }
    }
</script>
<link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />

<div id="divSipPollingSettingsControl" runat="server" ClientIDMode="Static">
    <table class="voipSipTrunkPollingSettingsTable">
        <tr>
            <td class="leftLabelColumn">
                <div class="sw-voip-edit-node-section-header"><%= SipPollingSettingsHeader %></div>
            </td>
            <td class="voipSipRightColumn">
                 <asp:CheckBox ID="cbSipTrunkMonitoringEnabled" ClientIDMode="Static" runat="server" onchange="sipTrunkMonitoringEnabledCheckBoxChanged()" />
                 <%=Resources.VNQMWebContent.VNQMWEB_BO_Enable_SipTrunk_Polling%>
            </td>
        </tr>
        <tr id="trSipTrunkFrequency" runat="server" ClientIDMode="Static">
            <td class="leftLabelColumn">
                <%=Resources.VNQMWebContent.VNQMWEB_BO_SipTrunk_ScanEvery%>:
            </td>
            <td class="voipSipRightColumn">
                <asp:TextBox ID="txtSipTrunkFrequency" ClientIDMode="Static" Text="5" CssClass="sipTrunkPollingFrequencyTextBox" Width="15%" runat="server"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="txtSipTrunkFrequencyValidator" runat="server" TargetControlID="txtSipTrunkFrequency" FilterType="Numbers" />
                <%=Resources.VNQMWebContent.VNQMWEBCODE_VB1_133%>
                <asp:RequiredFieldValidator runat="server" ClientIDMode="Static" ID="reqSipTrunkFrequency" ControlToValidate="txtSipTrunkFrequency" ErrorMessage="This value must be filled-in." />
            </td>
        </tr>
    </table>
</div>