﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationTabBar.ascx.cs" Inherits="Orion_Voip_Controls_NavigationTabBar" %>

<div class="sw-tabs"><div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">

<div class="x-tab-strip-wrap">
  <%-- 
  <div class="x-tab-strip-spacer" style="display: none;"><!-- --></div>
  --%>

  <ul class="x-tab-strip x-tab-strip-top">

    <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />

    <li class="x-tab-edge"></li>
    <div class="x-clear"><!-- --></div>
  </ul>

  <div class="x-tab-strip-spacer" style="border-bottom: none;"><!-- --></div>
</div>

</div>
</div>