<%@ WebService Language="C#" Class="Orion.Voip.Controls.CliConnectionControlService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Admin.Settings;

namespace Orion.Voip.Controls
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class CliConnectionControlService : WebService
    {
        private static readonly Log log = new Log();

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetCredentials()
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                IEnumerable<CliCiscoCredentialsPublic> credentialsList;
                using (var blProxy = BusinessLayer.GetBusinessLayer())
                {
                    credentialsList = blProxy.GetCliCiscoCredentials();
                }

                var list = (
                    from credentials in credentialsList
                    select new
                        {
                            id = credentials.Id,
                            name = credentials.Name,
                            user = credentials.Username,
                            level = NodePropertiesHelper.TransformEnableLevelValuesToClientSideRepresentation(
                                credentials.EnableLevel, 
                                credentials.EnableModeAvailable),
                        }
                    ).ToList();

                log.InfoFormat("CLI credentials retrieved. [Count={0}]", list.Count);
                return list;
            }
            catch (Exception ex)
            {
                log.Error("Error while getting CLI credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        public object ValidateCliConnectionCredentialId(
            int? engineId, string ip,
            int credentialId,
            CliConnectionProtocol protocol, int port, int timeout, bool useNewCliComponent)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                IPAddress ipAddress;
                if (!IPAddress.TryParse(ip, out ipAddress))
                    throw new ApplicationException("Node IP address has invalid format.");

                using (var blProxy = BusinessLayer.GetBusinessLayer())
                {
                    return blProxy.ValidateCliConnectionInfo(engineId, ipAddress,
                        credentialId,
                        protocol, port, timeout, useNewCliComponent);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while validating CLI credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        public object ValidateCliConnectionCredentials(
            int? engineId, string ip,
            string username, string password, string enablePassword, int enableLevel,
            CliConnectionProtocol protocol, int port, int timeout, bool useNewCliComponent)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                IPAddress ipAddress;
                if (!IPAddress.TryParse(ip, out ipAddress))
                    throw new ApplicationException("Node IP address has invalid format.");
                int? enableLevelServer;
                string enablePasswordServer;
                NodePropertiesHelper.TransformEnableLevelValuesToServerSideRepresentation(
                    enableLevel, enablePassword,
                    out enableLevelServer, out enablePasswordServer);
                using (var blProxy = BusinessLayer.GetBusinessLayer())
                {
                    return blProxy.ValidateCliConnectionInfo(engineId, ipAddress,
                        username, password, enablePasswordServer, enableLevelServer,
                        protocol, port, timeout, useNewCliComponent);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while validating CLI credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetCommonCliCredentials(IEnumerable<int> nodeIds)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();

            int? credentialId, port, timeout;
            CliConnectionProtocol? protocol;
            using (var blProxy = BusinessLayer.GetBusinessLayer())
            {
                var en = nodeIds.GetEnumerator();
                if (!en.MoveNext())
                    return null;

                if (!blProxy.GetCliConnectionInfo(en.Current, out credentialId, out protocol, out port, out timeout))
                    throw new ApplicationException("Error reading node properties.");

                while (en.MoveNext())
                {
                    int? tempCredentialId, tempPort, tempTimeout;
                    CliConnectionProtocol? tempProtocol;

                    if (!blProxy.GetCliConnectionInfo(en.Current, out tempCredentialId, out tempProtocol, out tempPort, out tempTimeout))
                        throw new ApplicationException("Error reading node properties.");

                    if (credentialId != tempCredentialId) credentialId = null;
                    if (port != tempPort) port = null;
                    if (timeout != tempTimeout) timeout = null;
                    if (protocol != tempProtocol) protocol = null;
                }
            }

            return new
                {
                    CredentialId = credentialId,
                    Port = port,
                    Timeout = timeout,
                    Protocol = protocol.HasValue ? protocol.Value.ToString() : null,
                };
        }
    }
}
