using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Voip_Controls_Cli_CliConnectionControl : ScriptUserControlBase
{
    public bool IsFipsEnabledOnAnyEngine
    {
        get
        {
            return EnginesDAL.IsFIPSModeEnabledOnAnyEngine();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.tbPort.Text = "22";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.btnValidate.Attributes["onclick"] = "$find('" + this.ClientID + "').btnValidate_Click(); return false;";
        this.btnAdvanced.ElementID = this.advancedBody.ClientID;
        this.btnAdvanced.OnClientExpanded = "$find('" + this.ClientID + "').btnAdvanced_Expanded();";
    }

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display: none;");
        writer.RenderBeginTag(HtmlTextWriterTag.Tbody);
    }

    protected override void RenderWrapperElementWithContent(HtmlTextWriter writer)
    {
        this.RenderWrapperElementBegin(writer);
        this.RenderWrapperElementEnd(writer);
        this.RenderContent(writer);
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.CliConnectionControl", this.ClientID);
        descriptor.AddElementProperty("ddlSavedCredentials", this.ddlSavedCredentials.ClientID);
        descriptor.AddElementProperty("tbUsername", this.tbUsername.ClientID);
        descriptor.AddElementProperty("tbPassword", this.tbPassword.ClientID);
        descriptor.AddElementProperty("tbEnablePassword", this.tbEnablePassword.ClientID);
        descriptor.AddElementProperty("ddlEnableLevel", this.ddlEnableLevel.ClientID);
        descriptor.AddElementProperty("ddlProtocol", this.ddlProtocol.ClientID);
        descriptor.AddElementProperty("tbPort", this.tbPort.ClientID);
        descriptor.AddElementProperty("tbTimeout", this.tbTimeout.ClientID);
        descriptor.AddComponentProperty("testResult", this.testResult.ClientID);
        descriptor.AddElementProperty("btnValidate", this.btnValidate.ClientID);
        descriptor.AddElementProperty("validationProgress", this.validationProgress.ClientID);
        descriptor.AddProperty("IsFipsEnabledOnAnyEngine", this.IsFipsEnabledOnAnyEngine);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("CliConnectionControl.js"));
    }

    #endregion
}
