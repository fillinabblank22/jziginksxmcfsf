<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CliConnectionControl.ascx.cs" Inherits="Orion_Voip_Controls_Cli_CliConnectionControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web"%>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>
<%@ Register TagPrefix="ipsla" Namespace="SolarWinds.Orion.IpSla.Web.UI" Assembly="SolarWinds.Orion.IpSla.Web" %>
<%@ Register TagPrefix="ipsla" TagName="TestResultControl" Src="~/Orion/Voip/Controls/TestResultControl.ascx" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
  <Services>
    <asp:ServiceReference Path="~/Orion/Voip/Controls/Cli/CliConnectionControl.asmx" />
    <asp:ServiceReference Path="~/Orion/Voip/Services/NodePropertiesService.asmx" />
  </Services>
</asp:ScriptManagerProxy>

<tbody>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_32 %>
        </td>
        <td colspan="9" class="SavedCredentialCls">
            <asp:TextBox ID="ddlSavedCredentials" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_33 %>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbUsername" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_34 %>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_35 %>
        </td>
        <td colspan="8" class="EnableLevelCls">
            <asp:TextBox ID="ddlEnableLevel" runat="server"></asp:TextBox>
        </td>
        <td style="word-wrap: break-word; white-space: normal">
            <a href="<%= HelpHelper.GetHelpUrl(HelpFragments.CliEnableLevelRequirements) %>" target="_blank" style="font-size: smaller;" class="sw-link"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_36 %></a>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_37%>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbEnablePassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label">
            <div style="padding-top: 5px; padding-bottom: 5px;">
                <ipsla:CollapseElement ID="btnAdvanced" runat="server" Collapsed="true">
                    <Handle><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_15%><span style="visibility: hidden;">:</span></Handle>
                </ipsla:CollapseElement>
            </div>
        </td>
        <td colspan="9"></td>
    </tr>
</tbody>
<tbody id="advancedBody" runat="server">
    <tr class="input">
        <td class="col1Label" style="padding-bottom: 0; margin-bottom: 0;">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_38 %>
        </td>
        <td colspan="9" style="padding-bottom: 0; margin-bottom: 0;" class ="ProtocolCls">
            <asp:TextBox ID="ddlProtocol" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_39 %>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbPort" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr class="input hint">
        <td class="col1Label">&nbsp;</td>
        <td colspan="9" style="word-wrap: break-word; white-space: normal">
	        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_40 %>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_41 %>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbTimeout" runat="server"></asp:TextBox> <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_42%>
        </td>
    </tr>
</tbody>
<tbody>
    <tr class="input">
        <td class="col1Label">&nbsp;</td>
        <td colspan="9">
            <div style="padding-top: 5px; padding-bottom: 5px;">
                <span style="vertical-align: middle; cursor: pointer;">
                    <orion:LocalizableButton runat="server" id="btnValidate" style="vertical-align: middle; cursor: pointer;" DisplayType="Small" LocalizedText="Test"/>
                </span>
	            <ipsla:TestResultControl ID="testResult" runat="server" />
	        </div>
        </td>
    </tr>
    <tr visible="false">
        <td colspan="10">
            <div id="validationProgress" class="validation" runat="server" style="position: absolute; left: 0; top: 0; width: 0; height: 0; z-index: 1000; overflow: hidden; display: none;">
                <div class="background"></div>
                <table><tr><td>
                    <div class="message">
                        <img src="/Orion/images/animated_loading_sm3_whbg.gif" style="vertical-align: middle;" />
                        <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_81%></span>
                    </div>
                </td></tr></table>
            </div>
        </td>
    </tr>
</tbody>
