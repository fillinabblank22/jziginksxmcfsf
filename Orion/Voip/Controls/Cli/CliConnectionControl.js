﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Voip.Controls");

Orion.Voip.Controls.CliConnectionControl = function(element) {
    Orion.Voip.Controls.CliConnectionControl.initializeBase(this, [element]);
    this.ddlSavedCredentials = null;
    this.tbUsername = null;
    this.tbPassword = null;
    this.tbEnablePassword = null;
    this.ddlEnableLevel = null;
    this.ddlProtocol = null;
    this.tbPort = null;
    this.tbTimeout = null;
    this.testResult = null;
    this.btnValidate = null;
    this.validationProgress = null;
    this.validationIp = null;
    this.validationEngineId = null;

    this.credentialsStore = null;
    this.extCredentialsCombo = null;
    this.extUsername = null;
    this.extPassword = null;
    this.extEnablePassword = null;
    this.enableLevelData = null;
    this.enableLevelStore = null;
    this.extEnableLevelCombo = null;
    this.protocolData = null;
    this.protocolStore = null;
    this.extProtocol = null;
    this.extPort = null;
    this.extTimeout = null;

    this.extEnableLevelCombo_valid = null;
    this.extPort_valid = null;
    this.extTimeout_valid = null;
    this.selectedCredentialsId = null;
    this.isFipsEnabledOnAnyEngine = false;
}

Orion.Voip.Controls.CliConnectionControl.prototype = {
    get_IsFipsEnabledOnAnyEngine: function () { return this.isFipsEnabledOnAnyEngine; },
    set_IsFipsEnabledOnAnyEngine: function(value) {
        this.isFipsEnabledOnAnyEngine = value;
    },

    get_ValidationIp: function () { return this.validationIp; },
    set_ValidationIp: function(value) {
        this.validationIp = value;
        this.btnValidate.style.display = (value != null ? 'inline-block' : 'none');
        this.testResult.set_Result(null);
    },

    get_ValidationEngineId: function() { return this.validationEngineId; },
    set_ValidationEngineId: function(value) { this.validationEngineId = value; },

    PreFillNodeCredentials: function (nodeIds) {
        this.testResult.set_Result(null);
        Orion.Voip.Controls.CliConnectionControlService.GetCommonCliCredentials(
            nodeIds,
            Function.createDelegate(this, function (result, context) {
                this.set_CredentialId(result.CredentialId);
                if (this.isFipsEnabledOnAnyEngine) {
                    this.set_Protocol(result.Protocol ? result.Protocol : 'Telnet');
                } else {
                    this.set_Protocol(result.Protocol ? result.Protocol : 'SshAuto');
                }
                this.set_Port(result.Port ? result.Port : 22);
                this.set_Timeout(result.Timeout ? result.Timeout : 5000);
            }),
            function (error, context) {
                // ignore
            },
            null
        );
    },

    SaveNodeCredentials: function (nodeIds, resultHandler, errorHandler) {
        if (CheckDemoMode()) return false;

        var credentialId = this.get_CredentialId();
        var credentialName = this.get_CredentialName();
        var connInfo = null;
        if (credentialId != null || (credentialName != null && credentialName.length > 0)) {
            connInfo = {
                CredentialId: credentialId,
                CredentialName: credentialName,
                Username: this.get_Username() || '',
                Password: this.get_Password() || '',
                EnablePassword: this.get_EnablePassword() || '',
                EnableLevel: this.get_EnableLevel(),
                Protocol: this.get_Protocol(),
                Port: this.get_Port(),
                Timeout: this.get_Timeout()
            };
        }
        Orion.Voip.Services.NodePropertiesService.UpdateCliConnectionInformation(nodeIds, connInfo,
            function (result, context) { resultHandler(); },
            function (error, context) { errorHandler(error); },
            null
        );
        return true;
    },

    get_Protocol: function () {
        return this.extProtocol.getValue();
    },
    set_Protocol: function (value) {
        this.extProtocol.setValue(value);
    },

    get_Port: function () {
        var num = this.extPort.getValue();
        return (num == null || num == '' || isNaN(num)
            ? null
            : eval(num));
    },
    set_Port: function (value) {
        this.extPort.setValue(value);
    },

    get_Timeout: function () {
        var num = this.extTimeout.getValue();
        return (num == null || num == '' || isNaN(num)
            ? null
            : eval(num));
    },
    set_Timeout: function (value) {
        this.extTimeout.setValue(value);
    },


    get_CredentialId: function () {
        return this.selectedCredentialsId;
    },
    set_CredentialId: function (value) {
        this.extCredentialsCombo.setValue();

        var loadCallbackScope = { credentialId: value, sender: this };
        var loadOptions = {
            callback: function (r, options, success) {
                var credentialId = this.credentialId;
                var storeIndex = this.sender.credentialsStore.findBy(function (record, id) { return (record.data.id == credentialId); });
                this.sender.RefreshCredentialsContents(storeIndex);
            },
            scope: loadCallbackScope
        };

        this.credentialsStore.load(loadOptions);
    },

    get_CredentialName: function () { return this.extCredentialsCombo.getValue(); },
    get_Username: function () { return this.extUsername.getValue(); },
    get_Password: function () { return this.extPassword.getValue(); },
    get_EnablePassword: function () { return this.extEnablePassword.getValue(); },

    get_EnableLevel: function () {
        var name = this.extEnableLevelCombo.getValue();
        var index = this.enableLevelStore.find('name', name);
        if (index == -1) {
            return null;
        }
        var record = this.enableLevelStore.getAt(index);
        if (record == null) {
            return null;
        }
        return record.data.value;
    },
    set_EnableLevel: function (value) {
        var index = this.enableLevelStore.find('value', value);
        if (index == -1) {
            return;
        }
        var record = this.enableLevelStore.getAt(index);
        if (record == null) {
            return;
        }
        this.extEnableLevelCombo.setValue(record.data.name);
    },

    get_ddlSavedCredentials: function () { return this.ddlSavedCredentials; },
    set_ddlSavedCredentials: function (value) { this.ddlSavedCredentials = value; },

    get_tbUsername: function () { return this.tbUsername; },
    set_tbUsername: function (value) { this.tbUsername = value; },

    get_tbPassword: function () { return this.tbPassword; },
    set_tbPassword: function (value) { this.tbPassword = value; },

    get_tbEnablePassword: function () { return this.tbEnablePassword; },
    set_tbEnablePassword: function (value) { this.tbEnablePassword = value; },

    get_ddlEnableLevel: function () { return this.ddlEnableLevel; },
    set_ddlEnableLevel: function (value) { this.ddlEnableLevel = value; },

    get_ddlProtocol: function () { return this.ddlProtocol; },
    set_ddlProtocol: function (value) { this.ddlProtocol = value; },

    get_rbProtocolSsh: function () { return this.rbProtocolSsh; },
    set_rbProtocolSsh: function (value) { this.rbProtocolSsh = value; },

    get_tbPort: function () { return this.tbPort; },
    set_tbPort: function (value) { this.tbPort = value; },

    get_tbTimeout: function () { return this.tbTimeout; },
    set_tbTimeout: function (value) { this.tbTimeout = value; },

    get_testResult: function () { return this.testResult; },
    set_testResult: function(value) { this.testResult = value; },

    get_btnValidate: function () { return this.btnValidate; },
    set_btnValidate: function (value) { this.btnValidate = value; },

    get_validationProgress: function () { return this.validationProgress; },
    set_validationProgress: function (value) { this.validationProgress = value; },

    ShowValidationMask: function () {
        var progress = $(this.validationProgress);
        var container = $(this.get_element()).parents('.ipsla_MaskArea').eq(0);
        progress.width(container.width());
        progress.height(container.height());
        progress.find('.background').css('opacity', 0.7);
        this.validationProgress.style.display = 'block';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'hidden');
        }
    },

    HideValidationMask: function () {
        this.validationProgress.style.filter = '';
        this.validationProgress.style.display = 'none';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'visible');
        }
    },

    RefreshCredentialsContents: function (storeIndex) {
        
        var disableConnEdit = true;
        var disableCredEdit = true;
        var name = this.extCredentialsCombo.getValue();
        var record = null;
        if (storeIndex == null) {
            storeIndex = this.credentialsStore.findBy(function (record, id) { return (record.data.name == name); });
            storeIndex = storeIndex >= 0 ? storeIndex : null;
        }
        if (storeIndex != null) {
            record = this.credentialsStore.getAt(storeIndex);
            name = record != null ? record.data.name : '';
            this.extCredentialsCombo.setValue(name);
        }
        if (record == null) {
            disableCredEdit = name == '';
            disableConnEdit = disableCredEdit;
        }
        else {
            disableConnEdit = false;
            disableCredEdit = true;
        }

        this.selectedCredentialsId = record ? record.data.id : null;
        this.extUsername.setDisabled(disableCredEdit);
        this.extUsername.setValue(record ? record.data.user : '');
        this.extPassword.setDisabled(disableCredEdit);
        this.extPassword.setValue(record ? '********' : '');
        this.set_EnableLevel(record ? record.data.level : -2);
        this.extEnablePassword.setDisabled(disableCredEdit || (this.get_EnableLevel() == -2));
        this.extEnablePassword.setValue(record && record.data.level != -2 ? '********' : '');
        this.extEnableLevelCombo.setDisabled(disableCredEdit);
        this.extProtocol.setDisabled(disableConnEdit);
        this.extPort.setDisabled(disableConnEdit);
        this.extTimeout.setDisabled(disableConnEdit);
        this.btnValidate.disabled = disableConnEdit;
        this.testResult.set_Result(null);
    },

    btnValidate_Click: function () {
        if (CheckDemoMode()) return false;

        var credentialId = this.get_CredentialId();
        var credentialName = this.get_CredentialName();
        if (credentialId != null || (credentialName != null && credentialName.length > 0)) {
            var userName = this.get_Username().trim();
            var password = this.get_Password().trim();
            if (userName.length > 0 && password.length > 0) {
                var successHandler = Function.createDelegate(this, function (result, context) {
                    this.HideValidationMask();
                    this.testResult.set_Result(result);
                });
                var errorHandler = Function.createDelegate(this, function (error, context) {
                    this.HideValidationMask();
                    Ext.Msg.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_15;E=js}", +error.get_message()));
                });
                this.ShowValidationMask();
                if (credentialId != null) {
                    Orion.Voip.Controls.CliConnectionControlService.ValidateCliConnectionCredentialId(
                        this.get_ValidationEngineId(), this.get_ValidationIp(),
                        credentialId,
                        this.get_Protocol(), this.get_Port(), this.get_Timeout(),false,
                        successHandler,
                        errorHandler,
                        null
                    );
                } else {
                    Orion.Voip.Controls.CliConnectionControlService.ValidateCliConnectionCredentials(
                        this.get_ValidationEngineId(), this.get_ValidationIp(),
                        this.get_Username(), this.get_Password(), this.get_EnablePassword(), this.get_EnableLevel(),
                        this.get_Protocol(), this.get_Port(), this.get_Timeout(),false,
                        successHandler,
                        errorHandler,
                        null
                    );
                }
            }
            else {
                if (userName.length == 0 && password.length == 0) {
                    Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_PG_15;E=js}');
                } else if (userName.length == 0) {
                    Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_PG_13;E=js}');
                }
                else {
                    Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_PG_14;E=js}');
                }
            }
        }
    },

    btnAdvanced_Expanded: function () {
        this.extProtocol.setWidth(100);
    },

    syncSize: function () {
        this.extCredentialsCombo.syncSize();
        this.extEnableLevelCombo.syncSize();
        this.extProtocol.syncSize();
    },

    validate: function () {
        if (this.extEnableLevel_valid != null) {
            this.extEnableLevel.focus();
            Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_16;E=js}', this.extEnableLevel_valid);
            return false;
        }
        if (this.extPort_valid != null) {
            this.extPort.focus();
            Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_17;E=js}', this.extPort_valid);
            return false;
        }
        if (this.extTimeout_valid != null) {
            this.extTimeout.focus();
            Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_18;E=js}', this.extTimeout_valid);
            return false;
        }
        return true;
    },
    initialize: function () {
        Orion.Voip.Controls.CliConnectionControl.callBaseMethod(this, 'initialize');
        
        // Add custom initialization here
        Ext.QuickTips.init();

        this.enableLevelData = [
            [-2, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_19;E=js}'],
            [-1, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_20;E=js}'],
            [0, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_21;E=js}'],
            [1, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_22;E=js}'],
            [2, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_23;E=js}'],
            [3, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_24;E=js}'],
            [4, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_25;E=js}'],
            [5, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_26;E=js}'],
            [6, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_27;E=js}'],
            [7, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_28;E=js}'],
            [8, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_29;E=js}'],
            [9, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_30;E=js}'],
            [10, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_31;E=js}'],
            [11, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_32;E=js}'],
            [12, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_33;E=js}'],
            [13, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_34;E=js}'],
            [14, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_35;E=js}'],
            [15, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_36;E=js}']];
        this.enableLevelStore = new Ext.data.SimpleStore({
            fields: ['value', 'name'],
            data: this.enableLevelData
        });

        if (this.isFipsEnabledOnAnyEngine) {
            this.protocolData = [
            ['Telnet', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_37;E=js}'],
            ['Ssh2', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_40;E=js}']];
        } else {
            this.protocolData = [
            ['Telnet', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_37;E=js}'],
            ['SshAuto', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_38;E=js}'],
            ['Ssh1', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_39;E=js}'],
            ['Ssh2', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_40;E=js}']];
        };
        
        this.protocolStore = new Ext.data.SimpleStore({
            fields: ['value', 'name'],
            data: this.protocolData
        });

        this.credentialsStore = new Ext.data.Store({
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '/Orion/Voip/Controls/Cli/CliConnectionControl.asmx/GetCredentials',
                method: 'POST',
                headers: { 'Content-type': 'application/json' }
            }),
            reader: new Ext.data.JsonReader({
                root: 'd',
                id: 'id'
            }, [
                { name: 'id', type: 'int' },
                { name: 'name', type: 'string' },
                { name: 'user', type: 'string' },
                { name: 'level' }
            ]),
            sortInfo: { field: 'name', direction: "ASC" }
        });
        this.extCredentialsCombo = new Ext.form.ComboBox({
            applyTo: this.ddlSavedCredentials,
            store: this.credentialsStore,
            displayField: 'name',
            mode: 'local',
            triggerAction: 'all',
            emptyText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_41;E=js}',
            // width: 200,
            autoWidth: true,
            lazyRender: true,
            autoSelect: false,
            typeAhead: true,
            enableKeyEvents: true,
            listClass: 'x-menu x-menu-neutralize',
            listeners: {
                blur: Function.createDelegate(this, function (field, newValue, oldValue) {
                    this.RefreshCredentialsContents(null);
                }),
                keypress: Function.createDelegate(this, function (field, eventObject) {
                    var delegate = Function.createDelegate(this, function () { this.RefreshCredentialsContents(null); });
                    setTimeout(function () { delegate(); }, 0);
                }),
                select: Function.createDelegate(this, function (combo, record, index) {
                    this.RefreshCredentialsContents(index);
                })
            }
        });
        this.extUsername = new Ext.form.TextField({
            applyTo: this.tbUsername,
            disabledCls: 'x-item-disabled extDisabled',
            width: 200
        });
        this.extPassword = new Ext.form.TextField({
            applyTo: this.tbPassword,
            disabledCls: 'x-item-disabled extDisabled',
            inputType: 'password',
            width: 200
        });
        this.extEnablePassword = new Ext.form.TextField({
            applyTo: this.tbEnablePassword,
            disabledCls: 'x-item-disabled extDisabled',
            inputType: 'password',
            width: 200
        });
        this.extEnableLevelCombo = new Ext.form.ComboBox({
            applyTo: this.ddlEnableLevel,
            store: this.enableLevelStore,
            displayField: 'name',
            mode: 'local',
            triggerAction: 'all',
            forceSelection: true,
            // width: 200,
            autoWidth: true,
            lazyRender: false,
            autoSelect: false,
            editable: false,
            typeAhead: true,
            listClass: 'x-menu x-menu-neutralize',
            listeners: {
                change: Function.createDelegate(this, function (field, newValue, oldValue) {
                    this.extEnablePassword.setDisabled(this.get_EnableLevel() == -2);
                }),
                select: Function.createDelegate(this, function (combo, record, index) {
                    this.extEnablePassword.setDisabled(this.get_EnableLevel() == -2);
                })
            }
        });
        this.extProtocol = new Ext.form.ComboBox({
            applyTo: this.ddlProtocol,
            store: this.protocolStore,
            displayField: 'name',
            valueField: 'value',
            mode: 'local',
            triggerAction: 'all',
            forceSelection: true,
            autoWidth: true,
            //            width: 150,
            lazyRender: false,
            autoSelect: false,
            editable: false,
            typeAhead: true,
            listClass: 'x-menu x-menu-neutralize',
            listeners: {
                change: Function.createDelegate(this, function (field, newValue, oldValue) {
                    this.tbPort.value = newValue == 'Telnet' ? '23' : '22';
                }),
                select: Function.createDelegate(this, function (combo, record, index) {
                    this.tbPort.value = record.data.value == 'Telnet' ? '23' : '22';
                })
            }
        });

        if (this.isFipsEnabledOnAnyEngine) {
            this.extProtocol.setValue('Telnet');
        } else {
            this.extProtocol.setValue('SshAuto');
        };
        

        this.extPort = new Ext.form.NumberField({
            applyTo: this.tbPort,
            allowDecimals: false,
            allowNegative: false,
            allowBlank: false,
            blankText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_42;E=js}',
            minValue: 1,
            minText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_43;E=js}',
            maxValue: 65535,
            maxText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_44;E=js}',
            width: 50,
            listeners: {
                invalid: Function.createDelegate(this, function (sender, msg) {
                    this.extPort_valid = msg;
                }),
                valid: Function.createDelegate(this, function (sender) {
                    this.extPort_valid = null;
                })
            }
        });
        this.extTimeout = new Ext.form.NumberField({
            applyTo: this.tbTimeout,
            allowDecimals: false,
            allowNegative: false,
            allowBlank: false,
            blankText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_45;E=js}',
            minValue: 0,
            minText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_46;E=js}',
            width: 50,
            listeners: {
                invalid: Function.createDelegate(this, function (sender, msg) {
                    this.extTimeout_valid = msg;
                }),
                valid: Function.createDelegate(this, function (sender) {
                    this.extTimeout_valid = null;
                })
            }
        });

        this.RefreshCredentialsContents(null);

        this.set_ValidationIp(this.validationIp);
    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Controls.CliConnectionControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.CliConnectionControl.registerClass('Orion.Voip.Controls.CliConnectionControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
