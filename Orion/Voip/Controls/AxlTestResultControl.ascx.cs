using System;
using System.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Controls_AxlTestResultControl : ScriptUserControlBase
{
	public bool? Result { get; set; }
    protected static readonly string AxlCredentialsFailedHelpLink = "http://www.solarwinds.com/documentation/kbloader.aspx?kb=4131&lang=en";

	protected void Page_Load(object sender, EventArgs e)
	{
		if (this.Result.HasValue)
		{
			if (this.Result.Value)
			{
				this.elFailed.Style[HtmlTextWriterStyle.Display] = "none";
			}
			else
			{
				this.elSuccess.Style[HtmlTextWriterStyle.Display] = "none";
			}
		}
		else
		{
			this.elFailed.Style[HtmlTextWriterStyle.Display] = "none";
			this.elSuccess.Style[HtmlTextWriterStyle.Visibility] = "hidden";
		}
	}

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute("id", this.ClientID);
        writer.RenderBeginTag(HtmlTextWriterTag.Span);

    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.AxlTestResultControl", this.ClientID);
        descriptor.AddElementProperty("elSuccess", this.elSuccess.ClientID);
        descriptor.AddElementProperty("elFailed", this.elFailed.ClientID);
        descriptor.AddProperty("result", this.Result);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("AxlTestResultControl.js"));
    }

    #endregion
}