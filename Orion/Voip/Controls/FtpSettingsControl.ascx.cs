﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_Controls_Ftp_FtpSettingsControl : ScriptUserControlBase
{
    protected static readonly string SelectCredentialsLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_99;
    protected static readonly string UsernameLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_100;
    protected static readonly string PasswordLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_101;
    protected static readonly string FTPServerLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_102;
    protected static readonly string FTPServerHelpText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_103;
    protected static readonly string PortLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_104;
    protected static readonly string PassiveModeLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_105;
    protected static readonly string SecureConnectionLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_106;
    protected static readonly string PathLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_107;
    protected static readonly string PollingFrequencyLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_108;
    protected static readonly string PollingFrequencyUnitLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_133;
    protected static readonly string PollingFrequencyHelpText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_109;
    protected static readonly string DeleteCDRLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_110;
    protected static readonly string DeleteCDRHelpText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_111;
    protected static readonly string RequiredFieldError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_112;
    protected static readonly string PathHelpText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_113;
    protected static readonly string BannerText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_114;
    protected static readonly string BannerLinkText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_115;
    protected const string BannerLink = "http://www.solarwinds.com/documentation/kbloader.aspx?kb=4093&lang=en";

    protected string DeleteCDRHelpLink = string.Empty;

    private const string helpFragment = "OrionIPSLAMonitorAGAddEditVoIPFTPServer";
    private static readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        DeleteCDRHelpLink = HelpLocator.GetHelpUrl(helpFragment);

        this.btnValidate.Attributes["onclick"] = "if (Page_ClientValidate('VoipFtpSettings')) { $find('" + this.ClientID + "').btnValidate_Click($(this)); } return false;";
        userNameValidator.ClientValidationFunction = "VoipFtpValidateName_" + this.ClientID;

        tbPort.Text = Port.ToString();
        tbFrequency.Text = Frequency.ToString();

        if (IsPostBack)
        {
            // passwords are not sent on postback but we need control to keep its value
            tbPassword.Attributes["value"] = tbPassword.Text;
        }
    }

    public int CredentialsId
    {
        get
        {
            if (string.IsNullOrEmpty(tbReturnCredentialsId.Value))
                return 0;

            return int.Parse(tbReturnCredentialsId.Value);
        }
    }

    public string CredentialsName
    {
        get { return tbReturnCredentialsName.Value; }
        set { tbReturnCredentialsName.Value = value; }
    }

    public string Username
    {
        get { return tbUsername.Text; }
        set { tbUsername.Text = value; }
    }

    public string Password
    {
        get { return tbPassword.Text; }
        set { tbPassword.Text = value; }
    }

    public string FTPServer
    {
        get { return tbFTPServer.Text; }
        set { tbFTPServer.Text = value; }
    }

    public string Path
    {
        get { return tbPath.Text; }
        set { tbPath.Text = value; }
    }

    public int Port
    {
        get
        {
            int port;
            if (Int32.TryParse(tbPort.Text, out port))
                return port;

            return (SecuredConnection ? VoipCCMFtpConnection.DefaultSFtpPort : VoipCCMFtpConnection.DefaultFtpPort); // default port in case of wrong port definition
        }
        set { tbPort.Text = value.ToString(); }
    }

    public int Frequency
    {
        get
        {
            int frequency;
            if (Int32.TryParse(tbFrequency.Text, out frequency))
                return frequency;

            return VoipCCMFtpConnection.DefaultFrequency; // default frequency in case of wrong definition
        }
        set { tbFrequency.Text = value.ToString(); }
    }

    public bool PassiveMode
    {
        get { return cbPassiveMode.Checked; }
        set { cbPassiveMode.Checked = value; }
    }

    public bool SecuredConnection
    {
        get { return cbSecureConnection.Checked; }
        set { cbSecureConnection.Checked = value; }
    }

    public bool DeleteAfterDownload
    {
        get { return cbDeleteFilesAfterDownload.Checked; }
        set { cbDeleteFilesAfterDownload.Checked = value; }
    }

    public int PreselectedCredentialsId { get; set; }

    public string TargetIP { get; set; }
    public int TargetEngine { get; set; }
    public IEnumerable<int> NodeIds { get; set; }

    protected void userNameValidator_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CredentialsId == 0 && string.IsNullOrEmpty(Username.Trim()))
            args.IsValid = false;
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.Ftp.FtpSettingsControl", this.ClientID);
        descriptor.AddProperty("tbSavedCredentialsId", this.tbSavedCredentialsId.ClientID);
        descriptor.AddProperty("tbSavedCredentialsName", this.tbSavedCredentialsName.ClientID);
        descriptor.AddProperty("tbReturnCredentialsId", this.tbReturnCredentialsId.ClientID);
        descriptor.AddProperty("tbReturnCredentialsName", this.tbReturnCredentialsName.ClientID);
        descriptor.AddElementProperty("tbUsername", this.tbUsername.ClientID);
        descriptor.AddElementProperty("tbPassword", this.tbPassword.ClientID);
        descriptor.AddElementProperty("tbFTPServer", this.tbFTPServer.ClientID);
        descriptor.AddElementProperty("tbPath", this.tbPath.ClientID);
        descriptor.AddElementProperty("tbPort", this.tbPort.ClientID);
        descriptor.AddElementProperty("tbFrequency", this.tbFrequency.ClientID);
        descriptor.AddElementProperty("cbPassiveMode", this.cbPassiveMode.ClientID);
        descriptor.AddElementProperty("cbSecureConnection", this.cbSecureConnection.ClientID);
        descriptor.AddElementProperty("cbDeleteFilesAfterDownload", this.cbDeleteFilesAfterDownload.ClientID);
        descriptor.AddComponentProperty("testResult", this.testResult.ClientID);
        descriptor.AddElementProperty("btnValidate", this.btnValidate.ClientID);
        descriptor.AddElementProperty("validationProgress", this.validationProgress.ClientID);

        descriptor.AddProperty("ValidationIp", TargetIP);
        descriptor.AddProperty("ValidationEngineId", TargetEngine);
        descriptor.AddProperty("NodeIds", !IsPostBack ? NodeIds : Enumerable.Empty<int>());
        descriptor.AddProperty("PreselectedCredentialsId", (IsPostBack ? CredentialsId : PreselectedCredentialsId));
        descriptor.AddProperty("PreselectedCredentialsName", CredentialsName);
        descriptor.AddProperty("PreselectedCredentialsUsername", Username);
        descriptor.AddProperty("PreselectedCredentialsPassword", Password);

        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("FtpSettingsControl.js"));
    }

    #endregion
}