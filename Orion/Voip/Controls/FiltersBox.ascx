<%@ Import namespace="SolarWinds.Orion.IpSla.Web.Filters"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FiltersBox.ascx.cs" Inherits="Orion_Voip_Controls_FiltersBox" %>

    <asp:Repeater ID="InnerFilterArray" runat="server" DataSource='<%#this.AndFILTER.Filters %>'>
        
        <HeaderTemplate>
            <table cellpadding="0" cellspacing="0" style="padding-left:5%; width:95%;">
            <thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </thead>
        </HeaderTemplate>
        
        <ItemTemplate>
            <tr>
                <td width="30%"><%#Eval("DisplayValue")%></td>
                <td width="30%"><%#FilterUtility.OperFilterToString((SqlOperators)Eval("OperFilter"))%></td>
                <td width="30%">'<%#Eval("ValueFilter")%>'</td>
                <td width="10%" style="text-align:right;" >
                    <asp:ImageButton runat="server" 
                                     ID="btnDeleteFilter" 
                                     ImageUrl="~/Orion/Voip/images/icon_delete.gif"
                                     OnClick="btnDeleteFilter_Click"
                                     CommandArgument='<%#Container.ItemIndex %>'
                                     CommandName='<%#this.EXTItemIndex %>'
                                     ToolTip="Delete this condition" 
                                     CausesValidation = "false" />
                </td>
            </tr>
        </ItemTemplate>
        
        <FooterTemplate>
        </table>
        </FooterTemplate>
    </asp:Repeater>

