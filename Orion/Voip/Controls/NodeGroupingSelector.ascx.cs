﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_Voip_Controls_NodeGroupingSelector : System.Web.UI.UserControl
{
    public event EventHandler SelectedValueChanged;

    public string SelectedValue
    {
        get { return ddlGroupBy.SelectedValue; }
    }

    public bool IncludeSiteProperties { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            var customProperties = NodesCustomPropertiesDAL.Instance.GetCustomPropertyNames(true);
            foreach (var propName in customProperties)
            {
                this.ddlGroupBy.Items.Add(new ListItem(propName, string.Format("CustomProperties.{0}", propName)));
            }
            if (IncludeSiteProperties)
            {
                this.ddlGroupBy.Items.Add(new ListItem(Resources.VNQMWebContent.VNQMWEBCODE_VB1_37, SitesDAL.RegionGroupingTypeName));
            }
        }
    }

    protected void ddlGroupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.RaiseSelectedValueChanged();
    }

    private void RaiseSelectedValueChanged()
    {
        if (this.SelectedValueChanged != null)
        {
            this.SelectedValueChanged(this, new EventArgs());
        }
    }

}
