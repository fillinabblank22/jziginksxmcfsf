﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Controls_EditSLALocation : System.Web.UI.UserControl
{
    public void Initialize(int? targetNodeId, int? sourceLocationId, int? targetLocationId)
    {
        ddlOriginRegion.Items.Clear();
        ddlTargetRegion.Items.Clear();

        using (DataTable regionTable = RegionsDAL.Instance.GetAllRegionRecords())
        {
            ddlOriginRegion.DataSource = regionTable;
            ddlOriginRegion.DataTextField = "RegionName";
            ddlOriginRegion.DataValueField = "RegionID";
            ddlOriginRegion.DataBind();

            ddlOriginRegion.Items.Insert(0, new ListItem(VNQMWebContent.VNQMWEBCODE_TM0_1, "-1"));

            ddlTargetRegion.DataSource = regionTable;
            ddlTargetRegion.DataTextField = "RegionName";
            ddlTargetRegion.DataValueField = "RegionID";
            ddlTargetRegion.DataBind();

            ddlTargetRegion.Items.Insert(0, new ListItem(VNQMWebContent.VNQMWEBCODE_TM0_1, "-1"));
        }

        if (sourceLocationId != null)
        {
            ddlOriginRegion.SelectedValue = sourceLocationId.ToString();
        }

        if (targetNodeId == null)
        {
            ddlTargetRegion.Enabled = false;
        }
        else
        {
            ddlTargetRegion.Enabled = true;

            if (targetLocationId != null)
            {
                ddlTargetRegion.SelectedValue = targetLocationId.ToString();
            }
        }
    }

    public int? GetSourceLocation()
    {
        if (ddlOriginRegion.SelectedValue == "-1")
        {
            return null;
        }
        else
        {
            return int.Parse(ddlOriginRegion.SelectedValue);
        }
    }

    public int? GetTargetLocation()
    {
        if (ddlTargetRegion.SelectedValue == "-1")
        {
            return null;
        }
        else
        {
            return int.Parse(ddlTargetRegion.SelectedValue);
        }
    }

    private static bool IsValid(Control parent)
    {
        foreach (Control ctl in parent.Controls)
        {
            var validator = ctl as IValidator;
            if (validator != null && !validator.IsValid)
                return false;
            if (!IsValid(ctl))
                return false;
        }
        return true;
    }

    protected override void OnPreRender(EventArgs e)
    {
        var scriptManager = ScriptManager.GetCurrent(Page);
        if (scriptManager.IsInAsyncPostBack)
        {
            scriptManager.RegisterDataItem(this,
                IsValid(this) ? "{IsValid:true}" : "{IsValid:false}",
                true);
        }
        base.OnPreRender(e);
    }
}
