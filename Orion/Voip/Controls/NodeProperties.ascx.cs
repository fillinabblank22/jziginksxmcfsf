﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.UI;
using Orion.Voip.Controls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web;
using Node = SolarWinds.Orion.Core.Common.Models.Node;

public partial class Orion_Voip_Controls_NodeProperties : UserControl, INodePropertyPlugin
{
    private static readonly Log log = new Log();

    private const string PluginName = "IPSLANodeProperties";

    protected static readonly string PluginTitle = VNQMWebContent.VNQMWEBCODE_VB1_249;
    protected static readonly string PluginEnableLabel = VNQMWebContent.VNQMWEBCODE_VB1_250;
    protected static readonly string PluginEnableLabelHelpfulText = VNQMWebContent.VNQMWEBCODE_VB1_251;
    protected static readonly string AXLCredentialsHeader = VNQMWebContent.VNQMWEBCODE_VB1_123;
    protected static readonly string FTPSettingsHeader = VNQMWebContent.VNQMWEBCODE_VB1_125;
    protected static readonly string DifferentAxlCredentialsWarning = VNQMWebContent.VNQMWEBCODE_VB1_252;
    protected static readonly string DifferentFtpCredentialsWarning = VNQMWebContent.VNQMWEBCODE_VB1_253;
    protected static readonly string AXLCredentialsHeaderLabelHelpfulText = VNQMWebContent.VNQMWEBCODE_VB1_88;

    private EditCmPluginMode cmMode;
    private Dictionary<string, object> _propertyBag;
    private IList<Node> _nodes;
    private NodePropertyPluginExecutionMode _mode;
    private NodePropertyPluginWrapper _plugin;

    private static readonly string InvalidDataMessage = VNQMWebContent.VNQMWEBCODE_VB1_254;
    private static readonly string InvalidAxlCredentialsMessage = VNQMWebContent.VNQMWEBCODE_VB1_255;
    private static readonly string InvalidFtpSettingsMessage = VNQMWebContent.VNQMWEBCODE_VB1_256;

    private const string AXLCREDENTIALS_SESSIONID = PluginName + "_AxlCredentials";
    private const string FTPCREDENTIALS_SESSIONID = PluginName + "_FtpCredentials";
    private const string FTPSERVER_SESSIONID = PluginName + "_FtpServer";
    private const string PATH_SESSIONID = PluginName + "_Path";
    private const string PORT_SESSIONID = PluginName + "_Port";
    private const string SECURED_SESSIONID = PluginName + "_Secured";
    private const string PASSIVE_SESSIONID = PluginName + "_Passive";
    private const string FREQUENCY_SESSIONID = PluginName + "_Frequency";
    private const string DELETEFILES_SESSIONID = PluginName + "_DeleteFiles";
    private const string ENABLE_CDR_SESSIONID = PluginName + "_EnableCDR";

    private static readonly string InvalidCliCredMessage = VNQMWebContent.VNQMWEBCODE_VM_5;
   

    private const string AVAYA_ENABLE_CDR_SESSIONID = PluginName + "_AvayaEnableCDR";
    private const string AVAYA_ENABLE_CLI_SESSIONID = PluginName + "_AvayaEnableCLI";

    protected void Page_Load(object sender, EventArgs e)
    {
        cbEnableCDRPolling.Text = PluginEnableLabel;
    }

    #region Properties

    private string ErrorMessage { get; set; }

    #region Cisco CM
    /// <summary>
    /// Defines if plugin "Enable multiple selection" checkbox is visible. Normally property is set from this.Initialize()
    /// </summary>
    public bool MultipleSelectionVisible
    {
        get { return cbMultipleSelection.Visible; }
        set { cbMultipleSelection.Visible = value; }
    }

    public bool MultipleSelectionPollingVisible
    {
        get { return cbMultipleSelectionPolling.Visible; }
        set { cbMultipleSelectionPolling.Visible = value; }
    }

    /// <summary>
    /// Defines if plugin "Enable" checkbox is visible.
    /// </summary>
    public bool EnableCDRPolling
    {
        get
        {
            if (!_propertyBag.ContainsKey(ENABLE_CDR_SESSIONID))
            {
                _propertyBag[ENABLE_CDR_SESSIONID] = false;
            }
            return (bool)_propertyBag[ENABLE_CDR_SESSIONID] && _plugin.Visible;
        }
        set
        {
            _propertyBag[ENABLE_CDR_SESSIONID] = value;
            cbEnableCDRPolling.Checked = value;

            SetCiscoCredControlVisibility(value && MultipleSelectionSelected);
        }
    }

    public int SelectedAxlCredentialsId
    {
        get
        {
            if (!_propertyBag.ContainsKey(AXLCREDENTIALS_SESSIONID))
            {
                _propertyBag[AXLCREDENTIALS_SESSIONID] = 0;
            }
            return (int)_propertyBag[AXLCREDENTIALS_SESSIONID];
        }
        set
        {
            _propertyBag[AXLCREDENTIALS_SESSIONID] = value;
        }
    }

    public int SelectedFtpCredentialsId
    {
        get
        {
            if (!_propertyBag.ContainsKey(FTPCREDENTIALS_SESSIONID))
            {
                _propertyBag[FTPCREDENTIALS_SESSIONID] = 0;
            }
            return (int)_propertyBag[FTPCREDENTIALS_SESSIONID];
        }
        set
        {
            _propertyBag[FTPCREDENTIALS_SESSIONID] = value;
        }
    }

    public string SelectedFtpServer
    {
        get
        {
            if (!_propertyBag.ContainsKey(FTPSERVER_SESSIONID))
            {
                _propertyBag[FTPSERVER_SESSIONID] = string.Empty;
            }
            return (string)_propertyBag[FTPSERVER_SESSIONID];
        }
        set
        {
            _propertyBag[FTPSERVER_SESSIONID] = value;
        }
    }

    public int SelectedPort
    {
        get
        {
            if (!_propertyBag.ContainsKey(PORT_SESSIONID))
            {
                _propertyBag[PORT_SESSIONID] = VoipCCMFtpConnection.DefaultFtpPort;
            }
            return (int)_propertyBag[PORT_SESSIONID];
        }
        set
        {
            _propertyBag[PORT_SESSIONID] = value;
        }
    }

    public string SelectedPath
    {
        get
        {
            if (!_propertyBag.ContainsKey(PATH_SESSIONID))
            {
                _propertyBag[PATH_SESSIONID] = string.Empty;
            }
            return (string)_propertyBag[PATH_SESSIONID];
        }
        set
        {
            _propertyBag[PATH_SESSIONID] = value;
        }
    }

    public bool SelectedSecuredConnection
    {
        get
        {
            if (!_propertyBag.ContainsKey(SECURED_SESSIONID))
            {
                _propertyBag[SECURED_SESSIONID] = false;
            }
            return (bool)_propertyBag[SECURED_SESSIONID];
        }
        set
        {
            _propertyBag[SECURED_SESSIONID] = value;
        }
    }

    public bool SelectedPassiveMode
    {
        get
        {
            if (!_propertyBag.ContainsKey(PASSIVE_SESSIONID))
            {
                _propertyBag[PASSIVE_SESSIONID] = true;
            }
            return (bool)_propertyBag[PASSIVE_SESSIONID];
        }
        set
        {
            _propertyBag[FTPSERVER_SESSIONID] = value;
        }
    }

    public bool SelectedDeleteFiles
    {
        get
        {
            if (!_propertyBag.ContainsKey(DELETEFILES_SESSIONID))
            {
                _propertyBag[DELETEFILES_SESSIONID] = false;
            }
            return (bool)_propertyBag[DELETEFILES_SESSIONID];
        }
        set
        {
            _propertyBag[DELETEFILES_SESSIONID] = value;
        }
    }

    public int SelectedFrequency
    {
        get
        {
            if (!_propertyBag.ContainsKey(FREQUENCY_SESSIONID))
            {
                _propertyBag[FREQUENCY_SESSIONID] = VoipCCMFtpConnection.DefaultFrequency;
            }
            return (int)_propertyBag[FREQUENCY_SESSIONID];
        }
        set
        {
            _propertyBag[FREQUENCY_SESSIONID] = value;
        }
    }

    /// <summary>
    /// Defines if plugin is enabled to edit multiple items.
    /// </summary>
    public bool MultipleSelectionSelected
    {
        get { return cbMultipleSelection.Checked; }
        set
        {
            cbMultipleSelection.Checked = value;
            cbMultipleSelection_CheckedChanged(this, EventArgs.Empty);
        }
    }

    public bool MultipleSelectionPollingSelected
    {
        get { return cbMultipleSelectionPolling.Checked; }
        set 
        { 
            cbMultipleSelectionPolling.Checked = value;
            cbMultipleSelectionPolling_CheckedChanged(this, EventArgs.Empty);
        }
    }
   
    #endregion

    #region Avaya CM

    public bool AvayaMultipleSelectionPollingVisible
    {
        get { return cbAvayaMultipleSelectionPolling.Visible; }
        set { cbAvayaMultipleSelectionPolling.Visible = value; }
    }

    public bool AvayaMultipleSelectionCliVisible
    {
        get { return cbAvayaMultipleSelectionCli.Visible; }
        set { cbAvayaMultipleSelectionCli.Visible = value; }
    }

    public bool EnableAvayaCDRPolling
    {
        get
        {
            if (!_propertyBag.ContainsKey(AVAYA_ENABLE_CDR_SESSIONID))
            {
                _propertyBag[AVAYA_ENABLE_CDR_SESSIONID] = false;
            }
            return (bool)_propertyBag[AVAYA_ENABLE_CDR_SESSIONID] && _plugin.Visible;
        }
        set
        {
            _propertyBag[AVAYA_ENABLE_CDR_SESSIONID] = value;
            cbAvayaEnableCDRPolling.Checked = value;
        }
    }

    public bool EnableAvayaCli
    {
        get
        {
            if (!_propertyBag.ContainsKey(AVAYA_ENABLE_CLI_SESSIONID))
            {
                _propertyBag[AVAYA_ENABLE_CLI_SESSIONID] = false;
            }
            return (bool)_propertyBag[AVAYA_ENABLE_CLI_SESSIONID] && _plugin.Visible;
        }
        set
        {
            _propertyBag[AVAYA_ENABLE_CLI_SESSIONID] = value;

            cbAvayaMultipleSelectionCli.Checked = value;
            SetAvayaCredControlVisibility(value && AvayaMultipleSelectionCliSelected);
        }
    }

    public bool AvayaMultipleSelectionPollingSelected
    {
        get { return cbAvayaMultipleSelectionPolling.Checked; }
        set
        {
            cbAvayaMultipleSelectionPolling.Checked = value;
            cbAvayaMultipleSelectionPolling_CheckedChanged(this, EventArgs.Empty);
        }
    }

    public bool AvayaMultipleSelectionCliSelected
    {
        get { return cbAvayaMultipleSelectionCli.Checked; }
        set
        {
            cbAvayaMultipleSelectionCli.Checked = value;
            cbAvayaMultipleSelectionCli_CheckedChanged(this, EventArgs.Empty);
        }
    }

    #endregion

    #endregion

    #region Event handlers

    protected void cbMultipleSelectionPolling_CheckedChanged(object sender, EventArgs e)
    {
        tbCcmPollingInterval.Enabled = cbMultipleSelectionPolling.Checked;
    }

    #region Cisco CM

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        SetCiscoCredControlVisibility(EnableCDRPolling && MultipleSelectionSelected);
        cbEnableCDRPolling.Enabled = MultipleSelectionSelected;
    }

    protected void cbEnableCDRPolling_CheckedChanged(object sender, EventArgs e)
    {
        EnableCDRPolling = cbEnableCDRPolling.Checked;

        if (EnableCDRPolling && SipPollingSettingsControl.SipTrunkMonitoringEnabled)
            SipPollingSettingsControl.EnableFrequencyValidator = true;
        else
            SipPollingSettingsControl.EnableFrequencyValidator = false;
    }

    #endregion

    #region Avaya CM

    protected void cbAvayaMultipleSelectionPolling_CheckedChanged(object sender, EventArgs e)
    {
        cbAvayaEnableCDRPolling.Enabled = AvayaMultipleSelectionPollingSelected;
    }

    protected void cbAvayaEnableCDRPolling_CheckedChanged(object sender, EventArgs e)
    {
        EnableAvayaCDRPolling = cbAvayaEnableCDRPolling.Checked;
    }

    protected void cbAvayaMultipleSelectionCli_CheckedChanged(object sender, EventArgs e)
    {
        //SetAvayaCredControlVisibility(EnableAvayaCli && AvayaMultipleSelectionCliSelected);
        EnableAvayaCli = cbAvayaMultipleSelectionCli.Checked;
    }
    #endregion

    #endregion

    #region Helpers

    private void SetCiscoCredControlVisibility(bool visible)
    {
        // don't use "Visible" property to allow ebedded javascript to be processed even when control is hidden
        CDRSettingsBox.Style["display"] = visible ? "block" : "none";
    }

    private void SetAvayaCredControlVisibility(bool visible)
    {
        // don't use "Visible" property to allow ebedded javascript to be processed even when control is hidden
        AvayaCDRSettingsBox.Style["display"] = visible ? "block" : "none";
    }
    private bool TestAxlConnection(int nodeId, string nodeIpAddress, int credentialsId, string userName, string password)
    {
        IPAddress ipAddress;
        if (!IPAddress.TryParse(nodeIpAddress, out ipAddress))
        { 
            log.WarnFormat("Node IP address has invalid format: {0}", nodeIpAddress);
            return false;
        }

        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            return credentialsId == 0
                ? proxy.ValidateAxlCredentials(new[] {nodeId}, ipAddress, userName, password)
                : proxy.ValidateAxlCredentials(new[] {nodeId}, ipAddress, credentialsId);
        }
    }

    private bool TestFtpConnection(int engineId, string nodeIpAddress, string ftpServer, int credentialsId, string userName, string password, string directory, bool secureConnection, int port, bool passiveMode)
    {
        IPAddress ipAddress;
        if (!IPAddress.TryParse(nodeIpAddress, out ipAddress))
        {
            log.WarnFormat("Node IP address has invalid format: {0}", nodeIpAddress);
            return false;
        }

        using (IIpSlaBusinessLayer proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            if (credentialsId == 0)
                return proxy.ValidateVoipCCMFtpConnectionInfo(engineId, ipAddress, ftpServer, userName, password, directory, secureConnection, port, passiveMode);
            else
                return proxy.ValidateVoipCCMFtpConnectionInfo(engineId, ipAddress, ftpServer, credentialsId, directory, secureConnection, port, passiveMode);
        }
    }

    private void ClearDataFromSession()
    {
        SelectedAxlCredentialsId = 0;
        SelectedFtpCredentialsId = 0;
        SelectedSecuredConnection = false;
        SelectedPort = VoipCCMFtpConnection.DefaultFtpPort;
        SelectedPath = string.Empty;
        SelectedPassiveMode = false;
        SelectedFtpServer = string.Empty;
        SelectedFrequency = VoipCCMFtpConnection.DefaultFrequency;
        SelectedDeleteFiles = false;
    }

    private void FillFirstTimeValues()
    {
        CiscoCcmCredBlock.Visible = false;
        AvayaCmCredBlock.Visible = false;
        VoipGatewaySettingsBlock.Visible = false;
       
        if (cmMode == EditCmPluginMode.CiscoCmOnly)
        {
            CiscoCcmCredBlock.Visible = true;
            cbEnableCDRPolling.Checked = EnableCDRPolling;
            cbEnableCDRPolling_CheckedChanged(this, EventArgs.Empty);
            axlConnectionControl.PreselectedCredentialsId = SelectedAxlCredentialsId;

            ftpSettingsControl.PreselectedCredentialsId = SelectedFtpCredentialsId;
            ftpSettingsControl.FTPServer = SelectedFtpServer;
            ftpSettingsControl.Frequency = SelectedFrequency;
            ftpSettingsControl.Path = SelectedPath;
            ftpSettingsControl.Port = SelectedPort;
            ftpSettingsControl.PassiveMode = SelectedPassiveMode;
            ftpSettingsControl.DeleteAfterDownload = SelectedDeleteFiles;
            ftpSettingsControl.SecuredConnection = SelectedSecuredConnection;

        }else if (cmMode == EditCmPluginMode.AvayaCmOnly)
        {
            AvayaCmCredBlock.Visible = true;
        }
    }

    private bool AreFtpConnectionInfoSame(VoipCCMFtpConnection info1, VoipCCMFtpConnection info2)
    {
        if (info1 == null && info2 == null)
            return true;
        if ((info1 == null && info2 != null) || (info1 != null && info2 == null))
            return false;

        return info1.CredentialID == info2.CredentialID
               && info1.FtpServer == info2.FtpServer
               && info1.Frequency == info2.Frequency
               && info1.Directory == info2.Directory
               && info1.Port == info2.Port
               && info1.PassiveMode == info2.PassiveMode
               && info1.DeleteDownloadedFiles == info2.DeleteDownloadedFiles
               && info1.SecureConnection == info2.SecureConnection;
    }

    private void SaveNodeData(int nodeId)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            if (EnableCDRPolling)
            {
                proxy.UpdateNodeAxlCredentials(nodeId, SelectedAxlCredentialsId);

                proxy.SaveVoipCCMFtpConnectionInfo(nodeId,
                    ftpSettingsControl.FTPServer,
                    ftpSettingsControl.Path,
                    SelectedFtpCredentialsId,
                    ftpSettingsControl.DeleteAfterDownload,
                    ftpSettingsControl.Frequency,
                    ftpSettingsControl.PassiveMode,
                    ftpSettingsControl.SecuredConnection,
                    ftpSettingsControl.Port);
            }
            else
            {
                proxy.DeleteVoipCCMFtpConnectionInfo(nodeId);
                proxy.DeleteNodeAxlCredentials(nodeId);
            }

        }
    }

    #endregion

    #region INodePropertyPlugin members

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        // save localy passed args
        _propertyBag = pluginState;
        _nodes = nodes;
        _mode = mode;
        _plugin = NodePropertyPluginManager.Plugins.Single(p => p.Name == PluginName);

        cmMode = DetectCmPluginMode(_nodes);       

        // all nodes must be voip nodes to display this control
        if (cmMode == EditCmPluginMode.NoCm && !IsNodeVoipGateway(_nodes))
        {
            _plugin.Visible = false;
            return;
        }

        if (!IsPostBack)
        {
            FillFirstTimeValues();

            switch (_mode)
            {
                case NodePropertyPluginExecutionMode.EditProperies:
                    _plugin.Visible = true;
                    InitializeCommonCallManagerSettings();
                    if (cmMode == EditCmPluginMode.CiscoCmOnly)
                        InitializeCiscoCallManagerSettings();
                    else if (cmMode == EditCmPluginMode.AvayaCmOnly)
                        InitializeAvayaCallManagerSettings();
                    break;
                case NodePropertyPluginExecutionMode.WizDefineNode:
                    _plugin.Visible = false;
                    break;
                default:
                    _plugin.Visible = false;
                    break;
            }
        }
        else
        {
            if (cmMode == EditCmPluginMode.CiscoCmOnly)
            {
                SelectedAxlCredentialsId = axlConnectionControl.CredentialsId;
                axlConnectionControl.PreselectedCredentialsId = SelectedAxlCredentialsId;

                axlConnectionControl.TargetEngine = _nodes[0].EngineID;
                axlConnectionControl.TargetIP = _nodes[0].IpAddress;
                
                ftpSettingsControl.TargetEngine = _nodes[0].EngineID;
                ftpSettingsControl.TargetIP = _nodes[0].IpAddress;

                SelectedFtpCredentialsId = ftpSettingsControl.CredentialsId;
                ftpSettingsControl.PreselectedCredentialsId = SelectedFtpCredentialsId;

                SelectedSecuredConnection = ftpSettingsControl.SecuredConnection;
                SelectedPort = ftpSettingsControl.Port;
                SelectedPath = ftpSettingsControl.Path;
                SelectedPassiveMode = ftpSettingsControl.PassiveMode;
                SelectedFtpServer = ftpSettingsControl.FTPServer;
                SelectedFrequency = ftpSettingsControl.Frequency;
                SelectedDeleteFiles = ftpSettingsControl.DeleteAfterDownload;

            }
            else if (cmMode == EditCmPluginMode.AvayaCmOnly)
            {
                AvayaCliSettingsControl.ValidationIp = _nodes[0].IpAddress;
                AvayaCliSettingsControl.NodeIds = _nodes.Select(x => x.ID);
            }
        }

        if (IsNodeVoipGateway(_nodes))
        {
            _plugin.Visible = true;
            InitializeVoipGatewaySettings(IsPostBack);
            CommonCcmSettings.Visible = false;           
        }
    }

    public bool Update()
    {
        bool res = true;
        res &= UpdateCmCommonSettings();
        res &= UpdateVoipGatewaySettings();

        if (cmMode == EditCmPluginMode.CiscoCmOnly)
            res &= UpdateCiscoCmSettings();
        else if (cmMode == EditCmPluginMode.AvayaCmOnly)
            res &= UpdateAvayaCmSettings();
        return res;
    }

    public bool Validate()
    {
        if (cmMode == EditCmPluginMode.CiscoCmOnly)
        {
            if (!ValidateCiscoCmInternal())
            {
                throw new ArgumentException(ErrorMessage);
            }
        }
        else if(cmMode == EditCmPluginMode.AvayaCmOnly)
        {
            if (!ValidateAvayaCmInternal())
            {
                throw new ArgumentException(ErrorMessage);
            }
        }
        return true;
    }

    #endregion

    #region private methods
    
    private void InitializeCommonCallManagerSettings()
    {
        if (_nodes.Count > 1)
        {
            MultipleSelectionPollingVisible = true;
            MultipleSelectionPollingSelected = false;

            tbCcmPollingInterval.Text = IpSlaSettings.CCMPollingIntervalMinutes.Value.ToString();
        }
        else
        {
            MultipleSelectionPollingVisible = false;
            MultipleSelectionPollingSelected = true;
            tbCcmPollingInterval.Text = GetCcmPollingFrequency(_nodes[0].ID).ToString();
        }
        tbCcmPollingInterval.Text = _nodes.Count > 1
                                        ? IpSlaSettings.CCMPollingIntervalMinutes.Value.ToString()
                                        : GetCcmPollingFrequency(_nodes[0].ID).ToString();
    }

    private EditCmPluginMode DetectCmPluginMode(IList<Node> nodes)
    {
        var nodesCount = nodes.Count;

        var cmsType = CallManagersDAL.Instance.GetCallManagersTypeByNodeId(nodes.Select(x => x.ID));
        
        if (cmsType == null)
            return EditCmPluginMode.NoCm;

        if (cmsType.Count(x => x.Value == CallManagerType.ACM) == nodesCount)
            return EditCmPluginMode.AvayaCmOnly;

        return cmsType.Count(x => x.Value == CallManagerType.CCM) == nodesCount ? EditCmPluginMode.CiscoCmOnly : EditCmPluginMode.MixedCm;
    }

    private bool IsNodeVoipGateway(IEnumerable<Node> nodes)
    {
        var gateways = VoipGatewaysDAL.Instance.GetGatewaysForNodes(nodes.Select(x => x.ID));
        return gateways.Any();
    }

    private void InitializeVoipGatewaySettings(bool isPostBack)
    {
        if (!isPostBack)
        {
            VoipGatewaySettingsBlock.Visible = true;
            var voipGateway = VoipGatewaysDAL.Instance.GetGatewayByNodeId(_nodes[0].ID);

            var sipTrunkMonitoringEnabled = voipGateway.SipTrunkMonitoringEnabled;
            VoipGatewaySipMonitoringSettingsControl.SipTrunkMonitoringEnabled = sipTrunkMonitoringEnabled.Value;

            int? maxConcurrentSipCalls = voipGateway.MaxConcurrentSipCalls;
            if (maxConcurrentSipCalls.HasValue)
            {
                VoipGatewaySipMonitoringSettingsControl.MaxConcurrentCallsOverwritten = true;
                VoipGatewaySipMonitoringSettingsControl.MaxConcurrentSipCalls = maxConcurrentSipCalls.Value.ToString();
                VoipGatewaySipMonitoringSettingsControl.OverwrittenMaxConcurrentSipCallsVisible = true;
            }
            else
            {
                VoipGatewaySipMonitoringSettingsControl.MaxConcurrentCallsOverwritten = false;
                VoipGatewaySipMonitoringSettingsControl.MaxConcurrentSipCalls = string.Empty;
                VoipGatewaySipMonitoringSettingsControl.OverwrittenMaxConcurrentSipCallsVisible = false;
            }
        }
        else
        {
            if (VoipGatewaySipMonitoringSettingsControl.MaxConcurrentCallsOverwritten)
            {
                VoipGatewaySettingsBlock.Visible = true;
                VoipGatewaySipMonitoringSettingsControl.OverwrittenMaxConcurrentSipCallsVisible = true;
            }
            else
            {
                VoipGatewaySipMonitoringSettingsControl.MaxConcurrentCallsOverwritten = false;
                VoipGatewaySipMonitoringSettingsControl.OverwrittenMaxConcurrentSipCallsVisible = false;
            }
        }

    }

    private int GetCcmPollingFrequency(int nodeId)
    {
        var frequency = CallManagersDAL.Instance.GetCallManagerPollingFrequency(nodeId);
        return frequency == 0 ? IpSlaSettings.CCMPollingIntervalMinutes.Value : frequency;
    }

    private void UpdateCcmPollingFrequency(IEnumerable<int> nodeIds, int interval)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            proxy.SetCallManagersPollingFrequency(nodeIds, interval);
        }
    }

    private void UpdateVoipGatewayMaxConcurrentSipCalls(IEnumerable<int> nodeIds, int? maxConcurrentSipCalls)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            proxy.SetVoipGatewayMaxConcurrentSipCalls(nodeIds, maxConcurrentSipCalls);
        }
    }

    private void UpdateVoipGatewaySipTrunkMonitoringEnabled(IEnumerable<int> nodeIds, bool sipTrunkMonitoringEnabled)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            proxy.SetVoipGatewaySipTrunkMonitoringEnabled(nodeIds, sipTrunkMonitoringEnabled);
        }
    }

    public int GetCcmSipTrunkPollingFrequency(int nodeId)
    {
        return CallManagersDAL.Instance.GetSipTrunkPollingFrequency(nodeId);
    }

    private void UpdateCcmSipTrunkMonitoringSettings(IEnumerable<int> nodeIds, bool sipTrunkMonitoringEnabled, int interval)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            proxy.SetCallManagersSipTrunkMonitoringSettings(nodeIds, sipTrunkMonitoringEnabled, interval);
        }
    }

    private bool UpdateCmCommonSettings()
    {
        switch (_mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
                return true;
            case NodePropertyPluginExecutionMode.WizChangeProperties: //Add node last step
                return true;
            case NodePropertyPluginExecutionMode.EditProperies: //Edit node

                if (_nodes != null && MultipleSelectionPollingSelected)
                {
                    var interval = int.Parse(tbCcmPollingInterval.Text);                   
                    UpdateCcmPollingFrequency(_nodes.Select(n => n.ID), interval);                    
                }               
                return true;
            default:
                return false;
        }
    }
  

    #region Avaya CM

    private void InitializeAvayaCallManagerSettings()
    {
        AvayaCliSettingsControl.ValidationIp = _nodes[0].IpAddress;
        AvayaCliSettingsControl.NodeIds = _nodes.Select(x => x.ID);


        if (_nodes.Count > 1)
        {
            AvayaMultipleSelectionPollingVisible = true;
            AvayaMultipleSelectionCliVisible = true;
            AvayaMultipleSelectionPollingSelected = false;
            AvayaMultipleSelectionCliSelected = false;
        }
        else
        {
            AvayaMultipleSelectionPollingVisible = false;
            AvayaMultipleSelectionCliVisible = false;
            AvayaMultipleSelectionPollingSelected = true;
            AvayaMultipleSelectionCliSelected = true;
        }

        EnableAvayaCDRPolling = CallManagersDAL.Instance.IsMonitoringEnabled(_nodes[0].ID);
    }

    private bool UpdateAvayaCmSettings()
    {
        var nodeIds = _nodes.Select(x => x.ID).ToList();

        if (AvayaMultipleSelectionCliSelected)
        {
           UpdateAvayaCli(nodeIds);
        }

        if (AvayaMultipleSelectionPollingSelected)
        {
            EnableAvayaMonitoring(nodeIds,EnableAvayaCDRPolling);
        }

        return true;
    }

    private void EnableAvayaMonitoring(IEnumerable<int> nodeIds, bool enable)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            proxy.EnableCallManagersMonitoring(nodeIds,enable);
        }
    }

    private void UpdateAvayaCli(IEnumerable<int> nodeIds)
    {
        if (AvayaCliSettingsControl.CredentialsId == 0)
        {

            CreateCliCredentials(AvayaCliSettingsControl.CredentialsName,
                                 AvayaCliSettingsControl.Username,
                                 AvayaCliSettingsControl.Password,
                                 AvayaCliSettingsControl.EnablePassword,
                                 AvayaCliSettingsControl.EnableLevel,
                                 nodeIds,
                                 AvayaCliSettingsControl.Port,
                                 AvayaCliSettingsControl.Timeout,
                                 AvayaCliSettingsControl.Protocol);
        }
        else
        {
            UpdateCliCredentialSettings(AvayaCliSettingsControl.CredentialsId, nodeIds, AvayaCliSettingsControl.Port,
                                 AvayaCliSettingsControl.Timeout,
                                 AvayaCliSettingsControl.Protocol);
        }
    }

    private int CreateCliCredentials(string credName, string userName, string password, string enabledPassword, int? enableLevel, IEnumerable<int> nodeIds, int port, int timeout, CliConnectionProtocol protocol)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            var credId = proxy.CreateCliCiscoCredentials(credName, userName, password, enabledPassword, enableLevel);
            foreach (var nodeId in nodeIds)
            {
                proxy.UpdateCliConnectionInfo(nodeId, credId, protocol, port, timeout);
            }
            return credId;
        }
    }

    private void UpdateCliCredentialSettings(int credId, IEnumerable<int> nodeIds, int port, int timeout, CliConnectionProtocol protocol)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            foreach (var nodeId in nodeIds)
            {
                proxy.UpdateCliConnectionInfo(nodeId, credId, protocol, port, timeout);
            }
        }
    }

    private bool ValidateAvayaCmInternal()
    {
        ErrorMessage = string.Empty;
        Page.Validate();

        if (!EnableAvayaCli || !AvayaMultipleSelectionCliSelected)
        {
            //This means we do not edit anything, so no need to validate
            return true;
        }

        if (!Page.IsValid)
        {
            ErrorMessage = InvalidDataMessage;
            return false;
        }

        bool success =true;

        try
        {
            using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
            {
                foreach (var node in _nodes)
                {
                    if (AvayaCliSettingsControl.CredentialsId == 0)
                    {
                        success &= proxy.ValidateCliConnectionInfo(node.EngineID,
                                                                   IPAddress.Parse(node.IpAddress),
                                                                   AvayaCliSettingsControl.Username,
                                                                   AvayaCliSettingsControl.Password,
                                                                   AvayaCliSettingsControl.EnablePassword,
                                                                   AvayaCliSettingsControl.EnableLevel,
                                                                   AvayaCliSettingsControl.Protocol,
                                                                   AvayaCliSettingsControl.Port,
                                                                   AvayaCliSettingsControl.Timeout, true);
                    }
                    else
                    {
                        success &= proxy.ValidateCliConnectionInfo(node.EngineID,
                                                                   IPAddress.Parse(node.IpAddress),
                                                                   AvayaCliSettingsControl.CredentialsId,
                                                                   AvayaCliSettingsControl.Protocol,
                                                                   AvayaCliSettingsControl.Port,
                                                                   AvayaCliSettingsControl.Timeout, true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            success = false;
        }

        if (!success)
            ErrorMessage = InvalidCliCredMessage;

        return success;
    }

    #endregion

    #region Cisco CM
    
    private bool ValidateCiscoCmInternal()
    {
        ErrorMessage = string.Empty;

        if (!EnableCDRPolling || !MultipleSelectionSelected)
        {
            //This means we do not edit anything, so no need to validate
            return true;
        }

        Page.Validate();

        bool allOK = Page.IsValid;
        bool axlOk = true;
        bool ftpOk = true;

        if (!allOK)
        {
            ErrorMessage = InvalidDataMessage;
        }
        else
        {
            foreach (Node node in _nodes)
            {
                axlOk &= TestAxlConnection(node.ID, node.IpAddress, axlConnectionControl.CredentialsId,
                    axlConnectionControl.Username, axlConnectionControl.Password);
                allOK &= axlOk;

                ftpOk &= TestFtpConnection(
                    node.EngineID,
                    node.IpAddress,
                    ftpSettingsControl.FTPServer,
                    ftpSettingsControl.CredentialsId,
                    ftpSettingsControl.Username,
                    ftpSettingsControl.Password,
                    ftpSettingsControl.Path,
                    ftpSettingsControl.SecuredConnection,
                    ftpSettingsControl.Port,
                    ftpSettingsControl.PassiveMode);
                allOK &= ftpOk;
            }
        }

        if (!axlOk)
            ErrorMessage += InvalidAxlCredentialsMessage + " ";
        if (!ftpOk)
            ErrorMessage += InvalidFtpSettingsMessage + " ";

        return allOK;
    }

    private int CreateAxlCredentials(string name, string username, string password)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            return proxy.CreateAxlCredentials(name, username, password);
        }
    }

    private int CreateFtpCredentials(string name, string username, string password)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            return proxy.CreateFtpCredentials(name, username, password);
        }
    }

    private void InitializeCiscoCallManagerSettings()
    {
        int sipPollingFrequency =  GetCcmSipTrunkPollingFrequency(_nodes[0].ID);
        if (sipPollingFrequency > 0)
        {
            SipPollingSettingsControl.SipTrunkMonitoringEnabled = true;
            SipPollingSettingsControl.SipTrunkPollingFrequency = sipPollingFrequency.ToString();
            SipPollingSettingsControl.SipTrunkPollinFrequencyVisible = true;
        }
        else
        {
            SipPollingSettingsControl.SipTrunkMonitoringEnabled = false;
            SipPollingSettingsControl.SipTrunkPollingFrequency = String.Empty;
            SipPollingSettingsControl.SipTrunkPollinFrequencyVisible = false;
        }

        axlConnectionControl.TargetEngine = _nodes[0].EngineID;
        axlConnectionControl.TargetIP = _nodes[0].IpAddress;
        axlConnectionControl.NodeIds = _nodes.Select(n => n.ID);

        ftpSettingsControl.TargetEngine = _nodes[0].EngineID;
        ftpSettingsControl.TargetIP = _nodes[0].IpAddress;
        ftpSettingsControl.NodeIds = _nodes.Select(n => n.ID);
        //Handle Multiple nodes.
        //The user has to explicitly check the box if they want to edit FTP credentials for multiple nodes.
        if (_nodes.Count > 1)
        {
            MultipleSelectionVisible = true;
            MultipleSelectionSelected = false;

        }
        else
        {
            MultipleSelectionVisible = false;
            MultipleSelectionSelected = true;
        }

        EnableCDRPolling = false;

        bool areAxlCredentialsDifferent = false;
        bool areFtpCredentialsDifferent = false;
        // check if credentials for nodes differ and display warning if they do
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(this.AppRelativeVirtualPath))
        {
            int lastAxlCredentials = 0;
            VoipCCMFtpConnection lastFtpConnection = null;
            bool first = true;

            foreach (Node node in _nodes)
            {
                int axlCredentialsId = proxy.GetNodeAxlCredentialsId(node.ID);
                VoipCCMFtpConnection info = proxy.GetVoipCCMFtpConnectionInfo(node.ID);

                if (first)
                {
                    lastAxlCredentials = axlCredentialsId;
                    lastFtpConnection = info;
                    first = false;
                }
                else
                {
                    if (lastAxlCredentials != axlCredentialsId)
                    {
                        areAxlCredentialsDifferent = true;
                    }

                    if (!AreFtpConnectionInfoSame(lastFtpConnection, info))
                    {
                        areFtpCredentialsDifferent = true;
                    }
                }
            }

            if (lastFtpConnection != null || lastAxlCredentials != 0)
            {
                EnableCDRPolling = true;
            }

            if (areAxlCredentialsDifferent)
            {
                differentAxlCredentialsWarningDiv.Visible = true;
            }

            if (areFtpCredentialsDifferent)
            {
                differentFtpSettingsWarningDiv.Visible = true;
            }
            else if (lastFtpConnection != null)
            {
                ftpSettingsControl.PreselectedCredentialsId = lastFtpConnection.CredentialID;
                ftpSettingsControl.FTPServer = lastFtpConnection.FtpServer;
                ftpSettingsControl.Frequency = lastFtpConnection.Frequency;
                ftpSettingsControl.Path = lastFtpConnection.Directory;
                ftpSettingsControl.Port = lastFtpConnection.Port;
                ftpSettingsControl.PassiveMode = lastFtpConnection.PassiveMode;
                ftpSettingsControl.DeleteAfterDownload = lastFtpConnection.DeleteDownloadedFiles;
                ftpSettingsControl.SecuredConnection = lastFtpConnection.SecureConnection;
            }
        }
    }

    private bool UpdateCiscoCmSettings()
    {
        if (SipPollingSettingsControl != null)
        {
            var sipFrequency = string.IsNullOrEmpty(SipPollingSettingsControl.SipTrunkPollingFrequency)
                ? 0
                : Convert.ToInt32(SipPollingSettingsControl.SipTrunkPollingFrequency);

            var sipTrunkMonitoringEnabled = EnableCDRPolling &&
                SipPollingSettingsControl.SipTrunkMonitoringEnabled && sipFrequency > 0;

            UpdateCcmSipTrunkMonitoringSettings(_nodes.Select(n => n.ID), sipTrunkMonitoringEnabled,
                sipTrunkMonitoringEnabled ? sipFrequency : 0);
        }

        if (!MultipleSelectionSelected)
        {
            return true;
        }

        if (EnableCDRPolling && axlConnectionControl.CredentialsId == 0)
        {
            // Create new credentials in credentials store
            SelectedAxlCredentialsId = CreateAxlCredentials(axlConnectionControl.CredentialsName, axlConnectionControl.Username, axlConnectionControl.Password);
            axlConnectionControl.PreselectedCredentialsId = SelectedAxlCredentialsId;
        }
        else
        {
            SelectedAxlCredentialsId = axlConnectionControl.CredentialsId;
        }

        if (EnableCDRPolling && ftpSettingsControl.CredentialsId == 0)
        {
            // Create new credentials in credentials store
            SelectedFtpCredentialsId = CreateFtpCredentials(ftpSettingsControl.CredentialsName, ftpSettingsControl.Username, ftpSettingsControl.Password);
            ftpSettingsControl.PreselectedCredentialsId = SelectedFtpCredentialsId;
        }
        else
        {
            SelectedFtpCredentialsId = ftpSettingsControl.CredentialsId;
        }

        switch (_mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
                return true;
            case NodePropertyPluginExecutionMode.WizChangeProperties: //Add node last step
                return true;
            case NodePropertyPluginExecutionMode.EditProperies: //Edit node
                if (_nodes != null)
                {
                    if (_nodes.Count > 0)
                    {
                        foreach (var node in _nodes)
                        {
                            SaveNodeData(node.ID);
                        }
                    }
                }

                ClearDataFromSession();
                return true;
            default:
                return false;
        }
    }

    private bool UpdateVoipGatewaySettings()
    {
        if (_nodes != null)
        {
            bool sipTrunkMonitoringEnabled = VoipGatewaySipMonitoringSettingsControl.SipTrunkMonitoringEnabled;

            UpdateVoipGatewaySipTrunkMonitoringEnabled(_nodes.Select(n => n.ID), sipTrunkMonitoringEnabled);

            int? maxConcurrentSipCalls = null;
            if (VoipGatewaySipMonitoringSettingsControl.MaxConcurrentCallsOverwritten)
                maxConcurrentSipCalls = int.Parse(VoipGatewaySipMonitoringSettingsControl.MaxConcurrentSipCalls);

            UpdateVoipGatewayMaxConcurrentSipCalls(_nodes.Select(n => n.ID), maxConcurrentSipCalls);
        }
        return true;
    }

    #endregion

    #endregion

    private enum EditCmPluginMode
    {
        NoCm, // selected nodes list (_nodes) does not contain any voip nodes (neither voip gateway/call manager) 
        AvayaCmOnly,
        CiscoCmOnly,
        MixedCm
    }
    
    
}