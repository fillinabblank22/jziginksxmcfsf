﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Voip_Controls_Wizards_Operations_OrionNodesInfo : System.Web.UI.UserControl
{
    public string Message { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        this.message.InnerText = (this.Message ?? "All nodes must exist in Orion NPM.");
    }
}
