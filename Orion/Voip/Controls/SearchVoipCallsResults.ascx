﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchVoipCallsResults.ascx.cs" Inherits="Orion_Voip_Controls_SearchVoipCallsResults" %>
<asp:ScriptManagerProxy ID="scriptManager" runat="server">
  <Services>
    <asp:ServiceReference Path="~/Orion/Voip/Services/VoipCallSearchService.asmx" />
    <asp:ServiceReference Path="~/Orion/Voip/Services/VoipWebUserSettingService.asmx" />
  </Services>
</asp:ScriptManagerProxy>
<div id="extArea" runat="server" class="voipSearchGrid"></div>