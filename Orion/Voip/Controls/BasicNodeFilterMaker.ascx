<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BasicNodeFilterMaker.ascx.cs" Inherits="Orion_Voip_Controls_BasicNodeFilterMaker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="atk" %>

<script language="javascript" type="text/javascript" src="/Orion/Voip/js/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript" src="/Orion/js/jquery/ui.core.js"></script>

<script type="text/javascript">
    function FieldChanged() {

        var tbox = document.getElementById('<%= this.ddlValue.ClientID %>');
        tbox.value = '';

        $('#<%= this.ddlField.ClientID %>').flushCache();
    }
</script>

<%--<script type="text/javascript">
    // Work around browser behavior of "auto-submitting" simple forms
    var frm = document.getElementById("aspnetForm");
    if (frm) {
        frm.onsubmit = function() { return false; };
    }
</script>--%>
            

<script type="text/javascript">
    function InitAutoComplete() {
        $("#<%= this.ddlValue.ClientID %>").autocomplete("/Orion/Voip/Services/AutoCompleteHandler.ashx"
            ,
            {
                extraParams:
                {
                    fieldName: function() {
                        return $("#<%= this.ddlField.ClientID %>").val();
                    },
                    dialogType: function() {
                        return "<%= this.FilterType.ToString() %>";
                    }
                },
                minChars: 0,
                delay: 100,
                cacheLength: 1
            });
        }

        function pageLoad() {
            InitAutoComplete();
        }
</script>

<table class="FilterMaker">
<thead>
    <tr>
        <th class="FilterHeader">Field:</th>
        <th class="FilterHeader">Comparison:</th>
        <th class="FilterHeader">Value:</th>
    </tr>
</thead>
   <tr>
        <td class="FilterBody">
            <asp:DropDownList ID="ddlField" runat="server" Width="110px" onchange="FieldChanged();"
            ></asp:DropDownList>
        </td>
        <td class="FilterBody">
            <asp:DropDownList ID="ddlComparison" runat="server" Width="110px" /><br />
        </td>
        <td class="FilterBody">
            <asp:TextBox ID="ddlValue" runat="server" Width="190px" AutoComplete="off" CssClass="ddlValueClass"></asp:TextBox>
        </td> 
   </tr>
</table>