﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls");

Orion.Voip.Controls.AxlTestResultControl = function(element) {
    Orion.Voip.Controls.AxlTestResultControl.initializeBase(this, [element]);
    this.elSuccess = null;
    this.elFailed = null;
    this.result = null;
};

Orion.Voip.Controls.AxlTestResultControl.prototype = {

    get_elSuccess: function () {
        return this.elSuccess;
    },
    set_elSuccess: function (value) {
        this.elSuccess = value;
    },
    get_elFailed: function () {
        return this.elFailed;
    },
    set_elFailed: function (value) {
        this.elFailed = value;
    },

    get_Result: function () {
        return this.result;
    },
    set_Result: function (value) {
        this.result = value;
        this.elFailed.style.display = (value != null && !value ? 'inline' : 'none');
        this.elSuccess.style.visibility = (value != null ? 'visible' : 'hidden');
        this.elSuccess.style.display = (value == null || value ? 'inline' : 'none');
    },

    initialize: function () {
        Orion.Voip.Controls.AxlTestResultControl.callBaseMethod(this, 'initialize');

        // Add custom initialization here
    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Controls.AxlTestResultControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.AxlTestResultControl.registerClass('Orion.Voip.Controls.AxlTestResultControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
