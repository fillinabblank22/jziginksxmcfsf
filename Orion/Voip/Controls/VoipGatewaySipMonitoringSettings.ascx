﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipGatewaySipMonitoringSettings.ascx.cs" Inherits="Orion.Voip.Controls.OrionVoipControlsVoipGatewaySipMonitoringSettingsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script type="text/javascript">

    function maxConcurrentCallsOverwrittenCheckBoxChanged() {

        if ($('#cbMaxConcurrentSipCallsOverwritten').is(":checked")) {
            $("#trMaxConcurrentSipCalls").css("visibility", "visible");
            
            if ($("#txtMaxConcurrentSipCalls").val().length === 0) {
                $("#txtMaxConcurrentSipCalls").val('100');
            }
            enableValidators(true);
        } else {
            $("#trMaxConcurrentSipCalls").css("visibility", "hidden");
            enableValidators(false);
        }
    }

    function enableValidators(enable){

        var reqMaxConcurrentSipCallsValidator = document.getElementById("reqMaxConcurrentSipCalls");
        var positiveMaxConcurrentSipCallsValidator = document.getElementById("positiveMaxConcurrentSipCalls");
        if (reqMaxConcurrentSipCallsValidator) {
            ValidatorEnable(reqMaxConcurrentSipCallsValidator, enable);
        }
        if (positiveMaxConcurrentSipCallsValidator) {
            ValidatorEnable(positiveMaxConcurrentSipCallsValidator, enable);
        }
    }

</script>
<link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />

<div id="divVoipGatewaySipMonitoringSettingsControl" runat="server" ClientIDMode="Static">
    <table class="voipSipTrunkPollingSettingsTable">
        <tr id="trVoipGatewayEnableSipTrunkMonitoring" runat="server" ClientIDMode="Static">
            <td class="leftLabelColumn">&nbsp;</td>
            <td>
                &ensp;<asp:CheckBox ID="cbSipTrunkMonitoringEnabled" ClientIDMode="Static" runat="server"/>
                <%=Resources.VNQMWebContent.VNQMWEB_GatewayEnableSipPolling%>
                <br />
            </td>
        </tr>
        <tr id="trVoipGatewayMaxConcurrentSipCalls" runat="server" ClientIDMode="Static">
            <td class="leftLabelColumn">
                <%=Resources.VNQMWebContent.VNQMWEB_BO_MaxConcurrentSipCalls%>:
            </td>
            <td>
                &ensp;<asp:CheckBox ID="cbMaxConcurrentSipCallsOverwritten" ClientIDMode="Static" runat="server" onchange="maxConcurrentCallsOverwrittenCheckBoxChanged()" />
                <%=Resources.VNQMWebContent.VNQMWEB_GatewayMaxConcurrentSipCallsOverride%>
                <br />
            </td>
        </tr>
        <tr id="trMaxConcurrentSipCalls" runat="server" ClientIDMode="Static">
            <td class="leftLabelColumn">
            </td>
            <td class="voipSipRightColumn">
                <asp:TextBox ID="txtMaxConcurrentSipCalls" ClientIDMode="Static" CssClass="sipTrunkPollingFrequencyTextBox" Width="15%" runat="server" MaxLength="6"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="txtMaxConcurrentSipCallsValidator" runat="server" TargetControlID="txtMaxConcurrentSipCalls" FilterType="Numbers" />
                <ajaxToolkit:FilteredTextBoxExtender ID="positiveMaxConcurrentSipCallsValidator" runat="server" TargetControlID="txtMaxConcurrentSipCalls" FilterType="Numbers" />

                <asp:RequiredFieldValidator runat="server" ClientIDMode="Static" ID="reqMaxConcurrentSipCalls" Enabled="False"
                                            ControlToValidate="txtMaxConcurrentSipCalls" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_112 %>" />
                <asp:CompareValidator ID="positiveMaxConcurrentSipCalls" runat="server"
                                      ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_213 %>"
                                      ControlToValidate="txtMaxConcurrentSipCalls"
                                      Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1" />
            </td>
        </tr>
    </table>
</div>