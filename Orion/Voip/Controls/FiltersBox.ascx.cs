using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.IpSla.Web.Filters;

public partial class Orion_Voip_Controls_FiltersBox : System.Web.UI.UserControl
{
    private ANDFilter _andFilter;
    private int _extItemIndex;
    public event EventHandler DeleteFilter;

   
    #region Properties

    public ANDFilter AndFILTER
    {
        get
        {
            return _andFilter;
        }
        set
        { 
            _andFilter = value;
        }
    }

    public int EXTItemIndex
    {
        get { return _extItemIndex; }
        set { _extItemIndex = value;}
    }
    #endregion

    protected void btnDeleteFilter_Click(object sender, EventArgs e)
    {
        
        if (this.DeleteFilter != null)
            this.DeleteFilter(sender, e);
    }
}
