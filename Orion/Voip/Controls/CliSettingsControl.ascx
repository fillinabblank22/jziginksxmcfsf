﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CliSettingsControl.ascx.cs" Inherits="Orion_Voip_Controls_CliSettingsControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web"%>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="ipsla" Namespace="SolarWinds.Orion.IpSla.Web.UI" Assembly="SolarWinds.Orion.IpSla.Web" %>
<%@ Register TagPrefix="ipsla" TagName="TestResultControl" Src="~/Orion/Voip/Controls/TestResultControl.ascx" %>

<orion:Include runat="server" Module="VoIP" File="Admin.js" />

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
  <Services>
    <asp:ServiceReference Path="~/Orion/Voip/Controls/Cli/CliConnectionControl.asmx" />
  </Services>
</asp:ScriptManagerProxy>

<script type="text/javascript">
    function VoipCliValidateName_<%= this.ClientID %>(sender, args) {
        if ($('#<%= this.hfReturnCredentialsId.ClientID %>').val() == 0
            && $('#<%= this.tbUsername.ClientID %>').val() == '') {
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }

    function VoipCliValidatePassword_<%= this.ClientID %>(sender, args) {
        if ($('#<%= this.hfReturnCredentialsId.ClientID %>').val() == 0
            && $('#<%= this.tbPassword.ClientID %>').val() == '') {
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }

    function ValidatePort(sender,args) {
        if($('#<%= this.tbPort.ClientID %>').val() == '') {
            sender.innerHTML = "<%= PortNumberRequiredFieldError %>";                
            args.IsValid = false;
            return;
        }
        else if($('#<%= this.tbPort.ClientID %>').val() <= 0 ) {
            sender.innerHTML = "<%= PortNumberMinValueError %>";
            args.IsValid = false;
            return;
        }
        else if($('#<%= this.tbPort.ClientID %>').val() > 65535 ) {
            sender.innerHTML = "<%= PortNumberMaxValueError %>";
            args.IsValid = false;
            return;
        }
        args.IsValid = true;
    }
    function ValidateTimeout(sender,args) {
        if($('#<%= this.tbTimeout.ClientID %>').val() == '') {
            sender.innerHTML = "<%= TimeoutRequiredFieldError %>";
            args.IsValid = false;
            return;
        }
        args.IsValid = true;
    }
</script>

<table class="cliSettingsTable">
<tbody>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_32 %>
        </td>
        <td colspan="9" class="SavedCredentialCls">
            <asp:HiddenField ID="hfReturnCredentialsId" runat="server"></asp:HiddenField>
            <asp:TextBox ID="ddlSavedCredentials" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_33 %>
        </td>
        <td colspan="8">
            <asp:TextBox ID="tbUsername" runat="server"></asp:TextBox>                      
        </td>    
        <td>
             <asp:CustomValidator runat="server" ID="userNameValidator" OnServerValidate="userNameValidator_OnServerValidate" 
            ControlToValidate="tbUsername" Display="Dynamic" ValidationGroup="VoipCliCredentials" ValidateEmptyText="True">
                <%= RequiredFieldError %>
            </asp:CustomValidator> 
        </td>  
    </tr>
    <tr class="input">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_34 %>
        </td>
        <td colspan="8">
            <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
             <asp:CustomValidator runat="server" ID="passwordValidator" OnServerValidate="passwordValidator_OnServerValidate" 
            ControlToValidate="tbPassword" Display="Dynamic" ValidationGroup="VoipCliCredentials" ValidateEmptyText="True">
                <%= RequiredFieldError %>
            </asp:CustomValidator> 
        </td>  
    </tr>
    <tr class="input" style="<%=this.HideEnableLevel?"display:none":""%>">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_35 %>
        </td>
        <td colspan="8" class="EnableLevelCls">
            <asp:HiddenField ID="hfReturnEnableLevelValue" runat="server"></asp:HiddenField>
            <asp:TextBox ID="ddlEnableLevel" runat="server"></asp:TextBox>
        </td>
        <td style="word-wrap: break-word; white-space: normal;padding-top:5px;">
            <a href="<%= HelpHelper.GetHelpUrl(HelpFragments.CliEnableLevelRequirements) %>" target="_blank" style="font-size: smaller;" class="sw-link"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_36 %></a>
        </td>
    </tr>
    <tr class="input" style="<%=this.HideEnableLevel?"display:none":""%>">
        <td class="col1Label">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_37%>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbEnablePassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label">
            <div style="padding-top: 5px; padding-bottom: 5px;">
                <ipsla:CollapseElement ID="btnAdvanced" runat="server" Collapsed="true">
                    <Handle><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_15%><span style="visibility: hidden;">:</span></Handle>
                </ipsla:CollapseElement>
            </div>
        </td>
        <td colspan="9"></td>
    </tr>
</tbody>
<tbody id="advancedBody" runat="server">
    <tr class="input">
        <td class="col1Label" style="padding-bottom: 0; margin-bottom: 0;">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_38 %>
        </td>
        <td colspan="9" style="padding-bottom: 0; margin-bottom: 0;" class ="ProtocolCls">
             <asp:HiddenField ID="hfReturnProtocolValue" runat="server"></asp:HiddenField>
            <asp:TextBox ID="ddlProtocol" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_39 %>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbPort" runat="server"></asp:TextBox>
             <ajaxToolkit:FilteredTextBoxExtender ID="tbPortTBE" runat="server" TargetControlID="tbPort" FilterType="Numbers"/>
              <asp:CustomValidator runat="server" ID="portNumberValidator" ClientValidationFunction="ValidatePort"
            ControlToValidate="tbPort" Display="Dynamic" ValidationGroup="VoipCliCredentials" ValidateEmptyText="True">               
            </asp:CustomValidator>
        </td>
    </tr>
    <tr class="input hint">
        <td class="col1Label">&nbsp;</td>
        <td colspan="9" style="word-wrap: break-word; white-space: normal">
	        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_40 %>
        </td>
    </tr>
    <tr class="input">
        <td class="col1Label caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_41 %>
        </td>
        <td colspan="9">
            <asp:TextBox ID="tbTimeout" runat="server"></asp:TextBox> <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_42%>
            <ajaxToolkit:FilteredTextBoxExtender ID="tbTimeoutTBE" runat="server" TargetControlID="tbTimeout" FilterType="Numbers"/>
             <asp:CustomValidator runat="server" ID="timeoutValidator" ClientValidationFunction="ValidateTimeout" 
            ControlToValidate="tbTimeout" Display="Dynamic" ValidationGroup="VoipCliCredentials" ValidateEmptyText="True">               
            </asp:CustomValidator>
        </td>
    </tr>
</tbody>
<tbody>
    <tr class="input">
        <td class="col1Label">&nbsp;</td>
        <td colspan="9">
            <div style="padding-top: 5px; padding-bottom: 5px;">
                <span style="vertical-align: middle; cursor: pointer;">
                    <orion:LocalizableButton runat="server" id="btnValidate" style="vertical-align: middle; cursor: pointer;" DisplayType="Small" LocalizedText="Test"  CausesValidation="true" ValidationGroup="VoipCliCredentials"/>
                </span>
	            <ipsla:TestResultControl ID="testResult" runat="server" />
	        </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="validationProgress" runat="server" style="display: none;">
                <img src="/Orion/images/animated_loading_sm3_whbg.gif" style="vertical-align: middle;" />
                <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_81%></span>
            </div>
        </td>
    </tr>
</tbody>
    
</table>
<%-- Render the DIV control only in demo mode --%>
<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
<%}%>