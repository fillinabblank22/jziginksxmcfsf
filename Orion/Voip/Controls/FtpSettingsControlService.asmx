<%@ WebService Language="C#" Class="Orion.Voip.Controls.Ftp.FtpSettingsControlService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web;

namespace Orion.Voip.Controls.Ftp
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class FtpSettingsControlService : WebService
    {
        private static readonly Log log = new Log();

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetCredentials()
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                IEnumerable<FtpCredentialsPublic> credentialsList;
                using (var blProxy = GetBusinessLayer())
                {
                    credentialsList = blProxy.GetFtpCredentials();
                }

                var list = (
                    from credentials in credentialsList
                    select new
                        {
                            id = credentials.Id,
                            name = credentials.Name,
                            user = credentials.Username
                        }
                    ).ToList();

                log.DebugFormat("FTP credentials retrieved. [Count={0}]", list.Count);
                return list;
            }
            catch (Exception ex)
            {
                log.Error("Error while getting FTP credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        public object ValidateFtpCredentialsId(int engineId, string nodeIp, string ftpServer, int credentialId, string directory, bool secured, int port, bool passiveMode)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                IPAddress ipAddress;
                if (!IPAddress.TryParse(nodeIp, out ipAddress))
                    throw new ApplicationException("Node IP address has invalid format.");
                
                using (var blProxy = GetBusinessLayer())
                {
                    return blProxy.ValidateVoipCCMFtpConnectionInfo(engineId, ipAddress, ftpServer, credentialId, directory, secured, port, passiveMode);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while validating FTP credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        public object ValidateFtpCredentials(int engineId, string nodeIp, string ftpServer, string username, string password, string directory, bool secured, int port, bool passiveMode)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                if (username == null)
                    throw new ArgumentNullException("username");
                if (password == null)
                    throw new ArgumentNullException("password");

                IPAddress ipAddress;
                if (!IPAddress.TryParse(nodeIp, out ipAddress))
                    throw new ApplicationException("Node IP address has invalid format.");
                
                using (var blProxy = GetBusinessLayer())
                {
                    return blProxy.ValidateVoipCCMFtpConnectionInfo(engineId, ipAddress, ftpServer, username, password, directory, secured, port, passiveMode);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while validating FTP credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetCommonFtpCredentials(IEnumerable<int> nodeIds)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            using (var blProxy = GetBusinessLayer())
            {
                int lastCredentialsId = 0;
                foreach (int nodeId in nodeIds)
                {
                    int credentialsId = blProxy.GetNodeFtpCredentialsId(nodeId);
                    if (lastCredentialsId == 0)
                    {
                        lastCredentialsId = credentialsId;
                    }
                    else if (lastCredentialsId != credentialsId)
                    {
                        lastCredentialsId = 0;
                        break;
                    }
                }

                return new
                           {
                               CredentialId = lastCredentialsId
                           };
            }
        }

        private IIpSlaBusinessLayer GetBusinessLayer()
        {
            return
                BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(
                    this.Context.Request.AppRelativeCurrentExecutionFilePath);
        }
    }
}
