<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InternalCustomChart.ascx.cs" 
    Inherits="Orion_Voip_Controls_InternalCustomChart" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<link rel="stylesheet" type="text/css" href="/Orion/Voip/Styles/Charts.css" />

<div runat="server" id="chartContainer"></div>

<br />

<div runat="server" id="editControlsContainer">
	<table width="640" style=""
	       border="0" cellspacing="0" cellpadding="2">
	
	<tr class="PageHeader DefaultShading" style="height: 2em;">
	    <td style="width: 5px;">&nbsp;</td>
		<td align="left" colspan="2"><%= VNQMWebContent.VNQMWEBDATA_TM0_61 %></td>
	    <td>
            <orion:LocalizableButton runat="server" ID="ImageButton1" 
                                     OnClick="OnRefresh" LocalizedText="Refresh" DisplayType="Primary" />
        </td>
		<td align="right">
		    <orion:LocalizableButtonLink runat="server" ID="RawDataLinkTop" DisplayType="Small"
                                         NavigateUrl="/Orion/Voip/Resources/ChartData.aspx" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_60 %>" />
		</td>
	</tr>
    <tr>
	    <td>&nbsp;</td>
    </tr>
    
	<tr>
	    <td>&nbsp;</td>
		<td colspan="3"><b><%=VNQMWebContent.VNQMWEBDATA_TM0_62 %></b></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>
	    <td style="width: 10px;">&nbsp;</td>
	    <td><%= VNQMWebContent.VNQMWEBDATA_TM0_63 %></td>
	    <td><asp:TextBox runat="server" ID="ChartTitle" Width="225px"></asp:TextBox></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><%= VNQMWebContent.VNQMWEBDATA_TM0_64 %></td>
	    <td><asp:TextBox runat="server" ID="SubTitle" Width="225px"></asp:TextBox></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><%= VNQMWebContent.VNQMWEBDATA_TM0_65 %></td>
	    <td><asp:TextBox runat="server" ID="SubTitle2" Width="225px"></asp:TextBox></td>
	</tr>

	<tr style="height: 20px;">
	    <td colspan="5"><hr class="Property" /></td>
    </tr>
	
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= VNQMWebContent.VNQMWEBDATA_TM0_67 %></b></td>
		<td>
            <asp:DropDownList ID="TimePeriodsList" runat="server" Width="230px"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><%= VNQMWebContent.VNQMWEBDATA_TM0_66 %></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= VNQMWebContent.VNQMWEBDATA_TM0_68 %></b></td>		
	</tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
        <td><%= VNQMWebContent.VNQMWEBDATA_TM0_69 %></td>	
        <td>
            <asp:TextBox runat="server" ID="PeriodBegin"  Width="225px"></asp:TextBox>
            <span style="color: Red;" runat="server" ID="PeriodBeginError" visible="false"><%= VNQMWebContent.VNQMWEBDATA_TM0_70 %></span>
        </td>
    </tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
        <td><%= VNQMWebContent.VNQMWEBDATA_TM0_71 %></td>	
        <td>
            <asp:TextBox runat="server" ID="PeriodEnd"  Width="225px"></asp:TextBox>
            <span style="color: Red;" runat="server" ID="PeriodEndError" visible="false"><%= VNQMWebContent.VNQMWEBDATA_TM0_70 %></span>
        </td>
    </tr>
    <tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
        <td style="font-size: 8pt;">
            <%= String.Format(VNQMWebContent.VNQMWEBDATA_TM0_72, DateTimeSyntaxExamples()) %>
        </td>
    </tr>
	
	<tr style="height: 20px;">
		<td colspan="5"><hr class="Property" /></td>		
	</tr>
	
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= VNQMWebContent.VNQMWEBDATA_TM0_73 %></b></td>
		<td>
            <asp:DropDownList ID="SampleSizeList" runat="server" Width = "230px"></asp:DropDownList>		    
		</td>
		
	</tr>

	<tr style="height: 20px;">
		<td colspan="5"><hr class="Property" /></td>		
	</tr>
	
	<tr>
	    <td>&nbsp;</td>	
		<td colspan="2"><b><%= VNQMWebContent.VNQMWEBDATA_TM0_74 %></b></td>
	</tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td><%= VNQMWebContent.VNQMWEBDATA_TM0_75 %></td>	
        <td><asp:TextBox ID="Width" Columns="10" MaxLength="4" runat="server"></asp:TextBox></td>
    </tr>
	<tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td><%= VNQMWebContent.VNQMWEBDATA_TM0_76 %></td>	
        <td><asp:TextBox ID="Height" Columns="10" MaxLength="4" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td>&nbsp;</td>	
	    <td style="font-size: 8pt;"><%= VNQMWebContent.VNQMWEBDATA_TM0_77 %></td>
    </tr>
	
	<tr class="Property DarkShading" style="height: 25px;">
	    <td>&nbsp;</td>	
	    <td colspan="2"></td>
        <td>
	        <orion:LocalizableButton runat="server" ID="LocalizableButton1" 
                                     OnClick="OnRefresh" LocalizedText="Refresh" DisplayType="Primary" />
        </td>
		<td align="right">
		    <orion:LocalizableButtonLink runat="server" ID="RawDataLinkBottom" DisplayType="Small"
                                         NavigateUrl="/Orion/Voip/Resources/ChartData.aspx" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_60 %>" />
		</td>
	</tr>
	
	
	</table>    
</div>