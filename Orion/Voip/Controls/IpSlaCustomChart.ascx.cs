using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Data.Database;

public partial class Orion_Voip_Controls_IpSlaCustomChart : UserControl
{
    private const int DefaultWidth = 640;
    private const string CustomText = "Custom";
    private bool enableEditing = true;
    private DataSet chartData;
    private VoIPChartInfo chartInfo;

    public bool EnableEditing
    {
        get { return enableEditing; }
        set { enableEditing = value; }
    }
    
    public DataSet ChartData
    {
        get { return chartData; }
        set { chartData = value; }
    }

    public VoIPChartInfo ChartInfo
    {
        get { return chartInfo; }
        set { chartInfo = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Filling dropdown list with periods
            foreach (string period in Periods.GenerateSelectionList())
            {
                TimePeriodsList.Items.Add(period);
            }
            TimePeriodsList.Items.Add(CustomText);

            SampleSizeList.DataSource = SampleSizes.GenerateSelectionList();
            SampleSizeList.DataTextField = "Name";
            SampleSizeList.DataValueField = "SizeName";
            SampleSizeList.DataBind();

            editControlsContainer.Visible = enableEditing;

            // Create the chart info with the default values.
            this.ChartInfo = GetDefaultChartInfo();

            // Load the chart info with value from the URL
            UpdateInfoFromRequest(this.ChartInfo);

            SampleSizeList.SelectedValue = this.ChartInfo.SampleSize;
            Width.Text = this.ChartInfo.Width.ToString();
            Height.Text = this.ChartInfo.Height.ToString();
            
            ChartBase chart = this.ChartInfo.CreateChart();

            this.ChartData = this.ChartInfo.LoadData();
            chart.DataBind(this.ChartData);

            chartContainer.Style["Width"] = String.Format("{0}px;", this.ChartInfo.Width);
            chartContainer.Controls.Add(chart);

            // Set the Export Url correctly.
            RawDataLinkTop.HRef = BuildUrl(RawDataLinkTop.HRef);
            RawDataLinkBottom.HRef = BuildUrl(RawDataLinkBottom.HRef);
        }
    }

    private VoIPChartInfo GetDefaultChartInfo()
    {
        var key = (OperationResultsKey)Enum.Parse(typeof(OperationResultsKey), this.ChartType);
        var info = new IpSlaOperationMinMaxAvgChartInfo(key);
        info.NetObject = NetObjectFactory.Create(this.NetObjectId);
        info.Width = DefaultWidth;
        info.Title = info.NetObject.Name;
        info.SubTitle = info.DisplayName;
        info.SubTitle2 = info.Period;
        return info;
    }

    private void UpdateInfoFromRequest(VoIPChartInfo info)
    {
        SetPeriodValue(info);
        info.SubTitle2 = info.Period;

        if (!String.IsNullOrEmpty(Request.Params["Title"]))
        {
            info.Title = Request.Params["Title"];
            ChartTitle.Text = Request.Params["Title"];
        }

        if (!String.IsNullOrEmpty(Request.Params["SubTitle"]))
        {
            info.SubTitle = Request.Params["SubTitle"];
            SubTitle.Text = Request.Params["SubTitle"];
        }
        
        if (!String.IsNullOrEmpty(Request.Params["SubTitle2"]))
        {
            info.SubTitle2 = Request.Params["SubTitle2"];
            SubTitle2.Text = Request.Params["SubTitle2"];
        }

        if (!String.IsNullOrEmpty(Request.Params["SampleSize"]))
        {
            info.SampleSize = Request.Params["SampleSize"];
        }

        int intValue;

        if (int.TryParse(Request.Params["Width"], out intValue))
        {
            info.Width = intValue;
        }

        if (int.TryParse(Request.Params["Height"], out intValue))
        {
            info.Height = intValue;
        }


    }

    private void SetPeriodValue(VoIPChartInfo info)
    {
        string period;
        if (!String.IsNullOrEmpty(period = Request.Params["Period"]))
        {
            if (period == CustomText)
            {
                TimePeriodsList.SelectedValue = period;
            }
            else
            {
                Period p = Periods.GetPeriod(period);
                if (p != null)
                {
                    TimePeriodsList.SelectedValue = p.Name;
                }
                else
                {
                    TimePeriodsList.SelectedValue = info.Period;
                }
            }
        }
        else
        {
            // set the default value.
            TimePeriodsList.SelectedValue = info.Period;
        }

        if (!String.IsNullOrEmpty(Request.Params["PeriodBegin"]))
        {
            PeriodBegin.Text = Request.Params["PeriodBegin"];
        }

        if (!String.IsNullOrEmpty(Request.Params["PeriodEnd"]))
        {
            PeriodEnd.Text = Request.Params["PeriodEnd"];
        }

        // If we have a custom start and end date, make sure we swith the combo to "custom"
        if ((!string.IsNullOrEmpty(PeriodBegin.Text)) && (!string.IsNullOrEmpty(PeriodEnd.Text)))
        {
            TimePeriodsList.SelectedValue = CustomText;
        }


        string periodSetting = TimePeriodsList.SelectedValue;

        if (periodSetting == CustomText)
        {
            // Show any error messages for the start and end dates
            PeriodBeginError.Visible = !IsDateValid(PeriodBegin.Text);
            PeriodEndError.Visible = !IsDateValid(PeriodEnd.Text);

            // They selected the custom text value.  Use the Period Begin and End values
            DateTime periodBegin;
            DateTime periodEnd;

            if (DateTime.TryParse(PeriodBegin.Text, out periodBegin) &&
                 DateTime.TryParse(PeriodEnd.Text, out periodEnd))
            {
                periodSetting = Periods.GetCustomPeriodString(periodBegin, periodEnd);
            }
            else
            {
                periodSetting = Periods.DEFAULT_PERIOD_VALUE;
            }
        }

        info.Period = periodSetting;

    }


    public string ChartType
    {
        get
        {
            return Request.Params["ChartType"];
        }
    }

    public string NetObjectId
    {
        get
        {
            return Request.Params["NetObjectId"];
        }
    }



    protected void OnRefresh(object sender, ImageClickEventArgs e)
    {
        Response.Redirect(BuildUrl(Request.Url.AbsolutePath));
    }

    private string BuildUrl(string basePage)
    {
        SolarWinds.Orion.IpSla.Web.UrlBuilder builder = new SolarWinds.Orion.IpSla.Web.UrlBuilder(basePage);

        builder["ChartType"] = this.ChartType;
        builder["NetObjectId"] = this.NetObjectId;

        if (!String.IsNullOrEmpty(ChartTitle.Text))
            builder["Title"] = ChartTitle.Text;

        if (!String.IsNullOrEmpty(SubTitle.Text))
            builder["SubTitle"] = SubTitle.Text;

        if (!String.IsNullOrEmpty(SubTitle2.Text))
            builder["SubTitle2"] = SubTitle2.Text;

        bool isCustom = TimePeriodsList.SelectedValue == CustomText;
        builder["Period"] = TimePeriodsList.SelectedValue;

        if (isCustom)
        {
            if (!String.IsNullOrEmpty(PeriodBegin.Text))
                builder["PeriodBegin"] = PeriodBegin.Text;

            if (!String.IsNullOrEmpty(PeriodEnd.Text))
                builder["PeriodEnd"] = PeriodEnd.Text;
        }

        if (!String.IsNullOrEmpty(SampleSizeList.SelectedValue))
            builder["SampleSize"] = SampleSizeList.SelectedValue;

        if (!String.IsNullOrEmpty(Width.Text))
            builder["Width"] = Width.Text;

        if (!String.IsNullOrEmpty(Height.Text))
            builder["Height"] = Height.Text;

        return builder.Url;
    }


    
    protected void OnPrintableVersion(object sender, ImageClickEventArgs e)
    {
        Response.Redirect(BuildUrl("/Orion/Voip/Resources/ViewIpSlaChart.aspx"));
    }

    protected static string DateTimeSyntaxExamples()
    {
        //<b>1/8/2008</b> or <b>08-Jan-08</b> or <b>11:58 AM</b> or <b>1/8/2008 11:58:10 AM</b> or <b>Jan 08,2008 11:58</b>
        DateTime dt = DateTime.Now;

        // 'dt' is already in local time (by specification of DateTime.Now property)
        return String.Format("<b>{0}</b> or <b>{1}</b> or <b>{2}</b>", 
                            dt.ToShortDateString(), 
                            dt.ToShortTimeString(), 
                            dt);
    }

    protected static bool IsDateValid(string text)
    {
        DateTime dt;
        return DateTime.TryParse(text, out dt);
    }


}
