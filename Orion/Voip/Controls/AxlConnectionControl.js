﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Voip.Controls.Axl");

Orion.Voip.Controls.Axl.AxlConnectionControl = function(element) {
    Orion.Voip.Controls.Axl.AxlConnectionControl.initializeBase(this, [element]);
    this.tbSavedCredentialsId = null;
    this.tbSavedCredentialsName = null;
    this.tbReturnCredentialsId = null;
    this.tbReturnCredentialsName = null;
    this.tbUsername = null;
    this.tbPassword = null;
    this.testResult = null;
    this.btnValidate = null;
    this.validationProgress = null;
    this.validationIp = null;
    this.validationEngineId = null;

    this.credentialsStore = null;
    this.extCredentialsCombo = null;
    this.extUsername = null;
    this.extPassword = null;

    this.nodeIds = null;
    this.preselectedCredentialsId = null;
    this.preselectedCredentialsName = null;
    this.preselectedCredentialsUsername = null;
    this.preselectedCredentialsPassword = null;
};

Orion.Voip.Controls.Axl.AxlConnectionControl.prototype = {

    get_ValidationIp: function () { return this.validationIp; },
    set_ValidationIp: function (value) {
        this.validationIp = value;
        if (this.btnValidate) {
            this.btnValidate.style.display = (value != null ? 'inline-block' : 'none');
        }
        if (this.testResult) {
            this.testResult.set_Result(null);
        }
    },

    get_ValidationEngineId: function () { return this.validationEngineId; },
    set_ValidationEngineId: function (value) { this.validationEngineId = value; },

    get_NodeIds: function () { return this.nodeIds; },
    set_NodeIds: function (nodeIds) { this.nodeIds = nodeIds; },

    PreFillNodeCredentials: function (nodeIds) {
        this.testResult.set_Result(null);
        Orion.Voip.Controls.Axl.AxlConnectionControlService.GetCommonAxlCredentials(
            nodeIds,
            Function.createDelegate(this, function (result, context) {
                this.set_CredentialId(result.CredentialId);
            }),
            function (error, context) {
                // ignore
            },
            null
        );
    },

    get_CredentialId: function () {
        return this.extCredentialsCombo.getValue();
    },
    set_CredentialId: function (value) {
        if (value) {
            this.extCredentialsCombo.setValue();

            var loadCallbackScope = { credentialId: value, sender: this };
            var loadOptions = {
                callback: function (r, options, success) {
                    var credentialId = this.credentialId;
                    var storeIndex = this.sender.credentialsStore.findBy(function (record, id) { return (record.data.id == credentialId); });
                    this.sender.RefreshCredentialsContents(storeIndex);
                },
                scope: loadCallbackScope
            };

            this.credentialsStore.load(loadOptions);
        } else if (this.preselectedCredentialsName.length == 0) {
            this.extCredentialsCombo.clearValue();
        }
    },

    get_CredentialName: function () { return this.extCredentialsCombo.getRawValue(); },
    set_CredentialName: function (value) { this.extCredentialsCombo.setRawValue(value); },

    get_Username: function () { return this.extUsername.getValue(); },
    get_Password: function () { return this.extPassword.getValue(); },

    get_PreselectedCredentialsId: function () { return this.preselectedCredentialsId; },
    set_PreselectedCredentialsId: function (value) { this.preselectedCredentialsId = value; },

    get_PreselectedCredentialsName: function () { return this.preselectedCredentialsName; },
    set_PreselectedCredentialsName: function (value) { this.preselectedCredentialsName = value; },

    get_PreselectedCredentialsUsername: function () { return this.preselectedCredentialsUsername; },
    set_PreselectedCredentialsUsername: function (value) { this.preselectedCredentialsUsername = value; },

    get_PreselectedCredentialsPassword: function () { return this.preselectedCredentialsPassword; },
    set_PreselectedCredentialsPassword: function (value) { this.preselectedCredentialsPassword = value; },

    get_tbSavedCredentialsId: function () { return this.tbSavedCredentialsId; },
    set_tbSavedCredentialsId: function (value) { this.tbSavedCredentialsId = value; },

    get_tbSavedCredentialsName: function () { return this.tbSavedCredentialsName; },
    set_tbSavedCredentialsName: function (value) { this.tbSavedCredentialsName = value; },

    get_tbReturnCredentialsId: function () { return this.tbReturnCredentialsId; },
    set_tbReturnCredentialsId: function (value) { this.tbReturnCredentialsId = value; },

    get_tbReturnCredentialsName: function () { return this.tbReturnCredentialsName; },
    set_tbReturnCredentialsName: function (value) { this.tbReturnCredentialsName = value; },

    get_tbUsername: function () { return this.tbUsername; },
    set_tbUsername: function (value) { this.tbUsername = value; },

    get_tbPassword: function () { return this.tbPassword; },
    set_tbPassword: function (value) { this.tbPassword = value; },

    get_testResult: function () { return this.testResult; },
    set_testResult: function (value) { this.testResult = value; },

    get_btnValidate: function () { return this.btnValidate; },
    set_btnValidate: function (value) { this.btnValidate = value; },

    get_validationProgress: function () { return this.validationProgress; },
    set_validationProgress: function (value) { this.validationProgress = value; },

    ShowValidationMask: function (btnValidateElement) {
        var isAddCallManagerWizardExist = $('#AddCallManagerWizard').length > 0;

        if (isAddCallManagerWizardExist) {
            //enable test,next buttons
            var btnNextElement, btnBackElement, btnCancelElement;

            btnNextElement = $('#'+GetNextButtonClientID());
            btnBackElement = $('#'+GetBackButtonClientID());
            btnCancelElement = $('#'+GetCancelButtonClientID());

            var disableLinks = new Array(btnValidateElement, btnNextElement, btnBackElement, btnCancelElement);

            for (var i = 0; i < disableLinks.length; i++) {
                var link = $(disableLinks[i]);
                // get the 'href' and 'onclick' attribute values
                var linkHRef = link.attr('href');
                var linkOnClick = link.attr('onclick');

                // set the dummy attribute values
                link.attr('href', '#').attr('onclick', 'return false;');

                // add a temporary attribute to keep the original 'href' value
                if (linkHRef != undefined)
                    link.attr('tmphref', linkHRef);

                // add a temporary attribute to keep the original 'onclick' value
                if (linkOnClick != undefined)
                    link.attr('tmponclick', linkOnClick);

                // add the class disabled links
                link.addClass('sw-btn-disabled');
            }
        }

        this.validationProgress.style.display = 'block';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'hidden');
        }
    },

    HideValidationMask: function (btnValidateElement) {
        
        this.validationProgress.style.display = 'none';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'visible');
        }

        var isAddCallManagerWizardExist = $('#AddCallManagerWizard').length > 0;

        if (isAddCallManagerWizardExist) {
            //enable test,next buttons
            var btnNextElement, btnBackElement, btnCancelElement;

            btnNextElement = $('#'+GetNextButtonClientID());
            btnBackElement = $('#'+GetBackButtonClientID());
            btnCancelElement = $('#'+GetCancelButtonClientID());

            var enableLinks = new Array(btnValidateElement, btnNextElement, btnBackElement, btnCancelElement);

            for (var i = 0; i < enableLinks.length; i++) {
                var link = $(enableLinks[i]);
                // get the original values of 'href' and 'onclick' attributes from temporary attributes
                var originalLinkHRef = link.attr('tmphref');
                var originalLinkOnClick = link.attr('tmponclick');

                // restore the original 'href' value
                if (originalLinkHRef != undefined)
                    link.attr('href', originalLinkHRef);

                // restore the original 'onclick' value
                if (originalLinkOnClick != undefined)
                    link.attr('onclick', originalLinkOnClick);

                // remove temporary attributes
                link.removeAttr('tmphref').removeAttr('tmponclick');

                // remove the class disabled links
                link.removeClass('sw-btn-disabled');
            }
        }
    },

    RefreshCredentialsContents: function (storeIndex, callback) {
        var disableCredEdit = true;
        var name = this.get_CredentialName();
        var id = 0;
        var record = null;
        if (storeIndex == null) {
            storeIndex = this.credentialsStore.findBy(function (record, id) { return (record.data.name == name); });
            storeIndex = storeIndex >= 0 ? storeIndex : null;
        }
        if (storeIndex != null) {
            record = this.credentialsStore.getAt(storeIndex);
            id = record != null ? record.data.id : '';
            this.extCredentialsCombo.setValue(id);
        } else if (name == '') {
            this.extCredentialsCombo.clearValue();
            if (this.preselectedCredentialsName != '') {
                this.extCredentialsCombo.setRawValue(this.preselectedCredentialsName);
            }
            name = this.get_CredentialName();
        } else {
            // clear ID but preserve name
            this.extCredentialsCombo.clearValue();
            this.extCredentialsCombo.setRawValue(name);
        }
        if (record == null && name.length > 0) {
            disableCredEdit = false;
        }
        else {
            disableCredEdit = true;
        }

        // ExtJs combo with editable text has strange behavior when we need to read result value
        // from C#. Let's store values in separate fields that combo will not rewrite.
        $('#' + this.tbReturnCredentialsId).val(id);
        $('#' + this.tbReturnCredentialsName).val(name);

        this.extUsername.setDisabled(disableCredEdit);
        this.extUsername.setValue(record ? record.data.user : this.preselectedCredentialsUsername);
        this.extPassword.setDisabled(disableCredEdit);
        this.extPassword.setValue(record ? '********' : this.preselectedCredentialsPassword);
    },

    btnValidate_Click: function (btnValidateElement) {
        if ($("#isDemoModeAxl").length > 0) {
            alert('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_101;E=js}');
            return false;
        }
        var credentialId = this.get_CredentialId();
        var credentialName = this.get_CredentialName();
        var userName = this.get_Username();
        this.ShowValidationMask(btnValidateElement);
        if (credentialId || (credentialName && credentialName.length > 0 && userName && userName.length > 0)) {
            var successHandler = Function.createDelegate(this, function (result, context) {
                this.HideValidationMask(btnValidateElement);
                this.testResult.set_Result(result);
            });
            var errorHandler = Function.createDelegate(this, function (error, context) {
                this.HideValidationMask(btnValidateElement);
                window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_102;E=js}", error.get_message()));
            });
            if (credentialId) {
                Orion.Voip.Controls.Axl.AxlConnectionControlService.ValidateAxlConnectionCredentialId(
                    this.nodeIds,
                    this.get_ValidationIp(),
                    credentialId,
                    successHandler,
                    errorHandler,
                    null
                );
            } else {
                Orion.Voip.Controls.Axl.AxlConnectionControlService.ValidateAxlConnectionCredentials(
                    this.nodeIds,
                    this.get_ValidationIp(),
                    userName, this.get_Password(),
                    successHandler,
                    errorHandler,
                    null
                );
            }
        }
    },

    syncSize: function () {
        this.extCredentialsCombo.syncSize();
    },

    validate: function () {
        return true;
    },
    initialize: function () {
        Orion.Voip.Controls.Axl.AxlConnectionControl.callBaseMethod(this, 'initialize');

        this.credentialsStore = new Ext.data.Store({
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '/Orion/Voip/Controls/AxlConnectionControl.asmx/GetCredentials',
                method: 'POST',
                headers: { 'Content-type': 'application/json' }
            }),
            reader: new Ext.data.JsonReader({
                root: 'd',
                id: 'id'
            }, [
                { name: 'id', type: 'int' },
                { name: 'name', type: 'string' },
                { name: 'user', type: 'string' },
                { name: 'level' }
            ]),
            sortInfo: { field: 'name', direction: "ASC" }
        });

        this.extCredentialsCombo = new Ext.form.ComboBox({
            applyTo: this.tbSavedCredentialsName,
            hiddenId: this.tbSavedCredentialsId,
            hiddenName: this.tbSavedCredentialsId + "_name",
            store: this.credentialsStore,
            displayField: 'name',
            valueField: 'id',
            mode: 'local',
            triggerAction: 'all',
            emptyText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_41;E=js}',
            width: 200,
            lazyRender: true,
            autoSelect: false,
            typeAhead: true,
            enableKeyEvents: true,
            listClass: 'x-menu x-menu-neutralize',
            listeners: {
                blur: Function.createDelegate(this, function (field, newValue, oldValue) {
                    this.RefreshCredentialsContents(null);
                }),
                keypress: Function.createDelegate(this, function (field, eventObject) {
                    var delegate = Function.createDelegate(this, function () { this.RefreshCredentialsContents(null); });
                    setTimeout(function () { delegate(); }, 0);
                }),
                select: Function.createDelegate(this, function (combo, record, index) {
                    this.RefreshCredentialsContents(index);
                })
            }
        });
        this.extUsername = new Ext.form.TextField({
            applyTo: this.tbUsername,
            disabledClass: 'x-item-disabled',
            width: 200
        });
        this.extPassword = new Ext.form.TextField({
            applyTo: this.tbPassword,
            disabledClass: 'x-item-disabled',
            inputType: 'password',
            width: 200
        });

        if (this.preselectedCredentialsId) {
            this.set_CredentialId(this.preselectedCredentialsId);
        }
        else if (this.nodeIds) {
            this.PreFillNodeCredentials(this.nodeIds);
        }
        else if ($('#' + this.tbReturnCredentialsName).val()) {
            this.set_CredentialName($('#' + this.tbReturnCredentialsName).val());
        }

        this.RefreshCredentialsContents(null);

        this.set_ValidationIp(this.validationIp);
    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Controls.Axl.AxlConnectionControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Axl.AxlConnectionControl.registerClass('Orion.Voip.Controls.Axl.AxlConnectionControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
