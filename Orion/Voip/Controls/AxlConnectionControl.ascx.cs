using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Controls_Axl_AxlConnectionControl : ScriptUserControlBase
{
    protected static readonly string SelectCredentialsLabel = VNQMWebContent.VNQMWEBCODE_VB1_92;
    protected static readonly string UsernameLabel = VNQMWebContent.VNQMWEBCODE_VB1_93;
    protected static readonly string PasswordLabel = VNQMWebContent.VNQMWEBCODE_VB1_94;
    protected static readonly string RequiredFieldError = VNQMWebContent.VNQMWEBCODE_VB1_95;

    protected void Page_Load(object sender, EventArgs e)
    {
        btnValidate.Attributes["onclick"] = "if (Page_ClientValidate('VoipAxlCredentials')) { $find('" + ClientID +
                                            "').btnValidate_Click($(this)); } return false;";
        userNameValidator.ClientValidationFunction = "VoipAxlValidateName_" + ClientID;
        if (NodeIds != null)
        {
            ViewState["NodeIds"] = NodeIds.ToList();
        }
        if (IsPostBack)
        {
            // passwords are not sent on postback but we need control to keep its value
            tbPassword.Attributes["value"] = tbPassword.Text;
            NodeIds = ViewState["NodeIds"] as IEnumerable<int>;
        }
    }

    public int CredentialsId
        => string.IsNullOrEmpty(tbReturnCredentialsId.Value) ? 0 : int.Parse(tbReturnCredentialsId.Value);

    public string CredentialsName
    {
        get { return tbReturnCredentialsName.Value; }
        set { tbReturnCredentialsName.Value = value; }
    }

    public string Username
    {
        get { return tbUsername.Text; }
        set { tbUsername.Text = value; }
    }

    public string Password
    {
        get { return tbPassword.Text; }
        set { tbPassword.Text = value; }
    }

    public int PreselectedCredentialsId { get; set; }
    public string TargetIP { get; set; }
    public int TargetEngine { get; set; }
    public IEnumerable<int> NodeIds { get; set; }

    protected void userNameValidator_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CredentialsId == 0 && string.IsNullOrEmpty(Username.Trim()))
            args.IsValid = false;
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.Axl.AxlConnectionControl", ClientID);
        descriptor.AddProperty("tbSavedCredentialsId", tbSavedCredentialsId.ClientID);
        descriptor.AddProperty("tbSavedCredentialsName", tbSavedCredentialsName.ClientID);
        descriptor.AddProperty("tbReturnCredentialsId", tbReturnCredentialsId.ClientID);
        descriptor.AddProperty("tbReturnCredentialsName", tbReturnCredentialsName.ClientID);
        descriptor.AddElementProperty("tbUsername", tbUsername.ClientID);
        descriptor.AddElementProperty("tbPassword", tbPassword.ClientID);
        descriptor.AddComponentProperty("testResult", testResult.ClientID);
        descriptor.AddElementProperty("btnValidate", btnValidate.ClientID);
        descriptor.AddElementProperty("validationProgress", validationProgress.ClientID);

        descriptor.AddProperty("ValidationIp", TargetIP);
        descriptor.AddProperty("ValidationEngineId", TargetEngine);
        descriptor.AddProperty("NodeIds", NodeIds);
        descriptor.AddProperty("PreselectedCredentialsId", (IsPostBack ? CredentialsId : PreselectedCredentialsId));
        descriptor.AddProperty("PreselectedCredentialsName", CredentialsName);
        descriptor.AddProperty("PreselectedCredentialsUsername", Username);
        descriptor.AddProperty("PreselectedCredentialsPassword", Password);

        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(ResolveUrl("AxlConnectionControl.js"));
    }

    #endregion
}
