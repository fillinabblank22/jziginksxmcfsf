using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Resources;

public partial class Orion_Voip_Resources_RelativePeriod : System.Web.UI.UserControl
{
    private const string DefaultRelativePeriod = "Last 4 Hours";
    private readonly string DefaultLocalizedRelativePeriod = string.Format(VNQMWebContent.VNQMWEBDATA_TM0_102, 4, VNQMWebContent.VNQMWEBDATA_TM0_98);
    [PersistenceMode(PersistenceMode.Attribute)]
    public string TimePeriod
    {
        get
        {
            if (!string.IsNullOrEmpty(txtQuantity.Text))
                return string.Format("Last {0} {1}", this.txtQuantity.Text, this.lbxTimeType.SelectedValue);
            else
                return DefaultRelativePeriod;
        }
        set
        {
            if (string.IsNullOrEmpty(value))
                return;

            //TODO: Consider situation for 'Last hour', 'Today' or any absolute time period
            if (value.Equals("Last hour", StringComparison.InvariantCultureIgnoreCase) ||
                value.Equals("Today", StringComparison.InvariantCultureIgnoreCase) ||
                value.Equals("Yesterday", StringComparison.InvariantCultureIgnoreCase) ||
                value.Equals("Last Month", StringComparison.InvariantCultureIgnoreCase) ||
                value.Equals("Last 3 Months", StringComparison.InvariantCultureIgnoreCase) ||
                value.Contains("~"))
            {
                return;
            }

            ///TODO: refactor all this indian crap (!!!!)
            
            string[] parts = value.Split(' ');
            if (parts.Length != 3) 
                ThrowFormatException();
            if (!parts[0].Equals("Last", StringComparison.InvariantCultureIgnoreCase))
                ThrowFormatException();

            int quant;
            if (!int.TryParse(parts[1], out quant))
                ThrowFormatException();

            this.txtQuantity.Text = quant.ToString();

            bool found = false;
            foreach (ListItem item in this.lbxTimeType.Items)
            {
                if (item.Value.Equals(parts[2], StringComparison.InvariantCultureIgnoreCase))
                {
                    if (null != lbxTimeType.SelectedItem)
                        lbxTimeType.SelectedItem.Selected = false;
                    item.Selected = true;
                    found = true;
                    break;
                }
            }
            if (!found)
                ThrowFormatException();
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string LocalizedTimePeriod
    {
        get
        {
            if (!string.IsNullOrEmpty(txtQuantity.Text))
                return string.Format(VNQMWebContent.VNQMWEBDATA_TM0_102, this.txtQuantity.Text, this.lbxTimeType.SelectedItem.Text);
            else
                return DefaultLocalizedRelativePeriod;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool AutoPostback
    {
        get { return (null == ViewState["AutoPostback"]) ? false /* default */ : (bool)ViewState["AutoPostback"]; }
        set
        {
            ViewState["AutoPostback"] = value;
            this.lbxTimeType.AutoPostBack = value;
            this.txtQuantity.AutoPostBack = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ValidationEnabled
    {
        get
        {
            return this.valQuantityNumeric.Enabled || valQuantityRequired.Enabled;
        }
        set
        {
            this.valQuantityRequired.Enabled = value;
            this.valQuantityNumeric.Enabled = value;
        }
    }

    private const string CLIENT_DISABLE_TEMPLATE = @"
document.getElementById('{0}').disabled = {2};
document.getElementById('{1}').disabled = {2};
";

    public string GetClientDisableScript()
    {
        return string.Format(CLIENT_DISABLE_TEMPLATE, this.txtQuantity.ClientID, this.lbxTimeType.ClientID, "true");
    }

    public string GetClientEnableScript()
    {
        return string.Format(CLIENT_DISABLE_TEMPLATE, this.txtQuantity.ClientID, this.lbxTimeType.ClientID, "false");
    }

    private void ThrowFormatException()
    {
        throw new FormatException("Input string does not represent a valid relative time period.");
    }

    
}
