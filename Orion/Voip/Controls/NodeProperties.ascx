﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeProperties.ascx.cs"
    Inherits="Orion_Voip_Controls_NodeProperties" %>
<%@ Register TagPrefix="ipsla" TagName="SipPollingSettingsControl" Src="~/Orion/Voip/Controls/SipPollingSettingsControl.ascx" %>
<%@ Register TagPrefix="ipsla" TagName="AxlConnectionControl" Src="~/Orion/Voip/Controls/AxlConnectionControl.ascx" %>
<%@ Register TagPrefix="ipsla" TagName="FtpSettingsControl" Src="~/Orion/Voip/Controls/FtpSettingsControl.ascx" %>
<%@ Register TagPrefix="ipsla" TagName="CliSettingsControl" Src="~/Orion/Voip/Controls/CliSettingsControl.ascx" %>
<%@ Register TagPrefix="ipsla" TagName="VoipGatewaySipMonitoringSettingsControl" Src="~/Orion/Voip/Controls/VoipGatewaySipMonitoringSettings.ascx" %>
<div class="contentBlock">
    <div id="CommonCcmSettings" runat="server">
        <div class="contentBlockHeader">
            <asp:CheckBox ID="cbMultipleSelectionPolling" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelectionPolling_CheckedChanged" />
            <%=Resources.VNQMWebContent.VNQMWEBCODE_VM_2 %>
        </div>
        <div class="blueBox" id="CallManagerPollingBlock">
            <table>
                <tr>
                    <td class="leftLabelColumn">
                        <%=Resources.VNQMWebContent.VNQMWEBCODE_VM_1 %>
                    </td>
                    <td>
                    <td class="rightInputColumn" nowrap="nowrap">
                        <asp:TextBox runat="server" ID="tbCcmPollingInterval" MaxLength="6" Enabled="False" />

                        <asp:CompareValidator ID="CompareValidator3" runat="server"
                            Operator="GreaterThanEqual" Type="Integer"
                            ErrorMessage="*"
                            Display="Dynamic"
                            ValueToCompare="1"
                            ControlToValidate="tbCcmPollingInterval" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                            Display="Dynamic"
                            ErrorMessage="*"
                            ControlToValidate="tbCcmPollingInterval" />
                        <%=Resources.VNQMWebContent.VNQMWEBDATA_PG_12 %>
                    </td>
                </tr>
            </table>
        </div>
        <div id="CiscoCcmCredBlock" runat="server">
            <div class="contentBlockHeader" runat="server">
                <table>
                    <tr>
                        <td class="contentBlockModuleHeader">
                            <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
                            <%= PluginTitle %>
                        </td>
                        <td class="rightInputColumn">
                            <asp:CheckBox ID="cbEnableCDRPolling" runat="server" AutoPostBack="True" OnCheckedChanged="cbEnableCDRPolling_CheckedChanged" Enabled="false" />
                            <asp:Label runat="server" ID="lblHelpfulText" CssClass="helpfulText"><%= PluginEnableLabelHelpfulText %></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div></div>
            <div class="blueBox" runat="server" id="CDRSettingsBox">
                <ipsla:SipPollingSettingsControl runat="server" ID="SipPollingSettingsControl" />

                <table>
                    <tr>
                        <td class="leftLabelColumn">
                            <div class="sw-voip-edit-node-section-header"><%= AXLCredentialsHeader %></div>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblAXLCredHelpfulText" CssClass="helpfulText"><%= AXLCredentialsHeaderLabelHelpfulText%></asp:Label>
                        </td>
                    </tr>
                </table>
                <div class="sw-suggestion" runat="server" id="differentAxlCredentialsWarningDiv" visible="False" style="margin: 1em;">
                    <span class="sw-suggestion-icon"></span>
                    <%= DifferentAxlCredentialsWarning %>
                </div>
                <ipsla:AxlConnectionControl runat="server" ID="axlConnectionControl" />
                <br />

                <table>
                    <tr>
                        <td class="leftLabelColumn">
                            <div class="sw-voip-edit-node-section-header"><%= FTPSettingsHeader %></div>
                        </td>
                    </tr>
                </table>
                <div class="sw-suggestion" runat="server" id="differentFtpSettingsWarningDiv" visible="False" style="margin: 1em;">
                    <span class="sw-suggestion-icon"></span>
                    <%= DifferentFtpCredentialsWarning %>
                </div>
                <ipsla:FtpSettingsControl runat="server" ID="ftpSettingsControl" />
            </div>
        </div>
        <div id="AvayaCmCredBlock" runat="server" class="contentBlockHeader">

            <div class="contentBlock">
                <div class="contentBlockHeader">
                    <table>
                        <tr>
                            <td class="contentBlockModuleHeader">
                                <asp:CheckBox runat="server" ID="cbAvayaMultipleSelectionPolling" AutoPostBack="True" OnCheckedChanged="cbAvayaMultipleSelectionPolling_CheckedChanged" />
                                <%=Resources.VNQMWebContent.VNQMWEBCODE_VM_3 %>
                            </td>

                        </tr>

                    </table>
                </div>
                <div class="blueBox">
                    <div style="padding-left: 10px;">
                        <asp:CheckBox ID="cbAvayaEnableCDRPolling" runat="server" AutoPostBack="True" OnCheckedChanged="cbAvayaEnableCDRPolling_CheckedChanged" Enabled="False" />
                        <asp:Label runat="server" ID="lblAvayaHelpfulText" CssClass="helpLink"><%=Resources.VNQMWebContent.VNQMWEBCODE_SS_1 %></asp:Label>
                    </div>
                </div>
            </div>

            <div class="contentBlock">
                <div class="contentBlockHeader">
                    <asp:CheckBox ID="cbAvayaMultipleSelectionCli" runat="server" AutoPostBack="True" OnCheckedChanged="cbAvayaMultipleSelectionCli_CheckedChanged" />
                    <%=Resources.VNQMWebContent.VNQMWEBCODE_VM_4 %>
                </div>
                <div class="blueBox" runat="server" id="AvayaCDRSettingsBox">
                    <div style="padding-left: 10px;">
                        <ipsla:CliSettingsControl ID="AvayaCliSettingsControl" runat="server" HideEnableLevel="True" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="VoipGatewaySettingsBlock" runat="server">
        <div class="contentBlock">
            <div class="contentBlockHeader">
                <table>
                    <tr>
                        <td class="contentBlockModuleHeader"><%=Resources.VNQMWebContent.VNQMWEB_BO_VoipGatewayProperties %></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="blueBox" runat="server" id="VoipGatewaySipMonitoringSettingsBox">
            <div style="padding-left: 10px;">
                <ipsla:VoipGatewaySipMonitoringSettingsControl runat="server" ID="VoipGatewaySipMonitoringSettingsControl" />
            </div>
        </div>
    </div>
</div>


