﻿using System;
using SolarWinds.Orion.IpSla.Web.Charting;


public partial class Orion_Voip_Controls_ChartImage : System.Web.UI.Page
{
	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
	    ChartHandler.ProcessRequest(this.Context);
	}

	#endregion
}