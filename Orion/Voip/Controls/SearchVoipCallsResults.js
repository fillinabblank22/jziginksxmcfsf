﻿/*globals Type,Sys,Ext,ORION,Orion,SolarWinds*/
/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls");

Orion.Voip.Controls.SearchVoipCallsResults = function (element) {
    Orion.Voip.Controls.SearchVoipCallsResults.initializeBase(this, [element]);
    this.extArea = null;
    this.mainPanel = null;
    this.maxPageSize = 100;
    this.submitButtonId = null;
	this.lblSearchCriteriaId = null;
    this.pageSizeBoxTop = null;
    this.pagingToolbarTop = null;
    this.pageSizeBoxBottom = null;
    this.pagingToolbarBottom = null;
    this.resultsGrid = null;
    this.resultsPanel = null;
    this.resultsStore = null;
    this.resultsLimit = null;
    this.groupByPanel = null;
    this.groupByCombo = null;
    this.selectPanel = null;
    this.groupByStore = null;
    this.groupByList = null;
    this.loadingMask = null;
    this.selectorModel = null;
    this.searchCriteria = null;
	this.exportData = null;
};

Orion.Voip.Controls.SearchVoipCallsResults.prototype = {
    initialize: function () {
        Orion.Voip.Controls.SearchVoipCallsResults.callBaseMethod(this, 'initialize');

        // Add custom initialization here

        $(Function.createDelegate(this, this.initializeExtJs));
    },

    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Controls.SearchVoipCallsResults.callBaseMethod(this, 'dispose');
    },

    get_extArea: function () {
        return this.extArea;
    },
    set_extArea: function (value) {
        this.extArea = value;
    },

    get_maxPageSize: function () {
        return this.maxPageSize;
    },
    set_maxPageSize: function (value) {
        this.maxPageSize = value;
    },

    get_submitButtonId: function () {
        return this.submitButtonId;
    },
    set_submitButtonId: function (value) {
        this.submitButtonId = value;
    },
	get_lblSearchCriteriaId: function () {
        return this.lblSearchCriteriaId;
    },
    set_lblSearchCriteriaId: function (value) {
        this.lblSearchCriteriaId = value;
    },

    initializeExtJs: function () {
        Ext.QuickTips.init();
        this.initGroupByPanel();
        this.initResultsPanel();

        this.mainPanel = new Ext.Panel({
            id: 'mainPanel',
            layout: 'border',
            autoWidth: true,
            height: 400,
            renderTo: this.extArea,
            items: [this.groupByPanel, this.resultsPanel]
        });

        SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.GetVoipCallSearchResultsLimit(Function.createDelegate(this, this.onGetSearchResultsLimitSuccess),
            Function.createDelegate(this, this.onGetSearchResultsLimitError));
    },

    initResultsPanel: function () {
        this.resultsStore = new Ext.data.Store({
            id: 'resultsStore',
            proxy: new Ext.data.ScriptServiceProxy({
                scriptServiceMethod: SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.GetVoipCallSearchResults,
                methodArgs: ['searchCriteria', 'groupByCategory', 'groupByValue', 'orderBy', 'orderDirection', 'start', 'limit']
            }),
            reader: new Ext.data.ArrayReader({
                totalProperty: 'TotalRows',
                root: 'DataTable.Rows'
            }, [
                { name: 'ID', type: 'int' },
                { name: 'CcmID', type: 'int' },
                { name: 'CallID', type: 'int' },
                { name: 'CallingPartyNumber', type: 'string' },
                { name: 'FinalCalledPartyNumber', type: 'string' },
                { name: 'OrigDeviceName', type: 'string' },
                { name: 'OrigLicensed', type: 'int' },
                { name: 'OrigCCMRegionName', type: 'string' },
                { name: 'DestDeviceName', type: 'string' },
                { name: 'DestLicensed', type: 'int' },
                { name: 'DestCCMRegionName', type: 'string' },
                { name: 'Caption', type: 'string' },
                { name: 'OrigIpAddr', type: 'string' },
                { name: 'DestIpAddr', type: 'string' },
	            { name: 'CallStatus', type: 'bool' },
	            { name: 'OrigMosText', type: 'string' },
	            { name: 'DestMosText', type: 'string' },
                { name: 'OrigMosQuality', type: 'int' },
                { name: 'DestMosQuality', type: 'int' },
	            { name: 'OrigJitterText', type: 'string' },
	            { name: 'DestJitterText', type: 'string' },
                { name: 'OrigJitterQuality', type: 'int' },
                { name: 'DestJitterQuality', type: 'int' },
	            { name: 'OrigLatencyText', type: 'string' },
	            { name: 'DestLatencyText', type: 'string' },
                { name: 'OrigLatencyQuality', type: 'int' },
                { name: 'DestLatencyQuality', type: 'int' },
	            { name: 'OrigPacketLossText', type: 'string' },
	            { name: 'DestPacketLossText', type: 'string' },
                { name: 'OrigPacketLossQuality', type: 'int' },
                { name: 'DestPacketLossQuality', type: 'int' },
	            { name: 'StartTime', type: 'string' },
	            { name: 'EndTime', type: 'string' },
                { name: 'DateTimeDisconnectUtcBin', type: 'string' },
                { name: 'IncomingCallProtocol', type: 'string' },
                { name: 'OutgoingCallProtocol', type: 'string' }
            ]),
            paramNames: {
                sort: 'orderBy',
                dir: 'orderDirection'
            },
            remoteSort: true,
            sortInfo: { field: 'EndTime', direction: 'DESC' }
        });

        this.resultsStore.addListener("exception", Function.createDelegate(this, this.onDataStoreException));

        this.pageSizeBoxTop = new Ext.form.NumberField({
            id: 'pageSizeFieldTop',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: this.maxPageSize,
            value: this.getPageSize()
        });

        this.pageSizeBoxTop.on('change', Function.createDelegate(this, this.onPageSizeBoxChange));

        this.pagingToolbarTop = new Ext.PagingToolbar({
            store: this.resultsStore,
            pageSize: this.getPageSize(),
            displayInfo: true,
            displayMsg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_75;E=js}',
            emptyMsg: "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_76;E=js}",
            items: [
                    '-',
                    "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_77;E=js}",
                    this.pageSizeBoxTop
                ]
        });

        this.pageSizeBoxBottom = new Ext.form.NumberField({
            id: 'pageSizeFieldBottom',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: this.maxPageSize,
            value: this.getPageSize()
        });

        this.pageSizeBoxBottom.on('change', Function.createDelegate(this, this.onPageSizeBoxChange));

        this.pagingToolbarBottom = new Ext.PagingToolbar({
            store: this.resultsStore,
            pageSize: this.getPageSize(),
            displayInfo: true,
            displayMsg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_75;E=js}',
            emptyMsg: "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_76;E=js}",
            items: [
                    '-',
                    "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_77;E=js}",
                    this.pageSizeBoxBottom
                ]
        });


        this.selectorModel = new Ext.sw.voip.grid.RadioSelectionModel({
            hiddable: false
        });

        this.selectorModel.on("selectionchange", Function.createDelegate(this, this.updateButtonsState));

        this.resultsGrid = new Ext.grid.GridPanel({
            id: 'resultsGrid',
            store: this.resultsStore,
            region: 'center',
            layout: 'fit',
            columns: [
                this.selectorModel,
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_PG_16;E=js}', hidden: true, hideable: false, width: 100, sortable: true, dataIndex: 'ID' },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_78;E=js}', dataIndex: 'CallID', sortable: false, resizable: true, hidden: true, renderer: Function.createDelegate(this, this.renderCallId) },
            //                { header: 'CCM ID', dataIndex: 'CcmID', sortable: true, resizable: true },
            //                { header: 'ORIG IP', dataIndex: 'OrigIpAddr', sortable: true, resizable: true },
            //                { header: 'ORIG DEV NAME', dataIndex: 'OrigDeviceName', sortable: true, resizable: true },
            //                { header: 'DEST IP', dataIndex: 'DestIpAddr', sortable: true, resizable: true },
            //                { header: 'DEST DEV NAME', dataIndex: 'DestDeviceName', sortable: true, resizable: true },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_79;E=js}', dataIndex: 'CallingPartyNumber', sortable: false, resizable: true },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_80;E=js}', dataIndex: 'OrigCCMRegionName', sortable: false, resizable: true },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_81;E=js}', dataIndex: 'FinalCalledPartyNumber', sortable: false, resizable: true },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_82;E=js}', dataIndex: 'DestCCMRegionName', sortable: false, resizable: true },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_83;E=js}', dataIndex: 'Caption', sortable: false, resizable: true },
	            { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_84;E=js}', dataIndex: 'CallStatus', resizable: true, renderer: Function.createDelegate(this, this.renderCallStatus) },
	            { header: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_85;E=js}", "<span class='headerHint'>", "</span>"), dataIndex: 'OrigMosText', resizable: true, renderer: Function.createDelegate(this, this.renderMos) },
	            { header: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_86;E=js}", "<span class='headerHint'>", "</span>"), dataIndex: 'OrigJitterText', resizable: true, renderer: Function.createDelegate(this, this.renderJitter) },
	            { header: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_87;E=js}", "<span class='headerHint'>", "</span>"), dataIndex: 'OrigLatencyText', resizable: true, renderer: Function.createDelegate(this, this.renderLatency) },
                { header: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_88;E=js}", "<span class='headerHint'>", "</span>"), dataIndex: 'OrigPacketLossText', resizable: true, renderer: Function.createDelegate(this, this.renderPacketLoss) },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_89;E=js}', dataIndex: 'StartTime', sortable: false, resizable: true },
	            { header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_90;E=js}', dataIndex: 'EndTime', sortable: false, resizable: true },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_BO_IncomingCallProtocol;E=js}', dataIndex: 'IncomingCallProtocol', sortable: false, resizable: true },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_BO_OutgoingCallProtocol;E=js}', dataIndex: 'OutgoingCallProtocol', sortable: false, resizable: true },
                { id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1}],
            stripeRows: true,
            sm: this.selectorModel,
            autoExpandColumn: 'filler',
            tbar: this.pagingToolbarTop,
            bbar: this.pagingToolbarBottom,
            disabled: true,
            viewConfig: {
                getRowClass: Function.createDelegate(this, this.getRowClass)
            }

        });

        this.resultsPanel = new Ext.Panel({
            id: 'resultsPanel',
            region: 'center',
            layout: 'fit',
            frame: false,
            split: true,
            items: [this.resultsGrid],
            tbar: [
					{
					    id: 'viewCallDetailsButton',
					    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_91;E=js}',
					    iconCls: 'callButtonIcon',
					    handler: Function.createDelegate(this, this.onViewCallDetails)

					}, '-', {
					    id: 'viewOriginDetailsButton',
					    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_92;E=js}',
					    iconCls: 'phoneButtonIcon',
					    handler: Function.createDelegate(this, this.onViewOriginDetails)
					}, '-', {
					    id: 'viewDestinationDetailsButton',
					    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_93;E=js}',
					    iconCls: 'phoneButtonIcon',
					    handler: Function.createDelegate(this, this.onViewDestinationDetails)
					}, '-', {
					    id: 'ExportToExcelButton',
					    text: '@{R=VNQM.Strings;K=VNQMWEBJS_VS_1;E=js}',
					    iconCls: 'exportToExcelButtonIcon',
					    handler: Function.createDelegate(this, this.onExportToExcel)
						
					}]
        });
        this.updateButtonsState();
    },

    initGroupByPanel: function () {
        this.groupByCombo = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: [
                    ['none', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_94;E=js}'],
                    ['origRegion', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_95;E=js}'],
                    ['destRegion', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_96;E=js}']]
            }),
            valueField: 'itemValue',
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            layout: 'fit',
            editable: false,
            listeners: {
                select: Function.createDelegate(this, this.onGroupByComboSelect)
            }
        });

        this.selectPanel = new Ext.Container({
            region: 'north',
            height: 25,
            layout: 'fit',
            items: [this.groupByCombo]
        });

        var tpl = new Ext.XTemplate(
		    '<tpl for=".">',
                '<div class="voipSearchGroupByItem" id="{Value}">{Label}</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
	    );

        this.groupByStore = new Ext.data.Store({
            id: 'groupByStore',
            proxy: new Ext.data.ScriptServiceProxy({
                scriptServiceMethod: SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.GetGroupByItems,
                methodArgs: ['groupByCategory']
            }),
            reader: new Ext.data.ArrayReader({
                totalProperty: 'TotalRows',
                root: 'DataTable.Rows'
            }, [
                  { name: 'Value', mapping: 0 },
                  { name: 'Label', mapping: 1 },
                  { name: 'Type', mapping: 2 }
            ])
        });

        this.groupByList = new Ext.DataView({
            cls: 'voipSearchGroupByList',
            border: true,
            store: this.groupByStore,
            tpl: tpl,
            //height: SEUM_GetManagementGridHeight() - selectPanel.getSize().height,
            autoHeight: false,
            height: 345,
            autoScroll: true,
            singleSelect: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            itemSelector: 'div.voipSearchGroupByItem',
            listeners: {
                selectionchange: Function.createDelegate(this, this.onGroupByListSelectionChange)
            }
        });

        this.groupByPanel = new Ext.Panel({
            id: 'groupByPanel',
            title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_97;E=js}',
            region: 'west',
            width: 200,
            split: true,
            collapsible: true,
            items: [
                this.selectPanel,
                this.groupByList
            ]
        });

        var groupByCategoryToSet = this.getSettingValue('groupByCategory');
        if (this.groupByCombo.getStore().find('itemValue', groupByCategoryToSet) != -1) {
            this.groupByCombo.setValue(groupByCategoryToSet);
            this.refreshGroupByList(groupByCategoryToSet, Function.createDelegate(this, this.refreshGroupByValueFromUserSettings));
        }
        else {
            this.groupByCombo.setValue('none');
        }


        //        this.groupingPanel.on('bodyresize', function () {
        //            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        //        });
    },

    refreshGroupByValueFromUserSettings: function () {
        var groupByValueToSet = this.getSettingValue('groupByValue');
        if (!groupByValueToSet) {
            return;
        }
        var itemIndex = this.groupByList.getStore().find('Value', groupByValueToSet);
        if (itemIndex != -1) {
            this.groupByList.select(itemIndex, false, true);
        }
    },

	onExportToExcel: function () {
        var filename = document.title;
        var searchCondition = document.getElementById(this.lblSearchCriteriaId).textContent;

        $.ajax('/Orion/Voip/ReportAsExcel.aspx?filename=' + encodeURIComponent(filename) + '&searchCondition=' + encodeURIComponent(searchCondition), {
            async: false,
            cache: false,
            type: 'post',
            //data: txt,
            contentType: 'application/json; charset=UTF8',
            dataType: 'json',
            success: function (d) {
                if (d && d.url)
                    window.location = d.url;
            }
        });
    },
	
    onViewCallDetails: function () {
        this.disableButtons();
        var record = this.selectorModel.getSelected();
        if (typeof (record) == undefined) {
            this.updateButtonsState();
            return;
        }
        if (record.data.FinalCalledPartyNumber != 'unlicensed' || record.data.CallingPartyNumber != 'unlicensed') {
            var id = record.data.ID;
            window.location = '/orion/voip/calldetails.aspx?CallDetailsID=' + id;
        }
        else {
            this.showUnlicensed();
        }
    },
    showUnlicensed: function () {
        var htmlToDisplay = String.format(' <div ><table><tr> <td><img src="/Orion/Voip/images/lightbulb_tip_16x16.gif"></td> <td>{0}</td></tr>', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_98;E=js}');
        htmlToDisplay = htmlToDisplay + String.format('<tr> </tr> <tr> <td> &nbsp;</td> </tr><tr><td>&nbsp;</td><td class ="voipSearchPageLink">&raquo;<a class="sw-link" href="../Admin/Details/ModulesDetailsHost.aspx"> {0} </a></td></tr><tr><td> &nbsp;</td> </tr>', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_99;E=js}');
        htmlToDisplay = htmlToDisplay + String.format('<tr><td>&nbsp;</td><td class ="voipSearchPageLink">&raquo;<a class="sw-link" href ="http://www.solarwinds.com/embedded_in_products/productLink.aspx?id=upgrade_my_license"> {0} <a></td></tr></table> </div>', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_100;E=js}');
        this.showUnlicenseDialog = new Ext.Window({
            title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_101;E=js}',
            layout: 'fit',
            width: 590,
            height: 220,
            closeAction: 'hide',
            closable: true,
            modal: true,
            layout: 'anchor',
            items: [{
                title: '',
                html: htmlToDisplay,
                anchor: 'top 90%',
                autoScroll: false
            }],
            buttons: [{
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_63;E=js}',
                handler: Function.createDelegate(this, function () {
                    this.showUnlicenseDialog.hide();
                    var selItems = this.resultsGrid.getSelectionModel();
                    var count = selItems.getCount();
                    var map = this.resultsPanel.getTopToolbar().items.map;
                    map.viewCallDetailsButton.setDisabled(count === 0);
                    map.viewOriginDetailsButton.setDisabled(count === 0);
                    map.viewDestinationDetailsButton.setDisabled(count === 0);
					map.ExportToExcelButton.setDisabled(count === 0);
                })
            }]
        });

        this.showUnlicenseDialog.show(this);
    },

    onViewOriginDetails: function () {
        this.viewPartyDetails(true);
    },

    onViewDestinationDetails: function () {
        this.viewPartyDetails(false);
    },

    viewPartyDetails: function (origin) {
        this.disableButtons();
        var record = this.selectorModel.getSelected();
        if (typeof (record) == undefined) {
            return;
        }
        var voipCCMMonitoringID = record.data.CcmID;
        var deviceName = origin ? record.data.OrigDeviceName : record.data.DestDeviceName;
        var ext = origin ? record.data.CallingPartyNumber : record.data.FinalCalledPartyNumber;
        var ipAddress = origin ? record.data.OrigIpAddr : record.data.DestIpAddr;
        var phoneNumber = origin ? record.data.CallingPartyNumber : record.data.FinalCalledPartyNumber;
        if (phoneNumber != 'unlicensed') {
            SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.GetDetailUrl(voipCCMMonitoringID, deviceName, ext, ipAddress,
            Function.createDelegate(this, this.onGetDetailUrlSuccess),
            Function.createDelegate(this, this.onGetDetailUrlError));
        }
        else {
            this.showUnlicensed();
        }
    },

    onGetDetailUrlSuccess: function (result, context) {
        if (result) {
            window.location = result;
            return;
        }
        else {
            Ext.Msg.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_102;E=js}',
                msg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_103;E=js}',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO
            });

        }
        this.updateButtonsState();
    },

    onGetDetailUrlError: function (error, context) {
        this.checkSessionExpired(error);
        if (error.get_statusCode() > 0) {
            Ext.Msg.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}',
                msg: String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_104;E=js}', '<br/><br/>' + error.get_message()),
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR

            });
        }
        this.updateButtonsState();
    },

    checkSessionExpired: function (error) {
        if (error.get_statusCode() == 401 || error.get_statusCode() == 403) {
            alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_105;E=js}');
            window.location.reload();
            return;
        }
    },

    updateButtonsState: function () {
        var selItems = this.resultsGrid.getSelectionModel();
        var count = selItems.getCount();
        var map = this.resultsPanel.getTopToolbar().items.map;
		var rowCount = this.resultsGrid.getStore().data.getCount();

        if (count != 0) {
            map.viewCallDetailsButton.addClass("voipSearchButton");
            map.viewOriginDetailsButton.addClass("voipSearchButton");
            map.viewDestinationDetailsButton.addClass("voipSearchButton");
			map.ExportToExcelButton.addClass("voipSearchButton");
        }
        else {
            map.viewCallDetailsButton.removeClass("voipSearchButton");
            map.viewOriginDetailsButton.removeClass("voipSearchButton");
            map.viewDestinationDetailsButton.removeClass("voipSearchButton");
			map.ExportToExcelButton.addClass("voipSearchButton");
        }

        map.viewCallDetailsButton.setDisabled(count === 0);
        map.viewOriginDetailsButton.setDisabled(count === 0);
        map.viewDestinationDetailsButton.setDisabled(count === 0);
		map.ExportToExcelButton.setDisabled(count === 0 && rowCount === 0);

    },

    disableButtons: function () {
        var map = this.resultsPanel.getTopToolbar().items.map;
        map.viewCallDetailsButton.setDisabled(true);
        map.viewOriginDetailsButton.setDisabled(true);
        map.viewDestinationDetailsButton.setDisabled(true);
		map.ExportToExcelButton.setDisabled(true);
    },

    onDataStoreException: function (dataProxy, type, action, options, response, arg) {
        this.checkSessionExpired(arg);
        Ext.Msg.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}",
            msg: arg.get_exceptionType() + ': ' + arg.get_message(),
            icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
        });
        if (this.loadingMask) {
            this.loadingMask.hide();
        }
        this.updateButtonsState();
        $('#' + this.submitButtonId).removeAttr('searchInProgress');
    },

    onPageSizeBoxChange: function (f, numbox, o) {
        var pSize = parseInt(numbox, 10);
        var errorMsg = String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_106;E=js}", this.maxPageSize);
        if (isNaN(pSize) || pSize < 1 || pSize > this.maxPageSize) {
            Ext.Msg.show({
                title: "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}",
                msg: errorMsg,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            return;
        }

        if (this.pagingToolbarTop.pageSize != pSize) { // update page size only if it is different
            this.pageSizeBoxTop.setValue(pSize);
            this.pageSizeBoxBottom.setValue(pSize);
            this.pagingToolbarTop.pageSize = pSize;
            this.pagingToolbarBottom.pageSize = pSize;
            this.setSettingValue('searchGridPageSize', pSize);
            this.pagingToolbarTop.doLoad(this.pagingToolbarTop.cursor);
        }

    },

    getSettingValue: function (name) {
        return SolarWinds.Orion.IpSla.Web.UI.WebUserSettingsManager.GetProvider().GetValue(name);
    },

    setSettingValue: function (name, value) {
        SolarWinds.Orion.IpSla.Web.UI.WebUserSettingsManager.GetProvider().SetValue(name, value);
    },

    getPageSize: function () {
        var settingValue = this.getSettingValue('searchGridPageSize');
        if (isNaN(settingValue)) {
            return parseInt('40');
        }
        return parseInt(settingValue);
    },

    onGroupByListSelectionChange: function (view, selections) {
        this.loadGrid();
        this.setSettingValue('groupByValue', this.getGroupByValue());
    },

    onGroupByComboSelect: function (record, index) {
        this.refreshGroupByList(record.value);
        this.setSettingValue('groupByCategory', record.value);
    },

    getGroupByValue: function () {
        var records = this.groupByList.getSelectedRecords();
        if (records.length > 0) {
            return records[0].data.Value;
        }
        return '';
    },

    refreshGroupByList: function (groupByCategory, handler) {
        this.groupByStore.baseParams = { groupByCategory: groupByCategory };
        if (handler) {
            var internalHandler = Function.createDelegate(this, function () {
                handler();
                this.groupByStore.un('load', internalHandler);
            });
            this.groupByStore.on('load', internalHandler);
        }

        this.groupByStore.load();
    },

    renderMos: function (value, metadata, record, rowIndex, colIndex, store) {
        return this.renderMetric(record.data.OrigMosText, record.data.OrigMosQuality) + ' / ' + this.renderMetric(record.data.DestMosText, record.data.DestMosQuality);
    },

    renderJitter: function (value, metadata, record, rowIndex, colIndex, store) {
        return this.renderMetric(record.data.OrigJitterText, record.data.OrigJitterQuality) + ' / ' + this.renderMetric(record.data.DestJitterText, record.data.DestJitterQuality);
    },

    renderLatency: function (value, metadata, record, rowIndex, colIndex, store) {
        return this.renderMetric(record.data.OrigLatencyText, record.data.OrigLatencyQuality) + ' / ' + this.renderMetric(record.data.DestLatencyText, record.data.DestLatencyQuality);
    },

    renderPacketLoss: function (value, metadata, record, rowIndex, colIndex, store) {
        return this.renderMetric(record.data.OrigPacketLossText, record.data.OrigPacketLossQuality) + ' / ' + this.renderMetric(record.data.DestPacketLossText, record.data.DestPacketLossQuality);
    },

    renderMetric: function (value, quality) {
        if (quality == 2) {
            return "<span class='criticalValue'>" + value + "</span>";
        }
        if (quality == 1) {
            return "<span class='warningValue'>" + value + "</span>";
        }
        return value;
    },

    renderCallId: function (value) {
    	return value == -1 ? "@{R=VNQM.Strings;K=VNQMLIBCODE_AK1_14;E=js}" : value;
    },

    renderCallStatus: function (value, quality) {
        if (!value) {
            return String.format("<span class='criticalValue'>{0}</span>", "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_107;E=js}");
        }
        return "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_108;E=js}";
    },

    getRowClass: function (record, rowIndex, rp, ds) {
        if (!record.data.CallStatus) {
            return 'failedCallRow';
        }
    },

    Search: function (criteria) {
        this.searchCriteria = criteria;
        this.loadGrid();
    },

    loadGrid: function () {
        if (this.searchCriteria) {
            var groupByItem = this.getGroupByValue();
            //var activePage = Math.ceil((this.pagingToolbarTop.cursor + this.pagingToolbarTop.pageSize) / this.pagingToolbarTop.pageSize);
            this.resultsGrid.getView().hmenu.getComponent('asc').hide();
            this.resultsGrid.getView().hmenu.getComponent('desc').hide();
            this.resultsStore.baseParams = {
                'searchCriteria': this.searchCriteria,
                'groupByCategory': this.groupByCombo.getValue(),
                'groupByValue': groupByItem,
                'orderBy': this.resultsGrid.getStore().getSortState().field,
                'orderDirection': this.resultsGrid.getStore().getSortState().direction,
                'start': 0,
                'limit': this.pagingToolbarTop.pageSize
            };
            if (this.loadingMask == null) {
                this.loadingMask = new Ext.LoadMask(this.resultsGrid.el, {
                    msg: "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_36;E=js}"
                });
            }
            this.resultsGrid.store.on('beforeload', Function.createDelegate(this, function () {
                this.loadingMask.show();
            }));
            this.resultsGrid.store.on('load', Function.createDelegate(this, function () {
                if (this.resultsStore.getTotalCount() == this.resultsLimit) {
                    this.resultsGrid.getTopToolbar().displayMsg = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_109;E=js}', this.resultsLimit) + '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_75;E=js}';
                    this.resultsGrid.getBottomToolbar().displayMsg = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_109;E=js}', this.resultsLimit) + '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_75;E=js}';
                    
                } else {
                    this.resultsGrid.getTopToolbar().displayMsg = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_75;E=js}';
                    this.resultsGrid.getBottomToolbar().displayMsg = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_75;E=js}';
                }

                this.resultsGrid.getTopToolbar().updateInfo();
                this.resultsGrid.getBottomToolbar().updateInfo();
                this.resultsGrid.setDisabled(false);
                this.loadingMask.hide();
                this.updateButtonsState();
                $('#' + this.submitButtonId).removeAttr('searchInProgress');
            }));
            this.resultsGrid.getStore().load();
        }
    },

    onGetSearchResultsLimitSuccess: function (result, context) {
        if (result) {
            this.resultsLimit = result;
            return;
        }
        else {
            Ext.Msg.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_102;E=js}',
                msg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_110;E=js}',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO
            });

        }
        this.updateButtonsState();
    },

    onGetSearchResultsLimitError: function (error, context) {
        this.checkSessionExpired(error);
        if (error.get_statusCode() > 0) {
            Ext.Msg.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}',
                msg: String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_104;E=js}', '<br/><br/>' + error.get_message()),
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR

            });
        }
        this.updateButtonsState();
    }
};

Orion.Voip.Controls.SearchVoipCallsResults.registerClass('Orion.Voip.Controls.SearchVoipCallsResults', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') {
    Sys.Application.notifyScriptLoaded();
}

Ext.state.Manager.setProvider(new Ext.state.VoipExtStateProvider());
