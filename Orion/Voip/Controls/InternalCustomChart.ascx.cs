using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using Period=SolarWinds.Orion.IpSla.Web.Period;
using Periods=SolarWinds.Orion.IpSla.Web.Periods;
using SampleSizes=SolarWinds.Orion.IpSla.Web.Charting.SampleSizes;

using Keys = SolarWinds.Orion.IpSla.Web.GraphResource.Keys;

public partial class Orion_Voip_Controls_InternalCustomChart : UserControl
{
    private const int DefaultWidth = 640;
    private const string CustomText = "Custom";
    private bool enableEditing = true;
    private DataSet chartData;
    private IpSlaChartInfo chartInfo;

    public bool EnableEditing
    {
        get { return enableEditing; }
        set { enableEditing = value; }
    }
    
    public DataSet ChartData
    {
        get { return chartData; }
        set { chartData = value; }
    }

    public IpSlaChartInfo ChartInfo
    {
        get { return chartInfo; }
        set { chartInfo = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Filling dropdown list with periods
            TimePeriodsList.DataSource = Periods.GetTimePeriodList();
            TimePeriodsList.DataTextField = "DisplayName";
            TimePeriodsList.DataValueField = "Name";
            TimePeriodsList.DataBind();
            TimePeriodsList.Items.Add(new ListItem(VNQMWebContent.VNQMWEBDATA_VB1_20, CustomText));

            SampleSizeList.DataSource = SampleSizes.GenerateSelectionList();
            SampleSizeList.DataTextField = "DisplayName";
            SampleSizeList.DataValueField = "SizeName";
            SampleSizeList.DataBind();

            editControlsContainer.Visible = enableEditing;

            // Create the chart info with the default values.
            this.ChartInfo = GetDefaultChartInfo();

            // Load the chart info with value from the URL
            UpdateInfoFromRequest(this.ChartInfo);

            SampleSizeList.SelectedValue = this.ChartInfo.SampleSize;
            Width.Text = this.ChartInfo.Width.ToString();
            Height.Text = this.ChartInfo.Height.ToString();

            this.ChartData = this.ChartInfo.LoadData();

            ChartBase chart = this.ChartInfo.CreateChart();

            chart.DataBind(this.ChartData);

            chartContainer.Style["Width"] = String.Format("{0}px;", this.ChartInfo.Width);
            chartContainer.Controls.Add(chart);

            // Set the Export Url correctly.
            RawDataLinkTop.NavigateUrl = BuildUrl(RawDataLinkTop.NavigateUrl);
            RawDataLinkBottom.NavigateUrl = BuildUrl(RawDataLinkBottom.NavigateUrl);
        }
        AddTopRightLinks();
    }

    private void AddTopRightLinks()
    {
        if (this.Page.Master == null) return;

        try
        {

            var ctrl = ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinks");
            var div= ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinksDiv");
            if (ctrl == null || div==null) return;
            div.Visible = false;
            
            ctrl.Controls.Clear();
            ctrl.Controls.Add(new HyperLink
            {
                ID = "Img2",
                NavigateUrl = BuildUrl("/Orion/Voip/Resources/ViewChart.aspx"),
                CssClass = "printablePageLink",
                Text = VNQMWebContent.VNQMWEBCODE_TM0_2,
                EnableViewState = false
            });

            ctrl.Controls.Add(new ASP.IconHelpButton
            {
                ID = "HelpButton",
                HelpUrlFragment = "OrionVoipMonitorPHCustomChartPage-CustomizingOperations",
                EnableViewState = false
            });
        }
        catch (System.Web.HttpException)
        {
            // -- shouldn't occur --
            // the page may be using this control and incompatible - having control blocks in a content section.
            // if so, this exception will occur. we'd rather default to not having the links show up than see an error page.
            // -- shouldn't occur --
        }
    }

    private IpSlaChartInfo GetDefaultChartInfo()
    {
        var info = VoipChartInfoFactory.Create(this.ChartName);
        if (this.NetObjectId != null)
        {
            var netObject = NetObjectFactory.Create(this.NetObjectId);
            var operation = netObject as IpSlaOperation;
            if (operation != null)
            {
                int intValue;
                if (int.TryParse(Request.Params[Keys.Path], out intValue))
                {
                    operation.PathId = intValue;
                }
                if (int.TryParse(Request.Params[Keys.Hop], out intValue))
                {
                    operation.PathHopIndex = intValue - 1;
                }
            }
            info.NetObject = netObject;
            info.Title = info.NetObject.Name;
        }
        info.Width = DefaultWidth;
        info.SubTitle = info.DisplayName;
        info.SubTitle2 = info.Period;
        return info;
    }

    private void UpdateInfoFromRequest(IpSlaChartInfo info)
    {
        SetPeriodValue(info);
        info.SubTitle2 = info.Period;

        if (!String.IsNullOrEmpty(Request.Params[Keys.Title]))
        {
            info.Title = Request.Params[Keys.Title];
            ChartTitle.Text = Request.Params[Keys.Title];
        }

        if (!String.IsNullOrEmpty(Request.Params[Keys.SubTitle]))
        {
            info.SubTitle = Request.Params[Keys.SubTitle];
            SubTitle.Text = Request.Params[Keys.SubTitle];
        }
        
        if (!String.IsNullOrEmpty(Request.Params[Keys.SubTitle2]))
        {
            info.SubTitle2 = Request.Params[Keys.SubTitle2];
            SubTitle2.Text = Request.Params[Keys.SubTitle2];
        }

        if (!String.IsNullOrEmpty(Request.Params[Keys.SampleSize]))
        {
            info.SampleSize = Request.Params[Keys.SampleSize];
        }

        int intValue;

        if (int.TryParse(Request.Params[Keys.Width], out intValue))
        {
            info.Width = intValue;
        }

        if (int.TryParse(Request.Params[Keys.Height], out intValue))
        {
            info.Height = intValue;
        }
    }

    private void SetPeriodValue(IpSlaChartInfo info)
    {
        string period;
        if (!String.IsNullOrEmpty(period = Request.Params[Keys.Period]))
        {
            if (period == CustomText)
            {
                TimePeriodsList.SelectedValue = period;
            }
            else
            {
                Period p = Periods.GetPeriod(period);
                if (p != null)
                {
                    TimePeriodsList.SelectedValue = p.Name;
                }
                else
                {
                    TimePeriodsList.SelectedValue = info.Period;
                }
            }
        }
        else
        {
            // set the default value.
            TimePeriodsList.SelectedValue = info.Period;
        }

        if (!String.IsNullOrEmpty(Request.Params[Keys.PeriodBegin]))
        {
            PeriodBegin.Text = Request.Params[Keys.PeriodBegin];
        }

        if (!String.IsNullOrEmpty(Request.Params[Keys.PeriodEnd]))
        {
            PeriodEnd.Text = Request.Params[Keys.PeriodEnd];
        }

        // If we have a custom start and end date, make sure we swith the combo to "custom"
        if ((!string.IsNullOrEmpty(PeriodBegin.Text)) && (!string.IsNullOrEmpty(PeriodEnd.Text)))
        {
            TimePeriodsList.SelectedValue = CustomText;
        }

        string periodSetting = TimePeriodsList.SelectedValue;

        if (periodSetting == CustomText)
        {
            // Show any error messages for the start and end dates
            PeriodBeginError.Visible = !IsDateValid(PeriodBegin.Text);
            PeriodEndError.Visible = !IsDateValid(PeriodEnd.Text);

            // They selected the custom text value.  Use the Period Begin and End values
            DateTime periodBegin;
            DateTime periodEnd;

            if (DateTime.TryParse(PeriodBegin.Text, out periodBegin) &&
                 DateTime.TryParse(PeriodEnd.Text, out periodEnd))
            {
                periodSetting = Periods.GetCustomPeriodString(periodBegin, periodEnd);
            }
            else
            {
                periodSetting = Periods.DEFAULT_PERIOD_VALUE;
            }
        }

        info.Period = periodSetting;
    }

    public string ChartName
    {
        get
        {
            return Request.Params["ChartName"];
        }
    }

    public string NetObjectId
    {
        get { return Request.Params[Keys.NetObject]; }
    }

    public string PathId
    {
        get { return Request.Params[Keys.Path]; }
    }

    public string PathHop
    {
        get { return Request.Params[Keys.Hop]; }
    }

    protected void OnRefresh(object sender, EventArgs e)
    {
        Response.Redirect(BuildUrl(Request.Url.AbsolutePath));
    }

    private string BuildUrl(string basePage)
    {
        var builder = new UrlBuilder(basePage);

        builder[Keys.ChartName] = this.ChartName;
        builder[Keys.NetObject] = this.NetObjectId;
        builder[Keys.Path] = this.PathId;
        builder[Keys.Hop] = this.PathHop;

        if (!String.IsNullOrEmpty(ChartTitle.Text))
            builder[Keys.Title] = ChartTitle.Text;

        if (!String.IsNullOrEmpty(SubTitle.Text))
            builder[Keys.SubTitle] = SubTitle.Text;

        if (!String.IsNullOrEmpty(SubTitle2.Text))
            builder[Keys.SubTitle2] = SubTitle2.Text;

        bool isCustom = TimePeriodsList.SelectedValue == CustomText;
        builder[Keys.Period] = TimePeriodsList.SelectedValue;

        if (isCustom)
        {
            if (!String.IsNullOrEmpty(PeriodBegin.Text))
                builder[Keys.PeriodBegin] = PeriodBegin.Text;

            if (!String.IsNullOrEmpty(PeriodEnd.Text))
                builder[Keys.PeriodEnd] = PeriodEnd.Text;
        }

        if (!String.IsNullOrEmpty(SampleSizeList.SelectedValue))
            builder[Keys.SampleSize] = SampleSizeList.SelectedValue;

        if (!String.IsNullOrEmpty(Width.Text))
            builder[Keys.Width] = Width.Text;

        if (!String.IsNullOrEmpty(Height.Text))
            builder[Keys.Height] = Height.Text;

        return builder.Url;
    }

    protected static string DateTimeSyntaxExamples()
    {
        //<b>1/8/2008</b> or <b>08-Jan-08</b> or <b>11:58 AM</b> or <b>1/8/2008 11:58:10 AM</b> or <b>Jan 08,2008 11:58</b>
        DateTime dt = DateTime.Now;

        // 'dt' is already in local time (by specification of DateTime.Now property)
        return String.Format(VNQMWebContent.VNQMWEBCODE_TM0_3,
                            "<b>" + dt.ToShortDateString() + "</b>",
                            "<b>" + dt.ToShortTimeString() + "</b>",
                            "<b>" + dt + "</b>");
    }

    protected static bool IsDateValid(string text)
    {
        DateTime dt;
        return DateTime.TryParse(text, out dt);
    }
}
