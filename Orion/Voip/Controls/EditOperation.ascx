﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditOperation.ascx.cs" Inherits="Orion_Voip_Controls_EditOperation" %>

<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI" TagPrefix="voip" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI.OperationParameters" TagPrefix="op" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI.Thresholds" TagPrefix="ot" %>

<table class="Parameters" border="0" cellpadding="0" cellspacing="0">
    <op:OperationName runat="server" ID="opOperationName" />
    <op:Description runat="server" ID="opDescription" />
    <op:Frequency runat="server" ID="opFrequency" />
    <op:HttpUrl runat="server" ID="opHttpUrl" />
    <op:FtpUrl runat="server" ID="opFtpUrl" />
    <op:DhcpServer runat="server" ID="opDhcpServer" />
    <op:DnsServer runat="server" ID="opDnsServer" />
    <op:DnsNameToResolve runat="server" ID="opDnsNameToResolve" />
    <op:Port runat="server" ID="opPort" />
    <op:OperationTag runat="server" ID="opTag" />
    <op:OperationOwner runat="server" ID="opOwner" />
</table>
<br />
<table class="Thresholds" border="0" cellpadding="0" cellspacing="0">
    <tr runat="server" id="ThresholdHeader">
        <td colspan="2">&nbsp;</td>
        <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_35 %></td>
        <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_37 %></td>
        <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_48 %></td>
    </tr>
    <ot:RoundTripTime runat="server" id="RTTThreshold" />
    <ot:MOS runat="server" id="MOSThreshold" />
    <ot:Jitter runat="server" id="JitterThreshold" />
    <ot:Latency runat="server" id="LatencyThreshold" />
    <ot:PacketLoss runat="server" id="PacketLossThreshold" />
    <ot:NumberOfHops runat="server" id="NumberOfHopsThreshold" />
</table>
<br />
<voip:VoipCollapsePanel ID="AdvancedPanel" runat="server" Collapsed="true">
    <titletemplate>
        <span><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_49 %></span>
    </titletemplate>
    <blocktemplate>
        <table class="AdvancedParameters">
            <op:CodecTypeSelect runat="server" id="opCodecTypeSelect" />
            <op:TypeOfService runat="server" id="opTypeOfService" />
            <op:VrfName runat="server" id="opVrfName" />
            <op:SourceIpAddress runat="server" id="opSourceIpAddress" />
            <op:TargetIpAddress runat="server" id="opTargetIpAddress" />
            <op:ControlEnable runat="server" id="opControlEnable" />
        </table>
    </blocktemplate>
</voip:VoipCollapsePanel>





